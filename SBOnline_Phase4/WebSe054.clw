

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE054.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE055.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE057.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE059.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateProgress       PROCEDURE  (NetWebServerWorker p_web,LONG fRecords,STRING fReturnURL,<STRING fTitleText>) ! Declare Procedure
p_packet            STRING(255)
packetlen           long
  CODE
    p_packet =     '<div id="waitframe" class="nt-process">' & |
        '<p class="nt-process-text">Please wait while ServiceBase searches for matching records<br /><br/>'& |
        'This may take a few seconds....<br/><br/>'

    p_packet = CLIP(p_packet) & CLIP(fTitleText) & '<br/>'

    pushpack(p_web,CLIP(p_packet) & |
        '<br/><script type="text/javascript">drawProgressBar(''#ff0000'', 250, ' & fRecords & ');</script>' & |
        '</p>'& |
        '<p>' & |
        '<a href="' & CLIP(fReturnURL) & '">Cancel Process</a>' & |
        '</p>' & |
        '</div>')



brwPendingWebOrder   PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
tag:tagged:IsInvalid  Long
orw:PartNumber:IsInvalid  Long
orw:Description:IsInvalid  Long
orw:Quantity:IsInvalid  Long
Change5:IsInvalid  Long
Delete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(ORDWEBPR)
                      Project(orw:RecordNumber)
                      Project(orw:PartNumber)
                      Project(orw:Description)
                      Project(orw:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
TagFile::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('brwPendingWebOrder')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('brwPendingWebOrder:NoForm')
      loc:NoForm = p_web.GetValue('brwPendingWebOrder:NoForm')
      loc:FormName = p_web.GetValue('brwPendingWebOrder:FormName')
    else
      loc:FormName = 'brwPendingWebOrder_frm'
    End
    p_web.SSV('brwPendingWebOrder:NoForm',loc:NoForm)
    p_web.SSV('brwPendingWebOrder:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('brwPendingWebOrder:NoForm')
    loc:FormName = p_web.GSV('brwPendingWebOrder:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('brwPendingWebOrder') & '_' & lower(loc:parent)
  else
    loc:divname = lower('brwPendingWebOrder')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'brwPendingWebOrder' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','brwPendingWebOrder')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('brwPendingWebOrder') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('brwPendingWebOrder')
      p_web.DivHeader('popup_brwPendingWebOrder','nt-hidden')
      p_web.DivHeader('brwPendingWebOrder',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_brwPendingWebOrder_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_brwPendingWebOrder_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('brwPendingWebOrder',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ORDWEBPR,orw:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'ORW:PARTNUMBER') then p_web.SetValue('brwPendingWebOrder_sort','1')
    ElsIf (loc:vorder = 'ORW:DESCRIPTION') then p_web.SetValue('brwPendingWebOrder_sort','2')
    ElsIf (loc:vorder = 'ORW:QUANTITY') then p_web.SetValue('brwPendingWebOrder_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('brwPendingWebOrder:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('brwPendingWebOrder:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('brwPendingWebOrder:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('brwPendingWebOrder:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'frmUpdateWebOrder'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('brwPendingWebOrder')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('brwPendingWebOrder_sort',net:DontEvaluate)
  p_web.SetSessionValue('brwPendingWebOrder_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(orw:PartNumber)','-UPPER(orw:PartNumber)')
    Loc:LocateField = 'orw:PartNumber'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(orw:Description)','-UPPER(orw:Description)')
    Loc:LocateField = 'orw:Description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'orw:Quantity','-orw:Quantity')
    Loc:LocateField = 'orw:Quantity'
    Loc:LocatorCase = 0
  of 5
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 6
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(orw:AccountNumber),+UPPER(orw:PartNumber)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('brwPendingWebOrder_LocatorPic','@n3')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('orw:PartNumber')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('brwPendingWebOrder_LocatorPic','@s30')
  Of upper('orw:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('brwPendingWebOrder_LocatorPic','@s30')
  Of upper('orw:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('brwPendingWebOrder_LocatorPic','@s8')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('brwPendingWebOrder:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="brwPendingWebOrder:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="brwPendingWebOrder:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('brwPendingWebOrder:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ORDWEBPR"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="orw:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{brwPendingWebOrder.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwPendingWebOrder',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2brwPendingWebOrder','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwPendingWebOrder_LocatorPic'),,,'onchange="brwPendingWebOrder.locate(''Locator2brwPendingWebOrder'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2brwPendingWebOrder',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwPendingWebOrder.locate(''Locator2brwPendingWebOrder'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="brwPendingWebOrder_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwPendingWebOrder.cl(''brwPendingWebOrder'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'brwPendingWebOrder_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="brwPendingWebOrder_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="brwPendingWebOrder_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','brwPendingWebOrder',p_web.Translate(''),'Click here to sort by tagged',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','brwPendingWebOrder',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','brwPendingWebOrder',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','brwPendingWebOrder',p_web.Translate('Quantity'),'Click here to sort by Quantity',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','brwPendingWebOrder',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','brwPendingWebOrder',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('orw:recordnumber',lower(loc:vorder),1,1) = 0 !and ORDWEBPR{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','orw:RecordNumber',clip(loc:vorder) & ',' & 'orw:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('orw:RecordNumber'),p_web.GetValue('orw:RecordNumber'),p_web.GetSessionValue('orw:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'UPPER(orw:AccountNumber) = UPPER(''' & p_web.GSV('BookingStoresAccount') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwPendingWebOrder',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('brwPendingWebOrder_Filter')
    p_web.SetSessionValue('brwPendingWebOrder_FirstValue','')
    p_web.SetSessionValue('brwPendingWebOrder_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ORDWEBPR,orw:RecordNumberKey,loc:PageRows,'brwPendingWebOrder',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ORDWEBPR{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ORDWEBPR,loc:firstvalue)
              Reset(ThisView,ORDWEBPR)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ORDWEBPR{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ORDWEBPR,loc:lastvalue)
            Reset(ThisView,ORDWEBPR)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(orw:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwPendingWebOrder_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwPendingWebOrder.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwPendingWebOrder.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwPendingWebOrder.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwPendingWebOrder.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwPendingWebOrder_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="brwPendingWebOrder_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'brwPendingWebOrder_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwPendingWebOrder',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('brwPendingWebOrder_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('brwPendingWebOrder_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1brwPendingWebOrder','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwPendingWebOrder_LocatorPic'),,,'onchange="brwPendingWebOrder.locate(''Locator1brwPendingWebOrder'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1brwPendingWebOrder',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwPendingWebOrder.locate(''Locator1brwPendingWebOrder'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="brwPendingWebOrder_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwPendingWebOrder.cl(''brwPendingWebOrder'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwPendingWebOrder_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('brwPendingWebOrder_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('brwPendingWebOrder_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwPendingWebOrder_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwPendingWebOrder.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwPendingWebOrder.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwPendingWebOrder.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwPendingWebOrder.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwPendingWebOrder_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="brwPendingWebOrder_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'brwPendingWebOrder_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    TAG:SessionId = p_web.SessionId
    tag:taggedValue = orw:PartNumber
    Access:TagFile.Fetch(tag:keyID)
    loc:field = p_web.AddBrowseValue('brwPendingWebOrder','ORDWEBPR',orw:RecordNumberKey) !orw:RecordNumber
    p_web._thisrow = p_web._nocolon('orw:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and orw:RecordNumber = p_web.GetValue('orw:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('brwPendingWebOrder:LookupField')) = orw:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((orw:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('brwPendingWebOrder','ORDWEBPR',orw:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ORDWEBPR{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ORDWEBPR)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ORDWEBPR{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ORDWEBPR)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::orw:PartNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::orw:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::orw:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Change5
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('brwPendingWebOrder','ORDWEBPR',orw:RecordNumberKey)
  TableQueue.Id[1] = orw:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btibrwPendingWebOrder;if (btibrwPendingWebOrder != 1){{var brwPendingWebOrder=new browseTable(''brwPendingWebOrder'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('orw:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''frmUpdateWebOrder'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'brwPendingWebOrder.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'brwPendingWebOrder.applyGreenBar();btibrwPendingWebOrder=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwPendingWebOrder')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwPendingWebOrder')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwPendingWebOrder')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwPendingWebOrder')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ORDWEBPR)
  p_web._CloseFile(TagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ORDWEBPR)
  Bind(orw:Record)
  Clear(orw:Record)
  NetWebSetSessionPics(p_web,ORDWEBPR)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('orw:RecordNumber',p_web.GetValue('orw:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  orw:RecordNumber = p_web.GSV('orw:RecordNumber')
  loc:result = p_web._GetFile(ORDWEBPR,orw:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(orw:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(ORDWEBPR)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(ORDWEBPR)
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  orw:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(ORDWEBPR,orw:RecordNumberKey)
  p_web.FileToSessionQueue(ORDWEBPR)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = true,true,false)
  ! Validation goes here, if fails set loc:invalid & loc:alert
  tag:sessionID = p_web.SessionId
  tag:taggedValue = orw:PartNumber
  If Access:TagFile.TryFetch(tag:keyTagged) = 0
      TAG:Tagged = Choose(p_web.GetValue('value') = 1,true,false)
      Access:TagFile.Update()
      p_web._trace('record updated')
  else
      TAG:Tagged = Choose(p_web.GetValue('value') = 1,true,false)
      Access:TagFile.Insert()
      p_web._trace('record inserted')
  end
  
  
  if loc:invalid = ''
    p_web.SetSessionValue('tag:tagged',tag:tagged)
    do CheckForDuplicate
  Else
    tag:tagged = loc:was
  End
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  Else
    do SendAlert
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  Loc:Eip = 0
  do CallBrowse
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_tag:tagged_'&orw:RecordNumber,,net:crc,,loc:extra)
      If Loc:ViewOnly = 0
          loc:fieldclass = ''
          loc:extra = Choose(tag:tagged = true,'checked','')
              loc:javascript = 'onchange="brwPendingWebOrder.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"'
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&orw:RecordNumber),clip(true),p_web.combine(p_web.site.style.browseCheck,,loc:fieldclass),loc:extra,,,loc:javascript,,) & '<13,10>'
      Else
          loc:fieldclass = ''
          loc:extra = Choose(tag:tagged = true,'checked','')
              loc:javascript = 'onchange="brwPendingWebOrder.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"'
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&orw:RecordNumber),clip(true),p_web.combine(p_web.site.style.browseCheck,,loc:fieldclass),loc:extra & ' disabled',,,loc:javascript,,) & '<13,10>'
      End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::orw:PartNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_orw:PartNumber_'&orw:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(orw:PartNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::orw:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_orw:Description_'&orw:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(orw:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::orw:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_orw:Quantity_'&orw:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(orw:Quantity,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Change5   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_Change5_'&orw:RecordNumber,,net:crc,,loc:extra)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'brwPendingWebOrder',p_web.AddBrowseValue('brwPendingWebOrder','ORDWEBPR',orw:RecordNumberKey),,loc:FormPopup,'frmUpdateWebOrder') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwPendingWebOrder_Delete_'&orw:RecordNumber,,net:crc,,loc:extra)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'brwPendingWebOrder',p_web.AddBrowseValue('brwPendingWebOrder','ORDWEBPR',orw:RecordNumberKey),,loc:FormPopup,'frmUpdateWebOrder') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('orw:RecordNumber',orw:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
frmMakeTaggedOrders  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWebOrderNumber    LONG                                  !
FilesOpened     Long
ORDHEAD::State  USHORT
ORDITEMS::State  USHORT
TagFile::State  USHORT
ORDWEBPR::State  USHORT
txtError:IsInvalid  Long
txtMessage:IsInvalid  Long
btnPrintOrder:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmMakeTaggedOrders')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmMakeTaggedOrders_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmMakeTaggedOrders','')
    p_web.DivHeader('frmMakeTaggedOrders',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmMakeTaggedOrders') = 0
        p_web.AddPreCall('frmMakeTaggedOrders')
        p_web.DivHeader('popup_frmMakeTaggedOrders','nt-hidden')
        p_web.DivHeader('frmMakeTaggedOrders',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(300)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmMakeTaggedOrders_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmMakeTaggedOrders_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmMakeTaggedOrders')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ORDHEAD)
  p_web._OpenFile(ORDITEMS)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(ORDWEBPR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ORDHEAD)
  p_Web._CloseFile(ORDITEMS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(ORDWEBPR)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmMakeTaggedOrders_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmMakeTaggedOrders'
    end
    p_web.formsettings.proc = 'frmMakeTaggedOrders'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  If CheckIfTagged(p_web) = 0
    loc:TabNumber += 1
  End
  If CheckIfTagged(p_web) = 1
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmMakeTaggedOrders_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmMakeTaggedOrders')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmMakeTaggedOrders_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmMakeTaggedOrders_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmMakeTaggedOrders_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Create Order'
      p_web.site.SaveButton.Class = 'DoubleButton'
    IF (CheckIfTagged(p_web) = 0)
        p_web.site.SaveButton.Class = 'NoShow'
    END
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Create Web Order') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Create Web Order',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmMakeTaggedOrders',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If CheckIfTagged(p_web) = 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmMakeTaggedOrders0_div')&'">'&p_web.Translate('')&'</a></li>'& CRLF
      End
      If CheckIfTagged(p_web) = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmMakeTaggedOrders1_div')&'">'&p_web.Translate('')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmMakeTaggedOrders_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmMakeTaggedOrders_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmMakeTaggedOrders_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmMakeTaggedOrders_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmMakeTaggedOrders_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf CheckIfTagged(p_web) = 0
    ElsIf CheckIfTagged(p_web) = 1
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmMakeTaggedOrders')>0,p_web.GSV('showtab_frmMakeTaggedOrders'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmMakeTaggedOrders'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmMakeTaggedOrders') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmMakeTaggedOrders'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmMakeTaggedOrders')>0,p_web.GSV('showtab_frmMakeTaggedOrders'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmMakeTaggedOrders') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If CheckIfTagged(p_web) = 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('') & ''''
      End
      If CheckIfTagged(p_web) = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmMakeTaggedOrders_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmMakeTaggedOrders')>0,p_web.GSV('showtab_frmMakeTaggedOrders'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmMakeTaggedOrders",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmMakeTaggedOrders')>0,p_web.GSV('showtab_frmMakeTaggedOrders'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmMakeTaggedOrders_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmMakeTaggedOrders') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmMakeTaggedOrders')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If CheckIfTagged(p_web) = 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtError
        do Comment::txtError
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If CheckIfTagged(p_web) = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmMakeTaggedOrders1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtMessage
        do Comment::txtMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnPrintOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnPrintOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Validate::txtError  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtError  ! copies value to session value if valid.
  do Comment::txtError ! allows comment style to be updated.

ValidateValue::txtError  Routine
    If not (1=0)
    End

Value::txtError  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('txtError') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtError" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate('You have not tagged any parts',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtError  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtError:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('txtError') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtMessage  ! copies value to session value if valid.
  do Comment::txtMessage ! allows comment style to be updated.

ValidateValue::txtMessage  Routine
    If not (1=0)
    End

Value::txtMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('txtMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtMessage" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Print Order, the click "Create Order" to complete the process.',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('txtMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnPrintOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnPrintOrder  ! copies value to session value if valid.
  do Comment::btnPrintOrder ! allows comment style to be updated.

ValidateValue::btnPrintOrder  Routine
    If not (1=0)
    End

Value::btnPrintOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('btnPrintOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnPrintOrder','Print Order',p_web.combine(Choose('Print Order' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('PendingWebOrderReport'&'','_blank'),,loc:disabled,'/images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnPrintOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnPrintOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('btnPrintOrder') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmMakeTaggedOrders_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('frmMakeTaggedOrders_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmMakeTaggedOrders_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmMakeTaggedOrders_tab_' & 0)
    do GenerateTab0
  of lower('frmMakeTaggedOrders_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)

  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmMakeTaggedOrders:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmMakeTaggedOrders:Primed',0)
  p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If CheckIfTagged(p_web) = 0
  End
  If CheckIfTagged(p_web) = 1
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmMakeTaggedOrders_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
  
      locWebOrderNumber = 0
      Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
      orh:procesed    = 0
      Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
      Loop
          If Access:ORDHEAD.NEXT()
              Break
          End !If
          If orh:procesed    <> 0      |
              Then Break.  ! End If
          If orh:Account_No = p_web.GSV('BookingStoresAccount')
              locWebOrderNumber = orh:Order_no
              Break
          End !If orh:Account_No = job:Account_Number
      End !Loop
  
      Access:ORDWEBPR.ClearKey(orw:PartNumberKey)
      orw:AccountNumber = p_web.GSV('BookingStoresAccount')
      orw:PartNumber = tag:taggedValue
      If Access:ORDWEBPR.TryFetch(orw:PartNumberKey) = Level:Benign
                      !Found
          If (locWebOrderNumber > 0)
                          !This is an order raised for this account,
                          !now lets see if the same part has been ordered and add to that
              Found# = 0
              Access:ORDITEMS.ClearKey(ori:Keyordhno)
              ori:ordhno = locWebOrderNumber
              Set(ori:Keyordhno,ori:Keyordhno)
              Loop
                  If Access:ORDITEMS.NEXT()
                      Break
                  End !If
                  If ori:ordhno <> locWebOrderNumber      |
                      Then Break.  ! End If
                  If ori:partno = orw:PartNumber And ori:partdiscription = orw:Description
                      ori:qty += orw:Quantity
                      Access:ORDITEMS.Update()
                      Found# = 1
                      Break
                  End !ori:partdiscription = wpr:Description
              End !Loop
  
              If Found# = 0
                              !Make a new part line
                  If Access:ORDITEMS.PrimeRecord() = Level:Benign
                      ori:ordhno          = locWebOrderNumber
                                  !ori:manufact        = job:Manufacturer
                      ori:qty             = orw:Quantity
                      ori:itemcost        = orw:ItemCost
                      ori:totalcost       = orw:ItemCost * orw:Quantity
                      ori:partno          = orw:PartNumber
                      ori:partdiscription = orw:Description
                      If Access:ORDITEMS.TryInsert() = Level:Benign
                                      !Insert Successful
                      Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                                      !Insert Failed
                      End !If Access:ORDITEMS.TryInsert() = Level:Benign
                  End !If Access:ORDITEMS.PrimeRecord() = Level:Benign
              End !If Found# = 0
          Else !If tmp:WebOrderNumber
                          !There is no existing orders for this account
              If Access:ORDHEAD.PrimeRecord() = Level:Benign
                  orh:account_no      = p_web.GSV('BookingStoresAccount')
  
                              !Get Sub or Trade Account details?
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = p_web.GSV('BookingAccount')
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                          !Found
                          orh:CustName        = tra:Company_Name
                          orh:CustAdd1        = tra:Address_Line1
                          orh:CustAdd2        = tra:Address_Line2
                          orh:CustAdd3        = tra:Address_Line3
                          orh:CustPostCode    = tra:Postcode
                          orh:CustTel         = tra:Telephone_Number
                          orh:CustFax         = tra:Fax_Number
  
                          orh:dName           = tra:Company_Name
                          orh:dAdd1           = tra:Address_Line1
                          orh:dAdd2           = tra:Address_Line2
                          orh:dAdd3           = tra:Address_Line3
                          orh:dPostCode       = tra:Postcode
                          orh:dTel            = tra:Telephone_Number
                          orh:dFax            = tra:Fax_Number
  
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
                  orh:thedate         = Today()
                  orh:thetime         = Clock()
                  orh:procesed        = 0
                  orh:WhoBooked       = p_web.GSV('BookingUserCode')
                  orh:Department      = ''
                  orh:CustOrderNumber = ''
                  If Access:ORDHEAD.TryInsert() = Level:Benign
                                  !Insert Successful
                                  !Make a new part line
                      If Access:ORDITEMS.PrimeRecord() = Level:Benign
                          ori:ordhno          = orh:Order_No
                                      !ori:manufact        = job:Manufacturer
                          ori:qty             = orw:Quantity
                          ori:itemcost        = orw:ItemCost
                          ori:totalcost       = orw:ItemCost * orw:Quantity
                          ori:partno          = orw:PartNumber
                          ori:partdiscription = orw:Description
                          If Access:ORDITEMS.TryInsert() = Level:Benign
                                          !Insert Successful
                          Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                                          !Insert Failed
                          End !If Access:ORDITEMS.TryInsert() = Level:Benign
                      End !If Access:ORDITEMS.PrimeRecord() = Level:Benign
  
                  Else !If Access:ORDHEAD.TryInsert() = Level:Benign
                                  !Insert Failed
                  End !If Access:ORDHEAD.TryInsert() = Level:Benign
              End !If Access:ORDHEAD.PrimeRecord() = Level:Benign
          End !If tmp:WebOrderNumber
          Delete(ORDWEBPR)
      Else!If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
  End !Loop x# = 1 To Records(glo:Queue)
  
  ClearTagFile(p_web)
  p_web.DeleteSessionValue('frmMakeTaggedOrders_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If CheckIfTagged(p_web) = 0
    loc:InvalidTab += 1
    do ValidateValue::txtError
    If loc:Invalid then exit.
  End
  ! tab = 2
  If CheckIfTagged(p_web) = 1
    loc:InvalidTab += 1
    do ValidateValue::txtMessage
    If loc:Invalid then exit.
    do ValidateValue::btnPrintOrder
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmMakeTaggedOrders:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locWebOrderNumber',locWebOrderNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locWebOrderNumber = p_web.GSV('locWebOrderNumber') ! LONG
brwStockReceive      PROCEDURE  (NetWebServerWorker p_web)
locStatus            STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
tag:tagged:IsInvalid  Long
stotmp:PartNumber:IsInvalid  Long
stotmp:Description:IsInvalid  Long
stotmp:ItemCost:IsInvalid  Long
stotmp:Quantity:IsInvalid  Long
stotmp:QuantityReceived:IsInvalid  Long
locStock:IsInvalid  Long
stotmp:ExchangeOrder:IsInvalid  Long
stotmp:LoanOrder:IsInvalid  Long
locStatus:IsInvalid  Long
Other7:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOCKRECEIVETMP)
                      Project(stotmp:RecordNumber)
                      Project(stotmp:PartNumber)
                      Project(stotmp:Description)
                      Project(stotmp:ItemCost)
                      Project(stotmp:Quantity)
                      Project(stotmp:QuantityReceived)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
TagFile::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('brwStockReceive')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('brwStockReceive:NoForm')
      loc:NoForm = p_web.GetValue('brwStockReceive:NoForm')
      loc:FormName = p_web.GetValue('brwStockReceive:FormName')
    else
      loc:FormName = 'brwStockReceive_frm'
    End
    p_web.SSV('brwStockReceive:NoForm',loc:NoForm)
    p_web.SSV('brwStockReceive:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('brwStockReceive:NoForm')
    loc:FormName = p_web.GSV('brwStockReceive:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('brwStockReceive') & '_' & lower(loc:parent)
  else
    loc:divname = lower('brwStockReceive')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'brwStockReceive' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','brwStockReceive')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('brwStockReceive') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('brwStockReceive')
      p_web.DivHeader('popup_brwStockReceive','nt-hidden')
      p_web.DivHeader('brwStockReceive',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_brwStockReceive_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_brwStockReceive_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('brwStockReceive',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOCKRECEIVETMP,stotmp:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TAG:TAGGED') then p_web.SetValue('brwStockReceive_sort','1')
    ElsIf (loc:vorder = 'STOTMP:PARTNUMBER') then p_web.SetValue('brwStockReceive_sort','2')
    ElsIf (loc:vorder = 'STOTMP:DESCRIPTION') then p_web.SetValue('brwStockReceive_sort','3')
    ElsIf (loc:vorder = 'STOTMP:ITEMCOST') then p_web.SetValue('brwStockReceive_sort','4')
    ElsIf (loc:vorder = 'STOTMP:QUANTITY') then p_web.SetValue('brwStockReceive_sort','5')
    ElsIf (loc:vorder = 'STOTMP:QUANTITYRECEIVED') then p_web.SetValue('brwStockReceive_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('brwStockReceive:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('brwStockReceive:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('brwStockReceive:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('brwStockReceive:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'brwStockReceive'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('brwStockReceive')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('brwStockReceive_sort',net:DontEvaluate)
  p_web.SetSessionValue('brwStockReceive_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'tag:tagged','-tag:tagged')
    Loc:LocateField = 'tag:tagged'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stotmp:PartNumber)','-UPPER(stotmp:PartNumber)')
    Loc:LocateField = 'stotmp:PartNumber'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stotmp:Description)','-UPPER(stotmp:Description)')
    Loc:LocateField = 'stotmp:Description'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'stotmp:ItemCost','-stotmp:ItemCost')
    Loc:LocateField = 'stotmp:ItemCost'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'stotmp:Quantity','-stotmp:Quantity')
    Loc:LocateField = 'stotmp:Quantity'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'stotmp:QuantityReceived','-stotmp:QuantityReceived')
    Loc:LocateField = 'stotmp:QuantityReceived'
    Loc:LocatorCase = 0
  of 7
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+stotmp:SessionID,+UPPER(stotmp:PartNumber)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('brwStockReceive_LocatorPic','@n3')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('stotmp:PartNumber')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('brwStockReceive_LocatorPic','@s30')
  Of upper('stotmp:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('brwStockReceive_LocatorPic','@s30')
  Of upper('stotmp:ItemCost')
    loc:SortHeader = p_web.Translate('Item Cost')
    p_web.SetSessionValue('brwStockReceive_LocatorPic','@n10.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('stotmp:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('brwStockReceive_LocatorPic','@n-14')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('stotmp:QuantityReceived')
    loc:SortHeader = p_web.Translate('Quantity Rcvd')
    p_web.SetSessionValue('brwStockReceive_LocatorPic','@n-14')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('brwStockReceive:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="brwStockReceive:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="brwStockReceive:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('brwStockReceive:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOCKRECEIVETMP"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stotmp:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{brwStockReceive.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockReceive',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2brwStockReceive','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwStockReceive_LocatorPic'),,,'onchange="brwStockReceive.locate(''Locator2brwStockReceive'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2brwStockReceive',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwStockReceive.locate(''Locator2brwStockReceive'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="brwStockReceive_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockReceive.cl(''brwStockReceive'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'brwStockReceive_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('brwStockReceive_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="brwStockReceive_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('brwStockReceive_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="brwStockReceive_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','brwStockReceive',p_web.Translate(''),'Click here to sort by tagged',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','brwStockReceive',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','brwStockReceive',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','brwStockReceive',p_web.Translate('Item Cost'),'Click here to sort by Item Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','brwStockReceive',p_web.Translate('Quantity'),'Click here to sort by Quantity',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','brwStockReceive',p_web.Translate('Quantity Rcvd'),'Click here to sort by Quantity Received',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','brwStockReceive',p_web.Translate('Stock'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','brwStockReceive',p_web.Translate('Exchange'),'Click here to sort by Exchange Order',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'11','brwStockReceive',p_web.Translate('Loan'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','brwStockReceive',p_web.Translate('Received'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (stotmp:ExchangeOrder <> 1) AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','brwStockReceive',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stotmp:recordnumber',lower(loc:vorder),1,1) = 0 !and STOCKRECEIVETMP{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stotmp:RecordNumber',clip(loc:vorder) & ',' & 'stotmp:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stotmp:RecordNumber'),p_web.GetValue('stotmp:RecordNumber'),p_web.GetSessionValue('stotmp:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'stotmp:SessionID = ' & p_web.SessionID
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockReceive',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('brwStockReceive_Filter')
    p_web.SetSessionValue('brwStockReceive_FirstValue','')
    p_web.SetSessionValue('brwStockReceive_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOCKRECEIVETMP,stotmp:RecordNumberKey,loc:PageRows,'brwStockReceive',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOCKRECEIVETMP{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOCKRECEIVETMP,loc:firstvalue)
              Reset(ThisView,STOCKRECEIVETMP)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOCKRECEIVETMP{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOCKRECEIVETMP,loc:lastvalue)
            Reset(ThisView,STOCKRECEIVETMP)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      IF (stotmp:Received = 1)
          locStatus = 'Received'
      ELSE
          locStatus = 'Pending'
      END
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stotmp:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwStockReceive_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockReceive.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockReceive.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockReceive.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockReceive.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockReceive_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockReceive',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('brwStockReceive_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('brwStockReceive_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1brwStockReceive','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwStockReceive_LocatorPic'),,,'onchange="brwStockReceive.locate(''Locator1brwStockReceive'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1brwStockReceive',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwStockReceive.locate(''Locator1brwStockReceive'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="brwStockReceive_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockReceive.cl(''brwStockReceive'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockReceive_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('brwStockReceive_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('brwStockReceive_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwStockReceive_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockReceive.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockReceive.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockReceive.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockReceive.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockReceive_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    TAG:SessionId = p_web.SessionId
    tag:taggedValue = stotmp:RecordNumber
    Access:TagFile.Fetch(tag:keyID)
    loc:field = p_web.AddBrowseValue('brwStockReceive','STOCKRECEIVETMP',stotmp:RecordNumberKey) !stotmp:RecordNumber
    p_web._thisrow = p_web._nocolon('stotmp:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stotmp:RecordNumber = p_web.GetValue('stotmp:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('brwStockReceive:LookupField')) = stotmp:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stotmp:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('brwStockReceive','STOCKRECEIVETMP',stotmp:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOCKRECEIVETMP{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOCKRECEIVETMP)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOCKRECEIVETMP{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOCKRECEIVETMP)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stotmp:PartNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stotmp:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::stotmp:ItemCost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::stotmp:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::stotmp:QuantityReceived
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif stotmp:ExchangeOrder = 0
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::locStock
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif stotmp:ExchangeOrder = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::stotmp:ExchangeOrder
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif stotmp:LoanOrder = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::stotmp:LoanOrder
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif stotmp:Received = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::locStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (stotmp:ExchangeOrder <> 1) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Other7
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('brwStockReceive','STOCKRECEIVETMP',stotmp:RecordNumberKey)
  TableQueue.Id[1] = stotmp:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btibrwStockReceive;if (btibrwStockReceive != 1){{var brwStockReceive=new browseTable(''brwStockReceive'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stotmp:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'brwStockReceive.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'brwStockReceive.applyGreenBar();btibrwStockReceive=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockReceive')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockReceive')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockReceive')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockReceive')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOCKRECEIVETMP)
  p_web._CloseFile(TagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOCKRECEIVETMP)
  Bind(stotmp:Record)
  Clear(stotmp:Record)
  NetWebSetSessionPics(p_web,STOCKRECEIVETMP)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stotmp:RecordNumber',p_web.GetValue('stotmp:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stotmp:RecordNumber = p_web.GSV('stotmp:RecordNumber')
  loc:result = p_web._GetFile(STOCKRECEIVETMP,stotmp:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stotmp:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOCKRECEIVETMP)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  of upper('Other7')
    do Validate::Other7
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOCKRECEIVETMP)
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stotmp:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(STOCKRECEIVETMP,stotmp:RecordNumberKey)
  p_web.FileToSessionQueue(STOCKRECEIVETMP)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = 1,1,0)
  ! Validation goes here, if fails set loc:invalid & loc:alert
  tag:sessionID = p_web.SessionId
  tag:taggedValue = stotmp:RecordNumber
  If Access:TagFile.TryFetch(tag:keyTagged) = 0
      TAG:Tagged = Choose(p_web.GetValue('value') = 1,true,false)
      Access:TagFile.Update()
      p_web._trace('record updated')
  else
      TAG:Tagged = Choose(p_web.GetValue('value') = 1,true,false)
      Access:TagFile.Insert()
      p_web._trace('record inserted')
  end
  
  
  if loc:invalid = ''
    p_web.SetSessionValue('tag:tagged',tag:tagged)
    do CheckForDuplicate
  Else
    tag:tagged = loc:was
  End
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  Else
    do SendAlert
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_tag:tagged_'&stotmp:RecordNumber,,net:crc,,loc:extra)
      If (stotmp:Received <> 1) and loc:Viewonly = 0
          loc:fieldclass = ''
          loc:extra = Choose(tag:tagged = 1,'checked','')
              loc:javascript = 'onchange="brwStockReceive.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"'
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&stotmp:RecordNumber),clip(1),p_web.combine(p_web.site.style.browseCheck,,loc:fieldclass),loc:extra,,,loc:javascript,,) & '<13,10>'
      Else
          loc:fieldclass = ''
          loc:extra = Choose(tag:tagged = 1,'checked','')
              loc:javascript = 'onchange="brwStockReceive.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"'
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&stotmp:RecordNumber),clip(1),p_web.combine(p_web.site.style.browseCheck,,loc:fieldclass),loc:extra & ' disabled',,,loc:javascript,,) & '<13,10>'
      End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:PartNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:PartNumber_'&stotmp:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stotmp:PartNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:Description_'&stotmp:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stotmp:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:ItemCost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:ItemCost_'&stotmp:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stotmp:ItemCost,'@n10.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:Quantity_'&stotmp:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stotmp:Quantity,'@n-14')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:QuantityReceived   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:QuantityReceived_'&stotmp:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stotmp:QuantityReceived,'@n-14')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStock   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif stotmp:ExchangeOrder = 0 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_locStock_'&stotmp:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_locStock_'&stotmp:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:ExchangeOrder   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif stotmp:ExchangeOrder = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:ExchangeOrder_'&stotmp:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:ExchangeOrder_'&stotmp:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stotmp:LoanOrder   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif stotmp:LoanOrder = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:LoanOrder_'&stotmp:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_stotmp:LoanOrder_'&stotmp:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStatus   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif stotmp:Received = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_locStatus_'&stotmp:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_locStatus_'&stotmp:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::Other7  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stotmp:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(STOCKRECEIVETMP,stotmp:RecordNumberKey)
  p_web.FileToSessionQueue(STOCKRECEIVETMP)
  DO ClearList
  DO BuildList
  Loc:Eip = 0
  do CallBrowse
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Other7   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (stotmp:ExchangeOrder <> 1)
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockReceive_Other7_'&stotmp:RecordNumber,,net:crc,,loc:extra)
      loc:disabled = ''
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','Amend','Amend',p_web.combine(Choose('Amend' <> '',p_web.site.style.BrowseOtherButtonWithText,p_web.site.style.BrowseOtherButtonWithOutText),'SmallButton'),loc:formname,p_web._MakeURL('frmUpdateStockReceive' & '&' & p_web._noColon('stotmp:RecordNumber')&'='& p_web.escape(stotmp:RecordNumber)&''),clip('_self'),,,loc:disabled,,,,,,0,,,,'nt-left')!j
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END
  return
ClearList           ROUTINE
    ClearStockReceiveList(p_web)
BuildList           ROUTINE
    BuildStockReceiveList(p_web,p_web.GSV('ret:Ref_Number'),p_web.GSV('ret:ExchangeOrder'),p_web.GSV('ret:LoanOrder'))
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stotmp:RecordNumber',stotmp:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
frmStockReceiveProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locMessage           STRING(255)                           !
FilesOpened     Long
RETSTOCK::State  USHORT
RETSALES::State  USHORT
STOCK::State  USHORT
STOCKRECEIVETMP::State  USHORT
TagFile::State  USHORT
txtError:IsInvalid  Long
txtMessage:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('frmStockReceiveProcess')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmStockReceiveProcess_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmStockReceiveProcess','')
    p_web.DivHeader('frmStockReceiveProcess',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmStockReceiveProcess') = 0
        p_web.AddPreCall('frmStockReceiveProcess')
        p_web.DivHeader('popup_frmStockReceiveProcess','nt-hidden')
        p_web.DivHeader('frmStockReceiveProcess',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmStockReceiveProcess_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmStockReceiveProcess_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmStockReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmStockReceiveProcess',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmStockReceiveProcess')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(RETSTOCK)
  p_web._OpenFile(RETSALES)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(STOCKRECEIVETMP)
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmStockReceiveProcess_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmStockReceiveProcess'
    end
    p_web.formsettings.proc = 'frmStockReceiveProcess'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Error') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Error') = 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmStockReceiveProcess_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmStockReceiveProcess')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmStockReceiveProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmStockReceiveProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmStockReceiveProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('Error',0)
  IF (CheckIfTagged(p_web) = 0)
      p_web.SSV('Error',1)
  ELSE
      processed# = 0
      Access:TagFile.ClearKey(tag:keyTagged)
      tag:sessionID = p_web.SessionID
      SET(tag:keyTagged,tag:keyTagged)
      LOOP UNTIL Access:TagFile.Next()
          IF (tag:sessionID <> p_web.SessionID)
              BREAK
          END
          Access:STOCKRECEIVETMP.Clearkey(stotmp:RecordNumberKey)
          stotmp:RecordNumber = tag:taggedValue
          IF (Access:STOCKRECEIVETMP.Tryfetch(stotmp:RecordNumberKey))
              CYCLE
          END
  
          Access:RETSTOCK.Clearkey(res:Record_Number_Key)
          res:Record_Number   = stotmp:RESRecordNumber
          If Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign
              Access:RETSALES.ClearKey(ret:Ref_Number_Key)
              ret:Ref_Number = res:Ref_Number
              IF (Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign)
                  IF (ret:ExchangeOrder)
                      CYCLE
                  END
  
              END
                  !Found
              If ~res:Received
                  Access:STOCK.ClearKey(sto:Location_Key)
                  sto:Location    = p_web.GSV('Default:SiteLocation')
                  sto:Part_Number = res:Part_Number
                  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                          !Found
                      sto:Quantity_Stock += res:QuantityReceived
                      If Access:STOCK.Update() = Level:Benign
                          processed# = 1
                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                              'ADD', | ! Transaction_Type
                              '', | ! Depatch_Note_Number
                              0, | ! Job_Number
                              0, | ! Sales_Number
                              res:Quantity, | ! Quantity
                              res:Item_Cost, | ! Purchase_Cost
                              res:Sale_Cost, | ! Sale_Cost
                              res:Retail_Cost, | ! Retail_Cost
                              'STOCK ADDED FROM ORDER', | ! Notes
                              'RAPID STOCK RECEIVED <13,10>INVOICE NO: ' & p_web.GSV('locInvoiceNumber'), |
                              p_web.GSV('BookingUserCode'), |
                              sto:Quantity_Stock) ! Information
                                ! Added OK
                          Else ! AddToStockHistory
                                ! Error
                          End ! AddToStockHistory
                              ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                          if (sto:Suspend)
                              sto:Suspend = 0
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                      'ADD', | ! Transaction_Type
                                      '', | ! Depatch_Note_Number
                                      0, | ! Job_Number
                                      0, | ! Sales_Number
                                      0, | ! Quantity
                                      res:Item_Cost, | ! Purchase_Cost
                                      res:Sale_Cost, | ! Sale_Cost
                                      res:Retail_Cost, | ! Retail_Cost
                                      'PART UNSUSPENDED', | ! Notes
                                      '', |
                                      p_web.GSV('BookingUserCode'), |
                                      sto:Quantity_Stock) ! Information
                                  End ! AddToStockHistory
                              End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                          end
                          If res:QuantityReceived < res:Quantity
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                  'DEC', | ! Transaction_Type
                                  '', | ! Depatch_Note_Number
                                  0, | ! Job_Number
                                  0, | ! Sales_Number
                                  res:Quantity - res:QuantityReceived, | ! Quantity
                                  res:Item_Cost, | ! Purchase_Cost
                                  res:Sale_Cost, | ! Sale_Cost
                                  res:Retail_Cost, | ! Retail_Cost
                                  'RAPID STOCK RECEIVED ADJUSTMENT', | ! Notes
                                  'INVOICE NO: ' & p_web.GSV('locInvoiceNumber'), |
                                  p_web.GSV('BookingUserCode'), |
                                  sto:Quantity_Stock) ! Information
                                    ! Added OK
                              Else ! AddToStockHistory
                                    ! Error
                              End ! AddToStockHistory
                          End !If res:QuantityReceived < res:Quantity
                          res:Received = 1
                          res:DateReceived = Today()
                          Access:RETSTOCK.Update()
  
                              !Only work out average if more than 0 is received
                          If res:QuantityReceived > 0
                              AvPurchaseCost$ = sto:AveragePurchaseCost
                              PurchaseCost$ = sto:Purchase_Cost
                              SaleCost$ = sto:Sale_Cost
                              RetailCost$ = sto:Retail_Cost
  
                              sto:AveragePurchaseCost = AveragePurchaseCost(sto:Ref_Number,sto:Quantity_stock,locMessage)
                              sto:Purchase_Cost   = VodacomClass.Markup(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
                              sto:Sale_Cost       = VodacomClass.Markup(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
                              BHAddToDebugLog('Av Cost: ' & AvPurchaseCost$ & ' - ' & sto:AveragePurchaseCost &  ' - ' & sto:Sale_Cost)
                                  !Write record of information!
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                  'ADD', | ! Transaction_Type
                                  '', | ! Depatch_Note_Number
                                  0, | ! Job_Number
                                  0, | ! Sales_Number
                                  0, | ! Quantity
                                  sto:Purchase_Cost, | ! Purchase_Cost
                                  sto:Sale_Cost, | ! Sale_Cost
                                  sto:Retail_Cost, | ! Retail_Cost
                                  'AVERAGE COST CALCULATION', | ! Notes
                                  Clip(locMessage),|
                                  p_web.GSV('BookingUserCode'), |
                                  sto:Quantity_Stock,|
                                  AvPurchaseCost$,|
                                  PurchaseCost$,|
                                  SaleCost$,|
                                  RetailCost$)
                                    ! Added OK
                              Else ! AddToStockHistory
                                    ! Error
                              End ! AddToStockHistory
                          End !If res:QuantityReceived > 0
  
                          Access:STOCK.Update()
                      End !If Access:STOCK.Update() = Level:Benign
                  Else!If Access:STOCK.TryFetch(sto:Location_Part_Description_Key) = Level:Benign
                          !Error
                  End!If Access:STOCK.TryFetch(sto:Location_Part_Description_Key) = Level:Benign
              End !If ~res:Recevied
          Else! If Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign
      End !Loop x# = 1 To Records(glo:Queue)
  END
  
  IF (processed# = 0)
      p_web.SSV('Error',1)
  END
  
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Process Tagged Items') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Process Tagged Items',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmStockReceiveProcess',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('Error') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmStockReceiveProcess0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      End
      If p_web.GSV('Error') = 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmStockReceiveProcess1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmStockReceiveProcess_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmStockReceiveProcess_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmStockReceiveProcess_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmStockReceiveProcess_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmStockReceiveProcess_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Error') = 1
    ElsIf p_web.GSV('Error') = 0
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmStockReceiveProcess')>0,p_web.GSV('showtab_frmStockReceiveProcess'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmStockReceiveProcess'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmStockReceiveProcess') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmStockReceiveProcess'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmStockReceiveProcess')>0,p_web.GSV('showtab_frmStockReceiveProcess'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmStockReceiveProcess') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('Error') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      End
      If p_web.GSV('Error') = 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmStockReceiveProcess_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmStockReceiveProcess')>0,p_web.GSV('showtab_frmStockReceiveProcess'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmStockReceiveProcess",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmStockReceiveProcess')>0,p_web.GSV('showtab_frmStockReceiveProcess'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmStockReceiveProcess_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmStockReceiveProcess') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmStockReceiveProcess')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('Error') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtError
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtError
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('Error') = 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceiveProcess1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Validate::txtError  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtError  ! copies value to session value if valid.
  do Comment::txtError ! allows comment style to be updated.

ValidateValue::txtError  Routine
    If not (1=0)
    End

Value::txtError  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceiveProcess_' & p_web._nocolon('txtError') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtError" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate('You have not tagged any parts that can be received',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtError  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtError:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceiveProcess_' & p_web._nocolon('txtError') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtMessage  ! copies value to session value if valid.
  do Comment::txtMessage ! allows comment style to be updated.

ValidateValue::txtMessage  Routine
    If not (1=0)
    End

Value::txtMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceiveProcess_' & p_web._nocolon('txtMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtMessage" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate('The tagged items have been processed',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceiveProcess_' & p_web._nocolon('txtMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmStockReceiveProcess_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('frmStockReceiveProcess_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmStockReceiveProcess_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmStockReceiveProcess_tab_' & 0)
    do GenerateTab0
  of lower('frmStockReceiveProcess_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmStockReceiveProcess_form:ready_',1)

  p_web.SetSessionValue('frmStockReceiveProcess_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmStockReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmStockReceiveProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmStockReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmStockReceiveProcess_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmStockReceiveProcess:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmStockReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmStockReceiveProcess_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmStockReceiveProcess:Primed',0)
  p_web.setsessionvalue('showtab_frmStockReceiveProcess',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('Error') = 1
  End
  If p_web.GSV('Error') = 0
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmStockReceiveProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ClearStocKReceiveList(p_web)
  ClearTagFile(p_web)
  BuildStockReceiveList(p_web,p_web.GSV('ret:Ref_Number'),p_web.GSV('ret:ExchangeOrder'),p_web.GSV('ret:LoanOrder'))
  p_web.DeleteSessionValue('frmStockReceiveProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Error') = 1
    loc:InvalidTab += 1
    do ValidateValue::txtError
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('Error') = 0
    loc:InvalidTab += 1
    do ValidateValue::txtMessage
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmStockReceiveProcess:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locMessage',locMessage) ! STRING(255)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locMessage = p_web.GSV('locMessage') ! STRING(255)
