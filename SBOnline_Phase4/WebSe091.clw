

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE091.INC'),ONCE        !Local module procedure declarations
                     END


frmPrintAllGRN       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
GRNOTESR::State  USHORT
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('frmPrintAllGRN')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmPrintAllGRN_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmPrintAllGRN','')
    p_web.DivHeader('frmPrintAllGRN',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmPrintAllGRN') = 0
        p_web.AddPreCall('frmPrintAllGRN')
        p_web.DivHeader('popup_frmPrintAllGRN','nt-hidden')
        p_web.DivHeader('frmPrintAllGRN',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmPrintAllGRN_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmPrintAllGRN_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmPrintAllGRN',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmPrintAllGRN')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(GRNOTESR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(GRNOTESR)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmPrintAllGRN_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmPrintAllGRN'
    end
    p_web.formsettings.proc = 'frmPrintAllGRN'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmPrintAllGRN_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmPrintAllGRN')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmPrintAllGRN_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmPrintAllGRN_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmPrintAllGRN_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmPrintAllGRN',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmPrintAllGRN0_div')&'">'&p_web.Translate('Available Reports For Selected Invoice')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmPrintAllGRN_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmPrintAllGRN_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPrintAllGRN_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmPrintAllGRN_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPrintAllGRN_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmPrintAllGRN'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPrintAllGRN') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmPrintAllGRN'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPrintAllGRN') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Available Reports For Selected Invoice') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmPrintAllGRN_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmPrintAllGRN",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmPrintAllGRN_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmPrintAllGRN') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmPrintAllGRN')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Available Reports For Selected Invoice')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Available Reports For Selected Invoice')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Available Reports For Selected Invoice')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Available Reports For Selected Invoice')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      do SendPacket
      Do showreports
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPrintAllGRN_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmPrintAllGRN_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPrintAllGRN_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)

  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmPrintAllGRN:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmPrintAllGRN:Primed',0)
  p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmPrintAllGRN_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmPrintAllGRN_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmPrintAllGRN:Primed',0)

showreports  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
  found# = 0
  Access:GRNOTESR.ClearKey(grr:Order_Number_Key)
  grr:Order_Number        = p_web.GSV('locInvoiceNumber')
  Set(grr:Order_Number_Key,grr:Order_Number_Key)
  Loop
      If Access:GRNOTESR.NEXT()
          Break
      End !If
      If grr:Order_Number        <> p_web.GSV('locInvoiceNumber')      |
          Then Break.  ! End If
      packet = clip(packet) & p_web.AsciiToUTF(|
          '<A HREF="Goods_Received_Note_Retail?GRN='& grr:Goods_Received_Number & |
          '&INV=' & p_web.GSV('locInvoiceNumber') & |
          '&GRD=' & grr:Goods_Received_Date & |
          '&REP=Y" target="_blank">Goods Received Note ( ' & FORMAT(grr:Goods_Received_Date,@d06) & ' )</A><br>',net:OnlyIfUTF)
      found# = 1
  !        GLO:Select1 = grr:Goods_Received_Number
  !        GLO:Select2 = tmp:InvoiceNumber
  !        GLO:Select3 = grr:Goods_Received_Date
  !        glo:Select4 = 'REPRINT'
  !        Goods_Received_Note_Retail
  !        GLO:Select1 = ''
  !        GLO:Select2 = ''
  !        GLO:Select3 = ''
  !        glo:Select4 = ''
  End !Loop
  IF (found# = 0)
      packet = CLIP(packet) & p_web.AsciiToUtf( |
          '<span class="red large">There are no Good Receive Notes Created For This Invoice</span>',net:OnlyIFUTF)
  END
BuildStockReceiveList PROCEDURE  (NetWebServerWorker p_web,LONG fRefNumber,BYTE fExchangeOrder,BYTE fLoanOrder) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    Access:RETSTOCK.Clearkey(res:Despatched_Key)
    res:Ref_Number = fRefNumber
    res:Despatched = 'YES'
    SET(res:Despatched_Key,res:Despatched_Key)
    LOOP UNTIL Access:RETSTOCK.Next()
        IF (res:Ref_Number <> fRefNumber OR |
            res:Despatched <> 'YES')
            BREAK
        END

        addNewPart# = 1
        IF (p_web.GSV('ret:ExchangeOrder') = 1 OR p_web.GSV('ret:LoanOrder') = 1)
                ! #12127 Group Parts If Exchange Order. Keep Received Seperate (Bryan: 05/07/2011)
            Access:STOCKRECEIVETMP.Clearkey(stotmp:ReceivedPartNumberKey)
            stotmp:SessionID = p_web.SessionID
            stotmp:Received = res:Received
            stotmp:PartNumber = res:Part_Number
            IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:ReceivedPartNumberKey) = Level:Benign)
                stotmp:Quantity += 1
                stotmp:QuantityReceived += res:QuantityReceived
                Access:STOCKRECEIVETMP.TryUpdate()
                addNewPart# = 0
            END
        END ! IF (ret:ExchangeOrder = 1)
        IF (addNewPart# = 1)

            IF (Access:STOCKRECEIVETMP.PrimeRecord() = Level:Benign)
                stotmp:SessionID = p_web.SessionID
                stotmp:PartNumber = res:Part_Number
                stotmp:Description = res:Description
                stotmp:ItemCost = res:Item_Cost
                stotmp:Quantity = res:Quantity
                stotmp:QuantityReceived = res:QuantityReceived
                stotmp:RESRecordNumber = res:Record_Number
                stotmp:Received = res:Received
                stotmp:ExchangeOrder = fExchangeOrder
                stotmp:LoanOrder = fLoanOrder

                IF (Access:STOCKRECEIVETMP.TryInsert())
                    Access:STOCKRECEIVETMP.CancelAutoInc()
                END
            END
        END ! IF (addNewPart# = 1)
    END

    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKRECEIVETMP.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKRECEIVETMP.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSTOCK.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSTOCK.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKRECEIVETMP.Close
     Access:RETSTOCK.Close
     FilesOpened = False
  END
ClearStockReceiveList PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    Access:STOCKRECEIVETMP.Clearkey(stotmp:PartNumberKey)
    stotmp:SessionID = p_web.SessionID
    SET(stotmp:PartNumberKey,stotmp:PartNumberKey)
    LOOP UNTIL Access:STOCKRECEIVETMP.Next()
        IF (stotmp:SessionID <> p_web.SessionID)
            BREAK
        END

        Access:STOCKRECEIVETMP.DeleteRecord(0)
    END

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKRECEIVETMP.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKRECEIVETMP.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKRECEIVETMP.Close
     FilesOpened = False
  END
