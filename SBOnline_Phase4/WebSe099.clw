

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ABWMFPAR.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE099.INC'),ONCE        !Local module procedure declarations
                     END



Goods_Received_Note_Retail PROCEDURE(<NetWebServerWorker p_web>)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:Order_No_Filter  REAL
tmp:Date_Received_Filter DATE
Parts_Q              QUEUE,PRE(pq)
PartNo               STRING(30)
Description          STRING(30)
QtyOrdered           REAL
QtyReceived          LONG
Location             STRING(30)
Cost                 REAL
OrderNumber          LONG
LineCost             REAL
                     END
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Date_Despatched)
                       PROJECT(ret:Ref_Number)
                     END
Report               REPORT('Goods Received Note'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5844,760),USE(ReportRunDate),TRN,LEFT,FONT(,8,,)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6521,760),USE(ReportRunTime),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,323,3135,201),USE(tra:Address_Line1,,?tra:Address_Line1:2),TRN
                         STRING(@s30),AT(156,104,4156,263),USE(tra:Company_Name,,?tra:Company_Name:2),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635,3135,201),USE(tra:Address_Line3,,?tra:Address_Line3:2),TRN
                         STRING('REPRINT'),AT(3385,677),USE(?Reprint),TRN,HIDE,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(156,802,3135,201),USE(tra:Postcode,,?tra:Postcode:2),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094,2495,201),USE(tra:Fax_Number,,?tra:Fax_Number:2),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250,2500,201),USE(tra:EmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(tra:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(tra:Address_Line1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(tra:Address_Line2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(tra:Address_Line3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2292),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4083,2292),USE(tra:Postcode),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4396,2448,990,188),USE(tra:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s30),AT(521,2604,990,188),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4396,2604,990,188),USE(tra:Fax_Number),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(tra:Account_Number),TRN,FONT(,8,,)
                         STRING(@s8),AT(1615,3385),USE(ret:Ref_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(3083,3385),USE(ret:Date_Despatched),TRN,FONT(,8,,)
                         STRING(@s20),AT(5844,938,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,)
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING('RRC GOODS RECEIVED NOTE'),AT(4396,104,2917,313),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(573,958,2495,201),USE(tra:Telephone_Number,,?tra:Telephone_Number:2),TRN
                         STRING('Printed by'),AT(5083,938),USE(?String67),TRN,FONT(,8,COLOR:Black,,CHARSET:ANSI)
                         STRING(@s30),AT(156,479,3135,201),USE(tra:Address_Line2,,?tra:Address_Line2:2),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?BREAK1)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@s8),AT(125,0),USE(pq:QtyOrdered),TRN,RIGHT,FONT(,7,COLOR:Black,)
                           STRING(@s30),AT(1833,0),USE(pq:PartNo),TRN,FONT(,7,COLOR:Black,)
                           STRING(@s30),AT(3333,0),USE(pq:Description),TRN,FONT(,7,COLOR:Black,)
                           STRING(@s8),AT(833,0),USE(pq:QtyReceived),TRN,RIGHT,FONT(,7,COLOR:Black,)
                           STRING(@s30),AT(5052,0),USE(pq:Location),TRN,LEFT,FONT(,7,COLOR:Black,)
                           STRING(@n14.2),AT(6719,0),USE(total_cost_temp),TRN,FONT(,7,COLOR:Black,,CHARSET:ANSI)
                         END
detail1                  DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                         END
Totals                   DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                           STRING('Total Items: '),AT(396,83),USE(?String47),TRN,FONT(,10,,FONT:bold)
                           STRING('Total Lines: '),AT(396,323),USE(?String40),TRN,FONT(,,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,323,773,201),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,83,773,201),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING('Total GRN Value:'),AT(4844,156),USE(?String41),TRN,FONT(,,,FONT:bold)
                         END
Continue                 DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                           STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                           STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                         END
detailExchange           DETAIL,AT(,,,188),USE(?detailExchange)
                           STRING(@n_8),AT(156,0),USE(pq:OrderNumber),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@n_8),AT(833,0),USE(pq:QtyReceived,,?pq:QtyReceived:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s60),AT(1354,0,2917,156),USE(pq:PartNo,,?pq:PartNo:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(4427,0,1406,156),USE(pq:Description,,?pq:Description:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5990,0),USE(pq:Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6823,0),USE(pq:LineCost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1125),USE(?FOOTER1)
                         TEXT,AT(104,521,7344,521),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed:2)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Sale Number'),AT(1604,3177),USE(?String32),TRN,FONT(,8,,FONT:bold)
                         STRING('Sales Date'),AT(3083,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact'),AT(4531,3177),USE(?String45),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         GROUP,AT(104,3698,7344,500),USE(?groupTitle1)
                           STRING('Qty Ordered'),AT(156,3854),USE(?strQtyOrdered),TRN,FONT(,8,,FONT:bold)
                           STRING('Qty Received'),AT(990,3854),USE(?strQtyReceived),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1875,3854),USE(?String34),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3333,3854),USE(?String33),TRN,FONT(,8,,FONT:bold)
                           STRING('Shelf Location'),AT(5052,3854),USE(?String36),TRN,FONT(,8,,FONT:bold)
                           STRING('GRN Value'),AT(6719,3854),USE(?String66),TRN,FONT('Arial',8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         END
                         GROUP,AT(52,3646,7344,500),USE(?groupTitle2),TRN,HIDE
                           STRING('Line Cost'),AT(6792,3854),USE(?String69:7),TRN,FONT(,8,,FONT:bold)
                           STRING('Order No'),AT(156,3854),USE(?String69),TRN,FONT(,8,,FONT:bold)
                           STRING('Quantity'),AT(823,3854),USE(?String69:2),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number / Model Number'),AT(1354,3854),USE(?String69:3),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(4427,3854),USE(?String69:4),TRN,FONT(,8,,FONT:bold)
                           STRING('Item Cost'),AT(6135,3854),USE(?String69:6),TRN,FONT(,8,,FONT:bold)
                         END
                         STRING('Received By'),AT(104,9844),USE(?String64),TRN,FONT('Arial',9,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         LINE,AT(937,9990,2656,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v6.30
! CW Template version    v6.3
! CW Version             6300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Goods_Received_Note_Retail')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSALES.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:STOCK.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  DO BindFileFields
  IF (p_web.IfExistsValue('GRN'))
      p_web.StoreValue('GRN')
  END
  IF (p_web.IfExistsValue('INV'))
      p_web.StoreValue('INV')
  END
  IF (p_web.IfExistsValue('GRD'))
      p_web.StoreValue('GRD')
  END
  IF (p_web.IfExistsValue('REP'))
      p_web.StoreValue('REP')
  END
  
  tmp:Order_No_Filter = p_web.GSV('INV')
  tmp:Date_Received_Filter = p_web.GSV('GRD')
  
  !Fetch this traders address
  Access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('BookingAccount')
  access:tradeacc.fetch(tra:account_Number_key)
  
  
  !fetch the record
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = tmp:Order_No_Filter
  Access:RETSALES.TryFetch(ret:Invoice_Number_Key)
  
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = p_web.GSV('BookingUserPassword')
  Access:USERS.TryFetch(use:Password_Key)
  tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - RETAIL'
  Access:STANTEXT.Tryfetch(stt:Description_Key)
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(RETSALES)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(RETSALES,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      SET(ret:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'ret:Invoice_Number = tmp:Order_No_Filter'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
      
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
            SETTARGET(REPORT)
            ?groupTitle1{prop:hide} = 1
            ?groupTitle2{prop:hide} = 0
            SETTARGET()
        END
        
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Ref_Number  = ret:Ref_Number
        Set(res:Part_Number_Key,res:Part_Number_Key)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Ref_Number  <> ret:Ref_Number      |
                Then Break.  ! End If
            If ~res:Received
                Cycle
            End !If ~res:Received
            If res:GRNNumber <> p_web.GSV('GRN')
                Cycle
            End !If res:GRNNumber <> glo:Select1
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                Parts_Q.PartNo = res:Part_Number
        
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location     = VodacomClass.MainStoreLocation()
                sto:Part_Number  = res:Part_Number
                IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
        
                END ! IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
        
                IF (ret:ExchangeOrder = 1)
                    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                    xch:Ref_Number = res:ExchangeRefNumber
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = xch:ESN
                    ELSE ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = ''
                    END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:ExchangeModelNumber)
                END
                IF (ret:LoanOrder = 1)
                    Access:LOAN.ClearKey(loa:Ref_Number_Key)
                    loa:Ref_Number = res:LoanRefNumber
                    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = loa:ESN
                    ELSE
                        Parts_Q.Description = ''
                    END
                    Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:LoanModelNumber)
                END
        
        
                Parts_Q.QtyReceived = res:Quantity
                Parts_Q.Cost        = res:Item_Cost
                Parts_Q.LineCost    = res:Item_Cost
                Parts_Q.OrderNumber = res:Purchase_Order_Number
        
                ADD(Parts_Q)
            ElSE ! IF (ret:ExchangeOrder = 1)
        
        
                Parts_Q.pq:PartNo = res:Part_Number
                Parts_Q.pq:Cost = res:Purchase_Cost
                get(Parts_Q, Parts_Q.pq:PartNo, Parts_Q:pq:Cost)
                if error()  ! Insert queue entry
                    Parts_Q.pq:PartNo = res:Part_Number
                    Parts_Q.pq:Description = res:Description
                    Parts_Q.pq:QtyOrdered = res:Quantity   !=====================================
                    Parts_Q.pq:Cost = res:Item_Cost        ! Was purchase_cost    ref G132 / l277
                    if res:Despatched = 'YES'              !=====================================
            !            if res:QuantityReceived <> 0
                            Parts_Q.pq:QtyReceived = res:QuantityReceived
            !            else
            !                Parts_Q.pq:QtyReceived = res:Quantity
            !            end
                    else
                        Parts_Q.pq:QtyReceived = 0
                    end
                    Parts_Q.pq:Location = ''
        
                    !Find the shelf locatin of the RRC's part
                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password    = p_web.GSV('BookingUserPassword')
                    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = res:Part_Number
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Found
                            Parts_Q.pq:Location = sto:Shelf_Location
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        
        
                    add(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
                else
                    Parts_Q.pq:QtyOrdered += res:Quantity
                    if res:Despatched = 'YES'
            !            if res:QuantityReceived <> 0
                            Parts_Q.pq:QtyReceived += res:QuantityReceived
            !            else
            !                Parts_Q.pq:QtyReceived += res:Quantity
            !            end
                    end
                    put(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
                end
            END ! IF (ret:ExchangeOrder = 1)
        end
        
        sort(Parts_Q, Parts_Q.pq:PartNo)
        
        loop a# = 1 to records(Parts_Q)
            get(Parts_Q,a#)
        
            If order_temp <> res:Ref_Number And first_page_temp <> 1
                Print(rpt:detail1)
            End
        
            tmp:RecordsCount += 1
            count# += 1
            If count# > 20
                Print(rpt:continue)
                Print(rpt:detail1)
                count# = 1
            End!If count# > 25
        
            total_cost_temp = Parts_Q.pq:Cost * Parts_Q.pq:QtyReceived
            total_cost_total_temp += total_cost_temp
            total_quantity_temp   += Parts_Q.pq:QtyReceived
            total_lines_temp      += 1
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                Print(rpt:detailExchange)
            ELSE
                Print(rpt:detail)
            END
            order_temp = res:Ref_Number
            first_page_temp = 0
            print# = 1
        end
        
        If print# = 1
            Print(rpt:totals)
        End !If print# = 1
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSALES,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
    If Not p_web &= Null
      p_web.NoOp()
    End
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RETSALES')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  
  !removed ref G132/L277
  
  !If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  !Else!If def:use_invoice_address = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:User_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  !End!If def:use_invoice_address = 'YES'
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  SYSTEM{PROP:PrintMode} = 3
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  If p_web.GSV('REP') = 'Y' ! Reprint
      SetTarget(Report)
      ?Reprint{prop:Hide} = 0
      SetTarget()
  End !glo:Select4 = 'REPRINT'
  Report{PROPPRINT:Extend}=True
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Goods_Received_Note_Retail'
  END
  Report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  LocalAttribute.Init(Report)
  Do SetStaticControlsAttributes
  



GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('tmp:Order_No_Filter',tmp:Order_No_Filter)
  BIND(def:RECORD)
  BIND(dep:RECORD)
  BIND(xch:RECORD)
  BIND(ret:RECORD)
  BIND(res:RECORD)
  BIND(stt:RECORD)
  BIND(sto:RECORD)
  BIND(sup:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','Goods_Received_Note_Retail','Goods_Received_Note_Retail','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

AddToStockAllocation PROCEDURE  (LONG func:PartRecordNumber,STRING func:PartType,LONG func:Quantity,STRING func:Status,STRING func:Engineer,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
    vod.AddPartToStockAllocation(func:PartRecordNumber,func:PartType,func:Quantity,func:Status,func:Engineer,,p_web)
GetStatus            PROCEDURE  (LONG f_number,BYTE f_audit,STRING f_type,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
    vod.SetStatus(f_number,f_audit,f_type,p_web)

    SendSMSText(f_type[1],'Y','N',p_web)
ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TimeOut') > 0
    TempTimeOut = ThisMessageBox.GetGlobalSetting('TimeOut')
    ThisMessageBox.SetGlobalSetting('TimeOut', 0)
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
  if TempTimeOut
    ThisMessageBox.SetGlobalSetting('TimeOut',TempTimeOut)
  end
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('MS Sans Serif',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
        ThisMessageBox.ReturnValue = ThisMessageBox.PreOpen(LMBD:MessageText,LMBD:HeadingText,LMBD:Defaults)
        if ThisMessageBox.ReturnValue then
          self.Response = RequestCompleted
          return level:notify
        end
        ThisMessageBox.DontShowThisAgain = ?DontShowThisAgain
        ThisMessageBox.TimeOutPrompt = ?TimerCounter
        ThisMessageBox.HAControl = ?HALink
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
      IF (INSTRING('WINDOWS 7',UPPER(SYSTEM {PROP:WindowsVersion}),1,1) OR |
          INSTRING('WINDOWS VISTA',UPPER(SYSTEM{PROP:WindowsVersion}),1,1) OR |
          INSTRING('SERVER 2008',UPPER(SYSTEM{PROP:WindowsVersion}),1,1))
          0{prop:FontName} = 'Segoe UI'
          0{prop:FontSize} = 9
      ELSE
          0{prop:FontName} = 'Tahoma'
          0{prop:FontSize} = 8
      END
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
      IF (0{prop:Width} < 150)
          0{prop:Width} = 150
      END
      0{PROP:Height} = 0{PROP:Height} + 2
  
      SETPENCOLOR(COLOR:BTNSHADOW)
      BOX(-1,-1,0{PROP:Width} + 2,0{PROP:Height} - 18,COLOR:White)
      IMAGE(?Image1{PROP:Xpos},?Image1{PROP:Ypos},?Image1{PROP:Width},?Image1{PROP:Height},?Image1{PROP:Text})
  
      ?Button1{PROP:Ypos} = ?Button1{PROP:Ypos} + 4
      ?Button2{PROP:Ypos} = ?Button2{PROP:Ypos} + 4
      ?Button3{PROP:Ypos} = ?Button3{PROP:Ypos} + 4
      ?Button4{PROP:Ypos} = ?Button4{PROP:Ypos} + 4
      ?Button5{PROP:Ypos} = ?Button5{PROP:Ypos} + 4
      ?Button6{PROP:Ypos} = ?Button6{PROP:Ypos} + 4
      ?Button7{PROP:Ypos} = ?Button7{PROP:Ypos} + 4
      ?Button8{PROP:Ypos} = ?Button8{PROP:Ypos} + 4
  
      IF (?Button8{PROP:Hide} = 0)
          b8# = ?Button8{PROP:Width} + 4
      END
      IF (?Button7{PROP:Hide} = 0)
          b7# = ?Button7{PROP:Width} + 4
      END
      IF (?Button6{PROP:Hide} = 0)
          b6# = ?Button6{PROP:Width} + 4
      END
      IF (?Button5{PROP:Hide} = 0)
          b5# = ?Button5{PROP:Width} + 4
      END
      IF (?Button4{PROP:Hide} = 0)
          b4# = ?Button4{PROP:Width} + 4
      END
      IF (?Button3{PROP:Hide} = 0)
          b3# = ?Button3{PROP:Width} + 4
      END
      IF (?Button2{PROP:Hide} = 0)
          b2# = ?Button2{PROP:Width} + 4
      END
      IF (?Button1{PROP:Hide} = 0)
          b1# = ?Button1{PROP:Width} + 4
      END
  
      ?Button8{PROP:Xpos} = 0{PROP:Width} - b8#
      ?Button7{PROP:Xpos} = 0{PROP:Width} - b8# - b7#
      ?Button6{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6#
      ?Button5{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5#
      ?Button4{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4#
      ?Button3{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3#
      ?Button2{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3# - b2#
      ?Button1{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3# - b2# - b1#
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    ThisMessageBox.Close()
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

FormNewPassword      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locNewPassword       STRING(30)                            !
locConfirmNewPassword STRING(30)                           !
locMobileNumber      STRING(20)                            !
FilesOpened     Long
USERS::State  USHORT
txtCompany:IsInvalid  Long
txtUser:IsInvalid  Long
locNewPassword:IsInvalid  Long
locConfirmNewPassword:IsInvalid  Long
locMobileNumber:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormNewPassword')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormNewPassword_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormNewPassword','')
    p_web.DivHeader('FormNewPassword',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormNewPassword') = 0
        p_web.AddPreCall('FormNewPassword')
        p_web.DivHeader('popup_FormNewPassword','nt-hidden')
        p_web.DivHeader('FormNewPassword',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormNewPassword_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormNewPassword_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormNewPassword',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  p_web._MessageDone = 1 ! suppress any messages on this form.
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormNewPassword',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormNewPassword')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormNewPassword_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormNewPassword'
    end
    p_web.formsettings.proc = 'FormNewPassword'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('NewPasswordRequired') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('UserMobileRequired') = 1
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locNewPassword') = 0
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  Else
    locNewPassword = p_web.GetSessionValue('locNewPassword')
  End
  if p_web.IfExistsValue('locConfirmNewPassword') = 0
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  Else
    locConfirmNewPassword = p_web.GetSessionValue('locConfirmNewPassword')
  End
  if p_web.IfExistsValue('locMobileNumber') = 0
    p_web.SetSessionValue('locMobileNumber',locMobileNumber)
  Else
    locMobileNumber = p_web.GetSessionValue('locMobileNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locNewPassword')
    locNewPassword = p_web.GetValue('locNewPassword')
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  Else
    locNewPassword = p_web.GetSessionValue('locNewPassword')
  End
  if p_web.IfExistsValue('locConfirmNewPassword')
    locConfirmNewPassword = p_web.GetValue('locConfirmNewPassword')
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  Else
    locConfirmNewPassword = p_web.GetSessionValue('locConfirmNewPassword')
  End
  if p_web.IfExistsValue('locMobileNumber')
    locMobileNumber = p_web.GetValue('locMobileNumber')
    p_web.SetSessionValue('locMobileNumber',locMobileNumber)
  Else
    locMobileNumber = p_web.GetSessionValue('locMobileNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormNewPassword_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormNewPassword_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormNewPassword_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormNewPassword_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'LoginForm'

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('locNewPassword','')
  p_web.SSV('locConfirmNewPassword','')
  p_web.SSV('locMobileNumber','')
      p_web.site.SaveButton.TextValue = 'Login'
 locNewPassword = p_web.RestoreValue('locNewPassword')
 locConfirmNewPassword = p_web.RestoreValue('locConfirmNewPassword')
 locMobileNumber = p_web.RestoreValue('locMobileNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormNewPassword',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormNewPassword0_div')&'">'&p_web.Translate('Login Details')&'</a></li>'& CRLF
      If p_web.GSV('NewPasswordRequired') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormNewPassword1_div')&'">'&p_web.Translate('New Login Password Required')&'</a></li>'& CRLF
      End
      If p_web.GSV('UserMobileRequired') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormNewPassword2_div')&'">'&p_web.Translate('Mobile Number Required')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormNewPassword_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormNewPassword_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormNewPassword_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormNewPassword_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormNewPassword_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormNewPassword'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormNewPassword') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormNewPassword'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormNewPassword') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Login Details') & ''''
      If p_web.GSV('NewPasswordRequired') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('New Login Password Required') & ''''
      End
      If p_web.GSV('UserMobileRequired') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Mobile Number Required') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormNewPassword_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormNewPassword",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormNewPassword_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormNewPassword') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormNewPassword')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Login Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Login Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Login Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Login Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtCompany
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtUser
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
  If p_web.GSV('NewPasswordRequired') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('New Login Password Required')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'New Login Password Required')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('New Login Password Required')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('New Login Password Required')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNewPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNewPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locConfirmNewPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locConfirmNewPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('UserMobileRequired') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Mobile Number Required')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Mobile Number Required')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Mobile Number Required')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Mobile Number Required')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locMobileNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Validate::txtCompany  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtCompany  ! copies value to session value if valid.

ValidateValue::txtCompany  Routine
    If not (1=0)
    End

Value::txtCompany  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('txtCompany') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtCompany" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('LoginDetails1'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::txtUser  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtUser  ! copies value to session value if valid.

ValidateValue::txtUser  Routine
    If not (1=0)
    End

Value::txtUser  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('txtUser') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtUser" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('LoginDetails2'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locNewPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('New Password:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNewPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNewPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNewPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locNewPassword  ! copies value to session value if valid.
  do Value::locNewPassword
  do SendAlert

ValidateValue::locNewPassword  Routine
    If not (1=0)
  If locNewPassword = '' and p_web.GSV('NewPasswordRequired') = 1
    loc:Invalid = 'locNewPassword'
    locNewPassword:IsInvalid = true
    loc:alert = p_web.translate('New Password:') & ' ' & p_web.site.RequiredText
  End
    locNewPassword = Upper(locNewPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locNewPassword',locNewPassword).
    End

Value::locNewPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('NewPasswordRequired') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    locNewPassword = p_web.RestoreValue('locNewPassword')
    do ValidateValue::locNewPassword
    If locNewPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewPassword'',''formnewpassword_locnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locNewPassword',p_web.GetSessionValueFormat('locNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locConfirmNewPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm New Password:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locConfirmNewPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locConfirmNewPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locConfirmNewPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locConfirmNewPassword  ! copies value to session value if valid.
  do Value::locConfirmNewPassword
  do SendAlert

ValidateValue::locConfirmNewPassword  Routine
    If not (1=0)
  If locConfirmNewPassword = '' and p_web.GSV('NewPasswordRequired') = 1
    loc:Invalid = 'locConfirmNewPassword'
    locConfirmNewPassword:IsInvalid = true
    loc:alert = p_web.translate('Confirm New Password:') & ' ' & p_web.site.RequiredText
  End
    locConfirmNewPassword = Upper(locConfirmNewPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword).
    End

Value::locConfirmNewPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('NewPasswordRequired') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    locConfirmNewPassword = p_web.RestoreValue('locConfirmNewPassword')
    do ValidateValue::locConfirmNewPassword
    If locConfirmNewPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locConfirmNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConfirmNewPassword'',''formnewpassword_locconfirmnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locConfirmNewPassword',p_web.GetSessionValueFormat('locConfirmNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locMobileNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Mobile Number:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileNumber  ! copies value to session value if valid.
  do Value::locMobileNumber
  do SendAlert

ValidateValue::locMobileNumber  Routine
    If not (1=0)
  If locMobileNumber = '' and p_web.GSV('UserMobileRequired') = 1
    loc:Invalid = 'locMobileNumber'
    locMobileNumber:IsInvalid = true
    loc:alert = p_web.translate('Mobile Number:') & ' ' & p_web.site.RequiredText
  End
    locMobileNumber = Upper(locMobileNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileNumber',locMobileNumber).
    End

Value::locMobileNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('UserMobileRequired') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    locMobileNumber = p_web.RestoreValue('locMobileNumber')
    do ValidateValue::locMobileNumber
    If locMobileNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locMobileNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMobileNumber'',''formnewpassword_locmobilenumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMobileNumber',p_web.GetSessionValueFormat('locMobileNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormNewPassword_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('FormNewPassword_nexttab_' & 1)
    locNewPassword = p_web.GSV('locNewPassword')
    do ValidateValue::locNewPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locNewPassword
      !do SendAlert
      !exit
    End
    locConfirmNewPassword = p_web.GSV('locConfirmNewPassword')
    do ValidateValue::locConfirmNewPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locConfirmNewPassword
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormNewPassword_nexttab_' & 2)
    locMobileNumber = p_web.GSV('locMobileNumber')
    do ValidateValue::locMobileNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileNumber
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormNewPassword_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormNewPassword_tab_' & 0)
    do GenerateTab0
  of lower('FormNewPassword_tab_' & 1)
    do GenerateTab1
  of lower('FormNewPassword_locNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewPassword
      of event:timer
        do Value::locNewPassword
      else
        do Value::locNewPassword
      end
  of lower('FormNewPassword_locConfirmNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConfirmNewPassword
      of event:timer
        do Value::locConfirmNewPassword
      else
        do Value::locConfirmNewPassword
      end
  of lower('FormNewPassword_tab_' & 2)
    do GenerateTab2
  of lower('FormNewPassword_locMobileNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMobileNumber
      of event:timer
        do Value::locMobileNumber
      else
        do Value::locMobileNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)

  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('NewPasswordRequired') = 1
      If not (1=0)
          If p_web.IfExistsValue('locNewPassword')
            locNewPassword = p_web.GetValue('locNewPassword')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locConfirmNewPassword')
            locConfirmNewPassword = p_web.GetValue('locConfirmNewPassword')
          End
      End
  End
  If p_web.GSV('UserMobileRequired') = 1
      If not (1=0)
          If p_web.IfExistsValue('locMobileNumber')
            locMobileNumber = p_web.GetValue('locMobileNumber')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
      IF (loc:Invalid)
          EXIT
      END
      IF (p_web.GSV('NewPasswordRequired') = 1)
          ! Do the passwords match?
          IF (p_web.GSV('locNewPassword') <> p_web.GSV('locConfirmNewPassword'))
              loc:Invalid = 'locConfirmNewPassword'
              loc:Alert = 'The entered passwords do not match.'
              EXIT
          END
  
          ! Has the password changed?
          IF (p_web.GSV('locNewPassword') = p_web.GSV('loc:Password'))
              loc:Invalid = 'locNewPassword'
              loc:Alert = 'You have not entered a NEW password.'
              EXIT
          END !IF
          ! Is the password already taken?
          Access:USERS.Clearkey(use:Password_Key)
          use:Password = p_web.GSV('locNewPassword')
          IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
              loc:Invalid = 'locNewPassword'
              loc:Alert = 'The selected password is already in use. Please try again.'
              EXIT
          END  ! IF
  
      END ! IF
  
      IF (p_web.GSV('UserMobileRequired') = 1)
          IF (NOT PassMobileNumberFormat(p_web.GSV('locMobileNumber')))
              loc:Alert = 'The Mobile Number Format Is Not Correct'
              loc:Invalid = 'locMobileNumber'
              EXIT
          END ! IF
          ! All ok, get original user and update it
          Access:USERS.Clearkey(use:Password_Key)
          use:Password = p_web.GSV('loc:Password')
          IF (Access:USERS.Tryfetch(use:Password_Key))
              loc:Alert = 'An Error Has Occurred'
              loc:Invalid = 'locNewPassword'
              EXIT
          END !IF
      END ! IF
  
      ! All ok, get original user and update it
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = p_web.GSV('loc:Password')
      IF (Access:USERS.Tryfetch(use:Password_Key))
          loc:Alert = 'An Error Has Occurred'
          loc:Invalid = 'locNewPassword'
          EXIT
      END !IF
  
      IF (p_web.GSV('NewPasswordRequired') = 1)
          IF (p_web.GSV('locNewPassword') <> '')
              use:Password = p_web.GSV('locNewPassword')
              use:PasswordLastChanged = TODAY()
          END ! IF
      END ! IF
  
      IF (p_web.GSV('UserMobileRequired') = 1)
          IF (p_web.GSV('locMobileNumber') <> '')
              use:MobileNumber = p_web.GSV('locMobileNumber')
          END ! IF
      END ! IF
  
      Access:USERS.TryUpdate()
  
      SetLoginDefaults(p_web)

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormNewPassword_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormNewPassword_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::txtCompany
    If loc:Invalid then exit.
    do ValidateValue::txtUser
    If loc:Invalid then exit.
  ! tab = 1
  If p_web.GSV('NewPasswordRequired') = 1
    loc:InvalidTab += 1
    do ValidateValue::locNewPassword
    If loc:Invalid then exit.
    do ValidateValue::locConfirmNewPassword
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('UserMobileRequired') = 1
    loc:InvalidTab += 1
    do ValidateValue::locMobileNumber
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locNewPassword')
  p_web.StoreValue('locConfirmNewPassword')
  p_web.StoreValue('locMobileNumber')

SetLoginDefaults     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = p_web.GSV('loc:Password')
    IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
        Access:TRADEACC.Clearkey(tra:SiteLocationKey)
        tra:SiteLocation = use:Location
        IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
            p_web.SSV('BookingBranchID',tra:BranchIdentification)
            p_web.SSV('BookingAccount',Clip(tra:Account_Number))
            p_web.SSV('BookingName',Clip(DoCaps(tra:Company_Name)))
            p_web.SSV('BookingUserPassword',use:Password)
            p_web.SSV('BookingUserCode',use:User_Code)
            p_web.SSV('BookingSiteLocation',tra:SiteLocation)
            p_web.SSV('BookingRestrictParts',use:RestrictParts)
            p_web.SSV('BookingRestrictChargeable',use:RestrictChargeable)
            p_web.SSV('BookingSkillLevel',use:SkillLevel)
            p_web.SSV('BookingStoresAccount',tra:StoresAccount)
            If tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                p_web.SSV('BookingSite','ARC')
                !p_web.SSV('Filter:BrowseJobs','Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                p_web.SSV('Filter:BrowseJobs','')

            Else ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
                p_web.SSV('BookingSite','RRC')
                !p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)  And Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)')
            End ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
            ! Set Some Defaults
            p_web.SSV('Default:InTransitPUP',GETINI('RRC','InTransitToPUPLocation',,Path() & '\SB2KDEF.INI'))
            p_web.SSV('Default:PUPLocation',GETINI('RRC','AtPUPLocation',,Path() & '\SB2KDEF.INI'))
            p_web.SSV('Default:ARCLocation',GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusSendToRRC',GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:DespatchToCustomer',GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:InTransitARC',GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:RRCLocation',GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusSendToARC',GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:InTransitRRC',GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusReceivedFromPUP',GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI'))
            p_web.SSV('Default:AccountNumber',tra:Account_Number)
            p_web.SSV('Default:SiteLocation',tra:SiteLocation)

            p_web.SSV('Default:TermsText',GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')) ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
            p_web.DeleteSessionValue('loc:Password')
            glo:Password = use:Password

            p_web.SSV('ARC:AccountNumber',GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))

            p_web.SSV('LoginDetails1','Site: ' & clip(tra:Company_Name))
            p_web.SSV('LoginDetails2','User: ' & clip(use:forename) & ' ' & clip(use:Surname))

            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number    = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
            if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                ! Found
                p_web.SSV('ARC:SiteLocation',tra:SiteLocation)
            else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)

            IF (SecurityCheckFailed(use:Password,'JOBS - INSERT')) ! #11682 Check access. (Bryan: 07/09/2010)
                p_web.SSV('Hide:ButtonCreateNewJob',1)
            ELSE
                p_web.SSV('Hide:ButtonCreateNewJob',0)
            END

            IF (SecurityCheckFailed(use:Password,'JOBS - CHANGE'))
                p_web.SSV('Hide:ButtonJobSearch',1)
            ELSE
                p_web.SSV('Hide:ButtonJobSearch',0)
            END
            
        
        END  !IF
    END ! IF

    Do CloseFiles

    p_web.SSV('NewPasswordRequired','')
    p_web.SSV('UserMobileRequired','')
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
SendSMSText          PROCEDURE  (String SentJob,String SentAuto,String SentSpecial,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
    vod.SendSMS(SentJob,SentAuto,SentSpecial,p_web)  ! #12477 Call the SMS routine (DBH: 17/05/2012)
