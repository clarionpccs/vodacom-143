

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE024.INC'),ONCE        !Local module procedure declarations
                     END


CloseWebPage         PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet         String(NET:MaxBinData)
packetlen      Long
CRLF           String('<13,10>')
NBSP           String('&#160;')
loc:options    String(OptionsStringLen)  ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)

  CODE
  GlobalErrors.SetProcedureName('CloseWebPage')
    p_web.SetSessionValue('tmp:Manufacturer','')
    p_web.SetSessionValue('tmp:ModelNumber','')
    p_web.SetSessionValue('tmp:TransitType','')
    p_web.SetSessionValue('tmp:IMEINumber','')
    p_web.SetSessionValue('tmp:ProductCode','')
    p_web.SetSessionValue('tmp:UnitType','')
    p_web.SetSessionValue('tmp:Network','')
    p_web.SetSessionValue('tmp:MobileNumber','')
    p_web.SetSessionValue('tmp:ChargeableJob','')
    p_web.SetSessionValue('tmp:ChargeableChargeType','')
    p_web.SetSessionValue('tmp:WarrantyJob','')
    p_web.SetSessionValue('tmp:WarrantyChargeType','')
    p_web.SetSessionValue('tmp:Colour','')
    p_web.SetSessionValue('tmp:DOP','')
    p_web.SetSessionValue('tmp:AccountNumber','')
    p_web.SetSessionValue('tmp:OrderNumber','')
    p_web.SetSessionValue('tmp:Initial','')
    p_web.SetSessionValue('tmp:Title','')
    p_web.SetSessionValue('tmp:Surname','')
    p_web.SetSessionValue('tmp:EndUserTelephoneNumber','')
    p_web.SetSessionValue('tmp:AuthorityNumber','')
    p_web.SetSessionValue('tmp:BookingOption','')
    p_web.SetSessionValue('tmp:VSACustomer','')
    p_web.SetSessionValue('tmp:HubRepair','')
    p_web.SetSessionValue('tmp:PrintDuplicateJobCard','')
    p_web.SetSessionValue('tmp:Contract','')
    p_web.SetSessionValue('tmp:PrePaid','')
    p_web.SetSessionValue('tmp:CustomerName','')
    p_web.SetSessionValue('tmp:AddressLine1','')
    p_web.SetSessionValue('tmp:AddressLine2','')
    p_web.SetSessionValue('tmp:Suburb','')
    p_web.SetSessionValue('tmp:Postcode','')
    p_web.SetSessionValue('tmp:TelephoneNumber','')
    p_web.SetSessionValue('tmp:FaxNumber','')
    p_web.SetSessionValue('tmp:CustomerNameCollection','')
    p_web.SetSessionValue('tmp:AddressLine1Collection','')
    p_web.SetSessionValue('tmp:AddressLine2Collection','')
    p_web.SetSessionValue('tmp:SuburbCollection','')
    p_web.SetSessionValue('tmp:PostcodeCollection','')
    p_web.SetSessionValue('tmp:TelephoneNumberCollection','')
    p_web.SetSessionValue('tmp:CompanyNameDelivery','')
    p_web.SetSessionValue('tmp:AddressLine1Delivery','')
    p_web.SetSessionValue('tmp:AddressLine2Delivery','')
    p_web.SetSessionValue('tmp:SuburbDelivery','')
    p_web.SetSessionValue('tmp:PostcodeDelivery','')
    p_web.SetSessionValue('tmp:TelephoneNumberDelivery','')
    p_web.SetSessionValue('tmp:EmailAddress','')
    p_web.SetSessionValue('tmp:OutgoingCourier','')
    p_web.SetSessionValue('tmp:VATNumber','')
    p_web.SetSessionValue('tmp:IDNumber','')
    p_web.SetSessionValue('tmp:SMSNotification','')
    p_web.SetSessionValue('tmp:SMSMobileNumber','')
    p_web.SetSessionValue('tmp:EmailNotification','')
    p_web.SetSessionValue('tmp:EmailAlertAddress','')
    p_web.SetSessionValue('tmp:CollectionText','')
    p_web.SetSessionValue('tmp:DeliveryText','')
    p_web.SetSessionValue('tmp:SubSubAccountNumber','')
    p_web.SetSessionValue('tmp:CheckAntenna','')
    p_web.SetSessionValue('tmp:CheckLens','')
    p_web.SetSessionValue('tmp:CheckFCover','')
    p_web.SetSessionValue('tmp:CheckBCover','')
    p_web.SetSessionValue('tmp:CheckKeypad','')
    p_web.SetSessionValue('tmp:CheckBattery','')
    p_web.SetSessionValue('tmp:CheckCharger','')
    p_web.SetSessionValue('tmp:CheckLCD','')
    p_web.SetSessionValue('tmp:CheckSimReader','')
    p_web.SetSessionValue('tmp:CheckSystemConnector','')
    p_web.SetSessionValue('tmp:CheckNone','')
    p_web.SetSessionValue('tmp:CheckNotes','')
    p_web.SetSessionValue('tmp:FaultDescription','')
    p_web.SetSessionValue('tmp:EngineerNotes','')
    p_web.SetSessionValue('tmp:InvoiceText','')
    p_web.SetSessionValue('tmp:OBF','')
    p_web.SetSessionValue('tmp:Location','')
    p_web.SetSessionValue('tmp:Workshop','')
    p_web.SetSessionValue('tmp:TurnaroundTime','')
    p_web.SetSessionValue('tmp:MSN','')
    p_web.SetSessionValue('tmp:IMEIValidation','')
    p_web.SetSessionValue('tmp:POP','')
    p_web.SetSessionValue('tmp:OBFValidated','')
    p_web.SetSessionValue('tmp:OBFValidateDate','')
    p_web.SetSessionValue('tmp:OBFValidateTime','')

    p_web.SetSessionValue('ReadOnly:Manufacturer','')
    p_web.SetSessionValue('ReadOnly:ModelNumber','')
    p_web.SetSessionValue('ReadOnly:TransitType','')
    p_web.SetSessionValue('ReadOnly:IMEINumber','')
    p_web.SetSessionValue('ReadOnly:ProductCode','')
    p_web.SetSessionValue('ReadOnly:UnitType','')
    p_web.SetSessionValue('ReadOnly:Network','')
    p_web.SetSessionValue('ReadOnly:MobileNumber','')
    p_web.SetSessionValue('ReadOnly:ChargeableJob','')
    p_web.SetSessionValue('ReadOnly:ChargeableChargeType','')
    p_web.SetSessionValue('ReadOnly:WarrantyJob','')
    p_web.SetSessionValue('ReadOnly:WarrantyChargeType','')
    p_web.SetSessionValue('ReadOnly:Colour','')
    p_web.SetSessionValue('ReadOnly:DOP','')
    p_web.SetSessionValue('ReadOnly:AccountNumber','')
    p_web.SetSessionValue('ReadOnly:OrderNumber','')
    p_web.SetSessionValue('ReadOnly:Initial','')
    p_web.SetSessionValue('ReadOnly:Title','')
    p_web.SetSessionValue('ReadOnly:Surname','')
    p_web.SetSessionValue('ReadOnly:EndUserTelephoneNumber','')
    p_web.SetSessionValue('ReadOnly:AuthorityNumber','')
    p_web.SetSessionValue('ReadOnly:BookingOption','')
    p_web.SetSessionValue('ReadOnly:VSACustomer','')
    p_web.SetSessionValue('ReadOnly:HubRepair','')
    p_web.SetSessionValue('ReadOnly:PrintDuplicateJobCard','')
    p_web.SetSessionValue('ReadOnly:Contract','')
    p_web.SetSessionValue('ReadOnly:PrePaid','')
    p_web.SetSessionValue('ReadOnly:CustomerName','')
    p_web.SetSessionValue('ReadOnly:AddressLine1','')
    p_web.SetSessionValue('ReadOnly:AddressLine2','')
    p_web.SetSessionValue('ReadOnly:Suburb','')
    p_web.SetSessionValue('ReadOnly:Postcode','')
    p_web.SetSessionValue('ReadOnly:TelephoneNumber','')
    p_web.SetSessionValue('ReadOnly:FaxNumber','')
    p_web.SetSessionValue('ReadOnly:CustomerNameCollection','')
    p_web.SetSessionValue('ReadOnly:AddressLine1Collection','')
    p_web.SetSessionValue('ReadOnly:AddressLine2Collection','')
    p_web.SetSessionValue('ReadOnly:SuburbCollection','')
    p_web.SetSessionValue('ReadOnly:PostcodeCollection','')
    p_web.SetSessionValue('ReadOnly:TelephoneNumberCollection','')
    p_web.SetSessionValue('ReadOnly:CompanyNameDelivery','')
    p_web.SetSessionValue('ReadOnly:AddressLine1Delivery','')
    p_web.SetSessionValue('ReadOnly:AddressLine2Delivery','')
    p_web.SetSessionValue('ReadOnly:SuburbDelivery','')
    p_web.SetSessionValue('ReadOnly:PostcodeDelivery','')
    p_web.SetSessionValue('ReadOnly:TelephoneNumberDelivery','')
    p_web.SetSessionValue('ReadOnly:EmailAddress','')
    p_web.SetSessionValue('ReadOnly:OutgoingCourier','')
    p_web.SetSessionValue('ReadOnly:VATNumber','')
    p_web.SetSessionValue('ReadOnly:IDNumber','')
    p_web.SetSessionValue('ReadOnly:SMSNotification','')
    p_web.SetSessionValue('ReadOnly:SMSMobileNumber','')
    p_web.SetSessionValue('ReadOnly:EmailNotification','')
    p_web.SetSessionValue('ReadOnly:EmailAlertAddress','')
    p_web.SetSessionValue('ReadOnly:CollectionText','')
    p_web.SetSessionValue('ReadOnly:DeliveryText','')
    p_web.SetSessionValue('ReadOnly:SubSubAccountNumber','')
    p_web.SetSessionValue('ReadOnly:CheckAntenna','')
    p_web.SetSessionValue('ReadOnly:CheckLens','')
    p_web.SetSessionValue('ReadOnly:CheckFCover','')
    p_web.SetSessionValue('ReadOnly:CheckBCover','')
    p_web.SetSessionValue('ReadOnly:CheckKeypad','')
    p_web.SetSessionValue('ReadOnly:CheckBattery','')
    p_web.SetSessionValue('ReadOnly:CheckCharger','')
    p_web.SetSessionValue('ReadOnly:CheckLCD','')
    p_web.SetSessionValue('ReadOnly:CheckSimReader','')
    p_web.SetSessionValue('ReadOnly:CheckSystemConnector','')
    p_web.SetSessionValue('ReadOnly:CheckNone','')
    p_web.SetSessionValue('ReadOnly:CheckNotes','')
    p_web.SetSessionValue('ReadOnly:FaultDescription','')
    p_web.SetSessionValue('ReadOnly:EngineerNotes','')
    p_web.SetSessionValue('ReadOnly:InvoiceText','')
    p_web.SetSessionValue('ReadOnly:OBF','')
    p_web.SetSessionValue('ReadOnly:Location','')
    p_web.SetSessionValue('ReadOnly:Workshop','')
    p_web.SetSessionValue('ReadOnly:TurnaroundTime','')
    p_web.SetSessionValue('ReadOnly:MSN','')
    p_web.SetSessionValue('ReadOnly:IMEIValidation','')
    p_web.SetSessionValue('ReadOnly:POP','')
    p_web.SetSessionValue('ReadOnly:OBFValidated','')
    p_web.SetSessionValue('ReadOnly:OBFValidateDate','')
    p_web.SetSessionValue('ReadOnly:OBFValidateTime','')

    p_web.SetSessionValue('Hide:Manufacturer','')
    p_web.SetSessionValue('Hide:ModelNumber','')
    p_web.SetSessionValue('Hide:TransitType','')
    p_web.SetSessionValue('Hide:IMEINumber','')
    p_web.SetSessionValue('Hide:ProductCode','')
    p_web.SetSessionValue('Hide:UnitType','')
    p_web.SetSessionValue('Hide:Network','')
    p_web.SetSessionValue('Hide:MobileNumber','')
    p_web.SetSessionValue('Hide:ChargeableJob','')
    p_web.SetSessionValue('Hide:ChargeableChargeType','')
    p_web.SetSessionValue('Hide:WarrantyJob','')
    p_web.SetSessionValue('Hide:WarrantyChargeType','')
    p_web.SetSessionValue('Hide:Colour','')
    p_web.SetSessionValue('Hide:DOP','')
    p_web.SetSessionValue('Hide:AccountNumber','')
    p_web.SetSessionValue('Hide:OrderNumber','')
    p_web.SetSessionValue('Hide:Initial','')
    p_web.SetSessionValue('Hide:Title','')
    p_web.SetSessionValue('Hide:Surname','')
    p_web.SetSessionValue('Hide:EndUserTelephoneNumber','')
    p_web.SetSessionValue('Hide:AuthorityNumber','')
    p_web.SetSessionValue('Hide:BookingOption','')
    p_web.SetSessionValue('Hide:VSACustomer','')
    p_web.SetSessionValue('Hide:HubRepair','')
    p_web.SetSessionValue('Hide:PrintDuplicateJobCard','')
    p_web.SetSessionValue('Hide:Contract','')
    p_web.SetSessionValue('Hide:PrePaid','')
    p_web.SetSessionValue('Hide:CustomerName','')
    p_web.SetSessionValue('Hide:AddressLine1','')
    p_web.SetSessionValue('Hide:AddressLine2','')
    p_web.SetSessionValue('Hide:Suburb','')
    p_web.SetSessionValue('Hide:Postcode','')
    p_web.SetSessionValue('Hide:TelephoneNumber','')
    p_web.SetSessionValue('Hide:FaxNumber','')
    p_web.SetSessionValue('Hide:CustomerNameCollection','')
    p_web.SetSessionValue('Hide:AddressLine1Collection','')
    p_web.SetSessionValue('Hide:AddressLine2Collection','')
    p_web.SetSessionValue('Hide:SuburbCollection','')
    p_web.SetSessionValue('Hide:PostcodeCollection','')
    p_web.SetSessionValue('Hide:TelephoneNumberCollection','')
    p_web.SetSessionValue('Hide:CompanyNameDelivery','')
    p_web.SetSessionValue('Hide:AddressLine1Delivery','')
    p_web.SetSessionValue('Hide:AddressLine2Delivery','')
    p_web.SetSessionValue('Hide:SuburbDelivery','')
    p_web.SetSessionValue('Hide:PostcodeDelivery','')
    p_web.SetSessionValue('Hide:TelephoneNumberDelivery','')
    p_web.SetSessionValue('Hide:EmailAddress','')
    p_web.SetSessionValue('Hide:OutgoingCourier','')
    p_web.SetSessionValue('Hide:VATNumber','')
    p_web.SetSessionValue('Hide:IDNumber','')
    p_web.SetSessionValue('Hide:SMSNotification','')
    p_web.SetSessionValue('Hide:SMSMobileNumber','')
    p_web.SetSessionValue('Hide:EmailNotification','')
    p_web.SetSessionValue('Hide:EmailAlertAddress','')
    p_web.SetSessionValue('Hide:CollectionText','')
    p_web.SetSessionValue('Hide:DeliveryText','')
    p_web.SetSessionValue('Hide:SubSubAccountNumber','')
    p_web.SetSessionValue('Hide:CheckAntenna','')
    p_web.SetSessionValue('Hide:CheckLens','')
    p_web.SetSessionValue('Hide:CheckFCover','')
    p_web.SetSessionValue('Hide:CheckBCover','')
    p_web.SetSessionValue('Hide:CheckKeypad','')
    p_web.SetSessionValue('Hide:CheckBattery','')
    p_web.SetSessionValue('Hide:CheckCharger','')
    p_web.SetSessionValue('Hide:CheckLCD','')
    p_web.SetSessionValue('Hide:CheckSimReader','')
    p_web.SetSessionValue('Hide:CheckSystemConnector','')
    p_web.SetSessionValue('Hide:CheckNone','')
    p_web.SetSessionValue('Hide:CheckNotes','')
    p_web.SetSessionValue('Hide:FaultDescription','')
    p_web.SetSessionValue('Hide:EngineerNotes','')
    p_web.SetSessionValue('Hide:InvoiceText','')
    p_web.SetSessionValue('Hide:OBF','')
    p_web.SetSessionValue('Hide:Location','')
    p_web.SetSessionValue('Hide:Workshop','')
    p_web.SetSessionValue('Hide:TurnaroundTime','')
    p_web.SetSessionValue('Hide:MSN','')
    p_web.SetSessionValue('Hide:IMEIValidation','')
    p_web.SetSessionValue('Hide:POP','')
    p_web.SetSessionValue('Hide:OBFValidated','')
    p_web.SetSessionValue('Hide:OBFValidateDate','')
    p_web.SetSessionValue('Hide:OBFValidateTime','')

    p_web.SetSessionValue('Comment:Manufacturer','')
    p_web.SetSessionValue('Comment:ModelNumber','')
    p_web.SetSessionValue('Comment:TransitType','')
    p_web.SetSessionValue('Comment:IMEINumber','')
    p_web.SetSessionValue('Comment:ProductCode','')
    p_web.SetSessionValue('Comment:UnitType','')
    p_web.SetSessionValue('Comment:Network','')
    p_web.SetSessionValue('Comment:MobileNumber','')
    p_web.SetSessionValue('Comment:ChargeableJob','')
    p_web.SetSessionValue('Comment:ChargeableChargeType','')
    p_web.SetSessionValue('Comment:WarrantyJob','')
    p_web.SetSessionValue('Comment:WarrantyChargeType','')
    p_web.SetSessionValue('Comment:Colour','')
    p_web.SetSessionValue('Comment:DOP','')
    p_web.SetSessionValue('Comment:AccountNumber','')
    p_web.SetSessionValue('Comment:OrderNumber','')
    p_web.SetSessionValue('Comment:Initial','')
    p_web.SetSessionValue('Comment:Title','')
    p_web.SetSessionValue('Comment:Surname','')
    p_web.SetSessionValue('Comment:EndUserTelephoneNumber','')
    p_web.SetSessionValue('Comment:AuthorityNumber','')
    p_web.SetSessionValue('Comment:BookingOption','')
    p_web.SetSessionValue('Comment:VSACustomer','')
    p_web.SetSessionValue('Comment:HubRepair','')
    p_web.SetSessionValue('Comment:PrintDuplicateJobCard','')
    p_web.SetSessionValue('Comment:Contract','')
    p_web.SetSessionValue('Comment:PrePaid','')
    p_web.SetSessionValue('Comment:CustomerName','')
    p_web.SetSessionValue('Comment:AddressLine1','')
    p_web.SetSessionValue('Comment:AddressLine2','')
    p_web.SetSessionValue('Comment:Suburb','')
    p_web.SetSessionValue('Comment:Postcode','')
    p_web.SetSessionValue('Comment:TelephoneNumber','')
    p_web.SetSessionValue('Comment:FaxNumber','')
    p_web.SetSessionValue('Comment:CustomerNameCollection','')
    p_web.SetSessionValue('Comment:AddressLine1Collection','')
    p_web.SetSessionValue('Comment:AddressLine2Collection','')
    p_web.SetSessionValue('Comment:SuburbCollection','')
    p_web.SetSessionValue('Comment:PostcodeCollection','')
    p_web.SetSessionValue('Comment:TelephoneNumberCollection','')
    p_web.SetSessionValue('Comment:CompanyNameDelivery','')
    p_web.SetSessionValue('Comment:AddressLine1Delivery','')
    p_web.SetSessionValue('Comment:AddressLine2Delivery','')
    p_web.SetSessionValue('Comment:SuburbDelivery','')
    p_web.SetSessionValue('Comment:PostcodeDelivery','')
    p_web.SetSessionValue('Comment:TelephoneNumberDelivery','')
    p_web.SetSessionValue('Comment:EmailAddress','')
    p_web.SetSessionValue('Comment:OutgoingCourier','')
    p_web.SetSessionValue('Comment:VATNumber','')
    p_web.SetSessionValue('Comment:IDNumber','')
    p_web.SetSessionValue('Comment:SMSNotification','')
    p_web.SetSessionValue('Comment:SMSMobileNumber','')
    p_web.SetSessionValue('Comment:EmailNotification','')
    p_web.SetSessionValue('Comment:EmailAlertAddress','')
    p_web.SetSessionValue('Comment:CollectionText','')
    p_web.SetSessionValue('Comment:DeliveryText','')
    p_web.SetSessionValue('Comment:SubSubAccountNumber','')
    p_web.SetSessionValue('Comment:CheckAntenna','')
    p_web.SetSessionValue('Comment:CheckLens','')
    p_web.SetSessionValue('Comment:CheckFCover','')
    p_web.SetSessionValue('Comment:CheckBCover','')
    p_web.SetSessionValue('Comment:CheckKeypad','')
    p_web.SetSessionValue('Comment:CheckBattery','')
    p_web.SetSessionValue('Comment:CheckCharger','')
    p_web.SetSessionValue('Comment:CheckLCD','')
    p_web.SetSessionValue('Comment:CheckSimReader','')
    p_web.SetSessionValue('Comment:CheckSystemConnector','')
    p_web.SetSessionValue('Comment:CheckNone','')
    p_web.SetSessionValue('Comment:CheckNotes','')
    p_web.SetSessionValue('Comment:FaultDescription','')
    p_web.SetSessionValue('Comment:EngineerNotes','')
    p_web.SetSessionValue('Comment:InvoiceText','')
    p_web.SetSessionValue('Comment:OBF','')
    p_web.SetSessionValue('Comment:Location','')
    p_web.SetSessionValue('Comment:Workshop','')
    p_web.SetSessionValue('Comment:TurnaroundTime','')
    p_web.SetSessionValue('Comment:MSN','')
    p_web.SetSessionValue('Comment:IMEIValidation','')
    p_web.SetSessionValue('Comment:POP','')
    p_web.SetSessionValue('Comment:OBFValidated','')
    p_web.SetSessionValue('Comment:OBFValidateDate','')
    p_web.SetSessionValue('Comment:OBFValidateTime','')

    p_web.SetSessionValue('Save:Manufacturer','')
    p_web.SetSessionValue('Save:ModelNumber','')
    p_web.SetSessionValue('Save:TransitType','')
    p_web.SetSessionValue('Save:IMEINumber','')
    p_web.SetSessionValue('Save:ProductCode','')
    p_web.SetSessionValue('Save:UnitType','')
    p_web.SetSessionValue('Save:Network','')
    p_web.SetSessionValue('Save:MobileNumber','')
    p_web.SetSessionValue('Save:ChargeableJob','')
    p_web.SetSessionValue('Save:ChargeableChargeType','')
    p_web.SetSessionValue('Save:WarrantyJob','')
    p_web.SetSessionValue('Save:WarrantyChargeType','')
    p_web.SetSessionValue('Save:Colour','')
    p_web.SetSessionValue('Save:DOP','')
    p_web.SetSessionValue('Save:AccountNumber','')
    p_web.SetSessionValue('Save:OrderNumber','')
    p_web.SetSessionValue('Save:Initial','')
    p_web.SetSessionValue('Save:Title','')
    p_web.SetSessionValue('Save:Surname','')
    p_web.SetSessionValue('Save:EndUserTelephoneNumber','')
    p_web.SetSessionValue('Save:AuthorityNumber','')
    p_web.SetSessionValue('Save:BookingOption','')
    p_web.SetSessionValue('Save:VSACustomer','')
    p_web.SetSessionValue('Save:HubRepair','')
    p_web.SetSessionValue('Save:PrintDuplicateJobCard','')
    p_web.SetSessionValue('Save:Contract','')
    p_web.SetSessionValue('Save:PrePaid','')
    p_web.SetSessionValue('Save:CustomerName','')
    p_web.SetSessionValue('Save:AddressLine1','')
    p_web.SetSessionValue('Save:AddressLine2','')
    p_web.SetSessionValue('Save:Suburb','')
    p_web.SetSessionValue('Save:Postcode','')
    p_web.SetSessionValue('Save:TelephoneNumber','')
    p_web.SetSessionValue('Save:FaxNumber','')
    p_web.SetSessionValue('Save:CustomerNameCollection','')
    p_web.SetSessionValue('Save:AddressLine1Collection','')
    p_web.SetSessionValue('Save:AddressLine2Collection','')
    p_web.SetSessionValue('Save:SuburbCollection','')
    p_web.SetSessionValue('Save:PostcodeCollection','')
    p_web.SetSessionValue('Save:TelephoneNumberCollection','')
    p_web.SetSessionValue('Save:CompanyNameDelivery','')
    p_web.SetSessionValue('Save:AddressLine1Delivery','')
    p_web.SetSessionValue('Save:AddressLine2Delivery','')
    p_web.SetSessionValue('Save:SuburbDelivery','')
    p_web.SetSessionValue('Save:PostcodeDelivery','')
    p_web.SetSessionValue('Save:TelephoneNumberDelivery','')
    p_web.SetSessionValue('Save:EmailAddress','')
    p_web.SetSessionValue('Save:OutgoingCourier','')
    p_web.SetSessionValue('Save:VATNumber','')
    p_web.SetSessionValue('Save:IDNumber','')
    p_web.SetSessionValue('Save:SMSNotification','')
    p_web.SetSessionValue('Save:SMSMobileNumber','')
    p_web.SetSessionValue('Save:EmailNotification','')
    p_web.SetSessionValue('Save:EmailAlertAddress','')
    p_web.SetSessionValue('Save:CollectionText','')
    p_web.SetSessionValue('Save:DeliveryText','')
    p_web.SetSessionValue('Save:SubSubAccountNumber','')
    p_web.SetSessionValue('Save:CheckAntenna','')
    p_web.SetSessionValue('Save:CheckLens','')
    p_web.SetSessionValue('Save:CheckFCover','')
    p_web.SetSessionValue('Save:CheckBCover','')
    p_web.SetSessionValue('Save:CheckKeypad','')
    p_web.SetSessionValue('Save:CheckBattery','')
    p_web.SetSessionValue('Save:CheckCharger','')
    p_web.SetSessionValue('Save:CheckLCD','')
    p_web.SetSessionValue('Save:CheckSimReader','')
    p_web.SetSessionValue('Save:CheckSystemConnector','')
    p_web.SetSessionValue('Save:CheckNone','')
    p_web.SetSessionValue('Save:CheckNotes','')
    p_web.SetSessionValue('Save:FaultDescription','')
    p_web.SetSessionValue('Save:EngineerNotes','')
    p_web.SetSessionValue('Save:InvoiceText','')
    p_web.SetSessionValue('Save:OBF','')
    p_web.SetSessionValue('Save:Location','')
    p_web.SetSessionValue('Save:Workshop','')
    p_web.SetSessionValue('Save:TurnaroundTime','')
    p_web.SetSessionValue('Save:MSN','')
    p_web.SetSessionValue('Save:IMEIValidation','')
    p_web.SetSessionValue('Save:POP','')
    p_web.SetSessionValue('Save:OBFValidated','')
    p_web.SetSessionValue('Save:OBFValidateDate','')
    p_web.SetSessionValue('Save:OBFValidateTime','')

    Loop x# = 1 To 20
        p_web.SetSessionValue('ReadOnly:FaultCode' & x#,'')
        p_web.SetSessionValue('tmp:FaultCode' & x#,'')
        p_web.SetSessionValue('Hide:FaultCode' & x#,'')
        p_web.SetSessionValue('Hide:FaultCodeLookup' & x#,'')
        p_web.SetSessionValue('Comment:FaultCode' & x#,'')
        p_web.SetSessionValue('Comment:FaultCodeLookup' & x#,'')
        p_web.SetSessionValue('Save:FaultCode' & x#,'')
    End ! Loop x# = 1 To 20

    p_web.SetSessionValue('Error:Generic','')
  p_web.SetValue('_parentPage','CloseWebPage')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web.BodyOnLoad(p_web.Combine(p_web.site.bodyclass,'PageBody'),,p_web.Combine(p_web.site.bodydivclass,'PageBodyDiv'))
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header(p_web.Combine(p_web.site.HtmlClass,))
  packet = clip(packet) & '<head>'&|
      '<title>'&p_web.Translate(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>' &|
      clip(p_web.MetaHeaders)
    Do CloseWindow
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                         '</div></body><13,10></html><13,10>'
CloseWindow  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<script>top.close()<</script> <13,10>'&|
    '',net:OnlyIfUTF)
CountBouncer         PROCEDURE  (f_RefNumber,f_DateBooked,f_IMEI,func:Cjob,func:CCharge,func:CRepair,func:WJob,func:WCharge,func:WRepair) ! Declare Procedure
tmp:DateBooked       DATE                                  !
tmp:count            LONG                                  !
save_job2_id         USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
save_mfo_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wob_id          USHORT,AUTO                           !
tmp:IMEI             STRING(30)                            !
tmp:Manufacturer     STRING(30)                            !
tmp:IgnoreChargeable BYTE(0)                               !Ignore Chargeable
tmp:IgnoreWarranty   BYTE(0)                               !Ignore Warranty
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
local       Class
OutFaultExcluded    Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob),Byte
            End
  CODE
    RETURN vod.CountJobBouncers()

!!Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
!!From that count how many times the IMEI number has been booked in before
!tmp:IgnoreChargeable = GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI')
!tmp:IgnoreWarranty  = GETINI('BOUNCER','IgnoreWarranty',,CLIP(PATH())&'\SB2KDEF.INI')
!
!CheckBouncer# = 1
!If func:CJob = 'YES'
!    If tmp:IgnoreChargeable
!        CheckBouncer# = 0
!    Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!        Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!        cha:Charge_Type = func:CCharge
!        If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            !Found
!            If cha:ExcludeFromBouncer
!                CheckBouncer# = 0
!            Else !If cha:ExcludeFromBouncer
!                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                rtd:Manufacturer = job:Manufacturer
!                rtd:Chargeable   = 'YES'
!                rtd:Repair_Type  = func:CRepair
!                If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                    !Found
!                    If rtd:ExcludeFromBouncer
!                        CheckBouncer# = 0
!                    End !If rtd:ExcludeFromBouncer
!                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!            End !If cha:ExcludeFromBouncer
!        Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!End !If func:CJob = 'YES'
!
!!Is the job's outfault excluded?
!If CheckBouncer#
!    If Local.OutFaultExcluded(job:Ref_Number,job:Warranty_job,job:Chargeable_Job)
!        CheckBouncer# = 0
!    End !If Local.OutFaultExcluded()
!End !If CheckBouncer#
!
!!Is the job's infault excluded?
!If CheckBouncer#
!    Access:MANFAULT.ClearKey(maf:InFaultKey)
!    maf:Manufacturer = job:Manufacturer
!    maf:InFault      = 1
!    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!        Access:MANFAULO.ClearKey(mfo:Field_Key)
!        mfo:Manufacturer = job:Manufacturer
!        mfo:Field_Number = maf:Field_Number
!        Case maf:Field_Number
!        Of 1
!            mfo:Field        = job:Fault_Code1
!        Of 2
!            mfo:Field        = job:Fault_Code2
!        Of 3
!            mfo:Field        = job:Fault_Code3
!        Of 4
!            mfo:Field        = job:Fault_Code4
!        Of 5
!            mfo:Field        = job:Fault_Code5
!        Of 6
!            mfo:Field        = job:Fault_Code6
!        Of 7
!            mfo:Field        = job:Fault_Code7
!        Of 8
!            mfo:Field        = job:Fault_Code8
!        Of 9
!            mfo:Field        = job:Fault_Code9
!        Of 10
!            mfo:Field        = job:Fault_Code10
!        Of 11
!            mfo:Field        = job:Fault_Code11
!        Of 12
!            mfo:Field        = job:Fault_Code12
!        End !Case maf:Field_Number
!        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!            !Found
!            If mfo:ExcludeFromBouncer
!                CheckBouncer# = 0
!            End !If mfo:ExcludeBouncer
!        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!End !If CheckBouncer#
!
!!Lets write the bouncers into a memory queue
!Free(glo:Queue20)
!Clear(glo:Queue20)
!
!
!If CheckBouncer#
!    If func:WJob = 'YES'
!        If tmp:IgnoreWarranty
!            CheckBouncer# = 0
!        Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!            Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!            cha:Charge_Type = func:WCharge
!            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Found
!                If cha:ExcludeFromBouncer
!                    CheckBouncer# = 0
!                Else !If cha:ExcludeFromBouncer
!                    Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                    rtd:Manufacturer = job:Manufacturer
!                    rtd:Warranty     = 'YES'
!                    rtd:Repair_Type  = func:WRepair
!                    If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                        !Found
!                        If rtd:ExcludeFromBouncer
!                            CheckBouncer# = 0
!                        End !If rtd:ExcludeFromBouncer
!                    Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                End !If cha:ExcludeFromBouncer
!            Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!        End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!    End !If func:CJob = 'YES'
!End !If CheckBouncer#
!
!If CheckBouncer#
!    tmp:count    = 0
!    If f_IMEI <> 'N/A' And f_IMEI <> ''
!        Set(defaults)
!        Access:Defaults.Next()
!
!        setcursor(cursor:wait)
!        save_job2_id = access:jobs2_alias.savefile()
!        access:jobs2_alias.clearkey(job2:esn_key)
!        job2:esn = f_imei
!        set(job2:esn_key,job2:esn_key)
!        loop
!            if access:jobs2_alias.next()
!                break
!            end !if
!            if job2:esn <> f_imei      |
!                then break.  ! end if
!            yldcnt# += 1
!            if yldcnt# > 25
!                yield() ; yldcnt# = 0
!            end !if
!
!            If job2:Cancelled = 'YES'
!                Cycle
!            End !If job2:Cancelled = 'YES'
!
!            If job2:Exchange_Unit_Number <> ''
!                Cycle
!            End !If job2:Exchange_Unit_Number <> ''
!
!            If job2:Chargeable_Job = 'YES'
!                If tmp:IgnoreChargeable
!                    Cycle
!                End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job2:Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job2:Repair_Type <> ''
!                            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Chargeable   = 'YES'
!                            rtd:Repair_Type  = job2:Repair_Type
!                            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!
!                        End !If job2:Repair_Type <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!
!            If job2:Warranty_Job = 'YES'
!                If tmp:IgnoreWarranty
!                    Cycle
!                End !If tmp:IgnoreWarranty
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job2:Warranty_Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job2:Repair_Type_Warranty <> ''
!                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Warranty     = 'YES'
!                            rtd:Repair_Type  = job2:Repair_Type_Warranty
!                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        End !If job2:Repair_Type_Warranty <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!            !Has job got the same In Fault?
!
!            Access:MANFAULT.ClearKey(maf:InFaultKey)
!            maf:Manufacturer = job:Manufacturer
!            maf:InFault      = 1
!            If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Found
!                Case maf:Field_Number
!                Of 1
!                    If job2:Fault_Code1 <> job:Fault_Code1
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 2
!                    If job2:Fault_Code2 <> job:Fault_Code2
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 3
!                    If job2:Fault_Code3 <> job:Fault_Code3
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 4
!                    If job2:Fault_Code4 <> job:Fault_Code4
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 5
!                    If job2:Fault_Code5 <> job:Fault_Code5
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 6
!                    If job2:Fault_Code6 <> job:Fault_Code6
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 7
!                    If job2:Fault_Code7 <> job:Fault_Code7
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 8
!                    If job2:Fault_Code8 <> job:Fault_Code8
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 9
!                    If job2:Fault_Code9 <> job:Fault_Code9
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 10
!                    If job2:Fault_Code10 <> job:Fault_Code10
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 11
!                    If job2:Fault_Code11 <> job:Fault_Code11
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 12
!                    If job2:Fault_Code12 <> job:Fault_Code12
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!                End !Case maf:Field_Number
!                !Is the infault excluded?
!                Access:MANFAULO.ClearKey(mfo:Field_Key)
!                mfo:Manufacturer = job:Manufacturer
!                mfo:Field_Number = maf:Field_Number
!                Case maf:Field_Number
!                Of 1
!                    mfo:Field        = job2:Fault_Code1
!                Of 2
!                    mfo:Field        = job2:Fault_Code2
!                Of 3
!                    mfo:Field        = job2:Fault_Code3
!                Of 4
!                    mfo:Field        = job2:Fault_Code4
!                Of 5
!                    mfo:Field        = job2:Fault_Code5
!                Of 6
!                    mfo:Field        = job2:Fault_Code6
!                Of 7
!                    mfo:Field        = job2:Fault_Code7
!                Of 8
!                    mfo:Field        = job2:Fault_Code8
!                Of 9
!                    mfo:Field        = job2:Fault_Code9
!                Of 10
!                    mfo:Field        = job2:Fault_Code10
!                Of 11
!                    mfo:Field        = job2:Fault_Code11
!                Of 12
!                    mfo:Field        = job2:Fault_Code12
!                End !Case maf:Field_Number
!                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Found
!                    If mfo:ExcludeFromBouncer
!                        Cycle
!                    End !If mfo:ExcludeBouncer
!                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!            Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!
!            !Are the outfaults excluded?
!            If Local.OutFaultExcluded(job2:Ref_Number,job2:Warranty_job,job2:Chargeable_Job)
!                Cycle
!            End !If Local.OutFaultExcluded()
!
!            If job2:ref_number <> f_refnumber
!                Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                Of 2 !Despatched Date
!                    If job2:Date_Despatched <> ''
!                        If job2:date_despatched + def:bouncertime > f_datebooked And job2:date_despatched <= f_datebooked
!                            tmp:count += 1
!                            GLO:Pointer20 = job2:Ref_Number
!                            ADd(glo:Queue20)
!                        End!If job2:date_booked + man:warranty_period < Today()
!
!                    Else !If job2:Date_Despatched <> ''
!
!                        !Need to get the webjob record for the bouncer job
!                        !Will save the file, and then restore it afterwards.
!                        !Therefore, no records should be lost.
!
!                        Save_wob_ID = Access:WEBJOB.SaveFile()
!
!                        Access:WEBJOB.Clearkey(wob:RefNumberKey)
!                        wob:RefNumber   = job2:Ref_Number
!                        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            !Found
!
!                        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            !Error
!                        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!
!
!                        If wob:DateJobDespatched <> ''
!                            If wob:DateJobDespatched + def:bouncertime > f_datebooked And wob:DateJobDespatched <= f_datebooked
!
!                                tmp:count += 1
!                                GLO:Pointer20 = job2:Ref_Number
!                                ADd(glo:Queue20)
!                            End!If job2:date_booked + man:warranty_period < Today()
!
!                        End !If wob:JobDateDespatched <> ''
!
!                        Access:WEBJOB.RestoreFile(Save_wob_ID)
!
!                    End !If job2:Date_Despatched <> ''
!                Of 1 !Completed Date
!                    If job2:date_completed + def:bouncertime > f_datebooked And job2:date_completed <= f_datebooked
!                        tmp:count += 1
!                        GLO:Pointer20 = job2:Ref_Number
!                        ADd(glo:Queue20)
!
!                    End!If job2:date_booked + man:warranty_period < Today()
!                Else !Booking Date
!                    If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
!                        tmp:count += 1
!                        GLO:Pointer20 = job2:Ref_Number
!                        ADd(glo:Queue20)
!                    End!If job2:date_booked + man:warranty_period < Today()
!
!                End !Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                Else !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                End !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!            End!If job2:esn <> job2:ref_number
!        end !loop
!        access:jobs2_alias.restorefile(save_job2_id)
!        setcursor()
!    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
!End !If CheckBouncer#
!Return tmp:count
Local.OutFaultExcluded       Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob)
local:FaultCode     String(30)
Code
    !Loop through outfaults
    Save_joo_ID = Access:JOBOUTFL.SaveFile()
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = local:JobNumber
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
           Break
        End !If
        If joo:JobNumber <> local:JobNumber      |
            Then Break.  ! End If
        !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Lookup the Fault Code lookup to see if it's excluded
            Save_mfo_ID = Access:MANFAULO.SaveFile()
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                   Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                Or mfo:Field_Number <> maf:Field_Number      |
                Or mfo:Field        <> joo:FaultCode      |
                    Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
                    !Make sure the descriptions match in case of duplicates
                    If mfo:ExcludeFromBouncer
                        Return Level:Fatal
                    End !If mfo:ExcludeFromBoucer
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
            Access:MANFAULO.RestoreFile(Save_mfo_ID)

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    End !Loop
    Access:JOBOUTFL.RestoreFile(Save_joo_ID)

    !Is an outfault records on parts for this manufacturer
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        !Loop through the parts as see if any of the faults codes are excluded
        If local:WarrantyJob = 'YES'

            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = local:JobNumber
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> local:JobNumber      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            local:FaultCode        = wpr:Fault_Code1
                        Of 2
                            local:FaultCode        = wpr:Fault_Code2
                        Of 3
                            local:FaultCode        = wpr:Fault_Code3
                        Of 4
                            local:FaultCode        = wpr:Fault_Code4
                        Of 5
                            local:FaultCode        = wpr:Fault_Code5
                        Of 6
                            local:FaultCode        = wpr:Fault_Code6
                        Of 7
                            local:FaultCode        = wpr:Fault_Code7
                        Of 8
                            local:FaultCode        = wpr:Fault_Code8
                        Of 9
                            local:FaultCode        = wpr:Fault_Code9
                        Of 10
                            local:FaultCode        = wpr:Fault_Code10
                        Of 11
                            local:FaultCode        = wpr:Fault_Code11
                        Of 12
                            local:FaultCode        = wpr:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = local:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> local:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ExcludeFromBouncer
                            Return Level:Fatal
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If job:Warranty_Job = 'YES'

        If local:ChargeableJob = 'YES'
            !Loop through the parts as see if any of the faults codes are excluded
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = local:JobNumber
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> local:JobNumber      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            local:FaultCode        = par:Fault_Code1
                        Of 2
                            local:FaultCode        = par:Fault_Code2
                        Of 3
                            local:FaultCode        = par:Fault_Code3
                        Of 4
                            local:FaultCode        = par:Fault_Code4
                        Of 5
                            local:FaultCode        = par:Fault_Code5
                        Of 6
                            local:FaultCode        = par:Fault_Code6
                        Of 7
                            local:FaultCode        = par:Fault_Code7
                        Of 8
                            local:FaultCode        = par:Fault_Code8
                        Of 9
                            local:FaultCode        = par:Fault_Code9
                        Of 10
                            local:FaultCode        = par:Fault_Code10
                        Of 11
                            local:FaultCode        = par:Fault_Code11
                        Of 12
                            local:FaultCode        = par:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = local:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> local:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ExcludeFromBouncer
                            Return Level:Fatal
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'
    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Return Level:Benign
LiveBouncers         PROCEDURE  (f_DateBooked,f_IMEI)      ! Declare Procedure
tmp:DateBooked       DATE                                  !
tmp:count            LONG                                  !
save_job2_id         USHORT,AUTO                           !
tmp:IMEI             STRING(30)                            !
tmp:Manufacturer     STRING(30)                            !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    !Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
    !From that see if any of the bouncer jobs are still live, i.e. not completed.

    tmp:count    = 0
    If f_IMEI <> 'N/A' And f_IMEI <> ''
        Set(defaults)
        Access:Defaults.Next()

        save_job2_id = access:jobs2_alias.savefile()
        access:jobs2_alias.clearkey(job2:esn_key)
        job2:esn = f_imei
        set(job2:esn_key,job2:esn_key)
        loop
            if access:jobs2_alias.next()
               break
            end !if
            if job2:esn <> f_imei      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if

            If job2:Cancelled = 'YES'
                Cycle
            End !If job2:Cancelled = 'YES'


            If job2:cancelled <> 'YES'
                If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
                    If job2:Date_Completed = ''
                        Return Level:Fatal
                        Break
                    End !If job2:Date_Completed = ''

                End!If job2:date_booked + man:warranty_period < Today()
            End!If job2:esn <> job2:ref_number
        end !loop
        access:jobs2_alias.restorefile(save_job2_id)
    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
    Return Level:Benign
PreviousIMEI         PROCEDURE  (f:IMEI)                   ! Declare Procedure
save_jobs_alias_id   USHORT,AUTO                           !
save_exchange_id     USHORT,AUTO                           !
save_jobthird_id     USHORT,AUTO                           !
  CODE
    If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
        Return False
    End ! If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
    Found# = False
    Save_JOBS_ALIAS_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
    job_ali:ESN = f:IMEI
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop ! Begin Loop
        If Access:JOBS_ALIAS.Next()
            Break
        End ! If Access:JOBS_ALIAS.Next()
        If job_ali:ESN <> f:IMEI
            Break
        End ! If job_ali:ESN <> f:IMEI
        Found# = True
        Break
    End ! Loop
    Access:JOBS_ALIAS.RestoreFile(Save_JOBS_ALIAS_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_JOBTHIRD_ID = Access:JOBTHIRD.SaveFile()
    Access:JOBTHIRD.Clearkey(jot:OriginalIMEIKey)
    jot:OriginalIMEI     = f:IMEI
    Set(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
    Loop ! Begin Loop
        If Access:JOBTHIRD.Next()
            Break
        End ! If Access:JOBTHIRD.Next()
        If jot:OriginalIMEI <> f:IMEI
            Break
        End ! If jot:OriginalIMEI <> f:IMEI
        Found# = True
        Break
    End ! Loop
    Access:JOBTHIRD.RestoreFile(Save_JOBTHIRD_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
    Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
    xch:ESN = f:IMEI
    Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
    Loop ! Begin Loop
        If Access:EXCHANGE.Next()
            Break
        End ! If Access:EXCHANGE.Next()
        IF xch:ESN <> f:IMEI
            Break
        End ! IF xch:ESN <> f:IMEI
        If xch:Job_Number <> ''
            Found# = True
            Break
        End ! If xch:Job_Number <> ''
    End ! Loop
    Access:EXCHANGE.RestoreFile(Save_EXCHANGE_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Return False



SetBottom            PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:SetBottom -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('SetBottom')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
    Packet = '<a name="bottom" id="bottom"></a>'
    Do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
