

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE010.INC'),ONCE        !Local module procedure declarations
                     END


WOBEDIStatus         PROCEDURE  (fStatus)                  ! Declare Procedure
  CODE
    Case fStatus
    Of 'NO'
        Return 'PENDING'
    Of 'YES'
        Return 'SUBMITTED'
    Of 'APP'
        Return 'APPROVED'
    Of 'PAY'
        Return 'PAID'
    Of 'EXC'
        Return 'REJECTED'
    Of 'EXM'
        Return 'REJECTED'
    Of 'AAJ'
        Return 'ACCEPTED REJECTION'
    Of 'REJ'
        Return 'FINAL REJECTION'
    End !func:Status
    Return ''
useStockAllocation   PROCEDURE  (fLocation)                ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    retValue# = 0

    Access:LOCATION.Clearkey(loc:location_Key)
    loc:location    = fLocation
    if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Found
        if (loc:useRapidStock)
            retValue# = 1
        end !if (loc:useRapidStock)
    else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)

    do closeFiles

    return retValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
UpdateDateTimeStamp  PROCEDURE  (f:RefNumber)              ! Declare Procedure
JOBSTAMP::State  USHORT
FilesOpened     BYTE(0)
  CODE
    ! Inserting (DBH 05/08/2008) # 10253 - Update date/time stamp everytime a job changes
    do openfiles
    do savefiles
    Access:JOBSTAMP.ClearKey(jos:JOBSRefNumberKey)
    jos:JOBSRefNumber = f:RefNumber
    If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Found
        jos:DateStamp = Today()
        jos:TimeStamp = Clock()
        Access:JOBSTAMP.TryUpdate()
    Else ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Error
        If Access:JOBSTAMP.PrimeRecord() = Level:Benign
            jos:JOBSRefNumber = f:RefNumber
            jos:DateStamp = Today()
            jos:TimeStamp = Clock()
            If Access:JOBSTAMP.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:JOBSTAMP.TryInsert() = Level:Benign
                Access:JOBSTAMP.CancelAutoInc()
            End ! If Access:JOBSTAMP.TryInsert() = Level:Benign
        End ! If Access.JOBSTAMP.PrimeRecord() = Level:Benign
    End ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign

    do restorefiles
    do closefiles
    ! End (DBH 05/08/2008) #10253
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSTAMP.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSTAMP.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSTAMP.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  JOBSTAMP::State = Access:JOBSTAMP.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBSTAMP::State <> 0
    Access:JOBSTAMP.RestoreFile(JOBSTAMP::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
SecurityCheckFailed  PROCEDURE  (f:Password,f:Area)        ! Declare Procedure
USERS::State  USHORT
ACCAREAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 1

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = f:Password
    If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
        acc:User_Level = use:User_Level
        acc:Access_Area = f:Area
        If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
            Return# = 0
        End ! If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
    End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:ACCAREAS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  USERS::State = Access:USERS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  ACCAREAS::State = Access:ACCAREAS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF USERS::State <> 0
    Access:USERS.RestoreFile(USERS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ACCAREAS::State <> 0
    Access:ACCAREAS.RestoreFile(ACCAREAS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
saveJob              PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    Access:WEBJOB.Clearkey(wob:refNumberKey)
    wob:refNumber    = p_web.GSV('wob:ref_Number')
    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(WEBJOB)
        access:WEBJOB.tryUpdate()
    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

    Access:JOBS.Clearkey(job:ref_Number_Key)
    job:ref_Number    = p_web.GSV('wob:RefNumber')
    if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBS)
        access:JOBS.tryupdate()
    else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE)
        access:JOBSE.tryupdate()
    else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)


    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE2)
        access:JOBSE2.tryupdate()
    else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)


    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBNOTES)
        if access:JOBNOTES.tryupdate()
        end
    else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS.Close
     Access:JOBSE.Close
     Access:JOBNOTES.Close
     Access:JOBSE2.Close
     Access:WEBJOB.Close
     FilesOpened = False
  END
