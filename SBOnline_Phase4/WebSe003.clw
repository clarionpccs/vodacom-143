

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE029.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE034.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE061.INC'),ONCE        !Req'd for module callout resolution
                     END


PassExchangeAssessment PROCEDURE  (Long fJobNumber)        ! Declare Procedure
rtnValue             BYTE                                  !
PARTS::State  USHORT
WARPARTS::State  USHORT
MANFAUPA::State  USHORT
MANFAULO::State  USHORT
MANFAULT::State  USHORT
JOBOUTFL::State  USHORT
JOBS::State  USHORT
FilesOpened     BYTE(0)
tmp:FaultCode   STRING(255)
  CODE
    DO openFiles
    DO SaveFiles
    !Loop through outfaults
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = fJobNumber
    IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))

    END

    rtnValue = 0

    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
            Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
                        Then Break.  ! End If
    !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                    Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                                Or mfo:Field_Number <> maf:Field_Number      |
                                Or mfo:Field        <> joo:FaultCode      |
                                Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
    !Make sure the descriptions match in case of duplicates
                    If mfo:ReturnToRRC
                        rtnValue = 0
                        Do ExitProc
                    End !If mfo:ReturnToRRC
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    End !Loop

    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    map:Manufacturer = job:Manufacturer

    map:Manufacturer = job:Manufacturer



    !Is an outfault records on parts for this manufacturer
        Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Found
    !Loop through the parts as see if any of the faults codes are excluded
            If job:Warranty_Job = 'YES'

                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                        Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded

    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = wpr:Fault_Code1
                        Of 2
                            tmp:FaultCode        = wpr:Fault_Code2
                        Of 3
                            tmp:FaultCode        = wpr:Fault_Code3
                        Of 4
                            tmp:FaultCode        = wpr:Fault_Code4
                        Of 5
                            tmp:FaultCode        = wpr:Fault_Code5
                        Of 6
                            tmp:FaultCode        = wpr:Fault_Code6
                        Of 7
                            tmp:FaultCode        = wpr:Fault_Code7
                        Of 8
                            tmp:FaultCode        = wpr:Fault_Code8
                        Of 9
                            tmp:FaultCode        = wpr:Fault_Code9
                        Of 10
                            tmp:FaultCode        = wpr:Fault_Code10
                        Of 11
                            tmp:FaultCode        = wpr:Fault_Code11
                        Of 12
                            tmp:FaultCode        = wpr:Fault_Code12
                        End !Case map:Field_Number

                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop

                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                End !Loop
            End !If job:Warranty_Job = 'YES'

            If job:Chargeable_Job = 'YES'
    !Loop through the parts as see if any of the faults codes are excluded
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                        Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded

    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = par:Fault_Code1
                        Of 2
                            tmp:FaultCode        = par:Fault_Code2
                        Of 3
                            tmp:FaultCode        = par:Fault_Code3
                        Of 4
                            tmp:FaultCode        = par:Fault_Code4
                        Of 5
                            tmp:FaultCode        = par:Fault_Code5
                        Of 6
                            tmp:FaultCode        = par:Fault_Code6
                        Of 7
                            tmp:FaultCode        = par:Fault_Code7
                        Of 8
                            tmp:FaultCode        = par:Fault_Code8
                        Of 9
                            tmp:FaultCode        = par:Fault_Code9
                        Of 10
                            tmp:FaultCode        = par:Fault_Code10
                        Of 11
                            tmp:FaultCode        = par:Fault_Code11
                        Of 12
                            tmp:FaultCode        = par:Fault_Code12
                        End !Case map:Field_Number

                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop

                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                End !Loop
            End !If job:Chargeable_Job = 'YES'
        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

        rtnValue = 1
        Do ExitProc


ExitProc    ROUTINE
    DO RestoreFiles
    DO CloseFiles
    RETURN rtnValue
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:MANFAUPA.Close
     Access:MANFAULO.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:JOBS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  PARTS::State = Access:PARTS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  WARPARTS::State = Access:WARPARTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAUPA::State = Access:MANFAUPA.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULO::State = Access:MANFAULO.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULT::State = Access:MANFAULT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBOUTFL::State = Access:JOBOUTFL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS::State = Access:JOBS.SaveFile()                     ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF PARTS::State <> 0
    Access:PARTS.RestoreFile(PARTS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF WARPARTS::State <> 0
    Access:WARPARTS.RestoreFile(WARPARTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAUPA::State <> 0
    Access:MANFAUPA.RestoreFile(MANFAUPA::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULO::State <> 0
    Access:MANFAULO.RestoreFile(MANFAULO::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULT::State <> 0
    Access:MANFAULT.RestoreFile(MANFAULT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBOUTFL::State <> 0
    Access:JOBOUTFL.RestoreFile(JOBOUTFL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS::State <> 0
    Access:JOBS.RestoreFile(JOBS::State)                   ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
CheckLength          PROCEDURE  (f_type,f_ModelNumber,f_number) ! Declare Procedure
tmp:LengthFrom       LONG                                  !Length From
tmp:LengthTo         LONG                                  !Length To
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MODELNUM::State  USHORT
FilesOpened     BYTE(0)
  CODE
    If f_Type = 'MOBILE'
        tmp:LengthFrom = GETINI('COMPULSORY','MobileLengthFrom',,Clip(Path()) & '\SB2KDEF.INI')
        tmp:LengthTo   = GETINI('COMPULSORY','MobileLengthTo',,Clip(Path()) & '\SB2KDEF.INI')
        If Len(Clip(f_Number)) < tmp:LengthFrom Or |
            Len(Clip(f_Number)) > tmp:LengthTo

            Return Level:Fatal
        End ! Len(Clip(f_Number)) > tmp:LengthTo

    Else ! If f_Type = 'MOBILE'
        !Return The Correct Length Of A Model Number
        Return# = 0
        Do OpenFiles
        Do SaveFiles
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = f_ModelNumber
        if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
            If f_number <> 'N/A'
                Case f_type
                    Of 'IMEI'
                        If Len(Clip(f_number)) < mod:esn_length_from Or Len(Clip(f_number)) > mod:esn_length_to
                            Return# = 1
                        End!If Clip(Len(f_number)) < mod:esn_length_from Or Clip(Len(f_number)) > mod:esn_length_to


                    Of 'MSN'
                        If Len(Clip(f_number)) < mod:msn_length_from Or len(clip(f_number)) > mod:msn_length_to
                            Return# = 1
                        End!If Clip(Len(f_number)) < mod:esn_length_from
                End!Case f_type
            End!If f_number <> 'N/A'

        End!if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
        Do RestoreFiles
        Do CloseFiles
        Return Return#
    End ! If f_Type = 'MOBILE'

    Return Level:Benign
!--------------------------------------
OpenFiles  ROUTINE
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MODELNUM.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
PassMobileNumberFormat PROCEDURE  (String f:MobileNumber)  ! Declare Procedure
tmp:Format           STRING(30),AUTO                       !Format
tmp:StartQuotes      BYTE(0)                               !Start Quotes
  CODE
   If f:MobileNumber = ''
        Return True
    End ! If Clip(f:MobileNumber) = ''

    Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
    Of 0 ! Mobile Number Length
        If CheckLength('MOBILE','',f:MobileNumber)
            Return False
        End ! If CheckLength('MOBILE','',f:MobileNumber)
        Return True
    Of 1 ! Mobile Number Format
    ! End (DBH 22/06/2006) #7597

        tmp:Format  = GETINI('MOBILENUMBER','Format',,Clip(Path()) & '\SB2KDEF.INI')

        If tmp:Format = ''
            Return True
        End ! If Clip(tmp:Format) = ''

        ! Inserting (DBH 22/06/2006) #7597 - Check the length of the mobile before starting the format check
        Len# = 0
        Loop x# = 1 To Len(tmp:Format)
            If Sub(tmp:Format,x#,1) <> '*' And Sub(tmp:Format,x#,1) <> ''
                Len# += 1
            End ! If Sub(tmp:Format,x#,1) <> '*'
        End ! Loop x# = 1 To Len(tmp:Format)
        MobLen# = 0
        Loop x# = 1 To Len(f:MobileNumber)
            If Sub(f:MobileNumber,x#,1) <> ''
                MobLen# += 1
            End ! If Sub(f:MobileNumber,x#,1) <> ''
        End ! Loop x# = 1 To Len(f:MobileNumber)

        If MobLen# <> Len#
            Return False
        End ! If Len(f:MobileNumber) <> Len#
        ! End (DBH 22/06/2006) #7597


        Error# = 0
        y# = 1
        Loop x# = 1 To Len(tmp:Format)
            If tmp:StartQuotes
                If Sub(tmp:Format,x#,1) = '*'
                    tmp:StartQuotes = 0
                    Cycle
                End ! If Sub(tmp:Format,x#,1) = '"'
                If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    Error# = 1
                    Break
                Else ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    y# += 1
                    Cycle
                End ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
            End ! If tmp:StartQuotes
            If Sub(tmp:Format,x#,1) = '0'
                If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
                    Error# = 1
                    Break
                End ! If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
            End ! If Sub(tmp:Format,x#,1) = '0'

            If Sub(tmp:Format,x#,1) = '*'
                tmp:StartQuotes = 1
                Cycle
            End ! If Sub(tmp:Format,x#,1) = '"'

            y# += 1
        End ! Loop x# = 1 To Len(Clip(tmp:Format))

        If Error# = 0
            Return True
        Else ! If Error# = 0
            Return False
        End ! If Error# = 0
    Else
        Return True
    End ! Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
IndexPage            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
text_Site:IsInvalid  Long
text_User:IsInvalid  Long
text_Version:IsInvalid  Long
Button:CreateNewJob:IsInvalid  Long
button:JobSearch:IsInvalid  Long
Button:PrintRoutines:IsInvalid  Long
button:CreateMultipleJobs:IsInvalid  Long
buttonIndivdualDespatch:IsInvalid  Long
buttonMultipleDespatch:IsInvalid  Long
blankString:IsInvalid  Long
btnStockControl:IsInvalid  Long
Button:LogOut:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('IndexPage')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'IndexPage_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('IndexPage','')
    p_web.DivHeader('IndexPage',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('IndexPage') = 0
        p_web.AddPreCall('IndexPage')
        p_web.DivHeader('popup_IndexPage','nt-hidden')
        p_web.DivHeader('IndexPage',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_IndexPage_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_IndexPage_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_IndexPage',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_IndexPage',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndexPage',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_IndexPage',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_IndexPage',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndexPage',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_IndexPage',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('IndexPage')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('IndexPage_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'IndexPage'
    end
    p_web.formsettings.proc = 'IndexPage'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  If p_web.GSV('PH') > 2
    loc:TabNumber += 1
  End
  If p_web.GSV('PH') > 3
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('IndexPage_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferIndexPage')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('IndexPage_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('IndexPage_ChainTo')
    loc:formaction = p_web.GetSessionValue('IndexPage_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
      IF (p_web.GSV('NewPasswordRequired') = 1 OR p_web.GSV('UserMobileRequired') = 1)
          IF (p_web.GSV('NewPasswordRequired') = 1 AND p_web.GSV('UserMobileRequired') = 1)
              p_web.script('alert(''Your Password Has Expired. You must enter a new one.\n\nYou are required to supply your mobile number before you can login.'');')
          ELSIF (p_web.GSV('NewPasswordRequired') = 1 AND p_web.GSV('UserMobileRequired') = 0)
              p_web.script('alert(''Your Password Has Expired. You must enter a new one.'');')
          ELSIF (p_web.GSV('NewPasswordRequired') = 0 AND p_web.GSV('UserMobileRequired') = 1)
              p_web.script('alert(''You are required to supply your mobile number before you can login.'');')
          END
          p_web.Script('window.location.href=''FormNewPassword'';')
          EXIT
      END
      ClearJobVariables(p_web)
  
      ClearUpdateJobVariables(p_web)
  
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Main Menu') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Main Menu',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_IndexPage',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndexPage0_div')&'">'&p_web.Translate('Login Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndexPage1_div')&'">'&p_web.Translate('Rapid Procedures')&'</a></li>'& CRLF
      If p_web.GSV('PH') > 2
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndexPage2_div')&'">'&p_web.Translate('Despatch Procedures')&'</a></li>'& CRLF
      End
      If p_web.GSV('PH') > 3
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndexPage3_div')&'">'&p_web.Translate('Stock Control')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndexPage4_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="IndexPage_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="IndexPage_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'IndexPage_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="IndexPage_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'IndexPage_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_IndexPage')>0,p_web.GSV('showtab_IndexPage'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_IndexPage'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_IndexPage') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_IndexPage'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_IndexPage')>0,p_web.GSV('showtab_IndexPage'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_IndexPage') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Login Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Rapid Procedures') & ''''
      If p_web.GSV('PH') > 2
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Procedures') & ''''
      End
      If p_web.GSV('PH') > 3
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Stock Control') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_IndexPage_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_IndexPage')>0,p_web.GSV('showtab_IndexPage'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"IndexPage",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_IndexPage')>0,p_web.GSV('showtab_IndexPage'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_IndexPage_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('IndexPage') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('IndexPage')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Login Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndexPage0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Login Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Login Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Login Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text_Site
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text_User
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text_User
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text_Version
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Rapid Procedures')&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndexPage1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Rapid Procedures')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Rapid Procedures')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Rapid Procedures')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Button:CreateNewJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:JobSearch
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:JobSearch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::Button:PrintRoutines
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Button:PrintRoutines
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('PH') > 2
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:CreateMultipleJobs
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
  If p_web.GSV('PH') > 2
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Despatch Procedures')&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndexPage2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Despatch Procedures')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Despatch Procedures')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Despatch Procedures')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonIndivdualDespatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonMultipleDespatch
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonMultipleDespatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
      If false
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::blankString
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab3  Routine
  If p_web.GSV('PH') > 3
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Stock Control')&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndexPage3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Stock Control')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Stock Control')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Stock Control')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnStockControl
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab4  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndexPage4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndexPage4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Button:LogOut
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::text_Site  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text_Site  ! copies value to session value if valid.

ValidateValue::text_Site  Routine
    If not (1=0)
    End

Value::text_Site  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('text_Site') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text_Site" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('LoginDetails1'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::text_User  Routine
  packet = clip(packet) & p_web.DivHeader('IndexPage_' & p_web._nocolon('text_User') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text_User  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text_User  ! copies value to session value if valid.

ValidateValue::text_User  Routine
    If not (1=0)
    End

Value::text_User  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('text_User') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text_User" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('LoginDetails2'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::text_Version  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text_Version  ! copies value to session value if valid.

ValidateValue::text_Version  Routine
    If not (1=0)
    End

Value::text_Version  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('text_Version') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text_Version" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('VersionNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::Button:CreateNewJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Button:CreateNewJob  ! copies value to session value if valid.

ValidateValue::Button:CreateNewJob  Routine
    If not (p_web.GSV('Hide:ButtonCreateNewJob') = 1)
    End

Value::Button:CreateNewJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonCreateNewJob') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('Button:CreateNewJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonCreateNewJob') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Create Single Job','Create Single Job',p_web.combine(Choose('Create Single Job' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('PreNewJobBooking&' & p_web._jsok('MultipleJobBooking=0') &''&'','_self'),,loc:disabled,,,,,'Book a new job',0,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:JobSearch  Routine
  packet = clip(packet) & p_web.DivHeader('IndexPage_' & p_web._nocolon('button:JobSearch') & '_prompt',Choose(p_web.GSV('Hide:ButtonJobSearch') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ButtonJobSearch') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:JobSearch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:JobSearch  ! copies value to session value if valid.

ValidateValue::button:JobSearch  Routine
    If not (p_web.GSV('Hide:ButtonJobSearch') = 1)
    End

Value::button:JobSearch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonJobSearch') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('button:JobSearch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonJobSearch') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','JobSearch','Job Search',p_web.combine(Choose('Job Search' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('JobSearch'&'','_self'),,loc:disabled,,,,,'Search for a job',,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::Button:PrintRoutines  Routine
  packet = clip(packet) & p_web.DivHeader('IndexPage_' & p_web._nocolon('Button:PrintRoutines') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::Button:PrintRoutines  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Button:PrintRoutines  ! copies value to session value if valid.

ValidateValue::Button:PrintRoutines  Routine
    If not (1=0)
    End

Value::Button:PrintRoutines  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('Button:PrintRoutines') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintRoutines','Print Routines',p_web.combine(Choose('Print Routines' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('PrintRoutines'&'','_self'),,loc:disabled,,,,,'Print paperwork',,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::button:CreateMultipleJobs  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:CreateMultipleJobs  ! copies value to session value if valid.

ValidateValue::button:CreateMultipleJobs  Routine
  If p_web.GSV('PH') > 2
    If not (p_web.GSV('Hide:ButtonCreateNewJob') = 1)
    End
  End

Value::button:CreateMultipleJobs  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonCreateNewJob') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('button:CreateMultipleJobs') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonCreateNewJob') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateMultipleJobs','Create Multiple Jobs',p_web.combine(Choose('Create Multiple Jobs' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('PreNewJobBooking&' & p_web._jsok('MultipleJobBooking=1') &''&'','_self'),,loc:disabled,,,,,'Book multiple jobs',,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::buttonIndivdualDespatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonIndivdualDespatch  ! copies value to session value if valid.

ValidateValue::buttonIndivdualDespatch  Routine
    If not (1=0)
    End

Value::buttonIndivdualDespatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('buttonIndivdualDespatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','IndividualDespatch','Individual Despatch',p_web.combine(Choose('Individual Despatch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('IndividualDespatch'&'','_self'),,loc:disabled,,,,,'Despatch single jobs',,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::buttonMultipleDespatch  Routine
  packet = clip(packet) & p_web.DivHeader('IndexPage_' & p_web._nocolon('buttonMultipleDespatch') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonMultipleDespatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonMultipleDespatch  ! copies value to session value if valid.

ValidateValue::buttonMultipleDespatch  Routine
    If not (1=0)
    End

Value::buttonMultipleDespatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('buttonMultipleDespatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','MultipleDespatch','Multiple Despatch',p_web.combine(Choose('Multiple Despatch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('MultipleBatchDespatch'&'','_self'),,loc:disabled,,,,,'Despatch batches of jobs',,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::blankString  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::blankString  ! copies value to session value if valid.

ValidateValue::blankString  Routine
  If false
    If not (1=0)
    End
  End

Value::blankString  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('blankString') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="blankString" class="'&clip('DoubleButton hidden')&'"'&clip(loc:extra)&'>' & p_web.Translate(' ',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::btnStockControl  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnStockControl  ! copies value to session value if valid.

ValidateValue::btnStockControl  Routine
    If not (1=0)
    End

Value::btnStockControl  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('btnStockControl') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnStockControl','Stock Control',p_web.combine(Choose('Stock Control' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('FormBrowseStock'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::Button:LogOut  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Button:LogOut  ! copies value to session value if valid.

ValidateValue::Button:LogOut  Routine
    If not (1=0)
    End

Value::Button:LogOut  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndexPage_' & p_web._nocolon('Button:LogOut') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Logout','Log Out',p_web.combine(Choose('Log Out' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('LoginForm')&''&'','_self'),,loc:disabled,'images/pcancel.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('IndexPage_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('IndexPage_nexttab_' & 1)
    If loc:Invalid then exit.
  of lower('IndexPage_nexttab_' & 2)
    If loc:Invalid then exit.
  of lower('IndexPage_nexttab_' & 3)
    If loc:Invalid then exit.
  of lower('IndexPage_nexttab_' & 4)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_IndexPage_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('IndexPage_tab_' & 0)
    do GenerateTab0
  of lower('IndexPage_tab_' & 1)
    do GenerateTab1
  of lower('IndexPage_tab_' & 2)
    do GenerateTab2
  of lower('IndexPage_tab_' & 3)
    do GenerateTab3
  of lower('IndexPage_tab_' & 4)
    do GenerateTab4
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('IndexPage_form:ready_',1)

  p_web.SetSessionValue('IndexPage_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_IndexPage',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_IndexPage',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('IndexPage:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('IndexPage:Primed',0)
  p_web.setsessionvalue('showtab_IndexPage',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('PH') > 2
  End
  If p_web.GSV('PH') > 3
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('IndexPage_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('IndexPage_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 5
    loc:InvalidTab += 1
    do ValidateValue::text_Site
    If loc:Invalid then exit.
    do ValidateValue::text_User
    If loc:Invalid then exit.
    do ValidateValue::text_Version
    If loc:Invalid then exit.
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::Button:CreateNewJob
    If loc:Invalid then exit.
    do ValidateValue::button:JobSearch
    If loc:Invalid then exit.
    do ValidateValue::Button:PrintRoutines
    If loc:Invalid then exit.
    do ValidateValue::button:CreateMultipleJobs
    If loc:Invalid then exit.
  ! tab = 6
  If p_web.GSV('PH') > 2
    loc:InvalidTab += 1
    do ValidateValue::buttonIndivdualDespatch
    If loc:Invalid then exit.
    do ValidateValue::buttonMultipleDespatch
    If loc:Invalid then exit.
    do ValidateValue::blankString
    If loc:Invalid then exit.
  End
  ! tab = 7
  If p_web.GSV('PH') > 3
    loc:InvalidTab += 1
    do ValidateValue::btnStockControl
    If loc:Invalid then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::Button:LogOut
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('IndexPage:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

SelectModelNumbers   PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
mod:Model_Number:IsInvalid  Long
mod:Manufacturer:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MODELNUM)
                      Project(mod:Model_Number)
                      Project(mod:Model_Number)
                      Project(mod:Manufacturer)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('SelectModelNumbers')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectModelNumbers:NoForm')
      loc:NoForm = p_web.GetValue('SelectModelNumbers:NoForm')
      loc:FormName = p_web.GetValue('SelectModelNumbers:FormName')
    else
      loc:FormName = 'SelectModelNumbers_frm'
    End
    p_web.SSV('SelectModelNumbers:NoForm',loc:NoForm)
    p_web.SSV('SelectModelNumbers:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectModelNumbers:NoForm')
    loc:FormName = p_web.GSV('SelectModelNumbers:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectModelNumbers') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectModelNumbers')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'SelectModelNumbers' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','SelectModelNumbers')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('SelectModelNumbers') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('SelectModelNumbers')
      p_web.DivHeader('popup_SelectModelNumbers','nt-hidden')
      p_web.DivHeader('SelectModelNumbers',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_SelectModelNumbers_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_SelectModelNumbers_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('SelectModelNumbers',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MODELNUM,mod:Model_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MOD:MODEL_NUMBER') then p_web.SetValue('SelectModelNumbers_sort','1')
    ElsIf (loc:vorder = 'MOD:MANUFACTURER') then p_web.SetValue('SelectModelNumbers_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectModelNumbers:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectModelNumbers:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectModelNumbers:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectModelNumbers:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'SelectModelNumbers'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('SelectModelNumbers')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectModelNumbers_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('SelectModelNumbers_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mod:Model_Number)','-UPPER(mod:Model_Number)')
    Loc:LocateField = 'mod:Model_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mod:Manufacturer)','-UPPER(mod:Manufacturer)')
    Loc:LocateField = 'mod:Manufacturer'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mod:Manufacturer),+UPPER(mod:Model_Number)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mod:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('SelectModelNumbers_LocatorPic','@s30')
  Of upper('mod:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('SelectModelNumbers_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('SelectModelNumbers:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectModelNumbers:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectModelNumbers:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectModelNumbers:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MODELNUM"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mod:Model_Number_Key"></input><13,10>'
  end
  If p_web.Translate('Select Model Number') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Select Model Number',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{SelectModelNumbers.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectModelNumbers',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2SelectModelNumbers','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('SelectModelNumbers_LocatorPic'),,,'onchange="SelectModelNumbers.locate(''Locator2SelectModelNumbers'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2SelectModelNumbers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="SelectModelNumbers.locate(''Locator2SelectModelNumbers'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="SelectModelNumbers_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectModelNumbers.cl(''SelectModelNumbers'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SelectModelNumbers_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('SelectModelNumbers_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="SelectModelNumbers_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('SelectModelNumbers_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="SelectModelNumbers_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','SelectModelNumbers',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','SelectModelNumbers',p_web.Translate('Model Number'),'Click here to sort by Model Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','SelectModelNumbers',p_web.Translate('Manufacturer'),'Click here to sort by Manufacturer',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('mod:model_number',lower(loc:vorder),1,1) = 0 !and MODELNUM{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','mod:Model_Number',clip(loc:vorder) & ',' & 'mod:Model_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mod:Model_Number'),p_web.GetValue('mod:Model_Number'),p_web.GetSessionValue('mod:Model_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = p_web.GSV('filter:ModelNumber') & ' AND UPPER(mod:Active) = ''Y'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectModelNumbers',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectModelNumbers_Filter')
    p_web.SetSessionValue('SelectModelNumbers_FirstValue','')
    p_web.SetSessionValue('SelectModelNumbers_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MODELNUM,mod:Model_Number_Key,loc:PageRows,'SelectModelNumbers',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MODELNUM{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MODELNUM,loc:firstvalue)
              Reset(ThisView,MODELNUM)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MODELNUM{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MODELNUM,loc:lastvalue)
            Reset(ThisView,MODELNUM)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mod:Model_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="SelectModelNumbers_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectModelNumbers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectModelNumbers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectModelNumbers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectModelNumbers.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'SelectModelNumbers_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'SelectModelNumbers',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectModelNumbers',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectModelNumbers_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectModelNumbers_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1SelectModelNumbers','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('SelectModelNumbers_LocatorPic'),,,'onchange="SelectModelNumbers.locate(''Locator1SelectModelNumbers'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1SelectModelNumbers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="SelectModelNumbers.locate(''Locator1SelectModelNumbers'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="SelectModelNumbers_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectModelNumbers.cl(''SelectModelNumbers'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'SelectModelNumbers_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('SelectModelNumbers_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectModelNumbers_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="SelectModelNumbers_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectModelNumbers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectModelNumbers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectModelNumbers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectModelNumbers.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'SelectModelNumbers_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'SelectModelNumbers',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('SelectModelNumbers','MODELNUM',mod:Model_Number_Key) !mod:Model_Number
    p_web._thisrow = p_web._nocolon('mod:Model_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and mod:Model_Number = p_web.GetValue('mod:Model_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectModelNumbers:LookupField')) = mod:Model_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((mod:Model_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('SelectModelNumbers','MODELNUM',mod:Model_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MODELNUM{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MODELNUM)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MODELNUM{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MODELNUM)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('LeftJustify')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::mod:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('LeftJustify')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::mod:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('SelectModelNumbers','MODELNUM',mod:Model_Number_Key)
  TableQueue.Id[1] = mod:Model_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiSelectModelNumbers;if (btiSelectModelNumbers != 1){{var SelectModelNumbers=new browseTable(''SelectModelNumbers'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('mod:Model_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'SelectModelNumbers.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'SelectModelNumbers.applyGreenBar();btiSelectModelNumbers=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectModelNumbers')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectModelNumbers')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectModelNumbers')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectModelNumbers')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MODELNUM)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MODELNUM)
  Bind(mod:Record)
  Clear(mod:Record)
  NetWebSetSessionPics(p_web,MODELNUM)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('mod:Model_Number',p_web.GetValue('mod:Model_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  mod:Model_Number = p_web.GSV('mod:Model_Number')
  loc:result = p_web._GetFile(MODELNUM,mod:Model_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mod:Model_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MODELNUM)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MODELNUM)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('SelectModelNumbers_Select_'&mod:Model_Number,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectModelNumbers',p_web.AddBrowseValue('SelectModelNumbers','MODELNUM',mod:Model_Number_Key),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mod:Model_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('SelectModelNumbers_mod:Model_Number_'&mod:Model_Number,'LeftJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mod:Model_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mod:Manufacturer   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('SelectModelNumbers_mod:Manufacturer_'&mod:Model_Number,'LeftJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mod:Manufacturer,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mod:Model_Number_Key)
    loc:Invalid = 'mod:Model_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> '&clip('Model Number')&' = ' & clip(mod:Model_Number)
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('mod:Model_Number',mod:Model_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
