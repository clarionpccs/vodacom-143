

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE051.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE045.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE050.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE056.INC'),ONCE        !Req'd for module callout resolution
                     END


DespatchConfirmation PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyName       STRING(30)                            !
locCourierCost       REAL                                  !
locLabourCost        REAL                                  !
locPartsCost         REAL                                  !
locSubTotal          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locInvoiceNumber     REAL                                  !
locInvoiceDate       DATE                                  !
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locCourier           STRING(30)                            !
FilesOpened     Long
EXCHHIST::State  USHORT
LOANHIST::State  USHORT
EXCHANGE::State  USHORT
LOAN::State  USHORT
JOBSCONS::State  USHORT
COURIER::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
job:Account_Number:IsInvalid  Long
locCompanyName:IsInvalid  Long
job:Charge_Type:IsInvalid  Long
job:Repair_Type:IsInvalid  Long
aLine:IsInvalid  Long
textBillingDetails:IsInvalid  Long
textInvoiceDetails:IsInvalid  Long
locCourierCost:IsInvalid  Long
locLabourCost:IsInvalid  Long
locInvoiceNumber:IsInvalid  Long
locPartsCost:IsInvalid  Long
locSubTotal:IsInvalid  Long
locInvoiceDate:IsInvalid  Long
locVAT:IsInvalid  Long
locTotal:IsInvalid  Long
job:Courier:IsInvalid  Long
buttonCreateInvoice:IsInvalid  Long
buttonPaymentDetails:IsInvalid  Long
buttonPrintWaybill:IsInvalid  Long
buttonPrintDespatchNote:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
job:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DespatchConfirmation')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'DespatchConfirmation_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DespatchConfirmation','')
    p_web.DivHeader('DespatchConfirmation',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('DespatchConfirmation') = 0
        p_web.AddPreCall('DespatchConfirmation')
        p_web.DivHeader('popup_DespatchConfirmation','nt-hidden')
        p_web.DivHeader('DespatchConfirmation',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_DespatchConfirmation_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_DespatchConfirmation_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DespatchConfirmation',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('DespatchConfirmation')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locCompanyName')
    p_web.DeleteSessionValue('locCourierCost')
    p_web.DeleteSessionValue('locLabourCost')
    p_web.DeleteSessionValue('locPartsCost')
    p_web.DeleteSessionValue('locSubTotal')
    p_web.DeleteSessionValue('locVAT')
    p_web.DeleteSessionValue('locTotal')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locInvoiceDate')
    p_web.DeleteSessionValue('locLabourVatRate')
    p_web.DeleteSessionValue('locPartsVatRate')
    p_web.DeleteSessionValue('locCourier')

    ! Other Variables
    p_web.DeleteSessionValue('DespatchConfirmation:FirstTime')
DespatchProcess     Routine
DATA
locWaybillNumber    STRING(30)
CODE
    p_web.SSV('Hide:PrintDespatchNote',1)
    p_web.SSV('Hide:PrintWaybill',1)

    IF (p_web.GSV('cou:PrintWaybill') = 1)

        p_web.SSV('Hide:PrintWaybill',0)

        locWaybillNumber = NextWaybillNumber()
        IF (locWaybillNumber = 0)
            EXIT
        END

        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locWaybillNumber
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
            EXIT
        END

        IF (p_web.GSV('BookingSite') = 'RRC')

            way:AccountNumber = p_web.GSV('BookingAccount')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                way:WaybillID = 2
                p_web.SSV('wob:JobWaybillNumber',locWaybillNumber)
            OF 'EXC'
                way:WaybillID = 3
                p_web.SSV('wob:ExcWaybillNumber',way:WayBillNumber)
            OF 'LOA'
                way:WaybillID = 4
                p_web.SSV('wob:LoaWaybillNumber',way:WayBillNumber)
            END

            IF (p_web.GSV('job:Who_Booked') = 'WEB')
                ! PUP Job
                way:WayBillType = 9
                way:WaybillID = 21
            ELSE
                way:WayBillType = 2
            END

            way:FromAccount = p_web.GSV('BookingAccount')
            way:ToAccount = p_web.GSV('job:Account_Number')

        ELSE
            way:WayBillType = 1 ! Created From RRC
            way:AccountNumber = p_web.GSV('wob:HeadAccountNumber')
            way:FromAccount = p_web.GSV('ARC:AccountNumber')
            IF (p_web.GSV('jobe:WebJob') = 1)
                way:ToAccount = p_web.GSV('wob:HeadAccountNumber')
                Case p_web.GSV('DespatchType')
                of 'JOB'
                    way:WaybillID = 5 ! ARC to RRC (JOB)
                of 'EXC'
                    way:WaybillID = 6 ! ARC to RRC (EXC)
                END

            ELSE
                way:ToAccount = job:Account_Number
                Case p_web.GSV('DespatchType')
                OF 'JOB'
                    way:WaybillID = 7 ! ARC To Customer (JOB)
                of 'EXC'
                    way:WaybillID = 8 ! ARC To Customer (EXC)
                of 'LOA'
                    way:WaybillID = 9 ! ARC To Customer (LOA)
                END

            END

        END


        IF (Access:WAYBILLS.TryUpdate())
            EXIT
        END

        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
            waj:WayBillNumber = way:WayBillNumber
            waj:JobNumber = p_web.GSV('job:Ref_Number')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                waj:IMEINumber = p_web.GSV('job:ESN')
            OF 'EXC'
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Available_Key) = Level:Benign)
                    waj:IMEINumber = xch:ESN
                END

            OF 'LOA'
                Access:LOAN.ClearKey(loa:Ref_Number_Key)
                loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                    waj:IMEINumber = loa:ESN
                END

            END
            waj:OrderNumber = p_web.GSV('job:Order_Number')
            waj:SecurityPackNumber = p_web.GSV('locSecurityPackID')
            waj:JobType = p_web.GSV('DespatchType')
            IF (Access:WAYBILLJ.TryUpdate() = Level:Benign)
            ELSE
                Access:WAYBILLJ.CancelAutoInc()
            END
        END

    ELSE ! IF (p_web.GSV('cou:PrintWaybill') = 1)
        if (p_web.GSV('cou:AutoConsignmentNo') = 1)
            Access:COURIER.Clearkey(cou:Courier_Key)
            cou:Courier = p_web.GSV('cou:Courier')
            if (Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign)
                cou:LastConsignmentNo += 1
                Access:COURIER.TryUpdate()
                locWaybillNumber = cou:LastConsignmentNo
            end
        ELSE
            locWaybillNumber = p_web.GSV('locConsignmentNumber')
        end
        p_web.SSV('Hide:PrintDespatchNote',0)

    END ! IF (p_web.GSV('cou:PrintWaybill') = 1)

    p_web.SSV('locWaybillNumber',locWaybillNumber)

    CASE p_web.GSV('DespatchType')
    OF 'JOB'
        !Despatch:Job(locWayBillNumber)
        Despatch:Job(p_web)

    OF 'EXC'
        Despatch:Exc(p_web)

    OF 'LOA'
        Despatch:Loa(p_web)

    END

    ! Save Files
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        IF (Access:JOBS.TryUpdate() = Level:Benign)

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(WEBJOB)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)

                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        IF (Access:JOBSE.TryUpdate() = Level:Benign)

                        END
                    END
                END
            END
        END
    END




ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString    CSTRING(30)
CODE

    paymentFail# = 0

    IF (p_web.GSV('BookingSite') = 'RRC')
        locPaymentString = locPaymentStringRRC
    ELSE
        locPaymentString = locPaymentStringARC
    END

    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
        IF (p_web.GSV('job:Loan_Unit_Number') = 0)
            IF (p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Chargeable_Job') <> 'YES')
                paymentFail# = 1
            END
        END
    ELSE
        paymentFail# = 1
    END
    IF (paymentFail# = 1)
        p_web.SSV('Hide:PaymentDetails',1)
    ELSE
        p_web.SSV('Hide:PaymentDetails',0)
    END

OpenFiles  ROUTINE
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBSCONS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(VATCODE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('DespatchConfirmation_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'DespatchConfirmation'
    end
    p_web.formsettings.proc = 'DespatchConfirmation'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO deletesessionvalues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('locCourierCost')
    p_web.SetPicture('locCourierCost','@n14.2')
  End
  p_web.SetSessionPicture('locCourierCost','@n14.2')
  If p_web.IfExistsValue('locLabourCost')
    p_web.SetPicture('locLabourCost','@n14.2')
  End
  p_web.SetSessionPicture('locLabourCost','@n14.2')
  If p_web.IfExistsValue('locPartsCost')
    p_web.SetPicture('locPartsCost','@n14.2')
  End
  p_web.SetSessionPicture('locPartsCost','@n14.2')
  If p_web.IfExistsValue('locSubTotal')
    p_web.SetPicture('locSubTotal','@n14.2')
  End
  p_web.SetSessionPicture('locSubTotal','@n14.2')
  If p_web.IfExistsValue('locInvoiceDate')
    p_web.SetPicture('locInvoiceDate','@d06')
  End
  p_web.SetSessionPicture('locInvoiceDate','@d06')
  If p_web.IfExistsValue('locVAT')
    p_web.SetPicture('locVAT','@n14.2')
  End
  p_web.SetSessionPicture('locVAT','@n14.2')
  If p_web.IfExistsValue('locTotal')
    p_web.SetPicture('locTotal','@n14.2')
  End
  p_web.SetSessionPicture('locTotal','@n14.2')

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:Account_Number') = 0
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  Else
    job:Account_Number = p_web.GetSessionValue('job:Account_Number')
  End
  if p_web.IfExistsValue('locCompanyName') = 0
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  Else
    locCompanyName = p_web.GetSessionValue('locCompanyName')
  End
  if p_web.IfExistsValue('job:Charge_Type') = 0
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type') = 0
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  Else
    job:Repair_Type = p_web.GetSessionValue('job:Repair_Type')
  End
  if p_web.IfExistsValue('locCourierCost') = 0
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  Else
    locCourierCost = p_web.GetSessionValue('locCourierCost')
  End
  if p_web.IfExistsValue('locLabourCost') = 0
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  Else
    locLabourCost = p_web.GetSessionValue('locLabourCost')
  End
  if p_web.IfExistsValue('locInvoiceNumber') = 0
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End
  if p_web.IfExistsValue('locPartsCost') = 0
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  Else
    locPartsCost = p_web.GetSessionValue('locPartsCost')
  End
  if p_web.IfExistsValue('locSubTotal') = 0
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  Else
    locSubTotal = p_web.GetSessionValue('locSubTotal')
  End
  if p_web.IfExistsValue('locInvoiceDate') = 0
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  Else
    locInvoiceDate = p_web.GetSessionValue('locInvoiceDate')
  End
  if p_web.IfExistsValue('locVAT') = 0
    p_web.SetSessionValue('locVAT',locVAT)
  Else
    locVAT = p_web.GetSessionValue('locVAT')
  End
  if p_web.IfExistsValue('locTotal') = 0
    p_web.SetSessionValue('locTotal',locTotal)
  Else
    locTotal = p_web.GetSessionValue('locTotal')
  End
  if p_web.IfExistsValue('job:Courier') = 0
    p_web.SetSessionValue('job:Courier',job:Courier)
  Else
    job:Courier = p_web.GetSessionValue('job:Courier')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  Else
    job:Account_Number = p_web.GetSessionValue('job:Account_Number')
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  Else
    locCompanyName = p_web.GetSessionValue('locCompanyName')
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  Else
    job:Repair_Type = p_web.GetSessionValue('job:Repair_Type')
  End
  if p_web.IfExistsValue('locCourierCost')
    locCourierCost = p_web.dformat(clip(p_web.GetValue('locCourierCost')),'@n14.2')
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  Else
    locCourierCost = p_web.GetSessionValue('locCourierCost')
  End
  if p_web.IfExistsValue('locLabourCost')
    locLabourCost = p_web.dformat(clip(p_web.GetValue('locLabourCost')),'@n14.2')
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  Else
    locLabourCost = p_web.GetSessionValue('locLabourCost')
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End
  if p_web.IfExistsValue('locPartsCost')
    locPartsCost = p_web.dformat(clip(p_web.GetValue('locPartsCost')),'@n14.2')
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  Else
    locPartsCost = p_web.GetSessionValue('locPartsCost')
  End
  if p_web.IfExistsValue('locSubTotal')
    locSubTotal = p_web.dformat(clip(p_web.GetValue('locSubTotal')),'@n14.2')
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  Else
    locSubTotal = p_web.GetSessionValue('locSubTotal')
  End
  if p_web.IfExistsValue('locInvoiceDate')
    locInvoiceDate = p_web.dformat(clip(p_web.GetValue('locInvoiceDate')),'@d06')
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  Else
    locInvoiceDate = p_web.GetSessionValue('locInvoiceDate')
  End
  if p_web.IfExistsValue('locVAT')
    locVAT = p_web.dformat(clip(p_web.GetValue('locVAT')),'@n14.2')
    p_web.SetSessionValue('locVAT',locVAT)
  Else
    locVAT = p_web.GetSessionValue('locVAT')
  End
  if p_web.IfExistsValue('locTotal')
    locTotal = p_web.dformat(clip(p_web.GetValue('locTotal')),'@n14.2')
    p_web.SetSessionValue('locTotal',locTotal)
  Else
    locTotal = p_web.GetSessionValue('locTotal')
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  Else
    job:Courier = p_web.GetSessionValue('job:Courier')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('DespatchConfirmation_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndividualDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DespatchConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DespatchConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('DespatchConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndividualDespatch'

GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.site.SaveButton.TextValue = 'OK'
  
  IF (p_web.GSV('DespatchConfirmation:FirstTime') = 0)
      p_web.SSV('DespatchConfirmation:FirstTime',1)
      ! Call this code once
      IF (p_web.GSV('Show:DespatchInvoiceDetails') = 1)
  
          IF (p_web.GSV('CreateDespatchInvoice') = 1 )
              CreateTheInvoice(p_web)
          END
      END
      DO DespatchProcess
  
  
  END
  ! Hideous Calculations to work out invoice/costs
  p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DespatchConfirmation')
  p_web.SSV('URL:CreateInvoiceTarget','_self')
  p_web.SSV('URL:CreateInvoiceText','Create Invoice')
  
  isJobInvoiced(p_web)
  IF (p_web.GSV('IsJobInvoiced') = 1)
      p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
      p_web.SSV('URL:CreateInvoiceTarget','_blank')
      p_web.SSV('URL:CreateInvoiceText','Print Invoice')
  
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCLabourCost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCPartsCost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('BookingAccount')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:RRCInvoiceDate)
      else
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Invoice_Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
  
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('Default:AccountNumber')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:ARCInvoiceDate)
      end
  
  ELSE
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
  
  
      else
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Parts_Cost'))
  
      end
      IF (InvoiceSubAccounts(p_web.GSV('job:Account_Number')))
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
      ELSE
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
  
  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
  
      END
  
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCLabourCost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCPartsCost') * locPartsVatRate/100))
      ELSE
  
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Labour_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Parts_Cost') * locPartsVatRate/100))
      END
  
  
  END
  p_web.SSV('locSubTotal',p_web.GSV('locCourierCost') + |
      p_web.GSV('locLabourCost') + |
      p_web.GSV('locPartsCost'))
  p_web.SSV('locTotal',p_web.GSV('locSubTotal') + |
      p_web.GSV('locVAT'))
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
  END
  p_web.SSV('locCompanyName',sub:Company_Name)
  
  
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PAYMENT TYPES'))
      p_web.SSV('Hide:PaymentDetails',1)
  ELSE
      p_web.SSV('Hide:PaymentDetails',0)
  END
  
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locCourierCost = p_web.RestoreValue('locCourierCost')
 locLabourCost = p_web.RestoreValue('locLabourCost')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locPartsCost = p_web.RestoreValue('locPartsCost')
 locSubTotal = p_web.RestoreValue('locSubTotal')
 locInvoiceDate = p_web.RestoreValue('locInvoiceDate')
 locVAT = p_web.RestoreValue('locVAT')
 locTotal = p_web.RestoreValue('locTotal')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Despatch Confirmation') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Despatch Confirmation',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_DespatchConfirmation',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('Show:DespatchInvoiceDetails') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DespatchConfirmation0_div')&'">'&p_web.Translate('Invoice Details')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DespatchConfirmation1_div')&'">'&p_web.Translate('Despatch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="DespatchConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="DespatchConfirmation_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DespatchConfirmation_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="DespatchConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DespatchConfirmation_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Show:DespatchInvoiceDetails') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Courier')
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_DespatchConfirmation'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DespatchConfirmation') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_DespatchConfirmation'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DespatchConfirmation') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('Show:DespatchInvoiceDetails') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Invoice Details') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_DespatchConfirmation_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"DespatchConfirmation",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_DespatchConfirmation_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('DespatchConfirmation') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('DespatchConfirmation')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Invoice Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Invoice Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Invoice Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Invoice Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Account_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Account_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Account_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCompanyName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCompanyName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCompanyName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Repair_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Repair_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Repair_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::aLine
        do Comment::aLine
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::textBillingDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textBillingDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textBillingDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::textInvoiceDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textInvoiceDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textInvoiceDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCourierCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCourierCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCourierCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locLabourCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locLabourCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locLabourCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPartsCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPartsCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPartsCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSubTotal
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSubTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSubTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceDate
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locInvoiceDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locVAT
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locVAT
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locVAT
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locTotal
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Courier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonCreateInvoice
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCreateInvoice
        do Comment::buttonCreateInvoice
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonPaymentDetails
        do Comment::buttonPaymentDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Despatch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Despatch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Despatch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Despatch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::job:Account_Number  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Account_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Account_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Account_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Account_Number  ! copies value to session value if valid.
  do Comment::job:Account_Number ! allows comment style to be updated.

ValidateValue::job:Account_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Account_Number',job:Account_Number).
    End

Value::job:Account_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Account_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Account_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCompanyName  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCompanyName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCompanyName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCompanyName = p_web.GetValue('Value')
  End
  do ValidateValue::locCompanyName  ! copies value to session value if valid.
  do Comment::locCompanyName ! allows comment style to be updated.

ValidateValue::locCompanyName  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locCompanyName',locCompanyName).
    End

Value::locCompanyName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locCompanyName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyName'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCompanyName  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCompanyName:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Charge_Type  ! copies value to session value if valid.
  do Comment::job:Charge_Type ! allows comment style to be updated.

ValidateValue::job:Charge_Type  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Charge_Type',job:Charge_Type).
    End

Value::job:Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Repair_Type  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Repair_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Repair_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Repair_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Repair_Type  ! copies value to session value if valid.
  do Comment::job:Repair_Type ! allows comment style to be updated.

ValidateValue::job:Repair_Type  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Repair_Type',job:Repair_Type).
    End

Value::job:Repair_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Repair_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Repair_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::aLine  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::aLine  ! copies value to session value if valid.
  do Comment::aLine ! allows comment style to be updated.

ValidateValue::aLine  Routine
    If not (1=0)
    End

Value::aLine  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::aLine  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if aLine:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::textBillingDetails  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Billing Details:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::textBillingDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textBillingDetails  ! copies value to session value if valid.
  do Comment::textBillingDetails ! allows comment style to be updated.

ValidateValue::textBillingDetails  Routine
    If not (1=0)
    End

Value::textBillingDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textBillingDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textBillingDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::textInvoiceDetails  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Invoice Details:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::textInvoiceDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textInvoiceDetails  ! copies value to session value if valid.
  do Comment::textInvoiceDetails ! allows comment style to be updated.

ValidateValue::textInvoiceDetails  Routine
    If not (1=0)
    End

Value::textInvoiceDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textInvoiceDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textInvoiceDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCourierCost  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCourierCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCourierCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locCourierCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locCourierCost  ! copies value to session value if valid.
  do Comment::locCourierCost ! allows comment style to be updated.

ValidateValue::locCourierCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locCourierCost',locCourierCost).
    End

Value::locCourierCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locCourierCost = p_web.RestoreValue('locCourierCost')
    do ValidateValue::locCourierCost
    If locCourierCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locCourierCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCourierCost',p_web.GetSessionValue('locCourierCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCourierCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCourierCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locLabourCost  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Labour Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locLabourCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locLabourCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locLabourCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locLabourCost  ! copies value to session value if valid.
  do Comment::locLabourCost ! allows comment style to be updated.

ValidateValue::locLabourCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locLabourCost',locLabourCost).
    End

Value::locLabourCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locLabourCost = p_web.RestoreValue('locLabourCost')
    do ValidateValue::locLabourCost
    If locLabourCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locLabourCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locLabourCost',p_web.GetSessionValue('locLabourCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locLabourCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locLabourCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('IsJobInvoiced') <> 1,'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locInvoiceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locInvoiceNumber  ! copies value to session value if valid.
  do Comment::locInvoiceNumber ! allows comment style to be updated.

ValidateValue::locInvoiceNumber  Routine
    If not (p_web.GSV('IsJobInvoiced') <> 1)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber).
    End

Value::locInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locInvoiceNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locPartsCost  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Parts Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPartsCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPartsCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locPartsCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locPartsCost  ! copies value to session value if valid.
  do Comment::locPartsCost ! allows comment style to be updated.

ValidateValue::locPartsCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPartsCost',locPartsCost).
    End

Value::locPartsCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locPartsCost = p_web.RestoreValue('locPartsCost')
    do ValidateValue::locPartsCost
    If locPartsCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locPartsCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartsCost',p_web.GetSessionValue('locPartsCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPartsCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPartsCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSubTotal  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Sub Total'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSubTotal  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSubTotal = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locSubTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locSubTotal  ! copies value to session value if valid.
  do Comment::locSubTotal ! allows comment style to be updated.

ValidateValue::locSubTotal  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locSubTotal',locSubTotal).
    End

Value::locSubTotal  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locSubTotal = p_web.RestoreValue('locSubTotal')
    do ValidateValue::locSubTotal
    If locSubTotal:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locSubTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSubTotal',p_web.GetSessionValue('locSubTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSubTotal  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSubTotal:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locInvoiceDate  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('IsJobInvoiced') <> 1,'',p_web.Translate('Invoice Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceDate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceDate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06'  !FieldPicture = 
    locInvoiceDate = p_web.Dformat(p_web.GetValue('Value'),'@d06')
  End
  do ValidateValue::locInvoiceDate  ! copies value to session value if valid.
  do Comment::locInvoiceDate ! allows comment style to be updated.

ValidateValue::locInvoiceDate  Routine
    If not (p_web.GSV('IsJobInvoiced') <> 1)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceDate',locInvoiceDate).
    End

Value::locInvoiceDate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locInvoiceDate'),'@d06')) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceDate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceDate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locVAT  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('VAT'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locVAT  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locVAT = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locVAT = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locVAT  ! copies value to session value if valid.
  do Comment::locVAT ! allows comment style to be updated.

ValidateValue::locVAT  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locVAT',locVAT).
    End

Value::locVAT  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locVAT = p_web.RestoreValue('locVAT')
    do ValidateValue::locVAT
    If locVAT:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locVAT
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locVAT',p_web.GetSessionValue('locVAT'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locVAT  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locVAT:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locTotal  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Total'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locTotal  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locTotal = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locTotal  ! copies value to session value if valid.
  do Comment::locTotal ! allows comment style to be updated.

ValidateValue::locTotal  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locTotal',locTotal).
    End

Value::locTotal  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locTotal = p_web.RestoreValue('locTotal')
    do ValidateValue::locTotal
    If locTotal:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locTotal',p_web.GetSessionValue('locTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locTotal  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locTotal:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Courier  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Courier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Courier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Courier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Courier  ! copies value to session value if valid.
  do Value::job:Courier
  do SendAlert
  do Comment::job:Courier ! allows comment style to be updated.

ValidateValue::job:Courier  Routine
    If not (1=0)
    job:Courier = Upper(job:Courier)
      if loc:invalid = '' then p_web.SetSessionValue('job:Courier',job:Courier).
    End

Value::job:Courier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    job:Courier = p_web.RestoreValue('job:Courier')
    do ValidateValue::job:Courier
    If job:Courier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''despatchconfirmation_job:courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBSCONS)
  bind(joc:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(VATCODE)
  bind(vat:Record)
  p_web._OpenFile(INVOICE)
  bind(inv:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Courier_OptionView)
  job:Courier_OptionView{prop:order} = p_web.CleanFilter(job:Courier_OptionView,'UPPER(cou:Courier)')
  Set(job:Courier_OptionView)
  Loop
    Next(job:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Courier') = 0
      p_web.SetSessionValue('job:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Courier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Courier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonCreateInvoice  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonCreateInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateInvoice  ! copies value to session value if valid.
  do Value::buttonCreateInvoice
  do Comment::buttonCreateInvoice ! allows comment style to be updated.

ValidateValue::buttonCreateInvoice  Routine
    If not (1=0)
    End

Value::buttonCreateInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateInvoice'',''despatchconfirmation_buttoncreateinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),p_web.combine(Choose(p_web.GSV('URL:CreateInvoiceText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip(p_web.GSV('URL:CreateInvoice'))&''&'',p_web.GSV('URL:CreateInvoiceTarget')),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCreateInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCreateInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPaymentDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPaymentDetails  ! copies value to session value if valid.
  do Comment::buttonPaymentDetails ! allows comment style to be updated.

ValidateValue::buttonPaymentDetails  Routine
    If not (p_web.GSV('Hide:PaymentDetails') = 1)
    End

Value::buttonPaymentDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PaymentDetails') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PaymentDetails','PaymentDetails',p_web.combine(Choose('PaymentDetails' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=DespatchConfirmation')&''&'','_self'),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPaymentDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPaymentDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PaymentDetails') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintWaybill  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintWaybill  ! copies value to session value if valid.
  do Value::buttonPrintWaybill
  do Comment::buttonPrintWaybill ! allows comment style to be updated.

ValidateValue::buttonPrintWaybill  Routine
    If not (p_web.GSV('Hide:PrintWaybill') = 1)
    End

Value::buttonPrintWaybill  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''despatchconfirmation_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintWaybill','Print Waybill',p_web.combine(Choose('Print Waybill' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('Waybill?var=' & RANDOM(1,9999999))&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintWaybill  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintWaybill:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintDespatchNote  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintDespatchNote  ! copies value to session value if valid.
  do Value::buttonPrintDespatchNote
  do Comment::buttonPrintDespatchNote ! allows comment style to be updated.

ValidateValue::buttonPrintDespatchNote  Routine
    If not (p_web.GSV('Hide:PrintDespatchNote') = 1)
    End

Value::buttonPrintDespatchNote  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''despatchconfirmation_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintDespatchNote','Print Despatch Note',p_web.combine(Choose('Print Despatch Note' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('DespatchNote?var=' & RANDOM(1,9999999))&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintDespatchNote  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintDespatchNote:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DespatchConfirmation_nexttab_' & 0)
    job:Account_Number = p_web.GSV('job:Account_Number')
    do ValidateValue::job:Account_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Account_Number
      !do SendAlert
      do Comment::job:Account_Number ! allows comment style to be updated.
      !exit
    End
    locCompanyName = p_web.GSV('locCompanyName')
    do ValidateValue::locCompanyName
    If loc:Invalid
      loc:retrying = 1
      do Value::locCompanyName
      !do SendAlert
      do Comment::locCompanyName ! allows comment style to be updated.
      !exit
    End
    job:Charge_Type = p_web.GSV('job:Charge_Type')
    do ValidateValue::job:Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Charge_Type
      !do SendAlert
      do Comment::job:Charge_Type ! allows comment style to be updated.
      !exit
    End
    job:Repair_Type = p_web.GSV('job:Repair_Type')
    do ValidateValue::job:Repair_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Repair_Type
      !do SendAlert
      do Comment::job:Repair_Type ! allows comment style to be updated.
      !exit
    End
    locCourierCost = p_web.GSV('locCourierCost')
    do ValidateValue::locCourierCost
    If loc:Invalid
      loc:retrying = 1
      do Value::locCourierCost
      !do SendAlert
      do Comment::locCourierCost ! allows comment style to be updated.
      !exit
    End
    locLabourCost = p_web.GSV('locLabourCost')
    do ValidateValue::locLabourCost
    If loc:Invalid
      loc:retrying = 1
      do Value::locLabourCost
      !do SendAlert
      do Comment::locLabourCost ! allows comment style to be updated.
      !exit
    End
    locInvoiceNumber = p_web.GSV('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceNumber
      !do SendAlert
      do Comment::locInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    locPartsCost = p_web.GSV('locPartsCost')
    do ValidateValue::locPartsCost
    If loc:Invalid
      loc:retrying = 1
      do Value::locPartsCost
      !do SendAlert
      do Comment::locPartsCost ! allows comment style to be updated.
      !exit
    End
    locSubTotal = p_web.GSV('locSubTotal')
    do ValidateValue::locSubTotal
    If loc:Invalid
      loc:retrying = 1
      do Value::locSubTotal
      !do SendAlert
      do Comment::locSubTotal ! allows comment style to be updated.
      !exit
    End
    locInvoiceDate = p_web.GSV('locInvoiceDate')
    do ValidateValue::locInvoiceDate
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceDate
      !do SendAlert
      do Comment::locInvoiceDate ! allows comment style to be updated.
      !exit
    End
    locVAT = p_web.GSV('locVAT')
    do ValidateValue::locVAT
    If loc:Invalid
      loc:retrying = 1
      do Value::locVAT
      !do SendAlert
      do Comment::locVAT ! allows comment style to be updated.
      !exit
    End
    locTotal = p_web.GSV('locTotal')
    do ValidateValue::locTotal
    If loc:Invalid
      loc:retrying = 1
      do Value::locTotal
      !do SendAlert
      do Comment::locTotal ! allows comment style to be updated.
      !exit
    End
    job:Courier = p_web.GSV('job:Courier')
    do ValidateValue::job:Courier
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Courier
      !do SendAlert
      do Comment::job:Courier ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('DespatchConfirmation_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_DespatchConfirmation_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DespatchConfirmation_tab_' & 0)
    do GenerateTab0
  of lower('DespatchConfirmation_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      of event:timer
        do Value::job:Courier
        do Comment::job:Courier
      else
        do Value::job:Courier
      end
  of lower('DespatchConfirmation_buttonCreateInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateInvoice
      of event:timer
        do Value::buttonCreateInvoice
        do Comment::buttonCreateInvoice
      else
        do Value::buttonCreateInvoice
      end
  of lower('DespatchConfirmation_tab_' & 1)
    do GenerateTab1
  of lower('DespatchConfirmation_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      of event:timer
        do Value::buttonPrintWaybill
        do Comment::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  of lower('DespatchConfirmation_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      of event:timer
        do Value::buttonPrintDespatchNote
        do Comment::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)

  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
      If not (1=0)
          If p_web.IfExistsValue('job:Courier')
            job:Courier = p_web.GetValue('job:Courier')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO deletesessionvalues
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:InvalidTab += 1
    do ValidateValue::job:Account_Number
    If loc:Invalid then exit.
    do ValidateValue::locCompanyName
    If loc:Invalid then exit.
    do ValidateValue::job:Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Repair_Type
    If loc:Invalid then exit.
    do ValidateValue::aLine
    If loc:Invalid then exit.
    do ValidateValue::textBillingDetails
    If loc:Invalid then exit.
    do ValidateValue::textInvoiceDetails
    If loc:Invalid then exit.
    do ValidateValue::locCourierCost
    If loc:Invalid then exit.
    do ValidateValue::locLabourCost
    If loc:Invalid then exit.
    do ValidateValue::locInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::locPartsCost
    If loc:Invalid then exit.
    do ValidateValue::locSubTotal
    If loc:Invalid then exit.
    do ValidateValue::locInvoiceDate
    If loc:Invalid then exit.
    do ValidateValue::locVAT
    If loc:Invalid then exit.
    do ValidateValue::locTotal
    If loc:Invalid then exit.
    do ValidateValue::job:Courier
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateInvoice
    If loc:Invalid then exit.
    do ValidateValue::buttonPaymentDetails
    If loc:Invalid then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::buttonPrintWaybill
    If loc:Invalid then exit.
    do ValidateValue::buttonPrintDespatchNote
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCourierCost')
  p_web.StoreValue('locLabourCost')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locPartsCost')
  p_web.StoreValue('locSubTotal')
  p_web.StoreValue('locInvoiceDate')
  p_web.StoreValue('locVAT')
  p_web.StoreValue('locTotal')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

BrowseStockControl   PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sto:Part_Number:IsInvalid  Long
sto:Description:IsInvalid  Long
sto:Manufacturer:IsInvalid  Long
sto:Shelf_Location:IsInvalid  Long
sto:Supplier:IsInvalid  Long
sto:Quantity_Stock:IsInvalid  Long
sto:AveragePurchaseCost:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOCK)
                      Project(sto:Ref_Number)
                      Project(sto:Part_Number)
                      Project(sto:Description)
                      Project(sto:Manufacturer)
                      Project(sto:Shelf_Location)
                      Project(sto:Supplier)
                      Project(sto:Quantity_Stock)
                      Project(sto:AveragePurchaseCost)
                      Project(sto:Purchase_Cost)
                      Project(sto:Sale_Cost)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseStockControl')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStockControl:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStockControl:NoForm')
      loc:FormName = p_web.GetValue('BrowseStockControl:FormName')
    else
      loc:FormName = 'BrowseStockControl_frm'
    End
    p_web.SSV('BrowseStockControl:NoForm',loc:NoForm)
    p_web.SSV('BrowseStockControl:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStockControl:NoForm')
    loc:FormName = p_web.GSV('BrowseStockControl:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStockControl') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStockControl')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseStockControl' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseStockControl')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseStockControl') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseStockControl')
      p_web.DivHeader('popup_BrowseStockControl','nt-hidden')
      p_web.DivHeader('BrowseStockControl',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseStockControl_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseStockControl_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseStockControl',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOCK,sto:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STO:PART_NUMBER') then p_web.SetValue('BrowseStockControl_sort','1')
    ElsIf (loc:vorder = 'STO:DESCRIPTION') then p_web.SetValue('BrowseStockControl_sort','2')
    ElsIf (loc:vorder = 'STO:SHELF_LOCATION') then p_web.SetValue('BrowseStockControl_sort','8')
    ElsIf (loc:vorder = 'STO:SUPPLIER') then p_web.SetValue('BrowseStockControl_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStockControl:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStockControl:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStockControl:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStockControl:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormStockControl'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 1
  loc:LocatorType = Net:Begins
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseStockControl')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
      p_web.site.ChangeButton.TextValue = 'View Stock Item'
      p_web.site.ChangeButton.Class = 'DoubleButton'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStockControl_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseStockControl_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Part_Number)','-UPPER(sto:Part_Number)')
    Loc:LocateField = 'sto:Part_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Description)','-UPPER(sto:Description)')
    Loc:LocateField = 'sto:Description'
    Loc:LocatorCase = 0
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Shelf_Location)','-UPPER(sto:Shelf_Location)')
    Loc:LocateField = 'sto:Shelf_Location'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Supplier)','-UPPER(sto:Supplier)')
    Loc:LocateField = 'sto:Supplier'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(sto:Location),+sto:Suspend,+UPPER(sto:Part_Number)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locFilterByManufacturer') = 1)
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sto:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@s30')
  Of upper('sto:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@s30')
  Of upper('sto:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@s30')
  Of upper('sto:Shelf_Location')
    loc:SortHeader = p_web.Translate('Shelf Location')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@s30')
  Of upper('sto:Supplier')
    loc:SortHeader = p_web.Translate('Supplier')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@s30')
  Of upper('sto:Quantity_Stock')
    loc:SortHeader = p_web.Translate('Qty')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@N8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:AveragePurchaseCost')
    loc:SortHeader = p_web.Translate('Purch Cost')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warr')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warr')
    p_web.SetSessionValue('BrowseStockControl_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseStockControl:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStockControl:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStockControl:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStockControl:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOCK"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sto:Ref_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseStockControl.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockControl',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseStockControl','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseStockControl_LocatorPic'),,,'onchange="BrowseStockControl.locate(''Locator2BrowseStockControl'',this.value);"',,0,,,'Locator') & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseStockControl',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseStockControl.locate(''Locator2BrowseStockControl'',this.value);" ',,,,,,'Locator') & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseStockControl_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockControl.cl(''BrowseStockControl'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockControl_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseStockControl_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseStockControl_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseStockControl_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseStockControl_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseStockControl',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseStockControl',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseStockControl',p_web.Translate('Manufacturer'),'Click here to sort by Manufacturer',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','BrowseStockControl',p_web.Translate('Shelf Location'),'Click here to sort by Shelf Location',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseStockControl',p_web.Translate('Supplier'),'Click here to sort by Supplier',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseStockControl',p_web.Translate('Qty'),'Click here to sort by Quantity In Stock',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','BrowseStockControl',p_web.Translate('Purch Cost'),'',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','BrowseStockControl',p_web.Translate('In Warr'),'',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'11','BrowseStockControl',p_web.Translate('Out Warr'),'',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sto:ref_number',lower(loc:vorder),1,1) = 0 !and STOCK{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sto:Ref_Number',clip(loc:vorder) & ',' & 'sto:Ref_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sto:Ref_Number'),p_web.GetValue('sto:Ref_Number'),p_web.GetSessionValue('sto:Ref_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locFilterByManufacturer') = 1)
      loc:FilterWas = 'UPPER(sto:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND sto:Suspend = 0 AND Upper(sto:Manufacturer) = UPPER(''' & p_web.GSV('locManufacturer') & ''')'
  Else
        loc:FilterWas = 'UPPER(sto:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND sto:Suspend = 0'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockControl',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStockControl_Filter')
    p_web.SetSessionValue('BrowseStockControl_FirstValue','')
    p_web.SetSessionValue('BrowseStockControl_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOCK,sto:Ref_Number_Key,loc:PageRows,'BrowseStockControl',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOCK{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOCK,loc:firstvalue)
              Reset(ThisView,STOCK)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOCK{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOCK,loc:lastvalue)
            Reset(ThisView,STOCK)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sto:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a full or partial search term in the locator field above')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    p_web.SSV('ShowBrowseStock',0)
    p_web.SSV('sto:Ref_Number','')
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockControl_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockControl.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockControl.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockControl.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockControl.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockControl_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseStockControl_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:found
          Loc:IsChange = 0
          If loc:selecting = 0 or loc:popup
            If loc:viewonly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseStockControl',,,loc:FormPopup,'FormStockControl')
              loc:isChange = 1
            End
          End
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockControl_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockControl',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStockControl_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStockControl_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseStockControl','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseStockControl_LocatorPic'),,,'onchange="BrowseStockControl.locate(''Locator1BrowseStockControl'',this.value);"',,0,,,'Locator') & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseStockControl',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseStockControl.locate(''Locator1BrowseStockControl'',this.value);" ',,,,,,'Locator') & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseStockControl_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockControl.cl(''BrowseStockControl'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockControl_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseStockControl_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStockControl_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockControl_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockControl.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockControl.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockControl.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockControl.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockControl_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseStockControl_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:found
        Loc:IsChange = 0
        If loc:selecting = 0 or loc:popup
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseStockControl',,,loc:FormPopup,'FormStockControl')
            loc:isChange = 1
          End
        End
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockControl_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseStockControl','STOCK',sto:Ref_Number_Key) !sto:Ref_Number
    p_web._thisrow = p_web._nocolon('sto:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sto:Ref_Number = p_web.GetValue('sto:Ref_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStockControl:LookupField')) = sto:Ref_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sto:Ref_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseStockControl','STOCK',sto:Ref_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOCK{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOCK)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOCK{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOCK)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(150)&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(120)&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(120)&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Shelf_Location
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Supplier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextCenter')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Quantity_Stock
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:AveragePurchaseCost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseStockControl','STOCK',sto:Ref_Number_Key)
  TableQueue.Id[1] = sto:Ref_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseStockControl;if (btiBrowseStockControl != 1){{var BrowseStockControl=new browseTable(''BrowseStockControl'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sto:Ref_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,0,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormStockControl'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseStockControl.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseStockControl.applyGreenBar();btiBrowseStockControl=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockControl')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockControl')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockControl')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockControl')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  Clear(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sto:Ref_Number',p_web.GetValue('sto:Ref_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  loc:result = p_web._GetFile(STOCK,sto:Ref_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sto:Ref_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOCK)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOCK)
! ----------------------------------------------------------------------------------------
value::sto:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Part_Number_'&sto:Ref_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Description_'&sto:Ref_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Manufacturer   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Manufacturer_'&sto:Ref_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Manufacturer,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Shelf_Location   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Shelf_Location_'&sto:Ref_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Shelf_Location,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Supplier   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Supplier_'&sto:Ref_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Supplier,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Quantity_Stock   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Quantity_Stock_'&sto:Ref_Number,'SmallTextCenter',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Quantity_Stock,'@N8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:AveragePurchaseCost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:AveragePurchaseCost_'&sto:Ref_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:AveragePurchaseCost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Purchase_Cost_'&sto:Ref_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Purchase_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockControl_sto:Sale_Cost_'&sto:Ref_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Sale_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sto:Ref_Number',sto:Ref_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SaveSessionVars  ROUTINE
RestoreSessionVars  ROUTINE
FormAddStock         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locQuantity          LONG                                  !
locDespatchNoteNumber STRING(30)                           !
locAdditionalNotes   STRING(255)                           !
FilesOpened     Long
STOCK::State  USHORT
sto:Part_Number:IsInvalid  Long
sto:Description:IsInvalid  Long
sto:AveragePurchaseCost:IsInvalid  Long
sto:PurchaseMarkUp:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
sto:Percentage_Mark_Up:IsInvalid  Long
locDespatchNoteNumber:IsInvalid  Long
locQuantity:IsInvalid  Long
locAdditionalNotes:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('FormAddStock')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormAddStock_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormAddStock','Change')
    p_web.DivHeader('FormAddStock',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormAddStock') = 0
        p_web.AddPreCall('FormAddStock')
        p_web.DivHeader('popup_FormAddStock','nt-hidden')
        p_web.DivHeader('FormAddStock',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormAddStock_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormAddStock_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAddStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormAddStock',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormAddStock',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddStock',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormAddStock',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddStock',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddStock',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAddStock',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormAddStock')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
      p_web.SSV('locQuantity',1)
      p_web.SSV('locAdditionalNotes','')
      p_web.SSV('locDespatchnoteNumber','')
  
  END
  p_web.FileToSessionQueue(STOCK)
  p_web.SetValue('FormAddStock_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormAddStock'
    end
    p_web.formsettings.proc = 'FormAddStock'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('sto:AveragePurchaseCost')
    p_web.SetPicture('sto:AveragePurchaseCost','@n14.2')
  End
  p_web.SetSessionPicture('sto:AveragePurchaseCost','@n14.2')
  If p_web.IfExistsValue('sto:PurchaseMarkUp')
    p_web.SetPicture('sto:PurchaseMarkUp','@n14.2')
  End
  p_web.SetSessionPicture('sto:PurchaseMarkUp','@n14.2')
  If p_web.IfExistsValue('sto:Purchase_Cost')
    p_web.SetPicture('sto:Purchase_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Purchase_Cost','@n14.2')
  If p_web.IfExistsValue('sto:Sale_Cost')
    p_web.SetPicture('sto:Sale_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Sale_Cost','@n14.2')
  If p_web.IfExistsValue('sto:Percentage_Mark_Up')
    p_web.SetPicture('sto:Percentage_Mark_Up','@n6.2')
  End
  p_web.SetSessionPicture('sto:Percentage_Mark_Up','@n6.2')
  If p_web.IfExistsValue('locQuantity')
    p_web.SetPicture('locQuantity','@n_6')
  End
  p_web.SetSessionPicture('locQuantity','@n_6')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('sto:Part_Number') = 0
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description') = 0
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('sto:AveragePurchaseCost') = 0
    p_web.SetSessionValue('sto:AveragePurchaseCost',sto:AveragePurchaseCost)
  Else
    sto:AveragePurchaseCost = p_web.GetSessionValue('sto:AveragePurchaseCost')
  End
  if p_web.IfExistsValue('sto:PurchaseMarkUp') = 0
    p_web.SetSessionValue('sto:PurchaseMarkUp',sto:PurchaseMarkUp)
  Else
    sto:PurchaseMarkUp = p_web.GetSessionValue('sto:PurchaseMarkUp')
  End
  if p_web.IfExistsValue('sto:Purchase_Cost') = 0
    p_web.SetSessionValue('sto:Purchase_Cost',sto:Purchase_Cost)
  Else
    sto:Purchase_Cost = p_web.GetSessionValue('sto:Purchase_Cost')
  End
  if p_web.IfExistsValue('sto:Sale_Cost') = 0
    p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  Else
    sto:Sale_Cost = p_web.GetSessionValue('sto:Sale_Cost')
  End
  if p_web.IfExistsValue('sto:Percentage_Mark_Up') = 0
    p_web.SetSessionValue('sto:Percentage_Mark_Up',sto:Percentage_Mark_Up)
  Else
    sto:Percentage_Mark_Up = p_web.GetSessionValue('sto:Percentage_Mark_Up')
  End
  if p_web.IfExistsValue('locDespatchNoteNumber') = 0
    p_web.SetSessionValue('locDespatchNoteNumber',locDespatchNoteNumber)
  Else
    locDespatchNoteNumber = p_web.GetSessionValue('locDespatchNoteNumber')
  End
  if p_web.IfExistsValue('locQuantity') = 0
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locAdditionalNotes') = 0
    p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes)
  Else
    locAdditionalNotes = p_web.GetSessionValue('locAdditionalNotes')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('sto:AveragePurchaseCost')
    sto:AveragePurchaseCost = p_web.GetValue('sto:AveragePurchaseCost')
    p_web.SetSessionValue('sto:AveragePurchaseCost',sto:AveragePurchaseCost)
  Else
    sto:AveragePurchaseCost = p_web.GetSessionValue('sto:AveragePurchaseCost')
  End
  if p_web.IfExistsValue('sto:PurchaseMarkUp')
    sto:PurchaseMarkUp = p_web.GetValue('sto:PurchaseMarkUp')
    p_web.SetSessionValue('sto:PurchaseMarkUp',sto:PurchaseMarkUp)
  Else
    sto:PurchaseMarkUp = p_web.GetSessionValue('sto:PurchaseMarkUp')
  End
  if p_web.IfExistsValue('sto:Purchase_Cost')
    sto:Purchase_Cost = p_web.GetValue('sto:Purchase_Cost')
    p_web.SetSessionValue('sto:Purchase_Cost',sto:Purchase_Cost)
  Else
    sto:Purchase_Cost = p_web.GetSessionValue('sto:Purchase_Cost')
  End
  if p_web.IfExistsValue('sto:Sale_Cost')
    sto:Sale_Cost = p_web.GetValue('sto:Sale_Cost')
    p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  Else
    sto:Sale_Cost = p_web.GetSessionValue('sto:Sale_Cost')
  End
  if p_web.IfExistsValue('sto:Percentage_Mark_Up')
    sto:Percentage_Mark_Up = p_web.GetValue('sto:Percentage_Mark_Up')
    p_web.SetSessionValue('sto:Percentage_Mark_Up',sto:Percentage_Mark_Up)
  Else
    sto:Percentage_Mark_Up = p_web.GetSessionValue('sto:Percentage_Mark_Up')
  End
  if p_web.IfExistsValue('locDespatchNoteNumber')
    locDespatchNoteNumber = p_web.GetValue('locDespatchNoteNumber')
    p_web.SetSessionValue('locDespatchNoteNumber',locDespatchNoteNumber)
  Else
    locDespatchNoteNumber = p_web.GetSessionValue('locDespatchNoteNumber')
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.dformat(clip(p_web.GetValue('locQuantity')),'@n_6')
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locAdditionalNotes')
    locAdditionalNotes = p_web.GetValue('locAdditionalNotes')
    p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes)
  Else
    locAdditionalNotes = p_web.GetSessionValue('locAdditionalNotes')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormAddStock_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAddStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAddStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAddStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
 locDespatchNoteNumber = p_web.RestoreValue('locDespatchNoteNumber')
 locQuantity = p_web.RestoreValue('locQuantity')
 locAdditionalNotes = p_web.RestoreValue('locAdditionalNotes')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('sto:Ref_Number') = '',1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Add Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Add Stock',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormAddStock',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormAddStock0_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormAddStock_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormAddStock_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormAddStock_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormAddStock_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormAddStock_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locDespatchNoteNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormAddStock')>0,p_web.GSV('showtab_FormAddStock'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormAddStock'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormAddStock') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormAddStock'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormAddStock')>0,p_web.GSV('showtab_FormAddStock'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormAddStock') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormAddStock_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormAddStock')>0,p_web.GSV('showtab_FormAddStock'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormAddStock",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormAddStock')>0,p_web.GSV('showtab_FormAddStock'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormAddStock_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormAddStock') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormAddStock')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormAddStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:AveragePurchaseCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:AveragePurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:AveragePurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:PurchaseMarkUp
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:PurchaseMarkUp
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:PurchaseMarkUp
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Purchase_Cost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Purchase_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Purchase_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Sale_Cost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Sale_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Sale_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Percentage_Mark_Up
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Percentage_Mark_Up
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Percentage_Mark_Up
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locDespatchNoteNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locDespatchNoteNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locDespatchNoteNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locQuantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAdditionalNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAdditionalNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAdditionalNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::sto:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Part_Number  ! copies value to session value if valid.
  do Comment::sto:Part_Number ! allows comment style to be updated.

ValidateValue::sto:Part_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Part_Number',sto:Part_Number).
    End

Value::sto:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Part_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Part_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Part_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Description  ! copies value to session value if valid.
  do Comment::sto:Description ! allows comment style to be updated.

ValidateValue::sto:Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Description',sto:Description).
    End

Value::sto:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:AveragePurchaseCost  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:AveragePurchaseCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Average Purchase Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:AveragePurchaseCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:AveragePurchaseCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:AveragePurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:AveragePurchaseCost  ! copies value to session value if valid.
  do Comment::sto:AveragePurchaseCost ! allows comment style to be updated.

ValidateValue::sto:AveragePurchaseCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:AveragePurchaseCost',sto:AveragePurchaseCost).
    End

Value::sto:AveragePurchaseCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:AveragePurchaseCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:AveragePurchaseCost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:AveragePurchaseCost'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:AveragePurchaseCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:AveragePurchaseCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:AveragePurchaseCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:PurchaseMarkUp  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:PurchaseMarkUp') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Percentage Mark Up'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:PurchaseMarkUp  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:PurchaseMarkUp = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:PurchaseMarkUp = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:PurchaseMarkUp  ! copies value to session value if valid.
  do Comment::sto:PurchaseMarkUp ! allows comment style to be updated.

ValidateValue::sto:PurchaseMarkUp  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:PurchaseMarkUp',sto:PurchaseMarkUp).
    End

Value::sto:PurchaseMarkUp  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:PurchaseMarkUp') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:PurchaseMarkUp
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:PurchaseMarkUp'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:PurchaseMarkUp  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:PurchaseMarkUp:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:PurchaseMarkUp') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:Purchase_Cost  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Purchase_Cost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('In Warranty Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Purchase_Cost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Purchase_Cost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:Purchase_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:Purchase_Cost  ! copies value to session value if valid.
  do Comment::sto:Purchase_Cost ! allows comment style to be updated.

ValidateValue::sto:Purchase_Cost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Purchase_Cost',sto:Purchase_Cost).
    End

Value::sto:Purchase_Cost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Purchase_Cost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Purchase_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Purchase_Cost'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Purchase_Cost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Purchase_Cost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Purchase_Cost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:Sale_Cost  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Sale_Cost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Out Warranty Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Sale_Cost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Sale_Cost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:Sale_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:Sale_Cost  ! copies value to session value if valid.
  do Comment::sto:Sale_Cost ! allows comment style to be updated.

ValidateValue::sto:Sale_Cost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost).
    End

Value::sto:Sale_Cost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Sale_Cost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Sale_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Sale_Cost'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Sale_Cost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Sale_Cost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Sale_Cost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:Percentage_Mark_Up  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Percentage Mark Up'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Percentage_Mark_Up  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Percentage_Mark_Up = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n6.2
    sto:Percentage_Mark_Up = p_web.Dformat(p_web.GetValue('Value'),'@n6.2')
  End
  do ValidateValue::sto:Percentage_Mark_Up  ! copies value to session value if valid.
  do Comment::sto:Percentage_Mark_Up ! allows comment style to be updated.

ValidateValue::sto:Percentage_Mark_Up  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Percentage_Mark_Up',sto:Percentage_Mark_Up).
    End

Value::sto:Percentage_Mark_Up  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Percentage_Mark_Up
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Percentage_Mark_Up'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Percentage_Mark_Up  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Percentage_Mark_Up:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locDespatchNoteNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('locDespatchNoteNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Despatch Note No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locDespatchNoteNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locDespatchNoteNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locDespatchNoteNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locDespatchNoteNumber  ! copies value to session value if valid.
  do Value::locDespatchNoteNumber
  do SendAlert
  do Comment::locDespatchNoteNumber ! allows comment style to be updated.

ValidateValue::locDespatchNoteNumber  Routine
    If not (1=0)
    locDespatchNoteNumber = Upper(locDespatchNoteNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locDespatchNoteNumber',locDespatchNoteNumber).
    End

Value::locDespatchNoteNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('locDespatchNoteNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locDespatchNoteNumber = p_web.RestoreValue('locDespatchNoteNumber')
    do ValidateValue::locDespatchNoteNumber
    If locDespatchNoteNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locDespatchNoteNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locDespatchNoteNumber'',''formaddstock_locdespatchnotenumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locDespatchNoteNumber',p_web.GetSessionValueFormat('locDespatchNoteNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locDespatchNoteNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locDespatchNoteNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('locDespatchNoteNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locQuantity  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('locQuantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locQuantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locQuantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_6'  !FieldPicture = 
    locQuantity = p_web.Dformat(p_web.GetValue('Value'),'@n_6')
  End
  do ValidateValue::locQuantity  ! copies value to session value if valid.
  do Value::locQuantity
  do SendAlert
  do Comment::locQuantity ! allows comment style to be updated.

ValidateValue::locQuantity  Routine
    If not (1=0)
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.RequiredText
  End
  If Numeric(locQuantity) = 0
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.NumericText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locQuantity',locQuantity).
    End

Value::locQuantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('locQuantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locQuantity = p_web.RestoreValue('locQuantity')
    do ValidateValue::locQuantity
    If locQuantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formaddstock_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValue('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_6',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locQuantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locQuantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('locQuantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAdditionalNotes  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('locAdditionalNotes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Additional Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAdditionalNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAdditionalNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAdditionalNotes = p_web.GetValue('Value')
  End
  do ValidateValue::locAdditionalNotes  ! copies value to session value if valid.
  do Value::locAdditionalNotes
  do SendAlert
  do Comment::locAdditionalNotes ! allows comment style to be updated.

ValidateValue::locAdditionalNotes  Routine
    If not (1=0)
    locAdditionalNotes = Upper(locAdditionalNotes)
      if loc:invalid = '' then p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes).
    End

Value::locAdditionalNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddStock_' & p_web._nocolon('locAdditionalNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locAdditionalNotes = p_web.RestoreValue('locAdditionalNotes')
    do ValidateValue::locAdditionalNotes
    If locAdditionalNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- locAdditionalNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAdditionalNotes'',''formaddstock_locadditionalnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locAdditionalNotes',p_web.GetSessionValue('locAdditionalNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locAdditionalNotes),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAdditionalNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAdditionalNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAddStock_' & p_web._nocolon('locAdditionalNotes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormAddStock_nexttab_' & 0)
    sto:Part_Number = p_web.GSV('sto:Part_Number')
    do ValidateValue::sto:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Part_Number
      !do SendAlert
      do Comment::sto:Part_Number ! allows comment style to be updated.
      !exit
    End
    sto:Description = p_web.GSV('sto:Description')
    do ValidateValue::sto:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Description
      !do SendAlert
      do Comment::sto:Description ! allows comment style to be updated.
      !exit
    End
    sto:AveragePurchaseCost = p_web.GSV('sto:AveragePurchaseCost')
    do ValidateValue::sto:AveragePurchaseCost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:AveragePurchaseCost
      !do SendAlert
      do Comment::sto:AveragePurchaseCost ! allows comment style to be updated.
      !exit
    End
    sto:PurchaseMarkUp = p_web.GSV('sto:PurchaseMarkUp')
    do ValidateValue::sto:PurchaseMarkUp
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:PurchaseMarkUp
      !do SendAlert
      do Comment::sto:PurchaseMarkUp ! allows comment style to be updated.
      !exit
    End
    sto:Purchase_Cost = p_web.GSV('sto:Purchase_Cost')
    do ValidateValue::sto:Purchase_Cost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Purchase_Cost
      !do SendAlert
      do Comment::sto:Purchase_Cost ! allows comment style to be updated.
      !exit
    End
    sto:Sale_Cost = p_web.GSV('sto:Sale_Cost')
    do ValidateValue::sto:Sale_Cost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Sale_Cost
      !do SendAlert
      do Comment::sto:Sale_Cost ! allows comment style to be updated.
      !exit
    End
    sto:Percentage_Mark_Up = p_web.GSV('sto:Percentage_Mark_Up')
    do ValidateValue::sto:Percentage_Mark_Up
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Percentage_Mark_Up
      !do SendAlert
      do Comment::sto:Percentage_Mark_Up ! allows comment style to be updated.
      !exit
    End
    locDespatchNoteNumber = p_web.GSV('locDespatchNoteNumber')
    do ValidateValue::locDespatchNoteNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locDespatchNoteNumber
      !do SendAlert
      do Comment::locDespatchNoteNumber ! allows comment style to be updated.
      !exit
    End
    locQuantity = p_web.GSV('locQuantity')
    do ValidateValue::locQuantity
    If loc:Invalid
      loc:retrying = 1
      do Value::locQuantity
      !do SendAlert
      do Comment::locQuantity ! allows comment style to be updated.
      !exit
    End
    locAdditionalNotes = p_web.GSV('locAdditionalNotes')
    do ValidateValue::locAdditionalNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::locAdditionalNotes
      !do SendAlert
      do Comment::locAdditionalNotes ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormAddStock_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormAddStock_tab_' & 0)
    do GenerateTab0
  of lower('FormAddStock_locDespatchNoteNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDespatchNoteNumber
      of event:timer
        do Value::locDespatchNoteNumber
        do Comment::locDespatchNoteNumber
      else
        do Value::locDespatchNoteNumber
      end
  of lower('FormAddStock_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      of event:timer
        do Value::locQuantity
        do Comment::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormAddStock_locAdditionalNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAdditionalNotes
      of event:timer
        do Value::locAdditionalNotes
        do Comment::locAdditionalNotes
      else
        do Value::locAdditionalNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormAddStock_form:ready_',1)

  p_web.SetSessionValue('FormAddStock_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormAddStock',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAddStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAddStock',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAddStock_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormAddStock:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAddStock_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormAddStock:Primed',0)
  p_web.setsessionvalue('showtab_FormAddStock',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locDespatchNoteNumber')
            locDespatchNoteNumber = p_web.GetValue('locDespatchNoteNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locQuantity')
            locQuantity = p_web.GetValue('locQuantity')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locAdditionalNotes')
            locAdditionalNotes = p_web.GetValue('locAdditionalNotes')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAddStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  
  
      Pointer# = Pointer(STOCK)
      Hold(STOCK,1)
      Get(STOCK,Pointer#)
      If Errorcode() = 43
          loc:Alert = 'This stock item is current in use by another station'
          loc:invalid = 'loc:Quantity'
          EXIT
      END
  
      sto:Quantity_Stock += p_web.GSV('locQuantity')
      IF (Access:STOCK.TryUpdate() = Level:Benign)
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
              'ADD', | ! Transaction_Type
              p_web.GSV('locDespatchNoteNumber'), | ! Depatch_Note_Number
              0, | ! Job_Number
              0, | ! Sales_Number
              p_web.GSV('locQuantity'), | ! Quantity
              sto:Purchase_Cost, | ! Purchase_Cost
              sto:Sale_Cost, | ! Sale_Cost
              sto:Retail_Cost, | ! Retail_Cost
              'STOCK ADDED', | ! Notes
              Clip(p_web.GSV('locAdditionalNotes')), |
              p_web.GSV('BookingUserCode'), |
              sto:Quantity_stock) ! Information
                    ! Added OK
          Else ! AddToStockHistory
                    ! Error
          End ! AddToStockHistory
      END
      RELEASE(STOCK)
  END
  p_web.DeleteSessionValue('FormAddStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::sto:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::sto:Description
    If loc:Invalid then exit.
    do ValidateValue::sto:AveragePurchaseCost
    If loc:Invalid then exit.
    do ValidateValue::sto:PurchaseMarkUp
    If loc:Invalid then exit.
    do ValidateValue::sto:Purchase_Cost
    If loc:Invalid then exit.
    do ValidateValue::sto:Sale_Cost
    If loc:Invalid then exit.
    do ValidateValue::sto:Percentage_Mark_Up
    If loc:Invalid then exit.
    do ValidateValue::locDespatchNoteNumber
    If loc:Invalid then exit.
    do ValidateValue::locQuantity
    If loc:Invalid then exit.
    do ValidateValue::locAdditionalNotes
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormAddStock:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('sto:AveragePurchaseCost')
  p_web.StoreValue('sto:PurchaseMarkUp')
  p_web.StoreValue('sto:Purchase_Cost')
  p_web.StoreValue('sto:Sale_Cost')
  p_web.StoreValue('sto:Percentage_Mark_Up')
  p_web.StoreValue('locDespatchNoteNumber')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locAdditionalNotes')

FormDepleteStock     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locQuantity          LONG                                  !
locAdditionalNotes   STRING(255)                           !
FilesOpened     Long
STOCK::State  USHORT
sto:Part_Number:IsInvalid  Long
sto:Description:IsInvalid  Long
locQuantity:IsInvalid  Long
locAdditionalNotes:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormDepleteStock')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormDepleteStock_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormDepleteStock','')
    p_web.DivHeader('FormDepleteStock',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormDepleteStock') = 0
        p_web.AddPreCall('FormDepleteStock')
        p_web.DivHeader('popup_FormDepleteStock','nt-hidden')
        p_web.DivHeader('FormDepleteStock',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormDepleteStock_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormDepleteStock_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormDepleteStock',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormDepleteStock')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  Access:Stock.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
      p_web.FileToSessionQueue(STOCK)
      p_web.SSV('locQuantity',1)
      p_web.SSV('locAdditionalNotes','')
  
  END
  p_web.SetValue('FormDepleteStock_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormDepleteStock'
    end
    p_web.formsettings.proc = 'FormDepleteStock'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('sto:Part_Number') = 0
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description') = 0
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('locQuantity') = 0
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locAdditionalNotes') = 0
    p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes)
  Else
    locAdditionalNotes = p_web.GetSessionValue('locAdditionalNotes')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.GetValue('locQuantity')
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locAdditionalNotes')
    locAdditionalNotes = p_web.GetValue('locAdditionalNotes')
    p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes)
  Else
    locAdditionalNotes = p_web.GetSessionValue('locAdditionalNotes')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormDepleteStock_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormDepleteStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormDepleteStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormDepleteStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
 locQuantity = p_web.RestoreValue('locQuantity')
 locAdditionalNotes = p_web.RestoreValue('locAdditionalNotes')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Deplete Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Deplete Stock',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormDepleteStock',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormDepleteStock0_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormDepleteStock_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormDepleteStock_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormDepleteStock_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormDepleteStock_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormDepleteStock_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locQuantity')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormDepleteStock')>0,p_web.GSV('showtab_FormDepleteStock'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormDepleteStock'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormDepleteStock') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormDepleteStock'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormDepleteStock')>0,p_web.GSV('showtab_FormDepleteStock'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormDepleteStock') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormDepleteStock_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormDepleteStock')>0,p_web.GSV('showtab_FormDepleteStock'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormDepleteStock",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormDepleteStock')>0,p_web.GSV('showtab_FormDepleteStock'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormDepleteStock_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormDepleteStock') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormDepleteStock')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDepleteStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locQuantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAdditionalNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAdditionalNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAdditionalNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::sto:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Part_Number  ! copies value to session value if valid.
  do Comment::sto:Part_Number ! allows comment style to be updated.

ValidateValue::sto:Part_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Part_Number',sto:Part_Number).
    End

Value::sto:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Part_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Part_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Part_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Description  ! copies value to session value if valid.
  do Comment::sto:Description ! allows comment style to be updated.

ValidateValue::sto:Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Description',sto:Description).
    End

Value::sto:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locQuantity  Routine
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locQuantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locQuantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locQuantity = p_web.GetValue('Value')
  End
  do ValidateValue::locQuantity  ! copies value to session value if valid.
  do Value::locQuantity
  do SendAlert
  do Comment::locQuantity ! allows comment style to be updated.

ValidateValue::locQuantity  Routine
    If not (1=0)
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.RequiredText
  End
  If Numeric(locQuantity) = 0
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.NumericText
  End
    locQuantity = Upper(locQuantity)
      if loc:invalid = '' then p_web.SetSessionValue('locQuantity',locQuantity).
    End

Value::locQuantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locQuantity = p_web.RestoreValue('locQuantity')
    do ValidateValue::locQuantity
    If locQuantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formdepletestock_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValueFormat('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locQuantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locQuantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAdditionalNotes  Routine
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Additional Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAdditionalNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAdditionalNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAdditionalNotes = p_web.GetValue('Value')
  End
  do ValidateValue::locAdditionalNotes  ! copies value to session value if valid.
  do Value::locAdditionalNotes
  do SendAlert
  do Comment::locAdditionalNotes ! allows comment style to be updated.

ValidateValue::locAdditionalNotes  Routine
    If not (1=0)
    locAdditionalNotes = Upper(locAdditionalNotes)
      if loc:invalid = '' then p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes).
    End

Value::locAdditionalNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locAdditionalNotes = p_web.RestoreValue('locAdditionalNotes')
    do ValidateValue::locAdditionalNotes
    If locAdditionalNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- locAdditionalNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAdditionalNotes'',''formdepletestock_locadditionalnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locAdditionalNotes',p_web.GetSessionValue('locAdditionalNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locAdditionalNotes),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAdditionalNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAdditionalNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormDepleteStock_nexttab_' & 0)
    sto:Part_Number = p_web.GSV('sto:Part_Number')
    do ValidateValue::sto:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Part_Number
      !do SendAlert
      do Comment::sto:Part_Number ! allows comment style to be updated.
      !exit
    End
    sto:Description = p_web.GSV('sto:Description')
    do ValidateValue::sto:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Description
      !do SendAlert
      do Comment::sto:Description ! allows comment style to be updated.
      !exit
    End
    locQuantity = p_web.GSV('locQuantity')
    do ValidateValue::locQuantity
    If loc:Invalid
      loc:retrying = 1
      do Value::locQuantity
      !do SendAlert
      do Comment::locQuantity ! allows comment style to be updated.
      !exit
    End
    locAdditionalNotes = p_web.GSV('locAdditionalNotes')
    do ValidateValue::locAdditionalNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::locAdditionalNotes
      !do SendAlert
      do Comment::locAdditionalNotes ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormDepleteStock_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormDepleteStock_tab_' & 0)
    do GenerateTab0
  of lower('FormDepleteStock_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      of event:timer
        do Value::locQuantity
        do Comment::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormDepleteStock_locAdditionalNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAdditionalNotes
      of event:timer
        do Value::locAdditionalNotes
        do Comment::locAdditionalNotes
      else
        do Value::locAdditionalNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)

  p_web.SetSessionValue('FormDepleteStock_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormDepleteStock',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormDepleteStock',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormDepleteStock:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormDepleteStock:Primed',0)
  p_web.setsessionvalue('showtab_FormDepleteStock',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locQuantity')
            locQuantity = p_web.GetValue('locQuantity')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locAdditionalNotes')
            locAdditionalNotes = p_web.GetValue('locAdditionalNotes')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormDepleteStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:STOCK.Clearkey(sto:Ref_Number_Key)
  sto:Ref_Number  = p_web.GSV('sto:Ref_Number')
  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Found
  
      IF (p_web.GSV('locQuantity') > sto:Quantity_Stock)
          loc:Alert = 'You cannot decrement the stock level below zero.'
          loc:Invalid = 'locQuantity'
          EXIT
      END
  
      Pointer# = Pointer(STOCK)
      Hold(STOCK,1)
      Get(STOCK,Pointer#)
      If Errorcode() = 43
          loc:Alert = 'This stock item is currently in use. Please try again in a moment.'
          loc:Invalid = 'locQuantity'
          EXIT
      END
      sto:Quantity_Stock -= p_web.GSV('locQuantity')
  
      IF (sto:Quantity_Stock < 0)
          sto:Quantity_Stock = 0
      END
  
      IF (sto:Location <> VodacomClass.MainStoreLocation())
          IF (VodacomClass.MainStoreSuspended(sto:Part_Number))
              sto:Suspend = 1
              loc:Alert = 'There are no more items in stock. This part has been suspended.'
          END
      END
      IF (Access:STOCK.TryUpdate() = Level:Benign)
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
              'DEC', | ! Transaction_Type
              '', | ! Depatch_Note_Number
              0, | ! Job_Number
              0, | ! Sales_Number
              p_web.GSV('locQuantity'), | ! Quantity
              sto:Purchase_Cost, | ! Purchase_Cost
              sto:Sale_Cost, | ! Sale_Cost
              sto:Retail_Cost, | ! Retail_Cost
              'STOCK DECREMENTED', | ! Notes
              Clip(p_web.GSV('locAdditionalNotes')),|
              p_web.GSV('BookingUsercode'), |
              sto:Quantity_stock) ! Information
                            ! Added OK
          Else ! AddToStockHistory
                            ! Error
          End ! AddToStockHistory
  
          if (sto:Suspend)
              if (AddToStockHistory(sto:Ref_Number, |
                  'ADD',|
                  '',|
                  0, |
                  0, |
                  0, |
                  sto:Purchase_Cost,|
                  sto:Sale_Cost, |
                  sto:Retail_Cost, |
                  'PART SUSPENDED',|
                  '',|
                  p_web.GSV('BookingUsercode'), |
                  sto:Quantity_stock))
              end
          end
      END
  
  END
  p_web.DeleteSessionValue('FormDepleteStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::sto:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::sto:Description
    If loc:Invalid then exit.
    do ValidateValue::locQuantity
    If loc:Invalid then exit.
    do ValidateValue::locAdditionalNotes
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormDepleteStock:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locAdditionalNotes')

BrowseStockHistory   PROCEDURE  (NetWebServerWorker p_web)
locDateIn            DATE                                  !
locDateOut           DATE                                  !
locJobRetail         STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
locdateIn:IsInvalid  Long
locDateOut:IsInvalid  Long
shi:Quantity:IsInvalid  Long
locJobRetail:IsInvalid  Long
shi:Purchase_Cost:IsInvalid  Long
shi:Sale_Cost:IsInvalid  Long
shi:User:IsInvalid  Long
shi:Notes:IsInvalid  Long
shi:Information:IsInvalid  Long
shi:StockOnHand:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOHIST)
                      Project(shi:Record_Number)
                      Project(shi:Quantity)
                      Project(shi:Purchase_Cost)
                      Project(shi:Sale_Cost)
                      Project(shi:User)
                      Project(shi:Notes)
                      Project(shi:Information)
                      Project(shi:StockOnHand)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseStockHistory')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStockHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStockHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseStockHistory:FormName')
    else
      loc:FormName = 'BrowseStockHistory_frm'
    End
    p_web.SSV('BrowseStockHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseStockHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStockHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseStockHistory:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStockHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStockHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseStockHistory' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseStockHistory')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseStockHistory') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseStockHistory')
      p_web.DivHeader('popup_BrowseStockHistory','nt-hidden')
      p_web.DivHeader('BrowseStockHistory',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseStockHistory_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseStockHistory_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseStockHistory',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOHIST,shi:record_number_key,loc:vorder)
    If False
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStockHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStockHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStockHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStockHistory:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseStockHistory'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseStockHistory')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
      p_web.site.CloseButton.Image = '/images/psave.png'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStockHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStockHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  if loc:vorder = ''
    loc:vorder = '+shi:Ref_Number,+shi:Date'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('locDateIn')
    loc:SortHeader = p_web.Translate('Date In')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@d06b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('locDateOut')
    loc:SortHeader = p_web.Translate('Date Out')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@d06b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('shi:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('locJobRetail')
    loc:SortHeader = p_web.Translate('Job/Retail')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s10')
  Of upper('shi:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warr')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('shi:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warr')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('shi:User')
    loc:SortHeader = p_web.Translate('User')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s3')
  Of upper('shi:Notes')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s255')
  Of upper('shi:Information')
    loc:SortHeader = p_web.Translate('Information')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s255')
  Of upper('shi:StockOnHand')
    loc:SortHeader = p_web.Translate('Stock On Hand')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s8')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseStockHistory:LookupFrom')
  End!Else
  loc:CloseAction = p_web.GSV('ReturnURL')
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStockHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStockHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStockHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOHIST"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="shi:record_number_key"></input><13,10>'
  end
  If p_web.Translate('Browse Stock History') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse Stock History',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseStockHistory.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseStockHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseStockHistory_LocatorPic'),,,'onchange="BrowseStockHistory.locate(''Locator2BrowseStockHistory'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseStockHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseStockHistory.locate(''Locator2BrowseStockHistory'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseStockHistory_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockHistory.cl(''BrowseStockHistory'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseStockHistory_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseStockHistory_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseStockHistory',p_web.Translate('Date In'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseStockHistory',p_web.Translate('Date Out'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseStockHistory',p_web.Translate('Quantity'),'Click here to sort by Quantity',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseStockHistory',p_web.Translate('Job/Retail'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','BrowseStockHistory',p_web.Translate('In Warr'),'Click here to sort by Purchase Cost',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','BrowseStockHistory',p_web.Translate('Out Warr'),'Click here to sort by Sale Cost',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','BrowseStockHistory',p_web.Translate('User'),'Click here to sort by User',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseStockHistory',p_web.Translate('Status'),'Click here to sort by Notes',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseStockHistory',p_web.Translate('Information'),'Click here to sort by Information',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseStockHistory',p_web.Translate('Stock On Hand'),'Click here to sort by Stock On Hand',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('shi:record_number',lower(loc:vorder),1,1) = 0 !and STOHIST{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','shi:Record_Number',clip(loc:vorder) & ',' & 'shi:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('shi:Record_Number'),p_web.GetValue('shi:Record_Number'),p_web.GetSessionValue('shi:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'shi:Ref_Number = ' & p_web.GSV('sto:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStockHistory_Filter')
    p_web.SetSessionValue('BrowseStockHistory_FirstValue','')
    p_web.SetSessionValue('BrowseStockHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOHIST,shi:record_number_key,loc:PageRows,'BrowseStockHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOHIST{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOHIST,loc:firstvalue)
              Reset(ThisView,STOHIST)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOHIST{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOHIST,loc:lastvalue)
            Reset(ThisView,STOHIST)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      locDateIn = ''
      locDateOut = ''
      locJobRetail = 'Stock'
      
      CASE shi:Transaction_Type
      OF 'ADD' OROF 'REC'
          locDateIn = shi:Date
      
      OF 'DEC'
          locDateOut = shi:Date
      
      
      END
      
      IF (shi:Job_Number > 0)
          locJobRetail = shi:Job_Number
      END
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(shi:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockHistory_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStockHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStockHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseStockHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseStockHistory_LocatorPic'),,,'onchange="BrowseStockHistory.locate(''Locator1BrowseStockHistory'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseStockHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseStockHistory.locate(''Locator1BrowseStockHistory'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseStockHistory_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockHistory.cl(''BrowseStockHistory'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseStockHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStockHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockHistory_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseStockHistory',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseStockHistory','STOHIST',shi:record_number_key) !shi:Record_Number
    p_web._thisrow = p_web._nocolon('shi:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and shi:Record_Number = p_web.GetValue('shi:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStockHistory:LookupField')) = shi:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((shi:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseStockHistory','STOHIST',shi:record_number_key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOHIST{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOHIST)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOHIST{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOHIST)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::locdateIn
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::locDateOut
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'"><13,10>'
          end ! loc:eip = 0
          do value::locJobRetail
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextCenter')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:User
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(150)&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Information
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextCenter')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:StockOnHand
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseStockHistory','STOHIST',shi:record_number_key)
  TableQueue.Id[1] = shi:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseStockHistory;if (btiBrowseStockHistory != 1){{var BrowseStockHistory=new browseTable(''BrowseStockHistory'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('shi:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseStockHistory.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseStockHistory.applyGreenBar();btiBrowseStockHistory=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOHIST)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOHIST)
  Bind(shi:Record)
  Clear(shi:Record)
  NetWebSetSessionPics(p_web,STOHIST)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('shi:Record_Number',p_web.GetValue('shi:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  shi:Record_Number = p_web.GSV('shi:Record_Number')
  loc:result = p_web._GetFile(STOHIST,shi:record_number_key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(shi:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOHIST)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOHIST)
! ----------------------------------------------------------------------------------------
value::locdateIn   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_locdateIn_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locDateIn,'@d06b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locDateOut   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_locDateOut_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locDateOut,'@d06b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Quantity_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Quantity,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locJobRetail   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_locJobRetail_'&shi:Record_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locJobRetail,'@s10')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Purchase_Cost_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Purchase_Cost,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Sale_Cost_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Sale_Cost,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:User   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:User_'&shi:Record_Number,'SmallTextCenter',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:User,'@s3')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Notes   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Notes_'&shi:Record_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Notes,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Information   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Information_'&shi:Record_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Information,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:StockOnHand   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:StockOnHand_'&shi:Record_Number,'SmallTextCenter',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:StockOnHand,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('shi:Record_Number',shi:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SaveSessionVars  ROUTINE
     p_web.SSV('locDateIn',locDateIn) ! DATE
     p_web.SSV('locDateOut',locDateOut) ! DATE
     p_web.SSV('locJobRetail',locJobRetail) ! STRING(30)
RestoreSessionVars  ROUTINE
     locDateIn = p_web.GSV('locDateIn') ! DATE
     locDateOut = p_web.GSV('locDateOut') ! DATE
     locJobRetail = p_web.GSV('locJobRetail') ! STRING(30)
