

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE007.INC'),ONCE        !Local module procedure declarations
                     END


IsUnitLiquidDamaged  PROCEDURE  (String f:ESN, Long f:JobNumber,<Byte ignoreARCCheck>) ! Declare Procedure
tmp:JobDate          DATE                                  !Job Date
  CODE
    Return vod.IsUnitLiquidDamage(f:ESN,f:JobNumber,ignoreARCCheck)
IsOneYearWarranty    PROCEDURE  (STRING manufacturer,STRING modelNumber) ! Declare Procedure
  CODE
    RETURN vod.OneYearWarranty(manufacturer,modelNumber)
CopyDOPFromBouncer   PROCEDURE  (String f:Manufacturer, Date f:PreviousDOP, *Date f:DOP,  String f:IMEINumber, String f:Bouncer, *Byte f:Found) ! Declare Procedure
    include('CopyDOPFromBouncer.inc','Local Data')
  CODE
    include('CopyDOPFromBouncer.inc','Processed Code')
ReleasedForDespatch  PROCEDURE  (f:JobNumber)              ! Declare Procedure
AUDIT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles
    Return# = 0
    Access:AUDIT.Clearkey(aud:Action_Key)
    aud:Ref_Number = f:JobNumber
    aud:Action = 'OUT OF REGION: RELEASED FOR DESPATCH'
    Set(aud:Action_Key,aud:Action_Key)
    Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> f:JobNumber
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
            Break
        End ! If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
        Return# = 1
        Break
    End ! Loop
    do restoreFiles
    do closeFiles

    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  AUDIT::State = Access:AUDIT.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF AUDIT::State <> 0
    Access:AUDIT.RestoreFile(AUDIT::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
JobPaid              PROCEDURE  (fJobNumber)               ! Declare Procedure
tmp:VAT              REAL                                  !
tmp:Total            REAL                                  !
tmp:AmountPaid       REAL                                  !
tmp:Paid             BYTE                                  !
tmp:LabourVatRate    REAL                                  !
tmp:PartsVatRate     REAL                                  !
tmp:Balance          REAL                                  !
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBS_ALIAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openfiles
    do savefiles


    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = fJobNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
    End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign

    tmp:Paid = 0
    tmp:AmountPaid = 0
    Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
    jpt:Ref_Number = job_ali:Ref_Number
    Set(jpt:All_Date_Key,jpt:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT.Next()
            Break
        End ! If Access:JOBPAYMT.Next()
        If jpt:Ref_Number <> job_ali:Ref_Number
            Break
        End ! If jpt:RefNumber <> f:RefNumber
        tmp:AmountPaid += jpt:Amount
    End ! Loop

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job_ali:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job_ali:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    If job_ali:Invoice_Number > 0
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! If job_ali:Invoice_Number > 0

    If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:InvRRCCLabourCost *(inv:Vat_Rate_Labour / 100) + |
                jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts / 100) + |
                job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)

            tmp:Total = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job_ali:Invoice_Courier_Cost + tmp:VAT

        Else ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:RRCCLabourCost *(tmp:LabourVatRate / 100) + |
                jobe:RRCCPartsCost * (tmp:PartsVatRate / 100) + |
                job_ali:Courier_Cost * (tmp:LabourVatRate / 100)

            tmp:Total = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job_ali:Courier_Cost + tmp:VAT

        End ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    Else ! If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100) + |
                job_ali:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) + |
                job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)

            tmp:Total = job_ali:Invoice_Labour_Cost + job_ali:Invoice_Parts_Cost + job_ali:Invoice_Courier_Cost + tmp:VAT

        Else ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Labour_Cost * (tmp:LabourVatRate / 100) + |
                job_ali:Parts_Cost * (tmp:PartsVatRate / 100) + |
                job_ali:Courier_Cost * (tmp:LabourVatRate / 100)

            tmp:Total = job_ali:Labour_Cost + job_ali:Parts_Cost + job_ali:Courier_Cost + tmp:VAT

        End ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    End ! If glo:WebJob

    If tmp:Balance < 0.01
        tmp:Paid = 1
    End ! If tmp:Balance < 0

    do restorefiles
    do closefiles

    return tmp:Paid
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBPAYMT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.Open                                   ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.UseFile                                ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBPAYMT.Close
     Access:JOBSE.Close
     Access:VATCODE.Close
     Access:INVOICE.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:JOBS_ALIAS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  JOBPAYMT::State = Access:JOBPAYMT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBSE::State = Access:JOBSE.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  VATCODE::State = Access:VATCODE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
  INVOICE::State = Access:INVOICE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS_ALIAS::State = Access:JOBS_ALIAS.SaveFile()         ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBPAYMT::State <> 0
    Access:JOBPAYMT.RestoreFile(JOBPAYMT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBSE::State <> 0
    Access:JOBSE.RestoreFile(JOBSE::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF VATCODE::State <> 0
    Access:VATCODE.RestoreFile(VATCODE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF INVOICE::State <> 0
    Access:INVOICE.RestoreFile(INVOICE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS_ALIAS::State <> 0
    Access:JOBS_ALIAS.RestoreFile(JOBS_ALIAS::State)       ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
