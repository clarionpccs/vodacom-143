

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE031.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE005.INC'),ONCE        !Req'd for module callout resolution
                     END


FormChangeDOP        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:DOP              DATE                                  !
tmp:NewDOP           DATE                                  !
tmp:ChangeReason     STRING(255)                           !
tmp:UserPassword     STRING(30)                            !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
JOBS::State  USHORT
tmp:UserPassword:IsInvalid  Long
job:DOP:IsInvalid  Long
tmp:NewDOP:IsInvalid  Long
tmp:ChangeReason:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormChangeDOP')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormChangeDOP_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormChangeDOP','')
    p_web.DivHeader('FormChangeDOP',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormChangeDOP') = 0
        p_web.AddPreCall('FormChangeDOP')
        p_web.DivHeader('popup_FormChangeDOP','nt-hidden')
        p_web.DivHeader('FormChangeDOP',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormChangeDOP_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormChangeDOP_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormChangeDOP',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormChangeDOP')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('Comment:NewDOP','Required')
  p_web.SSV('Comment:UserPassword','Enter Password and press TAB')
  p_web.SSV('tmp:UserPassword','')
  p_web.SSV('Hide:ChangeDOPButton',1)
  p_web.SSV('local:PasswordValidated','')
  p_web.SSV('tmp:NewDOP','')
  p_web.SSV('tmp:ChangeReason','')
  
  if p_web.IfExistsValue('frompage')
      p_web.StoreValue('frompage')
  end ! if p_web.IfExistsValue('frompage')
  p_web.SetValue('FormChangeDOP_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormChangeDOP'
    end
    p_web.formsettings.proc = 'FormChangeDOP'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP','@d06b')
  End
  p_web.SetSessionPicture('job:DOP','@d06b')
  If p_web.IfExistsValue('tmp:NewDOP')
    p_web.SetPicture('tmp:NewDOP','@d06b')
  End
  p_web.SetSessionPicture('tmp:NewDOP','@d06b')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:UserPassword') = 0
    p_web.SetSessionValue('tmp:UserPassword',tmp:UserPassword)
  Else
    tmp:UserPassword = p_web.GetSessionValue('tmp:UserPassword')
  End
  if p_web.IfExistsValue('job:DOP') = 0
    p_web.SetSessionValue('job:DOP',job:DOP)
  Else
    job:DOP = p_web.GetSessionValue('job:DOP')
  End
  if p_web.IfExistsValue('tmp:NewDOP') = 0
    p_web.SetSessionValue('tmp:NewDOP',tmp:NewDOP)
  Else
    tmp:NewDOP = p_web.GetSessionValue('tmp:NewDOP')
  End
  if p_web.IfExistsValue('tmp:ChangeReason') = 0
    p_web.SetSessionValue('tmp:ChangeReason',tmp:ChangeReason)
  Else
    tmp:ChangeReason = p_web.GetSessionValue('tmp:ChangeReason')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:UserPassword')
    tmp:UserPassword = p_web.GetValue('tmp:UserPassword')
    p_web.SetSessionValue('tmp:UserPassword',tmp:UserPassword)
  Else
    tmp:UserPassword = p_web.GetSessionValue('tmp:UserPassword')
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),'@d06b')
    p_web.SetSessionValue('job:DOP',job:DOP)
  Else
    job:DOP = p_web.GetSessionValue('job:DOP')
  End
  if p_web.IfExistsValue('tmp:NewDOP')
    tmp:NewDOP = p_web.dformat(clip(p_web.GetValue('tmp:NewDOP')),p_web.site.DatePicture)
    p_web.SetSessionValue('tmp:NewDOP',tmp:NewDOP)
  Else
    tmp:NewDOP = p_web.GetSessionValue('tmp:NewDOP')
  End
  if p_web.IfExistsValue('tmp:ChangeReason')
    tmp:ChangeReason = p_web.GetValue('tmp:ChangeReason')
    p_web.SetSessionValue('tmp:ChangeReason',tmp:ChangeReason)
  Else
    tmp:ChangeReason = p_web.GetSessionValue('tmp:ChangeReason')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormChangeDOP_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('frompage')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormChangeDOP_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormChangeDOP_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormChangeDOP_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('frompage')

GenerateForm   Routine
  do LoadRelatedRecords
 tmp:UserPassword = p_web.RestoreValue('tmp:UserPassword')
 tmp:NewDOP = p_web.RestoreValue('tmp:NewDOP')
 tmp:ChangeReason = p_web.RestoreValue('tmp:ChangeReason')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Override DOP',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('FormChangeDOP')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-FormChangeDOP"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormChangeDOP',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormChangeDOP0_div')&'">'&p_web.Translate('Confirm User')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormChangeDOP1_div')&'">'&p_web.Translate('Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormChangeDOP_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormChangeDOP_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormChangeDOP_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormChangeDOP_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormChangeDOP_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:UserPassword')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormChangeDOP')>0,p_web.GSV('showtab_FormChangeDOP'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormChangeDOP'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormChangeDOP') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormChangeDOP'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormChangeDOP')>0,p_web.GSV('showtab_FormChangeDOP'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormChangeDOP') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormChangeDOP_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormChangeDOP')>0,p_web.GSV('showtab_FormChangeDOP'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormChangeDOP",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormChangeDOP')>0,p_web.GSV('showtab_FormChangeDOP'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormChangeDOP_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormChangeDOP') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormChangeDOP')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Confirm User')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Confirm User')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Confirm User')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Confirm User')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:UserPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:UserPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:UserPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChangeDOP1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:DOP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:NewDOP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:NewDOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:NewDOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ChangeReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ChangeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ChangeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:UserPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:UserPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:UserPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:UserPassword = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:UserPassword  ! copies value to session value if valid.
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('tmp:UserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level    = use:User_Level
          acc:Access_Area    = 'JOB - DOP OVERRIDE'
          if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
              ! Found
              p_web.SSV('Comment:UserPassword','Password Accepted')
              p_web.SSV('Hide:ChangeDOPButton',0)
              p_web.SSV('local:PasswordValidated',1)
          else ! if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
              ! Error
              p_web.SSV('Comment:UserPassword','User does not have access')
              p_web.SSV('Hide:ChangeDOPButton',1)
              p_web.SSV('tmp:UserPassword','')
              p_web.SSV('local:PasswordValidated',0)
          end ! if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
  
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
          p_web.SSV('Comment:UserPassword','Invalid User')
          p_web.SSV('Hide:ChangeDOPButton',1)
          p_web.SSV('tmp:UserPassword','')
          p_web.SSV('local:PasswordValidated',0)
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  
  do Value::tmp:UserPassword
  do SendAlert
  do Comment::tmp:UserPassword ! allows comment style to be updated.
  do Prompt::tmp:ChangeReason
  do Value::tmp:ChangeReason  !1
  do Comment::tmp:ChangeReason
  do Prompt::tmp:NewDOP
  do Value::tmp:NewDOP  !1
  do Comment::tmp:NewDOP
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Comment::tmp:UserPassword

ValidateValue::tmp:UserPassword  Routine
    If not (1=0)
  If tmp:UserPassword = ''
    loc:Invalid = 'tmp:UserPassword'
    tmp:UserPassword:IsInvalid = true
    loc:alert = 'You do not have access. Click Cancel'
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:UserPassword',tmp:UserPassword).
    End

Value::tmp:UserPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:UserPassword = p_web.RestoreValue('tmp:UserPassword')
    do ValidateValue::tmp:UserPassword
    If tmp:UserPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:UserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:UserPassword'',''formchangedop_tmp:userpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:UserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','tmp:UserPassword',p_web.GetSessionValueFormat('tmp:UserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:UserPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:UserPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:DOP  Routine
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'',p_web.Translate('Previous DOP'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:DOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:DOP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06b'  !FieldPicture = @d6
    job:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b')
  End
  do ValidateValue::job:DOP  ! copies value to session value if valid.
  do Value::job:DOP
  do SendAlert
  do Comment::job:DOP ! allows comment style to be updated.

ValidateValue::job:DOP  Routine
    If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:DOP',job:DOP).
    End

Value::job:DOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:DOP = p_web.RestoreValue('job:DOP')
    do ValidateValue::job:DOP
    If job:DOP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  ! --- STRING --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formchangedop_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:DOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:DOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:NewDOP  Routine
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_prompt',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'',p_web.Translate('New DOP'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:NewDOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:NewDOP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    tmp:NewDOP = p_web.dformat(clip(p_web.GetValue('Value')),'@d06b')
  End
  do ValidateValue::tmp:NewDOP  ! copies value to session value if valid.
  if (p_web.GSV('tmp:NewDOP') > Today() Or p_web.GSV('tmp:NewDOP') = p_web.GSV('job:DOP'))
      p_web.SSV('tmp:NewDOP',0)
      p_web.SSV('Comment:NewDOP','Invalid Date.')
  else ! if (p_web.GSV('tmp:NewDOP') > Today())
      p_web.SSV('Comment:NewDOP','Required')
  end ! if (p_web.GSV('tmp:NewDOP') > Today())
  do Value::tmp:NewDOP
  do SendAlert
  do Comment::tmp:NewDOP ! allows comment style to be updated.
  do Comment::tmp:NewDOP

ValidateValue::tmp:NewDOP  Routine
    If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  If tmp:NewDOP = ''
    loc:Invalid = 'tmp:NewDOP'
    tmp:NewDOP:IsInvalid = true
    loc:alert = p_web.translate('New DOP') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:NewDOP',tmp:NewDOP).
    End

Value::tmp:NewDOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:NewDOP = p_web.RestoreValue('tmp:NewDOP')
    do ValidateValue::tmp:NewDOP
    If tmp:NewDOP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  ! --- DATE --- tmp:NewDOP
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:NewDOP'',''formchangedop_tmp:newdop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:NewDOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'1','')
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('tmp:NewDOP',p_web.GetSessionValue('tmp:NewDOP'),loc:fieldclass,loc:readonly,,'@d06b',loc:javascript,loc:options,loc:extra,,15,,,,0)
  do SendPacket
  !!Handcode Date Lookup Button
  !packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__NewDOP,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
  !do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:NewDOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:NewDOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:NEWDOP'))
  loc:class = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ChangeReason  Routine
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_prompt',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'',p_web.Translate('Change Reason'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ChangeReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ChangeReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ChangeReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ChangeReason  ! copies value to session value if valid.
  do Value::tmp:ChangeReason
  do SendAlert
  do Comment::tmp:ChangeReason ! allows comment style to be updated.

ValidateValue::tmp:ChangeReason  Routine
    If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  If tmp:ChangeReason = ''
    loc:Invalid = 'tmp:ChangeReason'
    tmp:ChangeReason:IsInvalid = true
    loc:alert = p_web.translate('Change Reason') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ChangeReason',tmp:ChangeReason).
    End

Value::tmp:ChangeReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('Hide:ChangeDOPButton') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:ChangeReason = p_web.RestoreValue('tmp:ChangeReason')
    do ValidateValue::tmp:ChangeReason
    If tmp:ChangeReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  ! --- TEXT --- tmp:ChangeReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ChangeReason'',''formchangedop_tmp:changereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'readonly','')
  do SendPacket
  p_web.CreateTextArea('tmp:ChangeReason',p_web.GetSessionValue('tmp:ChangeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:ChangeReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ChangeReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ChangeReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormChangeDOP_nexttab_' & 0)
    tmp:UserPassword = p_web.GSV('tmp:UserPassword')
    do ValidateValue::tmp:UserPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:UserPassword
      !do SendAlert
      do Comment::tmp:UserPassword ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormChangeDOP_nexttab_' & 1)
    job:DOP = p_web.GSV('job:DOP')
    do ValidateValue::job:DOP
    If loc:Invalid
      loc:retrying = 1
      do Value::job:DOP
      !do SendAlert
      do Comment::job:DOP ! allows comment style to be updated.
      !exit
    End
    tmp:NewDOP = p_web.GSV('tmp:NewDOP')
    do ValidateValue::tmp:NewDOP
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:NewDOP
      !do SendAlert
      do Comment::tmp:NewDOP ! allows comment style to be updated.
      !exit
    End
    tmp:ChangeReason = p_web.GSV('tmp:ChangeReason')
    do ValidateValue::tmp:ChangeReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ChangeReason
      !do SendAlert
      do Comment::tmp:ChangeReason ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormChangeDOP_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormChangeDOP_tab_' & 0)
    do GenerateTab0
  of lower('FormChangeDOP_tmp:UserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UserPassword
      of event:timer
        do Value::tmp:UserPassword
        do Comment::tmp:UserPassword
      else
        do Value::tmp:UserPassword
      end
  of lower('FormChangeDOP_tab_' & 1)
    do GenerateTab1
  of lower('FormChangeDOP_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      of event:timer
        do Value::job:DOP
        do Comment::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormChangeDOP_tmp:NewDOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:NewDOP
      of event:timer
        do Value::tmp:NewDOP
        do Comment::tmp:NewDOP
      else
        do Value::tmp:NewDOP
      end
  of lower('FormChangeDOP_tmp:ChangeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ChangeReason
      of event:timer
        do Value::tmp:ChangeReason
        do Comment::tmp:ChangeReason
      else
        do Value::tmp:ChangeReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)

  p_web.SetSessionValue('FormChangeDOP_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormChangeDOP',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormChangeDOP',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormChangeDOP:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormChangeDOP:Primed',0)
  p_web.setsessionvalue('showtab_FormChangeDOP',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('tmp:UserPassword')
            tmp:UserPassword = p_web.GetValue('tmp:UserPassword')
          End
      End
      If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
          If p_web.IfExistsValue('tmp:NewDOP')
            tmp:NewDOP = p_web.GetValue('tmp:NewDOP')
          End
      End
      If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
        If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
          If p_web.IfExistsValue('tmp:ChangeReason')
            tmp:ChangeReason = p_web.GetValue('tmp:ChangeReason')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormChangeDOP_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('local:PasswordValidated') = 0)
      loc:Invalid = 'tmp:UserPassword'
      loc:Alert = 'Your password has not yet been validated'
      p_web.SSV('tmp:UserPassword','')
      exit
  end ! if (p_web.GSV('local:PasswordValidated') = 0)
  p_web.DeleteSessionValue('FormChangeDOP_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::tmp:UserPassword
    If loc:Invalid then exit.
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::job:DOP
    If loc:Invalid then exit.
    do ValidateValue::tmp:NewDOP
    If loc:Invalid then exit.
    do ValidateValue::tmp:ChangeReason
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
      p_web.SSV('tmp:OldDOP',p_web.GSV('job:DOP'))
      p_web.SSV('job:DOP',p_web.GSV('tmp:NewDOP'))
      p_web.SSV('tmp:DateOfPurchase',p_web.GSV('tmp:NewDOP'))
  p_web.SetSessionValue('FormChangeDOP:Primed',0)
  p_web.StoreValue('tmp:UserPassword')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('tmp:NewDOP')
  p_web.StoreValue('tmp:ChangeReason')

OBFValidation        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
Error:OBF            STRING(255)                           !Error OBF
Error:Validation     STRING(255)                           !Error:Generic
tmp:IMEINumber       STRING(30)                            !IMEI Number
tmp:BOXIMEINumber    STRING(30)                            !BOX IMEI Number
tmp:Network          STRING(30)                            !Network
tmp:DOP              DATE                                  !Date Of Purchase
tmp:DateOfPurchase   DATE                                  !
tmp:ReturnDate       DATE                                  !Return Date
tmp:TalkTime         STRING(30)                            !Talk Time
tmp:OriginalDealer   STRING(255)                           !Original Dealer
tmp:BranchOfReturn   STRING(30)                            !Branch Of Return
tmp:StoreReferenceNumber STRING(30)                        !Store Reference Number
tmp:ProofOfPurchase  BYTE(0)                               !Proof Of Purchase
tmp:OriginalPackaging BYTE(0)                              !Original Packaging
tmp:OriginalAccessories BYTE(0)                            !Original Accessories
tmp:OriginalManuals  BYTE(0)                               !Original Manuals
tmp:PhysicalDamage   BYTE(0)                               !Physical Damage
tmp:LAccountNumber   STRING(30)                            !LAccount Number
tmp:ReplacementIMEINumber STRING(30)                       !Replacement I.M.E.I. Number
tmp:Replacement      BYTE(0)                               !Replacement
tmp:TransitType      STRING(30)                            !Transit Type
FilesOpened     Long
NETWORKS::State  USHORT
TRANTYPE::State  USHORT
JOBSE::State  USHORT
tmp:IMEINumber:IsInvalid  Long
tmp:BOXIMEINumber:IsInvalid  Long
tmp:Network:IsInvalid  Long
text_DOPOverride:IsInvalid  Long
text_DOPOverride2:IsInvalid  Long
tmp:DateOfPurchase:IsInvalid  Long
button_ChangeDOP:IsInvalid  Long
tmp:ReturnDate:IsInvalid  Long
tmp:TalkTime:IsInvalid  Long
tmp:OriginalDealer:IsInvalid  Long
tmp:BranchOfReturn:IsInvalid  Long
tmp:StoreReferenceNumber:IsInvalid  Long
tmp:ProofOfPurchase:IsInvalid  Long
tmp:OriginalPackaging:IsInvalid  Long
tmp:OriginalAccessories:IsInvalid  Long
tmp:OriginalManuals:IsInvalid  Long
tmp:PhysicalDamage:IsInvalid  Long
tmp:Replacement:IsInvalid  Long
tmp:LAccountNumber:IsInvalid  Long
tmp:ReplacementIMEINumber:IsInvalid  Long
Button:FailValidation:IsInvalid  Long
Passed:Validation:IsInvalid  Long
Error:Validation:IsInvalid  Long
tmp:TransitType:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('OBFValidation')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'OBFValidation_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('OBFValidation','Insert')
    p_web.DivHeader('OBFValidation',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('OBFValidation') = 0
        p_web.AddPreCall('OBFValidation')
        p_web.DivHeader('popup_OBFValidation','nt-hidden')
        p_web.DivHeader('OBFValidation',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_OBFValidation_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_OBFValidation_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_OBFValidation',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_OBFValidation',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_OBFValidation',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_OBFValidation',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_OBFValidation',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_OBFValidation',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_OBFValidation',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('OBFValidation')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
ValidationFailed        Routine
    p_web.SetSessionValue('OBFValidation','Failed')
    p_web.SetSessionValue('tmp:TransitType','')
    If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SetSessionValue('filter:TransitType','trt:ARC = 1 AND trt:OBF <> 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SetSessionValue('filter:TransitType','trt:RRC = 1 AND trt:OBF <> 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'

ValidateOBF         Routine
Data
local:Error             CString(255)
local:ReturnDate        Date()
local:DOP               Date()
Code
    If p_web.GetSessionValue('OBFValidation') = 'Failed'
        Exit
    End ! If p_web.GetSessionValue('OBFValidation') = 'Failed'


!    local:ReturnDate = Deformat(p_web.GetSessionValue('tmp:ReturnDate'),'@d06')
!    local:DOP = Deformat(p_web.GetSessionValue('tmp:DateOfPurchase'),'@d06')
    local:ReturnDate = p_web.GetSessionValue('tmp:ReturnDate')
    local:DOP = p_web.GetSessionValue('tmp:DateOfPurchase')

    If local:ReturnDate > 0 And local:DOP > 0
        If local:ReturnDate > local:DOP + 7
            p_web.SetSessionValue('Error:Validation','Handset has not returned within 7 days of purchase.')
            Do ValidationFailed
            Exit
        End ! If tmp:ReturnDate > tmp:DateOfPurchase + 7
    End ! If tmp:ReturnDate > 0 And tmp:DateOfPurchase > 0

    tmp:ProofOfPurchase = p_web.GetSessionValue('tmp:ProofOfPurchase')
    If tmp:ProofOfPurchase = 2
        p_web.SetSessionValue('Error:Validation','Handset Proof Of Purchase Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:ProofOfPurchase = 2

    tmp:OriginalPackaging = p_web.GetSessionValue('tmp:OriginalPackaging')
    If tmp:OriginalPackaging = 2
        p_web.SetSessionValue('Error:Validation','Handset Original Packaging Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:OriginalPackaging = 2

    tmp:OriginalAccessories = p_web.GetSessionValue('tmp:OriginalAccessories')
    If tmp:OriginalAccessories = 2!
        p_web.SetSessionValue('Error:Validation','Original Accessories Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:OriginalAccessores = 2

    tmp:OriginalMAnuals = p_web.GetSessionValue('tmp:OriginalManuals')
    If tmp:OriginalManuals = 2
        p_web.SetSessionValue('Error:Validation','Original Manuals Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:OriginalManuals = 2

    tmp:IMEINumber = p_web.GetSessionValue('tmp:IMEINumber')
    tmp:BOXIMEINumber = p_web.GetSessionValue('tmp:BOXIMEINumber')
    If tmp:IMEINumber <> '' And tmp:BOXIMEINumber <> ''
        If tmp:IMEINumber <> tmp:BOXIMEINumber
            p_web.SetSessionValue('Error:Validation','Handset IMEI Number does not match the BOX IMEI Number.')
            Do ValidationFailed
            Exit
        End ! If tmp:IMEINumber <> tmp:BOXIMEINumber
    End ! If tmp:IMEINumber <> '' And tmp:BOXIMEINumber <> ''

    tmp:PhysicalDamage = p_web.GetSessionValue('tmp:PhysicalDamage')
    If tmp:PhysicalDamage = 1
        p_web.SetSessionValue('Error:Validation','Handset has signs of physical damage.')
        Do ValidationFailed
        Exit
    End ! If tmp:PhysicalDamage = 1
!
    tmp:Network = p_web.GetSessionValue('tmp:Network')
    If tmp:Network <> '' And tmp:Network <> '- Select Network -' And tmp:Network <> '082 VODACOM'
        p_web.SetSessionValue('Error:Validation','Handset not supplied by Vodacom Service Provider within the last 12 months.')
        Do ValidationFailed
        Exit
    End ! If tmp:Network <> '' And tmp:Network <> '082 VODACOM'

    tmp:TalkTime = p_web.GetSessionValue('tmp:TalkTime')
    tmp:OriginalDealer = p_web.GetSessionValue('tmp:OriginalDealer')
    tmp:BranchOfReturn = p_web.GetSessionValue('tmp:BranchOfReturn')
    tmp:Replacement = p_web.GetSessionValue('tmp:Replacement')
    tmp:LAccountNumber = p_web.GetSessionValue('tmp:LAccountNumber')
    tmp:ReplacementIMEINumber = p_web.GetSessionValue('tmp:ReplacementIMEINumber')

    If tmp:BOXIMEINumber <> '' And |
        tmp:Network <> '' And |
        tmp:Network <> '- Select Network -' And |
        local:DOP > 0 And |
        local:ReturnDate > 0 And |
        tmp:BranchOfReturn <> '' And |
        tmp:ProofOfPurchase <> 0 And |
        tmp:OriginalAccessories <> 0 And |
        tmp:PhysicalDamage <> 0 And |
        tmp:OriginalPackaging <> 0 And |
        tmp:PhysicalDamage <> 0 And |
        tmp:OriginalManuals <> 0 And |
        (tmp:Replacement = 2 Or (tmp:Replacement = 1 And tmp:LAccountNumber <> '' And tmp:ReplacementIMEINumber <> ''))
        p_web.SetSessionValue('OBFValidation','Passed')
        p_web.SetSessionValue('filter:TransitType','trt:Transit_Type = ''' & p_web.GetSessionValue('save:TransitType') & '''')
        p_web.SetSessionValue('Comment:TransitType','OBF Passed')
    End ! If tmp:BOXIMEINumber <> ''
OpenFiles  ROUTINE
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('OBFValidation_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'OBFValidation'
    end
    p_web.formsettings.proc = 'OBFValidation'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  p_web.SetSessionValue('Hide:TransitType','')

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:DateOfPurchase')
    p_web.SetPicture('tmp:DateOfPurchase','@d06b')
  End
  p_web.SetSessionPicture('tmp:DateOfPurchase','@d06b')
  If p_web.IfExistsValue('tmp:ReturnDate')
    p_web.SetPicture('tmp:ReturnDate','@d06b')
  End
  p_web.SetSessionPicture('tmp:ReturnDate','@d06b')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:Network'
    p_web.setsessionvalue('showtab_OBFValidation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
      p_web.SetSessionValue('tmp:Network',net:Network)
      Do ValidateOBF
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:TransitType'
    p_web.setsessionvalue('showtab_OBFValidation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRANTYPE)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:IMEINumber') = 0
    p_web.SetSessionValue('tmp:IMEINumber',tmp:IMEINumber)
  Else
    tmp:IMEINumber = p_web.GetSessionValue('tmp:IMEINumber')
  End
  if p_web.IfExistsValue('tmp:BOXIMEINumber') = 0
    p_web.SetSessionValue('tmp:BOXIMEINumber',tmp:BOXIMEINumber)
  Else
    tmp:BOXIMEINumber = p_web.GetSessionValue('tmp:BOXIMEINumber')
  End
  if p_web.IfExistsValue('tmp:Network') = 0
    p_web.SetSessionValue('tmp:Network',tmp:Network)
  Else
    tmp:Network = p_web.GetSessionValue('tmp:Network')
  End
  if p_web.IfExistsValue('tmp:DateOfPurchase') = 0
    p_web.SetSessionValue('tmp:DateOfPurchase',tmp:DateOfPurchase)
  Else
    tmp:DateOfPurchase = p_web.GetSessionValue('tmp:DateOfPurchase')
  End
  if p_web.IfExistsValue('tmp:ReturnDate') = 0
    p_web.SetSessionValue('tmp:ReturnDate',tmp:ReturnDate)
  Else
    tmp:ReturnDate = p_web.GetSessionValue('tmp:ReturnDate')
  End
  if p_web.IfExistsValue('tmp:TalkTime') = 0
    p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime)
  Else
    tmp:TalkTime = p_web.GetSessionValue('tmp:TalkTime')
  End
  if p_web.IfExistsValue('tmp:OriginalDealer') = 0
    p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer)
  Else
    tmp:OriginalDealer = p_web.GetSessionValue('tmp:OriginalDealer')
  End
  if p_web.IfExistsValue('tmp:BranchOfReturn') = 0
    p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn)
  Else
    tmp:BranchOfReturn = p_web.GetSessionValue('tmp:BranchOfReturn')
  End
  if p_web.IfExistsValue('tmp:StoreReferenceNumber') = 0
    p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber)
  Else
    tmp:StoreReferenceNumber = p_web.GetSessionValue('tmp:StoreReferenceNumber')
  End
  if p_web.IfExistsValue('tmp:ProofOfPurchase') = 0
    p_web.SetSessionValue('tmp:ProofOfPurchase',tmp:ProofOfPurchase)
  Else
    tmp:ProofOfPurchase = p_web.GetSessionValue('tmp:ProofOfPurchase')
  End
  if p_web.IfExistsValue('tmp:OriginalPackaging') = 0
    p_web.SetSessionValue('tmp:OriginalPackaging',tmp:OriginalPackaging)
  Else
    tmp:OriginalPackaging = p_web.GetSessionValue('tmp:OriginalPackaging')
  End
  if p_web.IfExistsValue('tmp:OriginalAccessories') = 0
    p_web.SetSessionValue('tmp:OriginalAccessories',tmp:OriginalAccessories)
  Else
    tmp:OriginalAccessories = p_web.GetSessionValue('tmp:OriginalAccessories')
  End
  if p_web.IfExistsValue('tmp:OriginalManuals') = 0
    p_web.SetSessionValue('tmp:OriginalManuals',tmp:OriginalManuals)
  Else
    tmp:OriginalManuals = p_web.GetSessionValue('tmp:OriginalManuals')
  End
  if p_web.IfExistsValue('tmp:PhysicalDamage') = 0
    p_web.SetSessionValue('tmp:PhysicalDamage',tmp:PhysicalDamage)
  Else
    tmp:PhysicalDamage = p_web.GetSessionValue('tmp:PhysicalDamage')
  End
  if p_web.IfExistsValue('tmp:Replacement') = 0
    p_web.SetSessionValue('tmp:Replacement',tmp:Replacement)
  Else
    tmp:Replacement = p_web.GetSessionValue('tmp:Replacement')
  End
  if p_web.IfExistsValue('tmp:LAccountNumber') = 0
    p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber)
  Else
    tmp:LAccountNumber = p_web.GetSessionValue('tmp:LAccountNumber')
  End
  if p_web.IfExistsValue('tmp:ReplacementIMEINumber') = 0
    p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber)
  Else
    tmp:ReplacementIMEINumber = p_web.GetSessionValue('tmp:ReplacementIMEINumber')
  End
  if p_web.IfExistsValue('Error:Validation') = 0
    p_web.SetSessionValue('Error:Validation',Error:Validation)
  Else
    Error:Validation = p_web.GetSessionValue('Error:Validation')
  End
  if p_web.IfExistsValue('tmp:TransitType') = 0
    p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  Else
    tmp:TransitType = p_web.GetSessionValue('tmp:TransitType')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:IMEINumber')
    tmp:IMEINumber = p_web.GetValue('tmp:IMEINumber')
    p_web.SetSessionValue('tmp:IMEINumber',tmp:IMEINumber)
  Else
    tmp:IMEINumber = p_web.GetSessionValue('tmp:IMEINumber')
  End
  if p_web.IfExistsValue('tmp:BOXIMEINumber')
    tmp:BOXIMEINumber = p_web.GetValue('tmp:BOXIMEINumber')
    p_web.SetSessionValue('tmp:BOXIMEINumber',tmp:BOXIMEINumber)
  Else
    tmp:BOXIMEINumber = p_web.GetSessionValue('tmp:BOXIMEINumber')
  End
  if p_web.IfExistsValue('tmp:Network')
    tmp:Network = p_web.GetValue('tmp:Network')
    p_web.SetSessionValue('tmp:Network',tmp:Network)
  Else
    tmp:Network = p_web.GetSessionValue('tmp:Network')
  End
  if p_web.IfExistsValue('tmp:DateOfPurchase')
    tmp:DateOfPurchase = p_web.dformat(clip(p_web.GetValue('tmp:DateOfPurchase')),p_web.site.DatePicture)
    p_web.SetSessionValue('tmp:DateOfPurchase',tmp:DateOfPurchase)
  Else
    tmp:DateOfPurchase = p_web.GetSessionValue('tmp:DateOfPurchase')
  End
  if p_web.IfExistsValue('tmp:ReturnDate')
    tmp:ReturnDate = p_web.dformat(clip(p_web.GetValue('tmp:ReturnDate')),p_web.site.DatePicture)
    p_web.SetSessionValue('tmp:ReturnDate',tmp:ReturnDate)
  Else
    tmp:ReturnDate = p_web.GetSessionValue('tmp:ReturnDate')
  End
  if p_web.IfExistsValue('tmp:TalkTime')
    tmp:TalkTime = p_web.GetValue('tmp:TalkTime')
    p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime)
  Else
    tmp:TalkTime = p_web.GetSessionValue('tmp:TalkTime')
  End
  if p_web.IfExistsValue('tmp:OriginalDealer')
    tmp:OriginalDealer = p_web.GetValue('tmp:OriginalDealer')
    p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer)
  Else
    tmp:OriginalDealer = p_web.GetSessionValue('tmp:OriginalDealer')
  End
  if p_web.IfExistsValue('tmp:BranchOfReturn')
    tmp:BranchOfReturn = p_web.GetValue('tmp:BranchOfReturn')
    p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn)
  Else
    tmp:BranchOfReturn = p_web.GetSessionValue('tmp:BranchOfReturn')
  End
  if p_web.IfExistsValue('tmp:StoreReferenceNumber')
    tmp:StoreReferenceNumber = p_web.GetValue('tmp:StoreReferenceNumber')
    p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber)
  Else
    tmp:StoreReferenceNumber = p_web.GetSessionValue('tmp:StoreReferenceNumber')
  End
  if p_web.IfExistsValue('tmp:ProofOfPurchase')
    tmp:ProofOfPurchase = p_web.GetValue('tmp:ProofOfPurchase')
    p_web.SetSessionValue('tmp:ProofOfPurchase',tmp:ProofOfPurchase)
  Else
    tmp:ProofOfPurchase = p_web.GetSessionValue('tmp:ProofOfPurchase')
  End
  if p_web.IfExistsValue('tmp:OriginalPackaging')
    tmp:OriginalPackaging = p_web.GetValue('tmp:OriginalPackaging')
    p_web.SetSessionValue('tmp:OriginalPackaging',tmp:OriginalPackaging)
  Else
    tmp:OriginalPackaging = p_web.GetSessionValue('tmp:OriginalPackaging')
  End
  if p_web.IfExistsValue('tmp:OriginalAccessories')
    tmp:OriginalAccessories = p_web.GetValue('tmp:OriginalAccessories')
    p_web.SetSessionValue('tmp:OriginalAccessories',tmp:OriginalAccessories)
  Else
    tmp:OriginalAccessories = p_web.GetSessionValue('tmp:OriginalAccessories')
  End
  if p_web.IfExistsValue('tmp:OriginalManuals')
    tmp:OriginalManuals = p_web.GetValue('tmp:OriginalManuals')
    p_web.SetSessionValue('tmp:OriginalManuals',tmp:OriginalManuals)
  Else
    tmp:OriginalManuals = p_web.GetSessionValue('tmp:OriginalManuals')
  End
  if p_web.IfExistsValue('tmp:PhysicalDamage')
    tmp:PhysicalDamage = p_web.GetValue('tmp:PhysicalDamage')
    p_web.SetSessionValue('tmp:PhysicalDamage',tmp:PhysicalDamage)
  Else
    tmp:PhysicalDamage = p_web.GetSessionValue('tmp:PhysicalDamage')
  End
  if p_web.IfExistsValue('tmp:Replacement')
    tmp:Replacement = p_web.GetValue('tmp:Replacement')
    p_web.SetSessionValue('tmp:Replacement',tmp:Replacement)
  Else
    tmp:Replacement = p_web.GetSessionValue('tmp:Replacement')
  End
  if p_web.IfExistsValue('tmp:LAccountNumber')
    tmp:LAccountNumber = p_web.GetValue('tmp:LAccountNumber')
    p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber)
  Else
    tmp:LAccountNumber = p_web.GetSessionValue('tmp:LAccountNumber')
  End
  if p_web.IfExistsValue('tmp:ReplacementIMEINumber')
    tmp:ReplacementIMEINumber = p_web.GetValue('tmp:ReplacementIMEINumber')
    p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber)
  Else
    tmp:ReplacementIMEINumber = p_web.GetSessionValue('tmp:ReplacementIMEINumber')
  End
  if p_web.IfExistsValue('Error:Validation')
    Error:Validation = p_web.GetValue('Error:Validation')
    p_web.SetSessionValue('Error:Validation',Error:Validation)
  Else
    Error:Validation = p_web.GetSessionValue('Error:Validation')
  End
  if p_web.IfExistsValue('tmp:TransitType')
    tmp:TransitType = p_web.GetValue('tmp:TransitType')
    p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  Else
    tmp:TransitType = p_web.GetSessionValue('tmp:TransitType')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('OBFValidation_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'NewJobBooking?&Insert_btn=Insert&'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('OBFValidation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('OBFValidation_ChainTo')
    loc:formaction = p_web.GetSessionValue('OBFValidation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'

GenerateForm   Routine
  do LoadRelatedRecords
  ! Prime
  If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
      loc:Invalid = 'job:ESN'
      loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
  End ! If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
  
  p_web.site.SaveButton.TextValue = 'Save'
  p_web.site.CancelButton.TextValue = 'Quit'
  
  p_web.SetSessionValue('tmp:IMEINumber',p_web.GetSessionValue('tmp:ESN'))
 tmp:IMEINumber = p_web.RestoreValue('tmp:IMEINumber')
 tmp:BOXIMEINumber = p_web.RestoreValue('tmp:BOXIMEINumber')
 tmp:Network = p_web.RestoreValue('tmp:Network')
 tmp:DateOfPurchase = p_web.RestoreValue('tmp:DateOfPurchase')
 tmp:ReturnDate = p_web.RestoreValue('tmp:ReturnDate')
 tmp:TalkTime = p_web.RestoreValue('tmp:TalkTime')
 tmp:OriginalDealer = p_web.RestoreValue('tmp:OriginalDealer')
 tmp:BranchOfReturn = p_web.RestoreValue('tmp:BranchOfReturn')
 tmp:StoreReferenceNumber = p_web.RestoreValue('tmp:StoreReferenceNumber')
 tmp:ProofOfPurchase = p_web.RestoreValue('tmp:ProofOfPurchase')
 tmp:OriginalPackaging = p_web.RestoreValue('tmp:OriginalPackaging')
 tmp:OriginalAccessories = p_web.RestoreValue('tmp:OriginalAccessories')
 tmp:OriginalManuals = p_web.RestoreValue('tmp:OriginalManuals')
 tmp:PhysicalDamage = p_web.RestoreValue('tmp:PhysicalDamage')
 tmp:Replacement = p_web.RestoreValue('tmp:Replacement')
 tmp:LAccountNumber = p_web.RestoreValue('tmp:LAccountNumber')
 tmp:ReplacementIMEINumber = p_web.RestoreValue('tmp:ReplacementIMEINumber')
 Error:Validation = p_web.RestoreValue('Error:Validation')
 tmp:TransitType = p_web.RestoreValue('tmp:TransitType')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('OBF Validation') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('OBF Validation',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_OBFValidation',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_OBFValidation0_div')&'">'&p_web.Translate('OBF Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_OBFValidation1_div')&'">'&p_web.Translate('Validation Result')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="OBFValidation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="OBFValidation_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'OBFValidation_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="OBFValidation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'OBFValidation_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:TalkTime')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:BOXIMEINumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_OBFValidation')>0,p_web.GSV('showtab_OBFValidation'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_OBFValidation'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_OBFValidation') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_OBFValidation'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_OBFValidation')>0,p_web.GSV('showtab_OBFValidation'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_OBFValidation') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('OBF Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Validation Result') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_OBFValidation_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_OBFValidation')>0,p_web.GSV('showtab_OBFValidation'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"OBFValidation",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_OBFValidation')>0,p_web.GSV('showtab_OBFValidation'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_OBFValidation_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('OBFValidation') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('SelectNetworks') = 0 then SelectNetworks(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('SelectTransitTypes') = 0 then SelectTransitTypes(p_web).
    p_web.SetValue('_CallPopups',0)
    do AutoLookups
    p_web.AddPreCall('OBFValidation')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('OBF Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_OBFValidation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'OBF Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('OBF Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('OBF Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:IMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:IMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:IMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:BOXIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:BOXIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:BOXIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:Network
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:Network
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:Network
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Hide:ChangeDOP') = 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::text_DOPOverride
        do Comment::text_DOPOverride
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:ChangeDOP') = 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::text_DOPOverride2
        do Comment::text_DOPOverride2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:DateOfPurchase
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:DateOfPurchase
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:DateOfPurchase
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Hide:ChangeDOP') = 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button_ChangeDOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button_ChangeDOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ReturnDate
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ReturnDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ReturnDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:TalkTime
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:TalkTime
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:TalkTime
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OriginalDealer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OriginalDealer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OriginalDealer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:BranchOfReturn
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:BranchOfReturn
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:BranchOfReturn
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:StoreReferenceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:StoreReferenceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:StoreReferenceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ProofOfPurchase
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ProofOfPurchase
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ProofOfPurchase
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OriginalPackaging
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OriginalPackaging
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OriginalPackaging
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OriginalAccessories
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OriginalAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OriginalAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OriginalManuals
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OriginalManuals
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OriginalManuals
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:PhysicalDamage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:PhysicalDamage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:PhysicalDamage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:Replacement
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:Replacement
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:Replacement
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LAccountNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LAccountNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LAccountNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ReplacementIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ReplacementIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ReplacementIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::Button:FailValidation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Button:FailValidation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::Button:FailValidation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Validation Result')&'</a></h3>' & CRLF & p_web.DivHeader('tab_OBFValidation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Validation Result')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Validation Result')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Validation Result')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_OBFValidation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::Passed:Validation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Passed:Validation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::Passed:Validation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::Error:Validation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Error:Validation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::Error:Validation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:TransitType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:TransitType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:TransitType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:IMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Handset I.M.E.I. No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:IMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:IMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:IMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:IMEINumber  ! copies value to session value if valid.
  do Value::tmp:IMEINumber
  do SendAlert
  do Comment::tmp:IMEINumber ! allows comment style to be updated.

ValidateValue::tmp:IMEINumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:IMEINumber',tmp:IMEINumber).
    End

Value::tmp:IMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tmp:IMEINumber = p_web.RestoreValue('tmp:IMEINumber')
    do ValidateValue::tmp:IMEINumber
    If tmp:IMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:IMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:IMEINumber'',''obfvalidation_tmp:imeinumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:IMEINumber',p_web.GetSessionValueFormat('tmp:IMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:IMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:IMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:BOXIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Box I.M.E.I. Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:BOXIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:BOXIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:BOXIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:BOXIMEINumber  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:BOXIMEINumber
  do SendAlert
  do Comment::tmp:BOXIMEINumber ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:BOXIMEINumber  Routine
    If not (1=0)
  If tmp:BOXIMEINumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:BOXIMEINumber'
    tmp:BOXIMEINumber:IsInvalid = true
    loc:alert = p_web.translate('Box I.M.E.I. Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:BOXIMEINumber',tmp:BOXIMEINumber).
    End

Value::tmp:BOXIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:BOXIMEINumber = p_web.RestoreValue('tmp:BOXIMEINumber')
    do ValidateValue::tmp:BOXIMEINumber
    If tmp:BOXIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:BOXIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:BOXIMEINumber'',''obfvalidation_tmp:boximeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:BOXIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:BOXIMEINumber',p_web.GetSessionValueFormat('tmp:BOXIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:BOXIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:BOXIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:Network  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:Network') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Network'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:Network  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetSessionValue('tmp:Network')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:Network = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:Network = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('net:Network')
    tmp:Network = p_web.GetValue('net:Network')
  ElsIf p_web.RequestAjax = 1
    tmp:Network = net:Network
  End
  do ValidateValue::tmp:Network  ! copies value to session value if valid.
  Access:NETWORKS.Clearkey(net:NetworkKey)
  net:Network    = p_web.GSV('tmp:Network')
  if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
      ! Found
      p_web.SSV('comment:Network','')
      Do ValidateOBF
  else ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
      ! Error
      p_web.SSV('tmp:Network','')
      p_web.SSV('comment:Network','Invalid Network')
  end ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
  p_Web.SetValue('lookupfield','tmp:Network')
  do AfterLookup
  do Value::tmp:Network
  do SendAlert
  do Comment::tmp:Network
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:Network  Routine
    If not (1=0)
  If tmp:Network = ''
    loc:Invalid = 'tmp:Network'
    tmp:Network:IsInvalid = true
    loc:alert = p_web.translate('Network') & ' ' & p_web.site.RequiredText
  End
    tmp:Network = Upper(tmp:Network)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:Network',tmp:Network).
    End

Value::tmp:Network  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:Network') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:Network = p_web.RestoreValue('tmp:Network')
    do ValidateValue::tmp:Network
    If tmp:Network:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:Network
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:Network'',''obfvalidation_tmp:network_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Network')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:Network',p_web.GetSessionValue('tmp:Network'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''SelectNetworks'','''&p_web._nocolon('tmp:Network')&''','''&p_web.translate('Select Network')&''',1,'&Net:LookupRecord&',''net:Network'',''OBFValidation'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:Network  Routine
  data
loc:class  string(255)
  code
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetValue('Value')) !1
    loc:lookupdone = 1
  Else
    loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetSessionValue('tmp:Network')) !1
  End
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:Network:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('comment:Network'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:Network') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text_DOPOverride  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text_DOPOverride  ! copies value to session value if valid.
  do Comment::text_DOPOverride ! allows comment style to be updated.

ValidateValue::text_DOPOverride  Routine
  If p_web.GSV('Hide:ChangeDOP') = 0
    If not (1=0)
    End
  End

Value::text_DOPOverride  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text_DOPOverride" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Unit booked in previously.',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text_DOPOverride  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text_DOPOverride:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text_DOPOverride2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text_DOPOverride2  ! copies value to session value if valid.
  do Comment::text_DOPOverride2 ! allows comment style to be updated.

ValidateValue::text_DOPOverride2  Routine
  If p_web.GSV('Hide:ChangeDOP') = 0
    If not (1=0)
    End
  End

Value::text_DOPOverride2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text_DOPOverride2" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('The Date Of Purchase will be filled identically to the previous job. ',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text_DOPOverride2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text_DOPOverride2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:DateOfPurchase  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date Of Purchase'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:DateOfPurchase  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:DateOfPurchase = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    tmp:DateOfPurchase = p_web.dformat(clip(p_web.GetValue('Value')),'@d06b')
  End
  do ValidateValue::tmp:DateOfPurchase  ! copies value to session value if valid.
  If p_web.GSV('tmp:DateOfPurchase') > Today() Or p_web.GSV('tmp:DateOfPurchase') < Deformat('01/01/1980',@d06)
      p_web.SSV('tmp:DateOfPurchase','')
  Else ! If tmp:DateOfPurchase > Today() Or tmp:DateOfPurchase < Deformat('01/01/1980',@d06)
      Do ValidateOBF
  End ! If tmp:DateOfPurchase > Today() Or tmp:DateOfPurchase < Deformat('01/01/1980',@d06)
  
  !If IsDateFormatInvalid(p_web.GetSessionValue('tmp:DateOfPurchase'))
  !    p_web.SetSessionValue('tmp:DateOfPurchase','')
  !Else ! If IsDateFormatInvalid(p_web.GetSessionValue('tmp:DateOfPurchase'))
  !    If Deformat(p_web.GetSessionValue('tmp:DateOfPurchase'),@d06) > Today() Or |
  !        Deformat(p_web.GetSessionValue('tmp:DateOfPurchase'),@d06) < Deformat('01/01/1980',@d06)
  !        p_web.SetSessionValue('tmp:DateOfPurchase','')
  !    End ! Deformat(p_web.GetSessionValue('tmp:DOP'),'@d06') < Deformat('01/01/1980','@d06')
  
  !End ! If IsDateFormatInvalid(p_web.GetSessionValue('tmp:DateOfPurchase'))
  do Value::tmp:DateOfPurchase
  do SendAlert
  do Comment::tmp:DateOfPurchase ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:DateOfPurchase  Routine
    If not (1=0)
  If tmp:DateOfPurchase = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:DateOfPurchase'
    tmp:DateOfPurchase:IsInvalid = true
    loc:alert = p_web.translate('Date Of Purchase') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:DateOfPurchase',tmp:DateOfPurchase).
    End

Value::tmp:DateOfPurchase  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('Hide:ChangeDOP') = 0 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:DateOfPurchase = p_web.RestoreValue('tmp:DateOfPurchase')
    do ValidateValue::tmp:DateOfPurchase
    If tmp:DateOfPurchase:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DATE --- tmp:DateOfPurchase
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:DateOfPurchase'',''obfvalidation_tmp:dateofpurchase_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:DateOfPurchase')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Hide:ChangeDOP') = 0,'1','')
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('tmp:DateOfPurchase',p_web.GetSessionValue('tmp:DateOfPurchase'),loc:fieldclass,loc:readonly,,'@d06b',loc:javascript,loc:options,loc:extra,,15,,,,0)
  do SendPacket
  !!Handcode Date Lookup Button
  !if (p_web.GSV('Hide:ChangeDOP') = 1)
  !    packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__DateOfPurchase,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
  !    do SendPacket
  !end ! if (p_web.GSV('Hide:ChangeDOP') = 0)
  End
  p_web.DivFooter()
Comment::tmp:DateOfPurchase  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:DateOfPurchase:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('dd/mm/yyyy')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button_ChangeDOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button_ChangeDOP  ! copies value to session value if valid.
  do Comment::button_ChangeDOP ! allows comment style to be updated.

ValidateValue::button_ChangeDOP  Routine
  If p_web.GSV('Hide:ChangeDOP') = 0
    If not (1=0)
    End
  End

Value::button_ChangeDOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('button_ChangeDOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Override DOP','Override DOP',p_web.combine(Choose('Override DOP' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.WindowOpen(clip('FormChangeDOP?frompage=' & p_web.PageName)&''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button_ChangeDOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button_ChangeDOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('button_ChangeDOP') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ReturnDate  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Return Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ReturnDate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ReturnDate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    tmp:ReturnDate = p_web.dformat(clip(p_web.GetValue('Value')),'@d06b')
  End
  do ValidateValue::tmp:ReturnDate  ! copies value to session value if valid.
  If p_web.GSV('tmp:ReturnDate') > Today() Or p_web.GSV('tmp:ReturnDate') < Deformat('01/01/1980',@d06) Or p_web.GSV('tmp:ReturnDate') < p_web.GSV('tmp:DateOfPurchase')
      p_web.SSV('tmp:ReturnDate','')
  Else ! If p_web.GSV('tmp:ReturnDate') > Today() Or p_web.GSV('tmp:ReturnDate') < Deformat('01/01/1980',@d06)
      Do ValidateOBF
  End ! If p_web.GSV('tmp:ReturnDate') > Today() Or p_web.GSV('tmp:ReturnDate') < Deformat('01/01/1980',@d06)
  
  !If IsDateFormatInvalid(p_web.GetSessionValue('tmp:ReturnDate'))
  !    p_web.SetSessionValue('tmp:ReturnDate','')
  !Else ! If IsDateFormatInvalid(p_web.GetSessionValue('tmp:ReturnDate'))
  !    If Deformat(p_web.GetSessionValue('tmp:ReturnDate'),@d06) > Today() Or |
  !        Deformat(p_web.GetSessionValue('tmp:ReturnDate'),@d06) < Deformat('01/01/1980',@d06)
  !        p_web.SetSessionValue('tmp:ReturnDate','')
  !    End ! Deformat(p_web.GetSessionValue('tmp:DOP'),'@d06') < Deformat('01/01/1980','@d06')
  
  !End ! If IsDateFormatInvalid(p_web.GetSessionValue('tmp:ReturnDate'))
  do Value::tmp:ReturnDate
  do SendAlert
  do Comment::tmp:ReturnDate ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Value::tmp:BranchOfReturn  !1
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:ReturnDate  Routine
    If not (1=0)
  If tmp:ReturnDate = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:ReturnDate'
    tmp:ReturnDate:IsInvalid = true
    loc:alert = p_web.translate('Return Date') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ReturnDate',tmp:ReturnDate).
    End

Value::tmp:ReturnDate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:ReturnDate = p_web.RestoreValue('tmp:ReturnDate')
    do ValidateValue::tmp:ReturnDate
    If tmp:ReturnDate:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DATE --- tmp:ReturnDate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ReturnDate'',''obfvalidation_tmp:returndate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ReturnDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'1','')
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('tmp:ReturnDate',p_web.GetSessionValue('tmp:ReturnDate'),loc:fieldclass,loc:readonly,,'@d06b',loc:javascript,loc:options,loc:extra,,15,,,,0)
  do SendPacket
  !!Handcode Date Lookup Button
  !packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__ReturnDate,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
  !do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ReturnDate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ReturnDate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:TalkTime  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Talk Time'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:TalkTime  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:TalkTime = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:TalkTime = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:TalkTime  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:TalkTime
  do SendAlert
  do Comment::tmp:TalkTime ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:TalkTime  Routine
    If not (1=0)
  If tmp:TalkTime = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:TalkTime'
    tmp:TalkTime:IsInvalid = true
    loc:alert = p_web.translate('Talk Time') & ' ' & p_web.site.RequiredText
  End
    tmp:TalkTime = Upper(tmp:TalkTime)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime).
    End

Value::tmp:TalkTime  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:TalkTime = p_web.RestoreValue('tmp:TalkTime')
    do ValidateValue::tmp:TalkTime
    If tmp:TalkTime:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:TalkTime
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TalkTime'',''obfvalidation_tmp:talktime_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TalkTime')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:TalkTime',p_web.GetSessionValueFormat('tmp:TalkTime'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:TalkTime  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:TalkTime:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OriginalDealer  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Original Dealer Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OriginalDealer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OriginalDealer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:OriginalDealer = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:OriginalDealer  ! copies value to session value if valid.
      Do ValidateOBF
      if (sub(p_web.GSV('tmp:OriginalDealer'),1,1 ) = ' ')
          p_web.SSV('tmp:OriginalDealer',sub(p_web.GSV('tmp:OriginalDealer'),2,255))
      end !if (sub(p_web.GSV('tmp:OriginalDealer'),1,1 ) = ' ')
  
  do Value::tmp:OriginalDealer
  do SendAlert
  do Comment::tmp:OriginalDealer ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:OriginalDealer  Routine
    If not (1=0)
  If tmp:OriginalDealer = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:OriginalDealer'
    tmp:OriginalDealer:IsInvalid = true
    loc:alert = p_web.translate('Original Dealer Address') & ' ' & p_web.site.RequiredText
  End
    tmp:OriginalDealer = Upper(tmp:OriginalDealer)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer).
    End

Value::tmp:OriginalDealer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:OriginalDealer = p_web.RestoreValue('tmp:OriginalDealer')
    do ValidateValue::tmp:OriginalDealer
    If tmp:OriginalDealer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- tmp:OriginalDealer
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OriginalDealer'',''obfvalidation_tmp:originaldealer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalDealer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('tmp:OriginalDealer',p_web.GetSessionValue('tmp:OriginalDealer'),3,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:OriginalDealer),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OriginalDealer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OriginalDealer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:BranchOfReturn  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Branch Of Return'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:BranchOfReturn  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:BranchOfReturn = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:BranchOfReturn = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:BranchOfReturn  ! copies value to session value if valid.
      Do ValidateOBF
  
  do Value::tmp:BranchOfReturn
  do SendAlert
  do Comment::tmp:BranchOfReturn ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:BranchOfReturn  Routine
    If not (1=0)
  If tmp:BranchOfReturn = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:BranchOfReturn'
    tmp:BranchOfReturn:IsInvalid = true
    loc:alert = p_web.translate('Branch Of Return') & ' ' & p_web.site.RequiredText
  End
    tmp:BranchOfReturn = Upper(tmp:BranchOfReturn)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn).
    End

Value::tmp:BranchOfReturn  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:BranchOfReturn = p_web.RestoreValue('tmp:BranchOfReturn')
    do ValidateValue::tmp:BranchOfReturn
    If tmp:BranchOfReturn:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:BranchOfReturn
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:BranchOfReturn'',''obfvalidation_tmp:branchofreturn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:BranchOfReturn')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:BranchOfReturn',p_web.GetSessionValueFormat('tmp:BranchOfReturn'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:BranchOfReturn  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:BranchOfReturn:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:StoreReferenceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Store Ref Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:StoreReferenceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:StoreReferenceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:StoreReferenceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:StoreReferenceNumber  ! copies value to session value if valid.
      Do ValidateOBF
  
  do Value::tmp:StoreReferenceNumber
  do SendAlert
  do Comment::tmp:StoreReferenceNumber ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:StoreReferenceNumber  Routine
    If not (1=0)
    tmp:StoreReferenceNumber = Upper(tmp:StoreReferenceNumber)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber).
    End

Value::tmp:StoreReferenceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    tmp:StoreReferenceNumber = p_web.RestoreValue('tmp:StoreReferenceNumber')
    do ValidateValue::tmp:StoreReferenceNumber
    If tmp:StoreReferenceNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:StoreReferenceNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StoreReferenceNumber'',''obfvalidation_tmp:storereferencenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StoreReferenceNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:StoreReferenceNumber',p_web.GetSessionValueFormat('tmp:StoreReferenceNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:StoreReferenceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:StoreReferenceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('(CCV, RTV, Repair Order No)')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ProofOfPurchase  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Proof Of Purchase'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ProofOfPurchase  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ProofOfPurchase = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ProofOfPurchase = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ProofOfPurchase  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:ProofOfPurchase
  do SendAlert
  do Comment::tmp:ProofOfPurchase ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:ProofOfPurchase  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ProofOfPurchase',tmp:ProofOfPurchase).
    End

Value::tmp:ProofOfPurchase  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:ProofOfPurchase = p_web.RestoreValue('tmp:ProofOfPurchase')
    do ValidateValue::tmp:ProofOfPurchase
    If tmp:ProofOfPurchase:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:ProofOfPurchase
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ProofOfPurchase') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ProofOfPurchase'',''obfvalidation_tmp:proofofpurchase_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ProofOfPurchase')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ProofOfPurchase',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ProofOfPurchase_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ProofOfPurchase') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ProofOfPurchase'',''obfvalidation_tmp:proofofpurchase_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ProofOfPurchase')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ProofOfPurchase',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ProofOfPurchase_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ProofOfPurchase  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ProofOfPurchase:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OriginalPackaging  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Original Packaging'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OriginalPackaging  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OriginalPackaging = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:OriginalPackaging = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:OriginalPackaging  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:OriginalPackaging
  do SendAlert
  do Comment::tmp:OriginalPackaging ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:OriginalPackaging  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OriginalPackaging',tmp:OriginalPackaging).
    End

Value::tmp:OriginalPackaging  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:OriginalPackaging = p_web.RestoreValue('tmp:OriginalPackaging')
    do ValidateValue::tmp:OriginalPackaging
    If tmp:OriginalPackaging:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:OriginalPackaging
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalPackaging') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalPackaging'',''obfvalidation_tmp:originalpackaging_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalPackaging')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalPackaging',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalPackaging_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalPackaging') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalPackaging'',''obfvalidation_tmp:originalpackaging_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalPackaging')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalPackaging',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalPackaging_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OriginalPackaging  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OriginalPackaging:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OriginalAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Original Accessories'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OriginalAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OriginalAccessories = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:OriginalAccessories = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:OriginalAccessories  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:OriginalAccessories
  do SendAlert
  do Comment::tmp:OriginalAccessories ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:OriginalAccessories  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OriginalAccessories',tmp:OriginalAccessories).
    End

Value::tmp:OriginalAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:OriginalAccessories = p_web.RestoreValue('tmp:OriginalAccessories')
    do ValidateValue::tmp:OriginalAccessories
    If tmp:OriginalAccessories:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:OriginalAccessories
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalAccessories') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalAccessories'',''obfvalidation_tmp:originalaccessories_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalAccessories')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalAccessories',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalAccessories_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalAccessories') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalAccessories'',''obfvalidation_tmp:originalaccessories_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalAccessories')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalAccessories',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalAccessories_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OriginalAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OriginalAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OriginalManuals  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Original Manuals'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OriginalManuals  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OriginalManuals = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:OriginalManuals = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:OriginalManuals  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:OriginalManuals
  do SendAlert
  do Comment::tmp:OriginalManuals ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:OriginalManuals  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OriginalManuals',tmp:OriginalManuals).
    End

Value::tmp:OriginalManuals  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:OriginalManuals = p_web.RestoreValue('tmp:OriginalManuals')
    do ValidateValue::tmp:OriginalManuals
    If tmp:OriginalManuals:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:OriginalManuals
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalManuals') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalManuals'',''obfvalidation_tmp:originalmanuals_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalManuals')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalManuals',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalManuals_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalManuals') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalManuals'',''obfvalidation_tmp:originalmanuals_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalManuals')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalManuals',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalManuals_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OriginalManuals  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OriginalManuals:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:PhysicalDamage  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Physical Damage'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:PhysicalDamage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:PhysicalDamage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:PhysicalDamage = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:PhysicalDamage  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:PhysicalDamage
  do SendAlert
  do Comment::tmp:PhysicalDamage ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:PhysicalDamage  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:PhysicalDamage',tmp:PhysicalDamage).
    End

Value::tmp:PhysicalDamage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:PhysicalDamage = p_web.RestoreValue('tmp:PhysicalDamage')
    do ValidateValue::tmp:PhysicalDamage
    If tmp:PhysicalDamage:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:PhysicalDamage
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:PhysicalDamage') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:PhysicalDamage'',''obfvalidation_tmp:physicaldamage_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:PhysicalDamage')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:PhysicalDamage',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:PhysicalDamage_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:PhysicalDamage') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:PhysicalDamage'',''obfvalidation_tmp:physicaldamage_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:PhysicalDamage')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:PhysicalDamage',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:PhysicalDamage_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:PhysicalDamage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:PhysicalDamage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:Replacement  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Replacement'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:Replacement  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:Replacement = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:Replacement = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:Replacement  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:Replacement
  do SendAlert
  do Comment::tmp:Replacement ! allows comment style to be updated.
  do Prompt::tmp:LAccountNumber
  do Value::tmp:LAccountNumber  !1
  do Prompt::tmp:ReplacementIMEINumber
  do Value::tmp:ReplacementIMEINumber  !1
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:Replacement  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:Replacement',tmp:Replacement).
    End

Value::tmp:Replacement  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:Replacement = p_web.RestoreValue('tmp:Replacement')
    do ValidateValue::tmp:Replacement
    If tmp:Replacement:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:Replacement
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:Replacement') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:Replacement'',''obfvalidation_tmp:replacement_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Replacement')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:Replacement',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:Replacement_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:Replacement') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:Replacement'',''obfvalidation_tmp:replacement_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Replacement')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:Replacement',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:Replacement_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:Replacement  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:Replacement:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LAccountNumber  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_prompt',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'',p_web.Translate('L/Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LAccountNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LAccountNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LAccountNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LAccountNumber  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:LAccountNumber
  do SendAlert
  do Comment::tmp:LAccountNumber ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:LAccountNumber  Routine
    If not (p_web.GetSessionValue('tmp:Replacement') <> 1)
  If tmp:LAccountNumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:LAccountNumber'
    tmp:LAccountNumber:IsInvalid = true
    loc:alert = p_web.translate('L/Account Number') & ' ' & p_web.site.RequiredText
  End
    tmp:LAccountNumber = Upper(tmp:LAccountNumber)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber).
    End

Value::tmp:LAccountNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:LAccountNumber = p_web.RestoreValue('tmp:LAccountNumber')
    do ValidateValue::tmp:LAccountNumber
    If tmp:LAccountNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:Replacement') <> 1)
  ! --- STRING --- tmp:LAccountNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LAccountNumber'',''obfvalidation_tmp:laccountnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LAccountNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:LAccountNumber',p_web.GetSessionValueFormat('tmp:LAccountNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LAccountNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LAccountNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GetSessionValue('tmp:Replacement') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ReplacementIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_prompt',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'',p_web.Translate('Replacement IMEI No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ReplacementIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ReplacementIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ReplacementIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ReplacementIMEINumber  ! copies value to session value if valid.
      Do ValidateOBF
  do Value::tmp:ReplacementIMEINumber
  do SendAlert
  do Comment::tmp:ReplacementIMEINumber ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::tmp:ReplacementIMEINumber  Routine
    If not (p_web.GetSessionValue('tmp:Replacement') <> 1)
  If tmp:ReplacementIMEINumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:ReplacementIMEINumber'
    tmp:ReplacementIMEINumber:IsInvalid = true
    loc:alert = p_web.translate('Replacement IMEI No') & ' ' & p_web.site.RequiredText
  End
    tmp:ReplacementIMEINumber = Upper(tmp:ReplacementIMEINumber)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber).
    End

Value::tmp:ReplacementIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:ReplacementIMEINumber = p_web.RestoreValue('tmp:ReplacementIMEINumber')
    do ValidateValue::tmp:ReplacementIMEINumber
    If tmp:ReplacementIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:Replacement') <> 1)
  ! --- STRING --- tmp:ReplacementIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ReplacementIMEINumber'',''obfvalidation_tmp:replacementimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ReplacementIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ReplacementIMEINumber',p_web.GetSessionValueFormat('tmp:ReplacementIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ReplacementIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ReplacementIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GetSessionValue('tmp:Replacement') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::Button:FailValidation  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_prompt',Choose(p_web.GetSessionValue('OBFValidation') = 'Passed','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('OBFValidation') = 'Passed','',p_web.Translate('Fail Validation'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::Button:FailValidation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Button:FailValidation  ! copies value to session value if valid.
  p_web.SetSessionValue('Error:Validation','Validation Manually Failed')
  Do ValidationFailed
  do Value::Button:FailValidation
  do Comment::Button:FailValidation ! allows comment style to be updated.
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

ValidateValue::Button:FailValidation  Routine
    If not (p_web.GetSessionValue('OBFValidation') = 'Passed')
    End

Value::Button:FailValidation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('OBFValidation') = 'Passed','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GetSessionValue('OBFValidation') = 'Passed')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('ForceValidationFail')&'.disabled=true;sv(''Button:FailValidation'',''obfvalidation_button:failvalidation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ForceValidationFail','Force Validation Fail',p_web.combine(Choose('Force Validation Fail' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'images/pcancel.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::Button:FailValidation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if Button:FailValidation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GetSessionValue('OBFValidation') = 'Passed','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_comment',loc:class,Net:NoSend)
  If p_web.GetSessionValue('OBFValidation') = 'Passed'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::Passed:Validation  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_prompt',Choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::Passed:Validation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Passed:Validation  ! copies value to session value if valid.
  do Comment::Passed:Validation ! allows comment style to be updated.

ValidateValue::Passed:Validation  Routine
    If not (p_web.GetSessionValue('OBFValidation') <> 'Passed')
    End

Value::Passed:Validation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GetSessionValue('OBFValidation') <> 'Passed')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="Passed:Validation" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate('Passed Validation',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::Passed:Validation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if Passed:Validation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_comment',loc:class,Net:NoSend)
  If p_web.GetSessionValue('OBFValidation') <> 'Passed'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::Error:Validation  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('Error:Validation') & '_prompt',Choose(p_web.GetSessionValue('Error:Validation') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,'RedRegular')),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Error:Validation') = '','',p_web.Translate('Validation Failed'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::Error:Validation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    Error:Validation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    Error:Validation = p_web.GetValue('Value')
  End
  do ValidateValue::Error:Validation  ! copies value to session value if valid.
  do Value::Error:Validation
  do SendAlert
  do Comment::Error:Validation ! allows comment style to be updated.

ValidateValue::Error:Validation  Routine
    If not (p_web.GetSessionValue('Error:Validation') = '')
      if loc:invalid = '' then p_web.SetSessionValue('Error:Validation',Error:Validation).
    End

Value::Error:Validation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Error:Validation') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('Error:Validation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,'RedRegular')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    Error:Validation = p_web.RestoreValue('Error:Validation')
    do ValidateValue::Error:Validation
    If Error:Validation:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Error:Validation') = '')
  ! --- TEXT --- Error:Validation
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''Error:Validation'',''obfvalidation_error:validation_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('Error:Validation',p_web.GetSessionValue('Error:Validation'),3,60,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(Error:Validation),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::Error:Validation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if Error:Validation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GetSessionValue('Error:Validation') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('Error:Validation') & '_comment',loc:class,Net:NoSend)
  If p_web.GetSessionValue('Error:Validation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:TransitType  Routine
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_prompt',Choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','nt-hidden',p_web.combine(p_web.site.style.formprompt,,'RedRegular')),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','',p_web.Translate('Select Transit Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:TransitType  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(TRANTYPE,trt:Transit_Type_Key,trt:Transit_Type_Key,trt:Transit_Type,trt:Transit_Type,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(TRANTYPE,trt:Transit_Type_Key,trt:Transit_Type_Key,trt:Transit_Type,trt:Transit_Type,p_web.GetSessionValue('tmp:TransitType')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:TransitType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:TransitType = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('trt:Transit_Type')
    tmp:TransitType = p_web.GetValue('trt:Transit_Type')
  ElsIf p_web.RequestAjax = 1
    tmp:TransitType = trt:Transit_Type
  End
  do ValidateValue::tmp:TransitType  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','tmp:TransitType')
  do AfterLookup
  do Value::tmp:TransitType
  do SendAlert
  do Comment::tmp:TransitType

ValidateValue::tmp:TransitType  Routine
    If not (p_web.GetSessionValue('OBFValidation') <> 'Failed')
  If tmp:TransitType = ''
    loc:Invalid = 'tmp:TransitType'
    tmp:TransitType:IsInvalid = true
    loc:alert = p_web.translate('Select Transit Type') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:TransitType',tmp:TransitType).
    End

Value::tmp:TransitType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:TransitType = p_web.RestoreValue('tmp:TransitType')
    do ValidateValue::tmp:TransitType
    If tmp:TransitType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('OBFValidation') <> 'Failed')
  ! --- STRING --- tmp:TransitType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TransitType'',''obfvalidation_tmp:transittype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TransitType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:TransitType',p_web.GetSessionValue('tmp:TransitType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''SelectTransitTypes'','''&p_web._nocolon('tmp:TransitType')&''','''&p_web.translate('Select Transit Type')&''',1,'&Net:LookupRecord&','''',''OBFValidation'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:TransitType  Routine
  data
loc:class  string(255)
  code
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    loc:ok = p_web.GetDescription(TRANTYPE,trt:Transit_Type_Key,trt:Transit_Type_Key,trt:Transit_Type,trt:Transit_Type,p_web.GetValue('Value')) !1
    loc:lookupdone = 1
  Else
    loc:ok = p_web.GetDescription(TRANTYPE,trt:Transit_Type_Key,trt:Transit_Type_Key,trt:Transit_Type,trt:Transit_Type,p_web.GetSessionValue('tmp:TransitType')) !1
  End
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:TransitType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_comment',loc:class,Net:NoSend)
  If p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('OBFValidation_nexttab_' & 0)
    tmp:IMEINumber = p_web.GSV('tmp:IMEINumber')
    do ValidateValue::tmp:IMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:IMEINumber
      !do SendAlert
      do Comment::tmp:IMEINumber ! allows comment style to be updated.
      !exit
    End
    tmp:BOXIMEINumber = p_web.GSV('tmp:BOXIMEINumber')
    do ValidateValue::tmp:BOXIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:BOXIMEINumber
      !do SendAlert
      do Comment::tmp:BOXIMEINumber ! allows comment style to be updated.
      !exit
    End
    tmp:Network = p_web.GSV('tmp:Network')
    do ValidateValue::tmp:Network
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:Network
      !do SendAlert
      do Comment::tmp:Network ! allows comment style to be updated.
      !exit
    End
    tmp:DateOfPurchase = p_web.GSV('tmp:DateOfPurchase')
    do ValidateValue::tmp:DateOfPurchase
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:DateOfPurchase
      !do SendAlert
      do Comment::tmp:DateOfPurchase ! allows comment style to be updated.
      !exit
    End
    tmp:ReturnDate = p_web.GSV('tmp:ReturnDate')
    do ValidateValue::tmp:ReturnDate
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ReturnDate
      !do SendAlert
      do Comment::tmp:ReturnDate ! allows comment style to be updated.
      !exit
    End
    tmp:TalkTime = p_web.GSV('tmp:TalkTime')
    do ValidateValue::tmp:TalkTime
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:TalkTime
      !do SendAlert
      do Comment::tmp:TalkTime ! allows comment style to be updated.
      !exit
    End
    tmp:OriginalDealer = p_web.GSV('tmp:OriginalDealer')
    do ValidateValue::tmp:OriginalDealer
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OriginalDealer
      !do SendAlert
      do Comment::tmp:OriginalDealer ! allows comment style to be updated.
      !exit
    End
    tmp:BranchOfReturn = p_web.GSV('tmp:BranchOfReturn')
    do ValidateValue::tmp:BranchOfReturn
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:BranchOfReturn
      !do SendAlert
      do Comment::tmp:BranchOfReturn ! allows comment style to be updated.
      !exit
    End
    tmp:StoreReferenceNumber = p_web.GSV('tmp:StoreReferenceNumber')
    do ValidateValue::tmp:StoreReferenceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:StoreReferenceNumber
      !do SendAlert
      do Comment::tmp:StoreReferenceNumber ! allows comment style to be updated.
      !exit
    End
    tmp:ProofOfPurchase = p_web.GSV('tmp:ProofOfPurchase')
    do ValidateValue::tmp:ProofOfPurchase
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ProofOfPurchase
      !do SendAlert
      do Comment::tmp:ProofOfPurchase ! allows comment style to be updated.
      !exit
    End
    tmp:OriginalPackaging = p_web.GSV('tmp:OriginalPackaging')
    do ValidateValue::tmp:OriginalPackaging
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OriginalPackaging
      !do SendAlert
      do Comment::tmp:OriginalPackaging ! allows comment style to be updated.
      !exit
    End
    tmp:OriginalAccessories = p_web.GSV('tmp:OriginalAccessories')
    do ValidateValue::tmp:OriginalAccessories
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OriginalAccessories
      !do SendAlert
      do Comment::tmp:OriginalAccessories ! allows comment style to be updated.
      !exit
    End
    tmp:OriginalManuals = p_web.GSV('tmp:OriginalManuals')
    do ValidateValue::tmp:OriginalManuals
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OriginalManuals
      !do SendAlert
      do Comment::tmp:OriginalManuals ! allows comment style to be updated.
      !exit
    End
    tmp:PhysicalDamage = p_web.GSV('tmp:PhysicalDamage')
    do ValidateValue::tmp:PhysicalDamage
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:PhysicalDamage
      !do SendAlert
      do Comment::tmp:PhysicalDamage ! allows comment style to be updated.
      !exit
    End
    tmp:Replacement = p_web.GSV('tmp:Replacement')
    do ValidateValue::tmp:Replacement
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:Replacement
      !do SendAlert
      do Comment::tmp:Replacement ! allows comment style to be updated.
      !exit
    End
    tmp:LAccountNumber = p_web.GSV('tmp:LAccountNumber')
    do ValidateValue::tmp:LAccountNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LAccountNumber
      !do SendAlert
      do Comment::tmp:LAccountNumber ! allows comment style to be updated.
      !exit
    End
    tmp:ReplacementIMEINumber = p_web.GSV('tmp:ReplacementIMEINumber')
    do ValidateValue::tmp:ReplacementIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ReplacementIMEINumber
      !do SendAlert
      do Comment::tmp:ReplacementIMEINumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('OBFValidation_nexttab_' & 1)
    Error:Validation = p_web.GSV('Error:Validation')
    do ValidateValue::Error:Validation
    If loc:Invalid
      loc:retrying = 1
      do Value::Error:Validation
      !do SendAlert
      do Comment::Error:Validation ! allows comment style to be updated.
      !exit
    End
    tmp:TransitType = p_web.GSV('tmp:TransitType')
    do ValidateValue::tmp:TransitType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:TransitType
      !do SendAlert
      do Comment::tmp:TransitType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_OBFValidation_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('OBFValidation_tab_' & 0)
    do GenerateTab0
  of lower('OBFValidation_tmp:IMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:IMEINumber
      of event:timer
        do Value::tmp:IMEINumber
        do Comment::tmp:IMEINumber
      else
        do Value::tmp:IMEINumber
      end
  of lower('OBFValidation_tmp:BOXIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:BOXIMEINumber
      of event:timer
        do Value::tmp:BOXIMEINumber
        do Comment::tmp:BOXIMEINumber
      else
        do Value::tmp:BOXIMEINumber
      end
  of lower('OBFValidation_tmp:Network_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:Network
      of event:timer
        do Value::tmp:Network
        do Comment::tmp:Network
      else
        do Value::tmp:Network
      end
  of lower('OBFValidation_tmp:DateOfPurchase_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:DateOfPurchase
      of event:timer
        do Value::tmp:DateOfPurchase
        do Comment::tmp:DateOfPurchase
      else
        do Value::tmp:DateOfPurchase
      end
  of lower('OBFValidation_tmp:ReturnDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ReturnDate
      of event:timer
        do Value::tmp:ReturnDate
        do Comment::tmp:ReturnDate
      else
        do Value::tmp:ReturnDate
      end
  of lower('OBFValidation_tmp:TalkTime_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TalkTime
      of event:timer
        do Value::tmp:TalkTime
        do Comment::tmp:TalkTime
      else
        do Value::tmp:TalkTime
      end
  of lower('OBFValidation_tmp:OriginalDealer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalDealer
      of event:timer
        do Value::tmp:OriginalDealer
        do Comment::tmp:OriginalDealer
      else
        do Value::tmp:OriginalDealer
      end
  of lower('OBFValidation_tmp:BranchOfReturn_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:BranchOfReturn
      of event:timer
        do Value::tmp:BranchOfReturn
        do Comment::tmp:BranchOfReturn
      else
        do Value::tmp:BranchOfReturn
      end
  of lower('OBFValidation_tmp:StoreReferenceNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StoreReferenceNumber
      of event:timer
        do Value::tmp:StoreReferenceNumber
        do Comment::tmp:StoreReferenceNumber
      else
        do Value::tmp:StoreReferenceNumber
      end
  of lower('OBFValidation_tmp:ProofOfPurchase_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ProofOfPurchase
      of event:timer
        do Value::tmp:ProofOfPurchase
        do Comment::tmp:ProofOfPurchase
      else
        do Value::tmp:ProofOfPurchase
      end
  of lower('OBFValidation_tmp:OriginalPackaging_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalPackaging
      of event:timer
        do Value::tmp:OriginalPackaging
        do Comment::tmp:OriginalPackaging
      else
        do Value::tmp:OriginalPackaging
      end
  of lower('OBFValidation_tmp:OriginalAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalAccessories
      of event:timer
        do Value::tmp:OriginalAccessories
        do Comment::tmp:OriginalAccessories
      else
        do Value::tmp:OriginalAccessories
      end
  of lower('OBFValidation_tmp:OriginalManuals_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalManuals
      of event:timer
        do Value::tmp:OriginalManuals
        do Comment::tmp:OriginalManuals
      else
        do Value::tmp:OriginalManuals
      end
  of lower('OBFValidation_tmp:PhysicalDamage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PhysicalDamage
      of event:timer
        do Value::tmp:PhysicalDamage
        do Comment::tmp:PhysicalDamage
      else
        do Value::tmp:PhysicalDamage
      end
  of lower('OBFValidation_tmp:Replacement_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:Replacement
      of event:timer
        do Value::tmp:Replacement
        do Comment::tmp:Replacement
      else
        do Value::tmp:Replacement
      end
  of lower('OBFValidation_tmp:LAccountNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LAccountNumber
      of event:timer
        do Value::tmp:LAccountNumber
        do Comment::tmp:LAccountNumber
      else
        do Value::tmp:LAccountNumber
      end
  of lower('OBFValidation_tmp:ReplacementIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ReplacementIMEINumber
      of event:timer
        do Value::tmp:ReplacementIMEINumber
        do Comment::tmp:ReplacementIMEINumber
      else
        do Value::tmp:ReplacementIMEINumber
      end
  of lower('OBFValidation_Button:FailValidation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:FailValidation
      of event:timer
        do Value::Button:FailValidation
        do Comment::Button:FailValidation
      else
        do Value::Button:FailValidation
      end
  of lower('OBFValidation_tab_' & 1)
    do GenerateTab1
  of lower('OBFValidation_Error:Validation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Error:Validation
      of event:timer
        do Value::Error:Validation
        do Comment::Error:Validation
      else
        do Value::Error:Validation
      end
  of lower('OBFValidation_tmp:TransitType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TransitType
      of event:timer
        do Value::tmp:TransitType
        do Comment::tmp:TransitType
      else
        do Value::tmp:TransitType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('OBFValidation_form:ready_',1)

  p_web.SetSessionValue('OBFValidation_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_OBFValidation',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_OBFValidation',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('OBFValidation:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('OBFValidation:Primed',0)
  p_web.setsessionvalue('showtab_OBFValidation',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,,p_web.GetSessionValue('tmp:Network')) !2
  if loc:ok then p_web.FileToSessionQueue(NETWORKS).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(TRANTYPE,trt:Transit_Type_Key,trt:Transit_Type_Key,trt:Transit_Type,,p_web.GetSessionValue('tmp:TransitType')) !2
  if loc:ok then p_web.FileToSessionQueue(TRANTYPE).
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('tmp:BOXIMEINumber')
            tmp:BOXIMEINumber = p_web.GetValue('tmp:BOXIMEINumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:Network')
            tmp:Network = p_web.GetValue('tmp:Network')
          End
      End
      If not (1=0)
        If not (p_web.GSV('Hide:ChangeDOP') = 0)
          If p_web.IfExistsValue('tmp:DateOfPurchase')
            tmp:DateOfPurchase = p_web.GetValue('tmp:DateOfPurchase')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:ReturnDate')
            tmp:ReturnDate = p_web.GetValue('tmp:ReturnDate')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:TalkTime')
            tmp:TalkTime = p_web.GetValue('tmp:TalkTime')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:OriginalDealer')
            tmp:OriginalDealer = p_web.GetValue('tmp:OriginalDealer')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:BranchOfReturn')
            tmp:BranchOfReturn = p_web.GetValue('tmp:BranchOfReturn')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:StoreReferenceNumber')
            tmp:StoreReferenceNumber = p_web.GetValue('tmp:StoreReferenceNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:ProofOfPurchase')
            tmp:ProofOfPurchase = p_web.GetValue('tmp:ProofOfPurchase')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:OriginalPackaging')
            tmp:OriginalPackaging = p_web.GetValue('tmp:OriginalPackaging')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:OriginalAccessories')
            tmp:OriginalAccessories = p_web.GetValue('tmp:OriginalAccessories')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:OriginalManuals')
            tmp:OriginalManuals = p_web.GetValue('tmp:OriginalManuals')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:PhysicalDamage')
            tmp:PhysicalDamage = p_web.GetValue('tmp:PhysicalDamage')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:Replacement')
            tmp:Replacement = p_web.GetValue('tmp:Replacement')
          End
      End
      If not (p_web.GetSessionValue('tmp:Replacement') <> 1)
          If p_web.IfExistsValue('tmp:LAccountNumber')
            tmp:LAccountNumber = p_web.GetValue('tmp:LAccountNumber')
          End
      End
      If not (p_web.GetSessionValue('tmp:Replacement') <> 1)
          If p_web.IfExistsValue('tmp:ReplacementIMEINumber')
            tmp:ReplacementIMEINumber = p_web.GetValue('tmp:ReplacementIMEINumber')
          End
      End
      If not (p_web.GetSessionValue('OBFValidation') <> 'Failed')
          If p_web.IfExistsValue('tmp:TransitType')
            tmp:TransitType = p_web.GetValue('tmp:TransitType')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
  !Field Validation
      If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
          loc:Invalid = 'job:ESN'
          loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
          Exit
      End ! If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
  
      If p_web.GetSessionValue('OBFValidation') = 'Pending'
          loc:Invalid = 'tmp:IMEINumber'
          loc:Alert = 'You must complete all of the fields, or click "Force Validation Fail".'
          Exit
      Elsif p_web.GetSessionValue('OBFValidation') = 'Failed'
          If p_web.GetSessionValue('tmp:TransitType') = ''
              loc:Invalid = 'tmp:TransitType'
              loc:Alert = 'You must select a new Transit Type.'
              Exit
          End ! If p_web.GetSessionValue('OBFValidation') = 'Failed' And p_web.GetSessionValue('tmp:TransitType') = ''
      Else
          If p_web.GetSessionValue('tmp:Replacement') = 1
              If Sub(p_web.GetSessionValue('tmp:LAccountNumber'),1,1) <> 'L'
                  loc:Invalid = 'tmp:LAccountNumber'
                  loc:Alert = 'L/Account Number must begin with the letter "L".'
                  Exit
              End ! If Sub(p_web.GetSessionValue('tmp:LAccountNumber'),1,1) <> 'L'
          End ! If p_web.GetSessionValue('tmp:Replacement')
      End ! If p_web.GetSessionValue('OBFValidation') <> 'Failed'
  
  ! Inserting (DBH 06/03/2008) # 9836 - Automatically add accessories
      If p_web.GetSessionValue('OBFValidation') = 'Passed'
          p_web.SetSessionValue('tmp:TheJobAccessory','|;ORIGINAL PACKAGING;|;MANUALS')
      End ! If p_web.GetSessionValue('OBFValidation') = 'Passed'
  ! End (DBH 06/03/2008) #9836
  
      ! Force The Transit Type (DBH: 27/03/2008)
      p_web.SetSessionValue('Filter:TransitType','trt:Transit_Type = <39>' & p_web.GetSessionValue('job:Transit_Type') & '<39>')
      p_web.SetSessionValue('Comment:TransitType','OBF Validation: ' & p_web.GetSessionValue('OBFValidation'))
      p_web.SetSessionValue('ReadOnly:IMEINumber',1)
  
      p_web.SetSessionValue('ReadyForNewJobBooking',3)
  
      p_web.SSV('tmp:DOP',p_web.GSV('tmp:DateOfPurchase'))

ValidateDelete  Routine
  p_web.DeleteSessionValue('OBFValidation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('OBFValidation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:IMEINumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:BOXIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:Network
    If loc:Invalid then exit.
    do ValidateValue::text_DOPOverride
    If loc:Invalid then exit.
    do ValidateValue::text_DOPOverride2
    If loc:Invalid then exit.
    do ValidateValue::tmp:DateOfPurchase
    If loc:Invalid then exit.
    do ValidateValue::button_ChangeDOP
    If loc:Invalid then exit.
    do ValidateValue::tmp:ReturnDate
    If loc:Invalid then exit.
    do ValidateValue::tmp:TalkTime
    If loc:Invalid then exit.
    do ValidateValue::tmp:OriginalDealer
    If loc:Invalid then exit.
    do ValidateValue::tmp:BranchOfReturn
    If loc:Invalid then exit.
    do ValidateValue::tmp:StoreReferenceNumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:ProofOfPurchase
    If loc:Invalid then exit.
    do ValidateValue::tmp:OriginalPackaging
    If loc:Invalid then exit.
    do ValidateValue::tmp:OriginalAccessories
    If loc:Invalid then exit.
    do ValidateValue::tmp:OriginalManuals
    If loc:Invalid then exit.
    do ValidateValue::tmp:PhysicalDamage
    If loc:Invalid then exit.
    do ValidateValue::tmp:Replacement
    If loc:Invalid then exit.
    do ValidateValue::tmp:LAccountNumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:ReplacementIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::Button:FailValidation
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::Passed:Validation
    If loc:Invalid then exit.
    do ValidateValue::Error:Validation
    If loc:Invalid then exit.
    do ValidateValue::tmp:TransitType
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('OBFValidation:Primed',0)
  p_web.StoreValue('tmp:IMEINumber')
  p_web.StoreValue('tmp:BOXIMEINumber')
  p_web.StoreValue('tmp:Network')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:DateOfPurchase')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ReturnDate')
  p_web.StoreValue('tmp:TalkTime')
  p_web.StoreValue('tmp:OriginalDealer')
  p_web.StoreValue('tmp:BranchOfReturn')
  p_web.StoreValue('tmp:StoreReferenceNumber')
  p_web.StoreValue('tmp:ProofOfPurchase')
  p_web.StoreValue('tmp:OriginalPackaging')
  p_web.StoreValue('tmp:OriginalAccessories')
  p_web.StoreValue('tmp:OriginalManuals')
  p_web.StoreValue('tmp:PhysicalDamage')
  p_web.StoreValue('tmp:Replacement')
  p_web.StoreValue('tmp:LAccountNumber')
  p_web.StoreValue('tmp:ReplacementIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('Error:Validation')
  p_web.StoreValue('tmp:TransitType')

LookupSuburbs        PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
sur:Suburb:IsInvalid  Long
sur:Postcode:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SUBURB)
                      Project(sur:RecordNumber)
                      Project(sur:Suburb)
                      Project(sur:Postcode)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('LookupSuburbs')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupSuburbs:NoForm')
      loc:NoForm = p_web.GetValue('LookupSuburbs:NoForm')
      loc:FormName = p_web.GetValue('LookupSuburbs:FormName')
    else
      loc:FormName = 'LookupSuburbs_frm'
    End
    p_web.SSV('LookupSuburbs:NoForm',loc:NoForm)
    p_web.SSV('LookupSuburbs:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupSuburbs:NoForm')
    loc:FormName = p_web.GSV('LookupSuburbs:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupSuburbs') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupSuburbs')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'LookupSuburbs' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','LookupSuburbs')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('LookupSuburbs') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('LookupSuburbs')
      p_web.DivHeader('popup_LookupSuburbs','nt-hidden')
      p_web.DivHeader('LookupSuburbs',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(500)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_LookupSuburbs_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_LookupSuburbs_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('LookupSuburbs',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SUBURB,sur:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SUR:SUBURB') then p_web.SetValue('LookupSuburbs_sort','1')
    ElsIf (loc:vorder = 'SUR:POSTCODE') then p_web.SetValue('LookupSuburbs_sort','2')
    ElsIf (loc:vorder = 'SUR:POSTCODE') then p_web.SetValue('LookupSuburbs_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupSuburbs:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupSuburbs:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupSuburbs:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupSuburbs:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'LookupSuburbs'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('LookupSuburbs')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupSuburbs_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('LookupSuburbs_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sur:Suburb)','-UPPER(sur:Suburb)')
    Loc:LocateField = 'sur:Suburb'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sur:Postcode)','-UPPER(sur:Postcode)')
    Loc:LocateField = 'sur:Postcode'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(sur:Suburb)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sur:Suburb')
    loc:SortHeader = p_web.Translate('Suburb')
    p_web.SetSessionValue('LookupSuburbs_LocatorPic','@s30')
  Of upper('sur:Postcode')
    loc:SortHeader = p_web.Translate('Postcode')
    p_web.SetSessionValue('LookupSuburbs_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('LookupSuburbs:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupSuburbs:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupSuburbs:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupSuburbs:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SUBURB"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sur:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Suburb') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Select Suburb',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{LookupSuburbs.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupSuburbs',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2LookupSuburbs','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('LookupSuburbs_LocatorPic'),,,'onchange="LookupSuburbs.locate(''Locator2LookupSuburbs'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2LookupSuburbs',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="LookupSuburbs.locate(''Locator2LookupSuburbs'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="LookupSuburbs_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupSuburbs.cl(''LookupSuburbs'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'LookupSuburbs_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('LookupSuburbs_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="LookupSuburbs_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('LookupSuburbs_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="LookupSuburbs_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','LookupSuburbs',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','LookupSuburbs',p_web.Translate('Suburb'),'Click here to sort by Suburb',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','LookupSuburbs',p_web.Translate('Postcode'),'Click here to sort by Postcode',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sur:recordnumber',lower(loc:vorder),1,1) = 0 !and SUBURB{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sur:RecordNumber',clip(loc:vorder) & ',' & 'sur:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sur:RecordNumber'),p_web.GetValue('sur:RecordNumber'),p_web.GetSessionValue('sur:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupSuburbs',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupSuburbs_Filter')
    p_web.SetSessionValue('LookupSuburbs_FirstValue','')
    p_web.SetSessionValue('LookupSuburbs_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SUBURB,sur:RecordNumberKey,loc:PageRows,'LookupSuburbs',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SUBURB{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SUBURB,loc:firstvalue)
              Reset(ThisView,SUBURB)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SUBURB{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SUBURB,loc:lastvalue)
            Reset(ThisView,SUBURB)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sur:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="LookupSuburbs_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupSuburbs.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupSuburbs.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupSuburbs.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupSuburbs.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupSuburbs_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'LookupSuburbs',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupSuburbs',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupSuburbs_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupSuburbs_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1LookupSuburbs','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('LookupSuburbs_LocatorPic'),,,'onchange="LookupSuburbs.locate(''Locator1LookupSuburbs'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1LookupSuburbs',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="LookupSuburbs.locate(''Locator1LookupSuburbs'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="LookupSuburbs_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupSuburbs.cl(''LookupSuburbs'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupSuburbs_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('LookupSuburbs_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupSuburbs_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="LookupSuburbs_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupSuburbs.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupSuburbs.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupSuburbs.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupSuburbs.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupSuburbs_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'LookupSuburbs',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('LookupSuburbs','SUBURB',sur:RecordNumberKey) !sur:RecordNumber
    p_web._thisrow = p_web._nocolon('sur:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sur:RecordNumber = p_web.GetValue('sur:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupSuburbs:LookupField')) = sur:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sur:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('LookupSuburbs','SUBURB',sur:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SUBURB{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SUBURB)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SUBURB{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SUBURB)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::sur:Suburb
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('LeftJustify')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::sur:Postcode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('LookupSuburbs','SUBURB',sur:RecordNumberKey)
  TableQueue.Id[1] = sur:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiLookupSuburbs;if (btiLookupSuburbs != 1){{var LookupSuburbs=new browseTable(''LookupSuburbs'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sur:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'LookupSuburbs.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'LookupSuburbs.applyGreenBar();btiLookupSuburbs=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupSuburbs')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupSuburbs')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupSuburbs')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupSuburbs')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SUBURB)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SUBURB)
  Bind(sur:Record)
  Clear(sur:Record)
  NetWebSetSessionPics(p_web,SUBURB)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sur:RecordNumber',p_web.GetValue('sur:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sur:RecordNumber = p_web.GSV('sur:RecordNumber')
  loc:result = p_web._GetFile(SUBURB,sur:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sur:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SUBURB)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SUBURB)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupSuburbs_Select_'&sur:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupSuburbs',p_web.AddBrowseValue('LookupSuburbs','SUBURB',sur:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sur:Suburb   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupSuburbs_sur:Suburb_'&sur:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sur:Suburb,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sur:Postcode   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupSuburbs_sur:Postcode_'&sur:RecordNumber,'LeftJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sur:Postcode,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sur:RecordNumber',sur:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
LookupRRCAccounts    PROCEDURE  (NetWebServerWorker p_web)
BookingAccount       STRING(30)                            !BookingAccount
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
sub:Account_Number:IsInvalid  Long
sub:Company_Name:IsInvalid  Long
sub:Branch:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SUBTRACC)
                      Project(sub:RecordNumber)
                      Project(sub:Account_Number)
                      Project(sub:Company_Name)
                      Project(sub:Branch)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('LookupRRCAccounts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupRRCAccounts:NoForm')
      loc:NoForm = p_web.GetValue('LookupRRCAccounts:NoForm')
      loc:FormName = p_web.GetValue('LookupRRCAccounts:FormName')
    else
      loc:FormName = 'LookupRRCAccounts_frm'
    End
    p_web.SSV('LookupRRCAccounts:NoForm',loc:NoForm)
    p_web.SSV('LookupRRCAccounts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupRRCAccounts:NoForm')
    loc:FormName = p_web.GSV('LookupRRCAccounts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupRRCAccounts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupRRCAccounts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'LookupRRCAccounts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','LookupRRCAccounts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('LookupRRCAccounts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('LookupRRCAccounts')
      p_web.DivHeader('popup_LookupRRCAccounts','nt-hidden')
      p_web.DivHeader('LookupRRCAccounts',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(700)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_LookupRRCAccounts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_LookupRRCAccounts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('LookupRRCAccounts',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SUBTRACC,sub:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SUB:ACCOUNT_NUMBER') then p_web.SetValue('LookupRRCAccounts_sort','1')
    ElsIf (loc:vorder = 'SUB:ACCOUNT_NUMBER') then p_web.SetValue('LookupRRCAccounts_sort','1')
    ElsIf (loc:vorder = 'SUB:COMPANY_NAME') then p_web.SetValue('LookupRRCAccounts_sort','2')
    ElsIf (loc:vorder = 'SUB:COMPANY_NAME') then p_web.SetValue('LookupRRCAccounts_sort','2')
    ElsIf (loc:vorder = 'SUB:BRANCH') then p_web.SetValue('LookupRRCAccounts_sort','4')
    ElsIf (loc:vorder = 'SUB:BRANCH') then p_web.SetValue('LookupRRCAccounts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupRRCAccounts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupRRCAccounts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupRRCAccounts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupRRCAccounts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'LookupRRCAccounts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('LookupRRCAccounts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupRRCAccounts_sort',net:DontEvaluate)
  p_web.SetSessionValue('LookupRRCAccounts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 5
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sub:Account_Number)','-UPPER(sub:Account_Number)')
    Loc:LocateField = 'sub:Account_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sub:Company_Name)','-UPPER(sub:Company_Name)')
    Loc:LocateField = 'sub:Company_Name'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sub:Branch)','-UPPER(sub:Branch)')
    Loc:LocateField = 'sub:Branch'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:nobuffer = 1
    Loc:LocateField = 'sub:Account_Number'
    loc:sortheader = 'Account Number'
    loc:vorder = '+sub:Account_Number,+sub:Company_Name,+sub:Branch'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 1)
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 2)
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sub:Account_Number')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('LookupRRCAccounts_LocatorPic','@s15')
  Of upper('sub:Company_Name')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('LookupRRCAccounts_LocatorPic','@s30')
  Of upper('sub:Branch')
    loc:SortHeader = p_web.Translate('Branch')
    p_web.SetSessionValue('LookupRRCAccounts_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('LookupRRCAccounts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupRRCAccounts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupRRCAccounts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupRRCAccounts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SUBTRACC"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sub:RecordNumberKey"></input><13,10>'
  end
  if p_web.Translate('Select Account') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('Select Account',0)&'</div>'&CRLF
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{LookupRRCAccounts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupRRCAccounts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2LookupRRCAccounts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('LookupRRCAccounts_LocatorPic'),,,'onchange="LookupRRCAccounts.locate(''Locator2LookupRRCAccounts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2LookupRRCAccounts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="LookupRRCAccounts.locate(''Locator2LookupRRCAccounts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="LookupRRCAccounts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupRRCAccounts.cl(''LookupRRCAccounts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'LookupRRCAccounts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('LookupRRCAccounts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="LookupRRCAccounts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('LookupRRCAccounts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="LookupRRCAccounts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','LookupRRCAccounts',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','LookupRRCAccounts',p_web.Translate('Account Number'),'Click here to sort by Account Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','LookupRRCAccounts',p_web.Translate('Company Name'),'Click here to sort by Company Name',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','LookupRRCAccounts',p_web.Translate('Branch'),'Click here to sort by Branch',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sub:recordnumber',lower(loc:vorder),1,1) = 0 !and SUBTRACC{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sub:RecordNumber',clip(loc:vorder) & ',' & 'sub:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sub:RecordNumber'),p_web.GetValue('sub:RecordNumber'),p_web.GetSessionValue('sub:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 1)
      loc:FilterWas = 'Upper(sub:Main_Account_Number) = Upper(''' & P_web.GetSessionValue('BookingAccount') & ''')'
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 2)
      loc:FilterWas = 'Upper(sub:Generic_Account) = 1'
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupRRCAccounts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupRRCAccounts_Filter')
    p_web.SetSessionValue('LookupRRCAccounts_FirstValue','')
    p_web.SetSessionValue('LookupRRCAccounts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SUBTRACC,sub:RecordNumberKey,loc:PageRows,'LookupRRCAccounts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SUBTRACC{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SUBTRACC,loc:firstvalue)
              Reset(ThisView,SUBTRACC)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SUBTRACC{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SUBTRACC,loc:lastvalue)
            Reset(ThisView,SUBTRACC)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sub:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="LookupRRCAccounts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupRRCAccounts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupRRCAccounts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupRRCAccounts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupRRCAccounts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupRRCAccounts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'LookupRRCAccounts',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupRRCAccounts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupRRCAccounts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupRRCAccounts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1LookupRRCAccounts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('LookupRRCAccounts_LocatorPic'),,,'onchange="LookupRRCAccounts.locate(''Locator1LookupRRCAccounts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1LookupRRCAccounts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="LookupRRCAccounts.locate(''Locator1LookupRRCAccounts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="LookupRRCAccounts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupRRCAccounts.cl(''LookupRRCAccounts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupRRCAccounts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('LookupRRCAccounts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupRRCAccounts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="LookupRRCAccounts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupRRCAccounts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupRRCAccounts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupRRCAccounts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupRRCAccounts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupRRCAccounts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'LookupRRCAccounts',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('LookupRRCAccounts','SUBTRACC',sub:RecordNumberKey) !sub:RecordNumber
    p_web._thisrow = p_web._nocolon('sub:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sub:RecordNumber = p_web.GetValue('sub:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupRRCAccounts:LookupField')) = sub:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sub:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('LookupRRCAccounts','SUBTRACC',sub:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SUBTRACC{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SUBTRACC)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SUBTRACC{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SUBTRACC)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::sub:Account_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(300)&'"><13,10>'
          end ! loc:eip = 0
          do value::sub:Company_Name
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(280)&'"><13,10>'
          end ! loc:eip = 0
          do value::sub:Branch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('LookupRRCAccounts','SUBTRACC',sub:RecordNumberKey)
  TableQueue.Id[1] = sub:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiLookupRRCAccounts;if (btiLookupRRCAccounts != 1){{var LookupRRCAccounts=new browseTable(''LookupRRCAccounts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sub:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'LookupRRCAccounts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'LookupRRCAccounts.applyGreenBar();btiLookupRRCAccounts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupRRCAccounts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupRRCAccounts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupRRCAccounts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupRRCAccounts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SUBTRACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  Clear(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sub:RecordNumber',p_web.GetValue('sub:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sub:RecordNumber = p_web.GSV('sub:RecordNumber')
  loc:result = p_web._GetFile(SUBTRACC,sub:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sub:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SUBTRACC)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SUBTRACC)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupRRCAccounts_Select_'&sub:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupRRCAccounts',p_web.AddBrowseValue('LookupRRCAccounts','SUBTRACC',sub:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sub:Account_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupRRCAccounts_sub:Account_Number_'&sub:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sub:Account_Number,'@s15')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sub:Company_Name   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupRRCAccounts_sub:Company_Name_'&sub:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sub:Company_Name,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sub:Branch   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupRRCAccounts_sub:Branch_'&sub:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sub:Branch,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(sub:Main_Account_Key)
    loc:Invalid = 'sub:Main_Account_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Main_Account_Key --> sub:Main_Account_Number, '&clip('Account Number')&''
  End
  If Duplicate(sub:Account_Number_Key)
    loc:Invalid = 'sub:Account_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Account_Number_Key --> '&clip('Account Number')&' = ' & clip(sub:Account_Number)
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sub:RecordNumber',sub:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
LookupProductCodes   PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
mop:ProductCode:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MODPROD)
                      Project(mop:RecordNumber)
                      Project(mop:ProductCode)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('LookupProductCodes')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupProductCodes:NoForm')
      loc:NoForm = p_web.GetValue('LookupProductCodes:NoForm')
      loc:FormName = p_web.GetValue('LookupProductCodes:FormName')
    else
      loc:FormName = 'LookupProductCodes_frm'
    End
    p_web.SSV('LookupProductCodes:NoForm',loc:NoForm)
    p_web.SSV('LookupProductCodes:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupProductCodes:NoForm')
    loc:FormName = p_web.GSV('LookupProductCodes:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupProductCodes') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupProductCodes')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'LookupProductCodes' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','LookupProductCodes')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('LookupProductCodes') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('LookupProductCodes')
      p_web.DivHeader('popup_LookupProductCodes','nt-hidden')
      p_web.DivHeader('LookupProductCodes',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_LookupProductCodes_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_LookupProductCodes_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('LookupProductCodes',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MODPROD,mop:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MOP:PRODUCTCODE') then p_web.SetValue('LookupProductCodes_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupProductCodes:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupProductCodes:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupProductCodes:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupProductCodes:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'LookupProductCodes'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('LookupProductCodes')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    If p_web.IfExistsValue('LookupJobType')
      p_web.StoreValue('LookupJobType')
    End
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 22
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupProductCodes_sort',net:DontEvaluate)
  p_web.SetSessionValue('LookupProductCodes_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mop:ProductCode)','-UPPER(mop:ProductCode)')
    Loc:LocateField = 'mop:ProductCode'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mop:ModelNumber),+UPPER(mop:ProductCode)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mop:ProductCode')
    loc:SortHeader = p_web.Translate('Product Code')
    p_web.SetSessionValue('LookupProductCodes_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('LookupProductCodes:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupProductCodes:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupProductCodes:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupProductCodes:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MODPROD"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mop:RecordNumberKey"></input><13,10>'
  end
  if p_web.Translate('Select Product Code') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('Select Product Code',0)&'</div>'&CRLF
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{LookupProductCodes.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupProductCodes',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2LookupProductCodes','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('LookupProductCodes_LocatorPic'),,,'onchange="LookupProductCodes.locate(''Locator2LookupProductCodes'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2LookupProductCodes',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="LookupProductCodes.locate(''Locator2LookupProductCodes'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="LookupProductCodes_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupProductCodes.cl(''LookupProductCodes'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'LookupProductCodes_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('LookupProductCodes_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="LookupProductCodes_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('LookupProductCodes_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="LookupProductCodes_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','LookupProductCodes',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','LookupProductCodes',p_web.Translate('Product Code'),'Click here to sort by Product Code',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,22,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('mop:recordnumber',lower(loc:vorder),1,1) = 0 !and MODPROD{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','mop:RecordNumber',clip(loc:vorder) & ',' & 'mop:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mop:RecordNumber'),p_web.GetValue('mop:RecordNumber'),p_web.GetSessionValue('mop:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(mop:ModelNumber) = Upper(''' & p_web.GetSessionValue('job:Model_Number') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupProductCodes',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupProductCodes_Filter')
    p_web.SetSessionValue('LookupProductCodes_FirstValue','')
    p_web.SetSessionValue('LookupProductCodes_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MODPROD,mop:RecordNumberKey,loc:PageRows,'LookupProductCodes',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MODPROD{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MODPROD,loc:firstvalue)
              Reset(ThisView,MODPROD)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MODPROD{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MODPROD,loc:lastvalue)
            Reset(ThisView,MODPROD)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mop:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="LookupProductCodes_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupProductCodes.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupProductCodes.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupProductCodes.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupProductCodes.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupProductCodes_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'LookupProductCodes',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupProductCodes',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupProductCodes_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupProductCodes_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1LookupProductCodes','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('LookupProductCodes_LocatorPic'),,,'onchange="LookupProductCodes.locate(''Locator1LookupProductCodes'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1LookupProductCodes',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="LookupProductCodes.locate(''Locator1LookupProductCodes'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="LookupProductCodes_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupProductCodes.cl(''LookupProductCodes'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupProductCodes_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('LookupProductCodes_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupProductCodes_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="LookupProductCodes_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupProductCodes.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupProductCodes.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupProductCodes.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupProductCodes.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'LookupProductCodes_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'LookupProductCodes',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('LookupProductCodes','MODPROD',mop:RecordNumberKey) !mop:RecordNumber
    p_web._thisrow = p_web._nocolon('mop:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and mop:RecordNumber = p_web.GetValue('mop:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupProductCodes:LookupField')) = mop:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((mop:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('LookupProductCodes','MODPROD',mop:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MODPROD{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MODPROD)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MODPROD{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MODPROD)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('LeftJustify')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::mop:ProductCode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('LookupProductCodes','MODPROD',mop:RecordNumberKey)
  TableQueue.Id[1] = mop:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiLookupProductCodes;if (btiLookupProductCodes != 1){{var LookupProductCodes=new browseTable(''LookupProductCodes'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('mop:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'LookupProductCodes.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'LookupProductCodes.applyGreenBar();btiLookupProductCodes=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupProductCodes')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupProductCodes')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupProductCodes')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupProductCodes')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MODPROD)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MODPROD)
  Bind(mop:Record)
  Clear(mop:Record)
  NetWebSetSessionPics(p_web,MODPROD)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('mop:RecordNumber',p_web.GetValue('mop:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  mop:RecordNumber = p_web.GSV('mop:RecordNumber')
  loc:result = p_web._GetFile(MODPROD,mop:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mop:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MODPROD)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MODPROD)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupProductCodes_Select_'&mop:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupProductCodes',p_web.AddBrowseValue('LookupProductCodes','MODPROD',mop:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mop:ProductCode   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('LookupProductCodes_mop:ProductCode_'&mop:RecordNumber,'LeftJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mop:ProductCode,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mop:ProductCodeKey)
    loc:Invalid = 'mop:ModelNumber'
    loc:Alert = clip(p_web.site.DuplicateText) & ' ProductCodeKey --> mop:ModelNumber, '&clip('Product Code')&''
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('mop:RecordNumber',mop:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
