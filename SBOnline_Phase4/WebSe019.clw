

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE019.INC'),ONCE        !Local module procedure declarations
                     END


ForceInvoiceText     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Invoice Text
    If (def:Force_Invoice_Text = 'B' And func:Type = 'B') Or |
        (def:Force_Invoice_Text <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Invoice_Text = 'B'
    Return Level:Benign
ForceIncomingCourier PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Incoming Courier
    If (def:Force_Incoming_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Incoming_Courier <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Incoming_Courier = 'B'
    Return Level:Benign
ForceIMEI            PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !ESN
    If (def:Force_ESN = 'B' And func:Type = 'B') Or |
        (def:Force_ESN <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_ESN = 'B'
    Return Level:Benign
ForceFaultDescription PROCEDURE  (func:Type)               ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFIles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    !Fault Description
    If (def:Force_Fault_Description = 'B' and func:Type = 'B') Or |
        (def:Force_Fault_Description <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Fault_Description = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ForceFaultCodes      PROCEDURE  (func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,func:Type,fManufacturer) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    !(func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,fManufacturer)

    If func:Type = 'C' or func:type = 'X'
        If func:ChargeableJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:CChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:CRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = func:CRepairType
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
! Deleted (DBH 20/05/2006) #6733 - Do not check the manufacturer for char fault codes
!                                 Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!                                 man:Manufacturer    = fManufacturer
!                                 If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Found
!                                     If man:ForceCharFaultCodes
!                                         Return Level:Fatal
!                                     End !If man:ForceCharFaultCodes
!                                 Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Error
!                                 End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
! End (DBH 20/05/2006) #6733
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                    Else !If func:CRepairType <> ''
                        Return Level:Fatal
                    End !If func:CRepairType <> ''
                End !If cha:Force_Warranty = 'YES'
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Chargeable_Job = 'YES'
    End !If func:Type = 'C' or func:type = 'X'

    If func:Type = 'W' or func:Type = 'X'
        If func:WarrantyJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:WChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:WRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = func:WRepairType
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                    Else !If WRetairType <> ''
                        Return Level:Fatal
                    End !If WRetairType <> ''
                End !If cha:Force_Warranty = 'YES'

            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Warranty_Job = 'YES'
    End !If func:Type = 'W' or func:Type = 'X'
    Return Level:Benign
