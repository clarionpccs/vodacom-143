

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE066.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE065.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE067.INC'),ONCE        !Req'd for module callout resolution
                     END


DisplayOutFaults     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
BrowseJobOutFaults:IsInvalid  Long
BrowseOutFaultsChargeableParts:IsInvalid  Long
BrowseOutFaultsWarrantyParts:IsInvalid  Long
BrowseOutFaultsEstimateParts:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('DisplayOutFaults')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'DisplayOutFaults_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DisplayOutFaults','')
    p_web.DivHeader('DisplayOutFaults',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('DisplayOutFaults') = 0
        p_web.AddPreCall('DisplayOutFaults')
        p_web.DivHeader('popup_DisplayOutFaults','nt-hidden')
        p_web.DivHeader('DisplayOutFaults',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_DisplayOutFaults_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_DisplayOutFaults_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DisplayOutFaults',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('DisplayOutFaults')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseJobOutFaults')
      do Value::BrowseJobOutFaults
    of upper('BrowseOutFaultsChargeableParts')
      do Value::BrowseOutFaultsChargeableParts
    of upper('BrowseOutFaultsWarrantyParts')
      do Value::BrowseOutFaultsWarrantyParts
    of upper('BrowseOutFaultsEstimateParts')
      do Value::BrowseOutFaultsEstimateParts
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
   p_web.site.SaveButton.TextValue = 'OK'
  p_web.SetValue('DisplayOutFaults_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'DisplayOutFaults'
    end
    p_web.formsettings.proc = 'DisplayOutFaults'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('DisplayOutFaults_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'JobFaultCodes'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DisplayOutFaults_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DisplayOutFaults_ChainTo')
    loc:formaction = p_web.GetSessionValue('DisplayOutFaults_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'JobFaultCodes'

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Out Faults') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Out Faults',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_DisplayOutFaults',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DisplayOutFaults0_div')&'">'&p_web.Translate('Job Out Faults')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DisplayOutFaults1_div')&'">'&p_web.Translate('Part Out Faults')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="DisplayOutFaults_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="DisplayOutFaults_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DisplayOutFaults_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="DisplayOutFaults_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DisplayOutFaults_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_DisplayOutFaults'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DisplayOutFaults') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_DisplayOutFaults'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DisplayOutFaults') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Out Faults') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Out Faults') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_DisplayOutFaults_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"DisplayOutFaults",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_DisplayOutFaults_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('DisplayOutFaults') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('DisplayOutFaults')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseJobOutFaults') = 0
      p_web.SetValue('BrowseJobOutFaults:NoForm',1)
      p_web.SetValue('BrowseJobOutFaults:FormName',loc:formname)
      p_web.SetValue('BrowseJobOutFaults:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseJobOutFaults(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseJobOutFaults:NoForm')
      p_web.DeleteValue('BrowseJobOutFaults:FormName')
      p_web.DeleteValue('BrowseJobOutFaults:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseOutFaultsChargeableParts') = 0
      p_web.SetValue('BrowseOutFaultsChargeableParts:NoForm',1)
      p_web.SetValue('BrowseOutFaultsChargeableParts:FormName',loc:formname)
      p_web.SetValue('BrowseOutFaultsChargeableParts:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseOutFaultsChargeableParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseOutFaultsChargeableParts:NoForm')
      p_web.DeleteValue('BrowseOutFaultsChargeableParts:FormName')
      p_web.DeleteValue('BrowseOutFaultsChargeableParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseOutFaultsWarrantyParts') = 0
      p_web.SetValue('BrowseOutFaultsWarrantyParts:NoForm',1)
      p_web.SetValue('BrowseOutFaultsWarrantyParts:FormName',loc:formname)
      p_web.SetValue('BrowseOutFaultsWarrantyParts:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseOutFaultsWarrantyParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseOutFaultsWarrantyParts:NoForm')
      p_web.DeleteValue('BrowseOutFaultsWarrantyParts:FormName')
      p_web.DeleteValue('BrowseOutFaultsWarrantyParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseOutFaultsEstimateParts') = 0
      p_web.SetValue('BrowseOutFaultsEstimateParts:NoForm',1)
      p_web.SetValue('BrowseOutFaultsEstimateParts:FormName',loc:formname)
      p_web.SetValue('BrowseOutFaultsEstimateParts:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseOutFaultsEstimateParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseOutFaultsEstimateParts:NoForm')
      p_web.DeleteValue('BrowseOutFaultsEstimateParts:FormName')
      p_web.DeleteValue('BrowseOutFaultsEstimateParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Out Faults')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Out Faults')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Out Faults')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Out Faults')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseJobOutFaults
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Out Faults')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,'FormCentreFixed'),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,'FormCentreFixed'),Net:NoSend,'Part Out Faults')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,'FormCentreFixed'),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Out Faults')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,'FormCentreFixed'),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Out Faults')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseOutFaultsChargeableParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseOutFaultsWarrantyParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseOutFaultsEstimateParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::BrowseJobOutFaults  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('joo:RecordNumber')
  End
  do ValidateValue::BrowseJobOutFaults  ! copies value to session value if valid.
  do Comment::BrowseJobOutFaults ! allows comment style to be updated.

ValidateValue::BrowseJobOutFaults  Routine
    If not (1=0)
    End

Value::BrowseJobOutFaults  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseJobOutFaults --
  p_web.SetValue('BrowseJobOutFaults:NoForm',1)
  p_web.SetValue('BrowseJobOutFaults:FormName',loc:formname)
  p_web.SetValue('BrowseJobOutFaults:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&'"><!-- Net:BrowseJobOutFaults --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseJobOutFaults') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseJobOutFaults --><13,10>'
  end
  do SendPacket
Comment::BrowseJobOutFaults  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseJobOutFaults:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseJobOutFaults') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseOutFaultsChargeableParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('sofp:sessionID')
    p_web.StoreValue('sofp:partType')
    p_web.StoreValue('sofp:fault')
  End
  do ValidateValue::BrowseOutFaultsChargeableParts  ! copies value to session value if valid.
  do Comment::BrowseOutFaultsChargeableParts ! allows comment style to be updated.

ValidateValue::BrowseOutFaultsChargeableParts  Routine
    If not (p_web.GSV('job:Chargeable_Job') <>'YES')
    End

Value::BrowseOutFaultsChargeableParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <>'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsChargeableParts --
  p_web.SetValue('BrowseOutFaultsChargeableParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&'"><!-- Net:BrowseOutFaultsChargeableParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsChargeableParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsChargeableParts --><13,10>'
  end
  do SendPacket
Comment::BrowseOutFaultsChargeableParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseOutFaultsChargeableParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <>'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsChargeableParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <>'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseOutFaultsWarrantyParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('sofp:sessionID')
    p_web.StoreValue('sofp:partType')
    p_web.StoreValue('sofp:fault')
  End
  do ValidateValue::BrowseOutFaultsWarrantyParts  ! copies value to session value if valid.
  do Comment::BrowseOutFaultsWarrantyParts ! allows comment style to be updated.

ValidateValue::BrowseOutFaultsWarrantyParts  Routine
    If not (p_web.GSV('job:Warranty_Job') <> 'YES')
    End

Value::BrowseOutFaultsWarrantyParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsWarrantyParts --
  p_web.SetValue('BrowseOutFaultsWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&'"><!-- Net:BrowseOutFaultsWarrantyParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsWarrantyParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsWarrantyParts --><13,10>'
  end
  do SendPacket
Comment::BrowseOutFaultsWarrantyParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseOutFaultsWarrantyParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsWarrantyParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseOutFaultsEstimateParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('sofp:sessionID')
    p_web.StoreValue('sofp:partType')
    p_web.StoreValue('sofp:fault')
  End
  do ValidateValue::BrowseOutFaultsEstimateParts  ! copies value to session value if valid.
  do Comment::BrowseOutFaultsEstimateParts ! allows comment style to be updated.

ValidateValue::BrowseOutFaultsEstimateParts  Routine
    If not (p_web.GSV('job:Estimate') <> 'YES')
    End

Value::BrowseOutFaultsEstimateParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Estimate') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsEstimateParts --
  p_web.SetValue('BrowseOutFaultsEstimateParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsEstimateParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsEstimateParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&'"><!-- Net:BrowseOutFaultsEstimateParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsEstimateParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsEstimateParts --><13,10>'
  end
  do SendPacket
Comment::BrowseOutFaultsEstimateParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseOutFaultsEstimateParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsEstimateParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DisplayOutFaults_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('DisplayOutFaults_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_DisplayOutFaults_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DisplayOutFaults_tab_' & 0)
    do GenerateTab0
  of lower('DisplayOutFaults_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)

  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DisplayOutFaults_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('DisplayOutFaults_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::BrowseJobOutFaults
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowseOutFaultsChargeableParts
    If loc:Invalid then exit.
    do ValidateValue::BrowseOutFaultsWarrantyParts
    If loc:Invalid then exit.
    do ValidateValue::BrowseOutFaultsEstimateParts
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

BrowseOutFaultsChargeableParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
ChargeableCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sofp:fault:IsInvalid  Long
sofp:description:IsInvalid  Long
sofp:level:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAULO::State  USHORT
PARTS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If par:Fault_Code1 <> ''
            Local.ChargeableCode(1,par:fault_Code1)
        End !If par:Fault_Code1
        If par:Fault_Code2 <> ''
            Local.ChargeableCode(2,par:fault_Code2)
        End !If par:Fault_Code1
        If par:Fault_Code3 <> ''
            Local.ChargeableCode(3,par:fault_Code3)
        End !If par:Fault_Code1
        If par:Fault_Code4 <> ''
            Local.ChargeableCode(4,par:fault_Code4)
        End !If par:Fault_Code1
        If par:Fault_Code5 <> ''
            Local.ChargeableCode(5,par:fault_Code5)
        End !If par:Fault_Code1
        If par:Fault_Code6 <> ''
            Local.ChargeableCode(6,par:fault_Code6)
        End !If par:Fault_Code1
        If par:Fault_Code7 <> ''
            Local.ChargeableCode(7,par:fault_Code7)
        End !If par:Fault_Code1
        If par:Fault_Code8 <> ''
            Local.ChargeableCode(8,par:fault_Code8)
        End !If par:Fault_Code1
        If par:Fault_Code9 <> ''
            Local.ChargeableCode(9,par:fault_Code9)
        End !If par:Fault_Code1
        If par:Fault_Code10 <> ''
            Local.ChargeableCode(10,par:fault_Code10)
        End !If par:Fault_Code1
        If par:Fault_Code11 <> ''
            Local.ChargeableCode(11,par:fault_Code11)
        End !If par:Fault_Code1
        If par:Fault_Code12 <> ''
            Local.ChargeableCode(12,par:fault_Code12)
        End !If par:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsChargeableParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsChargeableParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsChargeableParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsChargeableParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsChargeableParts_frm'
    End
    p_web.SSV('BrowseOutFaultsChargeableParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsChargeableParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsChargeableParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsChargeableParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsChargeableParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsChargeableParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseOutFaultsChargeableParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseOutFaultsChargeableParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseOutFaultsChargeableParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseOutFaultsChargeableParts')
      p_web.DivHeader('popup_BrowseOutFaultsChargeableParts','nt-hidden')
      p_web.DivHeader('BrowseOutFaultsChargeableParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseOutFaultsChargeableParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseOutFaultsChargeableParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseOutFaultsChargeableParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsChargeableParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsChargeableParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsChargeableParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsChargeableParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseOutFaultsChargeableParts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseOutFaultsChargeableParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsChargeableParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsChargeableParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsChargeableParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Chargeable Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Chargeable Parts',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseOutFaultsChargeableParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseOutFaultsChargeableParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsChargeableParts_LocatorPic'),,,'onchange="BrowseOutFaultsChargeableParts.locate(''Locator2BrowseOutFaultsChargeableParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseOutFaultsChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsChargeableParts.locate(''Locator2BrowseOutFaultsChargeableParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseOutFaultsChargeableParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsChargeableParts.cl(''BrowseOutFaultsChargeableParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseOutFaultsChargeableParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseOutFaultsChargeableParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsChargeableParts',p_web.Translate('Fault'),'Click here to sort by Fault',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsChargeableParts',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsChargeableParts',p_web.Translate('Repair Index'),'Click here to sort by Repair Index',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sofp:sessionid',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:sessionID',clip(loc:vorder) & ',' & 'sofp:sessionID')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:partType',clip(loc:vorder) & ',' & 'sofp:partType')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:fault',clip(loc:vorder) & ',' & 'sofp:fault')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''C'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsChargeableParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsChargeableParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsChargeableParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseOutFaultsChargeableParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsChargeableParts_LocatorPic'),,,'onchange="BrowseOutFaultsChargeableParts.locate(''Locator1BrowseOutFaultsChargeableParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseOutFaultsChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsChargeableParts.locate(''Locator1BrowseOutFaultsChargeableParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseOutFaultsChargeableParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsChargeableParts.cl(''BrowseOutFaultsChargeableParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsChargeableParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseOutFaultsChargeableParts','SBO_OutFaultParts',sofp:FaultKey) !sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sofp:fault = p_web.GetValue('sofp:fault')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupField')) = sofp:fault and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sofp:fault = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseOutFaultsChargeableParts','SBO_OutFaultParts',sofp:FaultKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseOutFaultsChargeableParts','SBO_OutFaultParts',sofp:FaultKey)
  TableQueue.Id[1] = sofp:sessionID
  TableQueue.Id[2] = sofp:partType
  TableQueue.Id[3] = sofp:fault

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseOutFaultsChargeableParts;if (btiBrowseOutFaultsChargeableParts != 1){{var BrowseOutFaultsChargeableParts=new browseTable(''BrowseOutFaultsChargeableParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseOutFaultsChargeableParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsChargeableParts.applyGreenBar();btiBrowseOutFaultsChargeableParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsChargeableParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsChargeableParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsChargeableParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsChargeableParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sofp:sessionID',p_web.GetValue('sofp:sessionID'))
  p_web.SetSessionValue('sofp:partType',p_web.GetValue('sofp:partType'))
  p_web.SetSessionValue('sofp:fault',p_web.GetValue('sofp:fault'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sofp:sessionID = p_web.GSV('sofp:sessionID')
  sofp:partType = p_web.GSV('sofp:partType')
  sofp:fault = p_web.GSV('sofp:fault')
  loc:result = p_web._GetFile(SBO_OutFaultParts,sofp:FaultKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:sessionID)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_OutFaultParts)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SBO_OutFaultParts)
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_sofp:fault_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:fault,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_sofp:description_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_sofp:level_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:level,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.ChargeableCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0
                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END



                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'C'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'C'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
BrowseOutFaultsWarrantyParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
WarrantyCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sofp:fault:IsInvalid  Long
sofp:description:IsInvalid  Long
sofp:level:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAULO::State  USHORT
WARPARTS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If wpr:Fault_Code1 <> ''
            Local.WarrantyCode(1,wpr:fault_Code1)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code2 <> ''
            Local.WarrantyCode(2,wpr:fault_Code2)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code3 <> ''
            Local.WarrantyCode(3,wpr:fault_Code3)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code4 <> ''
            Local.WarrantyCode(4,wpr:fault_Code4)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code5 <> ''
            Local.WarrantyCode(5,wpr:fault_Code5)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code6 <> ''
            Local.WarrantyCode(6,wpr:fault_Code6)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code7 <> ''
            Local.WarrantyCode(7,wpr:fault_Code7)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code8 <> ''
            Local.WarrantyCode(8,wpr:fault_Code8)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code9 <> ''
            Local.WarrantyCode(9,wpr:fault_Code9)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code10 <> ''
            Local.WarrantyCode(10,wpr:fault_Code10)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code11 <> ''
            Local.WarrantyCode(11,wpr:fault_Code11)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code12 <> ''
            Local.WarrantyCode(12,wpr:fault_Code12)
        End !If wpr:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsWarrantyParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsWarrantyParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsWarrantyParts_frm'
    End
    p_web.SSV('BrowseOutFaultsWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsWarrantyParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseOutFaultsWarrantyParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseOutFaultsWarrantyParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseOutFaultsWarrantyParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseOutFaultsWarrantyParts')
      p_web.DivHeader('popup_BrowseOutFaultsWarrantyParts','nt-hidden')
      p_web.DivHeader('BrowseOutFaultsWarrantyParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseOutFaultsWarrantyParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseOutFaultsWarrantyParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseOutFaultsWarrantyParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsWarrantyParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsWarrantyParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseOutFaultsWarrantyParts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseOutFaultsWarrantyParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseOutFaultsWarrantyParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Warranty Parts',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseOutFaultsWarrantyParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseOutFaultsWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsWarrantyParts_LocatorPic'),,,'onchange="BrowseOutFaultsWarrantyParts.locate(''Locator2BrowseOutFaultsWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseOutFaultsWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsWarrantyParts.locate(''Locator2BrowseOutFaultsWarrantyParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseOutFaultsWarrantyParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsWarrantyParts.cl(''BrowseOutFaultsWarrantyParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseOutFaultsWarrantyParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseOutFaultsWarrantyParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsWarrantyParts',p_web.Translate('Fault'),'Click here to sort by Fault',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsWarrantyParts',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsWarrantyParts',p_web.Translate('Repair Index'),'Click here to sort by Repair Index',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sofp:sessionid',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:sessionID',clip(loc:vorder) & ',' & 'sofp:sessionID')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:partType',clip(loc:vorder) & ',' & 'sofp:partType')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:fault',clip(loc:vorder) & ',' & 'sofp:fault')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''W'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsWarrantyParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsWarrantyParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseOutFaultsWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsWarrantyParts_LocatorPic'),,,'onchange="BrowseOutFaultsWarrantyParts.locate(''Locator1BrowseOutFaultsWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseOutFaultsWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsWarrantyParts.locate(''Locator1BrowseOutFaultsWarrantyParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseOutFaultsWarrantyParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsWarrantyParts.cl(''BrowseOutFaultsWarrantyParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsWarrantyParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseOutFaultsWarrantyParts','SBO_OutFaultParts',sofp:FaultKey) !sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sofp:fault = p_web.GetValue('sofp:fault')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsWarrantyParts:LookupField')) = sofp:fault and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sofp:fault = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseOutFaultsWarrantyParts','SBO_OutFaultParts',sofp:FaultKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseOutFaultsWarrantyParts','SBO_OutFaultParts',sofp:FaultKey)
  TableQueue.Id[1] = sofp:sessionID
  TableQueue.Id[2] = sofp:partType
  TableQueue.Id[3] = sofp:fault

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseOutFaultsWarrantyParts;if (btiBrowseOutFaultsWarrantyParts != 1){{var BrowseOutFaultsWarrantyParts=new browseTable(''BrowseOutFaultsWarrantyParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseOutFaultsWarrantyParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsWarrantyParts.applyGreenBar();btiBrowseOutFaultsWarrantyParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(WARPARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sofp:sessionID',p_web.GetValue('sofp:sessionID'))
  p_web.SetSessionValue('sofp:partType',p_web.GetValue('sofp:partType'))
  p_web.SetSessionValue('sofp:fault',p_web.GetValue('sofp:fault'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sofp:sessionID = p_web.GSV('sofp:sessionID')
  sofp:partType = p_web.GSV('sofp:partType')
  sofp:fault = p_web.GSV('sofp:fault')
  loc:result = p_web._GetFile(SBO_OutFaultParts,sofp:FaultKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:sessionID)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_OutFaultParts)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SBO_OutFaultParts)
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_sofp:fault_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:fault,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_sofp:description_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_sofp:level_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:level,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(WARPARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(WARPARTS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.WarrantyCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0

                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END


                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'W'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'W'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
