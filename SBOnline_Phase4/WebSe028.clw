

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ABWMFPAR.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE028.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE049.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE050.INC'),ONCE        !Req'd for module callout resolution
                     END


XFiles PROCEDURE                                           ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,138,56,18),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,138,56,18),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:RTNORDER.Open                                     ! File RTNORDER used by this procedure, so make sure it's RelationManager is open
  Relate:SBO_GenericFile.Open                              ! File SBO_GenericFile used by this procedure, so make sure it's RelationManager is open
  Relate:SBO_OutFaultParts.Open                            ! File SBO_OutFaultParts used by this procedure, so make sure it's RelationManager is open
  Relate:SBO_OutParts.Open                                 ! File SBO_OutParts used by this procedure, so make sure it's RelationManager is open
  Relate:SMSText.Open                                      ! File SMSText used by this procedure, so make sure it's RelationManager is open
  Relate:STOCKALX.Open                                     ! File STOCKALX used by this procedure, so make sure it's RelationManager is open
  Relate:STOCKRECEIVETMP.Open                              ! File STOCKRECEIVETMP used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK_ALIAS.Open                                  ! File STOCK_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEAC2.Open                                     ! File TRADEAC2 used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles',QuickWindow)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RTNORDER.Close
    Relate:SBO_GenericFile.Close
    Relate:SBO_OutFaultParts.Close
    Relate:SBO_OutParts.Close
    Relate:SMSText.Close
    Relate:STOCKALX.Close
    Relate:STOCKRECEIVETMP.Close
    Relate:STOCK_ALIAS.Close
    Relate:TRADEAC2.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles',QuickWindow)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


CreditNoteRequest PROCEDURE(<NetWebServerWorker p_web>)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
locCreditNoteNumber  LONG
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
locLineCost          REAL
locWhoPrinted        STRING(60)
locTotalLines        LONG
locTotalLineCount    REAL
locItemCost          REAL
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(RTNORDER)
                       PROJECT(rtn:CreditNoteRequestNumber)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:InvoiceNumber)
                       PROJECT(rtn:OrderNumber)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:QuantityReturned)
                       PROJECT(rtn:ReturnType)
                     END
report               REPORT,AT(396,2729,7521,6302),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,979,7521,1385),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,583),USE(?string27),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,792),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,792),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,792),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,583),USE(locWhoPrinted),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,375),USE(tra:StoresAccount),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n_8),AT(5885,156),USE(rtn:CreditNoteRequestNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Credit Note No:'),AT(5000,156),USE(?string27:5),TRN,FONT(,8,,)
                         STRING('Account No:'),AT(5000,365),USE(?string27:4),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(5000,1000),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1000),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1000),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1000,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(EndOfReport)
detail                   DETAIL,AT(,,,177),USE(?detailband)
                           STRING(@n_8),AT(104,0),USE(rtn:QuantityReturned),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@s20),AT(677,0),USE(rtn:PartNumber),TRN,LEFT(1),FONT(,7,,)
                           STRING(@s25),AT(1771,0,1615,156),USE(rtn:Description),TRN,LEFT(1),FONT(,7,,)
                           STRING(@n_8),AT(3281,0),USE(rtn:OrderNumber),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@n_8),AT(3854,0),USE(rtn:InvoiceNumber),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@s25),AT(4531,0),USE(rtn:ReturnType),TRN,LEFT(1),FONT(,7,,)
                           STRING(@n14.2),AT(5833,0),USE(locItemCost),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@n14.2),AT(6563,0),SUM(locTotalLineCount),USE(locLineCost),TRN,RIGHT(1),FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,333),USE(?unnamed:2)
                           LINE,AT(156,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total No Of Lines:'),AT(156,104),USE(?String40),TRN,FONT(,8,,FONT:bold)
                           STRING(@n_8),AT(1250,104),CNT,RESET(endofreportbreak),USE(locTotalLines),TRN,FONT(,8,,)
                           STRING('Total:'),AT(5938,104),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(6438,104),USE(locTotalLineCount),TRN,RIGHT,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(375,9073,7521,2292),USE(?unnamed:4)
                         TEXT,AT(260,52,7031,781),USE(stt:Text),TRN,FONT(,8,,)
                         BOX,AT(156,885,7240,156),USE(?Box1),COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         STRING('AUTHORISATION'),AT(3333,885),USE(?String46),TRN,FONT(,8,,FONT:bold)
                         STRING('Franchisee/Delegate'),AT(833,1042),USE(?String46:2),TRN,FONT(,8,,FONT:bold)
                         STRING('National Stores Representative'),AT(2917,1042),USE(?String46:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Logistics Manager'),AT(5781,1042),USE(?String46:4),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(156,1042,7240,208),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(156,1250,7240,260),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name:'),AT(208,1250),USE(?String46:5),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(156,1510,7240,469),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(5156,885,0,1302),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING('Name:'),AT(5208,1250),USE(?String46:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Signature:'),AT(208,1771),USE(?String46:8),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(156,1979,7240,208),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Signature:'),AT(2552,1771),USE(?String46:9),TRN,FONT(,8,,FONT:bold)
                         STRING('Signature:'),AT(5208,1771),USE(?String46:10),TRN,FONT(,8,,FONT:bold)
                         STRING('Date:'),AT(208,1979),USE(?String46:11),TRN,FONT(,8,,FONT:bold)
                         STRING('Date:'),AT(2552,1979),USE(?String46:12),TRN,FONT(,8,,FONT:bold)
                         STRING('Date:'),AT(5208,1979),USE(?String46:13),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(2500,885,0,1302),USE(?Line2),COLOR(COLOR:Black)
                         STRING('Name:'),AT(2552,1250),USE(?String46:6),TRN,FONT(,8,,FONT:bold)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,4167,260),USE(address:CompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('CREDIT NOTE REQUEST'),AT(4583,0,2760,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(375,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part No'),AT(677,2083),USE(?strPartNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(1771,2083),USE(?strDescription),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Sale No'),AT(3333,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Inv No'),AT(4010,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Return Type'),AT(4531,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(6031,2083),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6771,2083),USE(?string25:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v6.30
! CW Template version    v6.3
! CW Version             6300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('CreditNoteRequest')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RTNORDER.Open
  Relate:STANTEXT.Open
  Relate:TRADEACC.Open
  Access:USERS.UseFile
  DO BindFileFields
  locCreditNoteNumber = p_web.GSV('rtn:CreditNoteRequestNumber')
  
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(RTNORDER)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(RTNORDER,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      SET(rtn:CreditNoteNumberKey)
      Process:View{Prop:Filter} = |
      'rtn:CreditNoteRequestNumber = locCreditNoteNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
      
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        IF (rtn:ExchangeOrder)
            locItemCost = rtn:ExchangePrice
            SETTARGET(REPORT)
            ?strPartNumber{prop:Text} = 'Model Number'
            ?strDescription{prop:Text} = 'IMEI Number'
            SETTARGET()
        ELSE
            locItemCost = rtn:PurchaseCost
        END
        
        locLineCost = rtn:QuantityReturned * locItemCost
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RTNORDER,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:RTNORDER.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
    If Not p_web &= Null
      p_web.NoOp()
    End
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RTNORDER')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  IF (Access:USERS.TryFetch(use:Password_Key))
  END
  
  Access:TRADEACC.Clearkey(tra:SiteLocationKey)
  tra:SiteLocation  = use:Location
  If Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign
      !Found
      address:CompanyName     = tra:Company_Name
      address:AddressLine1    = tra:Address_Line1
      address:AddressLine2    = tra:Address_Line2
      address:AddressLine3    = tra:Address_Line3
      address:Postcode        = tra:Postcode
      address:TelephoneNumber = tra:Telephone_Number
      address:FaxNumber       = tra:Fax_Number
      address:EmailAddress    = tra:EmailAddress
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locWhoPrinted = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'CREDIT NOTE REQUEST'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  SYSTEM{PROP:PrintMode} = 3
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  report{PROPPRINT:Extend}=True
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Credit Note Request'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  END
  LocalAttribute.Init(report)
  Do SetStaticControlsAttributes
  



GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('locCreditNoteNumber',locCreditNoteNumber)
  BIND(rtn:RECORD)
  BIND(stt:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','CreditNoteRequest','CreditNoteRequest','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End


WayBillDespatch PROCEDURE(<NetWebServerWorker p_web>)
DespatchType         STRING(3)
save_jea_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_xca_id          USHORT,AUTO
save_waj_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:Courier          STRING(30)
tmp:ConsignmentNumber STRING(30)
tmp:DespatchBatchNumber STRING(30)
tmp:IMEI             STRING(30)
tmp:Accessories      STRING(255)
FromAddressGroup     GROUP,PRE(from)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(60)
EmailAddress         STRING(255)
                     END
ToAddressGroup       GROUP,PRE(to)
AccountNumber        STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(30)
EmailAddress         STRING(255)
                     END
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(31)
Bar_Code_Temp        CSTRING(31)
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
tmp:Comments         STRING(255)
tmp:SecurityPackNumber STRING(30)
tmp:CustomerAddress  STRING(3)
tmp:WayBillID        LONG
tmp:Exchanged        STRING(1)
tmp:DateDespatched   DATE
tmp:TimeDespatched   TIME
tmp:HeadAccountNo    STRING(15)
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
report               REPORT,AT(448,4063,7771,5406),PAPER(PAPER:LETTER),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,469,7802,3219),USE(?unnamed)
                         STRING('Courier:'),AT(4896,781),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,781,3260,180),USE(tmp:Courier),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),TRN,FONT(,8,,)
                         STRING(@d6),AT(6042,365),USE(tmp:DateDespatched),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6042,573),USE(tmp:TimeDespatched),TRN,FONT(,8,,FONT:bold)
                         STRING('From Sender:'),AT(104,104),USE(?String16),TRN,FONT(,8,,)
                         STRING('WAYBILL REJECTION'),AT(5365,52),USE(?WaybillRejection),TRN,HIDE,FONT(,16,,FONT:bold)
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),TRN,FONT(,8,,)
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),TRN,FONT(,8,,)
                         STRING('?PP?'),AT(6563,990,375,208),USE(?CPCSPgOfPgStr),FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6354,990),USE(?String48),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,990),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),TRN,FONT(,8,,)
                         STRING(@s60),AT(938,1198),USE(from:ContactName),TRN,FONT(,8,,FONT:bold)
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,1354),USE(?String16:4),TRN,FONT(,8,,)
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),TRN,CENTER,FONT(,12,,FONT:bold)
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,260,4771,281),USE(from:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(3490,1979,3906,260),USE(Bar_Code_Temp),CENTER,FONT('C39 High 12pt LJ3',12,COLOR:Black,,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING(@s30),AT(104,469,3906,180),USE(from:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,625,3906,180),USE(from:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),TRN,FONT(,8,,)
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,781,3906,180),USE(from:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3490,2292,3906,260),USE(tmp:ConsignmentNumber),TRN,CENTER,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(4323,2969,3385,208),USE(glo:ErrorText),TRN,RIGHT,FONT(,8,,FONT:bold)
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
endofreportbreak       BREAK(EndOfReport),USE(?BREAK1)
detail                   DETAIL,AT(,,,240),USE(?detailband)
                           STRING(@s15),AT(104,0),USE(tmp:Ref_Number,,?job:Ref_Number:2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s18),AT(1250,0),USE(tmp:IMEI),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(5417,0,1875,156),USE(tmp:Accessories),TRN,FONT(,7,,),RESIZE
                           STRING(@s1),AT(7396,0),USE(tmp:Exchanged),TRN,FONT(,8,,)
                           STRING(@s15),AT(4375,0),USE(tmp:SecurityPackNumber),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s30),AT(2552,0,1771,156),USE(job:Order_Number),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,385),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number of Items On Waybill :'),AT(260,104),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(2344,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
SundryDetail             DETAIL,AT(,,,219),USE(?SundryDetail)
                           STRING(@s30),AT(146,0),USE(was:Description),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2344,0),USE(was:Quantity),TRN,FONT(,8,,)
                         END
CNRDetail                DETAIL,AT(,,,219),USE(?CNRDetail)
                           STRING(@s30),AT(146,0),USE(wcr:PartNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2344,0),USE(wcr:Description),TRN,FONT(,8,,)
                           STRING(@n_8),AT(4427,0),USE(wcr:Quantity),TRN,FONT(,8,,)
                         END
SundryNotes              DETAIL,AT(,,,1188),USE(?SundryNotes)
                           TEXT,AT(156,52,3594,729),USE(way:UserNotes),TRN,FONT(,8,,),RESIZE
                           STRING(@s30),AT(3844,52),USE(way:SecurityPackNumber),TRN,LEFT,FONT(,8,,)
                           LINE,AT(104,833,7292,0),USE(?Line6:3),COLOR(COLOR:Black)
                           STRING('Description'),AT(146,885),USE(?Description),TRN,FONT(,7,,)
                           STRING('Quantity'),AT(2344,885),USE(?Quantity),TRN,FONT(,7,,)
                           LINE,AT(104,1094,7292,0),USE(?Line6:4),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(385,9479,7781,1229),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,1042),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,448,7792,10521),USE(?unnamed:3)
                         STRING('Part Number'),AT(208,3333),USE(?crnPartNumber),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2406,3333),USE(?crnDescription),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),TRN,FONT(,7,,)
                         STRING('I.M.E.I. No'),AT(1313,3333),USE(?IMEINo),TRN,FONT(,7,,)
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),TRN,FONT(,7,,)
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),TRN,FONT(,7,,)
                         STRING('Quantity'),AT(4385,3333),USE(?crnQuantity),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),TRN,FONT(,7,,)
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v6.30
! CW Template version    v6.3
! CW Version             6300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WayBillDespatch')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHACC.Open
  Relate:STANTEXT.Open
  Relate:WAYBILLJ.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:EXCHANGE.UseFile
  Access:LOAN.UseFile
  Access:USERS.UseFile
  Access:JOBSE.UseFile
  Access:WAYBILLS.UseFile
  Access:JOBACC.UseFile
  Access:WAYSUND.UseFile
  Access:WAYCNR.UseFile
  DO BindFileFields
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      recordstoprocess    = Records(glo:q_jobnumber)
      DO OpenReportRoutine
      
    OF Event:Timer
        CASE way:WaybillID !============================================
        OF 400 OROF 401 ! Credit Note Request
            NewProcedure# = 1
            SetTarget(Report)
            ?JobNo{prop:Hide} = 1
            ?IMEINo{prop:Hide} = 1
            ?OrderNo{prop:Hide} = 1
            ?Accessories{prop:Hide} = 1
            ?SecurityPackNo{prop:Hide} = 1
            ?crnPartNumber{prop:Hide} = 0
            ?crnDescription{prop:Hide} = 0
            ?crnQuantity{prop:Hide} = 0
            tmp:DateDespatched = way:TheDate
            tmp:TimeDespatched = way:TheTime
            tmp:Courier        = way:Courier
            IF (way:WaybillID = 401)
                ?crnPartNumber{prop:Text} = 'Model Number'
                ?crnDescription{prop:Text} = 'IMEI Number'
                ?crnQuantity{prop:Hide} = 1
                ?wcr:Quantity{prop:Hide} = 1
            END
            SetTarget()
        
            first# = 1
            Access:WAYCNR.Clearkey(wcr:EnteredKey)
            wcr:WAYBILLSRecordNumber = way:RecordNumber
            Set(wcr:EnteredKey,wcr:EnteredKey)
            Loop ! Begin Loop
                If Access:WAYCNR.Next()
                    Break
                End ! If Access:WAYSUND.Next()
                If wcr:WAYBILLSRecordNumber <> way:RecordNumber
                    Break
                End ! If was:WAYBILLSRecordNumber <> way:RecordNumber
                Print(rpt:CNRDetail)
                count_temp += 1
            End ! Loop
        OF 300 !Sunry Waybill
            SetTarget(Report)
            ?JobNo{prop:Text} = 'User Notes'
            ?IMEINo{prop:Hide} = True
            ?OrderNo{prop:Hide} = True
            ?Accessories{prop:Hide} = True
            tmp:DateDespatched = way:TheDate
            tmp:TimeDespatched = way:TheTime
            tmp:Courier        = way:Courier
            SetTarget()
            Print(rpt:SundryNotes)
        
            Access:WAYSUND.Clearkey(was:EnteredKey)
            was:WAYBILLSRecordNumber = way:RecordNumber
            Set(was:EnteredKey,was:EnteredKey)
            Loop ! Begin Loop
                If Access:WAYSUND.Next()
                    Break
                End ! If Access:WAYSUND.Next()
                If was:WAYBILLSRecordNumber <> way:RecordNumber
                    Break
                End ! If was:WAYBILLSRecordNumber <> way:RecordNumber
                Print(rpt:SundryDetail)
            End ! Loop
        
        ELSE
            !New WayBill Procedure
            NewProcedure# = 0
            Save_waj_ID = Access:WAYBILLJ.SaveFile()
            Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
            waj:WayBillNumber = p_web.GSV('Waybill:ConsignmentNumber')
            Set(waj:JobNumberKey,waj:JobNumberKey)
            Loop
                If Access:WAYBILLJ.NEXT()
                   Break
                End !If
                If waj:WayBillNumber <> p_web.GSV('Waybill:ConsignmentNumber')     |
                    Then Break.  ! End If
                !Write the Date Despatched on the report - L879 (DBH: 17-07-2003)
        !                Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        !                way:WayBillNumber = func:ConsignmentNumber
        !                If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                    !Found
                    tmp:DateDespatched  = way:TheDate
                    tmp:TimeDespatched  = way:TheTime
        !                Else !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !                    !Error
        !                End !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        
                tmp:Exchanged = ''
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = waj:JobNumber
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber   = job:Ref_Number
                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Found
        
                    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = wob:HeadAccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
        
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
                    tmp:Ref_Number  = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
        
                    ! -----------------------------------------------------------------------------
                    ! VP115 / VP120  21st NOV 02 - must display correct IMEI number
                    case waj:WayBillNumber
                        of job:Exchange_Consignment_Number
                            ! Exchange waybill
                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                            xch:Ref_Number  = job:Exchange_Unit_Number
                            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key)
                                tmp:IMEI = waj:IMEINumber
                            else
                                ! Display exchange unit IMEI number
                                tmp:IMEI = xch:ESN
                            end
                        else
                            tmp:IMEI = waj:IMEINumber
                            If job:Exchange_Unit_Number <> 0
                                tmp:Exchanged = 'E'
                            End !If job:Exchange_Unit_Number <> 0
                    end
                    ! -----------------------------------------------------------------------------
        
                    !tmp:IMEI        = waj:IMEINumber
                    tmp:SecurityPackNumber  = waj:SecurityPackNumber
                    tmp:Accessories = ''
                    !Only print accessories, if it's a job, and there is no accessories attached
                    !Or it's an exchange unit being despatched
        
                    If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
                        way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
                        Save_jac_ID = Access:JOBACC.SaveFile()
                        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                        jac:Ref_Number = job:Ref_Number
                        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                        Loop
                            If Access:JOBACC.NEXT()
                               Break
                            End !If
                            If jac:Ref_Number <> job:Ref_Number      |
                                Then Break.  ! End If
                            ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
                            If way:WaybillType = 0 Or way:WaybillType = 1
                            ! End (DBH 08/03/2007) #8703
                                !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
                                If jac:Attached <> True
                                    Cycle
                                End !If jac:Attached <> True
                            End ! If way:WaybillType <> 0 And way:WaybillType <> 1
        
                            If tmp:Accessories = ''
                                tmp:Accessories = Clip(jac:Accessory)
                            Else !If tmp:Accessories = ''
                                tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                            End !If tmp:Accessories = ''
                        End !Loop
                        Access:JOBACC.RestoreFile(Save_jac_ID)
        
                    End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
                    !added by Paul 26/04/2010 - log no 10546
                    If clip(glo:Notes_Global) = 'OBF' then
                        !need to check if the new OBF address is filled in
                        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                        tra:Account_Number  = p_web.GSV('Waybill:ToAccountNumber')
                        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            If clip(tra:OBFAddress1) <> '' then
                                !address is filled in
                                SetTarget(Report)
                                to:CompanyName      = clip(tra:OBFCompanyName)
                                to:AddressLine1     = clip(tra:OBFAddress1)
                                to:AddressLine2     = clip(tra:OBFAddress2)
                                to:AddressLine3     = clip(tra:OBFSuburb)
                                to:TelephoneNumber  = clip(tra:OBFContactNumber)
                                to:ContactName      = clip(tra:OBFContactName)
                                to:EmailAddress     = clip(tra:OBFEmailAddress)
                                Settarget()
                            End
                        End
                    End
                    Print(rpt:Detail)
                    NewProcedure# = 1
                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            End !Loop
            Access:WAYBILLJ.RestoreFile(Save_waj_ID)
        END   !============================================
            If NewProcedure# = 0
            ! Don't really know the despatch date with any accuracy.
            ! So will show date printed on the report instead - L879 (DBH: 17-07-2003)
                tmp:DateDespatched = Today()
                tmp:TimeDespatched = Clock()
                SetTarget(Report)
                ?DespatchDate{prop:Text} = 'Date Printed:'
                ?DespatchTime{prop:Text} = 'Time Printed:'
                SetTarget()
        
                Free(print_group)
                Clear(print_group)
                setcursor(cursor:wait)
        
                If tmp:WaybillID <> 0
                    Loop x# = 1 To Records(glo:q_JobNumber)
                        Get(glo:Q_JobNumber, x#)
                        RecordsProcessed += 1
                        Do DisplayProgress
        
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = glo:Job_Number_Pointer
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            ! Found
                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:RefNumber  = job:Ref_Number
                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Found
        
                            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Error
                            End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
                            Access:WEBJOB.Clearkey(wob:RefNumberKey)
                            wob:RefNumber   = job:Ref_Number
                            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            ! Found
        
                            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            ! Error
                            End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                            tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
        
                            ! What is being despatched?
                            job# = 0
                            exc# = 0
                            loa# = 0
        
                            Case p_web.GSV('Waybill:ConsignmentNumber')
                            Of job:Incoming_Consignment_Number
                                job#                   = 1
                                tmp:SecurityPackNumber = jobe:InSecurityPackNo
                            Of job:Exchange_Consignment_Number
                                exc#                   = 1
                                tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                            Of job:Loan_Consignment_Number
                                loa#                   = 1
                                tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                            Of job:Consignment_Number
                                job#                   = 1
                                tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                            Of wob:ExcWaybillNumber
                                exc#                   = 1
                                tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                            Of wob:LoaWayBillNumber
                                loa#                   = 1
                                tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                            Of wob:JobWayBillNumber
                                job#                   = 1
                                tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                            End ! Case func:ConsignmentNumber
        
                            If job#
                                tmp:Accessories = ''
                                Save_jac_ID     = Access:JOBACC.SaveFile()
                                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                                jac:Ref_Number = job:Ref_Number
                                Set(jac:Ref_Number_Key, jac:Ref_Number_Key)
                                Loop
                                    If Access:JOBACC.NEXT()
                                        Break
                                    End ! If
                                    If jac:Ref_Number <> job:Ref_Number      |
                                        Then Break   ! End If
                                    End ! If
                                    If tmp:Accessories = ''
                                        tmp:Accessories = Clip(jac:Accessory)
                                    Else ! If tmp:Accessories = ''
                                        tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                                    End ! If tmp:Accessories = ''
                                End ! Loop
                                Access:JOBACC.RestoreFile(Save_jac_ID)
                                tmp:IMEI    = job:ESN
        
                            End ! If job#
        
                            If exc#
                                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                xch:Ref_Number  = job:Exchange_Unit_Number
                                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    Save_xca_ID = Access:EXCHACC.SaveFile()
                                    Access:EXCHACC.ClearKey(xca:Ref_Number_Key)
                                    xca:Ref_Number = xch:Ref_Number
                                    Set(xca:Ref_Number_Key, xca:Ref_Number_Key)
                                    Loop
                                        If Access:EXCHACC.NEXT()
                                            Break
                                        End ! If
                                        If xca:Ref_Number <> xch:Ref_Number      |
                                            Then Break   ! End If
                                        End ! If
                                        If tmp:Accessories = ''
                                            tmp:Accessories = Clip(xca:Accessory)
                                        Else ! If tmp:Accessories = ''
                                            tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(xca:Accessory)
                                        End ! If tmp:Accessories = ''
        
                                    End ! Loop
                                    Access:EXCHACC.RestoreFile(Save_xca_ID)
                                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                ! Error
        
                                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                tmp:IMEI    = xch:ESN
        
                            End ! If exc#
        
                            If loa#
                                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                loa:Ref_Number  = job:Loan_Unit_Number
                                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    tmp:IMEI    = loa:ESN
                                Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                ! Error
                                End ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                            End ! If loa#
                            Print(rpt:Detail)
                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        ! Error
                        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    End ! Loop x# = 1 To Records(glo:q_JobNumber)
                Else ! tmp:WaybillID <> 0
        
                    Loop x# = 1 To Records(glo:q_jobnumber)
                        Get(glo:q_jobnumber, x#)
                        recordsprocessed += 1
                        Do DisplayProgress
        
        
                        access:jobs.clearkey(job:ref_number_key)
                        job:ref_number = glo:job_number_pointer
                        if access:jobs.fetch(job:ref_number_key) = Level:Benign
                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:RefNumber  = job:Ref_Number
                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Found
                            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Error
                            End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
        
                        !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                        ! 26 Aug 2002 John
                        !
                            tmp:Ref_number = job:Ref_number
                            if glo:WebJob then
        
                                DespatchType = jobe:DespatchType
        
                            ! Change the tmp ref number if you
                            ! can Look up from the wob file
                                access:Webjob.clearkey(wob:RefNumberKey)
                                wob:RefNumber = job:Ref_number
                                if access:Webjob.fetch(wob:refNumberKey) = level:benign then
                                    tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                                end ! if
        
                                If p_web.GSV('Waybill:ToType') = 'CUS'
                                    SetTarget(Report)
                                    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                                    sub:Account_Number  = job:Account_Number
                                    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                        ! Found
                                        to:CompanyName     = sub:Company_Name
                                        to:AddressLine1    = sub:Address_Line1
                                        to:AddressLine2    = sub:Address_Line2
                                        to:AddressLine3    = sub:Address_Line3
                                        to:TelephoneNumber = sub:Telephone_Number
                                        to:ContactName     = sub:Contact_Name
                                        to:EmailAddress    = sub:EmailAddress
                                        if sub:UseCustDespAdd = 'YES'
                                            to:CompanyName     = job:Company_Name
                                            to:AddressLine1    = job:Address_Line1
                                            to:AddressLine2    = job:Address_Line2
                                            to:AddressLine3    = job:Address_Line3
                                            to:TelephoneNumber = job:Telephone_Number
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        else
                                            to:CompanyName     = job:Company_Name_Delivery
                                            to:AddressLine1    = job:Address_Line1_Delivery
                                            to:AddressLine2    = job:Address_Line2_Delivery
                                            to:AddressLine3    = job:Address_Line3_Delivery
                                            to:TelephoneNumber = job:Telephone_Delivery
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        end ! if
                                    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                    ! Error
                                    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
        
                                    settarget()
                                End ! If
        
                            ELSE
                                Despatchtype = job:Despatch_type
        
                                ! Check to see if it *IS* an RRC!
                                Access:SubTracc.ClearKey(sub:Account_Number_Key)
                                sub:Account_Number = p_web.GSV('Waybill:ToAccountNumber')
                                IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                            ! Error!
                            ! Don't need to change location here, it should be hanled by the despatch procedures
                            ! LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
                                    if Records(glo:q_jobnumber) = 1 ! Individual despatch
                                        settarget(Report)
                                        case tmp:CustomerAddress
                                        of 'CUS' ! Customer address
                                            to:CompanyName     = job:Company_Name
                                            to:AddressLine1    = job:Address_Line1
                                            to:AddressLine2    = job:Address_Line2
                                            to:AddressLine3    = job:Address_Line3
                                            to:TelephoneNumber = job:Telephone_Number
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        of 'DEL' ! Delivery address
                                            to:CompanyName     = job:Company_Name_Delivery
                                            to:AddressLine1    = job:Address_Line1_Delivery
                                            to:AddressLine2    = job:Address_Line2_Delivery
                                            to:AddressLine3    = job:Address_Line3_Delivery
                                            to:TelephoneNumber = job:Telephone_Delivery
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        end ! case
                                        settarget()
                                    end ! if
                                ELSE
                                    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                                    tra:Account_Number = sub:Main_Account_Number
                                    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                                        ! Error!
                                        if despatchType = 'JOB' then
        !                                            If glo:Select21 <> 'REPRINT'
        !                                                LocationChange(Clip(GETINI('RRC', 'DespatchToCustomer',, CLIP(PATH()) & '\SB2KDEF.INI')))
        !                                            End ! If glo:Select21 <> 'REPRINT'
        
                                            if Records(glo:q_jobnumber) = 1 and tmp:WaybillID = 0! Individual despatch
                                                settarget(Report)
                                                case tmp:CustomerAddress
                                                of 'CUS' ! Customer address
                                                    to:CompanyName     = job:Company_Name
                                                    to:AddressLine1    = job:Address_Line1
                                                    to:AddressLine2    = job:Address_Line2
                                                    to:AddressLine3    = job:Address_Line3
                                                    to:TelephoneNumber = job:Telephone_Number
                                                    to:ContactName     = ''
                                                    to:EmailAddress    = jobe:EndUserEmailAddress
                                                of 'DEL' ! Delivery address
                                                    to:CompanyName     = job:Company_Name_Delivery
                                                    to:AddressLine1    = job:Address_Line1_Delivery
                                                    to:AddressLine2    = job:Address_Line2_Delivery
                                                    to:AddressLine3    = job:Address_Line3_Delivery
                                                    to:TelephoneNumber = job:Telephone_Delivery
                                                    to:ContactName     = ''
                                                    to:EmailAddress    = jobe:EndUserEmailAddress
                                                end ! case
                                                settarget()
                                            end ! if
                                        end ! if
                                    ELSE
                                        IF tra:RemoteRepairCentre = TRUE
        !                                            if despatchType = 'JOB' And glo:Select21 <> 'REPRINT' then
        !                                                LocationChange(Clip(GETINI('RRC', 'InTransitRRC',, CLIP(PATH()) & '\SB2KDEF.INI')))
        !                                            end ! if
                                        ELSE
                                            if despatchType = 'JOB' then
        !                                                If glo:Select21 <> 'REPRINT'
        !                                                    LocationChange(Clip(GETINI('RRC', 'DespatchToCustomer',, CLIP(PATH()) & '\SB2KDEF.INI')))
        !                                                End ! If glo:Select21 <> 'REPRINT'
                                                if Records(glo:q_jobnumber) = 1 and tmp:WaybillID = 0! Individual despatch
                                                    settarget(Report)
                                                    case tmp:CustomerAddress
                                                    of 'CUS' ! Customer address
                                                        to:CompanyName     = job:Company_Name
                                                        to:AddressLine1    = job:Address_Line1
                                                        to:AddressLine2    = job:Address_Line2
                                                        to:AddressLine3    = job:Address_Line3
                                                        to:TelephoneNumber = job:Telephone_Number
                                                        to:ContactName     = ''
                                                        to:EmailAddress    = jobe:EndUserEmailAddress
                                                    of 'DEL' ! Delivery address
                                                        to:CompanyName     = job:Company_Name_Delivery
                                                        to:AddressLine1    = job:Address_Line1_Delivery
                                                        to:AddressLine2    = job:Address_Line2_Delivery
                                                        to:AddressLine3    = job:Address_Line3_Delivery
                                                        to:TelephoneNumber = job:Telephone_Delivery
                                                        to:ContactName     = ''
                                                        to:EmailAddress    = jobe:EndUserEmailAddress
                                                    end ! case
                                                    settarget()
                                                end ! if
                                            end ! if
                                        END ! IF
                                    END ! IF
                                END ! IF
                                Access:Jobs.Update()
                            end ! if
                            !--------------------------------------------------------------
        
                            Case DespatchType
                            Of 'EXC'
                                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                xch:Ref_Number  = job:Exchange_Unit_Number
                                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    Save_xca_ID = Access:EXCHACC.SaveFile()
                                    Access:EXCHACC.ClearKey(xca:Ref_Number_Key)
                                    xca:Ref_Number = xch:Ref_Number
                                    Set(xca:Ref_Number_Key, xca:Ref_Number_Key)
                                    Loop
                                        If Access:EXCHACC.NEXT()
                                            Break
                                        End ! If
                                        If xca:Ref_Number <> xch:Ref_Number      |
                                            Then Break   ! End If
                                        End ! If
                                        If tmp:Accessories = ''
                                            tmp:Accessories = Clip(xca:Accessory)
                                        Else ! If tmp:Accessories = ''
                                            tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(xca:Accessory)
                                        End ! If tmp:Accessories = ''
        
                                    End ! Loop
                                    Access:EXCHACC.RestoreFile(Save_xca_ID)
                                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                ! Error
        
                                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                tmp:IMEI    = xch:ESN
                            Else
                                tmp:Accessories = ''
                                Save_jac_ID     = Access:JOBACC.SaveFile()
                                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                                jac:Ref_Number = job:Ref_Number
                                Set(jac:Ref_Number_Key, jac:Ref_Number_Key)
                                Loop
                                    If Access:JOBACC.NEXT()
                                        Break
                                    End ! If
                                    If jac:Ref_Number <> job:Ref_Number      |
                                        Then Break   ! End If
                                    End ! If
                                    If tmp:Accessories = ''
                                        tmp:Accessories = Clip(jac:Accessory)
                                    Else ! If tmp:Accessories = ''
                                        tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                                    End ! If tmp:Accessories = ''
                                End ! Loop
                                Access:JOBACC.RestoreFile(Save_jac_ID)
                                tmp:IMEI    = job:ESN
        
                            End ! Case job:Despatch_Type
                            !--------------------------------------------------------------
                            If DespatchType = 'LOA'
        
                                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                loa:Ref_Number  = job:Loan_Unit_Number
                                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    tmp:IMEI    = loa:ESN
        
                                Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                ! Error
                                End ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                            End ! If job:Despatch_Type = 'LOA'
                            !--------------------------------------------------------------
                            If job:Despatch_Type <> ''
                                Despatch_Type_Temp = job:Despatch_Type
                            Else ! If job:Despatch_Type <> ''
                                Despatch_Type_Temp = 'JOB'
                            End ! If job:Despatch_Type <> ''
                            !--------------------------------------------------------------
        
        
                            ! Security Pack Number
                            If glo:WebJob
                                ! Waybill Generation
                                Case glo:Select20
                                Of 'WAYREPRINTJTOARC'
                                    tmp:SecurityPackNumber = jobe:InSecurityPackNo
                                Of 'WAYREPRINTJTOCUST'
                                    tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                Of 'WAYREPRINTE'
                                    tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                Else
                                    ! j - change this if it is a despatch from the RRC
                                    Case jobe:DespatchType
                                    Of 'EXC'
                                        tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                    Of 'LOA'
                                        tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                                    Of 'JOB'
                                        tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                    Else
                                        tmp:SecurityPackNumber = jobe:InSecurityPackNo
                                    End ! Case job:Despatch_Type
                                End ! Case glo:Select21
        
                            Else ! If glo:WebJob
                                ! Waybill Generation
                                Case glo:Select20
                                Of 'WAYREPRINTJTORRC'
                                    tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                Of 'WAYREPRINTE'
                                    tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                Of 'WAYREPRINTJTOCUST'
                                    tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                Else
        
                                    Case job:Despatch_Type
                                    Of 'EXC'
                                        tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                    Of 'LOA'
                                        tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                                    Else
                                        tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                    End ! Case job:Despatch_Type
                                End ! Case glo:Select21
        
                            End ! If glo:WebJob
                            Print(rpt:Detail)
                        !--------------------------------------------------------------
                        end! if access:jobs.fetch(job:ref_number_key) = Level:Benign
                    End! Loop x# = 1 To Records(glo:q_jobnumber)
        
                End ! tmp:WaybillID <> 0
                count_temp = Records(glo:q_jobnumber)
                Print(rpt:totals)
        
                setcursor()
            End ! NewProcedure# = 0
        
        
        
        LocalResponse = RequestCompleted
        BREAK
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHACC.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  !pass   func:FromAccountNumber - The Account number to appear under "From Sender"
  !       func:FromType          - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ToACcountNumber   - The Account number to appear under "Deliver To"
  !       func:ToType            - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ConsignmentNUmber - Waybill number
  !       func:Courier           - Despatch Courier
  
  !Complete change to waybill prefix
  !change made by Paul 29/09/2009 - log no 11042
  If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
      tmp:HeadAccountNo = clip(GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))
  Else
      !cheat and manually set to 'AA20'
      tmp:HeadAccountNo = 'AA20'
  End !If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
  tmp:Courier = p_web.GSV('Waybill::Courier')
  
  
  !now look up the waybill and check if it was procduced from the head account or not
  BHAddToDebugLog(p_web.GSV('Waybill:ConsignmentNumber'))
  BHAddToDebugLog(p_web.GSV('Waybill:FromAccount'))
  BHAddToDebugLog(p_web.GSV('Waybill:ToAccount'))
  
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = p_web.GSV('Waybill:ConsignmentNumber')
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
      If way:FromAccount = tmp:HeadAccountNo then
          !its been produced from the head account - so attach prefix accordingly
          If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
          Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = 'VDC' & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
          End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
      Else
          !must have been produced from an RRC account - so change the prefix
          !now look up the trade account
  
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = way:FromAccount
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              !now see if the RRC waybill prefix has been filled in
              If clip(tra:RRCWaybillPrefix) <> '' then
                  !Prefix filled in - so use it
                  tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
              Else
                  !Changed By Paul 17/09/09 - log no 11042
                  tmp:ConsignmentNumber = 'VDC' & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
              End !If clip(tra:RRCWaybillPrefix) <> '' then
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      End !If way:AccountNumber = tmp:HeadAccountNo then
  Else
      !error
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  
  
  !Change End
  
  
  
  
  
  
  SYSTEM{PROP:PrintMode} = 3
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = p_web.GSV('locWaybillNumber')
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
  Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
      !Error
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  
  
  
  report{PROPPRINT:Extend}=True
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:User_Code_Key)
  use:password =p_web.GSV('BookingUserCode')
  access:users.fetch(use:User_Code_Key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Barcode Bit
  ! Changing (DBH 04/08/2006) # 6643 - I think Code39 = 1
  !code_temp            = 3
  ! to (DBH 04/08/2006) # 6643
  Code_Temp = 1
  ! End (DBH 04/08/2006) #6643
  option_temp          = 0
  
  Bar_Code_String_Temp = CLIP(tmp:ConsignmentNumber)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  Bar_Code_Temp = '*' & CLIP(tmp:ConsignmentNumber) & '*'
  
  !Work out addresses
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  
  Case p_web.GSV('Waybill:FromType')
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = p_web.GSV('Wayill:FromAccountNumber')
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = tra:Company_Name
              from:AddressLine1       = tra:Address_Line1
              from:AddressLine2       = tra:Address_Line2
              from:AddressLine3       = tra:Address_Line3
              from:TelephoneNumber    = tra:Telephone_Number
              from:ContactName        = tra:Contact_Name
              from:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = p_web.GSV('Wayill:FromAccountNumber')
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = sub:Company_Name
              from:AddressLine1       = sub:Address_Line1
              from:AddressLine2       = sub:Address_Line2
              from:AddressLine3       = sub:Address_Line3
              from:TelephoneNumber    = sub:Telephone_Number
              from:ContactName        = sub:Contact_Name
              from:EmailAddress       = sub:EmailAddress
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      Of 'DEF'
          from:CompanyName        = def:User_Name
          from:AddressLine1       = def:Address_Line1
          from:AddressLine2       = def:Address_Line2
          from:AddressLine3       = def:Address_Line3
          from:TelephoneNumber    = def:Telephone_Number
          from:ContactName        = ''
          from:EmailAddress       = def:EmailAddress
  
  End !func:FromType
  
  Case p_web.GSV('Waybill:ToType')
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = p_web.GSV('Waybill:ToAccountNumber')
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = tra:Account_Number
              to:CompanyName        = tra:Company_Name
              to:AddressLine1       = tra:Address_Line1
              to:AddressLine2       = tra:Address_Line2
              to:AddressLine3       = tra:Address_Line3
              to:TelephoneNumber    = tra:Telephone_Number
              to:ContactName        = tra:Contact_Name
              to:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = p_web.GSV('Waybill:ToAccountNumber')
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = sub:Account_Number
              to:CompanyName        = sub:Company_Name
              to:AddressLine1       = sub:Address_Line1
              to:AddressLine2       = sub:Address_Line2
              to:AddressLine3       = sub:Address_Line3
              to:TelephoneNumber    = sub:Telephone_Number
              to:ContactName        = sub:Contact_Name
              to:EmailAddress       = sub:EmailAddress
              if sub:UseCustDespAdd = 'YES'
                  tmp:CustomerAddress = 'CUS'
              else
                  tmp:CustomerAddress = 'DEL'
              end
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
  End !func:ToType
  
      !New way of working out addressess
      tmp:WayBillID = 0
      Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
      way:WayBillNumber = p_web.GSV('Waybill:ConsignmentNumber')
      If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Found
          If way:WaybillID <> 0
  
              !Is this a rejection. If so, show title - 4285 (DBH: 14-06-2004)
              IF way:WaybillID = 13
                  SetTarget(Report)
                  ?WaybillRejection{prop:Hide} = False
                  SetTarget()
              End !IF way:WaybillID = 13
              tmp:WayBillID = way:WaybillID
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = way:FromAccount
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  from:CompanyName    = tra:Company_Name
                  from:AddressLine1   = tra:Address_Line1
                  from:AddressLine2   = tra:Address_Line2
                  from:AddressLine3   = tra:Address_Line3
                  from:TelephoneNumber= tra:Telephone_Number
                  from:ContactName    = tra:Contact_Name
                  from:EmailAddress   = tra:EmailAddress
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
              ! Inserting (DBH 07/07/2006) # 7149 - PUP to RRC Waybill. Pickup the address from the job
              If way:WayBillID = 20
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number  = way:FromAccount
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      from:CompanyName    = sub:Company_Name
                      from:AddressLine1   = sub:Address_Line1
                      from:AddressLine2   = sub:Address_Line2
                      from:AddressLine3   = sub:Address_Line3
                      from:TelephoneNumber= sub:Telephone_Number
                      from:ContactName    = sub:Contact_Name
                      from:EmailAddress   = sub:EmailAddress
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  ! Insert --- Use the prefix based on the account of the job (DBH: 11/08/2009) #11005
                  if (sub:VCPWaybillPRefix <> '')
                      settarget(report)
                      tmp:ConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
                      settarget()
                  else ! if (sub:VCPWaybillPRefix <> '')
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number    = sub:Main_Account_Number
                      if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          ! Found
                          if (tra:VCPWaybillPrefix <> '')
                              settarget(report)
                              tmp:ConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
                              settarget()
                          end ! if (tra:VCPWaybillPrefix <> '')
                      else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  end ! if (sub:VCPWaybillPRefix <> '')
                  ! end --- (DBH: 11/08/2009) #11005
  
              End ! If way:WayBillID = 20
              ! End (DBH 07/07/2006) #7149
  
              Case way:WayBillID
              Of 1 Orof 5 Orof 6 Orof 11 Orof 12 Orof 13 Orof 20
                  ! Inserting (DBH 19/10/2007) # 9451 - Make allowance for PUP Waybills going back to the customer
                  If way:ToAccount = 'CUSTOMER'
                      !added by Paul 28/04/2010 - Log no 11419
                      !lookup sub account to try and get the contact name
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = way:ToAccount
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:notify then
                        !lookup the head account
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = way:ToAccount
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign then
                              If tra:Use_Delivery_Address = 'YES' then
                                  to:ContactName     = clip(tra:Contact_Name)
                              Else
                                  to:ContactName     = ''
                              End
                          Else
                              to:ContactName     = ''
                          End
                      Else
                          If sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                      End
  
                      to:AccountNumber   = job:Account_Number
                      to:CompanyName     = job:Company_Name
                      to:AddressLine1    = job:Address_Line1
                      to:AddressLine2    = job:Address_Line2
                      to:AddressLine3    = job:Address_Line3
                      to:TelephoneNumber = job:Telephone_Number
  
                      to:EmailAddress    = jobe:EndUserEmailAddress
                  Else ! If way:ToAccount = 'CUSTOMER'
                  ! End (DBH 19/10/2007) #9451
  
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = way:ToAccount
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          to:AccountNumber  = tra:Account_Number
                          to:CompanyName    = tra:Company_Name
                          to:AddressLine1   = tra:Address_Line1
                          to:AddressLine2   = tra:Address_Line2
                          to:AddressLine3   = tra:Address_Line3
                          to:TelephoneNumber= tra:Telephone_Number
                          to:ContactName    = tra:Contact_Name
                          to:EmailAddress   = tra:EmailAddress
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  End ! If way:ToAccount = 'CUSTOMER'
              ! Inserting (DBH 07/07/2006) # 7149 - RRC to PUP Address from the job account
              Of 21 Orof 22 !RRC To PUP
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number  = way:ToAccount
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      to:AccountNumber  = sub:Account_Number
                      to:CompanyName    = sub:Company_Name
                      to:AddressLine1   = sub:Address_Line1
                      to:AddressLine2   = sub:Address_Line2
                      to:AddressLine3   = sub:Address_Line3
                      to:TelephoneNumber= sub:Telephone_Number
                      to:ContactName    = sub:Contact_Name
                      to:EmailAddress   = sub:EmailAddress
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  ! End (DBH 07/07/2006) #7149
              ! Inserting (DBH 05/09/2006) # 7995 - Use default addresses for sundry waybill
              Of 300 !Sundry Waybill. Address covered above
                  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = way:FromAccount
                  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      from:CompanyName    = tra:Company_Name
                      from:AddressLine1   = tra:Address_Line1
                      from:AddressLine2   = tra:Address_Line2
                      from:AddressLine3   = tra:Address_Line3
                      from:TelephoneNumber    = tra:Telephone_Number
                      from:ContactName    = tra:Contact_Name
                      from:EmailAddress   = tra:EmailAddress
                  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  If way:ToAccount = 'OTHER'
                      to:AccountNumber = way:OtherAccountNumber
                      to:CompanyName  = way:OtherCompanyName
                      to:AddressLine1 = way:OtherAddress1
                      to:AddressLine2 = way:OtherAddress2
                      to:AddressLine3 = way:OtherAddress3
                      to:TelephoneNumber = way:OtherTelephoneNO
                      to:ContactName  = way:OtherContactName
                      to:EmailAddress = way:OtherEmailAddress
                  Else ! If way:ToAccount = 'OTHER'
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = way:ToAccount
                      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          to:AccountNumber      = tra:Account_Number
                          to:CompanyName        = tra:Company_Name
                          to:AddressLine1       = tra:Address_Line1
                          to:AddressLine2       = tra:Address_Line2
                          to:AddressLine3       = tra:Address_Line3
                          to:TelephoneNumber    = tra:Telephone_Number
                          to:ContactName        = tra:Contact_Name
                          to:EmailAddress       = tra:EmailAddress
                      Else ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  End ! If way:ToAccount = 'OTHER'
              ! End (DBH 05/09/2006) #7995
              Else
                  Get(glo:q_JobNumber,1)
  
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = glo:Job_Number_Pointer
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
  
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                  sub:Account_Number  = way:ToAccount
                  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      if sub:UseCustDespAdd = 'YES'
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      else
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      end
                  Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
              End ! Case way:WayBillID
          End !If way:WaybillID <> 0
      Else!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  END
  LocalAttribute.Init(report)
  Do SetStaticControlsAttributes
  



GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('GLO:Select1',GLO:Select1)
  BIND(def:RECORD)
  BIND(dep:RECORD)
  BIND(xca:RECORD)
  BIND(xch:RECORD)
  BIND(jac:RECORD)
  BIND(job:RECORD)
  BIND(jobe:RECORD)
  BIND(loa:RECORD)
  BIND(stt:RECORD)
  BIND(sub:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
  BIND(waj:RECORD)
  BIND(way:RECORD)
  BIND(wcr:RECORD)
  BIND(was:RECORD)
  BIND(wob:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','WayBillDespatch','WayBillDespatch','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

MenuStockControl     PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MenuStockControl -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('MenuStockControl')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
  if p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_AccessWindowName','MenuStockControl')
      
    GlobalErrors.SetProcedureName()
    Return
  end
  if p_web.IfExistsValue('_LogoutNow_')
    if p_web.getvalue('_LogoutNow_') = 1
      p_web.SetSessionLevel(0)
      p_web.SetSessionValue('SecwinUserLogin','')
      p_web.SetSessionValue('SecwinUserNumber',0)
      p_web.SetSessionValue('SecwinUserPassword','')
      p_web.SetSessionLoggedIn(0)
    end
  end
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
  p_web.ClearBrowse('MenuStockControl')
  do WebMenus
  do MenuPopups
  do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
MenuPopups  Routine

WebMenus  Routine
  loc:options = ''
  packet = clip(packet) & p_web.jQuery('#' & clip('menu'),'dropDownMenu',loc:options)
  do SendPacket
  packet = clip(packet) & p_web.DivHeader('menu','nt-menu-div ui-corner-br',Net:NoSend) & '<13,10><ul id="'&clip('menu')&'" class="'&clip('nt-menu')&'"><13,10>'
  
        packet = clip(packet) & '<li><a href="#">'&p_web.Translate('Stock Control Options')&'</a>'
        do SendPacket
        do Menu:1
        packet = clip(packet) & '</li><13,10>'
  packet = clip(packet) & '</ul><13,10>' & p_web.divFooter(net:NoSend)
  do SendPacket

Menu:1  Routine
  packet = clip(packet) & '<ul><13,10>'
        packet = clip(packet) & '<li>'
          packet = clip(packet) & '<a href="'&p_web._MakeUrl('BrowseAdjustedWebOrderReceipts?' & clip(''))&'">'&p_web.Translate('Adjusted Web Order Receipts')&'</a>'
        packet = clip(packet) & '</li>'
        packet = clip(packet) & '<li>'
          packet = clip(packet) & '<a href="'&p_web._MakeUrl('BrowseRetailSales?' & clip(''))&'">'&p_web.Translate('Browse Retail Sales')&'</a>'
        packet = clip(packet) & '</li>'
        packet = clip(packet) & '<li>'
          packet = clip(packet) & '<a href="'&p_web._MakeUrl('frmPendingWebOrder?' & clip(''))&'">'&p_web.Translate('Pending Orders')&'</a>'
        packet = clip(packet) & '</li>'
        packet = clip(packet) & '<li>'
          packet = clip(packet) & '<a href="'&p_web._MakeUrl('frmStockReceive?' & clip(''))&'">'&p_web.Translate('Stock Receive Procedure')&'</a>'
        packet = clip(packet) & '</li>'
        packet = clip(packet) & '<li>'
          packet = clip(packet) & '<a href="'&p_web._MakeUrl('PageProcess?' & clip('ProcessType=ExchangeAllocation&ReturnURL=FormBrowseStock&RedirectURL=brwRapidExchangeAllocation'))&'">'&p_web.Translate('Exchange Allocation')&'</a>'
        packet = clip(packet) & '</li>'
        packet = clip(packet) & '<li>'
          packet = clip(packet) & '<a href="'&p_web._MakeUrl('frmStockAllocation?' & clip(''))&'">'&p_web.Translate('Stock Allocation')&'</a>'
        packet = clip(packet) & '</li>'
  packet = clip(packet) & '</ul><13,10>'
  do SendPacket

ShowWait             PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ShowWait -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('ShowWait')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
  do SendPacket
  Do ShowWait
  do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
ShowWait  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<div id="waitframe" class="nt-process"><13,10>'&|
    '<<p class="nt-process-text">Please wait while ServiceBase searches for matching records<<BR /><<br/><13,10>'&|
    'This may take several seconds. . .<13,10>'&|
    '<</p><13,10>'&|
    '<<BR/><<BR/><13,10>'&|
    '<<p><13,10>'&|
    '<<a href="FormBrowseStock">Cancel Process<</a><13,10>'&|
    '<</p><13,10>'&|
    '<</div><13,10>'&|
    '',net:OnlyIfUTF)
