

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE056.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE057.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE058.INC'),ONCE        !Req'd for module callout resolution
                     END


brwStockAllocation   PROCEDURE  (NetWebServerWorker p_web)
locEngineerName      STRING(60)                            !
locStockQuantity     LONG                                  !
locStockAvailable    BYTE                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
stl:JobNumber:IsInvalid  Long
stl:ShelfLocation:IsInvalid  Long
stl:Description:IsInvalid  Long
stl:PartNumber:IsInvalid  Long
locEngineerName:IsInvalid  Long
stl:Quantity:IsInvalid  Long
locStockQuantity:IsInvalid  Long
icnStockAvailable:IsInvalid  Long
icnStatus:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOCKALL)
                      Project(stl:RecordNumber)
                      Project(stl:JobNumber)
                      Project(stl:ShelfLocation)
                      Project(stl:Description)
                      Project(stl:PartNumber)
                      Project(stl:Quantity)
                      Project(stl:PartRecordNumber)
                      Project(stl:PartType)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
STOCK::State  USHORT
USERS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('brwStockAllocation')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('brwStockAllocation:NoForm')
      loc:NoForm = p_web.GetValue('brwStockAllocation:NoForm')
      loc:FormName = p_web.GetValue('brwStockAllocation:FormName')
    else
      loc:FormName = 'brwStockAllocation_frm'
    End
    p_web.SSV('brwStockAllocation:NoForm',loc:NoForm)
    p_web.SSV('brwStockAllocation:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('brwStockAllocation:NoForm')
    loc:FormName = p_web.GSV('brwStockAllocation:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('brwStockAllocation') & '_' & lower(loc:parent)
  else
    loc:divname = lower('brwStockAllocation')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'brwStockAllocation' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','brwStockAllocation')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('brwStockAllocation') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('brwStockAllocation')
      p_web.DivHeader('popup_brwStockAllocation','nt-hidden')
      p_web.DivHeader('brwStockAllocation',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_brwStockAllocation_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_brwStockAllocation_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('brwStockAllocation',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOCKALL,stl:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STL:JOBNUMBER') then p_web.SetValue('brwStockAllocation_sort','3')
    ElsIf (loc:vorder = 'STL:SHELFLOCATION') then p_web.SetValue('brwStockAllocation_sort','2')
    ElsIf (loc:vorder = 'STL:DESCRIPTION') then p_web.SetValue('brwStockAllocation_sort','4')
    ElsIf (loc:vorder = 'STL:PARTNUMBER') then p_web.SetValue('brwStockAllocation_sort','5')
    ElsIf (loc:vorder = 'LOCENGINEERNAME') then p_web.SetValue('brwStockAllocation_sort','6')
    ElsIf (loc:vorder = '+STL:ENGINEER') then p_web.SetValue('brwStockAllocation_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('brwStockAllocation:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('brwStockAllocation:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('brwStockAllocation:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('brwStockAllocation:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'brwStockAllocation'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('brwStockAllocation')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('brwStockAllocation_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 3
  End
  p_web.SetSessionValue('brwStockAllocation_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'stl:JobNumber','-stl:JobNumber')
    Loc:LocateField = 'stl:JobNumber'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:ShelfLocation)','-UPPER(stl:ShelfLocation)')
    Loc:LocateField = 'stl:ShelfLocation'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:Description)','-UPPER(stl:Description)')
    Loc:LocateField = 'stl:Description'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:PartNumber)','-UPPER(stl:PartNumber)')
    Loc:LocateField = 'stl:PartNumber'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'+stl:Engineer','-stl:Engineer')
    Loc:LocateField = 'stl:Engineer'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(stl:Location),+UPPER(stl:Status),+UPPER(stl:PartNumber)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locStockAllocationType') = 'ALL')
  ElsIf (p_web.GSV('locStockAllocationType') <> 'ALL')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stl:JobNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s8')
  Of upper('stl:ShelfLocation')
    loc:SortHeader = p_web.Translate('Shelf Location')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('stl:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('stl:PartNumber')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('locEngineerName')
  OrOf upper('stl:Engineer')
    loc:SortHeader = p_web.Translate('Engineer Name')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s60')
  Of upper('stl:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s8')
  Of upper('locStockQuantity')
    loc:SortHeader = p_web.Translate('Stock Qty')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('brwStockAllocation:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="brwStockAllocation:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="brwStockAllocation:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('brwStockAllocation:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOCKALL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stl:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{brwStockAllocation.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2brwStockAllocation','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwStockAllocation_LocatorPic'),,,'onchange="brwStockAllocation.locate(''Locator2brwStockAllocation'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2brwStockAllocation',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwStockAllocation.locate(''Locator2brwStockAllocation'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="brwStockAllocation_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockAllocation.cl(''brwStockAllocation'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('brwStockAllocation_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="brwStockAllocation_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('brwStockAllocation_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="brwStockAllocation_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','brwStockAllocation',p_web.Translate('Job Number'),'Click here to sort by Job Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','brwStockAllocation',p_web.Translate('Shelf Location'),'Click here to sort by Shelf Location',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','brwStockAllocation',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','brwStockAllocation',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','brwStockAllocation',p_web.Translate('Engineer Name'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','brwStockAllocation',p_web.Translate('Quantity'),'Click here to sort by Quantity',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','brwStockAllocation',p_web.Translate('Stock Qty'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','brwStockAllocation',p_web.Translate('Available'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (p_web.GSV('RapidLocation') = 1) AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','brwStockAllocation',p_web.Translate('Status'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stl:recordnumber',lower(loc:vorder),1,1) = 0 !and STOCKALL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stl:RecordNumber',clip(loc:vorder) & ',' & 'stl:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stl:RecordNumber'),p_web.GetValue('stl:RecordNumber'),p_web.GetSessionValue('stl:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locStockAllocationType') = 'ALL')
      loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''')'
  ElsIf (p_web.GSV('locStockAllocationType') <> 'ALL')
      loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND UPPER(stl:Status) = UPPER(''' & p_web.GSV('locStockAllocationType') & ''')'
  Else
        loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND UPPER(stl:Status) = UPPER(''' & p_web.GSV('locStockAllocationType') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('brwStockAllocation_Filter')
    p_web.SetSessionValue('brwStockAllocation_FirstValue','')
    p_web.SetSessionValue('brwStockAllocation_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOCKALL,stl:RecordNumberKey,loc:PageRows,'brwStockAllocation',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOCKALL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOCKALL,loc:firstvalue)
              Reset(ThisView,STOCKALL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOCKALL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOCKALL,loc:lastvalue)
            Reset(ThisView,STOCKALL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = stl:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          locEngineerName    = stl:Engineer & ' (' & Clip(use:Forename) & ' ' & Clip(use:Surname) & ')'
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          locEngineerName    = stl:Engineer
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      !------------------------------------------------------------------
      !Show the pretty green box to show the stock is available
      locStockAvailable = 0
      locStockQuantity = 0
      
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = stl:PartRefNumber
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
          locStockQuantity   = sto:Quantity_Stock
          If locStockQuantity >= stl:Quantity
              locStockAvailable = 1
          End !If sto:Quantity_Stock <= stl:Quantity
      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          Access:STOCK.ClearKey(sto:Location_Key)
          sto:Location    = use:Location
          sto:Part_Number = stl:PartNumber
          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Found
              locStockQuantity = sto:Quantity_Stock
              If locStockQuantity >= stl:Quantity
                  locStockAvailable = 1
              End !If sto:Quantity_Stock <= stl:Quantity
          Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              locStockQuantity = ''
          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      
      
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stl:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwStockAllocation_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockAllocation.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockAllocation.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockAllocation.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockAllocation.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('brwStockAllocation_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('brwStockAllocation_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1brwStockAllocation','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwStockAllocation_LocatorPic'),,,'onchange="brwStockAllocation.locate(''Locator1brwStockAllocation'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1brwStockAllocation',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwStockAllocation.locate(''Locator1brwStockAllocation'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="brwStockAllocation_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockAllocation.cl(''brwStockAllocation'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('brwStockAllocation_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('brwStockAllocation_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwStockAllocation_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockAllocation.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockAllocation.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockAllocation.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockAllocation.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('brwStockAllocation','STOCKALL',stl:RecordNumberKey) !stl:RecordNumber
    p_web._thisrow = p_web._nocolon('stl:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stl:RecordNumber = p_web.GetValue('stl:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('brwStockAllocation:LookupField')) = stl:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stl:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('brwStockAllocation','STOCKALL',stl:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOCKALL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOCKALL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOCKALL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOCKALL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
          p_web.SSV('RecordSelected',1)
          p_web.SSV('stl:PartNumber',stl:PartNumber)
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:JobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:ShelfLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:PartNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::locEngineerName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::stl:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::locStockQuantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif locStockAvailable = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::icnStockAvailable
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('RapidLocation') = 1) AND  true
          If Loc:Eip = 0
            if false
            elsif stl:Status = 'WEB'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            elsif stl:Status = 'PIK'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            elsif stl:Status = 'PRO'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            elsif stl:status = 'RET'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::icnStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('brwStockAllocation','STOCKALL',stl:RecordNumberKey)
  TableQueue.Id[1] = stl:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btibrwStockAllocation;if (btibrwStockAllocation != 1){{var brwStockAllocation=new browseTable(''brwStockAllocation'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stl:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'brwStockAllocation.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'brwStockAllocation.applyGreenBar();btibrwStockAllocation=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockAllocation')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockAllocation')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockAllocation')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockAllocation')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOCKALL)
  p_web._CloseFile(STOCK)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOCKALL)
  Bind(stl:Record)
  Clear(stl:Record)
  NetWebSetSessionPics(p_web,STOCKALL)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stl:RecordNumber',p_web.GetValue('stl:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stl:RecordNumber = p_web.GSV('stl:RecordNumber')
  loc:result = p_web._GetFile(STOCKALL,stl:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stl:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOCKALL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOCKALL)
! ----------------------------------------------------------------------------------------
value::stl:JobNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:JobNumber_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:JobNumber,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:ShelfLocation   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:ShelfLocation_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:ShelfLocation,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:Description_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:PartNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:PartNumber_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:PartNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locEngineerName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_locEngineerName_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locEngineerName,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:Quantity_'&stl:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:Quantity,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStockQuantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_locStockQuantity_'&stl:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locStockQuantity,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::icnStockAvailable   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif locStockAvailable = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStockAvailable_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStockAvailable_'&stl:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) & p_web.CreateImage('/images/reject.png','','','',,,loc:javascript,,0,'') &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::icnStatus   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('RapidLocation') = 1)
    if false
    elsif stl:Status = 'WEB' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_purple.png','','','',, ,loc:javascript,,0,,)
    elsif stl:Status = 'PIK' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_blue.png','','','',, ,loc:javascript,,0,,)
    elsif stl:Status = 'PRO' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_pink.png','','','',, ,loc:javascript,,0,,)
    elsif stl:status = 'RET' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_red.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_black.png','','','',,,loc:javascript,,0,'') &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stl:RecordNumber',stl:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SaveSessionVars  ROUTINE
     p_web.SSV('locEngineerName',locEngineerName) ! STRING(60)
     p_web.SSV('locStockQuantity',locStockQuantity) ! LONG
     p_web.SSV('locStockAvailable',locStockAvailable) ! BYTE
RestoreSessionVars  ROUTINE
     locEngineerName = p_web.GSV('locEngineerName') ! STRING(60)
     locStockQuantity = p_web.GSV('locStockQuantity') ! LONG
     locStockAvailable = p_web.GSV('locStockAvailable') ! BYTE
frmChangeStockStatus PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockRefNo        LONG                                  !
FoundAnyPart         BYTE                                  !
FoundOrderedPart     BYTE                                  !
locJobNumber         LONG                                  !
RapidQueue           QUEUE,PRE(rapque)                     !
SessionID            LONG                                  !
RecordNumber         LONG                                  !
                     END                                   !
RapidJobQueue        QUEUE,PRE(jobque)                     !
SessionID            LONG                                  !
JobNumber            LONG                                  !
                     END                                   !
                    MAP
AddFaulty               Procedure(String  func:Type)
                    END
FilesOpened     Long
STOFAULT::State  USHORT
USERS::State  USHORT
STOCK::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
ESTPARTS::State  USHORT
STOCKALL::State  USHORT
txtRequestFulfilled:IsInvalid  Long
txtErrorText:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmChangeStockStatus')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmChangeStockStatus_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmChangeStockStatus','')
    p_web.DivHeader('frmChangeStockStatus',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmChangeStockStatus') = 0
        p_web.AddPreCall('frmChangeStockStatus')
        p_web.DivHeader('popup_frmChangeStockStatus','nt-hidden')
        p_web.DivHeader('frmChangeStockStatus',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmChangeStockStatus_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmChangeStockStatus_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmChangeStockStatus',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmChangeStockStatus')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmChangeStockStatus_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmChangeStockStatus'
    end
    p_web.formsettings.proc = 'frmChangeStockStatus'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmChangeStockStatus_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'frmStockAllocation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmChangeStockStatus_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmChangeStockStatus_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmChangeStockStatus_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('STA'))
      p_web.StoreValue('STA')
  END
  
  Access:STOCKALL.Clearkey(stl:RecordNumberKey)
  stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
  Access:STOCKALL.Tryfetch(stl:RecordNumberKey)
  IF (p_web.GSV('STA') = 'PIK' OR (p_web.GSV('STA') = 'PRO'))
  
          !Found
      p_web.SSV('txtErrorText','')
      p_web.SSV('txtRequestFulfilled','The selected part has been updated.')
      If stl:PartNumber = 'EXCH'
          p_web.SSV('txtErrorText','The selected part in an "Exchange Part".')
          p_web.SSV('txtRequestFulfilled','')
      Else !If stl:PartNumber = 'EXCH'
          Error# = 0
  
          If stl:Status = 'WEB'
              p_web.SSV('txtErrorText','The selected part is On Order.')
              p_web.SSV('txtRequestFulfilled','')
              Error# = 1
          End !If stl:Status = 'WEB'
  
          If Error# = 0
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:recordnumberkey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                              !Found
                      par:Status  = p_web.GSV('STA')
                      Access:PARTS.Update()
                  Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                              !Error
                      Error# = 1
                  End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                              !Found
                      wpr:Status  = p_web.GSV('STA')
                      Access:WARPARTS.Update()
                  Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                              !Error
                      Error# = 1
                  End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
              Of 'EST'
                  Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                  epr:Record_Number   = stl:PartRecordNumber
                  If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                              !Found
                      epr:Status = p_web.GSV('STA')
                      Access:ESTPARTS.Update()
                  Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                              !Error
                  End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
              End !Case rapsto:PartType
              If Error# = 0
                  stl:Status = p_web.GSV('STA')
                  Access:STOCKALL.Update()
  
              End !If Error# = 0
  
          End !If Error# = 0
      End !If stl:PartNumber = 'EXCH'
  END
  IF (p_web.GSV('STA') = 'ALL')
      If stl:PartNumber = 'EXCH'
          p_web.SSV('txtErrorText','The selected part in an "Exchange Part".')
          p_web.SSV('txtRequestFulfilled','')
      Else !If stl:PartNumber = 'EXCH'
          Error# = 0
          If stl:Status = 'WEB'
              p_web.SSV('txtErrorText','The selected part in On Order.')
              p_web.SSV('txtRequestFulfilled','')
              Error# = 1
          End !If stl:Status = 'WEB'
          If Error# = 0
              p_web.SSV('txtErrorText','')
              p_web.SSV('txtRequestFulfilled','The selected part has been allocated.')
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:recordnumberkey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = par:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
                              AddFaulty('C')
                          End !If sto:ReturnFaultySpare
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          par:PartAllocated = 1
                          par:WebOrder = 0
                          Access:PARTS.Update()
                      End !If Error# = 0
                  Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                          !Error
                      Error# = 1
                  End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = wpr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
  
                              AddFaulty('W')
                          End !If sto:ReturnFaultySpare
  
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          wpr:PartAllocated = 1
                          wpr:WebOrder = 0
                          Access:WARPARTS.Update()
                      End !If Error# = 0
  
                  Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                          !Error
                      Error# = 1
                  End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
              Of 'EST'
                  Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                  epr:Record_Number   = stl:PartRecordNumber
                  If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = epr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
  
                              AddFaulty('E')
                          End !If sto:ReturnFaultySpare
  
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          epr:PartAllocated = 1
                          Access:ESTPARTS.Update()
                      End !If Error# = 0
  
                  Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                          !Error
                      Error# = 1
                  End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
  
              End !Case stl:PartType
              If Error# = 0
                  Relate:STOCKALL.Delete(0)
              End !If Error# = 0
  
  
              !Added 05/12/02 - L393 / VP109 -
              !If all warranty/chargeable/estimate parts are now allocated - change job status to 315 - In Repair
              FoundAnyPart = false
  
              !first check the chargeable parts
              access:Parts.clearkey(par:Part_Number_Key)
              par:Ref_Number = stl:JobNumber
              set(par:Part_Number_Key,par:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:Parts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if par:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if par:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              break
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
              End !loop to find an unallocated part
  
              if FoundAnyPart = false
                  !Check warranty parts
                  access:warParts.clearkey(wpr:Part_Number_Key)
                  wpr:Ref_Number = stl:JobNumber
                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:WarParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if wpr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if wpr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
  
              END !if I haven't FoundAnyPart
  
              if FoundAnyPart = false
                  !Check estimate parts
                  access:EstParts.clearkey(epr:Part_Number_Key)
                  epr:Ref_Number = stl:JobNumber
                  set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:EstParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if epr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if epr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
              END !if I haven't FoundAnyPart
  
              if FoundAnyPart = false
                   !change job status to 315
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = stl:JobNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      Error# = 0
  
                      Pointer# = Pointer(JOBS)
                      Hold(JOBS,1)
                      Get(JOBS,Pointer#)
                      If Errorcode() = 43
  
                          Error# = 1
                !          !Post(Event:CloseWindow)
                      ELSE !If Errorcode() = 43
                            !Found
                          getstatus(315,0,'JOB')
                          Access:JOBS.TryUpdate()
                      End  !If Errorcode() = 43
                      Release(JOBS)
                  END !if tryfetch on jobs
              END !if I haven't FoundAnyPart
  
  
          !end of Added 05/12/02 - L393 / VP109 -
  
          End !If Error# = 0
      End !If stl:PartNumber = 'EXCH'
  
  END
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Fulfil Request') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Fulfil Request',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmChangeStockStatus',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmChangeStockStatus0_div')&'">'&p_web.Translate('Fulfil Request')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmChangeStockStatus_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmChangeStockStatus_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmChangeStockStatus_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmChangeStockStatus_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmChangeStockStatus_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmChangeStockStatus'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmChangeStockStatus') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmChangeStockStatus'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmChangeStockStatus') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fulfil Request') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmChangeStockStatus_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmChangeStockStatus",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmChangeStockStatus_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmChangeStockStatus') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmChangeStockStatus')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Fulfil Request')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Fulfil Request')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Fulfil Request')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Fulfil Request')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtRequestFulfilled
        do Comment::txtRequestFulfilled
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::txtRequestFulfilled  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtRequestFulfilled  ! copies value to session value if valid.
  do Comment::txtRequestFulfilled ! allows comment style to be updated.

ValidateValue::txtRequestFulfilled  Routine
    If not (1=0)
    End

Value::txtRequestFulfilled  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtRequestFulfilled') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtRequestFulfilled" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtRequestFulfilled'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtRequestFulfilled  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtRequestFulfilled:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtRequestFulfilled') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtErrorText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtErrorText  ! copies value to session value if valid.
  do Comment::txtErrorText ! allows comment style to be updated.

ValidateValue::txtErrorText  Routine
    If not (1=0)
    End

Value::txtErrorText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtErrorText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtErrorText" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtErrorText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtErrorText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtErrorText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtErrorText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmChangeStockStatus_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmChangeStockStatus_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmChangeStockStatus_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)

  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmChangeStockStatus_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmChangeStockStatus_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::txtRequestFulfilled
    If loc:Invalid then exit.
    do ValidateValue::txtErrorText
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStockRefNo',locStockRefNo) ! LONG
     p_web.SSV('FoundAnyPart',FoundAnyPart) ! BYTE
     p_web.SSV('FoundOrderedPart',FoundOrderedPart) ! BYTE
     p_web.SSV('locJobNumber',locJobNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStockRefNo = p_web.GSV('locStockRefNo') ! LONG
     FoundAnyPart = p_web.GSV('FoundAnyPart') ! BYTE
     FoundOrderedPart = p_web.GSV('FoundOrderedPart') ! BYTE
     locJobNumber = p_web.GSV('locJobNumber') ! LONG
AddFaulty     Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
        Of 'W'
            stf:PartNumber  = wpr:Part_Number
            stf:Description = wpr:Description
            stf:Quantity    = wpr:Quantity
            stf:PurchaseCost    = wpr:Purchase_Cost
        Of 'C'
            stf:PartNumber  = par:Part_Number
            stf:Description = par:Description
            stf:Quantity    = par:Quantity
            stf:PurchaseCost    = par:Purchase_Cost
        Of 'E'
            stf:PartNumber  = epr:Part_Number
            stf:Description = epr:Description
            stf:Quantity    = epr:Quantity
            stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        stf:StoreUserCode   = p_web.GSV('BookingUserCode')
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
frmFulfilRequest     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockRefNo        LONG                                  !
FoundAnyPart         BYTE                                  !
FoundOrderedPart     BYTE                                  !
locJobNumber         LONG                                  !
RapidQueue           QUEUE,PRE(rapque)                     !
SessionID            LONG                                  !
RecordNumber         LONG                                  !
                     END                                   !
RapidJobQueue        QUEUE,PRE(jobque)                     !
SessionID            LONG                                  !
JobNumber            LONG                                  !
                     END                                   !
                    MAP
RemovePartFromStock     Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber),BYTE  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
AddFaulty               Procedure(String  func:Type)
FindOtherParts          Procedure(Long func:JobNumber,Long func:RecordNumber),BYTE    !Job Number, Record Number
                    END
FilesOpened     Long
STOFAULT::State  USHORT
USERS::State  USHORT
STOCK::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
ESTPARTS::State  USHORT
STOCKALL::State  USHORT
txtRequestFulfilled:IsInvalid  Long
txtErrorText:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmFulfilRequest')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmFulfilRequest_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmFulfilRequest','')
    p_web.DivHeader('frmFulfilRequest',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmFulfilRequest') = 0
        p_web.AddPreCall('frmFulfilRequest')
        p_web.DivHeader('popup_frmFulfilRequest','nt-hidden')
        p_web.DivHeader('frmFulfilRequest',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmFulfilRequest_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmFulfilRequest_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmFulfilRequest',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmFulfilRequest')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmFulfilRequest_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmFulfilRequest'
    end
    p_web.formsettings.proc = 'frmFulfilRequest'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmFulfilRequest_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'frmStockAllocation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmFulfilRequest_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmFulfilRequest_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmFulfilRequest_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('FulfilType'))
      p_web.StoreValue('FulFilType')
  END
  
  IF (p_web.GSV('FulFilType') = 'SINGLE')
      IF (p_web.IfExistsValue('stl:RecordNumber'))
          p_web.StoreValue('stl:RecordNumber')
          Access:STOCKALL.Clearkey(stl:RecordNumberKey)
          stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
          If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              p_web.SSV('txtErrorText','')
              p_web.SSV('txtRequestFulfilled','Request Fulfilled')
  
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:RecordNumberKey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                  !Found
                      locStockRefNo  = par:Part_Ref_Number
                  Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                  !Error
                      p_web.SSV('txtErrorText','The selected part does not exist. The entry will be removed from the list.')
                      p_web.SSV('txtRequestFulfilled','')
                      Relate:STOCKALL.Delete(0)
                  End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Found
                      locStockRefNo  = wpr:Part_Ref_Number
  
                  Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Error
                      p_web.SSV('txtErrorText','The selected part does not exist. The entry will be removed from the list.')
                      p_web.SSV('txtRequestFulfilled','')
                      Relate:STOCKALL.Delete(0)
                  End !If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
              End !Case stl:PartType
  
              If RemovePartFromStock(locStockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber) = Level:Benign
  
          !Added 05/12/02 - L393 / VP109 -
          !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
          !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
  
                  FoundAnyPart = false
                  FoundOrderedPart = false
  
          !first check the chargeable parts
                  access:Parts.clearkey(par:Part_Number_Key)
                  par:Ref_Number = stl:JobNumber
                  set(par:Part_Number_Key,par:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:Parts.next() then
                  !leave no more jobs
                          break
                      ELSE
                          if par:Ref_Number <> stl:JobNumber then
                      !Leave no more matching jobs
                              break
                          ELSE
                      !found a part for this job
                              if par:PartAllocated then
                          !ignore this it was allocated
                              ELSE
                          !found an unallocated part!
                                  FoundAnyPart = true
                                  If par:order_number <> ''
                                      If par:date_received = ''
                                          FoundOrderedPart = 1
  
                                      Else!If par:date_received = ''
                                  !ignore this it has been received
                                      End!If par:date_received = ''
                                  End
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                      if FoundAnyPart and FoundORderedPart then break.
                  End !loop to find an unallocated part
  
                  if FoundAnyPart and FoundOrderedPart then
              !I dont need to check any further
                  ELSE
              !Check warranty parts
                      access:warParts.clearkey(wpr:Part_Number_Key)
                      wpr:Ref_Number = stl:JobNumber
                      set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                      Loop !to find an unallocated part
                          if access:WarParts.next() then
                      !leave no more jobs
                              break
                          ELSE
                              if wpr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                                  break
                              ELSE
                          !found a part for this job
                                  if wpr:PartAllocated then
                              !ignore this it was allocated
                                  ELSE
                              !found an unallocated part!
                                      FoundAnyPart = true
                                      If wpr:order_number <> ''
                                          If wpr:date_received = ''
                                              FoundOrderedPart = 1
                                          Else!If par:date_received = ''
                                      !ignore this it has been received
                                          End!If par:date_received = ''
                                      End
  
                                  END !if partAllocated
                              END !If ref_numbers disagree
                          END !if access:jobs.next()
                          if foundanypart and foundorderedPart then break.
                      End !loop to find an unallocated part
  
                  END !if I have what I need already
  
                  if FoundAnyPart and foundorderedPart then
              !I dont need to check this
                  ELSE
              !Check estimate parts
                      access:EstParts.clearkey(epr:Part_Number_Key)
                      epr:Ref_Number = stl:JobNumber
                      set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop !to find an unallocated part
                          if access:EstParts.next() then
                      !leave no more jobs
                              break
                          ELSE
                              if epr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                                  break
                              ELSE
                          !found a part for this job
                                  if epr:PartAllocated then
                              !ignore this it was allocated
                                  ELSE
                              !found an unallocated part!
                                      FoundAnyPart = true
                                      If epr:order_number <> ''
                                          If epr:date_received = ''
                                              FoundOrderedPart = 1
                                          Else!If par:date_received = ''
                                      !ignore this it has been received
                                          End!If par:date_received = ''
                                      End
  
                                  END !if partAllocated
                              END !If ref_numbers disagree
                          END !if access:jobs.next()
                          if foundanypart and foundorderedpart then break.
                      End !loop to find an unallocated part
                  END !if I haven't FoundAnyPart
  
  
                  if FoundOrderedPart = false or FoundAnyPArt = false
               !change job status to 330 then to 315 as needed
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = stl:JobNumber
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          Error# = 0
  
                          Pointer# = Pointer(JOBS)
                          Hold(JOBS,1)
                          Get(JOBS,Pointer#)
                          If Errorcode() = 43
  !                        Case Missive('This job (' & Clip(brw2.q.stl:JobNumber) & ') is currently in use by another station. Unable to update status. '&|
  !                            '<13,10>You will have to do this manually.','ServiceBase 3g',|
  !                            'mstop.jpg','/OK')
  !                        Of 1 ! OK Button
  !                        End ! Case Missive
  !                        Error# = 1
            !          !Post(Event:CloseWindow)
                          ELSE !If Errorcode() = 43
                        !Found
                              if FoundOrderedPart = false
                                  getstatus(330,0,'JOB')  !Spares Requested
                              END !If no outstanding orders
                              if FoundAnyPart = false
                                  getstatus(345,0,'JOB')  !In repair
                              end !found no unallocated parts
                              Access:JOBS.TryUpdate()
                          End  !If Errorcode() = 43
                          Release(JOBS)
                      END !if tryfetch on jobs
                  END !if I haven't FoundAnyPart
          !end of Added 05/12/02 - L393 / VP109 -
  
          !Remove Entry from browse - 3451 (DBH: 28-10-2003)
                  Relate:STOCKALL.Delete(0)
              End !Local.RemovePartFromStock(tmp:StockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber)
  
          Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      !Error
              p_web.SSV('txtErrorText','An error occurred. Try again.')
              p_web.SSV('txtRequestFulfilled','')
  
          End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      ELSE
          p_web.SSV('txtErrorText','An error occurred. Try again.')
          p_web.SSV('txtRequestFulfilled','')
      END
  END ! IF (p_web.GSV('FulFilType') = 'SINGLE')
  IF (p_web.GSV('FulFilType') = 'MULTIPLE')
  p_web.SSV('txtErrorText','')
  p_web.SSV('txtRequestFulfilled','Request(s) Fulfilled')
      LOOP x# = 1 TO RECORDS(RapidQueue)
          GET(RapidQueue,x#)
          IF (RapidQueue.SessionID <> p_web.SessionID)
              CYCLE
          END
          Delete(RapidQueue)
      END
  
      LOOP x# = 1 TO RECORDS(RapidJobQueue)
          GET(RapidJobQueue,x#)
          IF (RapidJobQueue.SessionID <> p_web.SessionID)
              CYCLE
          END
          Delete(RapidJobQueue)
      END
  
      Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
      stl:Location  = p_web.GSV('Default:SiteLocation')
      stl:Status    = 'WEB'
      Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
      Loop
          If Access:STOCKALL.NEXT()
              Break
          End !If
          If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
              Or stl:Status    <> 'WEB'      |
              Then Break.  ! End If
          rapque:RecordNumber = stl:RecordNumber
          rapque:SessionID = p_web.SessionID
          Add(RapidQueue)
      End !Loop
  
      Sort(RapidQueue,rapque:RecordNumber)
  
      Loop x# = 1 To Records(RapidQueue)
          Get(RapidQueue,x#)
          IF (RapidQueue.SessionID <> p_web.SessionID)
              CYCLE
          END
  
          Access:STOCKALL.Clearkey(stl:RecordNumberKEy)
          stl:RecordNumber  = rapque:RecordNumber
          If Access:STOCKALL.Tryfetch(stl:RecordNumberKEy) = Level:Benign
              !Found
              !This is from stock
              !Has this job already been tried and failed?
              Sort(RapidJobQueue,jobque:JobNumber)
              jobque:JobNumber    = stl:JobNumber
              Get(RapidJobQueue,jobque:JobNumber)
              If ~Error()
                  Cycle
              End !If ~Error()
  
              locStockRefNo = stl:PartRefNumber
  
              Case stl:PartType
              OF 'CHA'
                  Access:Parts.ClearKey(par:recordnumberkey)
                  par:Record_Number = stl:PartRecordNumber
                  IF Access:Parts.Fetch(par:recordnumberkey)
                          !Error!
                      Cycle
                  ELSE
  
                  END
              OF 'WAR'
                  Access:WarParts.ClearKey(wpr:recordnumberkey)
                  wpr:Record_Number = stl:PartRecordNumber
                  IF Access:WarParts.Fetch(wpr:recordnumberkey)
                          !Error!
                      Cycle
                  ELSE
  
                  END
              End
  
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number  = locStockRefNo
              If Access:STOCK.Tryfetch(sto:Ref_Number_Key)
                 !Cannot find stock entry
                  Cycle
              END
              If stl:Quantity > sto:Quantity_Stock
                  !Cannot fullfill request
                  Cycle
              Else !stl:Quantity > sto:Quantity_Stock
                  !
              End !stl:Quantity > sto:Quantity_Stock
  
              locJobNumber   = stl:JobNumber
  
              If (FindOtherParts(stl:JobNumber,stl:RecordNumber) = Level:Benign)
                  !All parts are ok to be taken
                  Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
                  stl:Location  = p_web.GSV('Default:SiteLocation')
                  stl:Status    = 'WEB'
                  stl:JobNumber = locJobNumber
                  Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
                  Loop
                      If Access:STOCKALL.NEXT()
                          Break
                      End !If
                      If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
                          Or stl:Status    <> 'WEB'      |
                          Or stl:JobNumber <> locJobNumber |
                          Then Break.  ! End If
                      If RemovePartFromStock(stl:PartRefNumber,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber) = Level:Benign
                          Relate:STOCKALL.Delete(0)
                      End !.RemoveFromStock(par:Part_Ref_Number,par:Record_Number,par:Quantity,'C',par:Part_Number)
                  End !Loop
  
              Else !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
                  !Not all parts can be fullfilled
                  !Add job number to queue, so no other parts for this job will be picked
                  Sort(RapidJobQueue,jobque:JobNumber)
                  jobque:JobNumber = locJobNumber
                  jobque:SessionID = p_web.SessionID
                  Add(RapidJobQueue)
              End !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
  
  
          Else!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
  
          !Added 05/12/02 - L393 / VP109 -
          !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
          !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
  
          Access:STOCKALL.Clearkey(stl:RecordNumberKey)
          stl:recordNumber    = rapque:RecordNumber
          If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              !Found
              !Reget the record to check the status bits below
          Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              !Error
          End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
  
  
          FoundAnyPart = false
          FoundOrderedPart = false
  
          !first check the chargeable parts
          access:Parts.clearkey(par:Part_Number_Key)
          par:Ref_Number = stl:JobNumber
          set(par:Part_Number_Key,par:Part_Number_Key)
          Loop !to find an unallocated part
              if access:Parts.next() then
                  !leave no more jobs
                  break
              ELSE
                  if par:Ref_Number <> stl:JobNumber then
                      !Leave no more matching jobs
                      break
                  ELSE
                      !found a part for this job
                      if par:PartAllocated then
                          !ignore this it was allocated
                      ELSE
                          !found an unallocated part!
                          FoundAnyPart = true
                          If par:order_number <> ''
                              If par:date_received = ''
                                  FoundOrderedPart = 1
  
                              Else!If par:date_received = ''
                                  !ignore this it has been received
                              End!If par:date_received = ''
                          End
                      END !if partAllocated
                  END !If ref_numbers disagree
              END !if access:jobs.next()
              if FoundAnyPart and FoundORderedPart then break.
          End !loop to find an unallocated part
  
          if FoundAnyPart and FoundOrderedPart then
              !I dont need to check any further
          ELSE
              !Check warranty parts
              access:warParts.clearkey(wpr:Part_Number_Key)
              wpr:Ref_Number = stl:JobNumber
              set(wpr:Part_Number_Key,wpr:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:WarParts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if wpr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if wpr:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              If wpr:order_number <> ''
                                  If wpr:date_received = ''
                                      FoundOrderedPart = 1
                                  Else!If par:date_received = ''
                                      !ignore this it has been received
                                  End!If par:date_received = ''
                              End
  
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
                  if foundanypart and foundorderedPart then break.
              End !loop to find an unallocated part
  
          END !if I have what I need already
  
          if FoundAnyPart and foundorderedPart then
              !I dont need to check this
          ELSE
              !Check estimate parts
              access:EstParts.clearkey(epr:Part_Number_Key)
              epr:Ref_Number = stl:JobNumber
              set(epr:Part_Number_Key,epr:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:EstParts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if epr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if epr:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              If epr:order_number <> ''
                                  If epr:date_received = ''
                                      FoundOrderedPart = 1
                                  Else!If par:date_received = ''
                                      !ignore this it has been received
                                  End!If par:date_received = ''
                              End
  
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
                  if foundanypart and foundorderedpart then break.
              End !loop to find an unallocated part
          END !if I haven't FoundAnyPart
  
  
          if FoundOrderedPart = false or FoundAnyPArt = false
               !change job status to 330 then to 315 as needed
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = stl:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  Error# = 0
  
                  Pointer# = Pointer(JOBS)
                  Hold(JOBS,1)
                  Get(JOBS,Pointer#)
                  If Errorcode() = 43
                      CYCLE
  !                    Case Missive('This job is currently in use by another station. Unable to update status. You will have to do this manually.','ServiceBase 3g',|
  !                        'mstop.jpg','/OK')
  !                    Of 1 ! OK Button
  !                    End ! Case Missive
                      Error# = 1
            !          !Post(Event:CloseWindow)
                  ELSE !If Errorcode() = 43
                        !Found
                      if FoundOrderedPart = false
                          getstatus(330,0,'JOB')  !Spares Requested
                      END !If no outstanding orders
                      if FoundAnyPart = false
                          getstatus(345,0,'JOB')  !In repair
                      end !found no unallocated parts
                      Access:JOBS.TryUpdate()
                  End  !If Errorcode() = 43
                  Release(JOBS)
              END !if tryfetch on jobs
          END !if I haven't FoundAnyPart
  
          !end of Added 05/12/02 - L393 / VP109 -
  
      End !x# = 1 To Records(RapidQueue)
  
  END ! IF (p_web.GSV('FulFilType') = 'MULTIPLE')
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Fulfil Request') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Fulfil Request',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmFulfilRequest',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmFulfilRequest0_div')&'">'&p_web.Translate('Fulfil Request')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmFulfilRequest_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmFulfilRequest_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmFulfilRequest_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmFulfilRequest_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmFulfilRequest_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmFulfilRequest'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmFulfilRequest') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmFulfilRequest'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmFulfilRequest') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fulfil Request') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmFulfilRequest_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmFulfilRequest",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmFulfilRequest_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmFulfilRequest') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmFulfilRequest')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Fulfil Request')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Fulfil Request')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Fulfil Request')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Fulfil Request')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtRequestFulfilled
        do Comment::txtRequestFulfilled
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::txtRequestFulfilled  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtRequestFulfilled  ! copies value to session value if valid.
  do Comment::txtRequestFulfilled ! allows comment style to be updated.

ValidateValue::txtRequestFulfilled  Routine
    If not (1=0)
    End

Value::txtRequestFulfilled  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtRequestFulfilled') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtRequestFulfilled" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtRequestFulfilled'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtRequestFulfilled  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtRequestFulfilled:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtRequestFulfilled') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtErrorText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtErrorText  ! copies value to session value if valid.
  do Comment::txtErrorText ! allows comment style to be updated.

ValidateValue::txtErrorText  Routine
    If not (1=0)
    End

Value::txtErrorText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtErrorText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtErrorText" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtErrorText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtErrorText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtErrorText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtErrorText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmFulfilRequest_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmFulfilRequest_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmFulfilRequest_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)

  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmFulfilRequest_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmFulfilRequest_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::txtRequestFulfilled
    If loc:Invalid then exit.
    do ValidateValue::txtErrorText
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStockRefNo',locStockRefNo) ! LONG
     p_web.SSV('FoundAnyPart',FoundAnyPart) ! BYTE
     p_web.SSV('FoundOrderedPart',FoundOrderedPart) ! BYTE
     p_web.SSV('locJobNumber',locJobNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStockRefNo = p_web.GSV('locStockRefNo') ! LONG
     FoundAnyPart = p_web.GSV('FoundAnyPart') ! BYTE
     FoundOrderedPart = p_web.GSV('FoundOrderedPart') ! BYTE
     locJobNumber = p_web.GSV('locJobNumber') ! LONG
RemovePartFromStock   Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber)  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
Code
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = func:StockRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
        IF (BHFileInUse(STOCK))
            p_web.SSV('txtErrorText','Error: The selected stock item is in use.')
            p_web.SSV('txtRequestFulfilled','')
            Return Level:Fatal
        End

        If sto:Quantity_Stock < func:Quantity
            p_web.SSV('txtErrorText','Error: There are insufficient items in stock.')
            p_web.SSV('txtRequestFulfilled','')
            !There isn't enough in stock anymore
            Return Level:Fatal
        End !If sto:Quantity < func:Quantity

        If func:PartNumber = 'EXCH'
!            Case Missive('The selected part is an "Exchange Part".'&|
!                '<13,10>Click "Allocate Exchange" to allocate an exchange unit.','ServiceBase 3g',|
!                'mstop.jpg','/OK')
!            Of 1 ! OK Button
!            End ! Case Missive
            p_web.SSV('txtErrorText','Error: This in an "Exchange Part"')
            p_web.SSV('txtRequestFulfilled','')
        Else !If stl:PartNumber = 'EXCH'
            Error# = 0
            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:recordnumberkey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = par:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find the selected job.')
                        p_web.SSV('txtRequestFulfilled','')
                        Return Level:Fatal
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileInUse(STOCk))
                            p_web.SSV('txtErrorText','Error: The selected stock part is in use')
                            p_web.SSV('txtRequestFulfilled','')
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare

                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued')
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(par:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('C')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive
                        End !If sto:ReturnFaultySpare
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                        !Error
                    Error# = 1
                End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = wpr:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find the selected job.')
                        p_web.SSV('txtRequestFulfilled','')
                        Return Level:Fatal

                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileInUse(STOCK))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use')
                            p_web.SSV('txtRequestFulfilled','')
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare
                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued')
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('W')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive

                        End !If sto:ReturnFaultySpare

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                        !Error
                    Error# = 1
                End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
            Of 'EST'
                Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                epr:Record_Number   = func:PartRecordNumber
                If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = epr:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find the selected job.')
                        p_web.SSV('txtRequestFulfilled','')
                        Return Level:Fatal

                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = epr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileINUse(STOCK))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use')
                            p_web.SSV('txtRequestFulfilled','')
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare
                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued')
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('E')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive

                        End !If sto:ReturnFaultySpare

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                        !Error
                    Error# = 1
                End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
            End !Case stl:PartType
        End !If func:PartNumber = 'EXCH'

        If Error# = 0
            sto:Quantity_Stock  -= func:Quantity
            If sto:Quantity_Stock < 0
                sto:Quantity_Stock = 0
            End !If sto:Quantity_Stock < 0
            IF Access:STOCK.Update()
                !Error
            END

            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:RecordNumberKey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                        'DEC', | ! Transaction_Type
                        par:Despatch_Note_Number, | ! Depatch_Note_Number
                        par:Ref_Number, | ! Job_Number
                        0, | ! Sales_Number
                        func:Quantity, | ! Quantity
                        par:Purchase_Cost, | ! Purchase_Cost
                        par:Sale_Cost, | ! Sale_Cost
                        par:Retail_Cost, | ! Retail_Cost
                        'STOCK DECREMENTED', | ! Notes
                        '', |
                        p_web.GSV('BookingUsercode'), |
                        sto:Quantity_Stock) ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    par:WebOrder    = False
                    par:Status      = ''
                    par:PartAllocated   = 1
                    Access:PARTS.Update()
                Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                        'DEC', | ! Transaction_Type
                        wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                        wpr:Ref_Number, | ! Job_Number
                        0, | ! Sales_Number
                        func:Quantity, | ! Quantity
                        wpr:Purchase_Cost, | ! Purchase_Cost
                        wpr:Sale_Cost, | ! Sale_Cost
                        wpr:Retail_Cost, | ! Retail_Cost
                        'STOCK DECREMENTED', | ! Notes
                        '', |
                        p_web.GSV('BookingUsercode'), |
                        sto:Quantity_Stock) ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    wpr:WebOrder    = False
                    wpr:Status      = ''
                    wpr:PartAllocated   = 1
                    Access:WARPARTS.Update()
                Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
            End ! Case func:PartType
            ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
            If (sto:Location <> VodacomClass.MainStoreLocation())
                If (VodacomClass.MainStoreSuspended(sto:Part_Number))
                    if (sto:Quantity_Stock = 0)
                    ! Suspend Item
                        sto:Suspend = 1
                        If (Access:STOCK.TryUpdate() = Level:Benign)
                            if (AddToStockHistory(sto:Ref_Number, |
                                'ADD',|
                                '',|
                                0, |
                                0, |
                                0, |
                                sto:Purchase_Cost,|
                                sto:Sale_Cost, |
                                sto:Retail_Cost, |
                                'PART SUSPENDED',|
                                '', |
                                p_web.GSV('BookingUserCode'), |
                                sto:Quantity_Stock))
                            end
                        ENd ! If (Access:STOCK.TryUpdate() = Level:Benign)
                    else
                    ! Show warning
!                        Beep(Beep:SystemExclamation)  ;  Yield()
!                        Case Missive('This part has been suspended at Main Store. It will not be available for ordering.'&|
!                            '|'&|
!                            '|Current Stock Level: ' & Clip(sto:Quantity_stock) & '.','ServiceBase',|
!                            'mexclam.jpg','/&OK')
!                        Of 1 ! &OK Button
!                        End!Case Message
                    end
                end ! If (MainStoreSuspened(sto:Part_Number))
            End
        Else !If Error# = 0
            Return Level:Fatal
        End !If Error# = 0
    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        p_web.SSV('txtErrorText','Error: The selected stock item cannot be found. The line will be removed.')
        p_web.SSV('txtRequestFulfilled','')
        RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)

!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!        Access:JOBS.Clearkey(job:Ref_Number_Key)
!        job:Ref_Number  = stl:JobNumber
!        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!            !Error
!        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!
!        Access:USERS.Clearkey(use:User_Code_Key)
!        use:User_COde   = job:Engineer
!        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Error
!        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!        !Blank entries are still being left after a refresh.
!        !This will ensure that they can be deleted. - 3451 (DBH: 28-10-2003)
!        Case Missive('Unable to fund the selected part in Stock Control.'&|
!            '<13,10>Part:' & Clip(stl:PartNumber) & ' - ' & Clip(stl:Description) & '.'&|
!            '<13,10>Job: ' & Clip(job:Ref_NUmber) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) &|
!            '<13,10>Do you wish to REMOVE this part line from Stock Allocation?','ServiceBase 3g',|
!            'mquest.jpg','\Cancel|Remove')
!        Of 2 ! Remove Button
!            RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)
!        Of 1 ! Cancel Button
!        End ! Case Missive
!        Return Level:Fatal
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Return Level:Benign
AddFaulty     Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
        Of 'W'
            stf:PartNumber  = wpr:Part_Number
            stf:Description = wpr:Description
            stf:Quantity    = wpr:Quantity
            stf:PurchaseCost    = wpr:Purchase_Cost
        Of 'C'
            stf:PartNumber  = par:Part_Number
            stf:Description = par:Description
            stf:Quantity    = par:Quantity
            stf:PurchaseCost    = par:Purchase_Cost
        Of 'E'
            stf:PartNumber  = epr:Part_Number
            stf:Description = epr:Description
            stf:Quantity    = epr:Quantity
            stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        stf:StoreUserCode   = p_web.GSV('BookingUserCode')
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
FindOtherParts        Procedure(Long func:JobNumber,Long func:RecordNumber)    !Job Number, Record Number
Code
    Found# = 0
        !Lets find some other parts in the allocation list

    Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
    stl:Location  = p_web.GSV('Default:SiteLocation')
    stl:Status    = 'WEB'
    Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
    Loop
        If Access:STOCKALL.NEXT()
            Break
        End !If
        If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
            Or stl:Status    <> 'WEB'      |
            Then Break.  ! End If

            !Only look for parts from the same job
        If stl:JobNumber <> func:JobNumber
            Cycle
        End !If stl:JobNumber <> tmp:JobNumber
            !Make sure you haven't found the original record
        If stl:RecordNumber = func:RecordNumber
            Cycle
        End !If stl:RecordNumber = rapque:RecordNumber

            !Are there any in stock
        Case stl:PartType
        Of 'CHA'
            Access:PARTS.Clearkey(par:RecordNumberKey)
            par:Record_Number   = stl:PartRecordNumber
            If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Found
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                    If par:Quantity > sto:Quantity_Stock
                        Found# = 1
                        Break
                    End !If par:Quantity < sto:Quantity_Stock
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    Found# = 1
                    Break
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Error
            End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
        Of 'WAR'
            Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
            wpr:Record_Number   = stl:PartRecordNumber
            If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                    If wpr:Quantity > sto:Quantity_Stock
                        Found# = 1
                        Break
                    End !If wpr:Quantity > sto:Quantity_Stock
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    Found# = 1
                    Break
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else ! If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
                        !Error
            End !If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
        End !Case stl:PartType
    End !Loop

    Return Found#
FormStockControl     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
loc0_7               LONG                                  !
loc0_30              LONG                                  !
loc31_60             LONG                                  !
loc61_90             LONG                                  !
locAverageValue      LONG                                  !
locAverageText       STRING(30)                            !
FilesOpened     Long
LOCSHELF::State  USHORT
STOCK::State  USHORT
sto:DateBooked:IsInvalid  Long
sto:Part_Number:IsInvalid  Long
sto:Location:IsInvalid  Long
sto:Description:IsInvalid  Long
sto:Shelf_Location:IsInvalid  Long
sto:Supplier:IsInvalid  Long
sto:Second_Location:IsInvalid  Long
sto:Accessory:IsInvalid  Long
sto:AveragePurchaseCost:IsInvalid  Long
txtUsage:IsInvalid  Long
sto:Percentage_Mark_Up:IsInvalid  Long
loc0_7:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
loc0_30:IsInvalid  Long
sto:PurchaseMarkUp:IsInvalid  Long
loc31_60:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
loc61_90:IsInvalid  Long
sto:Quantity_Stock:IsInvalid  Long
locAverageText:IsInvalid  Long
sto:Sundry_Item:IsInvalid  Long
sto:Suspend:IsInvalid  Long
sto:ExchangeUnit:IsInvalid  Long
sto:ExchangeOrderCap:IsInvalid  Long
sto:AllowDuplicate:IsInvalid  Long
sto:ReturnFaultySpare:IsInvalid  Long
sto:ChargeablePartOnly:IsInvalid  Long
sto:AttachBySolder:IsInvalid  Long
sto:E1:IsInvalid  Long
sto:E2:IsInvalid  Long
sto:E3:IsInvalid  Long
sto:RepairLevel:IsInvalid  Long
sto:SkillLevel:IsInvalid  Long
sto:Manufacturer:IsInvalid  Long
FormStockModels:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormStockControl')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormStockControl_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormStockControl','Change')
    p_web.DivHeader('FormStockControl',p_web.combine(p_web.site.style.formdiv,))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormStockControl') = 0
        p_web.AddPreCall('FormStockControl')
        p_web.DivHeader('popup_FormStockControl','nt-hidden')
        p_web.DivHeader('FormStockControl',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormStockControl_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormStockControl_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormStockControl',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('sto:Ref_Number') = 0 then p_web.SetValue('sto:Ref_Number',p_web.GSV('sto:Ref_Number')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormStockControl',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockControl',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('sto:Ref_Number') = 0 then p_web.SetValue('sto:Ref_Number',p_web.GSV('sto:Ref_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormStockControl',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('sto:Ref_Number') = 0 then p_web.SetValue('sto:Ref_Number',p_web.GSV('sto:Ref_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockControl',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockControl',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockControl',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormStockControl')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOCSHELF)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOCSHELF)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('FormStockModels')
      do Value::FormStockModels
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  VodacomClass.StockUsage(p_web.GSV('sto:Ref_Number'),loc0_7,loc0_30, |
      loc31_60,loc61_90,locAverageValue)
  
  If locAverageValue < 1
      locAverageText = '< 1'
  Else!If average_temp < 1
      locAverageText = Int(locAverageValue)
  End!If average_temp < 1
  
  p_web.SSV('loc0_7',loc0_7)
  p_web.SSV('loc0_30',loc0_30)
  p_web.SSV('loc31_60',loc31_60)
  p_web.SSV('loc61_90',loc61_90)
  p_web.SSV('locAverageText',locAverageText)
  p_web.SetValue('FormStockControl_form:inited_',1)
  p_web.formsettings.file = 'STOCK'
  p_web.formsettings.key = 'sto:Ref_Number_Key'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'STOCK'
    p_web.formsettings.key = 'sto:Ref_Number_Key'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = sto:Ref_Number
    p_web.formsettings.FieldName[1] = 'sto:Ref_Number'
    do SetAction
    if p_web.GetSessionValue('FormStockControl:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormStockControl'
    end
    p_web.formsettings.proc = 'FormStockControl'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormStockControl:Primed') = 1
    p_web._deleteFile(STOCK)
    p_web.SetSessionValue('FormStockControl:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','STOCK')
  p_web.SetValue('UpdateKey','sto:Ref_Number_Key')
  If p_web.IfExistsValue('sto:DateBooked')
    p_web.SetPicture('sto:DateBooked','@d6')
  End
  p_web.SetSessionPicture('sto:DateBooked','@d6')
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Location')
    p_web.SetPicture('sto:Location','@s30')
  End
  p_web.SetSessionPicture('sto:Location','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('sto:Shelf_Location')
    p_web.SetPicture('sto:Shelf_Location','@s30')
  End
  p_web.SetSessionPicture('sto:Shelf_Location','@s30')
  If p_web.IfExistsValue('sto:Supplier')
    p_web.SetPicture('sto:Supplier','@s30')
  End
  p_web.SetSessionPicture('sto:Supplier','@s30')
  If p_web.IfExistsValue('sto:Second_Location')
    p_web.SetPicture('sto:Second_Location','@s30')
  End
  p_web.SetSessionPicture('sto:Second_Location','@s30')
  If p_web.IfExistsValue('sto:AveragePurchaseCost')
    p_web.SetPicture('sto:AveragePurchaseCost','@n14.2')
  End
  p_web.SetSessionPicture('sto:AveragePurchaseCost','@n14.2')
  If p_web.IfExistsValue('sto:Percentage_Mark_Up')
    p_web.SetPicture('sto:Percentage_Mark_Up','@n6.2')
  End
  p_web.SetSessionPicture('sto:Percentage_Mark_Up','@n6.2')
  If p_web.IfExistsValue('sto:Purchase_Cost')
    p_web.SetPicture('sto:Purchase_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Purchase_Cost','@n14.2')
  If p_web.IfExistsValue('sto:PurchaseMarkUp')
    p_web.SetPicture('sto:PurchaseMarkUp','@n14.2')
  End
  p_web.SetSessionPicture('sto:PurchaseMarkUp','@n14.2')
  If p_web.IfExistsValue('sto:Sale_Cost')
    p_web.SetPicture('sto:Sale_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Sale_Cost','@n14.2')
  If p_web.IfExistsValue('sto:Quantity_Stock')
    p_web.SetPicture('sto:Quantity_Stock','@N8')
  End
  p_web.SetSessionPicture('sto:Quantity_Stock','@N8')
  If p_web.IfExistsValue('sto:ExchangeOrderCap')
    p_web.SetPicture('sto:ExchangeOrderCap','@n-14')
  End
  p_web.SetSessionPicture('sto:ExchangeOrderCap','@n-14')
  If p_web.IfExistsValue('sto:RepairLevel')
    p_web.SetPicture('sto:RepairLevel','@n8')
  End
  p_web.SetSessionPicture('sto:RepairLevel','@n8')
  If p_web.IfExistsValue('sto:SkillLevel')
    p_web.SetPicture('sto:SkillLevel','@n8')
  End
  p_web.SetSessionPicture('sto:SkillLevel','@n8')
  If p_web.IfExistsValue('sto:Manufacturer')
    p_web.SetPicture('sto:Manufacturer','@s30')
  End
  p_web.SetSessionPicture('sto:Manufacturer','@s30')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'sto:Shelf_Location'
    p_web.setsessionvalue('showtab_FormStockControl',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOCSHELF)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.sto:Supplier')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('loc0_7') = 0
    p_web.SetSessionValue('loc0_7',loc0_7)
  Else
    loc0_7 = p_web.GetSessionValue('loc0_7')
  End
  if p_web.IfExistsValue('loc0_30') = 0
    p_web.SetSessionValue('loc0_30',loc0_30)
  Else
    loc0_30 = p_web.GetSessionValue('loc0_30')
  End
  if p_web.IfExistsValue('loc31_60') = 0
    p_web.SetSessionValue('loc31_60',loc31_60)
  Else
    loc31_60 = p_web.GetSessionValue('loc31_60')
  End
  if p_web.IfExistsValue('loc61_90') = 0
    p_web.SetSessionValue('loc61_90',loc61_90)
  Else
    loc61_90 = p_web.GetSessionValue('loc61_90')
  End
  if p_web.IfExistsValue('locAverageText') = 0
    p_web.SetSessionValue('locAverageText',locAverageText)
  Else
    locAverageText = p_web.GetSessionValue('locAverageText')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('loc0_7')
    loc0_7 = p_web.GetValue('loc0_7')
    p_web.SetSessionValue('loc0_7',loc0_7)
  Else
    loc0_7 = p_web.GetSessionValue('loc0_7')
  End
  if p_web.IfExistsValue('loc0_30')
    loc0_30 = p_web.GetValue('loc0_30')
    p_web.SetSessionValue('loc0_30',loc0_30)
  Else
    loc0_30 = p_web.GetSessionValue('loc0_30')
  End
  if p_web.IfExistsValue('loc31_60')
    loc31_60 = p_web.GetValue('loc31_60')
    p_web.SetSessionValue('loc31_60',loc31_60)
  Else
    loc31_60 = p_web.GetSessionValue('loc31_60')
  End
  if p_web.IfExistsValue('loc61_90')
    loc61_90 = p_web.GetValue('loc61_90')
    p_web.SetSessionValue('loc61_90',loc61_90)
  Else
    loc61_90 = p_web.GetSessionValue('loc61_90')
  End
  if p_web.IfExistsValue('locAverageText')
    locAverageText = p_web.GetValue('locAverageText')
    p_web.SetSessionValue('locAverageText',locAverageText)
  Else
    locAverageText = p_web.GetSessionValue('locAverageText')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormStockControl_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormStockControl')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockControl_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockControl_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockControl_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
 loc0_7 = p_web.RestoreValue('loc0_7')
 loc0_30 = p_web.RestoreValue('loc0_30')
 loc31_60 = p_web.RestoreValue('loc31_60')
 loc61_90 = p_web.RestoreValue('loc61_90')
 locAverageText = p_web.RestoreValue('locAverageText')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Edit Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Edit Stock',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormStockControl',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormStockControl0_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormStockControl1_div')&'">'&p_web.Translate('Cost / Qty / Usage Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormStockControl2_div')&'">'&p_web.Translate('Part Defaults')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormStockControl3_div')&'">'&p_web.Translate('Models This Part Can Be Used For')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormStockControl_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStockControl_FormStockModels_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormStockControl_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormStockControl_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormStockControl_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormStockControl_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormStockControl')>0,p_web.GSV('showtab_FormStockControl'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormStockControl'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormStockControl') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormStockControl'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormStockControl')>0,p_web.GSV('showtab_FormStockControl'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormStockControl') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Cost / Qty / Usage Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Defaults') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Models This Part Can Be Used For') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormStockControl_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormStockControl')>0,p_web.GSV('showtab_FormStockControl'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormStockControl",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormStockControl')>0,p_web.GSV('showtab_FormStockControl'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormStockControl_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormStockControl') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormStockControl')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('FormStockModels') = 0
      p_web.SetValue('FormStockModels:NoForm',1)
      p_web.SetValue('FormStockModels:FormName',loc:formname)
      p_web.SetValue('FormStockModels:parentIs','Form')
      p_web.SetValue('_parentProc','FormStockControl')
      FormStockModels(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('FormStockModels:NoForm')
      p_web.DeleteValue('FormStockModels:FormName')
      p_web.DeleteValue('FormStockModels:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormStockControl0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:DateBooked
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:DateBooked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Location
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Shelf_Location
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Shelf_Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Supplier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Supplier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Second_Location
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Second_Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Accessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Accessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Cost / Qty / Usage Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormStockControl1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Cost / Qty / Usage Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Cost / Qty / Usage Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Cost / Qty / Usage Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:AveragePurchaseCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:AveragePurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::txtUsage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtUsage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Percentage_Mark_Up
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Percentage_Mark_Up
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::loc0_7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::loc0_7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Purchase_Cost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Purchase_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::loc0_30
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::loc0_30
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:PurchaseMarkUp
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:PurchaseMarkUp
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::loc31_60
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::loc31_60
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Sale_Cost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Sale_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::loc61_90
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::loc61_90
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Quantity_Stock
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Quantity_Stock
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAverageText
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAverageText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Defaults')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormStockControl2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Defaults')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Defaults')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Defaults')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Sundry_Item
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Sundry_Item
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Suspend
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Suspend
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:ExchangeUnit
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:ExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:ExchangeOrderCap
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:ExchangeOrderCap
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:AllowDuplicate
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:AllowDuplicate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:ReturnFaultySpare
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:ReturnFaultySpare
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:ChargeablePartOnly
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:ChargeablePartOnly
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:AttachBySolder
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:AttachBySolder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:E1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:E1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:E2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:E2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:E3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:E3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:RepairLevel
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:RepairLevel
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:SkillLevel
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:SkillLevel
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Models This Part Can Be Used For')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormStockControl3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Models This Part Can Be Used For')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Models This Part Can Be Used For')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Models This Part Can Be Used For')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockControl3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Manufacturer
        do Value::sto:Manufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::FormStockModels
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::sto:DateBooked  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:DateBooked') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date Created'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:DateBooked  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:DateBooked = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @d6
    sto:DateBooked = p_web.Dformat(p_web.GetValue('Value'),'@d6')
  End
  do ValidateValue::sto:DateBooked  ! copies value to session value if valid.

ValidateValue::sto:DateBooked  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:DateBooked',sto:DateBooked).
    End

Value::sto:DateBooked  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:DateBooked') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:DateBooked
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:DateBooked'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Part_Number  ! copies value to session value if valid.

ValidateValue::sto:Part_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Part_Number',sto:Part_Number).
    End

Value::sto:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Location  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Location') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Location  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Location = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Location = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Location  ! copies value to session value if valid.

ValidateValue::sto:Location  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Location',sto:Location).
    End

Value::sto:Location  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Location') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Location'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Description  ! copies value to session value if valid.

ValidateValue::sto:Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Description',sto:Description).
    End

Value::sto:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Shelf_Location  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Shelf_Location') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Shelf Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Shelf_Location  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Shelf_Location = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Shelf_Location = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Shelf_Location  ! copies value to session value if valid.

ValidateValue::sto:Shelf_Location  Routine
    If not (1=0)
  If sto:Shelf_Location = ''
    loc:Invalid = 'sto:Shelf_Location'
    sto:Shelf_Location:IsInvalid = true
    loc:alert = p_web.translate('Shelf Location') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('sto:Shelf_Location',sto:Shelf_Location).
    End

Value::sto:Shelf_Location  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Shelf_Location') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Shelf_Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Shelf_Location'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Supplier  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Supplier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Supplier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Supplier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Supplier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Supplier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Supplier  ! copies value to session value if valid.

ValidateValue::sto:Supplier  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Supplier',sto:Supplier).
    End

Value::sto:Supplier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Supplier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Supplier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Supplier'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Second_Location  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Second_Location') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('2nd Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Second_Location  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Second_Location = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Second_Location = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Second_Location  ! copies value to session value if valid.

ValidateValue::sto:Second_Location  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Second_Location',sto:Second_Location).
    End

Value::sto:Second_Location  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Second_Location') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Second_Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Second_Location'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Accessory  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Accessory') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Accessory'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Accessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Accessory = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    sto:Accessory = p_web.GetValue('Value')
  End
  do ValidateValue::sto:Accessory  ! copies value to session value if valid.
  do Value::sto:Accessory
  do SendAlert

ValidateValue::sto:Accessory  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Accessory',sto:Accessory).
    End

Value::sto:Accessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Accessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:Accessory = p_web.RestoreValue('sto:Accessory')
    do ValidateValue::sto:Accessory
    If sto:Accessory:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:Accessory
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:Accessory'',''formstockcontrol_sto:accessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:Accessory') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:Accessory',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:AveragePurchaseCost  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:AveragePurchaseCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Average Purchase Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:AveragePurchaseCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:AveragePurchaseCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:AveragePurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:AveragePurchaseCost  ! copies value to session value if valid.

ValidateValue::sto:AveragePurchaseCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:AveragePurchaseCost',sto:AveragePurchaseCost).
    End

Value::sto:AveragePurchaseCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:AveragePurchaseCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:AveragePurchaseCost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:AveragePurchaseCost'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::txtUsage  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('txtUsage') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Usage'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::txtUsage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtUsage  ! copies value to session value if valid.

ValidateValue::txtUsage  Routine
    If not (1=0)
    End

Value::txtUsage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('txtUsage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Percentage_Mark_Up  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Percentage Mark Up'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Percentage_Mark_Up  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Percentage_Mark_Up = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n6.2
    sto:Percentage_Mark_Up = p_web.Dformat(p_web.GetValue('Value'),'@n6.2')
  End
  do ValidateValue::sto:Percentage_Mark_Up  ! copies value to session value if valid.

ValidateValue::sto:Percentage_Mark_Up  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Percentage_Mark_Up',sto:Percentage_Mark_Up).
    End

Value::sto:Percentage_Mark_Up  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Percentage_Mark_Up
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Percentage_Mark_Up'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::loc0_7  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc0_7') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('0 - 7 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::loc0_7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    loc0_7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    loc0_7 = p_web.GetValue('Value')
  End
  do ValidateValue::loc0_7  ! copies value to session value if valid.

ValidateValue::loc0_7  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('loc0_7',loc0_7).
    End

Value::loc0_7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc0_7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- loc0_7
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc0_7'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Purchase_Cost  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Purchase_Cost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('In Warranty Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Purchase_Cost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Purchase_Cost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:Purchase_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:Purchase_Cost  ! copies value to session value if valid.

ValidateValue::sto:Purchase_Cost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Purchase_Cost',sto:Purchase_Cost).
    End

Value::sto:Purchase_Cost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Purchase_Cost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Purchase_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Purchase_Cost'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::loc0_30  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc0_30') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('0 - 30 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::loc0_30  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    loc0_30 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    loc0_30 = p_web.GetValue('Value')
  End
  do ValidateValue::loc0_30  ! copies value to session value if valid.

ValidateValue::loc0_30  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('loc0_30',loc0_30).
    End

Value::loc0_30  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc0_30') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- loc0_30
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc0_30'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:PurchaseMarkUp  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:PurchaseMarkUp') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Percentage Mark Up'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:PurchaseMarkUp  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:PurchaseMarkUp = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:PurchaseMarkUp = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:PurchaseMarkUp  ! copies value to session value if valid.

ValidateValue::sto:PurchaseMarkUp  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:PurchaseMarkUp',sto:PurchaseMarkUp).
    End

Value::sto:PurchaseMarkUp  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:PurchaseMarkUp') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:PurchaseMarkUp
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:PurchaseMarkUp'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::loc31_60  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc31_60') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('31 - 60 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::loc31_60  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    loc31_60 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    loc31_60 = p_web.GetValue('Value')
  End
  do ValidateValue::loc31_60  ! copies value to session value if valid.

ValidateValue::loc31_60  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('loc31_60',loc31_60).
    End

Value::loc31_60  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc31_60') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- loc31_60
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc31_60'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Sale_Cost  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Sale_Cost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Out Warranty Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Sale_Cost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Sale_Cost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n14.2
    sto:Sale_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::sto:Sale_Cost  ! copies value to session value if valid.

ValidateValue::sto:Sale_Cost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost).
    End

Value::sto:Sale_Cost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Sale_Cost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Sale_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Sale_Cost'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::loc61_90  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc61_90') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('61 - 90 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::loc61_90  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    loc61_90 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    loc61_90 = p_web.GetValue('Value')
  End
  do ValidateValue::loc61_90  ! copies value to session value if valid.

ValidateValue::loc61_90  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('loc61_90',loc61_90).
    End

Value::loc61_90  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('loc61_90') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- loc61_90
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc61_90'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Quantity_Stock  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Quantity_Stock') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity In Stock'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Quantity_Stock  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Quantity_Stock = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @N8
    sto:Quantity_Stock = p_web.Dformat(p_web.GetValue('Value'),'@N8')
  End
  do ValidateValue::sto:Quantity_Stock  ! copies value to session value if valid.

ValidateValue::sto:Quantity_Stock  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Quantity_Stock',sto:Quantity_Stock).
    End

Value::sto:Quantity_Stock  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Quantity_Stock') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Quantity_Stock
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Quantity_Stock'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locAverageText  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('locAverageText') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Average Daily Use'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAverageText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAverageText = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAverageText = p_web.GetValue('Value')
  End
  do ValidateValue::locAverageText  ! copies value to session value if valid.

ValidateValue::locAverageText  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locAverageText',locAverageText).
    End

Value::locAverageText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('locAverageText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locAverageText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAverageText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Sundry_Item  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Sundry_Item') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Sundry Item'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Sundry_Item  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Sundry_Item = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    sto:Sundry_Item = p_web.GetValue('Value')
  End
  do ValidateValue::sto:Sundry_Item  ! copies value to session value if valid.
  do Value::sto:Sundry_Item
  do SendAlert

ValidateValue::sto:Sundry_Item  Routine
    If not (1=0)
  ! Automatic Dictionary Validation
    If InList(clip(sto:Sundry_Item) ,'NO','YES' ) = 0
      loc:Invalid = 'sto:Sundry_Item'
      sto:Sundry_Item:IsInvalid = true
      loc:Alert = p_web.translate('Sundry Item') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
      if loc:invalid = '' then p_web.SetSessionValue('sto:Sundry_Item',sto:Sundry_Item).
    End

Value::sto:Sundry_Item  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Sundry_Item') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:Sundry_Item = p_web.RestoreValue('sto:Sundry_Item')
    do ValidateValue::sto:Sundry_Item
    If sto:Sundry_Item:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:Sundry_Item
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:Sundry_Item'',''formstockcontrol_sto:sundry_item_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:Sundry_Item') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:Sundry_Item',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Suspend  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Suspend') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Suspend Part'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Suspend  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Suspend = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:Suspend = p_web.GetValue('Value')
  End
  do ValidateValue::sto:Suspend  ! copies value to session value if valid.
  do Value::sto:Suspend
  do SendAlert

ValidateValue::sto:Suspend  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Suspend',sto:Suspend).
    End

Value::sto:Suspend  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Suspend') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:Suspend = p_web.RestoreValue('sto:Suspend')
    do ValidateValue::sto:Suspend
    If sto:Suspend:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:Suspend
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:Suspend'',''formstockcontrol_sto:suspend_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:Suspend') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:Suspend',clip(1),,loc:readonly,,,loc:javascript,,'Suspend Part') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:ExchangeUnit  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeUnit') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Exchange Unit'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:ExchangeUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:ExchangeUnit = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    sto:ExchangeUnit = p_web.GetValue('Value')
  End
  do ValidateValue::sto:ExchangeUnit  ! copies value to session value if valid.
  do Value::sto:ExchangeUnit
  do SendAlert

ValidateValue::sto:ExchangeUnit  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:ExchangeUnit',sto:ExchangeUnit).
    End

Value::sto:ExchangeUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:ExchangeUnit = p_web.RestoreValue('sto:ExchangeUnit')
    do ValidateValue::sto:ExchangeUnit
    If sto:ExchangeUnit:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:ExchangeUnit
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:ExchangeUnit'',''formstockcontrol_sto:exchangeunit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:ExchangeUnit') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:ExchangeUnit',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:ExchangeOrderCap  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeOrderCap') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Exchange Order Cap'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:ExchangeOrderCap  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:ExchangeOrderCap = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n-14
    sto:ExchangeOrderCap = p_web.Dformat(p_web.GetValue('Value'),'@n-14')
  End
  do ValidateValue::sto:ExchangeOrderCap  ! copies value to session value if valid.

ValidateValue::sto:ExchangeOrderCap  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:ExchangeOrderCap',sto:ExchangeOrderCap).
    End

Value::sto:ExchangeOrderCap  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeOrderCap') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:ExchangeOrderCap
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('sto:ExchangeOrderCap'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:AllowDuplicate  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:AllowDuplicate') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Allow Duplicate Part On Jobs'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:AllowDuplicate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:AllowDuplicate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:AllowDuplicate = p_web.GetValue('Value')
  End
  do ValidateValue::sto:AllowDuplicate  ! copies value to session value if valid.
  do Value::sto:AllowDuplicate
  do SendAlert

ValidateValue::sto:AllowDuplicate  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:AllowDuplicate',sto:AllowDuplicate).
    End

Value::sto:AllowDuplicate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:AllowDuplicate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:AllowDuplicate = p_web.RestoreValue('sto:AllowDuplicate')
    do ValidateValue::sto:AllowDuplicate
    If sto:AllowDuplicate:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:AllowDuplicate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:AllowDuplicate'',''formstockcontrol_sto:allowduplicate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:AllowDuplicate') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:AllowDuplicate',clip(1),,loc:readonly,,,loc:javascript,,'<<tab.') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:ReturnFaultySpare  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ReturnFaultySpare') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Return Faulty Spare'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:ReturnFaultySpare  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:ReturnFaultySpare = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:ReturnFaultySpare = p_web.GetValue('Value')
  End
  do ValidateValue::sto:ReturnFaultySpare  ! copies value to session value if valid.
  do Value::sto:ReturnFaultySpare
  do SendAlert

ValidateValue::sto:ReturnFaultySpare  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:ReturnFaultySpare',sto:ReturnFaultySpare).
    End

Value::sto:ReturnFaultySpare  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ReturnFaultySpare') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:ReturnFaultySpare = p_web.RestoreValue('sto:ReturnFaultySpare')
    do ValidateValue::sto:ReturnFaultySpare
    If sto:ReturnFaultySpare:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:ReturnFaultySpare
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:ReturnFaultySpare'',''formstockcontrol_sto:returnfaultyspare_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:ReturnFaultySpare') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:ReturnFaultySpare',clip(1),,loc:readonly,,,loc:javascript,,'Return Faulty Spare') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:ChargeablePartOnly  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ChargeablePartOnly') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Chargeable Part Only'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:ChargeablePartOnly  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:ChargeablePartOnly = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:ChargeablePartOnly = p_web.GetValue('Value')
  End
  do ValidateValue::sto:ChargeablePartOnly  ! copies value to session value if valid.
  do Value::sto:ChargeablePartOnly
  do SendAlert

ValidateValue::sto:ChargeablePartOnly  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:ChargeablePartOnly',sto:ChargeablePartOnly).
    End

Value::sto:ChargeablePartOnly  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:ChargeablePartOnly') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:ChargeablePartOnly = p_web.RestoreValue('sto:ChargeablePartOnly')
    do ValidateValue::sto:ChargeablePartOnly
    If sto:ChargeablePartOnly:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:ChargeablePartOnly
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:ChargeablePartOnly'',''formstockcontrol_sto:chargeablepartonly_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:ChargeablePartOnly') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:ChargeablePartOnly',clip(1),,loc:readonly,,,loc:javascript,,'Chargeable Part Only') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:AttachBySolder  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:AttachBySolder') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Attached By Solder'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:AttachBySolder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:AttachBySolder = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:AttachBySolder = p_web.GetValue('Value')
  End
  do ValidateValue::sto:AttachBySolder  ! copies value to session value if valid.
  do Value::sto:AttachBySolder
  do SendAlert

ValidateValue::sto:AttachBySolder  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:AttachBySolder',sto:AttachBySolder).
    End

Value::sto:AttachBySolder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:AttachBySolder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:AttachBySolder = p_web.RestoreValue('sto:AttachBySolder')
    do ValidateValue::sto:AttachBySolder
    If sto:AttachBySolder:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:AttachBySolder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:AttachBySolder'',''formstockcontrol_sto:attachbysolder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:AttachBySolder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:AttachBySolder',clip(1),,loc:readonly,,,loc:javascript,,'Attach By Solder') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:E1  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:E1') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Access Level 1'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:E1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:E1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:E1 = p_web.GetValue('Value')
  End
  do ValidateValue::sto:E1  ! copies value to session value if valid.
  do Value::sto:E1
  do SendAlert

ValidateValue::sto:E1  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:E1',sto:E1).
    End

Value::sto:E1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:E1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:E1 = p_web.RestoreValue('sto:E1')
    do ValidateValue::sto:E1
    If sto:E1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:E1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:E1'',''formstockcontrol_sto:e1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:E1') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:E1',clip(1),,loc:readonly,,,loc:javascript,,'E1') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:E2  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:E2') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Access Level 2'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:E2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:E2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:E2 = p_web.GetValue('Value')
  End
  do ValidateValue::sto:E2  ! copies value to session value if valid.
  do Value::sto:E2
  do SendAlert

ValidateValue::sto:E2  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:E2',sto:E2).
    End

Value::sto:E2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:E2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:E2 = p_web.RestoreValue('sto:E2')
    do ValidateValue::sto:E2
    If sto:E2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:E2
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:E2'',''formstockcontrol_sto:e2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:E2') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:E2',clip(1),,loc:readonly,,,loc:javascript,,'E2') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:E3  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:E3') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Access Level 3'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:E3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:E3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    sto:E3 = p_web.GetValue('Value')
  End
  do ValidateValue::sto:E3  ! copies value to session value if valid.
  do Value::sto:E3
  do SendAlert

ValidateValue::sto:E3  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:E3',sto:E3).
    End

Value::sto:E3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:E3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    sto:E3 = p_web.RestoreValue('sto:E3')
    do ValidateValue::sto:E3
    If sto:E3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- sto:E3
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:E3'',''formstockcontrol_sto:e3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:E3') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:E3',clip(1),,loc:readonly,,,loc:javascript,,'E3') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:RepairLevel  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:RepairLevel') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Index'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:RepairLevel  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:RepairLevel = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n8
    sto:RepairLevel = p_web.Dformat(p_web.GetValue('Value'),'@n8')
  End
  do ValidateValue::sto:RepairLevel  ! copies value to session value if valid.

ValidateValue::sto:RepairLevel  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:RepairLevel',sto:RepairLevel).
    End

Value::sto:RepairLevel  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:RepairLevel') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:RepairLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:RepairLevel'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:SkillLevel  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:SkillLevel') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Skill Level'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:SkillLevel  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:SkillLevel = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n8
    sto:SkillLevel = p_web.Dformat(p_web.GetValue('Value'),'@n8')
  End
  do ValidateValue::sto:SkillLevel  ! copies value to session value if valid.

ValidateValue::sto:SkillLevel  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:SkillLevel',sto:SkillLevel).
    End

Value::sto:SkillLevel  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:SkillLevel') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:SkillLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:SkillLevel'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Manufacturer  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Manufacturer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Manufacturer'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Manufacturer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Manufacturer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Manufacturer = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Manufacturer  ! copies value to session value if valid.

ValidateValue::sto:Manufacturer  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Manufacturer',sto:Manufacturer).
    End

Value::sto:Manufacturer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockControl_' & p_web._nocolon('sto:Manufacturer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Manufacturer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Manufacturer'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::FormStockModels  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('stm:RecordNumber')
  End
  do ValidateValue::FormStockModels  ! copies value to session value if valid.

ValidateValue::FormStockModels  Routine
    If not (1=0)
    End

Value::FormStockModels  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  FormStockModels --
  p_web.SetValue('FormStockModels:NoForm',1)
  p_web.SetValue('FormStockModels:FormName',loc:formname)
  p_web.SetValue('FormStockModels:parentIs','Form')
  p_web.SetValue('_parentProc','FormStockControl')
  if p_web.RequestAjax = 0
    p_web.SSV('FormStockControl:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('FormStockControl_FormStockModels_embedded_div')&'"><!-- Net:FormStockModels --></div><13,10>'
    do SendPacket
    p_web.DivHeader('FormStockControl_' & lower('FormStockModels') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('FormStockControl:_popup_',1)
    elsif p_web.GSV('FormStockControl:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:FormStockModels --><13,10>'
  end
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormStockControl_nexttab_' & 0)
    sto:DateBooked = p_web.GSV('sto:DateBooked')
    do ValidateValue::sto:DateBooked
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:DateBooked
      !do SendAlert
      !exit
    End
    sto:Part_Number = p_web.GSV('sto:Part_Number')
    do ValidateValue::sto:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Part_Number
      !do SendAlert
      !exit
    End
    sto:Location = p_web.GSV('sto:Location')
    do ValidateValue::sto:Location
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Location
      !do SendAlert
      !exit
    End
    sto:Description = p_web.GSV('sto:Description')
    do ValidateValue::sto:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Description
      !do SendAlert
      !exit
    End
    sto:Shelf_Location = p_web.GSV('sto:Shelf_Location')
    do ValidateValue::sto:Shelf_Location
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Shelf_Location
      !do SendAlert
      !exit
    End
    sto:Supplier = p_web.GSV('sto:Supplier')
    do ValidateValue::sto:Supplier
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Supplier
      !do SendAlert
      !exit
    End
    sto:Second_Location = p_web.GSV('sto:Second_Location')
    do ValidateValue::sto:Second_Location
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Second_Location
      !do SendAlert
      !exit
    End
    sto:Accessory = p_web.GSV('sto:Accessory')
    do ValidateValue::sto:Accessory
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Accessory
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormStockControl_nexttab_' & 1)
    sto:AveragePurchaseCost = p_web.GSV('sto:AveragePurchaseCost')
    do ValidateValue::sto:AveragePurchaseCost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:AveragePurchaseCost
      !do SendAlert
      !exit
    End
    sto:Percentage_Mark_Up = p_web.GSV('sto:Percentage_Mark_Up')
    do ValidateValue::sto:Percentage_Mark_Up
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Percentage_Mark_Up
      !do SendAlert
      !exit
    End
    loc0_7 = p_web.GSV('loc0_7')
    do ValidateValue::loc0_7
    If loc:Invalid
      loc:retrying = 1
      do Value::loc0_7
      !do SendAlert
      !exit
    End
    sto:Purchase_Cost = p_web.GSV('sto:Purchase_Cost')
    do ValidateValue::sto:Purchase_Cost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Purchase_Cost
      !do SendAlert
      !exit
    End
    loc0_30 = p_web.GSV('loc0_30')
    do ValidateValue::loc0_30
    If loc:Invalid
      loc:retrying = 1
      do Value::loc0_30
      !do SendAlert
      !exit
    End
    sto:PurchaseMarkUp = p_web.GSV('sto:PurchaseMarkUp')
    do ValidateValue::sto:PurchaseMarkUp
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:PurchaseMarkUp
      !do SendAlert
      !exit
    End
    loc31_60 = p_web.GSV('loc31_60')
    do ValidateValue::loc31_60
    If loc:Invalid
      loc:retrying = 1
      do Value::loc31_60
      !do SendAlert
      !exit
    End
    sto:Sale_Cost = p_web.GSV('sto:Sale_Cost')
    do ValidateValue::sto:Sale_Cost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Sale_Cost
      !do SendAlert
      !exit
    End
    loc61_90 = p_web.GSV('loc61_90')
    do ValidateValue::loc61_90
    If loc:Invalid
      loc:retrying = 1
      do Value::loc61_90
      !do SendAlert
      !exit
    End
    sto:Quantity_Stock = p_web.GSV('sto:Quantity_Stock')
    do ValidateValue::sto:Quantity_Stock
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Quantity_Stock
      !do SendAlert
      !exit
    End
    locAverageText = p_web.GSV('locAverageText')
    do ValidateValue::locAverageText
    If loc:Invalid
      loc:retrying = 1
      do Value::locAverageText
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormStockControl_nexttab_' & 2)
    sto:Sundry_Item = p_web.GSV('sto:Sundry_Item')
    do ValidateValue::sto:Sundry_Item
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Sundry_Item
      !do SendAlert
      !exit
    End
    sto:Suspend = p_web.GSV('sto:Suspend')
    do ValidateValue::sto:Suspend
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Suspend
      !do SendAlert
      !exit
    End
    sto:ExchangeUnit = p_web.GSV('sto:ExchangeUnit')
    do ValidateValue::sto:ExchangeUnit
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:ExchangeUnit
      !do SendAlert
      !exit
    End
    sto:ExchangeOrderCap = p_web.GSV('sto:ExchangeOrderCap')
    do ValidateValue::sto:ExchangeOrderCap
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:ExchangeOrderCap
      !do SendAlert
      !exit
    End
    sto:AllowDuplicate = p_web.GSV('sto:AllowDuplicate')
    do ValidateValue::sto:AllowDuplicate
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:AllowDuplicate
      !do SendAlert
      !exit
    End
    sto:ReturnFaultySpare = p_web.GSV('sto:ReturnFaultySpare')
    do ValidateValue::sto:ReturnFaultySpare
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:ReturnFaultySpare
      !do SendAlert
      !exit
    End
    sto:ChargeablePartOnly = p_web.GSV('sto:ChargeablePartOnly')
    do ValidateValue::sto:ChargeablePartOnly
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:ChargeablePartOnly
      !do SendAlert
      !exit
    End
    sto:AttachBySolder = p_web.GSV('sto:AttachBySolder')
    do ValidateValue::sto:AttachBySolder
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:AttachBySolder
      !do SendAlert
      !exit
    End
    sto:E1 = p_web.GSV('sto:E1')
    do ValidateValue::sto:E1
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:E1
      !do SendAlert
      !exit
    End
    sto:E2 = p_web.GSV('sto:E2')
    do ValidateValue::sto:E2
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:E2
      !do SendAlert
      !exit
    End
    sto:E3 = p_web.GSV('sto:E3')
    do ValidateValue::sto:E3
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:E3
      !do SendAlert
      !exit
    End
    sto:RepairLevel = p_web.GSV('sto:RepairLevel')
    do ValidateValue::sto:RepairLevel
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:RepairLevel
      !do SendAlert
      !exit
    End
    sto:SkillLevel = p_web.GSV('sto:SkillLevel')
    do ValidateValue::sto:SkillLevel
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:SkillLevel
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormStockControl_nexttab_' & 3)
    sto:Manufacturer = p_web.GSV('sto:Manufacturer')
    do ValidateValue::sto:Manufacturer
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Manufacturer
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormStockControl_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormStockControl_tab_' & 0)
    do GenerateTab0
  of lower('FormStockControl_sto:Accessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Accessory
      of event:timer
        do Value::sto:Accessory
      else
        do Value::sto:Accessory
      end
  of lower('FormStockControl_tab_' & 1)
    do GenerateTab1
  of lower('FormStockControl_tab_' & 2)
    do GenerateTab2
  of lower('FormStockControl_sto:Sundry_Item_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Sundry_Item
      of event:timer
        do Value::sto:Sundry_Item
      else
        do Value::sto:Sundry_Item
      end
  of lower('FormStockControl_sto:Suspend_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Suspend
      of event:timer
        do Value::sto:Suspend
      else
        do Value::sto:Suspend
      end
  of lower('FormStockControl_sto:ExchangeUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:ExchangeUnit
      of event:timer
        do Value::sto:ExchangeUnit
      else
        do Value::sto:ExchangeUnit
      end
  of lower('FormStockControl_sto:AllowDuplicate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:AllowDuplicate
      of event:timer
        do Value::sto:AllowDuplicate
      else
        do Value::sto:AllowDuplicate
      end
  of lower('FormStockControl_sto:ReturnFaultySpare_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:ReturnFaultySpare
      of event:timer
        do Value::sto:ReturnFaultySpare
      else
        do Value::sto:ReturnFaultySpare
      end
  of lower('FormStockControl_sto:ChargeablePartOnly_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:ChargeablePartOnly
      of event:timer
        do Value::sto:ChargeablePartOnly
      else
        do Value::sto:ChargeablePartOnly
      end
  of lower('FormStockControl_sto:AttachBySolder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:AttachBySolder
      of event:timer
        do Value::sto:AttachBySolder
      else
        do Value::sto:AttachBySolder
      end
  of lower('FormStockControl_sto:E1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:E1
      of event:timer
        do Value::sto:E1
      else
        do Value::sto:E1
      end
  of lower('FormStockControl_sto:E2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:E2
      of event:timer
        do Value::sto:E2
      else
        do Value::sto:E2
      end
  of lower('FormStockControl_sto:E3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:E3
      of event:timer
        do Value::sto:E3
      else
        do Value::sto:E3
      end
  of lower('FormStockControl_tab_' & 3)
    do GenerateTab3
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormStockControl_form:ready_',1)

  p_web.SetSessionValue('FormStockControl_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormStockControl',0)
  Access:STOCK.PrimeRecord()
  Ans = Net:ChangeRecord
  p_web.SetSessionValue('FormStockControl:Primed',1)
  sto:Accessory = 'NO'
  p_web.SetSessionValue('sto:Accessory',sto:Accessory)
  sto:Minimum_Stock = 'NO'
  p_web.SetSessionValue('sto:Minimum_Stock',sto:Minimum_Stock)
  sto:Assign_Fault_Codes = 'NO'
  p_web.SetSessionValue('sto:Assign_Fault_Codes',sto:Assign_Fault_Codes)
  sto:Individual_Serial_Numbers = 'NO'
  p_web.SetSessionValue('sto:Individual_Serial_Numbers',sto:Individual_Serial_Numbers)
  sto:ExchangeUnit = 'NO'
  p_web.SetSessionValue('sto:ExchangeUnit',sto:ExchangeUnit)
  sto:Suspend = 0
  p_web.SetSessionValue('sto:Suspend',sto:Suspend)
  sto:E1 = 1
  p_web.SetSessionValue('sto:E1',sto:E1)
  sto:E2 = 1
  p_web.SetSessionValue('sto:E2',sto:E2)
  sto:E3 = 1
  p_web.SetSessionValue('sto:E3',sto:E3)
  sto:ReturnFaultySpare = 0
  p_web.SetSessionValue('sto:ReturnFaultySpare',sto:ReturnFaultySpare)
  sto:ChargeablePartOnly = 0
  p_web.SetSessionValue('sto:ChargeablePartOnly',sto:ChargeablePartOnly)
  sto:AttachBySolder = 0
  p_web.SetSessionValue('sto:AttachBySolder',sto:AttachBySolder)
  sto:AllowDuplicate = 0
  p_web.SetSessionValue('sto:AllowDuplicate',sto:AllowDuplicate)
  sto:DateBooked = Today()
  p_web.SetSessionValue('sto:DateBooked',sto:DateBooked)
  sto:ExcludeLevel12Repair = 0
  p_web.SetSessionValue('sto:ExcludeLevel12Repair',sto:ExcludeLevel12Repair)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockControl',0)
  Access:STOCK.PrimeRecord()
  Ans = Net:ChangeRecord
  p_web.SetSessionValue('FormStockControl:Primed',1)
  p_web._PreCopyRecord(STOCK,sto:Ref_Number_Key,Net:Web:Autonumbered)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormStockControl:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormStockControl:Primed',0)
  p_web.setsessionvalue('showtab_FormStockControl',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockControl_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormStockControl_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::sto:DateBooked
    If loc:Invalid then exit.
    do ValidateValue::sto:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::sto:Location
    If loc:Invalid then exit.
    do ValidateValue::sto:Description
    If loc:Invalid then exit.
    do ValidateValue::sto:Shelf_Location
    If loc:Invalid then exit.
    do ValidateValue::sto:Supplier
    If loc:Invalid then exit.
    do ValidateValue::sto:Second_Location
    If loc:Invalid then exit.
    do ValidateValue::sto:Accessory
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::sto:AveragePurchaseCost
    If loc:Invalid then exit.
    do ValidateValue::txtUsage
    If loc:Invalid then exit.
    do ValidateValue::sto:Percentage_Mark_Up
    If loc:Invalid then exit.
    do ValidateValue::loc0_7
    If loc:Invalid then exit.
    do ValidateValue::sto:Purchase_Cost
    If loc:Invalid then exit.
    do ValidateValue::loc0_30
    If loc:Invalid then exit.
    do ValidateValue::sto:PurchaseMarkUp
    If loc:Invalid then exit.
    do ValidateValue::loc31_60
    If loc:Invalid then exit.
    do ValidateValue::sto:Sale_Cost
    If loc:Invalid then exit.
    do ValidateValue::loc61_90
    If loc:Invalid then exit.
    do ValidateValue::sto:Quantity_Stock
    If loc:Invalid then exit.
    do ValidateValue::locAverageText
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::sto:Sundry_Item
    If loc:Invalid then exit.
    do ValidateValue::sto:Suspend
    If loc:Invalid then exit.
    do ValidateValue::sto:ExchangeUnit
    If loc:Invalid then exit.
    do ValidateValue::sto:ExchangeOrderCap
    If loc:Invalid then exit.
    do ValidateValue::sto:AllowDuplicate
    If loc:Invalid then exit.
    do ValidateValue::sto:ReturnFaultySpare
    If loc:Invalid then exit.
    do ValidateValue::sto:ChargeablePartOnly
    If loc:Invalid then exit.
    do ValidateValue::sto:AttachBySolder
    If loc:Invalid then exit.
    do ValidateValue::sto:E1
    If loc:Invalid then exit.
    do ValidateValue::sto:E2
    If loc:Invalid then exit.
    do ValidateValue::sto:E3
    If loc:Invalid then exit.
    do ValidateValue::sto:RepairLevel
    If loc:Invalid then exit.
    do ValidateValue::sto:SkillLevel
    If loc:Invalid then exit.
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::sto:Manufacturer
    If loc:Invalid then exit.
    do ValidateValue::FormStockModels
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
    If sto:LoanUnit <> '1' and sto:LoanUnit <> '0'
      loc:Invalid = 'sto:LoanUnit'
      loc:Alert = p_web.translate('sto:LoanUnit') & ' ' & clip(p_web.site.OneOfText) & ' ' & '1' & ' / ' & '0'
    End
  If Loc:Invalid <> '' then exit.
  ! Automatic Dictionary Validation
    If InList(clip(sto:Sundry_Item) ,'NO','YES' ) = 0
      loc:Invalid = 'sto:Sundry_Item'
      loc:Alert = p_web.translate('sto:Sundry_Item') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  If Loc:Invalid <> '' then exit.
  ! Automatic Dictionary Validation
    If InList(clip(sto:Superceeded) ,'NO','YES' ) = 0
      loc:Invalid = 'sto:Superceeded'
      loc:Alert = p_web.translate('sto:Superceeded') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  If Loc:Invalid <> '' then exit.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('FormStockControl:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormStockControl:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('loc0_7')
  p_web.StoreValue('loc0_30')
  p_web.StoreValue('loc31_60')
  p_web.StoreValue('loc61_90')
  p_web.StoreValue('locAverageText')
  p_web.StoreValue('')


PostDelete      Routine
BrowsePayments       PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
jpt:Date:IsInvalid  Long
jpt:Payment_Type:IsInvalid  Long
jpt:User_Code:IsInvalid  Long
jpt:Amount:IsInvalid  Long
Edit:IsInvalid  Long
Delete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBPAYMT)
                      Project(jpt:Record_Number)
                      Project(jpt:Date)
                      Project(jpt:Payment_Type)
                      Project(jpt:User_Code)
                      Project(jpt:Amount)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
JOBS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BrowsePayments')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowsePayments:NoForm')
      loc:NoForm = p_web.GetValue('BrowsePayments:NoForm')
      loc:FormName = p_web.GetValue('BrowsePayments:FormName')
    else
      loc:FormName = 'BrowsePayments_frm'
    End
    p_web.SSV('BrowsePayments:NoForm',loc:NoForm)
    p_web.SSV('BrowsePayments:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowsePayments:NoForm')
    loc:FormName = p_web.GSV('BrowsePayments:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowsePayments') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowsePayments')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowsePayments' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowsePayments')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowsePayments') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowsePayments')
      p_web.DivHeader('popup_BrowsePayments','nt-hidden')
      p_web.DivHeader('BrowsePayments',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowsePayments_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowsePayments_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowsePayments',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBPAYMT,jpt:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JPT:DATE') then p_web.SetValue('BrowsePayments_sort','1')
    ElsIf (loc:vorder = 'JPT:PAYMENT_TYPE') then p_web.SetValue('BrowsePayments_sort','2')
    ElsIf (loc:vorder = 'JPT:USER_CODE') then p_web.SetValue('BrowsePayments_sort','3')
    ElsIf (loc:vorder = 'JPT:AMOUNT') then p_web.SetValue('BrowsePayments_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowsePayments:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowsePayments:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowsePayments:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowsePayments:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormPayments'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowsePayments')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowsePayments_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowsePayments_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'jpt:Date','-jpt:Date')
    Loc:LocateField = 'jpt:Date'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jpt:Payment_Type)','-UPPER(jpt:Payment_Type)')
    Loc:LocateField = 'jpt:Payment_Type'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jpt:User_Code)','-UPPER(jpt:User_Code)')
    Loc:LocateField = 'jpt:User_Code'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'jpt:Amount','-jpt:Amount')
    Loc:LocateField = 'jpt:Amount'
    Loc:LocatorCase = 0
  of 5
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 6
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+jpt:Ref_Number,+jpt:Date'
  end
  If False ! add range fields to sort order
  Else
    If Instring('JPT:REF_NUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'jpt:Ref_Number,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('jpt:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@d6b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('jpt:Payment_Type')
    loc:SortHeader = p_web.Translate('Payment Type')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@s30')
  Of upper('jpt:User_Code')
    loc:SortHeader = p_web.Translate('User Code')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@s3')
  Of upper('jpt:Amount')
    loc:SortHeader = p_web.Translate('Payment Received')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowsePayments:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowsePayments:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowsePayments:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowsePayments:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBPAYMT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jpt:Record_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowsePayments.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePayments',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowsePayments','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowsePayments_LocatorPic'),,,'onchange="BrowsePayments.locate(''Locator2BrowsePayments'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowsePayments',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowsePayments.locate(''Locator2BrowsePayments'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowsePayments_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePayments.cl(''BrowsePayments'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowsePayments_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowsePayments_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowsePayments_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowsePayments_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowsePayments_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowsePayments',p_web.Translate('Date'),'Click here to sort by Date',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowsePayments',p_web.Translate('Payment Type'),'Click here to sort by Payment Type',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowsePayments',p_web.Translate('User Code'),'Click here to sort by User Code',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowsePayments',p_web.Translate('Payment Received'),'Click here to sort by Payment Received',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowsePayments',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowsePayments',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jpt:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('jpt:record_number',lower(loc:vorder),1,1) = 0 !and JOBPAYMT{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','jpt:Record_Number',clip(loc:vorder) & ',' & 'jpt:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jpt:Record_Number'),p_web.GetValue('jpt:Record_Number'),p_web.GetSessionValue('jpt:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'jpt:Ref_Number = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePayments',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowsePayments_Filter')
    p_web.SetSessionValue('BrowsePayments_FirstValue','')
    p_web.SetSessionValue('BrowsePayments_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBPAYMT,jpt:Record_Number_Key,loc:PageRows,'BrowsePayments',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBPAYMT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBPAYMT,loc:firstvalue)
              Reset(ThisView,JOBPAYMT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBPAYMT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBPAYMT,loc:lastvalue)
            Reset(ThisView,JOBPAYMT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jpt:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Payments')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowsePayments_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePayments.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePayments.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePayments.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePayments.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowsePayments_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowsePayments_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:selecting = 0 or loc:popup
      If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowsePayments',,,loc:FormPopup,'FormPayments')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowsePayments_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePayments',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowsePayments_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowsePayments_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowsePayments','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowsePayments_LocatorPic'),,,'onchange="BrowsePayments.locate(''Locator1BrowsePayments'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowsePayments',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowsePayments.locate(''Locator1BrowsePayments'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowsePayments_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePayments.cl(''BrowsePayments'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowsePayments_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowsePayments_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowsePayments_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowsePayments_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePayments.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePayments.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePayments.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePayments.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowsePayments_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowsePayments_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:selecting = 0 or loc:popup
    If loc:viewonly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowsePayments',,,loc:FormPopup,'FormPayments')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowsePayments_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowsePayments','JOBPAYMT',jpt:Record_Number_Key) !jpt:Record_Number
    p_web._thisrow = p_web._nocolon('jpt:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and jpt:Record_Number = p_web.GetValue('jpt:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowsePayments:LookupField')) = jpt:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((jpt:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowsePayments','JOBPAYMT',jpt:Record_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBPAYMT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBPAYMT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBPAYMT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBPAYMT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jpt:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jpt:Payment_Type
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jpt:User_Code
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jpt:Amount
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowsePayments','JOBPAYMT',jpt:Record_Number_Key)
  TableQueue.Id[1] = jpt:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowsePayments;if (btiBrowsePayments != 1){{var BrowsePayments=new browseTable(''BrowsePayments'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('jpt:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormPayments'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowsePayments.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowsePayments.applyGreenBar();btiBrowsePayments=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePayments')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePayments')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePayments')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePayments')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBPAYMT)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBPAYMT)
  Bind(jpt:Record)
  Clear(jpt:Record)
  NetWebSetSessionPics(p_web,JOBPAYMT)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('jpt:Record_Number',p_web.GetValue('jpt:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  jpt:Record_Number = p_web.GSV('jpt:Record_Number')
  loc:result = p_web._GetFile(JOBPAYMT,jpt:Record_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jpt:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBPAYMT)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBPAYMT)
! ----------------------------------------------------------------------------------------
value::jpt:Date   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePayments_jpt:Date_'&jpt:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jpt:Date,'@d6b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jpt:Payment_Type   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePayments_jpt:Payment_Type_'&jpt:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jpt:Payment_Type,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jpt:User_Code   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePayments_jpt:User_Code_'&jpt:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jpt:User_Code,'@s3')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jpt:Amount   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePayments_jpt:Amount_'&jpt:Record_Number,,net:crc,,loc:extra)
      loc:total[4] = loc:total[4] + (jpt:Amount)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jpt:Amount,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePayments_Edit_'&jpt:Record_Number,,net:crc,,loc:extra)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowsePayments',p_web.AddBrowseValue('BrowsePayments','JOBPAYMT',jpt:Record_Number_Key),,loc:FormPopup,'FormPayments') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePayments_Delete_'&jpt:Record_Number,,net:crc,,loc:extra)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowsePayments',p_web.AddBrowseValue('BrowsePayments','JOBPAYMT',jpt:Record_Number_Key),,loc:FormPopup,'FormPayments') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('jpt:Record_Number',jpt:Record_Number)

MakeFooter  Routine
  TableQueue.Kind = Net:RowFooter
  Clear(TableQueue.Id)
  Clear(TableQueue.Idx)
  If records(TableQueue) > 0
    packet = clip(packet) & '<tr>'
    If(loc:SelectionMethod  = Net:Radio)
      packet = clip(packet) & '<td width="1">&#160;</td>' ! first column is the select column
    End
      If loc:skip = 0
        loc:class = ' class="'&p_web.Combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&p_web.Combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&p_web.Combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = p_web.Combine(p_web.site.style.BrowseFooter,'BrowseFooter')
        If loc:class <> '' then loc:class = ' class="'&clip(loc:class)&'"'.
          loc:skip = 1
        packet = clip(packet) & '<td'&clip(loc:class)&'>' & Format(loc:total[4],'@n-14.2') &'</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:Selecting = 0
      If loc:skip = 0
        loc:class = ' class="'&p_web.Combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      End
      If loc:Selecting = 0
      If loc:skip = 0
        loc:class = ' class="'&p_web.Combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      End
    packet = clip(packet) & '</tr>'
    TableQueue.Kind = Net:RowFooter
  End
  do AddPacket

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
