

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE025.INC'),ONCE        !Local module procedure declarations
                     END


GoToBottom           PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:GoToBottom -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('GoToBottom')
    packet = '<script type="text/javascript">' & CRLF &|
            'window.location=''#bottom''' & CRLF &|
            '</script>'
    Do SendPacket
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
SetHubRepair         PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:SetHubRepair -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
FilesOpened     Long
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('SetHubRepair')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'SetHubRepair' & '_' & loc:parent
  Else
    loc:divname = 'SetHubRepair'
  End
    Do OpenFiles
    Do SaveFiles
    p_web.SetSessionValue('jobe:HubRepair',1)
    p_web.SetSessionValue('jobe:HubRepairDate',Today())
    p_web.SetSessionValue('jobe:HubRepairTime',Clock())
    Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
    rtd:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
    Set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
    Loop
        If Access:REPTYDEF.Next()
            Break
        End ! If Access:REPTYDEF.Next()
        If rtd:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
            Break
        End ! If rtd:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
        If rtd:BER = 11
            If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
                p_web.SetSessionValue('job:Repair_Type',rtd:Repair_Type)
            End ! If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
            If p_web.GetSessionValue('job:Warranty_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type_Warranty') = ''
                p_web.SetSessionValue('job:Repair_Type_Warranty',rtd:Repair_Type)
            End ! If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
            Break
        End ! If rtd:BER = 11
    End ! Loop

    ! Lookup up current engineer and mark as "HUB" (DBH: 18/01/2008)
    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber = p_web.GetSessionValue('job:Ref_Number')
    joe:UserCode = p_web.GetSessionValue('job:Engineer')
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.Next()
            Break
        End ! If Access:JOBSENG.Next()
        If joe:JobNumber <> p_web.GetSessionValue('job:Ref_Number')
            Break
        End ! If joe:JobNumber <> p_web.GetSessionValue('job:Ref_Number')
        If joe:UserCode <> p_web.GetSessionValue('job:Engineer')
            Break
        End ! If joe:UserCode <> p_web.GetSessionValue('job:Engineer')
        If joe:DateAllocated > Today()
            Break
        End ! If joe:DateAllocated > Today()
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        Access:JOBSENG.TryUPdate()
        Break
    End ! Loop

    ! Check for SMS/Email Alerts (DBH: 18/01/2008)
    If p_web.GetSessionValue('jobe2:SMSNotification')
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('job:account_Number'),|
            '2ARC','SMS',p_web.GetSessionValue('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('wob:HeadAccountNumber'),|
            '2ARC','SMS',p_web.GetSessionValue('jobe2:SMSAlertNumber'),'',0,'')
        end !if (p_web.GSV('job:Who_Booked') = 'WEB')
    End ! If p_web.GetSessionValue('jobe2:SMSNotification')
    If p_web.GetSessionValue('jobe2:EmailNotification')
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('job:account_Number'),|
            '2ARC','EMAIL',p_web.GetSessionValue('jobe2:EMailAlertAddress'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('wob:HeadAccountNumber'),|
            '2ARC','EMAIL',p_web.GetSessionValue('jobe2:EMailAlertAddress'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    End ! If p_web.GetSessionValue('jobe2:SMSNotification')

    Do RestoreFiles
    Do CloseFiles
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
  p_web.DivHeader(loc:divname,'adiv')
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web.DivFooter()
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
SaveFiles  ROUTINE
  JOBSENG::State = Access:JOBSENG.SaveFile()               ! Save File referenced in 'Other Files' so need to inform it's FileManager
  REPTYDEF::State = Access:REPTYDEF.SaveFile()             ! Save File referenced in 'Other Files' so need to inform it's FileManager
!--------------------------------------
RestoreFiles  ROUTINE
  IF JOBSENG::State <> 0
    Access:JOBSENG.RestoreFile(JOBSENG::State)             ! Restore File referenced in 'Other Files' so need to inform it's FileManager
  END
  IF REPTYDEF::State <> 0
    Access:REPTYDEF.RestoreFile(REPTYDEF::State)           ! Restore File referenced in 'Other Files' so need to inform it's FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
     FilesOpened = False
  END
IsThisModelAlternative PROCEDURE  (func:OriginalModel,func:NewModel) ! Declare Procedure
ESNMODEL::State  USHORT
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
    esn:Model_Number = func:OriginalModel
    Set(esn:Model_Number_Key,esn:Model_Number_Key)
    Loop
        If Access:ESNMODEL.NEXT()
           Break
        End !If
        If esn:Model_Number <> func:OriginalModel      |
            Then Break.  ! End If
        !Now check the alternative models to see if the
        !scanned model matches -  (DBH: 29-10-2003)
        Access:ESNMODAL.ClearKey(esa:RefModelNumberKey)
        esa:RefNumber   = esn:Record_Number
        esa:ModelNumber = func:NewModel
        If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Found
            Return# = 1
            Break
        Else !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Error
        End !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign

    End !Loop

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODEL.Close
     Access:ESNMODAL.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  ESNMODEL::State = Access:ESNMODEL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODEL::State <> 0
    Access:ESNMODEL.RestoreFile(ESNMODEL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
IsDateFormatInvalid  PROCEDURE  (f:Date)                   ! Declare Procedure
  CODE
    If Len(Clip(f:Date)) <> 10
        Return 1
    End ! If Len(Clip(f:Date)) <> 10
    If Sub(f:Date,3,1) <> '/'
        Return 1
    End ! If Sub(f:Date,3,1) <> '/'

    If Sub(f:Date,6,1) <> '/'
        Return 1
    End ! If Sub(f:date,6,1) <> '/'

    Return 0
BouncerHistory PROCEDURE (<NetWebServerWorker p_web>)      ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
Progress:Thermometer BYTE                                  !
tmp:Parts            STRING(25),DIM(15)                    !
tmp:IMEINumber       STRING(30)                            !IMEI Number
code_temp            BYTE                                  !
option_temp          BYTE                                  !
bar_code_string_temp CSTRING(21)                           !
bar_code_temp        CSTRING(21)                           !
barcodeJobNumber     STRING(20)                            !
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_Despatched)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                         PROJECT(jbn_ali:Invoice_Text)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(438,1135,7521,9740),PAPER(PAPER:A4),PRE(rpt),FONT('Tahoma',8,,FONT:regular),THOUS
                       HEADER,AT(396,469,7521,1000),USE(?unnamed)
                         STRING('Page:'),AT(6406,208),USE(?String35),TRN,FONT(,8,,FONT:bold)
                         STRING(@N3),AT(6823,208),USE(ReportPageNumber),TRN,FONT(,8,,FONT:bold)
                       END
detail                 DETAIL,AT(,,,3042),USE(?detailband)
                         STRING('Job Number:'),AT(208,52,1094,313),USE(?String2),TRN,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s8),AT(1823,260),USE(job_ali:Ref_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Unit Details:'),AT(208,521,1094,313),USE(?String2:2),TRN,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number:'),AT(1823,677),USE(?String6:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Booked:'),AT(5156,52),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6302,52),USE(job_ali:date_booked),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Date:'),AT(4531,52,1094,313),USE(?String2:4),TRN,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         STRING(@D6b),AT(6302,208),USE(job_ali:Date_Completed),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s20),AT(1823,52,2604,208),USE(barcodeJobNumber),LEFT,FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING(@D6b),AT(6302,365),USE(job_ali:Date_Despatched),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Despatched:'),AT(5156,365),USE(?String6:8),TRN,FONT(,8,,FONT:bold)
                         STRING('Completed:'),AT(5156,208),USE(?String6:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Reported Fault:'),AT(208,1042,1094,313),USE(?String2:5),TRN,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(1823,1042,5521,417),USE(jbn_ali:Fault_Description),TRN,FONT(,8,,,CHARSET:ANSI)
                         TEXT,AT(1823,1510,5521,417),USE(jbn_ali:Invoice_Text),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s16),AT(6250,521),USE(job_ali:ESN),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Mobile Number:'),AT(5156,833),USE(?String6:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Repair Details:'),AT(208,1510,1094,313),USE(?String2:6),TRN,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s16),AT(6250,677),USE(job_ali:MSN),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Parts Used:'),AT(208,1979,1094,313),USE(?String2:3),TRN,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         STRING('Chargeable Parts'),AT(208,2240),USE(?String37),TRN,FONT(,8,,FONT:underline)
                         STRING('Warranty Parts'),AT(3594,2240),USE(?String37:2),TRN,FONT(,8,,FONT:underline)
                         STRING(@s25),AT(1927,2448),USE(tmp:Parts[2]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2604),USE(tmp:Parts[3]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(1927,2604),USE(tmp:Parts[4]),FONT(,8,,)
                         STRING(@s25),AT(1927,2760),USE(tmp:Parts[6]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2448),USE(tmp:Parts[7]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2448),USE(tmp:Parts[8]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2760),USE(tmp:Parts[5]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2604),USE(tmp:Parts[10]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2604),USE(tmp:Parts[9]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2760),USE(tmp:Parts[12]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2760),USE(tmp:Parts[11]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2448),USE(tmp:Parts[1]),FONT(,8,,,CHARSET:ANSI)
                         LINE,AT(208,2969,7031,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Unit Type:'),AT(1823,833),USE(?String6:4),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(2917,677),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('I.M.E.I. Number:'),AT(5156,521),USE(?String6:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(1823,521),USE(?String6:2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(2917,521),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('M.S.N.:'),AT(5156,677),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(2917,833),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s16),AT(6250,833),USE(job_ali:Mobile_Number),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('BOUNCER HISTORY REPORT'),AT(1260,52,5000,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BouncerHistory')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:IMEINumber = p_web.GSV('job:ESN')
  Relate:JOBS_ALIAS.SetOpenRelated()
  Relate:JOBS_ALIAS.Open                                   ! File JOBS_ALIAS used by this procedure, so make sure it's RelationManager is open
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('BouncerHistory',ProgressWindow)            ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:ESN)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(job_ali:ESN_Key)
  ThisReport.AddRange(job_ali:ESN,tmp:IMEINumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
  END
  IF SELF.Opened
    INIMgr.Update('BouncerHistory',ProgressWindow)         ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Clear(tmp:Parts)
  PartNumber# = 0
  Access:PARTS.Clearkey(par:Part_Number_Key)
  par:Ref_Number = job_ali:Ref_Number
  Set(par:Part_Number_Key,par:Part_Number_Key)
  Loop
      If Access:PARTS.Next()
          Break
      End ! If Access:PARTS.Next()
      If par:Ref_Number <> job_ali:Ref_Number
          Break
      End ! If par:Ref_Number <> job_ali:Ref_Number
      PartNumber# += 1
      If PartNumber# > 6
          Break
      End ! If PartNumber# > 6
      tmp:Parts[PartNumber#] = par:Description
  End ! Loop
  
  PartNumber# = 6
  Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
  wpr:Ref_Number = job_ali:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.Next()
          Break
      End ! If Access:WARPARTS.Next()
      If wpr:Ref_Number <> job_ali:Ref_Number
          Break
      End ! If wpr:Ref_Number <> job_ali:Ref_Number
      PartNumber# += 1
      If PartNumber# > 12
          Break
      End ! If PartNumber# > 12
      tmp:Parts[PartNumber#] = wpr:Description
  End ! Loop
  
  barcodeJobNumber = '*' & clip(job_ali:Ref_Number) & '*'
  
  !!Barcode Bit and setup refno
  !code_temp            = 3
  !option_temp          = 0
  !
  !bar_code_string_temp = Clip(job_ali:Ref_Number)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  !
  !Settarget(Report)
  !
  !Draw_JobNo.Blank(Color:White)
  !Draw_JobNo.FontName = 'C128 High 12pt LJ3'
  !Draw_JobNo.FontStyle = font:Regular
  !Draw_JobNo.FontSize = 12
  !Draw_JobNo.Show(0,0,Bar_Code_Temp)
  !Draw_JobNo.Display()
  !
  !
  !SetTarget()
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','BouncerHistory','BouncerHistory','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

