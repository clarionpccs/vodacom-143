

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE036.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE033.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE037.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE038.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE039.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE040.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE042.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE043.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE044.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE045.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE046.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE061.INC'),ONCE        !Req'd for module callout resolution
                     END


TagValidateLoanAccessories PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
tag:tagged:IsInvalid  Long
acr:Accessory:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(ACCESSOR)
                      Project(acr:Accessory)
                      Project(acr:Accessory)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
TagFile::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('TagValidateLoanAccessories')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('TagValidateLoanAccessories:NoForm')
      loc:NoForm = p_web.GetValue('TagValidateLoanAccessories:NoForm')
      loc:FormName = p_web.GetValue('TagValidateLoanAccessories:FormName')
    else
      loc:FormName = 'TagValidateLoanAccessories_frm'
    End
    p_web.SSV('TagValidateLoanAccessories:NoForm',loc:NoForm)
    p_web.SSV('TagValidateLoanAccessories:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('TagValidateLoanAccessories:NoForm')
    loc:FormName = p_web.GSV('TagValidateLoanAccessories:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('TagValidateLoanAccessories') & '_' & lower(loc:parent)
  else
    loc:divname = lower('TagValidateLoanAccessories')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'TagValidateLoanAccessories' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','TagValidateLoanAccessories')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('TagValidateLoanAccessories') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('TagValidateLoanAccessories')
      p_web.DivHeader('popup_TagValidateLoanAccessories','nt-hidden')
      p_web.DivHeader('TagValidateLoanAccessories',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_TagValidateLoanAccessories_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_TagValidateLoanAccessories_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('TagValidateLoanAccessories',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ACCESSOR,acr:AccessOnlyKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TAG:TAGGED') then p_web.SetValue('TagValidateLoanAccessories_sort','2')
    ElsIf (loc:vorder = 'ACR:ACCESSORY') then p_web.SetValue('TagValidateLoanAccessories_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('TagValidateLoanAccessories:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('TagValidateLoanAccessories:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('TagValidateLoanAccessories:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('TagValidateLoanAccessories:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'TagValidateLoanAccessories'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('TagValidateLoanAccessories')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('TagValidateLoanAccessories_sort',net:DontEvaluate)
  p_web.SetSessionValue('TagValidateLoanAccessories_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tag:tagged','-tag:tagged')
    Loc:LocateField = 'tag:tagged'
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(acr:Accessory)','-UPPER(acr:Accessory)')
    Loc:LocateField = 'acr:Accessory'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(acr:Model_Number),+UPPER(acr:Accessory)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('Tag')
    p_web.SetSessionValue('TagValidateLoanAccessories_LocatorPic','@n3')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('acr:Accessory')
    loc:SortHeader = p_web.Translate('Accessory')
    p_web.SetSessionValue('TagValidateLoanAccessories_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('TagValidateLoanAccessories:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="TagValidateLoanAccessories:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="TagValidateLoanAccessories:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('TagValidateLoanAccessories:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ACCESSOR"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="acr:AccessOnlyKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{TagValidateLoanAccessories.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagValidateLoanAccessories',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2TagValidateLoanAccessories','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('TagValidateLoanAccessories_LocatorPic'),,,'onchange="TagValidateLoanAccessories.locate(''Locator2TagValidateLoanAccessories'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2TagValidateLoanAccessories',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="TagValidateLoanAccessories.locate(''Locator2TagValidateLoanAccessories'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="TagValidateLoanAccessories_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'TagValidateLoanAccessories.cl(''TagValidateLoanAccessories'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'TagValidateLoanAccessories_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('TagValidateLoanAccessories_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="TagValidateLoanAccessories_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('TagValidateLoanAccessories_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="TagValidateLoanAccessories_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','TagValidateLoanAccessories',p_web.Translate('Tag'),'Click here to sort by tagged',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','TagValidateLoanAccessories',p_web.Translate('Accessory'),'Click here to sort by Accessory',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('acr:accessory',lower(loc:vorder),1,1) = 0 !and ACCESSOR{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','acr:Accessory',clip(loc:vorder) & ',' & 'acr:Accessory')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('acr:Accessory'),p_web.GetValue('acr:Accessory'),p_web.GetSessionValue('acr:Accessory'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(acr:Model_Number) = Upper(''' & p_web.GSV('tmp:LoanModelNumber') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagValidateLoanAccessories',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('TagValidateLoanAccessories_Filter')
    p_web.SetSessionValue('TagValidateLoanAccessories_FirstValue','')
    p_web.SetSessionValue('TagValidateLoanAccessories_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ACCESSOR,acr:AccessOnlyKey,loc:PageRows,'TagValidateLoanAccessories',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ACCESSOR{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ACCESSOR,loc:firstvalue)
              Reset(ThisView,ACCESSOR)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ACCESSOR{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ACCESSOR,loc:lastvalue)
            Reset(ThisView,ACCESSOR)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acr:Accessory)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="TagValidateLoanAccessories_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'TagValidateLoanAccessories.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'TagValidateLoanAccessories.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'TagValidateLoanAccessories.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'TagValidateLoanAccessories.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'TagValidateLoanAccessories_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagValidateLoanAccessories',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('TagValidateLoanAccessories_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('TagValidateLoanAccessories_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1TagValidateLoanAccessories','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('TagValidateLoanAccessories_LocatorPic'),,,'onchange="TagValidateLoanAccessories.locate(''Locator1TagValidateLoanAccessories'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1TagValidateLoanAccessories',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="TagValidateLoanAccessories.locate(''Locator1TagValidateLoanAccessories'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="TagValidateLoanAccessories_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'TagValidateLoanAccessories.cl(''TagValidateLoanAccessories'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'TagValidateLoanAccessories_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('TagValidateLoanAccessories_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('TagValidateLoanAccessories_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="TagValidateLoanAccessories_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'TagValidateLoanAccessories.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'TagValidateLoanAccessories.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'TagValidateLoanAccessories.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'TagValidateLoanAccessories.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'TagValidateLoanAccessories_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.SessionID
    tag:taggedValue    = acr:Accessory
    if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Found
    else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Error
        tag:Tagged = 0
    end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
    loc:field = p_web.AddBrowseValue('TagValidateLoanAccessories','ACCESSOR',acr:AccessOnlyKey) !acr:Accessory
    p_web._thisrow = p_web._nocolon('acr:Accessory')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and acr:Accessory = p_web.GetValue('acr:Accessory')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('TagValidateLoanAccessories:LookupField')) = acr:Accessory and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((acr:Accessory = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('TagValidateLoanAccessories','ACCESSOR',acr:AccessOnlyKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ACCESSOR{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ACCESSOR)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ACCESSOR{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ACCESSOR)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(50)&'"><13,10>'
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::acr:Accessory
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('TagValidateLoanAccessories','ACCESSOR',acr:AccessOnlyKey)
  TableQueue.Id[1] = acr:Accessory

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiTagValidateLoanAccessories;if (btiTagValidateLoanAccessories != 1){{var TagValidateLoanAccessories=new browseTable(''TagValidateLoanAccessories'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('acr:Accessory',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'TagValidateLoanAccessories.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'TagValidateLoanAccessories.applyGreenBar();btiTagValidateLoanAccessories=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2TagValidateLoanAccessories')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1TagValidateLoanAccessories')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1TagValidateLoanAccessories')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2TagValidateLoanAccessories')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ACCESSOR)
  p_web._CloseFile(TagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ACCESSOR)
  Bind(acr:Record)
  Clear(acr:Record)
  NetWebSetSessionPics(p_web,ACCESSOR)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('acr:Accessory',p_web.GetValue('acr:Accessory'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  acr:Accessory = p_web.GSV('acr:Accessory')
  loc:result = p_web._GetFile(ACCESSOR,acr:AccessOnlyKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acr:Accessory)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(ACCESSOR)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(ACCESSOR)
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  acr:Accessory = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(ACCESSOR,acr:AccessOnlyKey)
  p_web.FileToSessionQueue(ACCESSOR)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = 1,1,0)
  ! Validation goes here, if fails set loc:invalid & loc:alert
  Access:TAGFILE.Clearkey(tag:keyTagged)
  tag:sessionID    = p_web.SessionID
  tag:taggedValue    = acr:Accessory
  if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Found
      tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
      access:TAGFILE.tryUpdate()
  else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Error
      if (Access:TAGFILE.PrimeRecord() = Level:Benign)
          tag:sessionID    = p_web.sessionID
          tag:taggedValue    = acr:Accessory
          tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
          if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Inserted
          else ! if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Error
          end ! if (Access:TAGFILE.TryInsert() = Level:Benign)
      end ! if (Access:TAGFILE.PrimeRecord() = Level:Benign)
  end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  if loc:invalid = ''
    p_web.SetSessionValue('tag:tagged',tag:tagged)
    do CheckForDuplicate
  Else
    tag:tagged = loc:was
  End
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  Else
    do SendAlert
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('TagValidateLoanAccessories_tag:tagged_'&acr:Accessory,,net:crc,,loc:extra)
      If (p_web.GSV('Hide:ValidateAccessoriesButton') <> 1) and loc:Viewonly = 0
          loc:fieldclass = ''
          loc:extra = Choose(tag:tagged = 1,'checked','')
              loc:javascript = 'onchange="TagValidateLoanAccessories.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"'
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&acr:Accessory),clip(1),p_web.combine(p_web.site.style.browseCheck,'BrowseEntry',loc:fieldclass),loc:extra,,,loc:javascript,,) & '<13,10>'
      Else
          loc:fieldclass = ''
          loc:extra = Choose(tag:tagged = 1,'checked','')
              loc:javascript = 'onchange="TagValidateLoanAccessories.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"'
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&acr:Accessory),clip(1),p_web.combine(p_web.site.style.browseCheck,'BrowseEntry',loc:fieldclass),loc:extra & ' disabled',,,loc:javascript,,) & '<13,10>'
      End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::acr:Accessory   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('TagValidateLoanAccessories_acr:Accessory_'&acr:Accessory,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(acr:Accessory,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(acr:Accesory_Key)
    loc:Invalid = 'acr:Model_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Accesory_Key --> acr:Model_Number, '&clip('Accessory')&''
  End
  If Duplicate(acr:Model_Number_Key)
    loc:Invalid = 'acr:Accessory'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> '&clip('Accessory')&', acr:Model_Number'
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('acr:Accessory',acr:Accessory)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseBatchesInProgress PROCEDURE  (NetWebServerWorker p_web)
locCompanyName       STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
muld:AccountNumber:IsInvalid  Long
locCompanyName:IsInvalid  Long
muld:Courier:IsInvalid  Long
muld:BatchNumber:IsInvalid  Long
muld:BatchTotal:IsInvalid  Long
muld:BatchType:IsInvalid  Long
ViewJobsInBatch:IsInvalid  Long
Delete:IsInvalid  Long
FinishBatch:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MULDESP)
                      Project(muld:RecordNumber)
                      Project(muld:AccountNumber)
                      Project(muld:Courier)
                      Project(muld:BatchNumber)
                      Project(muld:BatchTotal)
                      Project(muld:BatchType)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BrowseBatchesInProgress')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseBatchesInProgress:NoForm')
      loc:NoForm = p_web.GetValue('BrowseBatchesInProgress:NoForm')
      loc:FormName = p_web.GetValue('BrowseBatchesInProgress:FormName')
    else
      loc:FormName = 'BrowseBatchesInProgress_frm'
    End
    p_web.SSV('BrowseBatchesInProgress:NoForm',loc:NoForm)
    p_web.SSV('BrowseBatchesInProgress:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseBatchesInProgress:NoForm')
    loc:FormName = p_web.GSV('BrowseBatchesInProgress:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseBatchesInProgress') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseBatchesInProgress')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseBatchesInProgress' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseBatchesInProgress')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseBatchesInProgress') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseBatchesInProgress')
      p_web.DivHeader('popup_BrowseBatchesInProgress','nt-hidden')
      p_web.DivHeader('BrowseBatchesInProgress',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseBatchesInProgress_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseBatchesInProgress_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseBatchesInProgress',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MULDESP,muld:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MULD:ACCOUNTNUMBER') then p_web.SetValue('BrowseBatchesInProgress_sort','1')
    ElsIf (loc:vorder = 'LOCCOMPANYNAME') then p_web.SetValue('BrowseBatchesInProgress_sort','6')
    ElsIf (loc:vorder = 'MULD:COURIER') then p_web.SetValue('BrowseBatchesInProgress_sort','2')
    ElsIf (loc:vorder = 'MULD:BATCHNUMBER') then p_web.SetValue('BrowseBatchesInProgress_sort','3')
    ElsIf (loc:vorder = 'MULD:BATCHTOTAL') then p_web.SetValue('BrowseBatchesInProgress_sort','4')
    ElsIf (loc:vorder = 'MULD:BATCHTYPE') then p_web.SetValue('BrowseBatchesInProgress_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseBatchesInProgress:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseBatchesInProgress:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseBatchesInProgress:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseBatchesInProgress:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseBatchesInProgress'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseBatchesInProgress')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseBatchesInProgress_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseBatchesInProgress_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:AccountNumber)','-UPPER(muld:AccountNumber)')
    Loc:LocateField = 'muld:AccountNumber'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'locCompanyName','-locCompanyName')
    Loc:LocateField = 'locCompanyName'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:Courier)','-UPPER(muld:Courier)')
    Loc:LocateField = 'muld:Courier'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:BatchNumber)','-UPPER(muld:BatchNumber)')
    Loc:LocateField = 'muld:BatchNumber'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'muld:BatchTotal','-muld:BatchTotal')
    Loc:LocateField = 'muld:BatchTotal'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:BatchType)','-UPPER(muld:BatchType)')
    Loc:LocateField = 'muld:BatchType'
    Loc:LocatorCase = 0
  of 8
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 9
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 10
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(muld:HeadAccountNumber),+UPPER(muld:AccountNumber)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('muld:AccountNumber')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('locCompanyName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('muld:Courier')
    loc:SortHeader = p_web.Translate('Courier')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('muld:BatchNumber')
    loc:SortHeader = p_web.Translate('Batch Number')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('muld:BatchTotal')
    loc:SortHeader = p_web.Translate('Total In Batch')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s8')
  Of upper('muld:BatchType')
    loc:SortHeader = p_web.Translate('Batch Type')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s3')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseBatchesInProgress:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseBatchesInProgress:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseBatchesInProgress:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseBatchesInProgress:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MULDESP"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="muld:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseBatchesInProgress.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseBatchesInProgress',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseBatchesInProgress','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseBatchesInProgress_LocatorPic'),,,'onchange="BrowseBatchesInProgress.locate(''Locator2BrowseBatchesInProgress'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseBatchesInProgress',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseBatchesInProgress.locate(''Locator2BrowseBatchesInProgress'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseBatchesInProgress_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseBatchesInProgress.cl(''BrowseBatchesInProgress'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseBatchesInProgress_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseBatchesInProgress_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseBatchesInProgress_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseBatchesInProgress',p_web.Translate('Account Number'),'Click here to sort by Account Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseBatchesInProgress',p_web.Translate('Company Name'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseBatchesInProgress',p_web.Translate('Courier'),'Click here to sort by Courier',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseBatchesInProgress',p_web.Translate('Batch Number'),'Click here to sort by Batch Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseBatchesInProgress',p_web.Translate('Total In Batch'),'Click here to sort by Total In Batch',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (false) AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseBatchesInProgress',p_web.Translate('Batch Type'),'Click here to sort by Batch Type',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','BrowseBatchesInProgress',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','BrowseBatchesInProgress',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','BrowseBatchesInProgress',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('muld:recordnumber',lower(loc:vorder),1,1) = 0 !and MULDESP{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','muld:RecordNumber',clip(loc:vorder) & ',' & 'muld:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('muld:RecordNumber'),p_web.GetValue('muld:RecordNumber'),p_web.GetSessionValue('muld:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(muld:HeadAccountNumber) = Upper(''' & p_web.GSV('BookingAccount') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseBatchesInProgress',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseBatchesInProgress_Filter')
    p_web.SetSessionValue('BrowseBatchesInProgress_FirstValue','')
    p_web.SetSessionValue('BrowseBatchesInProgress_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MULDESP,muld:RecordNumberKey,loc:PageRows,'BrowseBatchesInProgress',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MULDESP{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MULDESP,loc:firstvalue)
              Reset(ThisView,MULDESP)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MULDESP{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MULDESP,loc:lastvalue)
            Reset(ThisView,MULDESP)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      locCompanyName = ''
      IF (muld:BatchType = 'TRA')
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = muld:AccountNumber
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
              locCompanyName = tra:Company_Name
          END
      ELSE
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = muld:AccountNumber
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              locCompanyName = sub:Company_Name
          END
      END
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(muld:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Batches To Despatch')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseBatchesInProgress_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseBatchesInProgress.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseBatchesInProgress.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseBatchesInProgress.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseBatchesInProgress.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseBatchesInProgress_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseBatchesInProgress_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseBatchesInProgress_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseBatchesInProgress',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseBatchesInProgress_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseBatchesInProgress_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseBatchesInProgress','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseBatchesInProgress_LocatorPic'),,,'onchange="BrowseBatchesInProgress.locate(''Locator1BrowseBatchesInProgress'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseBatchesInProgress',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseBatchesInProgress.locate(''Locator1BrowseBatchesInProgress'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseBatchesInProgress_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseBatchesInProgress.cl(''BrowseBatchesInProgress'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseBatchesInProgress_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseBatchesInProgress_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseBatchesInProgress_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseBatchesInProgress_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseBatchesInProgress.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseBatchesInProgress.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseBatchesInProgress.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseBatchesInProgress.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseBatchesInProgress_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseBatchesInProgress_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseBatchesInProgress_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseBatchesInProgress','MULDESP',muld:RecordNumberKey) !muld:RecordNumber
    p_web._thisrow = p_web._nocolon('muld:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and muld:RecordNumber = p_web.GetValue('muld:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseBatchesInProgress:LookupField')) = muld:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((muld:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseBatchesInProgress','MULDESP',muld:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MULDESP{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MULDESP)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MULDESP{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MULDESP)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::muld:AccountNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::locCompanyName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::muld:Courier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::muld:BatchNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::muld:BatchTotal
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (false) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::muld:BatchType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::ViewJobsInBatch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::FinishBatch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseBatchesInProgress','MULDESP',muld:RecordNumberKey)
  TableQueue.Id[1] = muld:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseBatchesInProgress;if (btiBrowseBatchesInProgress != 1){{var BrowseBatchesInProgress=new browseTable(''BrowseBatchesInProgress'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('muld:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseBatchesInProgress.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseBatchesInProgress.applyGreenBar();btiBrowseBatchesInProgress=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseBatchesInProgress')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseBatchesInProgress')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseBatchesInProgress')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseBatchesInProgress')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MULDESP)
  p_web._CloseFile(SUBTRACC)
  p_web._CloseFile(TRADEACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MULDESP)
  Bind(muld:Record)
  Clear(muld:Record)
  NetWebSetSessionPics(p_web,MULDESP)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)
  p_web._OpenFile(TRADEACC)
  Bind(tra:Record)
  NetWebSetSessionPics(p_web,TRADEACC)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('muld:RecordNumber',p_web.GetValue('muld:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  muld:RecordNumber = p_web.GSV('muld:RecordNumber')
  loc:result = p_web._GetFile(MULDESP,muld:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(muld:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MULDESP)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('ViewJobsInBatch')
    do Validate::ViewJobsInBatch
  of upper('FinishBatch')
    do Validate::FinishBatch
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MULDESP)
! ----------------------------------------------------------------------------------------
value::muld:AccountNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_muld:AccountNumber_'&muld:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(muld:AccountNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locCompanyName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_locCompanyName_'&muld:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locCompanyName,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:Courier   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_muld:Courier_'&muld:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(muld:Courier,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:BatchNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_muld:BatchNumber_'&muld:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(muld:BatchNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:BatchTotal   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_muld:BatchTotal_'&muld:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(muld:BatchTotal,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:BatchType   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (false)
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_muld:BatchType_'&muld:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(muld:BatchType,'@s3')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::ViewJobsInBatch  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  muld:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(MULDESP,muld:RecordNumberKey)
  p_web.FileToSessionQueue(MULDESP)
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::ViewJobsInBatch   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_ViewJobsInBatch_'&muld:RecordNumber,,net:crc,,loc:extra)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','ViewJobsInBatch','View Jobs In Batch',p_web.combine(Choose('View Jobs In Batch' <> '',p_web.site.style.BrowseOtherButtonWithText,p_web.site.style.BrowseOtherButtonWithOutText),'button-inbrowse'),loc:formname,,,p_web.WindowOpen(clip('BrowseJobsInBatch&locPassedBatchNumber = ' & muld:RecordNumber)&'&_bidv_=' & p_web.AddBrowseValue('BrowseBatchesInProgress','MULDESP',muld:RecordNumberKey) &''&'','_self'),,loc:disabled,,,,,,1,,,,'nt-left') !e
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallDeleteButton.TextValue = p_web.Translate('Delete Batch')
    p_web.site.SmallDeleteButton.Class = 'button-inbrowse DarkRedBold'
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_Delete_'&muld:RecordNumber,'CenterJustify',net:crc,,loc:extra)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseBatchesInProgress',p_web.AddBrowseValue('BrowseBatchesInProgress','MULDESP',muld:RecordNumberKey),,loc:FormPopup,'') & '<13,10>'
          End
    End
    p_web.site.SmallDeleteButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallDeleteButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::FinishBatch  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  muld:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(MULDESP,muld:RecordNumberKey)
  p_web.FileToSessionQueue(MULDESP)
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::FinishBatch   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseBatchesInProgress_FinishBatch_'&muld:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','FinishBatch','Finish Batch',p_web.combine(Choose('Finish Batch' <> '',p_web.site.style.BrowseOtherButtonWithText,p_web.site.style.BrowseOtherButtonWithOutText),'button-inbrowse GreenBold'),loc:formname,,,p_web.WindowOpen(clip('FinishBatch')&'&_bidv_=' & p_web.AddBrowseValue('BrowseBatchesInProgress','MULDESP',muld:RecordNumberKey) &''&'&PressedButton=1','_self'),,loc:disabled,,,,,,1,,,,'nt-left') !e
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('muld:RecordNumber',muld:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ViewJob              PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEngineeringOption STRING(30)                            !
locCurrentEngineer   STRING(60)                            !
locChargeableParts   BYTE                                  !
locWarrantyParts     BYTE                                  !
locCompleteRepair    STRING(10000)                         !
locCChargeTypeReason STRING(255)                           !
locWChargeTypeReason STRING(255)                           !
locCRepairTypeReason STRING(255)                           !
locWRepairTypeReason STRING(255)                           !
tmp:FoundAccessory   STRING(1000)                          !
locHOClaimStatus     STRING(30)                            !
locRRCClaimStatus    STRING(30)                            !
locJobType           STRING(30)                            !
locMobileLifetimeValue STRING(30)                          !
locMobileAverageSpend STRING(20)                           !
locMobileLoyaltyStatus STRING(30)                          !
locMobileUpgradeDate DATE                                  !
locMobileIDNumber    STRING(30)                            !
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
FilesOpened     Long
ESTPARTS::State  USHORT
PARTS::State  USHORT
WARPARTS::State  USHORT
UNITTYPE::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
TRADEACC::State  USHORT
JOBSE2::State  USHORT
JOBSE::State  USHORT
ACCSTAT::State  USHORT
USERS::State  USHORT
JOBNOTES::State  USHORT
NETWORKS::State  USHORT
SUBTRACC::State  USHORT
JOBSWARR::State  USHORT
JOBACC::State  USHORT
WAYBAWT::State  USHORT
link:AmendAddress:IsInvalid  Long
link:AuditTrail:IsInvalid  Long
link:EngineerHistory:IsInvalid  Long
link:StatusChanges:IsInvalid  Long
link:BrowseLocationHistory:IsInvalid  Long
text:jobNumber:IsInvalid  Long
text:DateBooked:IsInvalid  Long
locBookingOption:IsInvalid  Long
locEngineeringOption:IsInvalid  Long
gap:JobDetails:IsInvalid  Long
button:ChangeEngineeringOption:IsInvalid  Long
text:accountNumber:IsInvalid  Long
text:Completed:IsInvalid  Long
text:modelDetails:IsInvalid  Long
text:HubRepair:IsInvalid  Long
job:Order_Number:IsInvalid  Long
text:OBFValidated:IsInvalid  Long
job:ESN:IsInvalid  Long
job:MSN:IsInvalid  Long
locJobType:IsInvalid  Long
button:AmendJobType:IsInvalid  Long
job:Charge_Type:IsInvalid  Long
job:Repair_Type:IsInvalid  Long
job:Warranty_Charge_Type:IsInvalid  Long
job:Repair_Type_Warranty:IsInvalid  Long
textBouncer:IsInvalid  Long
buttonPreviousUnitHistory:IsInvalid  Long
job:Current_Status:IsInvalid  Long
job:Location:IsInvalid  Long
text:ExchangeUnit:IsInvalid  Long
text:LoanUnit:IsInvalid  Long
job:Exchange_Status:IsInvalid  Long
job:Loan_Status:IsInvalid  Long
text:SecondExchangeUnit:IsInvalid  Long
jbn:Fault_Description:IsInvalid  Long
jbn:Engineers_Notes:IsInvalid  Long
gap:IsInvalid  Long
button:EngineersNotes:IsInvalid  Long
locHOClaimStatus:IsInvalid  Long
locRRCClaimStatus:IsInvalid  Long
textLiquidDamage:IsInvalid  Long
locCurrentEngineer:IsInvalid  Long
locEngineerAllocated:IsInvalid  Long
jobe:Network:IsInvalid  Long
job:Authority_Number:IsInvalid  Long
job:Unit_Type:IsInvalid  Long
locRepairTypesNoParts:IsInvalid  Long
ViewEstimateParts:IsInvalid  Long
browse:ChargeableParts:IsInvalid  Long
browseWarrantyParts:IsInvalid  Long
locMobileLifetimeValue:IsInvalid  Long
locMobileAverageSpend:IsInvalid  Long
locMobileLoyaltyStatus:IsInvalid  Long
locMobileUpgradeDate:IsInvalid  Long
locMobileIDNumber:IsInvalid  Long
buttonAllocateEngineer:IsInvalid  Long
buttonAccessories:IsInvalid  Long
buttonFaultCodes:IsInvalid  Long
buttonEstimate:IsInvalid  Long
buttonContactHistory:IsInvalid  Long
buttonAllocateExchange:IsInvalid  Long
buttonAllocateLoan:IsInvalid  Long
buttonCreateInvoice:IsInvalid  Long
buttonViewCosts:IsInvalid  Long
buttonValidatePOP:IsInvalid  Long
buttonResendXML:IsInvalid  Long
buttonReceiptFromPUP:IsInvalid  Long
buttonCompleteRepair:IsInvalid  Long
locCompleteRepair:IsInvalid  Long
textCompleteRepair:IsInvalid  Long
locCChargeTypeReason:IsInvalid  Long
locCRepairTypeReason:IsInvalid  Long
locWChargeTypeReason:IsInvalid  Long
locWRepairTypeReason:IsInvalid  Long
textEstimateReady:IsInvalid  Long
buttonPrintEstimate:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
                    MAP
showHideCreateInvoice   PROCEDURE(),BYTE
ShowAlert     Procedure(String fAlert)
                    END

  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ViewJob')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ViewJob_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
    
  case p_stage
  of 0
    p_web.FormReady('ViewJob','Change')
    p_web.DivHeader('ViewJob',p_web.combine(p_web.site.style.formdiv,))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ViewJob') = 0
        p_web.AddPreCall('ViewJob')
        p_web.DivHeader('popup_ViewJob','nt-hidden')
        p_web.DivHeader('ViewJob',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ViewJob_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ViewJob_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ViewJob',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('wob:RecordNumber') = 0 then p_web.SetValue('wob:RecordNumber',p_web.GSV('wob:RecordNumber')).
    do PreCopy
    p_web.setsessionvalue('showtab_ViewJob',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewJob',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('wob:RecordNumber') = 0 then p_web.SetValue('wob:RecordNumber',p_web.GSV('wob:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ViewJob',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('wob:RecordNumber') = 0 then p_web.SetValue('wob:RecordNumber',p_web.GSV('wob:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewJob',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewJob',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewJob',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ViewJob')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
checkParts    routine
    p_web.SSV('CheckParts:Type','C')
    checkParts(p_web)
    p_web.SSV('GetStatus:StatusNumber',0)
    case p_web.GSV('CheckParts:ReturnValue')
        of 1 ! Pending Order
            p_web.SSV('GetStatus:StatusNumber',330)
        of 2 ! On Order
            p_web.SSV('GetStatus:StatusNumber',335)
        of 3 ! Back Order Status
            p_web.SSV('GetStatus:StatusNumber',340)
        of 4 ! Spares Received
            p_web.SSV('GetStatus:StatusNumber',345)
        else
            p_web.SSV('CheckParts:Type','W')
            checkParts(p_web)
            case p_web.GSV('CheckParts:ReturnValue')
                of 1 ! Pending Order
                    p_web.SSV('GetStatus:StatusNumber',330)
                of 2 ! On Order
                    p_web.SSV('GetStatus:StatusNumber',335)
                of 3 ! Back Order Status
                    p_web.SSV('GetStatus:StatusNumber',340)
                of 4 ! Spares Received
                    p_web.SSV('GetStatus:StatusNumber',345)
                else
                    if (sub(p_web.GSV('job:Current_Status'),1,3) = '330' Or |
                        sub(p_web.GSV('job:Current_Status'),1,3) = '340')

                        Access:AUDSTATS.Clearkey(aus:statusTimeKey)
                        aus:refNumber    = p_web.GSV('job:Ref_Number')
                        aus:type    = 'JOB'
                        aus:dateChanged    = today() + 1
                        set(aus:statusTimeKey,aus:statusTimeKey)
                        loop
                            if (Access:AUDSTATS.Previous())
                                Break
                            end ! if (Access:AUDSTATS.Next())
                            if (aus:refNumber    <> p_web.GSV('job:Ref_Number'))
                                Break
                            end ! if (aus:ref_Number    <> p_web.GSV('job:Ref_Number'))
                            if (aus:type    <> 'JOB')
                                Break
                            end ! if (aus:type    <> 'JOB')
                            if (aus:dateChanged    > today() + 1)
                                Break
                            end ! if (aus:dateChanged    <> today() + 1)

                            if (sub(aus:NewStatus,1,3) = '330')
                                p_web.SSV('GetStatus:StatusNumber',sub(aus:oldStatus,1,3))
                                break
                            end ! if (sub(aus:NewStatus,1,3) = '330')
                            if (sub(aus:NewStatus,1,3) = '340')
                                p_web.SSV('GetStatus:StatusNumber',sub(aus:oldStatus,1,3))
                                break
                            end ! if (sub(aus:NewStatus,1,3) = '330')
                        end ! loop
                    end ! if (sub(p_web.GSV('job:Current_Status',1,3) = '330' Or |
            end ! case p_web.GSV('CheckParts:ReturnValue')
    end ! case p_web.GSV('CheckParts:ReturnValue')
    if (p_web.GSV('GetStatus:StatusNumber') > 0)
        p_web.SSV('GetStatus:Type','JOB')
        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)
    end ! if (p_web.GSV('GetStatus:StatusNumber') > 0)
completeRepair      routine
    data
locErrorMessage     String(10000)
    code

    p_web.SSV('JobCompleteProcess',0)
    p_web.SSV('locCompleteRepair','')
    p_web.SSV('textCompleteRepair','')
    calculateBilling(p_web)
    if (p_web.GSV('job:Repair_Type') = '' and p_web.GSV('job:Repair_Type_Warranty') = '')
        locErrorMessage = 'The fault codes allocated have not returned a repair type.<13,10>'
    end ! if (p_web.GSV('job:Repair_Type') = '' and p_web.GSV('job:Repair_Type_Warranty') = '')

    if (p_web.GSV('job:Chargeble_Job') = 'YES')
        Access:PARTS.Clearkey(par:part_number_key)
        par:ref_Number    = p_web.GSV('job:Ref_Number')
        set(par:part_number_key,par:part_number_key)
        loop
            if (Access:PARTS.Next())
                Break
            end ! if (Access:PARTS.Next())
            if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (par:part_number_Key    <> job:Ref_Number)
            if (par:part_Number = 'EXCH')
                if (par:Status = 'REQ')
                    locErrorMessage = clip(locErrorMessage) & 'An exchange unit has been requested for this job. ' & |
                                    ' No exchange unit has been allocated.<13,10>'
                end ! if (par:Status = 'REQ')
            else ! if (par:part_Number = 'EXCH')
                Access:STOCK.Clearkey(sto:ref_Number_Key)
                sto:ref_Number    = par:part_Ref_Number
                if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Found
                    if (~par:PartAllocated and useStockAllocation(sto:Location))
                        locErrorMessage = clip(locErrorMessage) & |
                                'Part ' & Clip(par:Part_Number) & ', ' & |
                                 Clip(par:Description) & ' has not been allocated to this repair.<13,10>'
                    end ! if (par:PartAllocated)
                else ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
            end ! if (par:part_Number = 'EXCH')
        end ! loop
    end ! if (p_web.GSV('job:Chargeble_Job') = 'YES')

    if (p_web.GSV('job:Warranty_Job') = 'YES')
        Access:WARPARTS.Clearkey(wpr:part_number_key)
        wpr:Ref_Number    = p_web.GSV('job:Ref_Number')
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if (Access:WARPARTS.Next())
                Break
            end ! if (Access:PARTS.Next())
            if (wpr:ref_Number    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (par:part_number_Key    <> job:Ref_Number)
            if (wpr:part_Number = 'EXCH')
                if (wpr:Status = 'REQ')
                    locErrorMessage = clip(locErrorMessage) & 'An exchange unit has been requested for this job. ' & |
                                    ' No exchange unit has been allocated.<13,10>'
                end ! if (par:Status = 'REQ')
            else ! if (par:part_Number = 'EXCH')
                Access:STOCK.Clearkey(sto:ref_Number_Key)
                sto:ref_Number    = wpr:part_Ref_Number
                if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Found
                    if (~wpr:PartAllocated and useStockAllocation(sto:Location))
                        locErrorMessage = clip(locErrorMessage) & |
                                'Part ' & Clip(wpr:Part_Number) & ', ' & |
                                        Clip(wpr:Description) & ' has not been allocated to this repair.<13,10>'
                    end ! if (par:PartAllocated)
                else ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
            end ! if (par:part_Number = 'EXCH')
        end ! loop
    end ! if (p_web.GSV('job:Chargeble_Job') = 'YES')

    p_web.SSV('locCompleteRepair',locErrorMessage)

        p_web.SSV('CompulsoryFieldCheck:Type','C')

        compulsoryFieldCheck(p_web)

    if (p_web.GSV('locCompleteRepair') <> '')
        p_web.SSV('locCompleteRepair','The repair cannot be completed because of the following reasons:<13,10,13,10>' |
                & p_web.GSV('locCompleteRepair'))
        ShowALert('This repair cannot be completed.')
        exit
    end ! if (p_web.GSV('locCompleteRepair') <> '')

    p_web.SSV('JobPricingRoutine:Force',0)
    JobPricingRoutine(p_web)

    p_web.SSV('textCompleteRepair','The repair will be completed when you click "Save". ' & |
                'You can then QA the unit. You will not be able to un-complete the repair.')
    ShowAlert('This repair will be completed when you click Save.')

    p_web.SSV('job:On_Test','YES')
    p_web.SSV('job:Date_On_Test',Today())
    p_web.SSV('job:Time_On_Test',Clock())


    getStatus(605,0,'JOB',p_web)



deleteVariables     routine
    ! Remove Locks
    IF (p_web.GSV('Job:ViewOnly') <> 1)
        lockRecord(p_web.gsv('job:Ref_Number'),p_web.sessionid,1) ! Remove Lock
    END

    p_web.deleteSessionValue('locEngineeringOption')
    p_web.deleteSessionValue('locCurrentEngieer')
    p_web.deleteSessionValue('locChargeableParts')
    p_web.deleteSessionValue('locWarrantyParts')
    p_web.deleteSessionValue('locCompleRepair')
    p_web.deleteSessionValue('locCChargeTypeReason')
    p_web.deleteSessionValue('locWChargeTypeReason')
    p_web.deleteSessionValue('locCRepairTypeReason')
    p_web.deleteSessionValue('locWRepairTypeReason')
    p_web.deleteSessionValue('locEstimateReadyOption')
    p_web.deleteSessionValue('SecondTime')
    p_web.deleteSessionValue('Hide:ButtonViewCosts')
    p_web.deleteSessionValue('Hide:ButtonFaultCodes')
    p_web.deleteSessionValue('Hide:ButtonAccessories')
    p_web.deleteSessionValue('Hide:ButtonAllocateEngineer')
    p_web.DeleteSessionValue('Hide:ButtonCreateInvoice')

didAnythingChange   routine
    data
locNotes        String(255)
    code

    !Have Change Reasons Been Entered.

        If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
            if (p_web.GSV('locCChargeTypeReason') <> '' and p_web.GSV('Hide:CChargeTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: CHARGEABLE CHARGE TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:CChargeType') & '<13,10>REASON: ' & p_web.GSV('locCChargeTypeReason'))
                addToAudit(p_web)
            end !if (p_web.SSV('locCChargeTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
        If (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('save:WChargeType') and p_web.GSV('save:WChargeType') <> '')
            if (p_web.GSV('locWChargeTypeReason') <> '' and p_web.GSV('Hide:WChargeTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: WARRANTY CHARGE TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:WChargeType') & '<13,10>REASON: ' & p_web.GSV('locWChargeTypeReason'))
                addToAudit(p_web)
            end ! if (p_web.SSV('locWChargeTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
        If (p_web.GSV('job:Repair_Type') <> p_web.GSV('save:CRepairType') and p_web.GSV('save:CRepairType') <> '')
            if (p_web.GSV('locCRepairTypeReason') <> '' and p_web.GSV('Hide:CRepairTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: CHARGEABLE REPAIR TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:CRepairType') & '<13,10>REASON: ' & p_web.GSV('locCRepairTypeReason'))
                addToAudit(p_web)
            end ! if (p_web.SSV('locWRepairTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
        If (p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('save:WRepairType') and p_web.GSV('save:WRepairType') <> '')
            if (p_web.GSV('locWRepairTypeReason') <> '' and p_web.GSV('Hide:WRepairTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: WARRANTY REPAIR TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:WRepairType') & '<13,10>REASON: ' & p_web.GSV('locWChargeTypeReason'))
                addToAudit(p_web)
            end ! if (p_web.SSV('locWRepairTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')

        p_web.SSV('AddToAudit:Type','JOB')

    !----
        locNotes = ''
        if (p_web.GSV('save:FaultDescription') <> p_web.GSV('jbn:Fault_Description'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>' & CLip(jbn:Fault_Description)
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED: FAULT DESCRIPION')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)
        end ! if (p_web.GSV('save:FaultDescription') <> p_web.GSV('jbn:Fault_Description'))

        p_web.SSV('AddToAudit:Action',p_web.GSV('JOB UPDATED'))

        locNotes = ''
        if (p_web.GSV('save:ChargeableJob') <> p_web.GSV('job:Chargeable_Job'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>CHARGEABLE JOB: ' & p_web.GSV('job:Chargeable_Job')
        end ! if (p_web.GSV('save:ChargeableJob') <> p_web.GSV('job:Chargeable_Job'))

        if (p_web.GSV('save:WarrantyJob') <> p_web.GSV('job:Warranty_Job'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>WARRANTY JOB: ' & p_web.GSV('job:Warranty_Job')
        end ! if (p_web.GSV('save:WarrantyJob') <> p_web.GSV('job:Warranty_Job'))

        if (p_web.GSV('save:HubRepair') <> p_web.GSV('jobe:HubRepair'))
            if (p_web.GSV('jobe:HubRepair') = 1)
                locNotes = clip(locNotes) & |
                    '<13,10,13,10>HUB REPAIR SELECTED'
            else ! if (p_web.GSV('jobe:HubRepair') = 1)
                locNotes = clip(locNotes) & |
                    '<13,10,13,10>HUB REPAIR SELECTED'
            end ! if (p_web.GSV('jobe:HubRepair') = 1)
        end ! if (p_web.GSV('save:HubRepair') <> p_web.GSV('jobe:HubRepair'))

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)

        end ! if (clip(locNotes) <> '')

    !--

        locNotes = ''
        if (p_web.GSV('save:TransitType') <> p_web.GSV('job:Transit_Type'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>TRANS TYPE: ' & p_web.GSV('job:Transit_Type')
        end ! if (p_web.GSV('save:TransitType') <> p_web.GSV('job:Transit_Type'))

        if (p_web.GSV('save:MSN') <> p_web.GSV('job:MSN'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>MSN: ' & p_web.GSV('job:MSN')
        end ! if (p_web.GSV('save:MSN') <> p_web.GSV('job:MSN'))

        if (p_web.GSV('save:ModelNumber') <> p_web.GSV('job:Model_Number'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>MODEL NO: ' & p_web.GSV('job:Model_Number')
        end ! if (p_web.GSV('save:ModelNumber') <> p_web.GSV('job:Model_Number'))

        if (p_web.GSV('save:DOP') <> p_web.GSV('job:DOP'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>DOP: ' & CLip(Format(p_web.GSV('job:DOP'),@d6))
        end ! if (p_web.GSV('save:DOP') <> p_web.GSV('job:DOP'))

        if (p_web.GSV('save:OrderNumber') <> p_web.GSV('job:Order_Number'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>ORDER NO: ' & p_web.GSV('job:Order_Number')
        end ! if (p_web.GSV('save:OrderNumber') <> p_web.GSV('job:Order_Number'))

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED: FAULT DESCRIPTION')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)

        end ! if (clip(locNotes) <> '')

    !----
        locNotes = ''
        loop x# = 1 to 20
            if (x# < 13)
                if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('job:Fault_Code' & x#))
                    locNotes = clip(locNotes) & |
                        '<13,10,13,10>FAULT CODE' & x# & ': ' & p_web.GSV('job:Fault_Code' & x#)
                end !if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('job:Fault_Code' & x#))
            else  ! if (x# < 13)
                if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('wob:FaultCode' & x#))
                    locNotes = clip(locNotes) & |
                        '<13,10,13,10>FAULT CODE' & x# & ': ' & p_web.GSV('wob:FaultCode' & x#)
                end !if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('job:Fault_Code' & x#))
            end ! if (x# < 13)
        end ! loop x# = 1 to 20
        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)

        end ! if (clip(locNotes) <> '')

    !----
        if (p_web.GSV('save:AccountNumber') <> p_web.GSV('job:account_number'))
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','ACCOUNT NUMBER CHANGED')
            p_web.SSV('AddToAudit:Notes','PREVIOUS ACCOUNT: ' & p_web.GSV('save:AccountNumber') & |
                '<13,10>NEW ACCOUNT: ' & p_web.GSV('job:Account_Number'))
            addToAudit(p_web)
        end !if (p_web.GSV('save:AccountNumber') <> p_web.GSV('job:account_number'))

        if (p_web.GSV('save:CompanyName') <> p_web.GSV('job:Company_Name') Or |
            p_web.GSV('save:AddressLine1') <> p_web.GSV('job:Address_Line1') Or |
            p_web.GSV('save:AddressLine2') <> p_web.GSV('job:Address_Line2') Or |
            p_web.GSV('save:AddressLine3') <> p_web.GSV('job:Address_Line3') Or |
            p_web.GSV('save:Postcode') <> p_web.GSV('job:Postcode') Or |
            p_web.GSV('save:TelephoneNumber') <> p_web.GSV('job:Telephone_Number') Or |
            p_web.GSV('save:FaxNumber') <> p_web.GSV('job:Fax_Number'))

            p_web.SSV('AddToAudit:Notes','PREVIOUS: <13,10>' & p_web.GSV('save:CompanyName') & |
                '<13,10>' & p_web.GSV('save:AddressLine1') & |
                '<13,10>' & p_web.GSV('save:AddressLine2') & |
                '<13,10>' & p_web.GSV('save:AddressLine3') & |
                '<13,10>' & p_web.GSV('save:Postcode') & |
                '<13,10>TEL: ' & p_web.GSV('save:TelephoneNumber') & |
                '<13,10>FAX: ' & p_web.GSV('save:FaxNumber'))
            p_web.SSV('AddToAudit:Action','ADDRESS DETAILS CHANGED - CUSTOMER')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)
        end ! if (p_web.GSV('save:CompanyName') <> p_web.GSV('job:Company_Name') Or |

        if (p_web.GSV('save:CompanyNameDelivery') <> p_web.GSV('job:Company_Name_Delivery') Or |
            p_web.GSV('save:AddressLine1Delivery') <> p_web.GSV('job:Address_Line1_Delivery') Or |
            p_web.GSV('save:AddressLine2Delivery') <> p_web.GSV('job:Address_Line2_Delivery') Or |
            p_web.GSV('save:AddressLine3Delivery') <> p_web.GSV('job:Address_Line3_Delivery') Or |
            p_web.GSV('save:PostcodeDelivery') <> p_web.GSV('job:Postcode_Delivery') Or |
            p_web.GSV('save:TelephoneDelivery') <> p_web.GSV('job:Telephone_Delivery'))

            p_web.SSV('AddToAudit:Notes','PREVIOUS: <13,10>' & p_web.GSV('save:CompanyNameDelivery') & |
                '<13,10>' & p_web.GSV('save:AddressLine1Delivery') & |
                '<13,10>' & p_web.GSV('save:AddressLine2Delivery') & |
                '<13,10>' & p_web.GSV('save:AddressLine3Delivery') & |
                '<13,10>' & p_web.GSV('save:PostcodeDelivery') & |
                '<13,10>TEL: ' & p_web.GSV('save:TelephoneDelivery'))
            p_web.SSV('AddToAudit:Action','ADDRESS DETAILS CHANGED - DESPATCH')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)
        end ! if (p_web.GSV('save:CompanyName') <> p_web.GSV('job:Company_Name') Or |


haveAccessoriesChanged      routine
    data
locAccessoryQueue      Queue(),Pre(locque)
sessionID                  Long()
Accessory                  String(30)
                        End
locAuditNotes         String(255)
    code
        loop x# = 1 to records(locAccessoryQueue)
            get(locAccessoryQUeue,x#)
            if (locque:sessionID = p_web.sessionID)
                delete(locAccessoryQueue)
            end !if (locque:sessionID) = p_web.sessionID)
        end ! loop x# = 1 to records(locAccessoryQueue)

        if Clip(p_web.GSV('tmp:TheJobAccessory')) <> ''
            Loop x# = 1 To 1000
                If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                    Start# = x# + 2
                    Cycle
                End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'

                If Start# > 0
                    If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                        tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)

                        If tmp:FoundAccessory <> ''
                            locque:sessionID = p_web.SessionID
                            locque:Accessory = clip(tmp:foundACcessory)
                            add(locAccessoryQUeue)
                        End ! If tmp:FoundAccessory <> ''
                        Start# = 0
                    End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                End ! If Start# > 0
            End ! Loop x# = 1 To 1000

            If Start# > 0
                tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
                If tmp:FoundAccessory <> ''
                    If tmp:FoundAccessory <> ''
                        locque:sessionID = p_web.SessionID
                        locque:Accessory = clip(tmp:foundAccessory)
                        add(locAccessoryQUeue)
                    End ! If tmp:FoundAccessory <> ''
                End ! If tmp:FoundAccessory <> ''
            End ! If Start# > 0
        end ! If Clip(p_web.GSV('tmp:TheJobAccessory') <> ''

        locAuditNotes = ''
        loop x# = 1 to records(locAccessoryQueue)
            get(locAccessoryQueue,x#)
            if (locque:sessionID <> p_web.sessionID)
                cycle
            end ! if (locque:sessionID <> p_web.sessionID)
            Access:JOBACC.Clearkey(jac:ref_Number_Key)
            jac:ref_Number    = p_web.GSV('job:Ref_Number')
            jac:accessory    = locque:accessory
            if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                ! Found
            else ! if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                ! Error
                ! Accessory has been added
                locAuditNotes = clip(locAuditNotes) & '<13,10>' & clip(locque:accessory) & ' -ADDED'
                If Access:JOBACC.PrimeRecord() = Level:Benign
                    jac:Ref_Number = p_web.GSV('job:Ref_Number')
                    jac:Accessory = locque:accessory
                    jac:Damaged = 0

                    If p_web.GSV('BookingSite') = 'RRC'
                        jac:Attached = False
                    Else !If jobe:WebJob
                        jac:Attached = True
                    End !If jobe:WebJob
                    If Access:JOBACC.TryInsert() = Level:Benign
                    Else !If Access:JOBACC.TryInsert() = Level:Benign
                        Access:JOBACC.CancelAutoInc()
                    End !If Access:JOBACC.TryInsert() = Level:Benign

                End ! If Access:JOBACC.PrimeRcord() = Level:Benign

            end ! if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
        end ! loop x# = 1 to records(locAccessoryQueue)


        Access:JOBACC.Clearkey(jac:ref_Number_Key)
        jac:ref_Number    = p_web.GSV('job:Ref_Number')
        set(jac:ref_Number_Key,jac:ref_Number_Key)
        loop
            if (Access:JOBACC.Next())
                Break
            end ! if (Access:JOBACC.Next())
            if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
            clear(locAccessoryQueue)
            locque:sessionID = p_web.SessionID
            locque:accessory = jac:accessory
            get(locAccessoryQueue,locque:sessionID, locque:accessory)
            if (error())
                ! Accessory has been removed
                locAuditNotes = clip(locAuditNotes) & '<13,10>' & clip(locque:accessory) & ' -DELETED'
                access:JOBACC.deleterecord(0)
            end ! if (error())
        end ! loop

        if (locAuditNotes <> '')
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','JOB ACCESSORIES AMENDED')
            p_web.SSV('AddToAudit:Notes',sub(clip(locAuditNotes),3,255))
            addToAudit(p_web)
        end ! if (locAuditNotes <> '')
ReceiptFromPUP      Routine
Data
local:Site              String(3)
local:CurrentLocation   String(30)
local:CurrentStatus     String(3)
local:DefaultStatus     String(3)
local:AuditNotes        String(255)
Code

!    If glo:WebJob
!        ! Inserting (DBH 13/11/2006) # 8485 - Check that the logged in user matches the RRC of the job
!        Access:USERS.ClearKey(use:Password_Key)
!        use:Password = glo:Password
!        If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
!            !Found
!            If use:Location <> tmp:BookingSiteLocation
!                Case Missive('You cannot receive a job from a different RRC.','ServiceBase 3g',|
!                               'mstop.jpg','/OK')
!                    Of 1 ! OK Button
!                End ! Case Missive
!                Exit
!            End ! If use:Location <> tmp:BookingSiteLocation
!        Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
!            !Error
!        End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
!        ! End (DBH 13/11/2006) #8485
!        local:Site = 'RRC'
!        local:CurrentLocation = GETINI('RRC','RRCLocation',,Clip(Path()) & '\SB2KDEF.INI')
!    Else ! If glo:WebJob
!        local:Site = 'ARC'
!        local:CurrentLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
!    End ! If glo:WebJob
!    local:DefaultStatus = GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI')
!
!    If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
!! Changing (DBH 07/08/2006) # 8021 - Not at right location. Does the user have access to press this button?
!!        Case Missive('Warning! The current location of this job is NOT "In Transit From PUP".'&|
!!          'Are you sure you still want to receive this job from the PUP?','ServiceBase 3g',|
!!                       'mexclam.jpg','\Cancel|/Receive')
!!            Of 2 ! Receive Button
!!            Of 1 ! Cancel Button
!!                Exit
!!        End ! Case Missive
!! to (DBH 07/08/2006) # 8021
!        If SecurityCheck('FORCE RECEIPT FROM PUP')
!            Case Missive('Cannot receive this job!'&|
!              '|The current location is NOT "In Transit From PUP".','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
!            Exit
!        Else ! If SecurityCheck('FORCE RECEIPT FROM PUP')
!            Case Missive('Warning! The current location of this job is NOT "In Transit From PUP".'&|
!              'Are you sure you still want to receive this job from the PUP?','ServiceBase 3g',|
!                           'mexclam.jpg','\Cancel|/Receive')
!                Of 2 ! Receive Button
!                Of 1 ! Cancel Button
!                    Exit
!            End ! Case Missive
!        End ! If SecurityCheck('FORCE RECEIPT FROM PUP')
!! End (DBH 07/08/2006) #8021
!    Else ! If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
!        Case Missive('Are you sure you want to receive the PUP job into the ' & Clip(local:Site) & '?','ServiceBase 3g',|
!                       'mquest.jpg','\No|/Yes')
!            Of 2 ! Yes Button
!            Of 1 ! No Button
!                Exit
!        End ! Case Missive
!    End ! If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
!
!    do PUPValidation
!
!    local:AuditNotes = ''
!
!    Rtn# = AccessoryCheck('JOB')
!
!    If Rtn# = 1 Or Rtn# = 2
!        local:AuditNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'
!
!        Save_jac_ID = Access:JOBACC.SaveFile()
!        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
!        jac:Ref_Number = job:Ref_Number
!        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
!        Loop ! Begin JOBACC Loop
!            If Access:JOBACC.Next()
!                Break
!            End ! If !Access
!            If jac:Ref_Number <> job:Ref_Number
!                Break
!            End ! If
!            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>' & Clip(jac:Accessory)
!        End ! End JOBACC Loop
!        Access:JOBACC.RestoreFile(Save_jac_ID)
!        local:AuditNotes = Clip(local:AuditNotes) & '<13,10,13,10>ACCESSORIES RECEIVED:-'
!        Loop x# = 1 To Records(glo:Queue)
!            Get(glo:Queue,x#)
!            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>' & Clip(glo:Pointer)
!        End ! Loop x# = 1 To Records(glo:Queue)
!    End ! If Rtn# = 1 Or Rtn# = 2
!
!    If AddToAudit(job:Ref_Number,'JOB','UNIT RECEIVED AT ' & CLip(local:Site) & ' FROM PUP',Clip(local:AuditNotes))
!        LocationChange(GETINI('RRC',local:Site & 'Location',,Clip(Path()) & '\SB2KDEF.INI'))
!        GetStatus(local:DefaultStatus,0,'JOB')
!
!        Case Missive('This job will now be saved.','ServiceBase 3g',|
!                       'midea.jpg','/OK')
!            Of 1 ! OK Button
!        End ! Case Missive
!        ! Changing (DBH 03/07/2006) # 7149 - Do not do the OK validation
!        ! Post(Event:Accepted,?OK)
!        ! ! to (DBH 03/07/2006) # 7149
!        job:Workshop = 'YES'
!        Access:JOBS.Update()
!        do SetJobse
!        Access:JOBSE.Update()
!        ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
!        UpdateDateTimeStamp(job:Ref_Number)
!        ! End (DBH 16/09/2008) #10253
!        Post(Event:Accepted,?Close)
!        ! End (DBH 03/07/2006) #7149
!    End ! If AddToAuditEntry(job:Ref_Number,'JOB','UNIT RECEIVED AT ' & CLip(local:Site) & ' FROM PUP','')
!
!
!
ResendXML       Routine
Data
local:Seq        Long()
local:ToSendFolder  CString(255)
local:SentFolder    CString(255)
local:ReceivedFolder CString(255)
local:ProcessedFolder CString(255)
local:XMLFileName   CString(255)
Code
    Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
    trd:Company_Name = p_web.GSV('job:Third_Party_Site')
    If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Found
        If trd:LHubAccount = 0
            Exit
        End ! If trd:LHubAccount = 0
    Else ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Error
    End ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign


    local:ToSendFolder = GETINI('NMSFTP','XMLFolder',,Clip(Path()) & '\SB2KDEF.INI')
    If Sub(Clip(local:ToSendFolder),-1,1) <> '\'
        local:ToSendFolder = Clip(local:ToSendFolder) & '\'
    End ! If Sub(Clip(local:ToSendFolder),-1,1) <> '\'

    local:SentFolder = Clip(local:ToSendFolder) & 'Sent'

    local:ReceivedFolder = Clip(local:ToSendFolder) & 'Received'

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Processed'

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Bad'

    local:ToSendFolder = Clip(local:ToSendFolder) & 'ToSend'

    local:XMLFileName =  'RR_' & trb:Batch_Number & Format(Today(),@d12) & Clock() & '.xml'


    If objXmlExport.FOpen(local:ToSendFolder & '\' & local:XMLFileName,1) = Level:Benign
        objXmlExport.OpenTag('ShipmentOrderRequest','Version="4.0"')
        objXmlExport.OpenTag('Authentication')
            objXmlExport.WriteTag('SiteId',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('UserId',GETINI('NMSFTP','UserID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('ToolType','CT')
            objXmlExport.WriteTag('AccessKey',GETINI('NMSFTP','AccessKey',,Clip(Path()) & '\SB2KDEF.INI'))
        objXmlExport.CloseTag('Authentication')
        objXmlExport.WriteTag('ClientReferenceNumber',trb:Batch_Number & '-' & Today() & Clock())
        objXmlExport.WriteTag('ShipmentType','001')
        objXmlExport.WriteTag('SourceSiteId',trd:ASCID)
        objXmlExport.OpenTag('Booking')
            objXmlExport.WriteTag('SequenceNo',1)
            objXmlExport.WriteTag('BookingType','001')
            objXmlExport.WriteTag('ItemType','PHONE')
            objXmlExport.WriteTag('ClientBookingNumber',p_web.GSV('job:Ref_Number'))
            objXmlExport.WriteTag('FaultSymptomCode',Clip(p_web.GSV('job:Fault_Code1')))
            objXmlExport.WriteTag('ExistenceOfSymptom',Clip(p_web.GSV('wob:FaultCode15')))
            objXmlExport.WriteTag('OrderType','REPAIR')
            objXmlExport.WriteTag('ProductCode',Clip(p_web.GSV('job:ProductCode')))
            objXmlExport.WriteTag('OriginalSerialNumber',Clip(p_web.GSV('job:ESN')))
            objXmlExport.WriteTag('ClientBookingTimestamp',Format(Today(),@d05b) & ' ' & Format(Clock(),@t04b))
            If p_web.GSV('job:Warranty_Job') = 'YES'
                objXmlExport.WriteTag('WarrantyOption','001')
            Else ! If p_web.GSV('job:Warranty_Job = 'YES'
                objXmlExport.WriteTag('WarrantyOption','002')
            End ! If p_web.GSV('job:Warranty_Job = 'YES'
            objXmlExport.WriteTag('WarrantyDatabaseDate',Format(p_web.GSV('job:DOP'),@d05b) & ' ' & Format(Clock(),@t04b))
            objXmlExport.WriteTag('OwnershipCode','001')
            Found# = 0
            If p_web.GSV('job:Warranty_Job') = 'YES'
                If Instring('SOFTWARE',Upper(p_web.GSV('job:Repair_Type_Warranty')),1,1)
                    objXmlExport.WriteTag('TransactionType','002')
                    Found# = 1
                End ! If Instring('SOFTWARE',Upper(p_web.GSV('job:Warranty_Repair_Type),1,1)
            Else ! If p_web.GSV('job:Warranty_Job = 'YES'
                If Instring('SOFTWARE',Upper(p_web.GSV('job:Repair_Type')),1,1)
                    objXmlExport.WriteTag('TransactionType','002')
                    Found# = 1
                End ! If Instring('SOFTWARE',Upper(p_web.GSV('job:Repair_Type),1,1)
            End ! If p_web.GSV('job:Warranty_Job = 'YES'
            If Found# = 0
                objXmlExport.WriteTag('TransactionType','001')
            End ! If Found# = 0
        objXmlExport.CloseTag('Booking')
        objXmlExport.CloseTag('ShipmentOrderRequest')
        objXMLExport.FClose()
    End ! If objXmlExport.FOpen(Filename.xml,1) = Level:Benign

saveFields      routine
    p_web.SSV('save:CChargeType',p_web.GSV('job:Charge_Type'))
    p_web.SSV('save:CRepairType',p_web.GSV('job:Repair_Type'))
    p_web.SSV('save:WChargeType',p_web.GSV('job:Warranty_Charge_Type'))
    p_web.SSV('save:WRepairType',p_web.GSV('job:Repair_Type_Warranty'))
    p_web.SSV('save:ChargeableJob',p_web.GSV('job:Chargeable_Job'))
    p_web.SSV('save:WarrantyJob',p_web.GSV('job:Warranty_Job'))
    p_web.SSV('save:HubRepair',p_web.GSV('jobe:HubRepair'))
    p_web.SSV('save:TransitType',p_web.GSV('job:Transit_Type'))

    p_web.SSV('save:MSN',p_web.GSV('job:MSN'))
    p_web.SSV('save:ModelNumber',p_web.GSV('job:Model_Number'))
    p_web.SSV('save:DOP',p_web.GSV('job:DOP'))
    p_web.SSV('save:OrderNumber',p_web.GSV('job:Order_Number'))
    loop x#= 1 to 20
        if (x# < 13)
            p_web.SSV('save:FaultCode' & x#,p_web.GSV('job:Fault_Code' & x#))
        else
            p_web.SSV('save:FaultCode' & x#,p_web.GSV('wob:FaultCode' & x#))
        end ! if (x# < 13)
    end ! loop x#= 1 to 20

    p_web.SSV('save:FaultDescription',p_web.GSV('jbn:Fault_Description'))

    p_web.SSV('save:AccountNumber',p_web.GSV('job:Account_Number'))
    p_web.SSV('save:CompanyName',p_web.GSV('job:Company_Name'))
    p_web.SSV('save:AddressLine1',p_web.GSV('job:Address_Line1'))
    p_web.SSV('save:AddressLine2',p_web.GSV('job:Address_Line2'))
    p_web.SSV('save:AddressLine3',p_web.GSV('job:Address_Line3'))
    p_web.SSV('save:Postcode',p_web.GSV('job:Postcode'))
    p_web.SSV('save:TelephoneNumber',p_web.GSV('job:Telephone_Number'))
    p_web.SSV('save:FaxNumber',p_web.GSV('job:Fax_Number'))
    p_web.SSV('save:AddressLine1Delivery',p_web.GSV('job:Address_Line1_Delivery'))
    p_web.SSV('save:CompanyNameDelivery',p_web.GSV('job:Company_Name_Delivery'))
    p_web.SSV('save:AddressLine2Delivery',p_web.GSV('job:Address_Line2_Delivery'))
    p_web.SSV('save:AddressLine3Delivery',p_web.GSV('job:Address_Line3_Delivery'))
    p_web.SSV('save:PostcodeDelivery',p_web.GSV('job:Postcode_Delivery'))
    p_web.SSV('save:TelephoneDelivery',p_web.GSV('job:Telephone_Delivery'))


SetStatusAccessLevelFilter          routine
    p_web.SSV('filter:AccessLevel','***')
    if (p_web.GSV('job:Engineer') <> '')
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code    = p_web.GSV('job:Engineer')
        if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            ! Found
            p_web.SSV('filter:AccessLevel',use:User_Level)
        else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    else ! if (p_web.GSV('job:Engineer') <> '')
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = p_web.GSV('BookingUserPassword')
        if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
            ! Found
            p_web.SSV('filter:AccessLevel',use:User_Level)
        else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
    end ! if (p_web.GSV('job:Engineer') <> '')
waybillCheck        routine
    if (p_web.GSV('jobe:HubRepair') = 1)
        if (p_web.GSV('job:Incoming_Consignment_Number') <> '')
            Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
            wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
            wya:jobNumber    = p_web.GSV('job:ref_Number')
            if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
                ! Found
                relate:WAYBAWT.delete(0)
            end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)

            exit
        end ! if (p_web.GSV('job:Incoming_Consignment_Number') <> '')

        if (p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation'))
            Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
            wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
            wya:jobNumber    = p_web.GSV('job:ref_Number')
            if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
                ! Found
                relate:WAYBAWT.delete(0)
            end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)

            exit
        end ! if (p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation'))

        Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
        wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
        wya:jobNumber    = p_web.GSV('job:ref_Number')
        if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
            ! Found
        else ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
            ! Error
            if (Access:WAYBAWT.PrimeRecord() = Level:Benign)
                wya:jobNumber        = p_web.GSV('job:Ref_Number')
                wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
                wya:manufacturer    = p_web.GSV('job:manufacturer')
                wya:modelNumber        = p_web.GSV('job:model_Number')
                wya:IMEINumber        = p_web.GSV('job:ESN')
                if (Access:WAYBAWT.TryInsert() = Level:Benign)
                    ! Inserted
                else ! if (Access:WAYBAWT.TryInsert() = Level:Benign)
                    ! Error
                    Access:WAYBAWT.CancelAutoInc()
                end ! if (Access:WAYBAWT.TryInsert() = Level:Benign)
            end ! if (Access:WAYBAWT.PrimeRecord() = Level:Benign)
        end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
    else !if (p_web.GSV('jobe:HubRepair') = 1)
        Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
        wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
        wya:jobNumber    = p_web.GSV('job:ref_Number')
        if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
            ! Found
            relate:WAYBAWT.delete(0)
        end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
    end ! if (p_web.GSV('jobe:HubRepair') = 1)

OpenFiles  ROUTINE
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(UNITTYPE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(ACCSTAT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSWARR)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(WAYBAWT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(UNITTYPE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(ACCSTAT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSWARR)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WAYBAWT)
     FilesOpened = False
  END
      !Clear variables for update jobs
  
      ClearUpdateJobVariables(p_web)
  

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of upper('link:AmendAddress')
    do Validate::link:AmendAddress
  of upper('link:AuditTrail')
    do Validate::link:AuditTrail
  of upper('link:EngineerHistory')
    do Validate::link:EngineerHistory
  of upper('link:StatusChanges')
    do Validate::link:StatusChanges
  of upper('link:BrowseLocationHistory')
    do Validate::link:BrowseLocationHistory
  of upper('button:ChangeEngineeringOption')
    do Validate::button:ChangeEngineeringOption
  of upper('buttonPreviousUnitHistory')
    do Validate::buttonPreviousUnitHistory
  of upper('button:EngineersNotes')
    do Validate::button:EngineersNotes
  of upper('buttonAllocateEngineer')
    do Validate::buttonAllocateEngineer
  of upper('buttonAccessories')
    do Validate::buttonAccessories
  of upper('buttonEstimate')
    do Validate::buttonEstimate
  of upper('buttonContactHistory')
    do Validate::buttonContactHistory
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseEstimateParts')
      do Value::ViewEstimateParts
    of upper('BrowseChargeableParts')
      do Value::browse:ChargeableParts
    of upper('BrowseWarrantyParts')
      do Value::browseWarrantyParts
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  IF (p_web.IfExistsValue('IgnoreMessage'))
      p_web.StoreValue('IgnoreMessage')
  END
  
  p_web.SetValue('ViewJob_form:inited_',1)
  p_web.formsettings.file = 'WEBJOB'
  p_web.formsettings.key = 'wob:RecordNumberKey'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'WEBJOB'
    p_web.formsettings.key = 'wob:RecordNumberKey'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = wob:RecordNumber
    p_web.formsettings.FieldName[1] = 'wob:RecordNumber'
    do SetAction
    if p_web.GetSessionValue('ViewJob:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ViewJob'
    end
    p_web.formsettings.proc = 'ViewJob'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do waybillCheck
  do deleteVariables
  IF p_web.GetSessionValue('ViewJob:Primed') = 1
    p_web._deleteFile(WEBJOB)
    p_web.SetSessionValue('ViewJob:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','WEBJOB')
  p_web.SetValue('UpdateKey','wob:RecordNumberKey')
  If p_web.IfExistsValue('job:Order_Number')
    p_web.SetPicture('job:Order_Number','@s30')
  End
  p_web.SetSessionPicture('job:Order_Number','@s30')
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type_Warranty')
    p_web.SetPicture('job:Repair_Type_Warranty','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type_Warranty','@s30')
  If p_web.IfExistsValue('job:Current_Status')
    p_web.SetPicture('job:Current_Status','@s30')
  End
  p_web.SetSessionPicture('job:Current_Status','@s30')
  If p_web.IfExistsValue('job:Location')
    p_web.SetPicture('job:Location','@s30')
  End
  p_web.SetSessionPicture('job:Location','@s30')
  If p_web.IfExistsValue('job:Exchange_Status')
    p_web.SetPicture('job:Exchange_Status','@s30')
  End
  p_web.SetSessionPicture('job:Exchange_Status','@s30')
  If p_web.IfExistsValue('job:Loan_Status')
    p_web.SetPicture('job:Loan_Status','@s30')
  End
  p_web.SetSessionPicture('job:Loan_Status','@s30')
  If p_web.IfExistsValue('jobe:Network')
    p_web.SetPicture('jobe:Network','@s30')
  End
  p_web.SetSessionPicture('jobe:Network','@s30')
  If p_web.IfExistsValue('job:Authority_Number')
    p_web.SetPicture('job:Authority_Number','@s30')
  End
  p_web.SetSessionPicture('job:Authority_Number','@s30')
  If p_web.IfExistsValue('job:Unit_Type')
    p_web.SetPicture('job:Unit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Unit_Type','@s30')

AfterLookup Routine
  !!After Lookup
  !if (p_web.getvalue('lookupfield') = 'job:Current_Status')
  !    if (loc:LookupDone)
  !        p_web.FileToSessionQueue(ACCSTAT)
  !        p_web.SSV('job:Current_Status',acs:Status)
  !    end ! if loc:LookupDone
  !end ! if (p_web.getvalue('lookupfield') = 'job:Current_Status')
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  If p_web.GSV('textBouncer') <> ''
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Current_Status'
    p_web.setsessionvalue('showtab_ViewJob',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(ACCSTAT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Location')
  End
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Network'
    p_web.setsessionvalue('showtab_ViewJob',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Authority_Number')
  Of 'job:Unit_Type'
    p_web.setsessionvalue('showtab_ViewJob',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(UNITTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  If p_web.GSV('Hide:CustomerClassification') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  If p_web.GSV('job:Date_Completed') = 0
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:EstimateReady') = 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locEngineeringOption') = 0
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  Else
    locEngineeringOption = p_web.GetSessionValue('locEngineeringOption')
  End
  if p_web.IfExistsValue('job:Order_Number') = 0
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  Else
    job:Order_Number = p_web.GetSessionValue('job:Order_Number')
  End
  if p_web.IfExistsValue('job:ESN') = 0
    p_web.SetSessionValue('job:ESN',job:ESN)
  Else
    job:ESN = p_web.GetSessionValue('job:ESN')
  End
  if p_web.IfExistsValue('job:MSN') = 0
    p_web.SetSessionValue('job:MSN',job:MSN)
  Else
    job:MSN = p_web.GetSessionValue('job:MSN')
  End
  if p_web.IfExistsValue('locJobType') = 0
    p_web.SetSessionValue('locJobType',locJobType)
  Else
    locJobType = p_web.GetSessionValue('locJobType')
  End
  if p_web.IfExistsValue('job:Charge_Type') = 0
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type') = 0
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  Else
    job:Repair_Type = p_web.GetSessionValue('job:Repair_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type') = 0
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type_Warranty') = 0
    p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty)
  Else
    job:Repair_Type_Warranty = p_web.GetSessionValue('job:Repair_Type_Warranty')
  End
  if p_web.IfExistsValue('job:Current_Status') = 0
    p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  Else
    job:Current_Status = p_web.GetSessionValue('job:Current_Status')
  End
  if p_web.IfExistsValue('job:Location') = 0
    p_web.SetSessionValue('job:Location',job:Location)
  Else
    job:Location = p_web.GetSessionValue('job:Location')
  End
  if p_web.IfExistsValue('job:Exchange_Status') = 0
    p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  Else
    job:Exchange_Status = p_web.GetSessionValue('job:Exchange_Status')
  End
  if p_web.IfExistsValue('job:Loan_Status') = 0
    p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  Else
    job:Loan_Status = p_web.GetSessionValue('job:Loan_Status')
  End
  if p_web.IfExistsValue('jbn:Fault_Description') = 0
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  Else
    jbn:Fault_Description = p_web.GetSessionValue('jbn:Fault_Description')
  End
  if p_web.IfExistsValue('jbn:Engineers_Notes') = 0
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  Else
    jbn:Engineers_Notes = p_web.GetSessionValue('jbn:Engineers_Notes')
  End
  if p_web.IfExistsValue('locHOClaimStatus') = 0
    p_web.SetSessionValue('locHOClaimStatus',locHOClaimStatus)
  Else
    locHOClaimStatus = p_web.GetSessionValue('locHOClaimStatus')
  End
  if p_web.IfExistsValue('locRRCClaimStatus') = 0
    p_web.SetSessionValue('locRRCClaimStatus',locRRCClaimStatus)
  Else
    locRRCClaimStatus = p_web.GetSessionValue('locRRCClaimStatus')
  End
  if p_web.IfExistsValue('locCurrentEngineer') = 0
    p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer)
  Else
    locCurrentEngineer = p_web.GetSessionValue('locCurrentEngineer')
  End
  if p_web.IfExistsValue('jobe:Network') = 0
    p_web.SetSessionValue('jobe:Network',jobe:Network)
  Else
    jobe:Network = p_web.GetSessionValue('jobe:Network')
  End
  if p_web.IfExistsValue('job:Authority_Number') = 0
    p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
  Else
    job:Authority_Number = p_web.GetSessionValue('job:Authority_Number')
  End
  if p_web.IfExistsValue('job:Unit_Type') = 0
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  Else
    job:Unit_Type = p_web.GetSessionValue('job:Unit_Type')
  End
  if p_web.IfExistsValue('locMobileLifetimeValue') = 0
    p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue)
  Else
    locMobileLifetimeValue = p_web.GetSessionValue('locMobileLifetimeValue')
  End
  if p_web.IfExistsValue('locMobileAverageSpend') = 0
    p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend)
  Else
    locMobileAverageSpend = p_web.GetSessionValue('locMobileAverageSpend')
  End
  if p_web.IfExistsValue('locMobileLoyaltyStatus') = 0
    p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
  Else
    locMobileLoyaltyStatus = p_web.GetSessionValue('locMobileLoyaltyStatus')
  End
  if p_web.IfExistsValue('locMobileUpgradeDate') = 0
    p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate)
  Else
    locMobileUpgradeDate = p_web.GetSessionValue('locMobileUpgradeDate')
  End
  if p_web.IfExistsValue('locMobileIDNumber') = 0
    p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber)
  Else
    locMobileIDNumber = p_web.GetSessionValue('locMobileIDNumber')
  End
  if p_web.IfExistsValue('locCompleteRepair') = 0
    p_web.SetSessionValue('locCompleteRepair',locCompleteRepair)
  Else
    locCompleteRepair = p_web.GetSessionValue('locCompleteRepair')
  End
  if p_web.IfExistsValue('locCChargeTypeReason') = 0
    p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason)
  Else
    locCChargeTypeReason = p_web.GetSessionValue('locCChargeTypeReason')
  End
  if p_web.IfExistsValue('locCRepairTypeReason') = 0
    p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason)
  Else
    locCRepairTypeReason = p_web.GetSessionValue('locCRepairTypeReason')
  End
  if p_web.IfExistsValue('locWChargeTypeReason') = 0
    p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason)
  Else
    locWChargeTypeReason = p_web.GetSessionValue('locWChargeTypeReason')
  End
  if p_web.IfExistsValue('locWRepairTypeReason') = 0
    p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason)
  Else
    locWRepairTypeReason = p_web.GetSessionValue('locWRepairTypeReason')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  Else
    locEngineeringOption = p_web.GetSessionValue('locEngineeringOption')
  End
  if p_web.IfExistsValue('job:Order_Number')
    job:Order_Number = p_web.GetValue('job:Order_Number')
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  Else
    job:Order_Number = p_web.GetSessionValue('job:Order_Number')
  End
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  Else
    job:ESN = p_web.GetSessionValue('job:ESN')
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  Else
    job:MSN = p_web.GetSessionValue('job:MSN')
  End
  if p_web.IfExistsValue('locJobType')
    locJobType = p_web.GetValue('locJobType')
    p_web.SetSessionValue('locJobType',locJobType)
  Else
    locJobType = p_web.GetSessionValue('locJobType')
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  Else
    job:Repair_Type = p_web.GetSessionValue('job:Repair_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type_Warranty')
    job:Repair_Type_Warranty = p_web.GetValue('job:Repair_Type_Warranty')
    p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty)
  Else
    job:Repair_Type_Warranty = p_web.GetSessionValue('job:Repair_Type_Warranty')
  End
  if p_web.IfExistsValue('job:Current_Status')
    job:Current_Status = p_web.GetValue('job:Current_Status')
    p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  Else
    job:Current_Status = p_web.GetSessionValue('job:Current_Status')
  End
  if p_web.IfExistsValue('job:Location')
    job:Location = p_web.GetValue('job:Location')
    p_web.SetSessionValue('job:Location',job:Location)
  Else
    job:Location = p_web.GetSessionValue('job:Location')
  End
  if p_web.IfExistsValue('job:Exchange_Status')
    job:Exchange_Status = p_web.GetValue('job:Exchange_Status')
    p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  Else
    job:Exchange_Status = p_web.GetSessionValue('job:Exchange_Status')
  End
  if p_web.IfExistsValue('job:Loan_Status')
    job:Loan_Status = p_web.GetValue('job:Loan_Status')
    p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  Else
    job:Loan_Status = p_web.GetSessionValue('job:Loan_Status')
  End
  if p_web.IfExistsValue('jbn:Fault_Description')
    jbn:Fault_Description = p_web.GetValue('jbn:Fault_Description')
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  Else
    jbn:Fault_Description = p_web.GetSessionValue('jbn:Fault_Description')
  End
  if p_web.IfExistsValue('jbn:Engineers_Notes')
    jbn:Engineers_Notes = p_web.GetValue('jbn:Engineers_Notes')
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  Else
    jbn:Engineers_Notes = p_web.GetSessionValue('jbn:Engineers_Notes')
  End
  if p_web.IfExistsValue('locHOClaimStatus')
    locHOClaimStatus = p_web.GetValue('locHOClaimStatus')
    p_web.SetSessionValue('locHOClaimStatus',locHOClaimStatus)
  Else
    locHOClaimStatus = p_web.GetSessionValue('locHOClaimStatus')
  End
  if p_web.IfExistsValue('locRRCClaimStatus')
    locRRCClaimStatus = p_web.GetValue('locRRCClaimStatus')
    p_web.SetSessionValue('locRRCClaimStatus',locRRCClaimStatus)
  Else
    locRRCClaimStatus = p_web.GetSessionValue('locRRCClaimStatus')
  End
  if p_web.IfExistsValue('locCurrentEngineer')
    locCurrentEngineer = p_web.GetValue('locCurrentEngineer')
    p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer)
  Else
    locCurrentEngineer = p_web.GetSessionValue('locCurrentEngineer')
  End
  if p_web.IfExistsValue('jobe:Network')
    jobe:Network = p_web.GetValue('jobe:Network')
    p_web.SetSessionValue('jobe:Network',jobe:Network)
  Else
    jobe:Network = p_web.GetSessionValue('jobe:Network')
  End
  if p_web.IfExistsValue('job:Authority_Number')
    job:Authority_Number = p_web.GetValue('job:Authority_Number')
    p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
  Else
    job:Authority_Number = p_web.GetSessionValue('job:Authority_Number')
  End
  if p_web.IfExistsValue('job:Unit_Type')
    job:Unit_Type = p_web.GetValue('job:Unit_Type')
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  Else
    job:Unit_Type = p_web.GetSessionValue('job:Unit_Type')
  End
  if p_web.IfExistsValue('locMobileLifetimeValue')
    locMobileLifetimeValue = p_web.GetValue('locMobileLifetimeValue')
    p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue)
  Else
    locMobileLifetimeValue = p_web.GetSessionValue('locMobileLifetimeValue')
  End
  if p_web.IfExistsValue('locMobileAverageSpend')
    locMobileAverageSpend = p_web.GetValue('locMobileAverageSpend')
    p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend)
  Else
    locMobileAverageSpend = p_web.GetSessionValue('locMobileAverageSpend')
  End
  if p_web.IfExistsValue('locMobileLoyaltyStatus')
    locMobileLoyaltyStatus = p_web.GetValue('locMobileLoyaltyStatus')
    p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
  Else
    locMobileLoyaltyStatus = p_web.GetSessionValue('locMobileLoyaltyStatus')
  End
  if p_web.IfExistsValue('locMobileUpgradeDate')
    locMobileUpgradeDate = p_web.GetValue('locMobileUpgradeDate')
    p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate)
  Else
    locMobileUpgradeDate = p_web.GetSessionValue('locMobileUpgradeDate')
  End
  if p_web.IfExistsValue('locMobileIDNumber')
    locMobileIDNumber = p_web.GetValue('locMobileIDNumber')
    p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber)
  Else
    locMobileIDNumber = p_web.GetSessionValue('locMobileIDNumber')
  End
  if p_web.IfExistsValue('locCompleteRepair')
    locCompleteRepair = p_web.GetValue('locCompleteRepair')
    p_web.SetSessionValue('locCompleteRepair',locCompleteRepair)
  Else
    locCompleteRepair = p_web.GetSessionValue('locCompleteRepair')
  End
  if p_web.IfExistsValue('locCChargeTypeReason')
    locCChargeTypeReason = p_web.GetValue('locCChargeTypeReason')
    p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason)
  Else
    locCChargeTypeReason = p_web.GetSessionValue('locCChargeTypeReason')
  End
  if p_web.IfExistsValue('locCRepairTypeReason')
    locCRepairTypeReason = p_web.GetValue('locCRepairTypeReason')
    p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason)
  Else
    locCRepairTypeReason = p_web.GetSessionValue('locCRepairTypeReason')
  End
  if p_web.IfExistsValue('locWChargeTypeReason')
    locWChargeTypeReason = p_web.GetValue('locWChargeTypeReason')
    p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason)
  Else
    locWChargeTypeReason = p_web.GetSessionValue('locWChargeTypeReason')
  End
  if p_web.IfExistsValue('locWRepairTypeReason')
    locWRepairTypeReason = p_web.GetValue('locWRepairTypeReason')
    p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason)
  Else
    locWRepairTypeReason = p_web.GetSessionValue('locWRepairTypeReason')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ViewJob_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End
  ! Force Form Into "Change Only"??
  loc:Action = p_web.site.ChangePromptText
  loc:Act = Net:ChangeRecord

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'JobSearch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'JobSearch'

GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('ViewOnly'))
      p_web.StoreValue('ViewOnly')
  END
  
  
      if (p_web.GSV('SecondTime') = 0)
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              if (jobInUse(p_web.sessionid) = 1)
                  ShowALert('This job is use by another station. Click ''OK'' to view the job')
                  p_web.SSV('Job:ViewOnly',1)
  
              end !if (jobInUse() = 1)
              p_web.FileToSessionQueue(JOBS)
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
          ! If passed view only flag, make job view only
          IF (p_web.GSV('ViewOnly') = 1)
              p_web.SSV('Job:ViewOnly',1)
          END
  
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Found
              p_web.FileToSessionQueue(JOBSE)
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
  
          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
          jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              ! Found
              p_web.FileToSessionQueue(JOBSE2)
          else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              ! Error
              if (Access:JOBSE2.PrimeRecord() = Level:Benign)
                  jobe2:refNumber    = p_web.GSV('wob:RefNumber')
                  if (Access:JOBSE2.TryInsert() = Level:Benign)
                      ! Inserted
                      p_web.FileToSessionQueue(JOBSE2)
                  else ! if (Access:JOBSE2.TryInsert() = Level:Benign)
                      ! Error
                      Access:JOBSE2.CancelAutoInc()
                  end ! if (Access:JOBSE2.TryInsert() = Level:Benign)
              end ! if (Access:JOBSE2.PrimeRecord() = Level:Benign)
          end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
  
  
          Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
          jbn:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
              ! Found
              p_web.FileToSessionQueue(JOBNOTES)
  
          else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
              ! Error
              if (Access:JOBNOTES.PrimeRecord() = Level:Benign)
                  jbn:refNumber    = p_web.GSV('wob:RefNumber')
                  if (Access:JOBNOTES.TryInsert() = Level:Benign)
                      ! Inserted
                      p_web.FileToSessionQueue(JOBNOTES)
                  else ! if (Access:JOBNOTES.TryInsert() = Level:Benign)
                      ! Error
                  end ! if (Access:JOBNOTES.TryInsert() = Level:Benign)
              end ! if (Access:JOBNOTES.PrimeRecord() = Level:Benign)
          end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
  
          p_web.SSV('tmp:TheJobAccessory','')
          Access:JOBACC.Clearkey(jac:ref_Number_Key)
          jac:Ref_Number    = p_web.GSV('job:Ref_Number')
          set(jac:ref_Number_Key,jac:ref_Number_Key)
          loop
              if (Access:JOBACC.Next())
                  Break
              end ! if (Access:JOBACC.Next())
              if (jac:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  Break
              end ! if (jac:Ref_Number    <> p_web.GSV('job:Ref_Number'))
              p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & ';|;' & jac:Accessory)
          end ! loop
          !p_web.SSV('tmp:TheJobAccessory',clip(sub(p_web.GSV('tmp:TheJobAccessory'),3,1000)) & ':|;')
          p_web.SSV('tmp:TheJobAccessory',clip(sub(p_web.GSV('tmp:TheJobAccessory'),2,1000)) & ';|;')
  
  
          case p_web.GSV('jobe:Booking48HourOption')
          of 1
              p_web.SSV('locBookingOption','48 Hour Exchange')
          of 2
              p_web.SSV('locBookingOption','ARC Repair')
          of 3
              p_web.SSV('locBookingOption','7 Day TAT')
          OF 4
              p_web.SSV('locBookingOption','Liquid Damage')
          Else
              p_web.SSV('locBookingOption','Not Set')
          end ! case (jobe:Booking48HourOption)
  
          case p_web.GSV('jobe:Engineer48HourOption')
          of 1
              p_web.SSV('locEngineeringOption','48 Hour Exchange' )
          of 2
              p_web.SSV('locEngineeringOption','ARC Repair')
          of 3
              p_web.SSV('locEngineeringOption','7 Day TAT')
          of 4
              p_web.SSV('locEngineeringOption','Standard Repair')
          Else
              p_web.SSV('locEngineeringOption','Not Set')
          end ! case (jobe:Booking48HourOption)
  
          p_web.SSV('SecondTime',1)
  
          if (p_web.GSV('Job:ViewOnly') <> 1)
              if (p_web.GSV('BookingSite') = 'RRC' And |
                  ((p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation') And |
                  p_web.GSV('job:Location') <> p_web.GSV('Default:DespatchToCustomer') And |
                  p_web.GSV('job:Location') <> p_web.GSV('Default:InTransitPUP') And |
                  p_web.GSV('job:Location') <> p_web.GSV('Default:PUPLocation')) Or |
                  (p_web.GSV('jobe:HubRepair') = 1) Or |
                  (p_web.GSV('wob:HeadAccountNumber') <> p_web.GSV('BookingAccount'))))
  
                  ShowAlert('This job is not in this RRC''s control and cannot be amended.')
  
                  p_web.SSV('Job:ViewOnly',1)
              end ! if
          end ! if (p_web.GSV('Job:ViewOnly') <> 1)
  
          SentToHub(p_web)
  
          IF (p_web.GSV('job:warranty_job') = 'YES' AND p_web.GSV('wob:EDI') <> 'XXX')
              IF (p_web.GSV('job:date_Completed') > 0 AND p_web.GSV('SentToHub') = 0)
                  IF (p_web.GSV('wob:EDI') = 'NO' OR p_web.GSV('wob:EDI') = 'YES' OR p_web.GSV('wob:EDI') = 'PAY' OR p_web.GSV('wob:EDI') = 'APP')
                      p_web.SSV('Job:ViewOnly',1)
                  ELSE
                       ! Rejected warranty is handeled outsite this screen
                  END
              END
          END
          IF (p_web.GSV('Job:ViewOnly') <> 1)
              IF (p_web.GSV('BookingSite') = 'RRC' AND p_web.GSV('SentToHub') = 1)
                  if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'JOBS - RRC AMEND ARC JOB'))
                      p_web.SSV('Hide:ButtonAllocateEngineer',1)
                      p_web.SSV('Hide:ButtonCreateInvoice',1)
                      p_web.SSV('Hide:ValidatePOP',1)
                      p_web.SSV('Job:ViewOnly',1)
                  end
              END
          END
  
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              ! Show/Hide Adjustment Buttons (DBH: 10/06/2009)
          if (man:ForceParts)
              p_web.SSV('Hide:ChargeableAdjustment',0)
              p_web.SSV('Hide:WarrantyAdjustment',0)
          else ! if (man:ForceParts)
              p_web.SSV('Hide:ChargeableAdjustment',1)
              p_web.SSV('Hide:WarrantyAdjustment',1)
          end ! if (man:ForceParts)
      else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
      end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
  
          ! If job invoiced, then don't allow adjustment
      if (jobInvoiced(p_web.GSV('wob:RefNumber'),p_web.GSV('BookingSite')))
          p_web.SSV('hide:ChargeableAdjustment',1)
      end ! if (jobInvoice(p_web.GSV('wob:RefNumber'),p_web.GSV('BookingSite'))
  
      if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
          p_web.SSV('hide:WarrantyAdjustment',1)
      end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
  
      do saveFields
  
  
      p_web.SSV('Hide:CChargeType',1)
      p_web.SSV('Hide:WChargeType',1)
      p_web.SSV('Hide:CRepairType',1)
      p_web.SSV('Hide:WRepairType',1)
      p_web.SSV('locCChargeTypeReason','')
      p_web.SSV('locWChargeTypeReason','')
      p_web.SSV('locCRepairTypeReason','')
      p_web.SSV('locWChargeTypeReason','')
  
      p_web.SSV('Hide:EstimateQuery',1)
      p_web.SSV('Hide:EstimateReady',1)
  
      end ! if (p_web.GSV('FirstTime') = 1)
  
      p_web.SSV('textBouncer','')
  
      countHistory(p_web)
  
      if (p_web.GSV('CountHistory') > 0)
          p_web.SSV('textBouncer','There are ' & p_web.GSV('CountHistory') & ' Previous Jobs')
      end ! if (x# > 0)
  
      ! Set URL
      IF (p_web.GSV('Job:ViewOnly') = 1)
          p_web.SSV('URL:ViewCosts','ViewCosts')
      ELSE
          p_web.ssv('URL:ViewCosts','BillingConfirmation')
      END
  
  
      IF (p_web.GSV('Job:ViewOnly') <> 1)
          lockRecord(p_web.GSV('job:Ref_Number'),p_web.sessionid,0) ! Job Not Read Only. Add A Lock
      END
      ! Return to the browse, unless called from somewhere else
      p_web.SSV('locViewJobURL','BrowseWebmasterJobs')
      if (p_web.ifexistsvalue('BackURL'))
          p_web.SSV('locViewJobURL',p_web.getvalue('BackURL'))
      end ! if (p_web.ifexistsvalue('locViewJobNextURL'))
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number    = p_web.GSV('wob:HeadAccountNumber')
      if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          ! Found
          p_web.SSV('locJobNumber',p_web.GSV('wob:RefNumber') & '-' & clip(tra:BranchIdentification) & p_web.GSV('wob:JobNumber'))
  
          p_web.SSV('JobBookingSiteLocation',tra:SiteLocation)
      else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
  
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number    = p_web.GSV('job:Account_Number')
      if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          ! Found
          p_web.SSV('locTradeAccount',clip(sub:Account_Number) & ' (' & clip(sub:Company_Name) & ')')
      else ! if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
  
      if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          if (p_web.GSV('jobe:ExchangedAtRRC') = 1)
              p_web.SSV('locExchangeText','RRC Exchange Unit Attached')
          else ! if (p_web.GSV('jobe:ExchangedAtRRC') = 1)
              p_web.SSV('locExchangeText','ARC Exchange Unit Attached')
          end ! if (p_web.GSV('jobe:ExchangedAtRRC') = 1)
      else ! if (job:Exchange_Unit_Number > 0)
          p_web.SSV('locExchangeText','Not Issued')
      end ! if (job:Exchange_Unit_Number > 0)
  
      if (p_web.GSV('job:Loan_Unit_Number') > 0)
          p_web.SSV('locLoanText','Loan Unit Attached')
      else ! if (p_web.GSV('job:Loan_Unit_Number') > 0)
          p_web.SSV('locLoanText','Not Issued')
      end ! if (p_web.GSV('job:Loan_Unit_Number') > 0)
  
  
!      Access:USERS.Clearkey(use:User_Code_Key)
!      use:User_Code    = p_web.GSV('job:Engineer')
!      if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
!          ! Found
!          p_web.SSV('locCurrentEngineer',clip(use:Forename) & ' ' & clip(use:Surname))
!  
!  
!          Access:JOBSENG.Clearkey(joe:UserCodeKey)
!          joe:JobNumber    = p_web.GSV('wob:RefNumber')
!          joe:UserCode    = use:User_Code
!          joe:DateAllocated    = Today()
!          set(joe:UserCodeKey,joe:UserCodeKey)
!          loop
!              if (Access:JOBSENG.Previous())
!                  Break
!              end ! if (Access:JOBSENG.Next())
!              if (joe:JobNumber    <> p_web.GSV('wob:RefNumber'))
!                  Break
!              end ! if (joe:JobNumber    <> p_web.GSV('wob:RefNumber'))
!              if (joe:UserCode    <> use:User_Code)
!                  Break
!              end ! if (joe:UserCode    <> use:User_Code)
!              p_web.SSV('locEngineerAllocated',Format(joe:DateAllocated,@d06b) & '  (Eng Level: ' & joe:EngSkillLevel & ')')
!              Break
!          end ! loop
!  
!      else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
!          ! Error
!          p_web.SSV('locCurrentEngineer','Not Allocated')
!      end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)

    ! Show the Engineer Name
    DisplayJobEngineerDetails(p_web,p_web.GSV('job:Engineer'),p_web.GSV('wob:RefNumber'))
  
      p_web.SSV('FormEngineeringOption:FirstTime',1)
  
      p_web.SSV('hide:EngOptionButton',0)
      ! Do not allow an ARC engineer to change an RRC's options (DBH: 24-03-2005)
      If p_web.GSV('jobe:WebJob') = 1 And p_web.GSV('BookingSite') <> 'RRC'
          p_web.SSV('hide:EngOptionButton',1)
      End ! jobe:WebJob And ~job:WebJob
  
      ! If completed, do not allow to change engineer option (DBH: 24-03-2005)
      If p_web.GSV('job:Date_Completed') > 0
          p_web.SSV('hide:EngOptionButton',1)
      End ! job:Date_Completed <> ''
  
      IF (p_web.GSV('job:Warranty_Job') = 'YES')
          IF (p_web.GSV('job:Chargeable_Job') = 'YES')
              p_web.SSV('locJobType','Warranty / Chargeable')
          ELSE
              p_web.SSV('locJobType','Warranty')
          END
      ELSE
          p_web.SSV('locJobType','Chargeable')
      END
  
  
      do SetStatusAccessLevelFilter
  !Security Checks
      p_web.SSV('Hide:ExchangeButton',0)
      ! No access to add exchange. Hide button if none attached
      if (p_web.GSV('job:Exchange_Unit_Number') = 0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - ADD EXCHANGE UNIT'))
              p_web.SSV('Hide:ExchangeButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),' '))
      end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
  
      p_web.SSV('Hide:LoanButton',0)
      if (p_web.GSV('job:Loan_Unit_Number') = 0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - ADD LOAN UNIT'))
              p_web.SSV('Hide:LoanButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - ADD LOAN UNIT'))
      end ! if (p_web.GSV('job:Loan_Unit_Number') = 0)
  
      p_web.SSV('Hide:ValidatePOP',0)
      if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND POP DETAILS'))
          p_web.SSV('Hide:ValidatePOP',1)
      end ! if (securityCheckFailed('Hide:ValidatePOP'),'JOBS - AMEND POP DETAILS')
  
      p_web.SSV('Hide:ResendXML',1)
      if (p_web.GSV('job:Workshop') <> 'YES' and p_web.GSV('job:Third_Party_Site') <> '')
          Access:TRDPARTY.Clearkey(trd:company_Name_Key)
          trd:company_Name    = job:Third_Party_Site
          if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
              ! Found
              if (trd:LHubAccount)
  
  
                  Access:TRDBATCH.ClearKey(trb:JobStatusKey)
                  trb:Status = 'OUT'
                  trb:Ref_Number = p_web.GSV('job:Ref_Number ')
                  If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
                      !Found
                      p_web.SSV('Hide:ResendXML',0)
                  Else ! If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
                      !Error
                  End ! If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
              end ! if (trd:LHubAccount)
          else ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
              ! Error
          end ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
  
      end ! if (p_web.GSV('job:Workshop') <> 'YES' and p_web.GSV('job:Third_Party_Site') <> '')
  
      p_web.SSV('Hide:ReceiptFromPUP',1)
  
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'JOBS - SHOW COSTS'))
          p_web.SSV('Hide:ButtonViewCosts',1)
      end
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - AMEND FAULT CODES'))
          p_web.SSV('Hide:ButtonFaultCodes',1)
      end
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - ACCESSORIES'))
          p_web.SSV('Hide:ButtonAccessories',1)
      end
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - ALLOCATE JOB'))
          p_web.SSV('Hide:ButtonAllocateEngineer',1)
      end
      IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - CREATE INVOICE'))
          p_web.SSV('Hide:ButtonCreateInvoice',1)
      ELSE
          IF showHideCreateInvoice()
              p_web.SSV('Hide:ButtonCreateInvoice',1)
          ELSE
              p_web.SSV('Hide:ButtonCreateInvoice',0)
          END
  
      END
  
  
      ! Claim Status
      lochoclaimstatus = p_web.GSV('jobe:warrantyclaimstatus')
      locRRCclaimStatus = WOBEDIStatus(p_web.GSV('wob:EDI'))
  
      if (p_web.GSV('wob:EDI') = 'PAY')
          access:audit.clearkey(aud:action_key)
          aud:ref_number = p_web.GSV('job:ref_number')
          aud:action = 'WARRANTY CLAIM MARKED PAID (RRC)'
          set(aud:action_key,aud:action_key)
          loop
              if (access:audit.next())
                  break
              end
              if (aud:ref_number <> p_web.GSV('job:ref_Number'))
                  break
              end
              if (aud:action <> 'WARRANTY CLAIM MARKED PAID (RRC)')
                  break
              end
              lochoclaimstatus = clip(lochoclaimstatus) & ' (' & Format(aud:Date,@d6) & ')'
              break
          end
  
      end
      p_web.SSV('locHOClaimStatus',locHOClaimStatus)
      p_web.SSV('locRRCClaimStatus',locRRCClaimStatus)
  
  
  !Completion Check
  if (p_web.GSV('JobCompleteProcess') = 1)
      do completeRepair
  else ! if (p_web.GSV('Job:CompleteProcess') = 1)
      p_web.SSV('textCompleteRepair','')
  end ! if (p_web.GSV('Job:CompleteProcess') = 1)
  ! Customer Collection
  IF (CustCollectionValidated(p_web.GSV('job:Ref_Number')))
      p_web.SSV('Hide:CustomerClassification',0)
      CustClassificationFields(p_web.GSV('job:Ref_Number'),locMobileLifetimeValue, |
          locMobileAverageSpend, |
          locMobileLoyaltyStatus, |
          locMobileUpgradeDate, |
          locMobileIDNumber)
      p_web.SSV('locMobileLifetimeValue',locMobileLifetimeValue)
      p_web.SSV('locMobileAverageSpend',locMobileAverageSpend)
      p_web.SSV('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
      p_web.SSV('locMobileUpgradeDate',FORMAT(locMobileUpgradeDate,@d06b))
      p_web.SSV('locMobileIDNumber',locMobileIDNumber)
  
  ELSE
      p_web.SSV('Hide:CustomerClassification',1)
  END
  ! Liquid Damage #10544
      IF (IsUnitLiquidDamaged(p_web.GSV('job:ESN'),p_web.GSV('job:Ref_Number')) = TRUE)
          p_web.SSV('Hide:LiquidDamage',0)
      ELSE
          p_web.SSV('Hide:LiquidDamage',1)
      END
  p_web.SSV('RepairTypesNoParts',vod.RepairTypesNoParts(p_web.GSV('job:Chargeable_Job'), |
      p_web.GSV('job:Warranty_job'), |
      p_web.GSV('job:Manufacturer'), |
      p_web.GSV('job:Repair_Type'), |
      p_web.GSV('job:Repair_Type_Warranty')))
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locJobType = p_web.RestoreValue('locJobType')
 locHOClaimStatus = p_web.RestoreValue('locHOClaimStatus')
 locRRCClaimStatus = p_web.RestoreValue('locRRCClaimStatus')
 locCurrentEngineer = p_web.RestoreValue('locCurrentEngineer')
 locMobileLifetimeValue = p_web.RestoreValue('locMobileLifetimeValue')
 locMobileAverageSpend = p_web.RestoreValue('locMobileAverageSpend')
 locMobileLoyaltyStatus = p_web.RestoreValue('locMobileLoyaltyStatus')
 locMobileUpgradeDate = p_web.RestoreValue('locMobileUpgradeDate')
 locMobileIDNumber = p_web.RestoreValue('locMobileIDNumber')
 locCompleteRepair = p_web.RestoreValue('locCompleteRepair')
 locCChargeTypeReason = p_web.RestoreValue('locCChargeTypeReason')
 locCRepairTypeReason = p_web.RestoreValue('locCRepairTypeReason')
 locWChargeTypeReason = p_web.RestoreValue('locWChargeTypeReason')
 locWRepairTypeReason = p_web.RestoreValue('locWRepairTypeReason')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Amend Job',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('ViewJob')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-ViewJob"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ViewJob',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob0_div')&'">'&p_web.Translate('Browse')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob1_div')&'">'&p_web.Translate('Job Details')&'</a></li>'& CRLF
      If p_web.GSV('textBouncer') <> ''
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob2_div')&'">'&p_web.Translate('Bouncers')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob3_div')&'">'&p_web.Translate('Job Status')&'</a></li>'& CRLF
      If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob4_div')&'">'&p_web.Translate('Claim Status')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob5_div')&'">'&p_web.Translate('Repair Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob6_div')&'">'&p_web.Translate('Attached Parts')&'</a></li>'& CRLF
      If p_web.GSV('Hide:CustomerClassification') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob7_div')&'">'&p_web.Translate('Customer Classification')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob8_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob9_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      If p_web.GSV('job:Date_Completed') = 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob10_div')&'">'&p_web.Translate('Complete Repair')&'</a></li>'& CRLF
      End
      If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob11_div')&'">'&p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</a></li>'& CRLF
      End
      If p_web.GSV('Hide:EstimateReady') = 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewJob12_div')&'">'&p_web.Translate('Estimate Ready')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  do GenerateTab6
  do GenerateTab7
  do GenerateTab8
  do GenerateTab9
  do GenerateTab10
  do GenerateTab11
  do GenerateTab12
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ViewJob_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseEstimateParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseWarrantyParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseEstimateParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseWarrantyParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ViewJob_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewJob_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ViewJob_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseEstimateParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseWarrantyParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewJob_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='ACCSTAT'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.jbn:Engineers_Notes')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Authority_Number')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='UNITTYPE'
      If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
        If p_web.GSV('Hide:CChargeType') <> 1
          If Not (p_web.GSV('Hide:CChargeType') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locCChargeTypeReason')
          End
        End
      End
    End
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ViewJob')>0,p_web.GSV('showtab_ViewJob'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ViewJob'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewJob') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ViewJob'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ViewJob')>0,p_web.GSV('showtab_ViewJob'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewJob') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
      If p_web.GSV('textBouncer') <> ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Bouncers') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Status') & ''''
      If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Claim Status') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Repair Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Attached Parts') & ''''
      If p_web.GSV('Hide:CustomerClassification') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Customer Classification') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      If p_web.GSV('job:Date_Completed') = 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Complete Repair') & ''''
      End
      If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Reason For The Following Field(s) Changing') & ''''
      End
      If p_web.GSV('Hide:EstimateReady') = 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Estimate Ready') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ViewJob_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ViewJob')>0,p_web.GSV('showtab_ViewJob'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ViewJob",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ViewJob')>0,p_web.GSV('showtab_ViewJob'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ViewJob_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ViewJob') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('AmendAddress') = 0 then AmendAddress(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('BrowseAuditFilter') = 0 then BrowseAuditFilter(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('FormBrowseEngineerHistory') = 0 then FormBrowseEngineerHistory(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('BrowseStatusFilter') = 0 then BrowseStatusFilter(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('FormBrowseLocationHistory') = 0 then FormBrowseLocationHistory(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('FormEngineeringOption') = 0 then FormEngineeringOption(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('_BrowseIMEIHistory') = 0 then _BrowseIMEIHistory(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('SelectAccessJobStatus') = 0 then SelectAccessJobStatus(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('PickEngineersNotes') = 0 then PickEngineersNotes(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('SelectNetworks') = 0 then SelectNetworks(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('SelectUnitTypes') = 0 then SelectUnitTypes(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('AllocateEngineer') = 0 then AllocateEngineer(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('JobAccessories') = 0 then JobAccessories(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('JobEstimate') = 0 then JobEstimate(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('BrowseContactHistory') = 0 then BrowseContactHistory(p_web).
    p_web.SetValue('_CallPopups',0)
    do AutoLookups
    p_web.AddPreCall('ViewJob')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseEstimateParts') = 0
      p_web.SetValue('BrowseEstimateParts:NoForm',1)
      p_web.SetValue('BrowseEstimateParts:FormName',loc:formname)
      p_web.SetValue('BrowseEstimateParts:parentIs','Form')
      p_web.SetValue('_parentProc','ViewJob')
      BrowseEstimateParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseEstimateParts:NoForm')
      p_web.DeleteValue('BrowseEstimateParts:FormName')
      p_web.DeleteValue('BrowseEstimateParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseChargeableParts') = 0
      p_web.SetValue('BrowseChargeableParts:NoForm',1)
      p_web.SetValue('BrowseChargeableParts:FormName',loc:formname)
      p_web.SetValue('BrowseChargeableParts:parentIs','Form')
      p_web.SetValue('_parentProc','ViewJob')
      BrowseChargeableParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseChargeableParts:NoForm')
      p_web.DeleteValue('BrowseChargeableParts:FormName')
      p_web.DeleteValue('BrowseChargeableParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseWarrantyParts') = 0
      p_web.SetValue('BrowseWarrantyParts:NoForm',1)
      p_web.SetValue('BrowseWarrantyParts:FormName',loc:formname)
      p_web.SetValue('BrowseWarrantyParts:parentIs','Form')
      p_web.SetValue('_parentProc','ViewJob')
      BrowseWarrantyParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseWarrantyParts:NoForm')
      p_web.DeleteValue('BrowseWarrantyParts:FormName')
      p_web.DeleteValue('BrowseWarrantyParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Browse')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Browse')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Browse')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Browse')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:AmendAddress
        do Comment::link:AmendAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:AuditTrail
        do Comment::link:AuditTrail
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:EngineerHistory
        do Comment::link:EngineerHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:StatusChanges
        do Comment::link:StatusChanges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:BrowseLocationHistory
        do Comment::link:BrowseLocationHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:jobNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:jobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:jobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:DateBooked
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:DateBooked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:DateBooked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locBookingOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locBookingOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locBookingOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEngineeringOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::gap:JobDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::gap:JobDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:ChangeEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:ChangeEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:accountNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:accountNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:accountNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:Completed
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:Completed
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:Completed
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:modelDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:modelDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:modelDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:HubRepair
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:HubRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:HubRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Order_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Order_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Order_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:OBFValidated
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:OBFValidated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:OBFValidated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:ESN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:ESN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:ESN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:MSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locJobType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locJobType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locJobType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:AmendJobType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:AmendJobType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('job:Chargeable_Job') = 'YES'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('job:Chargeable_Job') = 'YES'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Repair_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Repair_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Repair_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('job:Warranty_Job') = 'YES'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Warranty_Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('job:Warranty_Job') = 'YES'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Repair_Type_Warranty
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Repair_Type_Warranty
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Repair_Type_Warranty
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
  If p_web.GSV('textBouncer') <> ''
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Bouncers')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Bouncers')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Bouncers')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Bouncers')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::textBouncer
        do Comment::textBouncer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPreviousUnitHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPreviousUnitHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Status')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Status')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Status')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Status')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Current_Status
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Current_Status
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Current_Status
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Location
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:ExchangeUnit
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:ExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:ExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:LoanUnit
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:LoanUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:LoanUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Exchange_Status
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Exchange_Status
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Exchange_Status
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Loan_Status
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Loan_Status
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Loan_Status
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('jobe:SecondExchangeNumber') > 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::text:SecondExchangeUnit
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:SecondExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:SecondExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Fault_Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Fault_Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jbn:Fault_Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Engineers_Notes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Engineers_Notes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jbn:Engineers_Notes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::gap
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::gap
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:EngineersNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:EngineersNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab4  Routine
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Claim Status')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Claim Status')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Claim Status')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Claim Status')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locHOClaimStatus
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locHOClaimStatus
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locHOClaimStatus
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locRRCClaimStatus
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locRRCClaimStatus
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locRRCClaimStatus
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab5  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Repair Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob5',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob5',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Repair Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Repair Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Repair Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob5',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('Hide:LiquidDamage') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::textLiquidDamage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textLiquidDamage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textLiquidDamage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCurrentEngineer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCurrentEngineer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCurrentEngineer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEngineerAllocated
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEngineerAllocated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEngineerAllocated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:Network
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:Network
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jobe:Network
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Authority_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Authority_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Authority_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Unit_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Unit_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Unit_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab6  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Attached Parts')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob6',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob6',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob6',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,'FormCentreFixed'),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob6',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,'FormCentreFixed'),Net:NoSend,'Attached Parts')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob6',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,'FormCentreFixed'),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Attached Parts')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob6',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,'FormCentreFixed'),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Attached Parts')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob6',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('RepairTypesNoParts') = 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locRepairTypesNoParts
        do Comment::locRepairTypesNoParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES') AND p_web.GSV('RepairTypesNoParts') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::ViewEstimateParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If (p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))) AND p_web.GSV('RepairTypesNoParts') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::browse:ChargeableParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('RepairTypesNoParts') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::browseWarrantyParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab7  Routine
  If p_web.GSV('Hide:CustomerClassification') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Customer Classification')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob7',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob7',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob7',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob7',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Customer Classification')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob7',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Customer Classification')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob7',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Customer Classification')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob7',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileLifetimeValue
        do Value::locMobileLifetimeValue
        do Comment::locMobileLifetimeValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileAverageSpend
        do Value::locMobileAverageSpend
        do Comment::locMobileAverageSpend
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileLoyaltyStatus
        do Value::locMobileLoyaltyStatus
        do Comment::locMobileLoyaltyStatus
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileUpgradeDate
        do Value::locMobileUpgradeDate
        do Comment::locMobileUpgradeDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileIDNumber
        do Value::locMobileIDNumber
        do Comment::locMobileIDNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab8  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob8',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob8',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob8',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob8',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob8',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob8',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob8',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('Job:ViewOnly') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonAllocateEngineer
        do Comment::buttonAllocateEngineer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonAccessories
        do Comment::buttonAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonFaultCodes
        do Comment::buttonFaultCodes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
      If p_web.GSV('Job:ViewOnly') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonEstimate
        do Comment::buttonEstimate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonContactHistory
        do Comment::buttonContactHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Job:ViewOnly') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonAllocateExchange
        do Comment::buttonAllocateExchange
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('Job:ViewOnly') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonAllocateLoan
        do Comment::buttonAllocateLoan
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonCreateInvoice
        do Comment::buttonCreateInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonViewCosts
        do Comment::buttonViewCosts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab9  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob9',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob9',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob9',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob9',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob9',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob9',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob9',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('Job:ViewOnly') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonValidatePOP
        do Comment::buttonValidatePOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('Job:ViewOnly') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonResendXML
        do Comment::buttonResendXML
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('job:Who_Booked') = 'WEB'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonReceiptFromPUP
        do Comment::buttonReceiptFromPUP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab10  Routine
  If p_web.GSV('job:Date_Completed') = 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Complete Repair')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob10',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob10',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob10',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob10',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Complete Repair')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob10',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Complete Repair')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob10',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Complete Repair')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob10',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonCompleteRepair
        do Comment::buttonCompleteRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCompleteRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCompleteRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::textCompleteRepair
        do Comment::textCompleteRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab11  Routine
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob11',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob11',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob11',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob11',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Enter Reason For The Following Field(s) Changing')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob11',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob11',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob11',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('Hide:CChargeType') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCChargeTypeReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCChargeTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCChargeTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('Hide:CRepairType') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCRepairTypeReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCRepairTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCRepairTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:WChargeType') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWChargeTypeReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWChargeTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWChargeTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      end
      do SendPacket
      If p_web.GSV('Hide:WRepairType') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWRepairTypeReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWRepairTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWRepairTypeReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab12  Routine
  If p_web.GSV('Hide:EstimateReady') = 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Estimate Ready')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewJob12',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob12',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob12',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob12',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Estimate Ready')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob12',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Estimate Ready')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob12',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Estimate Ready')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewJob12',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textEstimateReady
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textEstimateReady
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintEstimate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintEstimate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Validate::link:AmendAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:AmendAddress  ! copies value to session value if valid.
  do Comment::link:AmendAddress ! allows comment style to be updated.

ValidateValue::link:AmendAddress  Routine
    If not (1=0)
    End

Value::link:AmendAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('link:AmendAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnAmendAddress','Amend Addresses',p_web.combine(Choose('Amend Addresses' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('AmendAddress',p_web._jsok('Amend Addresses'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('link:AmendAddress'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:AmendAddress  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:AmendAddress:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('link:AmendAddress') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::link:AuditTrail  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:AuditTrail  ! copies value to session value if valid.
  do Comment::link:AuditTrail ! allows comment style to be updated.

ValidateValue::link:AuditTrail  Routine
    If not (1=0)
    End

Value::link:AuditTrail  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('link:AuditTrail') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnAuditTrail','Audit Trail',p_web.combine(Choose('Audit Trail' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('BrowseAuditFilter',p_web._jsok('Browse Audit Trail'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('link:AuditTrail'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:AuditTrail  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:AuditTrail:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('link:AuditTrail') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::link:EngineerHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:EngineerHistory  ! copies value to session value if valid.
  do Comment::link:EngineerHistory ! allows comment style to be updated.

ValidateValue::link:EngineerHistory  Routine
    If not (p_web.GSV('Hide:EngineerHistory') = 1)
    End

Value::link:EngineerHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EngineerHistory') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('link:EngineerHistory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:EngineerHistory') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnEngineerHistory','Engineer History',p_web.combine(Choose('Engineer History' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('FormBrowseEngineerHistory',p_web._jsok('Engineer History'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('link:EngineerHistory'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:EngineerHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:EngineerHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EngineerHistory') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('link:EngineerHistory') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EngineerHistory') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::link:StatusChanges  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:StatusChanges  ! copies value to session value if valid.
  do Comment::link:StatusChanges ! allows comment style to be updated.

ValidateValue::link:StatusChanges  Routine
    If not (1=0)
    End

Value::link:StatusChanges  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('link:StatusChanges') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnStatusChanges','Status Changes',p_web.combine(Choose('Status Changes' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('BrowseStatusFilter',p_web._jsok('Browse Status Changes'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('link:StatusChanges'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:StatusChanges  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:StatusChanges:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('link:StatusChanges') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::link:BrowseLocationHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:BrowseLocationHistory  ! copies value to session value if valid.
  do Comment::link:BrowseLocationHistory ! allows comment style to be updated.

ValidateValue::link:BrowseLocationHistory  Routine
    If not (1=0)
    End

Value::link:BrowseLocationHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('link:BrowseLocationHistory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnLocationHistory','Browse Location History',p_web.combine(Choose('Browse Location History' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('FormBrowseLocationHistory',p_web._jsok('Browse Location History'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('link:BrowseLocationHistory'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:BrowseLocationHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:BrowseLocationHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('link:BrowseLocationHistory') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:jobNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:jobNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:jobNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:jobNumber  ! copies value to session value if valid.
  do Comment::text:jobNumber ! allows comment style to be updated.

ValidateValue::text:jobNumber  Routine
    If not (1=0)
    End

Value::text:jobNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:jobNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:jobNumber" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locJobNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:jobNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:jobNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:jobNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:DateBooked  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:DateBooked') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Booked'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:DateBooked  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:DateBooked  ! copies value to session value if valid.
  do Comment::text:DateBooked ! allows comment style to be updated.

ValidateValue::text:DateBooked  Routine
    If not (1=0)
    End

Value::text:DateBooked  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:DateBooked') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:DateBooked" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(Format(p_web.GSV('job:Date_Booked'),@d06b) & ' ' & Format(p_web.GSV('job:Time_Booked'),@T01) & ' (' & p_web.GSV('job:Who_Booked') & ')',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:DateBooked  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:DateBooked:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:DateBooked') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locBookingOption  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locBookingOption') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Booking Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locBookingOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locBookingOption  ! copies value to session value if valid.
  do Comment::locBookingOption ! allows comment style to be updated.

ValidateValue::locBookingOption  Routine
    If not (1=0)
    End

Value::locBookingOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locBookingOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locBookingOption" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locBookingOption'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locBookingOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locBookingOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locBookingOption') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEngineeringOption  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locEngineeringOption') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Engineering Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEngineeringOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEngineeringOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEngineeringOption = p_web.GetValue('Value')
  End
  do ValidateValue::locEngineeringOption  ! copies value to session value if valid.
  do Comment::locEngineeringOption ! allows comment style to be updated.

ValidateValue::locEngineeringOption  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locEngineeringOption',locEngineeringOption).
    End

Value::locEngineeringOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locEngineeringOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEngineeringOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEngineeringOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locEngineeringOption') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::gap:JobDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::gap:JobDetails  ! copies value to session value if valid.
  do Comment::gap:JobDetails ! allows comment style to be updated.

ValidateValue::gap:JobDetails  Routine
    If not (1)
    End

Value::gap:JobDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('gap:JobDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::gap:JobDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if gap:JobDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('gap:JobDetails') & '_comment',loc:class,Net:NoSend)
  If 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:ChangeEngineeringOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:ChangeEngineeringOption  ! copies value to session value if valid.
  do Value::button:ChangeEngineeringOption
  do Comment::button:ChangeEngineeringOption ! allows comment style to be updated.
  do Value::locEngineeringOption  !1

ValidateValue::button:ChangeEngineeringOption  Routine
    If not (p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1)
    End

Value::button:ChangeEngineeringOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('button:ChangeEngineeringOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:ChangeEngineeringOption'',''viewjob_button:changeengineeringoption_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ChangeEngOption','Change Eng. Option',p_web.combine(Choose('Change Eng. Option' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('FormEngineeringOption',p_web._jsok('Change Engineering Option'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('button:ChangeEngineeringOption'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:ChangeEngineeringOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:ChangeEngineeringOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('button:ChangeEngineeringOption') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:accountNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:accountNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Trade Account'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:accountNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:accountNumber  ! copies value to session value if valid.
  do Comment::text:accountNumber ! allows comment style to be updated.

ValidateValue::text:accountNumber  Routine
    If not (1=0)
    End

Value::text:accountNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:accountNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:accountNumber" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locTradeAccount'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:accountNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:accountNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:accountNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:Completed  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:Completed') & '_prompt',Choose(p_web.GSV('job:Date_Completed') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Date_Completed') = 0,'',p_web.Translate('Completed'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:Completed  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:Completed  ! copies value to session value if valid.
  do Comment::text:Completed ! allows comment style to be updated.

ValidateValue::text:Completed  Routine
    If not (p_web.GSV('job:Date_Completed') = 0)
    End

Value::text:Completed  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Date_Completed') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:Completed') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Date_Completed') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:Completed" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(Format(p_web.GSV('job:Date_Completed'),@d06b) & ' ' & Format(p_web.GSV('job:Time_Completed'),@T01b) & ' - JOB COMPLETED',0) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:Completed  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:Completed:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Date_Completed') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:Completed') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Date_Completed') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:modelDetails  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:modelDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Model Details'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:modelDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:modelDetails  ! copies value to session value if valid.
  do Comment::text:modelDetails ! allows comment style to be updated.

ValidateValue::text:modelDetails  Routine
    If not (1=0)
    End

Value::text:modelDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:modelDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:modelDetails" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:modelDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:modelDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:modelDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:HubRepair  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:HubRepair') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Hub Repair'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:HubRepair  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:HubRepair  ! copies value to session value if valid.
  do Comment::text:HubRepair ! allows comment style to be updated.

ValidateValue::text:HubRepair  Routine
    If not (1=0)
    End

Value::text:HubRepair  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:HubRepair') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:HubRepair" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(Format(p_web.GSV('jobe:HubRepairDate'),@d06b) & ' ' & Format(p_web.GSV('jobe:HubRepairTime'),@T01b),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:HubRepair  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:HubRepair:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:HubRepair') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Order_Number  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Order_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Order Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Order_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Order_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Order_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Order_Number  ! copies value to session value if valid.
  do Comment::job:Order_Number ! allows comment style to be updated.

ValidateValue::job:Order_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Order_Number',job:Order_Number).
    End

Value::job:Order_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Order_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Order_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Order_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Order_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Order_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:OBFValidated  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:OBFValidated') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('OBF Validated'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:OBFValidated  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:OBFValidated  ! copies value to session value if valid.
  do Comment::text:OBFValidated ! allows comment style to be updated.

ValidateValue::text:OBFValidated  Routine
    If not (1=0)
    End

Value::text:OBFValidated  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:OBFValidated') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:OBFValidated" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(Format(p_web.GSV('jobe:OBFValidateDate'),@d06b) & ' ' & Format(p_web.GSV('jobe:OBFValidateTime'),@T01b),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:OBFValidated  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:OBFValidated:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:OBFValidated') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:ESN  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:ESN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('IMEI Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:ESN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:ESN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    job:ESN = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::job:ESN  ! copies value to session value if valid.
  do Comment::job:ESN ! allows comment style to be updated.

ValidateValue::job:ESN  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:ESN',job:ESN).
    End

Value::job:ESN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:ESN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:ESN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:ESN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:ESN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:MSN  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:MSN') & '_prompt',Choose(p_web.GSV('Hide:MSN') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:MSN') = 1,'',p_web.Translate('MSN'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:MSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:MSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    job:MSN = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::job:MSN  ! copies value to session value if valid.
  do Comment::job:MSN ! allows comment style to be updated.

ValidateValue::job:MSN  Routine
    If not (p_web.GSV('Hide:MSN') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:MSN',job:MSN).
    End

Value::job:MSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:MSN') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:MSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:MSN') = 1)
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:MSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:MSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:MSN') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:MSN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:MSN') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locJobType  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locJobType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locJobType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locJobType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locJobType = p_web.GetValue('Value')
  End
  do ValidateValue::locJobType  ! copies value to session value if valid.
  do Comment::locJobType ! allows comment style to be updated.

ValidateValue::locJobType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locJobType',locJobType).
    End

Value::locJobType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locJobType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locJobType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locJobType'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locJobType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locJobType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locJobType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:AmendJobType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:AmendJobType  ! copies value to session value if valid.
  do Comment::button:AmendJobType ! allows comment style to be updated.

ValidateValue::button:AmendJobType  Routine
    If not (p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1)
    End

Value::button:AmendJobType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('button:AmendJobType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ChangeJobType','Change Job Type(s)',p_web.combine(Choose('Change Job Type(s)' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.WindowOpen(clip('SetJobType')&''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:AmendJobType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:AmendJobType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('button:AmendJobType') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Cha. Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Charge_Type  ! copies value to session value if valid.
  do Comment::job:Charge_Type ! allows comment style to be updated.

ValidateValue::job:Charge_Type  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Charge_Type',job:Charge_Type).
    End
  End

Value::job:Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Charge_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Repair_Type  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Cha. Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Repair_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Repair_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Repair_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Repair_Type  ! copies value to session value if valid.
  do Comment::job:Repair_Type ! allows comment style to be updated.

ValidateValue::job:Repair_Type  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Repair_Type',job:Repair_Type).
    End
  End

Value::job:Repair_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Repair_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Repair_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Warranty_Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Warr. Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Warranty_Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Warranty_Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Warranty_Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Warranty_Charge_Type  ! copies value to session value if valid.
  do Comment::job:Warranty_Charge_Type ! allows comment style to be updated.

ValidateValue::job:Warranty_Charge_Type  Routine
  If p_web.GSV('job:Warranty_Job') = 'YES'
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type).
    End
  End

Value::job:Warranty_Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Warranty_Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Warranty_Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Repair_Type_Warranty  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type_Warranty') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Warr. Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Repair_Type_Warranty  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Repair_Type_Warranty = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Repair_Type_Warranty = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Repair_Type_Warranty  ! copies value to session value if valid.
  do Comment::job:Repair_Type_Warranty ! allows comment style to be updated.

ValidateValue::job:Repair_Type_Warranty  Routine
  If p_web.GSV('job:Warranty_Job') = 'YES'
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty).
    End
  End

Value::job:Repair_Type_Warranty  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type_Warranty') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Repair_Type_Warranty
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type_Warranty'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Repair_Type_Warranty  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Repair_Type_Warranty:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type_Warranty') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::textBouncer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textBouncer  ! copies value to session value if valid.
  do Comment::textBouncer ! allows comment style to be updated.

ValidateValue::textBouncer  Routine
    If not (1=0)
    End

Value::textBouncer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('textBouncer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textBouncer" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('textBouncer'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textBouncer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textBouncer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('textBouncer') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPreviousUnitHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPreviousUnitHistory  ! copies value to session value if valid.
  do Comment::buttonPreviousUnitHistory ! allows comment style to be updated.

ValidateValue::buttonPreviousUnitHistory  Routine
    If not (1=0)
    End

Value::buttonPreviousUnitHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonPreviousUnitHistory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PreviousUnitHistory','Previous Unit History',p_web.combine(Choose('Previous Unit History' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('_BrowseIMEIHistory',p_web._jsok('Previous Job History'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('buttonPreviousUnitHistory'),p_web._jsok('fromURL=ViewJob&currentJob=' & p_web.GSV('job:Ref_number'))),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPreviousUnitHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPreviousUnitHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonPreviousUnitHistory') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Current_Status  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Current_Status') & '_prompt',Choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','',p_web.Translate('Current Status'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Current_Status  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(ACCSTAT,acs:StatusOnlyKey,acs:StatusOnlyKey,acs:Status,acs:Status,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(ACCSTAT,acs:StatusOnlyKey,acs:StatusOnlyKey,acs:Status,acs:Status,p_web.GetSessionValue('job:Current_Status')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Current_Status = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Current_Status = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('acs:Status')
    job:Current_Status = p_web.GetValue('acs:Status')
  ElsIf p_web.RequestAjax = 1
    job:Current_Status = acs:Status
  End
  do ValidateValue::job:Current_Status  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','job:Current_Status')
  do AfterLookup
  do Comment::job:Current_Status

ValidateValue::job:Current_Status  Routine
    If not (p_web.GSV('locCurrentEngineer') = 'Not Allocated')
    job:Current_Status = Upper(job:Current_Status)
      if loc:invalid = '' then p_web.SetSessionValue('job:Current_Status',job:Current_Status).
    End

Value::job:Current_Status  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Current_Status') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:CurrentStatus') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Current_Status = p_web.RestoreValue('job:Current_Status')
    do ValidateValue::job:Current_Status
    If job:Current_Status:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = 'Not Allocated')
  ! --- STRING --- job:Current_Status
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CurrentStatus') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Current_Status'',''viewjob_job:current_status_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Current_Status')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Current_Status',p_web.GetSessionValue('job:Current_Status'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''SelectAccessJobStatus'','''&p_web._nocolon('job:Current_Status')&''','''&p_web.translate('Select Status')&''',1,'&Net:LookupRecord&',''acs:Status'',''ViewJob'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Current_Status  Routine
  data
loc:class  string(255)
  code
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    loc:ok = p_web.GetDescription(ACCSTAT,acs:StatusOnlyKey,acs:StatusOnlyKey,acs:Status,acs:Status,p_web.GetValue('Value')) !1
    loc:lookupdone = 1
  Else
    loc:ok = p_web.GetDescription(ACCSTAT,acs:StatusOnlyKey,acs:StatusOnlyKey,acs:Status,acs:Status,p_web.GetSessionValue('job:Current_Status')) !1
  End
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Current_Status:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Current_Status') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locCurrentEngineer') = 'Not Allocated'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Location  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Location') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Location  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Location = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Location = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Location  ! copies value to session value if valid.
  do Comment::job:Location ! allows comment style to be updated.

ValidateValue::job:Location  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Location',job:Location).
    End

Value::job:Location  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Location') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Location'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Location  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Location:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Location') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:ExchangeUnit  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:ExchangeUnit') & '_prompt',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'',p_web.Translate('Exchange Unit'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:ExchangeUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:ExchangeUnit  ! copies value to session value if valid.
  do Comment::text:ExchangeUnit ! allows comment style to be updated.

ValidateValue::text:ExchangeUnit  Routine
    If not (p_web.GSV('job:Exchange_Unit_Number') = 0)
    End

Value::text:ExchangeUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:ExchangeUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:ExchangeUnit" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locExchangeText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:ExchangeUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:ExchangeUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:ExchangeUnit') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:LoanUnit  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:LoanUnit') & '_prompt',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'',p_web.Translate('Loan Unit'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:LoanUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:LoanUnit  ! copies value to session value if valid.
  do Comment::text:LoanUnit ! allows comment style to be updated.

ValidateValue::text:LoanUnit  Routine
    If not (p_web.GSV('job:Loan_Unit_Number') = 0)
    End

Value::text:LoanUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Loan_Unit_Number') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:LoanUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:LoanUnit" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locLoanText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:LoanUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:LoanUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:LoanUnit') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Loan_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Exchange_Status  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Exchange_Status') & '_prompt',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'',p_web.Translate('Exchange Status'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Exchange_Status  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Exchange_Status = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Exchange_Status = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Exchange_Status  ! copies value to session value if valid.
  do Comment::job:Exchange_Status ! allows comment style to be updated.

ValidateValue::job:Exchange_Status  Routine
    If not (p_web.GSV('job:Exchange_Unit_Number') = 0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status).
    End

Value::job:Exchange_Status  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Exchange_Status') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Unit_Number') = 0)
  ! --- DISPLAY --- job:Exchange_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Exchange_Status'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Exchange_Status  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Exchange_Status:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Exchange_Status') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Loan_Status  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Loan_Status') & '_prompt',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'',p_web.Translate('Loan Status'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Loan_Status  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Loan_Status = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Loan_Status = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Loan_Status  ! copies value to session value if valid.
  do Comment::job:Loan_Status ! allows comment style to be updated.

ValidateValue::job:Loan_Status  Routine
    If not (p_web.GSV('job:Loan_Unit_Number') = 0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Loan_Status',job:Loan_Status).
    End

Value::job:Loan_Status  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Loan_Unit_Number') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Loan_Status') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Unit_Number') = 0)
  ! --- DISPLAY --- job:Loan_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Loan_Status'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Loan_Status  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Loan_Status:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Loan_Status') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Loan_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::text:SecondExchangeUnit  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:SecondExchangeUnit') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Second Exchange Unit'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::text:SecondExchangeUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:SecondExchangeUnit  ! copies value to session value if valid.
  do Comment::text:SecondExchangeUnit ! allows comment style to be updated.

ValidateValue::text:SecondExchangeUnit  Routine
  If p_web.GSV('jobe:SecondExchangeNumber') > 0
    If not (1=0)
    End
  End

Value::text:SecondExchangeUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('text:SecondExchangeUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:SecondExchangeUnit" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Second Exchange Unit Attached',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:SecondExchangeUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:SecondExchangeUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('text:SecondExchangeUnit') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jbn:Fault_Description  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Fault Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Fault_Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Fault_Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Fault_Description = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Fault_Description  ! copies value to session value if valid.
  do Value::jbn:Fault_Description
  do SendAlert
  do Comment::jbn:Fault_Description ! allows comment style to be updated.

ValidateValue::jbn:Fault_Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description).
    End

Value::jbn:Fault_Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,'TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jbn:Fault_Description = p_web.RestoreValue('jbn:Fault_Description')
    do ValidateValue::jbn:Fault_Description
    If jbn:Fault_Description:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jbn:Fault_Description
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Fault_Description'',''viewjob_jbn:fault_description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('jbn:Fault_Description',p_web.GetSessionValue('jbn:Fault_Description'),4,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::jbn:Fault_Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jbn:Fault_Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jbn:Engineers_Notes  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Engineers Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Engineers_Notes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Engineers_Notes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Engineers_Notes = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Engineers_Notes  ! copies value to session value if valid.
  !Workaround for blank
  if (sub(p_web.GSV('jbn:Engineers_Notes'),1,1) = ' ')
      p_web.SSV('jbn:Engineers_Notes',sub(p_web.GSV('jbn:Engineers_Notes'),2,255))
  end ! if (sub(p_web.GSV('jbn:Engineers_Notes',1,1) = '  ')
  do Value::jbn:Engineers_Notes
  do SendAlert
  do Comment::jbn:Engineers_Notes ! allows comment style to be updated.

ValidateValue::jbn:Engineers_Notes  Routine
    If not (1=0)
    jbn:Engineers_Notes = Upper(jbn:Engineers_Notes)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes).
    End

Value::jbn:Engineers_Notes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,'TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('Job:ViewOnly') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:Engineers_Notes = p_web.RestoreValue('jbn:Engineers_Notes')
    do ValidateValue::jbn:Engineers_Notes
    If jbn:Engineers_Notes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jbn:Engineers_Notes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Engineers_Notes'',''viewjob_jbn:engineers_notes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jbn:Engineers_Notes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jbn:Engineers_Notes',p_web.GetSessionValue('jbn:Engineers_Notes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::jbn:Engineers_Notes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jbn:Engineers_Notes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::gap  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::gap  ! copies value to session value if valid.
  do Comment::gap ! allows comment style to be updated.

ValidateValue::gap  Routine
    If not (1)
    End

Value::gap  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('gap') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::gap  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if gap:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('gap') & '_comment',loc:class,Net:NoSend)
  If 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:EngineersNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:EngineersNotes  ! copies value to session value if valid.
  do Value::button:EngineersNotes
  do Comment::button:EngineersNotes ! allows comment style to be updated.
  do Value::jbn:Engineers_Notes  !1

ValidateValue::button:EngineersNotes  Routine
    If not (p_web.GSV('Job:ViewOnly') = 1)
    End

Value::button:EngineersNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('button:EngineersNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:EngineersNotes'',''viewjob_button:engineersnotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','EngineersNotes','Engineers Notes',p_web.combine(Choose('Engineers Notes' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MenuButton'),loc:formname,,,p_web.OpenDialog('PickEngineersNotes',p_web._jsok('Engineers Notes'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('button:EngineersNotes'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:EngineersNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:EngineersNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('button:EngineersNotes') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Job:ViewOnly') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locHOClaimStatus  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locHOClaimStatus') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('H/O Status'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locHOClaimStatus  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locHOClaimStatus = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locHOClaimStatus = p_web.GetValue('Value')
  End
  do ValidateValue::locHOClaimStatus  ! copies value to session value if valid.
  do Comment::locHOClaimStatus ! allows comment style to be updated.

ValidateValue::locHOClaimStatus  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locHOClaimStatus',locHOClaimStatus).
    End

Value::locHOClaimStatus  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locHOClaimStatus') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locHOClaimStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locHOClaimStatus'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locHOClaimStatus  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locHOClaimStatus:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locHOClaimStatus') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locRRCClaimStatus  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locRRCClaimStatus') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('RRC Status'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locRRCClaimStatus  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locRRCClaimStatus = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locRRCClaimStatus = p_web.GetValue('Value')
  End
  do ValidateValue::locRRCClaimStatus  ! copies value to session value if valid.
  do Comment::locRRCClaimStatus ! allows comment style to be updated.

ValidateValue::locRRCClaimStatus  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locRRCClaimStatus',locRRCClaimStatus).
    End

Value::locRRCClaimStatus  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locRRCClaimStatus') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locRRCClaimStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locRRCClaimStatus'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locRRCClaimStatus  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locRRCClaimStatus:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locRRCClaimStatus') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::textLiquidDamage  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('textLiquidDamage') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::textLiquidDamage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textLiquidDamage  ! copies value to session value if valid.
  do Comment::textLiquidDamage ! allows comment style to be updated.

ValidateValue::textLiquidDamage  Routine
  If p_web.GSV('Hide:LiquidDamage') <> 1
    If not (1=0)
    End
  End

Value::textLiquidDamage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('textLiquidDamage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textLiquidDamage" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('LIQUID DAMAGE',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textLiquidDamage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textLiquidDamage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('textLiquidDamage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCurrentEngineer  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCurrentEngineer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Current Engineer'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCurrentEngineer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCurrentEngineer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCurrentEngineer = p_web.GetValue('Value')
  End
  do ValidateValue::locCurrentEngineer  ! copies value to session value if valid.
  do Comment::locCurrentEngineer ! allows comment style to be updated.

ValidateValue::locCurrentEngineer  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer).
    End

Value::locCurrentEngineer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locCurrentEngineer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locCurrentEngineer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCurrentEngineer'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCurrentEngineer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCurrentEngineer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCurrentEngineer') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEngineerAllocated  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locEngineerAllocated') & '_prompt',Choose(p_web.GSV('job:Engineer') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Engineer') = '','',p_web.Translate('Allocated'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEngineerAllocated  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locEngineerAllocated  ! copies value to session value if valid.
  do Comment::locEngineerAllocated ! allows comment style to be updated.

ValidateValue::locEngineerAllocated  Routine
    If not (p_web.GSV('job:Engineer') = '')
    End

Value::locEngineerAllocated  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Engineer') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locEngineerAllocated') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Engineer') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locEngineerAllocated" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locEngineerAllocated'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEngineerAllocated  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEngineerAllocated:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Engineer') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locEngineerAllocated') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Engineer') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jobe:Network  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('jobe:Network') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Network'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:Network  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetSessionValue('jobe:Network')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:Network = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    jobe:Network = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('net:Network')
    jobe:Network = p_web.GetValue('net:Network')
  ElsIf p_web.RequestAjax = 1
    jobe:Network = net:Network
  End
  do ValidateValue::jobe:Network  ! copies value to session value if valid.
      Access:NETWORKS.Clearkey(net:NetworkKey)
      net:Network    = p_web.GSV('jobe:Network')
      if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
          ! Found
      else ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
          ! Error
          p_web.SSV('jobe:Network','')
      end ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
  p_Web.SetValue('lookupfield','jobe:Network')
  do AfterLookup
  do Value::jobe:Network
  do SendAlert
  do Comment::jobe:Network

ValidateValue::jobe:Network  Routine
    If not (1=0)
    jobe:Network = Upper(jobe:Network)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:Network',jobe:Network).
    End

Value::jobe:Network  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('jobe:Network') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:Network = p_web.RestoreValue('jobe:Network')
    do ValidateValue::jobe:Network
    If jobe:Network:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:Network
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Network'',''viewjob_jobe:network_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Network')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','jobe:Network',p_web.GetSessionValue('jobe:Network'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),'Network',,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''SelectNetworks'','''&p_web._nocolon('jobe:Network')&''','''&p_web.translate('Select Network')&''',1,'&Net:LookupRecord&',''net:Network'',''ViewJob'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::jobe:Network  Routine
  data
loc:class  string(255)
  code
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetValue('Value')) !1
    loc:lookupdone = 1
  Else
    loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,net:Network,p_web.GetSessionValue('jobe:Network')) !1
  End
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jobe:Network:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('jobe:Network') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Authority_Number  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Authority Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Authority_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Authority_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Authority_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Authority_Number  ! copies value to session value if valid.
  do Value::job:Authority_Number
  do SendAlert
  do Comment::job:Authority_Number ! allows comment style to be updated.

ValidateValue::job:Authority_Number  Routine
    If not (1=0)
    job:Authority_Number = Upper(job:Authority_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Authority_Number',job:Authority_Number).
    End

Value::job:Authority_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('Job:ViewOnly') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Authority_Number = p_web.RestoreValue('job:Authority_Number')
    do ValidateValue::job:Authority_Number
    If job:Authority_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Authority_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Authority_Number'',''viewjob_job:authority_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Authority_Number',p_web.GetSessionValueFormat('job:Authority_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Authority_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Authority_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Unit_Type  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unit Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Unit_Type  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(UNITTYPE,uni:Unit_Type_Key,uni:Unit_Type_Key,uni:Unit_Type,uni:Unit_Type,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(UNITTYPE,uni:Unit_Type_Key,uni:Unit_Type_Key,uni:Unit_Type,uni:Unit_Type,p_web.GetSessionValue('job:Unit_Type')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Unit_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Unit_Type = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('uni:Unit_Type')
    job:Unit_Type = p_web.GetValue('uni:Unit_Type')
  ElsIf p_web.RequestAjax = 1
    job:Unit_Type = uni:Unit_Type
  End
  do ValidateValue::job:Unit_Type  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','job:Unit_Type')
  do AfterLookup
  do Value::job:Unit_Type
  do SendAlert
  do Comment::job:Unit_Type

ValidateValue::job:Unit_Type  Routine
    If not (1=0)
    job:Unit_Type = Upper(job:Unit_Type)
      if loc:invalid = '' then p_web.SetSessionValue('job:Unit_Type',job:Unit_Type).
    End

Value::job:Unit_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('Job:ViewOnly') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Unit_Type = p_web.RestoreValue('job:Unit_Type')
    do ValidateValue::job:Unit_Type
    If job:Unit_Type:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Unit_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Unit_Type'',''viewjob_job:unit_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Unit_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Unit_Type',p_web.GetSessionValue('job:Unit_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''SelectUnitTypes'','''&p_web._nocolon('job:Unit_Type')&''','''&p_web.translate('Select Unit Type')&''',1,'&Net:LookupRecord&',''uni:Unit_Type'',''ViewJob'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Unit_Type  Routine
  data
loc:class  string(255)
  code
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    loc:ok = p_web.GetDescription(UNITTYPE,uni:Unit_Type_Key,uni:Unit_Type_Key,uni:Unit_Type,uni:Unit_Type,p_web.GetValue('Value')) !1
    loc:lookupdone = 1
  Else
    loc:ok = p_web.GetDescription(UNITTYPE,uni:Unit_Type_Key,uni:Unit_Type_Key,uni:Unit_Type,uni:Unit_Type,p_web.GetSessionValue('job:Unit_Type')) !1
  End
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Unit_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locRepairTypesNoParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locRepairTypesNoParts  ! copies value to session value if valid.
  do Comment::locRepairTypesNoParts ! allows comment style to be updated.

ValidateValue::locRepairTypesNoParts  Routine
  If p_web.GSV('RepairTypesNoParts') = 1
    If not (1=0)
    End
  End

Value::locRepairTypesNoParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locRepairTypesNoParts') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locRepairTypesNoParts" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Repair Type does not allow parts',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locRepairTypesNoParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locRepairTypesNoParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locRepairTypesNoParts') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::ViewEstimateParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('epr:Record_Number')
  End
  do ValidateValue::ViewEstimateParts  ! copies value to session value if valid.
  do Comment::ViewEstimateParts ! allows comment style to be updated.

ValidateValue::ViewEstimateParts  Routine
  If (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES') AND p_web.GSV('RepairTypesNoParts') <> 1
    If not (p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
    End
  End

Value::ViewEstimateParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),1,0))
  ! --- BROWSE ---  BrowseEstimateParts --
  p_web.SetValue('BrowseEstimateParts:NoForm',1)
  p_web.SetValue('BrowseEstimateParts:FormName',loc:formname)
  p_web.SetValue('BrowseEstimateParts:parentIs','Form')
  p_web.SetValue('_parentProc','ViewJob')
  if p_web.RequestAjax = 0
    p_web.SSV('ViewJob:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('ViewJob_BrowseEstimateParts_embedded_div')&'"><!-- Net:BrowseEstimateParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('ViewJob_' & lower('BrowseEstimateParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('ViewJob:_popup_',1)
    elsif p_web.GSV('ViewJob:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseEstimateParts --><13,10>'
  end
  do SendPacket
Comment::ViewEstimateParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if ViewEstimateParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('ViewEstimateParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::browse:ChargeableParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('par:Record_Number')
  End
  do ValidateValue::browse:ChargeableParts  ! copies value to session value if valid.
  do Comment::browse:ChargeableParts ! allows comment style to be updated.

ValidateValue::browse:ChargeableParts  Routine
  If (p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))) AND p_web.GSV('RepairTypesNoParts') <> 1
    If not (p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES'))
    End
  End

Value::browse:ChargeableParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES'),1,0))
  ! --- BROWSE ---  BrowseChargeableParts --
  p_web.SetValue('BrowseChargeableParts:NoForm',1)
  p_web.SetValue('BrowseChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','ViewJob')
  if p_web.RequestAjax = 0
    p_web.SSV('ViewJob:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('ViewJob_BrowseChargeableParts_embedded_div')&'"><!-- Net:BrowseChargeableParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('ViewJob_' & lower('BrowseChargeableParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('ViewJob:_popup_',1)
    elsif p_web.GSV('ViewJob:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseChargeableParts --><13,10>'
  end
  do SendPacket
Comment::browse:ChargeableParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if browse:ChargeableParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES'),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('browse:ChargeableParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES')
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::browseWarrantyParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('wpr:Record_Number')
  End
  do ValidateValue::browseWarrantyParts  ! copies value to session value if valid.
  do Comment::browseWarrantyParts ! allows comment style to be updated.

ValidateValue::browseWarrantyParts  Routine
  If p_web.GSV('RepairTypesNoParts') <> 1
    If not (p_web.GSV('job:Warranty_Job') <> 'YES')
    End
  End

Value::browseWarrantyParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseWarrantyParts --
  p_web.SetValue('BrowseWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','ViewJob')
  if p_web.RequestAjax = 0
    p_web.SSV('ViewJob:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('ViewJob_BrowseWarrantyParts_embedded_div')&'"><!-- Net:BrowseWarrantyParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('ViewJob_' & lower('BrowseWarrantyParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('ViewJob:_popup_',1)
    elsif p_web.GSV('ViewJob:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseWarrantyParts --><13,10>'
  end
  do SendPacket
Comment::browseWarrantyParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if browseWarrantyParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('browseWarrantyParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locMobileLifetimeValue  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileLifetimeValue') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Lifetime Value'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileLifetimeValue  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileLifetimeValue = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileLifetimeValue = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileLifetimeValue  ! copies value to session value if valid.
  do Comment::locMobileLifetimeValue ! allows comment style to be updated.

ValidateValue::locMobileLifetimeValue  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue).
    End

Value::locMobileLifetimeValue  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileLifetimeValue') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locMobileLifetimeValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileLifetimeValue'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locMobileLifetimeValue  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locMobileLifetimeValue:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileLifetimeValue') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locMobileAverageSpend  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileAverageSpend') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Average Spend'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileAverageSpend  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileAverageSpend = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileAverageSpend = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileAverageSpend  ! copies value to session value if valid.
  do Comment::locMobileAverageSpend ! allows comment style to be updated.

ValidateValue::locMobileAverageSpend  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend).
    End

Value::locMobileAverageSpend  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileAverageSpend') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locMobileAverageSpend
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileAverageSpend'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locMobileAverageSpend  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locMobileAverageSpend:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileAverageSpend') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locMobileLoyaltyStatus  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileLoyaltyStatus') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Loyalty Status'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileLoyaltyStatus  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileLoyaltyStatus = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileLoyaltyStatus = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileLoyaltyStatus  ! copies value to session value if valid.
  do Comment::locMobileLoyaltyStatus ! allows comment style to be updated.

ValidateValue::locMobileLoyaltyStatus  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus).
    End

Value::locMobileLoyaltyStatus  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileLoyaltyStatus') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locMobileLoyaltyStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileLoyaltyStatus'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locMobileLoyaltyStatus  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locMobileLoyaltyStatus:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileLoyaltyStatus') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locMobileUpgradeDate  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileUpgradeDate') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Upgrade Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileUpgradeDate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileUpgradeDate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileUpgradeDate = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileUpgradeDate  ! copies value to session value if valid.
  do Comment::locMobileUpgradeDate ! allows comment style to be updated.

ValidateValue::locMobileUpgradeDate  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate).
    End

Value::locMobileUpgradeDate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileUpgradeDate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locMobileUpgradeDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileUpgradeDate'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locMobileUpgradeDate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locMobileUpgradeDate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileUpgradeDate') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locMobileIDNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileIDNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('ID Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileIDNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileIDNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileIDNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileIDNumber  ! copies value to session value if valid.
  do Comment::locMobileIDNumber ! allows comment style to be updated.

ValidateValue::locMobileIDNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber).
    End

Value::locMobileIDNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileIDNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locMobileIDNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileIDNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locMobileIDNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locMobileIDNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locMobileIDNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAllocateEngineer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAllocateEngineer  ! copies value to session value if valid.
  do Value::buttonAllocateEngineer
  do Comment::buttonAllocateEngineer ! allows comment style to be updated.
  do Value::job:Current_Status  !1
  do Value::locCurrentEngineer  !1
  do Value::locEngineerAllocated  !1

ValidateValue::buttonAllocateEngineer  Routine
  If p_web.GSV('Job:ViewOnly') <> 1
    If not (p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES')
    End
  End

Value::buttonAllocateEngineer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateEngineer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAllocateEngineer'',''viewjob_buttonallocateengineer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AllocateEngineer','Allocate Engineer',p_web.combine(Choose('Allocate Engineer' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.OpenDialog('AllocateEngineer',p_web._jsok('Allocate Engineer'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('buttonAllocateEngineer'),p_web._jsok('')),,loc:disabled,'/images/alleng.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAllocateEngineer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAllocateEngineer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateEngineer') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAccessories  ! copies value to session value if valid.
  do Comment::buttonAccessories ! allows comment style to be updated.

ValidateValue::buttonAccessories  Routine
    If not (p_web.GSV('Hide:ButtonAccessories') = 1)
    End

Value::buttonAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonAccessories') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonAccessories') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Accessories','Accessories',p_web.combine(Choose('Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.OpenDialog('JobAccessories',p_web._jsok('Job Accessories'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('buttonAccessories'),p_web._jsok('')),,loc:disabled,'/images/accessories.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ButtonAccessories') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAccessories') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ButtonAccessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonFaultCodes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonFaultCodes  ! copies value to session value if valid.
  do Comment::buttonFaultCodes ! allows comment style to be updated.

ValidateValue::buttonFaultCodes  Routine
    If not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1)
    End

Value::buttonFaultCodes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonFaultCodes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  If '_self'='_self' and Loc:Disabled = 0
    Loc:Disabled = -1
  End
  ! the button is set as a "submit" button, and this must have a URL
  packet = clip(packet) & p_web.CreateButton('submit','FaultCodes','Fault Codes',p_web.combine(Choose('Fault Codes' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,p_web._MakeURL('JobFaultCodes'&''),clip('_self'),,,loc:disabled,'/images/fault.png',,,,,,,,,'nt-left')!j
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonFaultCodes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonFaultCodes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonFaultCodes') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonEstimate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonEstimate  ! copies value to session value if valid.
  do Value::buttonEstimate
  do Comment::buttonEstimate ! allows comment style to be updated.
  do Value::browse:ChargeableParts  !1
  do Value::browseWarrantyParts  !1
  do Value::ViewEstimateParts  !1
  do Value::job:Current_Status  !1
  do Value::textEstimateReady  !1

ValidateValue::buttonEstimate  Routine
  If p_web.GSV('Job:ViewOnly') <> 1
    If not (p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set')
    End
  End

Value::buttonEstimate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonEstimate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonEstimate'',''viewjob_buttonestimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Estimate','Estimate',p_web.combine(Choose('Estimate' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.OpenDialog('JobEstimate',p_web._jsok('Job Estimate'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('buttonEstimate'),p_web._jsok('')),,loc:disabled,'/images/estimate.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonEstimate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonEstimate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonEstimate') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonContactHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonContactHistory  ! copies value to session value if valid.
  do Comment::buttonContactHistory ! allows comment style to be updated.

ValidateValue::buttonContactHistory  Routine
    If not (1=0)
    End

Value::buttonContactHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonContactHistory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ContactHistory','Contact History',p_web.combine(Choose('Contact History' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.OpenDialog('BrowseContactHistory',p_web._jsok('Browse Contact History'),0,'''' & lower('ViewJob') & '''' ,p_web.AddBrowseValue('ViewJob','WEBJOB',wob:RecordNumberKey),'',p_web._jsok('buttonContactHistory'),p_web._jsok('')),,loc:disabled,'images\contacthist.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonContactHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonContactHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonContactHistory') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAllocateExchange  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAllocateExchange  ! copies value to session value if valid.
  do Comment::buttonAllocateExchange ! allows comment style to be updated.

ValidateValue::buttonAllocateExchange  Routine
  If p_web.GSV('Job:ViewOnly') <> 1
    If not (p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set')
    End
  End

Value::buttonAllocateExchange  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateExchange') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PickExchangeUnit','Exchange Unit',p_web.combine(Choose('Exchange Unit' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('PickExchangeUnit&' & p_web._jsok('ReturnURL=ViewJob') &''&'','_self'),,loc:disabled,'/images/allexchange.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAllocateExchange  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAllocateExchange:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateExchange') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAllocateLoan  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAllocateLoan  ! copies value to session value if valid.
  do Comment::buttonAllocateLoan ! allows comment style to be updated.

ValidateValue::buttonAllocateLoan  Routine
  If p_web.GSV('Job:ViewOnly') <> 1
    If not (p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set')
    End
  End

Value::buttonAllocateLoan  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateLoan') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PickLoanUnit','Loan Unit',p_web.combine(Choose('Loan Unit' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('PickLoanUnit')&''&'','_self'),,loc:disabled,'/images/allexchange.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAllocateLoan  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAllocateLoan:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateLoan') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCreateInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateInvoice  ! copies value to session value if valid.
  do Comment::buttonCreateInvoice ! allows comment style to be updated.

ValidateValue::buttonCreateInvoice  Routine
    If not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1)
    End

Value::buttonCreateInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonCreateInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),p_web.combine(Choose(p_web.GSV('URL:CreateInvoiceText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip(p_web.GSV('URL:CreateInvoice'))&''&'',p_web.GSV('URL:CreateInvoiceTarget')),,loc:disabled,'\images\moneyp.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCreateInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCreateInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonCreateInvoice') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonViewCosts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonViewCosts  ! copies value to session value if valid.
  do Comment::buttonViewCosts ! allows comment style to be updated.

ValidateValue::buttonViewCosts  Routine
    If not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1)
    End

Value::buttonViewCosts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonViewCosts') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ViewCosts','View Costs',p_web.combine(Choose('View Costs' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip(p_web.GSV('URL:ViewCosts'))&''&'','_self'),,loc:disabled,'/images/moneyp.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonViewCosts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonViewCosts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonViewCosts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonValidatePOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonValidatePOP  ! copies value to session value if valid.
  do Comment::buttonValidatePOP ! allows comment style to be updated.

ValidateValue::buttonValidatePOP  Routine
  If p_web.GSV('Job:ViewOnly') <> 1
    If not (p_web.GSV('Hide:ValidatePOP') = 1)
    End
  End

Value::buttonValidatePOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ValidatePOP') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonValidatePOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ValidatePOP') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ValidatePOP','Validate POP',p_web.combine(Choose('Validate POP' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ProofOfPurchase')&''&'','_self'),,loc:disabled,'/images/validate.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonValidatePOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonValidatePOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ValidatePOP') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonValidatePOP') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ValidatePOP') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonResendXML  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonResendXML  ! copies value to session value if valid.
      do ResendXML
  do Value::buttonResendXML
  do Comment::buttonResendXML ! allows comment style to be updated.

ValidateValue::buttonResendXML  Routine
  If p_web.GSV('Job:ViewOnly') <> 1
    If not (p_web.GSV('Hide:ResendXML') = 1)
    End
  End

Value::buttonResendXML  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ResendXML') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonResendXML') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ResendXML') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('alert(''Job Resent.'');')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''buttonResendXML'',''viewjob_buttonresendxml_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ResendXML','Resend XML To NMS',p_web.combine(Choose('Resend XML To NMS' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,,loc:javascript,loc:disabled,'/images/resend.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonResendXML  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonResendXML:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ResendXML') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonResendXML') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ResendXML') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonReceiptFromPUP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonReceiptFromPUP  ! copies value to session value if valid.
  do Comment::buttonReceiptFromPUP ! allows comment style to be updated.

ValidateValue::buttonReceiptFromPUP  Routine
  If p_web.GSV('job:Who_Booked') = 'WEB'
    If not (1=0)
    End
  End

Value::buttonReceiptFromPUP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonReceiptFromPUP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ReceiptFromPUP','Receipt From PUP',p_web.combine(Choose('Receipt From PUP' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ReceiptFromPUP')&''&'','_self'),,loc:disabled,'/images/receipt.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonReceiptFromPUP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonReceiptFromPUP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonReceiptFromPUP') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCompleteRepair  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCompleteRepair  ! copies value to session value if valid.
  do Comment::buttonCompleteRepair ! allows comment style to be updated.

ValidateValue::buttonCompleteRepair  Routine
    If not (p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1)
    End

Value::buttonCompleteRepair  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonCompleteRepair') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CompleteRepair','Complete Repair',p_web.combine(Choose('Complete Repair' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('BillingConfirmation?JobCompleteProcess=1')&''&'','_self'),,loc:disabled,'images/complete.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCompleteRepair  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCompleteRepair:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonCompleteRepair') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locCompleteRepair  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCompleteRepair = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCompleteRepair = p_web.GetValue('Value')
  End
  do ValidateValue::locCompleteRepair  ! copies value to session value if valid.
  do Comment::locCompleteRepair ! allows comment style to be updated.

ValidateValue::locCompleteRepair  Routine
    If not (p_web.GSV('locCompleteRepair') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locCompleteRepair',locCompleteRepair).
    End

Value::locCompleteRepair  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locCompleteRepair') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locCompleteRepair') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,'RedBoldSmall')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locCompleteRepair = p_web.RestoreValue('locCompleteRepair')
    do ValidateValue::locCompleteRepair
    If locCompleteRepair:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locCompleteRepair') = '')
  ! --- TEXT --- locCompleteRepair
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('locCompleteRepair',p_web.GetSessionValue('locCompleteRepair'),10,60,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locCompleteRepair),,0,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCompleteRepair  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCompleteRepair:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locCompleteRepair') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCompleteRepair') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locCompleteRepair') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::textCompleteRepair  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textCompleteRepair  ! copies value to session value if valid.
  do Comment::textCompleteRepair ! allows comment style to be updated.

ValidateValue::textCompleteRepair  Routine
    If not (1=0)
    End

Value::textCompleteRepair  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('textCompleteRepair') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textCompleteRepair" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('textCompleteRepair'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textCompleteRepair  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textCompleteRepair:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('textCompleteRepair') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCChargeTypeReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_prompt',Choose(p_web.GSV('Hide:CChargeType') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,'FormPrompt2')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CChargeType') = 1,'',p_web.Translate('Reason For Chargeable Charge Type Change'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCChargeTypeReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCChargeTypeReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCChargeTypeReason = p_web.GetValue('Value')
  End
  do ValidateValue::locCChargeTypeReason  ! copies value to session value if valid.
  do Value::locCChargeTypeReason
  do SendAlert
  do Comment::locCChargeTypeReason ! allows comment style to be updated.

ValidateValue::locCChargeTypeReason  Routine
  If p_web.GSV('Hide:CChargeType') <> 1
    If not (p_web.GSV('Hide:CChargeType') = 1)
  If locCChargeTypeReason = ''
    loc:Invalid = 'locCChargeTypeReason'
    locCChargeTypeReason:IsInvalid = true
    loc:alert = p_web.translate('Reason For Chargeable Charge Type Change') & ' ' & p_web.site.RequiredText
  End
    locCChargeTypeReason = Upper(locCChargeTypeReason)
      if loc:invalid = '' then p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason).
    End
  End

Value::locCChargeTypeReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CChargeType') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locCChargeTypeReason = p_web.RestoreValue('locCChargeTypeReason')
    do ValidateValue::locCChargeTypeReason
    If locCChargeTypeReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CChargeType') = 1)
  ! --- TEXT --- locCChargeTypeReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCChargeTypeReason'',''viewjob_loccchargetypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locCChargeTypeReason',p_web.GetSessionValue('locCChargeTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locCChargeTypeReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCChargeTypeReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCChargeTypeReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GSV('Hide:CChargeType') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CChargeType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCRepairTypeReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_prompt',Choose(p_web.GSV('Hide:CRepairType') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,'FormPrompt2')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CRepairType') = 1,'',p_web.Translate('Reason For Chargeable Repair Type Change'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCRepairTypeReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCRepairTypeReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCRepairTypeReason = p_web.GetValue('Value')
  End
  do ValidateValue::locCRepairTypeReason  ! copies value to session value if valid.
  do Value::locCRepairTypeReason
  do SendAlert
  do Comment::locCRepairTypeReason ! allows comment style to be updated.

ValidateValue::locCRepairTypeReason  Routine
  If p_web.GSV('Hide:CRepairType') <> 1
    If not (p_web.GSV('Hide:CRepairType') = 1)
  If locCRepairTypeReason = ''
    loc:Invalid = 'locCRepairTypeReason'
    locCRepairTypeReason:IsInvalid = true
    loc:alert = p_web.translate('Reason For Chargeable Repair Type Change') & ' ' & p_web.site.RequiredText
  End
    locCRepairTypeReason = Upper(locCRepairTypeReason)
      if loc:invalid = '' then p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason).
    End
  End

Value::locCRepairTypeReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CRepairType') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locCRepairTypeReason = p_web.RestoreValue('locCRepairTypeReason')
    do ValidateValue::locCRepairTypeReason
    If locCRepairTypeReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CRepairType') = 1)
  ! --- TEXT --- locCRepairTypeReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCRepairTypeReason'',''viewjob_loccrepairtypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locCRepairTypeReason',p_web.GetSessionValue('locCRepairTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locCRepairTypeReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCRepairTypeReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCRepairTypeReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GSV('Hide:CRepairType') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CRepairType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWChargeTypeReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_prompt',Choose(p_web.GSV('Hide:WChargeType') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,'FormPrompt2')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:WChargeType') = 1,'',p_web.Translate('Reason For Warranty Charge Type Change'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWChargeTypeReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWChargeTypeReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWChargeTypeReason = p_web.GetValue('Value')
  End
  do ValidateValue::locWChargeTypeReason  ! copies value to session value if valid.
  do Value::locWChargeTypeReason
  do SendAlert
  do Comment::locWChargeTypeReason ! allows comment style to be updated.

ValidateValue::locWChargeTypeReason  Routine
  If p_web.GSV('Hide:WChargeType') <> 1
    If not (p_web.GSV('Hide:WChargeType') = 1)
  If locWChargeTypeReason = ''
    loc:Invalid = 'locWChargeTypeReason'
    locWChargeTypeReason:IsInvalid = true
    loc:alert = p_web.translate('Reason For Warranty Charge Type Change') & ' ' & p_web.site.RequiredText
  End
    locWChargeTypeReason = Upper(locWChargeTypeReason)
      if loc:invalid = '' then p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason).
    End
  End

Value::locWChargeTypeReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:WChargeType') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locWChargeTypeReason = p_web.RestoreValue('locWChargeTypeReason')
    do ValidateValue::locWChargeTypeReason
    If locWChargeTypeReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:WChargeType') = 1)
  ! --- TEXT --- locWChargeTypeReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWChargeTypeReason'',''viewjob_locwchargetypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locWChargeTypeReason',p_web.GetSessionValue('locWChargeTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locWChargeTypeReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWChargeTypeReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWChargeTypeReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GSV('Hide:WChargeType') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:WChargeType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWRepairTypeReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_prompt',Choose(p_web.GSV('Hide:WRepairType') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,'FormPrompt2')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:WRepairType') = 1,'',p_web.Translate('Reason For Warranty Repair Type Change'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWRepairTypeReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWRepairTypeReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWRepairTypeReason = p_web.GetValue('Value')
  End
  do ValidateValue::locWRepairTypeReason  ! copies value to session value if valid.
  do Value::locWRepairTypeReason
  do SendAlert
  do Comment::locWRepairTypeReason ! allows comment style to be updated.

ValidateValue::locWRepairTypeReason  Routine
  If p_web.GSV('Hide:WRepairType') <> 1
    If not (p_web.GSV('Hide:WRepairType') = 1)
  If locWRepairTypeReason = ''
    loc:Invalid = 'locWRepairTypeReason'
    locWRepairTypeReason:IsInvalid = true
    loc:alert = p_web.translate('Reason For Warranty Repair Type Change') & ' ' & p_web.site.RequiredText
  End
    locWRepairTypeReason = Upper(locWRepairTypeReason)
      if loc:invalid = '' then p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason).
    End
  End

Value::locWRepairTypeReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:WRepairType') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locWRepairTypeReason = p_web.RestoreValue('locWRepairTypeReason')
    do ValidateValue::locWRepairTypeReason
    If locWRepairTypeReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:WRepairType') = 1)
  ! --- TEXT --- locWRepairTypeReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWRepairTypeReason'',''viewjob_locwrepairtypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locWRepairTypeReason',p_web.GetSessionValue('locWRepairTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locWRepairTypeReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWRepairTypeReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWRepairTypeReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GSV('Hide:WRepairType') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:WRepairType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::textEstimateReady  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textEstimateReady  ! copies value to session value if valid.
  do Comment::textEstimateReady ! allows comment style to be updated.

ValidateValue::textEstimateReady  Routine
    If not (1=0)
    End

Value::textEstimateReady  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('textEstimateReady') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textEstimateReady" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Your estimate is now ready',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textEstimateReady  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textEstimateReady:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('textEstimateReady') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintEstimate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintEstimate  ! copies value to session value if valid.
          getStatus(520,0,'JOB',p_web)
  
          Access:JOBS.Clearkey(job:ref_Number_Key)
          job:ref_Number    = p_web.GSV('job:Ref_Number')
          if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  
  
          Access:WEBJOB.Clearkey(wob:refNumberKey)
          wob:refNumber    = p_web.GSV('job:Ref_Number')
          if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              access:WEBJOB.tryUpdate()
          else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  
  
          p_web.SSV('TotalPrice:Type','E')
          totalPrice(p_web)
  
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Notes','ESTIMATE VALUE: ' & Format(p_web.GSV('TotalPrice:Total'),@n14.2))
          p_web.SSV('AddToAudit:Action','ESTIMATE SENT')
          addToAudit(p_web)
  do Value::buttonPrintEstimate
  do Comment::buttonPrintEstimate ! allows comment style to be updated.
  do Value::job:Current_Status  !1

ValidateValue::buttonPrintEstimate  Routine
    If not (1=0)
    End

Value::buttonPrintEstimate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonPrintEstimate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintEstimate'',''viewjob_buttonprintestimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintEstimate','Print Estimate',p_web.combine(Choose('Print Estimate' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MainButton'),loc:formname,,,p_web.WindowOpen(clip('Estimate?var=' & RANDOM(1,9999999))&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintEstimate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintEstimate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewJob_' & p_web._nocolon('buttonPrintEstimate') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewJob_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 1)
    locEngineeringOption = p_web.GSV('locEngineeringOption')
    do ValidateValue::locEngineeringOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locEngineeringOption
      !do SendAlert
      do Comment::locEngineeringOption ! allows comment style to be updated.
      !exit
    End
    job:Order_Number = p_web.GSV('job:Order_Number')
    do ValidateValue::job:Order_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Order_Number
      !do SendAlert
      do Comment::job:Order_Number ! allows comment style to be updated.
      !exit
    End
    job:ESN = p_web.GSV('job:ESN')
    do ValidateValue::job:ESN
    If loc:Invalid
      loc:retrying = 1
      do Value::job:ESN
      !do SendAlert
      do Comment::job:ESN ! allows comment style to be updated.
      !exit
    End
    job:MSN = p_web.GSV('job:MSN')
    do ValidateValue::job:MSN
    If loc:Invalid
      loc:retrying = 1
      do Value::job:MSN
      !do SendAlert
      do Comment::job:MSN ! allows comment style to be updated.
      !exit
    End
    locJobType = p_web.GSV('locJobType')
    do ValidateValue::locJobType
    If loc:Invalid
      loc:retrying = 1
      do Value::locJobType
      !do SendAlert
      do Comment::locJobType ! allows comment style to be updated.
      !exit
    End
    job:Charge_Type = p_web.GSV('job:Charge_Type')
    do ValidateValue::job:Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Charge_Type
      !do SendAlert
      do Comment::job:Charge_Type ! allows comment style to be updated.
      !exit
    End
    job:Repair_Type = p_web.GSV('job:Repair_Type')
    do ValidateValue::job:Repair_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Repair_Type
      !do SendAlert
      do Comment::job:Repair_Type ! allows comment style to be updated.
      !exit
    End
    job:Warranty_Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Warranty_Charge_Type
      !do SendAlert
      do Comment::job:Warranty_Charge_Type ! allows comment style to be updated.
      !exit
    End
    job:Repair_Type_Warranty = p_web.GSV('job:Repair_Type_Warranty')
    do ValidateValue::job:Repair_Type_Warranty
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Repair_Type_Warranty
      !do SendAlert
      do Comment::job:Repair_Type_Warranty ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 2)
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 3)
    job:Current_Status = p_web.GSV('job:Current_Status')
    do ValidateValue::job:Current_Status
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Current_Status
      !do SendAlert
      do Comment::job:Current_Status ! allows comment style to be updated.
      !exit
    End
    job:Location = p_web.GSV('job:Location')
    do ValidateValue::job:Location
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Location
      !do SendAlert
      do Comment::job:Location ! allows comment style to be updated.
      !exit
    End
    job:Exchange_Status = p_web.GSV('job:Exchange_Status')
    do ValidateValue::job:Exchange_Status
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Exchange_Status
      !do SendAlert
      do Comment::job:Exchange_Status ! allows comment style to be updated.
      !exit
    End
    job:Loan_Status = p_web.GSV('job:Loan_Status')
    do ValidateValue::job:Loan_Status
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Loan_Status
      !do SendAlert
      do Comment::job:Loan_Status ! allows comment style to be updated.
      !exit
    End
    jbn:Fault_Description = p_web.GSV('jbn:Fault_Description')
    do ValidateValue::jbn:Fault_Description
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Fault_Description
      !do SendAlert
      do Comment::jbn:Fault_Description ! allows comment style to be updated.
      !exit
    End
    jbn:Engineers_Notes = p_web.GSV('jbn:Engineers_Notes')
    do ValidateValue::jbn:Engineers_Notes
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Engineers_Notes
      !do SendAlert
      do Comment::jbn:Engineers_Notes ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 4)
    locHOClaimStatus = p_web.GSV('locHOClaimStatus')
    do ValidateValue::locHOClaimStatus
    If loc:Invalid
      loc:retrying = 1
      do Value::locHOClaimStatus
      !do SendAlert
      do Comment::locHOClaimStatus ! allows comment style to be updated.
      !exit
    End
    locRRCClaimStatus = p_web.GSV('locRRCClaimStatus')
    do ValidateValue::locRRCClaimStatus
    If loc:Invalid
      loc:retrying = 1
      do Value::locRRCClaimStatus
      !do SendAlert
      do Comment::locRRCClaimStatus ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 5)
    locCurrentEngineer = p_web.GSV('locCurrentEngineer')
    do ValidateValue::locCurrentEngineer
    If loc:Invalid
      loc:retrying = 1
      do Value::locCurrentEngineer
      !do SendAlert
      do Comment::locCurrentEngineer ! allows comment style to be updated.
      !exit
    End
    jobe:Network = p_web.GSV('jobe:Network')
    do ValidateValue::jobe:Network
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:Network
      !do SendAlert
      do Comment::jobe:Network ! allows comment style to be updated.
      !exit
    End
    job:Authority_Number = p_web.GSV('job:Authority_Number')
    do ValidateValue::job:Authority_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Authority_Number
      !do SendAlert
      do Comment::job:Authority_Number ! allows comment style to be updated.
      !exit
    End
    job:Unit_Type = p_web.GSV('job:Unit_Type')
    do ValidateValue::job:Unit_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Unit_Type
      !do SendAlert
      do Comment::job:Unit_Type ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 6)
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 7)
    locMobileLifetimeValue = p_web.GSV('locMobileLifetimeValue')
    do ValidateValue::locMobileLifetimeValue
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileLifetimeValue
      !do SendAlert
      do Comment::locMobileLifetimeValue ! allows comment style to be updated.
      !exit
    End
    locMobileAverageSpend = p_web.GSV('locMobileAverageSpend')
    do ValidateValue::locMobileAverageSpend
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileAverageSpend
      !do SendAlert
      do Comment::locMobileAverageSpend ! allows comment style to be updated.
      !exit
    End
    locMobileLoyaltyStatus = p_web.GSV('locMobileLoyaltyStatus')
    do ValidateValue::locMobileLoyaltyStatus
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileLoyaltyStatus
      !do SendAlert
      do Comment::locMobileLoyaltyStatus ! allows comment style to be updated.
      !exit
    End
    locMobileUpgradeDate = p_web.GSV('locMobileUpgradeDate')
    do ValidateValue::locMobileUpgradeDate
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileUpgradeDate
      !do SendAlert
      do Comment::locMobileUpgradeDate ! allows comment style to be updated.
      !exit
    End
    locMobileIDNumber = p_web.GSV('locMobileIDNumber')
    do ValidateValue::locMobileIDNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileIDNumber
      !do SendAlert
      do Comment::locMobileIDNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 8)
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 9)
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 10)
    locCompleteRepair = p_web.GSV('locCompleteRepair')
    do ValidateValue::locCompleteRepair
    If loc:Invalid
      loc:retrying = 1
      do Value::locCompleteRepair
      !do SendAlert
      do Comment::locCompleteRepair ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 11)
    locCChargeTypeReason = p_web.GSV('locCChargeTypeReason')
    do ValidateValue::locCChargeTypeReason
    If loc:Invalid
      loc:retrying = 1
      do Value::locCChargeTypeReason
      !do SendAlert
      do Comment::locCChargeTypeReason ! allows comment style to be updated.
      !exit
    End
    locCRepairTypeReason = p_web.GSV('locCRepairTypeReason')
    do ValidateValue::locCRepairTypeReason
    If loc:Invalid
      loc:retrying = 1
      do Value::locCRepairTypeReason
      !do SendAlert
      do Comment::locCRepairTypeReason ! allows comment style to be updated.
      !exit
    End
    locWChargeTypeReason = p_web.GSV('locWChargeTypeReason')
    do ValidateValue::locWChargeTypeReason
    If loc:Invalid
      loc:retrying = 1
      do Value::locWChargeTypeReason
      !do SendAlert
      do Comment::locWChargeTypeReason ! allows comment style to be updated.
      !exit
    End
    locWRepairTypeReason = p_web.GSV('locWRepairTypeReason')
    do ValidateValue::locWRepairTypeReason
    If loc:Invalid
      loc:retrying = 1
      do Value::locWRepairTypeReason
      !do SendAlert
      do Comment::locWRepairTypeReason ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewJob_nexttab_' & 12)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ViewJob_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewJob_tab_' & 0)
    do GenerateTab0
  of lower('ViewJob_tab_' & 1)
    do GenerateTab1
  of lower('ViewJob_button:ChangeEngineeringOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:ChangeEngineeringOption
      of event:timer
        do Value::button:ChangeEngineeringOption
        do Comment::button:ChangeEngineeringOption
      else
        do Value::button:ChangeEngineeringOption
      end
  of lower('ViewJob_tab_' & 2)
    do GenerateTab2
  of lower('ViewJob_tab_' & 3)
    do GenerateTab3
  of lower('ViewJob_job:Current_Status_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Current_Status
      of event:timer
        do Value::job:Current_Status
        do Comment::job:Current_Status
      else
        do Value::job:Current_Status
      end
  of lower('ViewJob_jbn:Fault_Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Fault_Description
      of event:timer
        do Value::jbn:Fault_Description
        do Comment::jbn:Fault_Description
      else
        do Value::jbn:Fault_Description
      end
  of lower('ViewJob_jbn:Engineers_Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Engineers_Notes
      of event:timer
        do Value::jbn:Engineers_Notes
        do Comment::jbn:Engineers_Notes
      else
        do Value::jbn:Engineers_Notes
      end
  of lower('ViewJob_button:EngineersNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:EngineersNotes
      of event:timer
        do Value::button:EngineersNotes
        do Comment::button:EngineersNotes
      else
        do Value::button:EngineersNotes
      end
  of lower('ViewJob_tab_' & 4)
    do GenerateTab4
  of lower('ViewJob_tab_' & 5)
    do GenerateTab5
  of lower('ViewJob_jobe:Network_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Network
      of event:timer
        do Value::jobe:Network
        do Comment::jobe:Network
      else
        do Value::jobe:Network
      end
  of lower('ViewJob_job:Authority_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Authority_Number
      of event:timer
        do Value::job:Authority_Number
        do Comment::job:Authority_Number
      else
        do Value::job:Authority_Number
      end
  of lower('ViewJob_job:Unit_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Unit_Type
      of event:timer
        do Value::job:Unit_Type
        do Comment::job:Unit_Type
      else
        do Value::job:Unit_Type
      end
  of lower('ViewJob_tab_' & 6)
    do GenerateTab6
  of lower('ViewJob_tab_' & 7)
    do GenerateTab7
  of lower('ViewJob_tab_' & 8)
    do GenerateTab8
  of lower('ViewJob_buttonAllocateEngineer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAllocateEngineer
      of event:timer
        do Value::buttonAllocateEngineer
        do Comment::buttonAllocateEngineer
      else
        do Value::buttonAllocateEngineer
      end
  of lower('ViewJob_buttonEstimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonEstimate
      of event:timer
        do Value::buttonEstimate
        do Comment::buttonEstimate
      else
        do Value::buttonEstimate
      end
  of lower('ViewJob_tab_' & 9)
    do GenerateTab9
  of lower('ViewJob_buttonResendXML_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonResendXML
      of event:timer
        do Value::buttonResendXML
        do Comment::buttonResendXML
      else
        do Value::buttonResendXML
      end
  of lower('ViewJob_tab_' & 10)
    do GenerateTab10
  of lower('ViewJob_tab_' & 11)
    do GenerateTab11
  of lower('ViewJob_locCChargeTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCChargeTypeReason
      of event:timer
        do Value::locCChargeTypeReason
        do Comment::locCChargeTypeReason
      else
        do Value::locCChargeTypeReason
      end
  of lower('ViewJob_locCRepairTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCRepairTypeReason
      of event:timer
        do Value::locCRepairTypeReason
        do Comment::locCRepairTypeReason
      else
        do Value::locCRepairTypeReason
      end
  of lower('ViewJob_locWChargeTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWChargeTypeReason
      of event:timer
        do Value::locWChargeTypeReason
        do Comment::locWChargeTypeReason
      else
        do Value::locWChargeTypeReason
      end
  of lower('ViewJob_locWRepairTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWRepairTypeReason
      of event:timer
        do Value::locWRepairTypeReason
        do Comment::locWRepairTypeReason
      else
        do Value::locWRepairTypeReason
      end
  of lower('ViewJob_tab_' & 12)
    do GenerateTab12
  of lower('ViewJob_buttonPrintEstimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintEstimate
      of event:timer
        do Value::buttonPrintEstimate
        do Comment::buttonPrintEstimate
      else
        do Value::buttonPrintEstimate
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ViewJob_form:ready_',1)

  p_web.SetSessionValue('ViewJob_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ViewJob',0)
  Access:WEBJOB.PrimeRecord()
  Ans = Net:ChangeRecord
  p_web.SetSessionValue('ViewJob:Primed',1)
  wob:ReconciledMarker = 0
  p_web.SetSessionValue('wob:ReconciledMarker',wob:ReconciledMarker)
  wob:DateBooked = TODAY()
  p_web.SetSessionValue('wob:DateBooked',wob:DateBooked)
  wob:TimeBooked = Clock()
  p_web.SetSessionValue('wob:TimeBooked',wob:TimeBooked)
  wob:Completed = 'NO'
  p_web.SetSessionValue('wob:Completed',wob:Completed)
  wob:ReadyToDespatch = 0
  p_web.SetSessionValue('wob:ReadyToDespatch',wob:ReadyToDespatch)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewJob',0)
  Access:WEBJOB.PrimeRecord()
  Ans = Net:ChangeRecord
  p_web.SetSessionValue('ViewJob:Primed',1)
  p_web._PreCopyRecord(WEBJOB,wob:RecordNumberKey,Net:Web:Autonumbered)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ViewJob:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ViewJob:Primed',0)
  p_web.setsessionvalue('showtab_ViewJob',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(ACCSTAT,acs:StatusOnlyKey,acs:StatusOnlyKey,acs:Status,,p_web.GetSessionValue('job:Current_Status')) !2
  if loc:ok then p_web.FileToSessionQueue(ACCSTAT).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(NETWORKS,net:NetworkKey,net:NetworkKey,net:Network,,p_web.GetSessionValue('jobe:Network')) !2
  if loc:ok then p_web.FileToSessionQueue(NETWORKS).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(UNITTYPE,uni:Unit_Type_Key,uni:Unit_Type_Key,uni:Unit_Type,,p_web.GetSessionValue('job:Unit_Type')) !2
  if loc:ok then p_web.FileToSessionQueue(UNITTYPE).
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('textBouncer') <> ''
  End
      If not (p_web.GSV('locCurrentEngineer') = 'Not Allocated')
        If not (p_web.GSV('ReadOnly:CurrentStatus') = 1)
          If p_web.IfExistsValue('job:Current_Status')
            job:Current_Status = p_web.GetValue('job:Current_Status')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('jbn:Engineers_Notes')
            jbn:Engineers_Notes = p_web.GetValue('jbn:Engineers_Notes')
          End
        End
      End
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
  End
      If not (1=0)
          If p_web.IfExistsValue('jobe:Network')
            jobe:Network = p_web.GetValue('jobe:Network')
          End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('job:Authority_Number')
            job:Authority_Number = p_web.GetValue('job:Authority_Number')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('job:Unit_Type')
            job:Unit_Type = p_web.GetValue('job:Unit_Type')
          End
        End
      End
  If p_web.GSV('Hide:CustomerClassification') <> 1
  End
  If p_web.GSV('job:Date_Completed') = 0
  End
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
    If (p_web.GSV('Hide:CChargeType') <> 1)
      If not (p_web.GSV('Hide:CChargeType') = 1)
          If p_web.IfExistsValue('locCChargeTypeReason')
            locCChargeTypeReason = p_web.GetValue('locCChargeTypeReason')
          End
      End
    End
    If (p_web.GSV('Hide:CRepairType') <> 1)
      If not (p_web.GSV('Hide:CRepairType') = 1)
          If p_web.IfExistsValue('locCRepairTypeReason')
            locCRepairTypeReason = p_web.GetValue('locCRepairTypeReason')
          End
      End
    End
    If (p_web.GSV('Hide:WChargeType') <> 1)
      If not (p_web.GSV('Hide:WChargeType') = 1)
          If p_web.IfExistsValue('locWChargeTypeReason')
            locWChargeTypeReason = p_web.GetValue('locWChargeTypeReason')
          End
      End
    End
    If (p_web.GSV('Hide:WRepairType') <> 1)
      If not (p_web.GSV('Hide:WRepairType') = 1)
          If p_web.IfExistsValue('locWRepairTypeReason')
            locWRepairTypeReason = p_web.GetValue('locWRepairTypeReason')
          End
      End
    End
  End
  If p_web.GSV('Hide:EstimateReady') = 0
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  ! Validation
  if (p_web.GSV('job:Date_Completed') <> 0)
      p_web.SSV('CompulsoryFieldCheck:Type','C')
  else ! if (p_web.GSV('job:Date_Completed') <> 0)
      p_web.SSV('CompulsoryFieldCheck:Type','B')
  end !if (p_web.GSV('job:Date_Completed') <> 0)
  
  p_web.SSV('locCompleteRepair','')
  
  
    compulsoryFieldCheck(p_web)
  
  
  
  if (p_web.GSV('locCompleteRepair') <> '')
      p_web.SSV('locCompleteRepair','You cannot complete editing due to the following errors:<13,10>' & |
          p_web.GSV('locCompleteRepair'))
      loc:alert = 'You cannot complete editing. There are errors.'
      loc:invalid = 'locCompleteRepair'
      exit
  end ! if (p_web.GSV('locCompleteRepair') <> '')
  
  if (p_web.GSV('Hide:EstimateQuery') = 0 and p_web.GSV('locEstimateReadyOption') = 0)
      loc:alert = 'You selected Complete Repair and this job is an estimate. Choose an option'
      loc:invalid = 'locEstimateReadyOption'
      exit
  end ! if (p_web.GSV('Hide:EstimateQuery') = 0 and p_web.GSV('locEstimateReadyOption') = 0)
  
  p_web.SSV('Hide:CChargeType',1)
  p_web.SSV('Hide:WChargeType',1)
  p_web.SSV('Hide:CRepairType',1)
  p_web.SSV('Hide:WRepairType',1)
  
  
  If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
      if (p_web.GSV('locCChargeTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locCChargeTypeReason'
          p_web.SSV('Hide:CChargeType',0)
      end !if (p_web.SSV('locCChargeTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  If (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('save:WChargeType') and p_web.GSV('save:WChargeType') <> '')
      if (p_web.GSV('locWChargeTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locWChargeTypeReason'
          p_web.SSV('Hide:WChargeType',0)
      end ! if (p_web.SSV('locWChargeTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  If (p_web.GSV('job:Repair_Type') <> p_web.GSV('save:CRepairType') and p_web.GSV('save:CRepairType') <> '')
      if (p_web.GSV('locCRepairTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locCChargeTypeReason'
          p_web.SSV('Hide:CRepairType',0)
      end ! if (p_web.SSV('locWRepairTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  If (p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('save:WRepairType') and p_web.GSV('save:WRepairType') <> '')
      if (p_web.GSV('locWRepairTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locWChargeTypeReason'
          p_web.SSV('Hide:WRepairType',0)
      end ! if (p_web.SSV('locWRepairTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  
  if (loc:invalid) <> ''
      exit
  end ! if (loc:invalid) <> ''
  
  do CompleteForm
  do ValidateRecord
  ! Write Child Files
      Stop('Updating')
    IF (loc:Invalid <> '')
        EXIT
    END
  
      do CheckParts
      do wayBillCheck
  
      saveJob(p_web)
  
      do didAnythingChange
      do haveAccessoriesChanged
  
  
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
      UpdateDateTimeStamp(wob:RefNumber)
  ! End (DBH 16/09/2008) #10253
      do deleteVariables

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      stop('Validate All')
  p_web.DeleteSessionValue('ViewJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::link:AmendAddress
    If loc:Invalid then exit.
    do ValidateValue::link:AuditTrail
    If loc:Invalid then exit.
    do ValidateValue::link:EngineerHistory
    If loc:Invalid then exit.
    do ValidateValue::link:StatusChanges
    If loc:Invalid then exit.
    do ValidateValue::link:BrowseLocationHistory
    If loc:Invalid then exit.
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::text:jobNumber
    If loc:Invalid then exit.
    do ValidateValue::text:DateBooked
    If loc:Invalid then exit.
    do ValidateValue::locBookingOption
    If loc:Invalid then exit.
    do ValidateValue::locEngineeringOption
    If loc:Invalid then exit.
    do ValidateValue::gap:JobDetails
    If loc:Invalid then exit.
    do ValidateValue::button:ChangeEngineeringOption
    If loc:Invalid then exit.
    do ValidateValue::text:accountNumber
    If loc:Invalid then exit.
    do ValidateValue::text:Completed
    If loc:Invalid then exit.
    do ValidateValue::text:modelDetails
    If loc:Invalid then exit.
    do ValidateValue::text:HubRepair
    If loc:Invalid then exit.
    do ValidateValue::job:Order_Number
    If loc:Invalid then exit.
    do ValidateValue::text:OBFValidated
    If loc:Invalid then exit.
    do ValidateValue::job:ESN
    If loc:Invalid then exit.
    do ValidateValue::job:MSN
    If loc:Invalid then exit.
    do ValidateValue::locJobType
    If loc:Invalid then exit.
    do ValidateValue::button:AmendJobType
    If loc:Invalid then exit.
    do ValidateValue::job:Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Repair_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Repair_Type_Warranty
    If loc:Invalid then exit.
  ! tab = 7
  If p_web.GSV('textBouncer') <> ''
    loc:InvalidTab += 1
    do ValidateValue::textBouncer
    If loc:Invalid then exit.
    do ValidateValue::buttonPreviousUnitHistory
    If loc:Invalid then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::job:Current_Status
    If loc:Invalid then exit.
    do ValidateValue::job:Location
    If loc:Invalid then exit.
    do ValidateValue::text:ExchangeUnit
    If loc:Invalid then exit.
    do ValidateValue::text:LoanUnit
    If loc:Invalid then exit.
    do ValidateValue::job:Exchange_Status
    If loc:Invalid then exit.
    do ValidateValue::job:Loan_Status
    If loc:Invalid then exit.
    do ValidateValue::text:SecondExchangeUnit
    If loc:Invalid then exit.
    do ValidateValue::jbn:Fault_Description
    If loc:Invalid then exit.
    do ValidateValue::jbn:Engineers_Notes
    If loc:Invalid then exit.
    do ValidateValue::gap
    If loc:Invalid then exit.
    do ValidateValue::button:EngineersNotes
    If loc:Invalid then exit.
  ! tab = 14
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
    loc:InvalidTab += 1
    do ValidateValue::locHOClaimStatus
    If loc:Invalid then exit.
    do ValidateValue::locRRCClaimStatus
    If loc:Invalid then exit.
  End
  ! tab = 5
    loc:InvalidTab += 1
    do ValidateValue::textLiquidDamage
    If loc:Invalid then exit.
    do ValidateValue::locCurrentEngineer
    If loc:Invalid then exit.
    do ValidateValue::locEngineerAllocated
    If loc:Invalid then exit.
    do ValidateValue::jobe:Network
    If loc:Invalid then exit.
    do ValidateValue::job:Authority_Number
    If loc:Invalid then exit.
    do ValidateValue::job:Unit_Type
    If loc:Invalid then exit.
  ! tab = 6
    loc:InvalidTab += 1
    do ValidateValue::locRepairTypesNoParts
    If loc:Invalid then exit.
    do ValidateValue::ViewEstimateParts
    If loc:Invalid then exit.
    do ValidateValue::browse:ChargeableParts
    If loc:Invalid then exit.
    do ValidateValue::browseWarrantyParts
    If loc:Invalid then exit.
  ! tab = 15
  If p_web.GSV('Hide:CustomerClassification') <> 1
    loc:InvalidTab += 1
    do ValidateValue::locMobileLifetimeValue
    If loc:Invalid then exit.
    do ValidateValue::locMobileAverageSpend
    If loc:Invalid then exit.
    do ValidateValue::locMobileLoyaltyStatus
    If loc:Invalid then exit.
    do ValidateValue::locMobileUpgradeDate
    If loc:Invalid then exit.
    do ValidateValue::locMobileIDNumber
    If loc:Invalid then exit.
  End
  ! tab = 11
    loc:InvalidTab += 1
    do ValidateValue::buttonAllocateEngineer
    If loc:Invalid then exit.
    do ValidateValue::buttonAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonFaultCodes
    If loc:Invalid then exit.
    do ValidateValue::buttonEstimate
    If loc:Invalid then exit.
    do ValidateValue::buttonContactHistory
    If loc:Invalid then exit.
    do ValidateValue::buttonAllocateExchange
    If loc:Invalid then exit.
    do ValidateValue::buttonAllocateLoan
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateInvoice
    If loc:Invalid then exit.
    do ValidateValue::buttonViewCosts
    If loc:Invalid then exit.
  ! tab = 12
    loc:InvalidTab += 1
    do ValidateValue::buttonValidatePOP
    If loc:Invalid then exit.
    do ValidateValue::buttonResendXML
    If loc:Invalid then exit.
    do ValidateValue::buttonReceiptFromPUP
    If loc:Invalid then exit.
  ! tab = 8
  If p_web.GSV('job:Date_Completed') = 0
    loc:InvalidTab += 1
    do ValidateValue::buttonCompleteRepair
    If loc:Invalid then exit.
    do ValidateValue::locCompleteRepair
    If loc:Invalid then exit.
    do ValidateValue::textCompleteRepair
    If loc:Invalid then exit.
  End
  ! tab = 9
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
    loc:InvalidTab += 1
    do ValidateValue::locCChargeTypeReason
    If loc:Invalid then exit.
    do ValidateValue::locCRepairTypeReason
    If loc:Invalid then exit.
    do ValidateValue::locWChargeTypeReason
    If loc:Invalid then exit.
    do ValidateValue::locWRepairTypeReason
    If loc:Invalid then exit.
  End
  ! tab = 10
  If p_web.GSV('Hide:EstimateReady') = 0
    loc:InvalidTab += 1
    do ValidateValue::textEstimateReady
    If loc:Invalid then exit.
    do ValidateValue::buttonPrintEstimate
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('ViewJob:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('ViewJob:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Order_Number')
  p_web.StoreValue('')
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('locJobType')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Repair_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('job:Repair_Type_Warranty')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Current_Status')
  p_web.StoreValue('job:Location')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Exchange_Status')
  p_web.StoreValue('job:Loan_Status')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Fault_Description')
  p_web.StoreValue('jbn:Engineers_Notes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locHOClaimStatus')
  p_web.StoreValue('locRRCClaimStatus')
  p_web.StoreValue('')
  p_web.StoreValue('locCurrentEngineer')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:Network')
  p_web.StoreValue('job:Authority_Number')
  p_web.StoreValue('job:Unit_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locMobileLifetimeValue')
  p_web.StoreValue('locMobileAverageSpend')
  p_web.StoreValue('locMobileLoyaltyStatus')
  p_web.StoreValue('locMobileUpgradeDate')
  p_web.StoreValue('locMobileIDNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCompleteRepair')
  p_web.StoreValue('')
  p_web.StoreValue('locCChargeTypeReason')
  p_web.StoreValue('locCRepairTypeReason')
  p_web.StoreValue('locWChargeTypeReason')
  p_web.StoreValue('locWRepairTypeReason')
  p_web.StoreValue('')
  p_web.StoreValue('')


PostDelete      Routine
ShowAlert     Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
showHideCreateInvoice       PROCEDURE()
    CODE
        IF (p_web.gsv('job:Chargeable_Job') <> 'YES')
            RETURN TRUE
        END

        SentToHub(p_web)
        IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('SentToHub') <> 1)
            RETURN TRUE
        END

        IF (p_web.GSV('job:Bouncer') = 'X')
            RETURN TRUE
        END

        ! Job not completed
        IF (p_web.GSV('job:Date_Completed') = 0 AND p_web.GSV('job:Exchange_Unit_Number') = 0)
            RETURN TRUE
        END

        ! Job has not been priced
        IF (p_web.GSV('job:ignore_Chargeable_Charges') = 'YES')
            IF (p_web.GSV('BookingSite') = 'RRC')
                IF (p_web.GSV('jobe:RRCSubTotal') = 0)
                    RETURN TRUE
                END
            ELSE
                IF (p_web.GSV('job:Sub_Total') = 0)
                    RETURN TRUE
                END

            END
        END
        p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=ViewJob')
        p_web.SSV('URL:CreateInvoiceTarget','_self')
        p_web.SSV('URL:CreateInvoiceText','Create Invoice')

        IF (p_web.GSV('job:Invoice_Number') > 0)
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                IF (inv:Invoice_Type <> 'SIN')
                    RETURN TRUE
                END
                ! If job has been invoiced, just print the invoice, rather than asking to create one
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (inv:RRCInvoiceDate > 0)
                        p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                        p_web.SSV('URL:CreateInvoiceTarget','_blank')
                        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                    END
                ELSE
                    If (inv:ARCInvoiceDate > 0)
                        p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                        p_web.SSV('URL:CreateInvoiceTarget','_blank')
                        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                    END
                END
            END

        END


SetJobType           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locChargeableJob     STRING(3)                             !
locWarrantyJob       STRING(3)                             !
locCChargeType       STRING(30)                            !
locWChargeType       STRING(30)                            !
locCRepairType       STRING(30)                            !
locWRepairType       STRING(30)                            !
locConfirmationText  STRING(100)                           !
locErrorText         STRING(100)                           !
FilesOpened     Long
STOCKALL::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
REPTYDEF::State  USHORT
CHARTYPE::State  USHORT
locChargeableJob:IsInvalid  Long
locCChargeType:IsInvalid  Long
locCRepairType:IsInvalid  Long
locWarrantyJob:IsInvalid  Long
locWChargeType:IsInvalid  Long
locWRepairType:IsInvalid  Long
locErrorText:IsInvalid  Long
locConfirmationText:IsInvalid  Long
buttonConfirm:IsInvalid  Long
buttonSplitJob:IsInvalid  Long
buttonCancel:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
locCChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
locCRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
locWChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
locWRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
locClass                    CLASS
validateWarrantyParts           PROCEDURE(),BYTE
validateChargeableParts         PROCEDURE(),BYTE
                            END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('SetJobType')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'SetJobType_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SetJobType','')
    p_web.DivHeader('SetJobType',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('SetJobType') = 0
        p_web.AddPreCall('SetJobType')
        p_web.DivHeader('popup_SetJobType','nt-hidden')
        p_web.DivHeader('SetJobType',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_SetJobType_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_SetJobType_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_SetJobType',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_SetJobType',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SetJobType',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_SetJobType',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_SetJobType',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SetJobType',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SetJobType',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('SetJobType')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
deleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')
    p_web.DeleteSessionValue('ReadOnly:CRepairType')
    p_web.DeleteSessionValue('ReadOnly:CChargeType')
    p_web.DeleteSessionValue('ReadOnly:WRepairType')
    p_web.DeleteSessionValue('ReadOnly:WChargeType')
    p_web.DeleteSessionValue('locConfirmationText')
    p_web.DeleteSessionValue('locCChargeType')
    p_web.DeleteSessionValue('locChargeableJob')
    p_web.DeleteSessionValue('locCRepairType')
    p_web.DeleteSessionValue('locWarrantyJob')
    p_web.DeleteSessionValue('locWChargeType')
    p_web.DeleteSessionValue('locWRepairType')
enableFields        ROUTINE
    p_web.SSV('ReadOnly:ChargeableJOb',0)
    p_web.SSV('ReadOnly:WarrantyJob',0)
    p_web.SSV('ReadOnly:CRepairType',0)
    p_web.SSV('ReadOnly:CChargeType',0)
    p_web.SSV('ReadOnly:WRepairType',0)
    p_web.SSV('ReadOnly:WChargeType',0)
disableFields       ROUTINE
    p_web.SSV('ReadOnly:ChargeableJOb',1)
    p_web.SSV('ReadOnly:WarrantyJob',1)
    p_web.SSV('ReadOnly:CRepairType',1)
    p_web.SSV('ReadOnly:CChargeType',1)
    p_web.SSV('ReadOnly:WRepairType',1)
    p_web.SSV('ReadOnly:WChargeType',1)




loadJobData         ROUTINE
    p_web.SSV('locChargeableJob',p_web.GSV('job:Chargeable_Job'))
    p_web.SSV('locWarrantyJob',p_web.GSV('job:Warranty_Job'))
    p_web.SSV('locCChargeType',p_web.GSV('job:Charge_Type'))
    p_web.SSV('locCRepairType',p_web.GSV('job:Repair_Type'))
    p_web.SSV('locWChargeType',p_web.GSV('job:Warranty_Charge_Type'))
    p_web.SSV('locWRepairType',p_web.GSV('job:Repair_Type_Warranty'))
resetScreen         ROUTINE
    DO enablefields
    DO loadJobData

    p_web.SSV('Hide:ConfirmButton',1)
    p_web.SSV('Hide:CancelButton',1)
    p_web.SSV('Hide:SplitJobButton',1)
    p_web.SSV('locConfirmationText','')
    p_web.SSV('locErrorText','')

OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(CHARTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('SetJobType_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'SetJobType'
    end
    p_web.formsettings.proc = 'SetJobType'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locChargeableJob') = 0
    p_web.SetSessionValue('locChargeableJob',locChargeableJob)
  Else
    locChargeableJob = p_web.GetSessionValue('locChargeableJob')
  End
  if p_web.IfExistsValue('locCChargeType') = 0
    p_web.SetSessionValue('locCChargeType',locCChargeType)
  Else
    locCChargeType = p_web.GetSessionValue('locCChargeType')
  End
  if p_web.IfExistsValue('locCRepairType') = 0
    p_web.SetSessionValue('locCRepairType',locCRepairType)
  Else
    locCRepairType = p_web.GetSessionValue('locCRepairType')
  End
  if p_web.IfExistsValue('locWarrantyJob') = 0
    p_web.SetSessionValue('locWarrantyJob',locWarrantyJob)
  Else
    locWarrantyJob = p_web.GetSessionValue('locWarrantyJob')
  End
  if p_web.IfExistsValue('locWChargeType') = 0
    p_web.SetSessionValue('locWChargeType',locWChargeType)
  Else
    locWChargeType = p_web.GetSessionValue('locWChargeType')
  End
  if p_web.IfExistsValue('locWRepairType') = 0
    p_web.SetSessionValue('locWRepairType',locWRepairType)
  Else
    locWRepairType = p_web.GetSessionValue('locWRepairType')
  End
  if p_web.IfExistsValue('locErrorText') = 0
    p_web.SetSessionValue('locErrorText',locErrorText)
  Else
    locErrorText = p_web.GetSessionValue('locErrorText')
  End
  if p_web.IfExistsValue('locConfirmationText') = 0
    p_web.SetSessionValue('locConfirmationText',locConfirmationText)
  Else
    locConfirmationText = p_web.GetSessionValue('locConfirmationText')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locChargeableJob')
    locChargeableJob = p_web.GetValue('locChargeableJob')
    p_web.SetSessionValue('locChargeableJob',locChargeableJob)
  Else
    locChargeableJob = p_web.GetSessionValue('locChargeableJob')
  End
  if p_web.IfExistsValue('locCChargeType')
    locCChargeType = p_web.GetValue('locCChargeType')
    p_web.SetSessionValue('locCChargeType',locCChargeType)
  Else
    locCChargeType = p_web.GetSessionValue('locCChargeType')
  End
  if p_web.IfExistsValue('locCRepairType')
    locCRepairType = p_web.GetValue('locCRepairType')
    p_web.SetSessionValue('locCRepairType',locCRepairType)
  Else
    locCRepairType = p_web.GetSessionValue('locCRepairType')
  End
  if p_web.IfExistsValue('locWarrantyJob')
    locWarrantyJob = p_web.GetValue('locWarrantyJob')
    p_web.SetSessionValue('locWarrantyJob',locWarrantyJob)
  Else
    locWarrantyJob = p_web.GetSessionValue('locWarrantyJob')
  End
  if p_web.IfExistsValue('locWChargeType')
    locWChargeType = p_web.GetValue('locWChargeType')
    p_web.SetSessionValue('locWChargeType',locWChargeType)
  Else
    locWChargeType = p_web.GetSessionValue('locWChargeType')
  End
  if p_web.IfExistsValue('locWRepairType')
    locWRepairType = p_web.GetValue('locWRepairType')
    p_web.SetSessionValue('locWRepairType',locWRepairType)
  Else
    locWRepairType = p_web.GetSessionValue('locWRepairType')
  End
  if p_web.IfExistsValue('locErrorText')
    locErrorText = p_web.GetValue('locErrorText')
    p_web.SetSessionValue('locErrorText',locErrorText)
  Else
    locErrorText = p_web.GetSessionValue('locErrorText')
  End
  if p_web.IfExistsValue('locConfirmationText')
    locConfirmationText = p_web.GetValue('locConfirmationText')
    p_web.SetSessionValue('locConfirmationText',locConfirmationText)
  Else
    locConfirmationText = p_web.GetSessionValue('locConfirmationText')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('SetJobType_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BillingConfirmation?passedURL=ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SetJobType_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SetJobType_ChainTo')
    loc:formaction = p_web.GetSessionValue('SetJobType_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      ! Generate Form
      DO loadJobData
      p_web.SSV('Hide:ConfirmButton',1)
      p_web.SSV('Hide:CancelButton',1)
      p_web.SSV('Hide:SplitJobButton',1)
      DO enableFields
 locChargeableJob = p_web.RestoreValue('locChargeableJob')
 locCChargeType = p_web.RestoreValue('locCChargeType')
 locCRepairType = p_web.RestoreValue('locCRepairType')
 locWarrantyJob = p_web.RestoreValue('locWarrantyJob')
 locWChargeType = p_web.RestoreValue('locWChargeType')
 locWRepairType = p_web.RestoreValue('locWRepairType')
 locErrorText = p_web.RestoreValue('locErrorText')
 locConfirmationText = p_web.RestoreValue('locConfirmationText')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Set Job Type') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Set Job Type',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_SetJobType',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_SetJobType0_div')&'">'&p_web.Translate('Chargeable Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_SetJobType1_div')&'">'&p_web.Translate('Warranty Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_SetJobType2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="SetJobType_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="SetJobType_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SetJobType_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="SetJobType_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SetJobType_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locChargeableJob')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_SetJobType')>0,p_web.GSV('showtab_SetJobType'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_SetJobType'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_SetJobType') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_SetJobType'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_SetJobType')>0,p_web.GSV('showtab_SetJobType'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_SetJobType') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_SetJobType_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_SetJobType')>0,p_web.GSV('showtab_SetJobType'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"SetJobType",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_SetJobType')>0,p_web.GSV('showtab_SetJobType'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_SetJobType_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('SetJobType') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('SetJobType')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Chargeable Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_SetJobType0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Chargeable Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Chargeable Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Chargeable Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&130&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locChargeableJob
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locChargeableJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locChargeableJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&130&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCChargeType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&130&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Warranty Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_SetJobType1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Warranty Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Warranty Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Warranty Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&130&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWarrantyJob
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWarrantyJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWarrantyJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&130&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWChargeType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&130&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_SetJobType2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_SetJobType2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locErrorText
        do Comment::locErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locConfirmationText
        do Comment::locConfirmationText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonConfirm
        do Comment::buttonConfirm
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonSplitJob
        do Comment::buttonSplitJob
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonCancel
        do Comment::buttonCancel
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locChargeableJob  Routine
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Chargeable Job'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locChargeableJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locChargeableJob = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    locChargeableJob = p_web.GetValue('Value')
  End
  do ValidateValue::locChargeableJob  ! copies value to session value if valid.
      ! Chargeable Job
      DO disableFields
      p_web.SSV('Hide:ConfirmButton',0)
      p_web.SSV('Hide:CancelButton',0)
      p_web.SSV('Hide:SplitJobButton',1)
      p_web.SSV('JobTypeAction','')
  
  
      IF ( p_web.GSV('locChargeableJob') = 'YES')
          IF (p_web.GSV('locWarrantyJob') = 'YES')
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job a Split Warranty/Chargeable Job')
              p_web.SSV('Hide:SplitJobButton',0)
              p_web.SSV('JobTypeAction','CHATOSPLIT')
          ELSE
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job Chargeable')
              p_web.SSV('JobTypeAction','CHA')
          END
          p_web.SSV('ConfirmButtonText','Chargeable')
      ELSE
  
          CASE locClass.validateChargeableParts()
          OF 1
              DO resetScreen
              p_web.ssv('locErrorText','Cannot make job Warranty Only. There are chargeable parts attached.')
          OF 2
              DO resetScreen
              p_web.ssv('locErrorText','Cannot make job Warranty Only. This job is an estimate.')
          ELSE
  
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job Warranty Only')
              p_web.SSV('ConfirmButtonText','Warranty')
              p_web.SSV('JobTypeAction','WAR')
          END
  
      END
  do Value::locChargeableJob
  do SendAlert
  do Comment::locChargeableJob ! allows comment style to be updated.
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Comment::locCChargeType
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Comment::locCRepairType
  do Value::buttonConfirm  !1
  do Value::locConfirmationText  !1
  do Value::locWChargeType  !1
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1
  do Value::buttonCancel  !1
  do Value::buttonSplitJob  !1
  do Value::locErrorText  !1

ValidateValue::locChargeableJob  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locChargeableJob',locChargeableJob).
    End

Value::locChargeableJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    locChargeableJob = p_web.RestoreValue('locChargeableJob')
    do ValidateValue::locChargeableJob
    If locChargeableJob:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- locChargeableJob
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locChargeableJob'',''setjobtype_locchargeablejob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locChargeableJob')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ChargeableJob')= 1,'disabled','')
  If p_web.GetSessionValue('locChargeableJob') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locChargeableJob',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locChargeableJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locChargeableJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCChargeType  Routine
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_prompt',Choose(p_web.GSV('locChargeableJob') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locChargeableJob') <> 'YES','',p_web.Translate('Chargeable Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCChargeType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCChargeType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCChargeType = p_web.GetValue('Value')
  End
  do ValidateValue::locCChargeType  ! copies value to session value if valid.
  do Value::locCChargeType
  do SendAlert
  do Comment::locCChargeType ! allows comment style to be updated.
  do Value::locCRepairType  !1

ValidateValue::locCChargeType  Routine
    If not (p_web.GSV('locChargeableJob') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('locCChargeType',locCChargeType).
    End

Value::locCChargeType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locChargeableJob') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    locCChargeType = p_web.RestoreValue('locCChargeType')
    do ValidateValue::locCChargeType
    If locCChargeType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locChargeableJob') <> 'YES')
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCChargeType'',''setjobtype_loccchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locCChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locCChargeType') = 0
    p_web.SetSessionValue('locCChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locCChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locCChargeType_OptionView)
  locCChargeType_OptionView{prop:filter} = p_web.CleanFilter(locCChargeType_OptionView,'Upper(cha:Warranty) = ''NO''')
  locCChargeType_OptionView{prop:order} = p_web.CleanFilter(locCChargeType_OptionView,'UPPER(cha:Charge_Type)')
  Set(locCChargeType_OptionView)
  Loop
    Next(locCChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locCChargeType') = 0
      p_web.SetSessionValue('locCChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('locCChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locCChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCChargeType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCChargeType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locChargeableJob') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_prompt',Choose(p_web.GSV('locChargeableJob') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locChargeableJob') <> 'YES','',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::locCRepairType  ! copies value to session value if valid.
  do Value::locCRepairType
  do SendAlert
  do Comment::locCRepairType ! allows comment style to be updated.

ValidateValue::locCRepairType  Routine
    If not (p_web.GSV('locChargeableJob') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('locCRepairType',locCRepairType).
    End

Value::locCRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locChargeableJob') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    locCRepairType = p_web.RestoreValue('locCRepairType')
    do ValidateValue::locCRepairType
    If locCRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locChargeableJob') <> 'YES')
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCRepairType'',''setjobtype_loccrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locCRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locCRepairType') = 0
    p_web.SetSessionValue('locCRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locCRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locCRepairType_OptionView)
  locCRepairType_OptionView{prop:filter} = p_web.CleanFilter(locCRepairType_OptionView,'Upper(rtd:Chargeable) = ''YES'' AND Upper(rtd:Manufacturer) = Upper(''' & p_web.GSV('job:manufacturer') & ''') AND rtd:NotAvailable = 0')
  locCRepairType_OptionView{prop:order} = p_web.CleanFilter(locCRepairType_OptionView,'UPPER(rtd:Repair_Type)')
  Set(locCRepairType_OptionView)
  Loop
    Next(locCRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locCRepairType') = 0
      p_web.SetSessionValue('locCRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('locCRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locCRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locChargeableJob') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWarrantyJob  Routine
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Warranty Job'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWarrantyJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWarrantyJob = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    locWarrantyJob = p_web.GetValue('Value')
  End
  do ValidateValue::locWarrantyJob  ! copies value to session value if valid.
      ! Warranty Job
  DO disableFields
  p_web.SSV('JobTypeAction','')
  
  p_web.SSV('Hide:ConfirmButton',0)
  p_web.SSV('Hide:CancelButton',0)
  p_web.SSV('Hide:SplitJobButton',1)
  
  p_web.SSV('locErrorText','')
  
  IF ( p_web.GSV('locWarrantyJob') = 'YES')
      IF (p_web.GSV('locChargeableJob') = 'YES')
          p_web.SSV('locConfirmationText','Confirm that you wish to make this job a Split Warranty/Chargeable Job')
          p_web.SSV('Hide:SplitJobButton',0)
          p_web.SSV('JobTypeAction','WARTOSPLIT')
  
              ! Can't make warranty only.
          Case locClass.validateChargeableParts()
          of 1 ! Chargeable Parts
              p_web.SSV('Hide:ConfirmButton',1)
              p_web.SSV('locConfirmationText','Cannot make Warranty Only job as there are still Chargeable Parts attached. Please confirm if you wish to make this a split job.')
          of 2 ! Estimate
              p_web.SSV('Hide:ConfirmButton',1)
                          p_web.SSV('locConfirmationText','Cannot make Warranty Only job as it is an Estimate. Please confirm if you wish to make this a split job.')
          END
  
      ELSE
          p_web.SSV('locConfirmationText','Confirm that you wish to make this job Warranty')
          p_web.SSV('JobTypeAction','WAR')
      END
      p_web.SSV('ConfirmButtonText','Warranty')
  ELSE
      IF (locClass.validateWarrantyParts())
          p_web.SSV('locErrorText','Warning! There are still warranty parts attached. They will be transferred to Chargeable')
      END
  
      p_web.SSV('locConfirmationText','Confirm that you wish to make this job Chargeable Only')
      p_web.SSV('JobTypeAction','CHA')
      p_web.SSV('ConfirmButtonText','Chargeable')
  END
  do Value::locWarrantyJob
  do SendAlert
  do Comment::locWarrantyJob ! allows comment style to be updated.
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Comment::locWChargeType
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Comment::locWRepairType
  do Value::buttonConfirm  !1
  do Value::buttonCancel  !1
  do Value::locConfirmationText  !1
  do Value::locCChargeType  !1
  do Value::locCRepairType  !1
  do Value::locChargeableJob  !1
  do Value::buttonSplitJob  !1
  do Value::locErrorText  !1

ValidateValue::locWarrantyJob  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locWarrantyJob',locWarrantyJob).
    End

Value::locWarrantyJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    locWarrantyJob = p_web.RestoreValue('locWarrantyJob')
    do ValidateValue::locWarrantyJob
    If locWarrantyJob:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- locWarrantyJob
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWarrantyJob'',''setjobtype_locwarrantyjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyJob')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:WarrantyJob') = 1,'disabled','')
  If p_web.GetSessionValue('locWarrantyJob') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWarrantyJob',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWarrantyJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWarrantyJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWChargeType  Routine
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_prompt',Choose(p_web.GSV('locWarrantyJob') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locWarrantyJob') <> 'YES','',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWChargeType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWChargeType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWChargeType = p_web.GetValue('Value')
  End
  do ValidateValue::locWChargeType  ! copies value to session value if valid.
  do Value::locWChargeType
  do SendAlert
  do Comment::locWChargeType ! allows comment style to be updated.

ValidateValue::locWChargeType  Routine
    If not (p_web.GSV('locWarrantyJob') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('locWChargeType',locWChargeType).
    End

Value::locWChargeType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locWarrantyJob') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    locWChargeType = p_web.RestoreValue('locWChargeType')
    do ValidateValue::locWChargeType
    If locWChargeType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyJob') <> 'YES')
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWChargeType'',''setjobtype_locwchargetype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locWChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWChargeType') = 0
    p_web.SetSessionValue('locWChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locWChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWChargeType_OptionView)
  locWChargeType_OptionView{prop:filter} = p_web.CleanFilter(locWChargeType_OptionView,'Upper(cha:Warranty) = ''YES''')
  locWChargeType_OptionView{prop:order} = p_web.CleanFilter(locWChargeType_OptionView,'UPPER(cha:Charge_Type)')
  Set(locWChargeType_OptionView)
  Loop
    Next(locWChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWChargeType') = 0
      p_web.SetSessionValue('locWChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('locWChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWChargeType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWChargeType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locWarrantyJob') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_prompt',Choose(p_web.GSV('locWarrantyJob') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locWarrantyJob') <> 'YES','',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::locWRepairType  ! copies value to session value if valid.
  do Value::locWRepairType
  do SendAlert
  do Comment::locWRepairType ! allows comment style to be updated.

ValidateValue::locWRepairType  Routine
    If not (p_web.GSV('locWarrantyJob') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('locWRepairType',locWRepairType).
    End

Value::locWRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locWarrantyJob') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    locWRepairType = p_web.RestoreValue('locWRepairType')
    do ValidateValue::locWRepairType
    If locWRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyJob') <> 'YES')
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWRepairType'',''setjobtype_locwrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locWRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWRepairType') = 0
    p_web.SetSessionValue('locWRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locWRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWRepairType_OptionView)
  locWRepairType_OptionView{prop:filter} = p_web.CleanFilter(locWRepairType_OptionView,'Upper(rtd:Warranty) = ''YES'' AND Upper(rtd:Manufacturer) = Upper(''' & p_web.GSV('job:manufacturer') & ''') AND rtd:NotAvailable = 0')
  locWRepairType_OptionView{prop:order} = p_web.CleanFilter(locWRepairType_OptionView,'UPPER(rtd:Repair_Type)')
  Set(locWRepairType_OptionView)
  Loop
    Next(locWRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWRepairType') = 0
      p_web.SetSessionValue('locWRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('locWRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locWarrantyJob') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locErrorText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locErrorText = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locErrorText = p_web.GetValue('Value')
  End
  do ValidateValue::locErrorText  ! copies value to session value if valid.
  do Comment::locErrorText ! allows comment style to be updated.

ValidateValue::locErrorText  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locErrorText',locErrorText).
    End

Value::locErrorText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locErrorText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locErrorText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locErrorText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locErrorText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locErrorText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locConfirmationText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locConfirmationText = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locConfirmationText = p_web.GetValue('Value')
  End
  do ValidateValue::locConfirmationText  ! copies value to session value if valid.
  do Comment::locConfirmationText ! allows comment style to be updated.

ValidateValue::locConfirmationText  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locConfirmationText',locConfirmationText).
    End

Value::locConfirmationText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('locConfirmationText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locConfirmationText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locConfirmationText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locConfirmationText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locConfirmationText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('locConfirmationText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonConfirm  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonConfirm  ! copies value to session value if valid.
      ! Confirm change
  CASE p_web.GSV('JobTypeAction')
  OF 'CHA' ! Make Chargeable Job
      p_web.SSV('locChargeableJob','YES')
      p_web.SSV('locWarrantyJob','NO')
  OF 'WAR' ! Make Warranty Job
      p_web.SSV('locWarrantyJob','YES')
      p_web.SSV('locChargeableJob','NO')
  OF 'CHATOSPLIT' !Make Chargeable From Warranty
          ! Transfer Warranty Parts
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = p_web.GSV('job:Ref_Number')
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP
          IF (Access:WARPARTS.Next())
              BREAK
          END
          IF (wpr:Ref_Number <> p_web.GSV('job:ref_Number'))
              BREAK
          END
          IF (Access:PARTS.PrimeRecord() = Level:Benign)
              recordNo# = par:Record_Number
              par:Record :=: wpr:Record
              par:Record_Number = recordNo#
              IF (Access:PARTS.TryInsert())
                  Access:PARTS.CancelAutoInc()
              END
          END
              ! Is this part in stock allocation.
          Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
          stl:PartType = 'WAR'
          stl:PartRecordNumber = wpr:Record_Number
          IF (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
              stl:PartRecordNumber = par:Record_Number
              stl:parttype = 'CHA'
              Access:STOCK.TryUpdate()
          END
          Access:WARPARTS.DeleteRecord(0)
      END
  
      p_web.SSV('locWarrantyJob','NO')
      p_web.SSV('locChargeableJob','YES')
  
  OF 'WARTOSPLIT' !Make Warranty From Chargeable
          ! Transfer Chargeable Parts
      Access:PARTS.ClearKey(par:Part_Number_Key)
      par:Ref_Number = p_web.GSV('job:ref_number')
      SET(par:Part_Number_Key,par:Part_Number_Key)
      LOOP
          IF (Access:PARTS.Next())
              BREAK
          END
          IF (par:Ref_Number <> p_web.GSV('job:ref_number'))
              BREAK
          END
          IF (Access:WARPARTS.PrimeRecord() = Level:Benign)
              recordNo# = wpr:Record_Number
              wpr:Record :=: par:Record
              wpr:Record_Number = recordNo#
              IF (Access:WARPARTS.TryInsert())
                  Access:WARPARTS.CancelAutoInc()
              END
          END
              ! Is this part in stock allocation?
          Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
          stl:PartType = 'CHA'
          stl:PartRecordNumber = par:Record_Number
          IF (Access:STOCKALL.Tryfetch(stl:PartRecordTypeKey) = Level:Benign)
              stl:PartRecordNumber = wpr:Record_Number
              stl:PartType = 'WAR'
              Access:STOCKALL.TryUpdate()
          END
          Access:PARTS.DeleteRecord()
      END
      p_web.SSV('locChargeableJob','NO')
      p_web.SSV('locWarrantyJob','YES')
  
  END
  
  p_web.SSV('job:Chargeable_Job',p_web.GSV('locChargeableJob'))
  p_web.SSV('job:Warranty_Job',p_web.GSV('locWarrantyJob'))
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END!
  
  do resetScreen
  
  
  
  do Value::buttonConfirm
  do Comment::buttonConfirm ! allows comment style to be updated.
  do Value::buttonCancel  !1
  do Value::locConfirmationText  !1
  do Value::buttonCancel  !1
  do Value::buttonSplitJob  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1

ValidateValue::buttonConfirm  Routine
    If not (p_web.GSV('Hide:ConfirmButton') = 1)
    End

Value::buttonConfirm  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ConfirmButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('buttonConfirm') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ConfirmButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirm'',''setjobtype_buttonconfirm_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ConfirmChange',p_web.GSV('ConfirmButtonText'),p_web.combine(Choose(p_web.GSV('ConfirmButtonText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MainButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonConfirm  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonConfirm:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ConfirmButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('buttonConfirm') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ConfirmButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonSplitJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonSplitJob  ! copies value to session value if valid.
      ! Split Job
      ! Confirm change
  p_web.SSV('locChargeableJob','YES')
  p_web.SSV('locWarrantyJob','YES')
  p_web.SSV('job:chargeable_job','YES')
  p_web.SSV('job:warranty_Job','YES')
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END!
  
  do resetScreen
  do Value::buttonSplitJob
  do Comment::buttonSplitJob ! allows comment style to be updated.
  do Value::buttonConfirm  !1
  do Value::buttonCancel  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1

ValidateValue::buttonSplitJob  Routine
    If not (p_web.GSV('Hide:SplitJobButton') = 1)
    End

Value::buttonSplitJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:SplitJobButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJobButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonSplitJob'',''setjobtype_buttonsplitjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','SplitJob','Split Job',p_web.combine(Choose('Split Job' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MainButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonSplitJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonSplitJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:SplitJobButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:SplitJobButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCancel  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCancel  ! copies value to session value if valid.
      ! Cancel Change
      DO resetScreen
  do Value::buttonCancel
  do Comment::buttonCancel ! allows comment style to be updated.
  do Value::buttonConfirm  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Prompt::locChargeableJob
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1
  do Value::buttonSplitJob  !1

ValidateValue::buttonCancel  Routine
    If not (p_web.GSV('Hide:CancelButton') = 1)
    End

Value::buttonCancel  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CancelButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SetJobType_' & p_web._nocolon('buttonCancel') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CancelButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancel'',''setjobtype_buttoncancel_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Cancel','Cancel',p_web.combine(Choose('Cancel' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MainButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCancel  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCancel:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:CancelButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SetJobType_' & p_web._nocolon('buttonCancel') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CancelButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('SetJobType_nexttab_' & 0)
    locChargeableJob = p_web.GSV('locChargeableJob')
    do ValidateValue::locChargeableJob
    If loc:Invalid
      loc:retrying = 1
      do Value::locChargeableJob
      !do SendAlert
      do Comment::locChargeableJob ! allows comment style to be updated.
      !exit
    End
    locCChargeType = p_web.GSV('locCChargeType')
    do ValidateValue::locCChargeType
    If loc:Invalid
      loc:retrying = 1
      do Value::locCChargeType
      !do SendAlert
      do Comment::locCChargeType ! allows comment style to be updated.
      !exit
    End
    locCRepairType = p_web.GSV('locCRepairType')
    do ValidateValue::locCRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::locCRepairType
      !do SendAlert
      do Comment::locCRepairType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('SetJobType_nexttab_' & 1)
    locWarrantyJob = p_web.GSV('locWarrantyJob')
    do ValidateValue::locWarrantyJob
    If loc:Invalid
      loc:retrying = 1
      do Value::locWarrantyJob
      !do SendAlert
      do Comment::locWarrantyJob ! allows comment style to be updated.
      !exit
    End
    locWChargeType = p_web.GSV('locWChargeType')
    do ValidateValue::locWChargeType
    If loc:Invalid
      loc:retrying = 1
      do Value::locWChargeType
      !do SendAlert
      do Comment::locWChargeType ! allows comment style to be updated.
      !exit
    End
    locWRepairType = p_web.GSV('locWRepairType')
    do ValidateValue::locWRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::locWRepairType
      !do SendAlert
      do Comment::locWRepairType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('SetJobType_nexttab_' & 2)
    locErrorText = p_web.GSV('locErrorText')
    do ValidateValue::locErrorText
    If loc:Invalid
      loc:retrying = 1
      do Value::locErrorText
      !do SendAlert
      do Comment::locErrorText ! allows comment style to be updated.
      !exit
    End
    locConfirmationText = p_web.GSV('locConfirmationText')
    do ValidateValue::locConfirmationText
    If loc:Invalid
      loc:retrying = 1
      do Value::locConfirmationText
      !do SendAlert
      do Comment::locConfirmationText ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_SetJobType_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('SetJobType_tab_' & 0)
    do GenerateTab0
  of lower('SetJobType_locChargeableJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locChargeableJob
      of event:timer
        do Value::locChargeableJob
        do Comment::locChargeableJob
      else
        do Value::locChargeableJob
      end
  of lower('SetJobType_locCChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCChargeType
      of event:timer
        do Value::locCChargeType
        do Comment::locCChargeType
      else
        do Value::locCChargeType
      end
  of lower('SetJobType_locCRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCRepairType
      of event:timer
        do Value::locCRepairType
        do Comment::locCRepairType
      else
        do Value::locCRepairType
      end
  of lower('SetJobType_tab_' & 1)
    do GenerateTab1
  of lower('SetJobType_locWarrantyJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyJob
      of event:timer
        do Value::locWarrantyJob
        do Comment::locWarrantyJob
      else
        do Value::locWarrantyJob
      end
  of lower('SetJobType_locWChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWChargeType
      of event:timer
        do Value::locWChargeType
        do Comment::locWChargeType
      else
        do Value::locWChargeType
      end
  of lower('SetJobType_locWRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWRepairType
      of event:timer
        do Value::locWRepairType
        do Comment::locWRepairType
      else
        do Value::locWRepairType
      end
  of lower('SetJobType_tab_' & 2)
    do GenerateTab2
  of lower('SetJobType_buttonConfirm_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirm
      of event:timer
        do Value::buttonConfirm
        do Comment::buttonConfirm
      else
        do Value::buttonConfirm
      end
  of lower('SetJobType_buttonSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonSplitJob
      of event:timer
        do Value::buttonSplitJob
        do Comment::buttonSplitJob
      else
        do Value::buttonSplitJob
      end
  of lower('SetJobType_buttonCancel_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancel
      of event:timer
        do Value::buttonCancel
        do Comment::buttonCancel
      else
        do Value::buttonCancel
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('SetJobType_form:ready_',1)

  p_web.SetSessionValue('SetJobType_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_SetJobType',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SetJobType',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('SetJobType:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('SetJobType:Primed',0)
  p_web.setsessionvalue('showtab_SetJobType',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ChargeableJob')= 1)
          If p_web.IfExistsValue('locChargeableJob') = 0
            p_web.SetValue('locChargeableJob','NO')
            locChargeableJob = 'NO'
          Else
            locChargeableJob = p_web.GetValue('locChargeableJob')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:WarrantyJob') = 1)
          If p_web.IfExistsValue('locWarrantyJob') = 0
            p_web.SetValue('locWarrantyJob','NO')
            locWarrantyJob = 'NO'
          Else
            locWarrantyJob = p_web.GetValue('locWarrantyJob')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SetJobType_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      IF (p_web.GSV('job:Warranty_Job') <> 'YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Repair_Type_Warranty','')
      END
      IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
          p_web.SSV('job:Charge_Type','')
          p_web.SSV('job:Repair_Type','')
      END
  
      DO deleteSessionValues
  p_web.DeleteSessionValue('SetJobType_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locChargeableJob
    If loc:Invalid then exit.
    do ValidateValue::locCChargeType
    If loc:Invalid then exit.
    do ValidateValue::locCRepairType
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::locWarrantyJob
    If loc:Invalid then exit.
    do ValidateValue::locWChargeType
    If loc:Invalid then exit.
    do ValidateValue::locWRepairType
    If loc:Invalid then exit.
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::locErrorText
    If loc:Invalid then exit.
    do ValidateValue::locConfirmationText
    If loc:Invalid then exit.
    do ValidateValue::buttonConfirm
    If loc:Invalid then exit.
    do ValidateValue::buttonSplitJob
    If loc:Invalid then exit.
    do ValidateValue::buttonCancel
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('SetJobType:Primed',0)
  p_web.StoreValue('locChargeableJob')
  p_web.StoreValue('locCChargeType')
  p_web.StoreValue('locCRepairType')
  p_web.StoreValue('locWarrantyJob')
  p_web.StoreValue('locWChargeType')
  p_web.StoreValue('locWRepairType')
  p_web.StoreValue('locErrorText')
  p_web.StoreValue('locConfirmationText')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

locClass.validateWarrantyParts      PROCEDURE()
    CODE

        found# = 0
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = p_web.GSV('job:ref_Number')
        SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
        LOOP
            IF (Access:WARPARTS.Next())
                BREAK
            END
            IF (wpr:Ref_Number <> p_web.GSV('job:ref_number'))
                BREAK
            END
            found# = 1
            BREAK
        END

        return found#

locClass.validateChargeableParts    PROCEDURE()
    CODE
        found# = 0
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = p_web.gsv('job:Ref_number')
        SET(par:Part_Number_Key,par:Part_Number_Key)
        LOOP
            IF (Access:PARTS.Next())
                BREAK
            END
            IF (par:Ref_Number <> p_web.gsv('job:ref_Number'))
                BREAK
            END
            found# = 1
            BREAK
        END

        IF (p_web.gsv('job:estimate') = 'YES')
            found# = 2
        END

        RETURN found#

SelectAccessJobStatus PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
acs:Status:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(ACCSTAT)
                      Project(acs:RecordNumber)
                      Project(acs:Status)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('SelectAccessJobStatus')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectAccessJobStatus:NoForm')
      loc:NoForm = p_web.GetValue('SelectAccessJobStatus:NoForm')
      loc:FormName = p_web.GetValue('SelectAccessJobStatus:FormName')
    else
      loc:FormName = 'SelectAccessJobStatus_frm'
    End
    p_web.SSV('SelectAccessJobStatus:NoForm',loc:NoForm)
    p_web.SSV('SelectAccessJobStatus:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectAccessJobStatus:NoForm')
    loc:FormName = p_web.GSV('SelectAccessJobStatus:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectAccessJobStatus') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectAccessJobStatus')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'SelectAccessJobStatus' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','SelectAccessJobStatus')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('SelectAccessJobStatus') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('SelectAccessJobStatus')
      p_web.DivHeader('popup_SelectAccessJobStatus','nt-hidden')
      p_web.DivHeader('SelectAccessJobStatus',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_SelectAccessJobStatus_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_SelectAccessJobStatus_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('SelectAccessJobStatus',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ACCSTAT,acs:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'ACS:STATUS') then p_web.SetValue('SelectAccessJobStatus_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectAccessJobStatus:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectAccessJobStatus:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectAccessJobStatus:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectAccessJobStatus:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'SelectAccessJobStatus'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('SelectAccessJobStatus')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectAccessJobStatus_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectAccessJobStatus_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(acs:Status)','-UPPER(acs:Status)')
    Loc:LocateField = 'acs:Status'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(acs:AccessArea),+UPPER(acs:Status)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('acs:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('SelectAccessJobStatus_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('SelectAccessJobStatus:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectAccessJobStatus:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectAccessJobStatus:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectAccessJobStatus:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ACCSTAT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="acs:RecordNumberKey"></input><13,10>'
  end
  if p_web.Translate('Select Status') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('Select Status',0)&'</div>'&CRLF
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{SelectAccessJobStatus.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2SelectAccessJobStatus','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('SelectAccessJobStatus_LocatorPic'),,,'onchange="SelectAccessJobStatus.locate(''Locator2SelectAccessJobStatus'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2SelectAccessJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="SelectAccessJobStatus.locate(''Locator2SelectAccessJobStatus'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="SelectAccessJobStatus_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectAccessJobStatus.cl(''SelectAccessJobStatus'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SelectAccessJobStatus_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('SelectAccessJobStatus_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="SelectAccessJobStatus_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('SelectAccessJobStatus_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="SelectAccessJobStatus_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','SelectAccessJobStatus',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','SelectAccessJobStatus',p_web.Translate('Status'),'Click here to sort by Status',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('acs:recordnumber',lower(loc:vorder),1,1) = 0 !and ACCSTAT{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','acs:RecordNumber',clip(loc:vorder) & ',' & 'acs:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('acs:RecordNumber'),p_web.GetValue('acs:RecordNumber'),p_web.GetSessionValue('acs:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(acs:AccessArea) = Upper(''' & p_web.GSV('filter:AccessLevel') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectAccessJobStatus_Filter')
    p_web.SetSessionValue('SelectAccessJobStatus_FirstValue','')
    p_web.SetSessionValue('SelectAccessJobStatus_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ACCSTAT,acs:RecordNumberKey,loc:PageRows,'SelectAccessJobStatus',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ACCSTAT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ACCSTAT,loc:firstvalue)
              Reset(ThisView,ACCSTAT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ACCSTAT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ACCSTAT,loc:lastvalue)
            Reset(ThisView,ACCSTAT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acs:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="SelectAccessJobStatus_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectAccessJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectAccessJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectAccessJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectAccessJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'SelectAccessJobStatus_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'SelectAccessJobStatus',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectAccessJobStatus_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectAccessJobStatus_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1SelectAccessJobStatus','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('SelectAccessJobStatus_LocatorPic'),,,'onchange="SelectAccessJobStatus.locate(''Locator1SelectAccessJobStatus'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1SelectAccessJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="SelectAccessJobStatus.locate(''Locator1SelectAccessJobStatus'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="SelectAccessJobStatus_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectAccessJobStatus.cl(''SelectAccessJobStatus'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'SelectAccessJobStatus_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('SelectAccessJobStatus_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectAccessJobStatus_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="SelectAccessJobStatus_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectAccessJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectAccessJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectAccessJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectAccessJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'SelectAccessJobStatus_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'SelectAccessJobStatus',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('SelectAccessJobStatus','ACCSTAT',acs:RecordNumberKey) !acs:RecordNumber
    p_web._thisrow = p_web._nocolon('acs:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and acs:RecordNumber = p_web.GetValue('acs:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectAccessJobStatus:LookupField')) = acs:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((acs:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('SelectAccessJobStatus','ACCSTAT',acs:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ACCSTAT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ACCSTAT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ACCSTAT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ACCSTAT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::acs:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('SelectAccessJobStatus','ACCSTAT',acs:RecordNumberKey)
  TableQueue.Id[1] = acs:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiSelectAccessJobStatus;if (btiSelectAccessJobStatus != 1){{var SelectAccessJobStatus=new browseTable(''SelectAccessJobStatus'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('acs:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'SelectAccessJobStatus.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'SelectAccessJobStatus.applyGreenBar();btiSelectAccessJobStatus=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectAccessJobStatus')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectAccessJobStatus')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectAccessJobStatus')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectAccessJobStatus')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ACCSTAT)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ACCSTAT)
  Bind(acs:Record)
  Clear(acs:Record)
  NetWebSetSessionPics(p_web,ACCSTAT)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('acs:RecordNumber',p_web.GetValue('acs:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  acs:RecordNumber = p_web.GSV('acs:RecordNumber')
  loc:result = p_web._GetFile(ACCSTAT,acs:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acs:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(ACCSTAT)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(ACCSTAT)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('SelectAccessJobStatus_Select_'&acs:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',p_web.AddBrowseValue('SelectAccessJobStatus','ACCSTAT',acs:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::acs:Status   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('SelectAccessJobStatus_acs:Status_'&acs:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(acs:Status,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('acs:RecordNumber',acs:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
