

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE022.INC'),ONCE        !Local module procedure declarations
                     END


virtualSite          PROCEDURE  (fLocation)                ! Declare Procedure
LOCATION_ALIAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles

    returnValue# = 0
    Access:LOCATION_ALIAS.ClearKey(loc_ali:Location_Key)
    loc_ali:Location = fLocation
    If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
        !Found
        If loc_ali:VirtualSite
            returnValue# = 1
        End !If loc_ali:VirtualSite
    Else!If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign

    do restoreFiles
    do closeFiles

    return returnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION_ALIAS.Open                               ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION_ALIAS.UseFile                            ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION_ALIAS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  LOCATION_ALIAS::State = Access:LOCATION_ALIAS.SaveFile() ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF LOCATION_ALIAS::State <> 0
    Access:LOCATION_ALIAS.RestoreFile(LOCATION_ALIAS::State) ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
validFreeTextPart    PROCEDURE  (String fType, String fUserCode, String fManufacturer, String fModelNumber,String fPartNumber) ! Declare Procedure
USERS::State  USHORT
LOCATION::State  USHORT
STOCK::State  USHORT
STOMODEL::State  USHORT
FilesOpened     BYTE(0)
  CODE
! Return
! 0 - VALID
! 1 - Invalid User
! 2 - Inactive Site
! 3 - Suspended Stock Item
! 4 - Invalid Stock Item
! 5 - Restricted User

    do openFiles
    do saveFiles

    returnValue# = 0
    Access:USERS.Clearkey(use:user_code_key)
    use:user_code    = fUserCode
    if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Found
    else ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Error
        returnValue# = 1
    end ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)

    if (returnValue# = 0)
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = use:Location
        if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Found
            if (loc:Active = 0)
                returnValue# = 2
            end ! if (loc:Active = 0)
        else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Error
        end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
    end ! if (returnValue# = 0)

    if (returnValue# = 0)
        Access:STOCK.Clearkey(sto:Location_Manufacturer_Key)
        sto:Location    = use:Location
        sto:Manufacturer    = fManufacturer
        sto:Part_Number    = fPartNumber
        if (Access:STOCK.TryFetch(sto:Location_Manufacturer_Key) = Level:Benign)
            ! Found
            Access:STOMODEL.Clearkey(stm:Mode_Number_Only_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Model_Number    = fModelNumber
            if (Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign)
                ! Found
                if (sto:Suspend)
                   returnValue# = 3
                else ! if (sto:Suspend)
                    if (fType = 'C')
                        if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                            returnValue# = 5
                        else ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)

                        end ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                    end !if (fType = 'C')

                    if (fType = 'W')
                        if (use:RestrictParts And use:RestrictWarranty And sto:SkillLevel > use:SkillLevel)
                            returnValue# = 5
                        else ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)

                        end ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                    end !if (fType = 'C')
                end ! if (sto:Suspend)
            else ! if (Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign)
                ! Error
                returnValue# = 4
            end ! if (Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign)
        else ! if (Access:STOCK.TryFetch(sto:Location_Manufacture_Key) = Level:Benign)
            ! Error
            returnValue# = 4
        end ! if (Access:STOCK.TryFetch(sto:Location_Manufacture_Key) = Level:Benign)
    end ! if (returnValue# = 0)

    do restoreFiles
    do closeFiles


    return returnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:LOCATION.Close
     Access:STOCK.Close
     Access:STOMODEL.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  USERS::State = Access:USERS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  LOCATION::State = Access:LOCATION.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  STOCK::State = Access:STOCK.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  STOMODEL::State = Access:STOMODEL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF USERS::State <> 0
    Access:USERS.RestoreFile(USERS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF LOCATION::State <> 0
    Access:LOCATION.RestoreFile(LOCATION::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STOCK::State <> 0
    Access:STOCK.RestoreFile(STOCK::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STOMODEL::State <> 0
    Access:STOMODEL.RestoreFile(STOMODEL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
InWarrantyMarkup     PROCEDURE  (func:Manufacturer,func:SiteLocation) ! Declare Procedure
returnValue          LONG                                  !
MANUFACT::State  USHORT
MANMARK::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do OpenFiles
    do SaveFiles

    returnValue = 0

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
        Access:MANMARK.Clearkey(mak:SiteLocationKey)
        mak:RefNumber   = man:RecordNumber
        mak:SiteLocation    = func:SiteLocation
        If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Found
            returnValue = mak:InWarrantyMarkup
        Else ! If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Error
        End !If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

    do RestoreFiles
    do CloseFiles

    return returnValue
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANMARK.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANMARK.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANMARK.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANMARK::State = Access:MANMARK.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANMARK::State <> 0
    Access:MANMARK.RestoreFile(MANMARK::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
MessageAlert         PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MessageAlert -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('MessageAlert')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript"><13,10>' & |
        'alert("' & p_web.GSV('Message:Text') & '")<13,10>' & |
        'document.write(window.location.href = "' & p_web.GSV('Message:URL') & '")<13,10>' & |
        '</script>'
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
ClearJobVariables    PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ClearJobVariables -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('ClearJobVariables')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
    ! Clear Insert Job Variables
    p_web.DeleteSessionValue('tmp:ESN')
    p_web.DeleteSessionValue('tmp:TransitType')
    p_web.DeleteSessionValue('tmp:Manufacturer')
    p_web.DeleteSessionValue('tmp:ModelNumber')
    If p_web.gsv('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:ARC = 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:RRC = 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'
    p_web.SSV('tmp:TransitType',GETINI(p_web.GetSessionValue('BookingAccount'), 'InitialTransitType',, CLIP(PATH()) & '\SB2KDEF.INI'))
    p_web.SSV('Comment:TransitType','Required')
    p_web.DeleteSessionValue('Comment:IMEINumber')
    p_web.SSV('Comment:DOP','dd/mm/yyyy')

    p_web.DeleteSessionValue('Filter:Manufacturer')
    p_web.DeleteSessionValue('Filter:ModelNumber')

    p_web.SSV('Hide:IMEINumberButton',1)

    p_web.DeleteSessionValue('OBFValidation')
    p_web.DeleteSessionValue('Prompt:Validation')
    p_web.DeleteSessionValue('Error:Validation')
    p_web.DeleteSessionValue('Passed:Validation')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:OldDOP')
    p_web.SSV('Hide:ChangeDOP',1)
    p_web.SSV('Hide:ChangeDOPButton',1)
    p_web.DeleteSessionValue('tmp:NewDOP')
    p_web.DeleteSessionValue('tmp:ChangeReason')


    p_web.DeleteSessionValue('tmp:DateOfPurchase')

    p_web.DeleteSessionValue('Save:DOP')
    p_web.DeleteSessionValue('Required:DOP')
    p_web.DeleteSessionValue('ReadOnly:DOP')
    p_web.SSV('ReadOnly:Manufacturer',1)
    p_web.SSV('ReadOnly:ModelNumber',1)
    p_web.DeleteSessionValue('ReadOnly:TransitType')
    p_web.DeleteSessionValue('ReadOnly:IMEINumber')
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')

    p_web.DeleteSessionValue('tmp:Network')
    p_web.DeleteSessionValue('tmp:ReturnDate')
    p_web.DeleteSessionValue('tmp:DateOfPurchase')
    p_web.DeleteSessionValue('tmp:BOXIMEINumber')
    p_web.DeleteSessionValue('tmp:BranchOfReturn')
    p_web.DeleteSessionValue('tmp:LAccountNumber')
    p_web.DeleteSessionValue('tmp:OriginalAccessories')
    p_web.DeleteSessionValue('tmp:OriginalDealer')
    p_web.DeleteSessionValue('tmp:OriginalManuals')
    p_web.DeleteSessionValue('tmp:OriginalPackaging')
    p_web.DeleteSessionValue('tmp:PhysicalDamage')
    p_web.DeleteSessionValue('tmp:ProofOfPurchase')
    p_web.DeleteSessionValue('tmp:Replacement')
    p_web.DeleteSessionValue('tmp:ReplacementIMEINumber')
    p_web.DeleteSessionValue('StoreReferenceNumber')
    p_web.DeleteSessionValue('tmp:TalkTime')
    p_web.SSV('Hide:PreviousAddress',1)
    p_web.DeleteSessionValue('tmp:StoreReferenceNumber')

    p_web.SSV('ReadyForNewJobBooking',1)
    p_web.SSV('FirstTime',1)


    p_web.DeleteSessionValue('Required:Engineer')

    p_web.DeleteSessionValue('tmp:ExternalDamageCheckList')
    p_web.DeleteSessionValue('tmp:AmendFaultCodes')

    p_web.DeleteSessionValue('tmp:AmendAddressess')

    p_web.DeleteSessionValue('FranchiseAccount')


    p_web.DeleteSessionValue('tmp:AmendAccessories')
    p_web.DeleteSessionValue('tmp:ShowAccessory')
    p_web.DeleteSessionValue('tmp:TheAccessory')
    p_web.DeleteSessionValue('tmp:TheJobAccessory')
    p_web.DeleteSessionValue('tmp:AmendAddresses')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:POPType')
    p_web.DeleteSessionValue('tmp:PreviousAddress')
    p_web.DeleteSessionValue('tmp:UsePreviousAddress')
    p_web.DeleteSessionValue('AmendFaultCodes')
    p_web.DeleteSessionValue('tmp:FaultCode1')
    p_web.DeleteSessionValue('tmp:FaultCode2')
    p_web.DeleteSessionValue('tmp:FaultCode3')
    p_web.DeleteSessionValue('tmp:FaultCode4')
    p_web.DeleteSessionValue('tmp:FaultCode5')
    p_web.DeleteSessionValue('tmp:FaultCode6')
    p_web.DeleteSessionValue('tmp:ExternalDamageChecklist')
    p_web.DeleteSessionValue('filter:WarrantyChargeTYpe')
    p_web.DeleteSessionValue('Comment:POP')
    p_web.SSV('Drop:JobType','-------------------------------------')
    p_web.DeleteSessionValue('Drop:JobTypeDefault')


    ! --- Clear passed mq variables ----
    ! Inserting:  DBH 30/01/2009 #N/A
    p_web.DeleteSessionValue('mq:ChargeableJob')
    p_web.DeleteSessionValue('mq:Charge_Type')
    p_web.DeleteSessionValue('mq:WarrantyJob')
    p_web.DeleteSessionValue('mq:Warranty_Charge_Type')
    p_web.DeleteSessionValue('mq:DOP')
    p_web.DeleteSessionValue('mq:POP')
    p_web.DeleteSessionValue('mq:IMEIValidation')
    p_web.DeleteSessionValue('mq:IMEIValidationText')
    ! End: DBH 30/01/2009 #N/A
    ! -----------------------------------------

!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
