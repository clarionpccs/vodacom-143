

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE055.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE059.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE060.INC'),ONCE        !Req'd for module callout resolution
                     END


frmExchangeReceiveProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locIMEINumber        STRING(30)                            !
locScannedIMEINumber STRING(1000)                          !
qIMEI                QUEUE,PRE(qi)                         !
SessionID            LONG                                  !
IMEINumber           STRING(30)                            !
                     END                                   !
FilesOpened     Long
LOANHIST::State  USHORT
LOAN::State  USHORT
EXCHHIST::State  USHORT
EXCHANGE::State  USHORT
TagFile::State  USHORT
RETSTOCK::State  USHORT
STOCKRECEIVETMP::State  USHORT
SBO_OutParts::State  USHORT
txtError:IsInvalid  Long
locIMEINumber:IsInvalid  Long
locScannedIMEINumber:IsInvalid  Long
txtFinishedMessage:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
locScannedIMEINumber_OptionView   View(SBO_OutParts)
                          Project(sout:PartNumber)
                          Project(sout:PartNumber)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmExchangeReceiveProcess')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmExchangeReceiveProcess_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmExchangeReceiveProcess','')
    p_web.DivHeader('frmExchangeReceiveProcess',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmExchangeReceiveProcess') = 0
        p_web.AddPreCall('frmExchangeReceiveProcess')
        p_web.DivHeader('popup_frmExchangeReceiveProcess','nt-hidden')
        p_web.DivHeader('frmExchangeReceiveProcess',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmExchangeReceiveProcess_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmExchangeReceiveProcess_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmExchangeReceiveProcess')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(RETSTOCK)
  p_web._OpenFile(STOCKRECEIVETMP)
  p_web._OpenFile(SBO_OutParts)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(SBO_OutParts)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmExchangeReceiveProcess_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmExchangeReceiveProcess'
    end
    p_web.formsettings.proc = 'frmExchangeReceiveProcess'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Error') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Error') = 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locIMEINumber') = 0
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locScannedIMEINumber') = 0
    p_web.SetSessionValue('locScannedIMEINumber',locScannedIMEINumber)
  Else
    locScannedIMEINumber = p_web.GetSessionValue('locScannedIMEINumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locScannedIMEINumber')
    locScannedIMEINumber = p_web.GetValue('locScannedIMEINumber')
    p_web.SetSessionValue('locScannedIMEINumber',locScannedIMEINumber)
  Else
    locScannedIMEINumber = p_web.GetSessionValue('locScannedIMEINumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmExchangeReceiveProcess_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmExchangeReceiveProcess')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmExchangeReceiveProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmExchangeReceiveProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmExchangeReceiveProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('Error',0)
  p_web.SSV('locIMEINumber','')
  p_web.SSV('Comment:IMEINumber','')
  p_web.SSV('txtFinishedMessage','')
  
  IF (p_web.IfExistsValue('fLoan'))
      p_web.StoreValue('fLoan') ! Receive Loan Units
  END
  
  
  ClearSBOPartsList(p_web)
  IF (CheckIfTagged(p_web) = 0)
      p_web.SSV('Error',1)
  ELSE
      found# = 0
  
      ! Add items to be scanned to temp file (DBH: 29/11/2011)
  
      Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
      res:Ref_Number = p_web.GSV('ret:Ref_Number')
      res:Despatched = 'YES'
      SET(res:DespatchedPartKey,res:DespatchedPartKey)
      LOOP UNTIL Access:RETSTOCK.Next()
          IF (res:Ref_Number <> p_web.GSV('ret:Ref_Number') OR |
              res:Despatched <> 'YES')
              BREAK
          END
          IF (res:Received = 1)
              CYCLE
          END
  
          Access:TagFile.ClearKey(tag:keyTagged)
          tag:sessionID = p_web.SessionID
          SET(tag:keyTagged,tag:keyTagged)
          LOOP UNTIL Access:TagFile.Next()
              IF (tag:sessionID <> p_web.SessionID)
                  BREAK
              END
              IF (tag:tagged = 1)
                  Access:STOCKRECEIVETMP.Clearkey(stotmp:RecordNumberKey)
                  stotmp:RecordNumber = tag:taggedValue
                  IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
                      IF (stotmp:PartNumber = res:Part_Number)
                          IF (p_web.GSV('fLoan') = 1)
                              Access:LOAN.ClearKey(loa:Ref_Number_Key)
                              loa:Ref_Number = res:LoanRefNumber
                              IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                                  IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                      sout:SessionID = p_web.SessionID
                                      sout:PartNumber = loa:ESN
                                      sout:Description = loa:Ref_Number
                                      sout:LineType = ''
                                      IF (Access:SBO_OutParts.TryInsert())
                                          Access:SBO_OutParts.CancelAutoInc()
                                      END
                                      found# = 1
                                  END
                              END
  
                          ELSE
  
                              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                              xch:Ref_Number = res:ExchangeRefNumber
                              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                                  IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                      sout:SessionID = p_web.SessionID
                                      sout:PartNumber = xch:ESN
                                      sout:Description = xch:Ref_Number
                                      sout:LineType = ''
                                      IF (Access:SBO_OutParts.TryInsert())
                                          Access:SBO_OutParts.CancelAutoInc()
                                      END
                                      found# = 1
                                  END
                              END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                          END
  
                      END ! IF (stotmp:PartNumber = res:Part_Number)
                  END !IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
              END
          END ! Loop i# = 1 TO RECORDS(glo:Queue)
      END
  
  END
  
  IF (found# = 0)
      p_web.SSV('Error',1)
  END
  
      p_web.site.SaveButton.TextValue = 'OK'
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locScannedIMEINumber = p_web.RestoreValue('locScannedIMEINumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmExchangeReceiveProcess',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('Error') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmExchangeReceiveProcess0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      End
      If p_web.GSV('Error') = 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmExchangeReceiveProcess1_div')&'">'&p_web.Translate('Scanned IMEIs To Receive')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmExchangeReceiveProcess_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmExchangeReceiveProcess_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmExchangeReceiveProcess_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmExchangeReceiveProcess_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmExchangeReceiveProcess_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Error') = 1
    ElsIf p_web.GSV('Error') = 0
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmExchangeReceiveProcess')>0,p_web.GSV('showtab_frmExchangeReceiveProcess'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmExchangeReceiveProcess'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmExchangeReceiveProcess') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmExchangeReceiveProcess'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmExchangeReceiveProcess')>0,p_web.GSV('showtab_frmExchangeReceiveProcess'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmExchangeReceiveProcess') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('Error') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      End
      If p_web.GSV('Error') = 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Scanned IMEIs To Receive') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmExchangeReceiveProcess_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmExchangeReceiveProcess')>0,p_web.GSV('showtab_frmExchangeReceiveProcess'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmExchangeReceiveProcess",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmExchangeReceiveProcess')>0,p_web.GSV('showtab_frmExchangeReceiveProcess'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmExchangeReceiveProcess_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmExchangeReceiveProcess') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmExchangeReceiveProcess')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('Error') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtError
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtError
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('Error') = 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Scanned IMEIs To Receive')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Scanned IMEIs To Receive')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Scanned IMEIs To Receive')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Scanned IMEIs To Receive')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmExchangeReceiveProcess1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locScannedIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locScannedIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locScannedIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtFinishedMessage
        do Comment::txtFinishedMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Validate::txtError  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtError  ! copies value to session value if valid.
  do Comment::txtError ! allows comment style to be updated.

ValidateValue::txtError  Routine
    If not (1=0)
    End

Value::txtError  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('txtError') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtError" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate('You have not tagged any items that can be received',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtError  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtError:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('txtError') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('IMEI Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::locIMEINumber  ! copies value to session value if valid.
  p_web.SSV('Comment:IMEINumber','')
  Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
  sout:SessionID = p_web.SessionID
  sout:LineType = 'P' ! Already processed
  sout:PartNumber = p_web.GSV('locIMEINumber')
  IF (Access:SBO_OutParts.TryFetch(sout:PartNumberKey) = Level:Benign)
      p_web.SSV('Comment:IMEINumber','The selected IMEI Number has already been processed')
  ELSE
      Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
      sout:SessionID = p_web.SessionID
      sout:LineType = '' ! Already processed
      sout:PartNumber = p_web.GSV('locIMEINumber')
      IF (Access:SBO_OutParts.TryFetch(sout:PartNumberKey))
          p_web.SSV('Comment:IMEINumber','The selected IMEI Number has not been tagged')
      ELSE
          IF (p_web.GSV('fLoan') = 1)
              Access:LOAN.Clearkey(loa:Ref_Number_Key)
              loa:Ref_Number  = sout:Description
              IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                  loa:Available = 'AVL'
                  loa:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                  IF (Access:LOAN.TryUpdate() = Level:Benign)
                      Access:RETSTOCK.Clearkey(res:LoanRefNumberKey)
                      res:Ref_Number = p_web.GSV('ret:Ref_Number')
                      res:ExchangeRefNumber = loa:Ref_Number
                      IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                          res:Received = 1
                          res:DateReceived = TODAY()
                          IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
  
                          END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
                      END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                      IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                          loh:Ref_Number     = xch:Ref_Number
                          loh:Date        = TODAY()
                          loh:Time        = CLOCK()
                          Access:USERS.Clearkey(use:Password_Key)
                          use:Password = glo:Password
                          IF (Access:USERS.TryFetch(use:Password_Key))
                          END
                          loh:User        = use:User_Code
                          loh:Status        = 'UNIT RECEIVED'
                          loh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                              '<13,10>RETAIL INVOICE NUMBER: ' & p_web.GSV('ret:Invoice_Number') & |
                              '<13,10>PRICE: ' & Format(p_web.GSV('res:Item_Cost'),@n14.2)
  
                          IF (Access:LOANHIST.TryInsert())
                              Access:LOANHIST.CancelAutoInc()
                          END
                      END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                      sout:LineType = 'P' ! Processed
                      Access:SBO_OutParts.TryUpdate()
                      p_web.SSV('locIMEINumber','')
                  END ! IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
              END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ELSE
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number  = sout:Description
              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                  xch:Available = 'AVL'
                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                  IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                      Access:RETSTOCK.Clearkey(res:ExchangeRefNumberKey)
                      res:Ref_Number = p_web.GSV('ret:Ref_Number')
                      res:ExchangeRefNumber = xch:Ref_Number
                      IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                          res:Received = 1
                          res:DateReceived = TODAY()
                          IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
  
                          END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
                      END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                      IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                          exh:Ref_Number     = xch:Ref_Number
                          exh:Date        = TODAY()
                          exh:Time        = CLOCK()
                          Access:USERS.Clearkey(use:Password_Key)
                          use:Password = glo:Password
                          IF (Access:USERS.TryFetch(use:Password_Key))
                          END
                          exh:User        = use:User_Code
                          exh:Status        = 'UNIT RECEIVED'
                          exh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                              '<13,10>RETAIL INVOICE NUMBER: ' & p_web.GSV('ret:Invoice_Number') & |
                              '<13,10>PRICE: ' & Format(p_web.GSV('res:Item_Cost'),@n14.2)
  
                          IF (Access:EXCHHIST.TryInsert())
                              Access:EXCHHIST.CancelAutoInc()
                          END
                      END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                      sout:LineType = 'P' ! Processed
                      Access:SBO_OutParts.TryUpdate()
                      p_web.SSV('locIMEINumber','')
                  END ! IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
              END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          END
  
      END
  END
  
  ! Any IMEI's left?
  found# = 0
  Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
  sout:SessionID = p_web.SessionID
  sout:LineType = ''
  SET(sout:PartNumberKey,sout:PartNumberKey)
  LOOP UNTIL Access:SBO_OutParts.Next()
      IF (sout:SessionID <> p_web.SessionID OR |
          sout:LineType <> '')
          BREAK
      END
      found# = 1
      BREAK
  END
  IF (found# = 0)
      p_web.SSV('txtFinishedMessage','All I.M.E.I. Numbers Have Been Processed')
  END
  
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber ! allows comment style to be updated.
  do Value::locScannedIMEINumber  !1
  do Comment::locIMEINumber
  do Value::txtFinishedMessage  !1

ValidateValue::locIMEINumber  Routine
    If not (1=0)
    locIMEINumber = Upper(locIMEINumber)
      if loc:invalid = '' then p_web.SetSessionValue('locIMEINumber',locIMEINumber).
    End

Value::locIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locIMEINumber = p_web.RestoreValue('locIMEINumber')
    do ValidateValue::locIMEINumber
    If locIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''frmexchangereceiveprocess_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:IMEINumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locScannedIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Processed IMEI Number(s)'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locScannedIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locScannedIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locScannedIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::locScannedIMEINumber  ! copies value to session value if valid.
  do Comment::locScannedIMEINumber ! allows comment style to be updated.

ValidateValue::locScannedIMEINumber  Routine
    If not (1=0)
    locScannedIMEINumber = Upper(locScannedIMEINumber)
      if loc:invalid = '' then p_web.SetSessionValue('locScannedIMEINumber',locScannedIMEINumber).
    End

Value::locScannedIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    locScannedIMEINumber = p_web.RestoreValue('locScannedIMEINumber')
    do ValidateValue::locScannedIMEINumber
    If locScannedIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locScannedIMEINumber',loc:fieldclass,loc:readonly,10,174,0,loc:javascript,)
  loc:fieldclass = ''
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(RETSTOCK)
  bind(res:Record)
  p_web._OpenFile(STOCKRECEIVETMP)
  bind(stotmp:Record)
  p_web._OpenFile(SBO_OutParts)
  bind(sout:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locScannedIMEINumber_OptionView)
  locScannedIMEINumber_OptionView{prop:filter} = p_web.CleanFilter(locScannedIMEINumber_OptionView,'sout:SessionID = ' & p_web.SessionID & ' AND UPPER(sout:LineType) = ''P''')
  locScannedIMEINumber_OptionView{prop:order} = p_web.CleanFilter(locScannedIMEINumber_OptionView,'UPPER(-sout:RecordNumber)')
  Set(locScannedIMEINumber_OptionView)
  Loop
    Next(locScannedIMEINumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locScannedIMEINumber') = 0
      p_web.SetSessionValue('locScannedIMEINumber',sout:PartNumber)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(sout:PartNumber,sout:PartNumber,choose(sout:PartNumber = p_web.getsessionvalue('locScannedIMEINumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locScannedIMEINumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(SBO_OutParts)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locScannedIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locScannedIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtFinishedMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtFinishedMessage  ! copies value to session value if valid.
  do Comment::txtFinishedMessage ! allows comment style to be updated.

ValidateValue::txtFinishedMessage  Routine
    If not (1=0)
    End

Value::txtFinishedMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('txtFinishedMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtFinishedMessage" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtFinishedMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtFinishedMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtFinishedMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('txtFinishedMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmExchangeReceiveProcess_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('frmExchangeReceiveProcess_nexttab_' & 1)
    locIMEINumber = p_web.GSV('locIMEINumber')
    do ValidateValue::locIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locIMEINumber
      !do SendAlert
      do Comment::locIMEINumber ! allows comment style to be updated.
      !exit
    End
    locScannedIMEINumber = p_web.GSV('locScannedIMEINumber')
    do ValidateValue::locScannedIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locScannedIMEINumber
      !do SendAlert
      do Comment::locScannedIMEINumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmExchangeReceiveProcess_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmExchangeReceiveProcess_tab_' & 0)
    do GenerateTab0
  of lower('frmExchangeReceiveProcess_tab_' & 1)
    do GenerateTab1
  of lower('frmExchangeReceiveProcess_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      of event:timer
        do Value::locIMEINumber
        do Comment::locIMEINumber
      else
        do Value::locIMEINumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)

  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmExchangeReceiveProcess:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmExchangeReceiveProcess:Primed',0)
  p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('Error') = 1
  End
  If p_web.GSV('Error') = 0
      If not (1=0)
          If p_web.IfExistsValue('locIMEINumber')
            locIMEINumber = p_web.GetValue('locIMEINumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locScannedIMEINumber')
            locScannedIMEINumber = p_web.GetValue('locScannedIMEINumber')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmExchangeReceiveProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ClearStocKReceiveList(p_web)
  ClearTagFile(p_web)
  BuildStockReceiveList(p_web,p_web.GSV('ret:Ref_Number'),p_web.GSV('ret:ExchangeOrder'),p_web.GSV('ret:LoanOrder'))
  p_web.DeleteSessionValue('frmExchangeReceiveProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Error') = 1
    loc:InvalidTab += 1
    do ValidateValue::txtError
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('Error') = 0
    loc:InvalidTab += 1
    do ValidateValue::locIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::locScannedIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::txtFinishedMessage
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmExchangeReceiveProcess:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locScannedIMEINumber')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locIMEINumber',locIMEINumber) ! STRING(30)
     p_web.SSV('locScannedIMEINumber',locScannedIMEINumber) ! STRING(1000)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locIMEINumber = p_web.GSV('locIMEINumber') ! STRING(30)
     locScannedIMEINumber = p_web.GSV('locScannedIMEINumber') ! STRING(1000)
frmPrintGRN          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locGRN               DATE                                  !
FilesOpened     Long
GRNOTESR::State  USHORT
RETSTOCK::State  USHORT
RETSALES::State  USHORT
locGRN:IsInvalid  Long
Comment:locGRN:IsInvalid  Long
btnCreateGRN:IsInvalid  Long
btnPrintGRN:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('frmPrintGRN')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmPrintGRN_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmPrintGRN','')
    p_web.DivHeader('frmPrintGRN',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmPrintGRN') = 0
        p_web.AddPreCall('frmPrintGRN')
        p_web.DivHeader('popup_frmPrintGRN','nt-hidden')
        p_web.DivHeader('frmPrintGRN',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmPrintGRN_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmPrintGRN_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmPrintGRN',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmPrintGRN',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmPrintGRN',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintGRN',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmPrintGRN',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintGRN',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintGRN',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmPrintGRN',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmPrintGRN')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
CreateGRN           ROUTINE
    p_web.SSV('Comment:locGRN','')
    p_web.SSV('Hide:PrintGRN',1)
    p_web.SSV('locGRNNumber','')
    IF (p_web.GSV('locInvoiceNumber') > 0)
        Goods_Received# = 0                                         ! Check if any parts have been received on this date
        Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
        ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
        If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
            !Found
            Access:RETSTOCK.ClearKey(res:Part_Number_Key)
            res:Ref_Number  = ret:Ref_Number
            Set(res:Part_Number_Key,res:Part_Number_Key)
            Loop
                If Access:RETSTOCK.NEXT()
                    Break
                End !If
                If res:Ref_Number  <> ret:Ref_Number      |
                    Then Break.  ! End If
                If ~res:Received
                    Cycle
                End !If ~ret:Received
                If res:DateReceived = p_web.GSV('locGRN')
                    Goods_Received# = 1
                    break
                End !If ret:DateReceived = tmp:Goods_Received_Date
            End !Loop
        Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End        !If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign


        if Goods_Received# = 1
            If Access:GRNOTESR.PrimeRecord() = Level:Benign
                grr:Order_Number = p_web.GSV('locInvoiceNumber')
                grr:Goods_Received_date = p_web.GSV('locGRN')
                If Access:GRNOTESR.TryInsert() = Level:Benign
                    !Insert Successful
                    Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
                    ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
                    If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign

                        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                        res:Ref_Number  = ret:Ref_Number
                        Set(res:Part_Number_Key,res:Part_Number_Key)
                        Loop
                            If Access:RETSTOCK.NEXT()
                                Break
                            End !If
                            If res:Ref_Number  <> ret:Ref_Number      |
                                Then Break.  ! End If
                            If ~res:Received
                                Cycle
                            End !If ~ret:Received
                            If res:DateReceived = p_web.GSV('locGRN')
                                If res:GRNNumber = 0
                                    res:GRNNumber = grr:Goods_Received_Number
                                    Access:RETSTOCK.Update()
                                End !If ret:GRNNumber = 0
                            End !If ret:DateReceived = tmp:Goods_Received_Date
                        End !Loop

!                            GLO:Select1 = grr:Goods_Received_Number
!                            GLO:Select2 = tmp:InvoiceNumber
!                            GLO:Select3 = tmp:Goods_Received_Date
!                            Goods_Received_Note_Retail
!                            GLO:Select1 = ''
!                            GLO:Select2 = ''
!                            GLO:Select3 = ''
                        p_web.SSV('Comment:locGRN','GRN Created For Selected Date.')
                        p_web.SSV('Hide:PrintGRN',0)
                        p_web.SSV('locGRNNumber',grr:Goods_Received_Number)
                    End !If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                Else !If Access:GRNOTESR.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:GRNOTESR.CancelAutoInc()
                End !If Access:GRNOTESR.TryInsert() = Level:Benign
            End !If Access:GRNOTESR.PrimeRecord() = Level:Benign
        Else !if Goods_Received# = 1
            p_web.SSV('Comment:locGRN','No items were received on the selected date.')
        End !if Goods_Received# = 1

    End !if tmp:InvoiceNumber <> 0
OpenFiles  ROUTINE
  p_web._OpenFile(GRNOTESR)
  p_web._OpenFile(RETSTOCK)
  p_web._OpenFile(RETSALES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(GRNOTESR)
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(RETSALES)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('locGRNNumber','')
  p_web.SSV('Hide:PrintGRN',1)
  p_web.SSV('Comment:locGRN','')
  p_web.SSV('locGRN',TODAY())
  p_web.SetValue('frmPrintGRN_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmPrintGRN'
    end
    p_web.formsettings.proc = 'frmPrintGRN'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locGRN')
    p_web.SetPicture('locGRN',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('locGRN',p_web.site.DatePicture)

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locGRN') = 0
    p_web.SetSessionValue('locGRN',locGRN)
  Else
    locGRN = p_web.GetSessionValue('locGRN')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locGRN')
    locGRN = p_web.dformat(clip(p_web.GetValue('locGRN')),p_web.site.DatePicture)
    p_web.SetSessionValue('locGRN',locGRN)
  Else
    locGRN = p_web.GetSessionValue('locGRN')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmPrintGRN_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmPrintGRN')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmPrintGRN_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmPrintGRN_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmPrintGRN_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'OK'
 locGRN = p_web.RestoreValue('locGRN')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Create GRN') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Create GRN',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmPrintGRN',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmPrintGRN0_div')&'">'&p_web.Translate('Enter GRN Date')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmPrintGRN_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmPrintGRN_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPrintGRN_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmPrintGRN_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPrintGRN_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locGRN')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmPrintGRN')>0,p_web.GSV('showtab_frmPrintGRN'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmPrintGRN'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPrintGRN') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmPrintGRN'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmPrintGRN')>0,p_web.GSV('showtab_frmPrintGRN'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPrintGRN') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter GRN Date') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmPrintGRN_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmPrintGRN')>0,p_web.GSV('showtab_frmPrintGRN'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmPrintGRN",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmPrintGRN')>0,p_web.GSV('showtab_frmPrintGRN'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmPrintGRN_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmPrintGRN') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmPrintGRN')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Enter GRN Date')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Enter GRN Date')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Enter GRN Date')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Enter GRN Date')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locGRN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Comment:locGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::Comment:locGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnCreateGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnCreateGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnPrintGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnPrintGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locGRN  Routine
  packet = clip(packet) & p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('locGRN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('GRN Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locGRN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    locGRN = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do ValidateValue::locGRN  ! copies value to session value if valid.
  do Value::locGRN
  do SendAlert
  do Comment::locGRN ! allows comment style to be updated.

ValidateValue::locGRN  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locGRN',locGRN).
    End

Value::locGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('locGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('Hide:PrintGRN') = 0 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locGRN = p_web.RestoreValue('locGRN')
    do ValidateValue::locGRN
    If locGRN:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DATE --- locGRN
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locGRN'',''frmprintgrn_locgrn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Hide:PrintGRN') = 0,'1','')
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('locGRN',p_web.GetSessionValue('locGRN'),loc:fieldclass,loc:readonly,,,loc:javascript,loc:options,loc:extra,,15,,,,0)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._DateFormat()
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('locGRN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::Comment:locGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Comment:locGRN  ! copies value to session value if valid.
  do Comment::Comment:locGRN ! allows comment style to be updated.

ValidateValue::Comment:locGRN  Routine
    If not (1=0)
    End

Value::Comment:locGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('Comment:locGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="Comment:locGRN" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('Comment:locGRN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::Comment:locGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if Comment:locGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('Comment:locGRN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnCreateGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnCreateGRN  ! copies value to session value if valid.
  DO CreateGRN
  do Value::btnCreateGRN
  do Comment::btnCreateGRN ! allows comment style to be updated.
  do Value::btnPrintGRN  !1
  do Value::locGRN  !1
  do Comment::locGRN
  do Value::Comment:locGRN  !1

ValidateValue::btnCreateGRN  Routine
    If not (p_web.GSV('Hide:PrintGRN') = 0)
    End

Value::btnCreateGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintGRN') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('btnCreateGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintGRN') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCreateGRN'',''frmprintgrn_btncreategrn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnCreateGRN','Create GRN',p_web.combine(Choose('Create GRN' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnCreateGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnCreateGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintGRN') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('btnCreateGRN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintGRN') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnPrintGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnPrintGRN  ! copies value to session value if valid.
  do Comment::btnPrintGRN ! allows comment style to be updated.

ValidateValue::btnPrintGRN  Routine
    If not (p_web.GSV('Hide:PrintGRN') = 1)
    End

Value::btnPrintGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintGRN') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('btnPrintGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintGRN') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnPrintGRN','Print GRN',p_web.combine(Choose('Print GRN' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('Goods_Received_Note_Retail&' & p_web._jsok('GRN=' & p_web.GSV('locGRNNumber') & '&INV=' & p_web.GSV('locInvoiceNumber') & '&GRD=' & p_web.GSV('locGRN') & '&REP=N') &''&'','_blank'),,loc:disabled,'/images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnPrintGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnPrintGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintGRN') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPrintGRN_' & p_web._nocolon('btnPrintGRN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintGRN') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPrintGRN_nexttab_' & 0)
    locGRN = p_web.GSV('locGRN')
    do ValidateValue::locGRN
    If loc:Invalid
      loc:retrying = 1
      do Value::locGRN
      !do SendAlert
      do Comment::locGRN ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmPrintGRN_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPrintGRN_tab_' & 0)
    do GenerateTab0
  of lower('frmPrintGRN_locGRN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locGRN
      of event:timer
        do Value::locGRN
        do Comment::locGRN
      else
        do Value::locGRN
      end
  of lower('frmPrintGRN_btnCreateGRN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCreateGRN
      of event:timer
        do Value::btnCreateGRN
        do Comment::btnCreateGRN
      else
        do Value::btnCreateGRN
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmPrintGRN_form:ready_',1)

  p_web.SetSessionValue('frmPrintGRN_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmPrintGRN',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmPrintGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintGRN_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmPrintGRN',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmPrintGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintGRN_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmPrintGRN:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmPrintGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintGRN_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmPrintGRN:Primed',0)
  p_web.setsessionvalue('showtab_frmPrintGRN',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('Hide:PrintGRN') = 0)
          If p_web.IfExistsValue('locGRN')
            locGRN = p_web.GetValue('locGRN')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmPrintGRN_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmPrintGRN_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locGRN
    If loc:Invalid then exit.
    do ValidateValue::Comment:locGRN
    If loc:Invalid then exit.
    do ValidateValue::btnCreateGRN
    If loc:Invalid then exit.
    do ValidateValue::btnPrintGRN
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmPrintGRN:Primed',0)
  p_web.StoreValue('locGRN')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locGRN',locGRN) ! DATE
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locGRN = p_web.GSV('locGRN') ! DATE
frmPrintAllGRN       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
GRNOTESR::State  USHORT
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('frmPrintAllGRN')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmPrintAllGRN_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmPrintAllGRN','')
    p_web.DivHeader('frmPrintAllGRN',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmPrintAllGRN') = 0
        p_web.AddPreCall('frmPrintAllGRN')
        p_web.DivHeader('popup_frmPrintAllGRN','nt-hidden')
        p_web.DivHeader('frmPrintAllGRN',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmPrintAllGRN_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmPrintAllGRN_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPrintAllGRN',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmPrintAllGRN',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmPrintAllGRN')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(GRNOTESR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(GRNOTESR)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmPrintAllGRN_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmPrintAllGRN'
    end
    p_web.formsettings.proc = 'frmPrintAllGRN'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmPrintAllGRN_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmPrintAllGRN')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmPrintAllGRN_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmPrintAllGRN_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmPrintAllGRN_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmPrintAllGRN',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmPrintAllGRN0_div')&'">'&p_web.Translate('Available Reports For Selected Invoice')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmPrintAllGRN_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmPrintAllGRN_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPrintAllGRN_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmPrintAllGRN_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPrintAllGRN_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmPrintAllGRN'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPrintAllGRN') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmPrintAllGRN'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPrintAllGRN') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Available Reports For Selected Invoice') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmPrintAllGRN_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmPrintAllGRN",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmPrintAllGRN')>0,p_web.GSV('showtab_frmPrintAllGRN'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmPrintAllGRN_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmPrintAllGRN') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmPrintAllGRN')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Available Reports For Selected Invoice')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Available Reports For Selected Invoice')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Available Reports For Selected Invoice')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Available Reports For Selected Invoice')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPrintAllGRN0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      do SendPacket
      Do showreports
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPrintAllGRN_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmPrintAllGRN_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPrintAllGRN_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)

  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmPrintAllGRN:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmPrintAllGRN_form:ready_',1)
  p_web.SetSessionValue('frmPrintAllGRN_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmPrintAllGRN:Primed',0)
  p_web.setsessionvalue('showtab_frmPrintAllGRN',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmPrintAllGRN_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmPrintAllGRN_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmPrintAllGRN:Primed',0)

showreports  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
  found# = 0
  Access:GRNOTESR.ClearKey(grr:Order_Number_Key)
  grr:Order_Number        = p_web.GSV('locInvoiceNumber')
  Set(grr:Order_Number_Key,grr:Order_Number_Key)
  Loop
      If Access:GRNOTESR.NEXT()
          Break
      End !If
      If grr:Order_Number        <> p_web.GSV('locInvoiceNumber')      |
          Then Break.  ! End If
      packet = clip(packet) & p_web.AsciiToUTF(|
          '<A HREF="Goods_Received_Note_Retail?GRN='& grr:Goods_Received_Number & |
          '&INV=' & p_web.GSV('locInvoiceNumber') & |
          '&GRD=' & grr:Goods_Received_Date & |
          '&REP=Y" target="_blank">Goods Received Note ( ' & FORMAT(grr:Goods_Received_Date,@d06) & ' )</A><br>',net:OnlyIfUTF)
      found# = 1
  !        GLO:Select1 = grr:Goods_Received_Number
  !        GLO:Select2 = tmp:InvoiceNumber
  !        GLO:Select3 = grr:Goods_Received_Date
  !        glo:Select4 = 'REPRINT'
  !        Goods_Received_Note_Retail
  !        GLO:Select1 = ''
  !        GLO:Select2 = ''
  !        GLO:Select3 = ''
  !        glo:Select4 = ''
  End !Loop
  IF (found# = 0)
      packet = CLIP(packet) & p_web.AsciiToUtf( |
          '<span class="red large">There are no Good Receive Notes Created For This Invoice</span>',net:OnlyIFUTF)
  END
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
BuildStockReceiveList PROCEDURE  (NetWebServerWorker p_web,LONG fRefNumber,BYTE fExchangeOrder,BYTE fLoanOrder) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    Access:RETSTOCK.Clearkey(res:Despatched_Key)
    res:Ref_Number = fRefNumber
    res:Despatched = 'YES'
    SET(res:Despatched_Key,res:Despatched_Key)
    LOOP UNTIL Access:RETSTOCK.Next()
        IF (res:Ref_Number <> fRefNumber OR |
            res:Despatched <> 'YES')
            BREAK
        END

        addNewPart# = 1
        IF (p_web.GSV('ret:ExchangeOrder') = 1 OR p_web.GSV('ret:LoanOrder') = 1)
                ! #12127 Group Parts If Exchange Order. Keep Received Seperate (Bryan: 05/07/2011)
            Access:STOCKRECEIVETMP.Clearkey(stotmp:ReceivedPartNumberKey)
            stotmp:SessionID = p_web.SessionID
            stotmp:Received = res:Received
            stotmp:PartNumber = res:Part_Number
            IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:ReceivedPartNumberKey) = Level:Benign)
                stotmp:Quantity += 1
                stotmp:QuantityReceived += res:QuantityReceived
                Access:STOCKRECEIVETMP.TryUpdate()
                addNewPart# = 0
            END
        END ! IF (ret:ExchangeOrder = 1)
        IF (addNewPart# = 1)

            IF (Access:STOCKRECEIVETMP.PrimeRecord() = Level:Benign)
                stotmp:SessionID = p_web.SessionID
                stotmp:PartNumber = res:Part_Number
                stotmp:Description = res:Description
                stotmp:ItemCost = res:Item_Cost
                stotmp:Quantity = res:Quantity
                stotmp:QuantityReceived = res:QuantityReceived
                stotmp:RESRecordNumber = res:Record_Number
                stotmp:Received = res:Received
                stotmp:ExchangeOrder = fExchangeOrder
                stotmp:LoanOrder = fLoanOrder

                IF (Access:STOCKRECEIVETMP.TryInsert())
                    Access:STOCKRECEIVETMP.CancelAutoInc()
                END
            END
        END ! IF (addNewPart# = 1)
    END

    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKRECEIVETMP.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKRECEIVETMP.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSTOCK.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSTOCK.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKRECEIVETMP.Close
     Access:RETSTOCK.Close
     FilesOpened = False
  END
ClearStockReceiveList PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    Access:STOCKRECEIVETMP.Clearkey(stotmp:PartNumberKey)
    stotmp:SessionID = p_web.SessionID
    SET(stotmp:PartNumberKey,stotmp:PartNumberKey)
    LOOP UNTIL Access:STOCKRECEIVETMP.Next()
        IF (stotmp:SessionID <> p_web.SessionID)
            BREAK
        END

        Access:STOCKRECEIVETMP.DeleteRecord(0)
    END

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKRECEIVETMP.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKRECEIVETMP.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKRECEIVETMP.Close
     FilesOpened = False
  END
