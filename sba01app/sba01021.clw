

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01021.INC'),ONCE        !Local module procedure declarations
                     END


AddExchangeHistory   PROCEDURE  (LONG fRefNumber,STRING fStatus,<STRING fNotes>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    IF (fRefNumber = 0)
        RETURN
    END
    Relate:EXCHHIST.Open()
    IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
        exh:Ref_Number  = fRefNumber
        exh:Date        = TODAY()
        exh:Time        = CLOCK()
        exh:User        = glo:Usercode
        exh:Status      = fStatus
        exh:Notes       = fNotes
        IF (Access:EXCHHIST.TryInsert())
            Access:EXCHHIST.CancelAutoInc()
        END
    END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
    Relate:EXCHHIST.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
FaultySiteLocation   PROCEDURE                        ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
ReturnValue     STRING(30)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:LOCATION.Open
    Access:LOCATION.Clearkey(loc:FaultyLocationKey)
    loc:FaultyPartsLocation = TRUE
    IF (Access:LOCATION.TryFetch(loc:FaultyLocationKey) = Level:Benign)
        ReturnValue = loc:Location
    END ! IF (Access:LOCATION.TryFetch(loc:FaulyLocationKey) = Level:Benign)
   Relate:LOCATION.Close
    RETURN ReturnValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ExchangeLoanOrderDetails PROCEDURE  (LONG fRefNumber,*gExchangeOrders gExchOrd,<BYTE fLoan>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:EXCHHIST.Open
   Relate:LOANHIST.Open
    VodacomClass.GetExchangeLoanOrderDetails(fRefNumber,gExchOrd,fLoan)

!    IF (fLoan)
!        Clear(gExchOrd)
!        ! Get the Sale and check the dates
!        Access:LOANHIST.Clearkey(loh:Ref_Number_Key)
!        loh:Ref_Number = fRefNumber
!        loh:Date = TODAY()
!        SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
!        LOOP UNTIL Access:LOANHIST.Next()
!            IF (loh:Ref_Number <> fRefNumber)
!                BREAK
!            END
!            IF (loh:Date > TODAY())
!                BREAK
!            END
!            IF (loh:Status = 'UNIT RECEIVED' AND gExchOrd.OrderNumber = 0)
!                ! Going in reverse order, so this should be the first one it finds
!                invStart# = INSTRING('RETAIL INVOICE NUMBER',loh:Notes,1,1) + 23
!                priceStart# = INSTRING('PRICE',loh:Notes,1,1)
!                gExchOrd.InvoiceNumber = SUB(loh:Notes,invStart#,priceStart# - invStart# - 2)
!                gExchOrd.ReceivedDate = loh:Date
!                gexchOrd.Price = SUB(loh:Notes,priceStart# + 8,30)
!                gExchOrd.OrderNumber = SUB(loh:Notes,15,invStart# - 15)
!                gExchOrd.ReceivedBy = loh:User
!            END
!            IF (loh:Status = 'IN TRANSIT')
!                gExchOrd.OrderedDate = loh:Date
!                IF (gExchOrd.OrderNumber = 0)
!                    ! In case the unit received history doesn't exist. Use the intransit one
!                    invStart# = INSTRING('RETAIL INVOICE NUMBER',loh:Notes,1,1) + 23
!                    priceStart# = INSTRING('PRICE',loh:Notes,1,1)
!                    gExchOrd.OrderNumber = SUB(loh:Notes,15,invStart# - 15)
!                    gExchOrd.InvoiceNumber = SUB(loh:Notes,invStart#,priceStart# - invStart# - 2)
!                    gexchOrd.Price = SUB(loh:Notes,priceStart# + 8,30)
!                END
!            END
!        END
!    ELSE
!        Clear(gExchOrd)
!        ! Get the Sale and check the dates
!        Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
!        exh:Ref_Number = fRefNumber
!        exh:Date = TODAY()
!        SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
!        LOOP UNTIL Access:EXCHHIST.Next()
!            IF (exh:Ref_Number <> fRefNumber)
!                BREAK
!            END
!            IF (exh:Date > TODAY())
!                BREAK
!            END
!            IF (exh:Status = 'UNIT RECEIVED' AND gExchOrd.OrderNumber = 0)
!                ! Going in reverse order, so this should be the first one it finds
!                invStart# = INSTRING('RETAIL INVOICE NUMBER',exh:Notes,1,1) + 23
!                priceStart# = INSTRING('PRICE',exh:Notes,1,1)
!                gExchOrd.InvoiceNumber = SUB(exh:Notes,invStart#,priceStart# - invStart# - 2)
!                gExchOrd.ReceivedDate = exh:Date
!                gexchOrd.Price = SUB(exh:Notes,priceStart# + 8,30)
!                gExchOrd.OrderNumber = SUB(exh:Notes,15,invStart# - 15)
!                gExchOrd.ReceivedBy = exh:User
!            END
!            IF (exh:Status = 'IN TRANSIT')
!                gExchOrd.OrderedDate = exh:Date
!                IF (gExchOrd.OrderNumber = 0)
!                    ! In case the unit received history doesn't exist. Use the intransit one
!                    invStart# = INSTRING('RETAIL INVOICE NUMBER',exh:Notes,1,1) + 23
!                    priceStart# = INSTRING('PRICE',exh:Notes,1,1)
!                    gExchOrd.OrderNumber = SUB(exh:Notes,15,invStart# - 15)
!                    gExchOrd.InvoiceNumber = SUB(exh:Notes,invStart#,priceStart# - invStart# - 2)
!                    gexchOrd.Price = SUB(exh:Notes,priceStart# + 8,30)
!                END
!            END
!        END
!    END ! IF (fLoan)
   Relate:EXCHHIST.Close
   Relate:LOANHIST.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobHasBeenInvoiced   PROCEDURE  (LONG fInvoiceNumber) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_inv_id  USHORT,AUTO
retValue BYTE(0)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
        IF (fInvoiceNumber > 0)
            Relate:INVOICE.Open()

            save_inv_id = Access:INVOICE.SaveFile()

            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number = fInvoiceNumber
            IF (Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign)
                IF (glo:WebJob)
                    IF (inv:RRCInvoiceDate > 0)
                        retValue = 1
                    END
                ELSE
                    IF (inv:ARCInvoiceDate > 0)
                        retValue = 1
                    END
                END
            END

            Access:INVOICE.RestoreFile(save_inv_id)

            Relate:INVOICE.Close()
        END ! IF (fInvoiceNumber > 0)
        Return retValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobHasBeenPaid       PROCEDURE                        ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
retValue        BYTE(0)
locAmountPaid   REAL,AUTO
locLabourVatRate    REAL
locPartsVatRate     REAL
locJobTotal         REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
        ! Assume we are in the JOBS and JOBSE record
        Relate:JOBPAYMT.Open()

        locAmountPaid = 0
        Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
        jpt:Ref_Number = job:Ref_Number
        SET(jpt:All_Date_Key,jpt:All_Date_Key)
        LOOP UNTIL Access:JOBPAYMT.Next()
            IF (jpt:Ref_Number <> job:Ref_Number)
                BREAK
            END
            locAmountPaid += jpt:Amount
        END

        Relate:JOBPAYMT.Close()


        Relate:SUBTRACC.Open()
        Relate:TRADEACC.Open()
        Relate:VATCODE.Open()

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Invoice_Sub_Accounts = 'YES')
                    Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                    vat:Vat_Code = sub:Labour_Vat_Code
                    IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                        locLabourVatRate = vat:Vat_Rate
                    END ! IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                    Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                    vat:Vat_Code = sub:Parts_Vat_Code
                    IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                        locPartsVatRate = vat:Vat_Rate
                    END ! IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                ELSE
                    Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                    vat:Vat_Code = tra:Labour_Vat_Code
                    IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                        locLabourVatRate = vat:Vat_Rate
                    END ! IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                    Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                    vat:Vat_Code = tra:Parts_Vat_Code
                    IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)
                        locPartsVatRate = vat:Vat_Rate
                    END ! IF (Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign)

                END ! IF (tra:Invoice_Sub_Accounts = 'YES')
            END
        END

        Relate:SUBTRACC.Close()
        Relate:TRADEACC.Close()
        Relate:VATCODE.Close()

        Relate:INVOICE.Open()
        IF (job:Invoice_Number > 0)
            
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number
            IF (Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign)
            END
        END

        IF (glo:WebJob)
            IF (job:Invoice_Number > 0 AND inv:RRCInvoiceDate > 0)
                locJobTotal = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost + |
                                jobe:InvRRCCLabourCost *(inv:Vat_Rate_Labour / 100) + |
                                jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts / 100) + |
                                job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)
            ELSE
                locJobTotal = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost + |
                                jobe:RRCCLabourCost *(locLabourVatRate / 100) + |
                                jobe:RRCCPartsCost * (locPartsVatRate / 100) + |
                                job:Courier_Cost * (locLabourVatRate / 100)
            END
        ELSE ! IF (glo:WebJob)
            IF (job:Invoice_Number > 0 AND inv:ARCInvoiceDate > 0)
                locJobTotal = job:Invoice_Labour_Cost + job:Invoice_Parts_Cost + job:Invoice_Courier_Cost + |
                                job:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100) + |
                                job:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) + |
                                job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)
            ELSE
                locJobTotal = job:Labour_Cost + job:Parts_Cost + job:Courier_Cost + |
                                job:Labour_Cost * (locLabourVatRate / 100) + |
                                job:Parts_Cost * (locPartsVatRate / 100) + |
                                job:Courier_Cost * (locLabourVatRate / 100)
            END
        END ! IF (glo:WebJob)

        IF (locJobTotal - locAmountPaid < 0.01)
            retValue = 1
        END

        Relate:INVOICE.Close()

        RETURN retValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CanTheJobBeDespatched PROCEDURE                       ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
RetValue  BYTE(1)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    ! Assume we are in the JOBS, WEBJOB and JOBSE record
    IF (job:Chargeable_Job = 'YES')
        Relate:TRADEACC.Open()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        IF (glo:WebJob)
            tra:Account_Number = wob:HeadAccountNumber
        ELSE
            tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
        END
        IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
            IF (tra:Despatch_Paid_Jobs = 'YES' AND NOT JobHasBeenPaid())
                RetValue = 0
                Case Missive('Unable to despatch. The selected job has not been paid.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            END
            IF (RetValue <> 1)
                IF (tra:Despatch_Invoiced_Jobs = 'YES' And JobHasBeenInvoiced(job:Invoice_Number))
                    RetValue = 0
                    Case Missive('Unable to despatch. The selected job has not been invoiced.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                END ! IF (tra:Despatch_Invoiced_Jobs = 'YES' And JobHasBeenInvoiced())
            END
        ELSE
            RetValue = 1
        END ! IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
        Relate:TRADEACC.Close()
    ELSE
        RetValue = 1
    END

    RETURN RetValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SetTheJobStatus      PROCEDURE  (LONG fStatusNumber,STRING fType) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBS.Open
   Relate:JOBSE.Open
   Relate:WEBJOB.Open
    VodacomClass.SetJobStatus(fStatusNumber,fType)

    !012477 - after every status change check to see if an SMS text should be sent JC 02/12
    !message('Calling sendSMStext from set the job status')
    SendSMSTExt(fType[1],'Y','N')

!Debug erroring turned off for distruibution
!    if glo:ErrorText[1:5] = 'ERROR' then
!       !these messages can be removed once this has been shown to work
!        miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!
!    ELSE
!
!        miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!
!        if job:Current_Status = '510 ESTIMATE READY' and fType = 'JOB' then
!            !change to Job:Current_Status = '520 ESTIMATE SENT
!                !Alternate
!                VodacomClass.SetJobStatus(520,'JOB')
!                SendSMSTExt('J','Y','N')
!        END !if current status 510
!    END !if the system errored

    glo:Errortext = ''
    !012477 - after every status change check to see if an SMS text should be sent
   Relate:JOBS.Close
   Relate:JOBSE.Close
   Relate:WEBJOB.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
UpdateTheJobStatusHistory PROCEDURE                   ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    VodacomClass.UpdateJobStatusHistory()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
LoanStatus           PROCEDURE  (STRING fAvailable,STRING fJobNumber) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.GetLoanStatus(fAvailable,fJobNumber))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
LookupFileExtended   PROCEDURE  (*CSTRING fFileName,<STRING fFileMask>,<STRING fLookupTitle>) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
retValue    BYTE(0)
lookupTitle CSTRING(255)
fileMask    CSTRING(255)
savePath    CSTRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    IF (fLookupTitle <> '')
        lookupTitle = fLookupTitle
    ELSE
        lookupTitle = 'Choose File'
    END

    IF (fFileMask <> '')
        fileMask = fFileMask
    ELSE
        fileMask = 'All Files|*.*'
    END

    IF (glo:WebJob)
        IF (ClarioNET:GetFilesFromClient(0,'') = 'ok')
            ! Found
            GET(FileListQueue,1)
            fFileName = flq:Filename
            retValue = 0
        ELSE
            ! Error
            retValue = 1
        END

    ELSE
        savePath = PATH()
        IF (FILEDIALOG(lookupTitle,fFileName,fileMask,FILE:KeepDir + FILE:NoError + FILE:LongName))
            ! Found
            retValue = 0
        ELSE
            ! Error
            retValue = 1
        END
        SETPATH(savePath)
    END

    RETURN retValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
