

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01018.INC'),ONCE        !Local module procedure declarations
                     END


StockInUse           PROCEDURE  (func:ShowError)      ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Pointer# = Pointer(STOCK)
    Hold(STOCK,1)
    Get(STOCK,Pointer#)
    If Errorcode() = 43
        If func:ShowError
            Case Missive('The selected item is currently in use by another station.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If func:ShowError
        Return Level:Fatal
    End !If Errorcode() = 43
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
HubOutOfRegion       PROCEDURE  (f:HeadAccountNumber,f:Hub) ! Declare Procedure
save_trh_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If f:HeadAccountNumber = ''
        Return 0
    End ! If f:HeadAccountNumber = ''
    If f:Hub = ''
        Return 0
    End ! If f:Hub = ''
    Relate:TRAHUBS.Open()
    save_trh_id = Access:TRAHUBS.SaveFile()

    Found# = 0
    Access:TRAHUBS.Clearkey(trh:HubKey)
    trh:TRADEACCAccountNumber = f:HeadAccountNumber
    Set(trh:HubKey,trh:HubKey)
    Loop
        If Access:TRAHUBS.Next()
            Break
        End ! If Access:TRAHUBS.Next()
        If trh:TRADEACCAccountNumber <> f:HeadAccountNumber
            Break
        End ! If trh:TRADEACCAccountNumber <> wob:HeadAccountNumber
        Found# = 1
        If trh:Hub = f:Hub
            Found# = 2
            Break
        End ! If trh:Hub = f:Hub
    End ! Loop

    Access:TRAHUBS.RestoreFile(save_trh_id)
    Relate:TRAHUBS.Close()
    !0 = No Relevant Hub Data
    !1 = Hub is out of region
    !2 = Found Hub.
    Return Found#

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ReleasedForDespatch  PROCEDURE  (f:JobNumber)         ! Declare Procedure
save_aud_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Relate:AUDIT.Open()
    save_aud_id = Access:AUDIT.SaveFile()
    Return# = 0
    Access:AUDIT.Clearkey(aud:Action_Key)
    aud:Ref_Number = f:JobNumber
    aud:Action = 'OUT OF REGION: RELEASED FOR DESPATCH'
    Set(aud:Action_Key,aud:Action_Key)
    Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> f:JobNumber
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
            Break
        End ! If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
        Return# = 1
        Break
    End ! Loop
    Access:AUDIT.RestoreFile(save_aud_id)
    Relate:AUDIT.Close()

    Return Return#
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IsDateInValid        PROCEDURE  (f:Date)              ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If f:Date < Deformat('01/01/1990',@d06)
        Return 1
    End ! If f:Date < Deformat('01/01/1990',@d06)
    If f:Date > Today()
        Return 1
    End ! If f:Date > Today()
    Return 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
UseReplenishmentProcess PROCEDURE  (f:ModelNumber,f:AccountNumber) ! Declare Procedure
save_MANUFACT_id     USHORT,AUTO
save_MODELNUM_id     USHORT,AUTO
save_TRADEACC_id     USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Relate:MANUFACT.Open()
    Relate:MODELNUM.Open()
    Relate:TRADEACC.Open()

    save_MANUFACT_id = Access:MANUFACT.SaveFile()
    save_MODELNUM_id = Access:MODELNUM.SaveFile()
    save_TRADEACC_id = Access:TRADEACC.SaveFile()

    Return# = 0
    ! Inserting (DBH 21/02/2008) # 9717 - If model/manufacturer is used in Replenishment, then it can't be 48 Hour
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = f:AccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:IgnoreReplenishmentProcess = 0
            ! This account CAN use the replenishment process (DBH: 21/02/2008)
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number = f:ModelNumber
            If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                If mod:UseReplenishmentProcess = 1
                    Return# = 1
                Else ! If mod:UseReplenishmentProcess = 1
                    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        If man:UseReplenishmentProcess = 1
                            Return# = 1
                        End ! If man:UseReplenishmentProcess = 1
                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End ! If mod:UseReplenishmentProcess = 1
            End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        End ! If tra:IgnoreReplenishmentProcess = 0
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    ! End (DBH 21/02/2008) #9717

    Access:MANUFACT.RestoreFile(save_MANUFACT_id)
    Access:MODELNUM.RestoreFile(save_MODELNUM_id)
    Access:TRADEACC.RestoreFile(save_TRADEACC_id)

    Relate:MANUFACT.Close()
    Relate:MODELNUM.Close()
    Relate:TRADEACC.Close()

    Return Return#
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CopyDOPFromBouncer   PROCEDURE  (String f:Manufacturer, Date f:PreviousDOP, *Date f:DOP,  String f:IMEINumber, String f:Bouncer, *Byte f:Found) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
    include('CopyDOPFromBouncer.inc','Local Data')
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    include('CopyDOPFromBouncer.inc','Processed Code')
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
MQProcess            PROCEDURE  (String f:IMEINumber,*String f:POP,*Date f:DOP,*Byte f:Error,*String f:ErrorMessage,String f:OneYearWarranty) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
    include('MQProcess.inc','Local Data')
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    include('MQProcess.inc','Processed Code')
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
RunDOS               PROCEDURE  (string aCmd, byte aWait) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! dwCreationFlag values
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
! Run DOS Program

    CommandLine = clip(aCmd)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = CreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = GETINI('MQ','TimeOut',,Clip(Path()) & '\SB2KDEF.INI')
    if (timeout# = 0)
        timeout# = 20
    end ! if (timeout# = 0)
    timeout# = clock() + (timeout# * 100)

    if aWait then
        setcursor(cursor:wait)
        loop
            Sleep(100)
            ! check for when process is finished
            ReturnCode# = GetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
        while ProcessExitCode = STILL_ACTIVE and clock() < timeout#
        setcursor
    end

    return(true)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetExchangeStatus    PROCEDURE  (STRING fAvailable,STRING fJobNumber) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.GetExchangeStatus(fAvailable,fJobNumber))
!    case f:Available
!    of 'AVL'
!        Return 'AVAILABLE'
!    of 'EXC'
!        Return 'EXCHANGED - JOB NO: ' & f:JobNumber
!    of 'INC'
!        Return 'INCOMING TRANSIT - JOB NO: ' & f:JobNumber
!    of 'FAU'
!        Return 'FAULTY'
!    of 'NOA'
!        Return 'NOT AVAILABLE'
!    of 'REP'
!        Return 'IN REPAIR - JOB NO: ' & f:JobNumber
!    of 'SUS'
!        Return 'SUSPENDED'
!    of 'DES'
!        Return 'DESPATCHED - JOB NO: ' & f:JobNumber
!    of 'QA1'
!        Return 'ELECTRONIC QA REQUIRED - JOB NO: ' & f:JobNumber
!    of 'QA2'
!        Return 'MANUAL QA REQUIRED - JOB NO: ' & f:JobNumber
!    of 'QAF'
!        Return 'QA FAILED'
!    of 'RTS'
!        Return 'RETURN TO STOCK'
!    of 'INR'
!        Return 'EXCHANGE REPAIR'
!    of 'ESC'
!        Return 'EXCHANGE SCRAP CONFIRMED - JOB NO:' & f:JobNumber
!    of 'IT4'
!        Return 'IN TRANSIT TO 48HR STORES'
!    of 'ITS'
!        Return 'IN TRANSIT TO SCRAPPING'
!    of 'ITR'
!        Return 'IN TRANSIT TO RTM'
!    of 'ITP'
!        Return 'PHANTOM IN TRANSIT'
!    else
!        Return 'IN REPAIR - JOB NO: ' & f:JobNumber
!    end ! case f:Available

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CompleteAndRebookJob PROCEDURE  (f:RefNumber,f:NewLocation,f:Type) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
NewJobBooking Group
JobNumber           Long()
AccountNumber       String(30)
IMEINumber          String(30)
MSN                 String(30)
ModelNumber         String(30)
ProductCode         String(30)
UnitType            String(30)
Colour              String(30)
Network             String(30)
Type                String(3)
                End ! Group
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBS.Open
   Relate:JOBSE.Open
   Relate:EXCHANGE.Open
   Relate:EXCHHIST.Open
   Relate:WEBJOB.Open
   Relate:JOBSENG.Open
   Relate:JOBSOBF.Open
   Relate:TRADEACC.Open
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number    = f:RefNumber
    if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        ! Error
        Return
    end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber    = job:Ref_Number
    if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Found
    else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Error
        Return
    end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

    JobPricingRoutine(0)
    Access:JOBSE.Update()

    GetStatus(705,1,'JOB')

    job:Date_Completed = Today()
    job:Time_Completed = Clock()

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber    = job:Ref_Number
    if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        ! Found
    else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)

    wob:DateCompleted = job:Date_Completed
    wob:TimeCompleted = job:Time_Completed
    wob:Completed ='YES'
    access:WEBJOB.TryUpdate()

    ! Not sure if this is necessary
    Access:JOBSOBF.ClearKey(jof:RefNumberKey)
    jof:RefNumber = job:Ref_Number
    If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
        !Found
        jof:DateCompleted = job:Date_Completed
        jof:TimeCompleted = job:Time_Completed
        jof:HeadAccountNumber = wob:HeadAccountNumber
        jof:IMEINumber = job:ESN
        Access:JOBSOBF.Update()
    Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
    ! End (DBH 15/03/2007) #8718

    job:Completed = 'YES'

    !Lookup current engineer and mark as escalated
    Access:JOBSENG.ClearKey(joe:UserCodeKey)
    joe:JobNumber     = job:Ref_Number
    joe:UserCode      = job:Engineer
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.PREVIOUS()
           Break
        End !If
        If joe:JobNumber     <> job:Ref_Number       |
        Or joe:UserCode      <> job:Engineer      |
        Or joe:DateAllocated > Today()       |
            Then Break.  ! End If
        joe:Status = 'COMPLETED'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        Access:JOBSENG.Update()
        Break
    End !Loop

    job:edi = 'XXX'

    !Mark the job's exchange unit as being available ** Check This **
!    CompleteDoExchange(job:Exchange_Unit_Number)

    LocationChange(Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))

    if (f:Type = '48HR')
        ! Find the exchange unit for the job and updated the status. (And move if it's a RRC 48Hr) (DBH: 11/03/2009) #10473
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number    = wob:HeadAccountNumber
        if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            ! Found

            exchangeFound# = 0
            Access:EXCHANGE.Clearkey(xch:LocStockIMEIKey)
            xch:Location      = tra:SiteLocation
            xch:Stock_Type    = '48-HOUR SWAP UNITS'
            xch:ESN    = job:ESN
            if (Access:EXCHANGE.TryFetch(xch:LocStockIMEIKey) = Level:Benign)
                ! Found
                GetStatus(720,1,'JOB') ! Reebooked AT ARC
                exchangeFound# = 1
            else ! if (Access:EXCHANGE.TryFetch(xch:LocStockIMEIKey) = Level:Benign)
                ! Error
                ! Can't find IMEI so must be a refurb. Therefore the unit is at the ARC (DBH: 24/03/2009) #10473

                Access:EXCHANGE.Clearkey(xch:LocIMEIKey)
                xch:Location    =  GETINI('BOOKING','HeadSiteLocation',,Clip(Path()) & '\SB2KDEF.INI')
                xch:ESN         = job:ESN
                if (Access:EXCHANGE.TryFetch(xch:LocIMEIKey) = Level:Benign)
                    ! Found
                    xch:Stock_Type    = '48-HOUR SWAP UNITS'
                    GetStatus(734,1,'JOB') ! InTransit 48 Hour Stores
                    exchangeFound# = 1
                else ! if (Access:EXCHANGE.TryFetch(xch:LocIMEIKey) = Level:Benign)
                    ! Error
                end ! if (Access:EXCHANGE.TryFetch(xch:LocIMEIKey) = Level:Benign)
            end ! if (Access:EXCHANGE.TryFetch(xch:LocStockIMEIKey) = Level:Benign)
            if (exchangeFound# = 1)
                if (clip(f:NewLocation) <> '')
                    xch:Location = f:NewLocation
                end ! if (clip(f:NewLocation) <> '')
                xch:Available = 'NOA'
                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                Access:EXCHANGE.TryUpdate()

                if Access:EXCHHIST.PrimeRecord() = Level:Benign
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = Today()
                    exh:Time = Clock()

                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password    = glo:Password
                    if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                        ! Found
                    else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                        ! Error
                    end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                    exh:User = use:User_Code
                    exh:Status = 'BOOKED FOR REPAIR'
                    if Access:EXCHHIST.TryInsert() = Level:Benign
                        ! Inserted
                    else ! if Access:EXCHHIST.TryInsert() = Level:Benign
                        ! Error
                        Access:EXCHHIST.CancelAutoInc()
                    end ! if Access:EXCHHIST.TryInsert() = Level:Benign
                end ! if Access:EXCHHIST.PrimeRecord() = Level:Benign
            end ! if (exchangeFound# = 1)
        else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)


    end ! if (f:Type = '48HR')

    if (AddToAudit(job:Ref_Number,'JOB','JOB COMPLETED AND REBOOKED',''))
    end ! if (AddToAudit(job:Ref_Number,'JOB','UNIT REBOOKED',''))

    Access:JOBS.TryUpdate()

    NewJobBooking.JobNumber = job:Ref_Number
    if (f:Type = '48HR')
        NewJobBooking.AccountNumber = 'AA20-86013'
    elsif (f:Type = '3RD')
        NewJobBooking.AccountNumber = 'AA20-REFURBQ'
    end !if (f:Type = '48HR')

    NewJobBooking.IMEINumber = job:ESN
    NewJobBooking.MSN = job:MSN
    NewJobBooking.ModelNumber = job:Model_Number
    NewJobBooking.ProductCode = job:ProductCode
    NewJobBooking.UnitType = job:Unit_Type
    NewJobBooking.Colour = job:Colour
    NewJobBooking.Network = jobe:Network
    NewJobBooking.Type = f:Type


   Relate:JOBS.Close
   Relate:JOBSE.Close
   Relate:EXCHANGE.Close
   Relate:EXCHHIST.Close
   Relate:WEBJOB.Close
   Relate:JOBSENG.Close
   Relate:JOBSOBF.Close
   Relate:TRADEACC.Close
    CreateNewJob(Address(NewJobBooking))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
