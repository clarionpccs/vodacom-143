

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01002.INC'),ONCE        !Local module procedure declarations
                     END


CheckParts           PROCEDURE  (f_type)              ! Declare Procedure
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
tmp:FoundReceived    BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Return (1) = Pending Order
!Return (2) = On Order
!Return (3) = Back Order Spares
!Return (4) = Parts Recevied
!Return (0) = Received Order Or No Parts With Anything To Do WIth Orders
    tmp:FoundReceived = False
    Case f_type
        Of 'C'
            If job:Chargeable_Job = 'YES'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job:ref_number      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = par:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                Return(1)
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = MainStoreLocation()
                        sto:Part_Number = par:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                Return(3)
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        Return(1)
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        Return(1)
                    End
                    If par:order_number <> ''
                        If par:date_received = ''
                            Return(2)
                        Else
                            tmp:FoundReceived = True
                        End!If par:date_received = ''
                    End!If par:order_number <> ''
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()
            End ! If job:Chargeable_Job = 'YES'

        Of 'W'
            If job:Warranty_Job = 'YES'
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job:ref_number      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If wpr:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = wpr:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                Return(1)
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = MainStoreLocation()
                        sto:Part_Number = wpr:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                Return(3)
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        Return(1)
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)

                    If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                        Return(1)
                    End
                    If wpr:order_number <> ''
                        If wpr:date_received = ''
                            Return(2)
                        Else!If wpr:date_recieved = ''
                            tmp:FoundReceived = True
                        End!If wpr:date_recieved = ''
                    End!If wpr:order_number <> ''
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()

            End ! If job:Warranty_Job = 'YES'

    End!Case f_type

    If tmp:FoundReceived = True
        !All parts received - TrkBs: 4625 (DBH: 17-02-2005)
        Return(4)
    Else ! If tmp:FoundReceived = True
        !No parts attached - TrkBs: 4625 (DBH: 17-02-2005)
        Return Level:Benign
    End ! If tmp:FoundReceived = True


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CheckPricing         PROCEDURE  (f_type)              ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If job:chargeable_job = 'YES' and f_type = 'C'
        If job:account_number <> '' And job:charge_type <> '' And |
            job:model_number <> '' And job:unit_type <> '' And job:repair_type <> ''

            !Look for standard charge - L945 (DBH: 04-09-2003)
            Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
            sta:Model_Number = job:Model_Number
            sta:Charge_Type  = job:Charge_Type
            sta:Unit_Type    = job:Unit_Type
            sta:Repair_Type  = job:Repair_Type
            If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Found
            Else !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Error

                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                trc:Account_Number = tra:Account_Number
                trc:Model_Number   = job:Model_Number
                trc:Charge_Type    = job:Charge_Type
                trc:Unit_Type      = job:Unit_Type
                trc:Repair_Type    = job:Repair_Type
                If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Found

                Else !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Error
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = job:Account_Number
                    suc:Model_Number   = job:Model_Number
                    suc:Charge_Type    = job:Charge_Type
                    suc:Unit_Type      = job:Unit_Type
                    suc:Repair_Type    = job:Repair_Type
                    If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Found

                    Else !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Error
                        Case Missive('Error! No Pricing Structure.'&|
                          '<13,10>'&|
                          '<13,10>The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Type does not have a pricing structure setup.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                        Of 1 ! OK Button
                        End ! Case Missive
                        Return (1)
                    End !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                End !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
            End !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
        End !If job:account_number <> '' And job:charge_type <> '' And |
    End!If job:chargeable_job = 'YES'

    If job:warranty_job = 'YES' and f_type = 'W'
        If job:account_number <> '' And job:warranty_charge_type <> '' And |
            job:model_number <> '' And job:unit_type <> '' And job:repair_type_warranty <> ''
            !Look for standard charge - L945 (DBH: 04-09-2003)
            Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
            sta:Model_Number = job:Model_Number
            sta:Charge_Type  = job:Warranty_Charge_Type
            sta:Unit_Type    = job:Unit_Type
            sta:Repair_Type  = job:Repair_Type_Warranty
            If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Found
            Else !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Error

                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                trc:Account_Number = tra:Account_Number
                trc:Model_Number   = job:Model_Number
                trc:Charge_Type    = job:Warranty_Charge_Type
                trc:Unit_Type      = job:Unit_Type
                trc:Repair_Type    = job:Repair_Type_Warranty
                If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Found

                Else !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Error
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = job:Account_Number
                    suc:Model_Number   = job:Model_Number
                    suc:Charge_Type    = job:Warranty_Charge_Type
                    suc:Unit_Type      = job:Unit_Type
                    suc:Repair_Type    = job:Repair_Type_Warranty
                    If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Found

                    Else !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Error
                        Case Missive('Error! No Pricing Structure.'&|
                          '<13,10>'&|
                          '<13,10>The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Type does not have a pricing structure setup.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                        Of 1 ! OK Button
                        End ! Case Missive
                        Return (1)
                    End !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                End !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
            End !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
        End!If job:account_number <> '' And job:charge_type <> '' And |
    End!If job:chargeable_job = 'YES'

    Return(0)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Set_EDI              PROCEDURE  (f_manufacturer)      ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !This Function is no longer used.
    !It has been superceeded by PendingJob function
    !Any reference to set_EDI will need to be removed
    Return job:EDI
!    !Is this an EDI manufacturer
!    Case f_manufacturer
!        Of 'ALCATEL'
!            Return('EDI')
!        Of 'BOSCH'
!            Return('EDI')
!        Of 'ERICSSON'
!            Return('EDI')
!        Of 'MAXON'
!            Return('EDI')
!        Of 'MOTOROLA'
!            Return('EDI')
!        Of 'NEC'
!            Return('EDI')
!        Of 'NOKIA'
!            Return('EDI')
!        Of 'SIEMENS'
!            Return('EDI')
!        Of 'SAMSUNG'
!            Return('EDI')
!        Of 'MITSUBISHI'
!            Return('EDI')
!        Of 'TELITAL'
!            Return('EDI')
!        Of 'SAGEM'
!            Return('EDI')
!        Of 'SONY'
!            Return('EDI')
!        Of 'PANASONIC'
!            Return('EDI')
!        Else
!            Return('XXX')
!    End

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Capitalize           PROCEDURE  (f_string)            ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    f_string = CLIP(LEFT(f_string))

    STR_LEN#  = LEN(f_string)
    STR_POS#  = 1

    f_string = UPPER(SUB(f_string,STR_POS#,1)) & SUB(f_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(f_string,STR_POS#,1) = ' ' OR SUB(f_string,STR_POS#,1) = '-'  |
        OR SUB(f_string,STR_POS#,1) = '.' OR SUB(f_string,STR_POS#,1) = '/' |
        OR SUB(f_string,STR_POS#,1) = '&' OR SUB(f_string,STR_POS#,1) = '(' OR SUB(f_string,STR_POS#,1) = CHR(39)
        f_string = SUB(f_string,1,STR_POS#) & UPPER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)
     ELSE
        f_string = SUB(f_string,1,STR_POS#) & LOWER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)

     .
    .

    RETURN(f_string)

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetStatus            PROCEDURE  (f_number,f_audit,f_type) ! Declare Procedure
save_str_id          USHORT,AUTO
tmp:End_Days         LONG
tmp:End_Hours        LONG
tmp:CCList           STRING(500)
tmp:EmailMessage     STRING(1024)
WobAvailableFlag     BYTE
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !TB12477
    !Change needed for Webmaster so that despatch of a job to VCP does not change the status to "811 READY TO DESPATCH (LOAN)"
    !but rather "463 SEND TO PUP".
    if f_number = 811 and job:who_Booked = 'WEB' then
        f_number = 463
    END

    ! #13123 Ok, we've already assumed that jobs is open. So I will continue with that to make life easier.
    ! Don't change job to 810 READY TO DESPATCH, or 916 PAID AWAITNG DESPATCH
    ! if at 453 SEND TO PUP (DBH: 29/07/2013)
    IF (f_Number = 810 OR f_Number = 916)
        IF (job:Who_Booked = 'WEB' AND SUB(job:Current_Status,1,3) = '463')
            ! Do nothing
            RETURN
        END ! IF (job:Who_Booked = 'WEB' AND SUB(job:Current_Status,1,3) = '463')
    END ! IF (f_Number = 810 OR f_Number = 916)


    VodacomClass.SetStatus(f_number,f_audit,f_type)


    IF (job:Who_Booked = 'WEB' AND f_number = 110 AND f_type = 'EXC')
        ! #13203 Hideous bit of hardcoding to stop SMS being sent 
        ! when "DESPATCH EXCH UNIT' set on PUP jobs. (DBH: 11/11/2013)
        RETURN
    END !

    !012477 - after every status change check to see if an SMS text should be sent
    !JC 02/12
    !message('Calling SendSMSText from get status')
    SendSMSTExt(f_Type[1],'Y','N')

!Debug erroring turned off for distruibution
!    if glo:ErrorText[1:5] = 'ERROR' then
!       !these messages can be removed once this has been shown to work
!        miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!
!    ELSE
!
!        miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!
!        if job:Current_Status = '510 ESTIMATE READY' and f_type = 'JOB' then
!            !change to Job:Current_Status = '520 ESTIMATE SENT
!                !Alternate
!                VodacomClass.SetStatus(520,f_audit,'JOB')
!                SendSMSTExt('J','Y','N')
!        END !if current status 510
!    END !if the system errored
    glo:Errortext = ''
    !012477 - after every status change check to see if an SMS text should be sent










!!to change the status
!    !Fetch the webjob record
!    Access:Webjob.Open()
!    Access:webjob.UseFile()
!
!    ! Insert --- Open and close so don't have to elsewhere (DBH: 18/03/2009) #10473
!    Relate:STATUS.Open()
!    Relate:STAHEAD.Open()
!    Relate:JOBSTAGE.Open()
!    Relate:DEFAULTS.Open()
!    ! end --- (DBH: 18/03/2009) #10473
!
!    WobAvailableFlag = false
!    access:webjob.clearkey(wob:RefNumberKey)
!    wob:RefNumber = job:ref_number
!    if access:Webjob.fetch(wob:RefNumberKey)
!        !couldn't find one - make a note not to update
!        WobAvailableFlag = false
!    ELSE
!        WobAvailableFlag = true
!    END
!
!    Case f_type
!        Of 'JOB'
!            sterror# = 0
!            If job:cancelled = 'YES'
!                sterror# = 1
!            End!If job:cancelled = 'YES'
!            !message(wob:Current_Status &', '&job:current_Status)
!            If sterror# = 0
!                access:status.clearkey(sts:ref_number_only_key)
!                sts:ref_number  = f_number
!                IF access:status.tryfetch(sts:ref_number_only_key)
!                    job:current_status  = 'ERROR (' & Clip(f_number) & ')'
!                Else!IF access:status.tryfetch(sts:ref_number_only_key)
!                    !Only save previous status if the new status is different
!                    If sts:Status <> job:Current_Status
!                        job:PreviousStatus  = job:current_status
!                        access:users.clearkey(use:password_key)
!                        use:password    = glo:password
!                        access:users.fetch(use:password_key)
!                        job:statusUser      = use:user_code
!                        job:current_status  = sts:status
!
!! Changing (DBH 13/04/2007) # 8912 - Always update the status history if something has changed
!!                        If job:PreviousStatus <> job:Current_Status
!!
!!                            Access:AUDSTATS.Open()
!!                            Access:AUDSTATS.Usefile()
!! to (DBH 13/04/2007) # 8912
!                        Relate:AUDSTATS.Open()
!! End (DBH 13/04/2007) #8912
!                        If Access:AUDSTATS.Primerecord() = Level:Benign
!                            aus:RefNumber    = job:Ref_Number
!                            aus:Type         = 'JOB'
!                            aus:DateChanged  = Today()
!                            aus:TimeChanged  = Clock()
!                            aus:OldStatus    = job:PreviousStatus
!                            aus:NewStatus    = job:Current_Status
!                            aus:UserCode     = use:User_Code
!                            If Access:AUDSTATS.Tryinsert()
!                                Access:AUDSTATS.Cancelautoinc()
!                            End!If Access:AUDSTATS.Tryinsert()
!                        End!If Access:AUDSTATS.Primerecord() = Level:Benign
!
!                        Relate:AUDSTATS.Close()
!
!                        !Only update status details, if different from current status - TrkBs: 4761 (DBH: 14-02-2005)
!                        !Update the webjob record
!                        if WobAvailableFlag = true
!                            wob:Current_Status_Date = today()
!                            !message(wob:Current_Status &', '&job:current_Status)
!                        END !if wob available
!
!                        access:stahead.clearkey(sth:ref_number_key)
!                        sth:ref_number  = sts:heading_ref_number
!                        If access:stahead.tryfetch(sth:ref_number_key) = Level:Benign
!                            access:jobstage.clearkey(jst:job_stage_key)
!                            jst:ref_number  = job:ref_number
!                            jst:job_stage   = sth:heading
!                            If access:jobstage.tryfetch(jst:job_stage_key)
!                                Get(jobstage,0)
!                                jst:ref_number  = job:ref_number
!                                jst:job_stage   = sth:heading
!                                jst:date        = Today()
!                                jst:time        = Clock()
!                                jst:user        = use:user_code
!                                access:jobstage.tryinsert()
!                            End!If access:jobstage.tryfetch(jst:job_stage_key)
!                        End!If access:stahead.tryfetch(sth:ref_number_key) = Level:Benign
!                        If sts:use_turnaround_time  = 'YES'
!                            tmp:End_Days = Today()
!                            x# = 0
!                            count# = 0
!                            If sts:Turnaround_Days <> 0
!                                Loop
!                                    count# += 1
!                                    day_number# = (Today() + count#) % 7
!                                    If def:include_saturday <> 'YES'
!                                        If day_number# = 6
!                                            tmp:end_days += 1
!                                            Cycle
!                                        End
!                                    End
!                                    If def:include_sunday <> 'YES'
!                                        If day_number# = 0
!                                            tmp:end_days += 1
!                                            Cycle
!                                        End
!                                    End
!                                    tmp:end_days += 1
!                                    x# += 1
!                                    If x# >= sts:Turnaround_Days
!                                        Break
!                                    End!If x# >= sts:turnaround_days
!                                End!Loop
!                            End!If f_days <> ''
!
!                            tmp:End_Hours = Clock()
!                            If sts:Turnaround_Hours <> 0
!                                start_time_temp# = Clock()
!                                new_day# = 0
!                                Loop x# = 1 To sts:Turnaround_Hours
!                                    tmp:End_Hours += 360000
!                                    If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
!                                        If tmp:End_Hours > def:End_Work_Hours or tmp:End_Hours < def:Start_Work_Hours
!                                            tmp:End_Hours = def:Start_Work_Hours
!                                            tmp:End_Days += 1
!                                        End !If tmp:End_Hours > def:End_Work_Hours
!                                    End !If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
!
!                                End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
!                            End!If f_hours <> ''
!
!                            job:Status_End_Time = tmp:End_Hours
!                            job:status_end_date = tmp:End_Days
!                        Else!If sts:use_turnaround_time = 'YES'
!                            job:status_end_date = Deformat('1/1/2050',@d6)
!                            job:status_end_Time = Clock()
!                        End!If sts:use_turnaround_time  = 'YES'
!                        Do EmailCheckAndSend
!                        !End   - Only update status details, if different from current status - TrkBs: 4761 (DBH: 14-02-2005)
!
!                    End !If sts:Status <> job:Current_Status
!
!                End!IF access:status.tryfetch(sts:ref_number_only_key)
!
!                !Start - Make sure the web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!                If WobAvailableFlag = true
!                    wob:Current_Status = job:Current_Status
!                End ! If WobAvailableFlag = true
!                !End   - Make sure the web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!
!            End!If sterror# = 0
!
!        Of 'EXC'
!
!            previous_status"    = job:exchange_status
!            access:status.clearkey(sts:ref_number_only_key)
!            sts:ref_number    = f_number
!            IF access:status.tryfetch(sts:ref_number_only_key)
!                job:exchange_status    = 'ERROR (' & Clip(f_number) & ')'
!            Else!IF access:status.tryfetch(sts:ref_number_only_key)
!! Changing (DBH 13/04/2007) # 8912 - Always update status history when status has changed
!!                job:exchange_status    = sts:status
!!                If previous_status" <> job:exchange_Status
!!                    !Start - Only update status details if changed - TrkBs: 4761 (DBH: 14-02-2005)
!!                    Access:AUDSTATS.Open()
!!                    Access:AUDSTATS.Usefile()
!!                    If Access:AUDSTATS.Primerecord() = Level:Benign
!!                        aus:RefNumber    = job:Ref_Number
!!                        aus:Type         = 'EXC'
!!                        aus:DateChanged  = Today()
!!                        aus:TimeChanged  = Clock()
!!                        aus:OldStatus    = previous_status"
!!                        aus:NewStatus    = job:Exchange_Status
!!                        aus:UserCode     = use:User_Code
!!                        If Access:AUDSTATS.Tryinsert()
!!                            Access:AUDSTATS.Cancelautoinc()
!!                        End!If Access:AUDSTATS.Tryinsert()
!!                    End!If Access:AUDSTATS.Primerecord() = Level:Benign
!!                    Access:AUDSTATS.Close()
!! to (DBH 13/04/2007) # 8912
!                If sts:Status <> job:Exchange_Status
!                    job:Exchange_Status = sts:Status
!                    Relate:AUDSTATS.Open()
!                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
!                        access:users.clearkey(use:password_key)
!                        use:password    = glo:password
!                        access:users.fetch(use:password_key)
!
!                        aus:RefNumber    = job:Ref_Number
!                        aus:Type         = 'EXC'
!                        aus:DateChanged  = Today()
!                        aus:TimeChanged  = Clock()
!                        aus:OldStatus    = previous_status"
!                        aus:NewStatus    = job:Exchange_Status
!                        aus:UserCode     = use:User_Code
!                        If Access:AUDSTATS.TryInsert() = Level:Benign
!                            !Insert
!                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
!                            Access:AUDSTATS.CancelAutoInc()
!                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
!                    End ! If Access.AUDSTATS.PrimeRecord() = Level:Benign
!                    Relate:AUDSTATS.Close()
!                End ! If sts:Status <> job:Exchange_Status
!! End (DBH 13/04/2007) #8912
!                !Update the webjob record
!                if WobAvailableFlag = true
!                    wob:Exchange_Status_Date = today()
!                end !if wob available
!                Do EmailCheckAndSend
!                !End   - Only update status details if changed - TrkBs: 4761 (DBH: 14-02-2005)
!
!!                End!If job:PreviousStatus <> job:Current_Status
!            End!IF access:status.tryfetch(sts:ref_number_only_key)
!
!            !Start - Make sure the web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!            if WobAvailableFlag = true
!                wob:Exchange_Status = job:Exchange_Status
!            end !if wob available
!            !End   - Make sure the web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!
!
!        Of 'LOA'
!            previous_status"    = job:loan_status
!            access:status.clearkey(sts:ref_number_only_key)
!            sts:ref_number    = f_number
!            IF access:status.tryfetch(sts:ref_number_only_key)
!                job:loan_status    = 'ERROR (' & Clip(f_number) & ')'
!            Else!IF access:status.tryfetch(sts:ref_number_only_key)
!! Changing (DBH 13/04/2007) # 8912 - Always make sure the status history is updated
!!                job:loan_status    = sts:status
!!
!!                If previous_status" <> job:loan_status
!!                    !Start - Check web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!!                    Access:AUDSTATS.Open()
!!                    Access:AUDSTATS.Usefile()
!!                    If Access:AUDSTATS.Primerecord() = Level:Benign
!!                        aus:RefNumber    = job:Ref_Number
!!                        aus:Type         = 'LOA'
!!                        aus:DateChanged  = Today()
!!                        aus:TimeChanged  = Clock()
!!                        aus:OldStatus    = previous_status"
!!                        aus:NewStatus    = job:Loan_Status
!!                        aus:UserCode     = use:User_Code
!!                        If Access:AUDSTATS.Tryinsert()
!!                            Access:AUDSTATS.Cancelautoinc()
!!                        End!If Access:AUDSTATS.Tryinsert()
!!                    End!If Access:AUDSTATS.Primerecord() = Level:Benign
!!                    Access:AUDSTATS.Close()
!! to (DBH 13/04/2007) # 8912
!                If sts:Status <> job:Loan_Status
!                    job:Loan_Status = sts:Status
!                    Relate:AUDSTATS.Open()
!                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
!                        access:users.clearkey(use:password_key)
!                        use:password    = glo:password
!                        access:users.fetch(use:password_key)
!
!                        aus:RefNumber    = job:Ref_Number
!                        aus:Type         = 'LOA'
!                        aus:DateChanged  = Today()
!                        aus:TimeChanged  = Clock()
!                        aus:OldStatus    = previous_status"
!                        aus:NewStatus    = job:Loan_Status
!                        aus:UserCode     = use:User_Code
!                        If Access:AUDSTATS.TryInsert() = Level:Benign
!                            !Insert
!                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
!                            Access:AUDSTATS.CancelAutoInc()
!                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
!                    End ! If Access.AUDSTATS.PrimeRecord() = Level:Benign
!                End ! If sts:Status <> job:Loan_Status
!! End (DBH 13/04/2007) #8912
!                if WobAvailableFlag = true
!                    wob:Loan_Status_Date = today()
!                end!if wobAvailable
!                Do EmailCheckAndSend
!                !End   - Check web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!!                End!If job:PreviousStatus <> job:Current_Status
!
!            End!IF access:status.tryfetch(sts:ref_number_only_key)
!            !Start - Make sure the web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!            if WobAvailableFlag = true
!                wob:Loan_Status = job:Loan_Status
!            end!if wobAvailable
!            !End   - Make sure the web status is up to date - TrkBs: 4761 (DBH: 14-02-2005)
!
!        Of '2NE' !Second Exchange Status
!
!            !Change the Second Exchange Status
!            !Will have to assume that JOBSE is open and they we
!            !have already got the record -  (DBH: 19-12-2003)
!            Previous_Status"    = jobe:SecondExchangeStatus
!            Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
!            sts:Ref_Number  = f_Number
!            If Access:STATUS.Tryfetch(sts:Ref_Number_Only_Key) = Level:Benign
!                !Found
!! Changing (DBH 13/04/2007) # 8912 - Always make sure the status history is updated when there is a change
!!                jobe:SecondExchangeStatus = sts:Status
!!                If Previous_Status" <> jobe:SecondExchangeStatus
!!                    Access:AUDSTATS.Open()
!!                    Access:AUDSTATS.Usefile()
!!                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
!!                        aus:RefNumber       = job:Ref_Number
!!                        aus:Type            = '2NE'
!!                        aus:DateChanged     = Today()
!!                        aus:TimeChanged     = Clock()
!!                        aus:OldStatus       = Previous_Status"
!!                        aus:NewStatus       = jobe:SecondExchangeStatus
!!                        aus:UserCode        = use:User_Code
!!                        If Access:AUDSTATS.TryInsert() = Level:Benign
!!                            !Insert Successful
!!
!!                        Else !If Access:AUDSTATS.TryInsert() = Level:Benign
!!                            !Insert Failed
!!                            Access:AUDSTATS.CancelAutoInc()
!!                        End !If Access:AUDSTATS.TryInsert() = Level:Benign
!!                    End !If Access:AUDSTATS.PrimeRecord() = Level:Benign
!!                    Access:AUDSTATS.Close()
!!                End !If Previous_Status" <> jobe:SecondExchangeStatus
!! to (DBH 13/04/2007) # 8912
!                If sts:Status <> jobe:SecondExchangeStatus
!                    jobe:SecondExchangeStatus = sts:Status
!                    Relate:AUDSTATS.Open()
!                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
!                        access:users.clearkey(use:password_key)
!                        use:password    = glo:password
!                        access:users.fetch(use:password_key)
!
!                        aus:RefNumber       = job:Ref_Number
!                        aus:Type            = '2NE'
!                        aus:DateChanged     = Today()
!                        aus:TimeChanged     = Clock()
!                        aus:OldStatus       = Previous_Status"
!                        aus:NewStatus       = jobe:SecondExchangeStatus
!                        aus:UserCode        = use:User_Code
!                        If Access:AUDSTATS.TryInsert() = Level:Benign
!                            !Insert
!                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
!                            Access:AUDSTATS.CancelAutoInc()
!                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
!                    End ! If Access.AUDSTATS.PrimeRecord() = Level:Benign
!                End ! If sts:Status <> jobe:SecondExchangeStatus
!! End (DBH 13/04/2007) #8912
!            Else ! If Access:STATUS.Tryfetch(sts:Ref_Number_Key) = Level:Benign
!                !Error
!                jobe:SecondExchangeStatus   = 'ERROR (' & Clip(f_number) & ')'
!            End !If Access:STATUS.Tryfetch(sts:Ref_Number_Key) = Level:Benign
!            Access:JOBSE.Update()
!    End!Case f_type
!
!    if WobAvailableFlag = true
!        if access:webjob.update() then
!            !message('error on update')
!        End
!    end!if wob available
!
!    Access:webjob.Close()
!
!
!    ! Insert --- Open and close so don't have to elsewhere (DBH: 18/03/2009) #10473
!    Relate:STATUS.Close()
!    Relate:STAHEAD.Close()
!    Relate:JOBSTAGE.Close()
!    Relate:DEFAULTS.Close()
!    ! end --- (DBH: 18/03/2009) #10473
!EmailText       Routine
!    tmp:EmailMessage = 'ServiceBase 2000 Cellular - Automated Email <13,10>'
!    tmp:EmailMessage = Clip(tmp:EmailMessage) & 'Your Job Reference: ' & job:Ref_Number & '<13,10>'
!    If sts:RefManufacturer
!        tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>Manufacturer: ' & job:Manufacturer
!    End !If sts:RefManufacturer
!    If sts:RefModelNumber
!        tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>Model Number: ' & job:Model_Number
!    End !If sts:RefManufacturer
!    If sts:RefLabourCost
!        tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>Labour Cost (NET): ' & Format(job:Labour_Cost,@n14.2)
!    End !If sts:RefManufacturer
!    If sts:RefPartsCost
!        tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>Parts Cost (NET): ' & Format(job:Parts_Cost,@n14.2)
!    End !If sts:RefManufacturer
!    If sts:RefTotalCost
!        tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>Total Cost (INC): ' & Format(job:Sub_Total,@n14.2)
!    End !If sts:RefManufacturer
!    If sts:RefEstCost
!        tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>Total Estimated Cost (INC): ' & Format(job:Sub_Total_Estimate,@n14.2)
!    End !If sts:RefManufacturer
!    tmp:EmailMessage = Clip(tmp:EmailMessage) & '<13,10>' & Clip(sts:EmailBody) & '<13,10,13,10>' & Clip(sts:EmailFooter)
!AddContactHistory       Routine
!    If Access:CONTHIST.PrimeRecord() = Level:Benign
!        cht:Ref_Number = job:Ref_Number
!        cht:Date       = Today()
!        cht:Time       = Clock()
!        Access:USERS.Clearkey(use:Password_Key)
!        use:Password    = glo:password
!        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!            !Found
!
!        Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!
!        cht:User       = use:User_Code
!        cht:Action     = 'AUTOMATIC EMAIL SENT'
!        cht:Notes      = 'To: ' & CLip(tmp:CCList) & '<13,10>' &|
!                        'Subject: ' & Clip(sts:EmailSubject) & '<13,10>' &|
!                        'From: ' & Clip(sts:SenderEmailAddress) & '<13,10,13,10>' & Clip(tmp:EmailMessage)
!        cht:SystemHistory   = 1
!        If Access:CONTHIST.TryInsert() = Level:Benign
!            !Insert Successful
!        Else !If Access:CONTHIST.TryInsert() = Level:Benign
!            !Insert Failed
!        End !If Access:CONTHIST.TryInsert() = Level:Benign
!    End !If Access:CONTHIST.PrimeRecord() = Level:Benign
!EmailCheckAndSend     Routine
!    !Does the status want a email sent?
!    If sts:EnableEmail
!        Relate:TRADEACC.Open()
!        Relate:STARECIP.Open()
!        !Does the trade account want an email sent?
!        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!        sub:Account_Number  = job:Account_Number
!        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!            !Found
!            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!            tra:Account_Number  = sub:Main_Account_Number
!            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                !Found
!                If tra:Use_Sub_Accounts = 'YES'
!                    If sub:AutoSendStatusEmails
!                        Do EmailText
!
!                        If sub:EmailRecipientList
!
!                            !Need to email Recipients, so get through the
!                            !status recipients and match up with the trade ones, and
!                            !email to those people
!                            tmp:CCList = ''
!                            Save_str_ID = Access:STARECIP.SaveFile()
!                            Access:STARECIP.ClearKey(str:RecipientTypeKey)
!                            str:RefNumber     = sts:Ref_Number
!                            Set(str:RecipientTypeKey,str:RecipientTypeKey)
!                            Loop
!                                If Access:STARECIP.NEXT()
!                                   Break
!                                End !If
!                                If str:RefNumber     <> sts:Ref_Number      |
!                                    Then Break.  ! End If
!                                Access:SUBEMAIL.ClearKey(sue:RecipientTypeKey)
!                                sue:RefNumber     = sub:RecordNumber
!                                sue:RecipientType = str:RecipientType
!                                If Access:SUBEMAIL.TryFetch(sue:RecipientTypeKey) = Level:Benign
!                                    !Found
!                                    If tmp:CCList = ''
!                                        tmp:CCList = Clip(sue:ContactName) & '<' & Clip(sue:EmailAddress) & '>'
!                                    Else !If tmp:CCList = ''
!                                        tmp:CCList = Clip(tmp:CCList) & ',' & Clip(sue:ContactName) & '<' & Clip(sue:EmailAddress) & '>'
!                                    End !If tmp:CCList = ''
!
!                                Else!If Access:SUBEMAIL.TryFetch(sue:RecipientTypeKey) = Level:Benign
!                                    !Error
!                                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                                End!If Access:SUBEMAIL.TryFetch(sue:RecipientTypeKey) = Level:Benign
!                            End !Loop
!                            Access:STARECIP.RestoreFile(Save_str_ID)
!
!                            SendEmail(sts:SenderEmailAddress, tmp:CCList,sts:EmailSubject, |
!                                        '', '', '', tmp:EmailMessage)
!
!                        End !If sub:EmailRecipientList
!                        If sub:EmailEndUser
!                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
!                            jobe:RefNumber   = job:Ref_Number
!                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                                !Found
!
!                                SendEmail(sts:SenderEmailAddress,Clip(job:Company_Name_Delivery) & '<' & Clip(jobe:EndUserEmailAddress) & '>',|
!                                        sts:EmailSubject,'','','',tmp:EmailMessage)
!                                tmp:CCList = CLip(tmp:CCList) & ',' & Clip(job:Company_Name_Delivery) & '<' & Clip(jobe:EndUserEmailAddress) & '>'
!                            Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                        End !If tra:EmailEndUser
!
!                        Do AddContactHistory
!                    End !If sub:AutoSendStatusEmails
!
!                Else !If tra:Use_Sub_Accounts
!                    If tra:AutoSendStatusEmails
!                        Do EmailText
!
!                        If tra:EmailRecipientList
!                            !Need to email Recipients, so get through the
!                            !status recipients and match up with the trade ones, and
!                            !email to those people
!                            tmp:CCList = ''
!                            Save_str_ID = Access:STARECIP.SaveFile()
!                            Access:STARECIP.ClearKey(str:RecipientTypeKey)
!                            str:RefNumber     = sts:Ref_Number
!                            Set(str:RecipientTypeKey,str:RecipientTypeKey)
!                            Loop
!                                If Access:STARECIP.NEXT()
!                                   Break
!                                End !If
!                                If str:RefNumber     <> sts:Ref_Number      |
!                                    Then Break.  ! End If
!                                Access:TRAEMAIL.ClearKey(tre:RecipientKey)
!                                tre:RefNumber     = tra:RecordNumber
!                                tre:RecipientType = str:RecipientType
!                                If Access:TRAEMAIL.TryFetch(tre:RecipientKey) = Level:Benign
!                                    !Found
!                                    If tmp:CCList = ''
!                                        tmp:CCList = Clip(tre:ContactName) & '<' & Clip(tre:EmailAddress) & '>'
!                                    Else !If tmp:CCList = ''
!                                        tmp:CCList = Clip(tmp:CCList) & ',' & Clip(tre:ContactName) & '<' & Clip(tre:EmailAddress) & '>'
!                                    End !If tmp:CCList = ''
!
!
!                                Else!If Access:TRAEMAIL.TryFetch(tre:RecipientKey) = Level:Benign
!                                    !Error
!                                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                                End!If Access:TRAEMAIL.TryFetch(tre:RecipientKey) = Level:Benign
!                            End !Loop
!                            Access:STARECIP.RestoreFile(Save_str_ID)
!
!                            SendEmail(sts:SenderEmailAddress, tmp:CCList,sts:EmailSubject, |
!                                        '', '', '', tmp:EmailMessage)
!                        End !If tra:EmailRecipientList
!                        If tra:EmailEndUser
!                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
!                            jobe:RefNumber   = job:Ref_Number
!                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                                !Found
!                                SendEmail(sts:SenderEmailAddress,Clip(job:Company_Name_Delivery) & '<' & Clip(jobe:EndUserEmailAddress) & '>',|
!                                        sts:EmailSubject,'','','',tmp:EmailMessage)
!                                tmp:CCList = CLip(tmp:CCList) & ',' & Clip(job:Company_Name_Delivery) & '<' & Clip(jobe:EndUserEmailAddress) & '>'
!                            Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                        End !If tra:EmailEndUser
!
!                        Do AddContactHistory
!                    End !If tra:AutoSendStatusEmails
!                End !If tra:Use_Sub_Accounts
!            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!
!        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!        Relate:STARECIP.Close()
!        Relate:TRADEACC.Close()
!    End !If sts:EnableEmail
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
TurnaroundTime       PROCEDURE  (f_number,f_type)     ! Declare Procedure
clock_temp           TIME
tmp:end_days         LONG
tmp:end_hours        LONG
start_time_temp      TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(Defaults)
    access:defaults.next()
    tmp:end_days = Today()
    tmp:end_hours = Clock()

    Case f_type
        Of 'D' !Return Days

            x# = 0
            count# = 0
            If f_number <> 0
                Loop
                    count# += 1
                    day_number# = (Today() + count#) % 7
                    If def:include_saturday <> 'YES'
                        If day_number# = 6
                            tmp:end_days += 1
                            Cycle
                        End
                    End
                    If def:include_sunday <> 'YES'
                        If day_number# = 0
                            tmp:end_days += 1
                            Cycle
                        End
                    End
                    tmp:end_days += 1
                    x# += 1
                    If x# >= f_number
                        Break
                    End!If x# >= sts:turnaround_days
                End!Loop
            End!If f_days <> ''
            Return(tmp:end_days)
        Of 'T' !Return Time
            If f_number <> 0
                start_time_temp = Clock()
                new_day# = 0
                Loop x# = 1 To f_number
                    clock_temp = start_time_temp + (x# * 360000)
                    If def:start_work_hours <> '' And def:end_work_hours <> ''
                        If clock_temp < def:start_work_hours Or clock_temp > def:end_work_hours
                            tmp:end_hours = def:start_work_hours
                            start_time_temp = def:start_work_hours
                            tmp:end_days += 1
                        End!If clock_temp > def:start_work_hours And clock_temp < def:end_work_hours
                    End!If def:start_work_hours <> '' And def:end_work_hours <> ''
                    tmp:end_hours += 360000
                End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
            End!If f_hours <> ''
            Return(tmp:end_hours)
    End!Case f_type

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ShowText PROCEDURE (f_number,f_type)                  !Generated from procedure template - Window

LocalRequest         LONG
tmp:text             STRING(255)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Invoice Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Invoice Text'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel1),FILL(09A6A7CH)
                       TEXT,AT(202,150,276,130),USE(tmp:text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020013'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ShowText')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBNOTES_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  access:jobnotes_alias.clearkey(jbn_ali:refnumberkey)
  jbn_ali:refnumber = f_number
  if access:jobnotes_alias.tryfetch(jbn_ali:refnumberkey) = Level:Benign
      Case f_type
          Of 'FAULT'
              tmp:text    = jbn_ali:fault_description
              ?WindowTitle{prop:Text} = 'Fault Description'
          Of 'INVOICE'
              tmp:text    = jbn_ali:invoice_text
              ?WindowTitle{prop:Text} = 'Invoice Text'
          Of 'NOTES'
              tmp:text    = jbn_ali:engineers_notes
              ?WindowTitle{prop:Text} = 'Engineer''s Notes'
      End!Case f_type
  End!if access:jobnotes_alias.tryfetch(jbn_ali:refnumberkey) = Level:Benign
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020013'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020013'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020013'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ToBeExchanged        PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Will this or does this job have an exchange unit on it?
!1 = Exchange unit is already attached to the job
!2 = Exchange unit is NOT attached, but the transit type is marked 'Exchange Unit Required'
!3 = Exchange unit is NOT attached, but the Exchange Status is 108 Exchange Unit Required
    If job:exchange_unit_number <> ''
        Return 1
    End!If job:exchange_unit_number <> ''

    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        If trt:exchange_unit = 'YES'
            Return 2
        End!If trt:exchange = 'YES'
    End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign

    If Sub(job:exchange_status,1,3) = '108'
        Return 3
    End!If Sub(job:exchange_status,1,3) = '108'

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CheckLength          PROCEDURE  (f_type,f_ModelNumber,f_number,F_Silent) ! Declare Procedure
tmp:LengthFrom       LONG
tmp:LengthTo         LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!tb12423 - needs a silent mode so the messages do not appear - introduced f_Silent byte=0 on call

    If f_Type = 'MOBILE'
        tmp:LengthFrom = GETINI('COMPULSORY','MobileLengthFrom',,Clip(Path()) & '\SB2KDEF.INI')
        tmp:LengthTo   = GETINI('COMPULSORY','MobileLengthTo',,Clip(Path()) & '\SB2KDEF.INI')
        If Len(Clip(f_Number)) < tmp:LengthFrom Or |
            Len(Clip(f_Number)) > tmp:LengthTo

            If tmp:LengthFrom = tmp:LengthTo
                if F_Silent
                    glo:Errortext ='The Mobile Number should be ' & Clip(tmp:LengthFrom) & ' characters in length.'
                ELSE
                    Case Missive('The selected Mobile Number should be ' & Clip(tmp:LengthFrom) & ' characters in length.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                END !if F_Silent =false
            Else ! If tmp:LengthFrom = tmp:LengtTo
                if F_Silent
                    glo:ErrorText = 'The Mobile Number should be between ' & Clip(tmp:LengthFrom) & ' and ' & Clip(tmp:LengthTo) & ' characters in length.'
                ELSE
                    Case Missive('The selected Mobile Number should be between ' & Clip(tmp:LengthFrom) & ' and ' & Clip(tmp:LengthTo) & ' characters in length.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                END !if F_Silent =false
            End ! If tmp:LengthFrom = tmp:LengtTo
            Return Level:Fatal
        End ! Len(Clip(f_Number)) > tmp:LengthTo

    Else ! If f_Type = 'MOBILE'

        !Return The Correct Length Of A Model Number
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = f_ModelNumber
        if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
            If f_number <> 'N/A'
                Case f_type
                    Of 'IMEI'
                        If Len(Clip(f_number)) < mod:esn_length_from Or Len(Clip(f_number)) > mod:esn_length_to
                            If mod:esn_length_to = mod:esn_length_from
                                if F_Silent
                                    glo:ErrorText = 'I.M.E.I. Number should be ' & Clip(mod:ESN_Length_From) & ' characters in length.'
                                ELSE
                                    Case Missive('The selected I.M.E.I. Number should be ' & Clip(mod:ESN_Length_From) & ' characters in length.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                    End ! Case Missive
                                END !if F_Silent =false
                            Else!If mod:esn_length_to = mod:esn_length_from
                                if F_Silent
                                    glo:ErrorText = 'I.M.E.I. Number should be between ' & Clip(mod:ESN_Length_From) & ' and ' & Clip(mod:ESN_Length_To) & ' characters in length.'
                                ELSE
                                    Case Missive('The selected I.M.E.I. Number should be between ' & Clip(mod:ESN_Length_From) & ' and ' & Clip(mod:ESN_Length_To) & ' characters in length.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                    End ! Case Missive
                                END !if F_Silent =false

                            End!If mod:esn_length_to = mod:esn_length_from

                            Return Level:Fatal
                        End!If Clip(Len(f_number)) < mod:esn_length_from Or Clip(Len(f_number)) > mod:esn_length_to

                    Of 'MSN'
                        If Len(Clip(f_number)) < mod:msn_length_from Or len(clip(f_number)) > mod:msn_length_to
                            if mod:msn_length_to = mod:msn_length_from
                                if F_Silent
                                    glo:ErrorText = 'M.S.N. Number should be ' & Clip(mod:MSN_Length_From) & ' characters in length.'
                                ELSE
                                    Case Missive('The selected M.S.N. Number should be ' & Clip(mod:MSN_Length_From) & ' characters in length.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                    End ! Case Missive
                                END !if F_Silent =false

                            Else!if mod:msn_length_to = mod:msn_length_from
                                if F_Silent
                                    glo:ErrorText = 'M.S.N. Number should be between ' & Clip(mod:MSN_Length_From) & ' and ' & Clip(mod:MSN_Length_To) & ' characters in length.'
                                ELSE
                                    Case Missive('The selected M.S.N. Number should be between ' & Clip(mod:MSN_Length_From) & ' and ' & Clip(mod:MSN_Length_To) & ' characters in length.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                    End ! Case Missive
                                END !if F_Silent =false
                            End!if mod:msn_length_to = mod:msn_length_from
                            Return Level:Fatal
                        End!If Clip(Len(f_number)) < mod:esn_length_from
                End!Case f_type
            End!If f_number <> 'N/A'
        End!if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
    End ! If f_Type = 'MOBILE'

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CheckPaid            PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If job:date_completed = ''
        Return Level:Fatal
    Else!If job:date_completed = ''
        Total_Price('C',vat",total",balance")
        If total" = 0
            Return Level:Fatal
        Else!If total" = 0
            If balance" <= 0
                Return Level:Benign
            End!If balance" = 0
            If balance" > 0
                Return Level:Fatal
            End!If balance" < 0
        End!If total" = 0
    End!If job:date_completed = ''

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
