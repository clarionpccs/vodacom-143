

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01016.INC'),ONCE        !Local module procedure declarations
                     END


ForceFaultDescription PROCEDURE  (func:Type)          ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Fault Description
    If (def:Force_Fault_Description = 'B' and func:Type = 'B') Or |
        (def:Force_Fault_Description <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Fault_Description = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceInvoiceText     PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Invoice Text
    If (def:Force_Invoice_Text = 'B' And func:Type = 'B') Or |
        (def:Force_Invoice_Text <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Invoice_Text = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceRepairType      PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Repair Type
    If (def:Force_Repair_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Repair_Type <> 'I' And (func:Type = 'C' OR func:Type = 'T'))    ! #12363 Force Repair Type For Third Party Warranty (DBH: 29/02/2012)
        Return Level:Fatal
    End!If def:Force_Repair_Type = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceCourier         PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Incoming Courier
    If (def:Force_Outoing_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Outoing_Courier <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Incoming_Courier = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceFaultCodes      PROCEDURE  (func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !(func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType)

    If func:Type = 'C' or func:type = 'X'
        If func:ChargeableJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:CChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:CRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = job:Manufacturer
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = func:CRepairType
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
! Deleted (DBH 20/05/2006) #6733 - Do not check the manufacturer for char fault codes
!                                 Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!                                 man:Manufacturer    = job:Manufacturer
!                                 If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Found
!                                     If man:ForceCharFaultCodes
!                                         Return Level:Fatal
!                                     End !If man:ForceCharFaultCodes
!                                 Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Error
!                                 End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
! End (DBH 20/05/2006) #6733
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                    Else !If func:CRepairType <> ''
                        Return Level:Fatal
                    End !If func:CRepairType <> ''
                End !If cha:Force_Warranty = 'YES'
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Chargeable_Job = 'YES'
    End !If func:Type = 'C' or func:type = 'X'

    If func:Type = 'W' or func:Type = 'X'
        If func:WarrantyJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:WChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:WRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = job:Manufacturer
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = func:WRepairType
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                    Else !If WRetairType <> ''
                        Return Level:Fatal
                    End !If WRetairType <> ''
                End !If cha:Force_Warranty = 'YES'

            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Warranty_Job = 'YES'
    End !If func:Type = 'W' or func:Type = 'X'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceAccessories     PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B' And func:Type = 'B') Or |
        (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
        Return Level:Fatal
    End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
NoAccessories        PROCEDURE  (func:RefNumber)      ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    AccError# = 0
    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:Ref_Number = func:RefNumber
    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
    If Access:JOBACC.Next()
        Return Level:Fatal
    Else !If Access:JOBACC.Next()
        If jac:Ref_Number <> func:RefNUmber
            Return Level:Fatal
        End !If jac:Ref_Number <> job:Ref_Number
    End !If Access:JOBACC.Next()
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
NoFaultCodes         PROCEDURE  (func:Type)           ! Declare Procedure
save_maf_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_maf_ID = Access:MANFAULT.SaveFile()
    Access:MANFAULT.ClearKey(maf:Field_Number_Key)
    maf:Manufacturer = job:Manufacturer
    Set(maf:Field_Number_Key,maf:Field_Number_Key)
    Loop
        If Access:MANFAULT.NEXT()
           Break
        End !If
        If maf:Manufacturer <> job:Manufacturer |
            Then Break.  ! End If
        Case maf:Field_Number
            Of 1
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code1 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 2
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code2 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 3
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code3 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 4
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code4 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 5
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code5 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 6
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code6 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 7
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code7 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 8
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code8 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 9
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code9 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 10
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code10 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 11
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code11 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 12
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If job:Fault_Code12 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 13
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode13 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 14
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode14 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 15
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode15 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 16
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode16 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 17
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode17 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 18
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode18 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 19
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode19 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 20
                If (maf:Compulsory_At_Booking = 'YES') Or |
                    (maf:Compulsory = 'YES' and func:Type = 'C')
                    If wob:FaultCode20 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

        End !Case maf:Field
    End !Loop
    Access:MANFAULT.RestoreFile(Save_maf_ID)

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
NoPartFaultCodes     PROCEDURE  (func:Type)           ! Declare Procedure
save_map_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_map_ID = Access:MANFAUPA.SaveFile()
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.NEXT()
           Break
        End !If
        If map:Manufacturer <> job:Manufacturer |
            Then Break.  ! End If
        Case map:Field_Number
            Of 1
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code1 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |
            Of 2
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code2 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 3
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code3 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 4
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code4 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 5
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code5 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 6
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code6 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 7
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code7 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 8
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code8 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 9
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code9 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 10
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code10 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 11
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code11 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

            Of 12
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code12 = ''
                        Return Level:Fatal
                    End !If job:Fault_Code1 = ''
                End !If (maf:Compulsory_At_Booking = 'YES') Or |

        End !Case maf:Field
    End !Loop
    Access:MANFAUPA.RestoreFile(Save_map_ID)

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceNetwork         PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 'B' and func:Type = 'B') Or |
        (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') <> 'I' and func:Type = 'C')
        Return Level:Fatal
    Else !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Return Level:Benign
    End !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
