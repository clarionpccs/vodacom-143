

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01007.INC'),ONCE        !Local module procedure declarations
                     END


SuspendStock PROCEDURE (fLocation)                    !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
locLocation          STRING(30)
locTag               STRING(1)
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Manufacturer)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Location)
                       PROJECT(sto:Suspend)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
locTag                 LIKE(locTag)                   !List box control field - type derived from local data
locTag_Icon            LONG                           !Entry's icon ID
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Manufacturer       LIKE(sto:Manufacturer)         !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::sto:Location        LIKE(sto:Location)
HK12::sto:Suspend         LIKE(sto:Suspend)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse The Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('Suspend Parts'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:000000'),AT(468,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       LIST,AT(168,116,344,212),USE(?Browse:1),IMM,HVSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@80L(2)|M~Part Number~@s30@80L(2)|M~Description~@s30@32R(2)|M~Quantity' &|
   ' In Stock~@N8@80L(2)|M~Supplier~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                       PANEL,AT(164,334,352,26),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,102,124,10),USE(sto:Part_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON('&Rev tags'),AT(291,185,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(276,202,68,12),USE(?DASSHOWTAG),HIDE
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,102,124,10),USE(sto:Description),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,334),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(368,334),USE(?buttonSuspendTaggedParts),TRN,FLAT,ICON('sustagp.jpg')
                       BUTTON,AT(164,334),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(232,334),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(300,334),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = sto:Ref_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sto:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    locTag = '*'
  ELSE
    DELETE(glo:Queue)
    locTag = ''
  END
    Queue:Browse:1.locTag = locTag
  IF (locTag = '*')
    Queue:Browse:1.locTag_Icon = 2
  ELSE
    Queue:Browse:1.locTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::11:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sto:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::11:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::11:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::11:QUEUE = glo:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = sto:Ref_Number
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sto:Ref_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020744'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SuspendStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Access:STOAUDIT.UseFile
  SELF.FilesOpened = True
  locLocation = fLocation
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sto:LocDescSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sto:LocPartSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BIND('locTag',locTag)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(locTag,BRW1.Q.locTag)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Manufacturer,BRW1.Q.sto:Manufacturer)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Suspend,BRW1.Q.sto:Suspend)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020744'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020744'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020744'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?buttonSuspendTaggedParts
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendTaggedParts, Accepted)
          If (Records(glo:Queue) = 0)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You have not tagged any parts.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              Cycle
          End
      
          Beep(Beep:SystemQuestion)  ;  Yield()
          Case Missive('Are you sure you want to suspend the tagged parts for all Sites?','ServiceBase',|
                         'mquest.jpg','\&No|/&Yes')
          Of 2 ! &Yes Button
          Of 1 ! &No Button
              Cycle
          End!Case Message
      
          Prog.ProgressSetup(Records(glo:Queue))
      
          Loop x# = 1 to Records(glo:Queue)
              get(glo:Queue,x#)
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = glo:Pointer
              If (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                  If (Prog.InsideLoop())
                      Break
                  End
                  ! Double Check
                  if (sto:Suspend)
                      Cycle
                  end
                  if (sto:Quantity_Stock > 0)
                      Cycle
                  end
                  if (stockInUse(0))
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('Part ' & Clip(sto:part_Number) & ' not updated. It is in use.','ServiceBase',|
                                     'mstop.jpg','/&OK')
                      Of 1 ! &OK Button
                      End!Case Message
                      cycle
                  end
      
                  sto:Suspend = 1
                  If (Access:STOCK.TryUpdate() = Level:Benign)
                      If (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              sto:Purchase_Cost, |
                                              sto:Sale_Cost, |
                                              sto:Retail_Cost, |
                                              'PART SUSPENDED', |
                                              'AUTOMATED ROUTINE'))
                      End
                      Access:LOCATION.Clearkey(loc:RecordNumberKey)
                      loc:RecordNumber = 0
                      Set(loc:RecordNumberKey,loc:RecordNumberKey)
                      Loop Until Access:LOCATION.Next()
                          if (loc:Location = MainStoreLocation())
                              cycle
                          end
                          if (loc:Active = 0)
                              Cycle
                          end
      
                          Access:STOCK_ALIAS.Clearkey(sto_ali:Location_Key)
                          sto_ali:Location = loc:Location
                          sto_ali:Part_Number = sto:Part_Number
                          If (Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign)
                              if (sto_ali:Quantity_Stock > 0)
                                  Cycle
                              end
                              if (sto_ali:Suspend)
                                  Cycle
                              end
                              If (StockAliasInUse(0))
                                  Beep(Beep:SystemHand)  ;  Yield()
                                  Case Missive('Part: ' & Clip(sto_ali:Part_Number) & ', Location: ' & Clip(sto_ali:Location) & ' was not updated. Item is in use.','ServiceBase',|
                                                 'mstop.jpg','/&OK')
                                  Of 1 ! &OK Button
                                  End!Case Message
                                  Cycle
                              End
                              sto_ali:Suspend = 1
                              If (Access:STOCK_ALIAS.TryUpdate() = Level:Benign)
                                  If (AddToStockHistory(sto_ali:Ref_Number, |
                                                          'ADD', |
                                                          '', |
                                                          0, |
                                                          0, |
                                                          0, |
                                                          sto_ali:Purchase_Cost, |
                                                          sto_ali:Sale_Cost, |
                                                          sto_ali:Retail_Cost, |
                                                          'PART SUSPENDED', |
                                                          'AUTOMATED ROUTINE: MAIN STORE PART SUSPENDED'))
                                  End
                              End
                          End
                      End
      
                  End
              End
          end
      
          Prog.ProgressFinish()
          brw1.ResetSort(1)
      
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('Done.','ServiceBase',|
                         'mexclam.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendTaggedParts, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sto:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      locTag = ''
    ELSE
      locTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (locTag = '*')
    SELF.Q.locTag_Icon = 2
  ELSE
    SELF.Q.locTag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if (sto:Quantity_Stock <> 0)
      return record:filtered
  end
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sto:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 100
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
PriceVariantCheck PROCEDURE (fPartNumber,fAveragePrice,fEnteredPrice) !Generated from procedure template - Window

locCurrentAveragePrice REAL
locEnteredPrice      REAL
locPercentage        REAL
locPassword          STRING(20)
rtnValue             BYTE
locPartNumber        STRING(30)
QuickWindow          WINDOW('Price Variant Check'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,66,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Price Variant Check'),AT(168,68),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:000000'),AT(468,68),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       SHEET,AT(164,80,352,252),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Current Average Purchase Cost:'),AT(224,128),USE(?Prompt3),FONT(,,,FONT:bold)
                           STRING(@n10.2),AT(395,128),USE(locCurrentAveragePrice),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Part Number:'),AT(224,107),USE(?Prompt3:3),FONT(,,,FONT:bold)
                           STRING(@s30),AT(307,107),USE(locPartNumber),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n10.2),AT(395,147),USE(locEnteredPrice),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('The difference in price is greater/less  than:'),AT(224,176),USE(?Prompt5),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n10.2~%~),AT(392,176),USE(locPercentage),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Insert Password To Continue'),AT(288,216),USE(?Prompt6),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Password'),AT(249,251),USE(?locPassword:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s20),AT(301,251,124,10),USE(locPassword),FONT(,,0101010H,FONT:bold),COLOR(COLOR:White),UPR,PASSWORD
                           PROMPT('Entered Purchase Price:'),AT(224,147),USE(?Prompt3:2),FONT(,,,FONT:bold)
                         END
                       END
                       PANEL,AT(164,334,352,26),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?Cancel),FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,334),USE(?OK),FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(rtnValue)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020746'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PriceVariantCheck')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  locCurrentAveragePrice = fAveragePrice
  locEnteredPrice = fEnteredPrice
  locPercentage = getini('STOCK','PartPriceVariant',,path() & '\SB2KDEF.INI')
  locPartNumber = fPartNumber
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      rtnValue = FALSE
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      if (locPassword = '')
          Cycle
      end
      
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = locPassword
      If (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level = use:User_Level
          acc:Access_Area = 'PART PRICE VARIANT OVERRIDE'
          If (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
              rtnValue = TRUE
          Else
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You do not have access to this option.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              Cycle
          End
      Else
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Invalid Password.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message   \
          Cycle
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020746'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020746'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020746'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReturnStockProcess PROCEDURE                          !Generated from procedure template - Window

locReturnType        STRING(30)
locInvoiceNumber     STRING(30)
locTRUE              BYTE(1)
locSalesNumber       STRING(30)
locQuantity          LONG
locNotes             STRING(255)
locDateReceived      DATE
FDB9::View:FileDrop  VIEW(RETTYPES)
                       PROJECT(rtt:Description)
                       PROJECT(rtt:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?locReturnType
rtt:Description        LIKE(rtt:Description)          !List box control field - type derived from field
rtt:RecordNumber       LIKE(rtt:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Stock Process'),AT(168,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,84,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Return Spares'),USE(?Tab1)
                           PROMPT('Stock Item To Return'),AT(228,106),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold)
                           STRING(@s30),AT(328,106),USE(sto:Part_Number),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           LIST,AT(328,134,124,10),USE(locReturnType),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),FORMAT('120L(2)|M@s30@'),DROP(10,120),FROM(Queue:FileDrop)
                           PROMPT('Invoice Number'),AT(228,162),USE(?locInvoiceNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(328,162,64,10),USE(locInvoiceNumber),FONT(,,010101H,,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                           PROMPT('Sale Number'),AT(228,186),USE(?locSalesNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(328,186,64,10),USE(locSalesNumber),SKIP,FONT(,,010101H,,CHARSET:ANSI),READONLY
                           PROMPT('Quantity'),AT(228,212),USE(?locQuantity:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                           SPIN(@n-14),AT(328,212,64,10),USE(locQuantity),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,RANGE(1,9999),STEP(1)
                           PROMPT(' {29}'),AT(400,212),USE(?promptQuantityMax),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Notes'),AT(228,238),USE(?locNotes:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                           TEXT,AT(328,238,168,58),USE(locNotes),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Return Type'),AT(228,134),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold)
                         END
                       END
                       BUTTON,AT(444,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(376,334),USE(?buttonReturnStock),TRN,FLAT,HIDE,ICON('retstokp.jpg')
                       PANEL,AT(164,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020756'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReturnStockProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?buttonReturnStock,RequestCancelled)
  Relate:RETSALES.Open
  Relate:RETTYPES.Open
  Relate:RTNORDER.Open
  Relate:STOCK.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:RETSTOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?locReturnType{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?locReturnType{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDB9.Init(?locReturnType,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:RETTYPES,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(rtt:ActiveDescriptionKey)
  FDB9.AddRange(rtt:Active,locTRUE)
  FDB9.AddField(rtt:Description,FDB9.Q.rtt:Description)
  FDB9.AddField(rtt:RecordNumber,FDB9.Q.rtt:RecordNumber)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
    Relate:RETTYPES.Close
    Relate:RTNORDER.Close
    Relate:STOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?buttonReturnStock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnStock, Accepted)
      IF (locQuantity = 0)
          SELECT(?locQuantity)
          CYCLE
      END
      IF (locReturnType = '')
          SELECT(?locReturnType)
          CYCLE
      END
      
      Access:RETTYPES.Clearkey(rtt:DescriptionKey)
      rtt:Description = locReturnType
      IF (Access:RETTYPES.TryFetch(rtt:DescriptionKey) = Level:Benign)
          IF (rtt:UseReturnDays)
              IF (locDateReceived + rtt:SparesReturnDays < TODAY())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Part cannot be returned as was received at Franchise over the set period.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
          END ! IF (rtt:UseReturnDays)
      END
      
      IF (locQuantity > sto:Quantity_Stock)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The quantity entered is greater than the quantity in stock.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      IF (StockInUse(0))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected Stock Item is in use. Please try again.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      sto:Quantity_Stock -= locQuantity
      IF (sto:Quantity_Stock < 0)
          sto:Quantity_Stock = 0
      END
      
      IF (Access:STOCK.TryUpdate() = Level:Benign)
          ! Does an item already exist for this order?
          Access:RTNORDER.Clearkey(rtn:OrderStatusReturnRefNoKey)
          rtn:OrderNumber = ret:Ref_Number
          rtn:ExchangeOrder = ret:ExchangeOrder
          rtn:ReturnType = locReturnType
          rtn:Status = 'NEW'
          rtn:RefNumber = sto:Ref_Number
          IF (Access:RTNORDER.TryFetch(rtn:OrderStatusReturnRefNoKey) = Level:Benign)
              rtn:QuantityReturned += locQuantity
              rtn:Notes = CLIP(rtn:Notes) & '<13,10>' & locNotes
              Access:RTNORDER.TryUpdate()
          ELSE
              ! Create Return Order
              IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
                  rtn:Location = glo:Location
                  rtn:UserCode = glo:UserCode
                  rtn:RefNumber = sto:Ref_Number
                  rtn:OrderNumber = ret:Ref_Number
                  rtn:InvoiceNumber = ret:Invoice_Number
                  rtn:PartNumber = sto:Part_Number
                  rtn:Description = sto:Description
                  rtn:QuantityReturned = locQuantity
                  rtn:ExchangeOrder = ret:ExchangeOrder
                  rtn:Status = 'NEW'
                  rtn:Notes = locNotes
                  rtn:ReturnType = locReturnType
                  rtn:PurchaseCost = sto:Purchase_Cost
                  rtn:SaleCost = sto:Sale_Cost
                  IF (Access:RTNORDER.TryInsert())
                      Access:RTNORDER.CancelAutoInc()
                  END
              END
          END
          AddStockHistory('DEC',locQuantity,'RETURN ORDER GENERATED')
      END
      RELEASE(STOCK)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnStock, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020756'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020756'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020756'&'0')
      ***
    OF ?locInvoiceNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locInvoiceNumber, Accepted)
      IF (NOT 0{prop:AcceptAll})
          locQuantity = 0
          locSalesNumber = ''
          ?promptQuantityMax{prop:Text} = ''
          ?buttonReturnStock{prop:Hide} = 1
          ! Lookup Retail Sales
          Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
          ret:Invoice_Number = locInvoiceNumber
          IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Unable to find the selected Invoice Number.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              DISPLAY()
              CYCLE
          END
      
          DoNormalCheck# = 0
          If glo:WebJob
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = Clarionet:Global.Param2
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:StoresAccount <> ''
                      If tra:StoresAccount <> ret:Account_Number
                          Error# = 1
                      End !If tra:StoresAccount <> ret:Account_Number
                  Else !If tra:Stores_Account <> ''
                      DoNormalCheck# = 1
                  End !If tra:Stores_Account <> ''
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
      
          Else !If glo:WebJob
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:StoresAccount <> ''
                      If tra:StoresAccount <> ret:Account_Number
                          Error# = 1
                      End !If tra:StoresAccount <> ret:Account_Number
                  Else !If tra:Stores_Account <> ''
                      DoNormalCheck# = 1
                  End !If tra:Stores_Account <> ''
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          End !If glo:WebJob
      
          If DoNormalCheck#
              !Look up Head Account!
              Access:SubTrAcc.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = ret:Account_Number
              IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                !Error!
              ELSE
                  Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = sub:Main_Account_Number
                  IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                      !Error!
                  ELSE
                      IF glo:Location <> tra:SiteLocation
                          Error# = 1
                      END
                  END
              END
      
          End !If DoNormalCheck#
      
          If Error# = 1
              Case Missive('This invoice number is not for this site location.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              
              DISPLAY()
              CYCLE
          END
      
          ! How many of this part were on the retail sale?
          locAvailableQty# = 0
          found# = 0
          Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
          res:Ref_Number = ret:Ref_Number
          res:Despatched = 'YES'
          res:Part_Number = sto:Part_Number
          SET(res:DespatchedPartKey,res:DespatchedPartKey)
          LOOP UNTIL Access:RETSTOCK.Next()
              IF (res:Ref_Number <> ret:Ref_Number OR |
                  res:Despatched <> 'YES' OR |
                  res:Part_Number <> sto:Part_Number)
                  BREAK
              END
              found# = 1
              IF (res:Received <> 1)
                  CYCLE
              END
              locAvailableQty# += res:QuantityReceived
              ! Don't think this should happen, but received dates could all be different, so pick the newest
              IF (locDateReceived < res:DateReceived)
                  locDateReceived = res:DateReceived
              END
      
          END
          IF (found# = 0)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected part is not on the entered invoice.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              DISPLAY()
              CYCLE
          END
      
          IF (locAvailableQty# = 0)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected order has not yet been received.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              DISPLAY()
              CYCLE
          END
      
          ! How many of this part have already been ordered
          locAvailableQty# -= ReturnUnitAlreadyOnOrder(sto:Ref_Number,0,ret:Ref_Number)
      
          ! Don't allow to return more than is in stock
          IF (locAvailableQty# > sto:Quantity_Stock)
              locAvailableQty# = sto:Quantity_Stock
          END
      
          IF (locAvailableQty# <= 0)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('All the available quantity for this part has already been returned.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          ELSE
              ?promptQuantityMax{prop:Text} = 'Qty Available To Return: ' & locAvailableQty#
              ?locQuantity{prop:RangeHigh} = locAvailableQty#
          END
      END
      locSalesNumber = ret:Ref_Number
      ?buttonReturnStock{prop:Hide} = 0
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locInvoiceNumber, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?locReturnType{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?locReturnType{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseReturnOrders PROCEDURE                          !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
locLocation          STRING(30)
locTag               STRING(1)
locWaybillNumber     LONG
locExchangeOrder     BYTE
BRW8::View:Browse    VIEW(RTNORDER)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:QuantityReturned)
                       PROJECT(rtn:OrderNumber)
                       PROJECT(rtn:InvoiceNumber)
                       PROJECT(rtn:Notes)
                       PROJECT(rtn:RecordNumber)
                       PROJECT(rtn:Location)
                       PROJECT(rtn:ExchangeOrder)
                       PROJECT(rtn:Status)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
locTag                 LIKE(locTag)                   !List box control field - type derived from local data
locTag_Icon            LONG                           !Entry's icon ID
rtn:PartNumber         LIKE(rtn:PartNumber)           !List box control field - type derived from field
rtn:Description        LIKE(rtn:Description)          !List box control field - type derived from field
rtn:QuantityReturned   LIKE(rtn:QuantityReturned)     !List box control field - type derived from field
rtn:OrderNumber        LIKE(rtn:OrderNumber)          !List box control field - type derived from field
rtn:InvoiceNumber      LIKE(rtn:InvoiceNumber)        !List box control field - type derived from field
rtn:Notes              LIKE(rtn:Notes)                !List box control field - type derived from field
rtn:RecordNumber       LIKE(rtn:RecordNumber)         !Primary key field - type derived from field
rtn:Location           LIKE(rtn:Location)             !Browse key field - type derived from field
rtn:ExchangeOrder      LIKE(rtn:ExchangeOrder)        !Browse key field - type derived from field
rtn:Status             LIKE(rtn:Status)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK13::rtn:ExchangeOrder   LIKE(rtn:ExchangeOrder)
HK13::rtn:Location        LIKE(rtn:Location)
HK13::rtn:Status          LIKE(rtn:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,4,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       BUTTON,AT(172,276),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(240,276),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(308,276),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       PANEL,AT(168,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Orders'),AT(172,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(168,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Spares'),USE(?Tab1)
                           BUTTON('&Rev tags'),AT(273,193,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(301,196,1,1),USE(?DASSHOWTAG),HIDE
                           TEXT,AT(392,276,124,50),USE(rtn:Notes),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         END
                         TAB('Exchange Units'),USE(?Tab2)
                         END
                         TAB('Loan Units'),USE(?Tab3)
                         END
                       END
                       LIST,AT(172,98,344,174),USE(?List),IMM,MSG('Browsing Records'),FORMAT('11L(2)J@s1@100L(2)|M~Part Number~@s30@100L(2)|M~Description~@s30@37R(2)|M~Qty~@n' &|
   '_8@52R(2)|M~Sale No~@n_8@56R(2)|M~Invoice No~@n_8@0R(2)|M~Notes~@s255@'),FROM(Queue:Browse)
                       PANEL,AT(168,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(172,332),USE(?buttonDelete),TRN,FLAT,ICON('delordp.jpg')
                       BUTTON,AT(452,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(380,332),USE(?buttonProcess),TRN,FLAT,ICON('makretp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = rtn:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = rtn:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    locTag = '*'
  ELSE
    DELETE(glo:Queue)
    locTag = ''
  END
    Queue:Browse.locTag = locTag
  IF (locTag = '*')
    Queue:Browse.locTag_Icon = 2
  ELSE
    Queue:Browse.locTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = rtn:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::11:QUEUE = glo:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = rtn:RecordNumber
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = rtn:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020757'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseReturnOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CREDNOTR.Open
  Relate:EXCHANGE.Open
  Relate:RTNORDER.Open
  Relate:STOCK.Open
  Relate:WAYAUDIT.Open
  Access:USERS.UseFile
  Access:WAYBILLS.UseFile
  Access:WAYCNR.UseFile
  Access:STOHIST.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:RTNORDER,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,rtn:LocationStatusPartKey)
  BRW1.AddRange(rtn:Status)
  BRW1.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,rtn:PartNumber,1,BRW1)
  BIND('locTag',locTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(locTag,BRW1.Q.locTag)
  BRW1.AddField(rtn:PartNumber,BRW1.Q.rtn:PartNumber)
  BRW1.AddField(rtn:Description,BRW1.Q.rtn:Description)
  BRW1.AddField(rtn:QuantityReturned,BRW1.Q.rtn:QuantityReturned)
  BRW1.AddField(rtn:OrderNumber,BRW1.Q.rtn:OrderNumber)
  BRW1.AddField(rtn:InvoiceNumber,BRW1.Q.rtn:InvoiceNumber)
  BRW1.AddField(rtn:Notes,BRW1.Q.rtn:Notes)
  BRW1.AddField(rtn:RecordNumber,BRW1.Q.rtn:RecordNumber)
  BRW1.AddField(rtn:Location,BRW1.Q.rtn:Location)
  BRW1.AddField(rtn:ExchangeOrder,BRW1.Q.rtn:ExchangeOrder)
  BRW1.AddField(rtn:Status,BRW1.Q.rtn:Status)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Spares'
    ?Tab2{PROP:TEXT} = 'Exchange Units'
    ?List{PROP:FORMAT} ='11L(2)J@s1@#1#100L(2)|M~Part Number~@s30@#3#100L(2)|M~Description~@s30@#4#37R(2)|M~Qty~@n_8@#5#52R(2)|M~Sale No~@n_8@#6#56R(2)|M~Invoice No~@n_8@#7#0R(2)|M~Notes~@s255@#8#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CREDNOTR.Close
    Relate:EXCHANGE.Close
    Relate:RTNORDER.Close
    Relate:STOCK.Close
    Relate:WAYAUDIT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020757'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020757'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020757'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?buttonDelete
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonDelete, Accepted)
      BRW1.UpdateViewRecord()
      IF (rtn:RecordNumber > 0)
          Beep(Beep:SystemQuestion)  ;  Yield()
          Case Missive('Are you sure you want to remove the selected item from ordering?','ServiceBase',|
                         'mquest.jpg','\&No|/&Yes') 
          Of 2 ! &Yes Button
          Of 1 ! &No Button
              CYCLE
          End!Case Message
      
          IF (rtn:ExchangeOrder = 1)
              ! Make exchange unit available again
      
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number = rtn:RefNumber
              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Unable to find the exchange item.'&|
                      '|Delete Return Order anyway?','ServiceBase',|
                                 'mexclam.jpg','\&No|/&Yes') 
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      CYCLE
                  End!Case Message
              END
              xch:Available = 'AVL'
              IF (Access:EXCHANGE.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Failed to update exchange unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              AddExchangeHistory(xch:Ref_Number,'RETURN ORDER DELETED')
          ELSIF (rtn:ExchangeOrder = 2)
              ! Make loan unit available again
      
              Access:LOAN.Clearkey(loa:Ref_Number_Key)
              loa:Ref_Number = rtn:RefNumber
              IF (Access:LOAN.TryFetch(xch:Ref_Number_Key))
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Unable to find the loan item.'&|
                      '|Delete Return Order anyway?','ServiceBase',|
                                 'mexclam.jpg','\&No|/&Yes') 
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      CYCLE
                  End!Case Message
              END
              loa:Available = 'AVL'
              IF (Access:LOAN.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Failed to update loan unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              AddLoanHistory(loa:Ref_Number,'RETURN ORDER DELETED')
      
          ELSE
              ! Return Stock Item
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = rtn:RefNumber
              IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key))
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Unable to find the stock item.'&|
                      '|Delete Return Order anyway?','ServiceBase',|
                                 'mexclam.jpg','\&No|/&Yes') 
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      CYCLE
                  End!Case Message
      
              ELSE
                  IF (StockInUse(1))
                      CYCLE
                  END
              END
              sto:Quantity_Stock += rtn:QuantityReturned
              IF (Access:STOCK.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Failed to update stock item.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              RELEASE(STOCK)
              AddStockHistory('ADD',rtn:QuantityReturned,'RETURN ORDER DELETED')
          END
      
          Relate:RTNORDER.Delete(0)
          BRW1.ResetSort(1)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonDelete, Accepted)
    OF ?buttonProcess
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonProcess, Accepted)
      IF (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any items.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to make a Returns Order for the tagged items?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes') 
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      
      ! Get new credit not number
      IF (Access:CREDNOTR.PrimeRecord() = Level:Benign)
          cnr:Usercode = glo:UserCode
          IF (Access:CREDNOTR.TryInsert())
              Access:CREDNOTR.CancelAutoInc()
              STOP('An error occurred')
              CYCLE
          END
      
          ! Create Waybill Record
          locWaybillNumber = NextWaybillNumber()
          IF (locWaybillNumber = 0)
              STOP('An error occurred')
              CYCLE
          END
      
          Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
          way:WaybillNumber = locWaybillNumber
          IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
              IF (glo:WebJob)
                  way:FromAccount = Clarionet:Global.Param2
              ELSE
                  way:FromAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
              END
              way:AccountNumber = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
              way:ToAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
              way:WayBillType = 20 ! To ARC
              Case Choice(?CurrentTab)
              Of 1
                  way:WaybillID = 400 ! Credit Note Waybill (STOCK)
              Of 2
                  way:WaybillID = 401 ! Credit Note Waybill (EXCHANGE)
              OF 3
                  way:WaybillID = 402 ! Credit Note Waybill (LOAN)
              END
              way:Courier = 'RAM'
              way:TheDate = TODAY()
              way:TheTime = CLOCK()
              IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
      
              END ! IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
          END
      
          If Access:WAYAUDIT.PrimeRecord() = Level:Benign
              waa:WAYBILLSRecordNumber = way:RecordNumber
              waa:UserCode = glo:UserCode
              waa:Action   = 'WAYBILL CREATED'
              waa:TheDate  = Today()
              waa:TheTime  = Clock()
              If Access:WAYAUDIT.TryInsert() = Level:Benign
                      ! Insert
                      ! Waybill created (DBH: 04/09/2006)
                  
              Else ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                  Access:WAYAUDIT.CancelAutoInc()
              End ! If Access:WAYAUDIT.TryInsert() = Level:Benign
          End ! If Access.WAYAUDIT.PrimeRecord() = Level:Benign
      
      
      
          LOOP ll# = 1 TO RECORDS(glo:Queue)
              GET(glo:Queue,ll#)
              Access:RTNORDER.Clearkey(rtn:RecordNumberKey)
              rtn:RecordNumber = glo:Pointer
              IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey))
                  CYCLE
              END
              rtn:CreditNoteRequestNumber = cnr:RecordNumber
              rtn:Status = 'PRO' ! Mark as "Processed"
              rtn:WaybillNumber = locWaybillNumber
              rtn:DateOrdered = TODAY()
              rtn:TimeOrdered = CLOCK()
              rtn:WhoOrdered = glo:UserCode
              rtn:Ordered = 1
              Access:RTNORDER.TryUpdate()
      
              IF (rtn:ExchangeOrder = 1)
                  ! Add exchange hist
                  AddExchangeHistory(rtn:RefNumber,'RETURN ORDER PROCESSED','CREDIT NOTE REQ NO: ' & rtn:CreditNoteRequestNumber & |
                                  '<13,10>WAYBILL NO: ' & rtn:WaybillNumber)
              ELSIF (rtn:ExchangeOrder = 2)
                  ! Add loan hist
                  AddLoanHistory(rtn:RefNumber,'RETURN ORDER PROCESSED','CREDIT NOTE REQ NO: ' & rtn:CreditNoteRequestNumber & |
                                  '<13,10>WAYBILL NO: ' & rtn:WaybillNumber)
              ELSE
                  ! Add Stock History Enry
                  AddStockHistory('DEC',0,'RETURN ORDER PROCESSED',|
                                          'CREDIT NOTE REQ NO: ' & rtn:CreditNoteRequestNumber & |
                                          '<13,10>WAYBILL NO: ' & rtn:WaybillNumber)
              END
      
              IF (Access:WAYCNR.PrimeRecord() = Level:Benign)
                  wcr:WAYBILLSRecordnumber = way:RecordNumber
                  wcr:PartNumber = rtn:PartNumber
                  wcr:Description = rtn:Description
                  wcr:ExchangeOrder = rtn:ExchangeOrder
                  wcr:Quantity = rtn:QuantityReturned
                  wcr:RefNumber = rtn:RefNumber
                  IF (Access:WAYCNR.TryInsert())
                      Access:WAYCNR.CancelAutoInc()
                  END
              END ! IF (Access:WAYCNR.PrimeRecord() = Level:Benign)
          END ! LOOP
      
          CreditNoteRequest(cnr:RecordNumber) !Print Credit Request
      
          glo:EDI_Reason = '' ! To stop suprious text appearing. Such is the dangers of global variables.
          WayBillDespatch(way:FromAccount, 'TRA', way:ToAccount, 'TRA', way:WaybillNumber, way:Courier)
      
      
          BRW1.ResetSort(1)
          Post(event:Accepted,?DASUNTAGALL)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonProcess, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?List{PROP:FORMAT} ='11L(2)J@s1@#1#100L(2)|M~Part Number~@s30@#3#100L(2)|M~Description~@s30@#4#37R(2)|M~Qty~@n_8@#5#52R(2)|M~Sale No~@n_8@#6#56R(2)|M~Invoice No~@n_8@#7#0R(2)|M~Notes~@s255@#8#'
          ?Tab1{PROP:TEXT} = 'Spares'
        OF 2
          ?List{PROP:FORMAT} ='11L(2)J@s1@#1#100L(2)|M~Part Number~@s30@#3#100L(2)|M~Description~@s30@#4#37R(2)|M~Qty~@n_8@#5#52R(2)|M~Sale No~@n_8@#6#56R(2)|M~Invoice No~@n_8@#7#0R(2)|M~Notes~@s255@#8#'
          ?Tab2{PROP:TEXT} = 'Exchange Units'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      ! Reformat Browse
      CASE CHOICE(?CurrentTab)
        OF 1
          ?List{PROP:FORMAT} ='11L(2)J@s1@#1#100L(2)|M~Part Number~@s30@#3#100L(2)|M~Description~@s30@#4#37R(2)|M~Qty~@n_8@#5#52R(2)|M~Order No~@n_8@#6#56R(2)|M~Invoice No~@n_8@#7#0R(2)|M~Notes~@s255@#8#'
          ?Tab1{PROP:TEXT} = 'Spares'
        OF 2 OROF 3
          ?List{PROP:FORMAT} ='11L(2)J@s1@#1#100L(2)|M~Model Number~@s30@#3#100L(2)|M~I.M.E.I. No~@s30@#4#52R(2)|M~Order No~@n_8@#6#56R(2)|M~Invoice No~@n_8@#7#0R(2)|M~Notes~@s255@#8#'
      !    ?Tab2{PROP:TEXT} = 'Exchange Units'
      END
      
      Post(Event:Accepted,?DASUNTAGALL)
      
      CASE CHOICE(?CurrentTab)
      OF 1
          locExchangeOrder = 0 ! Stock Tab
      OF 2
          locExchangeOrder = 1 ! Exchange Tab
      OF 3
          locExchangeOrder = 2 ! Loan Tab
      END
      brw1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = glo:Location
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = locExchangeOrder
  GET(SELF.Order.RangeList.List,3)
  Self.Order.RangeList.List.Right = 'NEW'
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtn:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      locTag = ''
    ELSE
      locTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (locTag = '*')
    SELF.Q.locTag_Icon = 2
  ELSE
    SELF.Q.locTag_Icon = 1
  END


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtn:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

ReturnExchangeProcess PROCEDURE                       !Generated from procedure template - Window

locIMEINumber        STRING(30)
locReturnType        STRING(30)
locTRUE              BYTE(1)
qIMEI                QUEUE,PRE(imei)
IMEINumber           STRING(30)
ReturnType           STRING(30)
InvoiceNo            LONG
SaleNo               LONG
IMEIRefNumber        LONG
Price                REAL
                     END
locOrderNumber       LONG
locInvoiceNumber     LONG
locPrice             REAL
locReceivedDate      DATE
gExchOrd             QUEUE(gExchangeOrders),PRE()
                     END
FDB9::View:FileDrop  VIEW(RETTYPES)
                       PROJECT(rtt:Description)
                       PROJECT(rtt:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?locReturnType
rtt:Description        LIKE(rtt:Description)          !List box control field - type derived from field
rtt:RecordNumber       LIKE(rtt:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Stock Process'),AT(168,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,84,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Return Exchange Units'),USE(?Tab1)
                           LIST,AT(328,134,124,10),USE(locReturnType),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),FORMAT('120L(2)|M@s30@'),DROP(10,120),FROM(Queue:FileDrop)
                           PROMPT('I.M.E.I. Number To Return'),AT(228,176),USE(?locIMEINumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(328,176,124,10),USE(locIMEINumber),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('List Of IMEI''s To Return'),AT(168,214),USE(?Prompt5),FONT(,,,FONT:bold)
                           LIST,AT(168,226,344,102),USE(?List2),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~I.M.E.I. No~@s30@120L(2)|M~Return Type~@s30@56R(2)|M~Invoice No~@n_10@' &|
   '56R(2)|M~Sale No~@n_10@'),FROM(qIMEI)
                           PROMPT('Select Return Type'),AT(228,134),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(444,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(376,334),USE(?buttonReturnStock),TRN,FLAT,ICON('rtnexcp.jpg')
                       PANEL,AT(164,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020758'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReturnExchangeProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?buttonReturnStock,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:RETSALES.Open
  Relate:RETTYPES.Open
  Relate:RTNORDER.Open
  Relate:SUBTRACC.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:RETSTOCK.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?locReturnType{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?locReturnType{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDB9.Init(?locReturnType,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:RETTYPES,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(rtt:ActiveDescriptionKey)
  FDB9.AddRange(rtt:Active,locTRUE)
  FDB9.AddField(rtt:Description,FDB9.Q.rtt:Description)
  FDB9.AddField(rtt:RecordNumber,FDB9.Q.rtt:RecordNumber)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:RETSALES.Close
    Relate:RETTYPES.Close
    Relate:RTNORDER.Close
    Relate:SUBTRACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?buttonReturnStock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnStock, Accepted)
      IF (Records(qIMEI) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not selected an I.M.E.I.s to return.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to return the selected I.M.E.I. Numbers?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes') 
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      
      LOOP ll# = 1 TO RECORDS(qIMEI)
          GET(qIMEI,ll#)
          ! Create Return Order
          IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number = qIMEI.IMEIRefNumber
              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                  CYCLE
              END
              IF (xch:Available <> 'AVL')
                  ! Double check the unit is still available
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('The selected exchange unit is no longer available: ' & Clip(xch:ESN) & '.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              ! Mark the unit for return to main store
              xch:Available = 'RTM'
              IF (Access:EXCHANGE.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to update exchange unit ' & Clip(xch:ESN) & '.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              AddExchangeHistory(xch:Ref_Number,'RETURN ORDER GENERATED')
      
              rtn:Location = glo:Location
              rtn:UserCode = glo:UserCode
              rtn:RefNumber = qIMEI.IMEIRefNumber
              rtn:OrderNumber = qIMEI.SaleNo
              rtn:InvoiceNumber = qIMEI.InvoiceNo
              rtn:PartNumber = xch:Model_Number
              rtn:Description = xch:ESN
              rtn:QuantityReturned = 1
              rtn:ExchangeOrder = 1
              rtn:Status = 'NEW'
              rtn:Notes = ''
              rtn:ReturnType = qIMEI.ReturnType
              rtn:ExchangePrice = qIMEI.Price
              IF (Access:RTNORDER.TryInsert())
                  Access:RTNORDER.CancelAutoInc()
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnStock, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020758'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020758'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020758'&'0')
      ***
    OF ?locIMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locIMEINumber, Accepted)
      IF (locReturnType = '')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You must select a Return Type.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      qIMEI.IMEINumber = locIMEINumber
      GET(qIMEI,qIMEI.IMEINumber)
      IF (NOT ERROR())
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have already entered the selected I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Select(?locIMEINumber)
          CYCLE
      END
      
      ! Get Exchange Unit
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN = locIMEINumber
      IF (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Unable to find the selected I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Select(?locIMEINumber)
          CYCLE
      END
      IF (xch:Location <> glo:Location)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected I.M.E.I. is not in your location.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          SELECT(?locIMEINUmber)
          CYCLE
      END
      IF (xch:Available <> 'AVL')
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('Warning! The selected I.M.E.I. number is not "Available".'&|
              '|'&|
              '|Are you sure you want to continue?','ServiceBase',|
                         'mexclam.jpg','\&No|/&Yes') 
          Of 2 ! &Yes Button
          Of 1 ! &No Button
              SELECT(?locIMEINumber)
              CYCLE
          End!Case Message
      END
      
      ! Is the unit already on a returns order
      IF (ReturnUnitAlreadyOnOrder(xch:Ref_Number,1))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Error! The selected unit is awaiting, or is already on a Returns Order.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Select(?locIMEINumber)
          CYCLE
      END
      
      locOrderNumber = 0
      locInvoiceNumber = 0
      locPrice = 0
      locReceivedDate = 0
      
      ! Get the Sale and check the dates
      ExchangeLoanOrderDetails(xch:Ref_Number,gExchOrd)
      
      locInvoiceNumber    = gExchOrd.InvoiceNumber
      locReceivedDate     = gExchOrd.ReceivedDate
      locOrderNumber      = gExchOrd.OrderNumber
      locPrice            = gExchOrd.Price
      
      ! If you can't find the invoice number, ask the user to enter it.
      ! I don't think this is necessary, but it's a fall back incase.
      IF (locInvoiceNumber = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Unable to find the original invoice for the selected I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      !    locInvoiceNumber = EnterReturnInvoiceNumber()
      !    IF (locInvoiceNumber = 0)
      !        CYCLE
      !    END
      !    ! Get the retail sales from the invoice
      !    Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
      !    ret:Invoice_Number = locInvoiceNumber
      !    IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key))
      !        Beep(Beep:SystemHand)  ;  Yield()
      !        Case Missive('Unable to find the selected invoice.','ServiceBase',|
      !                       'mstop.jpg','/&OK') 
      !        Of 1 ! &OK Button
      !        End!Case Message
      !        CYCLE
      !    END
      !    ! Get one of the attached items on the sale to get the received date
      !    Access:RETSTOCK.Clearkey(res:Despatched_Only_Key)
      !    res:Ref_Number = ret:Ref_Number
      !    res:Despatched = 'YES'
      !    IF (Access:RETSTOCK.TryFetch(res:Despatched_Only_Key) = Level:Benign)
      !        locReceivedDate = res:DateReceived
      !    END
      !    locOrderNumber = ret:Ref_Number
      END
      
      
      IF (locReceivedDate > 0)
          Access:RETTYPES.Clearkey(rtt:DescriptionKey)
          rtt:Description = locReturnType
          IF (Access:RETTYPES.TryFetch(rtt:DescriptionKey) = Level:Benign)
              IF (rtt:UseReturnDays)
                IF (rtt:ExchangeReturnDays + locReceivedDate < TODAY())
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('Exchange cannot be returned as was received at Franchise over the set period.','ServiceBase',|
                                   'mstop.jpg','/&OK') 
                    Of 1 ! &OK Button
                    End!Case Message
                    Select(?locIMEINumber)
                    CYCLE
                  END
              END
          END
      END
      
      ! Add To Temp Q
      qIMEI.IMEINumber = locIMEINumber
      qIMEI.ReturnType = locReturnType
      qIMEI.InvoiceNo = locInvoiceNumber
      qIMEI.SaleNo = locOrderNumber
      qIMEI.IMEIRefNumber = xch:Ref_Number
      qIMEI.Price = locPrice
      ADD(qIMEI)
      locIMEINumber = ''
      Select(?locIMEINumber)
      DISPLAY()
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locIMEINumber, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?locReturnType{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?locReturnType{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseReturnOrderTracking PROCEDURE                   !Generated from procedure template - Window

locTRUE              BYTE(1)
locFALSE             BYTE(0)
locAcceptReject      STRING(8)
locLastCleanup       DATE
locExchangeOrder     BYTE
locLoanType          BYTE
BRW8::View:Browse    VIEW(RTNORDER)
                       PROJECT(rtn:WaybillNumber)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:DateCreated)
                       PROJECT(rtn:DateOrdered)
                       PROJECT(rtn:DateReceived)
                       PROJECT(rtn:RecordNumber)
                       PROJECT(rtn:Archived)
                       PROJECT(rtn:Ordered)
                       PROJECT(rtn:Location)
                       PROJECT(rtn:ExchangeOrder)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rtn:WaybillNumber      LIKE(rtn:WaybillNumber)        !List box control field - type derived from field
rtn:PartNumber         LIKE(rtn:PartNumber)           !List box control field - type derived from field
rtn:Description        LIKE(rtn:Description)          !List box control field - type derived from field
rtn:DateCreated        LIKE(rtn:DateCreated)          !List box control field - type derived from field
rtn:DateOrdered        LIKE(rtn:DateOrdered)          !List box control field - type derived from field
rtn:DateReceived       LIKE(rtn:DateReceived)         !List box control field - type derived from field
locAcceptReject        LIKE(locAcceptReject)          !List box control field - type derived from local data
rtn:RecordNumber       LIKE(rtn:RecordNumber)         !Primary key field - type derived from field
rtn:Archived           LIKE(rtn:Archived)             !Browse key field - type derived from field
rtn:Ordered            LIKE(rtn:Ordered)              !Browse key field - type derived from field
rtn:Location           LIKE(rtn:Location)             !Browse key field - type derived from field
rtn:ExchangeOrder      LIKE(rtn:ExchangeOrder)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::rtn:Archived        LIKE(rtn:Archived)
HK10::rtn:ExchangeOrder   LIKE(rtn:ExchangeOrder)
HK10::rtn:Location        LIKE(rtn:Location)
HK10::rtn:Ordered         LIKE(rtn:Ordered)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,2,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(68,40,552,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Stock Tracking'),AT(72,42),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(565,42),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(68,54,552,310),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Parts'),USE(?Tab1)
                         END
                         TAB('Exchanges'),USE(?Tab2)
                         END
                         TAB('Loans'),USE(?Tab3)
                           OPTION,AT(352,66,264,26),USE(locLoanType),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             RADIO('Returned To Main Stores'),AT(364,76),USE(?locLoanType:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('Returned Back To RRC'),AT(496,76),USE(?locLoanType:Radio1:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                           END
                         END
                       END
                       PROMPT('Waybill Number'),AT(72,72),USE(?rtn:WaybillNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@n_10),AT(140,72,64,10),USE(rtn:WaybillNumber),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       LIST,AT(72,94,544,240),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('56R(2)|M~Waybill Number~@n_10@120L(2)|M~Part Number/Model Number~@s30@120L(2)|M~' &|
   'Description/IMEI Number~@s30@52R(2)|M~Date Created~@d17@52R(2)|M~Date Returned~@' &|
   'd17b@52R(2)|M~Date Received~@d17b@52R(2)|M~Accepted/Rejected~L@s8@'),FROM(Queue:Browse)
                       BUTTON,AT(480,336),USE(?buttonReprintCreditRequest),TRN,FLAT,ICON('repcredp.jpg')
                       BUTTON,AT(548,336),USE(?buttonReprintWaybill),TRN,FLAT,ICON('repwayp.jpg')
                       PANEL,AT(68,366,552,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW8::Sort1:Locator  EntryLocatorClass                !Conditional Locator - glo:WebJob = 1
BRW8::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 3 AND locLoanType = 1
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:027590'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseReturnOrderTracking')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RTNAWAIT.Open
  Relate:RTNORDER.Open
  Relate:USERS.Open
  Relate:WAYBILLS.Open
  SELF.FilesOpened = True
  ! Do Daily Cleanup
  IF (glo:WebJob = 1)
      locLastCleanup = GETINI('RETORDCLEANUP',glo:UserAccountNumber,,PATH() & '\SB2KDEF.INI')
      IF (locLastCleanup < TODAY())
          rec# = 0
          Access:RTNORDER.Clearkey(rtn:ArcLocDateKey)
          rtn:Archived = 0
          rtn:Location = glo:Location
          rtn:DateCreated = 0
          SET(rtn:ArcLocDateKey,rtn:ArcLocDateKey)
          LOOP UNTIL Access:RTNORDER.Next()
              IF (rtn:Archived <> 0 OR |
                  rtn:Location <> glo:Location)
                  BREAK
              END
  
              rec# += 1
          END
  
          Prog.ProgressSetup(rec#)
  
          Access:RTNORDER.Clearkey(rtn:ArcLocDateKey)
          rtn:Archived = 0
          rtn:Location = glo:Location
          rtn:DateCreated = 0
          SET(rtn:ArcLocDateKey,rtn:ArcLocDateKey)
          LOOP UNTIL Access:RTNORDER.Next()
              IF (rtn:Archived <> 0 OR |
                  rtn:Location <> glo:Location)
                  BREAK
              END
              IF (Prog.InsideLoop('Maintenance...'))
                  BREAK
              END
              Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
              rta:RTNRecordNumber = rtn:RecordNumber
              IF (Access:RTNAWAIT.TryFetch(rta:RTNORDERKey) = Level:Benign)
                  IF (rta:AcceptReject = '')
                      ! Not accepted/rejected yet
                      CYCLE
                  END
              ELSE
                  ! Record not created yet
                  CYCLE
              END
              ! Leave items under 15 days old
              IF (rtn:DateCreated > TODAY() - 15)
                  CYCLE
              END
              rtn:Archived = 1
              Access:RTNORDER.TryUpdate()
          END
  
          Prog.ProgressFinish()
      END
      PUTINI('RETORDCLEANUP',glo:UserAccountNumber,TODAY(),PATH() & '\SB2KDEF.INI')
  ELSE
      locLastCleanup = GETINI('RETORDCLEANUP','MAIN STORE',,PATH() & '\SB2KDEF.INI')
      IF (locLastCleanup < TODAY())
          rec# = 0
  
          Access:RTNORDER.Clearkey(rtn:ArcDateKey)
          rtn:Archived = 0
          rtn:DateCreated = 0
          SET(rtn:ArcDateKey,rtn:ArcDateKey)
          LOOP UNTIL Access:RTNORDER.Next()
              IF (rtn:Archived <> 0)
                  BREAK
              END
  
              rec# += 1
          END
  
          Prog.ProgressSetup(rec#)
  
          Access:RTNORDER.Clearkey(rtn:ArcDateKey)
          rtn:Archived = 0
          rtn:DateCreated = 0
          SET(rtn:ArcDateKey,rtn:ArcDateKey)
          LOOP UNTIL Access:RTNORDER.Next()
              IF (rtn:Archived <> 0)
                  BREAK
              END
              Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
              rta:RTNRecordNumber = rtn:RecordNumber
              IF (Access:RTNAWAIT.TryFetch(rta:RTNORDERKey) = Level:Benign)
                  IF (rta:AcceptReject = '')
                      ! Not accepted/rejected yet
                      CYCLE
                  END
              ELSE
                  ! Record not created yet
                  CYCLE
              END
  
              IF (rtn:Ordered = 2)
                  ! Loan Rejected Sent Back To RRC
                  IF (rtn:DateReceived = 0)
                      ! Not received yet at RRC
                      CYCLE
                  END
                  IF (rtn:DateReceived > TODAY() - 15)
                      ! REceived in the last 15 days
                      CYCLE
                  END
              END ! IF
              IF (Prog.InsideLoop('Maintenance...'))
                  BREAK
              END
              ! Leave items under 15 days old
              IF (rtn:DateCreated > TODAY() - 15)
                  CYCLE
              END
              rtn:Archived = 1
              Access:RTNORDER.TryUpdate()
          END
  
          Prog.ProgressFinish()
      END
      PUTINI('RETORDCLEANUP','MAIN STORE',TODAY(),PATH() & '\SB2KDEF.INI')
  END
  
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:RTNORDER,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF (glo:WebJob = 1)
      ! #12532 Hide field for RRC (DBH: 10/04/2012)
      ?locLoanType{Prop:Hide} = 1
      locLoanType = 0
  END ! IF
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,rtn:LocArcOrdExcWaybillKey)
  BRW1.AddRange(rtn:ExchangeOrder)
  BRW1.AddLocator(BRW8::Sort1:Locator)
  BRW8::Sort1:Locator.Init(?rtn:WaybillNumber,rtn:WaybillNumber,1,BRW1)
  BRW1.AddSortOrder(,rtn:ArcOrdExcWaybillKey)
  BRW1.AddRange(rtn:ExchangeOrder)
  BRW1.AddLocator(BRW8::Sort2:Locator)
  BRW8::Sort2:Locator.Init(?rtn:WaybillNumber,rtn:WaybillNumber,1,BRW1)
  BRW1.AddSortOrder(,rtn:ArcOrdExcWaybillKey)
  BRW1.AddRange(rtn:ExchangeOrder)
  BRW1.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?rtn:WaybillNumber,rtn:WaybillNumber,1,BRW1)
  BIND('locAcceptReject',locAcceptReject)
  BRW1.AddField(rtn:WaybillNumber,BRW1.Q.rtn:WaybillNumber)
  BRW1.AddField(rtn:PartNumber,BRW1.Q.rtn:PartNumber)
  BRW1.AddField(rtn:Description,BRW1.Q.rtn:Description)
  BRW1.AddField(rtn:DateCreated,BRW1.Q.rtn:DateCreated)
  BRW1.AddField(rtn:DateOrdered,BRW1.Q.rtn:DateOrdered)
  BRW1.AddField(rtn:DateReceived,BRW1.Q.rtn:DateReceived)
  BRW1.AddField(locAcceptReject,BRW1.Q.locAcceptReject)
  BRW1.AddField(rtn:RecordNumber,BRW1.Q.rtn:RecordNumber)
  BRW1.AddField(rtn:Archived,BRW1.Q.rtn:Archived)
  BRW1.AddField(rtn:Ordered,BRW1.Q.rtn:Ordered)
  BRW1.AddField(rtn:Location,BRW1.Q.rtn:Location)
  BRW1.AddField(rtn:ExchangeOrder,BRW1.Q.rtn:ExchangeOrder)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RTNAWAIT.Close
    Relate:RTNORDER.Close
    Relate:USERS.Close
    Relate:WAYBILLS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '027590'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('027590'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('027590'&'0')
      ***
    OF ?locLoanType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locLoanType, Accepted)
      ! #12532 Hide unecessary columns (DBH: 10/04/2012)
      CASE locLoanType
      OF 0 ! Main Store
          ?List{proplist:Width,5} = 52
          ?List{proplist:Width,7} = 52
          ?buttonReprintCreditRequest{prop:Hide} = 0
      OF 1 ! RRC
          ?List{proplist:Width,5} = 0
          ?List{proplist:Width,7} = 0
          ?buttonReprintCreditRequest{prop:Hide} = 1
      END ! CASE
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locLoanType, Accepted)
    OF ?buttonReprintCreditRequest
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReprintCreditRequest, Accepted)
      BRW1.UpdateViewRecord()
      IF (rtn:CreditNoteRequestNumber > 0)
          CreditNoteRequest(rtn:CreditNoteRequestNumber) !Print Credit Request
      END
          
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReprintCreditRequest, Accepted)
    OF ?buttonReprintWaybill
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReprintWaybill, Accepted)
      BRW1.UpdateViewRecord()
      IF (rtn:WaybillNumber > 0)
          Access:WAYBILLS.Clearkey( way:WayBillNumberKey)
          way:WaybillNumber = rtn:WaybillNumber
          IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Unable to find the selected waybill.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
          ELSE
      
              WayBillDespatch(way:FromAccount, 'TRA', way:ToAccount, 'TRA', way:WaybillNumber, way:Courier)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReprintWaybill, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      CASE CHOICE(?CurrentTab)
      OF 1
          locExchangeOrder = 0 ! Stock Tab
      OF 2
          locExchangeOrder = 1 ! Exchange Tab
      OF 3
          locExchangeOrder = 2 ! Loan Tab
      END
      brw1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF glo:WebJob = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = glo:Location
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = locExchangeOrder
  ELSIF CHOICE(?CurrentTab) = 3 AND locLoanType = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 2
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = 2
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = locExchangeOrder
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF glo:WebJob = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3 AND locLoanType = 1
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())
  ! #12151 Display accepted/rejected column (Bryan: 26/09/2011)
  Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
  rta:RTNRecordNumber = rtn:RecordNumber
  IF (Access:RTNAWAIT.TryFetch(rta:RTNORDERKey))
      locAcceptReject = ''
  ELSE
      IF (rta:AcceptReject = 'ACCEPT')
          locAcceptReject = 'Accepted'
      ELSIF rta:AcceptReject = 'REJECT'
          locAcceptReject = 'Rejected'
      ELSE
          locAcceptReject = ''
      END
  END
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        ?Prog:CancelButton{prop:Hide} = 1
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = ''
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
ReturnWaybillConfirmation PROCEDURE (<BYTE fFaulty>)  !Generated from procedure template - Window

locWaybillType       STRING(20)
qItems               QUEUE,PRE(qi)
PartNumber           STRING(30)
Description          STRING(30)
Quantity             LONG
QuantityReceived     LONG
RecordNumber         LONG
                     END
qItemsProc           QUEUE,PRE()
PartNumber           STRING(30)
Description          STRING(30)
Quantity             LONG
QuantityReceived     LONG
RecordNumber         LONG
                     END
locPartNumber        STRING(30)
locExchangeOrder     BYTE
locWaybillID         LONG
locAccountNumber     STRING(30)
BRW9::View:Browse    VIEW(WAYBILLS)
                       PROJECT(way:WayBillNumber)
                       PROJECT(way:RecordNumber)
                       PROJECT(way:WayBillType)
                       PROJECT(way:Received)
                       PROJECT(way:AccountNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
way:WayBillNumber      LIKE(way:WayBillNumber)        !List box control field - type derived from field
locWaybillType         LIKE(locWaybillType)           !List box control field - type derived from local data
way:RecordNumber       LIKE(way:RecordNumber)         !Primary key field - type derived from field
way:WayBillType        LIKE(way:WayBillType)          !Browse key field - type derived from field
way:Received           LIKE(way:Received)             !Browse key field - type derived from field
way:AccountNumber      LIKE(way:AccountNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::way:AccountNumber   LIKE(way:AccountNumber)
HK11::way:Received        LIKE(way:Received)
HK11::way:WayBillType     LIKE(way:WayBillType)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,7,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PROMPT(' {30}'),AT(244,72),USE(?promptWaybillNumber)
                       PANEL,AT(68,42,552,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Stock To Main Store Waybill Confirmation'),AT(72,44),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(565,44),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(68,56,168,310),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Awaiting Receipt'),USE(?Tab1)
                         END
                         TAB('Received'),USE(?Tab2)
                         END
                       END
                       LIST,AT(72,86,160,244),USE(?List),IMM,MSG('Browsing Records'),FORMAT('71R(2)|M~Waybill Number~@n_8@80L(2)|M~Waybill Type~@s20@'),FROM(Queue:Browse)
                       SHEET,AT(240,56,380,310),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Unprocessed Items'),USE(?Tab3)
                           GROUP,AT(244,338,135,24),USE(?GroupReceive),HIDE
                             PROMPT(''),AT(244,338),USE(?promptEnterPart),TRN,FONT(,,,FONT:bold)
                             ENTRY(@s30),AT(244,352,124,10),USE(locPartNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           END
                           BUTTON,AT(548,336),USE(?buttonReceiveWaybill),DISABLE,TRN,FLAT,ICON('compp.jpg')
                           LIST,AT(244,86,372,246),USE(?List2),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~Part Number / Model Number~@s30@120L(2)|M~Description / IMEI Number~@s' &|
   '30@56R(2)|M~Qty~@n_8@56R(2)|M~Qty Rcvd~@n_8@'),FROM(qItems)
                         END
                         TAB('Processed Items'),USE(?Tab4)
                           LIST,AT(244,86,372,244),USE(?List3),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@56R(2)|M~Quantity~@n_8@56R' &|
   '(2)|M~Quantity Received~@n_8@'),FROM(qItemsProc)
                         END
                       END
                       ENTRY(@s8),AT(72,74,64,10),USE(way:WayBillNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Waybill Number'),TIP('Waybill Number'),UPR
                       BUTTON,AT(552,370),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(72,334),USE(?buttonSelectAnother),DISABLE,TRN,FLAT,ICON('selanotp.jpg')
                       BUTTON,AT(164,334),USE(?buttonShowItemsOnWaybill),TRN,FLAT,ICON('showwayp.jpg')
                       PANEL,AT(68,368,552,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2 AND fFaulty = 0
BRW9::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 AND fFaulty = 1
BRW9::Sort3:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2 AND fFaulty = 1
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
WaybillFinished     PROCEDURE(),BYTE
FinishWaybill       PROCEDURE()
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020760'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ReturnWaybillConfirmation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Determine which waybills to display
  locWaybillID = 20
  IF (fFaulty = 1)
      locWaybillID = 23
      IF (glo:WebJob)
          locAccountNumber = Clarionet:Global.Param2
      ELSE
          locAccountNumber = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
      END
  END
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHHIST.Open
  Relate:RTNORDER.Open
  Relate:STOHIST.Open
  Relate:WAYBILLS.Open
  Access:WAYCNR.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:WAYBILLS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (fFaulty = 1)
      ?WindowTitle{prop:text} = 'Returning Faulty Units Waybill Confirmation'
  END
  ?List{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,way:TypeRecNumberKey)
  BRW1.AddRange(way:Received)
  BRW1.AddLocator(BRW9::Sort1:Locator)
  BRW9::Sort1:Locator.Init(,way:WayBillNumber,1,BRW1)
  BRW1.AddSortOrder(,way:TypeAccountRecNumberKey)
  BRW1.AddRange(way:Received)
  BRW1.AddLocator(BRW9::Sort2:Locator)
  BRW9::Sort2:Locator.Init(?way:WayBillNumber,way:WayBillNumber,1,BRW1)
  BRW1.AddSortOrder(,way:TypeAccountRecNumberKey)
  BRW1.AddRange(way:Received)
  BRW1.AddLocator(BRW9::Sort3:Locator)
  BRW9::Sort3:Locator.Init(,way:WayBillNumber,1,BRW1)
  BRW1.AddSortOrder(,way:TypeRecNumberKey)
  BRW1.AddRange(way:Received)
  BRW1.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,way:WayBillNumber,1,BRW1)
  BIND('locWaybillType',locWaybillType)
  BRW1.AddField(way:WayBillNumber,BRW1.Q.way:WayBillNumber)
  BRW1.AddField(locWaybillType,BRW1.Q.locWaybillType)
  BRW1.AddField(way:RecordNumber,BRW1.Q.way:RecordNumber)
  BRW1.AddField(way:WayBillType,BRW1.Q.way:WayBillType)
  BRW1.AddField(way:Received,BRW1.Q.way:Received)
  BRW1.AddField(way:AccountNumber,BRW1.Q.way:AccountNumber)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHHIST.Close
    Relate:RTNORDER.Close
    Relate:STOHIST.Close
    Relate:WAYBILLS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020760'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020760'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020760'&'0')
      ***
    OF ?locPartNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locPartNumber, Accepted)
      Case locExchangeOrder
      OF 0 ! Part
          Found# = 0
          LOOP x# = 1 To RECORDS(qItems)
              GET(qItems,x#)
              IF (qItems.PartNumber <> locPartNumber)
                  CYCLE
              END
              IF (qItems.QuantityReceived = qItems.Quantity)
                  ! All received
                  Found# = 1
                  CYCLE
              END
              Found# = 2
              Access:WAYCNR.Clearkey(wcr:RecordNumberKey)
              wcr:RecordNumber = qItems.RecordNumber
              IF (Access:WAYCNR.TryFetch(wcr:RecordNumberKey) = Level:Benign)
                  wcr:QuantityReceived += 1
                  IF (wcr:QuantityReceived = wcr:Quantity)
                      Beep(Beep:SystemAsterisk)  ;  Yield()
                      Case Missive('You have received all of the selected Part Number.','ServiceBase',|
                                     'midea.jpg','/&OK') 
                      Of 1 ! &OK Button
                      End!Case Message
                      wcr:Processed = 1
                  END
                  IF (Access:WAYCNR.TryUpdate() = Level:Benign)
                      qItems.QuantityReceived = wcr:QuantityReceived
                      PUT(qItems)
                      Beep(Beep:SystemAsterisk)
                      locPartNumber = ''
                      SELECT(?locPartNumber)
                      DISPLAY()
                  END
              END
              IF (WaybillFinished())
                  Beep(Beep:SystemAsterisk)  ;  Yield()
                  Case Missive('The waybill is now complete.','ServiceBase',|
                                 'midea.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  FinishWaybill()
              END
              BREAK
          END ! Loop x# = 1 To RECORDS(qItems)
      
          Case Found#
          OF 0 ! Non Found
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Cannot find the selected Part Number.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              SELECT(?locPartNumber)
              CYCLE
          Of 1 ! Found, but all received
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected Part Number has already been received.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              SELECT(?locPartNumber)
              CYCLE
          END
      
      OF 1 OROF 2 ! Exchange or loan
          qItems.Description = locPartNumber
          GET(qItems,qItems.Description)
          IF ERROR()
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Cannot find the selected I.M.E.I. Number.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              SELECT(?locPartNumber)
              CYCLE
          END
          IF (qItems.QuantityReceived = 1)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected I.M.E.I. has already been received.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              SELECT(?locPartNumber)
              CYCLE
          END
          Access:WAYCNR.Clearkey(wcr:RecordNumberKey)
          wcr:RecordNumber = qItems.RecordNumber
          IF (Access:WAYCNR.TryFetch(wcr:RecordNumberKey) = Level:Benign)
              wcr:QuantityReceived = 1
              wcr:Processed = 1
              IF (Access:WAYCNR.TryUpdate() = Level:Benign)
                  qItems.QuantityReceived = 1
                  PUT(qItems)
                  Beep(Beep:SystemAsterisk)
                  locPartNumber = ''
                  SELECT(?locPartNumber)
                  DISPLAY()
      
                  IF (fFaulty = 1) ! This is a ARC to RRC Faulty Return
                      IF (locExchangeOrder = 2)
                          Access:LOAN.Clearkey(loa:Ref_Number_Key)
                          loa:Ref_Number = wcr:RefNumber
                          IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign)
                              loa:Available = 'AVL'
                              loa:StatusChangeDate = TODAY()
                              loa:Location = glo:Location
                              IF (Access:LOAN.TryUpdate() = Level:Benign)
                                  AddLoanHistory(loa:Ref_Number,'FAULTY UNIT RETURNED','WAYBILL CONFIRMATION: ' & way:WaybillNumber)
      
                                  ! #12532 Update the return orders screen (DBH: 10/04/2012)
                                  Access:RTNORDER.Clearkey(rtn:ArcOrdExcWaybillKey)
                                  rtn:Archived = 0
                                  rtn:Ordered = 2
                                  rtn:ExchangeOrder = 2
                                  rtn:WaybillNumber = way:WaybillNumber
                                  IF (Access:RTNORDER.Tryfetch(rtn:ArcOrdExcWaybillKey) = Level:Benign)
                                      rtn:DateReceived = TODAY()
                                      rtn:TimeReceived = CLOCK()
                                      rtn:WhoReceived = glo:Usercode
                                      IF (Access:RTNORDER.TryUpdate() = Level:Benign)
                                      END ! IF
                                  END ! IF
                              ELSE !IF (Access:LOAN.TryUpdate() = Level:Benign)
                                  Beep(Beep:SystemHand)  ;  Yield()
                                  Case Missive('Unable to update Loan Unit.','ServiceBase',|
                                                 'mstop.jpg','/&OK') 
                                  Of 1 ! &OK Button
                                  End!Case Message
                              END ! IF (Access:LOAN.TryUpdate() = Level:Benign)
                          ELSE
                              Beep(Beep:SystemHand)  ;  Yield()
                              Case Missive('Unable to update Loan Unit.','ServiceBase',|
                                             'mstop.jpg','/&OK') 
                              Of 1 ! &OK Button
                              End!Case Message
                          END
                      END
                  END
              END
          END
          IF (WaybillFinished())
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Missive('The waybill is now complete.','ServiceBase',|
                             'midea.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              FinishWaybill()
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locPartNumber, Accepted)
    OF ?buttonReceiveWaybill
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveWaybill, Accepted)
          IF (WaybillFinished())
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Missive('The waybill is now complete.','ServiceBase',|
                             'midea.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              FinishWaybill()
          ELSE
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Missive('Warning! Not all of the items on the waybill have been received. Are you sure you want to complete the waybill?'&|
                  '|'&|
                  '|(The outstanding items will not be received)','ServiceBase',|
                             'mexclam.jpg','&No|&Yes') 
              Of 2 ! &Yes Button
                  FinishWaybill()
              Of 1 ! &No Button
              End!Case Message
          END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveWaybill, Accepted)
    OF ?buttonSelectAnother
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSelectAnother, Accepted)
      FREE(qItems)
      ?List{prop:Disable} = 0
      ?buttonSelectAnother{prop:Disable} = 1
      ?buttonShowItemsOnWaybill{prop:Disable} = 0
      ?promptWaybillNumber{prop:Text} = ''
      ?GroupReceive{prop:Hide} = 1
      locPartNumber = ''
      ?buttonReceiveWaybill{prop:Disable} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSelectAnother, Accepted)
    OF ?buttonShowItemsOnWaybill
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonShowItemsOnWaybill, Accepted)
      BRW1.UpdateViewRecord()
      FREE(qItems)
      FREE(qItemsProc)
      Access:WAYCNR.Clearkey(wcr:EnteredKey)
      wcr:WAYBILLSRecordnumber = way:RecordNumber
      SET(wcr:EnteredKey,wcr:EnteredKey)
      LOOP UNTIL Access:WAYCNR.Next()
          IF (wcr:WAYBILLSRecordNumber <> way:RecordNumber)
              BREAK
          END
          IF (wcr:Processed = 1)
              qItemsProc.PartNumber = wcr:PartNumber
              qItemsProc.Description = wcr:Description
              qItemsProc.Quantity = wcr:Quantity
              qItemsProc.QuantityReceived = wcr:QuantityReceived
              qItemsProc.RecordNumber = wcr:RecordNumber
              ADD(qItemsProc)
          ELSE
              qItems.PartNumber = wcr:PartNumber
              qItems.Description = wcr:Description
              qItems.Quantity = wcr:Quantity
              qItems.QuantityReceived = wcr:QuantityReceived
              qItems.RecordNumber = wcr:RecordNumber
              ADD(qItems)
          END
      END
      ?promptWaybillNumber{prop:Text} = 'Waybill Number: ' & way:WaybillNumber
      ?List{prop:Disable} = 1
      ?buttonSelectAnother{prop:Disable} = 0
      ?buttonShowItemsOnWaybill{prop:Disable} = 1
      IF (way:Received = 0)
          ?GroupReceive{prop:Hide} = 0
          ?buttonReceiveWaybill{prop:Disable} = 0
          IF (BRW1.Q.locWaybillType = 'Exchange')
              ?promptEnterPart{prop:text} = 'Enter I.M.E.I. To Receive Item:'
              locExchangeOrder = 1
          ELSIF (BRW1.Q.locWaybillType = 'Loan')
              ?promptEnterPart{prop:text} = 'Enter I.M.E.I. To Receive Item:'
              locExchangeOrder = 2
          ELSE
              ?promptEnterPart{prop:text} = 'Enter Part Number To Receive 1 Item:'
              locExchangeOrder = 0
          END
      END
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonShowItemsOnWaybill, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
WaybillFinished     PROCEDURE()
returnValue BYTE(1)
    CODE
        Access:WAYCNR.Clearkey(wcr:EnteredKey)
        wcr:WAYBILLSRecordNumber = way:RecordNumber
        SET(wcr:EnteredKey,wcr:EnteredKey)
        LOOP UNTIL Access:WAYCNR.Next()
            IF (wcr:WAYBILLSRecordNumber <> way:RecordNumber)
                BREAK
            END
            IF (wcr:Processed = 0)
                ! Not all processed
                returnValue = 0
                BREAK
            END
        END

        RETURN returnValue
FinishWaybill     PROCEDURE()
    CODE
        ! Update the history of all the items on the waybill that have been processed
        Access:WAYCNR.Clearkey(wcr:EnteredKey)
        wcr:WAYBILLSRecordNumber = way:RecordNumber
        SET(wcr:EnteredKey,wcr:EnteredKey)
        LOOP UNTIL Access:WAYCNR.Next()
            IF (wcr:WAYBILLSRecordNumber <> way:RecordNumber)
                BREAK
            END
            IF (wcr:Processed = 1)
                Case wcr:ExchangeOrder
                Of 0 ! Stock
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number = wcr:RefNumber
                    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
                    END

                    AddStockHistory('ADD',0,'WAYBILL RECEIVED','WAYBILL NUMBER: ' & way:WaybillNumber)
                Of 1 ! Exchange
                    AddExchangeHistory(wcr:RefNumber,'WAYBILL RECEIVED','WAYBILL NUMBER: ' & way:WaybillNumber)
                Of 2 ! Loan
                    AddLoanHistory(wcr:RefNumber,'WAYBILL RECEIVED','WAYBILL NUMBER: ' & way:WaybillNumber)
                END
            ELSE
                Case wcr:ExchangeOrder
                OF 0 ! Stock
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number = wcr:RefNumber
                    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
                    END

                    AddStockHistory('ADD',0,'ITEM NOT RECEIVED ON WAYBILL','WAYBILL NUMBER: ' & way:WaybillNumber)
                OF 1 ! Exchange
                    AddExchangeHistory(wcr:RefNumber,'ITEM NOT RECEIVED ON WAYBILL','WAYBILL NUMBER: ' & way:WaybillNumber)
                OF 2 ! Loan
                    AddLoanHistory(wcr:RefNumber,'ITEM NOT RECEIVED ON WAYBILL','WAYBILL NUMBER: ' & way:WaybillNumber)
                END
            END
        END

        way:Received = 1
        Access:WAYBILLS.TryUpdate()
        BRW1.ResetSort(1)
        Post(Event:Accepted,?buttonSelectAnother)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2 AND fFaulty = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locWaybillID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  ELSIF Choice(?CurrentTab) = 1 AND fFaulty = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locWaybillID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locAccountNumber
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = 0
  ELSIF Choice(?CurrentTab) = 2 AND fFaulty = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locWaybillID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locAccountNumber
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locWaybillID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2 AND fFaulty = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 AND fFaulty = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 AND fFaulty = 1
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  CASE (way:WaybillID)
  OF 400
    locWaybillType = 'Stock'
  OF 401
    locWaybillType = 'Exchange'
  ELSE
    locWaybillType = 'Loan'
  END
  PARENT.SetQueueRecord
  SELF.Q.locWaybillType = locWaybillType              !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
BrowseReturningOrders PROCEDURE                       !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::32:TAGFLAG         BYTE(0)
DASBRW::32:TAGMOUSE        BYTE(0)
DASBRW::32:TAGDISPSTATUS   BYTE(0)
DASBRW::32:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:LabelType        BYTE(0)
save_res_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_stock_id        USHORT,AUTO
retstock_record_number_temp LONG
parts_record_number_temp REAL
warparts_record_number_temp REAL
LocalRequest         LONG
FilesOpened          BYTE
Received_Temp        STRING(20)
outstanding_temp     STRING('NO {1}')
job_number_temp      STRING(6)
type_temp            STRING(3)
tmp:tag              STRING('0')
tmp:Goods_Received_Date LONG
tmp:ExchangeUnit     BYTE(0)
tmp:StockItem        BYTE(0)
tmp:Currency         STRING(30)
tmp:BarCodeLabelNumber LONG
tmp:BarcodeLabelType BYTE(0)
tmp:OrderNumberPrefix STRING(3)
locCreditLocation    STRING(30)
locCreditReceived    STRING(3)
locFalse             BYTE
locCreditType        STRING(8)
locRTNTag            STRING(1)
locHandOverUserCode  STRING(3)
qRec                 QUEUE(qDefRec),PRE()
                     END
locFilter            BYTE
BRW28::View:Browse   VIEW(CREDNOTR)
                       PROJECT(cnr:RecordNumber)
                       PROJECT(cnr:Usercode)
                       PROJECT(cnr:DateCreated)
                       PROJECT(cnr:Received)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
cnr:RecordNumber       LIKE(cnr:RecordNumber)         !List box control field - type derived from field
locCreditLocation      LIKE(locCreditLocation)        !List box control field - type derived from local data
cnr:Usercode           LIKE(cnr:Usercode)             !List box control field - type derived from field
cnr:DateCreated        LIKE(cnr:DateCreated)          !List box control field - type derived from field
locCreditReceived      LIKE(locCreditReceived)        !List box control field - type derived from local data
cnr:Received           LIKE(cnr:Received)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW31::View:Browse   VIEW(RTNORDER)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:InvoiceNumber)
                       PROJECT(rtn:QuantityReturned)
                       PROJECT(rtn:DateReceived)
                       PROJECT(rtn:RecordNumber)
                       PROJECT(rtn:CreditNoteRequestNumber)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:3
locRTNTag              LIKE(locRTNTag)                !List box control field - type derived from local data
locRTNTag_Icon         LONG                           !Entry's icon ID
rtn:PartNumber         LIKE(rtn:PartNumber)           !List box control field - type derived from field
rtn:PartNumber_NormalFG LONG                          !Normal forground color
rtn:PartNumber_NormalBG LONG                          !Normal background color
rtn:PartNumber_SelectedFG LONG                        !Selected forground color
rtn:PartNumber_SelectedBG LONG                        !Selected background color
rtn:Description        LIKE(rtn:Description)          !List box control field - type derived from field
rtn:Description_NormalFG LONG                         !Normal forground color
rtn:Description_NormalBG LONG                         !Normal background color
rtn:Description_SelectedFG LONG                       !Selected forground color
rtn:Description_SelectedBG LONG                       !Selected background color
locCreditType          LIKE(locCreditType)            !List box control field - type derived from local data
locCreditType_NormalFG LONG                           !Normal forground color
locCreditType_NormalBG LONG                           !Normal background color
locCreditType_SelectedFG LONG                         !Selected forground color
locCreditType_SelectedBG LONG                         !Selected background color
rtn:InvoiceNumber      LIKE(rtn:InvoiceNumber)        !List box control field - type derived from field
rtn:InvoiceNumber_NormalFG LONG                       !Normal forground color
rtn:InvoiceNumber_NormalBG LONG                       !Normal background color
rtn:InvoiceNumber_SelectedFG LONG                     !Selected forground color
rtn:InvoiceNumber_SelectedBG LONG                     !Selected background color
rtn:QuantityReturned   LIKE(rtn:QuantityReturned)     !List box control field - type derived from field
rtn:QuantityReturned_NormalFG LONG                    !Normal forground color
rtn:QuantityReturned_NormalBG LONG                    !Normal background color
rtn:QuantityReturned_SelectedFG LONG                  !Selected forground color
rtn:QuantityReturned_SelectedBG LONG                  !Selected background color
rtn:DateReceived       LIKE(rtn:DateReceived)         !List box control field - type derived from field
rtn:DateReceived_NormalFG LONG                        !Normal forground color
rtn:DateReceived_NormalBG LONG                        !Normal background color
rtn:DateReceived_SelectedFG LONG                      !Selected forground color
rtn:DateReceived_SelectedBG LONG                      !Selected background color
rtn:RecordNumber       LIKE(rtn:RecordNumber)         !Primary key field - type derived from field
rtn:CreditNoteRequestNumber LIKE(rtn:CreditNoteRequestNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Parts Orders File'),AT(0,0,679,428),FONT('Tahoma',8,,,CHARSET:ANSI),COLOR(0D6EAEFH),IMM,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Browse The Parts Order File'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(608,396),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       SHEET,AT(4,28,672,366),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),SPREAD
                         TAB('Returned Orders'),USE(?Tab3)
                           OPTION('Order Type'),AT(416,42,112,20),USE(locFilter),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(496,50),USE(?outstanding_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Outstanding'),AT(428,50),USE(?outstanding_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           LIST,AT(152,68,376,102),USE(?List:2),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('70R(2)|M~Credit Note Req No~L@n08@120L(2)|M~Location~@s30@23L(2)|M~User~@s3@50R(' &|
   '2)|M~Date~@d17@12L(2)|M~Received~@s3@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(87,98,1,1),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(87,110,1,1),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(532,146),USE(?buttonDeleteCreditNoteRequest),TRN,FLAT,HIDE,ICON('delordp.jpg')
                           PANEL,AT(264,192,10,8),USE(?Panel3),FILL(COLOR:Red)
                           PROMPT('- Received'),AT(276,190),USE(?Prompt3)
                           BUTTON,AT(532,104),USE(?buttonCreditNoteRequest),FLAT,ICON('repcredp.jpg')
                           BUTTON,AT(8,366),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(76,366),USE(?DASTAGAl),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(144,366),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           LIST,AT(8,204,596,158),USE(?List:3),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)J@s1@128L(2)|M*~Part Number / Model Number~@s30@128L(2)|M*~Description / I' &|
   '.M.E.I. Number~@s30@40L(2)|M*~Type~@s8@56R(2)|M*~Invoice Number~@n_8@56R(2)|M*~Q' &|
   'uantity~@n_8@40R(2)|M*~Date Received~@d17b@'),FROM(Queue:Browse:3)
                           BUTTON,AT(608,202),USE(?buttonReceiveReturnPart),TRN,FLAT,ICON('recprtp.jpg')
                           BUTTON,AT(608,232),USE(?buttonReceiveTaggedReturnOrder),TRN,FLAT,ICON('rectagpp.jpg')
                           ENTRY(@n-14),AT(152,54,64,10),USE(cnr:RecordNumber),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(4,398),USE(?Button14),DISABLE,TRN,FLAT,HIDE,LEFT,ICON('editqtyp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_CNR              CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW28::Sort0:Locator EntryLocatorClass                !Default Locator
BRW_RTN              CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW31::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
InvalidItem PROCEDURE(),BYTE
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::32:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:3))
  BRW_RTN.UpdateBuffer
   glo:Queue.Pointer = rtn:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = rtn:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    locRTNTag = '*'
  ELSE
    DELETE(glo:Queue)
    locRTNTag = ''
  END
    Queue:Browse:3.locRTNTag = locRTNTag
  IF (locRTNTag = '*')
    Queue:Browse:3.locRTNTag_Icon = 2
  ELSE
    Queue:Browse:3.locRTNTag_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::32:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW_RTN.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW31::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = rtn:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW_RTN.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::32:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW_RTN.Reset
  SETCURSOR
  BRW_RTN.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::32:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::32:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::32:QUEUE = glo:Queue
    ADD(DASBRW::32:QUEUE)
  END
  FREE(glo:Queue)
  BRW_RTN.Reset
  LOOP
    NEXT(BRW31::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::32:QUEUE.Pointer = rtn:RecordNumber
     GET(DASBRW::32:QUEUE,DASBRW::32:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = rtn:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW_RTN.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::32:DASSHOWTAG Routine
   CASE DASBRW::32:TAGDISPSTATUS
   OF 0
      DASBRW::32:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::32:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::32:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW_RTN.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddToProcessing     ROUTINE
    IF (Access:RTNAWAIT.PrimeRecord() = Level:Benign)
        rta:RTNRecordNumber = rtn:RecordNumber
        rta:CNRRecordNumber = rtn:CreditNoteRequestNumber
        rta:RefNumber       = rtn:RefNumber
        rta:ExchangeOrder   = rtn:ExchangeOrder
        rta:PartNumber      = rtn:PartNumber
        rta:Description     = rtn:Description
        rta:UserCode        = rtn:UserCode
        rta:Quantity        = rtn:QuantityReturned
        rta:Location        = rtn:Location
        IF (Access:RTNAWAIT.TryInsert())
            STOP('An error occurred')
            Access:RTNAWAIT.CancelAutoInc()
            EXIT
        END ! IF (Access:RTNAWAIT.TryInsert())
    END ! IF (Access:RTNAWAIT.PrimeRecord() = Level:Benign)

    IF (rtn:ExchangeOrder = 1)
        ! Move the exchange unit to 'Awaiting Receipt'
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = rtn:RefNumber
        IF (Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign)
            xch:Location = 'AWAITING RECEIPT'
            IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                AddExchangeHistory(xch:Ref_Number,'AWAITING RECEIPT','CREDIT NOTE REQ NO: ' & rtn:CreditNoteRequestNumber)
            END
        END
    END

    IF (rtn:ExchangeOrder = 2)
        ! Move the loan unit
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = rtn:RefNumber
        IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign)
            loa:Location = 'AWAITING RECEIPT'
            IF (Access:LOAN.TryUpdate() = Level:Benign)
                AddLoanHistory(loa:Ref_Number,'AWAITING RECEIPT','CREDIT NOTE REQ NO: ' & rtn:CreditNoteRequestNumber)
            END
        END
    END
AllReceived     ROUTINE

    BRW_CNR.UpdateViewRecord()
    IF (cnr:RecordNumber = 0)
        EXIT
    END

    FOUND# = 0
    Access:RTNORDER.Clearkey(rtn:CreditNoteNumberKey)
    rtn:CreditNoteRequestNumber = cnr:RecordNumber
    SET(rtn:CreditNoteNumberKey,rtn:CreditNoteNumberKey)
    LOOP UNTIL ACCESS:RTNORDER.Next()
        IF (rtn:CreditNoteRequestNumber <> cnr:RecordNumber)
            BREAK
        END
        IF (rtn:Received <> 1)
            FOUND# = 1
            BREAK
        END
    END

    IF (FOUND# = 0)
        Beep(Beep:SystemExclamation)  ;  Yield()
        Case Missive('All items have been received. The Credit Note Request will now be marked as received.','ServiceBase',|
                       'mexclam.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        cnr:Received = 1
        cnr:DateReceived = TODAY()
        cnr:TimeReceived = CLOCK()
        cnr:ReceivedUsercode = glo:Usercode
        Access:CREDNOTR.TryUpdate()
        BRW_CNR.ResetSort(1)
    END
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020761'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseReturningOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CREDNOTR.Open
  Relate:CURRENCY.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:GRNOTES.Open
  Relate:JOBS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:RTNAWAIT.Open
  Relate:RTNORDER.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Relate:WAYBILLS.Open
  Access:PARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:JOBSTAGE.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:RETSTOCK.UseFile
  Access:SUPPLIER.UseFile
  Access:WAYCNR.UseFile
  SELF.FilesOpened = True
  BRW_CNR.Init(?List:2,Queue:Browse:2.ViewPosition,BRW28::View:Browse,Queue:Browse:2,Relate:CREDNOTR,SELF)
  BRW_RTN.Init(?List:3,Queue:Browse:3.ViewPosition,BRW31::View:Browse,Queue:Browse:3,Relate:RTNORDER,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW_CNR.Q &= Queue:Browse:2
  BRW_CNR.AddSortOrder(,cnr:ReceivedKey)
  BRW_CNR.AddRange(cnr:Received,locFilter)
  BRW_CNR.AddLocator(BRW28::Sort0:Locator)
  BRW28::Sort0:Locator.Init(?cnr:RecordNumber,cnr:RecordNumber,1,BRW_CNR)
  BIND('locCreditLocation',locCreditLocation)
  BIND('locCreditReceived',locCreditReceived)
  BRW_CNR.AddField(cnr:RecordNumber,BRW_CNR.Q.cnr:RecordNumber)
  BRW_CNR.AddField(locCreditLocation,BRW_CNR.Q.locCreditLocation)
  BRW_CNR.AddField(cnr:Usercode,BRW_CNR.Q.cnr:Usercode)
  BRW_CNR.AddField(cnr:DateCreated,BRW_CNR.Q.cnr:DateCreated)
  BRW_CNR.AddField(locCreditReceived,BRW_CNR.Q.locCreditReceived)
  BRW_CNR.AddField(cnr:Received,BRW_CNR.Q.cnr:Received)
  BRW_RTN.Q &= Queue:Browse:3
  BRW_RTN.AddSortOrder(,rtn:CreditNoteNumberKey)
  BRW_RTN.AddRange(rtn:CreditNoteRequestNumber,cnr:RecordNumber)
  BRW_RTN.AddLocator(BRW31::Sort0:Locator)
  BRW31::Sort0:Locator.Init(,rtn:CreditNoteRequestNumber,1,BRW_RTN)
  BIND('locRTNTag',locRTNTag)
  BIND('locCreditType',locCreditType)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW_RTN.AddField(locRTNTag,BRW_RTN.Q.locRTNTag)
  BRW_RTN.AddField(rtn:PartNumber,BRW_RTN.Q.rtn:PartNumber)
  BRW_RTN.AddField(rtn:Description,BRW_RTN.Q.rtn:Description)
  BRW_RTN.AddField(locCreditType,BRW_RTN.Q.locCreditType)
  BRW_RTN.AddField(rtn:InvoiceNumber,BRW_RTN.Q.rtn:InvoiceNumber)
  BRW_RTN.AddField(rtn:QuantityReturned,BRW_RTN.Q.rtn:QuantityReturned)
  BRW_RTN.AddField(rtn:DateReceived,BRW_RTN.Q.rtn:DateReceived)
  BRW_RTN.AddField(rtn:RecordNumber,BRW_RTN.Q.rtn:RecordNumber)
  BRW_RTN.AddField(rtn:CreditNoteRequestNumber,BRW_RTN.Q.rtn:CreditNoteRequestNumber)
  BRW_CNR.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW_CNR.AskProcedure = 0
      CLEAR(BRW_CNR.AskProcedure, 1)
    END
  END
  BRW_RTN.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW_RTN.AskProcedure = 0
      CLEAR(BRW_RTN.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:3{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:CREDNOTR.Close
    Relate:CURRENCY.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:GRNOTES.Close
    Relate:JOBS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:RTNAWAIT.Close
    Relate:RTNORDER.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
    Relate:WAYBILLS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?buttonReceiveReturnPart
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveReturnPart, Accepted)
      BRW_CNR.UpdateViewRecord()
      BRW_RTN.UpdateViewRecord()
      
      IF (InvalidItem())
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveReturnPart, Accepted)
    OF ?buttonReceiveTaggedReturnOrder
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveTaggedReturnOrder, Accepted)
      BRW_CNR.UpdateViewRecord()
      BRW_RTN.UpdateViewRecord()
      
      LOOP ll# = 1 TO RECORDS(glo:Queue)
          GET(glo:Queue,ll#)
          Access:RTNORDER.Clearkey(rtn:RecordNumberKey)
          rtn:RecordNumber = glo:Pointer
          IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey))
              DELETE(glo:Queue)
              CYCLE
          ELSE
              IF (InvalidItem())
                  DELETE(glo:Queue)
                  CYCLE
              END
          END
      END
      
      IF (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any items to be received.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          DISPLAY()
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveTaggedReturnOrder, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020761'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020761'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020761'&'0')
      ***
    OF ?locFilter
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locFilter, Accepted)
      BRW_CNR.ResetSort(1)
      BRW_RTN.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locFilter, Accepted)
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?buttonCreditNoteRequest
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonCreditNoteRequest, Accepted)
      CreditNoteRequest(BRW_CNR.q.cnr:RecordNumber) !Print Credit Request
      Do AllReceived
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonCreditNoteRequest, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAl
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?buttonReceiveReturnPart
      ThisWindow.Update
      locHandOverUserCode = ReceiveReturningOrder()
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveReturnPart, Accepted)
      If (locHandOverUserCode <> '')
          BRW_RTN.UpdateViewRecord()
          rtn:DateReceived    = TODAY()
          rtn:TimeReceived    = CLOCK()
          rtn:WhoReceived     = glo:Usercode
          rtn:WhoProcessed    = locHandOverUserCode
          rtn:Received        = 1
          IF (Access:RTNORDER.TryUpdate() = Level:Benign)
              ! Create Processing Record
              DO AddToProcessing
      
              FREE(qRec)
              qRec.RecordNumber = rtn:RecordNumber
              ADD(qRec)
              ReturnsHandover(rtn:CreditNoteRequestNumber,qRec)
      
              BRW_RTN.ResetSort(1)
          END ! IF (Access:RTNORDER.TryUpdate() = Level:Benign)
      END
      Do AllReceived
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveReturnPart, Accepted)
    OF ?buttonReceiveTaggedReturnOrder
      ThisWindow.Update
      locHandOverUserCode = ReceiveReturningOrder()
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveTaggedReturnOrder, Accepted)
      If (locHandOverUserCode <> '')
          FREE(qRec)
          ! Mark tagged items as received.
          ! Add to queue, for printout.
          LOOP ll# = 1 TO RECORDS(glo:Queue)
              GET(glo:Queue,ll#)
              Access:RTNORDER.Clearkey(rtn:RecordNumberKEy)
              rtn:RecordNumber = glo:Pointer
              IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey))
                  CYCLE
              END
              rtn:DateReceived    = TODAY()
              rtn:TimeReceived    = CLOCK()
              rtn:WhoReceived     = glo:Usercode
              rtn:WhoProcessed    = locHandOverUserCode
              rtn:Received        = 1
              IF (Access:RTNORDER.TryUpdate() = Level:Benign)
                  ! Create Processing Record
                  DO AddToProcessing
      
                  qRec.RecordNumber = rtn:RecordNumber
                  ADD(qRec)
              END ! IF (Access:RTNORDER.TryUpdate() = Level:Benign)
          END
      
          IF (RECORDS(qRec))
              ReturnsHandover(rtn:CreditNoteRequestNumber,qRec)
          END
          BRW_RTN.ResetSort(1)
      END
      POST(EVENT:Accepted,?DASUNTAGALL)
      Do AllReceived
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReceiveTaggedReturnOrder, Accepted)
    OF ?Button14
      ThisWindow.Update
      Amend_Quantity
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::32:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
InvalidItem     PROCEDURE()
    CODE
        ! Check if the waybill has been received.
        Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
        way:WaybillNumber = rtn:WaybillNumber
        IF (Access:WAYBILLS.TryFetch(way:WaybillNumberKey))
            ! Don't think we should ever be here
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive(Clip(rtn:PartNumber) & ' - ' & Clip(rtn:Description) &|
                '|'&|
                '|A Waybill has not been generated for this item.','ServiceBase',|
                           'mstop.jpg','\&Stop|&Ignore') 
            Of 2 ! &Ignore Button
            Of 1 ! &Stop Button
                RETURN TRUE
            End!Case Message
        END
        IF (way:Received = FALSE)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive(Clip(rtn:PartNumber) & ' - ' & Clip(rtn:Description) &|
                '|'&|
                '|The waybill for this item has not yet been received.','ServiceBase',|
                           'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
            RETURN TRUE
        END

        IF (rtn:Received = 1)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive(Clip(rtn:PartNumber) & ' - ' & Clip(rtn:Description) &|
                '|'&|
                '|The waybill for this item has not yet been received.','ServiceBase',|
                           'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
            RETURN TRUE
        END

        RETURN FALSE

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW_CNR.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(28, SetQueueRecord, ())
  Access:RTNORDER.Clearkey( rtn:CreditNoteNumberKey)
  rtn:CreditNoteRequestNumber = cnr:RecordNumber
  IF (Access:RTNORDER.Tryfetch(rtn:CreditNoteNumberKey))
      locCreditLocation = ''
  ELSE
      locCreditLocation = rtn:Location
  END
  
  IF (cnr:Received)
      locCreditReceived = 'YES'
  ELSE
      locCreditReceived = 'NO'
  END
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(28, SetQueueRecord, ())


BRW_CNR.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW_RTN.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtn:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      locRTNTag = ''
    ELSE
      locRTNTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  CASE (rtn:ExchangeOrder)
  OF 1
    locCreditType = 'Exchange'
  OF 2
    locCreditType = 'Loan'
  ELSE
    locCreditType = 'Spares'
  END
  PARENT.SetQueueRecord
  IF (locRTNTag = '*')
    SELF.Q.locRTNTag_Icon = 2
  ELSE
    SELF.Q.locRTNTag_Icon = 1
  END
  SELF.Q.rtn:PartNumber_NormalFG = -1
  SELF.Q.rtn:PartNumber_NormalBG = -1
  SELF.Q.rtn:PartNumber_SelectedFG = -1
  SELF.Q.rtn:PartNumber_SelectedBG = -1
  SELF.Q.rtn:Description_NormalFG = -1
  SELF.Q.rtn:Description_NormalBG = -1
  SELF.Q.rtn:Description_SelectedFG = -1
  SELF.Q.rtn:Description_SelectedBG = -1
  SELF.Q.locCreditType_NormalFG = -1
  SELF.Q.locCreditType_NormalBG = -1
  SELF.Q.locCreditType_SelectedFG = -1
  SELF.Q.locCreditType_SelectedBG = -1
  SELF.Q.rtn:InvoiceNumber_NormalFG = -1
  SELF.Q.rtn:InvoiceNumber_NormalBG = -1
  SELF.Q.rtn:InvoiceNumber_SelectedFG = -1
  SELF.Q.rtn:InvoiceNumber_SelectedBG = -1
  SELF.Q.rtn:QuantityReturned_NormalFG = -1
  SELF.Q.rtn:QuantityReturned_NormalBG = -1
  SELF.Q.rtn:QuantityReturned_SelectedFG = -1
  SELF.Q.rtn:QuantityReturned_SelectedBG = -1
  SELF.Q.rtn:DateReceived_NormalFG = -1
  SELF.Q.rtn:DateReceived_NormalBG = -1
  SELF.Q.rtn:DateReceived_SelectedFG = -1
  SELF.Q.rtn:DateReceived_SelectedBG = -1
  SELF.Q.locCreditType = locCreditType                !Assign formula result to display queue
   
   
   IF (rtn:Received = 1)
     SELF.Q.rtn:PartNumber_NormalFG = 255
     SELF.Q.rtn:PartNumber_NormalBG = 16777215
     SELF.Q.rtn:PartNumber_SelectedFG = 16777215
     SELF.Q.rtn:PartNumber_SelectedBG = 255
   ELSE
     SELF.Q.rtn:PartNumber_NormalFG = -1
     SELF.Q.rtn:PartNumber_NormalBG = -1
     SELF.Q.rtn:PartNumber_SelectedFG = -1
     SELF.Q.rtn:PartNumber_SelectedBG = -1
   END
   IF (rtn:Received = 1)
     SELF.Q.rtn:Description_NormalFG = 255
     SELF.Q.rtn:Description_NormalBG = 16777215
     SELF.Q.rtn:Description_SelectedFG = 16777215
     SELF.Q.rtn:Description_SelectedBG = 255
   ELSE
     SELF.Q.rtn:Description_NormalFG = -1
     SELF.Q.rtn:Description_NormalBG = -1
     SELF.Q.rtn:Description_SelectedFG = -1
     SELF.Q.rtn:Description_SelectedBG = -1
   END
   IF (rtn:Received = 1)
     SELF.Q.locCreditType_NormalFG = 255
     SELF.Q.locCreditType_NormalBG = 16777215
     SELF.Q.locCreditType_SelectedFG = 16777215
     SELF.Q.locCreditType_SelectedBG = 255
   ELSE
     SELF.Q.locCreditType_NormalFG = -1
     SELF.Q.locCreditType_NormalBG = -1
     SELF.Q.locCreditType_SelectedFG = -1
     SELF.Q.locCreditType_SelectedBG = -1
   END
   IF (rtn:Received = 1)
     SELF.Q.rtn:InvoiceNumber_NormalFG = 255
     SELF.Q.rtn:InvoiceNumber_NormalBG = 16777215
     SELF.Q.rtn:InvoiceNumber_SelectedFG = 16777215
     SELF.Q.rtn:InvoiceNumber_SelectedBG = 255
   ELSE
     SELF.Q.rtn:InvoiceNumber_NormalFG = -1
     SELF.Q.rtn:InvoiceNumber_NormalBG = -1
     SELF.Q.rtn:InvoiceNumber_SelectedFG = -1
     SELF.Q.rtn:InvoiceNumber_SelectedBG = -1
   END
   IF (rtn:Received = 1)
     SELF.Q.rtn:QuantityReturned_NormalFG = 255
     SELF.Q.rtn:QuantityReturned_NormalBG = 16777215
     SELF.Q.rtn:QuantityReturned_SelectedFG = 16777215
     SELF.Q.rtn:QuantityReturned_SelectedBG = 255
   ELSE
     SELF.Q.rtn:QuantityReturned_NormalFG = -1
     SELF.Q.rtn:QuantityReturned_NormalBG = -1
     SELF.Q.rtn:QuantityReturned_SelectedFG = -1
     SELF.Q.rtn:QuantityReturned_SelectedBG = -1
   END
   IF (rtn:Received = 1)
     SELF.Q.rtn:DateReceived_NormalFG = 255
     SELF.Q.rtn:DateReceived_NormalBG = 16777215
     SELF.Q.rtn:DateReceived_SelectedFG = 16777215
     SELF.Q.rtn:DateReceived_SelectedBG = 255
   ELSE
     SELF.Q.rtn:DateReceived_NormalFG = -1
     SELF.Q.rtn:DateReceived_NormalBG = -1
     SELF.Q.rtn:DateReceived_SelectedFG = -1
     SELF.Q.rtn:DateReceived_SelectedBG = -1
   END


BRW_RTN.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW_RTN.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW_RTN.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW31::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW31::RecordStatus=ReturnValue
  IF BRW31::RecordStatus NOT=Record:OK THEN RETURN BRW31::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtn:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::32:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW31::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW31::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW31::RecordStatus
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
ReturnProcessing PROCEDURE                            !Generated from procedure template - Window

locFilter            BYTE(99)
gExchOrd             GROUP(gExchangeOrders),PRE(geo)
                     END
gNewJob              GROUP(NewJobBooking),PRE(gnj)
                     END
locFaultySiteLocation STRING(30)
BRW8::View:Browse    VIEW(RTNAWAIT)
                       PROJECT(rta:CNRRecordNumber)
                       PROJECT(rta:PartNumber)
                       PROJECT(rta:Description)
                       PROJECT(rta:Location)
                       PROJECT(rta:RecordNumber)
                       PROJECT(rta:Archive)
                       PROJECT(rta:Processed)
                       PROJECT(rta:ExchangeOrder)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rta:CNRRecordNumber    LIKE(rta:CNRRecordNumber)      !List box control field - type derived from field
rta:PartNumber         LIKE(rta:PartNumber)           !List box control field - type derived from field
rta:Description        LIKE(rta:Description)          !List box control field - type derived from field
rta:Location           LIKE(rta:Location)             !List box control field - type derived from field
rta:RecordNumber       LIKE(rta:RecordNumber)         !Primary key field - type derived from field
rta:Archive            LIKE(rta:Archive)              !Browse key field - type derived from field
rta:Processed          LIKE(rta:Processed)            !Browse key field - type derived from field
rta:ExchangeOrder      LIKE(rta:ExchangeOrder)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(RTNAWAIT_ALIAS)
                       PROJECT(rta_ali:CNRRecordNumber)
                       PROJECT(rta_ali:PartNumber)
                       PROJECT(rta_ali:Description)
                       PROJECT(rta_ali:Location)
                       PROJECT(rta_ali:Archive)
                       PROJECT(rta_ali:Processed)
                       PROJECT(rta_ali:ExchangeOrder)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
rta_ali:CNRRecordNumber LIKE(rta_ali:CNRRecordNumber) !List box control field - type derived from field
rta_ali:CNRRecordNumber_NormalFG LONG                 !Normal forground color
rta_ali:CNRRecordNumber_NormalBG LONG                 !Normal background color
rta_ali:CNRRecordNumber_SelectedFG LONG               !Selected forground color
rta_ali:CNRRecordNumber_SelectedBG LONG               !Selected background color
rta_ali:PartNumber     LIKE(rta_ali:PartNumber)       !List box control field - type derived from field
rta_ali:PartNumber_NormalFG LONG                      !Normal forground color
rta_ali:PartNumber_NormalBG LONG                      !Normal background color
rta_ali:PartNumber_SelectedFG LONG                    !Selected forground color
rta_ali:PartNumber_SelectedBG LONG                    !Selected background color
rta_ali:Description    LIKE(rta_ali:Description)      !List box control field - type derived from field
rta_ali:Description_NormalFG LONG                     !Normal forground color
rta_ali:Description_NormalBG LONG                     !Normal background color
rta_ali:Description_SelectedFG LONG                   !Selected forground color
rta_ali:Description_SelectedBG LONG                   !Selected background color
rta_ali:Location       LIKE(rta_ali:Location)         !List box control field - type derived from field
rta_ali:Location_NormalFG LONG                        !Normal forground color
rta_ali:Location_NormalBG LONG                        !Normal background color
rta_ali:Location_SelectedFG LONG                      !Selected forground color
rta_ali:Location_SelectedBG LONG                      !Selected background color
rta_ali:Archive        LIKE(rta_ali:Archive)          !Browse key field - type derived from field
rta_ali:Processed      LIKE(rta_ali:Processed)        !Browse key field - type derived from field
rta_ali:ExchangeOrder  LIKE(rta_ali:ExchangeOrder)    !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::rta:Archive         LIKE(rta:Archive)
HK11::rta:ExchangeOrder   LIKE(rta:ExchangeOrder)
HK11::rta:Processed       LIKE(rta:Processed)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::rta_ali:Archive     LIKE(rta_ali:Archive)
HK12::rta_ali:ExchangeOrder LIKE(rta_ali:ExchangeOrder)
HK12::rta_ali:Processed   LIKE(rta_ali:Processed)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Tools'),USE(?Tools2)
                           ITEM('Archive Items'),USE(?ToolsArchiveItems)
                         END
                       END
                       BUTTON,AT(649,3,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(64,41,552,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Spare / Exchange / Loan Processing'),AT(69,43),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(565,43),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,55,552,30),USE(?Panel5),FILL(09A6A7CH)
                       LIST,AT(68,119,264,214),USE(?List),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('68R(2)|M~Credit Note Req No~@n_10@90L(2)|M~Part Number / Model No~@s30@90L(2)|M~' &|
   'Description / I.M.E.I. No~@s30@120L(2)|M~Franchise~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       SHEET,AT(64,87,272,278),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION('Filter'),AT(214,55,252,26),USE(locFilter),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             RADIO('All'),AT(222,65),USE(?locFilter:Radio1),TRN,FONT(,,COLOR:White,FONT:bold),VALUE('99')
                             RADIO('Spares Only'),AT(250,65),USE(?locFilter:Radio1:2),TRN,FONT(,,COLOR:White,FONT:bold),VALUE('0')
                             RADIO('Exchanges Only'),AT(314,65),USE(?locFilter:Radio1:3),TRN,FONT(,,COLOR:White,FONT:bold),VALUE('1')
                             RADIO('Loans Only'),AT(394,65),USE(?locFilter:Radio1:4),TRN,FONT(,,COLOR:White,FONT:bold),VALUE('2')
                           END
                           PROMPT('Awaiting Processing'),AT(68,91),USE(?Prompt5),TRN,FONT(,10,COLOR:White,FONT:bold)
                           ENTRY(@n_10),AT(68,105,64,10),USE(rta:CNRRecordNumber),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(68,336),USE(?buttonAcceptReturn),TRN,FLAT,ICON('accretp.jpg')
                           BUTTON,AT(140,336),USE(?buttonRejectReturn),TRN,FLAT,ICON('rejretp.jpg')
                         END
                       END
                       SHEET,AT(344,87,272,278),USE(?Sheet1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Processed'),AT(348,91),USE(?Prompt5:2),TRN,FONT(,10,COLOR:White,FONT:bold)
                           PANEL,AT(556,93,11,9),USE(?Panel3),FILL(COLOR:Green)
                           PROMPT('- Accepted'),AT(572,93),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('- Rejected'),AT(572,103),USE(?Prompt3:2),TRN,FONT(,,COLOR:White,FONT:bold)
                           PANEL,AT(556,103,11,9),USE(?Panel3:2),FILL(COLOR:Red)
                           ENTRY(@n_10),AT(348,107,64,10),USE(rta_ali:CNRRecordNumber),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           LIST,AT(348,119,264,214),USE(?List:2),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('68R(2)|M*~Credit Note Req No~@n_10@90L(2)|M*~Part Number / Model No~@s30@90L(2)|' &|
   'M*~Description / I.M.E.I. No~@s30@120L(2)|M*~Franchise~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(548,336),USE(?buttonReprintGRN),TRN,FLAT,ICON('repgrnp.jpg')
                         END
                       END
                       PANEL,AT(64,367,552,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW8::Sort1:Locator  EntryLocatorClass                !Conditional Locator - locFilter <> 99
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator EntryLocatorClass                !Default Locator
BRW10::Sort1:Locator EntryLocatorClass                !Conditional Locator - locFilter <> 99
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
MarkAsProcessed     PROCEDURE(STRING fType)
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
AveragePrice        ROUTINE
    DATA
SavePurchaseCost        REAL()
    CODE
        glo:ErrorText = ''
        SavePurchaseCost = sto:Purchase_Cost
        sto:Purchase_Cost   = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_Stock,sto:Purchase_Cost),@n14.2)
        If glo:ErrorText <> ''
            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                 'ADD', | ! Transaction_Type
                                 '', | ! Depatch_Note_Number
                                 0, | ! Job_Number
                                 0, | ! Sales_Number
                                 0, | ! Quantity
                                 sto:Purchase_Cost, | ! Purchase_Cost
                                 sto:Sale_Cost, | ! Sale_Cost
                                 sto:Retail_Cost, | ! Retail_Cost
                                 'AVERAGE COST CALCULATION', | ! Notes
                                 Clip(glo:ErrorText),|
                                                    sto:AveragePurchaseCost,|
                                                    savePurchaseCost,|
                                                    sto:Sale_Cost,|
                                                    sto:Retail_Cost)
                ! Added OK
            Else ! AddToStockHistory
                ! Error
            End ! AddToStockHistory
        End !If glo:ErrorText <> ''

        glo:ErrorText = ''
        If sto:Percentage_Mark_Up <> 0
            sto:Sale_Cost   = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:Percentage_Mark_Up/100))
        End !If sto:Percentage_Mark_Up <> 0
        If sto:RetailMarkUp <> 0
            sto:Retail_Cost = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:RetailMarkUp/100))
        End !If sto:RetailMarkUp <> 0
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:027630'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReturnProcessing')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:GRNOTESP.Open
  Relate:JOBS.Open
  Relate:RTNAWAIT_ALIAS.Open
  Relate:RTNORDER.Open
  Relate:STOCK_ALIAS.Open
  Relate:WAYLAWT.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:EXCHHIST.UseFile
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:RTNAWAIT,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:RTNAWAIT_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,rta:ArcProExcCNRKey)
  BRW8.AddRange(rta:ExchangeOrder)
  BRW8.AddLocator(BRW8::Sort1:Locator)
  BRW8::Sort1:Locator.Init(?rta:CNRRecordNumber,rta:CNRRecordNumber,1,BRW8)
  BRW8.AddSortOrder(,rta:ArcProCNRKey)
  BRW8.AddRange(rta:Processed)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?rta:CNRRecordNumber,rta:CNRRecordNumber,1,BRW8)
  BRW8.AddField(rta:CNRRecordNumber,BRW8.Q.rta:CNRRecordNumber)
  BRW8.AddField(rta:PartNumber,BRW8.Q.rta:PartNumber)
  BRW8.AddField(rta:Description,BRW8.Q.rta:Description)
  BRW8.AddField(rta:Location,BRW8.Q.rta:Location)
  BRW8.AddField(rta:RecordNumber,BRW8.Q.rta:RecordNumber)
  BRW8.AddField(rta:Archive,BRW8.Q.rta:Archive)
  BRW8.AddField(rta:Processed,BRW8.Q.rta:Processed)
  BRW8.AddField(rta:ExchangeOrder,BRW8.Q.rta:ExchangeOrder)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,rta_ali:ArcProExcCNRKey)
  BRW10.AddRange(rta_ali:ExchangeOrder)
  BRW10.AddLocator(BRW10::Sort1:Locator)
  BRW10::Sort1:Locator.Init(?rta_ali:CNRRecordNumber,rta_ali:CNRRecordNumber,1,BRW10)
  BRW10.AddSortOrder(,rta_ali:ArcProCNRKey)
  BRW10.AddRange(rta_ali:Processed)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(?rta_ali:CNRRecordNumber,rta_ali:CNRRecordNumber,1,BRW10)
  BRW10.AddField(rta_ali:CNRRecordNumber,BRW10.Q.rta_ali:CNRRecordNumber)
  BRW10.AddField(rta_ali:PartNumber,BRW10.Q.rta_ali:PartNumber)
  BRW10.AddField(rta_ali:Description,BRW10.Q.rta_ali:Description)
  BRW10.AddField(rta_ali:Location,BRW10.Q.rta_ali:Location)
  BRW10.AddField(rta_ali:Archive,BRW10.Q.rta_ali:Archive)
  BRW10.AddField(rta_ali:Processed,BRW10.Q.rta_ali:Processed)
  BRW10.AddField(rta_ali:ExchangeOrder,BRW10.Q.rta_ali:ExchangeOrder)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:GRNOTESP.Close
    Relate:JOBS.Close
    Relate:RTNAWAIT_ALIAS.Close
    Relate:RTNORDER.Close
    Relate:STOCK_ALIAS.Close
    Relate:WAYLAWT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '027630'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('027630'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('027630'&'0')
      ***
    OF ?locFilter
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW8.ApplyRange
      BRW8.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW10.ApplyRange
      BRW10.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?buttonAcceptReturn
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonAcceptReturn, Accepted)
      BRW8.UpdateViewRecord()
      IF (rta:PartNumber <> '') ! Make sure we have got a record
          IF (rta:ExchangeOrder = 0)
              Beep(Beep:SystemQuestion)  ;  Yield()
              Case Missive('Please confirm that item ' & Clip(rta:PartNumber) & ' has passed QA and that you wish to accept the return.','ServiceBase',|
                             'mquest.jpg','\Cancel|/&Confirm') 
              Of 2 ! &Confirm Button
              Of 1 ! Cancel Button
                  CYCLE
              End!Case Message
              ! Accepting a spare
              Access:STOCK.Clearkey(sto:Location_Key)
              sto:Location = MainStoreLocation()
              sto:Part_Number = rta:PartNumber
              IF (Access:STOCK.Tryfetch(sto:Location_Key))
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to find the selected part in the Main Store.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
      
              IF (StockInUse(1))
                  CYCLE
              END
      
              ! Add to stock
              sto:Quantity_Stock += rta:Quantity
      
              DO AveragePrice
      
              IF (Access:STOCK.TryUpdate())
                  STOP('An error occurred adding the stock item')
                  CYCLE
              END
      
              RELEASE(STOCK)
      
              ! Mark Return As "Processed"
              rta:NewRefNumber = sto:Ref_Number ! Save the pointer to the Main Store Stock Item
              MarkAsProcessed('ACCEPT')
      
          ELSIF (rta:ExchangeOrder = 1) ! IF (rtn:ExchangeOrder = 0)
              ! Accepting an exchange
      
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number = rta:RefNumber
              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to find the selected Exchange Unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              ! Get the stock type to move the exchange unit to
              IF (GetReturnExchangeStockType(rta:Description,xch:Stock_Type,0) = FALSE)
                  CYCLE
              END
              ! Move the exchange unit to the Main Store and make it available
              xch:Location = MainStoreLocation()
              xch:Available = 'AVL'
              IF (Access:EXCHANGE.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to update Exchange unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
      
              ! Mark Return As "Processed"
              MarkAsProcessed('ACCEPT')
      
      
          ELSIF (rta:ExchangeOrder = 2) ! Loan Unit
              ! Accepted A Loan
              Access:LOAN.Clearkey(loa:Ref_Number_Key)
              loa:Ref_Number = rta:RefNumber
              IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key))
                 Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to find the selected Loan Unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              ! Get the stock type to move the exchange unit to
              IF (GetReturnExchangeStockType(rta:Description,loa:Stock_Type,1) = FALSE)
                  CYCLE
              END
              ! Move the exchange unit to the Main Store and make it available
              loa:Location = MainStoreLocation()
              loa:Available = 'AVL'
              IF (Access:LOAN.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to update Loan unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
      
              ! Mark Return As "Processed"
              MarkAsProcessed('ACCEPT')
      
          END ! IF (rtn:ExchangeOrder = 0)
      
      END ! IF (rtn:PartNumber <> '') ! Make sure we have got a record
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonAcceptReturn, Accepted)
    OF ?buttonRejectReturn
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonRejectReturn, Accepted)
      BRW8.UpdateViewRecord()
      IF (rta:PartNumber <> '') ! Make sure we have got a record
          ! Get a reason
          glo:EDI_Reason = ''
          Get_EDI_Reason()
          IF (CLIP(glo:EDI_Reason) = '')
              CYCLE
          END
      
          IF (rta:ExchangeOrder = 0)
              locFaultySiteLocation = FaultySiteLocation() ! Get The Faulty Spares Location
      
              IF (locFaultySiteLocation = '')
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You have not setup a Site Location for Rejected Spares.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
              ELSE ! IF (locFaultySiteLocation = '')
      
                  ! See if the spare already exists. If not, add it
                  Access:STOCK.Clearkey(sto:Location_Key)
                  sto:Location = locFaultySiteLocation
                  sto:Part_Number = rta:PartNumber
                  IF (Access:STOCK.Tryfetch(sto:Location_Key))
                      ! Part does not exist add it based on the original part
                      Access:STOCK_ALIAS.Clearkey(sto_ali:Ref_Number_Key)
                      sto_ali:Ref_Number = rta:RefNumber
                      IF (Access:STOCK_ALIAS.TryFetch(sto_ali:Ref_Number_Key) = Level:Benign)
                          IF (Access:STOCK.PrimeRecord() = Level:Benign)
                              recNo# = sto:Ref_Number
                              sto:Record :=: sto_ali:Record
                              sto:Ref_Number = recNo#
                              sto:Quantity_Stock = 0
                              sto:Location = locFaultySiteLocation
      
                              ! Get the return record, to add the part cost values
                              Access:RTNORDER.Clearkey(rtn:RecordNumberKey)
                              rtn:RecordNumber = rta:RTNRecordNumber
                              IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey) = Level:Benign)
                                  sto:Purchase_Cost = rtn:PurchaseCost
                                  sto:Sale_Cost = rtn:SaleCost
                              END
      
                              IF (Access:STOCK.TryInsert())
                                  Access:STOCK.CancelAutoInc()
                                  STOP('An Error Occurred Creating The Stock Item')
                                  CYCLE
                              END
                              
                          END ! IF (Access:STOCK.PrimeRecord() = Level:Benign)
                      END ! IF (Access:STOCK_ALIAS.TryFetch(sto_ali:Ref_Number_Key) = Level:Benign)
      
                  ELSE
      
                  END
      
                  ! Mark Return As "Processed"
                  rta:NewRefNumber = sto:Ref_Number ! Save the pointer to the Faulty Store Stock Item
                  MarkAsProcessed('REJECT')
              END ! IF (locFaultySiteLocation = '')
      
          ELSIF (rta:ExchangeOrder = 1) ! Exchange
              ! Accepting an exchange
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number = rta:RefNumber
              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to find the selected Exchange Unit.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
              ELSE ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
              
      
                  rtaRecord# = rta:RecordNumber ! Save pointer to reget after booking job
                  ! Book New Job
      
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number = glo:UserAccountNumber
                  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                      STOP('An Error Has Occurred')
                      CYCLE
                  END
                  IF (tra:RtnExchangeAccount = '')
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('You have not set-up a Returns Exchange Account.','ServiceBase',|
                                     'mstop.jpg','/&OK') 
                      Of 1 ! &OK Button
                      End!Case Message
                  ELSE ! IF (tra:RtnExchangeAccount = '')
                  
      
                      Clear(gNewJob)
                      gNewJob.IMEINumber = xch:ESN
                      gNewJob.ModelNumber = xch:Model_Number
                      gNewJob.UnitType = 'HANDSET'
                      gNewJob.Type = 'RTN'
                      gNewJob.AccountNumber = tra:RtnExchangeAccount
      
                      Clear(gExchOrd)
                      ExchangeLoanOrderDetails(xch:Ref_Number,gExchOrd)
                      gNewJob.ActivationDate = gExchOrd.OrderedDate
                      
                      GlobalRequest = InsertRecord
                      glo:Insert_Global = 'YES'
                      Update_Jobs(,ADDRESS(gNewJob))
                      IF (GlobalResponse = RequestCancelled)
                          ! Didn't book the job, don't go any further
                          CYCLE
                      END
      
                      ! Reget Record
                      Access:RTNAWAIT.Clearkey(rta:RecordNumberKey)
                      rta:RecordNumber = rtaRecord#
                      IF (Access:RTNAWAIT.TryFetch(rta:RecordNumberKey))
                      END
      
                      ! Mark Return As "Processed"
                      MarkAsProcessed('REJECT')
                  
                  END ! IF (tra:RtnExchangeAccount = '')
              END  ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
          ELSIF (rta:ExchangeOrder = 2)
              ! Rejecting Loan
              Access:LOAN.Clearkey(loa:Ref_Number_Key)
              loa:Ref_Number = rta:RefNumber
              IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key))
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to find selected Loan Unit.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
      
              loa:Available = 'RTF' ! Return To Franchise
              loa:StatusChangeDate = TODAY()
              IF (Access:LOAN.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to update Loan unit.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              ! Mark Return As "Processed"
              MarkAsProcessed('REJECT')   
          END ! IF (rtn:ExchangeOrder = 0)
          glo:EDI_Reason = ''
      END ! IF (rtn:PartNumber <> '') ! Make sure we have got a record
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonRejectReturn, Accepted)
    OF ?buttonReprintGRN
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReprintGRN, Accepted)
      ! Reprint GRN Note
      BRW10.UpdateViewRecord()
      IF (rta_ali:PartNumber <> '')
          ! Make sure we have got a record
          GoodsReceivedNoteReturnsProcess(rta_ali:GRNNumber)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReprintGRN, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
MarkAsProcessed     PROCEDURE(STRING fType)
locHistoryNotes     STRING(255)
    CODE
        rta:Processed = TRUE
        rta:DateProcessed = TODAY()
        rta:TimeProcessed = CLOCK()
        rta:usercodeProcessed = glo:Usercode
        rta:AcceptReject = fType
        IF (Access:RTNAWAIT.TryUpdate())
        END

        ! Create GRN
        IF (Access:GRNOTESP.PrimeRecord() = Level:Benign)
            grp:RTARecordNumber = rta:RecordNumber
            grp:Usercode = glo:Usercode
            grp:AcceptReject = fType
            IF (Access:GRNOTESP.TryInsert())
                Access:GRNOTESP.CancelAutoInc()
                STOP('An Error Occurred')
            ELSE
                rta:GRNNumber = grp:RecordNumber
                Access:RTNAWAIT.TryUpdate()

                ! Set the history notes
                locHistoryNotes = 'CREDIT NOTE REQ NO: ' & rta:CNRRecordNumber & |
                                    '<13,10>GRN NO: ' & rta:GRNNumber

                IF (fType = 'REJECT')
                    ! Add the rejection reason
                    locHistoryNotes = CLIP(locHistoryNotes) & '<13,10>QTY: ' & rta:Quantity & '<13,10>' & CLIP(glo:EDI_Reason)
                END

                IF (rta:ExchangeOrder = 0)
                    IF (fType = 'REJECT')
                        ! Don't show quantity on STock History Entry
                        rta:Quantity = 0
                    END
                    AddStockHistory('ADD',rta:Quantity,'STOCK ITEM RETURN ' & CLIP(fType) & 'ED',locHistoryNotes) ! Add history item
                ELSIF (rta:ExchangeOrder = 1)
                    AddExchangeHistory(xch:Ref_Number,'EXCHANGE UNIT RETURN ' & CLIP(fType) & 'ED',locHistoryNotes) ! Add history item
                ELSIF (rta:ExchangeOrder = 2)
                    AddLoanHistory(loa:Ref_Number,'LOAN UNIT RETURN ' & CLIP(fType) & 'ED',locHistoryNotes) ! Add history item
                END

                ! IF REJECT Build List TO Send Back On waybill

                IF (rta:ExchangeOrder = 2 AND fType = 'REJECT')
                     ! Add to waybill pending list
                    IF (Access:WAYLAWT.PrimeRecord() = Level:Benign)
                        wal:IMEINumber = loa:ESN
                        wal:LOANRefNumber = loa:Ref_Number
                        wal:RTARecordNumber = rta:RecordNumber
                        wal:Location = rta:Location
                        IF (Access:WAYLAWT.TryInsert())
                            Access:WAYLAWT.CancelAutoInc()
                        END
                    END
                END
                GoodsReceivedNoteReturnsProcess(grp:RecordNumber)
            END
            
        END

        BRW8.ResetSort(1)
        BRW10.ResetSort(1)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF locFilter <> 99
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = locFilter
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW8.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF locFilter <> 99
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF locFilter <> 99
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = locFilter
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW10.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF locFilter <> 99
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.rta_ali:CNRRecordNumber_NormalFG = -1
  SELF.Q.rta_ali:CNRRecordNumber_NormalBG = -1
  SELF.Q.rta_ali:CNRRecordNumber_SelectedFG = -1
  SELF.Q.rta_ali:CNRRecordNumber_SelectedBG = -1
  SELF.Q.rta_ali:PartNumber_NormalFG = -1
  SELF.Q.rta_ali:PartNumber_NormalBG = -1
  SELF.Q.rta_ali:PartNumber_SelectedFG = -1
  SELF.Q.rta_ali:PartNumber_SelectedBG = -1
  SELF.Q.rta_ali:Description_NormalFG = -1
  SELF.Q.rta_ali:Description_NormalBG = -1
  SELF.Q.rta_ali:Description_SelectedFG = -1
  SELF.Q.rta_ali:Description_SelectedBG = -1
  SELF.Q.rta_ali:Location_NormalFG = -1
  SELF.Q.rta_ali:Location_NormalBG = -1
  SELF.Q.rta_ali:Location_SelectedFG = -1
  SELF.Q.rta_ali:Location_SelectedBG = -1
   
   
   IF (rta_ali:AcceptReject = 'REJECT')
     SELF.Q.rta_ali:CNRRecordNumber_NormalFG = 255
     SELF.Q.rta_ali:CNRRecordNumber_NormalBG = 16777215
     SELF.Q.rta_ali:CNRRecordNumber_SelectedFG = 16777215
     SELF.Q.rta_ali:CNRRecordNumber_SelectedBG = 255
   ELSE
     SELF.Q.rta_ali:CNRRecordNumber_NormalFG = 32768
     SELF.Q.rta_ali:CNRRecordNumber_NormalBG = 16777215
     SELF.Q.rta_ali:CNRRecordNumber_SelectedFG = 16777215
     SELF.Q.rta_ali:CNRRecordNumber_SelectedBG = 32768
   END
   IF (rta_ali:AcceptReject = 'REJECT')
     SELF.Q.rta_ali:PartNumber_NormalFG = 255
     SELF.Q.rta_ali:PartNumber_NormalBG = 16777215
     SELF.Q.rta_ali:PartNumber_SelectedFG = 16777215
     SELF.Q.rta_ali:PartNumber_SelectedBG = 255
   ELSE
     SELF.Q.rta_ali:PartNumber_NormalFG = 32768
     SELF.Q.rta_ali:PartNumber_NormalBG = 16777215
     SELF.Q.rta_ali:PartNumber_SelectedFG = 16777215
     SELF.Q.rta_ali:PartNumber_SelectedBG = 32768
   END
   IF (rta_ali:AcceptReject = 'REJECT')
     SELF.Q.rta_ali:Description_NormalFG = 255
     SELF.Q.rta_ali:Description_NormalBG = 16777215
     SELF.Q.rta_ali:Description_SelectedFG = 16777215
     SELF.Q.rta_ali:Description_SelectedBG = 255
   ELSE
     SELF.Q.rta_ali:Description_NormalFG = 32768
     SELF.Q.rta_ali:Description_NormalBG = 16777215
     SELF.Q.rta_ali:Description_SelectedFG = 16777215
     SELF.Q.rta_ali:Description_SelectedBG = 32768
   END
   IF (rta_ali:AcceptReject = 'REJECT')
     SELF.Q.rta_ali:Location_NormalFG = 255
     SELF.Q.rta_ali:Location_NormalBG = 16777215
     SELF.Q.rta_ali:Location_SelectedFG = 16777215
     SELF.Q.rta_ali:Location_SelectedBG = 255
   ELSE
     SELF.Q.rta_ali:Location_NormalFG = 32768
     SELF.Q.rta_ali:Location_NormalBG = 16777215
     SELF.Q.rta_ali:Location_SelectedFG = 16777215
     SELF.Q.rta_ali:Location_SelectedBG = 32768
   END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
GetReturnExchangeStockType PROCEDURE (STRING fIMEI,*STRING fStockType,BYTE fLoan) !Generated from procedure template - Window

RtnValue             BYTE
locExchangeStockType STRING(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,-1,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(244,148,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Exchange Unit'),AT(249,151),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(384,151),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,162,192,96),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT(''),AT(248,168,184,28),USE(?PromptStatus),TRN,CENTER,FONT(,,080FFFFH,FONT:bold)
                       PROMPT('Select Stock Type To Return Unit To:'),AT(253,206),USE(?Prompt4),TRN,FONT(,,COLOR:White,FONT:bold)
                       PROMPT('Stock Type'),AT(248,232),USE(?locExchangeStockType:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                       ENTRY(@s30),AT(296,232,112,10),USE(locExchangeStockType),SKIP,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(408,228),USE(?buttonLookupLoan),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                       BUTTON,AT(408,228),USE(?buttonLookup),TRN,FLAT,ICON('lookupp.jpg')
                       PANEL,AT(244,259,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(300,260),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(368,260),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(RtnValue)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020764'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('GetReturnExchangeStockType')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?promptStatus{prop:Text} = 'Please confirm that item ' & Clip(fIMEI) & ' has passed QA and that you wish to accept it''s receipt.'
  IF (fLoan)
      ?WindowTitle{prop:Text} = 'Return Loan Unit'
      ?ButtonLookup{prop:Hide} = 1
      ?ButtonLookupLoan{prop:hide} = 0
  END
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCKTYP.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      IF (locExchangeStockType = '')
          CYCLE
      END
      fStockType = locExchangeStockType
      RtnValue = TRUE
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020764'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020764'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020764'&'0')
      ***
    OF ?buttonLookupLoan
      ThisWindow.Update
      ! Bryan Simple Lookup
      CLEAR(stp:Record)
      stp:Stock_Type = locExchangeStockType
      stp:Use_Loan = 'YES'
      IF (Access:STOCKTYP.TryFetch(stp:Use_Loan_Key))
          ! Failed, set the key instead
          Clear(stp:Record)
          stp:Stock_Type = locExchangeStockType
          stp:Use_Loan = 'YES'
          SET(stp:Use_Loan_Key,stp:Use_Loan_Key)
          Access:STOCKTYP.TryNext()
      END ! IF (Access:STOCKTYP.TryFetch(stp:Use_Loan_Key))
      GlobalRequest = SelectRecord
      PickLoanStockType()
      IF (GlobalResponse = RequestCompleted)
          CHANGE(?locExchangeStockType,stp:Stock_Type)
          POST(Event:Accepted,?locExchangeStockType)
          SELECT(?buttonLookupLoan + 1)
          SELF.Request = SELF.OriginalRequest
      ELSE ! IF (GlobalResponse = RequestCompleted)
          SELECT(?buttonLookupLoan)
          SELF.Request = SELF.OriginalRequest
      END ! IF (GlobalResponse = RequestCompleted)
    OF ?buttonLookup
      ThisWindow.Update
      ! Bryan Simple Lookup
      CLEAR(stp:Record)
      stp:Stock_Type = locExchangeStockType
      stp:Use_Exchange = 'YES'
      IF (Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key))
          ! Failed, set the key instead
          Clear(stp:Record)
          stp:Stock_Type = locExchangeStockType
          stp:Use_Exchange = 'YES'
          SET(stp:Use_Exchange_Key,stp:Use_Exchange_Key)
          Access:STOCKTYP.TryNext()
      END ! IF (Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key))
      GlobalRequest = SelectRecord
      PickExchangeStockType()
      IF (GlobalResponse = RequestCompleted)
          CHANGE(?locExchangeStockType,stp:Stock_Type)
          POST(Event:Accepted,?locExchangeStockType)
          SELECT(?buttonLookup + 1)
          SELF.Request = SELF.OriginalRequest
      ELSE ! IF (GlobalResponse = RequestCompleted)
          SELECT(?buttonLookup)
          SELF.Request = SELF.OriginalRequest
      END ! IF (GlobalResponse = RequestCompleted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
