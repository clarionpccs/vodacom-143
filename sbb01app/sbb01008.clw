

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01008.INC'),ONCE        !Local module procedure declarations
                     END


EnterReturnInvoiceNumber PROCEDURE                    !Generated from procedure template - Window

locInvoiceNumber     LONG
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,-1,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(244,149,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Enter Return Invoice Number'),AT(249,151),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(385,151),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,162,192,96),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Unable to find the associated Invoice Number for the selected I.M.E.I.'),AT(248,166,184,26),USE(?Prompt3),TRN,CENTER,FONT(,,080FFFFH,FONT:bold)
                       PROMPT('Enter the invoice number below'),AT(248,200,184,16),USE(?Prompt3:2),TRN,CENTER,FONT(,,080FFFFH,FONT:bold)
                       PROMPT('Invoice Number'),AT(272,234),USE(?locInvoiceNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@n_8),AT(344,234,64,10),USE(locInvoiceNumber),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ
                       PANEL,AT(244,259,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(368,260),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(300,260),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(locInvoiceNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020765'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EnterReturnInvoiceNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      locInvoiceNumber = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      IF (locInvoiceNumber = 0)
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020765'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020765'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020765'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReturnLoanProcess PROCEDURE                           !Generated from procedure template - Window

locIMEINumber        STRING(30)
locReturnType        STRING(30)
locTRUE              BYTE(1)
qIMEI                QUEUE,PRE(imei)
IMEINumber           STRING(30)
InvoiceNo            LONG
SaleNo               LONG
IMEIRefNumber        LONG
Price                REAL
                     END
locOrderNumber       LONG
locInvoiceNumber     LONG
locPrice             REAL
locReceivedDate      DATE
gExchOrd             QUEUE(gExchangeOrders),PRE()
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Return Stock Process'),AT(168,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,84,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Return Loan Units'),USE(?Tab1)
                           PROMPT('I.M.E.I. Number To Return'),AT(228,140),USE(?locIMEINumber:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(328,140,124,10),USE(locIMEINumber),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('List Of IMEI''s To Return'),AT(168,182),USE(?Prompt5),FONT(,,,FONT:bold)
                           LIST,AT(168,192,344,136),USE(?List2),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~I.M.E.I. No~@s30@56R(2)|M~Invoice No~@n_10@56R(2)|M~Sale No~@n_10@'),FROM(qIMEI)
                         END
                       END
                       BUTTON,AT(444,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(376,334),USE(?buttonReturnStock),TRN,FLAT,ICON('retloanp.jpg')
                       PANEL,AT(164,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020769'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReturnLoanProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?buttonReturnStock,RequestCancelled)
  Relate:LOAN.Open
  Relate:RETSALES.Open
  Relate:RTNORDER.Open
  Relate:SUBTRACC.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:RETSTOCK.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOAN.Close
    Relate:RETSALES.Close
    Relate:RTNORDER.Close
    Relate:SUBTRACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?buttonReturnStock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnStock, Accepted)
      IF (Records(qIMEI) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not selected an I.M.E.I.s to return.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to return the selected I.M.E.I. Numbers?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes') 
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      
      LOOP ll# = 1 TO RECORDS(qIMEI)
          GET(qIMEI,ll#)
          ! Create Return Order
          IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
              Access:LOAN.Clearkey(loa:Ref_Number_Key)
              loa:Ref_Number = qIMEI.IMEIRefNumber
              IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                  CYCLE
              END
              IF (loa:Available <> 'AVL')
                  ! Double check the unit is still available
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('The selected loan unit is no longer available: ' & Clip(loa:ESN) & '.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              ! Mark the unit for return to main store
              loa:Available = 'RTM'
              IF (Access:LOAN.TryUpdate())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to update loan unit ' & Clip(loa:ESN) & '.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              AddLoanHistory(loa:Ref_Number,'RETURN ORDER GENERATED')
      
              rtn:Location = glo:Location
              rtn:UserCode = glo:UserCode
              rtn:RefNumber = qIMEI.IMEIRefNumber
              rtn:OrderNumber = qIMEI.SaleNo
              rtn:InvoiceNumber = qIMEI.InvoiceNo
              rtn:PartNumber = loa:Model_Number
              rtn:Description = loa:ESN
              rtn:QuantityReturned = 1
              rtn:ExchangeOrder = 2  ! #12341 Use '2' to signify Loan Unit (DBH: 27/01/2012)
              rtn:Status = 'NEW'
              rtn:Notes = ''
              rtn:ExchangePrice = qIMEI.Price
              IF (Access:RTNORDER.TryInsert())
                  Access:RTNORDER.CancelAutoInc()
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnStock, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020769'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020769'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020769'&'0')
      ***
    OF ?locIMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locIMEINumber, Accepted)
      qIMEI.IMEINumber = locIMEINumber
      GET(qIMEI,qIMEI.IMEINumber)
      IF (NOT ERROR())
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have already entered the selected I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Select(?locIMEINumber)
          CYCLE
      END
      
      ! Get Exchange Unit
      Access:LOAN.Clearkey(loa:ESN_Only_Key)
      loa:ESN = locIMEINumber
      IF (Access:LOAN.TryFetch(loa:ESN_Only_Key))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Unable to find the selected I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Select(?locIMEINumber)
          CYCLE
      END
      IF (loa:Location <> glo:Location)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected I.M.E.I. is not in your location.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          SELECT(?locIMEINUmber)
          CYCLE
      END
      IF (loa:Available <> 'AVL')
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('Warning! The selected I.M.E.I. number is not "Available".'&|
              '|'&|
              '|Are you sure you want to continue?','ServiceBase',|
                         'mexclam.jpg','\&No|/&Yes') 
          Of 2 ! &Yes Button
          Of 1 ! &No Button
              SELECT(?locIMEINumber)
              CYCLE
          End!Case Message
      END
      
      ! Is the unit already on a returns order
      IF (ReturnUnitAlreadyOnOrder(loa:Ref_Number,2))  ! #12341 Using exchangeunit = 2 as a flag for Loan Units (DBH: 27/01/2012)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Error! The selected unit is awaiting, or is already on a Returns Order.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Select(?locIMEINumber)
          CYCLE
      END
      
      locOrderNumber = 0
      locInvoiceNumber = 0
      locPrice = 0
      locReceivedDate = 0
      
      ! Get the Sale and check the dates
      ExchangeLoanOrderDetails(loa:Ref_Number,gExchOrd,1) ! #12341 Pass '1' to get Loan details (DBH: 27/01/2012)
      
      locInvoiceNumber    = gExchOrd.InvoiceNumber
      locReceivedDate     = gExchOrd.ReceivedDate
      locOrderNumber      = gExchOrd.OrderNumber
      locPrice            = gExchOrd.Price
      
      ! If you can't find the invoice number, ask the user to enter it.
      ! I don't think this is necessary, but it's a fall back incase.
      IF (locInvoiceNumber = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Unable to find the original invoice for the selected I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      IF (locReceivedDate > 0)
          IF (GETINI('STOCK','UseLoanReturnDays',,PATH() & '\SB2KDEF.INI') = 1)
              IF (GETINI('STOCK','LoanReturnDays',0,PATH() & '\SB2KDEF.INI') + locReceivedDate < TODAY())
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Loan cannot be returned as was received at Franchise over the set period.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Select(?locIMEINumber)
                  CYCLE
              END
          END !IF (GETINI('STOCK','UseLoanReturnDays',,PATH() & '\SB2KDEF.INI') = 1)
      END
      
      ! Add To Temp Q
      qIMEI.IMEINumber = locIMEINumber
      qIMEI.InvoiceNo = locInvoiceNumber
      qIMEI.SaleNo = locOrderNumber
      qIMEI.IMEIRefNumber = loa:Ref_Number
      qIMEI.Price = locPrice
      ADD(qIMEI)
      locIMEINumber = ''
      Select(?locIMEINumber)
      DISPLAY()
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locIMEINumber, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
LoanWaybillGeneration PROCEDURE                       !Generated from procedure template - Window

locIMEINumber        STRING(30)
locWaybillNumber     STRING(20)
locFALSE             BYTE(0)
locTRUE              BYTE(1)
qWaybillList         QUEUE,PRE(qwl)
Location             STRING(30)
WaybillNumber        LONG
                     END
BRW9::View:Browse    VIEW(WAYLAWT)
                       PROJECT(wal:IMEINumber)
                       PROJECT(wal:RecordNumber)
                       PROJECT(wal:Ready)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
wal:IMEINumber         LIKE(wal:IMEINumber)           !List box control field - type derived from field
wal:RecordNumber       LIKE(wal:RecordNumber)         !Primary key field - type derived from field
wal:Ready              LIKE(wal:Ready)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(WAYLAWT_ALIAS)
                       PROJECT(wal_ali:IMEINumber)
                       PROJECT(wal_ali:Ready)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wal_ali:IMEINumber     LIKE(wal_ali:IMEINumber)       !List box control field - type derived from field
wal_ali:Ready          LIKE(wal_ali:Ready)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(650,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,69,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Loan Waybill Generation'),AT(168,71),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,250),USE(?Sheet1),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Awaiting Processing'),AT(172,90),USE(?Prompt4),TRN,FONT(,,080FFFFH,FONT:bold)
                           PROMPT('Awaiting Waybill Generation'),AT(356,92),USE(?Prompt4:2),TRN,FONT(,,080FFFFH,FONT:bold)
                           LIST,AT(172,104,152,164),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('120L(2)|M~I.M.E.I. Number~@s30@'),FROM(Queue:Browse)
                           LIST,AT(356,104,152,164),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('120L(2)|M~I.M.E.I. Number~@s30@'),FROM(Queue:Browse:1)
                           PROMPT('Process I.M.E.I. Number:'),AT(172,300),USE(?locIMEINumber:Prompt),TRN,FONT(,,,FONT:bold)
                           ENTRY(@s30),AT(172,312,124,10),USE(locIMEINumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(260,272),USE(?btnDelete),TRN,FLAT,ICON('deletep.jpg')
                           BUTTON,AT(440,274),USE(?btnDelete:2),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       PANEL,AT(164,333,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,334),USE(?btnGenerateWaybill),TRN,FLAT,ICON('genwayp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_wal              CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW_walali           CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020770'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('LoanWaybillGeneration')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOAN.Open
  Relate:RTNORDER.Open
  Relate:TRADEACC.Open
  Relate:WAYAUDIT.Open
  Relate:WAYLAWT.Open
  Relate:WAYLAWT_ALIAS.Open
  Access:LOANHIST.UseFile
  Access:WAYBILLS.UseFile
  Access:WAYCNR.UseFile
  SELF.FilesOpened = True
  BRW_wal.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:WAYLAWT,SELF)
  BRW_walali.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:WAYLAWT_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW_wal.Q &= Queue:Browse
  BRW_wal.AddSortOrder(,wal:ReadyIMEINumberKey)
  BRW_wal.AddRange(wal:Ready,locFALSE)
  BRW_wal.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,wal:IMEINumber,1,BRW_wal)
  BRW_wal.AddField(wal:IMEINumber,BRW_wal.Q.wal:IMEINumber)
  BRW_wal.AddField(wal:RecordNumber,BRW_wal.Q.wal:RecordNumber)
  BRW_wal.AddField(wal:Ready,BRW_wal.Q.wal:Ready)
  BRW_walali.Q &= Queue:Browse:1
  BRW_walali.AddSortOrder(,wal_ali:ReadyIMEINumberKey)
  BRW_walali.AddRange(wal_ali:Ready,locTRUE)
  BRW_walali.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,wal_ali:IMEINumber,1,BRW_walali)
  BRW_walali.AddField(wal_ali:IMEINumber,BRW_walali.Q.wal_ali:IMEINumber)
  BRW_walali.AddField(wal_ali:Ready,BRW_walali.Q.wal_ali:Ready)
  BRW_wal.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW_wal.AskProcedure = 0
      CLEAR(BRW_wal.AskProcedure, 1)
    END
  END
  BRW_walali.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW_walali.AskProcedure = 0
      CLEAR(BRW_walali.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOAN.Close
    Relate:RTNORDER.Close
    Relate:TRADEACC.Close
    Relate:WAYAUDIT.Close
    Relate:WAYLAWT.Close
    Relate:WAYLAWT_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGenerateWaybill
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnGenerateWaybill, Accepted)
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to generate a Waybill for the selected I.M.E.I. Numbers?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes')
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      
      FREE(qWaybillList)
      ! Build temp q of locations
      Access:WAYLAWT.Clearkey(wal:ReadyIMEINumberKey)
      wal:Ready = 1
      SET(wal:ReadyIMEINumberKey,wal:ReadyIMEINumberKey)
      LOOP UNTIL Access:WAYLAWT.NEXT()
          IF (wal:Ready <> 1)
              BREAK
          END
          qWaybillList.Location = wal:Location
          GET(qWaybillList,qWaybillList.Location)
          IF ERROR()
              ADD(qWaybillList,qWaybillList.Location)
          END
      END
      
      
      IF (RECORDS(qWaybillList))
          LOOP i# = 1 TO RECORDS(qWaybillList)
              GET(qWaybillList,i#)
      
              ! Create A New Waybill For this location
              locWaybillNumber = NextWaybillNumber()
              IF (locWaybillNumber = 0)
                  STOP('An error occurred')
                  CYCLE
              END
      
              Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
              way:WaybillNumber = locWaybillNumber
              IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                  way:FromAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                  Access:TRADEACC.Clearkey(tra:SiteLocationKey)
                  tra:SiteLocation = qWaybillList.Location
                  IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
                      way:ToAccount = tra:Account_Number
                      way:AccountNumber = tra:Account_Number
                  ELSE
                      STOP('An Error Occurred')
                  END ! IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
                  way:WaybillType = 23 ! FAULT LOAN TO RRC
                  way:WaybillID = 402 ! LOAN
                  way:Courier = 'RAM'
                  way:TheDate = TODAY()
                  way:TheTime = CLOCK()
                  IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
                      If Access:WAYAUDIT.PrimeRecord() = Level:Benign
                          waa:WAYBILLSRecordNumber = way:RecordNumber
                          waa:UserCode = glo:UserCode
                          waa:Action   = 'WAYBILL CREATED'
                          waa:TheDate  = Today()
                          waa:TheTime  = Clock()
                          If Access:WAYAUDIT.TryInsert() = Level:Benign
                                  ! Insert
                                  ! Waybill created (DBH: 04/09/2006)
      
                          Else ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                              Access:WAYAUDIT.CancelAutoInc()
                          End ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                      End ! If Access.WAYAUDIT.PrimeRecord() = Level:Benign
      
                  ELSE
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('Unable to update Waybills.','ServiceBase',|
                                     'mstop.jpg','/&OK')
                      Of 1 ! &OK Button
                      End!Case Message
                      CYCLE
                  END ! IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
              END
      
              ! Now add the individual lines for this location to the waybill
              Access:WAYLAWT.Clearkey(wal:ReadyIMEINumberKey)
              wal:Ready = 1
              SET(wal:ReadyIMEINumberKey,wal:ReadyIMEINumberKey)
              LOOP UNTIL Access:WAYLAWT.NEXT()
                  IF (wal:Ready <> 1)
                      BREAK
                  END
                  IF (wal:Location <> qWaybillList.Location)
                      CYCLE
                  END
      
                  Access:LOAN.Clearkey(loa:Ref_Number_Key)
                  loa:Ref_Number = wal:LOANRefNumber
                  IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key))
                      CYCLE
                  ELSE
                      loa:Available = 'ITF'
                      loa:StatusChangeDate = TODAY()
                      IF (Access:LOAN.TryUpdate() = Level:Benign)
                          AddLoanHistory(loa:Ref_Number,'IN TRANSIT TO FRANCHISE','WAYBILL NO: ' & locWaybillNumber)
                          IF (Access:WAYCNR.PrimeRecord() = Level:Benign)
                              wcr:WAYBILLSRecordNumber = way:RecordNumber
                              wcr:PartNumber = loa:Model_Number
                              wcr:Description = loa:ESN
                              wcr:ExchangeOrder = 2
                              wcr:Quantity = 1
                              wcr:RefNumber = loa:Ref_Number
                              IF (Access:WAYCNR.TryInsert())
                                  Access:WAYCNR.CancelAutoInc()
                              END
                          END ! IF (Access:WAYCNR.PrimeRecord() = Level:Benign)
      
                          Access:WAYLAWT.DeleteRecord(0)
      
                          ! #12532 Add an entry to the Return Stock Tracking (DBH: 10/04/2012)
                          IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
                              rtn:Status = 'PRO' !?
                              rtn:WaybillNumber = locWaybillNumber
                              rtn:Ordered = 2 ! Loan Returning To RRC
                              rtn:UserCode = glo:UserCode
                              rtn:Location = glo:Location
                              rtn:PartNumber = loa:Model_Number
                              rtn:Description = loa:ESN
                              rtn:QuantityReturned = 1
                              rtn:ExchangeOrder = 2
                              IF (Access:RTNORDER.TryInsert())
                                  Access:RTNORDER.CancelAutoInc()
                              END ! IF
                          END ! IF
                      ELSE ! IF (Access:LOAN.TryUpdate() = Level:Benign)
                      END ! IF (Access:LOAN.TryUpdate() = Level:Benign)
                  END
              END ! LOOP
      
              qWaybillList.WaybillNumber = way:WaybillNumber ! Save the waybill number for later
              PUT(qWaybillList)
      
      
          END ! IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
      
          glo:EDI_Reason = '' ! To stop suprious text appearing. Such is the dangers of global variables.
      
          ! Now print all the waybills
          LOOP i# = 1 TO RECORDS(qWaybillList)
              GET(qWaybillList,i#)
              IF (qWaybillList.WaybillNumber > 0)
                  Access:TRADEACC.Clearkey(tra:SiteLocationKey)
                  tra:SiteLocation = qWaybillList.Location
                  IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey))
                      CYCLE
                  END ! IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
                  WayBillDespatch(GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'), 'TRA', tra:Account_Number, 'TRA', qWaybillList.WaybillNumber, 'RAM')
              END
          END ! LOOP
      
      END
      
      brw_wal.ResetSort(1)
      brw_walali.ResetSort(1)
      Select(?locIMEINumber)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnGenerateWaybill, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020770'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020770'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020770'&'0')
      ***
    OF ?locIMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locIMEINumber, Accepted)
      Access:WAYLAWT.Clearkey(wal:ReadyIMEINumberKey)
      wal:Ready = 0
      wal:IMEINumber = locIMEINumber
      IF (Access:WAYLAWT.Tryfetch(wal:ReadyIMEINumberKey))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Invalid I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      wal:Ready = 1
      IF (Access:WAYLAWT.TryUpdate())
          STOP('An Error Occurred')
          CYCLE
      END
      brw_wal.ResetSort(1)
      brw_walali.ResetSort(1)
      Select(?locIMEINumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locIMEINumber, Accepted)
    OF ?btnDelete
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnDelete, Accepted)
      brw_wal.UpdateViewRecord()
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to remove the selected I.M.E.I. Number from WayBill Generation?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes')
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      Access:WAYLAWT.DeleteRecord(0)
      
      brw_wal.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnDelete, Accepted)
    OF ?btnDelete:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnDelete:2, Accepted)
      brw_walali.UpdateViewRecord()
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to remove the selected I.M.E.I. Number back to "un processed"?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes')
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      wal_ali:Ready = 0
      Access:WAYLAWT_ALIAS.TryUpdate()
      
      brw_wal.ResetSort(1)
      brw_walali.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnDelete:2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW_wal.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW_walali.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

ImportStockFile PROCEDURE                             !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
locStockImportFailureFolder STRING(255)
Count                LONG
LocalFilename        STRING(255)
LocalError           BYTE
LocalSupplier        STRING(30)
SupplierChecked      BYTE
LocalMainStore       STRING(30)
LocalStockQueue      QUEUE,PRE(LSQ)
LocalStockPartNumber STRING(30)
                     END
ImportFile      FILE,DRIVER('BASIC'),PRE(IXX),NAME(Filename_1),CREATE,BINDABLE,THREAD
Record              Record
Supplier       String(30)
PartNumber     String(30)
quantity       String(30)
               End     !record
           End         !file definition


ExportFile      FILE,DRIVER('BASIC'),PRE(EXX),NAME(Filename_2),CREATE,BINDABLE,THREAD
Record              Record
LineNumber       String(30)
Supplier         String(30) 
PartNumber       String(30)
Quantity         String(30)
ErrorString      String(100)
                End     !record
            End         !file definition
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,2,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(245,150,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Select File for Import'),AT(249,152),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(380,152),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(245,164,192,94),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Import Filename'),AT(252,190),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s255),AT(252,208,180,10),USE(LocalFilename)
                       BUTTON,AT(404,224),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                       PANEL,AT(245,262,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(292,263),USE(?ButtonOK),FLAT,ICON('okp.jpg')
                       BUTTON,AT(364,264),USE(?ButtonCancel),FLAT,ICON('cancelp.jpg')
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup9          SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
FirstPass       Routine
    !check the import file for any errors

    set(ImportFile)
    next(ImportFile)    !get rid of the first line - just headers

    Loop
        next(ImportFile)
        if error() then break.

        if clip(LocalSupplier) = '' then
            LocalSupplier = IXX:Supplier
        END

        !Multiple Suppliers                 IXX:Supplier
        if LocalSupplier <> IXX:Supplier then
            !as soon as we find any errors - we are not going to import the file
            LocalError = true
            break   
        END

        if SupplierChecked = false then

            SupplierChecked = true
            !Unrecognised Supplier              IXX:Supplier
            Access:Supplier.clearkey(sup:Company_Name_Key)
            sup:Company_Name = IXX:Supplier
            if access:Supplier.fetch(sup:Company_Name_Key)
                !not found
                !as soon as we find any errors - we are not going to import the file
                LocalError = true
                break
            END

            !Supplier on existing requisition   IXX:Supplier
            access:ordpend.clearkey(ope:supplier_name_key)
            ope:supplier = IXX:Supplier
            set(ope:supplier_name_key,ope:supplier_name_key)
            loop
                if access:ordpend.next() then break.
                if ope:supplier <> IXX:Supplier then break.

                If ope:StockReqNumber = 0
                    !as soon as we find any errors - we are not going to import the file
                    LocalError = true
                    Break
                End !If ope:StockReqNumber = 0
            end !loop

        END !if supplierchecked

        if LocalError then break.   !from the loop through input file

        !duplicate Item?
        LocalStockQueue.LSQ:LocalStockPartNumber = IXX:PartNumber
        get(LocalStockQueue,LSQ:LocalStockPartNumber)
        if error() then
            !not found - add it so we can check later
            LocalStockQueue.LSQ:LocalStockPartNumber = IXX:PartNumber
            add(LocalStockQueue)
        ELSE
            LocalError = true
            break
        END

        !Unknown item?
        access:Stock.clearkey(sto:Location_Key)      !includes the partnumber
        sto:Location = LocalMainStore
        sto:Part_Number = IXX:PartNumber
        if access:Stock.fetch(sto:Location_Key)
            LocalError = true
            break
        END

        !sundry, suspended, superceeded
        if sto:Sundry_Item = 'YES' then
            LocalError = true
            break
        END

        if sto:Superceeded = 'YES' then
            LocalError = true
            break
        END
        if sto:Suspend = true then
            LocalError = true
            break
        END

    END !loop through import filename

    EXIT
ImportTheData       Routine

    !We know there are no errors because that was checked on first pass
    !start again from the beginning
    set(ImportFile)
    next(ImportFile)    !get rid of the first line - just headers
    Loop

        next(ImportFile)
        if error() then break.

        access:Stock.clearkey(sto:Location_Key)      !includes the partnumber
        sto:Location = LocalMainStore
        sto:Part_Number = IXX:PartNumber
        if  access:Stock.fetch(sto:Location_Key)
            !this has been checked before
        END

        MakePartsRequest(LocalSupplier,sto:Part_Number,sto:Description,deformat(IXX:Quantity))

    END !loop through import filename

    close(ImportFile)

    EXIT
ReportError         Routine

    !like first pass but this time write all errors to a new file
    locStockImportFailureFolder = GETINI('STOCK','STOCKFAIL','',CLIP(Path()) & '\SB2KDEF.INI')  ! #011096 New default (JAC: 23/03/2012)
    if clip(locStockImportFailureFolder) = '' then
        miss# = missive('Hmm... you have not set up a folder in defaults to receive an error report.|Please do some time soon.|'&|
                    'For the moment this error report will be put in the data folder.',|
                    'ServiceBase 3g','mstop.jpg','/OK')
        locStockImportFailureFolder = path()
    END

    FileName_2 = clip(locStockImportFailureFolder)&'\Errors_'&format(today(),@D12)&'_'&format(clock(),@T5)&'.csv'

    Remove(ExportFile)
    Create(ExportFile)
    Open(ExportFile)

    EXX:LineNumber  = 'ORIGINAL LINE'
    EXX:Supplier    = 'SUPPLIER'
    EXX:PartNumber  = 'PART NUMBER'
    EXX:Quantity    = 'QUANTITY'
    EXX:ErrorString = 'ERROR'
    Add(ExportFile)


    LocalError = false
    SupplierChecked = false
    free(LocalStockQueue)
    Count = 0

    !reset the import
    set(ImportFile)
    next(ImportFile)    !get rid of the first line - just headers
    Loop
        next(ImportFile)
        if error() then break.

        Count += 1

        !Multiple Suppliers                 IXX:Supplier
        if LocalSupplier <> IXX:Supplier then
            EXX:LineNumber  = Count
            EXX:Supplier    = IXX:Supplier
            EXX:PartNumber  = IXX:PartNumber
            EXX:Quantity    = IXX:Quantity
            EXX:ErrorString = 'Supplier does not match '&clip(LocalSupplier)
            Add(ExportFile)
        END

        if SupplierChecked = false then

            SupplierChecked = true
            !Unrecognised Supplier              IXX:Supplier
            Access:Supplier.clearkey(sup:Company_Name_Key)
            sup:Company_Name = IXX:Supplier
            if access:Supplier.fetch(sup:Company_Name_Key)
                EXX:LineNumber  = Count
                EXX:Supplier    = IXX:Supplier
                EXX:PartNumber  = IXX:PartNumber
                EXX:Quantity    = IXX:Quantity
                EXX:ErrorString = 'Unrecognised Supplier'
                Add(ExportFile)
            END

            !Supplier on existing requisition   IXX:Supplier
            access:ordpend.clearkey(ope:supplier_name_key)
            ope:supplier = IXX:Supplier
            set(ope:supplier_name_key,ope:supplier_name_key)
            loop
                if access:ordpend.next() then break.
                if ope:supplier <> IXX:Supplier then break.

                If ope:StockReqNumber = 0
                    EXX:LineNumber  = Count
                    EXX:Supplier    = IXX:Supplier
                    EXX:PartNumber  = IXX:PartNumber
                    EXX:Quantity    = IXX:Quantity
                    EXX:ErrorString = 'Supplier aleady on a requisition'
                    Add(ExportFile)
                    break
                End !If ope:StockReqNumber = 0
            end !loop

        END !if supplierchecked


        !duplicate Item?
        LocalStockQueue.LSQ:LocalStockPartNumber = IXX:PartNumber
        get(LocalStockQueue,LSQ:LocalStockPartNumber)
        if error() then
            !not found - add it
            LocalStockQueue.LSQ:LocalStockPartNumber = IXX:PartNumber
            add(LocalStockQueue)
        ELSE
            EXX:LineNumber  = Count
            EXX:Supplier    = IXX:Supplier
            EXX:PartNumber  = IXX:PartNumber
            EXX:Quantity    = IXX:Quantity
            EXX:ErrorString = 'Duplicate request for this part number'
            Add(ExportFile)
        END

        !Unknown item?
        access:Stock.clearkey(sto:Location_Key)      !includes the partnumber
        sto:Location = LocalMainStore
        sto:Part_Number = IXX:PartNumber
        if access:Stock.fetch(sto:Location_Key)
            EXX:LineNumber  = Count
            EXX:Supplier    = IXX:Supplier
            EXX:PartNumber  = IXX:PartNumber
            EXX:Quantity    = IXX:Quantity
            EXX:ErrorString = 'Unrecognised part number'
            Add(ExportFile)
        END

        !sundry, suspended, superceeded
        !message(clip(IXX:PartNumber)&' has sundry item as '&sto:Sundry_item)
        if sto:Sundry_Item = 'YES' then
            !message('Adding this to the export file')
            EXX:LineNumber  = Count
            EXX:Supplier    = IXX:Supplier
            EXX:PartNumber  = IXX:PartNumber
            EXX:Quantity    = IXX:Quantity
            EXX:ErrorString = 'This is a sundry item'
            Add(ExportFile)
        END

        if sto:Superceeded = 'YES' then
            EXX:LineNumber  = Count
            EXX:Supplier    = IXX:Supplier
            EXX:PartNumber  = IXX:PartNumber
            EXX:Quantity    = IXX:Quantity
            EXX:ErrorString = 'This part number has been superseeded'
            Add(ExportFile)
        END

        if sto:Suspend = true then
            EXX:LineNumber  = Count
            EXX:Supplier    = IXX:Supplier
            EXX:PartNumber  = IXX:PartNumber
            EXX:Quantity    = IXX:Quantity
            EXX:ErrorString = 'This part number is currently suspended'
            Add(ExportFile)
        END

    END !loop through import filename

    close(ImportFile)
    Close(ExportFile)

    miss# = missive('There were errors generated by this import file. You can find these in:|'&clip(FileName_2)&'|No orders were imported',|
                    'ServiceBase 3g','mstop.jpg','/OK')

    EXIT
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020777'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ImportStockFile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Access:STOCK.UseFile
  Access:SUPPLIER.UseFile
  Access:ORDPEND.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('CSV files','*.CSV')
  FileLookup9.AddMask('All Files','*.*')
  FileLookup9.WindowTitle='Import Filename'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
      !Prepare to import the file
      
      !find the main store - this will only work if there is exactly one
      Count = 0
      Access:LOCATION.ClearKey(loc:Main_Store_Key)
      loc:Main_Store = 'YES'
      set(loc:Main_Store_Key,loc:Main_Store_Key)
      loop until Access:LOCATION.Next()
          if loc:Main_Store <> 'YES' then break.
          Count += 1
          LocalMainStore = loc:Location
      end
      
      if count <> 1 then
          miss# = missive('This import will only work if there is a single main store. The data reports '&clip(Count)&' main stores',|
                          'ServiceBase 3g','mstop.jpg','/OK')
          Cycle
      END
      
      !first pass to see if there are any problems
      LocalError = false
      SupplierChecked = false
      LocalSupplier = ''
      free(LocalStockQueue)
      
      Filename_1 = LocalFilename
      Open(ImportFile)
      
      do FirstPass
      
      
      !end of first pass
      If LocalError then
          do ReportError
      ELSE
          do ImportTheData
          Post(Event:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020777'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020777'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020777'&'0')
      ***
    OF ?LookupFile
      ThisWindow.Update
      LocalFilename = FileLookup9.Ask(1)
      DISPLAY
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
KeyPendingOrders PROCEDURE                            !Generated from procedure template - Window

AppNotSelected       STRING(40)
AppSelected          STRING(40)
NotNotSelected       STRING(40)
NotSelected          STRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,6,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(165,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Key'),AT(169,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,86,352,244),USE(?Panel3),FILL(09A6A7CH)
                       GROUP('Line Tagging'),AT(192,102,292,86),USE(?Group2),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                       END
                       IMAGE('tick1.ico'),AT(423,122),USE(?Image1),CENTERED
                       STRING('Tagged Liine'),AT(228,130),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       IMAGE('notick1.ico'),AT(423,150),USE(?Image2),CENTERED
                       STRING('Untagged Line'),AT(228,156),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Line Colours'),AT(192,202,292,108),USE(?Group1),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                       END
                       ENTRY(@s40),AT(228,224,215,10),USE(AppNotSelected),CENTER,FONT(,,COLOR:Green,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Green,COLOR:White),READONLY
                       ENTRY(@s40),AT(228,242,215,10),USE(AppSelected),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Green,COLOR:White,COLOR:Green),READONLY
                       ENTRY(@s40),AT(228,262,215,10),USE(NotNotSelected),CENTER,FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Red,COLOR:White),READONLY
                       ENTRY(@s40),AT(228,280,215,10),USE(NotSelected),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Red,COLOR:White,COLOR:Red),READONLY
                       PANEL,AT(165,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?ButtonClose),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020791'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('KeyPendingOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  AppNotSelected = 'APPROVED STANDARD'
  AppSelected    = 'APPROVED WHEN SELECTED'
  NotNotSelected = 'NOT APPROVED STANDARD'
  NotSelected    = 'NOT APPROVED WHEN SELECTED'
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020791'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020791'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020791'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

CheckApproveOrdersPermitted PROCEDURE                 !Generated from procedure template - Window

ReturnByte           BYTE
LocalPassword        STRING(20)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,-2,28,24),USE(?ButtonHelp),SKIP,TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(245,148,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Enter Authorising Password'),AT(249,150),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(385,150),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,164,192,90),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Password'),AT(252,210),USE(?LocalPassword:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s20),AT(304,210,124,10),USE(LocalPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,PASSWORD
                       PANEL,AT(245,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(308,258),USE(?ButtonOK),FLAT,ICON('okp.jpg')
                       BUTTON,AT(372,258),USE(?ButtonCancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(ReturnByte)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020793'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CheckApproveOrdersPermitted')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ReturnByte = false
  PendingAuthorisingUser = ''
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:USERS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonCancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
      ReturnByte = false
      PendingAuthorisingUser = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020793'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020793'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020793'&'0')
      ***
    OF ?LocalPassword
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LocalPassword, Accepted)
      Access:users_Alias.clearkey(use_ali:password_key)
      use_ali:Password = LocalPassword
      if access:users_Alias.fetch(use_ali:password_key) then
          miss# = missive('Unknown Password','SeviceBase 3g','Mstop.jpg','/RETRY')
      ELSE
          Access:Accareas.clearkey(acc:Access_level_key)
          acc:Access_Area  = 'APPROVE OR REJECT ORDERS'
          acc:User_Level   = use_ali:User_Level
          if access:Accareas.fetch(acc:Access_level_key)
              Miss# = missive('Invalid Password','SeviceBase 3g','Mstop.jpg','/RETRY')
          ELSE
              PendingAuthorisingUser = clip(use_ali:Forename)&' '&clip(use_ali:Surname)
              ReturnByte = true
              POST(Event:CloseWindow)
          END
      END !if password.fetched ok
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LocalPassword, Accepted)
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      SELECT(?LocalPassword)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ViewPendAudTrail PROCEDURE (SentOrder)                !Generated from procedure template - Window

LocalOrderNO         LONG
BRW8::View:Browse    VIEW(PENDAUD)
                       PROJECT(PED:OrdOrder_Number)
                       PROJECT(PED:Action)
                       PROJECT(PED:ActionDate)
                       PROJECT(PED:ActionTime)
                       PROJECT(PED:UserName)
                       PROJECT(PED:AuthorisingUser)
                       PROJECT(PED:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
PED:OrdOrder_Number    LIKE(PED:OrdOrder_Number)      !List box control field - type derived from field
PED:Action             LIKE(PED:Action)               !List box control field - type derived from field
PED:ActionDate         LIKE(PED:ActionDate)           !List box control field - type derived from field
PED:ActionTime         LIKE(PED:ActionTime)           !List box control field - type derived from field
PED:UserName           LIKE(PED:UserName)             !List box control field - type derived from field
PED:AuthorisingUser    LIKE(PED:AuthorisingUser)      !List box control field - type derived from field
PED:RecordNumber       LIKE(PED:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,6,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(165,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Authorisation of Pending Order'),AT(169,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,86,352,246),USE(?Panel3),FILL(09A6A7CH)
                       LIST,AT(172,94,340,228),USE(?List),IMM,MSG('Browsing Records'),FORMAT('0R|M@n-14@54L(1)|M~Action~L(2)@s20@39L(1)|M~Date~L(2)@D01@23L(1)|M~Time~L(2)@t7@' &|
   '100L(2)|M~User Name~@s50@100L(2)|M~Authorising User~@s50@'),FROM(Queue:Browse)
                       PANEL,AT(165,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?ButtonClose),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020792'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ViewPendAudTrail')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:PENDAUD.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:PENDAUD,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  LocalOrderNO = SentOrder
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,PED:KeyOrderDateTimeDesc)
  BRW8.AddRange(PED:OrdOrder_Number,LocalOrderNO)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,PED:ActionDate,1,BRW8)
  BRW8.AddField(PED:OrdOrder_Number,BRW8.Q.PED:OrdOrder_Number)
  BRW8.AddField(PED:Action,BRW8.Q.PED:Action)
  BRW8.AddField(PED:ActionDate,BRW8.Q.PED:ActionDate)
  BRW8.AddField(PED:ActionTime,BRW8.Q.PED:ActionTime)
  BRW8.AddField(PED:UserName,BRW8.Q.PED:UserName)
  BRW8.AddField(PED:AuthorisingUser,BRW8.Q.PED:AuthorisingUser)
  BRW8.AddField(PED:RecordNumber,BRW8.Q.PED:RecordNumber)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PENDAUD.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020792'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020792'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020792'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

CheckDepatchNoteNumber PROCEDURE (SentDNN,SentDescription) !Generated from procedure template - Window

ReturnString         STRING(30)
despatch_note        STRING(30)
DescriptionString    STRING(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,-1,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(245,148,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Confirm Despatch Note Number'),AT(249,150),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(385,151),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(245,162,192,94),USE(?Panel3),FILL(09A6A7CH)
                       STRING(@s30),AT(267,180),USE(DescriptionString),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Despatch Note:'),AT(267,208),USE(?despatch_note:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s30),AT(267,224,148,10),USE(despatch_note),FONT(,,,FONT:bold,CHARSET:ANSI),REQ,UPR
                       PANEL,AT(245,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(304,258),USE(?ButtonCancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(368,258),USE(?ButtonOK),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(ReturnString)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020795'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CheckDepatchNoteNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  DescriptionString = SentDescription
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonCancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
      !just to be sure
      ReturnString = 'CANCEL'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    OF ?ButtonOK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
      if clip(despatch_note) = '' then cycle.
      
      if clip(despatch_note) = clip(SentDNN) then
          ReturnString = 'OK'
      ELSE
          If SecurityCheck('OVERRIDE INVOICE NUMBER CONF')
              !NOT GOT IT
              Case missive('This despatch note number does not match that already entered.|'&|
                           'Do you want to re-enter the number, or cancel this process',|
                           'ServiceBase 3g','mquest.jpg','/Re-Enter|\Cancel')
                  of 2    !Cancel
                      ReturnString = 'CANCEL'
                  of 1    !reenter
                      select(?despatch_note)
                      Cycle
              END !Case missive
      
          Else
              Case missive('This despatch note number does not match that already entered.|'&|
                           'Do you want to re-enter the number, or to override the existing number to match this new one?',|
                           'ServiceBase 3g','mquest.jpg','/Re-Enter|\Override|Cancel')
                  of 3    !Cancel
                      ReturnString = 'CANCEL'
                  of 2    !override
                      glo:ErrorText = despatch_note
                      ReturnString = 'OVERRIDE'
                  of 1    !reenter
                      select(?despatch_note)
                      Cycle
              END !Case missive
          End ! If SecurityCheck('OVERRIDE INVOICE NUMBER CONFIRMATION')
      
      
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020795'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020795'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020795'&'0')
      ***
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    OF ?ButtonOK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
