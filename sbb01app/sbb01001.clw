

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01001.INC'),ONCE        !Local module procedure declarations
                     END


Transfer_Stock PROCEDURE                              !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
current_site_location_temp STRING(30)
transfer_site_location_temp STRING(30)
quantity_temp        LONG
ReqNo                STRING(20)
tmp:Information      STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?transfer_site_location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Stock Transfer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Transfer'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Transfer'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Current Site Location'),AT(228,162),USE(?Prompt1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(324,162,124,10),USE(GLO:Select1),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Transfer Stock To'),AT(228,182),USE(?Prompt2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(324,182,124,10),USE(transfer_site_location_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity To Transfer'),AT(228,202),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<#p),AT(324,202,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),RANGE(1,999999)
                           PROMPT('Requisition Number'),AT(228,222),USE(?ReqNo:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s20),AT(324,222,124,10),USE(ReqNo),HIDE,FONT(,,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_sto_id   ushort,auto
save_stm_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020110'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Transfer_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  SET(Default2)
  Access:Default2.Next()
  !Get location!
  Access:Location.ClearKey(loc:Location_Key)
  loc:Location = GLO:Select1
  IF Access:Location.Fetch(loc:Location_Key)
    !ERROR
  ELSE
    IF loc:Main_Store = 'YES'
      IF de2:UseRequisitionNumber = 1
        UNHIDE(?ReqNo)
        UNHIDE(?ReqNo:Prompt)
        ?ReqNo{PROP:REQ} = TRUE
      ELSE
        ?ReqNo{PROP:REQ} = FALSE
      END
      DISPLAY()
      UPDATE()
    END
  END
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB1.Init(transfer_site_location_temp,?transfer_site_location_temp,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(loc:Location_Key)
  FDCB1.AddField(loc:Location,FDCB1.Q.loc:Location)
  FDCB1.AddField(loc:RecordNumber,FDCB1.Q.loc:RecordNumber)
  FDCB1.AddUpdateField(loc:Location,transfer_site_location_temp)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020110'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020110'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020110'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !Transfer
      finish# = 1
      If transfer_site_location_temp = ''
          Select(?transfer_site_location_temp)
          finish# = 0
      End
      If ?ReqNo{PROP:REQ} = TRUE AND ReqNo = ''
          Select(?ReqNo)
          finish# = 0
      End
      If finish# = 1
          If quantity_temp = ''
              Select(?quantity_temp)
              finish# = 0
          End
      End
      If finish# = 1
          If transfer_site_location_temp  = glo:select1
              Case Missive('You cannot transfer stock to the same location.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?transfer_site_location_temp)
              finish# = 0
          End
      End
      If finish# = 1
          access:stock_alias.clearkey(sto_ali:ref_number_key)
          sto_ali:ref_number = glo:select2
          If access:stock_alias.fetch(sto_ali:ref_number_key)
              Return Level:Fatal
          Else
              finish2# = 1
              If sto_ali:quantity_stock - quantity_temp < 0
                  Case Missive('You cannot exceed the quantity in stock.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Select(?quantity_temp)
                  finish2# = 0
      
              End
              If finish2# = 1
                  found# = 0
      
                  Save_sto_ID = Access:STOCK.SaveFile()
                  Access:STOCK.ClearKey(sto:Location_Key)
                  sto:Location    = Transfer_Site_Location_Temp
                  sto:Part_Number = sto_ali:Part_Number
                  Set(sto:Location_Key,sto:Location_Key)
                  Loop
                      If Access:STOCK.NEXT()
                         Break
                      End !If
                      If sto:Location    <> Transfer_Site_Location_Temp      |
                      Or sto:Part_Number <> sto_ali:Part_Number      |
                          Then Break.  ! End If
                      If sto:Ref_Number <> sto_Ali:Ref_Number
                          !Found a part in the location
                          Ref_Number$ = sto:Ref_Number
                          Found# = 1
                      End !If sto:Ref_Number <> sto_Ali:Ref_Number
      
                  End !Loop
                  Access:STOCK.RestoreFile(Save_sto_ID)
      
                  error# = 0
                  If found# = 0
                      Case Missive('Do you wish to create a NEW part in the new location?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              If access:stock.primerecord() = Level:Benign
                                  ref_number$                = sto:ref_number
                                  sto:record                :=: sto_ali:record
                                  sto:ref_number             = ref_number$
                                  sto:location               = transfer_site_location_temp
                                  sto:quantity_stock         = quantity_temp
                                  access:stock.insert()
                              End!If access:stock.primerecord() = Level:Benign
                              setcursor(cursor:wait)
      
                              save_stm_ali_id = access:stomodel_alias.savefile()
                              access:stomodel_alias.clearkey(stm_ali:model_number_key)
                              stm_ali:ref_number   = sto_ali:ref_number
                              set(stm_ali:model_number_key,stm_ali:model_number_key)
                              loop
                                  if access:stomodel_alias.next()
                                     break
                                  end !if
                                  if stm_ali:ref_number   <> sto_ali:ref_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  get(stomodel,0)
                                  if access:stomodel.primerecord() = Level:Benign
                                      stm:record      :=: stm_ali:record
                                      stm:ref_number   = sto:ref_number
                                      stm:location     = transfer_site_location_temp
                                      access:stomodel.insert()
                                  end!if access:stomodel.primerecord() = Level:Benign
      
                              end !loop
                              access:stomodel_alias.restorefile(save_stm_ali_id)
                              setcursor()
      
                              If ReqNo = ''
                                  tmp:Information = 'STOCK TRANSFERRED'
                              Else ! If ReqNo = ''
                                  tmp:Information = 'STOCK TRANSFERRED ON REQ. NO. ' & Clip(ReqNo)
                              End ! If ReqNo = ''
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                   'ADD', | ! Transaction_Type
                                                   '', | ! Depatch_Note_Number
                                                   0, | ! Job_Number
                                                   0, | ! Sales_Number
                                                   Quantity_Temp, | ! Quantity
                                                   sto:Purchase_Cost, | ! Purchase_Cost
                                                   sto:Sale_Cost, | ! Sale_Cost
                                                   sto:Retail_Cost, | ! Retail_Cost
                                                   tmp:Information, | ! Notes
                                                   'FROM LOCATION: ' & CLip(glo:Select1)) ! Information
                                  ! Added OK
                              Else ! AddToStockHistory
                                  ! Error
                              End ! AddToStockHistory
                          Of 1 ! No Button
                              error# = 1
                      End ! Case Missive
      
      
                  Else
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = ref_number$
                      if access:stock.fetch(sto:ref_number_key)
                          Return Level:Fatal
                      Else
                          sto:quantity_stock += quantity_temp
                          If access:Stock.update() = Level:Benign
                              If ReqNo = ''
                                  tmp:Information = 'STOCK TRANSFERRED'
                              Else ! If ReqNo = ''
                                  tmp:Information = 'STOCK TRANSFERRED ON REQ. NO. ' & Clip(ReqNo)
                              End ! If ReqNo = ''
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                   'ADD', | ! Transaction_Type
                                                   '', | ! Depatch_Note_Number
                                                   0, | ! Job_Number
                                                   0, | ! Sales_Number
                                                   Quantity_Temp, | ! Quantity
                                                   sto:Purchase_Cost, | ! Purchase_Cost
                                                   sto:Sale_Cost, | ! Sale_Cost
                                                   sto:Retail_Cost, | ! Retail_Cost
                                                   tmp:Information, | ! Notes
                                                   'FROM LOCATION: ' & CLip(glo:Select1)) ! Information
                                  ! Added OK
                              Else ! AddToStockHistory
                                  ! Error
                              End ! AddToStockHistory
      
                          End !If access:Stock.update() = Level:Benign
                      end !if
                  End
                  If error# = 0
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = glo:select2
                      If access:stock.fetch(sto:ref_number_key)
                          Return Level:Fatal
                      Else
                          sto:quantity_stock -= quantity_temp
                          If access:stock.update() = Level:Benign
                              If ReqNo = ''
                                  tmp:Information = 'STOCK TRANSFERRED'
                              Else ! If ReqNo = ''
                                  tmp:Information = 'STOCK TRANSFERRED ON REQ. NO. ' & Clip(ReqNo)
                              End ! If ReqNo = ''
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                   'DEC', | ! Transaction_Type
                                                   '', | ! Depatch_Note_Number
                                                   0, | ! Job_Number
                                                   0, | ! Sales_Number
                                                   Quantity_Temp, | ! Quantity
                                                   sto:Purchase_Cost, | ! Purchase_Cost
                                                   sto:Sale_Cost, | ! Sale_Cost
                                                   sto:Retail_Cost, | ! Retail_Cost
                                                   tmp:Information, | ! Notes
                                                   'FROM LOCATION: ' & CLip(Transfer_Site_Location_Temp)) ! Information
                                  ! Added OK
                              Else ! AddToStockHistory
                                  ! Error
                              End ! AddToStockHistory
                          End !If access:stock.update() = Level:Benign
                      end !if
                  End !If error# = 0
              End !If finish2# = 1
          end !if
          Post(event:closewindow)
      End !If finish# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      quantity_temp = 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1


FDCB1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(1, ValidateRecord, (),BYTE)
  !TB12448 second part hide franchises - JC - 12/07/12
  
      access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
      tra_ali:SiteLocation = loc:Location
      if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
          !eh?? - let it show
      ELSE
          if tra_ali:Stop_Account = 'YES' then return(record:filtered).
      END
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Add_Stock PROCEDURE (func:RefNumber)                  !Generated from procedure template - Window

save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
in_warranty_temp     REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
percentage_mark_up_temp REAL
percentage_mark_up_temp2 REAL
no_of_labels_temp    REAL
tmp:QuantityStock    LONG
out_warranty_temp    REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Window Title'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Add Stock'),USE(?AddStock),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity'),AT(236,98),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<<<#p),AT(316,98,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,RANGE(1,99999999)
                           PROMPT('Despatch Note'),AT(236,114),USE(?glo:select2:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(316,114,124,10),USE(despatch_note_temp),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Average Purch Cost'),AT(236,130),USE(?glo:select3:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(316,130,64,10),USE(purchase_cost_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Percentage Mark Up'),AT(236,146),USE(?percentage_mark_up_temp:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n7.2),AT(316,146,64,10),USE(percentage_mark_up_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('  %  '),AT(380,146),USE(?Prompt8),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('In Warranty'),AT(236,162),USE(?glo:select4:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(316,162,64,10),USE(in_warranty_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Percent Mark Up'),AT(236,178),USE(?percentage_mark_up_temp2:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n7.2),AT(316,178,64,10),USE(percentage_mark_up_temp2),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Out Warranty'),AT(236,194),USE(?retail_cost_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(316,194,64,10),USE(out_warranty_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Date Received'),AT(236,210),USE(?glo:select5:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(316,210,64,10),USE(date_received_temp),FONT('Tahoma',,,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(384,206),USE(?Button3),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Additional Notes'),AT(236,226),USE(?Prompt6),FONT(,,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(316,226,124,28),USE(additional_notes_temp),FONT(,,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Number Of Labels'),AT(236,258),USE(?no_of_labels_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@n6),AT(316,258,64,10),USE(no_of_labels_temp),DISABLE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,STEP(1)
                         END
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Add Stock'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020099'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Add_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:PARTS.Open
  Relate:STOCK_ALIAS.Open
  Relate:WEBJOB.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:WARPARTS.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:PARTS.Close
    Relate:STOCK_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020099'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020099'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020099'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      finish# = 1
      If date_received_temp = ''
          Select(?date_received_temp)
          finish# = 0
      End
      If finish# = 1
          If quantity_temp = ''
              Select(?quantity_temp)
              finish# = 0
          End
      End
      If finish# = 1
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = func:RefNumber
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              Pointer# = Pointer(STOCK)
              Hold(STOCK,1)
              Get(STOCK,Pointer#)
              If Errorcode() = 43
                  Case Missive('This stock item is currently in use by another station.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If Errorcode() = 43
                  sto:quantity_stock          += quantity_temp
                  If access:stock.tryupdate() = Level:Benign
                      Set(defaults)
                      access:Defaults.next()
                      Loop x# = 1 to no_of_labels_temp
                          glo:select1 = func:RefNumber
                          Case def:label_printer_type
                              Of 'TEC B-440 / B-442'
                                  Stock_Label(Today(),quantity_temp,'N/A',despatch_note_temp,out_warranty_temp)
                              Of 'TEC B-452'
                                  Stock_Label_B452(Today(),quantity_temp,'N/A',despatch_note_temp,out_warranty_temp)
                          End!Case def:label_printer_type
                      End!Loop x# = 1 to no_of_labels_temp
      
                      glo:select1 = ''
                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'ADD', | ! Transaction_Type
                                         Despatch_Note_Temp, | ! Depatch_Note_Number
                                         0, | ! Job_Number
                                         0, | ! Sales_Number
                                         Quantity_Temp, | ! Quantity
                                         sto:Purchase_Cost, | ! Purchase_Cost
                                         sto:Sale_Cost, | ! Sale_Cost
                                         sto:Retail_Cost, | ! Retail_Cost
                                         'STOCK ADDED', | ! Notes
                                         Clip(Additional_Notes_Temp)) ! Information
                        ! Added OK
                      Else ! AddToStockHistory
                        ! Error
                      End ! AddToStockHistory
      
                      !Check to see if any jobs, require these parts that have been added..(for some reason)
                      FoundPart# = 0
                      Save_par_ID = Access:PARTS.SaveFile()
                      Access:PARTS.ClearKey(par:PartAllocatedKey)
                      par:PartAllocated = 0
                      par:Part_Number   = sto:Part_Number
                      Set(par:PartAllocatedKey,par:PartAllocatedKey)
                      Loop
                          If Access:PARTS.NEXT()
                             Break
                          End !If
                          If par:PartAllocated <> 0      |
                          Or par:Part_Number   <> sto:Part_Number      |
                              Then Break.  ! End If
                          !Ok, found an unallocated part, but is it for this location?
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = par:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
                              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                              tra:Account_Number  = wob:HeadAccountNumber
                              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Found
                                  If tra:SiteLocation <> sto:Location
                                      Cycle
                                  End !If tra:SiteLocation <> sto:Location
                                  FoundPart# = 1
                                  Break
                              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      End !Loop
                      Access:PARTS.RestoreFile(Save_par_ID)
      
                      If FoundPart# = 0
                          Save_wpr_ID = Access:WARPARTS.SaveFile()
                          Access:WARPARTS.ClearKey(wpr:PartAllocatedKey)
                          wpr:PartAllocated = 0
                          wpr:Part_Number   = sto:Part_Number
                          Set(wpr:PartAllocatedKey,wpr:PartAllocatedKey)
                          Loop
                              If Access:WARPARTS.NEXT()
                                 Break
                              End !If
                              If wpr:PartAllocated <> 0      |
                              Or wpr:Part_Number   <> sto:Part_Number      |
                                  Then Break.  ! End If
                              !Ok, found an unallocated part, but is it for this location?
                              Access:WEBJOB.Clearkey(wob:RefNumberKey)
                              wob:RefNumber   = wpr:Ref_Number
                              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                  !Found
                                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                  tra:Account_Number  = wob:HeadAccountNumber
                                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                      !Found
                                      If tra:SiteLocation <> sto:Location
                                          Cycle
                                      End !If tra:SiteLocation <> sto:Location
                                      FoundPart# = 1
                                      Break
                                  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                      !Error
                                  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                  !Error
                              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                          End !Loop
                          Access:WARPARTS.RestoreFile(Save_wpr_ID)
                      End !If FoundPart# = 0
      
                      If FoundPart#
                          Case Missive('Note: There are jobs that require this part.'&|
                            '<13,10>'&|
                            '<13,10>These may now be allocated from Stock Allocation.','ServiceBase 3g',|
                                         'mexclam.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      End !If FoundPart#
                      Post(Event:CloseWindow)
      
                  Else !If access:stock.update() = Level:Benign
                      Case Missive('An error occured updating the stock record.'&|
                        '<13,10>'&|
                        '<13,10>Please try again.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End !If access:stock.update() = Level:Benign
      
              End !If Errorcode() = 43
          Else! If Access:.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = func:RefNumber
      If access:stock.fetch(sto:ref_number_key)
          Return Level:Fatal
      End
      quantity_temp = 1
      date_received_temp = Today()
      !purchase_cost_Temp = STO:Purchase_Cost
      !sale_cost_temp = STO:Sale_Cost
      !retail_cost_temp    = sto:Retail_cost
      !percentage_mark_up_temp = STO:Percentage_Mark_Up
      purchase_cost_Temp = sto:AveragePurchaseCost
      percentage_mark_up_temp = sto:PurchaseMarkUp
      in_warranty_temp = STO:Purchase_Cost
      percentage_mark_up_temp2 = STO:Percentage_Mark_Up
      out_warranty_temp = STO:Sale_Cost
      !Labels
      set(defaults)
      access:defaults.next()
      If def:receive_stock_label = 'YES'
          Enable(?no_of_labels_temp)
          no_of_labels_temp = 1
      Else!If def:receive_stock_label = 'YES'
          Disable(?no_of_labels_temp)
          no_of_labels_temp = 0
      End!If def:receive_stock_label = 'YES'
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Deplete_Stock PROCEDURE (func:RefNumber,func:Faulty)  !Generated from procedure template - Window

LocalRequest         LONG
tmp:QuantityStock    LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Decrement Stock Level'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Decrement Stock Level'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Decrement Stock Level'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity'),AT(240,150),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<<<#p),AT(316,150,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,RANGE(1,99999999)
                           PROMPT('Date'),AT(240,178),USE(?glo:select2:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(316,178,64,10),USE(date_received_temp),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(384,174),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Additional Notes'),AT(240,204),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(316,204,124,24),USE(additional_notes_temp),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020102'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Deplete_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STOCK.Open
  Relate:STOCK_ALIAS.Open
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?date_received_temp{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
    Relate:STOCK_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If date_received_temp = ''
          Select(?date_received_temp)
      Else
          if quantity_temp > sto:quantity_stock
              Case Missive('You cannot decrement the stock level below zero.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              quantity_temp = ''
              Select(?quantity_temp)
          Else
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number  = func:RefNumber
              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  !Found
      
                  Pointer# = Pointer(STOCK)
                  Hold(STOCK,1)
                  Get(STOCK,Pointer#)
                  If Errorcode() = 43
                      Case Missive('This stock item is currently in use by another station.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If Errorcode() = 43
      
                      sto:quantity_stock -= quantity_temp
      
                      if sto:quantity_stock < 0
                          sto:quantity_stock = 0
                      End
                      if (sto:Quantity_Stock = 0)
                          If (sto:Location <> MainStoreLocation())
                              If (MainStoreSuspended(sto:Part_Number))
                                  Beep(Beep:SystemExclamation)  ;  Yield()
                                  Case Missive('There are no more items in stock. '&|
                                      '|This part has been suspended.','ServiceBase',|
                                                 'mexclam.jpg','/&OK')
                                  Of 1 ! &OK Button
                                  End!Case Message
                                  sto:Suspend = 1
                              End
                          End
                      End
                      If access:stock.tryupdate() = Level:Benign
                          Case func:Faulty
                          Of 0
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'DEC', | ! Transaction_Type
                                                 '', | ! Depatch_Note_Number
                                                 0, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 Quantity_Temp, | ! Quantity
                                                 sto:Purchase_Cost, | ! Purchase_Cost
                                                 sto:Sale_Cost, | ! Sale_Cost
                                                 sto:Retail_Cost, | ! Retail_Cost
                                                 'STOCK DECREMENTED', | ! Notes
                                                 Clip(Additional_Notes_Temp)) ! Information
                                ! Added OK
                              Else ! AddToStockHistory
                                ! Error
                              End ! AddToStockHistory
                          Of 1
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'DEC', | ! Transaction_Type
                                                 '', | ! Depatch_Note_Number
                                                 0, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 Quantity_Temp, | ! Quantity
                                                 sto:Purchase_Cost, | ! Purchase_Cost
                                                 sto:Sale_Cost, | ! Sale_Cost
                                                 sto:Retail_Cost, | ! Retail_Cost
                                                 'RETURN OF FAULTY STOCK', | ! Notes
                                                 Clip(Additional_Notes_Temp)) ! Information
                                ! Added OK
                              Else ! AddToStockHistory
                                ! Error
                              End ! AddToStockHistory
                          End ! Case func:Faulty
      
                          if (sto:Suspend)
                              if (AddToStockHistory(sto:Ref_Number, |
                                                      'ADD',|
                                                      '',|
                                                      0, |
                                                      0, |
                                                      0, |
                                                      sto:Purchase_Cost,|
                                                      sto:Sale_Cost, |
                                                      sto:Retail_Cost, |
                                                      'PART SUSPENDED',|
                                                      ''))
                              end
                          end
      
                          If func:Faulty
                              FaultyStockReturnNote(sto:Ref_Number,Quantity_Temp)
                          End !If func:Type
      
                          Post(Event:CloseWindow)
                      Else !If access:stock.tryupdate() = Level:Benign
                          Case Missive('An error has occured updating the Stock Record. Please try again.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      End !If access:stock.tryupdate() = Level:Benign
                  End
              End! If Access:.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          End
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020102'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020102'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020102'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          date_received_temp = TINCALENDARStyle1(date_received_temp)
          Display(?date_received_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?date_received_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = func:RefNumber
      If access:stock.fetch(sto:ref_number_key)
          Return Level:Fatal
      end
      date_received_temp = Today()
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Stock_Order PROCEDURE                                 !Generated from procedure template - Window

LocalRequest         LONG
Average_Text_Temp    STRING(8)
save_shi_id          USHORT,AUTO
save_ori_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
tmp:WebOrderNumber   LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
supplier_temp        STRING(30)
Days_7_Temp          LONG
Days_30_Temp         LONG
Days_60_Temp         LONG
Days_90_Temp         LONG
Average_Temp         LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?supplier_temp
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Stock Order'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,212,220),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Order'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(168,98),USE(?sto:Part_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(244,98,124,10),USE(sto:Part_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(168,114),USE(?sto:Description:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(244,114,124,10),USE(sto:Description),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity'),AT(168,130),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<<<#p),AT(244,130,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,RANGE(1,99999999)
                           PROMPT('Supplier'),AT(168,146),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,146,124,10),USE(supplier_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Comments'),AT(168,162),USE(?Additional_Notes_Temp:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(244,162,124,28),USE(additional_notes_temp),HIDE,FONT(,,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       SHEET,AT(380,82,136,220),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Usage'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 7 Days'),AT(384,98),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,98),USE(Days_7_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(384,110),USE(?Prompt5:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,110),USE(Days_30_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(384,122),USE(?Prompt5:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,122),USE(Days_60_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(384,134),USE(?Prompt5:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,134),USE(Days_90_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(444,143,67,0),USE(?Line1),COLOR(COLOR:White)
                           PROMPT('Average Daily Use'),AT(384,150),USE(?Prompt5:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(472,150),USE(Average_Text_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(164,306,352,24),USE(?Panel2),FILL(09A6A7CH)
                       PROMPT('Current Price'),AT(172,314),USE(?sto:Sale_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n14.2),AT(248,314,64,10),USE(sto_ali:Sale_Cost),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Order'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ChangePartNumberIfExchange  Routine
    !Is this an exchange unit order? -  (DBH: 10-11-2003)
    If glo:Select2 <> '' And sto:Part_Number = 'EXCH'
        sto:Part_Number = 'EXCH ' & Clip(glo:Select2)
        sto:Description = Clip(glo:Select2) & ' EXCH UNIT'
    End !If glo:Select2 <> '' And sto:Part_Number = 'EXCH'
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020109'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Order')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sto:Part_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:ORDPEND.Open
  Relate:ORDWEBPR.Open
  Relate:STOCK_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB5.Init(supplier_temp,?supplier_temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(sup:Company_Name_Key)
  FDCB5.AddField(sup:Company_Name,FDCB5.Q.sup:Company_Name)
  FDCB5.AddField(sup:RecordNumber,FDCB5.Q.sup:RecordNumber)
  FDCB5.AddUpdateField(sup:Company_Name,supplier_temp)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:ORDPEND.Close
    Relate:ORDWEBPR.Close
    Relate:STOCK_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020109'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020109'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020109'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !Now for the new parts ordering
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number    = glo:select1
      
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
      
          If VirtualSite(sto:Location)
              if sto:Sundry_Item <> 'YES' ! Prevent exchange units being ordered this way
                  CreateWebOrder('S',Quantity_Temp)
              else
                  Case Missive('Cannot Order!'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              end
          Else !If glo:WebJob
              Do ChangePartNumberIfExchange
      
              Case def:SummaryOrders
                  Of 0 !Old Fashioned Orders
                      If access:ordpend.primerecord() = Level:Benign
                          ope:part_ref_number = sto:ref_number
                          ope:part_type       = 'STO'
                          ope:supplier        = supplier_temp
                          ope:part_number     = sto:part_number
                          ope:Description     = sto:description
                          ope:job_number      = ''
                          ope:quantity        = quantity_temp
                          ope:Reason          = Additional_Notes_Temp
                          access:ordpend.insert()
                      End !If access:ordpend.primerecord() = Level:Benign
      
                  Of 1 !New Orders
                      MakePartsRequest(Supplier_Temp,sto:Part_Number,sto:Description,Quantity_Temp)
      
                      Access:STOCK.Update()
      
              End!Case def:SummaryOrders
      
          !sto:quantity_to_order += quantity_temp
          !sto:pending_ref_number   = ope:ref_number
          !access:stock.update()
          !I don't understand why I was equating a sto:pending_ref_number field....the same part can be
          !ordered many times. Why? Why? Why? Brain broken again no doubt.
          End !If glo:WebJob
      
      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number    = glo:select1
      If access:stock.fetch(sto:ref_number_key)
          Return(Level:Fatal)
      End!If access:stock.fetch(sto:ref_number_key)
      
      StockUsage(sto:Ref_Number,Days_7_Temp,Days_30_Temp,Days_60_Temp,Days_90_Temp,Average_Temp)
      
      If average_temp < 1
          average_text_temp = '< 1'
      Else!If average_temp < 1
          average_text_temp = Int(average_temp)
      End!If average_temp < 1
      
      quantity_temp           = 1
      date_received_temp      = Today()
      purchase_cost_temp      = sto:purchase_cost
      sale_cost_temp          = sto:sale_cost
      supplier_temp           = sto:supplier
      Display()
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      Case def:SummaryOrders
          Of 0
              ?Additional_Notes_Temp{prop:Hide} = 0
              ?Additional_Notes_Temp:Prompt{prop:Hide} = 0
          Of 1
              ?Additional_Notes_Temp{prop:Hide} = 1
              ?Additional_Notes_Temp:Prompt{prop:Hide} = 1
      End !def:SummaryOrders
      
      IF Glo:WebJob
        Access:Stock_Alias.ClearKey(sto_ali:Location_Key)
        sto_ali:Location = MainStoreLocation()
        sto_ali:Part_Number = sto:Part_Number
        IF Access:Stock_Alias.Fetch(sto_ali:Location_Key)
          !Error!
        ELSE
          ?sto_ali:Sale_Cost{Prop:Hide} = 0
          ?sto:Sale_Cost:Prompt{Prop:Hide} = 0
        END
      ELSE
        ?sto_ali:Sale_Cost{Prop:Hide} = 1
        ?sto:Sale_Cost:Prompt{Prop:Hide} = 1
      END
      
      Do ChangePartNumberIfExchange
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Stock_Fault_Codes PROCEDURE                           !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
Window               WINDOW('Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,310),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Fault Codes'),USE(?Tab1)
                           PROMPT('Model Number'),AT(196,86),USE(?stm:Model_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(280,86,124,10),USE(stm:Model_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Fault Code 1:'),AT(196,115),USE(?stm:FaultCode1:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(280,115,124,10),USE(stm:FaultCode1),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 1')
                           ENTRY(@d6b),AT(280,115,64,10),USE(stm:FaultCode1,,?stm:FaultCode1:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 1')
                           BUTTON,AT(408,110),USE(?Lookup),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(420,110),USE(?PopCalendar),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 2:'),AT(196,134),USE(?stm:FaultCode2:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(280,134,124,10),USE(stm:FaultCode2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 2')
                           ENTRY(@d6b),AT(280,134,64,10),USE(stm:FaultCode2,,?stm:FaultCode2:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 2')
                           BUTTON,AT(408,130),USE(?Lookup:2),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(420,130),USE(?PopCalendar:2),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 3:'),AT(196,150),USE(?stm:FaultCode3:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(280,150,124,10),USE(stm:FaultCode3),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 3')
                           ENTRY(@d6b),AT(280,150,64,10),USE(stm:FaultCode3,,?stm:FaultCode3:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 3')
                           BUTTON,AT(408,147),USE(?Lookup:3),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(420,147),USE(?PopCalendar:3),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 4:'),AT(196,170),USE(?stm:FaultCode4:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(280,170,124,10),USE(stm:FaultCode4),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 4')
                           ENTRY(@d6b),AT(280,170,64,10),USE(stm:FaultCode4,,?stm:FaultCode4:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 4')
                           BUTTON,AT(420,166),USE(?PopCalendar:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,166),USE(?Lookup:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 5:'),AT(196,187),USE(?stm:FaultCode5:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(280,187,124,10),USE(stm:FaultCode5),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 5')
                           ENTRY(@d6b),AT(280,187,64,10),USE(stm:FaultCode5,,?stm:FaultCode5:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 5')
                           BUTTON,AT(420,182),USE(?PopCalendar:5),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,182),USE(?Lookup:5),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 6:'),AT(196,206),USE(?stm:FaultCode6:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(280,206,124,10),USE(stm:FaultCode6),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 6')
                           ENTRY(@d6b),AT(280,206,64,10),USE(stm:FaultCode6,,?stm:FaultCode6:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 6')
                           BUTTON,AT(420,202),USE(?PopCalendar:6),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,202),USE(?Lookup:6),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 7:'),AT(196,222),USE(?stm:FaultCode7:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S255),AT(280,222,124,10),USE(stm:FaultCode7),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 7')
                           ENTRY(@d6b),AT(280,222,64,10),USE(stm:FaultCode7,,?stm:FaultCode7:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 7')
                           BUTTON,AT(420,219),USE(?PopCalendar:7),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,219),USE(?Lookup:7),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 8:'),AT(196,242),USE(?stm:FaultCode8:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S255),AT(280,242,124,10),USE(stm:FaultCode8),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 8')
                           ENTRY(@d6b),AT(280,242,64,10),USE(stm:FaultCode8,,?stm:FaultCode8:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 8')
                           BUTTON,AT(420,238),USE(?PopCalendar:8),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,238),USE(?Lookup:8),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 9:'),AT(196,259),USE(?stm:FaultCode9:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S255),AT(280,259,124,10),USE(stm:FaultCode9),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 9')
                           ENTRY(@d6b),AT(280,259,64,10),USE(stm:FaultCode9,,?stm:FaultCode9:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 9')
                           BUTTON,AT(420,254),USE(?PopCalendar:9),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,254),USE(?Lookup:9),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 10:'),AT(196,278),USE(?stm:FaultCode10:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S255),AT(280,278,124,10),USE(stm:FaultCode10),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 10')
                           ENTRY(@d6b),AT(280,278,64,10),USE(stm:FaultCode10,,?stm:FaultCode10:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 10')
                           BUTTON,AT(420,274),USE(?PopCalendar:10),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,274),USE(?Lookup:10),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 11:'),AT(196,294),USE(?stm:FaultCode11:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S255),AT(280,294,124,10),USE(stm:FaultCode11),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Fault Code 11')
                           ENTRY(@d6b),AT(280,294,64,10),USE(stm:FaultCode11,,?stm:FaultCode11:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 11')
                           BUTTON,AT(420,291),USE(?PopCalendar:11),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,291),USE(?Lookup:11),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 12:'),AT(196,314),USE(?stm:FaultCode12:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S255),AT(280,314,124,10),USE(stm:FaultCode12),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 12')
                           ENTRY(@d6b),AT(280,314,64,10),USE(stm:FaultCode12,,?stm:FaultCode12:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fault Code 12')
                           BUTTON,AT(420,310),USE(?PopCalendar:12),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(408,310),USE(?Lookup:12),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                         END
                       END
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Item Fault Codes'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(484,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_map_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Fault_Coding        Routine   ! Manufacturers Fault Codes

    Hide(?stm:Faultcode1)
    Hide(?stm:Faultcode1:2)
    Hide(?stm:Faultcode2)
    Hide(?stm:Faultcode2:2)
    Hide(?stm:Faultcode3)
    Hide(?stm:Faultcode3:2)
    Hide(?stm:Faultcode4)
    Hide(?stm:Faultcode4:2)
    Hide(?stm:Faultcode5)
    Hide(?stm:Faultcode5:2)
    Hide(?stm:Faultcode6)
    Hide(?stm:Faultcode6:2)
    Hide(?stm:Faultcode7)
    Hide(?stm:Faultcode7:2)
    Hide(?stm:Faultcode8)
    Hide(?stm:Faultcode8:2)
    Hide(?stm:Faultcode9)
    Hide(?stm:Faultcode9:2)
    Hide(?stm:Faultcode10)
    Hide(?stm:Faultcode10:2)
    Hide(?stm:Faultcode11)
    Hide(?stm:Faultcode11:2)
    Hide(?stm:Faultcode12)
    Hide(?stm:Faultcode12:2)
    Hide(?stm:Faultcode1:prompt)
    Hide(?stm:Faultcode2:prompt)
    Hide(?stm:Faultcode3:prompt)
    Hide(?stm:Faultcode4:prompt)
    Hide(?stm:Faultcode5:prompt)
    Hide(?stm:Faultcode6:prompt)
    Hide(?stm:Faultcode7:prompt)
    Hide(?stm:Faultcode8:prompt)
    Hide(?stm:Faultcode9:prompt)
    Hide(?stm:Faultcode10:prompt)
    Hide(?stm:Faultcode11:prompt)
    Hide(?stm:Faultcode12:prompt)
    Hide(?popcalendar)
    Hide(?popcalendar:2)
    Hide(?popcalendar:3)
    Hide(?popcalendar:4)
    Hide(?popcalendar:5)
    Hide(?popcalendar:6)
    Hide(?popcalendar:7)
    Hide(?popcalendar:8)
    Hide(?popcalendar:9)
    Hide(?popcalendar:10)
    Hide(?popcalendar:11)
    Hide(?popcalendar:12)
    Hide(?lookup)
    Hide(?lookup:2)
    Hide(?lookup:3)
    Hide(?lookup:4)
    Hide(?lookup:5)
    Hide(?lookup:6)
    Hide(?lookup:7)
    Hide(?lookup:8)
    Hide(?lookup:9)
    Hide(?lookup:10)
    Hide(?lookup:11)
    Hide(?lookup:12)

    required# = 0

    setcursor(cursor:wait)

    save_map_id = access:manfaupa.savefile()
    access:manfaupa.clearkey(map:field_number_key)
    map:manufacturer = sto:manufacturer
    set(map:field_number_key,map:field_number_key)
    loop
        if access:manfaupa.next()
           break
        end !if
        if map:manufacturer <> sto:manufacturer      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Case map:field_number
            Of 1
                Unhide(?stm:Faultcode1:prompt)
                ?stm:Faultcode1:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar)
                    Unhide(?stm:Faultcode1:2)
                Else
                    Unhide(?stm:Faultcode1)
                    If map:lookup = 'YES'
                        Unhide(?lookup)
                    End
                End
            Of 2
                Unhide(?stm:Faultcode2:prompt)
                ?stm:Faultcode2:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:2)
                    Unhide(?stm:Faultcode2:2)
                Else
                    Unhide(?stm:Faultcode2)
                    If map:lookup = 'YES'
                        Unhide(?lookup:2)
                    End
                End

            Of 3
                Unhide(?stm:Faultcode3:prompt)
                ?stm:Faultcode3:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:3)
                    Unhide(?stm:Faultcode3:2)
                Else
                    Unhide(?stm:Faultcode3)
                    If map:lookup = 'YES'
                        Unhide(?lookup:3)
                    End
                End

            Of 4
                Unhide(?stm:Faultcode4:prompt)
                ?stm:Faultcode4:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:4)
                    Unhide(?stm:Faultcode4:2)
                Else
                    Unhide(?stm:Faultcode4)
                    If map:lookup = 'YES'
                        Unhide(?lookup:4)
                    End
                End

            Of 5
                Unhide(?stm:Faultcode5:prompt)
                ?stm:Faultcode5:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:5)
                    Unhide(?stm:Faultcode5:2)
                Else
                    Unhide(?stm:Faultcode5)
                    If map:lookup = 'YES'
                        Unhide(?lookup:5)
                    End
                End

            Of 6
                Unhide(?stm:Faultcode6:prompt)
                ?stm:Faultcode6:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:6)
                    Unhide(?stm:Faultcode6:2)
                Else
                    Unhide(?stm:Faultcode6)
                    If map:lookup = 'YES'
                        Unhide(?lookup:6)
                    End
                End
            Of 7
                Unhide(?stm:Faultcode7:prompt)
                ?stm:Faultcode7:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:7)
                    Unhide(?stm:Faultcode7:2)
                Else
                    Unhide(?stm:Faultcode7)
                    If map:lookup = 'YES'
                        Unhide(?lookup:7)
                    End
                End

            Of 8
                Unhide(?stm:Faultcode8:prompt)
                ?stm:Faultcode8:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:8)
                    Unhide(?stm:Faultcode8:2)
                Else
                    Unhide(?stm:Faultcode8)
                    If map:lookup = 'YES'
                        Unhide(?lookup:8)
                    End
                End

            Of 9
                Unhide(?stm:Faultcode9:prompt)
                ?stm:Faultcode9:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:9)
                    Unhide(?stm:Faultcode9:2)
                Else
                    Unhide(?stm:Faultcode9)
                    If map:lookup = 'YES'
                        Unhide(?lookup:9)
                    End
                End

            Of 10
                Unhide(?stm:Faultcode10:prompt)
                ?stm:Faultcode10:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:10)
                    Unhide(?stm:Faultcode10:2)
                Else
                    Unhide(?stm:Faultcode10)
                    If map:lookup = 'YES'
                        Unhide(?lookup:10)
                    End
                End

            Of 11
                Unhide(?stm:Faultcode11:prompt)
                ?stm:Faultcode11:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:11)
                    Unhide(?stm:Faultcode11:2)
                Else
                    Unhide(?stm:Faultcode11)
                    If map:lookup = 'YES'
                        Unhide(?lookup:11)
                    End
                End

            Of 12
                Unhide(?stm:Faultcode12:prompt)
                ?stm:Faultcode12:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:12)
                    Unhide(?stm:Faultcode12:2)
                Else
                    Unhide(?stm:Faultcode12)
                    If map:lookup = 'YES'
                        Unhide(?lookup:12)
                    End
                End

        End
    end !loop
    access:manfaupa.restorefile(save_map_id)
    setcursor()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020108'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Fault_Codes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?stm:Model_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:STOMODEL)
  Relate:CHARTYPE.Open
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?stm:FaultCode1{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode2{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode3{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode4{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode5{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode6{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode7{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode8{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode9{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode10{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode11{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode12{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?stm:FaultCode1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode1, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 1
      if access:manfaupa.fetch(map:field_number_key)
          If map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 1
              mfp:field        = stm:Faultcode1
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode1 = mfp:field
                  else
                      stm:Faultcode1 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode1)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          End!If map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode1, Accepted)
    OF ?Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 1
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 1
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode1 = mfo:field
          Else
              stm:FaultCode1 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode1 = mfp:field
          Else
              stm:FaultCode1 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode1 = TINCALENDARStyle1(stm:FaultCode1)
          Display(?stm:FaultCode1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?stm:FaultCode2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode2, Accepted)
      !access:manfaupa.clearkey(map:field_number_key)
      !map:manufacturer = sto:manufacturer
      !map:field_number = 2
      !if access:manfaupa.fetch(map:field_number_key)
      !    if map:force_lookup = 'YES'
      !        access:manfpalo.clearkey(mfp:field_key)
      !        mfp:manufacturer = sto:manufacturer
      !        mfp:field_number = 2
      !        mfp:field        = stm:Faultcode2
      !        if access:manfpalo.fetch(mfp:field_key)
      !            saverequest#      = globalrequest
      !            globalresponse    = requestcancelled
      !            globalrequest     = selectrecord
      !            browse_manufacturer_part_lookup
      !            if globalresponse = requestcompleted
      !                stm:Faultcode2 = mfp:field
      !            else
      !                stm:Faultcode2 = ''
      !                select(?-1)
      !            end
      !            display(?stm:Faultcode2)
      !            globalrequest     = saverequest#
      !        end!if access:manfpalo.fetch(mfp:field_key)
      !    end!if map:force_lookup = 'YES'
      !end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode2, Accepted)
    OF ?Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 2
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 2
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode2 = mfo:field
          Else
              stm:FaultCode2 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode2 = mfp:field
          Else
              stm:FaultCode2 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode2 = TINCALENDARStyle1(stm:FaultCode2)
          Display(?stm:FaultCode2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?stm:FaultCode3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode3, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 3
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 3
              mfp:field        = stm:Faultcode3
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode3 = mfp:field
                  else
                      stm:Faultcode3 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode3)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode3, Accepted)
    OF ?Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 3
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 3
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode3 = mfo:field
          Else
              stm:FaultCode3 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode3 = mfp:field
          Else
              stm:FaultCode3 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode3 = TINCALENDARStyle1(stm:FaultCode3)
          Display(?stm:FaultCode3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?stm:FaultCode4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode4, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 4
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 4
              mfp:field        = stm:Faultcode4
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode4 = mfp:field
                  else
                      stm:Faultcode4 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode4)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode4 = TINCALENDARStyle1(stm:FaultCode4)
          Display(?stm:FaultCode4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 4
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 4
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode4 = mfo:field
          Else
              stm:FaultCode4 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode4 = mfp:field
          Else
              stm:FaultCode4 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
    OF ?stm:FaultCode5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode5, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 5
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 5
              mfp:field        = stm:Faultcode5
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode5 = mfp:field
                  else
                      stm:Faultcode5 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode5)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode5 = TINCALENDARStyle1(stm:FaultCode5)
          Display(?stm:FaultCode5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 5
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 5
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode5 = mfo:field
          Else
              stm:FaultCode5 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode5 = mfp:field
          Else
              stm:FaultCode5 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
    OF ?stm:FaultCode6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode6, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 6
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 6
              mfp:field        = stm:Faultcode6
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode6 = mfp:field
                  else
                      stm:Faultcode6 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode6)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode6 = TINCALENDARStyle1(stm:FaultCode6)
          Display(?stm:FaultCode6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 6
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 6
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode6 = mfo:field
          Else
              stm:FaultCode6 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode6 = mfp:field
          Else
              stm:FaultCode6 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
    OF ?stm:FaultCode7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode7, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 7
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 7
              mfp:field        = stm:Faultcode7
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode7 = mfp:field
                  else
                      stm:Faultcode7 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode7)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode7 = TINCALENDARStyle1(stm:FaultCode7)
          Display(?stm:FaultCode7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 7
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 7
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode7 = mfo:field
          Else
              stm:FaultCode7 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode7 = mfp:field
          Else
              stm:FaultCode7 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
    OF ?stm:FaultCode8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode8, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 8
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 8
              mfp:field        = stm:Faultcode8
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode8 = mfp:field
                  else
                      stm:Faultcode8 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode8)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode8 = TINCALENDARStyle1(stm:FaultCode8)
          Display(?stm:FaultCode8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 8
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 8
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode8 = mfo:field
          Else
              stm:FaultCode8 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode8 = mfp:field
          Else
              stm:FaultCode8 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
    OF ?stm:FaultCode9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode9, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 9
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 9
              mfp:field        = stm:Faultcode9
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode9 = mfp:field
                  else
                      stm:Faultcode9 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode9)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode9 = TINCALENDARStyle1(stm:FaultCode9)
          Display(?stm:FaultCode9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 9
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 9
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode9 = mfo:field
          Else
              stm:FaultCode9 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode9 = mfp:field
          Else
              stm:FaultCode9 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
    OF ?stm:FaultCode10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode10, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 10
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 10
              mfp:field        = stm:Faultcode10
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode10 = mfp:field
                  else
                      stm:Faultcode10 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode10)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode10 = TINCALENDARStyle1(stm:FaultCode10)
          Display(?stm:FaultCode10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 10
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 10
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode10 = mfo:field
          Else
              stm:FaultCode10 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode10 = mfp:field
          Else
              stm:FaultCode10 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
    OF ?stm:FaultCode11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode11, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 11
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 11
              mfp:field        = stm:Faultcode11
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode11 = mfp:field
                  else
                      stm:Faultcode11 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode11)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode11 = TINCALENDARStyle1(stm:FaultCode11)
          Display(?stm:FaultCode11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 11
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 11
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode11 = mfo:field
          Else
              stm:FaultCode11 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode11 = mfp:field
          Else
              stm:FaultCode11 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
    OF ?stm:FaultCode12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode12, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 12
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 12
              mfp:field        = stm:Faultcode12
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode12 = mfp:field
                  else
                      stm:Faultcode12 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode12)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode12 = TINCALENDARStyle1(stm:FaultCode12)
          Display(?stm:FaultCode12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 12
      glo:select3  = ''
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = STO:manufacturer
      map:Field_Number = 12
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = STO:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode12 = mfo:field
          Else
              stm:FaultCode12 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              stm:FaultCode12 = mfp:field
          Else
              stm:FaultCode12 = ''
          end
        END
      END
      DISPLAY()
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020108'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020108'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020108'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?stm:FaultCode1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?stm:FaultCode2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?stm:FaultCode3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?stm:FaultCode4
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?stm:FaultCode5
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?stm:FaultCode6
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?stm:FaultCode7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?stm:FaultCode8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?stm:FaultCode9
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?stm:FaultCode10
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?stm:FaultCode11
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?stm:FaultCode12
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Fault_Coding
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Shelf_Location PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
save:ShelfLocation   STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::los:Record  LIKE(los:RECORD),STATIC
QuickWindow          WINDOW('Update the LOCSHELF File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       SHEET,AT(244,212,192,42),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Site Location'),AT(248,220),USE(?LOS:Site_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,220,124,10),USE(los:Site_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(248,236),USE(?LOS:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,236,124,10),USE(los:Shelf_Location),FONT('Tahoma',8,,FONT:bold),COLOR(080FFFFH),REQ,UPR
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Shelf Locations'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN,HIDE
                       END
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(304,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020097'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Shelf_Location')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(los:Record,History::los:Record)
  SELF.AddHistoryField(?los:Site_Location,1)
  SELF.AddHistoryField(?los:Shelf_Location,2)
  SELF.AddUpdateFile(Access:LOCSHELF)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCSHELF.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOCSHELF
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If los:Shelf_Location = 'UNALLOCATED'
      ?OK{prop:Disable} = 1
  End !los:Shelf_Location = 'UNALLOCATED'
  If ThisWindow.Request <> InsertRecord
      save:ShelfLocation  = los:Shelf_Location
  End !ThisWindow.Request <> InsertRecord
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCSHELF.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020097'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020097'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020097'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If ThisWindow.Request <> InsertRecord
      If save:ShelfLocation <> los:Shelf_Location
          Case Missive('All parts in location ' & Clip(save:ShelfLocation) & ' will not be changed to location ' & Clip(los:Shelf_Location) & '.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  los:Shelf_Location = save:ShelfLocation
                  Display()
                  Cycle
          End ! Case Missive
      End !If save:ShelfLocation <> los:Shelf_Location
  End !ThisWindow.Request <> InsertRecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Shelf_Location PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Shelf Location File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse:1)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Shelf Location File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Shelf Location'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(los:Shelf_Location),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,186),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,218),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(448,247),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020096'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Shelf_Location')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCSHELF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,los:Shelf_Location_Key)
  BRW1.AddRange(los:Site_Location)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?los:Shelf_Location,los:Shelf_Location,1,BRW1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(los:Shelf_Location,BRW1.Q.los:Shelf_Location)
  BRW1.AddField(los:Site_Location,BRW1.Q.los:Site_Location)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Shelf_Location
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020096'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020096'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020096'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateLocations PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
window               WINDOW('Update Stock Locations'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Updating Stock Location'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,244),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Part Number'),AT(228,164),USE(?sto:Part_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,164,124,10),USE(sto:Part_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(228,186),USE(?sto:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,186,124,10),USE(sto:Description),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(228,212),USE(?sto:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,212,124,10),USE(sto:Shelf_Location),FONT('Tahoma',8,,FONT:bold),COLOR(080FFFFH),REQ,UPR
                           BUTTON,AT(432,208),USE(?LookupShelfLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('2nd Location'),AT(228,234),USE(?sto:Second_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,234,124,10),USE(sto:Second_Location),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:sto:Shelf_Location                Like(sto:Shelf_Location)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020076'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateLocations')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCSHELF.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  tmp:ShelfLocation = sto:Shelf_Location
  tmp:SecondLocation = sto:Second_Location
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?sto:Shelf_Location{Prop:Tip} AND ~?LookupShelfLocation{Prop:Tip}
     ?LookupShelfLocation{Prop:Tip} = 'Select ' & ?sto:Shelf_Location{Prop:Tip}
  END
  IF ?sto:Shelf_Location{Prop:Msg} AND ~?LookupShelfLocation{Prop:Msg}
     ?LookupShelfLocation{Prop:Msg} = 'Select ' & ?sto:Shelf_Location{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCSHELF.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Shelf_Location
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020076'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020076'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020076'&'0')
      ***
    OF ?sto:Shelf_Location
      IF sto:Shelf_Location OR ?sto:Shelf_Location{Prop:Req}
        los:Site_Location = sto:Location
        los:Shelf_Location = sto:Shelf_Location
        !Save Lookup Field Incase Of error
        look:sto:Shelf_Location        = sto:Shelf_Location
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sto:Location = los:Site_Location
            sto:Shelf_Location = los:Shelf_Location
          ELSE
            !Restore Lookup On Error
            sto:Shelf_Location = look:sto:Shelf_Location
            SELECT(?sto:Shelf_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupShelfLocation
      ThisWindow.Update
      los:Site_Location = sto:Location
      los:Shelf_Location = sto:Shelf_Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sto:Location = los:Site_Location
          sto:Shelf_Location = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?sto:Shelf_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sto:Shelf_Location)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Access:STOCK.TryUpdate()
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      sto:Shelf_Location  = tmp:ShelfLocation
      sto:Second_Location = tmp:SecondLocation
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateSTOCK PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
RelocatedPurchCost   DECIMAL(7,2)
RelocatedInWarranty  DECIMAL(7,2)
RelocatedOutWarranty DECIMAL(7,2)
save_location_id     USHORT,AUTO
save_commoncp_id     USHORT,AUTO
save_commonwp_id     USHORT,AUTO
Model_Changed        BYTE
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
save_stock_id        USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
average_temp         LONG
save_shi_id          USHORT,AUTO
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
Average_text_temp    STRING(8)
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
pos                  STRING(255)
sav2:group           GROUP,PRE(sav2)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
Retail_Cost          REAL
Percentage_Mark_Up   REAL
Shelf_Location       STRING(30)
Manufacturer         STRING(30)
Second_Location      STRING(30)
Minimum_Level        REAL
Reorder_Level        REAL
Accessory            STRING('''NO''')
ExchangeUnit         STRING('''NO''')
Suspend              BYTE(0)
                     END
sav:group            GROUP,PRE(sav)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
Retail_Cost          REAL
Percentage_Mark_Up   REAL
Shelf_Location       STRING(30)
Manufacturer         STRING(30)
Second_Location      STRING(30)
Minimum_Level        REAL
Reorder_Level        REAL
Accessory            STRING('''NO''')
ExchangeUnit         STRING('''NO''')
Suspend              BYTE(0)
                     END
ref_number_temp      LONG
Part_Number_temp     STRING(30)
save_ope_id          USHORT,AUTO
Description_temp     STRING(30)
Manufacturer_temp    STRING(30)
Accessory_temp       STRING('''NO''')
save_orp_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
Replicate_Temp       STRING('YES')
OtherLocationQueue   QUEUE,PRE(othque)
Location             STRING(30)
QuantityInStock      LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CurrentRecord    LONG
tmp:SaveState        LONG
tmp:OldPartNumber    STRING(30)
tmp:OldDescription   STRING(30)
SaveRecord           GROUP,PRE(save)
RecordNumber         LONG
Location             STRING(30)
ShelfLocation        STRING(30)
SecondLocation       STRING(30)
QuantityStock        LONG
AveragePurchaseCost  REAL
PurchaseMarkUp       REAL
PurchaseCost         REAL
PercentageMarkUp     REAL
SaleCost             REAL
                     END
tmp:AllManufacturers BYTE(0)
Manufacturer_Q       QUEUE,PRE()
Manufacturer         STRING(30)
                     END
BRW8::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:RecordNumber)
                       PROJECT(stm:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
stm:Model_Number       LIKE(stm:Model_Number)         !List box control field - type derived from field
stm:RecordNumber       LIKE(stm:RecordNumber)         !Primary key field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::sto:Record  LIKE(sto:RECORD),STATIC
QuickWindow          WINDOW('Update Stock'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Insert / Amend Stock Item'),AT(7,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Manufacturer'),AT(512,60),USE(?sto:Manufacturer:Prompt),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       SHEET,AT(4,30,304,276),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General Details'),USE(?Tab3)
                           GROUP('Part Details'),AT(8,35,296,41),USE(?PartDetailsGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             BUTTON,AT(236,44),USE(?Button:ChangePartDetails),TRN,FLAT,ICON('chpartp.jpg')
                           END
                           PROMPT('Part Number'),AT(12,46),USE(?STO:Part_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,46,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Description'),AT(12,59),USE(?STO:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,59,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           GROUP('Location Details'),AT(8,80,296,64),USE(?LocationDetailsGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           END
                           PROMPT('Location'),AT(12,91),USE(?sto:Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,91,124,10),USE(sto:Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Shelf Location'),AT(12,104),USE(?STO:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,104,124,10),USE(sto:Shelf_Location),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(236,99),USE(?Lookup_Shelf_Location),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('2nd Location'),AT(12,115),USE(?STO:Second_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,115,124,10),USE(sto:Second_Location),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Supplier'),AT(12,128,,10),USE(?STO:Supplier:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,128,124,10),USE(sto:Supplier),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(236,123),USE(?Lookup_Supplier),TRN,FLAT,ICON('lookupp.jpg')
                           SHEET,AT(16,160,256,84),USE(?CostSheet),RIGHT,SPREAD
                             TAB('Main'),USE(?MainStoreTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               PROMPT('Purchase Cost'),AT(20,164),USE(?STO:Purchase_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n14.2),AT(116,164,64,10),USE(sto:Purchase_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                               PROMPT('Percentage Markup'),AT(20,180),USE(?STO:Purchase_Cost:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n6.2),AT(116,180,64,10),USE(sto:Percentage_Mark_Up),RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                               PROMPT('Selling Price'),AT(20,192),USE(?STO:Sale_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n14.2),AT(116,192,64,10),USE(sto:Sale_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                               PROMPT('Percentage Markup'),AT(20,208),USE(?STO:Purchase_Cost:Prompt:3),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n6.2),AT(116,208,64,10),USE(sto:RetailMarkup),HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Percentage Mark Up')
                               PROMPT('Retail Price'),AT(20,220),USE(?STO:Retail_Cost:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n14.2),AT(116,220,64,10),USE(sto:Retail_Cost),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             END
                             TAB('Virtual'),USE(?VirtualTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               PROMPT('Average Purch Cost'),AT(20,168),USE(?sto:AveragePurchaseCost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n10.2),AT(116,168,64,8),USE(sto:AveragePurchaseCost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Average Purchase Cost'),TIP('Average Purchase Cost'),UPR
                               PROMPT('Percentage Mark Up'),AT(20,184),USE(?sto:PurchaseMarkUp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n10.2),AT(116,184,64,8),USE(sto:PurchaseMarkUp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Percentage Mark Up'),TIP('Percentage Mark Up'),UPR
                               PROMPT('In Warranty'),AT(20,196),USE(?sto:Purchase_Cost:Prompt:4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n10.2),AT(116,196,64,8),USE(sto:Purchase_Cost,,?sto:Purchase_Cost:2),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                               PROMPT('Percent Mark Up'),AT(20,212),USE(?sto:VirtTradeMarkup:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n6.2),AT(116,212,64,8),USE(sto:Percentage_Mark_Up,,?sto:Percentage_Mark_Up:2),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Percent Mark Up'),TIP('Percent Mark Up'),UPR
                               PROMPT('Out Warranty'),AT(20,224),USE(?sto:Sale_Cost:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n10.2),AT(116,224,64,8),USE(sto:Sale_Cost,,?sto:Sale_Cost:2),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                             TAB('Reloc'),USE(?TabReloc),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               PROMPT('Purch Cost:'),AT(20,175),USE(?RelocatedPurchCost:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n-10.2),AT(108,175,64,10),USE(RelocatedPurchCost),RIGHT(12),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                               PROMPT('In Warranty:'),AT(20,193),USE(?RelocatedInWarranty:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n-10.2),AT(108,193,64,10),USE(RelocatedInWarranty),RIGHT(12),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                               PROMPT('Out Warranty:'),AT(20,215),USE(?RelocatedOutWarranty:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n-10.2),AT(108,215,64,10),USE(RelocatedOutWarranty),RIGHT(12),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             END
                           END
                           GROUP('Cost Details'),AT(8,150,296,112),USE(?CostDetailsGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           END
                           CHECK('Accessory'),AT(108,238),USE(sto:Accessory),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@n14.2),AT(184,238,48,8),USE(sto:AccessoryCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Accessory Cost'),TIP('Accessory Cost'),UPR
                           PROMPT('Acc Warranty Period'),AT(184,246),USE(?sto:AccWarrantyPeriod:Prompt),TRN,FONT(,7,COLOR:White,)
                           ENTRY(@s8),AT(184,254,48,8),USE(sto:AccWarrantyPeriod),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Accessory Warranty Period'),TIP('Accessory Warranty Period'),UPR
                           PROMPT('Days'),AT(236,254),USE(?AccWarDays),FONT(,8,COLOR:White,)
                           PROMPT('Accessory Cost'),AT(184,230),USE(?sto:AccessoryCost:Prompt),TRN,FONT(,7,COLOR:White,)
                         END
                       END
                       SHEET,AT(492,32,184,274),USE(?Sheet1),BELOW,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB,USE(?Model_Number_Tab)
                           PROMPT('Model Numbers That This Part Can Be Used For (Service Repairs Only)'),AT(496,33,176,25),USE(?Prompt26),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Pick Models From All Manufacturers'),AT(512,50),USE(tmp:AllManufacturers),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           LIST,AT(512,80,148,170),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)~Model Number~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(508,254),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(600,254),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           GROUP('Fault Codes Group'),AT(508,280,152,20),USE(?Fault_Codes_Group),HIDE
                             BUTTON,AT(604,278),USE(?Fault_Codes_Button),TRN,FLAT,HIDE,LEFT,ICON('viewcodp.jpg')
                             CHECK('Assign Fault Codes'),AT(512,285),USE(sto:Assign_Fault_Codes),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           END
                         END
                         TAB,USE(?Exchange_Unit_Tab),HIDE
                           PROMPT('Exchange Unit Associated Model Number'),AT(496,36),USE(?Prompt38),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Exchange Model Number'),AT(512,82),USE(?sto:ExchangeModelNumber:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Loan Model Number'),AT(512,106),USE(?sto:LoanModelNumber:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(512,92,128,10),USE(sto:ExchangeModelNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(512,116,128,10),USE(sto:LoanModelNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(644,112),USE(?LookupLoanModelNumber),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(644,88),USE(?LookupModelNumber),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       PROMPT('SRN:0000000'),AT(588,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(512,68,148,10),USE(sto:Manufacturer),VSCROLL,LEFT(2),FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Manufacturer_Q)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(312,308,176,84),USE(?Sheet5),FONT(,,,FONT:bold,CHARSET:ANSI),SPREAD
                         TAB('Stock Usage'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 7 Days'),AT(334,322),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(390,322),USE(days_7_temp),RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 30 Days'),AT(334,336),USE(?Prompt5:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(390,336),USE(days_30_temp),RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('31 - 60 Days'),AT(334,348),USE(?Prompt5:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(390,348),USE(days_60_temp),RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('61 - 90 Days'),AT(334,360),USE(?Prompt5:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(390,360),USE(days_90_temp),RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(390,372,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(334,376),USE(?Prompt5:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(422,376),USE(Average_text_temp),RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(492,308,184,84),USE(?Sheet7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 8'),USE(?Tab8)
                           PROMPT('Stock In Other Locations'),AT(496,313),USE(?Prompt25),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(496,324,176,64),USE(?List2),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('96L(2)|M~Location~@s30@32L(2)|M~Qty In Stock~@s8@'),FROM(OtherLocationQueue)
                         END
                       END
                       SHEET,AT(312,30,176,276),USE(?Sheet6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 6'),USE(?Tab6)
                           GROUP('Part Defaults'),AT(320,36,160,242),USE(?PartTypeDetailsGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Loan Unit'),AT(412,58),USE(sto:LoanUnit),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                             PROMPT('Exchange Order Cap'),AT(328,72),USE(?sto:ExchangeOrderCap:Prompt),DISABLE,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@n-14),AT(412,72,64,10),USE(sto:ExchangeOrderCap),DISABLE,RIGHT(2),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Order Cap'),TIP('Exchange Order Cap')
                             CHECK('Exclude From Level 1 && 2 Repairs'),AT(328,164),USE(sto:ExcludeLevel12Repair),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Exclude From Level 1 && 2 Repairs'),TIP('Exclude From Level 1 && 2 Repairs'),VALUE('1','0')
                           END
                           CHECK('Lev 1'),AT(334,184),USE(sto:E1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('E1'),TIP('E1'),VALUE('1','0')
                           CHECK('Sundry Item'),AT(328,46),USE(sto:Sundry_Item),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Exchange Unit'),AT(328,58),USE(sto:ExchangeUnit),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Allow Duplicate Part On Jobs'),AT(328,100),USE(sto:AllowDuplicate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Allow Duplicate Part On Jobs')
                           CHECK('Suspend Part'),AT(328,88),USE(sto:Suspend),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Suspend Part'),TIP('Suspend Part'),VALUE('1','0')
                           CHECK('Return Faulty Spare'),AT(328,112),USE(sto:ReturnFaultySpare),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Return Faulty Spare'),TIP('Return Faulty Spare'),VALUE('1','0')
                           CHECK('Chargeable Part Only'),AT(328,124),USE(sto:ChargeablePartOnly),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Chargeable Part Only'),TIP('Chargeable Part Only'),VALUE('1','0')
                           CHECK('Attach By Solder'),AT(328,136),USE(sto:AttachBySolder),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Attach By Solder'),TIP('Attach By Solder'),VALUE('1','0')
                           GROUP('Access Levels'),AT(326,176,148,20),USE(?AccessLevels),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           CHECK('Lev 2'),AT(386,184),USE(sto:E2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('E2'),TIP('E2'),VALUE('1','0')
                           CHECK('Lev 3'),AT(430,184),USE(sto:E3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('E3'),TIP('E3'),VALUE('1','0')
                           PROMPT('Repair Index'),AT(332,202),USE(?sto:RepairLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(412,202,64,10),USE(sto:RepairLevel),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Repair Level'),TIP('Repair Level'),UPR,RANGE(0,10),STEP(1)
                           PROMPT('Skill Level'),AT(332,218),USE(?sto:SkillLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(412,218,64,10),USE(sto:SkillLevel),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Skill Level'),TIP('Skill Level'),UPR,RANGE(0,10),STEP(1)
                         END
                       END
                       SHEET,AT(4,308,304,84),USE(?Sheet4),SPREAD
                         TAB('Availability Details'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Minimum Level '),AT(8,324,67,11),USE(?STO:Minimum_Level:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n8),AT(108,324,64,8),USE(sto:Minimum_Level),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           STRING('Quantity In Stock'),AT(8,340),USE(?String4),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Stock'),AT(132,340,44,12),USE(?Stock_Prompt),RIGHT,FONT('Tahoma',12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(184,334),USE(?Clear_Stock),SKIP,TRN,FLAT,LEFT,TIP('Reset Quantity In Stock'),ICON('zeroqp.jpg')
                           STRING('Quantity To Order'),AT(8,356),USE(?String8),FONT('Tahoma',12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('To Order'),AT(132,356,44,12),USE(?To_Order_Prompt),RIGHT,FONT(,12,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING('Quantity On Order'),AT(8,372),USE(?String10),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('On Order'),AT(132,372,44,12),USE(?On_Order_Prompt),RIGHT,FONT(,12,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(540,396),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(608,396),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Date Created: '),AT(12,404),USE(?Prompt33),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@d6),AT(72,404),USE(sto:DateBooked),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(275,398),USE(?Button:PartNumberHistory),TRN,FLAT,ICON('parhistp.jpg')
                       BUTTON,AT(184,398),USE(?Button:PrintLabel),TRN,FLAT,ICON('prnlabp.jpg')
                       BUTTON,AT(356,398),USE(?ChangeShelfLocation),TRN,FLAT,HIDE,LEFT,ICON('amlocp.jpg')
                       CHECK('Copy Part To All Sites'),AT(436,404),USE(Replicate_Temp),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_stm_id   ushort,auto
save_map_id   ushort,auto
    Map
AreThereModelNumbersAttached      Procedure(),Long
CountAttachedModelNumbers         Procedure(),Long
    End
!Save Entry Fields Incase Of Lookup
look:sto:Shelf_Location                Like(sto:Shelf_Location)
look:sto:Supplier                Like(sto:Supplier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ChangePartNumber        Routine
Data
local:CurrentPartNumber     Like(sto:Part_Number)
local:CurrentDescription    Like(sto:Description)
local:OriginalDescription   Like(sto:Description)
local:NewPartNumber         Like(sto:Part_Number)
local:NewDescription        Like(sto:Description)
local:CurrentLocation       Like(sto:Location)
local:Today                 Date()
local:Clock                 Time()
local:UserCode              String(3)

Code

    local:CurrentPartNumber = sto:Part_Number
    local:CurrentDescription = sto:Description
    local:CurrentLocation = sto:Location

    If ChangePartNumber(local:CurrentPartNumber,local:CurrentDescription,local:NewPArtNumber,local:NewDescription)
        ChangeAllPartNumbers(local:CurrentPartNumber,local:NewPartNumber,local:NewDescription,sto:Ref_Number,'UPDATE STOCK PART (' & Clip(local:CurrentLocation) & ')')
        sto:Part_Number = local:NewPartNumber
        sto:Description = local:NewDescription
        If Access:STOCK.Update() = Level:Benign
            ! Part updated correctly, add all the history stuff (DBH: 10/08/2007)
            If AddToStockHistory(sto:Ref_Number,'ADD','',0,0,0,sto:Purchase_Cost,sto:Sale_Cost,sto:Retail_Cost,|
                      'PART DETAILS UPDATED',|
                      'ORIGINAL PART NO: ' & Clip(local:CurrentPartNumber) & '<13,10>ORIGINAL DESC:' & Clip(local:CurrentDescription) & |
                      '<13,10>NEW PART NO: ' & Clip(local:NewPartNumber) & '<13,10>NEW DESC: ' & Clip(local:NewDescription))

            End ! '<13,10>NEW PART: ' & Clip(local:NewPartNumber) & ' - ' & Clip(local:NewDescription))
            If AddToPartNumberHistory(sto:Ref_Number,sto:Location,local:CurrentPartNumber,local:CurrentDescription,local:NewPartNumber,local:NewDescription,'UPDATE STOCK PART (' & Clip(local:CurrentLocation) & ')')

            End ! If AddToPartNumberHistory(sto:Ref_Number,sto:Location,local:CurrentPartNumber,local:CurrentDescription,local:NewPartNumber,local:NewDescription,'UPDATE STOCK PART (' & Clip(local:CurrentLocation) & ')')

            !

        End ! If Access:STOCK.Update() = Level:Benign

        Display()

    Else ! If ChangePartNumber(local:CurrentPartNumber,local:CurrentDescription,local:NewPArtNumber,local:NewDescription)

        Exit

    End ! If ChangePartNumber(local:CurrentPartNumber,local:CurrentDescription,local:NewPArtNumber,local:NewDescription)

Fault_Coding        Routine
    count# = 0
    setcursor(cursor:wait)

    If sto:manufacturer <> ''
        save_map_id = access:manfaupa.savefile()
        access:manfaupa.clearkey(map:field_number_key)
        map:manufacturer = sto:manufacturer
        set(map:field_number_key,map:field_number_key)
        loop
            if access:manfaupa.next()
               break
            end !if
            if map:manufacturer <> sto:manufacturer      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# = 1
            Break
        end !loop
        access:manfaupa.restorefile(save_map_id)

    End!If sto:manufacturer <> ''
    setcursor()

    If count# = 1
        Unhide(?fault_Codes_group)
    Else
        Hide(?fault_codes_group)
        sto:assign_fault_codes = 'NO'
    End

    If sto:assign_fault_codes = 'YES'
        Unhide(?fault_codes_button)
    Else
        Hide(?fault_codes_button)
    End
Fill_List   Routine
Hide_Fields     Routine

    If sto:Percentage_Mark_Up <> 0
        If sto:Location = MainStoreLocation()
            sto:sale_cost = Markups(sto:Sale_Cost,sto:Purchase_Cost,sto:Percentage_Mark_Up)
        Else !If loc:Location = MainStoreLocation()
            sto:Sale_Cost = Markups(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
        End !If loc:Location = MainStoreLocation()
        ?sto:Sale_Cost{prop:ReadOnly} = 1
        ?sto:Sale_Cost{prop:Skip} = 1
        ?sto:Sale_Cost{prop:color} = color:Silver
        ?sto:Sale_Cost:2{prop:ReadOnly} = 1
        ?sto:Sale_Cost:2{prop:Skip} = 1
        ?sto:Sale_Cost:2{prop:color} = color:Silver

    Else !sto:Percentage_Mark_Up <> 0
        ?sto:Sale_Cost{prop:ReadOnly} = 0
        ?sto:Sale_Cost{prop:Skip} = 0
        ?sto:Sale_Cost{prop:color} = color:White
        ?sto:Sale_Cost:2{prop:ReadOnly} = 0
        ?sto:Sale_Cost:2{prop:Skip} = 0
        ?sto:Sale_Cost:2{prop:color} = color:White

    End !sto:Percentage_Mark_Up <> 0

    If sto:RetailMarkup <> 0
        sto:Retail_Cost = Markups(sto:Retail_Cost,sto:Purchase_Cost,sto:RetailMarkup)
        ?sto:Retail_Cost{prop:ReadOnly} = 1
        ?sto:Retail_Cost{prop:Skip} = 1
        ?sto:Retail_Cost{prop:color} = color:Silver

    Else !sto:RetailMarkup <> 0
        ?sto:Retail_Cost{prop:ReadOnly} = 0
        ?sto:Retail_Cost{prop:Skip} = 0
        ?sto:Retail_Cost{prop:color} = color:White

    End !sto:RetailMarkup <> 0

    If sto:PurchaseMarkUp <> 0
        If loc:Location <> MainStoreLocation()
            sto:Purchase_Cost = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
        End !If loc:Locatio <> MainStoreLocation()
        ?sto:Purchase_Cost:2{prop:ReadOnly} = 1
        ?sto:Purchase_Cost:2{prop:Skip} = 1
        ?sto:Purchase_Cost:2{prop:color} = color:Silver

    Else !If sto:PurchaseMarkUp <> 0
        ?sto:Purchase_Cost:2{prop:ReadOnly} = 0
        ?sto:Purchase_Cost:2{prop:Skip} = 0
        ?sto:Purchase_Cost:2{prop:color} = color:White

    End !If sto:PurchaseMarkUp <> 0

!    If sto:VirtTradeMarkup <> 0
!        sto:VirtTradeCost = sto:VirtPurchaseCost + (sto:VirtPurchaseCost * (sto:VirtTradeMarkup/100))
!        ?sto:VirtTradeCost{prop:ReadOnly} = 1
!        ?sto:VirtTradeCost{prop:Skip} = 1
!        ?sto:VirtTradeCost{prop:color} = color:Silver
!    Else !sto:VirtTradeMarkup <> 0
!        ?sto:VirtTradeCost{prop:ReadOnly} = 0
!        ?sto:VirtTradeCost{prop:Skip} = 0
!        ?sto:VirtTradeCost{prop:color} = color:White
!    End !sto:VirtTradeMarkup <> 0
!
!    If sto:VirtRetailMarkup <> 0
!        sto:VirtRetailCost = sto:VirtPurchaseCost + (sto:VirtPurchaseCost * (sto:VirtRetailMarkup/100))
!        ?sto:VirtRetailCost{prop:ReadOnly} = 1
!        ?sto:VirtRetailCost{prop:Skip} = 1
!        ?sto:VirtRetailCost{prop:color} = color:Silver
!    Else !sto:VirtRetailMarkup <> 0
!        ?sto:VirtRetailCost{prop:ReadOnly} = 0
!        ?sto:VirtRetailCost{prop:Skip} = 0
!        ?sto:VirtRetailCost{prop:color} = color:White
!    End !sto:VirtRetailMarkup <> 0
!


    Display()
LevelRepairTickBox        Routine
    ! Only show the "Exclude From Level 1/2 Repair" tick box for Nokia - TrkBs: 5904 (DBH: 23-09-2005)
    If sto:Manufacturer = 'NOKIA'
        ?sto:ExcludeLevel12Repair{prop:Hide} = 0
    Else ! If sto:Manufacturer = 'NOKIA'
        ?sto:ExcludeLevel12Repair{prop:Hide} = 1
    End ! If sto:Manufacturer = 'NOKIA'
    Display()
Lookup_Shelf_Location       Routine
        glo:select2  = sto:location

        saverequest#      = globalrequest
        globalresponse    = requestcancelled
        globalrequest     = selectrecord
        browse_shelf_location
        if globalresponse = requestcompleted
            sto:shelf_location = los:shelf_location
            display()
        end
        globalrequest     = saverequest#
LookupLoanExchangeValue     ROUTINE
    Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
    mod:Manufacturer = sto:Manufacturer
    IF (sto:ExchangeUnit = 'YES')
        mod:Model_Number = sto:ExchangeModelNumber
        IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
            sto:Sale_Cost = mod:ExchReplaceValue
        END
    ELSIF (sto:LoanUnit)
        mod:Model_Number = sto:LoanModelNumber
        IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
            sto:Sale_Cost = mod:LoanReplacementValue
        END
    END
    DISPLAY()
DeleteAttachedModelNumbers      Routine

    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
    stm:Ref_Number = sto:Ref_Number
    Set(stm:Model_Number_Key,stm:Model_Number_Key)
    Loop Until Access:STOMODEL.Next()
        if (stm:Ref_Number <> sto:Ref_Number)
            Break
        End
        Relate:STOMODEL.Delete(0)
        Access:STOMODEL.Clearkey(stm:Model_Number_Key)
        stm:Ref_Number = sto:Ref_Number
        Set(stm:Model_Number_Key,stm:Model_Number_Key)
    End
Show_Details            Routine
    ?stock_prompt{prop:text}       = sto:quantity_stock
    ?to_order_prompt{prop:text}    = sto:quantity_to_order
    ?on_order_prompt{prop:text}    = sto:quantity_on_order
    Display()
ShowLoanExchangeDetails     ROUTINE
DATA
locModelNumber      STRING(30)
CODE

    !message('In the showloanexchangedetails with locations '&clip(Sto:Location)&'='&clip(MainStoreLocation()))

    IF (sto:Location = MainStoreLocation())
        IF (sto:ExchangeUnit = 'YES')

            ?STO:Purchase_Cost:Prompt{prop:Hide} = 1
            ?sto:Purchase_Cost{prop:Hide} = 1
            ?STO:Purchase_Cost:Prompt:3{prop:Hide} = 1
            ?STO:Retail_Cost:Prompt{prop:Hide} = 1
            ?sto:Percentage_Mark_Up{prop:Hide} = 1
            ?sto:RetailMarkup{prop:Hide} = 1
            ?sto:Retail_Cost{prop:Hide} = 1
            ?STO:Purchase_Cost:Prompt:2{prop:Hide} = 1
            sto:Sundry_Item = 'YES'

            Do LookupLoanExchangeValue

            ?sto:Sale_Cost{prop:ReadOnly} = 1
            ?sto:Sale_Cost{prop:Skip} = 1
            ?sto:ExchangeOrderCap{prop:Disable} = 0
            ?sto:ExchangeOrderCap:Prompt{prop:Disable} = 0
            ?Exchange_Unit_Tab{prop:Hide} = 0
            ?Model_Number_Tab{prop:Hide} = 1
            ?Replicate_Temp{prop:Disable} = 1

            ! #12347 Show/Hide Loan Details (DBH: 19/01/2012)
            ?sto:ExchangeModelNumber{prop:Hide} = 0
            ?sto:ExchangeModelNumber:Prompt{prop:Hide} = 0
            ?sto:LoanModelNumber{prop:Hide} = 1
            ?sto:LoanModelNumber:Prompt{Prop:Hide} = 1
            ?LookupModelNumber{prop:Hide} = 0
            ?LookupLoanModelNumber{prop:Hide} = 1

            Do DeleteAttachedModelNumbers
 !           message('This should have just swapped over the tabs')
        ELSIF (sto:LoanUnit = 1)

            ?STO:Purchase_Cost:Prompt{prop:Hide} = 1
            ?sto:Purchase_Cost{prop:Hide} = 1
            ?STO:Purchase_Cost:Prompt:3{prop:Hide} = 1
            ?STO:Retail_Cost:Prompt{prop:Hide} = 1
            ?sto:Percentage_Mark_Up{prop:Hide} = 1
            ?sto:RetailMarkup{prop:Hide} = 1
            ?sto:Retail_Cost{prop:Hide} = 1
            ?STO:Purchase_Cost:Prompt:2{prop:Hide} = 1
            sto:Sundry_Item = 'YES'

            Do LookupLoanExchangeValue
            ?sto:Sale_Cost{prop:ReadOnly} = 1
            ?sto:Sale_Cost{prop:Skip} = 1
            ?sto:ExchangeOrderCap{prop:Disable} = 1
            ?sto:ExchangeOrderCap:Prompt{prop:Disable} = 1
            ?Exchange_Unit_Tab{prop:Hide} = 0
            ?Model_Number_Tab{prop:Hide} = 1
            ?Replicate_Temp{prop:Disable} = 1

            ! #12347 Show/Hide Loan Details (DBH: 19/01/2012)
            ?sto:ExchangeModelNumber{prop:Hide} = 1
            ?sto:ExchangeModelNumber:Prompt{prop:Hide} = 1
            ?sto:LoanModelNumber{prop:Hide} = 0
            ?sto:LoanModelNumber:Prompt{Prop:Hide} = 0
            ?LookupModelNumber{prop:Hide} = 1
            ?LookupLoanModelNumber{prop:Hide} = 0

            Do DeleteAttachedModelNumbers
        ELSE
            ?sto:Sale_Cost{prop:ReadOnly} = 0
            ?sto:Sale_Cost{prop:Skip} = 0
            ?STO:Purchase_Cost:Prompt:2{prop:Hide} = 0
            ?STO:Purchase_Cost:Prompt{prop:Hide} = 0
            ?sto:Purchase_Cost{prop:Hide} = 0
!            ?STO:Purchase_Cost:Prompt:3{prop:Hide} = 0
!            ?STO:Retail_Cost:Prompt{prop:Hide} = 0
            ?sto:Percentage_Mark_Up{prop:Hide} = 0
!            ?sto:RetailMarkup{prop:Hide} = 0
!            ?sto:Retail_Cost{prop:Hide} = 0
            ?sto:ExchangeOrderCap{prop:Disable} = 1
            ?sto:ExchangeOrderCap:Prompt{prop:Disable} = 1
            ?Exchange_Unit_Tab{prop:Hide} = 1
            ?Model_Number_Tab{prop:Hide} = 0
            ?Replicate_Temp{prop:Disable} = 0
        END

        Bryan.CompFieldColour()
    ELSE   ! IF (sto:Location = MainStoreLocation())
        ?sto:LoanUnit{prop:Hide} = 1 ! This has no relevance when not Main Store
        sto:LoanUnit = 0
    END
Update_StoModel     Routine
    setcursor(cursor:wait)

    save_stm_id = access:stomodel.savefile()
    access:stomodel.clearkey(stm:ref_part_description)
    stm:location    = sto:location
    stm:ref_number  = sto:ref_number
    stm:part_number = sto:part_number
    set(stm:ref_part_description,stm:ref_part_description)
    loop
        if access:stomodel.next()
           break
        end !if
        if stm:location    <> sto:location      |
        or stm:ref_number  <> sto:ref_number      |
        or stm:part_number <> sto:part_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        stm:accessory   = sto:accessory
        access:stomodel.update()
    end !loop
    access:stomodel.restorefile(save_stm_id)
    setcursor()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Stock Item'
  OF ChangeRecord
    ActionMessage = 'Changing A Stock Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020077'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSTOCK')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sto:Record,History::sto:Record)
  SELF.AddHistoryField(?sto:Part_Number,3)
  SELF.AddHistoryField(?sto:Description,4)
  SELF.AddHistoryField(?sto:Location,13)
  SELF.AddHistoryField(?sto:Shelf_Location,11)
  SELF.AddHistoryField(?sto:Second_Location,14)
  SELF.AddHistoryField(?sto:Supplier,5)
  SELF.AddHistoryField(?sto:Purchase_Cost,6)
  SELF.AddHistoryField(?sto:Percentage_Mark_Up,10)
  SELF.AddHistoryField(?sto:Sale_Cost,7)
  SELF.AddHistoryField(?sto:RetailMarkup,52)
  SELF.AddHistoryField(?sto:Retail_Cost,8)
  SELF.AddHistoryField(?sto:AveragePurchaseCost,58)
  SELF.AddHistoryField(?sto:PurchaseMarkUp,59)
  SELF.AddHistoryField(?sto:Purchase_Cost:2,6)
  SELF.AddHistoryField(?sto:Percentage_Mark_Up:2,10)
  SELF.AddHistoryField(?sto:Sale_Cost:2,7)
  SELF.AddHistoryField(?sto:Accessory,26)
  SELF.AddHistoryField(?sto:AccessoryCost,9)
  SELF.AddHistoryField(?sto:AccWarrantyPeriod,63)
  SELF.AddHistoryField(?sto:Assign_Fault_Codes,28)
  SELF.AddHistoryField(?sto:ExchangeModelNumber,66)
  SELF.AddHistoryField(?sto:LoanModelNumber,68)
  SELF.AddHistoryField(?sto:Manufacturer,12)
  SELF.AddHistoryField(?sto:LoanUnit,67)
  SELF.AddHistoryField(?sto:ExchangeOrderCap,65)
  SELF.AddHistoryField(?sto:ExcludeLevel12Repair,64)
  SELF.AddHistoryField(?sto:E1,45)
  SELF.AddHistoryField(?sto:Sundry_Item,1)
  SELF.AddHistoryField(?sto:ExchangeUnit,30)
  SELF.AddHistoryField(?sto:AllowDuplicate,51)
  SELF.AddHistoryField(?sto:Suspend,32)
  SELF.AddHistoryField(?sto:ReturnFaultySpare,48)
  SELF.AddHistoryField(?sto:ChargeablePartOnly,49)
  SELF.AddHistoryField(?sto:AttachBySolder,50)
  SELF.AddHistoryField(?sto:E2,46)
  SELF.AddHistoryField(?sto:E3,47)
  SELF.AddHistoryField(?sto:RepairLevel,60)
  SELF.AddHistoryField(?sto:SkillLevel,61)
  SELF.AddHistoryField(?sto:Minimum_Level,18)
  SELF.AddHistoryField(?sto:DateBooked,62)
  SELF.AddUpdateFile(Access:STOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESDEF.Open
  Relate:COMMONCP.Open
  Relate:DEFAULTS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:MANFAUPA.UseFile
  Access:ORDERS.UseFile
  Access:ORDPEND.UseFile
  Access:ORDPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:LOCSHELF.UseFile
  Access:COMMONWP.UseFile
  Access:MODELNUM.UseFile
  Access:STOPARTS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  ! Build Manufacturer Q
  ! #11687 Use Q so that I can add a blank to it. (Bryan: 08/09/2010)
  Manufacturer_Q.Manufacturer = ''
  Add(Manufacturer_Q)
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer = ''
  Set(man:Manufacturer_Key,man:Manufacturer_Key)
  Loop Until Access:MANUFACT.Next()
      !TB12488 Disable Manufacturers
      !if man:Notes[1:8] = 'INACTIVE' then
      !TB13214 - change to using field for inactive
      if man:Inactive = 1 then
        !do not add
      ELSE
        Manufacturer_Q.Manufacturer = man:Manufacturer
        Add(Manufacturer_Q)
      END
  End
  ! Remove blanks from STOMODEL  (#12414)
      access:stomodel.clearkey(stm:Ref_Part_Description)
      stm:Ref_Number  = 0
      Set(stm:Ref_Part_Description,stm:Ref_Part_Description)
      LOOP
        If Access:STOMODEL.NEXT()
             Break
          End !If
          If stm:Ref_Number   <> 0      |
              Then Break.  ! End If
              Access:STOMODEL.DeleteRecord(0)
      End !Loop
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:STOMODEL,SELF)
    StockUsage(sto:Ref_Number,Days_7_Temp,Days_30_Temp,Days_60_Temp,Days_90_Temp,Average_Temp)

    If average_temp < 1
        average_text_temp = '< 1'
    Else!If average_temp < 1
        average_text_temp = Int(average_temp)
    End!If average_temp < 1
    !Evaluate Values
    If thiswindow.request <> Insertrecord
        Set(DEFAULTS)
        Access:DEFAULTS.Next()

        STO:Quantity_To_Order = 0                                                               !Go throught the pending order
        Case def:SummaryOrders
            Of 0 !Old Way
                setcursor(cursor:wait)                                                                  !file. And work out the quantity
                save_ope_id = access:ordpend.savefile()                                                 !awaiting order
                access:ordpend.clearkey(ope:part_ref_number_key)
                ope:part_ref_number =  sto:ref_number
                set(ope:part_ref_number_key,ope:part_ref_number_key)
                loop
                    if access:ordpend.next()
                       break
                    end !if
                    if ope:part_ref_number <> sto:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    sto:quantity_to_order += ope:quantity
                end !loop
                access:ordpend.restorefile(save_ope_id)
                setcursor()

            Of 1 !New Way
                sto:Quantity_To_Order   = sto:QuantityRequested
        End !Case def:SummaryOrders

        sto:quantity_on_order = 0                                                               !Go throught the order parts
        setcursor(cursor:wait)                                                                  !file. But check with the order
        save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
        access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
        orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
        set(orp:ref_number_key,orp:ref_number_key)                                              !up.
        loop
            if access:ordparts.next()
               break
            end !if
            if orp:part_ref_number <> sto:ref_number      |
                then break.  ! end if
            If orp:order_number <> ''
                access:orders.clearkey(ord:order_number_key)
                ord:order_number = orp:order_number
                if access:orders.fetch(ord:order_number_key) = Level:Benign
                    If ord:all_received = 'NO'
                        If orp:all_received = 'NO'
                            sto:quantity_on_order += orp:quantity
                        End!If orp:all_received <> 'YES'
                    End!If ord:all_received <> 'YES'
                end!if access:orders.fetch(ord:order_number_key) = Level:Benign
            End!If orp:order_number <> ''
        end !loop
        access:ordparts.restorefile(save_orp_id)
        setcursor()

        IF sto:quantity_to_order < 0
            sto:quantity_to_order = 0
        End!IF sto:quantity_to_order < 0
        If sto:quantity_on_order < 0
            sto:quantity_on_order = 0
        End!If sto:quantity_on_order < 0
    End!If thiswindow.request <> Insertrecord

    Clear(OtherLocationQueue)
    Free(OtherLocationQueue)
    !Find out how much stock there is in the other locations
    Save_loc_ID = Access:LOCATION.SaveFile()
    Access:LOCATION.ClearKey(loc:ActiveLocationKey)
    loc:Active   = 1
    Set(loc:ActiveLocationKey,loc:ActiveLocationKey)
    Loop
        If Access:LOCATION.NEXT()
           Break
        End !If
        If loc:Active   <> 1      |
            Then Break.  ! End If
        If loc:Location = sto:Location
            Cycle
        End !If loc:Location = sto:Location

        Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Part_Description_Key)
        sto_ali:Location    = loc:Location
        sto_ali:Part_Number = sto:Part_Number
        sto_ali:Description = sto:Description
        If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
            !Found
            othque:Location = loc:Location
            othque:QuantityInStock = sto_ali:Quantity_Stock
            Add(OtherLocationQueue)
        Else!If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
    End !Loop
    Access:LOCATION.RestoreFile(Save_loc_ID)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do Show_Details
  
  ?CostSheet{prop:NoSheet} = 1
  ?CostSheet{prop:Wizard} = 1
  ?VirtualTab{prop:Hide} = 1
  
  
  
  Do Hide_Fields
  Do LevelRepairTickBox
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  If glo:WebJob
      ?ChangeShelfLocation{prop:Hide} = 0
  End !glo:WebJob
  
  If SecurityCheck('STOCK - AVERAGE PURCHASE COST')
      If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          ?sto:Purchase_Cost{prop:Disable} = 1
      End !If ?sto:Purchase_Cost{prop:Text} = 'Average Purch Cost'
      ?sto:AveragePurchaseCost{prop:Disable} = 1
  End !SecurityCheck('STOCK - AVERAGE PURCHASE COST')
  
  ! Inserting (DBH 13/08/2007) # 9117 - Only show part number if user's got access
  If SecurityCheck('STOCK - CHANGE PART DETAILS') Or ThisWindow.Request = InsertRecord
      ?Button:ChangePartDetails{prop:Hide} = 1
  End ! If SecurityCheck('STOCK - CHANGE PART NUMBER')
  ! End (DBH 13/08/2007) #9117
  !Record in Use?
  If ThisWindow.Request <> Insertrecord
      Pointer# = Pointer(Stock)
      Hold(Stock,1)
      Get(Stock,Pointer#)
      If Errorcode() = 43
          Case Missive('This record is currently in use by another station.'&|
            '<13,10>'&|
            '<13,10>The screen will be VIEW ONLY.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          ThisWindow.Request = ViewRecord
          !Post(Event:CloseWindow)
      End !If Errorcode() = 43
  End !ThisWindow.Request <> Insertrecord
  if self.Request = ChangeRecord
      ?sto:Part_Number{Prop:ReadOnly} = true
      ?sto:Part_Number{Prop:Skip} = true
      ?sto:Description{prop:ReadOnly} = 1
      ?sto:Description{prop:Skip} = 1
      ! #11684 Tick the "all Manufacturers" tick box. (Bryan: 08/09/2010)
      if (sto:Manufacturer = '' and AreThereModelNumbersAttached() > 0)
          tmp:AllManufacturers = 1
      end
      ! #11687 Don't auto tick this box, as changes are replicated anyway.
      ! This tick is used for copying part detai (Bryan: 08/09/2010)
      Replicate_Temp = 'NO'
  end
  !clear blanks from stomodel file
  !nothin to do with this window - but a check to remove blanks
  !before you click the insert button
  access:stomodel_alias.clearkey(stm_ali:Model_Number_Key)
  stm_ali:Model_Number = ''
  if access:stomodel_alias.fetch(stm_ali:Model_Number_Key)= level:benign
      !this should not be here
      if relate:stomodel_alias.delete()
          !error
      END !if delete
  END!if fetch
  Model_Changed = FALSE
  
  if SecurityCheck('REPLICATE COSTS')
      ?Replicate_Temp{Prop:Disable} = true
  end
  
  ! #10396 Only allow to suspend if access level applied (DBH: 13/07/2010)
  if (SecurityCheck('SUSPEND STOCK ITEM'))
      ?sto:Suspend{prop:Disable} = 1
  end
  
  Do ShowLoanExchangeDetails
  If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      If sto:Location = MainStoreLocation() then
          if  RelocatedToMainStore then
              Select(?CostSheet,3)
              RelocatedPurchCost   = sto:Sale_Cost
              RelocatedInWarranty  = sto:Sale_Cost * (1 + InWarrantyMarkup(sto:Manufacturer,RelocatedSite)/100)
              RelocatedOutWarranty = sto:Sale_Cost * (1 + RelocatedMark_Up/100)
              !hide all the buttons except Cancel
              hide(?Button:ChangePartDetails)
              hide(?Lookup_Shelf_Location)
              hide(?Lookup_Supplier)
              hide(?Clear_Stock)
              hide(?Insert)
              hide(?Delete)
              hide(?Fault_Codes_Button)
              hide(?ChangeShelfLocation)
              hide(?OK)
  
          ELSE
              Select(?CostSheet,1)
              ?sto:Purchase_Cost:Prompt{prop:Text} = 'Average Purch Cost'
          END
      Else !sto:Location = MainStoreLocation()
          If VirtualSite(sto:Location)
              ?VirtualTab{prop:Hide} = 0
              ?MainStoreTab{prop:Hide} = 1
              Select(?CostSheet,2)
          End !If loc_ali:Virtual
      End !sto:Location = MainStoreLocation()
  End !GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  ?List{prop:vcr} = TRUE
  ?sto:Manufacturer{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?sto:Manufacturer{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?sto:Shelf_Location{Prop:Tip} AND ~?Lookup_Shelf_Location{Prop:Tip}
     ?Lookup_Shelf_Location{Prop:Tip} = 'Select ' & ?sto:Shelf_Location{Prop:Tip}
  END
  IF ?sto:Shelf_Location{Prop:Msg} AND ~?Lookup_Shelf_Location{Prop:Msg}
     ?Lookup_Shelf_Location{Prop:Msg} = 'Select ' & ?sto:Shelf_Location{Prop:Msg}
  END
  IF ?sto:Supplier{Prop:Tip} AND ~?Lookup_Supplier{Prop:Tip}
     ?Lookup_Supplier{Prop:Tip} = 'Select ' & ?sto:Supplier{Prop:Tip}
  END
  IF ?sto:Supplier{Prop:Msg} AND ~?Lookup_Supplier{Prop:Msg}
     ?Lookup_Supplier{Prop:Msg} = 'Select ' & ?sto:Supplier{Prop:Msg}
  END
  IF SELF.Request = ViewRecord
    DISABLE(?Button:ChangePartDetails)
    ?sto:Part_Number{PROP:ReadOnly} = True
    ?sto:Description{PROP:ReadOnly} = True
    ?sto:Location{PROP:ReadOnly} = True
    ?sto:Shelf_Location{PROP:ReadOnly} = True
    DISABLE(?Lookup_Shelf_Location)
    ?sto:Second_Location{PROP:ReadOnly} = True
    ?sto:Supplier{PROP:ReadOnly} = True
    DISABLE(?Lookup_Supplier)
    ?sto:Purchase_Cost{PROP:ReadOnly} = True
    ?sto:Percentage_Mark_Up{PROP:ReadOnly} = True
    ?sto:Sale_Cost{PROP:ReadOnly} = True
    ?sto:RetailMarkup{PROP:ReadOnly} = True
    ?sto:Retail_Cost{PROP:ReadOnly} = True
    ?sto:AveragePurchaseCost{PROP:ReadOnly} = True
    ?sto:PurchaseMarkUp{PROP:ReadOnly} = True
    ?sto:Purchase_Cost:2{PROP:ReadOnly} = True
    ?sto:Percentage_Mark_Up:2{PROP:ReadOnly} = True
    ?sto:Sale_Cost:2{PROP:ReadOnly} = True
    ?RelocatedPurchCost{PROP:ReadOnly} = True
    ?RelocatedInWarranty{PROP:ReadOnly} = True
    ?RelocatedOutWarranty{PROP:ReadOnly} = True
    DISABLE(?sto:Accessory)
    ?sto:AccessoryCost{PROP:ReadOnly} = True
    ?sto:AccWarrantyPeriod{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Delete)
    DISABLE(?Fault_Codes_Button)
    DISABLE(?sto:Assign_Fault_Codes)
    ?sto:ExchangeModelNumber{PROP:ReadOnly} = True
    ?sto:LoanModelNumber{PROP:ReadOnly} = True
    DISABLE(?LookupLoanModelNumber)
    DISABLE(?LookupModelNumber)
    DISABLE(?sto:Manufacturer)
    HIDE(?Prompt25)
    HIDE(?List2)
    DISABLE(?PartTypeDetailsGroup)
    ?sto:ExchangeOrderCap{PROP:ReadOnly} = True
    DISABLE(?AccessLevels)
    ?sto:Minimum_Level{PROP:ReadOnly} = True
    DISABLE(?Clear_Stock)
    HIDE(?String8)
    HIDE(?To_Order_Prompt)
    HIDE(?String10)
    HIDE(?On_Order_Prompt)
    HIDE(?OK)
    DISABLE(?Button:PartNumberHistory)
    DISABLE(?Button:PrintLabel)
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,stm:Mode_Number_Only_Key)
  BRW8.AddRange(stm:Ref_Number,sto:Ref_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,stm:Model_Number,1,BRW8)
  BRW8.AddField(stm:Model_Number,BRW8.Q.stm:Model_Number)
  BRW8.AddField(stm:RecordNumber,BRW8.Q.stm:RecordNumber)
  BRW8.AddField(stm:Ref_Number,BRW8.Q.stm:Ref_Number)
  IF ?sto:Accessory{Prop:Checked} = True
    UNHIDE(?sto:AccessoryCost:Prompt)
    UNHIDE(?sto:AccessoryCost)
    UNHIDE(?sto:AccWarrantyPeriod:Prompt)
    UNHIDE(?sto:AccWarrantyPeriod)
    UNHIDE(?AccWarDays)
  END
  IF ?sto:Accessory{Prop:Checked} = False
    HIDE(?sto:AccessoryCost:Prompt)
    HIDE(?sto:AccessoryCost)
    HIDE(?sto:AccWarrantyPeriod:Prompt)
    HIDE(?sto:AccWarrantyPeriod)
    HIDE(?AccWarDays)
  END
  IF ?tmp:AllManufacturers{Prop:Checked} = True
    HIDE(?sto:Manufacturer:Prompt)
    HIDE(?sto:Manufacturer)
  END
  IF ?tmp:AllManufacturers{Prop:Checked} = False
    UNHIDE(?sto:Manufacturer:Prompt)
    UNHIDE(?sto:Manufacturer)
  END
  BRW8.AskProcedure = 3
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RELEASE(Stock)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESDEF.Close
    Relate:COMMONCP.Close
    Relate:DEFAULTS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  If replicate_temp = 'YES' And thiswindow.response <> Requestcancelled
      If (Ref_Number_Temp > 0)
          Pick_Other_Location('SINGLE',ref_number_temp)
      End
  End!If replicate_temp = 'YES'
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    ref_number_temp = sto:ref_number
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  los:Site_Location = sto:Location                    ! Assign linking field value
  los:Shelf_Location = sto:Shelf_Location             ! Assign linking field value
  Access:LOCSHELF.Fetch(los:Shelf_Location_Key)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    If request = 1    !Insert
        Access:STOMODEL.CancelAutoInc()
        Error# = 0
        if tmp:AllManufacturers
            If sto:part_number = '' Or sto:description = '' Or sto:location = '' ! Or sto:manufacturer = ''
                Case Missive('The following fields MUST be filled in before you can attached Models to this part:'&|
                  '<13,10>'&|
                  '<13,10>Part Number, Description.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Error# = 1
            End!Case MessageEx
        Else
            If sto:part_number = '' Or sto:description = '' Or sto:location = '' Or sto:manufacturer = ''
                Case Missive('The following fields MUST be filled in before you can attached Models to this part:'&|
                  '<13,10>'&|
                  '<13,10>Part Number, Description, Manufacturer.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Error# = 1
            End!Case MessageEx
        End

        if Error# = 0
            if tmp:AllManufacturers
                glo:select2 = ''
            else
                glo:select2 = sto:manufacturer
            end
            Globalrequest   = Selectrecord
            Pick_Manufactuer_Model
            If Records(glo:Q_ModelNumber)

                Prog.ProgressSetup(Records(glo:Q_ModelNumber))
                Prog.ProgressText('Inserting Models...')
                Loop x# = 1 To Records(glo:Q_ModelNumber)
                    get(glo:Q_ModelNumber,x#)
                    if (prog.InsideLoop())
                        Break
                    end
                    get(stomodel,0)
                    if access:stomodel.primerecord() = level:benign

                        stm:ref_number   = sto:ref_number
                        stm:model_number = glo:model_number_pointer

                        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                        mod:Model_Number = stm:model_number
                        if not Access:MODELNUM.Fetch(mod:Model_Number_Key)
                            stm:manufacturer = mod:Manufacturer
                        else
                            stm:manufacturer = sto:manufacturer
                        end

                        stm:part_number  = sto:part_number
                        stm:description  = sto:description
                        stm:location     = sto:location
                        stm:accessory    = sto:accessory
                        if access:stomodel.tryinsert()
                            access:stomodel.cancelautoinc()
                        end
                    end!if access:stomodel.primerecord() = level:benign
                End !Loop x$ = 1 To Records(glo:Q_ModelNumber)
                prog.ProgressFinish()
            End !If Records(glo:Q_ModelNumber)
            glo:select2 = ''
        End

    Else
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseShelfLocations
      Browse_Suppliers
      Update_Stock_Model_Numbers
    END
    ReturnValue = GlobalResponse
  END
  End !If request = 1    !Insert
  BRW8.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?sto:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Accessory, Accepted)
      If sto:accessory <> accessory_temp
          If accessory_temp = 'NO'
                Case Missive('Are you sure you want to mark this item as an Accessory?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button

                        accessory_temp = sto:accessory
                        Do update_stomodel
                    Of 1 ! No Button

                        sto:accessory   = accessory_temp
                End ! Case Missive
          Else!If accessory_temp = 'NO'
                Case Missive('Are you sure you want to mark this item as NOT an Accessory?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                  accessory_temp = sto:accessory
                  Do update_stomodel
                    Of 1 ! No Button
                  sto:accessory   = accessory_temp
                End ! Case Missive
          End!If accessory_temp = 'YES'
      End!If sto:accessory <> accessory_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Accessory, Accepted)
    OF ?Insert
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      Model_Changed = TRUE
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Delete
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete, Accepted)
      Model_Changed = TRUE
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete, Accepted)
    OF ?sto:Suspend
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Suspend, Accepted)
      If ~0{prop:AcceptAll}
          If sto:Suspend = 1
              Case Missive('You have selected to Suspend this part.'&|
                '<13,10>'&|
                '<13,10>You will not be able to use this part for repairs, order it or sell it.'&|
                '<13,10>Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      sto:Suspend = 0
              End ! Case Missive
          Else !If sto:Suspend = 1
              Case Missive('This part is now free to be used for repairs, ordering or selling.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End !If sto:Suspend = 1
      End !0{prop:AcceptAll}
      Display(?sto:Suspend)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Suspend, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      sav2:Part_Number    =sto:Part_Number
      sav2:Description    =sto:Description
      sav2:Supplier       =sto:Supplier
      sav2:Purchase_Cost  =sto:Purchase_Cost
      sav2:Sale_Cost      =sto:Sale_Cost
      sav2:Retail_Cost    =sto:Retail_Cost
      sav2:Percentage_Mark_Up=sto:Percentage_Mark_Up
      sav2:Shelf_Location =sto:Shelf_Location
      sav2:Manufacturer   =sto:Manufacturer
      sav2:Second_Location=sto:Second_Location
      sav2:Minimum_Level  =sto:Minimum_Level
      sav2:Reorder_Level  =sto:Reorder_Level
      sav2:Accessory      =sto:Accessory
      sav2:ExchangeUnit   =sto:ExchangeUnit
      sav2:Suspend        =sto:Suspend
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Replicate_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Temp, Accepted)
      If ~0{prop:acceptall}
          If replicate_Temp = 'YES'
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Missive('When you save this record you will be asked to Copy This Part to All Other Site Locations.'&|
                  '|'&|
                  '|(Note: All Details Will Be Overwritten On All Other Parts)','ServiceBase',|
                             'mexclam.jpg','\&Cancel|/&Continue')
              Of 2 ! &Continue Button
              Of 1 ! &Cancel Button
                  Replicate_Temp = 'NO'
              End!Case Message
          End!If replicate_Temp = 'YES'
      End!If ~0{prop:acceptall}
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Temp, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:ChangePartDetails
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ChangePartDetails, Accepted)
      Do ChangePartNumber
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ChangePartDetails, Accepted)
    OF ?sto:Shelf_Location
      IF sto:Shelf_Location OR ?sto:Shelf_Location{Prop:Req}
        los:Site_Location = sto:Location
        los:Shelf_Location = sto:Shelf_Location
        los:Site_Location = sto:Location
        GLO:Select1 = sto:Location
        !Save Lookup Field Incase Of error
        look:sto:Shelf_Location        = sto:Shelf_Location
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sto:Location = los:Site_Location
            sto:Shelf_Location = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            sto:Shelf_Location = look:sto:Shelf_Location
            SELECT(?sto:Shelf_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Shelf_Location
      ThisWindow.Update
      los:Site_Location = sto:Location
      los:Shelf_Location = sto:Shelf_Location
      GLO:Select1 = sto:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sto:Location = los:Site_Location
          sto:Shelf_Location = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?sto:Shelf_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sto:Shelf_Location)
    OF ?sto:Supplier
      IF sto:Supplier OR ?sto:Supplier{Prop:Req}
        sup:Company_Name = sto:Supplier
        !Save Lookup Field Incase Of error
        look:sto:Supplier        = sto:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            sto:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            sto:Supplier = look:sto:Supplier
            SELECT(?sto:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Supplier
      ThisWindow.Update
      sup:Company_Name = sto:Supplier
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          sto:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?sto:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sto:Supplier)
    OF ?sto:Purchase_Cost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Purchase_Cost, Accepted)
      Do hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Purchase_Cost, Accepted)
    OF ?sto:Percentage_Mark_Up
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Percentage_Mark_Up, Accepted)
      Do hide_Fields

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Percentage_Mark_Up, Accepted)
    OF ?sto:RetailMarkup
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:RetailMarkup, Accepted)
      Do hide_Fields

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:RetailMarkup, Accepted)
    OF ?sto:AveragePurchaseCost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:AveragePurchaseCost, Accepted)
      Do hide_Fields

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:AveragePurchaseCost, Accepted)
    OF ?sto:PurchaseMarkUp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:PurchaseMarkUp, Accepted)
      Do hide_Fields

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:PurchaseMarkUp, Accepted)
    OF ?sto:Percentage_Mark_Up:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Percentage_Mark_Up:2, Accepted)
      Do hide_Fields

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Percentage_Mark_Up:2, Accepted)
    OF ?sto:Accessory
      IF ?sto:Accessory{Prop:Checked} = True
        UNHIDE(?sto:AccessoryCost:Prompt)
        UNHIDE(?sto:AccessoryCost)
        UNHIDE(?sto:AccWarrantyPeriod:Prompt)
        UNHIDE(?sto:AccWarrantyPeriod)
        UNHIDE(?AccWarDays)
      END
      IF ?sto:Accessory{Prop:Checked} = False
        HIDE(?sto:AccessoryCost:Prompt)
        HIDE(?sto:AccessoryCost)
        HIDE(?sto:AccWarrantyPeriod:Prompt)
        HIDE(?sto:AccWarrantyPeriod)
        HIDE(?AccWarDays)
      END
      ThisWindow.Reset
    OF ?tmp:AllManufacturers
      IF ?tmp:AllManufacturers{Prop:Checked} = True
        HIDE(?sto:Manufacturer:Prompt)
        HIDE(?sto:Manufacturer)
      END
      IF ?tmp:AllManufacturers{Prop:Checked} = False
        UNHIDE(?sto:Manufacturer:Prompt)
        UNHIDE(?sto:Manufacturer)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllManufacturers, Accepted)
      If ~0{prop:acceptall}
          ! Remove the current entries (Vodacare log L079)
          if (tmp:AllManufacturers = 0)
              If (AreThereModelNumbersAttached())
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('You have selected to use a single manufacturer. If you continue, all the attached Model Numbers will be deleted.','ServiceBase',|
                                 'mexclam.jpg','\&Cancel|/&Continue')
                  Of 2 ! &Continue Button
                      Do DeleteAttachedModelNumbers
                  Of 1 ! &Cancel Button
                      tmp:AllManufacturers = 1
                      ?sto:Manufacturer{prop:Hide} = 1
                      ?sto:Manufacturer:Prompt{prop:Hide} = 1
                      Display()
                      Cycle
                  End!Case Message
              End ! If (countRecords# > 0 and sto:Manufacturer <> '')
      
      
          !
          !    access:stomodel.clearkey(stm:model_number_key)
          !    stm:ref_number   = sto:ref_number
          !    set(stm:model_number_key,stm:model_number_key)
          !    loop
          !        if access:stomodel.next()
          !            break
          !        end
          !        if stm:ref_number <> sto:ref_number
          !            break
          !        end
          !        Relate:stomodel.Delete(0)
          !        access:stomodel.clearkey(stm:model_number_key)
          !        stm:ref_number   = sto:ref_number
          !        set(stm:model_number_key,stm:model_number_key)
          !    end
          Else
              sto:Manufacturer = ''
              Manufacturer_Temp = ''
              display()
          End
          BRW8.ResetSort(1)
          do Fault_Coding ! Only show fault coding in a manufacturer is selected
      end
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllManufacturers, Accepted)
    OF ?Fault_Codes_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes_Button, Accepted)
      brw8.UpdateViewRecord()
      ! #11687 Allow to pick a model number for different model numbers. (Bryan: 08/09/2010)
      if (stm:Manufacturer <> '' And stm:Model_Number <> '')
          GlobalRequest = ChangeRecord
          StockFaultCodesWindow
      End ! if (stm:Manufacturer <> '' And stm:Model_Number <> '')
      
      !Access:STOMODEL.ClearKey(stm:Model_Number_Key)
      !stm:Ref_Number   = sto:Ref_Number
      !stm:Manufacturer = sto:Manufacturer
      !stm:Model_Number = brw8.q.stm:Model_Number
      !If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
      !    GlobalRequest = ChangeRecord
      !    StockFaultCodesWindow
      !End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes_Button, Accepted)
    OF ?sto:Assign_Fault_Codes
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Assign_Fault_Codes, Accepted)
      Do fault_coding
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Assign_Fault_Codes, Accepted)
    OF ?sto:ExchangeModelNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:ExchangeModelNumber, Accepted)
      Do ShowLoanExchangeDetails
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:ExchangeModelNumber, Accepted)
    OF ?LookupLoanModelNumber
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupLoanModelNumber, Accepted)
          IF (NOT 0{PROP:AcceptAll})
              
              IF (sto:Manufacturer = '')
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Message('You must select a Manufacturer.','ServiceBase',|
                      Icon:Hand,'&OK',1) 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              
          
              mod:Model_number = sto:LoanModelNumber
              mod:Manufacturer = sto:Manufacturer
              IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
                  SET(mod:Manufacturer_Key,mod:Manufacturer_Key)
                  Access:MODELNUM.Next()
              END
              GlobalRequest = SelectRecord
              BrowseModelsByManufacturer(sto:Manufacturer)
              IF (GlobalResponse = RequestCompleted)
                  sto:LoanModelNumber = mod:Model_Number
                  DISPLAY()
              END
              SELF.Request = SELF.OriginalRequest
              Do ShowLoanExchangeDetails
      
      
          END  
              
              
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupLoanModelNumber, Accepted)
    OF ?LookupModelNumber
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelNumber, Accepted)
          IF (NOT 0{PROP:AcceptAll})
              
              IF (sto:Manufacturer = '')
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Message('You must select a Manufacturer.','ServiceBase',|
                      Icon:Hand,'&OK',1) 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
              
          
              mod:Model_number = sto:ExchangeModelNumber
              mod:Manufacturer = sto:Manufacturer
              IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
                  SET(mod:Manufacturer_Key,mod:Manufacturer_Key)
                  Access:MODELNUM.Next()
              END
              GlobalRequest = SelectRecord
              BrowseModelsByManufacturer(sto:Manufacturer)
              IF (GlobalResponse = RequestCompleted)
                  sto:ExchangeModelNumber = mod:Model_Number
                  DISPLAY()
              END
              SELF.Request = SELF.OriginalRequest
              Do ShowLoanExchangeDetails
      
      
          END  
              
              
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelNumber, Accepted)
    OF ?sto:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Manufacturer, Accepted)
      If ~0{prop:acceptall}
          If sto:manufacturer <> manufacturer_temp
              If manufacturer_temp = ''
                  if (AreThereModelNumbersAttached())
                      Beep(Beep:SystemExclamation)  ;  Yield()
                      Case Missive('Warning! You have selected a new Manufacturer. If  you continue the attached Model Numbers will be deleted.','ServiceBase',|
                                     'mexclam.jpg','\&Cancel|/&Continue')
                      Of 2 ! &Continue Button
                          Do DeleteAttachedModelNumbers
                      Of 1 ! &Cancel Button
                          Cycle
                      End!Case Message
                  end
                  manufacturer_temp = sto:manufacturer
      
              Else!If manufacturer_temp = ''
                  if (AreThereModelNumbersAttached())
                      Case Missive('Warning! If you continue to change the Manufacturer of this stock item, all the attached Model Number will be deleted.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              manufacturer_temp = sto:manufacturer
      
                              Do DeleteAttachedModelNumbers
                          Of 1 ! No Button
      
                              sto:manufacturer = manufacturer_temp
                      End ! Case Missive
                  Else
                      Manufacturer_Temp = sto:Manufacturer
                  End
              End!If manufacturer_temp = ''
              IF (sto:ExchangeUnit = 'YES')
                  sto:ExchangeModelNumber = ''
                  Display(?sto:ExchangeModelNumber)
              END
          End!If sto:manufacturer <> manufacturer_temp And manufacturer_temp <> ''
          Display(?sto:manufacturer)
      End!If ~0{prop:acceptall}
      Do Fault_Coding
      Do LevelRepairTickBox
      BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Manufacturer, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020077'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020077'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020077'&'0')
      ***
    OF ?sto:LoanUnit
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:LoanUnit, Accepted)
      IF (NOT 0{prop:AcceptAll})
          IF (sto:ExchangeUnit = 'YES' AND sto:LoanUnit)
              Beep(Beep:SystemQuestion)  ;  Yield()
              Case Missive('The item is already marked as an "Exchange Unit".'&|
                  '|Are you sure you want to change it to a "Loan Unit"?','ServiceBase',|
                             'mquest.jpg','\&No|/&Yes') 
              Of 2 ! &Yes Button
                  sto:ExchangeUnit = 'NO'
                  DISPLAY()
              Of 1 ! &No Button
                  sto:LoanUnit = 0
                  sto:ExchangeUnit = 'YES'
                  DISPLAY()
                  CYCLE
              End!Case Message
          END
      END
      Do ShowLoanExchangeDetails
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:LoanUnit, Accepted)
    OF ?sto:Sundry_Item
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Sundry_Item, Accepted)
      Do hide_fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Sundry_Item, Accepted)
    OF ?sto:ExchangeUnit
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:ExchangeUnit, Accepted)
      IF (NOT 0{prop:AcceptAll})
          IF (sto:ExchangeUnit = 'YES' AND sto:LoanUnit)
              Beep(Beep:SystemQuestion)  ;  Yield()
              Case Missive('The item is already marked as a "Loan Unit".'&|
                  '|Are you sure you want to change it to an "Exchange Unit"?','ServiceBase',|
                             'mquest.jpg','\&No|/&Yes') 
              Of 2 ! &Yes Button
                  sto:LoanUnit = 0
                  DISPLAY()
              Of 1 ! &No Button
                  sto:ExchangeUnit = 'NO'
                  sto:LoanUnit = 1
                  DISPLAY()
                  CYCLE
              End!Case Message
          END
      END
      Do ShowLoanExchangeDetails
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:ExchangeUnit, Accepted)
    OF ?Clear_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Stock, Accepted)
        Case Missive('This will zero the Quantity In Stock.'&|
          '<13,10>'&|
          '<13,10>Are you sure?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                StockQuantity$ = sto:Quantity_Stock
                sto:Quantity_Stock = 0
                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'DEC', | ! Transaction_Type
                                     '', | ! Depatch_Note_Number
                                     0, | ! Job_Number
                                     0, | ! Sales_Number
                                     StockQuanity$, | ! Quantity
                                     sto:Purchase_Cost, | ! Purchase_Cost
                                     sto:Sale_Cost, | ! Sale_Cost
                                     sto:Retail_Cost, | ! Retail_Cost
                                     'STOCK VALUE ZEROED', | ! Notes
                                     '') ! Information
                    ! Added OK
                Else ! AddToStockHistory
                    ! Error
                End ! AddToStockHistory
            Of 1 ! No Button
        End ! Case Missive

        Do show_details
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Stock, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button:PartNumberHistory
      ThisWindow.Update
      BrowsePartNumberHistory(sto:Ref_Number)
      ThisWindow.Reset
    OF ?Button:PrintLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PrintLabel, Accepted)
      Save_STOCK_ID = Access:STOCK.SaveFile()
      lab# = SelectLabelQuantity(1)
      If lab# > 0
          SingleStockBarcodeLabel(sto:Ref_Number,lab#)
      End ! If lab# > 0
      Access:STOCK.RestoreFile(Save_STOCK_ID)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PrintLabel, Accepted)
    OF ?ChangeShelfLocation
      ThisWindow.Update
      UpdateLocations
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sto:sundry_item = 'YES'       !===================================================================================================================
      sto:quantity_stock    = 1
      sto:quantity_to_order = 0
      sto:quantity_on_order = 0
  End!If sto:sundry_item = 'YES'
  
  !Retail costs should not be used,
  !so replicate the costs from the trade(selling) price... incase
  
  sto:Retail_Cost = sto:Sale_Cost
  sto:RetailMarkUp = sto:Percentage_Mark_Up
  
  IF (sto:ExchangeUnit = 'YES' AND sto:Location = MainStoreLocation())
      ! #12127 Exchange Parts only have one model number (Bryan: 28/06/2011)
      IF (sto:Manufacturer = '' OR sto:ExchangeModelNumber = '')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('This is an "Exchange Part". You must have a model number assigned to it.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
       END
  ELSIF (sto:LoanUnit = 1 AND sto:Location = MainStoreLocation())
      ! #12347 Loan Parts only have one model number (DBH: 19/01/2012)
      IF (sto:Manufacturer = '' OR sto:LoanModelNumber = '')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('This is an "Loan Part". You must have a model number assigned to it.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END ! IF (sto:Manufacturer = '' OR sto:LoanModelNumber = '')
  ELSE ! IF (sto:ExchangeUnit = 'YES' AND sto:Location = MainStoreLocation())
  
      models# = AreThereModelNumbersAttached()
      
      If (models# = 0)
          ! #11687 Check for models, but don't rely on a Manufacturer being applied. (Bryan: 08/09/2010)
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('There are NO Model Numbers attached to this part.'&|
              '|'&|
              '|If you continue this part will not apper in Service Stock Lists.'&|
              '|(Note: It will still appear in Retail Stock Lists)','ServiceBase',|
                         'mexclam.jpg','\&Cancel|/&Continue')
          Of 2 ! &Continue Button
      
          Of 1 ! &Cancel Button
              Cycle
          End!Case Message
      ELSE
      End ! If (AreThereModelNumbersAttached() = 0)
  END
  
  !Is this a new record, and has this part already been added to this location?
  found# = 0
  save_sto_ali_id = access:stock_alias.savefile()
  access:stock_alias.clearkey(sto_ali:location_key)
  sto_ali:location    = sto:location
  sto_ali:part_number = sto:part_number
  set(sto_ali:location_key,sto_ali:location_key)
  loop
      if access:stock_alias.next()
         break
      end !if
      IF sto_ali:location    <> sto:location
        BREAK
      END !End if     |
      IF sto_ali:part_number <> sto:part_number
        BREAK
      END  ! end if
      If sto:ref_number <> sto_ali:ref_number
          found# += 1
          Break
      End!If sto:ref_number <> sto_ali:ref_number
  end !loop
  
  
  !Part Numbers are unique alledegly
  If found# = 1
      Case Missive('The entered Part Number already exists in this location.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Select(?sto:part_number)
      Cycle
  End!If found# = 1
  
  ! #12127 Look for duplicate Model Numbers (Bryan: 01/07/2011)
  IF (sto:ExchangeUnit = 'YES' AND sto:Location = MainStoreLocation())
      Found# = 0
      Access:STOCK_ALIAS.Clearkey(sto_ali:ExchangeModelKey)
      sto_ali:Location = sto:Location
      sto_ali:Manufacturer = sto:Manufacturer
      sto_ali:ExchangeModelNumber = sto:ExchangeModelNumber
      SET(sto_ali:ExchangeModelKey,sto_ali:ExchangeModelKey)
      LOOP UNTIL Access:STOCK_ALIAS.Next()
          IF (sto_ali:Location <> sto:Location OR |
              sto_ali:Manufacturer <> sto:Manufacturer OR |
              sto_ali:ExchangeModelNumber <> sto:ExchangeModelNumber)
              BREAK
          END
          If (sto_ali:Ref_Number <> sto:Ref_Number)
              Found# = 1
              BREAK
          END
      END
      sto:LoanModelNumber = ''
      IF (Found# = 1)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Error! The selected Model Number has already been assigned to the Part: ' & Clip(sto_ali:Part_Number) & '.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END ! IF (Found# = 1)
      
  ELSIF (sto:LoanUnit = 1 AND sto:Location = MainStoreLocation())
      ! #12347 Loan Parts only have one model number (DBH: 19/01/2012)
      Found# = 0
      Access:STOCK_ALIAS.Clearkey(sto_ali:ExchangeModelKey)
      sto_ali:Location = sto:Location
      sto_ali:Manufacturer = sto:Manufacturer
      sto_ali:LoanModelNumber = sto:LoanModelNumber
      SET(sto_ali:ExchangeModelKey,sto_ali:ExchangeModelKey)
      LOOP UNTIL Access:STOCK_ALIAS.Next()
          IF (sto_ali:Location <> sto:Location OR |
              sto_ali:Manufacturer <> sto:Manufacturer OR |
              sto_ali:LoanModelNumber <> sto:LoanModelNumber)
              BREAK
          END
          If (sto_ali:Ref_Number <> sto:Ref_Number)
              Found# = 1
              BREAK
          END
      END
      sto:ExchangeModelNumber = ''
      IF (Found# = 1)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Error! The selected Model Number has already been assigned to the Part: ' & Clip(sto_ali:Part_Number) & '.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END ! IF (Found# = 1)
  ELSE
      ! Not an exchange/loan. Clear the model just in case
      sto:ExchangeModelNumber = ''
      sto:LoanModelNumber = ''
  END ! IF (sto:ExchangeUnit = 'YES')
  
  access:stock_alias.restorefile(save_sto_ali_id)
  
  !Check Access Levels
  Access:LOCATION.Clearkey(loc:Location_Key)
  loc:Location    = sto:Location
  If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
      !Found
      If sto:E1 <> loc:Level1 And |
          sto:E2 <> loc:Level2 And |
          sto:E3 <> loc:Level3
          Case Missive('The Access Levels of this part do not match the Access Levels of the location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !sto:E3 <> loc:Level3
  Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
      !Error
  End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
  
  ref_number_temp = sto:ref_Number
  !Stock History
  If ThisWindow.Request = InsertRecord
      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                         'ADD', | ! Transaction_Type
                         '', | ! Depatch_Note_Number
                         0, | ! Job_Number
                         0, | ! Sales_Number
                         0, | ! Quantity
                         sto:Purchase_Cost, | ! Purchase_Cost
                         sto:Sale_Cost, | ! Sale_Cost
                         sto:Retail_Cost, | ! Retail_Cost
                         'NEW STOCK ITEM CREATED', | ! Notes
                         '') ! Information
        ! Added OK
      Else ! AddToStockHistory
        ! Error
      End ! AddToStockHistory
  End !ThisWindow.Request = InsertRecord
  !Replicate
  If Thiswindow.Request <> InsertRecord
  !    Replicate_TEmp = 'NO'
      Access:STOCK_ALIAS.Clearkey(sto_ali:Ref_Number_Key)
      sto_ali:Ref_Number  = sto:Ref_Number
      If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
  
          If VirtualSite(sto:Location) = Level:Benign
              !Found
              If sto_Ali:Record <> sto:Record OR Model_Changed = TRUE
                  !If costs have changed, write into audit trail
                  If sto_ali:Purchase_Cost <> sto:Purchase_Cost Or |
                      sto_ali:Sale_Cost <> sto:Sale_Cost Or |
                      sto_ali:Retail_Cost <> sto:Retail_Cost
                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'ADD', | ! Transaction_Type
                                         '', | ! Depatch_Note_Number
                                         0, | ! Job_Number
                                         0, | ! Sales_Number
                                         0, | ! Quantity
                                         sto:Purchase_Cost, | ! Purchase_Cost
                                         sto:Sale_Cost, | ! Sale_Cost
                                         sto:Retail_Cost, | ! Retail_Cost
                                         'PREVIOUS DETAILS', | ! Notes
                                         'NEW DETAILS:-' & |
                                                      '<13,10>PURCHASE COST: ' & Format(sto:Purchase_Cost,@n14.2) &|
                                                      '<13,10>TRADE PRICE: ' & Format(sto:Sale_Cost,@n14.2) &|
                                                      '<13,10>RETAIL PRICE: ' & Format(sto:Retail_Cost,@n14.2) &|
                                                      '<13,10>OLD DETAILS:-' & |
                                                      '<13,10>PURCHASE COST: ' & Format(sto_ali:Purchase_Cost,@n14.2) &|
                                                      '<13,10>TRADE PRICE: ' & Format(sto_ali:Sale_Cost,@n14.2) &|
                                                      '<13,10>RETAIL PRICE: ' & Format(sto_ali:Retail_Cost,@n14.2)&|
                                                      '<13,10>QUANTITY: ' & Format(sto_ali:Quantity_Stock,@n14.2),  | ! Information
                                          sto_ali:AveragePurchaseCost, |
                                          sto_ali:Purchase_Cost, |
                                          sto_ali:Sale_Cost,  |
                                          sto_ali:Retail_Cost)
                        ! Added OK
  
                      Else ! AddToStockHistory
                        ! Error
                      End ! AddToStockHistory
                  End !If sto_ali
                  If (Replicate_Temp <> 'YES' and NOT(sto:Location = MainStoreLocation() And sto:ExchangeUnit = 'YES'))
                      ! #11687 If have chosen to replicate "copy", then there's no need to do it here. (Bryan: 08/09/2010)
                      Case Missive('You have changed this stock part. Do you wish to replicate this part, including the changes to all other occurances of this part?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              !Save old part number, incase they have changed
                              !and so they can be used to lookup other parts
                              tmp:OldPartNumber   = sto_ali:Part_Number
                              tmp:OldDescription  = sto_ali:Description
                              If Access:STOCK.TryUpdate() = Level:Benign
                                  !Save the record
                                  tmp:CurrentRecord = sto:Ref_Number
                                  tmp:SaveState   = GetState(STOCK)
  
                                  !Reget the current record from the alias file
                                  Access:STOCK_ALIAS.Clearkey(sto_ali:Ref_Number_Key)
                                  sto_ali:Ref_Number  = tmp:CurrentRecord
                                  If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
                                      !Found
  
                                      Prog.ProgressSetup(Records(LOCATION))
  
                                      Access:LOCATION.Clearkey(loc:Location_Key)
                                      loc:Location = ''
                                      Set(loc:Location_Key,loc:Location_Key)
                                      Loop Until Access:LOCATION.Next()
                                          if (prog.InsideLoop())
                                              Break
                                          End
  
                                          if (loc:Location = sto_ali:Location)
                                              Cycle
                                          end
  
                                            !Find the part at that location
                                            Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                            sto:Location    = loc:Location
                                            sto:Part_Number = tmp:OldPartNumber
                                            sto:Description = tmp:OldDescription
                                            If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                !Found
                                                save:RecordNumber   = sto:Ref_Number
                                                save:Location       = sto:Location
                                                save:QuantityStock = sto:Quantity_Stock
                                                save:ShelfLocation  = sto:Shelf_Location
                                                save:SecondLocation = sto:Second_Location
                                                save:AveragePurchaseCost    = sto:AveragePurchaseCost
                                                save:PurchaseMarkup = sto:PurchaseMarkup
                                                save:PurchaseCost  = sto:Purchase_Cost
                                                save:PercentageMarkUp = sto:Percentage_Mark_Up
                                                save:SaleCost       = sto:Sale_Cost
                                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                   'ADD', | ! Transaction_Type
                                                                   '', | ! Depatch_Note_Number
                                                                   0, | ! Job_Number
                                                                   0, | ! Sales_Number
                                                                   0, | ! Quantity
                                                                   sto:Purchase_Cost, | ! Purchase_Cost
                                                                   sto:Sale_Cost, | ! Sale_Cost
                                                                   sto:Retail_Cost, | ! Retail_Cost
                                                                   'PREVIOUS DETAILS', | ! Notes
                                                                   'PREVIOUS DETAILS:-' & |
                                                                                '<13,10>PART NUMBER: ' & Clip(sto:Part_Number) &|
                                                                                '<13,10>DESCRIPTION: ' & Clip(sto:Description) &|
                                                                                '<13,10>SUPPLIER: ' & CLip(sto:Supplier) &|
                                                                                '<13,10>PURCHASE COST: ' & CLip(sto:Purchase_Cost) &|
                                                                                '<13,10>TRADE PRICE: ' & CLip(sto:Sale_Cost) &|
                                                                                '<13,10>RETAIL PRICE: ' & CLip(sto:Retail_Cost) &|
                                                                                '<13,10>SHELF LOCATION: ' & CLip(sto:Shelf_Location) &|
                                                                                '<13,10>2ND LOCATION: ' & CLip(sto:Second_Location) &|
                                                                                '<13,10>MANUFACTURER: ' & CLip(sto:Manufacturer) &|
                                                                                '<13,10>MINIMUM LEVEL: ' & CLip(sto:Minimum_Level) &|
                                                                                '<13,10>MAKE UP TO LEVEL: ' & Clip(sto:Reorder_Level)) ! Information
                                                  ! Added OK
  
                                                Else ! AddToStockHistory
                                                  ! Error
                                                End ! AddToStockHistory
  
                                                sto:Record :=: sto_ali:Record
                                                sto:Ref_Number      = save:RecordNumber
                                                sto:Location        = save:Location
                                                sto:Quantity_Stock   = save:QuantityStock
                                                sto:Shelf_Location  = save:ShelfLocation
                                                sto:Second_Location = save:SecondLocation
  
                                                If sto_ali:Location = MainStoreLocation()
                                                    If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                                        If loc:VirtualSite
                                                            sto:AveragePurchaseCost = save:AveragePurchaseCost
                                                            !sto:PurchaseMarkup      = save:PurchaseMarkUp
                                                            !sto:Purchase_Cost       = save:PurchaseCost
                                                            !sto:Percentage_Mark_Up    = save:PercentageMarkUp
                                                            !sto:Sale_Cost           = save:SaleCost
  
                                                            sto:PurchaseMarkUp      = InWarrantyMarkup(sto:Manufacturer,loc:Location)
                                                            sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp
                                                            sto:Purchase_Cost       = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:PurchaseMarkup/100))
                                                            sto:Sale_Cost           = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:Percentage_Mark_Up/100))
                                                        End !If loc:VirtualSite
                                                    End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                                End !If sto_ali:Location = MainStoreLocation()
                                                If Access:STOCK.Update() = Level:Benign
                                                    !Update the stockmodel file
  
                                                    !1 - remove the existing records for this stock ref number
                                                    access:stomodel.clearkey(stm:Ref_Part_Description)
                                                    stm:Ref_Number  = sto:Ref_Number
                                                    Set(stm:Ref_Part_Description,stm:Ref_Part_Description)
                                                    loop
                                                        if access:stomodel.next() then break.
                                                        if stm:Ref_Number <> sto:Ref_Number then break.
                                                        if relate:stomodel.delete(0)
                                                            !error
                                                        END !if relate delete
                                                    END !Loop
  
                                                    !Added By Neil1
                                                    access:stomodel.clearkey(stm:Ref_Part_Description)
                                                    stm:Ref_Number  = 0
                                                    Set(stm:Ref_Part_Description,stm:Ref_Part_Description)
                                                    LOOP
                                                      If Access:STOMODEL.NEXT()
                                                           Break
                                                        End !If
                                                        If stm:Ref_Number   <> 0      |
                                                            Then Break.  ! End If
                                                        Delete(StoModel)
                                                    End !Loop
  
                                                    !2 - for each stm record on sto_ali:refNumber create matching for this sto:refnumber
                                                    access:stomodel_alias.clearkey(stm_ali:Ref_Part_Description)
                                                    stm_ali:Ref_Number  = sto_ali:ref_number
                                                    set(stm_ali:Ref_Part_Description,stm_ali:Ref_Part_Description)
  
                                                    Loop
                                                        If Access:STOMODEL_ALIAS.NEXT() then break.
                                                        If stm_ALI:Ref_Number  <> sto_ALI:Ref_Number then break.
                                                        access:stomodel.primerecord()
                                                        temp# = stm:RecordNumber
                                                        stm:record    :=: stm_ali:record
                                                        stm:ref_number = sto:ref_Number
                                                        stm:Location   = sto:location
                                                        stm:RecordNumber = temp#
                                                        if Access:STOMODEL.TryInsert()
                                                            !error
                                                            Access:STOMODEL.CancelAutoinc()
                                                        END !if tryinsert
                                                    End !Loop
  
                                                End !If Access:STOCK.Update()
  
  
                                            Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                !Error
                                                !Assert(0,'<13,10>Fetch Error<13,10>')
                                            End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                      End !Loop
                                      prog.ProgressFinish()
  
                                  Else! If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
  
                              End !If Access.STOCK.TryUpdate() = Level:Benign
                              RestoreState(STOCK,tmp:SaveState)
                              FreeState(STOCK,tmp:SaveState)
                          Of 1 ! No Button
                      End ! Case Missive
                  End ! If (Replicate_Temp <> 'YES')
              End !If sto_Ali:Record <> sto:Record
          Else !If VirtualSite(sto:Location) = Level:Benign
              !If costs have changed, write into audit trail
              If sto_ali:Purchase_Cost <> sto:Purchase_Cost Or |
                  sto_ali:Sale_Cost <> sto:Sale_Cost Or |
                  sto_ali:AveragePurchaseCost <> sto:AveragePurchaseCOst
                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'ADD', | ! Transaction_Type
                                     '', | ! Depatch_Note_Number
                                     0, | ! Job_Number
                                     0, | ! Sales_Number
                                     0, | ! Quantity
                                     sto:Purchase_Cost, | ! Purchase_Cost
                                     sto:Sale_Cost, | ! Sale_Cost
                                     sto:Retail_Cost, | ! Retail_Cost
                                     'PREVIOUS DETAILS', | ! Notes
                                     'NEW DETAILS:-' & |
                                                  '<13,10>AVR PURCHASE COST: ' & Format(sto:AveragePurchaseCost,@n14.2) &|
                                                  '<13,10>IN WARRANTY: ' & Format(sto:Purchase_Cost,@n14.2) &|
                                                  '<13,10>OUT WARRANTY: ' & Format(sto:Sale_Cost,@n14.2) &|
                                                  '<13,10>OLD DETAILS:-' & |
                                                  '<13,10>AVR PURCHASE COST: ' & Format(sto_ali:AveragePurchaseCost,@n14.2) &|
                                                  '<13,10>IN WARRANTY: ' & Format(sto_ali:Purchase_Cost,@n14.2) &|
                                                  '<13,10>OUT WARRANTY: ' & Format(sto_ali:Sale_Cost,@n14.2),  | ! Information
                                          sto_ali:AveragePurchaseCost, |
                                          sto_ali:Purchase_Cost, |
                                          sto_ali:Sale_Cost,  |
                                          sto_ali:Retail_Cost)
                    ! Added OK
  
                  Else ! AddToStockHistory
                    ! Error
                  End ! AddToStockHistory
  
              End !If sto_ali
  
          End !If VirtualSite(sto:Location) = Level:Benign
      Else! If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
  
  End !Thiswindow.Request <> InsertRecord
  !Call Replicate procedure
  If sto:Location <> MainStoreLocation()
      Replicate_Temp = 'NO'
  ELSE
      ! Don't replicate if Exchange Unit At Main Store
      IF (sto:ExchangeUnit = 'YES')
          Replicate_Temp = 'NO'
      END
  End !loc:Location <> MainStoreLocation()
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?sto:Shelf_Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Shelf_Location, AlertKey)
      Post(Event:accepted,?Lookup_Shelf_Location)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Shelf_Location, AlertKey)
    END
  OF ?sto:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Supplier, AlertKey)
      Post(Event:accepted,?Lookup_Supplier)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Supplier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Save Fields
      manufacturer_temp   = sto:manufacturer
      part_number_temp    = sto:part_number
      description_temp    = sto:description
      accessory_temp      = sto:accessory
      
      sav:Part_Number         = STO:Part_Number
      sav:Description         = STO:Description
      sav:Supplier            = STO:Supplier
      sav:Purchase_Cost       = STO:Purchase_Cost
      sav:Sale_Cost           = STO:Sale_Cost
      sav:Retail_Cost         = STO:Retail_Cost
      sav:Percentage_Mark_Up  = STO:Percentage_Mark_Up
      sav:Shelf_Location      = STO:Shelf_Location
      sav:Manufacturer        = STO:Manufacturer
      sav:Second_Location     = STO:Second_Location
      sav:Minimum_Level       = STO:Minimum_Level
      sav:Reorder_Level       = STO:Reorder_Level
      sav:Accessory           = STO:Accessory
      sav:Suspend             = sto:suspend
      sav:ExchangeUnit        = sto:ExchangeUnit
      
      Do Fault_Coding
      If VirtualSite(sto:Location) = Level:Benign
          Unhide(?replicate_temp)
          IF (sto:Location = MainStoreLocation() And sto:ExchangeUnit = 'YES')
              ?Replicate_Temp{prop:Hide} = 1
          END
      End!If thiswindow.request = Insertrecord
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      Do Fill_List
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
AreThereModelNumbersAttached        Procedure()
rtnValue    LONG()
    Code

    rtnValue = 0
    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
    stm:Ref_Number = sto:Ref_Number
    Set(stm:Model_Number_Key,stm:Model_Number_Key)
    Loop Until Access:STOMODEL.Next()
        if (stm:Ref_Number <> sto:Ref_Number)
            Break
        End
        rtnValue += 1
        
    End
    Return rtnValue
CountAttachedModelNumbers       Procedure()
    Code
        SetCursor(Cursor:Wait)
        countRecords# = 0
        Access:STOMODEL.Clearkey(stm:Model_Number_Key)
        stm:Ref_Number = sto:Ref_Number
        Set(stm:Model_Number_Key,stm:Model_Number_Key)
        Loop Until Access:STOMODEL.Next()
            if (stm:Ref_Number <> sto:Ref_Number)
                Break
            End
            countRecords# += 1
        End
        SetCursor()
        Return countRecords#
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?sto:Manufacturer{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?sto:Manufacturer{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
UpdateCurrencies PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
save:DailyRate       REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::cur:Record  LIKE(cur:RECORD),STATIC
QuickWindow          WINDOW('Update the CURRENCY File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Currency Exchange Rate'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD
                         TAB('312'),USE(?Tab:1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('UPDATING DAILY EXCHANGE RATE'),AT(269,116),USE(?UpdatingText),HIDE,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Currency Code'),AT(236,140),USE(?cur:CurrencyCode:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(320,140,124,10),USE(cur:CurrencyCode),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),REQ,UPR
                           PROMPT('Description'),AT(236,160),USE(?cur:Description:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(320,160,124,10),USE(cur:Description),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),REQ,UPR
                           OPTION('Divide / Multiple By'),AT(320,180,124,50),USE(cur:DivideMultiply),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('   * - Multiply By'),AT(344,192),USE(?cur:DivideMultiply:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('*')
                             RADIO('   / - Divide By'),AT(344,212),USE(?cur:DivideMultiply:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('/')
                           END
                           PROMPT('Daily Rate'),AT(236,240),USE(?cur:DailyRate:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.4),AT(320,240,64,10),USE(cur:DailyRate),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),REQ,UPR
                           PROMPT('Correlation Code'),AT(236,266),USE(?cur:CorrelationCode:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,264,124,10),USE(cur:CorrelationCode),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(372,332),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(372,332),USE(?UpdateButton),TRN,FLAT,HIDE,ICON('updatep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a CURRENCY Record'
  OF ChangeRecord
    ActionMessage = 'Changing a CURRENCY Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020631'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateCurrencies')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(cur:Record,History::cur:Record)
  SELF.AddHistoryField(?cur:CurrencyCode,2)
  SELF.AddHistoryField(?cur:Description,3)
  SELF.AddHistoryField(?cur:DivideMultiply,5)
  SELF.AddHistoryField(?cur:DailyRate,4)
  SELF.AddHistoryField(?cur:CorrelationCode,7)
  SELF.AddUpdateFile(Access:CURRENCY)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CURRENCY.Open
  Relate:CURRENCY_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CURRENCY
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! If "updating" then show update rather than ok buttons - TrkBs: 5110 (DBH: 17-05-2005)
  If glo:Select1 = 'UPDATE CURRENCY'
      ! Check to see if the daily rate is change during the update - TrkBs: 5110 (DBH: 17-05-2005)
      ?OK{Prop:Hide} = True
      ?UpdateButton{PROP:Hide} = False
      save:DailyRate = cur:DailyRate
      ?cur:CurrencyCode{PROP:ReadOnly} = True
      ?cur:Description{PROP:ReadOnly} = True
      ?cur:DivideMultiply{prop:Disable} = True
      ?cur:CurrencyCode{PROP:Skip} = True
      ?cur:Description{PROP:Skip} = True
      ?UpdatingText{PROP:Hide} = False
  End ! ThisWindow.Request = ViewRecord
  ! End   - If "updating" then show update rather than ok buttons - TrkBs: 5110 (DBH: 17-05-2005)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CURRENCY.Close
    Relate:CURRENCY_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020631'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020631'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020631'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?UpdateButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateButton, Accepted)
      Post(Event:Accepted,?OK)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If glo:Select1 = 'UPDATE CURRENCY'
      If cur:DailyRate = save:DailyRate
          Case Missive('You have not changed the Daily Rate.'&|
            '|Do you wish to update this record anyway?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Cycle
          End ! Case Missive
      End ! If cur:DailyRate = save:DailyRate
      cur:LastUpdateDate = Today()
  End ! If glo:Select1 = 'UPDATE CURRENCY'
  
  If (cur:CorrelationCode <> '') ! ! #11382 Prevent duplicate correlation codes (DBH: 06/08/2010)
      Access:CURRENCY_ALIAS.Clearkey(cur_ali:CorrelationCodeKey)
      cur_ali:CorrelationCode = cur:CorrelationCode
      If (Access:CURRENCY_ALIAS.TryFetch(cur_ali:CorrelationCodeKey) = Level:Benign)
          if (cur_ali:RecordNumber <> cur:RecordNumber)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error! The selected correlation code has already been used.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              Cycle
          end ! if (cur_ali:RecordNumber <> cur:RecordNumber)
      end ! If (Access:CURRENCY_ALIAS.TryFetch(cur_ali:CorrelationCodeKey) = Level:Benign)
  end ! If (cur:CorrelationCode <> '')
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

ReceiveReturningOrder PROCEDURE                       !Generated from procedure template - Window

locEngineer          STRING(60)
locEngineerUserCode  STRING(3)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(647,2,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(244,166,192,96),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Credit Note Request Number:'),AT(285,170),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold)
                       STRING(@n_8),AT(314,182),USE(cnr:RecordNumber),TRN,CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                       STRING(@n_8),AT(314,210),USE(rtn:WaybillNumber),TRN,CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,152,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Receive Order'),AT(247,154),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(383,154),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Waybill Number:'),AT(309,198),USE(?Prompt3:2),TRN,FONT(,,COLOR:White,FONT:bold)
                       PROMPT('Select Engineer To Handover To:'),AT(279,228),USE(?Prompt6),TRN,FONT(,,COLOR:White,FONT:bold)
                       PROMPT('Engineer'),AT(248,244),USE(?locEngineer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s60),AT(288,244,116,10),USE(locEngineer)
                       PANEL,AT(244,264,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(408,240),USE(?LookupEngineer),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(368,264),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(300,264),USE(?buttonFinish),TRN,FLAT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(locEngineerUserCode)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020762'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReceiveReturningOrder')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CREDNOTR.Open
  Relate:RTNORDER.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CREDNOTR.Close
    Relate:RTNORDER.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      locEngineerUserCode = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    OF ?buttonFinish
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
      IF (locEngineerUserCode = '' OR locEngineer = '')
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020762'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020762'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020762'&'0')
      ***
    OF ?LookupEngineer
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickEngineers
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEngineer, Accepted)
      IF (GlobalResponse = RequestCompleted)
          locEngineer = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
          locEngineerUserCode = use:User_Code
          DISPLAY()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEngineer, Accepted)
    OF ?buttonFinish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
