

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01005.INC'),ONCE        !Local module procedure declarations
                     END


UpdateOrderStock PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::orstmp:Record LIKE(orstmp:RECORD),STATIC
QuickWindow          WINDOW('Update the ORDSTOCK File'),AT(,,196,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOrderStock'),GRAY,RESIZE,MDI
                       SHEET,AT(4,4,188,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?orstmp:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,20,40,10),USE(orstmp:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Order Number'),AT(8,34),USE(?orstmp:OrderNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,34,40,10),USE(orstmp:OrderNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Order Number'),TIP('Order Number'),UPR
                           PROMPT('Part Number'),AT(8,48),USE(?orstmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,48,124,10),USE(orstmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           PROMPT('Location'),AT(8,62),USE(?orstmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,62,124,10),USE(orstmp:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),UPR
                           PROMPT('Quantity'),AT(8,76),USE(?orstmp:Quantity:Prompt),TRN
                           SPIN(@n8),AT(64,76,51,10),USE(orstmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity'),TIP('Quantity'),UPR,STEP(1)
                         END
                       END
                       BUTTON('OK'),AT(98,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(147,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(147,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOrderStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orstmp:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orstmp:Record,History::orstmp:Record)
  SELF.AddHistoryField(?orstmp:RecordNumber,1)
  SELF.AddHistoryField(?orstmp:OrderNumber,2)
  SELF.AddHistoryField(?orstmp:PartNumber,3)
  SELF.AddHistoryField(?orstmp:Location,4)
  SELF.AddHistoryField(?orstmp:Quantity,5)
  SELF.AddUpdateFile(Access:ORDSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDSTOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

GetDespatchNumber PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DespatchNoteNumber STRING(30)
window               WINDOW('Despatch Note Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Despatch Note Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Despatch Note Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(280,204,124,10),USE(tmp:DespatchNoteNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Despatch Note Number'),TIP('Despatch Note Number'),UPR
                         END
                       END
                       BUTTON,AT(368,258),USE(?Button1),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:DespatchNoteNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020103'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GetDespatchNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonHelp, Accepted)
      tmp:DespatchNoteNumber = ''
       POST(Event:CloseWindow)
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020103'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020103'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020103'&'0')
      ***
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonHelp, Accepted)
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AddNewPart PROCEDURE (func:RecordNumber)              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Location         STRING(30)
tmp:PartNumber       STRING(30)
tmp:Description      STRING(30)
tmp:ShelfLocation    STRING(30)
ActionMessage        CSTRING(40)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sto:Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?sto:Shelf_Location
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB5::View:FileDropCombo VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
window               WINDOW('Insert New Part'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Inserting A New Part'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Add New Part To Stock'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(236,135),USE(?tmp:Location:prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(312,135,124,10),USE(sto:Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Part Number'),AT(236,159),USE(?tmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,159,124,10),USE(sto:Part_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),REQ,UPR
                           PROMPT('Description'),AT(236,183),USE(?tmp:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,183,124,10),USE(sto:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),REQ,UPR
                           PROMPT('Shelf Location'),AT(236,207),USE(?tmp:ShelfLocation:promp),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(312,207,124,10),USE(sto:Shelf_Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020098'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AddNewPart')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:STOCK)
  Relate:LOCATION.Open
  Access:ORDPARTS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB4.Init(sto:Location,?sto:Location,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(loc:Location_Key)
  FDCB4.AddField(loc:Location,FDCB4.Q.loc:Location)
  FDCB4.AddField(loc:RecordNumber,FDCB4.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(sto:Shelf_Location,?sto:Shelf_Location,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCSHELF,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder(los:Shelf_Location_Key)
  FDCB5.AddRange(los:Site_Location,sto:Location)
  FDCB5.AddField(los:Shelf_Location,FDCB5.Q.los:Shelf_Location)
  FDCB5.AddField(los:Site_Location,FDCB5.Q.los:Site_Location)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020098'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020098'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020098'&'0')
      ***
    OF ?sto:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Location, Accepted)
      FDCB5.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Location, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
  orp:Record_Number   = func:RecordNumber
  If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
      !Found
      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                         'ADD', | ! Transaction_Type
                         orp:DespatchNoteNumber, | ! Depatch_Note_Number
                         0, | ! Job_Number
                         0, | ! Sales_Number
                         sto:Quantity_Stock, | ! Quantity
                         sto:Purchase_Cost, | ! Purchase_Cost
                         sto:Sale_Cost, | ! Sale_Cost
                         sto:Retail_Cost, | ! Retail_Cost
                         'STOCK ADDED FROM ORDER', | ! Notes
                         'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                          '<13,10>DESPATCH NOTE NO: ' & Clip(orp:DespatchNoteNumber)) ! Information
        ! Added OK
  
      Else ! AddToStockHistory
        ! Error
      End ! AddToStockHistory
  Else! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
      !Error
  End! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateMinimumStock PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
Average_text_temp    STRING(8)
Average_Temp         LONG
save_shi_id          USHORT,AUTO
tmp:PurchaseCost     REAL
tmp:SaleCost         REAL
tmp:RetailCost       REAL
History::smin:Record LIKE(smin:RECORD),STATIC
QuickWindow          WINDOW('Minimum Stock Level Item'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,208,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Part Details'),USE(?PartDetails),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(168,98),USE(?SMIN:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(244,98,124,10),USE(smin:PartNumber),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),MSG('Part Number'),TIP('Part Number'),UPR,READONLY
                           PROMPT('Description'),AT(168,114),USE(?SMIN:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(244,114,124,10),USE(smin:Description),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),MSG('Descriptoin'),TIP('Descriptoin'),UPR,READONLY
                           PROMPT('Supplier'),AT(168,130),USE(?SMIN:Supplier:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(244,130,100,10),USE(smin:Supplier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('Supplier'),TIP('Supplier'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseRight),ALRT(MouseLeft2),REQ,UPR
                           BUTTON,AT(344,126),USE(?LookupSuppliers),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Purchase Price'),AT(168,146),USE(?sto:Purchase_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(244,146,64,10),USE(tmp:PurchaseCost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Selling Price'),AT(168,162),USE(?sto:Sale_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(244,162,64,10),USE(tmp:SaleCost),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Minimum Level'),AT(168,178),USE(?SMIN:MinLevel:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(244,178,64,10),USE(smin:MinLevel),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),MSG('Minimum Level'),TIP('Minimum Level'),UPR,READONLY
                           PROMPT('Quantity On Order'),AT(168,194),USE(?SMIN:QuantityOnOrder:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(244,194,64,10),USE(smin:QuantityOnOrder),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),MSG('Quantity On Order'),TIP('Quantity On Order'),UPR,READONLY
                           PROMPT('Quantity In Stock'),AT(168,210),USE(?SMIN:QuantityInStock:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(244,210,64,10),USE(smin:QuantityInStock),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),MSG('Quantity In Stock'),TIP('Quantity In Stock'),UPR,READONLY
                           PROMPT('Quantity Required'),AT(168,226),USE(?smin:QuantityRequired:prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@s8),AT(244,226,64,10),USE(smin:QuantityRequired),FONT('Tahoma',8,COLOR:Red,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),UPR,RANGE(1,99999),STEP(1),MSG('Quantity Required')
                         END
                       END
                       SHEET,AT(376,82,140,248),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Usage'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 7 Days'),AT(380,98),USE(?07Days),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,98),USE(days_7_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14),AT(440,111),USE(days_30_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14),AT(440,135),USE(days_90_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Average Daily Use'),AT(380,151),USE(?AverageDailyUse),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(472,151),USE(Average_text_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Supplier Defaults:'),AT(380,170),USE(?SupplierDefaults),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('History Usage'),AT(380,182),USE(?Prompt16),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n2),AT(496,182),USE(sup:HistoryUsage),RIGHT,FONT(,,,FONT:bold)
                           STRING(@s8),AT(472,194),USE(sup:Factor),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Multiply By Factor Of'),AT(380,194),USE(?MultipleByFactorOf),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Normal Supply Period'),AT(380,207),USE(?NormalSupplyPeriod),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n3),AT(492,207),USE(sup:Normal_Supply_Period),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Minimum Order Value'),AT(380,220),USE(?MinimumOrderValue),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(472,220),USE(sup:Minimum_Order_Value),RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(516,148,-59,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n-14),AT(440,124),USE(days_60_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(380,111),USE(?030Days),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('31 - 60 Days'),AT(380,124),USE(?3160Days),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('61 - 90 Days'),AT(380,135),USE(?6190Days),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Minimum Stock Item'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(168,298),USE(?UpdateAndOrder),TRN,FLAT,LEFT,ICON('uporderp.jpg')
                       BUTTON,AT(380,332),USE(?OKButton),TRN,FLAT,HIDE,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(236,298),USE(?UpdateAndRemove),TRN,FLAT,LEFT,ICON('upremorp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:smin:Supplier                Like(smin:Supplier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
UpdateStock     Routine
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = smin:PartRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        sto:Purchase_Cost   = tmp:PurchaseCost
        sto:Sale_Cost       = tmp:SaleCost
        sto:Retail_Cost     = tmp:RetailCost
        sto:Supplier        = smin:Supplier
        Access:STOCK.Update()
    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
CreateOrder     Routine
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Case def:SummaryOrders
        Of 0 !Old Fashioned ORders
            If access:ordpend.primerecord() = Level:Benign
                ope:part_ref_number = sto:ref_number
                ope:part_type       = 'STO'
                ope:supplier        = smin:Supplier
                ope:part_number     = sto:part_number
                ope:Description     = sto:description
                ope:job_number      = ''
                ope:quantity        = smin:QuantityRequired
                access:ordpend.insert()
            End !If access:ordpend.primerecord() = Level:Benign

        Of 1 !New ORders
            MakePartsRequest(smin:Supplier,sto:Part_Number,sto:Description,smin:QuantityRequired)

            sto:QuantityRequested += smin:QuantityRequired
            Access:STOCK.Update()

    End!Case def:SummaryOrders
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Minimum Stock Level Item'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020079'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMinimumStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SMIN:PartNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(smin:Record,History::smin:Record)
  SELF.AddHistoryField(?smin:PartNumber,3)
  SELF.AddHistoryField(?smin:Description,4)
  SELF.AddHistoryField(?smin:Supplier,5)
  SELF.AddHistoryField(?smin:MinLevel,7)
  SELF.AddHistoryField(?smin:QuantityOnOrder,8)
  SELF.AddHistoryField(?smin:QuantityInStock,9)
  SELF.AddHistoryField(?smin:QuantityRequired,10)
  SELF.AddUpdateFile(Access:STOCKMIN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDPEND.Open
  Relate:STOCKMIN.Open
  Access:STOCK.UseFile
  Access:SUPPLIER.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCKMIN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OKButton
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
      glo:DeleteStock = 0
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = smin:PartRefNumber
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
          tmp:PurchaseCost    = sto:Purchase_Cost
          tmp:SaleCost        = sto:Sale_Cost
          tmp:RetailCost      = sto:Retail_Cost
      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
      Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
      sup:Company_Name    = smin:Supplier
      If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
          !Found
  
      Else! If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
  
  
      StockUsage(sto:Ref_Number,Days_7_Temp,Days_30_Temp,Days_60_Temp,Days_90_Temp,Average_Temp)
  
      If average_temp < 1
          average_text_temp = '< 1'
      Else!If average_temp < 1
          average_text_temp = Int(average_temp)
      End!If average_temp < 1
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?smin:Supplier{Prop:Tip} AND ~?LookupSuppliers{Prop:Tip}
     ?LookupSuppliers{Prop:Tip} = 'Select ' & ?smin:Supplier{Prop:Tip}
  END
  IF ?smin:Supplier{Prop:Msg} AND ~?LookupSuppliers{Prop:Msg}
     ?LookupSuppliers{Prop:Msg} = 'Select ' & ?smin:Supplier{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPEND.Close
    Relate:STOCKMIN.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSuppliers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?smin:Supplier
      IF smin:Supplier OR ?smin:Supplier{Prop:Req}
        sup:Company_Name = smin:Supplier
        !Save Lookup Field Incase Of error
        look:smin:Supplier        = smin:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            smin:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            smin:Supplier = look:smin:Supplier
            SELECT(?smin:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSuppliers
      ThisWindow.Update
      sup:Company_Name = smin:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          smin:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?smin:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?smin:Supplier)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020079'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020079'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020079'&'0')
      ***
    OF ?UpdateAndOrder
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndOrder, Accepted)
      Do UpdateStock
      Do CreateOrder
      glo:DeleteStock = 1
      Post(Event:Accepted,?OkButton)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndOrder, Accepted)
    OF ?OKButton
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?UpdateAndRemove
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndRemove, Accepted)
      Do UpdateStock
      glo:DeleteStock = 1
      Post(Event:Accepted,?OkButton)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndRemove, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?smin:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?smin:Supplier, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?smin:Supplier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseMinimumStock PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
Date_Run             LONG
Time_Run             LONG
tmp:UseScheduledMinStock BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:MinimumLevel     LONG
tmp:QuantityToOrder  LONG
tmp:QuantityOnOrder  LONG
tmp:Usage            LONG
save_sto_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
save_shi_id          USHORT,AUTO
save_smin_id         USHORT,AUTO
tmp:Location         STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCKMIN)
                       PROJECT(smin:Description)
                       PROJECT(smin:PartNumber)
                       PROJECT(smin:Supplier)
                       PROJECT(smin:MinLevel)
                       PROJECT(smin:QuantityOnOrder)
                       PROJECT(smin:QuantityInStock)
                       PROJECT(smin:QuantityRequired)
                       PROJECT(smin:RecordNumber)
                       PROJECT(smin:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
smin:Description       LIKE(smin:Description)         !List box control field - type derived from field
smin:PartNumber        LIKE(smin:PartNumber)          !List box control field - type derived from field
smin:Supplier          LIKE(smin:Supplier)            !List box control field - type derived from field
smin:MinLevel          LIKE(smin:MinLevel)            !List box control field - type derived from field
smin:QuantityOnOrder   LIKE(smin:QuantityOnOrder)     !List box control field - type derived from field
smin:QuantityInStock   LIKE(smin:QuantityInStock)     !List box control field - type derived from field
smin:QuantityRequired  LIKE(smin:QuantityRequired)    !List box control field - type derived from field
smin:RecordNumber      LIKE(smin:RecordNumber)        !Primary key field - type derived from field
smin:Location          LIKE(smin:Location)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::smin:Location        LIKE(smin:Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB8::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Minimum Stock Routine'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(68,58),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s20),AT(144,58,124,10),USE(tmp:Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       STRING('String 1'),AT(344,58,268,12),USE(?String1),FONT('Tahoma',12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON('&Insert'),AT(396,58,42,12),USE(?Insert),HIDE
                       BUTTON,AT(68,332),USE(?Change),TRN,FLAT,LEFT,ICON('proitemp.jpg')
                       BUTTON,AT(132,332),USE(?ProcessAllItems),TRN,FLAT,LEFT,ICON('praitemp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Minimum Stock Routine'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(548,332),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       LIST,AT(68,108,544,220),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@120L(2)|M~Part Number~@s30@120L(2)|M~Supplier~@s30@38' &|
   'L(2)|M~Min Level~@s8@38L(2)|M~On Order~@s8@38L(2)|M~In Stock~@s8@38L(2)|M~Requir' &|
   'ed~@s8@'),FROM(Queue:Browse:1)
                       SHEET,AT(64,78,552,286),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Description'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(68,94,124,10),USE(smin:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Descriptoin'),TIP('Descriptoin'),UPR
                         END
                         TAB('By Part Number'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(68,94,124,10),USE(smin:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),UPR
                         END
                       END
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020078'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseMinimumStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:STOCKMIN.Open
  Relate:TRADEACC_ALIAS.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:ORDERS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:SUPPLIER.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCKMIN,SELF)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  tmp:UseScheduledMinStock = GETINI('STOCK','UseScheduledMinStock',,CLIP(PATH()) & '\SB2KDEF.INI')
  
  IF tmp:UseScheduledMinStock = FALSE
  
      Case Missive('Minimum Stock Routine.'&|
        '<13,10>'&|
        '<13,10>This routine may take a long time to complete.'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
  
            Prog.ProgressSetup(Records(STOCK))
  
            Save_sto_ID = Access:STOCK.SaveFile()
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = 1
            Set(sto:Ref_Number_Key,sto:Ref_Number_Key)
            Loop
                If Access:STOCK.NEXT()
                   Break
                End !If
                If Prog.InsideLoop()
                  Break
                End ! If Prog.InsideLoop()
  
                !Work Out Supplier Usage Value
                Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                sup:Company_Name = sto:Supplier
                If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                    !Found
  
                Else!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
  
                !Work Out Minimum Level
                tmp:Usage   = 0
                tmp:MinimumLevel    = 0
                Save_shi_ID = Access:STOHIST.SaveFile()
                Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
                shi:Ref_Number       = sto:Ref_Number
                shi:Transaction_Type = 'DEC'
                shi:Date             = Today() - sup:HistoryUsage
                Set(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
                Loop
                    If Access:STOHIST.NEXT()
                       Break
                    End !If
                    If shi:Ref_Number       <> sto:Ref_Number      |
                    Or shi:Transaction_Type <> 'DEC'      |
                    Or shi:Date             > Today()     |
                        Then Break.  ! End If
  
                    IF INSTRING('STOCK AUDIT',shi:despatch_note_number,1)
                      CYCLE
                    END
                    tmp:Usage   += shi:Quantity
  
                End !Loop
                Access:STOHIST.RestoreFile(Save_shi_ID)
  
                tmp:MinimumLevel    = ABS(tmp:Usage * sup:Factor/100)
                !Work Out Quantity On Order
                If def:SummaryOrders !New Order Process
  
                    If tmp:MinimumLevel - sto:QuantityRequested - sto:Quantity_Stock > 1
                        If Access:STOCKMIN.PrimeRecord() = Level:Benign
                            smin:Location            = sto:Location
                            smin:PartNumber          = sto:Part_Number
                            smin:Description         = sto:Description
                            smin:Supplier            = sto:Supplier
                            smin:Usage               = tmp:Usage
                            smin:MinLevel            = tmp:MinimumLevel
                            smin:QuantityOnOrder     = sto:QuantityRequested
                            smin:QuantityInStock     = sto:Quantity_Stock
                            smin:QuantityRequired    = (tmp:MinimumLevel - sto:QuantityRequested - sto:Quantity_Stock)
                            smin:PartRefNumber       = sto:Ref_number
                            smin:QuantityJobsPending = ''
  
                            If Access:STOCKMIN.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:STOCKMIN.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:STOCKMIN.TryInsert() = Level:Benign
                        End !If Access:STOCKMIN.PrimeRecord() = Level:Benign
                    End !If sto:Minimum_Stock - sto:QuantityRequested - sto:Quantity_Stock > 1
                Else !If def:SummaryOrders
                    tmp:QuantityToOrder = 0
                    save_ope_id = access:ordpend.savefile()                                                 !awaiting order
                    access:ordpend.clearkey(ope:part_ref_number_key)
                    ope:part_ref_number =  sto:ref_number
                    set(ope:part_ref_number_key,ope:part_ref_number_key)
                    loop
                        if access:ordpend.next()
                           break
                        end !if
                        if ope:part_ref_number <> sto:ref_number      |
                            then break.  ! end if
  
                        tmp:QuantityToOrder += ope:quantity
                    end !loop
                    access:ordpend.restorefile(save_ope_id)
  
                    tmp:QuantityOnOrder = 0
                    setcursor(cursor:wait)                                                                  !file. But check with the order
                    save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
                    access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
                    orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
                    set(orp:ref_number_key,orp:ref_number_key)                                              !up.
                    loop
                        if access:ordparts.next()
                           break
                        end !if
                        if orp:part_ref_number <> sto:ref_number      |
                            then break.  ! end if
                        If orp:order_number <> ''
                            access:orders.clearkey(ord:order_number_key)
                            ord:order_number = orp:order_number
                            if access:orders.fetch(ord:order_number_key) = Level:Benign
                                If ord:all_received = 'NO'
                                    If orp:all_received = 'NO'
                                        tmp:QuantityOnOrder += orp:quantity
                                    End!If orp:all_received <> 'YES'
                                End!If ord:all_received <> 'YES'
                            end!if access:orders.fetch(ord:order_number_key) = Level:Benign
                        End!If orp:order_number <> ''
                    end !loop
                    access:ordparts.restorefile(save_orp_id)
  
                    If tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock > 1
                        If Access:STOCKMIN.PrimeRecord() = Level:Benign
                            smin:Location            = sto:Location
                            smin:PartNumber          = sto:Part_Number
                            smin:Description         = sto:Description
                            smin:Supplier            = sto:Supplier
                            smin:Usage               = tmp:Usage
                            smin:MinLevel            = tmp:MinimumLevel
                            smin:QuantityOnOrder     = (tmp:QuantityOnOrder + tmp:QuantityToOrder)
                            smin:QuantityInStock     = sto:Quantity_Stock
                            smin:QuantityRequired    = tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock
                            smin:PartRefNumber       = sto:Ref_number
                            smin:QuantityJobsPending = ''
  
                            If Access:STOCKMIN.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:STOCKMIN.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:STOCKMIN.TryInsert() = Level:Benign
                        End !If Access:STOCKMIN.PrimeRecord() = Level:Benign
                    End !If tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock > 1
  
                End !If def:SummaryOrders
  
            End !Loop
            Access:STOCK.RestoreFile(Save_sto_ID)
  
            Prog.ProgressFinish()
  
          Of 1 ! No Button
  
            Post(Event:CloseWindow)
      End ! Case Missive
  ELSE
  
  End
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(defstock)
  access:defstock.next()
  If dst:use_site_location = 'YES'
      Tmp:Location = dst:site_location
  Else
      Tmp:Location = ''
  End
  If glo:WebJob
      If COMMAND('WebAcc')
          Clarionet:Global.Param2 = Command('WebAcc')
      End !If COMMAND('WebAcc')
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          Tmp:Location   = tra:SiteLocation
          DISABLE(?Tmp:Location)
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = LevelBenign
  End !glo:WebJob
  !brw1.resetsort(1)
  Display()
  IF tmp:UseScheduledMinStock = TRUE
    Date_Run = GETINI('STOCK','ScheduledFileDate',,CLIP(PATH()) & '\SB2KDEF.INI')
    Time_Run = GETINI('STOCK','ScheduledFileTime',,CLIP(PATH()) & '\SB2KDEF.INI')
    ?String1{PROP:Text} = 'LAST RUN :'&CLIP(FORMAT(Date_Run,@d6))&' '&CLIP(FORMAT(Time_Run,@T1))
    DISPLAY()
  ELSE
    ?String1{PROP:Text} = ''
    DISPLAY()
  END
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,smin:LocationPartNoKey)
  BRW1.AddRange(smin:Location)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?smin:PartNumber,smin:PartNumber,1,BRW1)
  BRW1.AddSortOrder(,smin:LocationDescriptionKey)
  BRW1.AddRange(smin:Location)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?smin:Description,smin:Description,1,BRW1)
  BRW1.AddField(smin:Description,BRW1.Q.smin:Description)
  BRW1.AddField(smin:PartNumber,BRW1.Q.smin:PartNumber)
  BRW1.AddField(smin:Supplier,BRW1.Q.smin:Supplier)
  BRW1.AddField(smin:MinLevel,BRW1.Q.smin:MinLevel)
  BRW1.AddField(smin:QuantityOnOrder,BRW1.Q.smin:QuantityOnOrder)
  BRW1.AddField(smin:QuantityInStock,BRW1.Q.smin:QuantityInStock)
  BRW1.AddField(smin:QuantityRequired,BRW1.Q.smin:QuantityRequired)
  BRW1.AddField(smin:RecordNumber,BRW1.Q.smin:RecordNumber)
  BRW1.AddField(smin:Location,BRW1.Q.smin:Location)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB8.Init(tmp:Location,?tmp:Location,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(loc:Location_Key)
  FDCB8.AddField(loc:Location,FDCB8.Q.loc:Location)
  FDCB8.AddField(loc:RecordNumber,FDCB8.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  brw1.InsertControl = 0
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:STOCKMIN.Close
    Relate:TRADEACC_ALIAS.Close
  IF tmp:UseScheduledMinStock = FALSE
    Remove(STOCKMIN)
    If Error()
        Access:STOCKMIN.Close()
        Remove(STOCKMIN)
    End !Error
  End
  END
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  glo:DeleteStock = 0
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMinimumStock
    ReturnValue = GlobalResponse
  END
  If glo:DeleteStock = 1
      Delete(STOCKMIN)
  End !If glo:DeleteStock = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?ProcessAllItems
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAllItems, Accepted)
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      Case Missive('Process All Items.'&|
        '<13,10>'&|
        '<13,10>If you continue all the parts displayed in the Minimum Stock Browse will be made ready for ordering. Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Setcursor(Cursor:Wait)
              Save_smin_ID = Access:STOCKMIN.SaveFile()
              Access:STOCKMIN.ClearKey(smin:LocationPartNoKey)
              smin:Location   = tmp:Location
              Set(smin:LocationPartNoKey,smin:LocationPartNoKey)
              Loop
                  If Access:STOCKMIN.NEXT()
                     Break
                  End !If
                  If smin:Location   <> tmp:Location      |
                      Then Break.  ! End If
                  CountRecords# += 1
              End !Loop
              Access:STOCKMIN.RestoreFile(Save_smin_ID)
              Setcursor()
      
              thiswindow.reset(1)
      
              Prog.ProgressSetup(CountRecords#)
      
              Save_smin_ID = Access:STOCKMIN.SaveFile()
              Access:STOCKMIN.ClearKey(smin:LocationPartNoKey)
              smin:Location   = tmp:Location
              Set(smin:LocationPartNoKey,smin:LocationPartNoKey)
              Loop
                  If Access:STOCKMIN.NEXT()
                     Break
                  End !If
                  If smin:Location   <> tmp:Location      |
                      Then Break.  ! End If
      
                  If Prog.InsideLoop()
                      Break
                  End ! If Prog.InsideLoop()
      
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = smin:PartRefNumber
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
      
                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  Case def:SummaryOrders
                      Of 0 !Old Fashioned ORders
                          If access:ordpend.primerecord() = Level:Benign
                              ope:part_ref_number = sto:ref_number
                              ope:part_type       = 'STO'
                              ope:supplier        = smin:Supplier
                              ope:part_number     = sto:part_number
                              ope:Description     = sto:description
                              ope:job_number      = ''
                              ope:quantity        = smin:QuantityRequired
                              access:ordpend.insert()
                          End !If access:ordpend.primerecord() = Level:Benign
      
                      Of 1 !New ORders
                          MakePartsRequest(smin:Supplier,sto:Part_Number,sto:Description,smin:QuantityRequired)
                          sto:QuantityRequested += smin:QuantityRequired
                          Access:STOCK.Update()
      
                  End!Case def:SummaryOrders
                  Delete(STOCKMIN)
              End !Loop
              Access:STOCKMIN.RestoreFile(Save_smin_ID)
      
              Prog.ProgressFinish()
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAllItems, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020078'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020078'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020078'&'0')
      ***
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Post(Event:CloseWindow)
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?smin:Description
      Select(?Browse:1)
    OF ?smin:PartNumber
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F9Key
              Post(Event:Accepted,?Change)
          Of F10Key
              Post(Event:Accepted,?ProcessAllItems)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  !TB12448 second part hide franchises - JC - 12/07/12
  
      access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
      tra_ali:SiteLocation = loc:Location
      if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
          !eh?? - let it show
      ELSE
          if tra_ali:Stop_Account = 'YES' then return(record:filtered).
      END
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 500
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
RRCPartsReceive      PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
tmp:JobNumber        LONG
tmp:Location         STRING(30)
JobsQueue            QUEUE,PRE(jobque)
JobNumber            LONG
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Free(JobsQueue)
    Clear(JobsQueue)

    Found# = 0
    Loop Until Found# = 0
        Save_par_ID = Access:PARTS.SaveFile()
        Access:PARTS.ClearKey(par:WebOrderKey)
        par:WebOrder    = 1
        Set(par:WebOrderKey,par:WebOrderKey)
        Loop
            If Access:PARTS.NEXT()
               Break
            End !If
            If par:WebOrder    <> 1      |
                Then Break.  ! End If

            !See if the job for this part has already
            !been checked and failed
            Sort(JobsQueue,jobque:JobNumber)
            jobque:JobNumber    = par:Ref_Number
            Get(JobsQueue,jobque:JobNumber)
            If ~Error()
                Cycle
            End !If ~Error()

            If par:Quantity < sto:Quantity_Stock
                tmp:JobNumber   = par:Ref_Number
                Found# = 1
                Break
            End !If par:Quantity < sto:Quantity_Stock
        End !Loop
        Access:PARTS.RestoreFile(Save_par_ID)

        Save_wpr_ID = Access:WARPARTS.SaveFile()
        Access:WARPARTS.ClearKey(wpr:WebOrderKey)
        wpr:WebOrder    = 1
        Set(wpr:WebOrderKey,wpr:WebOrderKey)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:WebOrder    <> 1      |
                Then Break.  ! End If

            !See if the job for this part has already
            !been checked and failed
            Sort(JobsQueue,jobque:JobNumber)
            jobque:JobNumber    = wpr:Ref_Number
            Get(JobsQueue,jobque:JobNumber)
            If ~Error()
                Cycle
            End !If ~Error()

            If wpr:Quantity < sto:Quantity_Stock
                tmp:JobNumber   = wpr:Ref_Number
                Found# = 1
                Break
            End !If par:Quantity < sto:Quantity_Stock
        End !Loop
        Access:WARPARTS.RestoreFile(Save_wpr_ID)


        If Found#
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = tmp:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                PendingParts# = 0
                Failed# = 0

                If job:Chargeable_Job = 'YES'
                    Save_par_ID = Access:PARTS.SaveFile()
                    Access:PARTS.ClearKey(par:Part_Number_Key)
                    par:Ref_Number  = job:Ref_Number
                    Set(par:Part_Number_Key,par:Part_Number_Key)
                    Loop
                        If Access:PARTS.NEXT()
                           Break
                        End !If
                        If par:Ref_Number  <> job:Ref_Number      |
                            Then Break.  ! End If
                        If par:WebOrder
                            PendingParts# += 1
                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = tmp:Location
                            sto:Part_Number = par:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Found
                                If sto:Quantity_Stock < par:Quantity
                                    Failed# += 1
                                    Break
                                End !If sto:Quantity_Stock > par:Quantity
                            Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Error
                            End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                        End !If par:WebOrder
                    End !Loop
                    Access:PARTS.RestoreFile(Save_par_ID)

                End !If job:Chargeable_Job = 'YES'

                If job:Warranty_Job = 'YES'
                    Save_wpr_ID = Access:WARPARTS.SaveFile()
                    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                    wpr:Ref_Number  = job:Ref_Number
                    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    Loop
                        If Access:WARPARTS.NEXT()
                           Break
                        End !If
                        If wpr:Ref_Number  <> job:Ref_Number      |
                            Then Break.  ! End If
                        If wpr:WebOrder
                            PendingParts# += 1
                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = tmp:Location
                            sto:Part_Number = wpr:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Found
                                If sto:Quantity_Stock < wpr:Quantity
                                    Failed# += 1
                                    Break
                                End !If sto:Quantity_Stock > par:Quantity
                            Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Error
                            End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                        End !If par:WebOrder

                    End !Loop
                    Access:WARPARTS.RestoreFile(Save_wpr_ID)
                End !If job:Warranty_Job = 'YES'

                If PendingParts#
                    If Failed#
                        !Add job number to a queue, so it can be ignored until next time
                        jobque:JobNumber    = job:Ref_Number
                        Get(JobsQueue,jobque:JobNumber)
                        If Error()
                            jobque:JobNumber    = job:Ref_Number
                            Add(JobsQueue)
                        End !If Error()
                    Else !If Failed#
                        !Ok, so there should be enough in stock, so go through the parts
                        !again and receive them
                        If job:Chargeable_Job = 'YES'
                            Save_par_ID = Access:PARTS.SaveFile()
                            Access:PARTS.ClearKey(par:Part_Number_Key)
                            par:Ref_Number  = job:Ref_Number
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.NEXT()
                                   Break
                                End !If
                                If par:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
                                If par:WebOrder
                                    Access:STOCK.Clearkey(sto:Location_Key)
                                    sto:Location    = tmp:Location
                                    sto:Part_Number = par:Part_Number
                                    If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Found
                                        sto:Quantity_Stock -= par:Quantity
                                        If sto:Quantity_stock <= 0
                                            sto:Quantity_Stock = 0
                                        End !If sto:Quantity_stock <= 0
                                        If Access:STOCK.Update() = Level:Benign
                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'DEC', | ! Transaction_Type
                                                                 '', | ! Depatch_Note_Number
                                                                 par:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 par:Quantity, | ! Quantity
                                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                                 par:Sale_Cost, | ! Sale_Cost
                                                                 par:Retail_Cost, | ! Retail_Cost
                                                                 'STOCK DECREMENTED', | ! Notes
                                                                 'WEB ORDER') ! Information
                                                ! Added OK
                                            Else ! AddToStockHistory
                                                ! Error
                                            End ! AddToStockHistory

                                            par:Date_Received = Today()
                                            par:WebOrder = 0
                                            Access:PARTS.Update()
                                        End !If Access:STOCK.Update() = Level:Benign
                                    Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Error
                                    End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                End !If par:WebOrder
                            End !Loop
                            Access:PARTS.RestoreFile(Save_par_ID)
                        End !If job:Chargeable_Job = 'YES'

                        If job:Warranty_Job = 'YES'
                            Save_wpr_ID = Access:WARPARTS.SaveFile()
                            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                            wpr:Ref_Number  = job:Ref_Number
                            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                            Loop
                                If Access:WARPARTS.NEXT()
                                   Break
                                End !If
                                If wpr:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
                                If wpr:WebOrder
                                    Access:STOCK.Clearkey(sto:Location_Key)
                                    sto:Location    = tmp:Location
                                    sto:Part_Number = wpr:Part_Number
                                    If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Found
                                        sto:Quantity_Stock -= wpr:Quantity
                                        If sto:Quantity_stock <= 0
                                            sto:Quantity_Stock = 0
                                        End !If sto:Quantity_stock <= 0
                                        If Access:STOCK.Update() = Level:Benign
                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'DEC', | ! Transaction_Type
                                                                 '', | ! Depatch_Note_Number
                                                                 wpr:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 wpr:Quantity, | ! Quantity
                                                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                                                 wpr:Sale_Cost, | ! Sale_Cost
                                                                 wpr:Retail_Cost, | ! Retail_Cost
                                                                 'STOCK DECREMENTED', | ! Notes
                                                                 'WEB ORDER') ! Information
                                                ! Added OK

                                            Else ! AddToStockHistory
                                                ! Error
                                            End ! AddToStockHistory

                                            wpr:Date_Received = Today()
                                            wpr:WebOrder = 0
                                            Access:WARPARTS.Update()
                                        End !If Access:STOCK.Update() = Level:Benign
                                    Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Error
                                    End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                End !If par:WebOrder
                            End !Loop
                            Access:PARTS.RestoreFile(Save_par_ID)

                        End !If job:Chargeable_Job = 'YES'
                    End !If Failed#
                End !If PendingParts#

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        End !If Found#

    End !Loop Until Found# = 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
BrowseStockRequisitions PROCEDURE                     !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_ord_id          USHORT,AUTO
sav:file_name        STRING(255)
FilesOpened          BYTE
tag_temp             STRING(1)
order_type_temp      STRING(8)
cost_temp            REAL
save_wpr_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
save_par_id          USHORT,AUTO
type_temp            STRING(3)
no_temp              STRING('NO')
yes_temp             STRING('YES')
tmp:TotalCost        REAL
tmp:False            BYTE(0)
tmp:True             BYTE(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
DisplayType          STRING(1)
AppFound             LONG
NotAppFound          LONG
BRW1::View:Browse    VIEW(ORDPEND)
                       PROJECT(ope:Description)
                       PROJECT(ope:Part_Number)
                       PROJECT(ope:Quantity)
                       PROJECT(ope:Ref_Number)
                       PROJECT(ope:StockReqNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ope:Description        LIKE(ope:Description)          !List box control field - type derived from field
ope:Part_Number        LIKE(ope:Part_Number)          !List box control field - type derived from field
ope:Quantity           LIKE(ope:Quantity)             !List box control field - type derived from field
order_type_temp        LIKE(order_type_temp)          !List box control field - type derived from local data
type_temp              LIKE(type_temp)                !List box control field - type derived from local data
ope:Ref_Number         LIKE(ope:Ref_Number)           !List box control field - type derived from field
ope:StockReqNumber     LIKE(ope:StockReqNumber)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(REQUISIT)
                       PROJECT(req:RecordNumber)
                       PROJECT(req:Supplier)
                       PROJECT(req:Approved)
                       PROJECT(req:Ordered)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
req:RecordNumber       LIKE(req:RecordNumber)         !List box control field - type derived from field
req:RecordNumber_NormalFG LONG                        !Normal forground color
req:RecordNumber_NormalBG LONG                        !Normal background color
req:RecordNumber_SelectedFG LONG                      !Selected forground color
req:RecordNumber_SelectedBG LONG                      !Selected background color
req:Supplier           LIKE(req:Supplier)             !List box control field - type derived from field
req:Supplier_NormalFG  LONG                           !Normal forground color
req:Supplier_NormalBG  LONG                           !Normal background color
req:Supplier_SelectedFG LONG                          !Selected forground color
req:Supplier_SelectedBG LONG                          !Selected background color
req:Approved           LIKE(req:Approved)             !List box control field - type derived from field
req:Ordered            LIKE(req:Ordered)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Pending Orders'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,216,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('By Requisition Number'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION,AT(69,69,199,19),USE(DisplayType),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             RADIO('Approved'),AT(77,75),USE(?DisplayType:Radio1),VALUE('Y')
                             RADIO('Unapproved'),AT(140,75),USE(?DisplayType:Radio2),VALUE('N')
                             RADIO('All'),AT(216,75),USE(?DisplayType:Radio3),VALUE('A')
                           END
                           BUTTON,AT(212,86),USE(?PrintStockRequisition),TRN,FLAT,LEFT,ICON('repreqp.jpg')
                           ENTRY(@s8),AT(68,96,64,10),USE(req:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Stock Requisition Number'),TIP('Stock Requisition Number'),UPR
                           BUTTON,AT(76,336),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(140,336),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(204,336),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       LIST,AT(292,114,248,214),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('106L(2)|FM~Description~@s30@92L(2)|FM~Part Number~@s30@143L(2)|FM~Quantity~@p<<<<<<' &|
   '<<<<<<<<#p@39L(2)|FM~Job No~@s8@26L(2)|FM~Type~@s3@36R(2)F~Ref Number~@p<<<<<<<<<<<<<<<<#p@/'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Pending Orders'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(284,54,332,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Parts Awaiting Order'),USE(?Tab:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(288,74,256,258),USE(?Sheet3),SPREAD
                             TAB('By Description'),USE(?Tab6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(292,96,124,10),USE(ope:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             END
                             TAB('By Part Number'),USE(?Tab4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(292,96,124,10),USE(ope:Part_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             END
                           END
                           BUTTON,AT(546,118),USE(?ButtonAppTagged),TRN,FLAT,ICON('AppTagp.jpg')
                           BUTTON,AT(546,150),USE(?ButtonRejTagged),FLAT,ICON('RejTagp.jpg')
                           BUTTON,AT(356,336),USE(?Amend_Details),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(424,336),USE(?Delete_Part),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(288,336),USE(?Valuate_Order),TRN,FLAT,LEFT,ICON('valordp.jpg')
                         END
                       END
                       BUTTON('&Rev tags'),AT(20,102,20,14),USE(?DASREVTAG),HIDE
                       BUTTON,AT(548,86),USE(?Create_Order),TRN,FLAT,LEFT,ICON('makordp.jpg')
                       BUTTON('sho&W tags'),AT(24,152,16,14),USE(?DASSHOWTAG),HIDE
                       LIST,AT(68,114,208,214),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@53L(2)|M*~Requisition No~@s8@120L(2)|M*~Supplier~@s30@0L(2)|M@s1@'),FROM(Queue:Browse)
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(140,366),USE(?ButtonKey),FLAT,ICON('keyp.jpg')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet3) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  EntryLocatorClass                !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   glo:Queue.Pointer = req:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = req:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = req:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
  ! Before Embed Point: %DasTagAfterTagAll) DESC(Tagging After Tag All) ARG(7)
  !TB12981 - need not to tag those not in correct status
  if DisplayType <> 'A' then
      Loop x# = 1 to records(Glo:queue)
          get(glo:Queue,x#)
          Access:Requisit.clearkey(req:RecordNumberKey)
          req:RecordNumber = glo:Queue.pointer
          if access:Requisit.fetch(req:RecordNumberKey)
              !error??
          ELSE
              if req:Approved <> DisplayType then
                  glo:Queue.pointer = 0
                  put(glo:Queue)
              END
          END !if requisit.fetch
      END !loop though records in queue to see if they match current status
  END !if not type = All
  ! After Embed Point: %DasTagAfterTagAll) DESC(Tagging After Tag All) ARG(7)
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::7:QUEUE = glo:Queue
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer = req:RecordNumber
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = req:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
PendAudCheck    Routine

    !need to find an ordpend with a ope:PrevStoReqNo > 0
    Access:ordpend.clearkey(ope:ReqDescriptionKey)  !has ope:StockReqNumber as first parameter
    ope:StockReqNumber = req:RecordNumber
    set(ope:ReqDescriptionKey,ope:ReqDescriptionKey)
    Loop

        if access:Ordpend.next() then break.
        if ope:StockReqNumber <> req:RecordNumber then break.

        !update
        if ope:PrevStoReqNo > 0 then
            !got one
            Break
        END
    END !loop to find ope:StockReqNumber

    if ope:PrevStoReqNo > 0 then
        Loop
            Access:Pendaud.clearkey(PED:KeyReqRecordNumber)
            PED:ReqRecordNumber = ope:PrevStoReqNo
            set(PED:KeyReqRecordNumber,PED:KeyReqRecordNumber)
            !any remaining
            if access:Pendaud.next() then break.
            if PED:ReqRecordNumber <> ope:PrevStoReqNo then break.
            !update
            PED:ReqRecordNumber = ope:StockReqNumber
            Access:Pendaud.update()
        END !loop through pendaud
    END !if ope:PrevStoReqNo found

    EXIT
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020093'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseStockRequisitions')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DisplayType:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CURRENCY.Open
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:PENDAUD.Open
  Relate:REQUISIT.Open
  Relate:RETSTOCK.Open
  Relate:STATUS.Open
  Access:SUPPLIER.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:JOBSTAGE.UseFile
  Access:STOMODEL.UseFile
  Access:MODELNUM.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORDPEND,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:REQUISIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  DisplayType = 'A'     !TB12981 -Force approval of requestes before they become orders - JC 11/02/13
  
  !look this up once on entry - rather than ten times during the process
  access:users.clearkey(use:password_key)
  use:password    =glo:password
  access:users.fetch(use:password_key)
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,ope:ReqPartNumber)
  BRW1.AddRange(ope:StockReqNumber,req:RecordNumber)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?ope:Part_Number,ope:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,ope:ReqDescriptionKey)
  BRW1.AddRange(ope:StockReqNumber,req:RecordNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ope:Description,ope:Description,1,BRW1)
  BIND('order_type_temp',order_type_temp)
  BIND('type_temp',type_temp)
  BRW1.AddField(ope:Description,BRW1.Q.ope:Description)
  BRW1.AddField(ope:Part_Number,BRW1.Q.ope:Part_Number)
  BRW1.AddField(ope:Quantity,BRW1.Q.ope:Quantity)
  BRW1.AddField(order_type_temp,BRW1.Q.order_type_temp)
  BRW1.AddField(type_temp,BRW1.Q.type_temp)
  BRW1.AddField(ope:Ref_Number,BRW1.Q.ope:Ref_Number)
  BRW1.AddField(ope:StockReqNumber,BRW1.Q.ope:StockReqNumber)
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,req:OrderedNumberKey)
  BRW6.AddRange(req:Ordered,tmp:False)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?req:RecordNumber,req:RecordNumber,1,BRW6)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tag_temp,BRW6.Q.tag_temp)
  BRW6.AddField(req:RecordNumber,BRW6.Q.req:RecordNumber)
  BRW6.AddField(req:Supplier,BRW6.Q.req:Supplier)
  BRW6.AddField(req:Approved,BRW6.Q.req:Approved)
  BRW6.AddField(req:Ordered,BRW6.Q.req:Ordered)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab6{PROP:TEXT} = 'By Description'
    ?Tab4{PROP:TEXT} = 'By Part Number'
    ?Browse:1{PROP:FORMAT} ='106L(2)|FM~Description~@s30@#1#92L(2)|FM~Part Number~@s30@#2#143L(2)|FM~Quantity~@p<<<<<<<#p@#3#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CURRENCY.Close
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:PENDAUD.Close
    Relate:REQUISIT.Close
    Relate:RETSTOCK.Close
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonAppTagged
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonAppTagged, Accepted)
      !TB12981 -Force approval of requestes before they become orders - JC 11/02/13
      
          If ~Records(glo:Queue)
              Case Missive('You have not tagged any Order.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
      
          Else!If ~Records(glo:Queue)
      
              if CheckApproveOrdersPermitted() then
                  if Missive('Authority accepted||Are you sure you want to Approve Orders for all the Tagged Requisitions?||NOTE: This procedure cannot be reversed','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes') = 2 then
                      Loop x# = 1 To Records(glo:Queue)
                          Get(glo:Queue,x#)
      
                          if glo:Queue.pointer = 0 then cycle.
      
                          Access:REQUISIT.ClearKey(req:RecordNumberKey)
                          req:RecordNumber = glo:Pointer
                          If Access:REQUISIT.TryFetch(req:RecordNumberKey) = Level:Benign
                              !Found
                              req:Approved = 'Y'
                              Access:REQUISIT.update()
      
                              !add audit trail
                              Access:Pendaud.primerecord()
                              PED:ReqRecordNumber = req:RecordNumber
                              PED:Action          = 'ACCEPTED'
                              PED:ActionDate      = today()
                              PED:ActionTime      = clock()
                              PED:UserName        = clip(use:Forename)&' '&clip(use:Surname)
                              PED:AuthorisingUser = PendingAuthorisingUser
                              Access:pendaud.update()
      
                              !whilst here - check for previous audit that need updateing
                              Do PendAudCheck
      
      
                          END !if fetch worked
                      END !loop through glo:Queue
                  END !if missive =2
              END !if CheckApproveOrdersPermitted
          END !if there are records in the queue
      
      
          !untag all after approval
          DO DASBRW::7:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonAppTagged, Accepted)
    OF ?ButtonKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonKey, Accepted)
      !TB12981 -Force approval of requestes before they become orders - JC 11/02/13
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonKey, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DisplayType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DisplayType, Accepted)
      !TB12981 -Force approval of requestes before they become orders - JC 11/02/13
      BRW6.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DisplayType, Accepted)
    OF ?PrintStockRequisition
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintStockRequisition, Accepted)
          Access:REQUISIT.Clearkey(req:RecordNumberKey)
          req:RecordNumber    = brw6.q.req:RecordNumber
          If Access:REQUISIT.Tryfetch(req:RecordNumberKey) = Level:Benign
              !Found
              Clear(glo:Q_PartsOrder)
              Free(glo:Q_PartsOrder)
              Save_ope_ID = Access:ORDPEND.SaveFile()
              Access:ORDPEND.ClearKey(ope:ReqPartNumber)
              ope:StockReqNumber = req:RecordNumber
              Set(ope:ReqPartNumber,ope:ReqPartNumber)
              Loop
                  If Access:ORDPEND.NEXT()
                     Break
                  End !If
                  If ope:StockReqNumber <> req:RecordNumber      |
                      Then Break.  ! End If
                  Sort(glo:Q_PartsOrder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
                  glo:q_order_number  = req:RecordNumber
                  glo:q_part_number   = ope:part_number
                  glo:q_description   = ope:description
                  glo:q_supplier      = req:supplier
      
                  GetPendingCosts(ope:Part_Type,glo:q_Purchase_Cost,glo:q_Sale_Cost,glo:q_Supplier)
      
                  Get(glo:Q_PartsOrder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
                  If error()
                      glo:q_order_number  = req:RecordNumber
                      glo:q_part_number   = ope:part_number
                      glo:q_description   = ope:description
                      glo:q_supplier      = req:supplier
                      GetPendingCosts(ope:Part_Type,glo:q_Purchase_Cost,glo:q_Sale_Cost,glo:q_Supplier)
                      glo:q_quantity      = ope:quantity
                      Add(glo:Q_PartsOrder)
                  Else!If error()
                      glo:q_quantity      += ope:quantity
                      Put(glo:Q_PartsOrder)
                  End!If error()
      
              End !Loop
              Access:ORDPEND.RestoreFile(Save_ope_ID)
      
              StockRequisition()
      
          Else ! If Access:REQUISIT.Tryfetch(req:RecordNumberKey) = Level:Benign
              !Error
          End !If Access:REQUISIT.Tryfetch(req:RecordNumberKey) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintStockRequisition, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020093'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020093'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020093'&'0')
      ***
    OF ?ButtonRejTagged
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonRejTagged, Accepted)
      !TB12981 -Force approval of requestes before they become orders - JC 11/02/13
      
          If ~Records(glo:Queue)
              Case Missive('You have not tagged any Order.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
      
          Else!If ~Records(glo:Queue)
      
              if CheckApproveOrdersPermitted() then
      
                  if Missive('Authority accepted||Are you sure you want to REJECT Orders for all the Tagged Requisitions?||NOTE: This procedure cannot be undone.','ServiceBase 3g',|
                                 'mquest.jpg','/No|\Yes') = 2 then
                      Loop x# = 1 To Records(glo:Queue)
      
                          Get(glo:Queue,x#)
      
                          if glo:Queue.pointer = 0 then cycle.
      
                          Access:REQUISIT.ClearKey(req:RecordNumberKey)
                          req:RecordNumber = glo:Pointer
                          If Access:REQUISIT.TryFetch(req:RecordNumberKey) = Level:Benign
                              !Found
                              req:Approved = 'R'
                              Access:REQUISIT.update()
      
                              !add audit trail
                              Access:Pendaud.primerecord()
                              PED:ReqRecordNumber = req:RecordNumber
                              PED:Action          = 'REJECTED'
                              PED:ActionDate      = today()
                              PED:ActionTime      = clock()
                              PED:UserName        = clip(use:Forename)&' '&clip(use:Surname)
                              PED:AuthorisingUser = PendingAuthorisingUser
                              Access:pendaud.update()
      
                              !whilst here - check for previous audit that need updateing
                              Do PendAudCheck
      
                              !now make it "go back to previous screen"
                              Loop
                                  !set key inside loop because I am going to change ope:StockReqNumber
                                  Access:ordpend.clearkey(ope:ReqDescriptionKey)  !has ope:StockReqNumber as first parameter
                                  ope:StockReqNumber = req:RecordNumber
                                  set(ope:ReqDescriptionKey,ope:ReqDescriptionKey)
                                  !any left?
                                  if access:Ordpend.next() then break.
                                  if ope:StockReqNumber <> req:RecordNumber then break.
                                  !update
                                  ope:PrevStoReqNo = req:RecordNumber  !save this req number so I can update pendaud later
                                  ope:StockReqNumber = 0
                                  Access:Ordpend.update()
                              END !loop to find ope:StockReqNumber
      
      
                          END !if fetch worked
                      END !loop through glo:Queue
                  END !if missive =2
              END !if CheckApproveOrdersPermitted() then
          END !if there are records in the queue
      
          DO DASBRW::7:DASUNTAGALL
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonRejTagged, Accepted)
    OF ?Amend_Details
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Amend_Details, Accepted)
      thiswindow.reset(1)
      glo:select1  = ope:ref_number
      Amend_Pending_Part
      glo:select1 = ''
      
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Amend_Details, Accepted)
    OF ?Delete_Part
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part, Accepted)
    Case Missive('Are you sure you want to remove this part from the Order Generation?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
          Brw1.UpdateViewRecord()
          Case ope:part_type
              Of 'JOB'
                  setcursor(cursor:wait)
                  save_par_id = access:parts.savefile()
                  access:parts.clearkey(par:PendingRefNoKey)
                  par:ref_number         = ope:job_number
                  par:pending_ref_number = ope:ref_number
                  set(par:PendingRefNoKey,par:PendingRefNoKey)
                  loop
                      if access:parts.next()
                         break
                      end !if
                      if par:ref_number         <> ope:job_number      |
                      or par:pending_ref_number <> ope:ref_number      |
                          then break.  ! end if
                      If par:part_number  = ope:part_number
                          Delete(parts)
                      End!If par:part_number  = ope:part_number
                  end !loop
                  access:parts.restorefile(save_par_id)
                  setcursor()
              Of 'WAR'
                  setcursor(cursor:wait)
                  save_wpr_id = access:warparts.savefile()
                  access:warparts.clearkey(wpr:PendingRefNoKey)
                  wpr:ref_number         = ope:job_number
                  wpr:pending_ref_number = ope:ref_number
                  set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
                  loop
                      if access:warparts.next()
                         break
                      end !if
                      if wpr:ref_number         <> ope:job_number      |
                      or wpr:pending_ref_number <> ope:ref_number      |
                          then break.  ! end if
                      If wpr:part_number  = ope:part_number
                          Delete(warparts)
                      End!If wpr:part_number  = ope:part_number
                  end !loop
                  access:warparts.restorefile(save_wpr_id)
                  setcursor()
              Of 'STO'
              Of 'RET'
!                  setcursor(cursor:wait)
!                  save_wpr_id = access:warparts.savefile()
!                  access:warparts.clearkey(wpr:PendingRefNoKey)
!                  wpr:ref_number         = ope:job_number
!                  wpr:pending_ref_number = ope:ref_number
!                  set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
!                  loop
!                      if access:warparts.next()
!                         break
!                      end !if
!                      if wpr:ref_number         <> ope:job_number      |
!                      or wpr:pending_ref_number <> ope:ref_number      |
!                          then break.  ! end if
!                      If wpr:part_number  = ope:part_number
!                          Delete(warparts)
!                      End!If wpr:part_number  = ope:part_number
!                  end !loop
!                  access:warparts.restorefile(save_wpr_id)
!                  setcursor()

          End!Case ope:ref_number
          Delete(ordpend)
          If error()
              stop(error())
          End!If error()
        Of 1 ! No Button
    End ! Case Missive
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part, Accepted)
    OF ?Valuate_Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Order, Accepted)
      cost_Temp = 0
      setcursor(cursor:Wait)
      Save_ope_ID = Access:ORDPEND.SaveFile()
      Access:ORDPEND.ClearKey(ope:ReqPartNumber)
      ope:StockReqNumber = req:RecordNumber
      Set(ope:ReqPartNumber,ope:ReqPartNumber)
      Loop
          If Access:ORDPEND.NEXT()
             Break
          End !If
          If ope:StockReqNumber <> req:RecordNumber      |
              Then Break.  ! End If
      
          Case ope:Part_Type
              Of 'JOB'
                  save_Par_Id = access:Parts.savefile()
                  access:Parts.clearkey(par:PendingRefNoKey)
                  par:Ref_Number         = ope:Job_Number
                  par:Pending_Ref_Number = ope:Ref_Number
                  set(par:PendingRefNoKey,par:PendingRefNoKey)
                  loop
                      if access:Parts.next()
                         break
                      end !if
                      if par:Ref_Number         <> ope:Job_Number      |
                      or par:Pending_Ref_Number <> ope:Ref_Number      |
                          then break.  ! end if
                      If par:Part_Number  = ope:Part_Number
                          cost_Temp   += (par:Purchase_Cost * ope:Quantity)
                          Break
                      End!If par:Part_Number  = ope:Part_Number
                  end !loop
                  access:Parts.restorefile(save_Par_Id)
              Of 'WAR'
                  save_Wpr_Id = access:Warparts.savefile()
                  access:Warparts.clearkey(wpr:PendingRefNoKey)
                  wpr:Ref_Number         = ope:Job_Number
                  wpr:Pending_Ref_Number = ope:Ref_Number
                  set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
                  loop
                      if access:Warparts.next()
                         break
                      end !if
                      if wpr:Ref_Number         <> ope:Job_Number      |
                      or wpr:Pending_Ref_Number <> ope:Ref_Number      |
                          then break.  ! end if
                      If wpr:Part_Number = ope:Part_Number
                          cost_Temp   += wpr:Purchase_Cost * ope:Quantity
                          Break
                      End!If wpr:Part_Number = ope:Part_Number
                  end !loop
                  access:Warparts.restorefile(save_Wpr_Id)
              Of 'STO'
                  access:Stock.clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number = ope:Part_Ref_Number
                  if access:Stock.Fetch(sto:Ref_Number_Key) = Level:Benign
                      cost_Temp   += sto:Purchase_Cost * ope:Quantity
                  End!if access:Stock.fetch(sto:Ref_Number_Key) = Level:Benign
          End!Case ope:Part_Type
      End !Loop
      Access:ORDPEND.RestoreFile(Save_ope_ID)
      setcursor()
      
      GetCurrencyConversion(req:Supplier,Cost_Temp,CurrCode",Amount$)
      If CurrCode" <> ''
          Case Missive('The value of this order is:|R ' & Format(Cost_Temp,@n14.2) & '.'&|
            '|' & Clip(CurrCode") & ' ' & Format(Amount$,@n14.2) & '.','ServiceBase 3g',|
                         'midea.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else ! CurrCode" <> ''
          Case Missive('The value of this order is R ' & Format(Cost_Temp,@n14.2) & '.','ServiceBase 3g',|
                         'midea.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      End ! CurrCode" <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Order, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Create_Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Order, Accepted)
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
      
          If ~Records(glo:Queue)
              Case Missive('You have not tagged any Supplier.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
      
          Else!If ~Records(glo:Queue)
      
              Case Missive('Are you sure you want to Generate Orders for all the Tagged Requisitions?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
      
                      !TB12981 - check list for un/approved and show relevant message - JC 11/02/13
                      AppFound = 0
                      NotAppFound = 0
                      Loop x# = 1 to records(glo:queue)
                          get(glo:queue,x#)
      
                          if glo:Queue.pointer = 0 then cycle.
      
                          Access:REQUISIT.ClearKey(req:RecordNumberKey)
                          req:RecordNumber = glo:Pointer
                          If Access:REQUISIT.Fetch(req:RecordNumberKey) = Level:Benign
                              !Found
                          End
                          
                          if req:Approved = 'Y' then
                              AppFound = 1
                          ELSE
                              NotAppFound = 1
                          END
                      END !loop to find if we have approved and unapproved in queue
                      Case AppFound + NotAppFound
                          of 2
                              Miss# = missive('|Approved and unapproved requisitions are tagged.|Orders will be generated for approved requisitions only','ServiceBase 3g',|
                                          'mwarn.jpg','OK')
                          of 1
                              if NotAppFound = 1 then
                                  Miss# = missive('Orders cannot be generated, only unapproved requisition/s are tagged.','ServiceBase 3g',|
                                          'mstop.jpg','OK')
                              END
                      END
                      !TB12981 - END check list for unapproved and show relevant message - JC 11/02/13
                      !Only proceed if there is something to do
                      if AppFound = 1
      
                          Clear(glo:Q_PartsOrder)
                          Free(glo:Q_PartsOrder)
                          recordspercycle     = 25
                          recordsprocessed    = 0
                          percentprogress     = 0
                          setcursor(cursor:wait)
                          open(progresswindow)
                          progress:thermometer    = 0
                          ?progress:pcttext{prop:text} = '0% Completed'
      
                          recordstoprocess    = Records(glo:Queue)
                          Loop x# = 1 To Records(glo:Queue)
                              Get(glo:Queue,x#)
      
                              if glo:Queue.pointer = 0 then cycle.
                              Do GetNextRecord2
      
                              Access:REQUISIT.ClearKey(req:RecordNumberKey)
                              req:RecordNumber = glo:Pointer
                              If Access:REQUISIT.TryFetch(req:RecordNumberKey) = Level:Benign
                                  !Found
      
                              Else!If Access:REQUISIT.TryFetch(req:RecordNumberKey) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                  Cycle
                              End!If Access:REQUISIT.TryFetch(req:RecordNumberKey) = Level:Benign  
      
      
                              !TB12981 - don't allow to make if the request is not approved - JC 11/02/13
                              if req:Approved <> 'Y' then
                                  cycle
                              END
      
      
                              get(orders,0)
                              if access:orders.primerecord() = level:benign
                                  ! Inserting (DBH 03/12/2007) # 9034 - If next order default is higher than auto number, then use that
                                  NextOrder# = GETINI('STOCK','NextOrderNumber',,Clip(Path()) & '\SB2KDEF.INI')
                                  If NextOrder# > 0 And ord:Order_Number < NextOrder#
                                      ord:Order_Number = NextOrder#
                                  End ! If NextOrder# > 0 And ord:Order_Number < NextOrder#
                                  ! End (DBH 03/12/2007) #9034
      
      
                                  ord:supplier     = req:Supplier
                                  ord:date         = Today()
                                  ord:printed      = 'NO'
                                  ord:all_received = 'NO'
                                  ord:user        = use:user_code
      
                                  !TB12981 - update pending audit trails to have the new order number
                                  Access:Pendaud.clearkey(PED:KeyReqRecordNumber)
                                  PED:ReqRecordNumber = req:RecordNumber
                                  set(PED:KeyReqRecordNumber,PED:KeyReqRecordNumber)
                                  loop
                                      if access:Pendaud.next() then break.
                                      if PED:ReqRecordNumber <> req:RecordNumber then break.
      
                                      PED:OrdOrder_Number = ord:Order_Number
                                      Access:Pendaud.update()
                                  END !loop through pendaud
      
                                  ! Add currency information - TrkBs: 5110 (DBH: 18-05-2005)
                                  Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                                  sup:Company_Name = req:Supplier
                                  If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                      !Found
                                      If sup:UseForeignCurrency
                                          Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
                                          cur:CurrencyCode = sup:CurrencyCode
                                          If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                              !Found
                                              ord:OrderedCurrency         = sup:CurrencyCode
                                              ord:OrderedDailyRate        = cur:DailyRate
                                              ord:OrderedDivideMultiply   = cur:DivideMultiply
                                              ! I don't think it's necessary to save this information there.. but you never know - TrkBs: 5110 (DBH: 18-05-2005)
                                          Else !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                              !Error
                                          End !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                      End ! If sup:UseForeignCurrency
                                  Else !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                      !Error
                                  End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                  ! End   - Add currency information - TrkBs: 5110 (DBH: 18-05-2005)
      
                                  ! Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
                                  If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
                                      ord:BatchRunNotPrinted      = False
                                  Else ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
                                      ord:BatchRunNotPrinted      = True
                                  End ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
                                  ! End   - Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
      
                                  access:orders.insert()
                              End!if access:orders.primerecord() = level:benign
      
                              Save_ope_ID = Access:ORDPEND.SaveFile()
                              Access:ORDPEND.ClearKey(ope:ReqPartNumber)
                              ope:StockReqNumber = req:RecordNumber
                              Set(ope:ReqPartNumber,ope:ReqPartNumber)
                              Loop
                                  If Access:ORDPEND.NEXT()
                                     Break
                                  End !If
                                  If ope:StockReqNumber <> req:RecordNumber      |
                                      Then Break.  ! End If
      
                                  get(ordparts,0)
                                  if access:ordparts.primerecord() = level:benign
                                      orp:order_number    = ord:order_number
                                      Case def:SummaryOrders
                                          Of 0! Old Way
      
                                              Case ope:part_type
                                                  Of 'JOB'
                                                      orp:job_number  = ope:job_number
                                                      access:parts.clearkey(par:PendingRefNoKey)
                                                      par:ref_number  = ope:job_number
                                                      par:pending_ref_number  = ope:ref_number
                                                      If access:parts.fetch(par:PendingRefNoKey)
                                                          Delete(ordpend)
                                                          Cycle
                                                      Else!If access:parts.fetch(par:PendingRefNoKey)
                                                          par:order_number    = ord:order_number
                                                          par:pending_ref_number  = ''
                                                          par:date_ordered    = Today()
                                                          par:order_part_number   = orp:record_number
                                                          access:parts.update()
      
                                                          orp:quantity    = ope:quantity
                                                          orp:part_number = par:part_number
                                                          orp:description = par:description
                                                          orp:purchase_cost   = par:purchase_cost
                                                          orp:sale_cost   = par:sale_cost
                                                          orp:Retail_Cost = par:Retail_Cost
                                                          orp:part_Type   = 'JOB'
                                                          orp:part_ref_number = par:part_ref_number
      
                                                          access:jobs.clearkey(job:ref_number_key)
                                                          job:ref_number  = par:ref_number
                                                          If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                              !call the status routine
                                                              GetStatus(335,1,'JOB') !spares ordered
      
                                                              access:jobs.update()
                                                          End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                      End!If access:parts.fetch(par:PendingRefNoKey)
      
                                                  Of 'WAR'
                                                      orp:job_number  = ope:job_number
                                                      access:warparts.clearkey(wpr:PendingRefNoKey)
                                                      wpr:ref_number  = ope:job_number
                                                      wpr:pending_ref_number  = ope:ref_number
                                                      If access:warparts.fetch(wpr:PendingRefNoKey)
                                                          Delete(ordpend)
                                                          Cycle
                                                      Else!If access:warparts.fetch(wpr:PendingRefNoKey)
                                                          wpr:order_number    = ord:order_number
                                                          wpr:pending_ref_number  = ''
                                                          wpr:date_ordered    = Today()
                                                          wpr:order_part_number   = orp:record_number
                                                          access:warparts.update()
      
                                                          orp:quantity    = ope:quantity
                                                          orp:part_number = wpr:part_number
                                                          orp:description = wpr:description
                                                          orp:purchase_cost   = wpr:purchase_cost
                                                          orp:sale_cost       = wpr:sale_cost
                                                          orp:retail_Cost     = wpr:Retail_Cost
                                                          orp:part_type       = 'WAR'
                                                          orp:part_ref_number = wpr:part_ref_number
      
                                                          access:jobs.clearkey(job:ref_number_key)
                                                          job:ref_number  = wpr:ref_number
                                                          If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                              !call the status routine
                                                              GetStatus(335,0,'JOB') !spares ordered
      
                                                              access:jobs.update()
                                                          End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                      End!If access:warparts.fetch(wpr:PendingRefNoKey)
                                                  Of 'STO'
                                                      !The new way of ordering retail parts is always a summary order
                                                      !It will therefore not have a Part_Ref_Number filled in.
                                                      !So need to try and get the costs from the Main Store
                                                      orp:Reason          = ope:Reason
                                                      orp:part_ref_number = ope:part_ref_number
                                                      orp:Quantity        = ope:Quantity
                                                      orp:Part_Number     = ope:Part_Number
                                                      orp:Description     = ope:Description
                                                      orp:Part_Type       = 'STO'
      
                                                      Access:STOCK.ClearKey(sto:Ref_Number_Key)
                                                      sto:Ref_Number  = ope:Part_Ref_Number
                                                      If Access:STOCK.Fetch(sto:Ref_Number_Key)
                                                          Access:STOCK.ClearKey(sto:Location_Key)
                                                          sto:Location    = MainStoreLocation()
                                                          sto:Part_Number = ope:Part_Number
                                                          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                              orp:Purchase_Cost      = sto:Purchase_Cost
                                                              orp:Sale_Cost          = sto:Sale_Cost
                                                              orp:Retail_Cost        = sto:Retail_Cost
                                                              IF (sto:ExchangeUnit = 'YES')
                                                                  ! #12127 Lookup the Exchange Replacement Value from the model number attached to the stock item (Bryan: 30/06/2011)
                                                                  GetExchangePartCosts(sto:Manufacturer,sto:ExchangeModelNumber,req:Supplier,orp:Purchase_Cost,orp:Sale_Cost,orp:FreeExchangeStock)
                                                              END ! IF (sto:ExchangeUnit = 'YES')
      
                                                          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                      Else!If access:stock.fetch(sto:ref_number_key)
                                                          orp:quantity    = ope:quantity
                                                          !If this is an exchange order, do not pick up
                                                          !the part number/description from the stock file.
                                                          !It will overwrite the model number in the description -  (DBH: 10-11-2003)
                                                          If sto:Part_Number <> 'EXCH'
                                                              orp:part_number = sto:part_number
                                                              orp:description = sto:description
                                                          End !If sto:Part_Number <> 'EXCH'
                                                          orp:purchase_cost   = sto:purchase_cost
                                                          orp:sale_cost   = sto:sale_cost
                                                          orp:Retail_Cost = sto:Retail_Cost
                                                          orp:part_type   = 'STO'
                                                          IF (sto:ExchangeUnit = 'YES')
                                                              ! #12127 Lookup the Exchange Replacement Value from the model number attached to the stock item (Bryan: 30/06/2011)
                                                              GetExchangePartCosts(sto:Manufacturer,sto:ExchangeModelNumber,req:Supplier,orp:Purchase_Cost,orp:Sale_Cost,orp:FreeExchangeStock)
                                                          END ! IF (sto:ExchangeUnit = 'YES')
      
                                                      End!If access:stock.fetch(sto:ref_number_key)
      
      
                                                  Of 'RET'
                                                      orp:job_number  = ope:job_number
                                                      access:retstock.clearkey(res:part_number_key)
                                                      res:ref_number  = ope:job_number
                                                      res:pending_ref_number = ope:ref_number
                                                      if access:retstock.tryfetch(res:pending_ref_number_key)
                                                          Delete(ordpend)
                                                          Cycle
                                                      Else!if access:retstock.tryfetch(res:pending_ref_number_key)
                                                          res:order_number    = ord:order_number
                                                          res:pending_ref_number  = ''
                                                          res:date_ordered    = Today()
                                                          res:order_part_number  = orp:record_number
                                                          res:despatched    = 'ORD'
                                                          access:retstock.update()
      
                                                          orp:quantity    = res:quantity
                                                          orp:part_number = res:part_number
                                                          orp:description = res:description
                                                          orp:purchase_cost   = res:purchase_cost
                                                          orp:sale_cost   = res:sale_cost
                                                          orp:retail_cost = res:retail_cost
                                                          orp:part_type   = 'RET'
                                                          orp:part_ref_number = res:part_ref_number
                                                          orp:account_number  = ope:account_number
                                                          orp:allocated_to_sale   = 'NO'
                                                      End!if access:retstock.tryfetch(res:pending_ref_number_key)
      
                                              End!Case ope:part_type
                                          Of 1! New Way
                                              If ope:Part_Type = 'RET'
                                                  orp:job_number  = ope:job_number
                                                  access:retstock.clearkey(res:part_number_key)
                                                  res:ref_number  = ope:job_number
                                                  res:pending_ref_number = ope:ref_number
                                                  if access:retstock.tryfetch(res:pending_ref_number_key)
                                                      Delete(ordpend)
                                                      Cycle
                                                  Else!if access:retstock.tryfetch(res:pending_ref_number_key)
                                                      res:order_number    = ord:order_number
                                                      res:pending_ref_number  = ''
                                                      res:date_ordered    = Today()
                                                      res:order_part_number  = orp:record_number
                                                      res:despatched    = 'ORD'
                                                      access:retstock.update()
      
                                                      orp:quantity    = ope:quantity
                                                      orp:part_number = res:part_number
                                                      orp:description = res:description
                                                      orp:purchase_cost   = res:purchase_cost
                                                      orp:sale_cost   = res:sale_cost
                                                      orp:retail_cost = res:retail_cost
                                                      orp:part_type   = 'RET'
                                                      orp:part_ref_number = res:part_ref_number
                                                      orp:account_number  = ope:account_number
                                                      orp:allocated_to_sale   = 'NO'
                                                  End!if access:retstock.tryfetch(res:pending_ref_number_key)
                                              Else !If ope:Part_Type = 'RET'
                                                  orp:Part_Ref_Number    = ''
                                                  orp:Quantity           = ope:Quantity
                                                  orp:Part_Number        = ope:Part_Number
                                                  orp:Description        = ope:Description
                                                  orp:part_type   = 'STO'
      
                                                  !Dunno the costs, so try and find it in Main Store
                                                  Access:LOCATION.ClearKey(loc:Main_Store_Key)
                                                  loc:Main_Store = 'YES'
                                                  Set(loc:Main_Store_Key,loc:Main_Store_Key)
                                                  If Access:LOCATION.Next() = Level:Benign
                                                      If loc:Main_Store = 'YES'
                                                          Access:STOCK.ClearKey(sto:Location_Key)
                                                          sto:Location    = loc:Location
                                                          sto:Part_Number = ope:Part_Number
                                                          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                              orp:Purchase_Cost      = sto:Purchase_Cost
                                                              orp:Sale_Cost          = sto:Sale_Cost
                                                              orp:Retail_Cost        = sto:Retail_Cost
                                                          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                      End!If loc:Main_Store = 'YES'
                                                  End!If Access:LOCATION.Next() = Level:Benign
                                              End !If ope:Part_Type = 'RET'
                                      End!Case def:SummaryOrders
      
                                      ! Add currency information - TrkBs: 5110 (DBH: 18-05-2005)
                                      Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                                      sup:Company_Name = req:Supplier
                                      If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                          !Found
                                          If sup:UseForeignCurrency
                                              Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
                                              cur:CurrencyCode = sup:CurrencyCode
                                              If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                                  !Found
                                                  orp:OrderedCurrency         = sup:CurrencyCode
                                                  orp:OrderedDailyRate        = cur:DailyRate
                                                  orp:OrderedDivideMultiply   = cur:DivideMultiply
                                                  ! I don't think it's necessary to save this information there.. but you never know - TrkBs: 5110 (DBH: 18-05-2005)
                                              Else !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                                  !Error
                                              End !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                          End ! If sup:UseForeignCurrency
                                      Else !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                          !Error
                                      End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                      ! End   - Add currency information - TrkBs: 5110 (DBH: 18-05-2005)
      
                                      orp:all_received    = 'NO'
                                      access:ordparts.insert()
                                  End!if access:ordparts.primerecord() = level:benign
                      !Now put the order into a memory table, so the report will be a summary
                                  Sort(glo:Q_PartsOrder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
                                  glo:q_order_number  = orp:order_number
                                  glo:q_part_number   = orp:part_number
                                  glo:q_description   = orp:description
                                  glo:q_supplier      = ope:supplier
                                  glo:q_purchase_cost = orp:purchase_cost
                                  glo:q_sale_cost     = orp:sale_cost
      
                                  Get(glo:Q_PartsOrder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
                                  If error()
                                      glo:q_order_number  = orp:order_number
                                      glo:q_part_number   = orp:part_number
                                      glo:q_description   = orp:description
                                      glo:q_supplier      = ope:supplier
                                      glo:q_purchase_cost = orp:purchase_cost
                                      glo:q_sale_cost     = orp:sale_cost
                                      glo:q_quantity      = orp:quantity
                                      Add(glo:Q_PartsOrder)
                                  Else!If error()
                                      glo:q_quantity      += orp:quantity
                                      Put(glo:Q_PartsOrder)
                                  End!If error()
      
                                  Delete(ordpend)
      
                                  req:Ordered = 1
                                  Access:REQUISIT.TryUpdate()
      
                              End !Loop
                              Access:ORDPEND.RestoreFile(Save_ope_ID)
                              req:Ordered = 1
                              Access:REQUISIT.TryUpdate()
      
                          End!Loop x# = 1 To Records(glo:Queue)
                          setcursor()
                          close(progresswindow)
      
                          Set(defaults)
                          access:defaults.next()
      
                          Case Missive('Orders have been created. '&|
                            '<13,10>'&|
                            '<13,10>They will now be printed.','ServiceBase 3g',|
                                         'midea.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
      
                          glo:select1  = ''
                          Parts_Order
                      END !if AppFound =1 (ie there was something to process
      
                      !TB12981 - if some requests was not approved, then don't close the window - JC 11/02/13
                      if NotAppFound = 0 then
                          Post(Event:CloseWindow)
                      ELSE
                          !untag everything
                          DO DASBRW::7:DASUNTAGALL
                      END
      
                  Of 1 ! No Button
      
              End ! Case Missive you want to generate orders?
      
          End!If ~Records(Qu
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Order, Accepted)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonKey
      ThisWindow.Update
      KeyPendingOrders
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DASTAG)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet3
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet3)
        OF 1
          ?Browse:1{PROP:FORMAT} ='106L(2)|FM~Description~@s30@#1#92L(2)|FM~Part Number~@s30@#2#143L(2)|FM~Quantity~@p<<<<<<<#p@#3#'
          ?Tab6{PROP:TEXT} = 'By Description'
        OF 2
          ?Browse:1{PROP:FORMAT} ='92L(2)|FM~Part Number~@s30@#2#106L(2)|FM~Description~@s30@#1#143L(2)|FM~Quantity~@p<<<<<<<#p@#3#'
          ?Tab4{PROP:TEXT} = 'By Part Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?Dastag)
          Of F6Key
              Post(Event:Accepted,?DasTagall)
          Of F7Key
              Post(Event:Accepted,?DasUntagAll)
          Of F8Key
              Post(Event:Accepted,?Valuate_Order)
          Of F10Key
              Post(Event:Accepted,?Create_Order)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::7:DASUNTAGALL
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet3) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (ope:Job_Number <> '')
    order_type_temp = ope:Job_Number
  ELSE
    order_type_temp = 'Stock'
  END
  CASE (ope:Part_Type)
  OF 'JOB'
    type_temp = 'CHA'
  ELSE
    type_temp = ope:Part_Type
  END
  PARENT.SetQueueRecord
  SELF.Q.order_type_temp = order_type_temp            !Assign formula result to display queue
  SELF.Q.type_temp = type_temp                        !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = req:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END
  IF (req:Approved = 'N')
    SELF.Q.req:RecordNumber_NormalFG = 255
    SELF.Q.req:RecordNumber_NormalBG = 16777215
    SELF.Q.req:RecordNumber_SelectedFG = 16777215
    SELF.Q.req:RecordNumber_SelectedBG = 255
  ELSIF (req:Approved = 'Y')
    SELF.Q.req:RecordNumber_NormalFG = 32768
    SELF.Q.req:RecordNumber_NormalBG = 16777215
    SELF.Q.req:RecordNumber_SelectedFG = 16777215
    SELF.Q.req:RecordNumber_SelectedBG = 32768
  ELSE
    SELF.Q.req:RecordNumber_NormalFG = 0
    SELF.Q.req:RecordNumber_NormalBG = 16777215
    SELF.Q.req:RecordNumber_SelectedFG = 16777215
    SELF.Q.req:RecordNumber_SelectedBG = 10119804
  END
  IF (req:Approved = 'N')
    SELF.Q.req:Supplier_NormalFG = 255
    SELF.Q.req:Supplier_NormalBG = 16777215
    SELF.Q.req:Supplier_SelectedFG = 16777215
    SELF.Q.req:Supplier_SelectedBG = 255
  ELSIF (req:Approved = 'Y')
    SELF.Q.req:Supplier_NormalFG = 32768
    SELF.Q.req:Supplier_NormalBG = 16777215
    SELF.Q.req:Supplier_SelectedFG = 16777215
    SELF.Q.req:Supplier_SelectedBG = 32768
  ELSE
    SELF.Q.req:Supplier_NormalFG = 0
    SELF.Q.req:Supplier_NormalBG = 16777215
    SELF.Q.req:Supplier_SelectedFG = 16777215
    SELF.Q.req:Supplier_SelectedBG = 10119804
  END


BRW6.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, ValidateRecord, (),BYTE)
  if DisplayType = 'A' then       !TB12981 -Force approval of requestes before they become orders - JC 11/02/13
      !show all but the rejected
      if req:Approved = 'R' then return(record:filtered).
  ELSE
      if DisplayType <> req:Approved then return(record:Filtered).
  END !if display type = all
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = req:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
GetPendingCosts      PROCEDURE  (STRING func:PartType,*REAL func:PurchaseCost,*REAL func:SaleCost,<STRING fSupplier>) ! Declare Procedure
locByte              BYTE
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:STOCK.Open
   Relate:SUPPLIER.Open
   Relate:RETSTOCK.Open
   Relate:LOCATION.Open
   Relate:MODELNUM.Open
   Relate:STOMODEL.Open
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Case def:SummaryOrders
        Of 0
            Case func:PartType
                Of 'JOB'
                    Access:PARTS.Clearkey(par:PendingRefNoKey)
                    par:Ref_Number  = ope:Job_Number
                    par:Pending_Ref_Number  = ope:Ref_Number
                    If Access:PARTS.Tryfetch(par:PendingRefNoKey) = Level:Benign
                        !Found
                        func:PurchaseCost = par:purchase_cost
                        func:SaleCost     = par:sale_cost

                    Else ! If Access:PARTS.Tryfetch(par:PendingRefNoKey) = Level:Benign
                        !Error
                    End !If Access:PARTS.Tryfetch(par:PendingRefNoKey) = Level:Benign
                Of 'WAR'
                    Access:WARPARTS.Clearkey(wpr:PendingRefNoKey)
                    wpr:Ref_Number  = ope:Job_Number
                    wpr:Pending_Ref_Number  = ope:Ref_Number
                    If Access:WARPARTS.Tryfetch(wpr:PendingRefNoKey) = Level:Benign
                        !Found
                        func:PurchaseCost = wpr:purchase_cost
                        func:SaleCost     = wpr:sale_cost

                    Else ! If Access:PARTS.Tryfetch(par:PendingRefNoKey) = Level:Benign
                        !Error
                    End !If Access:PARTS.Tryfetch(par:PendingRefNoKey) = Level:Benign
                Of 'STO'
                    Access:STOCK.ClearKey(sto:Ref_Number_Key)
                    sto:Ref_Number  = ope:Part_Ref_Number
                    If Access:STOCK.Fetch(sto:Ref_Number_Key)
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = MainStoreLocation()
                        sto:Part_Number = ope:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            func:PurchaseCost = sto:purchase_cost
                            func:SaleCost     = sto:sale_cost
                            IF (sto:ExchangeUnit = 'YES')
                                IF (fSupplier <> '')
                                    ! #12213 Used the passed supplier, incase its not the default (Bryan: 27/07/2011)
                                    sto:Supplier = fSupplier
                                END
                                GetExchangePartCosts(sto:Manufacturer,sto:ExchangeModelNumber,sto:Supplier,func:PurchaseCost,func:SaleCost,locByte)
                            END
                        End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    Else!If access:stock.fetch(sto:ref_number_key)
                        func:PurchaseCost = sto:purchase_cost
                        func:SaleCost     = sto:sale_cost

                        IF (sto:ExchangeUnit = 'YES')
                            IF (fSupplier <> '')
                                ! #12213 Used the passed supplier, incase its not the default (Bryan: 27/07/2011)
                                sto:Supplier = fSupplier
                            END
                            GetExchangePartCosts(sto:Manufacturer,sto:ExchangeModelNumber,sto:Supplier,func:PurchaseCost,func:SaleCost,locByte)
                        END


                    End!If access:stock.fetch(sto:ref_number_key)
                Of 'RET'
                    orp:job_number  = ope:job_number
                    access:retstock.clearkey(res:part_number_key)
                    res:ref_number  = ope:job_number
                    res:pending_ref_number = ope:ref_number
                    if access:retstock.tryfetch(res:pending_ref_number_key)
                    Else!if access:retstock.tryfetch(res:pending_ref_number_key)
                        func:PurchaseCost = res:purchase_cost
                        func:SaleCost     = res:sale_cost

                    End!if access:retstock.tryfetch(res:pending_ref_number_key)

            End !func:PartType
        Of 1
            If func:PartType = 'RET'
                orp:job_number  = ope:job_number
                access:retstock.clearkey(res:part_number_key)
                res:ref_number  = ope:job_number
                res:pending_ref_number = ope:ref_number
                if access:retstock.tryfetch(res:pending_ref_number_key)
                Else!if access:retstock.tryfetch(res:pending_ref_number_key)
                    func:PurchaseCost = res:purchase_cost
                    func:SaleCost     = res:sale_cost

                End!if access:retstock.tryfetch(res:pending_ref_number_key)
            Else !If func:PartType = 'RET'

                !Dunno the costs, so try and find it in Main Store
                Access:LOCATION.ClearKey(loc:Main_Store_Key)
                loc:Main_Store = 'YES'
                Set(loc:Main_Store_Key,loc:Main_Store_Key)
                If Access:LOCATION.Next() = Level:Benign
                    If loc:Main_Store = 'YES'
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = loc:Location
                        sto:Part_Number = ope:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            func:PurchaseCost = sto:purchase_cost
                            func:SaleCost     = sto:sale_cost

                        End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    End!If loc:Main_Store = 'YES'
                End!If Access:LOCATION.Next() = Level:Benign
            End !If func:PartType = 'RET'
    End !def:SummaryOrders
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:STOCK.Close
   Relate:SUPPLIER.Close
   Relate:RETSTOCK.Close
   Relate:LOCATION.Close
   Relate:MODELNUM.Close
   Relate:STOMODEL.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
BrowseGoodsReceivedNotes PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Supplier         STRING(30)
BRW1::View:Browse    VIEW(GRNOTES)
                       PROJECT(grn:Goods_Received_Number)
                       PROJECT(grn:Order_Number)
                       PROJECT(grn:Goods_Received_Date)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
grn:Goods_Received_Number LIKE(grn:Goods_Received_Number) !List box control field - type derived from field
grn:Order_Number       LIKE(grn:Order_Number)         !List box control field - type derived from field
tmp:Supplier           LIKE(tmp:Supplier)             !List box control field - type derived from local data
grn:Goods_Received_Date LIKE(grn:Goods_Received_Date) !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Goods Received Note File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,108,276,216),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('41R(2)|M~GRN~@p<<<<<<<<<<<<<<<<#p@54R(2)|M~Order No~L@n~SS~010@117L(2)|M~Supplier~@s30@4' &|
   '0R(2)|M~Recvd Date~L@d06b@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Goods Received Note File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By GRN Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n10),AT(168,96,,10),USE(grn:Goods_Received_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Goods Received Note Number')
                           BUTTON,AT(448,112),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,140),USE(?Goods_Received_Note_Button),TRN,FLAT,LEFT,ICON('repgrnp.jpg')
                           BUTTON,AT(448,190),USE(?Insert:3),TRN,FLAT,HIDE,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,218),USE(?Change:3),TRN,FLAT,HIDE,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,246),USE(?Delete:3),TRN,FLAT,HIDE,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020080'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseGoodsReceivedNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:GRNOTES.Open
  Relate:ORDERS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:GRNOTES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  BRW1.InsertControl = 0
  BRW1.ChangeControl = 0
  BRW1.DeleteControl = 0
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,grn:Goods_Received_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?grn:Goods_Received_Number,grn:Goods_Received_Number,1,BRW1)
  BIND('tmp:Supplier',tmp:Supplier)
  BRW1.AddField(grn:Goods_Received_Number,BRW1.Q.grn:Goods_Received_Number)
  BRW1.AddField(grn:Order_Number,BRW1.Q.grn:Order_Number)
  BRW1.AddField(tmp:Supplier,BRW1.Q.tmp:Supplier)
  BRW1.AddField(grn:Goods_Received_Date,BRW1.Q.grn:Goods_Received_Date)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:GRNOTES.Close
    Relate:ORDERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Goods_Received_Note_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Goods_Received_Note_Button, Accepted)
      BRW1.UpdateBuffer()
      
      if grn:Goods_Received_Number <> 0
          GLO:Select1 = grn:Goods_Received_Number
          Goods_Received_Note
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Goods_Received_Note_Button, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020080'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020080'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020080'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?grn:Goods_Received_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?grn:Goods_Received_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?grn:Goods_Received_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  Access:ORDERS.ClearKey(ord:Order_Number_Key)
  ord:Order_Number = grn:Order_Number
  if Access:ORDERS.Fetch(ord:Order_Number_Key)
    tmp:Supplier = ''
  else
    tmp:Supplier = ord:Supplier
  end
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Pick_Trans_Location PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer20                     LIKE(glo:Pointer20)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tag                  STRING(1)
BRW1::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag                    LIKE(tag)                      !List box control field - type derived from local data
tag_Icon               LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Location File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@80L(2)|M~Location~@s30@'),FROM(Queue:Browse:1)
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       BUTTON('&Rev tags'),AT(648,132,32,12),USE(?DASREVTAG),HIDE,LEFT
                       BUTTON('sho&W tags'),AT(640,148,40,12),USE(?DASSHOWTAG),HIDE,LEFT
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Location File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Location'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(loc:Location),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,191),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,220),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,247),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue20.Pointer20 = loc:Location
   GET(glo:Queue20,glo:Queue20.Pointer20)
  IF ERRORCODE()
     glo:Queue20.Pointer20 = loc:Location
     ADD(glo:Queue20,glo:Queue20.Pointer20)
    tag = '*'
  ELSE
    DELETE(glo:Queue20)
    tag = ''
  END
    Queue:Browse:1.tag = tag
  IF (tag = '*')
    Queue:Browse:1.tag_Icon = 2
  ELSE
    Queue:Browse:1.tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue20)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue20.Pointer20 = loc:Location
     ADD(glo:Queue20,glo:Queue20.Pointer20)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue20)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue20)
    GET(glo:Queue20,QR#)
    DASBRW::9:QUEUE = glo:Queue20
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue20)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer20 = loc:Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer20)
    IF ERRORCODE()
       glo:Queue20.Pointer20 = loc:Location
       ADD(glo:Queue20,glo:Queue20.Pointer20)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020082'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Trans_Location')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:TRADEACC_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCATION,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,loc:Location_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?loc:Location,loc:Location,1,BRW1)
  BIND('tag',tag)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag,BRW1.Q.tag)
  BRW1.AddField(loc:Location,BRW1.Q.loc:Location)
  BRW1.AddField(loc:RecordNumber,BRW1.Q.loc:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?Browse:1{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020082'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020082'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020082'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = loc:Location
     GET(glo:Queue20,glo:Queue20.Pointer20)
    IF ERRORCODE()
      tag = ''
    ELSE
      tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag = '*')
    SELF.Q.tag_Icon = 2
  ELSE
    SELF.Q.tag_Icon = 1
  END


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  !TB12448 second part hide franchises - JC - 12/07/12
  
      access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
      tra_ali:SiteLocation = loc:Location
      if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
          !eh?? - let it show
      ELSE
          if tra_ali:Stop_Account = 'YES' then return(record:filtered).
      END
  
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = loc:Location
     GET(glo:Queue20,glo:Queue20.Pointer20)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

