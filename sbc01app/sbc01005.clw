

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01005.INC'),ONCE        !Local module procedure declarations
                     END


ManModSelect PROCEDURE                                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGFLAG         BYTE(0)
DASBRW::10:TAGMOUSE        BYTE(0)
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
localManufacturer    STRING(30)
LocalTag1            STRING(1)
LocalTag2            STRING(1)
FDB8::View:FileDrop  VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:Notes)
                       PROJECT(man:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?localManufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:Notes              LIKE(man:Notes)                !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag1              LIKE(LocalTag1)                !List box control field - type derived from local data
LocalTag1_Icon         LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(68,42,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Manufacturer / Model Number Charge Rate Selection'),AT(70,44),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:000000'),AT(572,44),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(68,56,552,304),USE(?Panel5),FILL(09A6A7CH)
                       STRING('Manufacturer'),AT(88,72),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(88,82,162,10),USE(localManufacturer),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L|M@s30@0L|M@s255@'),DROP(10,162),FROM(Queue:FileDrop)
                       PROMPT('Charges will be applied to the following Model Numbers:'),AT(392,76,149,18),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(88,100,204,220),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('12L(2)|FMI@s1@120L|M~Model Number~@s30@0L|FM@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(308,176),USE(?AddButton),TRN,FLAT,ICON('addmodp.jpg')
                       LIST,AT(392,100,204,222),USE(?List3),COLOR(COLOR:White),FORMAT('13L(9)|MI@s1@105L(2)|M~Manufacturer~@s30@120L(2)|M~Model Number~@s30@'),FROM(glo:ManModQueue)
                       BUTTON,AT(308,236),USE(?RemoveButton),TRN,FLAT,ICON('remmodp.jpg')
                       BUTTON,AT(88,326),USE(?DASTAG),TRN,FLAT,ICON('STgItemP.jpg')
                       BUTTON,AT(172,326),USE(?DASTAGAll),TRN,FLAT,ICON('STagAllP.jpg')
                       BUTTON,AT(255,326),USE(?DASUNTAGALL),TRN,FLAT,ICON('SUntagAP.jpg')
                       BUTTON,AT(559,326),USE(?UntagAll),TRN,FLAT,ICON('SUntagAP.jpg')
                       BUTTON('&Rev tags'),AT(104,368,22,14),USE(?DASREVTAG),HIDE
                       BUTTON,AT(476,362),USE(?OKButton),TRN,FLAT,ICON('okp.jpg'),STD(STD:Close)
                       BUTTON,AT(391,326),USE(?TagItem),TRN,FLAT,ICON('STgItemP.jpg')
                       BUTTON,AT(475,326),USE(?TagAll),TRN,FLAT,ICON('STagAllP.jpg')
                       BUTTON('sho&W tags'),AT(72,368,26,14),USE(?DASSHOWTAG),HIDE
                       PANEL,AT(68,362,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(548,362),USE(?CancelButton),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB8                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ManModFile      FILE,DRIVER('ASCII'),PRE(MMF),NAME(PaulsFIlename),CREATE,BINDABLE,THREAD
Record              Record
LineNumber          string(10)
Manufacturer        String(30)
ModelNo             String(30)
                    End
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW9.UpdateBuffer
   glo:Queue.Pointer = mod:Model_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = mod:Model_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    LocalTag1 = '*'
  ELSE
    DELETE(glo:Queue)
    LocalTag1 = ''
  END
    Queue:Browse.LocalTag1 = LocalTag1
  IF (LocalTag1 = '*')
    Queue:Browse.LocalTag1_Icon = 2
  ELSE
    Queue:Browse.LocalTag1_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = mod:Model_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::10:QUEUE = glo:Queue
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer = mod:Model_Number
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = mod:Model_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020751'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ManModSelect')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Free(glo:ManModQueue)
  
  
  !declare the icons for the ManMod Queue
  ?List3{PROP:IconList,1} = '~notick1.ico'
  ?List3{PROP:IconList,2} = '~tick1.ico'
  ?localManufacturer{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,mod:Manufacturer_Key)
  BRW9.AddRange(mod:Manufacturer,localManufacturer)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,mod:Model_Number,1,BRW9)
  BIND('LocalTag1',LocalTag1)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(LocalTag1,BRW9.Q.LocalTag1)
  BRW9.AddField(mod:Model_Number,BRW9.Q.mod:Model_Number)
  BRW9.AddField(mod:Manufacturer,BRW9.Q.mod:Manufacturer)
  FDB8.Init(?localManufacturer,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:MANUFACT,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(man:Manufacturer_Key)
  FDB8.AddField(man:Manufacturer,FDB8.Q.man:Manufacturer)
  FDB8.AddField(man:Notes,FDB8.Q.man:Notes)
  FDB8.AddField(man:RecordNumber,FDB8.Q.man:RecordNumber)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?AddButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddButton, Accepted)
      loop x# = 1 to records(glo:Queue)
          Get(glo:Queue,x#)
      
          If error() then
              break
          End
      
          MMQ:Taged           = ''
          MMQ:TagIcon         = 1
          MMQ:manufacturer    = clip(localManufacturer)
          MMQ:ModelNumber     = clip(GLO:Pointer)
      
          Add(glo:ManModQueue)
      
      End
      
      free(glo:Queue)
      
      Brw9.ResetSort(1)
      
      sort(glo:ManModQueue,MMQ:manufacturer,MMQ:ModelNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddButton, Accepted)
    OF ?RemoveButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveButton, Accepted)
      !remove the tagged items from the queue
      
      loop records(glo:ManModQueue) times
          loop x# = 1 to records(glo:ManModQueue)
              get(glo:ManModQueue,x#)
              If error() then
                  break
              End
      
              !Is it a tagged one?
              If MMQ:Taged = '*' then
                  delete(glo:ManModQueue)
              End
      
          End
      End
      
      Sort(glo:ManModQueue,MMQ:manufacturer,MMQ:ModelNumber)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveButton, Accepted)
    OF ?UntagAll
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UntagAll, Accepted)
      Loop x# = 1 to records(glo:ManModQueue)
          Get(glo:ManModQueue,x#)
      
          If error() then
              break
          End
      
          MMQ:Taged   = ''
          MMQ:TagIcon = 1
      
          Put(glo:ManModQueue)
      
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UntagAll, Accepted)
    OF ?TagItem
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TagItem, Accepted)
      x# = choice(?list3)
      
      get(glo:ManModQueue,x#)
      
      
      case MMQ:Taged
      of ''
          MMQ:Taged = '*'
      
          MMQ:TagIcon = 2
      of '*'
          MMQ:Taged = ''
      
          MMQ:TagIcon = 1
      end
      
      put(glo:ManModQueue)
      
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TagItem, Accepted)
    OF ?TagAll
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TagAll, Accepted)
      loop x# = 1 to records(glo:ManModQueue)
          Get(glo:ManModQueue,x#)
          If error() then
              break
          End
          MMQ:Taged   = '*'
          MMQ:TagIcon = 2
      
          Put(glo:ManModQueue)
      
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TagAll, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      Free(glo:ManModQueue)
      
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020751'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020751'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020751'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?localManufacturer
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?localManufacturer)
    free(glo:Queue)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?localManufacturer)
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::10:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDB8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  if man:Notes[1:8] = 'INACTIVE' then return(record:filtered).
  !TB12488 Disable Manufacturers  J 22/06/2012
  !TB13214 - change to using field for inactive  - J 05/02/14
  if man:Inactive = 1 then return(Record:Filtered).
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = mod:Model_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      LocalTag1 = ''
    ELSE
      LocalTag1 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag1 = '*')
    SELF.Q.LocalTag1_Icon = 2
  ELSE
    SELF.Q.LocalTag1_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  !check if these are in the queue for updating
  
  loop x# = 1 to records(glo:ManModQueue)
      get(glo:ManModQueue,x#)
  
      If error() then
          break
      End
  
      If mod:Manufacturer = MMQ:manufacturer AND mod:Model_Number = MMQ:ModelNumber then
          return record:filtered
      End
  End
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = mod:Model_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, ValidateRecord, (),BYTE)
  RETURN ReturnValue

EnterExpiryPassword PROCEDURE                         !Generated from procedure template - Window

locPassword          STRING(30)
ActionMessage        CSTRING(40)
locError             BYTE(1)
window               WINDOW('Enter Expiry Date Password'),AT(,,195,73),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),CENTER,TILED,GRAY,DOUBLE
                       PROMPT('SB3g No:'),AT(8,14),USE(?glo:PassAccount:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                       ENTRY(@s30),AT(68,14,124,10),USE(glo:PassAccount),SKIP,TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SB2000 Number'),TIP('SB2000 Number'),UPR,READONLY
                       PROMPT('Enter Password:'),AT(8,32),USE(?locPassword:Prompt),TRN
                       ENTRY(@s30),AT(68,32,124,10),USE(locPassword),REQ,PASSWORD
                       BUTTON('Cancel'),AT(136,54,56,16),USE(?Cancel)
                       BUTTON('Save'),AT(76,54,56,16),USE(?Save)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(locError)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EnterExpiryPassword')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?glo:PassAccount:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?Save,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Save
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Save, Accepted)
      IF (locPassword = '')
          CYCLE
      END ! IF
      
      !IF (FullAccess_NEW(glo:PassAccount,locPassword) = Level:Benign)
      IF (locPassword = '66pa1m3r5t0n66')
          locError = FALSE
      ELSE
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Invalid password!','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Save, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

PutIniUpdateAll      PROCEDURE                        ! Declare Procedure
LocalDir             QUEUE(File:Queue),PRE(LDR)
                     END
ImportFileName       STRING(255),STATIC
X                    LONG
Y                    LONG
Count                LONG
LocalFile            STRING(50)
LocalSection         STRING(50)
LocalEntry           STRING(50)
LocalValue           STRING(200)
RestrictedEntry      STRING(100)
RestrictedSection    STRING(100)
WantedSection        BYTE
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
ImportFile File,Driver('ASCII'),Pre(impfil),Name(ImportFileName),Create,Bindable,Thread
Record              Record
LineString                  String(300)
                    End
                End
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:INIDATA.Open
    !Save all ini file entries to the INIDATA.DAT file so VCP can find and read them
    !J - 09/09/14 - TB13367

    !set up defaults  - we don't bother saving some entries or sections
    !others may follow so leave room here for flexibiliy...
    !note format of | between each name stops confusing 'MAX' with 'MAXIMISE'
    RestrictedEntry   = Upper('|Maximize|Minimize|XPos|YPos|Width|Height|Sectors|')
    RestrictedSection = Upper('|__Dont_Touch_Me__|')

    !Directory the folder for all ini files
    Free(localdir)
    Directory(localdir,clip(path())&'\*.ini',ff_:NORMAL)

    !Loop through each ini file
    Loop Count = 1 to records(LocalDir)

        Get(localDir,Count)
        !Need a file name to import it
        ImportFileName = clip(Path())&'\'&clip(LDR:Name)

        !get the start of the name for the data field INI:IniFile
        Y = instring('.',LDR:Name,1,1) - 1
        LocalFile = Upper(LDR:Name[ 1 : Y ])

        !now we can open the file and get the info
        Open(ImportFile)
        If not (Error())
            Set(ImportFile)
            Loop

                !get the next line in this file
                next(ImportFile)
                if Error() then break.  !run out of this file

                If impfil:LineString[1] = '[' then

                    !this is a change of Section first and last brackets [] hold the name
                    Y = Len(clip(impfil:LineString)) - 1
                    LocalSection = Upper(impfil:LineString[ 2 : Y ])
                    !Is this section forbidden or wanted?
                    If Instring('|'&clip(LocalSection)&'|',clip(RestrictedSection),1,1) = 0
                        WantedSection = true
                    ELSe
                        WantedSection = false
                    END !if instring
                ELSE

                    !is this section wanted?
                    If WantedSection then

                        !not forbidden section - keep working on it ...
                        !this is an entry line there must be an = in it
                        X = instring('=',clip(impfil:LineString),1,1)
                        Y = Len(clip(impfil:LineString))
                        LocalEntry = Upper(impfil:linestring[ 1 : X - 1 ])
                        LocalValue = impfil:linestring[ X + 1 : Y ]    !not uppered because there may be meaning to lower case

                        !is this one forbidden?
                        If instring('|'&clip(LocalEntry)&'|'  ,clip(RestrictedEntry)  ,1,1) = 0 then

                            !not forbidden - does this one already exist?
                            access:IniData.clearkey(INI:KeyFileSectionEntry)
                            INI:IniFile     = LocalFile
                            INI:IniSection  = LocalSection
                            INI:IniEntry    = LocalEntry
                            If access:Inidata.fetch(INI:KeyFileSectionEntry) then

                                !not found add it
                                Access:IniData.primerecord()
                                INI:IniFile    = LocalFile
                                INI:IniSection = LocalSection
                                INI:IniEntry   = LocalEntry
                                INI:IniValue   = LocalValue
                                Access:IniData.update()
                                
                            ELSE

                                !If changed update it
                                if clip(INI:IniValue) <> clip(LocalValue) then
                                    INI:IniValue = LocalValue
                                    Access:IniData.update()
                                END !if changed

                            END !if matching record not found
                        END !if not found in restricted entries
                    END !if not found in restricted section
                END !if this is a new section
            END !loop through this importfile

            Close(ImportFile)

        End !if not error on open the ini file
    END !loop through the ini files
   Relate:INIDATA.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
