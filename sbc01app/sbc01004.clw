

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01004.INC'),ONCE        !Local module procedure declarations
                     END


DeleteChargesOption PROCEDURE                         !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Selection        BYTE(1)
tmp:Return           BYTE(0)
window               WINDOW('Delete Charges'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION,AT(244,154,200,88),USE(tmp:Selection),BOXED
                             RADIO,AT(256,167,12,17),USE(?tmp:Selection:Radio1),VALUE('1')
                             RADIO,AT(256,191,12,17),USE(?tmp:Selection:Radio2),VALUE('2')
                             RADIO,AT(256,215,12,17),USE(?tmp:Selection:Radio3),VALUE('3')
                           END
                           PROMPT('Delete charges for the selected Model Number ONLY.'),AT(272,166,160,16),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Delete charges for ALL Model Numbers of the selected Manufacturer ONLY.'),AT(272,190,160,24),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Delete charges for ALL Model Numbers AND ALL Manufacturers.'),AT(272,214,160,24),USE(?Prompt3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Delete Standard Charges'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020116'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DeleteChargesOption')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:Selection:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020116'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020116'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020116'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      tmp:Return  = tmp:Selection
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Return = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AboutScreen PROCEDURE                                 !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
RecipAddress         STRING(10000)
MsgSubject           STRING(255)
MsgNoteText          STRING(10000)
AttachList           STRING(1000)
CCAddress            STRING(10000)
BCCAddress           STRING(10000)
Connection           STRING('Default {20}')
Disconnect           BYTE(0)
RtnVal               BYTE(0)
tmp:HideCompanyLogos BYTE(0)
tmp:LogoPath         STRING(255)
locExpiryDate        DATE
savExpiryDate        DATE
NewDate              DATE
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('About ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       GROUP,AT(275,334,164,24),USE(?Group:Logo),HIDE
                         PROMPT('Logo Path (366px X 120px jpg)'),AT(275,334),USE(?tmp:LogoPath:Prompt),LEFT,FONT('Tahoma',7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         BUTTON,AT(412,340),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                         ENTRY(@s255),AT(276,344,132,10),USE(tmp:LogoPath),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Logo Path'),TIP('Logo Path'),UPR
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,192,248),USE(?Panel6),FILL(09A6A7CH)
                       PANEL,AT(360,82,156,248),USE(?Panel7)
                       PROMPT('Licensed By:'),AT(168,104),USE(?Prompt3),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('PC Control Systems Ltd.'),AT(184,118),USE(?String3),TRN,FONT(,14,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       STRING('Hamilton House'),AT(224,136),USE(?String4),TRN,FONT(,10,COLOR:White,FONT:bold)
                       PANEL,AT(368,108,140,74),USE(?Panel8),FILL(COLOR:White),BEVEL(-1,1)
                       IMAGE('sblogo.jpg'),AT(372,112),USE(?Image1)
                       STRING('66 Palmerston Road'),AT(224,148),USE(?String5),TRN,FONT(,10,COLOR:White,FONT:bold)
                       STRING('Northampton'),AT(224,160),USE(?String6),TRN,FONT(,10,COLOR:White,FONT:bold)
                       STRING('UK'),AT(224,172),USE(?String7),TRN,FONT(,10,COLOR:White,FONT:bold)
                       BUTTON('Save Date'),AT(296,282,56,16),USE(?btnSaveDate),HIDE
                       STRING('NN1 5EX'),AT(224,184),USE(?String8),TRN,FONT(,10,COLOR:White,FONT:bold)
                       STRING('Telephone: '),AT(168,204),USE(?String9),TRN,FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('01604 601677'),AT(224,202),USE(?String11),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       STRING('Fax: '),AT(168,216),USE(?String10),TRN,FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('01604 601676'),AT(224,214),USE(?String12),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       STRING('Homepage:'),AT(168,228),USE(?String14),TRN,FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('www.pccontrolsystems.com'),AT(224,228,148,12),USE(?Weblink),TRN,FONT('Tahoma',8,COLOR:Aqua,FONT:bold+FONT:underline,CHARSET:ANSI)
                       REGION,AT(224,226,112,12),USE(?MouseOver),IMM,CURSOR('finger.cur')
                       IMAGE('pwrc55.gif'),AT(472,230),USE(?Image2)
                       STRING('E-mail:'),AT(168,240),USE(?String13),TRN,FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('support@pccontrolsystems.com'),AT(224,240,144,12),USE(?Emaillink),TRN,FONT('Tahoma',8,COLOR:Aqua,FONT:bold+FONT:underline,CHARSET:ANSI)
                       STRING('Version:'),AT(168,256),USE(?String17),FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s30),AT(224,258,104,12),USE(defv:VersionNumber),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('Licence No:'),AT(168,270),USE(?String18),FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s10),AT(224,270),USE(def:License_Number,,?DEF:License_Number:2),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@d6),AT(224,284),USE(locExpiryDate),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(224,284,68,10),USE(locExpiryDate,,?locExpiryDate:2),HIDE,FONT(,,01010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ
                       STRING('Expiry Date:'),AT(168,284),USE(?locExpiryDate:Prompt),FONT(,8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       REGION,AT(224,238,128,12),USE(?MouseOver:2),IMM,CURSOR('finger.cur')
                       IMAGE('pvsw.gif'),AT(368,272),USE(?Image3)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('About ServiceBase'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('closep.jpg'),DEFAULT
                       CHECK('Use Alternative Logos'),AT(172,342),USE(tmp:HideCompanyLogos),HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hide Company Logos'),TIP('Hide Company Logos'),VALUE('1','0')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup12         SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020121'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AboutScreen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:LogoPath:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTV)
  Access:DEFAULTV.Next()
  If Error()
      If Access:DEFAULTV.PrimeRecord() = Level:Benign
          defv:VersionNumber    = '???'
          defv:NagDate    = Today()
          If Access:DEFAULTV.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULTV..TryInsert() = Level:Benign
              !Insert Failed
          End !DIf Access:DEFAULTV..TryInsert() = Level:Benign
      End !If Access:DEFAULTV..PrimeRecord() = Level:Benign
      Set(DEFAULTV)
      Access:DEFAULTV.Next()
  End !Error()
  
  If defv:ReUpdate = 0
      NagScreen(1)
  Else !defv:ReUpdate = 0
      If Today() => defv:NagDate
          NagScreen(0)
      End !Today() > defv:NagDate
  
  End !defv:ReUpdate = 0
  
  If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
      ?tmp:HideCompanyLogos{prop:Hide} = 0
      ?Group:Logo{prop:Hide} = 0
      ?locExpiryDate{prop:Hide}  = 1
      ?locExpiryDate:2{prop:Hide} = 0
      ?btnSaveDate{prop:Hide} = 0
  End ! If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
  tmp:HideCompanyLogos = GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:LogoPath = GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI')
  
  !TB13129 - remove license.dat from the file list and use new field in Default2
  !share(license)
  !if (error())
  !    create(license)
  !    share(license)
  !end
  !set(license,0)
  !next(license)
  !if (error())
  !    lic:ExpiryDate = DEFORMAT('01/07/2012',@d06)
  !    add(license)
  !end
  !if (lic:ExpiryDate = 0)
  !    lic:ExpiryDate = DEFORMAT('01/07/2012',@d06)
  !    add(license)
  !end
  !locExpiryDate = lic:ExpiryDate
  !savExpiryDate = lic:ExpiryDate
  !close(license)
  
  !TB13129 - licence expiry date
      SET(DEFAULT2,0)
      Next(Default2)
      if error() then halt().
      if de2:J_Collection_Rate = 0 then
          de2:J_Collection_Rate = deformat('01/07/14')
      END
      locExpiryDate = de2:J_Collection_Rate       !lic:ExpiryDate
      savExpiryDate = de2:J_Collection_Rate       !lic:ExpiryDate
  !TB13129 - licence expiry date
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:HideCompanyLogos{Prop:Checked} = True
    ENABLE(?Group:Logo)
  END
  IF ?tmp:HideCompanyLogos{Prop:Checked} = False
    DISABLE(?Group:Logo)
  END
  FileLookup12.Init
  FileLookup12.Flags=BOR(FileLookup12.Flags,FILE:LongName)
  FileLookup12.SetMask('JPG File','*.jpg')
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      PUTINIext('SBDEF','HideCompanyLogos',tmp:HideCompanyLogos,Clip(Path()) & '\SB2KDEF.INI')
      PUTINIext('SBDEF','LogoPath',tmp:LogoPath,Clip(Path()) & '\SB2KDEF.INI')
      
      !done on save date button
      !de2:J_Collection_Rate = NewDate
      !Access:Default2.tryupdate()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      ThisWindow.Update
      tmp:LogoPath = Upper(FileLookup12.Ask(1)  )
      DISPLAY
    OF ?btnSaveDate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnSaveDate, Accepted)
      IF (locExpiryDate <> savExpiryDate)
          IF (EnterExpiryPassword())
              ! Error
              locExpiryDate = savExpiryDate
          ELSE
              ! OK Change Date
      !        SHARE(License)
      !        SET(License,0)
      !        NEXT(License)
      !        lic:ExpiryDate = locExpiryDate
      !        PUT(License)
      !        IF (ERROR())
      !            STOP(ERROR())
      !        ELSE
                  ! #12598 Update the field for VCP and PUP (DBH: 21/06/2012)
      !            Relate:DEFAULT2.Open()
      !            SET(DEFAULT2,0)
      !            Access:DEFAULT2.Next()
                  de2:PLE = locExpiryDate
                  de2:J_Collection_Rate = locExpiryDate
                  Access:DEFAULT2.TryUpdate()
      !            Relate:DEFAULT2.Close()
      
                  Beep(Beep:SystemAsterisk)  ;  Yield()
                  Case Missive('Date saved.','ServiceBase',|
                                 'midea.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
      !        END
      !        CLOSE(License)
      
          END !IF
      END
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnSaveDate, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020121'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020121'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020121'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?tmp:HideCompanyLogos
      IF ?tmp:HideCompanyLogos{Prop:Checked} = True
        ENABLE(?Group:Logo)
      END
      IF ?tmp:HideCompanyLogos{Prop:Checked} = False
        DISABLE(?Group:Logo)
      END
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(Defaults)
      If access:defaults.next()
          get(defaults,0)
          if access:defaults.primerecord() = level:benign
              if access:defaults.insert()
                  access:defaults.cancelautoinc()
              end
          end!if access:defaults.primerecord() = level:benign
      End!If access:defaults.next()
      
      
      
      Set(defstock)
      If access:defstock.next()
          get(defstock,0)
          if access:defstock.primerecord() = level:benign
              if access:defstock.insert()
                  access:defstock.cancelautoinc()
              end
          end!if access:defstock.primerecord() = level:benign
      End!If access:defstock.next()
      
      
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BouncerDefaults PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CRepairType      STRING(30)
tmp:WRepairType      STRING(30)
window               WINDOW('Bouncer Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Bouncer Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('When a Bouncer is "Submitted", please select the Charge Type that the job will a' &|
   'utomatically change to.'),AT(184,157,312,20),USE(?Prompt3),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Chargeable Type'),AT(212,186),USE(?tmp:CRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(312,186,124,10),USE(tmp:CRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Chargeable Repair Type'),TIP('Chargeable Repair Type'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(440,181),USE(?LookupChargeableRepairType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Warranty Charge Type'),AT(212,205),USE(?tmp:WRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(312,205,124,10),USE(tmp:WRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Warranty Repair Type'),TIP('Warranty Repair Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(440,202),USE(?LookupWarrantyRepairType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:CRepairType                Like(tmp:CRepairType)
look:tmp:WRepairType                Like(tmp:WRepairType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020123'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BouncerDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Access:REPTYDEF.UseFile
  SELF.FilesOpened = True
  tmp:CRepairType = GETINI('BOUNCER','ChargeableChargeType',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:WRepairType = GETINI('BOUNCER','WarrantyChargeType',,CLIP(PATH())&'\SB2KDEF.INI')
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:CRepairType{Prop:Tip} AND ~?LookupChargeableRepairType{Prop:Tip}
     ?LookupChargeableRepairType{Prop:Tip} = 'Select ' & ?tmp:CRepairType{Prop:Tip}
  END
  IF ?tmp:CRepairType{Prop:Msg} AND ~?LookupChargeableRepairType{Prop:Msg}
     ?LookupChargeableRepairType{Prop:Msg} = 'Select ' & ?tmp:CRepairType{Prop:Msg}
  END
  IF ?tmp:WRepairType{Prop:Tip} AND ~?LookupWarrantyRepairType{Prop:Tip}
     ?LookupWarrantyRepairType{Prop:Tip} = 'Select ' & ?tmp:WRepairType{Prop:Tip}
  END
  IF ?tmp:WRepairType{Prop:Msg} AND ~?LookupWarrantyRepairType{Prop:Msg}
     ?LookupWarrantyRepairType{Prop:Msg} = 'Select ' & ?tmp:WRepairType{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020123'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020123'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020123'&'0')
      ***
    OF ?tmp:CRepairType
      IF tmp:CRepairType OR ?tmp:CRepairType{Prop:Req}
        cha:Charge_Type = tmp:CRepairType
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:tmp:CRepairType        = tmp:CRepairType
        IF Access:CHARTYPE.TryFetch(cha:Charge_Type_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:CRepairType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tmp:CRepairType = look:tmp:CRepairType
            SELECT(?tmp:CRepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeableRepairType
      ThisWindow.Update
      cha:Charge_Type = tmp:CRepairType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:CRepairType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tmp:CRepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:CRepairType)
    OF ?tmp:WRepairType
      IF tmp:WRepairType OR ?tmp:WRepairType{Prop:Req}
        cha:Charge_Type = tmp:WRepairType
        !Save Lookup Field Incase Of error
        look:tmp:WRepairType        = tmp:WRepairType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:WRepairType = cha:Charge_Type
          ELSE
            !Restore Lookup On Error
            tmp:WRepairType = look:tmp:WRepairType
            SELECT(?tmp:WRepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarrantyRepairType
      ThisWindow.Update
      cha:Charge_Type = tmp:WRepairType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:WRepairType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tmp:WRepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:WRepairType)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      PUTINIext('BOUNCER','ChargeableChargeType',tmp:CRepairType,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('BOUNCER','WarrantyChargeType',tmp:WRepairType,CLIP(PATH()) & '\SB2KDEF.INI')
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:CRepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:CRepairType, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:CRepairType, AlertKey)
    END
  OF ?tmp:WRepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:WRepairType, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:WRepairType, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
EmailDefaults PROCEDURE                               !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Set Email Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Email Defaults'),USE(?BookingDefaultsTab)
                           PROMPT('Server Address'),AT(240,170),USE(?def:EmailServerAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(340,170,112,10),USE(def:EmailServerAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Server Address'),TIP('Email Server Address'),UPR
                           PROMPT('Server Port Number'),AT(240,188),USE(?def:EmailServerPort:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(340,188,64,10),USE(def:EmailServerPort),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Service Port Number'),TIP('Email Service Port Number'),UPR
                           GROUP('Authentication (if required)'),AT(220,204,252,58),USE(?Group2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Email Username'),AT(240,218),USE(?de2:EmailUserName:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(340,218,124,10),USE(de2:EmailUserName),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Email Username'),TIP('Email Username')
                             PROMPT('Email Password'),AT(240,238),USE(?de2:EmailPassword:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s60),AT(340,238,124,10),USE(de2:EmailPassword),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Email Password'),TIP('Email Password')
                           END
                           PROMPT('Default Email Address'),AT(240,274),USE(?de2:DefaultFromEmail:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s100),AT(340,274,124,10),USE(de2:DefaultFromEmail),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           STRING('(Used to populate the "FROM" field in emails)'),AT(240,288),USE(?String1),FONT(,,COLOR:White,FONT:regular,CHARSET:ANSI)
                         END
                       END
                       BUTTON,AT(384,332),USE(?OkButton),FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020127'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('EmailDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  Set(DEFAULT2)
  If Access:DEFAULT2.Next()
      If Access:DEFAULT2.PrimeRecord() = Level:Benign
          If Access:DEFAULT2.TryInsert() = Level:Benign
              ! Inserted
          Else ! If Access:DEFAULT2.TryInsert() = Level:Benign
              Access:DEFAULT2.CancelAutoInc()
          End ! If Access:DEFAULT2.TryInsert() = Level:Benign
      End ! If Access:DEFAULT2.PrimeRecord() = Level:Benign
  End ! If Access:DEFAULT2.Next()
  
  
  
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020127'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020127'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020127'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      Access:DEFAULT2.Update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
EstimateDefaults PROCEDURE                            !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:USEBERComparison BYTE(0)
tmp:BERCostComparison LONG
tmp:CheckPartAvailable BYTE(0)
tmp:CheckPartsText   STRING(255)
tmp:ForceEstimateAccepted BYTE(0)
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Estimate Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Estimate Defaults'),USE(?BookingDefaultsTab)
                           CHECK('User Estimate Cost Comparison'),AT(292,136),USE(tmp:USEBERComparison),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('User Estimate Cost Comparison'),TIP('User Estimate Cost Comparison'),VALUE('1','0')
                           PROMPT('BER Cost Comparison (%)'),AT(216,148,60,16),USE(?tmp:BERCostComparison:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(292,148,64,10),USE(tmp:BERCostComparison),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('BER Cost Comparison (%)'),TIP('BER Cost Comparison (%)'),UPR
                           CHECK('Check Parts Available When Estimate Ready'),AT(292,168),USE(tmp:CheckPartAvailable),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Check Parts Available When Estimate Ready'),TIP('Check Parts Available When Estimate Ready'),VALUE('1','0')
                           PROMPT('Overdue Backorder Message Text'),AT(216,184,72,24),USE(?tmp:CheckPartsText:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(292,184,124,56),USE(de2:OverdueBackOrderText),VSCROLL,LEFT,FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('Overdue Backorder Message Text'),TIP('Overdue Backorder Message Text'),UPR
                           CHECK('Force Estimates To Be Accepted/Rejected'),AT(292,248),USE(tmp:ForceEstimateAccepted),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force Estimates To Be Accepted/Rejected'),TIP('Force Estimates To Be Accepted/Rejected'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020128'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('EstimateDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  Set(Default2)
  If access:default2.next()
      get(default2,0)
      if access:default2.primerecord() = level:benign
          if access:default2.insert()
              access:default2.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  tmp:UseBERComparison = Clip(GETINI('ESTIMATE','UseBERComparison',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:BERCostComparison = Clip(GETINI('ESTIMATE','BERComparison',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:CheckPartAvailable =Clip(GETINI('ESTIMATE','CheckPartAvailable',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:ForceEstimateAccepted = GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI')
  
  
  
  
  
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:USEBERComparison{Prop:Checked} = True
    UNHIDE(?tmp:BERCostComparison:Prompt)
    UNHIDE(?tmp:BERCostComparison)
  END
  IF ?tmp:USEBERComparison{Prop:Checked} = False
    HIDE(?tmp:BERCostComparison:Prompt)
    HIDE(?tmp:BERCostComparison)
  END
  IF ?tmp:CheckPartAvailable{Prop:Checked} = True
    UNHIDE(?tmp:CheckPartsText:Prompt)
    UNHIDE(?de2:OverdueBackOrderText)
  END
  IF ?tmp:CheckPartAvailable{Prop:Checked} = False
    HIDE(?tmp:CheckPartsText:Prompt)
    HIDE(?de2:OverdueBackOrderText)
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020128'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020128'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020128'&'0')
      ***
    OF ?tmp:USEBERComparison
      IF ?tmp:USEBERComparison{Prop:Checked} = True
        UNHIDE(?tmp:BERCostComparison:Prompt)
        UNHIDE(?tmp:BERCostComparison)
      END
      IF ?tmp:USEBERComparison{Prop:Checked} = False
        HIDE(?tmp:BERCostComparison:Prompt)
        HIDE(?tmp:BERCostComparison)
      END
      ThisWindow.Reset
    OF ?tmp:CheckPartAvailable
      IF ?tmp:CheckPartAvailable{Prop:Checked} = True
        UNHIDE(?tmp:CheckPartsText:Prompt)
        UNHIDE(?de2:OverdueBackOrderText)
      END
      IF ?tmp:CheckPartAvailable{Prop:Checked} = False
        HIDE(?tmp:CheckPartsText:Prompt)
        HIDE(?de2:OverdueBackOrderText)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      access:default2.update()
      PUTINIext('ESTIMATE','UseBERComparison',tmp:UseBERComparison,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('ESTIMATE','BERComparison',tmp:BERCostComparison,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('ESTIMATE','CheckPartAvailable',tmp:CheckPartAvailable,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('ESTIMATE','ForceAccepted',tmp:ForceEstimateAccepted,CLIP(PATH()) & '\SB2KDEF.INI')
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RRCDefaults PROCEDURE                                 !Generated from procedure template - Window

tmp:RRCLocation      STRING(30)
tmp:InTransit        STRING(30)
tmp:ARCLocation      STRING(30)
tmp:RTMLocation      STRING(30)
tmp:SendToARC        STRING(30)
tmp:DespatchedToARC  STRING(30)
tmp:ReceivedAtARC    STRING(30)
tmp:SendToRRC        STRING(30)
tmp:DespatchedToRRC  STRING(30)
tmp:ReceivedAtRRC    STRING(30)
tmp:InTransitRRC     STRING(30)
tmp:SendToCustomer   STRING(30)
tmp:ARCReceivedQuery STRING(30)
tmp:RRCReceivedQuery STRING(30)
tmp:ExcDespatchedToRRC STRING(30)
tmp:ExcReceivedAtRRC STRING(30)
tmp:AtPUPLocation    STRING(30)
tmp:InTransitFromPUP STRING(30)
tmp:InTransitToPUP   STRING(30)
tmp:BookedATPUP      STRING(30)
tmp:SentFromPUP      STRING(30)
tmp:ReceivedFromPUP  STRING(30)
tmp:DespatchToPUP    STRING(30)
tmp:SentToPUP        STRING(30)
tmp:ReceivedAtPUP    STRING(30)
tmp:DespatchedToPUPCustomer STRING(30)
tmp:VCPDefaultTransitType STRING(30)
window               WINDOW('R.R.C. Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Internal Location Defaults'),USE(?Tab1)
                           PROMPT('At R.R.C.'),AT(76,88),USE(?tmp:RRCLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,88,124,10),USE(tmp:RRCLocation),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('At R.R.C.'),TIP('At R.R.C.'),UPR
                           PROMPT('In Transit To ARC'),AT(76,108),USE(?tmp:InTransit:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,108,124,10),USE(tmp:InTransit),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('In Transit'),TIP('In Transit'),UPR
                           PROMPT('At A.R.C.'),AT(76,128),USE(?tmp:ARCLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,128,124,10),USE(tmp:ARCLocation),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('At A.R.C.'),TIP('At A.R.C.'),UPR
                           PROMPT('In Transit To RRC'),AT(76,148),USE(?tmp:InTransitRRC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,148,124,10),USE(tmp:InTransitRRC),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('In Transit To RRC'),TIP('In Transit To RRC'),UPR
                           PROMPT('Despatch To Customer'),AT(76,168),USE(?tmp:SendToCustomer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,168,124,10),USE(tmp:SendToCustomer),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Despatched To Customer'),TIP('Despatched To Customer'),UPR
                           PROMPT('Return To Manufact.'),AT(76,188),USE(?tmp:RTMLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,188,124,10),USE(tmp:RTMLocation),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Return To Manufacturer'),TIP('Return To Manufacturer'),UPR
                           PROMPT('At P.U.P.'),AT(76,210),USE(?tmp:AtPUPLocation:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(176,210,124,10),USE(tmp:AtPUPLocation),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('At P.U.P.'),TIP('At P.U.P.'),UPR
                           PROMPT('In Transit From P.U.P.'),AT(76,232),USE(?tmp:InTransitFromPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(176,232,124,10),USE(tmp:InTransitFromPUP),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('In Transit From P.U.P.'),TIP('In Transit From P.U.P.'),UPR
                           PROMPT('In Transit To P.U.P.'),AT(76,254),USE(?tmp:InTransitToPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(176,254,124,10),USE(tmp:InTransitToPUP),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('In Transit To P.U.P.'),TIP('In Transit To P.U.P.'),UPR
                         END
                         TAB('Status Change Defaults'),USE(?Tab2)
                           GROUP,AT(68,68,543,200),USE(?Group2),DISABLE
                             PROMPT('Send To A.R.C.'),AT(68,72),USE(?tmp:SendToARC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,72,124,10),USE(tmp:SendToARC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Send To A.R.C.'),TIP('Send To A.R.C.'),UPR
                             BUTTON,AT(296,68),USE(?SendToARC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Booked At P.U.P.'),AT(344,72),USE(?tmp:BookedATPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,72,124,10),USE(tmp:BookedATPUP),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Booked At P.U.P.'),TIP('Booked At P.U.P.'),UPR
                             BUTTON,AT(584,68),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Despatched To A.R.C.'),AT(68,92),USE(?tmp:DespatchToARC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,92,124,10),USE(tmp:DespatchedToARC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Despatched To A.R.C.'),TIP('Despatched To A.R.C.'),UPR
                             BUTTON,AT(296,88),USE(?DespatchedToARC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Sent From P.U.P.'),AT(344,92),USE(?tmp:SentFromPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,92,124,10),USE(tmp:SentFromPUP),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Sent From P.U.P.'),TIP('Sent From P.U.P.'),UPR
                             BUTTON,AT(584,88),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Received At A.R.C.'),AT(68,112),USE(?tmp:ReceivedAtARC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,112,124,10),USE(tmp:ReceivedAtARC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Received At A.R.C.'),TIP('Received At A.R.C.'),UPR
                             BUTTON,AT(296,108),USE(?ReceivedAtARC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Received From P.U.P.'),AT(344,112),USE(?tmp:ReceivedFromPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,112,124,10),USE(tmp:ReceivedFromPUP),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Received From P.U.P.'),TIP('Received From P.U.P.'),UPR
                             BUTTON,AT(584,108),USE(?CallLookup:3),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Send To R.R.C.'),AT(68,134),USE(?tmp:SendToRRC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,132,124,10),USE(tmp:SendToRRC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Send To R.R.C.'),TIP('Send To R.R.C.'),UPR
                             BUTTON,AT(296,128),USE(?SendToRRC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Despatch To P.U.P.'),AT(344,132),USE(?tmp:DespatchToPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,132,124,10),USE(tmp:DespatchToPUP),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Despatch To P.U.P.'),TIP('Despatch To P.U.P.'),UPR
                             BUTTON,AT(584,128),USE(?CallLookup:4),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Despatched To R.R.C.'),AT(68,152),USE(?tmp:DespatchedToRRC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,154,124,10),USE(tmp:DespatchedToRRC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Despatched To R.R.C.'),TIP('Despatched To R.R.C.'),UPR
                             BUTTON,AT(296,150),USE(?DespatchedToRRC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Sent To P.U.P.'),AT(344,154),USE(?tmp:SentToPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,154,124,10),USE(tmp:SentToPUP),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Sent To P.U.P.'),TIP('Sent To P.U.P.'),UPR
                             BUTTON,AT(584,152),USE(?CallLookup:5),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Received At R.R.C.'),AT(68,174),USE(?tmp:ReceivedAtRRC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,174,124,10),USE(tmp:ReceivedAtRRC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Received At R.R.C.'),TIP('Received At R.R.C.'),UPR
                             BUTTON,AT(296,170),USE(?ReceivedAtRRC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Received At P.U.P.'),AT(344,174),USE(?tmp:ReceivedAtPUP:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,174,124,10),USE(tmp:ReceivedAtPUP),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Received At P.U.P.'),TIP('Received At P.U.P.'),UPR
                             BUTTON,AT(584,170),USE(?CallLookup:6),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('A.R.C. Received Query'),AT(68,192),USE(?tmp:ARCReceivedQueue:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,194,124,10),USE(tmp:ARCReceivedQuery),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('A.R.C. Received Query'),TIP('A.R.C. Received Query'),UPR
                             BUTTON,AT(296,190),USE(?ARCReceived),SKIP,TRN,FLAT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ICON('lookupp.jpg')
                             PROMPT('Despatch To P.U.P. Customer'),AT(344,194),USE(?tmp:DespatchedToPUPCustomer:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,194,124,10),USE(tmp:DespatchedToPUPCustomer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Despatch To P.U.P. Customer'),TIP('Despatch To P.U.P. Customer'),UPR
                             BUTTON,AT(584,192),USE(?CallLookup:7),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('R.R.C. Received Query'),AT(68,214),USE(?tmp:RRCReceivedQuery:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,214,124,10),USE(tmp:RRCReceivedQuery),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('R.R.C. Received Query'),TIP('R.R.C. Received Query'),UPR
                             BUTTON,AT(296,208),USE(?RRCReceived),SKIP,TRN,FLAT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ICON('lookupp.jpg')
                             PROMPT('VCP Transit Type'),AT(344,214),USE(?tmp:VCPDefaultTransitType:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(456,214,124,10),USE(tmp:VCPDefaultTransitType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('VCP Transit Type'),TIP('VCP Transit Type'),UPR
                             BUTTON,AT(584,210),USE(?CallLookup:8),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Exchange Despatched To R.R.C.'),AT(68,232,96,16),USE(?tmp:ExcDespatchedToRRC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,234,124,10),USE(tmp:ExcDespatchedToRRC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Exchange Despatch To R.R.C.'),TIP('Exchange Despatch To R.R.C.'),UPR
                             BUTTON,AT(296,230),USE(?LookupExcDespatchedStatus),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Exchange Received At R.R.C.'),AT(68,252,76,16),USE(?tmp:ExcReceivedAtRRC:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(168,254,124,10),USE(tmp:ExcReceivedAtRRC),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Exchange Received At R.R.C.'),TIP('Exchange Received At R.R.C.'),UPR
                             BUTTON,AT(296,248),USE(?LookupExcReceivedRRC),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           END
                         END
                       END
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('R.R.C. Defaults'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:RRCLocation                Like(tmp:RRCLocation)
look:tmp:InTransit                Like(tmp:InTransit)
look:tmp:ARCLocation                Like(tmp:ARCLocation)
look:tmp:InTransitRRC                Like(tmp:InTransitRRC)
look:tmp:SendToCustomer                Like(tmp:SendToCustomer)
look:tmp:RTMLocation                Like(tmp:RTMLocation)
look:tmp:SendToARC                Like(tmp:SendToARC)
look:tmp:BookedATPUP                Like(tmp:BookedATPUP)
look:tmp:DespatchedToARC                Like(tmp:DespatchedToARC)
look:tmp:SentFromPUP                Like(tmp:SentFromPUP)
look:tmp:ReceivedAtARC                Like(tmp:ReceivedAtARC)
look:tmp:ReceivedFromPUP                Like(tmp:ReceivedFromPUP)
look:tmp:SendToRRC                Like(tmp:SendToRRC)
look:tmp:DespatchToPUP                Like(tmp:DespatchToPUP)
look:tmp:DespatchedToRRC                Like(tmp:DespatchedToRRC)
look:tmp:SentToPUP                Like(tmp:SentToPUP)
look:tmp:ReceivedAtRRC                Like(tmp:ReceivedAtRRC)
look:tmp:ReceivedAtPUP                Like(tmp:ReceivedAtPUP)
look:tmp:ARCReceivedQuery                Like(tmp:ARCReceivedQuery)
look:tmp:DespatchedToPUPCustomer                Like(tmp:DespatchedToPUPCustomer)
look:tmp:RRCReceivedQuery                Like(tmp:RRCReceivedQuery)
look:tmp:VCPDefaultTransitType                Like(tmp:VCPDefaultTransitType)
look:tmp:ExcDespatchedToRRC                Like(tmp:ExcDespatchedToRRC)
look:tmp:ExcReceivedAtRRC                Like(tmp:ExcReceivedAtRRC)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020132'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RRCDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCINTER.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
  
      ?tmp:RRCLocation{prop:Disable} = False
      ?tmp:InTransit{prop:Disable} = False
      ?tmp:ARCLocation{prop:Disable} = False
      ?tmp:InTransitRRC{prop:Disable} = False
      ?tmp:SendToCustomer{prop:Disable} = False
      ?tmp:RTMLocation{prop:Disable} = False
      ?tmp:AtPUPLocation{prop:Disable} = False
      ?tmp:InTransitFromPUP{prop:Disable} = False
      ?tmp:InTransitToPUP{prop:Disable} = False
      ?Group2{prop:Disable}= False
  
  End !PassAccount(glo:PassAccount,glo:Password) = Level:Benign
  
  tmp:RRCLocation = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:InTransit   = GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ARCLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:RTMLocation = GETINI('RRC','RTMLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:InTransitRRC = GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:SendToCustomer = GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')
  
  tmp:SendToARC   = GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:DespatchedToARC   = GETINI('RRC','StatusDespatchedToARC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ReceivedAtARC   = GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:SendToRRC    = GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:DespatchedToRRC = GETINI('RRC','StatusDespatchedToRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ReceivedAtRRC   = GETINI('RRC','StatusReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ARCReceivedQuery    = GETINI('RRC','StatusARCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:RRCReceivedQuery    = GETINI('RRC','StatusRRCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI')
  
  tmp:ExcDespatchedToRRC  = GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ExcReceivedAtRRC    = GETINI('RRC','ExchangeStatusReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  
  tmp:AtPUPLocation   = GETINI('RRC','AtPUPLocation','AT PUP',Clip(Path()) & '\SB2KDEF.INI')
  tmp:InTransitFromPUP    = GETINI('RRC','InTransitFromPUPLocation','IN TRANSIT FROM PUP',Clip(Path()) & '\SB2KDEF.INI')
  tmp:InTransitToPUP  = GETINI('RRC','InTransitToPUPLocation','IN TRANSIT TO PUP',Clip(Path()) & '\SB2KDEF.INI')
  tmp:BookedATPUP = GETINI('RRC','StatusBookingInAtPUP',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:SentFromPUP = GETINI('RRC','StatusSentFromPUP',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:ReceivedFromPUP = GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:DespatchToPUP   = GETINI('RRC','StatusDespatchToPUP',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:SentToPUP       = GETINI('RRC','StatusSentToPUP',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:ReceivedAtPUP   = GETINI('RRC','StatusReceivedAtPUP',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:DespatchedToPUPCustomer = GETINI('RRC','StatusDespatchedToPUPCustomer',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:VCPDefaultTransitType = GETINI('RRC','VCPDefaultTransitType',,Clip(Path()) & '\SB2KDEF.INI')
  
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:SendToARC{Prop:Tip} AND ~?SendToARC{Prop:Tip}
     ?SendToARC{Prop:Tip} = 'Select ' & ?tmp:SendToARC{Prop:Tip}
  END
  IF ?tmp:SendToARC{Prop:Msg} AND ~?SendToARC{Prop:Msg}
     ?SendToARC{Prop:Msg} = 'Select ' & ?tmp:SendToARC{Prop:Msg}
  END
  IF ?tmp:DespatchedToARC{Prop:Tip} AND ~?DespatchedToARC{Prop:Tip}
     ?DespatchedToARC{Prop:Tip} = 'Select ' & ?tmp:DespatchedToARC{Prop:Tip}
  END
  IF ?tmp:DespatchedToARC{Prop:Msg} AND ~?DespatchedToARC{Prop:Msg}
     ?DespatchedToARC{Prop:Msg} = 'Select ' & ?tmp:DespatchedToARC{Prop:Msg}
  END
  IF ?tmp:ReceivedAtARC{Prop:Tip} AND ~?ReceivedAtARC{Prop:Tip}
     ?ReceivedAtARC{Prop:Tip} = 'Select ' & ?tmp:ReceivedAtARC{Prop:Tip}
  END
  IF ?tmp:ReceivedAtARC{Prop:Msg} AND ~?ReceivedAtARC{Prop:Msg}
     ?ReceivedAtARC{Prop:Msg} = 'Select ' & ?tmp:ReceivedAtARC{Prop:Msg}
  END
  IF ?tmp:SendToRRC{Prop:Tip} AND ~?SendToRRC{Prop:Tip}
     ?SendToRRC{Prop:Tip} = 'Select ' & ?tmp:SendToRRC{Prop:Tip}
  END
  IF ?tmp:SendToRRC{Prop:Msg} AND ~?SendToRRC{Prop:Msg}
     ?SendToRRC{Prop:Msg} = 'Select ' & ?tmp:SendToRRC{Prop:Msg}
  END
  IF ?tmp:DespatchedToRRC{Prop:Tip} AND ~?DespatchedToRRC{Prop:Tip}
     ?DespatchedToRRC{Prop:Tip} = 'Select ' & ?tmp:DespatchedToRRC{Prop:Tip}
  END
  IF ?tmp:DespatchedToRRC{Prop:Msg} AND ~?DespatchedToRRC{Prop:Msg}
     ?DespatchedToRRC{Prop:Msg} = 'Select ' & ?tmp:DespatchedToRRC{Prop:Msg}
  END
  IF ?tmp:ReceivedAtRRC{Prop:Tip} AND ~?ReceivedAtRRC{Prop:Tip}
     ?ReceivedAtRRC{Prop:Tip} = 'Select ' & ?tmp:ReceivedAtRRC{Prop:Tip}
  END
  IF ?tmp:ReceivedAtRRC{Prop:Msg} AND ~?ReceivedAtRRC{Prop:Msg}
     ?ReceivedAtRRC{Prop:Msg} = 'Select ' & ?tmp:ReceivedAtRRC{Prop:Msg}
  END
  IF ?tmp:ARCReceivedQuery{Prop:Tip} AND ~?ARCReceived{Prop:Tip}
     ?ARCReceived{Prop:Tip} = 'Select ' & ?tmp:ARCReceivedQuery{Prop:Tip}
  END
  IF ?tmp:ARCReceivedQuery{Prop:Msg} AND ~?ARCReceived{Prop:Msg}
     ?ARCReceived{Prop:Msg} = 'Select ' & ?tmp:ARCReceivedQuery{Prop:Msg}
  END
  IF ?tmp:RRCReceivedQuery{Prop:Tip} AND ~?RRCReceived{Prop:Tip}
     ?RRCReceived{Prop:Tip} = 'Select ' & ?tmp:RRCReceivedQuery{Prop:Tip}
  END
  IF ?tmp:RRCReceivedQuery{Prop:Msg} AND ~?RRCReceived{Prop:Msg}
     ?RRCReceived{Prop:Msg} = 'Select ' & ?tmp:RRCReceivedQuery{Prop:Msg}
  END
  IF ?tmp:ExcDespatchedToRRC{Prop:Tip} AND ~?LookupExcDespatchedStatus{Prop:Tip}
     ?LookupExcDespatchedStatus{Prop:Tip} = 'Select ' & ?tmp:ExcDespatchedToRRC{Prop:Tip}
  END
  IF ?tmp:ExcDespatchedToRRC{Prop:Msg} AND ~?LookupExcDespatchedStatus{Prop:Msg}
     ?LookupExcDespatchedStatus{Prop:Msg} = 'Select ' & ?tmp:ExcDespatchedToRRC{Prop:Msg}
  END
  IF ?tmp:ExcReceivedAtRRC{Prop:Tip} AND ~?LookupExcReceivedRRC{Prop:Tip}
     ?LookupExcReceivedRRC{Prop:Tip} = 'Select ' & ?tmp:ExcReceivedAtRRC{Prop:Tip}
  END
  IF ?tmp:ExcReceivedAtRRC{Prop:Msg} AND ~?LookupExcReceivedRRC{Prop:Msg}
     ?LookupExcReceivedRRC{Prop:Msg} = 'Select ' & ?tmp:ExcReceivedAtRRC{Prop:Msg}
  END
  IF ?tmp:BookedATPUP{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:BookedATPUP{Prop:Tip}
  END
  IF ?tmp:BookedATPUP{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:BookedATPUP{Prop:Msg}
  END
  IF ?tmp:SentFromPUP{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:SentFromPUP{Prop:Tip}
  END
  IF ?tmp:SentFromPUP{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:SentFromPUP{Prop:Msg}
  END
  IF ?tmp:ReceivedFromPUP{Prop:Tip} AND ~?CallLookup:3{Prop:Tip}
     ?CallLookup:3{Prop:Tip} = 'Select ' & ?tmp:ReceivedFromPUP{Prop:Tip}
  END
  IF ?tmp:ReceivedFromPUP{Prop:Msg} AND ~?CallLookup:3{Prop:Msg}
     ?CallLookup:3{Prop:Msg} = 'Select ' & ?tmp:ReceivedFromPUP{Prop:Msg}
  END
  IF ?tmp:DespatchToPUP{Prop:Tip} AND ~?CallLookup:4{Prop:Tip}
     ?CallLookup:4{Prop:Tip} = 'Select ' & ?tmp:DespatchToPUP{Prop:Tip}
  END
  IF ?tmp:DespatchToPUP{Prop:Msg} AND ~?CallLookup:4{Prop:Msg}
     ?CallLookup:4{Prop:Msg} = 'Select ' & ?tmp:DespatchToPUP{Prop:Msg}
  END
  IF ?tmp:SentToPUP{Prop:Tip} AND ~?CallLookup:5{Prop:Tip}
     ?CallLookup:5{Prop:Tip} = 'Select ' & ?tmp:SentToPUP{Prop:Tip}
  END
  IF ?tmp:SentToPUP{Prop:Msg} AND ~?CallLookup:5{Prop:Msg}
     ?CallLookup:5{Prop:Msg} = 'Select ' & ?tmp:SentToPUP{Prop:Msg}
  END
  IF ?tmp:ReceivedAtPUP{Prop:Tip} AND ~?CallLookup:6{Prop:Tip}
     ?CallLookup:6{Prop:Tip} = 'Select ' & ?tmp:ReceivedAtPUP{Prop:Tip}
  END
  IF ?tmp:ReceivedAtPUP{Prop:Msg} AND ~?CallLookup:6{Prop:Msg}
     ?CallLookup:6{Prop:Msg} = 'Select ' & ?tmp:ReceivedAtPUP{Prop:Msg}
  END
  IF ?tmp:DespatchedToPUPCustomer{Prop:Tip} AND ~?CallLookup:7{Prop:Tip}
     ?CallLookup:7{Prop:Tip} = 'Select ' & ?tmp:DespatchedToPUPCustomer{Prop:Tip}
  END
  IF ?tmp:DespatchedToPUPCustomer{Prop:Msg} AND ~?CallLookup:7{Prop:Msg}
     ?CallLookup:7{Prop:Msg} = 'Select ' & ?tmp:DespatchedToPUPCustomer{Prop:Msg}
  END
  IF ?tmp:VCPDefaultTransitType{Prop:Tip} AND ~?CallLookup:8{Prop:Tip}
     ?CallLookup:8{Prop:Tip} = 'Select ' & ?tmp:VCPDefaultTransitType{Prop:Tip}
  END
  IF ?tmp:VCPDefaultTransitType{Prop:Msg} AND ~?CallLookup:8{Prop:Msg}
     ?CallLookup:8{Prop:Msg} = 'Select ' & ?tmp:VCPDefaultTransitType{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCINTER.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickAvailableLocations
      PickAvailableLocations
      PickAvailableLocations
      PickAvailableLocations
      PickAvailableLocations
      PickAvailableLocations
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      PickAllJobStatus
      Browse_Transit_Types
      PickAllJobStatus
      PickAllJobStatus
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020132'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020132'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020132'&'0')
      ***
    OF ?tmp:RRCLocation
      IF tmp:RRCLocation OR ?tmp:RRCLocation{Prop:Req}
        loi:Location = tmp:RRCLocation
        !Save Lookup Field Incase Of error
        look:tmp:RRCLocation        = tmp:RRCLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:RRCLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:RRCLocation = look:tmp:RRCLocation
            SELECT(?tmp:RRCLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:InTransit
      IF tmp:InTransit OR ?tmp:InTransit{Prop:Req}
        loi:Location = tmp:InTransit
        !Save Lookup Field Incase Of error
        look:tmp:InTransit        = tmp:InTransit
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:InTransit = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:InTransit = look:tmp:InTransit
            SELECT(?tmp:InTransit)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:ARCLocation
      IF tmp:ARCLocation OR ?tmp:ARCLocation{Prop:Req}
        loi:Location = tmp:ARCLocation
        !Save Lookup Field Incase Of error
        look:tmp:ARCLocation        = tmp:ARCLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:ARCLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:ARCLocation = look:tmp:ARCLocation
            SELECT(?tmp:ARCLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:InTransitRRC
      IF tmp:InTransitRRC OR ?tmp:InTransitRRC{Prop:Req}
        loi:Location = tmp:InTransitRRC
        !Save Lookup Field Incase Of error
        look:tmp:InTransitRRC        = tmp:InTransitRRC
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:InTransitRRC = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:InTransitRRC = look:tmp:InTransitRRC
            SELECT(?tmp:InTransitRRC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:SendToCustomer
      IF tmp:SendToCustomer OR ?tmp:SendToCustomer{Prop:Req}
        loi:Location = tmp:SendToCustomer
        !Save Lookup Field Incase Of error
        look:tmp:SendToCustomer        = tmp:SendToCustomer
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:SendToCustomer = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:SendToCustomer = look:tmp:SendToCustomer
            SELECT(?tmp:SendToCustomer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:RTMLocation
      IF tmp:RTMLocation OR ?tmp:RTMLocation{Prop:Req}
        loi:Location = tmp:RTMLocation
        !Save Lookup Field Incase Of error
        look:tmp:RTMLocation        = tmp:RTMLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:RTMLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:RTMLocation = look:tmp:RTMLocation
            SELECT(?tmp:RTMLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:SendToARC
      IF tmp:SendToARC OR ?tmp:SendToARC{Prop:Req}
        sts:Status = tmp:SendToARC
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:SendToARC        = tmp:SendToARC
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:SendToARC = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:SendToARC = look:tmp:SendToARC
            SELECT(?tmp:SendToARC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?SendToARC
      ThisWindow.Update
      sts:Status = tmp:SendToARC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:SendToARC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:SendToARC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SendToARC)
    OF ?tmp:BookedATPUP
      IF tmp:BookedATPUP OR ?tmp:BookedATPUP{Prop:Req}
        sts:Status = tmp:BookedATPUP
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:BookedATPUP        = tmp:BookedATPUP
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:BookedATPUP = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:BookedATPUP = look:tmp:BookedATPUP
            SELECT(?tmp:BookedATPUP)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      sts:Status = tmp:BookedATPUP
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:BookedATPUP = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:BookedATPUP)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:BookedATPUP)
    OF ?tmp:DespatchedToARC
      IF tmp:DespatchedToARC OR ?tmp:DespatchedToARC{Prop:Req}
        sts:Status = tmp:DespatchedToARC
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:DespatchedToARC        = tmp:DespatchedToARC
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:DespatchedToARC = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:DespatchedToARC = look:tmp:DespatchedToARC
            SELECT(?tmp:DespatchedToARC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DespatchedToARC
      ThisWindow.Update
      sts:Status = tmp:DespatchedToARC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:DespatchedToARC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:DespatchedToARC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:DespatchedToARC)
    OF ?tmp:SentFromPUP
      IF tmp:SentFromPUP OR ?tmp:SentFromPUP{Prop:Req}
        sts:Status = tmp:SentFromPUP
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:SentFromPUP        = tmp:SentFromPUP
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:SentFromPUP = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:SentFromPUP = look:tmp:SentFromPUP
            SELECT(?tmp:SentFromPUP)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      sts:Status = tmp:SentFromPUP
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:SentFromPUP = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:SentFromPUP)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SentFromPUP)
    OF ?tmp:ReceivedAtARC
      IF tmp:ReceivedAtARC OR ?tmp:ReceivedAtARC{Prop:Req}
        sts:Status = tmp:ReceivedAtARC
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ReceivedAtARC        = tmp:ReceivedAtARC
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ReceivedAtARC = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:ReceivedAtARC = look:tmp:ReceivedAtARC
            SELECT(?tmp:ReceivedAtARC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?ReceivedAtARC
      ThisWindow.Update
      sts:Status = tmp:ReceivedAtARC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ReceivedAtARC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ReceivedAtARC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ReceivedAtARC)
    OF ?tmp:ReceivedFromPUP
      IF tmp:ReceivedFromPUP OR ?tmp:ReceivedFromPUP{Prop:Req}
        sts:Status = tmp:ReceivedFromPUP
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ReceivedFromPUP        = tmp:ReceivedFromPUP
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ReceivedFromPUP = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:ReceivedFromPUP = look:tmp:ReceivedFromPUP
            SELECT(?tmp:ReceivedFromPUP)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:3
      ThisWindow.Update
      sts:Status = tmp:ReceivedFromPUP
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ReceivedFromPUP = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ReceivedFromPUP)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ReceivedFromPUP)
    OF ?tmp:SendToRRC
      IF tmp:SendToRRC OR ?tmp:SendToRRC{Prop:Req}
        sts:Status = tmp:SendToRRC
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:SendToRRC        = tmp:SendToRRC
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:SendToRRC = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:SendToRRC = look:tmp:SendToRRC
            SELECT(?tmp:SendToRRC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?SendToRRC
      ThisWindow.Update
      sts:Status = tmp:SendToRRC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:SendToRRC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:SendToRRC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SendToRRC)
    OF ?tmp:DespatchToPUP
      IF tmp:DespatchToPUP OR ?tmp:DespatchToPUP{Prop:Req}
        sts:Status = tmp:DespatchToPUP
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:DespatchToPUP        = tmp:DespatchToPUP
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:DespatchToPUP = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:DespatchToPUP = look:tmp:DespatchToPUP
            SELECT(?tmp:DespatchToPUP)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update
      sts:Status = tmp:DespatchToPUP
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:DespatchToPUP = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:DespatchToPUP)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:DespatchToPUP)
    OF ?tmp:DespatchedToRRC
      IF tmp:DespatchedToRRC OR ?tmp:DespatchedToRRC{Prop:Req}
        sts:Status = tmp:DespatchedToRRC
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:DespatchedToRRC        = tmp:DespatchedToRRC
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:DespatchedToRRC = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:DespatchedToRRC = look:tmp:DespatchedToRRC
            SELECT(?tmp:DespatchedToRRC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DespatchedToRRC
      ThisWindow.Update
      sts:Status = tmp:DespatchedToRRC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:DespatchedToRRC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:DespatchedToRRC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:DespatchedToRRC)
    OF ?tmp:SentToPUP
      IF tmp:SentToPUP OR ?tmp:SentToPUP{Prop:Req}
        sts:Status = tmp:SentToPUP
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:SentToPUP        = tmp:SentToPUP
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:SentToPUP = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:SentToPUP = look:tmp:SentToPUP
            SELECT(?tmp:SentToPUP)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update
      sts:Status = tmp:SentToPUP
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:SentToPUP = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:SentToPUP)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SentToPUP)
    OF ?tmp:ReceivedAtRRC
      IF tmp:ReceivedAtRRC OR ?tmp:ReceivedAtRRC{Prop:Req}
        sts:Status = tmp:ReceivedAtRRC
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ReceivedAtRRC        = tmp:ReceivedAtRRC
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ReceivedAtRRC = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:ReceivedAtRRC = look:tmp:ReceivedAtRRC
            SELECT(?tmp:ReceivedAtRRC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?ReceivedAtRRC
      ThisWindow.Update
      sts:Status = tmp:ReceivedAtRRC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ReceivedAtRRC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ReceivedAtRRC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ReceivedAtRRC)
    OF ?tmp:ReceivedAtPUP
      IF tmp:ReceivedAtPUP OR ?tmp:ReceivedAtPUP{Prop:Req}
        sts:Status = tmp:ReceivedAtPUP
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ReceivedAtPUP        = tmp:ReceivedAtPUP
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ReceivedAtPUP = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:ReceivedAtPUP = look:tmp:ReceivedAtPUP
            SELECT(?tmp:ReceivedAtPUP)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:6
      ThisWindow.Update
      sts:Status = tmp:ReceivedAtPUP
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ReceivedAtPUP = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ReceivedAtPUP)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ReceivedAtPUP)
    OF ?tmp:ARCReceivedQuery
      IF tmp:ARCReceivedQuery OR ?tmp:ARCReceivedQuery{Prop:Req}
        sts:Status = tmp:ARCReceivedQuery
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ARCReceivedQuery        = tmp:ARCReceivedQuery
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ARCReceivedQuery = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:ARCReceivedQuery = look:tmp:ARCReceivedQuery
            SELECT(?tmp:ARCReceivedQuery)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?ARCReceived
      ThisWindow.Update
      sts:Status = tmp:ARCReceivedQuery
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ARCReceivedQuery = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ARCReceivedQuery)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ARCReceivedQuery)
    OF ?tmp:DespatchedToPUPCustomer
      IF tmp:DespatchedToPUPCustomer OR ?tmp:DespatchedToPUPCustomer{Prop:Req}
        sts:Status = tmp:DespatchedToPUPCustomer
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:DespatchedToPUPCustomer        = tmp:DespatchedToPUPCustomer
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:DespatchedToPUPCustomer = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:DespatchedToPUPCustomer = look:tmp:DespatchedToPUPCustomer
            SELECT(?tmp:DespatchedToPUPCustomer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:7
      ThisWindow.Update
      sts:Status = tmp:DespatchedToPUPCustomer
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:DespatchedToPUPCustomer = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:DespatchedToPUPCustomer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:DespatchedToPUPCustomer)
    OF ?tmp:RRCReceivedQuery
      IF tmp:RRCReceivedQuery OR ?tmp:RRCReceivedQuery{Prop:Req}
        sts:Status = tmp:RRCReceivedQuery
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:RRCReceivedQuery        = tmp:RRCReceivedQuery
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:RRCReceivedQuery = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:RRCReceivedQuery = look:tmp:RRCReceivedQuery
            SELECT(?tmp:RRCReceivedQuery)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?RRCReceived
      ThisWindow.Update
      sts:Status = tmp:RRCReceivedQuery
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:RRCReceivedQuery = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:RRCReceivedQuery)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:RRCReceivedQuery)
    OF ?tmp:VCPDefaultTransitType
      IF tmp:VCPDefaultTransitType OR ?tmp:VCPDefaultTransitType{Prop:Req}
        trt:Transit_Type = tmp:VCPDefaultTransitType
        !Save Lookup Field Incase Of error
        look:tmp:VCPDefaultTransitType        = tmp:VCPDefaultTransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(22,SelectRecord) = RequestCompleted
            tmp:VCPDefaultTransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            tmp:VCPDefaultTransitType = look:tmp:VCPDefaultTransitType
            SELECT(?tmp:VCPDefaultTransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:8
      ThisWindow.Update
      trt:Transit_Type = tmp:VCPDefaultTransitType
      
      IF SELF.RUN(22,Selectrecord)  = RequestCompleted
          tmp:VCPDefaultTransitType = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?tmp:VCPDefaultTransitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:VCPDefaultTransitType)
    OF ?tmp:ExcDespatchedToRRC
      IF tmp:ExcDespatchedToRRC OR ?tmp:ExcDespatchedToRRC{Prop:Req}
        sts:Status = tmp:ExcDespatchedToRRC
        sts:Exchange = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ExcDespatchedToRRC        = tmp:ExcDespatchedToRRC
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ExcDespatchedToRRC = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            !Restore Lookup On Error
            tmp:ExcDespatchedToRRC = look:tmp:ExcDespatchedToRRC
            SELECT(?tmp:ExcDespatchedToRRC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExcDespatchedStatus
      ThisWindow.Update
      sts:Status = tmp:ExcDespatchedToRRC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ExcDespatchedToRRC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ExcDespatchedToRRC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ExcDespatchedToRRC)
    OF ?tmp:ExcReceivedAtRRC
      IF tmp:ExcReceivedAtRRC OR ?tmp:ExcReceivedAtRRC{Prop:Req}
        sts:Status = tmp:ExcReceivedAtRRC
        sts:Exchange = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ExcReceivedAtRRC        = tmp:ExcReceivedAtRRC
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            tmp:ExcReceivedAtRRC = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            !Restore Lookup On Error
            tmp:ExcReceivedAtRRC = look:tmp:ExcReceivedAtRRC
            SELECT(?tmp:ExcReceivedAtRRC)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExcReceivedRRC
      ThisWindow.Update
      sts:Status = tmp:ExcReceivedAtRRC
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          tmp:ExcReceivedAtRRC = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ExcReceivedAtRRC)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ExcReceivedAtRRC)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      PUTINI('RRC','RRCLocation',tmp:RRCLocation,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','InTransit',tmp:InTransit,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','ARCLocation',tmp:ARCLocation,CLIP(PATH()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','RTMLocation',tmp:RTMLocation,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','InTransitRRC',tmp:InTransitRRC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','DespatchToCustomer',tmp:SendToCustomer,CLIP(PATH()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','StatusSendToARC',tmp:SendToARC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusDespatchedToARC',tmp:DespatchedToARC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusReceivedAtARC',tmp:ReceivedAtARC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusSendToRRC',tmp:SendToRRC,CLIP(PATH()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','StatusDespatchedToRRC',tmp:DespatchedToRRC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusReceivedAtRRC',tmp:ReceivedAtRRC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusARCReceivedQuery',tmp:ARCReceivedQuery,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusRRCReceivedQuery',tmp:RRCReceivedQuery ,CLIP(PATH()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','ExchangeStatusDespatchToRRC',tmp:ExcDespatchedToRRC,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RRC','ExchangeStatusReceivedAtRRC',tmp:ExcReceivedAtRRC,CLIP(PATH()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','AtPUPLocation',tmp:AtPUPLocation,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','InTransitFromPUPLocation',tmp:InTransitFromPUP,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','InTransitToPUPLocation',tmp:InTransitToPUP,Clip(Path()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','StatusBookingInAtPUP',tmp:BookedATPUP,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusSentFromPUP',tmp:SentFromPUP,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusReceivedFromPUP',tmp:ReceivedFromPUP,Clip(Path()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','StatusDespatchToPUP',tmp:DespatchToPUP,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusSentToPUP',tmp:SentToPUP,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusReceivedAtPUP',tmp:ReceivedAtPUP,Clip(Path()) & '\SB2KDEF.INI')
      PUTINI('RRC','StatusDespatchedToPUPCustomer',tmp:DespatchedToPUPCustomer,Clip(Path()) & '\SB2KDEF.INI')
      
      PUTINI('RRC','VCPDefaultTransitType',tmp:VCPDefaultTransitType,Clip(Path()) & '\SB2KDEF.INI')
      
      PutIniUpdateAll
      
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseDefaults PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:TradeOrder       BYTE(0)
tmp:UserOrder        BYTE(0)
tmp:FaultCodeOrder   BYTE(0)
window               WINDOW('Browse Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           OPTION('Trade Account Browse Order'),AT(200,145,280,36),USE(tmp:TradeOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Acc No'),AT(208,161),USE(?tmp:TradeOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('By Company Name'),AT(295,161),USE(?tmp:TradeOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             RADIO('By Branch'),AT(416,161),USE(?tmp:TradeOrder:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                           END
                           OPTION('Users Browse Order'),AT(200,185,280,36),USE(tmp:UserOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Surname'),AT(251,201),USE(?tmp:UserOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('By Forename'),AT(363,201),USE(?tmp:UserOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                           END
                           OPTION('Fault Codes Browse Order'),AT(200,225,280,36),USE(tmp:FaultCodeOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Fault Code'),AT(251,241),USE(?tmp:UserOrder:Radio1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('By Description'),AT(363,241),USE(?tmp:UserOrder:Radio2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                           END
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Order Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(384,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020124'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:TradeOrder:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  tmp:TradeOrder = GETINI('BROWSEORDER','TradeAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:UserOrder   = GETINI('BROWSEORDER','Users',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:FaultCodeOrder   = GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI')
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020124'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020124'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020124'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      PUTINIext('BROWSEORDER','TradeAccount',tmp:TradeOrder,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('BROWSEORDER','Users',tmp:UserOrder,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('BROWSEORDER','FaultCode',tmp:FaultCodeOrder,CLIP(PATH()) & '\SB2KDEF.INI')
      
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BastionDefaults PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:WSDL             STRING(255)
tmp:UserID           STRING(30)
tmp:Password         STRING(30)
window               WINDOW('Bastion Interface Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Bastion Interface Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Details'),USE(?Tab1)
                           PROMPT('WSDL URL'),AT(241,167),USE(?tmp:WSDL:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(291,167,148,10),USE(tmp:WSDL),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('User ID'),AT(241,188),USE(?tmp:UserID:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(291,188,148,10),USE(tmp:UserID),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Password'),AT(241,210),USE(?tmp:Password:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(291,210,148,10),USE(tmp:Password),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020142'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BastionDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:WSDL     = GETINI('SoapClient','WSDL')
  tmp:UserID   = GETINI('SoapClient','UserID')
  tmp:Password = GETINI('SoapClient','Password')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      PUTINIext('SoapClient', 'WSDL', tmp:WSDL,'')
      PUTINIext('SoapClient', 'UserID', tmp:UserID,'')
      PUTINIext('SoapClient', 'Password', tmp:Password,'')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020142'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020142'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020142'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Unlock_Jobs PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Batch_Number)
                       PROJECT(job:Internal_Status)
                       PROJECT(job:Auto_Search)
                       PROJECT(job:who_booked)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                       PROJECT(job:Cancelled)
                       PROJECT(job:Bouncer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Batch_Number       LIKE(job:Batch_Number)         !List box control field - type derived from field
job:Internal_Status    LIKE(job:Internal_Status)      !List box control field - type derived from field
job:Auto_Search        LIKE(job:Auto_Search)          !List box control field - type derived from field
job:who_booked         LIKE(job:who_booked)           !List box control field - type derived from field
job:date_booked        LIKE(job:date_booked)          !List box control field - type derived from field
job:time_booked        LIKE(job:time_booked)          !List box control field - type derived from field
job:Cancelled          LIKE(job:Cancelled)            !List box control field - type derived from field
job:Bouncer            LIKE(job:Bouncer)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the JOBS File'),AT(,,411,170),FONT('MS Sans Serif',8,,),IMM,HLP('Unlock_Jobs'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,20,342,146),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('44L(2)|M~Job Number~@s8@52L(2)|M~Batch Number~@s8@64L(2)|M~Internal Status~@s10@' &|
   '80L(2)|M~Auto Search~@s30@44L(2)|M~who booked~@s3@80R(2)|M~date booked~C(0)@d6b@' &|
   '80R(2)|M~time booked~C(0)@t1@40L(2)|M~Cancelled~@s3@36L(2)|M~Bouncer~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('Button 2'),AT(360,19,45,14),USE(?Button2)
                       SHEET,AT(4,4,350,162),USE(?CurrentTab)
                         TAB('By Job Number'),USE(?Tab:2)
                         END
                       END
                       BUTTON('Close'),AT(364,152,45,14),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Unlock_Jobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job:Ref_Number,1,BRW1)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Batch_Number,BRW1.Q.job:Batch_Number)
  BRW1.AddField(job:Internal_Status,BRW1.Q.job:Internal_Status)
  BRW1.AddField(job:Auto_Search,BRW1.Q.job:Auto_Search)
  BRW1.AddField(job:who_booked,BRW1.Q.job:who_booked)
  BRW1.AddField(job:date_booked,BRW1.Q.job:date_booked)
  BRW1.AddField(job:time_booked,BRW1.Q.job:time_booked)
  BRW1.AddField(job:Cancelled,BRW1.Q.job:Cancelled)
  BRW1.AddField(job:Bouncer,BRW1.Q.job:Bouncer)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      BRW1.UpdateBuffer()
      RELEASE(jobs)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


UnLockSBOnlineJobs PROCEDURE                          !Generated from procedure template - Browse

BRW9::View:Browse    VIEW(JOBSLOCK)
                       PROJECT(lock:JobNumber)
                       PROJECT(lock:DateLocked)
                       PROJECT(lock:TimeLocked)
                       PROJECT(lock:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
lock:JobNumber         LIKE(lock:JobNumber)           !List box control field - type derived from field
lock:DateLocked        LIKE(lock:DateLocked)          !List box control field - type derived from field
lock:TimeLocked        LIKE(lock:TimeLocked)          !List box control field - type derived from field
lock:RecordNumber      LIKE(lock:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Unlock SBOnline Jobs'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Unlock SB Online Jobs'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:000000'),AT(468,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       SHEET,AT(164,82,352,250),USE(?Sheet1),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Locked SBOnline Jobs'),USE(?Tab1),FONT(,,,FONT:bold)
                           ENTRY(@s8),AT(212,100,72,10),USE(lock:JobNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           LIST,AT(212,114,256,212),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('61R(2)|M~Job Number~L@s8@81R(2)|M~Date Locked~L@d17@32R(2)|M~Time Locked~L@t7@'),FROM(Queue:Browse)
                         END
                       END
                       PANEL,AT(164,334,352,26),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(168,334),USE(?buttonUnlockJob),TRN,FLAT,ICON('unlockp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020735'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UnLockSBOnlineJobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBSLOCK.Open
  SELF.FilesOpened = True
      setcursor(cursor:Wait)
      Prog.ProgressSetup(Records(JOBSLOCK))
      Access:JOBSLOCK.Clearkey(lock:RecordNumberKey)
      lock:RecordNumber = 1
      Set(lock:RecordNumberKey,lock:RecordNumberKey)
      Loop
          If (access:JOBSLOCK.Next())
              Break
          End
          prog.ProgressText('Updating....')
          If (prog.InsideLoop())
              Break
          end
          If (lock:DateLocked < Today())
              Access:JOBSLOCK.DeleteRecord(0)
          End ! If (lock:DateLocked < Today())
      End
      setcursor()
      Prog.ProgressFinish()
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:JOBSLOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,lock:JobNumberKey)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?lock:JobNumber,lock:JobNumber,1,BRW9)
  BRW9.AddField(lock:JobNumber,BRW9.Q.lock:JobNumber)
  BRW9.AddField(lock:DateLocked,BRW9.Q.lock:DateLocked)
  BRW9.AddField(lock:TimeLocked,BRW9.Q.lock:TimeLocked)
  BRW9.AddField(lock:RecordNumber,BRW9.Q.lock:RecordNumber)
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSLOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020735'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020735'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020735'&'0')
      ***
    OF ?buttonUnlockJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonUnlockJob, Accepted)
          BRW9.UpdateViewRecord()
          Beep(Beep:SystemQuestion)  ;  Yield()
          Case Missive('Warning! If you remove the lock from a job that IS ACTUALLY is use, you may result in data loss.'&|
              '|'&|
              '|Are you sure you want to remove the lock from the selected job?','ServiceBase',|
                         'mquest.jpg','\&No|/&Yes')
          Of 2 ! &Yes Button
              Access:JOBSLOCK.DeleteRecord(0)
              BRW9.ResetSort(1)
          Of 1 ! &No Button
          End!Case Message
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonUnlockJob, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  ! Disable Button If No Records
      If (NOT Records(Queue:Browse))
          ?buttonUnlockJob{prop:Disable} = 1
      Else
          ?buttonUnlockJob{prop:Disable} = 0
      End
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, ResetQueue, (BYTE ResetMode))


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Browse
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 10
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
