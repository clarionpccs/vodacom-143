

   MEMBER('vodr0093.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


OBFReport PROCEDURE                                   !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
tmp:Clipboard        ANY
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
TempFilePath         CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:LastDetailColumn STRING(2)
tmp:LastSummaryColumn STRING(2)
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
JobsBooked           LONG
JobsOpen             LONG
                     END
tmp:Tag              STRING(1)
tmp:LastDetailRow    LONG
AccountSummaryQueue  QUEUE,PRE(accque)
AccountNumber        STRING(30)
OBFSent              LONG
OBFAccepted          LONG
OBFRejected          LONG
                     END
UserNameQueue        QUEUE,PRE(useque)
UserCode             STRING(3)
JobsProcessed        LONG
                     END
ReasonQueue          QUEUE,PRE(reaque)
RecordNumber         LONG
Quantity             LONG
                     END
BRW12::View:Browse   VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('OBF Report'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Completed Date From'),AT(243,122),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(335,122,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(403,118),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Completed Date To'),AT(243,144),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(335,144,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           BUTTON,AT(403,142),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT(''),AT(68,154),USE(?StatusText)
                           LIST,AT(235,170,205,168),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@66L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(448,248),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(279,235,1,1),USE(?DASREVTAG),HIDE
                           BUTTON,AT(448,282),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('sho&W tags'),AT(283,267,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(448,314),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           PROMPT('Ensure Excel is NOT running before you begin!'),AT(238,342,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(72,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
WriteLine               Procedure()
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                   String(2000)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW12.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:RecordCount       Long()
local:Desktop           Cstring(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'OBF Report'
    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')

    glo:ExportFile = Clip(local:LocalPath) & 'ARC' & Clock() & '.CSV'
    Remove(glo:ExportFile)
    Create(ExportFile)
    Open(ExportFile)
    tmp:LastDetailRow = 12

    Loop x# = 1 To Records(glo:Queue)
        Get(glo:Queue,x#)

        Access:JOBSOBF.Clearkey(jof:HeadAccountCompletedKey)
        jof:HeadAccountNumber = glo:Pointer
        jof:DateCompleted = tmp:StartDate
        Set(jof:HeadAccountCompletedKey,jof:HeadAccountCompletedKey)
        Loop ! Begin Loop
            If Access:JOBSOBF.Next()
                Break
            End ! If Access:JOBSOBF.Next()
            If jof:HeadAccountNumber <> glo:Pointer
                Break
            End ! If jof:HeadAccountNumber <> glo:Pointer
            If jof:DateCompleted > tmp:EndDate
                Break
            End ! If jof:DateCompleted > tmp:EndDate

            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel = 1

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jof:RefNumber
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

            ! Criteria____________________________________________________________________________________
            !Found
            If jof:Status <> 2 And jof:Status <> 3
                Cycle
            End ! If jof:Status <> 2 And jof:Status <> 3

            Local.WriteLine()
            local:RecordCount += 1

            ! Write To Summary Queues ____________________________________________________________________________________
            accque:AccountNumber = jof:HeadAccountNumber
            Get(AccountSummaryQueue,accque:AccountNumber)
            If Error()
                accque:AccountNumber = jof:HeadAccountNumber
                accque:OBFSent = 1
                Case jof:Status
                Of 2
                    accque:OBFAccepted = 1
                    accque:OBFRejected = 0
                Of 3
                    accque:OBFAccepted = 0
                    accque:OBFRejected = 1
                End ! Case jof:Status
                Add(AccountSummaryQueue)
            Else ! If Error()
                accque:OBFSent += 1
                Case jof:Status
                Of 2
                    accque:OBFAccepted += 1
                Of 3
                    accque:OBFRejected += 1
                End ! Case jof:Status
                Put(AccountSummaryQueue)
            End ! If Error()

            useque:UserCode = jof:UserCode
            Get(UserNameQueue,useque:UserCode)
            If Error()
                useque:UserCode = jof:UserCode
                useque:JobsProcessed = 1
                Add(UserNameQueue)
            Else ! If Error()
                useque:JobsProcessed += 1
                Put(UserNameQueue)
            End ! If Error()

            If jof:Status = 3
                Found# = 0
                Access:OBFREASN.Clearkey(obf:RecordNumberKey)
                obf:RecordNumber = 1
                Set(obf:RecordNumberKey,obf:RecordNumberKey)
                Loop ! Begin Loop
                    If Access:OBFREASN.Next()
                        Break
                    End ! If Access:OBFREASN.Next()
                    If Instring(Clip(obf:RejectionText),Clip(jof:RejectionReason),1,1)
                        Found# = 1
                        reaque:RecordNumber = obf:RecordNumber
                        Get(ReasonQueue,reaque:RecordNumber)
                        If Error()
                            reaque:RecordNumber = obf:RecordNumber
                            reaque:Quantity = 1
                            Add(ReasonQueue)
                        Else ! If Error()
                            reaque:Quantity += 1
                            Put(ReasonQueue)
                        End ! If Error()
                    End ! If Instring(Clip(obf:RejectionText),Clip(jof:RejectionReason),1,1)
                End ! Loop
                If Found# = 0
                    ! Didn't find rejection text in the default list, therefore add to the "misc" list (DBH: 15/03/2007)
                    reaque:RecordNumber = 0
                    Get(Reasonqueue,reaque:RecordNumber)
                    If Error()
                        reaque:RecordNumber = 0
                        reaque:Quantity = 1
                        Add(ReasonQueue)
                    Else ! If Error()
                        reaque:Quantity += 1
                        Put(ReasonQueue)
                    End ! If Error()
                End ! If Found# = 0

            End ! If jof:Status = 3

        End ! Loop

    End ! Loop x# = 1 To Records(glo:Queue)
    Close(ExportFile)


    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        tmp:LastDetailColumn = 'X'

        SetClipBoard('')
        E1.OpenWorkBook(glo:ExportFile)
        E1.Copy('A1',Clip(tmp:LastDetailColumn) & local:RecordCount)
        tmp:Clipboard = ClipBoard()
        SetClipBoard('Blank')
        E1.CloseWorkBook(3)

        E1.NewWorkBook()
        E1.SelectCells('A12')
        SetClipBoard(tmp:ClipBoard)
        E1.Paste()
        SetClipBoard('Blank')
        E1.RenameWorkSheet(excel:ProgramName)

        Do DrawTitle

        local.DrawBox('A9',Clip(tmp:LastDetailColumn) & '9','A9',Clip(tmp:LastDetailColumn) & '9',color:Silver)
        local.DrawBox('A11',Clip(tmp:LastDetailColumn) & '11','A11',Clip(tmp:LastDetailColumn) & '11',color:Silver)

        E1.WriteToCell('Section Name','A9')
        E1.WriteToCell(excel:ProgramName,'B9')
        E1.WriteToCell('Total Records','E9')
        E1.WriteToCell(local:RecordCount,'F9')
        E1.WriteToCell('Showing','G9')
        E1.WriteToCell('=SUBTOTAL(2,A12:A' & local:RecordCount + 12,'H9')

        ! Define Column Titles (DBH: 01/08/2006)
        E1.WriteToCell('User Code'                  ,'A11')
        E1.WriteTOCell('User Surname'               ,'B11')
        E1.WriteToCell('User Forename'              ,'C11')
        E1.WriteToCell('SB Job Number'              ,'D11')
        E1.WriteToCell('Franchise Branch Number'    ,'E11')
        E1.WriteToCell('Franchse Job Number'        ,'F11')
        E1.WriteToCell('Date Booked'                ,'G11')
        E1.WriteToCell('Head Account Number'        ,'H11')
        E1.WriteToCell('Head Account Name'          ,'I11')
        E1.WriteToCell('Trade Account'              ,'J11')
        E1.WriteToCell('Trade Account Name'         ,'K11')
        E1.WriteToCell('Make'                       ,'L11')
        E1.WriteToCell('Model Number'               ,'M11')
        E1.WriteToCell('I.M.EI. Number'             ,'N11')
        E1.WriteToCell('M.S.N. Number'              ,'O11')
        E1.WriteToCell('Charge Type'                ,'P11')
        E1.WriteToCell('OBF Accepted'               ,'Q11')
        E1.WriteToCell('OBF Rejected'               ,'R11')
        E1.WriteToCell('Date Accepted/Rejected'     ,'S11')
        E1.WriteToCell('R Number'                   ,'T11')
        E1.WriteToCell('Rejection Reason'           ,'U11')
        E1.WriteToCell('Handling Fee'               ,'V11')
        E1.WriteToCell('V.A.T.'                     ,'W11')
        E1.WriteToCell('Total Handling Fee'         ,'X11')

        E1.SetCellFontName('Tahoma','A9',Clip(tmp:LastDetailColumn) & local:RecordCount + 12)
        E1.SetCellFontSize(8,'A1',Clip(tmp:LastDetailColumn) & local:RecordCount + 12)
        E1.SetCellFontStyle('Bold','A11',Clip(tmp:LastDetailColumn) & '11')
        E1.SetCellFontStyle('Bold','A9')
        E1.SetCellFontStyle('Bold','F9')
        E1.SetCellFontStyle('Bold','H9')
        E1.SetCellBackgroundColor(color:Silver,'A9',Clip(tmp:LastDetailColumn) & '9')
        E1.SetCellBackgroundColor(color:Silver,'A11',Clip(tmp:LastDetailColumn) & '11')
        E1.SetCellNumberFormat(oix:NumberFormatNumber, ,2,,'V12','X' & tmp:LastDetailRow)
        E1.SelectCells('N12','O' & tmp:LastDetailRow)
!        E1.ReplaceText ('$$', '<9>', 1)
        E1.SetCellNumberFormat(oix:NumberFormatNumber, ,0,,'N12','O' & tmp:LastDetailRow)


        E1.SetColumnWidth('A',Clip(tmp:LastDetailColumn))
        E1.SelectCells('D12')
        E1.FreezePanes()

        ! Create Summary Worksheet (DBH: 01/08/2006)
        E1.InsertWorksheet()
        E1.RenameWorkSheet('Summary')

        Do DrawTitle

        tmp:LastSummaryColumn = 'H'
        local.DrawBox('A9',Clip(tmp:LastSummaryColumn) & '9','A9',Clip(tmp:LastSummaryColumn) & '9',color:Silver)
        local.DrawBox('A10',Clip(tmp:LastSummaryColumn) & '10','A10',Clip(tmp:LastSummaryColumn) & '10',color:Silver)

        E1.WriteToCell('Summary','A9')

        ! Define Summary Column Titles (DBH: 01/08/2006)
        E1.WriteToCell('Head Account Number'        ,'A10')
        E1.WriteToCell('Head Account Name'          ,'B10')
        E1.WriteToCell('OBF Sent'                   ,'C10')
        E1.WriteToCell('OBF Accepted'               ,'D10')
        E1.WriteToCell('% Accepted'                 ,'E10')
        E1.WriteToCell('OBF Rejected'               ,'F10')
        E1.WriteToCell('% Rejected'                 ,'G10')
        E1.WriteToCell('Total'                      ,'H10')

        E1.SelectCells('A11')


        ! Account Summary____________________________________________________________________________________
        local:RecordCount = 11
        TotalOBF# = 0
        TotalAccepted# = 0
        TotalRejected# = 0
        Sort(AccountSummaryQueue,accque:AccountNumber)
        Loop x# = 1 To Records(AccountSummaryQueue)
            Get(AccountSummaryQueue,x#)
            E1.WriteToCell(accque:AccountNumber     ,'A' & local:RecordCount)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = accque:AccountNumber
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            E1.WriteToCell(tra:Company_Name         ,'B' & local:RecordCount)
            E1.WriteToCell(accque:OBFSent           ,'C' & local:RecordCount)
            E1.WriteToCell(accque:OBFAccepted       ,'D' & local:RecordCount)
            E1.WriteToCell(Format((accque:OBFAccepted / accque:OBFSent) * 100,@n14.2) & '%','E' & local:RecordCount)
            E1.WriteToCell(accque:OBFRejected       ,'F' & local:RecordCount)
            E1.WriteToCell(Format((accque:OBFRejected / accque:OBFSent) * 100,@n14.2) & '%','G' & local:RecordCount)
            E1.WriteToCell(Format(((accque:OBFAccepted / accque:OBFSent) * 100) + ((accque:OBFRejected / accque:OBFSent) * 100),@n14.2) & '%','H' & local:RecordCount)
            local:RecordCount += 1
            TotalOBF# += accque:OBFSent
            TotalAccepted# += accque:OBFAccepted
            TotalRejected# += accque:OBFRejected
        End ! Loop x# = 1 To Records(AccountSummary)
        E1.WriteToCell('Total'                      ,'B' & local:RecordCount)
        E1.WriteToCell(TotalOBF#                    ,'C' & local:RecordCount)
        E1.WriteToCell(TotalAccepted#               ,'D' & local:RecordCount)
        E1.WriteToCell(Format((TotalAccepted# / TotalOBF# * 100),@n14.2) & '%'  ,'E' & local:RecordCount)
        E1.WriteToCell(TotalRejected#               ,'F' & local:RecordCount)
        E1.WriteToCell(Format((TotalRejected# / TotalOBF# * 100),@n14.2) & '%'  ,'G' & local:RecordCount)
        E1.WriteToCell(Format((TotalAccepted# / TotalOBF# * 100) + (TotalRejected# / TotalOBF# * 100),@n14.2) & '%' ,'H' & local:RecordCount)

        local.DrawBox('A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,'A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,color:Silver)
        E1.SetCellFontStyle('Bold','A9',Clip(tmp:LastSummaryColumn) & '10')
        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)

        ! User Summary____________________________________________________________________________________

        local:RecordCount += 2
        tmp:LastSummaryColumn = 'C'
        local.DrawBox('A9',Clip(tmp:LastSummaryColumn) & '9','A9',Clip(tmp:LastSummaryColumn) & '9',color:Silver)
        local.DrawBox('A10',Clip(tmp:LastSummaryColumn) & '10','A10',Clip(tmp:LastSummaryColumn) & '10',color:Silver)

        E1.WriteToCell('User Surname'           ,'A' & local:RecordCount)
        E1.WriteToCell('User Forename'          ,'B' & local:RecordCount)
        E1.WriteToCell('Jobs Processed'         ,'C' & local:RecordCount)
        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)
        local.DrawBox('A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,'A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,color:Silver)

        local:RecordCount += 1
        TotalJobs# = 0
        Sort(UserNameQueue,useque:UserCode)
        Loop x# = 1 To Records(UserNameQueue)
            Get(UserNameQueue,x#)
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = useque:UserCode
            If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Found
                E1.WriteToCell(use:Surname      ,'A' & local:RecordCount)
                E1.WriteToCell(use:Forename     ,'B' & local:RecordCount)
            Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Error
                E1.WriteToCell('Unknown User Code: ' & useque:UserCode  ,'A' & local:RecordCount)
            End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
            E1.WriteToCell(useque:JobsProcessed  ,'C' & local:RecordCount)
            local:RecordCount += 1
            TotalJobs# += useque:JobsProcessed
        End ! Loop x# = 1 To Records(UserNameQueue)
        E1.WriteToCell('Total'                  ,'B' & local:RecordCount)
        E1.WriteToCell(TotalJobs#               ,'C' & local:RecordCount)
        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)
        local.DrawBox('A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,'A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,color:Silver)

        ! REason Summary____________________________________________________________________________________

        local:RecordCount += 2
        tmp:LastSummaryColumn = 'E'

        E1.WriteToCell('Description'            ,'A' & local:RecordCount)
        E1.WriteToCell('Rejection Text'         ,'B' & local:RecordCount)
        E1.WriteToCell('Quantity'               ,'C' & local:RecordCount)
        E1.WriteToCell('% Used'                 ,'D' & local:RecordCount)
        E1.WriteToCell('% Of OBF'               ,'E' & local:RecordCount)
        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)
        local.DrawBox('A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,'A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,color:Silver)


        local:RecordCount += 1
        Access:OBFREASN.Clearkey(obf:RecordNumberKey)
        obf:RecordNumber = 1
        Set(obf:RecordNumberKey,obf:RecordNumberKey)
        Loop ! Begin Loop
            If Access:OBFREASN.Next()
                Break
            End ! If Access:OBFREASN.Next()
            E1.WriteToCell(obf:Description      ,'A' & local:RecordCount)
            E1.WriteToCell(obf:RejectionText    ,'B' & local:RecordCount)
            reaque:RecordNumber = obf:RecordNumber
            Get(ReasonQueue,reaque:RecordNumber)
            If ~Error()
                E1.WriteToCell(reaque:Quantity  ,'C' & local:RecordCount)
                E1.WriteToCell(Format((reaque:Quantity / TotalRejected#) * 100,@n_14.2) & '%'   ,'D' & local:RecordCount)
                E1.WriteToCell(Format((reaque:Quantity / TotalOBF#) * 100,@n_14.2) & '%'   ,'E' & local:RecordCount)
            Else ! If Error()

            End ! If Error()
            local:RecordCOunt += 1
        End ! Loop
        E1.WriteToCell('Miscellaneous'          ,'A' & local:RecordCount)
        reaque:RecordNumber = 0
        Get(ReasonQueue,reaque:RecordNumber)
        If ~Error()
            E1.WriteToCell(reaque:Quantity  ,'C' & local:RecordCount)
            E1.WriteToCell(Format((reaque:Quantity / TotalRejected#) * 100,@n_14.2) & '%'   ,'D' & local:RecordCount)
            E1.WriteToCell(Format((reaque:Quantity / TotalOBF#) * 100,@n_14.2) & '%'   ,'E' & local:RecordCount)
        Else ! If Error()

        End ! If Error()

        E1.SetCellFontName('Tahoma','A9','Z' & local:RecordCount)
        E1.SetCellFontSize(8,'A1','Z' & local:RecordCount)
        E1.SetColumnWidth('A','Z')
        E1.SelectCells('A11')

        E1.SaveAs(excel:FileName)
        E1.CloseWorkBook(3)
        E1.Kill()

        ! Use alternative method to assign autofilters (DBH: 31/07/2006)
        ExcelSetup(0)
        ExcelOpenDoc(excel:FileName)
        ExcelSelectSheet(excel:ProgramName)
        ExcelAutoFilter('A11:' & CLip(tmp:LastDetailColumn) & '11')
        ExcelAutoFit('A:' & CLip(tmp:LastDetailCOlumn))
        ExcelSelectRange('B12')
!        ExcelSelectSheet('Summary')
!        ExcelAutoFilter('A10:' & CLip(tmp:LastSummaryCOlumn) & '10')
!        ExcelAutoFit('A:' & CLip(tmp:LastSummaryColumn))
!        ExcelSelectRange('A11')
        ExcelSaveWorkBook(excel:FileName)
        ExcelClose()

        local.UpdatePRogressWindow('Finishing Off Formatting..')

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)

DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(excel:ProgramName, 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A7','D7',color:Silver)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')



getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020675'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OBFReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:OBFREASN.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:JOBSOBF.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5002'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW12.Q &= Queue:Browse
  BRW12.AddSortOrder(,tra:Account_Number_Key)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,tra:Account_Number,1,BRW12)
  BRW12.SetFilter('(tra:BranchIdentification > 0)')
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tmp:Tag,BRW12.Q.tmp:Tag)
  BRW12.AddField(tra:Account_Number,BRW12.Q.tra:Account_Number)
  BRW12.AddField(tra:Company_Name,BRW12.Q.tra:Company_Name)
  BRW12.AddField(tra:RecordNumber,BRW12.Q.tra:RecordNumber)
  BRW12.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:OBFREASN.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020675'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020675'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020675'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WriteLine         Procedure()
Code
    Clear(exp:Record)
    !User Code
    exp:Line1 = '"' & Clip(jof:UserCode)
    !User Surname
    !User Forename
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = jof:UserCode
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(use:Surname)
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(use:Forename)
    Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        exp:Line1   = Clip(exp:Line1) & '","* Unknown User *'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    !SB Job Number
    exp:Line1   = Clip(exp:Line1) & '","' & job:Ref_Number
    !Franchise Branch Number
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    exp:Line1   = Clip(exp:Line1) & '","' & tra:BranchIdentification
    !Franchise Job Number
    exp:Line1   = Clip(exp:Line1) & '","' & wob:JobNumber
    !Date Booked
    exp:Line1   = Clip(exp:Line1) & '","' & Format(job:Date_Booked,@d06)
    !Head Account Number
    exp:Line1   = Clip(exp:Line1) & '","' & wob:HeadAccountNumber
    !Head Account Name
    exp:Line1   = Clip(exp:Line1) & '","' & tra:Company_Name
    !Trade Account
    exp:Line1   = Clip(exp:Line1) & '","' & job:Account_Number
    !Trade Account Name
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
    exp:Line1   = Clip(exp:Line1) & '","' & sub:Company_Name
    !Make
    exp:Line1   = Clip(exp:Line1) & '","' & job:Manufacturer
    !Model Number
    exp:Line1   = Clip(exp:Line1) & '","' & job:Model_Number
    !IMEI Number
    exp:Line1   = Clip(exp:Line1) & '","''' & job:ESN
    !MSN
    If Clip(job:MSN) <> '' And job:MSN <> 'N/A'
        exp:Line1   = Clip(exp:Line1) & '","''' & job:MSN
    Else ! If Clip(job:MSN) <> '' And job:MSN <> 'N/A'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Clip(job:MSN) <> '' And job:MSN <> 'N/A'
    !Charge Type
    exp:Line1   = Clip(exp:Line1) & '","' & job:Warranty_Charge_Type
    !OBF Accepted
    If jof:Status = 2
        exp:Line1   = Clip(exp:Line1) & '","YES'
    Else ! If jof:Status = 1
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If jof:Status = 1
    !OBF Rejected
    If jof:Status = 3
        exp:Line1   = Clip(exp:Line1) & '","YES'
    Else ! If jof:Status = 2
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If jof:Status = 2
    !Date Accepted / Rejected
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jof:DateProcessed,@d06)
    !R Number
    If jof:Status = 2
        exp:Line1   = Clip(exp:Line1) & '","' & jof:RNumber
    Else ! If jof:Status = 1
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If jof:Status = 1
    !Rejection Reason
    If jof:Status = 3
        exp:Line1   = Clip(exp:Line1) & '","' & jof:RejectionReason
    Else ! If jof:Status = 2
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If jof:Status = 2
    !Handling Fee
    !VAT
    !Total Handling Fee
    If jobe:WebJob = 0
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    Else ! If jobe:WebJob = 0
        exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe:HandlingFee,@n_14.2)
        exp:Line1   = Clip(exp:Line1) & '","' & Format((jobe:HandlingFee * (VatRate(job:Account_Number,'L') / 100)),@n_14.2)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe:HandlingFee + (jobe:HandlingFee * (VatRate(job:Account_Number,'L') / 100)),@n_14.2)
    End ! If jobe:WebJob = 0
    exp:Line1   = Clip(exp:Line1) & '"'
    Add(ExportFile)
    tmp:LastDetailRow += 1
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
