

   MEMBER('sbd04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD04003.INC'),ONCE        !Local module procedure declarations
                     END


SDS_Thermal_Multiple PROCEDURE  (Func:AccountNo)      ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    If tmp:High = 0
        SDSLabel_Thermal_NT(Func:AccountNo)
    Else!If tmp:High = 0
        SDSLabel_Thermal_9X(Func:AccountNo)
    End!If tmp:High = 0
