   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
    MODULE('SBD03APP.DLL')
ExchangeLOanDeviceSoldReport PROCEDURE(BYTE fLoan),DLL !
    END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('VODR0001.CLW')
Main                   PROCEDURE   !
     END
        MODULE('CELLMAIN.DLL')
cellmain:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
cellmain:Kill          PROCEDURE,DLL
        END
        MODULE('SBD03APP.DLL')
sbd03app:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
sbd03app:Kill          PROCEDURE,DLL
        END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode



FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE,EXTERNAL,DLL(dll_mode)
GlobalResponse       BYTE,EXTERNAL,DLL(dll_mode)
VCRRequest           LONG,EXTERNAL,DLL(dll_mode)
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('vodr0106.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  cellmain:Init(GlobalErrors, INIMgr)                 ! Initialise dll (ABC)
  sbd03app:Init(GlobalErrors, INIMgr)                 ! Initialise dll (ABC)
  Main
  INIMgr.Update
  cellmain:Kill()
  sbd03app:Kill()
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  GlobalErrors.Kill


