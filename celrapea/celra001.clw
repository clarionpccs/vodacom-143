

   MEMBER('celrapea.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


Startup              PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    if ClarioNET:Global.Param2 ='' then
        !this was loaded directly
        GLO:webJob = false
    Else
        Glo:WebJob = true
        glo:password = clip(ClarioNET:GLobal.Param1)
    END
    If glo:WebJob
        !Look for a redirection file
        !set path and update procedure
        g_path = GETINI('global','datapath','xx7fail8zz', clip(path()) & '\celweb.ini')
        !message('g_path is ' & clip(g_path))
        if clip(g_path)='xx7fail8zz' then
            !not using an ini file - let it go onto own checks
        ELSE
            setpath (clip(g_path))
        end
    End ! If glo:WebJob
   Relate:DEFAULTS.Open
   Relate:LOGGED.Open
   Relate:USERS.Open
   Relate:DEFAULTV.Open
   Relate:TRADEACC.Open
    ! Close splash (DBH: 24-05-2005)
    PUTINI('STARTING','Run',True,)
    error# = 1
    if GLO:webJob then
        !glo:Password was set on enty to the passed variable - LogonUserPassword
        access:users.clearkey(use:password_key)
        use:password = glo:Password
        if access:users.fetch(use:password_key) = level:benign then error#=0.
        If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() then error#=1.
        
        !check the traders details
        a#=instring(',',ClarioNET:Global.Param2,1,1)
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = clip(ClarioNET:Global.Param2[1:a#-1])
        if not access:tradeacc.fetch(tra:account_number_key)
            If Clip(tra:password)<> ClarioNET:Global.Param2[a#+1:len(clip(ClarioNET:Global.Param2))]
                halt(0,'Incorrect Trade details')
            END
            !glo:Default_Site_Location = tra:SiteLocation
        end
        !Send back the version number to the client
        set(defaultv)
        if access:defaultv.next()
            !error  - this is handled elsewhere ignored here
        ELSE
            !pass current version number back to client
            ClarioNET:CallClientProcedure('VERSIONSAVE',defv:VersionNumber)
        END

    ELSE
        Loop x# = 1 To Len(Clip(Command()))
            If Sub(Command(),x#,1) = '%'
                glo:Password = Clip(Sub(Command(),x#+1,30))
                Access:USERS.Clearkey(use:Password_Key)
                use:Password    = glo:Password
                If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    !Found
                    error# = 0
                    If use:RenewPassword <> 0 
                        If use:PasswordLastChanged = ''
                            use:PasswordLastChanged = Today()
                            Access:USERS.Update()
                        Else !If use:PasswordLastChanged = ''
                            If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() 
                                error#=1
                            End
                        End
                    End !If use:RenewPassword <> 0
                Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                
                Break
            End!If Sub(Command(),x#,1) = '%'
        End!Loop x# = 1 To Len(Comman())
    END !if GLO:WebJob

    If error# = 1
        Do Login
    End

    Browse_Exchange_Audits

    Do Log_out

   Relate:DEFAULTS.Close
   Relate:LOGGED.Close
   Relate:USERS.Close
   Relate:DEFAULTV.Close
   Relate:TRADEACC.Close
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
    End
Log_out      Routine
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
