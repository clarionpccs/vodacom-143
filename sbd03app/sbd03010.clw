

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03010.INC'),ONCE        !Local module procedure declarations
                     END








Exchange_Audit_Report PROCEDURE(Audit_No)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(30)
tmp:DefaultFax       STRING(30)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Local_Stock_Type     STRING(15)
Stock_Type_Filter    STRING(1)
Stock_Type_Filter_Display STRING(20)
Grand_Total_Qty      LONG
Grand_Available_Qty  LONG
Stock_Category_filter STRING(30)
Summary_Filter       STRING(3)
Total_No_Of_Lines    LONG
Suppress_Zero        STRING(1)
BarCode_String       CSTRING(16)
Barcode_IMEI         CSTRING(21)
code_temp            BYTE
Option_temp          BYTE
bar_code_temp_String CSTRING(21)
bar_code_string      CSTRING(21)
Serial_No_Rep        STRING(20)
Audit_Qty            LONG
Line_Cost            REAL
Total_Line           REAL
Total_Audited        LONG
Percentage_Audited   REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
Make_Model           STRING(60)
TheAddress           GROUP,PRE(address)
TheName              STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
                     END
WinPrint             CLASS(CWin32file)
                     END
ExportFilename       STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(EXCHANGE)
                       PROJECT(xch:ESN)
                       PROJECT(xch:Stock_Type)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING('EXCHANGE AUDIT REPORT'),AT(4948,573),USE(?String3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16:5),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D6),AT(6302,1302),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(6250,1458),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6552,1458),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6719,1458,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('This Audit is not fully completed'),AT(83,1531),USE(?StringAuditNotComplete),TRN,HIDE,FONT(,12,,FONT:bold,CHARSET:ANSI)
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),TRN,RIGHT(10),FONT('Arial',14,,FONT:bold)
                         STRING(@s30),AT(94,52),USE(address:TheName),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,469,3531,198),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,781),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel: '),AT(94,990),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(563,990),USE(address:Telephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(94,1146),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(563,1146),USE(address:Fax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1302),USE(?String16:4),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3531,198),USE(address:EmailAddress),TRN,FONT(,9,,)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s30),AT(104,10),USE(xch:Stock_Type),FONT('Arial',8,,)
                           STRING(@s60),AT(2448,10),USE(Make_Model),FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(5115,10),USE(xch:ESN),FONT('Arial',8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,667),USE(?unnamed:4)
                           LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                           STRING(@n-10),AT(1615,260),USE(Total_Audited),HIDE,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Stock Items Audited:'),AT(104,260),USE(?String26),TRN,HIDE,FONT('Arial',8,,FONT:bold)
                           STRING(@n-10.2),AT(1615,417),USE(Percentage_Audited),HIDE,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('%'),AT(2240,417),USE(?String30),TRN,HIDE,FONT('Arial',8,,)
                           STRING('Percentage Audited:'),AT(104,417),USE(?String27),TRN,HIDE,FONT('Arial',8,,FONT:bold)
                           STRING('Total Number Of Lines:'),AT(104,104),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-10),AT(1625,104),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:6)
                       END
detail2                DETAIL,AT(,,,260),USE(?unnamed:7)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),TRN,CENTER,FONT(,12,,FONT:bold)
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Type'),AT(135,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Make/Model'),AT(2469,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('IMEI'),AT(5135,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 1 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'AuditExch'
  If PrintOption(PreviewReq,glo:ExportReport,'Exchange Audit Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:EXCHANGE.Open
  Relate:DEFAULTS.Open
  Relate:TRADEACC.Open
  Access:EXCHAMF.UseFile
  Access:EXCHAUI.UseFile
  
  
  RecordsToProcess = BYTES(EXCHANGE)
  RecordsPerCycle = 10
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(EXCHANGE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(Exchaui)*2
      
      !Set Stock File (Stock Type Key)
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        SETTARGET(Report)
        
        ?String21{PROP:Text} = 'SHORTAGES'
        
        if emf:Complete_Flag = false then
            unhide(?StringAuditNotComplete)
        END !if completed
        
        SETTARGET()
        
        if glo:ExportToCSV = 'Y' then
            !top of page lines
            WinPrint.write('"EXCHANGE AUDIT REPORT","'&clip(emf:Site_location)&'"<13,10>'&|
                           '"AUDIT NUMBER","'&cliP(emf:Audit_Number)&'"<13,10>"'&|
                           Format(today(),@d7)&'"<13,10,13,10>"SHORTAGES"<13,10>')
        
            if emf:Complete_Flag = false then
                WinPrint.write('"This Audit is not fully completed"<13,10,13,10>')
            END
        
            Winprint.write('"Stock Type","Make","Model","IMEI"<13,10>')
        END
        
        Total_No_Of_Lines = 0
        
        Access:ExchAui.ClearKey(eau:Audit_Number_Key)
        eau:Audit_Number = Audit_No
        SET(eau:Audit_Number_Key,eau:Audit_Number_Key)
        LOOP
          IF Access:ExchAui.Next()
            BREAK
          END
          IF eau:Audit_Number <> Audit_No
            BREAK
          END
        
          SETTARGET(ProgressWindow)
          DO DisplayProgress
        
          !Shortages first?!
          IF eau:Confirmed <> 0
            CYCLE
          END
        
          Access:Exchange.ClearKey(xch:Ref_Number_Key)
          xch:Ref_Number = eau:Ref_Number
          IF Access:Exchange.Fetch(xch:Ref_Number_Key)
            !Error!
            CYCLE
          END
        
          Make_Model = CLIP(xch:Manufacturer)&'-'&CLIP(xch:Model_Number)
          Total_No_Of_Lines += 1
        
          if glo:ExportToCSV = 'Y' then
            Winprint.write('"' & clip(xch:Stock_Type) & '","' & clip(xch:Manufacturer) &'","'& clip(xch:Model_Number) & '","' & clip(xch:ESN) & '"<13,10>')
          ELSE
            PRINT(RPT:Detail)
          END !if glo Export to csv
        
        END
        
        IF Total_No_Of_Lines = 0
        
          if glo:ExportToCSV = 'Y' then
            Winprint.write('<13,10>,"NO ITEMS TO REPORT"<13,10>')
          ELSE
            PRINT(RPT:Detail2)
          END !if glo Export to csv
        
        ELSE
        
            if glo:ExportToCSV = 'Y' then
                Winprint.write('<13,10>"Total number of lines:","'&format(Total_No_Of_Lines,@n9)&'"<13,10>')     !&|
                               !'"Stock Items Audited:","'& format(Total_Audited,@n9)&'"<13,10>'&|
                               !'"Percentage Audited:","' & format(Percentage_Audited,@n11.2) & '%"<13,10>')
            ELSE
                PRINT(RPT:Detail1)
            END !if glo Export to csv
        
        END !if there were some lines printed
        
        Grand_Available_Qty = 0
        Total_No_Of_Lines = 0
        Total_Line = 0
        Total_Audited = 0
        SETTARGET(Report)
        ?String21{PROP:Text} = 'EXCESSES'
        SETTARGET()
        
        If glo:ExportToCSV = 'Y' then
        
            WinPrint.write('<13,10,13,10>"EXCESSES"<13,10>')
        
            !top of page lines
            if emf:Complete_Flag = false then
                WinPrint.write('"This Audit is not fully completed"<13,10>')
            END
        
            Winprint.write('"Stock Type","Make","Model","IMEI"<13,10>')
        
        END
        
        Access:ExchAui.ClearKey(eau:Audit_Number_Key)
        eau:Audit_Number = Audit_No
        SET(eau:Audit_Number_Key,eau:Audit_Number_Key)
        LOOP
          IF Access:ExchAui.Next()
            BREAK
          END
          IF eau:Audit_Number <> Audit_No
            BREAK
          END
          SETTARGET(ProgressWindow)
          DO DisplayProgress
        
          !this time excesses
          IF eau:New_IMEI <> 1
            CYCLE
          END
        
          Access:Exchange.ClearKey(xch:Ref_Number_Key)
          xch:Ref_Number = eau:Ref_Number
          IF Access:Exchange.Fetch(xch:Ref_Number_Key)
            !Error!
            CYCLE
          END
          Make_Model = CLIP(xch:Manufacturer)&'-'&CLIP(xch:Model_Number)
          Total_No_Of_Lines += 1
        
          if glo:ExportToCSV = 'Y' then
            Winprint.write('"' & clip(xch:Stock_Type) & '","' & clip(xch:Manufacturer) &'","'& clip(xch:Model_Number) & '","' & clip(xch:ESN) & '"<13,10>')
          ELSE
            PRINT(RPT:Detail)
          END !if glo Export to csv
        END !loop through Exchaudi
        
        
        IF Total_No_Of_Lines = 0
          if glo:ExportToCSV = 'Y' then
            Winprint.write('<13,10>,"NO ITEMS TO REPORT"<13,10>')
          ELSE
            PRINT(RPT:Detail2)
          END !if glo Export to csv
        
        ELSE
            if glo:ExportToCSV = 'Y' then
                Winprint.write('<13,10>"Total number of lines:","'&format(Total_No_Of_Lines,@n9)&'"<13,10>')     !&|
                               !'"Stock Items Audited:","'& format(Total_Audited,@n9)&'"<13,10>'&|
                               !'"Percentage Audited:","' & format(Percentage_Audited,@n11.2) & '%"<13,10>')
            ELSE
                !PRINT(RPT:Detail1) this is a new page not wanted now
            END !if glo Export to csv
        
        END !if there was some reported
        
        if glo:ExportToCSV = 'Y' then
            WinPrint.close()
            Miss# = missive('CSV Export Complete : |'&clip(ExportFilename),'ServiceBase 3g','Mexclam.jpg','OK')
            if glo:WebJob then
                SendFileToClient(ExportFilename)
                Remove(ExportFilename)
            END
        END
        
        LocalResponse = RequestCompleted
        BREAK
        
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(EXCHANGE,'QUICKSCAN=off').
  ! Before Embed Point: %EndOfReportGeneration) DESC(End of Report Generation) ARG()
  if glo:ExportToCSV = 'Y' then CPCSDummyDetail = 1.
  !CPCSDummyDetail is used when the "[] Generate a Page even if NO Detail is printed" is ticked
  !That is needed for the previews to work properly, but this turns it off at the last second
  ! After Embed Point: %EndOfReportGeneration) DESC(End of Report Generation) ARG()
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHAMF.Close
    Relate:TRADEACC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
      !*************************set the display address to the head account if booked on as a web job***********************
      if glo:webjob then
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = clarionet:global.param2
          IF access:tradeacc.fetch(tra:account_number_key)
            !Error!
          END
          address:TheName           = tra:Company_Name
          address:AddressLine1       = tra:Address_Line1
          address:AddressLine2       = tra:Address_Line2
          address:AddressLine3       = tra:Address_Line3
          address:Postcode            = tra:Postcode
          address:Telephone    = tra:Telephone_Number
          address:Fax          = tra:Fax_Number
          address:EmailAddress        = tra:EmailAddress
      Else
          address:TheName=          def:User_Name
          address:AddressLine1=     def:Address_Line1
          address:AddressLine2=     def:Address_Line2
          address:AddressLine3=     def:Address_Line3
          address:Postcode=         def:Postcode
          address:Telephone=        def:Telephone_Number
          address:Fax=              def:Fax_Number
          address:EmailAddress=     def:EmailAddress
  
      end
      !*******
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening The Report
  Total_No_Of_Lines = 0
  if glo:ExportToCSV = 'Y' then
      !message('This will export to CSV')
      ExportFilename = SetReportsFolder('ServiceBase Export','Exchange Audit Report',glo:WebJob) & 'Exchange_Audit_'&format(Today(),@d12)&'_'&format(Clock(),@t5)&'.csv'
      !message('Export file name '&clip(ExportFilename))
      WinPrint.init(ExportFilename,append_mode)
  END
  
  
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Exchange_Audit_Report'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Loan_Audit_Report PROCEDURE(Audit_No)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Local_Stock_Type     STRING(15)
Stock_Type_Filter    STRING(1)
Stock_Type_Filter_Display STRING(20)
Grand_Total_Qty      LONG
Grand_Available_Qty  LONG
Stock_Category_filter STRING(30)
Summary_Filter       STRING(3)
Total_No_Of_Lines    LONG
Suppress_Zero        STRING(1)
BarCode_String       CSTRING(16)
Barcode_IMEI         CSTRING(21)
code_temp            BYTE
Option_temp          BYTE
bar_code_temp_String CSTRING(21)
bar_code_string      CSTRING(21)
Serial_No_Rep        STRING(20)
Audit_Qty            LONG
Line_Cost            REAL
Total_Line           REAL
Total_Audited        LONG
Percentage_Audited   REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
Make_Model           STRING(60)
tmp:DefaultTelephone STRING(30)
tmp:DefaultFax       STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(LOAN)
                       PROJECT(loa:ESN)
                       PROJECT(loa:Stock_Type)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING('LOAN AUDIT REPORT'),AT(4948,573),USE(?String3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D6),AT(6302,1302),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(6250,1458),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6552,1458),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6719,1458,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),TRN,RIGHT(10),FONT('Arial',14,,FONT:bold)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(94,781),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel: '),AT(94,990),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(94,1146),USE(?String16:9),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1302),USE(?String16:4),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3531,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s30),AT(104,10),USE(loa:Stock_Type),FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(5104,10),USE(loa:ESN),FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s60),AT(2438,10),USE(Make_Model),FONT('Arial',8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,667),USE(?unnamed:4)
                           LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                           STRING(@n-10),AT(1615,260),USE(Total_Audited),HIDE,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Stock Items Audited:'),AT(104,260),USE(?String26),TRN,HIDE,FONT('Arial',8,,FONT:bold)
                           STRING(@n-10.2),AT(1615,417),USE(Percentage_Audited),HIDE,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('%'),AT(2240,417),USE(?String30),TRN,HIDE,FONT('Arial',8,,)
                           STRING('Percentage Audited:'),AT(104,417),USE(?String27),TRN,HIDE,FONT('Arial',8,,FONT:bold)
                           STRING('Total Number Of Lines:'),AT(104,104),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-10),AT(1625,104),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:6)
                       END
detail2                DETAIL,AT(,,,260),USE(?unnamed:7)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),TRN,CENTER,FONT(,12,,FONT:bold)
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Type'),AT(135,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Make/Model'),AT(2469,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('IMEI'),AT(5135,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Loan_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'AuditLoan'
  If PrintOption(PreviewReq,glo:ExportReport,'Loan Audit Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOAN.Open
  Relate:DEFAULTS.Open
  Relate:TRADEACC.Open
  Access:LOANAMF.UseFile
  Access:LOANAUI.UseFile
  
  
  RecordsToProcess = BYTES(LOAN)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOAN,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(Loanaui)*2
      
      !Set Stock File (Stock Type Key)
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Okay, we need to go through , H, G, and X & Y
        SETTARGET(Report)
        ?String21{PROP:Text} = 'SHORTAGES'
        SETTARGET()
        !Loop not here any more
        Access:LoanAui.ClearKey(lau:Audit_Number_Key)
        lau:Audit_Number = Audit_No
        SET(lau:Audit_Number_Key,lau:Audit_Number_Key)
        LOOP
          IF Access:LoanAui.Next()
            BREAK
          END
          IF lau:Audit_Number <> Audit_No
            BREAK
          END
          SETTARGET(ProgressWindow)
          DO DisplayProgress
        
          !Shortages first?!
        
          IF lau:Confirmed <> 0
            CYCLE
          END
        
          Access:LOAN.ClearKey(loa:Ref_Number_Key)
          loa:Ref_Number = lau:Ref_Number
          IF Access:LOAN.Fetch(loa:Ref_Number_Key)
            !Error!
            CYCLE
          END
          Make_Model = CLIP(loa:Manufacturer)&'-'&CLIP(loa:Model_Number)
          Total_No_Of_Lines += 1
          PRINT(RPT:Detail)
        END
        IF Total_No_Of_Lines = 0
          PRINT(RPT:Detail2)
        END
        
        IF Total_No_Of_Lines = 0
          PRINT(RPT:Detail2)
        END
        PRINT(RPT:Detail1)
        Grand_Available_Qty = 0
        Total_No_Of_Lines = 0
        Total_Line = 0
        Total_Audited = 0
        SETTARGET(Report)
        ?String21{PROP:Text} = 'EXCESSES'
        SETTARGET()
        Access:LoanAui.ClearKey(lau:Audit_Number_Key)
        lau:Audit_Number = Audit_No
        SET(lau:Audit_Number_Key,lau:Audit_Number_Key)
        LOOP
          IF Access:LoanAui.Next()
            BREAK
          END
          IF lau:Audit_Number <> Audit_No
            BREAK
          END
          SETTARGET(ProgressWindow)
          DO DisplayProgress
        
          !Shortages first?!
        
          IF lau:New_IMEI <> 1
            CYCLE
          END
        
          Access:LOAN.ClearKey(loa:Ref_Number_Key)
          loa:Ref_Number = lau:Ref_Number
          IF Access:LOAN.Fetch(loa:Ref_Number_Key)
            !Error!
            CYCLE
          END
          Make_Model = CLIP(loa:Manufacturer)&'-'&CLIP(loa:Model_Number)
          Total_No_Of_Lines += 1
          PRINT(RPT:Detail)
        END
        IF Total_No_Of_Lines = 0
          PRINT(RPT:Detail2)
        END
        BREAK
        
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOAN,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:LOAN.Close
    Relate:TRADEACC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening The Report
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Total_No_Of_Lines = 0
      !*************************set the display address to the head account if booked on as a web job***********************
      if glo:webjob then
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = clarionet:global.param2
          IF access:tradeacc.fetch(tra:account_number_key)
            !Error!
          END
          def:User_Name           = tra:Company_Name
          def:Address_Line1       = tra:Address_Line1
          def:Address_Line2       = tra:Address_Line2
          def:Address_Line3       = tra:Address_Line3
          def:Postcode            = tra:Postcode
          tmp:DefaultTelephone    = tra:Telephone_Number
          tmp:DefaultFax          = tra:Fax_Number
          def:EmailAddress        = tra:EmailAddress
  
      end
      !*********************************************************************************************************************
  
  
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Loan_Audit_Report'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













SiteLocationPartLevelMatchReport PROCEDURE(pQueue)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Summary_Filter       STRING(3)
Suppress_Zero        STRING(1)
BarCode_String       CSTRING(16)
Barcode_IMEI         CSTRING(21)
code_temp            BYTE
Option_temp          BYTE
Serial_No_Rep        STRING(20)
Audit_Qty            LONG
Line_Cost            REAL
Total_No_Of_Lines    LONG
Total_Qty_Of_Parts   LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
Parts_Q              QUEUE,PRE(ptq)
Site_Location        STRING(30)
Part_No              STRING(30)
Description          STRING(30)
Shelf_Location       STRING(30)
Skill_Level_1        STRING(1)
Skill_Level_2        STRING(1)
Skill_Level_3        STRING(1)
Quantity             REAL
                     END
tmp:Counter          LONG
tmp:CurrentSiteLocation STRING(30)
tmp:SiteSkillLevel1  STRING(10)
tmp:SiteSkillLevel2  STRING(10)
tmp:SiteSkillLevel3  STRING(10)
!-----------------------------------------------------------------------------
Process:View         VIEW(LOCATION)
                     END
! Before Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
PartTemplateQueue   queue,type
PTSite_Location        STRING(30)
PTPart_No              STRING(30)
PTDescription          STRING(30)
PTShelf_Location       STRING(30)
PTSkill_Level_1        STRING(1)
PTSkill_Level_2        STRING(1)
PTSkill_Level_3        STRING(1)
PTQuantity             REAL
                    end

pRefQueue           &PartTemplateQueue
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
! After Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
Report               REPORT('Match Adjustments Report'),AT(396,2802,7521,7875),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING('Site Location :'),AT(4948,573),USE(?String3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(6063,573),USE(tmp:CurrentSiteLocation),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Site Skill Level :'),AT(4948,750),USE(?String32),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s10),AT(6063,750),USE(tmp:SiteSkillLevel1),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s10),AT(6063,906),USE(tmp:SiteSkillLevel2),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s10),AT(6063,1052),USE(tmp:SiteSkillLevel3),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Adjustments Made :'),AT(4948,1229),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D6),AT(6063,1229),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1b),AT(6771,1229),USE(ReportRunTime),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(6063,1396),USE(tmp:printedby),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Made By :'),AT(4948,1396),USE(?String33),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('SITE LOCATION PART LEVEL MATCH ADJUSTMENTS'),AT(1823,0,5313,260),USE(?String21),TRN,LEFT(10),FONT('Arial',14,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s30),AT(104,0),USE(ptq:Part_No),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2073,0),USE(ptq:Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@N8),AT(6688,0),USE(ptq:Quantity),TRN,RIGHT(1),FONT('Arial',8,,)
                           STRING(@s30),AT(4010,0,1563,208),USE(ptq:Shelf_Location),FONT('Arial',8,,)
                           STRING(@s1),AT(5740,0),USE(ptq:Skill_Level_1),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s1),AT(6073,0),USE(ptq:Skill_Level_2),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s1),AT(6365,0),USE(ptq:Skill_Level_3),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,438),USE(?unnamed:6)
                         LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines :'),AT(115,94),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-10),AT(1635,94),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity Of Parts :'),AT(115,250),USE(?String26),TRN,FONT('Arial',8,,FONT:bold)
                         STRING(@n-10),AT(1625,250),USE(Total_Qty_Of_Parts),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                       FOOTER,AT(396,10708,7521,302),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Part Number'),AT(135,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Description'),AT(2104,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Shelf Location'),AT(4042,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Skill Level'),AT(5771,1979),USE(?String8),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity'),AT(6771,1979),USE(?String9:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('SiteLocationPartLevelMatchReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOCATION.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  if pQueue <> 0 !Pointer = NULL?
      pRefQueue &= (pQueue) !Assign pointer reference (pGroup - by value not address)
      loop a# = 1 to records(pRefQueue)
          get(pRefQueue, a#)
          Parts_Q.ptq:Site_Location = pRefQueue.PTSite_Location
          Parts_Q.ptq:Part_No       = pRefQueue.PTPart_No
          Parts_Q.ptq:Description   = pRefQueue.PTDescription
          Parts_Q.ptq:Shelf_Location = pRefQueue.PTShelf_Location
          Parts_Q.ptq:Skill_Level_1  = pRefQueue.PTSkill_Level_1
          Parts_Q.ptq:Skill_Level_2  = pRefQueue.PTSkill_Level_2
          Parts_Q.ptq:Skill_Level_3  = pRefQueue.PTSkill_Level_3
          Parts_Q.ptq:Quantity       = pRefQueue.PTQuantity
          add(Parts_Q,Parts_Q.ptq:Site_Location)
      end
  end
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(LOCATION)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOCATION,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(Parts_Q)
      
      tmp:Counter = 1
      
      sort(Parts_Q,Parts_Q.ptq:Site_Location)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        ! Process queue
        
        get(Parts_Q, tmp:Counter)
        if error()
            if Total_No_Of_Lines <> 0
                print(RPT:Detail1)
            end
            LocalResponse = RequestCompleted
            break
        end
        
        settarget(ProgressWindow)                                           ! Update progress bar
        do DisplayProgress
        
        settarget(Report)
        
        if tmp:CurrentSiteLocation = ''
            Total_No_Of_Lines = 0
            Total_Qty_Of_Parts = 0
            tmp:CurrentSiteLocation = Parts_Q.ptq:Site_Location
            do DisplaySiteSkillLevel
        else
            if tmp:CurrentSiteLocation <> Parts_Q.ptq:Site_Location         ! Has site location changed ?
                if Total_No_Of_Lines <> 0                                   ! Print totals
                    print(RPT:Detail1)
                end
                endpage(Report)                                             ! Start new page
                Total_No_Of_Lines = 0
                Total_Qty_Of_Parts = 0
                tmp:CurrentSiteLocation = Parts_Q.ptq:Site_Location         ! Update variables to current location
                do DisplaySiteSkillLevel
            end
        end
        
        print(RPT:Detail)
        
        Total_Qty_Of_Parts += Parts_Q.ptq:Quantity
        Total_No_Of_Lines  += 1
        tmp:Counter        += 1
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOCATION,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='SiteLocationPartLevelMatchReport'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
DisplaySiteSkillLevel Routine

    tmp:SiteSkillLevel1 = '1 NO'
    tmp:SiteSkillLevel2 = '2 NO'
    tmp:SiteSkillLevel3 = '3 NO'

    Access:LOCATION.ClearKey(loc:Location_Key)
    loc:Location = tmp:CurrentSiteLocation
    if not Access:LOCATION.Fetch(loc:Location_Key)
        tmp:SiteSkillLevel1 = '1 ' & choose(loc:Level1 = 1, 'YES', 'NO')
        tmp:SiteSkillLevel2 = '2 ' & choose(loc:Level2 = 1, 'YES', 'NO')
        tmp:SiteSkillLevel3 = '3 ' & choose(loc:Level3 = 1, 'YES', 'NO')
    end
! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()











PendingWebOrderReport PROCEDURE(SentTime)
RejectRecord         LONG,AUTO
LineCost             DECIMAL(10,2)
TotalLineCost        DECIMAL(10,2)
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
address:Telephone_Number STRING(20)
address:Fax_No       STRING(20)
address:Email        STRING(50)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Total_No_Of_Lines    LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
OrderDate            DATE
OrderTime            TIME
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDWEBPR)
                       PROJECT(orw:Description)
                       PROJECT(orw:ItemCost)
                       PROJECT(orw:PartNumber)
                       PROJECT(orw:Quantity)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(52,833),USE(address:Post_Code),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4948,1292),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D06),AT(5854,1292),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Email:'),AT(52,1302),USE(?String33),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s50),AT(365,1302),USE(address:Email),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Ordered:'),AT(4948,1135),USE(?String35),TRN,FONT('Arial',8,,)
                         STRING(@d06),AT(5854,1135),USE(OrderDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t7),AT(6552,1135),USE(OrderTime),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(52,1146),USE(?String34),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(52,990),USE(?String32),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(5875,1458),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6177,1458),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6344,1458,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,0),USE(Address:User_Name),TRN,FONT('Arial',14,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s8),AT(104,0),USE(orw:Quantity),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(781,0),USE(orw:PartNumber),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(4740,0),USE(orw:ItemCost),TRN,RIGHT(1),FONT('Arial',8,,)
                           STRING(@n-14.2),AT(5781,0),USE(LineCost),TRN,RIGHT,FONT('Arial',8,,)
                           STRING(@s30),AT(2760,0,,208),USE(orw:Description),TRN,FONT('Arial',8,,)
                         END
                       END
detail4                DETAIL,AT(,,,365),USE(?unnamed:9)
                         LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total'),AT(4375,104),USE(?String28),TRN,FONT('Arial',8,,FONT:bold)
                         LINE,AT(4375,52,2250,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(5781,104),USE(TotalLineCost),TRN,RIGHT,FONT('Arial',8,,FONT:bold)
                         STRING('Total Number Of Lines:'),AT(104,94),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-14),AT(1625,94),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('STOCK ORDER'),AT(5781,52),USE(?String3),TRN,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity'),AT(104,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(781,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Description'),AT(2760,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Item Cost'),AT(5000,1979),USE(?String8),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6042,1979),USE(?String9:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('PendingWebOrderReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'StockOrder'
  If PrintOption(PreviewReq,glo:ExportReport,'Stock Order') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDWEBPR.Open
  Relate:DEFAULTS.Open
  Relate:GENSHORT.Open
  Relate:STOAUDIT.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = BYTES(ORDWEBPR)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDWEBPR,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(ORDWEBPR)
      
      !Set Stock File (Stock Type Key)
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        If glo:WebJob
            tra:Account_Number  = Clarionet:Global.Param2
        Else ! If glo:WebJob
            tra:Account_Number  = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
        End ! If glo:WebJob
        
        IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
           !Found
           address:User_Name        = tra:company_name
           address:Address_Line1    = tra:address_line1
           address:Address_Line2    = tra:address_line2
           address:Address_Line3    = tra:address_line3
           address:Post_code        = tra:postcode
           address:Telephone_Number = tra:telephone_number
           address:Fax_No           = tra:Fax_Number
           address:Email            = tra:EmailAddress
        ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        SETTARGET()
        !Loop not here any more
        Access:ORDWEBPR.CLEARKEY(orw:PartNumberKey)
        SET(orw:PartNumberKey,orw:PartNumberKey)
        LOOP
          IF Access:ORDWEBPR.NEXT() THEN BREAK.
          glo:Queue.Pointer = orw:RecordNumber
          SETTARGET(ProgressWindow)
          DO DisplayProgress
          GET(glo:Queue,glo:Queue.Pointer)
          IF ERROR() THEN CYCLE.
          LineCost = orw:ItemCost * orw:Quantity
          TotalLineCost += LineCost
          PRINT(RPT:Detail)
          Total_No_Of_Lines += 1
        END !LOOP
        IF Total_No_Of_Lines <> 0 THEN
           PRINT(RPT:Detail4)
        END !IF
        LocalResponse = RequestCompleted
        BREAK
        
        
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDWEBPR,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:GENSHORT.Close
    Relate:ORDWEBPR.Close
    Relate:STOAUDIT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening The Report
  Total_No_Of_Lines = 0
  OrderDate = today()
  OrderTime = SentTime    !clock()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='PendingWebOrderReport'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END







Status_Report_Criteria_Web PROCEDURE                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::24:TAGDISPSTATUS   BYTE(0)
DASBRW::24:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::25:TAGDISPSTATUS   BYTE(0)
DASBRW::25:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::30:TAGDISPSTATUS   BYTE(0)
DASBRW::30:QUEUE          QUEUE
Pointer7                      LIKE(glo:Pointer7)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::31:TAGDISPSTATUS   BYTE(0)
DASBRW::31:QUEUE          QUEUE
Pointer8                      LIKE(glo:Pointer8)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
local:FileNameJobs   STRING(255),STATIC
local:FileNameAudit  STRING(255),STATIC
local:FileNameParts  STRING(255),STATIC
local:FileNameWarParts STRING(255),STATIC
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_stac_id         USHORT,AUTO
tmp:Description      STRING(60)
Tmp:ShortReportFlag  BYTE
tmp:NewDescription   STRING(60)
tmp:OutputType       BYTE(0)
tmp:PaperReportType  BYTE(0)
tmp:ExportJobs       BYTE(0)
tmp:ExportAudit      BYTE(0)
tmp:ExportParts      BYTE(0)
tmp:ExportWarparts   BYTE(0)
tmp:ExportPath       STRING(255)
tmp:HeadAccountTag   STRING(1)
tmp:SubAccountTag    STRING(1)
tmp:No               STRING('NO {1}')
tmp:CChargeTypeTag   STRING(1)
tmp:Yes              STRING('YES')
tmp:WChargeType      STRING(3)
tmp:CRepairTypeTag   STRING(1)
tmp:WRepairTypeTag   STRING(1)
tmp:StatusType       BYTE(0)
tmp:LocationTag      STRING(1)
tmp:UserTag          STRING(1)
tmp:ENGINEER         STRING('ENGINEER {22}')
tmp:Manufacturer     STRING(30)
tmp:SelectManufacturer BYTE(0)
tmp:ManufacturerTag  STRING(1)
tmp:ModelNumberTag   STRING(1)
tmp:UnitTypeTag      STRING(1)
tmp:TransitTypeTag   STRING(1)
tmp:TurnaroundTimeTag STRING(1)
tmp:StatusTag        STRING(1)
tmp:SavedCriteria    STRING(60)
tmp:SavedLocation    STRING(30)
tmp:Workshop         BYTE(0)
tmp:JobType          BYTE(0)
tmp:CompletedType    BYTE(0)
tmp:DespatchedType   BYTE(0)
tmp:JobBatchNumber   LONG
tmp:EDIBatchNumber   LONG
tmp:ReportOrder      STRING(30)
tmp:DateRangeType    BYTE(0)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:InvoiceType      BYTE(0)
tmp:ActualPartsCost  REAL
tmp:FirstPart        LONG
tmp:CustomerStatus   BYTE(1)
tmp:Job_Number       STRING(20)
tmp:ARCChargeableCostPrice REAL
tmp:ARCWarrantyCostPrice REAL
tmp:RRCChargeableCostPrice REAL
tmp:RRCWarrantyCostPrice REAL
tmp:VAT              REAL
tmp:HandlingFeeType  STRING(30)
tmp:HandlingFeeLocation STRING(30)
tmp:ARCMarkup        REAL
tmp:ARCCharge        REAL
tmp:True             BYTE(1)
tmp:PartsCost        REAL
tmp:LabourCost       REAL
tmp:CourierCost      REAL
tmp:LabourVATRate    REAL
tmp:PartsVATRate     REAL
LOC:RRCLocation      STRING(30)
LOC:ARCLocation      STRING(30)
LOC:RRCDateRecived   STRING(10)
LOC:PUPflag          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Generic_Account)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:HeadAccountTag     LIKE(tmp:HeadAccountTag)       !List box control field - type derived from local data
tmp:HeadAccountTag_Icon LONG                          !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Generic_Account    LIKE(sub:Generic_Account)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:SubAccountTag      LIKE(tmp:SubAccountTag)        !List box control field - type derived from local data
tmp:SubAccountTag_Icon LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:CChargeTypeTag     LIKE(tmp:CChargeTypeTag)       !List box control field - type derived from local data
tmp:CChargeTypeTag_Icon LONG                          !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
tmp:WChargeType        LIKE(tmp:WChargeType)          !List box control field - type derived from local data
tmp:WChargeType_Icon   LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Manufacturer)
                       PROJECT(rtd:RecordNumber)
                       PROJECT(rtd:Chargeable)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
tmp:CRepairTypeTag     LIKE(tmp:CRepairTypeTag)       !List box control field - type derived from local data
tmp:CRepairTypeTag_Icon LONG                          !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Manufacturer       LIKE(rtd:Manufacturer)         !List box control field - type derived from field
rtd:RecordNumber       LIKE(rtd:RecordNumber)         !Primary key field - type derived from field
rtd:Chargeable         LIKE(rtd:Chargeable)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Manufacturer)
                       PROJECT(rtd:RecordNumber)
                       PROJECT(rtd:Warranty)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:6
tmp:WRepairTypeTag     LIKE(tmp:WRepairTypeTag)       !List box control field - type derived from local data
tmp:WRepairTypeTag_Icon LONG                          !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Manufacturer       LIKE(rtd:Manufacturer)         !List box control field - type derived from field
rtd:RecordNumber       LIKE(rtd:RecordNumber)         !Primary key field - type derived from field
rtd:Warranty           LIKE(rtd:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                       PROJECT(sts:Exchange)
                       PROJECT(sts:Loan)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?List:7
tmp:StatusTag          LIKE(tmp:StatusTag)            !List box control field - type derived from local data
tmp:StatusTag_Icon     LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
sts:Exchange           LIKE(sts:Exchange)             !Browse key field - type derived from field
sts:Loan               LIKE(sts:Loan)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(LOCINTER)
                       PROJECT(loi:Location)
                     END
Queue:Browse:7       QUEUE                            !Queue declaration for browse/combo box using ?List:8
tmp:LocationTag        LIKE(tmp:LocationTag)          !List box control field - type derived from local data
tmp:LocationTag_Icon   LONG                           !Entry's icon ID
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                       PROJECT(use:User_Type)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?List:9
tmp:UserTag            LIKE(tmp:UserTag)              !List box control field - type derived from local data
tmp:UserTag_Icon       LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !Primary key field - type derived from field
use:User_Type          LIKE(use:User_Type)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW16::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:9       QUEUE                            !Queue declaration for browse/combo box using ?List:10
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW17::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?List:11
tmp:ModelNumberTag     LIKE(tmp:ModelNumberTag)       !List box control field - type derived from local data
tmp:ModelNumberTag_Icon LONG                          !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
Queue:Browse:11      QUEUE                            !Queue declaration for browse/combo box using ?List:12
tmp:UnitTypeTag        LIKE(tmp:UnitTypeTag)          !List box control field - type derived from local data
tmp:UnitTypeTag_Icon   LONG                           !Entry's icon ID
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW22::View:Browse   VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
Queue:Browse:12      QUEUE                            !Queue declaration for browse/combo box using ?List:13
tmp:TransitTypeTag     LIKE(tmp:TransitTypeTag)       !List box control field - type derived from local data
tmp:TransitTypeTag_Icon LONG                          !Entry's icon ID
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW23::View:Browse   VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
Queue:Browse:13      QUEUE                            !Queue declaration for browse/combo box using ?List:14
tmp:TurnaroundTimeTag  LIKE(tmp:TurnaroundTimeTag)    !List box control field - type derived from local data
tmp:TurnaroundTimeTag_Icon LONG                       !Entry's icon ID
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB18::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Status Report / Export Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,310),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Load Previous Criteria'),AT(210,94),USE(?Prompt46),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           BUTTON,AT(344,99),USE(?SelectCriteria),TRN,FLAT,LEFT,ICON('selcritp.jpg')
                           PROMPT('Select a previously saved criteria'),AT(210,107),USE(?Prompt47),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Criteria Used:'),AT(212,131),USE(?Prompt48),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(272,131),USE(tmp:SavedCriteria),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           LINE,AT(179,150,323,0),USE(?Line1),COLOR(COLOR:White),LINEWIDTH(1)
                           PROMPT('Status Report Type'),AT(210,166),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold+FONT:underline,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select how you want to output the results of this report:'),AT(210,182),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Report Output Type'),AT(214,202,244,48),USE(tmp:OutputType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper Report'),AT(226,222),USE(?tmp:OutputType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Export File'),AT(294,222),USE(?tmp:OutputType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION,AT(366,210,87,35),USE(Tmp:ShortReportFlag),TRN,HIDE
                             RADIO(' Full Version'),AT(382,214),USE(?tmp:OutputType:Radio4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO(' Short Version'),AT(382,230),USE(?tmp:OutputType:Radio3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('Note: A tag list should be completely unticked to disregard that element from th' &|
   'e criteria. E.g. To run the Status Report regardless of location, leave the inte' &|
   'rnal location tag list completely unticked.'),AT(214,266,256,24),USE(?Prompt66),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('2'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Paper Report Type'),AT(213,94),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select what type of report you want to print:'),AT(213,110,253,11),USE(?Prompt5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Detailed'),AT(213,134),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Produces a report listing all the jobs that match the selected criteria.'),AT(213,150),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Produces a report listing how many jobs are in each status.'),AT(213,186,236,24),USE(?Prompt7:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Produces a report listing all jobs that match the selected criteria, including P' &|
   'revious Status information'),AT(213,226,236,24),USE(?Prompt7:3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Paper Report Type'),AT(213,251,255,48),USE(tmp:PaperReportType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Detailed'),AT(221,270),USE(?tmp:PaperReportType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Summary'),AT(299,270),USE(?tmp:PaperReportType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Status Change'),AT(383,270),USE(?tmp:PaperReportType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           PROMPT('Summary'),AT(213,170),USE(?Prompt6:2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Status Change'),AT(213,214),USE(?Prompt6:3),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                         END
                         TAB('3'),USE(?Tab3),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Export File Details'),AT(211,94),USE(?Prompt12),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('For evey job that matches the report criteria, select which of that job''s files ' &|
   'you want exported.'),AT(211,110,200,20),USE(?Prompt13),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Path where you want to create the export files.'),AT(215,238,200,16),USE(?Prompt13:2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Export Path'),AT(219,270),USE(?tmp:ExportPath:Prompt),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(295,270,160,10),USE(tmp:ExportPath),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Export Path'),TIP('Export Path'),UPR
                           BUTTON,AT(460,262),USE(?LookupExportPath),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Export File Path'),AT(215,222),USE(?Prompt12:2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('Export Jobs File'),AT(227,150),USE(tmp:ExportJobs),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Jobs'),TIP('Export Jobs'),VALUE('1','0')
                           CHECK('Export Audit File'),AT(227,166),USE(tmp:ExportAudit),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Audit File'),TIP('Export Audit File'),VALUE('1','0')
                           CHECK('Export Chargeable Parts File'),AT(227,182),USE(tmp:ExportParts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Parts File'),TIP('Export Parts File'),VALUE('1','0')
                           CHECK('Export Warranty Parts File'),AT(227,198),USE(tmp:ExportWarparts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Warranty Parts File'),TIP('Export Warranty Parts File'),VALUE('1','0')
                         END
                         TAB('5'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Sub Account Number'),AT(301,94),USE(?Prompt18),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Sub Trade Acount To Include'),AT(261,106),USE(?Prompt19),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(232,126,216,160),USE(?List:2),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(77,254,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(77,278,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(240,286),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,286),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,286),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('4'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Generic Account Number'),AT(232,94),USE(?String1),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Generic Accounts to Include'),AT(232,110),USE(?Prompt17),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(232,126,216,160),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@65L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(81,262,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(81,278,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(240,286),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,286),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,286),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('18'),USE(?Tab18),HIDE
                           PROMPT('Workshop Type'),AT(88,94),USE(?Prompt49),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('In Workshop'),AT(88,110,260,27),USE(tmp:Workshop),BOXED
                             RADIO('In Workshop Only'),AT(96,120),USE(?tmp:Workshop:Radio1),VALUE('1')
                             RADIO('Not In Workshop Only'),AT(172,120),USE(?tmp:Workshop:Radio2),VALUE('2')
                             RADIO('Both'),AT(263,120),USE(?tmp:Workshop:Radio3),VALUE('0')
                           END
                           PROMPT('Job Type'),AT(88,142),USE(?Prompt50),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Job Type'),AT(68,154,258,48),USE(tmp:JobType),BOXED
                             RADIO('Warranty Only'),AT(74,166),USE(?tmp:JobType:Radio1),VALUE('1')
                             RADIO('Warranty (inc Split)'),AT(150,163),USE(?tmp:JobType:Radio2),VALUE('2')
                             RADIO('Split Only'),AT(241,163),USE(?tmp:JobType:Radio3),VALUE('3')
                             RADIO('Chargeable Only'),AT(74,182),USE(?tmp:JobType:Radio4),VALUE('4')
                             RADIO('Chargeable (inc Split)'),AT(150,182),USE(?tmp:JobType:Radio5),VALUE('5')
                             RADIO('All'),AT(242,182),USE(?tmp:JobType:Radio6),VALUE('0')
                           END
                           PROMPT('Completed Type'),AT(288,203),USE(?Prompt51),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Completed Type'),AT(288,214,260,27),USE(tmp:CompletedType),BOXED
                             RADIO('Completed Only'),AT(294,227),USE(?tmp:CompletedType:Radio1),VALUE('1')
                             RADIO('Incomplete Only'),AT(370,227),USE(?tmp:CompletedType:Radio2),VALUE('2')
                             RADIO('Both'),AT(462,227),USE(?tmp:CompletedType:Radio3),VALUE('0')
                           END
                           PROMPT('Despatched Type'),AT(84,235),USE(?Prompt52),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Despatched Type'),AT(84,251,256,32),USE(tmp:DespatchedType),BOXED
                             RADIO('Despatched Only'),AT(92,262),USE(?tmp:DespatchedType:Radio1),VALUE('1')
                             RADIO('Not Despatched Only'),AT(168,262),USE(?tmp:DespatchedType:Radio2),VALUE('2')
                             RADIO('Both'),AT(260,262),USE(?tmp:DespatchedType:Radio3),VALUE('0')
                           END
                           PROMPT('Invoice Type'),AT(260,286),USE(?Prompt67),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Invoice Type'),AT(260,291,256,28),USE(tmp:InvoiceType),BOXED
                             RADIO('Invoiced Only'),AT(268,302),USE(?tmp:InvoiceType:Radio1),VALUE('1')
                             RADIO('Non-Invoiced Only'),AT(344,302),USE(?tmp:InvoiceType:Radio2),VALUE('2')
                             RADIO('Both'),AT(436,302),USE(?tmp:InvoiceType:Radio3),VALUE('0')
                           END
                         END
                         TAB('6'),USE(?Tab6),HIDE
                           PROMPT('Chargeable Charge Types'),AT(160,110),USE(?Prompt20),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select the Chargeable Charge Types to include'),AT(160,122),USE(?Prompt21)
                           LIST,AT(160,142,148,144),USE(?List:3),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:2)
                         END
                         TAB('7'),USE(?Tab7),HIDE
                           PROMPT('Warranty Charge Types'),AT(160,110),USE(?Prompt22),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Warranty Charge Types To Include'),AT(160,126),USE(?Prompt23)
                           LIST,AT(160,142,148,144),USE(?List:4),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s3@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:3)
                         END
                         TAB('8'),USE(?Tab8),HIDE
                           PROMPT('Chargeable Repair Types'),AT(160,110),USE(?Prompt24),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Chargeable Repair Types To Include'),AT(160,126),USE(?Prompt25)
                           LIST,AT(160,142,256,144),USE(?List:5),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Repair Type~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:4)
                         END
                         TAB('9'),USE(?Tab9),HIDE
                           PROMPT('Warranty Repair Types'),AT(160,110),USE(?Prompt26),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Warranty Repair Types To Include'),AT(160,122),USE(?Prompt27)
                           LIST,AT(160,142,256,144),USE(?List:6),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Repair Type~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:5)
                         END
                         TAB('10'),USE(?Tab10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Status Types'),AT(315,94),USE(?Prompt28),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Type and Status To Include.'),AT(263,106),USE(?Prompt29),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Note: This is the status which will appear on the paper report. If you select "C' &|
   'ustomer" Status, then the Job Status will be shown, unless the unit has an Excha' &|
   'nge attached. Then the Exchange Status will be shown.'),AT(137,118,406,18),USE(?Prompt30),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Status Type'),AT(262,138,156,28),USE(tmp:StatusType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Job'),AT(270,150),USE(?tmp:StatusType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Exchange'),AT(313,150),USE(?tmp:StatusType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Loan'),AT(377,150),USE(?tmp:StatusType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           CHECK('Show "Customer" Status On Report'),AT(265,170),USE(tmp:CustomerStatus),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Show "Customer" Status On Report'),TIP('Show "Customer" Status On Report'),VALUE('1','0')
                           LIST,AT(266,182,148,144),USE(?List:7),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Status~@s30@'),FROM(Queue:Browse:6)
                           BUTTON('&Rev tags'),AT(81,262,50,13),USE(?DASREVTAG:7),HIDE
                           BUTTON('sho&W tags'),AT(77,282,70,13),USE(?DASSHOWTAG:7),HIDE
                           BUTTON,AT(424,218),USE(?DASTAG:7),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(424,246),USE(?DASTAGAll:7),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(424,278),USE(?DASUNTAGALL:7),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('11'),USE(?Tab11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Internal Locations'),AT(305,94),USE(?Prompt31),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select Which Internal Locations To Include'),AT(260,110),USE(?Prompt32),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,126,148,160),USE(?List:8),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse:7)
                           BUTTON('&Rev tags'),AT(81,250,50,13),USE(?DASREVTAG:8),HIDE
                           BUTTON('sho&W tags'),AT(73,270,70,13),USE(?DASSHOWTAG:8),HIDE
                           BUTTON,AT(244,286),USE(?DASTAG:8),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(312,286),USE(?DASTAGAll:8),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(380,286),USE(?DASUNTAGALL:8),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('12'),USE(?Tab12),HIDE
                           PROMPT('Engineers'),AT(160,110),USE(?Prompt33),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select Which Engineers To Include'),AT(160,126),USE(?Prompt34)
                           LIST,AT(161,146,215,144),USE(?List:9),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@109L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@'),FROM(Queue:Browse:8)
                         END
                         TAB('13'),USE(?Tab13),HIDE
                           PROMPT('Manufacturers'),AT(160,110),USE(?Prompt35),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Manufacturers To Include'),AT(160,122),USE(?Prompt36)
                           PROMPT('If you wish to include all Model Numbers for a selected Manufacturer, select the' &|
   ' manufacturer(s) you require below, but do NOT select any Model Numbers from the' &|
   ' next screen.'),AT(160,138,249,24),USE(?Prompt64)
                           LIST,AT(160,174,148,144),USE(?List:10),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:9)
                         END
                         TAB('14'),USE(?Tab14),HIDE
                           PROMPT('Model Numbers'),AT(160,110),USE(?Prompt37),FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Select Which Model Numbers To Include'),AT(160,126),USE(?Prompt38)
                           CHECK('Select Manufacturer'),AT(161,150),USE(tmp:SelectManufacturer),MSG('Select Manurfacturer'),TIP('Select Manurfacturer'),VALUE('1','0')
                           COMBO(@s20),AT(256,150,124,10),USE(tmp:Manufacturer),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Manufacturer'),AT(256,142),USE(?Prompt39),FONT(,7,,)
                           LIST,AT(160,166,148,144),USE(?List:11),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:10)
                         END
                         TAB('15'),USE(?Tab15),HIDE
                           PROMPT('Unit Types'),AT(160,110),USE(?Prompt40),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Unit Types to include'),AT(160,126),USE(?Prompt41)
                           LIST,AT(160,142,148,144),USE(?List:12),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Unit Type~@s30@'),FROM(Queue:Browse:11)
                         END
                         TAB('16'),USE(?Tab16),HIDE
                           PROMPT('Transit Types'),AT(160,110),USE(?Prompt42),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Transit Types to include'),AT(160,126),USE(?Prompt43)
                           LIST,AT(160,142,148,144),USE(?List:13),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Transit Type~@s30@'),FROM(Queue:Browse:12)
                         END
                         TAB('17'),USE(?Tab17),HIDE
                           PROMPT('Turnaround Times'),AT(160,110),USE(?Prompt44),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Turnaround Times to include'),AT(160,126),USE(?Prompt45)
                           LIST,AT(160,142,148,144),USE(?List:14),IMM,MSG('Browsing Records'),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Turnaround Time~@s30@'),FROM(Queue:Browse:13)
                         END
                         TAB('Tab 19'),USE(?Tab19),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Batch Numbers'),AT(216,262),USE(?Prompt53),HIDE,FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which of the following batch numbers you wish to include'),AT(216,278),USE(?Prompt54),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Batch Number'),AT(224,302),USE(?tmp:JobBatchNumber:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(300,302,64,10),USE(tmp:JobBatchNumber),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Batch Number'),TIP('Job Batch Number'),UPR
                           PROMPT('EDI Batch Number'),AT(224,322),USE(?tmp:EDIBatchNumber:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(300,322,64,10),USE(tmp:EDIBatchNumber),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('EDI Batch Number'),TIP('EDI Batch Number'),UPR
                           PROMPT('Report/Export Order'),AT(212,94),USE(?Prompt57),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which order you wish your report to appear in. Note that Date Booked (if ' &|
   'using Date Booked date range) or Date Completed (if using Date Completed date ra' &|
   'nge) will produce the quickest report.'),AT(212,106,380,24),USE(?Prompt58),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Report Order'),AT(224,142),USE(?Prompt59),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(296,142,124,10),USE(tmp:ReportOrder),LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),DROP(10,124),FROM('DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER')
                           PROMPT('Date Range'),AT(212,170),USE(?Prompt60),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select the Date Range type and date range the report will use'),AT(212,182),USE(?Prompt61),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Date Range Type'),AT(214,198,206,31),USE(tmp:DateRangeType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Booking Date'),AT(236,210),USE(?tmp:DateRangeType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Completed Date'),AT(328,210),USE(?tmp:DateRangeType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('Start Date'),AT(224,230),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(296,230,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           PROMPT('End Date'),AT(224,246),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(296,250,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Status Report / Export Wizard'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(68,366),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(136,366),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(480,366),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

Wizard13         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:6
Q                      &Queue:Browse:5                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:7
Q                      &Queue:Browse:6                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW10::Sort1:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 0
BRW10::Sort2:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 1
BRW10::Sort3:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 2
BRW14                CLASS(BrowseClass)               !Browse using ?List:8
Q                      &Queue:Browse:7                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:9
Q                      &Queue:Browse:8                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
BRW16                CLASS(BrowseClass)               !Browse using ?List:10
Q                      &Queue:Browse:9                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW16::Sort0:Locator StepLocatorClass                 !Default Locator
BRW17                CLASS(BrowseClass)               !Browse using ?List:11
Q                      &Queue:Browse:10               !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW17::Sort0:Locator StepLocatorClass                 !Default Locator
BRW17::Sort1:Locator StepLocatorClass                 !Conditional Locator - tmp:SelectManufacturer = 0
BRW17::Sort2:Locator StepLocatorClass                 !Conditional Locator - tmp:SelectManufacturer = 1
BRW21                CLASS(BrowseClass)               !Browse using ?List:12
Q                      &Queue:Browse:11               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
BRW22                CLASS(BrowseClass)               !Browse using ?List:13
Q                      &Queue:Browse:12               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW22::Sort0:Locator StepLocatorClass                 !Default Locator
BRW23                CLASS(BrowseClass)               !Browse using ?List:14
Q                      &Queue:Browse:13               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                 !Default Locator
FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FileLookup34         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP


LocalGetDays       Procedure(String, String, Date) , long       !func:Sat,Byte func:Sun,Date func:Start)


    END !MAP
ExportFileJobs    File,Driver('ASCII'),Pre(expjobs),Name(local:FileNameJobs),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileAudit    File,Driver('ASCII'),Pre(expaudit),Name(local:FileNameAudit),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileParts    File,Driver('ASCII'),Pre(expparts),Name(local:FileNameParts),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileWarparts    File,Driver('ASCII'),Pre(expwarparts),Name(local:FileNameWarParts),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::24:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:HeadAccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:HeadAccountTag = ''
  END
    Queue:Browse.tmp:HeadAccountTag = tmp:HeadAccountTag
  IF (tmp:HeadAccounttag = '*')
    Queue:Browse.tmp:HeadAccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:HeadAccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::24:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::24:QUEUE = glo:Queue
    ADD(DASBRW::24:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::24:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::24:QUEUE,DASBRW::24:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASSHOWTAG Routine
   CASE DASBRW::24:TAGDISPSTATUS
   OF 0
      DASBRW::24:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::24:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::24:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::25:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = sub:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = sub:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:SubAccountTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:SubAccountTag = ''
  END
    Queue:Browse:1.tmp:SubAccountTag = tmp:SubAccountTag
  IF (tmp:SubAccounttag = '*')
    Queue:Browse:1.tmp:SubAccountTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:SubAccountTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = sub:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::25:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::25:QUEUE = glo:Queue2
    ADD(DASBRW::25:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::25:QUEUE.Pointer2 = sub:Account_Number
     GET(DASBRW::25:QUEUE,DASBRW::25:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = sub:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASSHOWTAG Routine
   CASE DASBRW::25:TAGDISPSTATUS
   OF 0
      DASBRW::25:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::25:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::25:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::30:DASTAGONOFF Routine
  GET(Queue:Browse:6,CHOICE(?List:7))
  BRW10.UpdateBuffer
   glo:Queue7.Pointer7 = sts:Status
   GET(glo:Queue7,glo:Queue7.Pointer7)
  IF ERRORCODE()
     glo:Queue7.Pointer7 = sts:Status
     ADD(glo:Queue7,glo:Queue7.Pointer7)
    tmp:StatusTag = '*'
  ELSE
    DELETE(glo:Queue7)
    tmp:StatusTag = ''
  END
    Queue:Browse:6.tmp:StatusTag = tmp:StatusTag
  IF (tmp:Statustag = '*')
    Queue:Browse:6.tmp:StatusTag_Icon = 2
  ELSE
    Queue:Browse:6.tmp:StatusTag_Icon = 1
  END
  PUT(Queue:Browse:6)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue7)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue7.Pointer7 = sts:Status
     ADD(glo:Queue7,glo:Queue7.Pointer7)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASUNTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue7)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASREVTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::30:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue7)
    GET(glo:Queue7,QR#)
    DASBRW::30:QUEUE = glo:Queue7
    ADD(DASBRW::30:QUEUE)
  END
  FREE(glo:Queue7)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::30:QUEUE.Pointer7 = sts:Status
     GET(DASBRW::30:QUEUE,DASBRW::30:QUEUE.Pointer7)
    IF ERRORCODE()
       glo:Queue7.Pointer7 = sts:Status
       ADD(glo:Queue7,glo:Queue7.Pointer7)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASSHOWTAG Routine
   CASE DASBRW::30:TAGDISPSTATUS
   OF 0
      DASBRW::30:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::30:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::30:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:7{PROP:Text} = 'Show All'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:7{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List:7,CHOICE(?List:7))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::31:DASTAGONOFF Routine
  GET(Queue:Browse:7,CHOICE(?List:8))
  BRW14.UpdateBuffer
   glo:Queue8.Pointer8 = loi:Location
   GET(glo:Queue8,glo:Queue8.Pointer8)
  IF ERRORCODE()
     glo:Queue8.Pointer8 = loi:Location
     ADD(glo:Queue8,glo:Queue8.Pointer8)
    tmp:LocationTag = '*'
  ELSE
    DELETE(glo:Queue8)
    tmp:LocationTag = ''
  END
    Queue:Browse:7.tmp:LocationTag = tmp:LocationTag
  IF (tmp:Locationtag = '*')
    Queue:Browse:7.tmp:LocationTag_Icon = 2
  ELSE
    Queue:Browse:7.tmp:LocationTag_Icon = 1
  END
  PUT(Queue:Browse:7)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue8)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue8.Pointer8 = loi:Location
     ADD(glo:Queue8,glo:Queue8.Pointer8)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASUNTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue8)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASREVTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::31:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue8)
    GET(glo:Queue8,QR#)
    DASBRW::31:QUEUE = glo:Queue8
    ADD(DASBRW::31:QUEUE)
  END
  FREE(glo:Queue8)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::31:QUEUE.Pointer8 = loi:Location
     GET(DASBRW::31:QUEUE,DASBRW::31:QUEUE.Pointer8)
    IF ERRORCODE()
       glo:Queue8.Pointer8 = loi:Location
       ADD(glo:Queue8,glo:Queue8.Pointer8)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASSHOWTAG Routine
   CASE DASBRW::31:TAGDISPSTATUS
   OF 0
      DASBRW::31:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::31:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::31:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:8{PROP:Text} = 'Show All'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:8{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:8,CHOICE(?List:8))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Export      Routine
! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
    If IsDateInValid(tmp:StartDate)
        Beep(Beep:SystemHand);  Yield()
       Case Missive('Invalid Start Date.','ServiceBase 3g',|
                      'mstop.jpg','/OK')
           Of 1 ! OK Button
       End ! Case Missive
       tmp:StartDate = ''
       Select(?tmp:StartDate)
       Exit
    End ! If IsDateInValid(tmp:StartDate)
    If IsDateInValid(tmp:EndDate)
        Beep(Beep:SystemHand);  Yield()
       Case Missive('Invalid End Date.','ServiceBase 3g',|
                      'mstop.jpg','/OK')
           Of 1 ! OK Button
       End ! Case Missive
       tmp:EndDate = ''
       Select(?tmp:EndDate)
       Exit
    End ! If IsDateInValid(tmp:StartDate)

    Beep(Beep:SystemQuestion);  Yield()
    Case Missive('Confirm Date Range:'&|
        '|    From:   ' & Format(tmp:StartDate,@d18) & |
        '|    To:        ' & Format(tmp:EndDate,@d18) & |
        '||Run Report?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
        Of 1 ! No Button
            Display()
            Exit
    End ! Case Missive
! End (DBH 14/02/2008) #9752
    !if this is for a summary report just go through all the checks
    !and give a total at the end, just don't export anything.
    If tmp:OutputType = 1 And tmp:PaperReportType = 1
        JobsError# = 1
        AuditError# = 1
        PartsError# = 1
        WarPartsError# = 1

    Else !If tmp:OutputType = 1 And tmp:PaperReportType = 1

        !Remove the backslash - 3383 (DBH: 29-01-2004)

        local:FileNameJobs   = Clip(tmp:ExportPath) & ' JOBSDATA.CSV'
        local:FIlenameAudit  = Clip(tmp:ExportPath) & ' AUDITDATA.CSV'
        local:FileNameParts  = Clip(tmp:ExportPath) & ' PARTSDATA.CSV'
        local:FileNameWarParts = Clip(tmp:ExportPath) & ' WARPARTSDATA.CSV'

        Remove(local:FileNameJobs)
        Remove(local:FileNameAudit)
        Remove(Local:FileNameParts)
        Remove(Local:FileNameWarParts)

        JobsError# = 0
        If tmp:ExportJobs
            Open(ExportFileJobs)
            If Error()
                Create(ExportFileJobs)
                Open(ExportFileJobs)
                If Error()
                    JobsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportJobs
            JobsError# = 1
        End !If tmp:ExportJobs

        AuditError# = 0
        If tmp:ExportAudit
            Open(ExportFileAudit)
            If Error()
                Create(ExportFileAudit)
                Open(ExportFileAudit)
                IF Error()
                    AuditError# = 1
                End !IF Error()
            End !If Error()
        Else !If tmp:ExportAudit
            AuditError# = 1
        End !If tmp:ExportAudit

        PartsError# = 0
        If tmp:ExportParts
            Open(ExportFileParts)
            If Error()
                Create(ExportFileParts)
                Open(ExportFileParts)
                If Error()
                    PartsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportParts
            PartsError# = 1
        End !If tmp:ExportParts

        WarPartsError# = 0
        If tmp:ExportWarparts
            Open(ExportFileWarparts)
            If Error()
                Create(ExportFileWarParts)
                Open(ExportFileWarparts)
                If Error()
                    WarPartsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportWarparts
            WarPartsError# = 1
        End !If tmp:ExportWarparts
    End !If tmp:OutputType = 1 And tmp:PaperReportType = 1

    !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column
    !some defaults want looking up
    LOC:RRCLocation         = GETINI('RRC','RRCLocation','AT FRANCHISE'   , '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
    LOC:ARCLocation         = GETINI('RRC','ARCLocation','RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
    !End TB1326

    If JobsError# = 0
        if Tmp:ShortReportFlag then
            do jobsFileTitleSimple
        ELSE
            Do NewFileTitle
        END !if Tmp:ShortReportFlag
    End !If JobsError# = 0

    If AuditError# = 0
        Do AuditFileTitle
    End !If AuditError# = 0

    If PartsError# = 0
        Do PartsFileTitle
    End !If PartsError# = 0

    If WarPartsError# = 0
        Do WarPartsFileTitle
    End !If PartsError# = 0
    jobCount# = 0

    Prog.progressSetup(Records(JOBS))

    Save_job_ID = Access:JOBS.SaveFile()

    Case tmp:ReportOrder
        Of 'JOB NUMBER'
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            set(job:ref_number_key,0)
        Of 'I.M.E.I. NUMBER'
            Access:JOBS.Clearkey(job:ESN_Key)
            set(job:esn_key,0)
        Of 'ACCOUNT NUMBER'
            Access:JOBS.Clearkey(job:AccountNumberKey)
            set(job:accountnumberkey,0)
        Of 'MODEL NUMBER'
            Access:JOBS.Clearkey(job:Model_Number_Key)
            set(job:model_number_key,0)
        Of 'STATUS'
            Case tmp:StatusType
                Of 0 !Job Status
                    Access:JOBS.Clearkey(job:By_Status)
                    set(job:by_status,0)
                Of 1 !Exchange Status
                    Access:JOBS.Clearkey(job:ExcStatusKey)
                    Set(job:ExcStatusKey)
                Of 2 !Loan Status
                    Access:JOBS.Clearkey(job:LoanStatusKey)
                    Set(job:LoanStatusKey)
            End !Case tmp:StatusType
        Of 'MSN'
            Access:JOBS.Clearkey(job:MSN_Key)
            set(job:msn_key,0)
        Of 'SURNAME'
            Access:JOBS.Clearkey(job:Surname_Key)
            set(job:surname_key,0)
        Of 'ENGINEER'
            Access:JOBS.Clearkey(job:Engineer_Key)
            set(job:engineer_key,0)
        Of 'MOBILE NUMBER'
            Access:JOBS.Clearkey(job:MobileNumberKey)
            set(job:mobilenumberkey,0)
        Of 'DATE BOOKED'
            Access:JOBS.Clearkey(job:Date_Booked_Key)
            Case tmp:DateRangeType
                Of 0 !Booking
                    job:date_booked = tmp:StartDate
                    set(job:date_booked_key,job:date_booked_key)
                Of 1 !Completed
                    Set(job:Date_Booked_Key,0)
            End !tmp:DateRangeType
        Of 'DATE COMPLETED'
            Access:JOBS.ClearKey(job:DateCompletedKey)
            Case tmp:DateRangeType
                Of 0 !Booking
                    Set(job:DateCompletedKey,0)
                Of 1 !Completed
                    job:date_completed = tmp:StartDate
                    set(job:datecompletedkey,job:datecompletedkey)
            End !Case tmp:DateRangeType
    End !Case tmp:ReportOrder

    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If Prog.InsideLoop()
            Break
        End !If Prog.InsideLoop()

        Case tmp:ReportOrder
            Of 'DATE BOOKED'
                If tmp:DateRangeType = 0
                    If job:Date_Booked > tmp:EndDate
                        Break
                    End !If job:Date_Booked > tmp:EndDate
                End !If tmp:DateRangeType = 0
            Of 'DATE COMPLETED'
                If tmp:DateRangeType = 1
                    If job:Date_Completed > tmp:EndDate
                        Break
                    End !If job:Date_Completed > tmp:EndDate
                End !If tmp:DateRangeType = 1
        End !Case tmp:ReportOrder

        If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:JobBatchNumber,tmp:EDIBatchNumber)
            Cycle
        End !If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType)

        If JobsError# = 0
            if Tmp:ShortReportFlag   then
                do ExportJobsSimple
            ELSE
                Do NewFileExport
            END !If glo:webjob
        End !If JobsError# = 0

        If AuditError# = 0

            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:TypeRefKey)
            aud:Ref_Number = job:Ref_Number
            Set(aud:TypeRefKey,aud:TypeRefKey)
            Loop
                If Access:AUDIT.NEXT()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number|
                    Then Break.  ! End If
                Do ExportAudit
            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)
        End !If AuditError# = 0

        If PartsError# = 0
            tmp:FirstPart = 0
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                Do ExportParts
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If PartsError# = 0

        If WarpartsError# = 0
            tmp:FirstPart = 0
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number |
                    Then Break.  ! End If
                Do ExportWarparts
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If WarpartsError# = 0

        jobCount# += 1
        Prog.ProgressText('Records Found: ' & jobCount#)

    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

    Prog.ProgressFinish()

    Case Missive('Routine Completed.'&|
      '<13,10>'&|
      '<13,10>' & JobCount# & ' records(s) matched the criteria.','ServiceBase 3g',|
                   'midea.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive

    Close(ExportFileJobs)
    Close(ExportFileAudit)
    Close(ExportFileParts)
    Close(ExportFileWarParts)

    if glo:webjob then
        if JobsError# = 0
            SendFileToClient(local:FileNameJobs)
        END
        if AuditError# = 0
            SendFileToClient(local:FileNameAudit)
        END
        if PartsError# = 0
            SendFileToClient(local:FileNameParts)
        END
        if WarPartsError# = 0
            sendFileToClient(local:FileNameWarParts)
        END

        Remove(local:FileNameJobs)
        Remove(local:FileNameAudit)
        Remove(Local:FileNameParts)
        Remove(Local:FileNameWarParts)

    END !If glo:webjob




NewFileTitle       Routine      !LONG TITLE
        Clear(expjobs:Record)
        expjobs:Line1   = 'STATUS REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
        Add(ExportFileJobs)

        Clear(expjobs:Record)
        expjobs:Line1   = 'Job No'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Franchise Branch Code')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Franchise Job Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Unit Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Manufacturer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Model Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Repair Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Booked')
        !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column I (after the column Date Booked)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'Date Received From PUP'
        !end TB13236
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Job Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Exch Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Loan Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Of Purchase')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('EDI Batch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('H/O Claim Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Claim Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exclude From EDI')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exclude From EDI')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Unit Exchanged')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Third Party Site')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3rd Party Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3rd Party V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3rd Party Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Exchange Fee')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Handling Fee Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Handling Fee Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Handling Fee')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Exchange Fee')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Vat')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Lost Loan Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Vat')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC MarkUp')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Order Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Authority Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Fault Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Main Out Fault Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 4')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 5')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 6')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 7')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 8')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 9')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 10')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 11')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 12')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 13')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 14')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 15')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 16')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 17')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 18')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 19')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 20')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineers Notes')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'Booked By'   ! #11836 New columns (Bryan: 16/05/2011)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'Despatched By'      ! #11836 New columns (Bryan: 16/05/2011)
        Add(ExportFileJobs)
NewFileExport      Routine     !LONG EXPORT

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    !TB136200 - J - 26/02/14 - and new field - Booking agent  - currently in jobse2
    Access:jobse2.clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    if access:jobse2.fetch(jobe2:RefNumberKey)
        !error
    END
    !END TB13200


    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber  = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign

    Clear(expjobs:Record)
    expjobs:Line1   = StripComma(job:Ref_Number)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tra:BranchIdentification)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:JobNumber)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Unit_Type)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Manufacturer)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Model_Number)

    !TB13236 - J - 18/02/14 - If exchange issued at RRC then this is RRC otherwise as before
    If job:Exchange_Unit_Number <> ''  and  jobe:ExchangedATRRC then
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
    ELSE !ELSE of TB13236
        If SentToHub(job:Ref_Number)
            If job:Third_Party_Site <> ''
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3RD')
            Else !If job:Third_Party_Site <> ''
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC')
            End !If job:Third_Party_Site <> ''
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
        End !If SentToHub(job:Ref_Number)
    END  !END of  TB13236

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Booked,@d06b))

    !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column I after date booked
    LOC:RRCDateRecived = ''    !reset to blank
    LOC:PUPflag = 0            !used to indicate this has been at PUP
    
    Access:LocatLog.ClearKey(lot:DateKey)
    lot:RefNumber = job:Ref_number
    lot:TheDate = 0
    SET(lot:DateKey,lot:DateKey)
    LOOP
        if Access:LocatLog.NEXT() then break.
        IF lot:RefNumber <> job:Ref_Number then break.
        
        IF instring('PUP',lot:PreviousLocation,1,1) then LOC:PUPflag = 1.    !it has been at the PUP
        IF lot:NewLocation <> LOC:RRCLocation then cycle.

        if lot:NewLocation = LOC:ARCLocation then break.  !has been sent to ARC, there will be a return date that we do not want
        
        if LOC:PUPflag = 1 then
            LOC:RRCDateRecived = Format(lot:TheDate,@d06)
        END
        BREAK

    END !LOOP

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & clip(LOC:RRCDateRecived)
    !END TB13236

    ! Inserting (DBH 27/11/2006) # 8524 - Show job statuses
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripCOmma(job:Current_Status)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Status)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Status)
    ! End (DBH 27/11/2006) #8524

    !TB13249 - if job:completed is blank show 'NO' - J - 06/03/14
    if clip(job:completed) = '' then job:completed = 'NO'.  !will not be saved
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Completed)

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Completed,@d06b))
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:DOP,@d06b))
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:EDI_Batch_Number)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:WarrantyClaimStatus)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(WOBEDIStatus(wob:EDI))
    If job:Warranty_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Warranty_Charge_Type)
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty    = 'YES'
        cha:Charge_Type = job:Warranty_Charge_Type
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(cha:Exclude_EDI)
        Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
        End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type_Warranty)
        !Added L397 - extra column for Repair Type - Exclude from EDI flag
        Access:reptydef.clearkey(rtd:Repair_Type_Key)
        rtd:Repair_Type = Job:Repair_type_Warranty
        if access:reptydef.fetch(rtd:Repair_Type_Key)
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        ELSE
            if rtd:ExcludeFromEDI then
                expjobs:Line1   = Clip(expjobs:Line1) & ','&StripComma('YES')
            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ','&StripComma('NO')
            END
        END !if access reptydef

        !End of added L397
    Else !If job:Warranty_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
        expjobs:Line1   = Clip(expjobs:Line1) & ','
        expjobs:Line1   = Clip(expjobs:Line1) & ','
        expjobs:Line1   = Clip(expjobs:Line1) & ','
        expjobs:Line1   = Clip(expjobs:Line1) & ','
    End !If job:Warranty_Job = 'YES'
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number_Warranty)

    If job:Invoice_Number_Warranty <> 0
        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        inv:Invoice_Number  = job:Invoice_Number_Warranty
        If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(inv:Date_Created,@d06b))
        Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    Else !If job:Warranty_Invoice_Number <> 0
        expjobs:Line1   = Clip(expjobs:Line1) & ','
    End !If job:Warranty_Invoice_Number <> 0

    If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Charge_Type)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type)
    Else !If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
        expjobs:Line1   = Clip(expjobs:Line1) & ','
        expjobs:Line1   = Clip(expjobs:Line1) & ','
    End !If job:Chargeable_Job = 'YES'

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number)

    If job:Invoice_Number <> 0
        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        inv:Invoice_Number  = job:Invoice_Number
        If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(inv:Date_Created,@d06b))
        Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    Else !If job:Warranty_Invoice_Number <> 0
        expjobs:Line1   = Clip(expjobs:Line1) & ','
    End !If job:Warranty_Invoice_Number <> 0

    !Check the JOBTHIRD to see if there is an entry for this job.
    !If there is then that means it's been sent to third party,
    !therefore get the original IMEI /MSN incase it has changed.
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefNumberKey)
    If Access:JOBTHIRD.Next() = Level:Benign
        If jot:RefNumber = job:Ref_Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalIMEI)
        Else !If jot:RefNumber = job:Ref_Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
        End !If jot:RefNumber = job:Ref_Number
    Else !If Access:JOBTHIRD.Next() = Level:Benign
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
    End !If Access:JOBTHIRD.Next() = Level:Benign

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)

    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefNumberKey)
    If Access:JOBTHIRD.Next() = Level:Benign
        If jot:RefNumber = job:Ref_Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalMSN)
        Else !If jot:RefNumber = job:Ref_Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
        End !If jot:RefNumber = job:Ref_Number
    Else !If Access:JOBTHIRD.Next() = Level:Benign
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
    End !If Access:JOBTHIRD.Next() = Level:Benign

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)

    If job:Exchange_Unit_Number <> 0
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:Location)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:ESN)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:MSN)
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    Else !If job:Exchange_Unit_Number <> 0
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Exchange_Unit_Number <> 0

    If job:Third_Party_Site <> ''
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Third_Party_Site)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:ARC3rdPartyCost)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:ARC3rdPartyVAT)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Round(jobe:ARC3rdPartyCost,.01) + Round(jobe:ARC3rdPartyVAT,.01))
    Else !If job:Third_Party_Site <> ''
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Third_Party_Site <> ''

    tmp:ARCChargeableCostPrice = 0
    tmp:RRCChargeableCostPrice = 0

    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCChargeableCostPrice += par:AveragePurchaseCost
        tmp:RRCChargeableCostPrice += par:RRCAveragePurchaseCost
    End !Loop

    Access:PARTS.RestoreFile(Save_par_ID)

    !ARC Costs
    If job:Chargeable_Job = 'YES'
        If SentToHub(job:Ref_Number)
            tmp:CourierCost    = job:Courier_Cost
            tmp:PartsCost      = job:Parts_Cost
            tmp:LabourCost     = job:Labour_Cost

            !If job is an estimate, show the estimate costs UNTIL the job is invoiced (L761)
            !Do not show estimate costs for RRC to ARC jobs.
            If job:Estimate = 'YES' And job:Invoice_Number = 0 And ~jobe:WebJob
                tmp:CourierCost = job:Courier_Cost_Estimate
                tmp:PartsCost   = job:Parts_Cost_Estimate
                tmp:LabourCost  = job:Labour_Cost_Estimate
            End !If job:Estimate = 'YES' And job:Invoice_Number = 0

            !Show invoiced values, if the job has been
            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If job:Invoice_Number <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = job:Invoice_Number
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    tmp:CourierCost    = job:Invoice_Courier_Cost
                    tmp:PartsCost      = job:Invoice_Parts_Cost
                    tmp:LabourCost     = job:Invoice_Labour_Cost
                    tmp:LabourVatRate   = inv:Vat_Rate_Labour
                    tmp:PartsVatRate    = inv:Vat_Rate_Parts
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCChargeableCostPrice,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n14.2))
            tmp:Vat = (tmp:LabourCost * tmp:LabourVatRate/100) + |
                                (tmp:PartsCost * tmp:PartsVatRate/100) + |
                                (tmp:CourierCost * tmp:LabourVatRate/100)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost + |
                                                            tmp:LabourCost + |
                                                            tmp:CourierCost + |
                                                            tmp:VAT,@n14.2))
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If SentToHub(job:Ref_Number)
    Else !If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Chargeable_Job = 'YES'

    If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Courier_Cost_Warranty))
    Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeATRRC
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeATRRC

    tmp:ARCWarrantyCostPrice = 0
    tmp:RRCWarrantyCostPrice = 0

    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCWarrantyCostPrice += wpr:AveragePurchaseCost
        tmp:RRCWarrantyCostPrice += wpr:RRCAveragePurchaseCost
    End !Loop

    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    If job:Warranty_Job = 'YES'
        If SentToHub(job:Ref_Number)
            !Show invoiced values, if the job has been
            tmp:CourierCost    = job:Courier_Cost
            tmp:PartsCost      = jobe:ClaimPartsCost
            tmp:LabourCost     = jobe:CLaimValue
            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If job:Invoice_Number_Warranty <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = job:Invoice_Number_Warranty
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    tmp:CourierCost    = job:WInvoice_Courier_Cost
                    tmp:PartsCost      = jobe:InvClaimPartsCost
                    tmp:LabourCost     = jobe:InvoiceClaimValue
                    tmp:LabourVatRate   = inv:Vat_Rate_Labour
                    tmp:PartsVatRate    = inv:Vat_Rate_Parts
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCWarrantyCostPrice,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n14.2))

            tmp:VAT = tmp:PartsCost * tmp:PartsVatRate/100 + |
                                                        tmp:LabourCost * tmp:LabourVatRate/100 + |
                                                        tmp:CourierCost * tmp:LabourVatRate/100
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n14.2))

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:PartsCost + |
                                                                    tmp:LabourCost + |
                                                                    tmp:CourierCost + |
                                                                    tmp:VAT)
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If SentToHub(job:Ref_Number)

    Else !If job:Warranty_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Warranty_Job = 'YES'

    tmp:HandlingFeeType = 'REPAIR'
    If SentToHub(job:Ref_Number)
        tmp:HandlingFeeLocation = 'ARC'
    Else !If SentToHub(job:Ref_Number)
        tmp:HandlingFeeLocation = 'RRC'
    End !If SentToHub(job:Ref_Number)


    If job:Exchange_Unit_Number <> ''
        tmp:HandlingFeeType = 'REPAIR'
        If jobe:ExchangedAtRRC
            tmp:HandlingFeeLocation = 'RRC'
        Else !If jobe:ExchangedAtRRC
            tmp:HandlingFeeLocation = 'ARC'
        End !If jobe:ExchangedAtRRC
    End !If job:Exchange_Unit_Number <> ''

    if jobe:OBFvalidateDate <> ''
        tmp:HandlingFeeType = 'OBF'
    End !if jobe:OBFvalidateDate <> ''

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:HandlingFeeType)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:HandlingFeeLocation)

    If jobe:WebJob
        If SentToHub(job:Ref_Number)
            If job:Exchange_Unit_Number <> 0
                If jobe:ExchangedATRRC
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:ExchangeRate)
                Else !If jobe:ExchangedATRRC
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:HandlingFee)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
                End !If jobe:ExchangedATRRC
            Else !If job:Exchange_Unit_Number <> 0
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:HandlingFee)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !If job:Exchange_Unit_Number <> 0
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If SentToHub(job:Ref_Number)
    Else !If jobe:WebJob
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If jobe:WebJob

    !RRC Chargeable Costs
    If job:Chargeable_Job = 'YES'
        !Only show costs for RRC jobs
        If jobe:WebJob
            tmp:CourierCost     = job:Courier_Cost
            tmp:PartsCost       = jobe:RRCCPartsCost
            tmp:LabourCost      = jobe:RRCCLabourCost

            !If job is an estimate, show the estimate costs UNTIL the job is invoiced (L761)
            If job:Estimate = 'YES' And job:Invoice_Number = 0
                tmp:CourierCost = job:Courier_Cost_Estimate
                tmp:PartsCost   = jobe:RRCEPartsCost
                tmp:LabourCost  = jobe:RRCELabourCost
            End !If job:Estimate = 'YES' And job:Invoice_Number = 0

            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If job:Invoice_Number <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = job:Invoice_Number
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    If inv:RRCInvoiceDate <> ''
                        tmp:CourierCost     = job:Invoice_Courier_Cost
                        tmp:PartsCost      = jobe:InvRRCCPartsCost
                        tmp:LabourCost     = jobe:InvRRCCLabourCost
                        tmp:LabourVatRate   = inv:Vat_Rate_Labour
                        tmp:PartsVatRate    = inv:Vat_Rate_Parts
                    End !If inv:RRCInvoiceDate <> ''
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:RRCChargeableCostPrice,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n14.2))
            tmp:Vat = (tmp:LabourCost * tmp:LabourVatRate/100) + |
                                (tmp:PartsCost * tmp:PartsVatRate/100) + |
                                (tmp:CourierCost * tmp:LabourVatRate/100)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost + |
                                                                    tmp:PartsCost + |
                                                                    tmp:CourierCost + |
                                                                    tmp:VAT,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:CourierCost,@n14.2))
        Else !If ~SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If ~SentToHub(job:Ref_Number)
    Else !If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Chargeable_Job = 'YES'

    !RRC Warranty Costs
    If job:Warranty_Job = 'YES'
        If ~SentToHub(job:Ref_Number)
            !Show invoiced values, if the job has been
            tmp:PartsCost       = jobe:RRCWPartsCost
            tmp:LabourCost      = jobe:RRCWLabourCost
            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If wob:RRCWInvoiceNumber <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = wob:RRCWInvoiceNumber
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    tmp:PartsCost      = jobe:InvRRCWPartsCost
                    tmp:LabourCost     = jobe:InvRRCWLabourCost
                    tmp:LabourVatRate   = inv:Vat_Rate_Labour
                    tmp:PartsVatRate    = inv:Vat_Rate_Parts
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:RRCWarrantyCostPrice,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n14.2))
            tmp:Vat = (tmp:LabourCost * tmp:LabourVatRate/100) + |
                                (tmp:PartsCost * tmp:PartsVatRate/100)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n14.2))

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost + |
                                                                    tmp:PartsCost + |
                                                                    tmp:VAT,@n14.2))
        Else !If ~SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If ~SentToHub(job:Ref_Number)
    Else !If job:Warranty_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Warranty_Job = 'YES'

    !ARC Charge and ARC Markup
    If job:Chargeable_Job = 'YES'
        If jobe:WebJob And SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format((job:Courier_Cost+job:Parts_Cost+job:Labour_Cost) + (jobe:ARC3rdPartyCost),@n14.2))
            tmp:ARCMarkup = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost
            if tmp:ARCMarkup <> 0
                tmp:ARCMarkup = tmp:ARCMarkup - tmp:ARCCharge
                if tmp:ARCMarkup < 0
                    tmp:ARCMarkup = 0
                end
            end
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCMarkup,@n14.2))
        Else !If jobe:WebJob And SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If jobe:WebJob And SentToHub(job:Ref_Number)

    Else !If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Chargeable_Job = 'YES'

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Account_Number)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Order_Number)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Authority_Number)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Fault_Description))

    !Lets hope that the main out fault is written in the fault code field.
    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:MainFault    = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        Case maf:Field_Number
            Of 1
                mfo:Field        = job:Fault_Code1
            Of 2
                mfo:Field        = job:Fault_Code2
            Of 3
                mfo:Field        = job:Fault_Code3
            Of 4
                mfo:Field        = job:Fault_Code4
            Of 5
                mfo:Field        = job:Fault_Code5
            Of 6
                mfo:Field        = job:Fault_Code6
            Of 7
                mfo:Field        = job:Fault_Code7
            Of 8
                mfo:Field        = job:Fault_Code8
            Of 9
                mfo:Field        = job:Fault_Code9
            Of 10
                mfo:Field        = job:Fault_Code10
            Of 11
                mfo:Field        = job:Fault_Code11
            Of 12
                mfo:Field        = job:Fault_Code12
        End !Case maf:Field_Number
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(mfo:Description)
        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code1)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code2)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code3)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code4)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code5)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code6)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code7)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code8)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code9)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code10)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code11)
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code12)
    ! Inserting (DBH 09/11/2006) # 8457 - Add extra fault codes
    !Fault Code 13
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode13)
    !Fault Code 14
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode14)
    !Fault Code 15
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode15)
    !Fault Code 16
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode16)
    !Fault Code 17
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode17)
    !Fault Code 18
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode18)
    !Fault Code 19
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode19)
    !Fault Code 20
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode20)
    ! End (DBH 09/11/2006) #8457

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Engineers_Notes))
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Invoice_Text))

    Count# = 0
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If

        Count# += 1

        If Count# > 5
            Break
        End !If Count# > 5

        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(par:Part_Number)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(par:Description)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(par:Quantity)

        If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:AveragePurchaseCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:Sale_Cost,@n14.2))
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCAveragePurchaseCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCSaleCost,@n14.2))
        End !If SentToHub(job:Ref_Number)

    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)

    If Count# < 5
        Loop
            Count# += 1
            If Count# > 5
                Break
            End !If Count# > 5
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop
    End !If Count# < 5

    Count# = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If

        Count# += 1

        If Count# > 5
            Break
        End !If Count# > 5

        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wpr:Part_Number)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wpr:Description)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wpr:Quantity)

        If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:AveragePurchaseCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:Purchase_Cost,@n14.2))
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCAveragePurchaseCost,@n14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCPurchaseCost,@n14.2))
        End !If SentToHub(job:Ref_Number)

    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    If Count# < 5
        Loop
            Count# += 1
            If Count# > 5
                Break
            End !If Count# > 5
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop
    End !If Count# < 5

    ! Who booked  ! #11836 New columns (Bryan: 16/05/2011)
    !TB13200 - special case of WEB now introducde
    if Job:Who_Booked = 'WEB' then
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(jobe2:SIDBookingName))
    ELSE
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = job:Who_Booked
        IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
        ELSE
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        END
    END !tb13200 web
    ! Who despatched to customer    ! #11836 New columns (Bryan: 16/05/2011)

    found# = 0
    IF (job:Date_Completed <> '')
        Access:JOBSCONS.Clearkey(joc:DateKey)
        joc:RefNumber = job:Ref_Number
        joc:TheDate = 0
        Set(joc:DateKey,joc:DateKey)
        LOOP UNTIL Access:JOBSCONS.Next()
            IF (joc:RefNumber <> job:Ref_Number)
                BREAK
            END
            IF (joc:DespatchFrom = 'RRC' AND joc:DespatchTo = 'CUSTOMER')
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code = joc:UserCode
                IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
                    found# = 1
                    BREAK
                END
            END ! IF (joc:DespatchFrom = 'RRC' AND joc:DespatchTo = 'CUSTOMER')

        END
    END !IF (job:Date_Completed <> )
    IF (Found# = 0)
        expjobs:Line1   = Clip(expjobs:Line1) & ','
    END


    expjobs:line1   = StripQuotes(expjobs:Line1)
    Add(ExportFileJobs)
JobsFileTitleSimple       Routine  !SHORT TITLE
        Clear(expjobs:Record)
        expjobs:Line1   = 'STATUS REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
        Add(ExportFileJobs)

        Clear(expjobs:Record)

        expjobs:Line1   = 'Job No'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Booked')


        !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column C (after the column Date Booked)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'Date Received From PUP'
        !end TB13236

        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Telephone')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Customer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Phone')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Model Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Repair Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job/Exchange')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Days in Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Total Days')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Eng')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Booked By')    ! #11836 New columns (Bryan: 16/05/2011)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Despatched By')  ! #11836 New columns (Bryan: 16/05/2011)

        Add(ExportFileJobs)
ExportJobsSimple      Routine       !SHORT EXPORT

            Clear(expjobs:Record)
            tmp:Job_Number = job:Ref_Number

            access:webjob.clearkey(wob:RefNumberKey)
            wob:refnumber = job:Ref_Number
            if not access:webjob.fetch(wob:RefNumberKey)
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = wob:HeadAccountNumber
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Job_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
            end
            access:jobse.clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:ref_number
            if access:jobse.fetch(jobe:RefnumberKey)
                !error
            END
            !TB136200 - J - 26/02/14 - and new field - Booking agent  from jobse2
            Access:jobse2.clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_number
            if access:jobse2.fetch(jobe2:RefNumberKey)
                !error
            END
            !END TB13200

            expjobs:Line1   = StripComma(tmp:Job_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Booked,@d06))

            !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column C
            LOC:RRCDateRecived = ''    !reset to blank
            LOC:PUPflag = 0            !used to indicate this has been at PUP
            
            Access:LocatLog.ClearKey(lot:DateKey)
            lot:RefNumber = job:Ref_number
            lot:TheDate = 0
            SET(lot:DateKey,lot:DateKey)
            LOOP
                if Access:LocatLog.NEXT() then break.
                IF lot:RefNumber <> job:Ref_Number then break.
                
                IF instring('PUP',lot:PreviousLocation,1,1) then LOC:PUPflag = 1.    !it has been at the PUP
                IF lot:NewLocation <> LOC:RRCLocation then cycle.

                if lot:NewLocation = LOC:ARCLocation then break.  !has been sent to ARC, there will be a return date that we do not want
                
                if LOC:PUPflag = 1 then
                    LOC:RRCDateRecived = Format(lot:TheDate,@d06)
                END
                BREAK

            END !LOOP

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & clip(LOC:RRCDateRecived)

            !end TB13236

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Account_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(clip(job:Title)&' '&clip(job:Initial)&' '&clip(job:Surname))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:EndUserTelNo)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Model_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)

            !TB13236 - J - 18/02/14 - If exchange issued at RRC then this is RRC otherwise as before
            If job:Exchange_Unit_Number <> ''  and  jobe:ExchangedATRRC then
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
            ELSE !ELSE of TB13236
                If SentToHub(job:Ref_Number)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC')
                Else !If SentToHub(job:RefNumber)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
                End !If SentToHub(job:RefNumber)
            END  !END of  TB13236

            if job:Exchange_Unit_Number = 0 then
                !no exchange unit attached
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'J'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(WOB:Current_status)
                !work out days in status
                If wob:Current_Status_Date = 0
                    ! Show a blank column if the field is blank (i.e. an aborted job) - TrkBs: 6634 (DBH: 02-11-2005)
                    statusdays" = 0
                Else ! If wob:Current_Status_Date = 0
                    statusdays" = LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Current_Status_Date)   !was today() - wob:Current_Status_Date
                End ! If wob:Current_Status_Date = 0

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StatusDays")

            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'E'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(WOB:Exchange_status)
                !work out days in status
                If wob:Exchange_Status_Date = 0
                    statusdays" = 0
                Else !If wob:Exchange_Status_Date = 0
                    statusdays" = LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Exchange_Status_Date)   !was today() - wob:Exchange_Status_Date
                End !If wob:Exchange_Status_Date = 0

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StatusDays")
            END
            statusdays" = LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,job:date_booked)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Statusdays")                               !was today()- job:date_booked)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Location)

            !Work out jjob type
            if job:Warranty_Job = 'YES' then
                if job:Chargeable_Job = 'YES'
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'S'
                ELSE !if both
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'W'
                END !if both
            ELSE
                if job:Chargeable_Job = 'YES'
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'C'
                ELSE !if only chargeable
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'N'
                END !if NEither?
            END !if warranty job
            if job:Loan_Unit_Number = 0 then
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'NO'
            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'YES'
            END !if no loan
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Engineer)


            ! Who booked  ! #11836 New columns (Bryan: 16/05/2011)
            !TB136200 - J - 26/02/14 - and new field - Special case of WEB            
            if job:who_booked = 'WEB' then
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe2:SIDBookingName)
            ELSE
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code = job:Who_Booked
                IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
                ELSE
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                END
            END !TB13200 WEB
            ! Who despatched to customer    ! #11836 New columns (Bryan: 16/05/2011)

            found# = 0
            IF (job:Date_Completed <> '')
                Access:JOBSCONS.Clearkey(joc:DateKey)
                joc:RefNumber = job:Ref_Number
                joc:TheDate = 0
                Set(joc:DateKey,joc:DateKey)
                LOOP UNTIL Access:JOBSCONS.Next()
                    IF (joc:RefNumber <> job:Ref_Number)
                        BREAK
                    END
                    IF (joc:DespatchFrom = 'RRC' AND joc:DespatchTo = 'CUSTOMER')
                        Access:USERS.Clearkey(use:User_Code_Key)
                        use:User_Code = joc:UserCode
                        IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
                            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
                            found# = 1
                            BREAK
                        END
                    END ! IF (joc:DespatchFrom = 'RRC' AND joc:DespatchTo = 'CUSTOMER')

                END
            END !IF (job:Date_Completed <> )
            IF (Found# = 0)
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            END

            Add(ExportFileJobs)

SummaryStatusReport        Routine
! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
    If IsDateInValid(tmp:StartDate)
        Beep(Beep:SystemHand);  Yield()
       Case Missive('Invalid Start Date.','ServiceBase 3g',|
                      'mstop.jpg','/OK')
           Of 1 ! OK Button
       End ! Case Missive
       tmp:StartDate = ''
       Select(?tmp:StartDate)
       Exit
    End ! If IsDateInValid(tmp:StartDate)
    If IsDateInValid(tmp:EndDate)
        Beep(Beep:SystemHand);  Yield()
       Case Missive('Invalid End Date.','ServiceBase 3g',|
                      'mstop.jpg','/OK')
           Of 1 ! OK Button
       End ! Case Missive
       tmp:EndDate = ''
       Select(?tmp:EndDate)
       Exit
    End ! If IsDateInValid(tmp:StartDate)

    Beep(Beep:SystemQuestion);  Yield()
    Case Missive('Confirm Date Range:'&|
        '|    From:   ' & Format(tmp:StartDate,@d18) & |
        '|    To:        ' & Format(tmp:EndDate,@d18) & |
        '||Run Report?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
        Of 1 ! No Button
            Display()
            Exit
    End ! Case Missive
! End (DBH 14/02/2008) #9752
    Status_Summary_Report(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:ReportOrder,tmp:PaperReportType,tmp:JobBatchNumber,tmp:EDIBatchNumber,tmp:CustomerStatus)
StatusReport        Routine
! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
    If IsDateInValid(tmp:StartDate)
        Beep(Beep:SystemHand);  Yield()
       Case Missive('Invalid Start Date.','ServiceBase 3g',|
                      'mstop.jpg','/OK')
           Of 1 ! OK Button
       End ! Case Missive
       tmp:StartDate = ''
       Select(?tmp:StartDate)
       Exit
    End ! If IsDateInValid(tmp:StartDate)
    If IsDateInValid(tmp:EndDate)
        Beep(Beep:SystemHand);  Yield()
       Case Missive('Invalid End Date.','ServiceBase 3g',|
                      'mstop.jpg','/OK')
           Of 1 ! OK Button
       End ! Case Missive
       tmp:EndDate = ''
       Select(?tmp:EndDate)
       Exit
    End ! If IsDateInValid(tmp:StartDate)

    Beep(Beep:SystemQuestion);  Yield()
    Case Missive('Confirm Date Range:'&|
        '|    From:   ' & Format(tmp:StartDate,@d18) & |
        '|    To:        ' & Format(tmp:EndDate,@d18) & |
        '||Run Report?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
        Of 1 ! No Button
            Display()
            Exit
    End ! Case Missive
! End (DBH 14/02/2008) #9752
    StatusReport(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:ReportOrder,tmp:PaperReportType,tmp:JobBatchNumber,tmp:EDIBatchNumber,tmp:CustomerStatus)
SaveCriteria        Routine
    if glo:webjob then
        tmp:SavedLocation = clip(ClarioNET:Global.Param2)
    Else
        tmp:SavedLocation = ''
    End !if glo webjob

    tmp:NewDescription = InsertCriteriaDescription()

    If tmp:NewDescription <> ''
        If Access:STATREP.PrimeRecord() = Level:Benign
            star:Description = tmp:NewDescription
            star:Location = tmp:SavedLocation

            If Access:STATREP.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:STATREP..TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:STATREP..TryInsert() = Level:Benign

            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'HEAD ACCOUNT'
                    stac:FieldValue  = glo:Pointer
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue2)
                Get(glo:Queue2,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'SUB ACCOUNT'
                    stac:FieldValue  = glo:Pointer2
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue3)
                Get(glo:Queue3,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'CHA CHARGE TYPE'
                    stac:FieldValue  = glo:Pointer3
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue4)
                Get(glo:Queue4,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'WAR CHARGE TYPE'
                    stac:FieldValue  = glo:Pointer4
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue5)
                Get(glo:Queue5,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'CHA REPAIR TYPE'
                    stac:FieldValue  = glo:Pointer5
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue6)
                Get(glo:Queue6,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'WAR REPAIR TYPE'
                    stac:FieldValue  = glo:Pointer6
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue7)
                Get(glo:Queue7,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'STATUS'
                    stac:FieldValue  = glo:Pointer7
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue8)
                Get(glo:Queue8,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'LOCATION'
                    stac:FieldValue  = glo:Pointer8
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)


            Loop x# = 1 To Records(glo:Queue9)
                Get(glo:Queue9,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'ENGINEER'
                    stac:FieldValue  = glo:Pointer9
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue10)
                Get(glo:Queue10,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'MANUFACTURER'
                    stac:FieldValue  = glo:Pointer10
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue11)
                Get(glo:Queue11,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'MODEL NUMBER'
                    stac:FieldValue  = glo:Pointer11
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue12)
                Get(glo:Queue12,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'UNIT TYPE'
                    stac:FieldValue  = glo:Pointer12
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue13)
                Get(glo:Queue13,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'TRANSIT TYPE'
                    stac:FieldValue  = glo:Pointer13
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue14)
                Get(glo:Queue14,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:Location = tmp:savedLocation
                    stac:OptionType  = 'TURNAROUND TIME'
                    stac:FieldValue  = glo:Pointer14
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'OUTPUT TYPE'
                stac:FieldValue     = tmp:OutputType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'PAPER REPORT TYPE'
                stac:FieldValue     = tmp:PaperReportType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'EXPORT JOBS'
                stac:FieldValue     = tmp:ExportJobs
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'EXPORT AUDIT'
                stac:FieldValue     = tmp:ExportAudit
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'EXPORT PARTS'
                stac:FieldValue     = tmp:ExportParts
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'EXPORT WARPARTS'
                stac:FieldValue     = tmp:ExportWarParts
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'STATUS TYPE'
                stac:FieldValue     = tmp:StatusType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'SELECT MANUFACTURER'
                stac:FieldValue     = tmp:SelectManufacturer
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'PICK MANUFACTURER'
                stac:FieldValue     = tmp:Manufacturer
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'WORKSHOP'
                stac:FieldValue     = tmp:Workshop
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'JOBTYPE'
                stac:FieldValue     = tmp:JobType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'COMPLETED TYPE'
                stac:FieldValue     = tmp:CompletedType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'DESPATCHED TYPE'
                stac:FieldValue     = tmp:DespatchedType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'JOB BATCH NUMBER'
                stac:FieldValue     = tmp:JobBatchNumber
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'EDI BATCH NUMBER'
                stac:FieldValue     = tmp:EDIBatchNumber
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'REPORT ORDER'
                stac:FieldValue     = tmp:ReportOrder
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:Location = tmp:savedLocation
                stac:OptionType     = 'DATE RANGE TYPE'
                stac:FieldValue     = tmp:DateRangeType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

        End !If Access:STATREP..PrimeRecord() = Level:Benign
    End !If tmp:NewDescription <> ''
JobsFileTitle       Routine
        Clear(expjobs:Record)
        expjobs:Line1   = 'STATUS REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
        Add(ExportFileJobs)

        Clear(expjobs:Record)
        expjobs:Line1   = 'Job No'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Batch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Who Booked')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Booked')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Booked')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Title')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Initial')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Surname')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Order Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Model Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Manufacturer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Unit Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Mobile Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Workshop')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Authority Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Or Purchase')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Transit Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Intermittent Fault')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Turnaround Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Postcode')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Company Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Address 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Address 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Address 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Telephone Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fax Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Postcode')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Company Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Address 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Address 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Address 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Telephone Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Postcode')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Company Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Address 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Address 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Address 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Telephone Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Repair')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Repair Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Repair Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('On Test')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('On Test Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('On Test Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('QA Passed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('QA Passed Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('QA Passed Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Job')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate If Over')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Ready')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Accepted')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Rejected')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Courier Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Labour Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Sub Total')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Courier Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Labour Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Sub Total')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Actual Chargeable Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Courier Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Labour Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Sub Total')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Actual Warranty Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Chargeable Paid')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Warranty Paid')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Unit Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Issue Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Despatch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Despatch User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Unit Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Issue Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Despatch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Despatch User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Date Despatched')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Despatch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Despatch User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Despatched Flag')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Third Party Site')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 4')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 5')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 6')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 7')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 8')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 9')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 10')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 11')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 12')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 4')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 5')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 6')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 7')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 8')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 9')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 10')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 11')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 12')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('EDI Batch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('EDI Flag')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineers Notes')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('SIM Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Skill Level')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Status Days')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Days')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Courier Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Labour Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Sub Total')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Courier Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Labour Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Parts Cost')
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Sub Total')





        Add(ExportFileJobs)
AuditFileTitle      Routine
        Clear(expaudit:Record)
        expaudit:Line1    = 'Job Number'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Date'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Time'
        expaudit:Line1    = Clip(expaudit:Line1) & ',User'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Action'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Notes'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Type'
        Add(ExportFileAudit)
ExportAudit      Routine
        Clear(expaudit:Record)

        tmp:Job_Number = aud:Ref_Number

        access:webjob.clearkey(wob:RefNumberKey)
        wob:refnumber = aud:Ref_Number
        if not access:webjob.fetch(wob:RefNumberKey)
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:Job_Number = aud:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
        end

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
        END ! IF

        expaudit:Line1    = StripComma(tmp:Job_Number)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(Format(aud:Date,@d06))
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(Format(aud:Time,@t01))
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:User)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Action)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud2:Notes)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Type)
        expaudit:Line1    = StripQuotes(expaudit:Line1)
        Add(ExportFileAudit)
PartsFileTitle      Routine
        Clear(expparts:Record)
        expparts:Line1    = 'Job Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Part Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Description'
        expparts:Line1    = Clip(expparts:Line1) & ',Supplier'
        !expparts:Line1    = Clip(expparts:Line1) & ',Purchase Cost'
        !expparts:Line1    = Clip(expparts:Line1) & ',Sale Cost'
        !expparts:Line1    = Clip(expparts:Line1) & ',Retail Cost'
        expparts:Line1    = Clip(expparts:Line1) & ',Quantity'
        expparts:Line1    = Clip(expparts:Line1) & ',Exclude From Order'
        expparts:Line1    = Clip(expparts:Line1) & ',Despatch Note Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Order Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Date Received'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Codes Checked'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 1'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 2'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 3'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 4'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 5'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 6'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 7'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 8'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 9'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 10'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 11'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 12'
        expparts:Line1    = Clip(expparts:Line1) & ',First Part'
        Add(ExportFileParts)
ExportParts      Routine
        Clear(expparts:Record)

        tmp:Job_Number = par:Ref_Number

        access:webjob.clearkey(wob:RefNumberKey)
        wob:refnumber = par:Ref_Number
        if not access:webjob.fetch(wob:RefNumberKey)
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:Job_Number = par:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
        end

        expparts:Line1    = Stripcomma(tmp:Job_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Part_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Description)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Supplier)
        !expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Purchase_Cost)
        !expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Sale_Cost)
        !expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(Par:Retail_Cost)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Quantity)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Exclude_From_Order)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Despatch_Note_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Order_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(Format(par:Date_Received,@d06))
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Codes_Checked)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code1)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code2)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code3)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code4)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code5)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code6)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code7)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code8)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code9)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code10)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code11)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code12)
        If tmp:FirstPart = 0
            expparts:Line1    = Clip(expparts:Line1) & ',1'
            tmp:FirstPart = 1
        Else !If tmp:FirstPart = 0
            expparts:Line1    = Clip(expparts:Line1) & ',0'
        End !If tmp:FirstPart = 0
        expparts:Line1      = StripQuotes(expparts:Line1)
        Add(ExportFileParts)
WarPartsFileTitle       Routine
        Clear(expwarparts:Record)
        expwarparts:Line1    = 'Job Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Part Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Description'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Supplier'
        !expwarparts:Line1    = Clip(expparts:Line1) & ',Purchase Cost'
        !expwarparts:Line1    = Clip(expparts:Line1) & ',Sale Cost'
        !expwarparts:Line1    = Clip(expparts:Line1) & ',Retail Cost'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Quantity'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Exclude From Order'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Despatch Note Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Order Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Date Received'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Codes Checked'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 1'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 2'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 3'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 4'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 5'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 6'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 7'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 8'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 9'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 10'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 11'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 12'
        expwarparts:Line1    = Clip(expparts:Line1) & ',First Part'
        Add(ExportFileWarParts)
ExportJobs      Routine        !NOT USED
            Clear(expjobs:Record)
            tmp:Job_Number = job:Ref_Number

            access:webjob.clearkey(wob:RefNumberKey)
            wob:refnumber = job:Ref_Number
            if not access:webjob.fetch(wob:RefNumberKey)
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = wob:HeadAccountNumber
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Job_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
            end

            expjobs:Line1   = StripComma(tmp:Job_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Batch_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Who_Booked)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Booked,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_Booked,@t01))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Title)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Initial)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Surname)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Account_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Order_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Chargeable_Job)

            If job:Chargeable_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Charge_Type)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type)
            Else !If job:Chargeable_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Chargeable_Job = 'YES'
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Warranty_Job)

            If job:Warranty_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Warranty_Charge_Type)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type_Warranty)
            Else !If job:Warranty_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Warranty_Job = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Model_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Manufacturer)

            !Check the JOBTHIRD to see if there is an entry for this job.
            !If there is then that means it's been sent to third party,
            !therefore get the original IMEI /MSN incase it has changed.
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.Next() = Level:Benign
                If jot:RefNumber = job:Ref_Number
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalIMEI)
                Else !If jot:RefNumber = job:Ref_Number
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
                End !If jot:RefNumber = job:Ref_Number
            Else !If Access:JOBTHIRD.Next() = Level:Benign
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
            End !If Access:JOBTHIRD.Next() = Level:Benign

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)

            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.Next() = Level:Benign
                If jot:RefNumber = job:Ref_Number
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalMSN)
                Else !If jot:RefNumber = job:Ref_Number
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
                End !If jot:RefNumber = job:Ref_Number
            Else !If Access:JOBTHIRD.Next() = Level:Benign
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
            End !If Access:JOBTHIRD.Next() = Level:Benign

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Unit_Type)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Mobile_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Workshop)

            !Location only applies if the job is in the workshop
            If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Location)
            Else !If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Workshop = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Authority_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:DOP,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Transit_Type)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Intermittent_Fault)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Turnaround_Time)

            If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Engineer)
            Else !If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Workshop = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Postcode)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line2)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line3)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fax_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Postcode_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line1_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line2_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line3_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Postcode_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line1_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line2_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line3_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:In_Repair)
            If job:In_Repair = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_In_Repair,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_In_Repair,@t01))
            Else !If job:In_Repair = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:In_Repair = 'YES'
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:On_Test)

            If job:On_Test = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_On_Test,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_In_Repair,@t01))
            Else !If job:On_Test = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:On_Test = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Completed)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Completed,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_Completed,@t01))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:QA_Passed)

            If job:QA_Passed = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_QA_Passed,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_QA_Passed,@t01))
            Else !If job:QA_Passed = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:QA_Passed = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate)

!            If job:Estimate = 'YES'
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_If_Over)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_Ready)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_Accepted)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_Rejected)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost_Estimate)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Labour_Cost_Estimate)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Parts_Cost_Estimate)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Sub_Total_Estimate)
!            Else !If job:Estimate = 'YES'
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!            End !If job:Estimate = 'YES'

!            If job:Chargeable_Job = 'YES'
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Labour_Cost)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Parts_Cost)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Sub_Total)
!                tmp:ActualPartsCost = 0
!                Save_par_ID = Access:PARTS.SaveFile()
!                Access:PARTS.ClearKey(par:Part_Number_Key)
!                par:Ref_Number  = job:Ref_Number
!                Set(par:Part_Number_Key,par:Part_Number_Key)
!                Loop
!                    If Access:PARTS.NEXT()
!                       Break
!                    End !If
!                    If par:Ref_Number  <> job:Ref_Number       |
!                        Then Break.  ! End If
!                    tmp:ActualPartsCost += par:Purchase_Cost * par:quantity
!                End !Loop
!                Access:PARTS.RestoreFile(Save_par_ID)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:ActualPartsCost)
!            Else !If job:Chargeale_Job = 'YES'
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!            End !If job:Chargeale_Job = 'YES'

!            If job:Warranty_Job = 'YES'
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost_Warranty)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Labour_Cost_Warranty)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Parts_Cost_Warranty)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Sub_Total_Warranty)
!                tmp:ActualPartsCost = 0
!                Save_wpr_ID = Access:WARPARTS.SaveFile()
!                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
!                wpr:Ref_Number  = job:Ref_Number
!                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!                Loop
!                    If Access:WARPARTS.NEXT()
!                       Break
!                    End !If
!                    If wpr:Ref_Number  <> job:Ref_Number       |
!                        Then Break.  ! End If
!                    tmp:ActualPartsCost += wpr:Purchase_Cost * wpr:quantity
!                End !Loop
!                Access:WARPARTS.RestoreFile(Save_wpr_ID)
!                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:ActualPartsCost)
!            Else !If job:Warranty_Job = 'YES'
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!                expjobs:Line1   = Clip(expjobs:Line1) & ','
!            End !If job:Warranty_Job = 'YES'

            !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Paid)
            !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Paid_Warranty)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Current_Status)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Status)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Status)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Unit_Number)
            If job:Loan_Unit_Number <> 0
                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                loa:Ref_Number  = job:Loan_Unit_Number
                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                    !Found
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(loa:ESN)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(loa:MSN)
                Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Loan_Issued_date,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_User)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Courier)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Consignment_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Despatch_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Despatched_User)
            Else !If job:Loan_Unit_Number <> 0
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Loan_Unit_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Unit_Number)

            If job:Exchange_Unit_Number <> 0
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:ESN)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:MSN)
                Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Exchange_Issued_Date,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_User)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Courier)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Consignment_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Despatch_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Despatched_User)
            Else !If job:Exchange_Unit_Number <> 0
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Exchange_Unit_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Despatched,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Despatch_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Despatch_User)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Consignment_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Incoming_Courier)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Incoming_Consignment_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Incoming_Date,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Despatched)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Third_Party_Site)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code2)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code3)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code4)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code5)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code6)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code7)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code8)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code9)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code10)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code11)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code12)

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            Access:JOBSE.TryFetch(jobe:RefNumberKey)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode2)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode3)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode4)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode5)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode6)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode7)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode8)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode9)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode10)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode11)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode12)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:EDI_Batch_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:EDI)

            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            Access:JOBNOTES.TryFetch(jbn:RefNumberKey)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Fault_Description))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Engineers_Notes))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Invoice_Text))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Collection_Text))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Delivery_Text))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:SIMNumber)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:SkillLevel)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Invoice_Date,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number_Warranty)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Invoice_Date_Warranty,@d06))

            Save_aus_ID = Access:AUDSTATS.SaveFile()
            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber   = job:Ref_Number
            aus:DateChanged = Today()
            Set(aus:DateChangedKey,aus:DateChangedKey)
            Loop
                If Access:AUDSTATS.PREVIOUS()
                   Break
                End !If
                If aus:RefNumber   <> job:Ref_Number      |
                Or aus:DateChanged > Today()      |
                    Then Break.  ! End If
                Case tmp:StatusType
                    Of 0
                        If aus:Type <> 'JOB'
                            Break
                        End !If aus:Type <> 'JOB'
                        If aus:newstatus = job:Current_Status

                            Days#    = GetDays(Sat#,Sun#,aus:DateChanged)
                            Break
                        End!If aud:newstatus = job:Current_Status

                    Of 1
                        If aus:Type <> 'EXC'
                            Break
                        End !If aus:Type <> 'EXC'
                        If aus:newstatus = job:Exchange_Status
                            Days#    = GetDays(Sat#,Sun#,aus:DateChanged)
                            Break
                        End!If aud:newstatus = job:Current_Status

                    Of 2
                        If aus:Type <> 'LOA'
                            Break
                        End !If aus:Type <> 'LOA'
                        If aus:newstatus = job:Loan_Status
                            Days#    = GetDays(Sat#,Sun#,aus:DateChanged)
                            Break
                        End!If aud:newstatus = job:Current_Status

                End !Case func:StatusType
            End !Loop
            Access:AUDSTATS.RestoreFile(Save_aus_ID)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Days#)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(GetDays(Sat#,Sun#,job:Date_Booked))

            expjobs:line1   = StripQuotes(expjobs:Line1)
            Add(ExportFileJobs)
ExportWarParts      Routine
        Clear(expwarparts:Record)

        tmp:Job_Number = wpr:Ref_Number

        access:webjob.clearkey(wob:RefNumberKey)
        wob:refnumber = wpr:Ref_Number
        if not access:webjob.fetch(wob:RefNumberKey)
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:Job_Number = wpr:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
        end

        expwarparts:Line1    = Stripcomma(tmp:Job_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Part_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Description)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Supplier)
        !expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Purchase_Cost)
        !expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Sale_Cost)
        !expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Retail_Cost)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Quantity)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Exclude_From_Order)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Despatch_Note_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Order_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(Format(wpr:Date_Received,@d06))
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Codes_Checked)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code1)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code2)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code3)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code4)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code5)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code6)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code7)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code8)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code9)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code10)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code11)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code12)
        If tmp:FirstPart = 0
            expwarparts:Line1    = Clip(expwarparts:Line1) & ',1'
            tmp:FirstPart = 1
        Else !If tmp:FirstPart = 0
            expwarparts:Line1    = Clip(expwarparts:Line1) & ',0'
        End !If tmp:FirstPart = 0
        expwarparts:Line1       = StripQuotes(expwarparts:Line1)
        Add(ExportFileWarParts)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020178'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Status_Report_Criteria_Web')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt46
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STATCRIT.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:WEBJOB.Open
  Access:STATREP.UseFile
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBSE.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:AUDSTATS.UseFile
  Access:REPTYDEF.UseFile
  Access:INVOICE.UseFile
  Access:JOBSCONS.UseFile
  Access:AUDIT2.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  !Use the server's temp folder to compile the report - TrkBs:  (DBH: 17-02-2005)
  tmp:ExportPath = Clip(Clarionet:Global.Param2) & ' StatusReport-JOBS ' & |
                  Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & Format(Clock(),@t2)
  If GetTempPathA(255,TempFilePath)
      If Sub(TempFilePath,-1,1) = '\'
          tmp:ExportPath = Clip(TempFilePath) & Clip(tmp:ExportPath)
      Else !If Sub(TempFilePath,-1,1) = '\'
          tmp:ExportPath = Clip(TempFilePath) & '\' & Clip(tmp:ExportPath)
      End !If Sub(TempFilePath,-1,1) = '\'
  End
  !End   - Use the server's temp folder to compile the report - TrkBs:  (DBH: 17-02-2005)
  
  !!Give the export a unique number - 3833 (DBH: 29-01-2004)
  !IF Sub(def:ExportPath,-1,1) <> '\'
  !    def:ExportPath = CLip(def:ExportPath) & '\'
  !End !Sub(def:ExportPath,-1,1) = '\'
  !tmp:ExportPath  = Clip(def:ExportPath) & Clip(Clarionet:Global.Param2) & ' StatusReport-JOBS ' & |
  !                Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & Format(Clock(),@t2)
  
  
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate = Today()
  tmp:ReportOrder = 'DATE BOOKED'
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:CHARTYPE,SELF)
  BRW7.Init(?List:4,Queue:Browse:3.ViewPosition,BRW7::View:Browse,Queue:Browse:3,Relate:CHARTYPE,SELF)
  BRW8.Init(?List:5,Queue:Browse:4.ViewPosition,BRW8::View:Browse,Queue:Browse:4,Relate:REPTYDEF,SELF)
  BRW9.Init(?List:6,Queue:Browse:5.ViewPosition,BRW9::View:Browse,Queue:Browse:5,Relate:REPTYDEF,SELF)
  BRW10.Init(?List:7,Queue:Browse:6.ViewPosition,BRW10::View:Browse,Queue:Browse:6,Relate:STATUS,SELF)
  BRW14.Init(?List:8,Queue:Browse:7.ViewPosition,BRW14::View:Browse,Queue:Browse:7,Relate:LOCINTER,SELF)
  BRW15.Init(?List:9,Queue:Browse:8.ViewPosition,BRW15::View:Browse,Queue:Browse:8,Relate:USERS,SELF)
  BRW16.Init(?List:10,Queue:Browse:9.ViewPosition,BRW16::View:Browse,Queue:Browse:9,Relate:MANUFACT,SELF)
  BRW17.Init(?List:11,Queue:Browse:10.ViewPosition,BRW17::View:Browse,Queue:Browse:10,Relate:MODELNUM,SELF)
  BRW21.Init(?List:12,Queue:Browse:11.ViewPosition,BRW21::View:Browse,Queue:Browse:11,Relate:UNITTYPE,SELF)
  BRW22.Init(?List:13,Queue:Browse:12.ViewPosition,BRW22::View:Browse,Queue:Browse:12,Relate:TRANTYPE,SELF)
  BRW23.Init(?List:14,Queue:Browse:13.ViewPosition,BRW23::View:Browse,Queue:Browse:13,Relate:TURNARND,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if glo:webjob then
      ?Tmp:ExportPath:prompt{Prop:hide}=true
      ?tmp:ExportPath{prop:hide}=true
      ?lookupExportPath{prop:hide}=true
      ?prompt13:2{prop:hide}=true
      ?prompt12:2{prop:hide}=true
  
  END !If glo:webjob
  ?List:2{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ?List:6{prop:vcr} = TRUE
  ?List:7{prop:vcr} = TRUE
  ?List:8{prop:vcr} = TRUE
  ?List:9{prop:vcr} = TRUE
  ?List:10{prop:vcr} = TRUE
  ?List:11{prop:vcr} = TRUE
  ?List:12{prop:vcr} = TRUE
  ?List:13{prop:vcr} = TRUE
  ?List:14{prop:vcr} = TRUE
  ?tmp:ReportOrder{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard13.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,sub:GenericAccountKey)
  BRW4.AddRange(sub:Generic_Account,tmp:True)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,sub:Account_Number,1,BRW4)
  BIND('tmp:HeadAccountTag',tmp:HeadAccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:HeadAccountTag,BRW4.Q.tmp:HeadAccountTag)
  BRW4.AddField(sub:Account_Number,BRW4.Q.sub:Account_Number)
  BRW4.AddField(sub:Company_Name,BRW4.Q.sub:Company_Name)
  BRW4.AddField(sub:RecordNumber,BRW4.Q.sub:RecordNumber)
  BRW4.AddField(sub:Generic_Account,BRW4.Q.sub:Generic_Account)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:SubAccountTag',tmp:SubAccountTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:SubAccountTag,BRW5.Q.tmp:SubAccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,cha:Warranty_Key)
  BRW6.AddRange(cha:Warranty,tmp:No)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,cha:Charge_Type,1,BRW6)
  BIND('tmp:CChargeTypeTag',tmp:CChargeTypeTag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:CChargeTypeTag,BRW6.Q.tmp:CChargeTypeTag)
  BRW6.AddField(cha:Charge_Type,BRW6.Q.cha:Charge_Type)
  BRW6.AddField(cha:Warranty,BRW6.Q.cha:Warranty)
  BRW7.Q &= Queue:Browse:3
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,cha:Warranty_Key)
  BRW7.AddRange(cha:Warranty,tmp:Yes)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,cha:Charge_Type,1,BRW7)
  BIND('tmp:WChargeType',tmp:WChargeType)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:WChargeType,BRW7.Q.tmp:WChargeType)
  BRW7.AddField(cha:Charge_Type,BRW7.Q.cha:Charge_Type)
  BRW7.AddField(cha:Warranty,BRW7.Q.cha:Warranty)
  BRW8.Q &= Queue:Browse:4
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,rtd:Chargeable_Key)
  BRW8.AddRange(rtd:Chargeable,tmp:Yes)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,rtd:Repair_Type,1,BRW8)
  BIND('tmp:CRepairTypeTag',tmp:CRepairTypeTag)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:CRepairTypeTag,BRW8.Q.tmp:CRepairTypeTag)
  BRW8.AddField(rtd:Repair_Type,BRW8.Q.rtd:Repair_Type)
  BRW8.AddField(rtd:Manufacturer,BRW8.Q.rtd:Manufacturer)
  BRW8.AddField(rtd:RecordNumber,BRW8.Q.rtd:RecordNumber)
  BRW8.AddField(rtd:Chargeable,BRW8.Q.rtd:Chargeable)
  BRW9.Q &= Queue:Browse:5
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,rtd:Warranty_Key)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,rtd:Warranty,1,BRW9)
  BIND('tmp:WRepairTypeTag',tmp:WRepairTypeTag)
  ?List:6{PROP:IconList,1} = '~notick1.ico'
  ?List:6{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:WRepairTypeTag,BRW9.Q.tmp:WRepairTypeTag)
  BRW9.AddField(rtd:Repair_Type,BRW9.Q.rtd:Repair_Type)
  BRW9.AddField(rtd:Manufacturer,BRW9.Q.rtd:Manufacturer)
  BRW9.AddField(rtd:RecordNumber,BRW9.Q.rtd:RecordNumber)
  BRW9.AddField(rtd:Warranty,BRW9.Q.rtd:Warranty)
  BRW10.Q &= Queue:Browse:6
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,sts:JobKey)
  BRW10.AddRange(sts:Job,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort1:Locator)
  BRW10::Sort1:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:ExchangeKey)
  BRW10.AddRange(sts:Exchange,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort2:Locator)
  BRW10::Sort2:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:LoanKey)
  BRW10.AddRange(sts:Loan,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort3:Locator)
  BRW10::Sort3:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:Status_Key)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,sts:Status,1,BRW10)
  BIND('tmp:StatusTag',tmp:StatusTag)
  ?List:7{PROP:IconList,1} = '~notick1.ico'
  ?List:7{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tmp:StatusTag,BRW10.Q.tmp:StatusTag)
  BRW10.AddField(sts:Status,BRW10.Q.sts:Status)
  BRW10.AddField(sts:Ref_Number,BRW10.Q.sts:Ref_Number)
  BRW10.AddField(sts:Job,BRW10.Q.sts:Job)
  BRW10.AddField(sts:Exchange,BRW10.Q.sts:Exchange)
  BRW10.AddField(sts:Loan,BRW10.Q.sts:Loan)
  BRW14.Q &= Queue:Browse:7
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,loi:Location_Key)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,loi:Location,1,BRW14)
  BIND('tmp:LocationTag',tmp:LocationTag)
  ?List:8{PROP:IconList,1} = '~notick1.ico'
  ?List:8{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(tmp:LocationTag,BRW14.Q.tmp:LocationTag)
  BRW14.AddField(loi:Location,BRW14.Q.loi:Location)
  BRW15.Q &= Queue:Browse:8
  BRW15.RetainRow = 0
  BRW15.AddSortOrder(,use:User_Type_Key)
  BRW15.AddRange(use:User_Type,tmp:ENGINEER)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,use:Surname,1,BRW15)
  BIND('tmp:UserTag',tmp:UserTag)
  ?List:9{PROP:IconList,1} = '~notick1.ico'
  ?List:9{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(tmp:UserTag,BRW15.Q.tmp:UserTag)
  BRW15.AddField(use:Surname,BRW15.Q.use:Surname)
  BRW15.AddField(use:Forename,BRW15.Q.use:Forename)
  BRW15.AddField(use:User_Code,BRW15.Q.use:User_Code)
  BRW15.AddField(use:User_Type,BRW15.Q.use:User_Type)
  BRW16.Q &= Queue:Browse:9
  BRW16.RetainRow = 0
  BRW16.AddSortOrder(,man:Manufacturer_Key)
  BRW16.AddLocator(BRW16::Sort0:Locator)
  BRW16::Sort0:Locator.Init(,man:Manufacturer,1,BRW16)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:10{PROP:IconList,1} = '~notick1.ico'
  ?List:10{PROP:IconList,2} = '~tick1.ico'
  BRW16.AddField(tmp:ManufacturerTag,BRW16.Q.tmp:ManufacturerTag)
  BRW16.AddField(man:Manufacturer,BRW16.Q.man:Manufacturer)
  BRW16.AddField(man:RecordNumber,BRW16.Q.man:RecordNumber)
  BRW17.Q &= Queue:Browse:10
  BRW17.RetainRow = 0
  BRW17.AddSortOrder(,mod:Model_Number_Key)
  BRW17.AddLocator(BRW17::Sort1:Locator)
  BRW17::Sort1:Locator.Init(,mod:Model_Number,1,BRW17)
  BRW17.AddSortOrder(,mod:Manufacturer_Key)
  BRW17.AddRange(mod:Manufacturer,tmp:Manufacturer)
  BRW17.AddLocator(BRW17::Sort2:Locator)
  BRW17::Sort2:Locator.Init(,mod:Model_Number,1,BRW17)
  BRW17.AddSortOrder(,mod:Manufacturer_Key)
  BRW17.AddLocator(BRW17::Sort0:Locator)
  BRW17::Sort0:Locator.Init(,mod:Manufacturer,1,BRW17)
  BIND('tmp:ModelNumberTag',tmp:ModelNumberTag)
  ?List:11{PROP:IconList,1} = '~notick1.ico'
  ?List:11{PROP:IconList,2} = '~tick1.ico'
  BRW17.AddField(tmp:ModelNumberTag,BRW17.Q.tmp:ModelNumberTag)
  BRW17.AddField(mod:Model_Number,BRW17.Q.mod:Model_Number)
  BRW17.AddField(mod:Manufacturer,BRW17.Q.mod:Manufacturer)
  BRW21.Q &= Queue:Browse:11
  BRW21.RetainRow = 0
  BRW21.AddSortOrder(,uni:Unit_Type_Key)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,uni:Unit_Type,1,BRW21)
  BIND('tmp:UnitTypeTag',tmp:UnitTypeTag)
  ?List:12{PROP:IconList,1} = '~notick1.ico'
  ?List:12{PROP:IconList,2} = '~tick1.ico'
  BRW21.AddField(tmp:UnitTypeTag,BRW21.Q.tmp:UnitTypeTag)
  BRW21.AddField(uni:Unit_Type,BRW21.Q.uni:Unit_Type)
  BRW22.Q &= Queue:Browse:12
  BRW22.RetainRow = 0
  BRW22.AddSortOrder(,trt:Transit_Type_Key)
  BRW22.AddLocator(BRW22::Sort0:Locator)
  BRW22::Sort0:Locator.Init(,trt:Transit_Type,1,BRW22)
  BIND('tmp:TransitTypeTag',tmp:TransitTypeTag)
  ?List:13{PROP:IconList,1} = '~notick1.ico'
  ?List:13{PROP:IconList,2} = '~tick1.ico'
  BRW22.AddField(tmp:TransitTypeTag,BRW22.Q.tmp:TransitTypeTag)
  BRW22.AddField(trt:Transit_Type,BRW22.Q.trt:Transit_Type)
  BRW23.Q &= Queue:Browse:13
  BRW23.RetainRow = 0
  BRW23.AddSortOrder(,tur:Turnaround_Time_Key)
  BRW23.AddLocator(BRW23::Sort0:Locator)
  BRW23::Sort0:Locator.Init(,tur:Turnaround_Time,1,BRW23)
  BIND('tmp:TurnaroundTimeTag',tmp:TurnaroundTimeTag)
  ?List:14{PROP:IconList,1} = '~notick1.ico'
  ?List:14{PROP:IconList,2} = '~tick1.ico'
  BRW23.AddField(tmp:TurnaroundTimeTag,BRW23.Q.tmp:TurnaroundTimeTag)
  BRW23.AddField(tur:Turnaround_Time,BRW23.Q.tur:Turnaround_Time)
  IF ?tmp:SelectManufacturer{Prop:Checked} = True
    ENABLE(?Prompt39)
    ENABLE(?tmp:Manufacturer)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = False
    DISABLE(?Prompt39)
    DISABLE(?tmp:Manufacturer)
  END
  FDCB18.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo
  FDCB18.AddSortOrder(man:Manufacturer_Key)
  FDCB18.AddField(man:Manufacturer,FDCB18.Q.man:Manufacturer)
  FDCB18.AddField(man:RecordNumber,FDCB18.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FileLookup34.Init
  FileLookup34.Flags=BOR(FileLookup34.Flags,FILE:LongName)
  FileLookup34.Flags=BOR(FileLookup34.Flags,FILE:Directory)
  FileLookup34.SetMask('All Files','*.*')
  FileLookup34.WindowTitle='Export Path'
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  BRW15.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW15.AskProcedure = 0
      CLEAR(BRW15.AskProcedure, 1)
    END
  END
  BRW16.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW16.AskProcedure = 0
      CLEAR(BRW16.AskProcedure, 1)
    END
  END
  BRW17.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW17.AskProcedure = 0
      CLEAR(BRW17.AskProcedure, 1)
    END
  END
  BRW21.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW21.AskProcedure = 0
      CLEAR(BRW21.AskProcedure, 1)
    END
  END
  BRW22.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW22.AskProcedure = 0
      CLEAR(BRW22.AskProcedure, 1)
    END
  END
  BRW23.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW23.AskProcedure = 0
      CLEAR(BRW23.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  ?DASSHOWTAG:7{PROP:Text} = 'Show All'
  ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue8)
  ?DASSHOWTAG:8{PROP:Text} = 'Show All'
  ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue8)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STATCRIT.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard13.Validate()
        DISABLE(Wizard13.NextControl())
     ELSE
        ENABLE(Wizard13.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:OutputType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OutputType, Accepted)
      if tmp:OutputType = 0 then
          hide(?Tmp:ShortReportFlag)
      ELse
          unhide(?Tmp:ShortReportFlag)
      
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OutputType, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectCriteria
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseStatusCriteria
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectCriteria, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              tmp:SavedCriteria   =   star:Description
              if glo:webjob then
                  tmp:SavedLocation = clip(ClarioNET:Global.Param2)
              Else
                  tmp:SavedLocation = ''
              End !if glo webjob
              Free(glo:Queue)
              Free(glo:Queue2)
              Free(glo:Queue3)
              Free(glo:Queue4)
              Free(glo:Queue5)
              Free(glo:Queue6)
              Free(glo:Queue7)
              Free(glo:Queue8)
              Free(glo:Queue9)
              Free(glo:Queue10)
              Free(glo:Queue11)
              Free(glo:Queue12)
              Free(glo:Queue13)
              Free(glo:Queue14)
      
      
              Setcursor(Cursor:Wait)
              Save_STAC_ID = Access:STATCRIT.SaveFile()
              Access:STATCRIT.ClearKey(stac:LocationDescriptionKey)
              STAC:Description = tmp:SavedCriteria
              STAC:Location    = tmp:SavedLocation
              Set(stac:LocationDescriptionKey,stac:LocationDescriptionKey)
              Loop
                  If Access:STATCRIT.NEXT()
                     Break
                  End !If
                  If STAC:Description <> tmp:SavedCriteria      |
                      Then Break.  ! End If
                  if STAC:Location    <> tmp:SavedLocation then break.
      
                  Case stac:OptionType
                      Of 'HEAD ACCOUNT'
                          glo:Pointer = stac:FieldValue
                          Add(glo:Queue)
                      Of 'SUB ACCOUNT'
                          glo:Pointer2 = stac:FieldValue
                          Add(glo:Queue2)
                      Of 'CHA CHARGE TYPE'
                          glo:Pointer3 = stac:FieldValue
                          Add(glo:Queue3)
                      Of 'WAR CHARGE TYPE'
                          glo:Pointer4 = stac:FieldValue
                          Add(glo:Queue4)
                      Of 'CHA REPAIR TYPE'
                          glo:Pointer5 = stac:FieldValue
                          Add(glo:Queue5)
                      Of 'WAR REPAIR TYPE'
                          glo:Pointer6 = stac:FieldValue
                          Add(glo:Queue6)
                      Of 'STATUS'
                          glo:Pointer7 = stac:FieldValue
                          Add(glo:Queue7)
                      Of 'LOCATION'
                          glo:Pointer8 = stac:FieldValue
                          Add(glo:Queue8)
                      Of 'ENGINEER'
                          glo:Pointer9 = stac:FieldValue
                          Add(glo:Queue9)
                      Of 'MANUFACTURER'
                          glo:Pointer10 = stac:FieldValue
                          Add(glo:Queue10)
                      Of 'MODEL NUMBER'
                          glo:Pointer11 = stac:FieldValue
                          Add(glo:Queue11)
                      Of 'UNIT TYPE'
                          glo:Pointer12 = stac:FieldValue
                          Add(glo:Queue12)
                      Of 'TRANSIT TYPE'
                          glo:Pointer13 = stac:FieldValue
                          Add(glo:Queue13)
                      Of 'TURNAROUND TIME'
                          glo:Pointer14 = stac:FieldValue
                          Add(glo:Queue14)
                      Of 'OUTPUT TYPE'
                          tmp:OutputType  = stac:FieldValue
                      Of 'PAPER REPORT TYPE'
                          tmp:PaperReportType = stac:FieldValue
                      Of 'EXPORT JOBS'
                          tmp:ExportJobs  = stac:FieldValue
                      Of 'EXPORT AUDIT'
                          tmp:ExportAudit = stac:FieldValue
                      Of 'EXPORT PARTS'
                          tmp:ExportParts = stac:FieldValue
                      Of 'EXPORT WARPARTS'
                          tmp:ExportWarParts = stac:FieldValue
                      Of 'STATUS TYPE'
                          tmp:StatusType = stac:FieldValue
                      Of 'SELECT MANUFACTURER'
                          tmp:SelectManufacturer  = stac:FieldValue
                      Of 'PICK MANUFACTURER'
                          tmp:Manufacturer    = stac:FieldValue
                      Of 'WORKSHOP'
                          tmp:Workshop = stac:FieldValue
                      Of 'JOBTYPE'
                          tmp:JobType = stac:FieldValue
                      Of 'COMPLETED TYPE'
                          tmp:Completedtype = stac:FieldValue
                      Of 'DESPATCHED TYPE'
                          tmp:DespatchedType = stac:FieldValue
                      Of 'JOB BATCH NUMBER'
                          tmp:JobBatchNumber  = stac:FieldValue
                      Of 'EDI BATCH NUMBER'
                          tmp:EDIBatchNumber  = stac:FieldValue
                      Of 'REPORT ORDER'
                          tmp:ReportOrder = stac:FieldValue
                      Of 'DATE RANGE TYPE'
                          tmp:DateRangeType = stac:FieldValue
                  End !Case stac:OptionType
              End !Loop
              Access:STATCRIT.RestoreFile(Save_STAC_ID)
              Setcursor()
      
              Post(Event:Accepted,?tmp:SelectManufacturer)
      
          Of Requestcancelled
      End!Case Globalreponse
      
      if tmp:OutputType = 0 then
          hide(?Tmp:ShortReportFlag)
      ELse
          unhide(?Tmp:ShortReportFlag)
      END
      
      Display()
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectCriteria, Accepted)
    OF ?LookupExportPath
      ThisWindow.Update
      tmp:ExportPath = Upper(FileLookup34.Ask(1)  )
      DISPLAY
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:StatusType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
      BRW10.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
    OF ?DASREVTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:SelectManufacturer
      IF ?tmp:SelectManufacturer{Prop:Checked} = True
        ENABLE(?Prompt39)
        ENABLE(?tmp:Manufacturer)
      END
      IF ?tmp:SelectManufacturer{Prop:Checked} = False
        DISABLE(?Prompt39)
        DISABLE(?tmp:Manufacturer)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
      BRW17.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW17.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020178'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020178'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020178'&'0')
      ***
    OF ?VSBackButton
      ThisWindow.Update
         Wizard13.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      DoNext# = 1
      Case Choice(?Sheet1)
          Of 1
              Case tmp:OutputType
                  Of 0
                      ?Tab3{prop:Hide} = 1
                      ?Tab2{prop:Hide} = 0
                  Of 1
                      ?Tab2{prop:Hide} = 1
                      ?Tab3{prop:Hide} = 0
              End !Case tmp:OutputType
          Of 3
              If tmp:ExportJobs = 0 And |
                  tmp:ExportAudit = 0 And |
                  tmp:ExportParts = 0 And |
                  tmp:ExportWarPArts = 0
                  Case Missive('You must select at least ONE file to export.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  DoNext# = 0
              End !tmp:ExportWarPArts = 0
          Of 6 !Job Types etc
              If tmp:Workshop = 2
                  ?Tab11{prop:Hide} = 1
                  ?Tab12{prop:Hide} = 1
              Else !If tmp:Workshop = 2
                  ?Tab11{prop:Hide} = 0
                  ?Tab12{prop:Hide} = 0
              End !If tmp:Workshop = 2
      
              Case tmp:JobType
                  Of 1 !Warranty Only
                      ?Tab6{prop:Hide} = 1
                      ?Tab8{prop:Hide} = 1
                      ?Tab7{prop:Hide} = 0
                      ?Tab9{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 0
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 0
                  Of 4 !Chargeable Only
                      ?Tab7{prop:Hide} = 1
                      ?Tab9{prop:Hide} = 1
                      ?Tab6{prop:Hide} = 0
                      ?Tab8{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 1
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 1
                  Else
                      ?Tab7{prop:Hide} = 0
                      ?Tab9{prop:Hide} = 0
                      ?Tab6{prop:Hide} = 0
                      ?Tab8{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 0
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 0
              End !Case tmp:JobType
      
              If tmp:CompletedType = 2
                  tmp:ReportOrder = 2
                  ?tmp:DateRangeType:Radio2{prop:Disable} = 1
                  ?tmp:ReportOrder{prop:From} = 'DATE BOOKED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER'
              Else !If tmp:CompltedType = 2
                  ?tmp:DateRangeType:Radio2{prop:Disable} = 0
                  ?tmp:ReportOrder{prop:From} = 'DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER'
              End !If tmp:CompltedType = 2
      End !Case Choice(?CurrentTab)
      If DoNext# = 1
         Wizard13.TakeAccepted()
      End !If DoNext# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      Case Missive('Do you want to save your criteria for future use, or run the report/export?','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|Run Only|Save-Run|Save Only')
          Of 4 ! Save Only Button
              Do SaveCriteria
              Post(Event:CloseWindow)
          Of 3 ! Save-Run Button
              Do SaveCriteria
      
              Case tmp:OutputType
                  Of 0 !Report
                      Case tmp:PaperReportType
                          Of 1
                              Do SummaryStatusReport
                          Else
                              Do StatusReport
                      End !Case tmp:PaperReportType
                  Of 1 !Export
                      Do Export
              End !Case tmp:OutputType
          Of 2 ! Run Only Button
      
              Case tmp:OutputType
                  Of 0 !Report
                      Case tmp:PaperReportType
                          Of 1
                              Do SummaryStatusReport
                          Else
                              Do StatusReport
                      End !Case tmp:PaperReportType
                  Of 1 !Export
                      Do Export
              End !Case tmp:OutputType
          Of 1 ! Cancel Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:2)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  OF ?List:7
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:7, AlertKey)
      If KeyCOde() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:7)
      End ! If KeyCOde() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:7, AlertKey)
    END
  OF ?List:8
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:8, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:8)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:8, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:7
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:8
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalGetDays       Procedure(String func:Sat,string func:Sun,Date func:Start)
    
loc:Count       Long
loc:Days        Long

    Code

    loc:Days = 0
    loc:Count = 0

    Loop

        loc:Count += 1

        If (func:Start + loc:Count) %7 = 0 And func:Sun <> 'YES'
            Cycle
        End
        If (func:Start + loc:Count) %7 = 6 And func:Sat <> 'YES'
            Cycle
        End

        loc:Days += 1

        If func:Start + loc:Count >= Today()
            Break
        End 

    End !Loop

    Return loc:Days
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:HeadAccountTag = ''
    ELSE
      tmp:HeadAccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:HeadAccounttag = '*')
    SELF.Q.tmp:HeadAccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:HeadAccountTag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::24:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sub:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:SubAccountTag = ''
    ELSE
      tmp:SubAccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:SubAccounttag = '*')
    SELF.Q.tmp:SubAccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:SubAccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(5, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  If sub:Main_Account_Number <> Clarionet:Global.Param2
      Return Record:Filtered
  End !sub:Main_Account_Number <> Clarionet:Global.Param2
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sub:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::25:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(5, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:CChargeTypetag = '*')
    SELF.Q.tmp:CChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:CChargeTypeTag_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:WChargeType = '*')
    SELF.Q.tmp:WChargeType_Icon = 2
  ELSE
    SELF.Q.tmp:WChargeType_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:CRepairTypetag = '*')
    SELF.Q.tmp:CRepairTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:CRepairTypeTag_Icon = 1
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:WRepairTypetag = '*')
    SELF.Q.tmp:WRepairTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:WRepairTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:StatusType = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:StatusType = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF tmp:StatusType = 2
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = sts:Status
     GET(glo:Queue7,glo:Queue7.Pointer7)
    IF ERRORCODE()
      tmp:StatusTag = ''
    ELSE
      tmp:StatusTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Statustag = '*')
    SELF.Q.tmp:StatusTag_Icon = 2
  ELSE
    SELF.Q.tmp:StatusTag_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = sts:Status
     GET(glo:Queue7,glo:Queue7.Pointer7)
    EXECUTE DASBRW::30:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue8.Pointer8 = loi:Location
     GET(glo:Queue8,glo:Queue8.Pointer8)
    IF ERRORCODE()
      tmp:LocationTag = ''
    ELSE
      tmp:LocationTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Locationtag = '*')
    SELF.Q.tmp:LocationTag_Icon = 2
  ELSE
    SELF.Q.tmp:LocationTag_Icon = 1
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue8.Pointer8 = loi:Location
     GET(glo:Queue8,glo:Queue8.Pointer8)
    EXECUTE DASBRW::31:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:Usertag = '*')
    SELF.Q.tmp:UserTag_Icon = 2
  ELSE
    SELF.Q.tmp:UserTag_Icon = 1
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW16.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:Manufacturertag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW16.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW17.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:SelectManufacturer = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:SelectManufacturer = 1
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW17.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:ModelNumbertag = '*')
    SELF.Q.tmp:ModelNumberTag_Icon = 2
  ELSE
    SELF.Q.tmp:ModelNumberTag_Icon = 1
  END


BRW17.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:UnitTypetag = '*')
    SELF.Q.tmp:UnitTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:UnitTypeTag_Icon = 1
  END


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW22.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:TransitTypetag = '*')
    SELF.Q.tmp:TransitTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:TransitTypeTag_Icon = 1
  END


BRW22.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW23.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (tmp:TurnaroundTimetag = '*')
    SELF.Q.tmp:TurnaroundTimeTag_Icon = 2
  ELSE
    SELF.Q.tmp:TurnaroundTimeTag_Icon = 1
  END


BRW23.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Wizard13.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW6.Q &= NULL) ! Has Browse Object been initialized?
       BRW6.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW7.Q &= NULL) ! Has Browse Object been initialized?
       BRW7.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW10.Q &= NULL) ! Has Browse Object been initialized?
       BRW10.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW14.Q &= NULL) ! Has Browse Object been initialized?
       BRW14.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW15.Q &= NULL) ! Has Browse Object been initialized?
       BRW15.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW16.Q &= NULL) ! Has Browse Object been initialized?
       BRW16.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW17.Q &= NULL) ! Has Browse Object been initialized?
       BRW17.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW21.Q &= NULL) ! Has Browse Object been initialized?
       BRW21.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW22.Q &= NULL) ! Has Browse Object been initialized?
       BRW22.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW23.Q &= NULL) ! Has Browse Object been initialized?
       BRW23.ResetQueue(Reset:Queue)
    END

Wizard13.TakeBackEmbed PROCEDURE
   CODE

Wizard13.TakeNextEmbed PROCEDURE
   CODE

Wizard13.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 500
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Stock_Check_AUDIT_Criteria PROCEDURE                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
Location_Temp        STRING(30)
tag_temp             STRING(1)
tmp:showquantity     BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW3::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Stock Check Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,246),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Site Location'),AT(168,90),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(240,90,124,10),USE(GLO:Select1),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           LIST,AT(240,106,148,186),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),VCR,FORMAT('11L(2)I@s1@120L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(392,182),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(184,140,50,13),USE(?DASREVTAG),HIDE
                           BUTTON,AT(392,212),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON('sho&W tags'),AT(168,170,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(392,242),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           OPTION('Report Order'),AT(240,296,200,28),USE(GLO:Select5),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Location'),AT(266,309),USE(?glo:select5:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('By Description'),AT(350,309),USE(?glo:select5:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                         END
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Check Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = los:Shelf_Location
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = los:Shelf_Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = los:Shelf_Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = los:Shelf_Location
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = los:Shelf_Location
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020179'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Check_AUDIT_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCSHELF.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  glo:select1 = ''
  glo:select2 = 'NO'
  glo:select3 = 'NO'
  glo:select4 = ''
  glo:select5 = 1
  glo:select6 = 0
  glo:select7 = Deformat('1/1/1990',@d6)
  glo:select8 = Today()
  glo:Select10 = 'YES' !exclude susspended stock
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      glo:Select1 = use:Location
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:LOCSHELF,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,los:Shelf_Location_Key)
  BRW3.AddRange(los:Site_Location,GLO:Select1)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,los:Shelf_Location,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(los:Shelf_Location,BRW3.Q.los:Shelf_Location)
  BRW3.AddField(los:Site_Location,BRW3.Q.los:Site_Location)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  glo:select4 = ''
  glo:select5 = ''
  glo:Select6 = ''
  glo:select7 = ''
  glo:select8 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCSHELF.Close
  END
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020179'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020179'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020179'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End!If glo:select1 = ''
      If error# = 0 And ~Records(glo:Queue)
          Case Missive('You must select at least ONE Shelf Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If error# = 0 And ~Records(glo:Queue)
      If error# = 0
          Stock_Check_Report(1)
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = los:Shelf_Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = los:Shelf_Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue







BastionWarrantyCheckReport PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
qa_reason_temp       STRING(255)
engineer_temp        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:txtInfo          STRING(5000)
tmp:JobType          STRING(30)
tmp:ChargeType       STRING(30)
tmp:SupplierInfo     STRING(500)
tmp:ActivityInfo     STRING(500)
tmp:BlacklistInfo    STRING(500)
tmp:UsageInfo        STRING(500)
tmp:SwapoutInfo      STRING(500)
tmp:WarrantyInfo     STRING(500)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(406,1677,7521,9146),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,9115),USE(?detailband)
                           STRING('Job Number:'),AT(313,208),USE(?String2),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(2979,208),USE(job_ali:Ref_Number),TRN,LEFT,FONT(,12,,FONT:bold)
                           STRING('Supplier'),AT(313,2031),USE(?String2:2),TRN,FONT(,12,,FONT:bold+FONT:underline,CHARSET:ANSI)
                           TEXT,AT(313,2333,6510,729),USE(tmp:SupplierInfo),FONT('Arial',12,,FONT:regular,CHARSET:ANSI)
                           STRING('Date Booked:'),AT(313,771),USE(?String6),TRN,FONT(,12,,FONT:bold)
                           STRING(@d6b),AT(2979,771),USE(job_ali:date_booked),LEFT,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Job Type:'),AT(313,1042),USE(?String31),TRN,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(2979,1042),USE(tmp:JobType),TRN,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('Charge Type:'),AT(313,1302),USE(?String32),TRN,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(2979,1302),USE(tmp:ChargeType),TRN,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('Activity On Vodacom Network'),AT(313,3177),USE(?String6:2),TRN,FONT(,12,,FONT:bold+FONT:underline)
                           TEXT,AT(313,3490,6510,729),USE(tmp:ActivityInfo),FONT('Arial',12,,FONT:regular,CHARSET:ANSI)
                           STRING('Blacklist / Greylist Information'),AT(313,4323),USE(?String6:3),TRN,FONT(,12,,FONT:bold+FONT:underline)
                           TEXT,AT(313,4635,6510,729),USE(tmp:BlacklistInfo),FONT('Arial',12,,FONT:regular,CHARSET:ANSI)
                           STRING('Usage Information'),AT(313,5469),USE(?String6:4),TRN,FONT(,12,,FONT:bold+FONT:underline)
                           TEXT,AT(313,5781,6510,729),USE(tmp:UsageInfo),FONT('Arial',12,,FONT:regular,CHARSET:ANSI)
                           STRING('I.M.E.I. Number'),AT(313,479),USE(?String6:5),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(2979,479),USE(job_ali:ESN),TRN,LEFT,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Swapout Information'),AT(313,6615),USE(?String6:7),TRN,FONT(,12,,FONT:bold+FONT:underline)
                           TEXT,AT(313,6927,6510,729),USE(tmp:SwapoutInfo),FONT('Arial',12,,FONT:regular,CHARSET:ANSI)
                           STRING('Warranty Information'),AT(313,7760),USE(?String6:8),TRN,FONT(,12,,FONT:bold+FONT:underline)
                           TEXT,AT(313,8073,6510,729),USE(tmp:WarrantyInfo),FONT('Arial',12,,FONT:regular,CHARSET:ANSI)
                         END
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('BASTION WARRANTY CHECK'),AT(1188,52,5156,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('BastionWarrantyCheckReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFPRINT.Open
  Access:AUDIT.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = 1
      
      Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
      job_ali:Ref_Number = GLO:Select1
      set(job_ali:Ref_Number_Key, job_ali:Ref_Number_Key)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        if Access:JOBS_ALIAS.Next()
            LocalResponse = RequestCompleted
            break
        end
        
        if job_ali:Ref_Number <> GLO:Select1
            LocalResponse = RequestCompleted
            break
        end
        
        if job_ali:Warranty_Job = 'YES'
            tmp:JobType = 'WARRANTY'
            tmp:ChargeType = job_ali:Warranty_Charge_Type
        end
        
        if job_ali:Chargeable_Job = 'YES'
            if tmp:JobType = ''
                tmp:JobType = 'CHARGEABLE'
                tmp:ChargeType = job_ali:Charge_Type
            else
                tmp:JobType = clip(tmp:JobType) & '/CHARGEABLE'
                tmp:ChargeType = clip(tmp:ChargeType) & '/' & job_ali:Charge_Type
            end
        end
        
        do DisplayProgress
        
        setcursor(cursor:wait)
        save_aud_id = access:audit.savefile()
        access:audit.clearkey(aud:action_key)
        aud:ref_number = glo:select1
        aud:action     = 'BASTION WARRANTY CHECK'
        set(aud:action_key,aud:action_key)
        loop
            if access:audit.next()
               break
            end !if
            if aud:ref_number <> glo:select1      |
            or aud:action     <> 'BASTION WARRANTY CHECK'      |
                then break.  ! end if
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                tmp:txtInfo = aud2:notes
                Break
            END ! IF
        
        end !loop
        access:audit.restorefile(save_aud_id)
        setcursor()
        
        if tmp:txtInfo <> ''
            do SplitString
            print(RPT:Detail)
        end
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFPRINT.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','BASTION VALIDATION PRINT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'BASTION VALIDATION PRINT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'BASTION VALIDATION PRINT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='BastionWarrantyCheckReport'
  END
  report{Prop:Preview} = PrintPreviewImage


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
SplitString routine
    ! Split audit trail notes field
    data
tmp:Info    long(1)
tmp:Count   long
tmp:Hold    long
tmp:Length  long
    code

    tmp:Length = len(clip(tmp:txtInfo))

    loop tmp:Count = 1 to tmp:Length
        if tmp:txtInfo[tmp:Count] = '<13>'
            case tmp:Info
                of 1
                    tmp:SupplierInfo = tmp:txtInfo[1:(tmp:Count - 1)]
                of 2
                    tmp:ActivityInfo = tmp:txtInfo[tmp:Hold :(tmp:Count - 1)]
                of 3
                    tmp:BlacklistInfo = tmp:txtInfo[tmp:Hold :(tmp:Count - 1)]
                of 4
                    tmp:UsageInfo = tmp:txtInfo[tmp:Hold :(tmp:Count - 1)]
                of 5
                    tmp:SwapoutInfo = tmp:txtInfo[tmp:Hold :(tmp:Count - 1)]
            end
            tmp:Hold  = tmp:Count + 2
            tmp:Info += 1
        end
    end

    tmp:WarrantyInfo = tmp:txtInfo[tmp:Hold : tmp:Length]





! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()





Status_Report_Criteria PROCEDURE                      !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::24:TAGDISPSTATUS   BYTE(0)
DASBRW::24:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::25:TAGDISPSTATUS   BYTE(0)
DASBRW::25:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::26:TAGDISPSTATUS   BYTE(0)
DASBRW::26:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::27:TAGDISPSTATUS   BYTE(0)
DASBRW::27:QUEUE          QUEUE
Pointer4                      LIKE(glo:Pointer4)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::28:TAGDISPSTATUS   BYTE(0)
DASBRW::28:QUEUE          QUEUE
Pointer5                      LIKE(glo:Pointer5)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::29:TAGDISPSTATUS   BYTE(0)
DASBRW::29:QUEUE          QUEUE
Pointer6                      LIKE(glo:Pointer6)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::30:TAGDISPSTATUS   BYTE(0)
DASBRW::30:QUEUE          QUEUE
Pointer7                      LIKE(glo:Pointer7)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::31:TAGDISPSTATUS   BYTE(0)
DASBRW::31:QUEUE          QUEUE
Pointer8                      LIKE(glo:Pointer8)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::32:TAGDISPSTATUS   BYTE(0)
DASBRW::32:QUEUE          QUEUE
Pointer9                      LIKE(glo:Pointer9)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::33:TAGDISPSTATUS   BYTE(0)
DASBRW::33:QUEUE          QUEUE
Pointer10                     LIKE(glo:Pointer10)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::35:TAGDISPSTATUS   BYTE(0)
DASBRW::35:QUEUE          QUEUE
Pointer11                     LIKE(glo:Pointer11)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::36:TAGDISPSTATUS   BYTE(0)
DASBRW::36:QUEUE          QUEUE
Pointer12                     LIKE(glo:Pointer12)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::37:TAGDISPSTATUS   BYTE(0)
DASBRW::37:QUEUE          QUEUE
Pointer13                     LIKE(glo:Pointer13)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::38:TAGDISPSTATUS   BYTE(0)
DASBRW::38:QUEUE          QUEUE
Pointer14                     LIKE(glo:Pointer14)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
local:FileNameJobs   STRING(255),STATIC
local:FileNameAudit  STRING(255),STATIC
local:FileNameParts  STRING(255),STATIC
local:FileNameWarParts STRING(255),STATIC
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_stac_id         USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_epr_id          USHORT,AUTO
tmp:Description      STRING(60)
Tmp:ShortReportFlag  BYTE
tmp:NewDescription   STRING(60)
tmp:Job_Number       STRING(20)
tmp:OutputType       BYTE(0)
tmp:PaperReportType  BYTE(0)
tmp:ExportJobs       BYTE(0)
tmp:ExportAudit      BYTE(0)
tmp:ExportParts      BYTE(0)
tmp:ExportWarparts   BYTE(0)
tmp:ExportPath       STRING(255)
tmp:HeadAccountTag   STRING(1)
tmp:SubAccountTag    STRING(1)
tmp:No               STRING('NO {1}')
tmp:CChargeTypeTag   STRING(1)
tmp:Yes              STRING('YES')
tmp:WChargeType      STRING(3)
tmp:CRepairTypeTag   STRING(1)
tmp:WRepairTypeTag   STRING(1)
tmp:StatusType       BYTE(0)
tmp:LocationTag      STRING(1)
tmp:UserTag          STRING(1)
tmp:ENGINEER         STRING('ENGINEER {22}')
tmp:Manufacturer     STRING(30)
tmp:SelectManufacturer BYTE(0)
tmp:ManufacturerTag  STRING(1)
tmp:ModelNumberTag   STRING(1)
tmp:UnitTypeTag      STRING(1)
tmp:TransitTypeTag   STRING(1)
tmp:TurnaroundTimeTag STRING(1)
tmp:StatusTag        STRING(1)
tmp:SavedCriteria    STRING(60)
tmp:Workshop         BYTE(0)
tmp:JobType          BYTE(0)
tmp:CompletedType    BYTE(0)
tmp:DespatchedType   BYTE(0)
tmp:JobBatchNumber   LONG
tmp:EDIBatchNumber   LONG
tmp:ReportOrder      STRING(30)
tmp:DateRangeType    BYTE(0)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:InvoiceType      BYTE(0)
tmp:ActualPartsCost  REAL
tmp:FirstPart        LONG
tmp:CustomerStatus   BYTE(1)
tmp:ARCChargeableCostPrice REAL
tmp:ARCWarrantyCostPrice REAL
tmp:RRCChargeableCostPrice REAL
tmp:RRCWarrantyCostPrice REAL
tmp:VAT              REAL
tmp:HandlingFeeType  STRING(30)
tmp:HandlingFeeLocation STRING(30)
tmp:ARCMarkup        REAL
tmp:ARCCharge        REAL
tmp:PartsCost        REAL
tmp:LabourCost       REAL
tmp:CourierCost      REAL
tmp:LabourVatRate    REAL
tmp:PartsVatRate     REAL
LOC:RRCLocation      STRING(30)
LOC:ARCLocation      STRING(30)
LOC:RRCDateRecived   STRING(10)
LOC:PUPflag          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:HeadAccountTag     LIKE(tmp:HeadAccountTag)       !List box control field - type derived from local data
tmp:HeadAccountTag_Icon LONG                          !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:SubAccountTag      LIKE(tmp:SubAccountTag)        !List box control field - type derived from local data
tmp:SubAccountTag_Icon LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:CChargeTypeTag     LIKE(tmp:CChargeTypeTag)       !List box control field - type derived from local data
tmp:CChargeTypeTag_Icon LONG                          !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
tmp:WChargeType        LIKE(tmp:WChargeType)          !List box control field - type derived from local data
tmp:WChargeType_Icon   LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Manufacturer)
                       PROJECT(rtd:RecordNumber)
                       PROJECT(rtd:Chargeable)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
tmp:CRepairTypeTag     LIKE(tmp:CRepairTypeTag)       !List box control field - type derived from local data
tmp:CRepairTypeTag_Icon LONG                          !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Manufacturer       LIKE(rtd:Manufacturer)         !List box control field - type derived from field
rtd:RecordNumber       LIKE(rtd:RecordNumber)         !Primary key field - type derived from field
rtd:Chargeable         LIKE(rtd:Chargeable)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Manufacturer)
                       PROJECT(rtd:RecordNumber)
                       PROJECT(rtd:Warranty)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:6
tmp:WRepairTypeTag     LIKE(tmp:WRepairTypeTag)       !List box control field - type derived from local data
tmp:WRepairTypeTag_Icon LONG                          !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Manufacturer       LIKE(rtd:Manufacturer)         !List box control field - type derived from field
rtd:RecordNumber       LIKE(rtd:RecordNumber)         !Primary key field - type derived from field
rtd:Warranty           LIKE(rtd:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                       PROJECT(sts:Exchange)
                       PROJECT(sts:Loan)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?List:7
tmp:StatusTag          LIKE(tmp:StatusTag)            !List box control field - type derived from local data
tmp:StatusTag_Icon     LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
sts:Exchange           LIKE(sts:Exchange)             !Browse key field - type derived from field
sts:Loan               LIKE(sts:Loan)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(LOCINTER)
                       PROJECT(loi:Location)
                     END
Queue:Browse:7       QUEUE                            !Queue declaration for browse/combo box using ?List:8
tmp:LocationTag        LIKE(tmp:LocationTag)          !List box control field - type derived from local data
tmp:LocationTag_Icon   LONG                           !Entry's icon ID
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                       PROJECT(use:User_Type)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?List:9
tmp:UserTag            LIKE(tmp:UserTag)              !List box control field - type derived from local data
tmp:UserTag_Icon       LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !Primary key field - type derived from field
use:User_Type          LIKE(use:User_Type)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW16::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:9       QUEUE                            !Queue declaration for browse/combo box using ?List:10
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW17::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?List:11
tmp:ModelNumberTag     LIKE(tmp:ModelNumberTag)       !List box control field - type derived from local data
tmp:ModelNumberTag_Icon LONG                          !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
Queue:Browse:11      QUEUE                            !Queue declaration for browse/combo box using ?List:12
tmp:UnitTypeTag        LIKE(tmp:UnitTypeTag)          !List box control field - type derived from local data
tmp:UnitTypeTag_Icon   LONG                           !Entry's icon ID
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW22::View:Browse   VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
Queue:Browse:12      QUEUE                            !Queue declaration for browse/combo box using ?List:13
tmp:TransitTypeTag     LIKE(tmp:TransitTypeTag)       !List box control field - type derived from local data
tmp:TransitTypeTag_Icon LONG                          !Entry's icon ID
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW23::View:Browse   VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
Queue:Browse:13      QUEUE                            !Queue declaration for browse/combo box using ?List:14
tmp:TurnaroundTimeTag  LIKE(tmp:TurnaroundTimeTag)    !List box control field - type derived from local data
tmp:TurnaroundTimeTag_Icon LONG                       !Entry's icon ID
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB18::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Status Report / Export Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,56,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('1'),USE(?Tab1)
                           PROMPT('Load Previous Criteria'),AT(210,101),USE(?Prompt46),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           BUTTON,AT(172,116),USE(?SelectCriteria),TRN,FLAT,LEFT,ICON('selcritp.jpg')
                           PROMPT('Select a previously saved criteria'),AT(210,114),USE(?Prompt47),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Criteria Used:'),AT(210,138),USE(?Prompt48),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(266,138),USE(tmp:SavedCriteria),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           LINE,AT(186,164,308,0),USE(?Line1),COLOR(COLOR:White),LINEWIDTH(1)
                           OPTION,AT(362,215,90,38),USE(Tmp:ShortReportFlag),HIDE
                             RADIO(' Full Version'),AT(374,223),USE(?Tmp:ShortReportFlag:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO(' Short Version'),AT(374,239),USE(?Tmp:ShortReportFlag:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('Status Report Type'),AT(210,170),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold+FONT:underline,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select how you want to output the results of this report:'),AT(210,186),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Report Output Type'),AT(214,207,248,48),USE(tmp:OutputType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper Report'),AT(226,226),USE(?tmp:OutputType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Export File'),AT(302,226),USE(?tmp:OutputType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('Note: A tag list should be completely unticked to disregard that element from th' &|
   'e criteria. E.g. To run the Status Report regardless of location, leave the inte' &|
   'rnal location tag list completely unticked.'),AT(214,271,256,24),USE(?Prompt66),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('2'),USE(?Tab2)
                           PROMPT('Paper Report Type'),AT(214,98),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select what type of report you want to print:'),AT(214,117,253,11),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Detailed'),AT(213,143),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Produces a report listing all the jobs that match the selected criteria.'),AT(213,159),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Produces a report listing how many jobs are in each status.'),AT(214,194,236,24),USE(?Prompt7:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Produces a report listing all jobs that match the selected criteria, including P' &|
   'revious Status information'),AT(214,234,236,24),USE(?Prompt7:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Paper Report Type'),AT(214,258,254,48),USE(tmp:PaperReportType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Detailed'),AT(222,277),USE(?tmp:PaperReportType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Summary'),AT(302,277),USE(?tmp:PaperReportType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Status Change'),AT(388,277),USE(?tmp:PaperReportType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           PROMPT('Summary'),AT(214,178),USE(?Prompt6:2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Status Change'),AT(214,221),USE(?Prompt6:3),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                         END
                         TAB('3'),USE(?Tab3)
                           PROMPT('Export File Details'),AT(211,98),USE(?Prompt12),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('For evey job that matches the report criteria, select which of that job''s files ' &|
   'you want exported.'),AT(211,114,200,20),USE(?Prompt13),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Path where you want to create the export files.'),AT(215,242,200,16),USE(?Prompt13:2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Export Path'),AT(219,277),USE(?tmp:ExportPath:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(295,274,160,10),USE(tmp:ExportPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Export Path'),TIP('Export Path'),UPR
                           BUTTON,AT(460,271),USE(?LookupExportPath),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Export File Path'),AT(215,226),USE(?Prompt12:2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('Export Jobs File  (JOBSDATA.CSV)'),AT(227,154),USE(tmp:ExportJobs),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Jobs'),TIP('Export Jobs'),VALUE('1','0')
                           CHECK('Export Audit File   (AUDITDATA.CSV)'),AT(227,170),USE(tmp:ExportAudit),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Audit File'),TIP('Export Audit File'),VALUE('1','0')
                           CHECK('Export Chargeable Parts File   (PARTSDATA.CSV)'),AT(227,186),USE(tmp:ExportParts),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Parts File'),TIP('Export Parts File'),VALUE('1','0')
                           CHECK('Export Warranty Parts File   (WARPARTSDATA.CSV)'),AT(227,202),USE(tmp:ExportWarparts),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Export Warranty Parts File'),TIP('Export Warranty Parts File'),VALUE('1','0')
                         END
                         TAB('4'),USE(?Tab4)
                           STRING('Header Account Number'),AT(295,98),USE(?String1),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Trade Header Accounts to Include'),AT(251,114),USE(?Prompt17),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('If you wish to include all Sub Accounts for a selected Head Account, select the ' &|
   'Head Account(s) you require below, but do NOT select any Sub Accounts from the n' &|
   'ext screen.'),AT(212,128,256,28),USE(?Prompt65),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(232,160,216,144),USE(?List),IMM,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(173,250,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(173,266,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(233,306),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,306),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(385,306),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('5'),USE(?Tab5)
                           PROMPT('Sub Account Number'),AT(301,98),USE(?Prompt18),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Sub Trade Acount To Include'),AT(261,112),USE(?Prompt19),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(232,130,216,160),USE(?List:2),IMM,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(77,258,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(77,282,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(232,294),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,294),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(384,294),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('18'),USE(?Tab18)
                           OPTION('Workshop Type'),AT(187,117,307,28),USE(tmp:Workshop),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('In Workshop Only'),AT(195,127),USE(?tmp:Workshop:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             RADIO('Not In Workshop Only'),AT(309,127),USE(?tmp:Workshop:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             RADIO('Both'),AT(437,130),USE(?tmp:Workshop:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                           END
                           OPTION('Job Type'),AT(187,151,307,48),USE(tmp:JobType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Warranty Only'),AT(193,162),USE(?tmp:JobType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             RADIO('Warranty (inc Split)'),AT(307,162),USE(?tmp:JobType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             RADIO('Split Only'),AT(435,162),USE(?tmp:JobType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('3')
                             RADIO('Chargeable Only'),AT(193,178),USE(?tmp:JobType:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('4')
                             RADIO('Chargeable (inc Split)'),AT(307,178),USE(?tmp:JobType:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('5')
                             RADIO('All'),AT(435,178),USE(?tmp:JobType:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                           END
                           OPTION('Completed Type'),AT(187,207,307,27),USE(tmp:CompletedType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Completed Only'),AT(193,221),USE(?tmp:CompletedType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             RADIO('Incomplete Only'),AT(307,221),USE(?tmp:CompletedType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             RADIO('Both'),AT(435,221),USE(?tmp:CompletedType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                           END
                           OPTION('Despatched Type'),AT(187,242,307,32),USE(tmp:DespatchedType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Despatched Only'),AT(195,253),USE(?tmp:DespatchedType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             RADIO('Not Despatched Only'),AT(309,253),USE(?tmp:DespatchedType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             RADIO('Both'),AT(437,253),USE(?tmp:DespatchedType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                           END
                           OPTION('Invoice Type'),AT(187,285,307,28),USE(tmp:InvoiceType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Invoiced Only'),AT(195,298),USE(?tmp:InvoiceType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             RADIO('Non-Invoiced Only'),AT(309,298),USE(?tmp:InvoiceType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             RADIO('Both'),AT(437,298),USE(?tmp:InvoiceType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                           END
                         END
                         TAB('6'),USE(?Tab6)
                           PROMPT('Chargeable Charge Types'),AT(292,98),USE(?Prompt20),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select the Chargeable Charge Types to include'),AT(253,112),USE(?Prompt21),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,130,148,160),USE(?List:3),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(81,255,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(77,279,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON,AT(244,294),USE(?DASTAG:3),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,294),USE(?DASTAGAll:3),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,294),USE(?DASUNTAGALL:3),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('7'),USE(?Tab7)
                           PROMPT('Warranty Charge Types'),AT(295,98),USE(?Prompt22),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Warranty Charge Types To Include'),AT(251,114),USE(?Prompt23),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,130,148,160),USE(?List:4),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s3@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Rev tags'),AT(85,258,50,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(81,279,70,13),USE(?DASSHOWTAG:4),HIDE
                           BUTTON,AT(244,294),USE(?DASTAG:4),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,294),USE(?DASTAGAll:4),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,294),USE(?DASUNTAGALL:4),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('8'),USE(?Tab8)
                           PROMPT('Chargeable Repair Types'),AT(299,98),USE(?Prompt24),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Chargeable Repair Types To Include'),AT(261,114),USE(?Prompt25),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(212,130,256,160),USE(?List:5),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Repair Type~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:4)
                           BUTTON('&Rev tags'),AT(73,258,50,13),USE(?DASREVTAG:5),HIDE
                           BUTTON('sho&W tags'),AT(77,279,70,13),USE(?DASSHOWTAG:5),HIDE
                           BUTTON,AT(240,292),USE(?DASTAG:5),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,292),USE(?DASTAGAll:5),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,292),USE(?DASUNTAGALL:5),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('9'),USE(?Tab9)
                           PROMPT('Warranty Repair Types'),AT(303,98),USE(?Prompt26),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Warranty Repair Types To Include'),AT(265,112),USE(?Prompt27),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(212,128,256,160),USE(?List:6),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Repair Type~@s30@120L(2)~Manufacturer~@s30@'),FROM(Queue:Browse:5)
                           BUTTON('&Rev tags'),AT(81,258,50,13),USE(?DASREVTAG:6),HIDE
                           BUTTON('sho&W tags'),AT(81,274,70,13),USE(?DASSHOWTAG:6),HIDE
                           BUTTON,AT(240,290),USE(?DASTAG:6),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,290),USE(?DASTAGAll:6),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,290),USE(?DASUNTAGALL:6),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('10'),USE(?Tab10)
                           PROMPT('Status Types'),AT(315,98),USE(?Prompt28),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Type and Status To Include.'),AT(263,111),USE(?Prompt29),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Note: This is the status which will appear on the paper report. If you select "C' &|
   'ustomer" Status, then the Job Status will be shown, unless the unit has an Excha' &|
   'nge attached. Then the Exchange Status will be shown.'),AT(164,120,352,32),USE(?Prompt30),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Status Type'),AT(264,148,156,24),USE(tmp:StatusType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Job'),AT(270,156),USE(?tmp:StatusType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Exchange'),AT(313,156),USE(?tmp:StatusType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Loan'),AT(377,156),USE(?tmp:StatusType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           CHECK('Show "Customer" Status On Report'),AT(265,174),USE(tmp:CustomerStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Show "Customer" Status On Report'),TIP('Show "Customer" Status On Report'),VALUE('1','0')
                           LIST,AT(266,188,148,144),USE(?List:7),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Status~@s30@'),FROM(Queue:Browse:6)
                           BUTTON('&Rev tags'),AT(81,266,50,13),USE(?DASREVTAG:7),HIDE
                           BUTTON('sho&W tags'),AT(77,287,70,13),USE(?DASSHOWTAG:7),HIDE
                           BUTTON,AT(420,223),USE(?DASTAG:7),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(420,256),USE(?DASTAGAll:7),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(420,290),USE(?DASUNTAGALL:7),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('11'),USE(?Tab11)
                           PROMPT('Internal Locations'),AT(305,98),USE(?Prompt31),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select Which Internal Locations To Include'),AT(260,114),USE(?Prompt32),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,130,148,160),USE(?List:8),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse:7)
                           BUTTON('&Rev tags'),AT(81,255,50,13),USE(?DASREVTAG:8),HIDE
                           BUTTON('sho&W tags'),AT(73,274,70,13),USE(?DASSHOWTAG:8),HIDE
                           BUTTON,AT(240,294),USE(?DASTAG:8),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,294),USE(?DASTAGAll:8),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,294),USE(?DASUNTAGALL:8),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('12'),USE(?Tab12)
                           PROMPT('Engineers'),AT(321,98),USE(?Prompt33),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select Which Engineers To Include'),AT(276,114),USE(?Prompt34),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(233,136,215,160),USE(?List:9),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@109L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@'),FROM(Queue:Browse:8)
                           BUTTON('&Rev tags'),AT(77,258,50,13),USE(?DASREVTAG:9),HIDE
                           BUTTON('sho&W tags'),AT(77,282,70,13),USE(?DASSHOWTAG:9),HIDE
                           BUTTON,AT(240,298),USE(?DASTAG:9),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,298),USE(?DASTAGAll:9),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,298),USE(?DASUNTAGALL:9),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('13'),USE(?Tab13)
                           PROMPT('Manufacturers'),AT(313,98),USE(?Prompt35),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Manufacturers To Include'),AT(267,112),USE(?Prompt36),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('If you wish to include all Model Numbers for a selected Manufacturer, select the' &|
   ' manufacturer(s) you require below, but do NOT select any Model Numbers from the' &|
   ' next screen.'),AT(96,128,488,16),USE(?Prompt64),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,150,148,160),USE(?List:10),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:9)
                           BUTTON('&Rev tags'),AT(85,266,50,13),USE(?DASREVTAG:10),HIDE
                           BUTTON('sho&W tags'),AT(85,282,70,13),USE(?DASSHOWTAG:10),HIDE
                           BUTTON,AT(240,312),USE(?DASTAG:10),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,312),USE(?DASTAGAll:10),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,312),USE(?DASUNTAGALL:10),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('14'),USE(?Tab14)
                           PROMPT('Model Numbers'),AT(311,98),USE(?Prompt37),FONT(,,080FFFFH,FONT:bold+FONT:underline,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Which Model Numbers To Include'),AT(265,114),USE(?Prompt38),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select Manufacturer'),AT(231,138),USE(tmp:SelectManufacturer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select Manurfacturer'),TIP('Select Manurfacturer'),VALUE('1','0')
                           COMBO(@s20),AT(325,138,124,10),USE(tmp:Manufacturer),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Manufacturer'),AT(325,130),USE(?Prompt39),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(266,154,150,148),USE(?List:11),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:10)
                           BUTTON('&Rev tags'),AT(82,263,50,13),USE(?DASREVTAG:11),HIDE
                           BUTTON('sho&W tags'),AT(78,279,70,13),USE(?DASSHOWTAG:11),HIDE
                           BUTTON,AT(240,306),USE(?DASTAG:11),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,306),USE(?DASTAGAll:11),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,306),USE(?DASUNTAGALL:11),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('15'),USE(?Tab15)
                           PROMPT('Unit Types'),AT(320,98),USE(?Prompt40),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Unit Types to include'),AT(277,114),USE(?Prompt41),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,130,148,160),USE(?List:12),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Unit Type~@s30@'),FROM(Queue:Browse:11)
                           BUTTON('&Rev tags'),AT(74,255,50,13),USE(?DASREVTAG:12),HIDE
                           BUTTON('sho&W tags'),AT(74,277,70,13),USE(?DASSHOWTAG:12),HIDE
                           BUTTON,AT(240,294),USE(?DASTAG:12),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,294),USE(?DASTAGAll:12),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,294),USE(?DASUNTAGALL:12),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('16'),USE(?Tab16)
                           PROMPT('Transit Types'),AT(315,98),USE(?Prompt42),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Transit Types to include'),AT(271,114),USE(?Prompt43),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,130,148,160),USE(?List:13),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Transit Type~@s30@'),FROM(Queue:Browse:12)
                           BUTTON('&Rev tags'),AT(74,255,50,13),USE(?DASREVTAG:13),HIDE
                           BUTTON('sho&W tags'),AT(78,287,70,13),USE(?DASSHOWTAG:13),HIDE
                           BUTTON,AT(240,294),USE(?DASTAG:13),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(312,294),USE(?DASTAGAll:13),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(380,294),USE(?DASUNTAGALL:13),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('17'),USE(?Tab17)
                           PROMPT('Turnaround Times'),AT(305,98),USE(?Prompt44),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which Turnaround Times to include'),AT(262,114),USE(?Prompt45),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(266,130,148,160),USE(?List:14),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)J@s1@120L(2)|M~Turnaround Time~@s30@'),FROM(Queue:Browse:13)
                           BUTTON('&Rev tags'),AT(82,255,50,13),USE(?DASREVTAG:14),HIDE
                           BUTTON('sho&W tags'),AT(78,271,70,13),USE(?DASSHOWTAG:14),HIDE
                           BUTTON,AT(240,294),USE(?DASTAG:14),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(309,294),USE(?DASTAGAll:14),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,294),USE(?DASUNTAGALL:14),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 19'),USE(?Tab19)
                           PROMPT('Batch Numbers'),AT(212,98),USE(?Prompt53),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which of the following batch numbers you wish to include'),AT(212,114),USE(?Prompt54),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Batch Number'),AT(220,128),USE(?tmp:JobBatchNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(296,128,64,10),USE(tmp:JobBatchNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Batch Number'),TIP('Job Batch Number'),UPR
                           PROMPT('EDI Batch Number'),AT(220,144),USE(?tmp:EDIBatchNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(296,144,64,10),USE(tmp:EDIBatchNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('EDI Batch Number'),TIP('EDI Batch Number'),UPR
                           PROMPT('Report/Export Order'),AT(212,166),USE(?Prompt57),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which order you wish your report to appear in. Note that Date Booked (if ' &|
   'using Date Booked date range) or Date Completed (if using Date Completed date ra' &|
   'nge) will produce the quickest report.'),AT(212,176,348,32),USE(?Prompt58),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Report Order'),AT(224,216),USE(?Prompt59),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(296,216,124,10),USE(tmp:ReportOrder),LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),DROP(10,124),FROM('DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER')
                           PROMPT('Date Range'),AT(212,238),USE(?Prompt60),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select the Date Range type and date range the report will use'),AT(212,250),USE(?Prompt61),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Date Range Type'),AT(214,262,206,31),USE(tmp:DateRangeType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Booking Date'),AT(236,275),USE(?tmp:DateRangeType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Completed Date'),AT(328,275),USE(?tmp:DateRangeType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('Start Date'),AT(224,298),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(296,298,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(364,294),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(224,318),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(296,318,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(364,314),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Status Report / Export Wizard'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(68,366),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(136,366),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(480,366),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
Wizard13         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:6
Q                      &Queue:Browse:5                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:7
Q                      &Queue:Browse:6                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW10::Sort1:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 0
BRW10::Sort2:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 1
BRW10::Sort3:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 2
BRW14                CLASS(BrowseClass)               !Browse using ?List:8
Q                      &Queue:Browse:7                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:9
Q                      &Queue:Browse:8                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
BRW16                CLASS(BrowseClass)               !Browse using ?List:10
Q                      &Queue:Browse:9                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW16::Sort0:Locator StepLocatorClass                 !Default Locator
BRW17                CLASS(BrowseClass)               !Browse using ?List:11
Q                      &Queue:Browse:10               !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW17::Sort0:Locator StepLocatorClass                 !Default Locator
BRW17::Sort1:Locator StepLocatorClass                 !Conditional Locator - tmp:SelectManufacturer = 0
BRW17::Sort2:Locator StepLocatorClass                 !Conditional Locator - tmp:SelectManufacturer = 1
BRW21                CLASS(BrowseClass)               !Browse using ?List:12
Q                      &Queue:Browse:11               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
BRW22                CLASS(BrowseClass)               !Browse using ?List:13
Q                      &Queue:Browse:12               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW22::Sort0:Locator StepLocatorClass                 !Default Locator
BRW23                CLASS(BrowseClass)               !Browse using ?List:14
Q                      &Queue:Browse:13               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                 !Default Locator
FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FileLookup34         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP


LocalGetDays       Procedure(String, String, Date) , long       !func:Sat,Byte func:Sun,Date func:Start)


    END !MAP
ExportFileJobs    File,Driver('ASCII'),Pre(expjobs),Name(local:FileNameJobs),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileAudit    File,Driver('ASCII'),Pre(expaudit),Name(local:FileNameAudit),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileParts    File,Driver('ASCII'),Pre(expparts),Name(local:FileNameParts),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileWarparts    File,Driver('ASCII'),Pre(expwarparts),Name(local:FileNameWarParts),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::24:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:HeadAccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:HeadAccountTag = ''
  END
    Queue:Browse.tmp:HeadAccountTag = tmp:HeadAccountTag
  IF (tmp:HeadAccounttag = '*')
    Queue:Browse.tmp:HeadAccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:HeadAccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::24:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::24:QUEUE = glo:Queue
    ADD(DASBRW::24:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::24:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::24:QUEUE,DASBRW::24:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASSHOWTAG Routine
   CASE DASBRW::24:TAGDISPSTATUS
   OF 0
      DASBRW::24:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::24:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::24:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::25:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = sub:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = sub:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:SubAccountTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:SubAccountTag = ''
  END
    Queue:Browse:1.tmp:SubAccountTag = tmp:SubAccountTag
  IF (tmp:SubAccounttag = '*')
    Queue:Browse:1.tmp:SubAccountTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:SubAccountTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = sub:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::25:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::25:QUEUE = glo:Queue2
    ADD(DASBRW::25:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::25:QUEUE.Pointer2 = sub:Account_Number
     GET(DASBRW::25:QUEUE,DASBRW::25:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = sub:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASSHOWTAG Routine
   CASE DASBRW::25:TAGDISPSTATUS
   OF 0
      DASBRW::25:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::25:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::25:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::26:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW6.UpdateBuffer
   glo:Queue3.Pointer3 = cha:Charge_Type
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = cha:Charge_Type
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:CChargeTypeTag = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:CChargeTypeTag = ''
  END
    Queue:Browse:2.tmp:CChargeTypeTag = tmp:CChargeTypeTag
  IF (tmp:CChargeTypetag = '*')
    Queue:Browse:2.tmp:CChargeTypeTag_Icon = 2
  ELSE
    Queue:Browse:2.tmp:CChargeTypeTag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = cha:Charge_Type
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::26:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::26:QUEUE = glo:Queue3
    ADD(DASBRW::26:QUEUE)
  END
  FREE(glo:Queue3)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::26:QUEUE.Pointer3 = cha:Charge_Type
     GET(DASBRW::26:QUEUE,DASBRW::26:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = cha:Charge_Type
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASSHOWTAG Routine
   CASE DASBRW::26:TAGDISPSTATUS
   OF 0
      DASBRW::26:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::26:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::26:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::27:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW7.UpdateBuffer
   glo:Queue4.Pointer4 = cha:Charge_Type
   GET(glo:Queue4,glo:Queue4.Pointer4)
  IF ERRORCODE()
     glo:Queue4.Pointer4 = cha:Charge_Type
     ADD(glo:Queue4,glo:Queue4.Pointer4)
    tmp:WChargeType = '*'
  ELSE
    DELETE(glo:Queue4)
    tmp:WChargeType = ''
  END
    Queue:Browse:3.tmp:WChargeType = tmp:WChargeType
  IF (tmp:WChargeType = '*')
    Queue:Browse:3.tmp:WChargeType_Icon = 2
  ELSE
    Queue:Browse:3.tmp:WChargeType_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(glo:Queue4)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue4.Pointer4 = cha:Charge_Type
     ADD(glo:Queue4,glo:Queue4.Pointer4)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue4)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::27:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue4)
    GET(glo:Queue4,QR#)
    DASBRW::27:QUEUE = glo:Queue4
    ADD(DASBRW::27:QUEUE)
  END
  FREE(glo:Queue4)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::27:QUEUE.Pointer4 = cha:Charge_Type
     GET(DASBRW::27:QUEUE,DASBRW::27:QUEUE.Pointer4)
    IF ERRORCODE()
       glo:Queue4.Pointer4 = cha:Charge_Type
       ADD(glo:Queue4,glo:Queue4.Pointer4)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASSHOWTAG Routine
   CASE DASBRW::27:TAGDISPSTATUS
   OF 0
      DASBRW::27:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::27:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::27:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::28:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW8.UpdateBuffer
   glo:Queue5.Pointer5 = rtd:Repair_Type
   GET(glo:Queue5,glo:Queue5.Pointer5)
  IF ERRORCODE()
     glo:Queue5.Pointer5 = rtd:Repair_Type
     ADD(glo:Queue5,glo:Queue5.Pointer5)
    tmp:CRepairTypeTag = '*'
  ELSE
    DELETE(glo:Queue5)
    tmp:CRepairTypeTag = ''
  END
    Queue:Browse:4.tmp:CRepairTypeTag = tmp:CRepairTypeTag
  IF (tmp:CRepairTypetag = '*')
    Queue:Browse:4.tmp:CRepairTypeTag_Icon = 2
  ELSE
    Queue:Browse:4.tmp:CRepairTypeTag_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue5)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue5.Pointer5 = rtd:Repair_Type
     ADD(glo:Queue5,glo:Queue5.Pointer5)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue5)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::28:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue5)
    GET(glo:Queue5,QR#)
    DASBRW::28:QUEUE = glo:Queue5
    ADD(DASBRW::28:QUEUE)
  END
  FREE(glo:Queue5)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::28:QUEUE.Pointer5 = rtd:Repair_Type
     GET(DASBRW::28:QUEUE,DASBRW::28:QUEUE.Pointer5)
    IF ERRORCODE()
       glo:Queue5.Pointer5 = rtd:Repair_Type
       ADD(glo:Queue5,glo:Queue5.Pointer5)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASSHOWTAG Routine
   CASE DASBRW::28:TAGDISPSTATUS
   OF 0
      DASBRW::28:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::28:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::28:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:5{PROP:Text} = 'Show All'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:5{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::29:DASTAGONOFF Routine
  GET(Queue:Browse:5,CHOICE(?List:6))
  BRW9.UpdateBuffer
   glo:Queue6.Pointer6 = rtd:Repair_Type
   GET(glo:Queue6,glo:Queue6.Pointer6)
  IF ERRORCODE()
     glo:Queue6.Pointer6 = rtd:Repair_Type
     ADD(glo:Queue6,glo:Queue6.Pointer6)
    tmp:WRepairTypeTag = '*'
  ELSE
    DELETE(glo:Queue6)
    tmp:WRepairTypeTag = ''
  END
    Queue:Browse:5.tmp:WRepairTypeTag = tmp:WRepairTypeTag
  IF (tmp:WRepairTypetag = '*')
    Queue:Browse:5.tmp:WRepairTypeTag_Icon = 2
  ELSE
    Queue:Browse:5.tmp:WRepairTypeTag_Icon = 1
  END
  PUT(Queue:Browse:5)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue6)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue6.Pointer6 = rtd:Repair_Type
     ADD(glo:Queue6,glo:Queue6.Pointer6)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASUNTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue6)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASREVTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::29:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue6)
    GET(glo:Queue6,QR#)
    DASBRW::29:QUEUE = glo:Queue6
    ADD(DASBRW::29:QUEUE)
  END
  FREE(glo:Queue6)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::29:QUEUE.Pointer6 = rtd:Repair_Type
     GET(DASBRW::29:QUEUE,DASBRW::29:QUEUE.Pointer6)
    IF ERRORCODE()
       glo:Queue6.Pointer6 = rtd:Repair_Type
       ADD(glo:Queue6,glo:Queue6.Pointer6)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASSHOWTAG Routine
   CASE DASBRW::29:TAGDISPSTATUS
   OF 0
      DASBRW::29:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::29:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::29:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:6{PROP:Text} = 'Show All'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:6{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:6,CHOICE(?List:6))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::30:DASTAGONOFF Routine
  GET(Queue:Browse:6,CHOICE(?List:7))
  BRW10.UpdateBuffer
   glo:Queue7.Pointer7 = sts:Status
   GET(glo:Queue7,glo:Queue7.Pointer7)
  IF ERRORCODE()
     glo:Queue7.Pointer7 = sts:Status
     ADD(glo:Queue7,glo:Queue7.Pointer7)
    tmp:StatusTag = '*'
  ELSE
    DELETE(glo:Queue7)
    tmp:StatusTag = ''
  END
    Queue:Browse:6.tmp:StatusTag = tmp:StatusTag
  IF (tmp:Statustag = '*')
    Queue:Browse:6.tmp:StatusTag_Icon = 2
  ELSE
    Queue:Browse:6.tmp:StatusTag_Icon = 1
  END
  PUT(Queue:Browse:6)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue7)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue7.Pointer7 = sts:Status
     ADD(glo:Queue7,glo:Queue7.Pointer7)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASUNTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue7)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASREVTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::30:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue7)
    GET(glo:Queue7,QR#)
    DASBRW::30:QUEUE = glo:Queue7
    ADD(DASBRW::30:QUEUE)
  END
  FREE(glo:Queue7)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::30:QUEUE.Pointer7 = sts:Status
     GET(DASBRW::30:QUEUE,DASBRW::30:QUEUE.Pointer7)
    IF ERRORCODE()
       glo:Queue7.Pointer7 = sts:Status
       ADD(glo:Queue7,glo:Queue7.Pointer7)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASSHOWTAG Routine
   CASE DASBRW::30:TAGDISPSTATUS
   OF 0
      DASBRW::30:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::30:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::30:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:7{PROP:Text} = 'Show All'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:7{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List:7,CHOICE(?List:7))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::31:DASTAGONOFF Routine
  GET(Queue:Browse:7,CHOICE(?List:8))
  BRW14.UpdateBuffer
   glo:Queue8.Pointer8 = loi:Location
   GET(glo:Queue8,glo:Queue8.Pointer8)
  IF ERRORCODE()
     glo:Queue8.Pointer8 = loi:Location
     ADD(glo:Queue8,glo:Queue8.Pointer8)
    tmp:LocationTag = '*'
  ELSE
    DELETE(glo:Queue8)
    tmp:LocationTag = ''
  END
    Queue:Browse:7.tmp:LocationTag = tmp:LocationTag
  IF (tmp:Locationtag = '*')
    Queue:Browse:7.tmp:LocationTag_Icon = 2
  ELSE
    Queue:Browse:7.tmp:LocationTag_Icon = 1
  END
  PUT(Queue:Browse:7)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue8)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue8.Pointer8 = loi:Location
     ADD(glo:Queue8,glo:Queue8.Pointer8)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASUNTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue8)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASREVTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::31:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue8)
    GET(glo:Queue8,QR#)
    DASBRW::31:QUEUE = glo:Queue8
    ADD(DASBRW::31:QUEUE)
  END
  FREE(glo:Queue8)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::31:QUEUE.Pointer8 = loi:Location
     GET(DASBRW::31:QUEUE,DASBRW::31:QUEUE.Pointer8)
    IF ERRORCODE()
       glo:Queue8.Pointer8 = loi:Location
       ADD(glo:Queue8,glo:Queue8.Pointer8)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASSHOWTAG Routine
   CASE DASBRW::31:TAGDISPSTATUS
   OF 0
      DASBRW::31:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::31:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::31:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:8{PROP:Text} = 'Show All'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:8{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:8,CHOICE(?List:8))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::32:DASTAGONOFF Routine
  GET(Queue:Browse:8,CHOICE(?List:9))
  BRW15.UpdateBuffer
   glo:Queue9.Pointer9 = use:User_Code
   GET(glo:Queue9,glo:Queue9.Pointer9)
  IF ERRORCODE()
     glo:Queue9.Pointer9 = use:User_Code
     ADD(glo:Queue9,glo:Queue9.Pointer9)
    tmp:UserTag = '*'
  ELSE
    DELETE(glo:Queue9)
    tmp:UserTag = ''
  END
    Queue:Browse:8.tmp:UserTag = tmp:UserTag
  IF (tmp:Usertag = '*')
    Queue:Browse:8.tmp:UserTag_Icon = 2
  ELSE
    Queue:Browse:8.tmp:UserTag_Icon = 1
  END
  PUT(Queue:Browse:8)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASTAGALL Routine
  ?List:9{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW15.Reset
  FREE(glo:Queue9)
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue9.Pointer9 = use:User_Code
     ADD(glo:Queue9,glo:Queue9.Pointer9)
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASUNTAGALL Routine
  ?List:9{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue9)
  BRW15.Reset
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASREVTAGALL Routine
  ?List:9{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::32:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue9)
    GET(glo:Queue9,QR#)
    DASBRW::32:QUEUE = glo:Queue9
    ADD(DASBRW::32:QUEUE)
  END
  FREE(glo:Queue9)
  BRW15.Reset
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::32:QUEUE.Pointer9 = use:User_Code
     GET(DASBRW::32:QUEUE,DASBRW::32:QUEUE.Pointer9)
    IF ERRORCODE()
       glo:Queue9.Pointer9 = use:User_Code
       ADD(glo:Queue9,glo:Queue9.Pointer9)
    END
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASSHOWTAG Routine
   CASE DASBRW::32:TAGDISPSTATUS
   OF 0
      DASBRW::32:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:9{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:9{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:9{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::32:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:9{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:9{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:9{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::32:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:9{PROP:Text} = 'Show All'
      ?DASSHOWTAG:9{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:9{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:9{PROP:Text})
   BRW15.ResetSort(1)
   SELECT(?List:9,CHOICE(?List:9))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::33:DASTAGONOFF Routine
  GET(Queue:Browse:9,CHOICE(?List:10))
  BRW16.UpdateBuffer
   glo:Queue10.Pointer10 = man:Manufacturer
   GET(glo:Queue10,glo:Queue10.Pointer10)
  IF ERRORCODE()
     glo:Queue10.Pointer10 = man:Manufacturer
     ADD(glo:Queue10,glo:Queue10.Pointer10)
    tmp:ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue10)
    tmp:ManufacturerTag = ''
  END
    Queue:Browse:9.tmp:ManufacturerTag = tmp:ManufacturerTag
  IF (tmp:Manufacturertag = '*')
    Queue:Browse:9.tmp:ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse:9.tmp:ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse:9)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASTAGALL Routine
  ?List:10{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW16.Reset
  FREE(glo:Queue10)
  LOOP
    NEXT(BRW16::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue10.Pointer10 = man:Manufacturer
     ADD(glo:Queue10,glo:Queue10.Pointer10)
  END
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASUNTAGALL Routine
  ?List:10{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue10)
  BRW16.Reset
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASREVTAGALL Routine
  ?List:10{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::33:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue10)
    GET(glo:Queue10,QR#)
    DASBRW::33:QUEUE = glo:Queue10
    ADD(DASBRW::33:QUEUE)
  END
  FREE(glo:Queue10)
  BRW16.Reset
  LOOP
    NEXT(BRW16::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::33:QUEUE.Pointer10 = man:Manufacturer
     GET(DASBRW::33:QUEUE,DASBRW::33:QUEUE.Pointer10)
    IF ERRORCODE()
       glo:Queue10.Pointer10 = man:Manufacturer
       ADD(glo:Queue10,glo:Queue10.Pointer10)
    END
  END
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASSHOWTAG Routine
   CASE DASBRW::33:TAGDISPSTATUS
   OF 0
      DASBRW::33:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:10{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:10{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:10{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::33:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:10{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:10{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:10{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::33:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:10{PROP:Text} = 'Show All'
      ?DASSHOWTAG:10{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:10{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:10{PROP:Text})
   BRW16.ResetSort(1)
   SELECT(?List:10,CHOICE(?List:10))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::35:DASTAGONOFF Routine
  GET(Queue:Browse:10,CHOICE(?List:11))
  BRW17.UpdateBuffer
   glo:Queue11.Pointer11 = mod:Model_Number
   GET(glo:Queue11,glo:Queue11.Pointer11)
  IF ERRORCODE()
     glo:Queue11.Pointer11 = mod:Model_Number
     ADD(glo:Queue11,glo:Queue11.Pointer11)
    tmp:ModelNumberTag = '*'
  ELSE
    DELETE(glo:Queue11)
    tmp:ModelNumberTag = ''
  END
    Queue:Browse:10.tmp:ModelNumberTag = tmp:ModelNumberTag
  IF (tmp:ModelNumbertag = '*')
    Queue:Browse:10.tmp:ModelNumberTag_Icon = 2
  ELSE
    Queue:Browse:10.tmp:ModelNumberTag_Icon = 1
  END
  PUT(Queue:Browse:10)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASTAGALL Routine
  ?List:11{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW17.Reset
  FREE(glo:Queue11)
  LOOP
    NEXT(BRW17::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue11.Pointer11 = mod:Model_Number
     ADD(glo:Queue11,glo:Queue11.Pointer11)
  END
  SETCURSOR
  BRW17.ResetSort(1)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASUNTAGALL Routine
  ?List:11{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue11)
  BRW17.Reset
  SETCURSOR
  BRW17.ResetSort(1)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASREVTAGALL Routine
  ?List:11{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::35:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue11)
    GET(glo:Queue11,QR#)
    DASBRW::35:QUEUE = glo:Queue11
    ADD(DASBRW::35:QUEUE)
  END
  FREE(glo:Queue11)
  BRW17.Reset
  LOOP
    NEXT(BRW17::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::35:QUEUE.Pointer11 = mod:Model_Number
     GET(DASBRW::35:QUEUE,DASBRW::35:QUEUE.Pointer11)
    IF ERRORCODE()
       glo:Queue11.Pointer11 = mod:Model_Number
       ADD(glo:Queue11,glo:Queue11.Pointer11)
    END
  END
  SETCURSOR
  BRW17.ResetSort(1)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASSHOWTAG Routine
   CASE DASBRW::35:TAGDISPSTATUS
   OF 0
      DASBRW::35:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:11{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:11{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:11{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::35:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:11{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:11{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:11{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::35:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:11{PROP:Text} = 'Show All'
      ?DASSHOWTAG:11{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:11{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:11{PROP:Text})
   BRW17.ResetSort(1)
   SELECT(?List:11,CHOICE(?List:11))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::36:DASTAGONOFF Routine
  GET(Queue:Browse:11,CHOICE(?List:12))
  BRW21.UpdateBuffer
   glo:Queue12.Pointer12 = uni:Unit_Type
   GET(glo:Queue12,glo:Queue12.Pointer12)
  IF ERRORCODE()
     glo:Queue12.Pointer12 = uni:Unit_Type
     ADD(glo:Queue12,glo:Queue12.Pointer12)
    tmp:UnitTypeTag = '*'
  ELSE
    DELETE(glo:Queue12)
    tmp:UnitTypeTag = ''
  END
    Queue:Browse:11.tmp:UnitTypeTag = tmp:UnitTypeTag
  IF (tmp:UnitTypetag = '*')
    Queue:Browse:11.tmp:UnitTypeTag_Icon = 2
  ELSE
    Queue:Browse:11.tmp:UnitTypeTag_Icon = 1
  END
  PUT(Queue:Browse:11)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASTAGALL Routine
  ?List:12{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW21.Reset
  FREE(glo:Queue12)
  LOOP
    NEXT(BRW21::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue12.Pointer12 = uni:Unit_Type
     ADD(glo:Queue12,glo:Queue12.Pointer12)
  END
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASUNTAGALL Routine
  ?List:12{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue12)
  BRW21.Reset
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASREVTAGALL Routine
  ?List:12{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::36:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue12)
    GET(glo:Queue12,QR#)
    DASBRW::36:QUEUE = glo:Queue12
    ADD(DASBRW::36:QUEUE)
  END
  FREE(glo:Queue12)
  BRW21.Reset
  LOOP
    NEXT(BRW21::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::36:QUEUE.Pointer12 = uni:Unit_Type
     GET(DASBRW::36:QUEUE,DASBRW::36:QUEUE.Pointer12)
    IF ERRORCODE()
       glo:Queue12.Pointer12 = uni:Unit_Type
       ADD(glo:Queue12,glo:Queue12.Pointer12)
    END
  END
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASSHOWTAG Routine
   CASE DASBRW::36:TAGDISPSTATUS
   OF 0
      DASBRW::36:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:12{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:12{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:12{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::36:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:12{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:12{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:12{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::36:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:12{PROP:Text} = 'Show All'
      ?DASSHOWTAG:12{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:12{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:12{PROP:Text})
   BRW21.ResetSort(1)
   SELECT(?List:12,CHOICE(?List:12))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::37:DASTAGONOFF Routine
  GET(Queue:Browse:12,CHOICE(?List:13))
  BRW22.UpdateBuffer
   glo:Queue13.Pointer13 = trt:Transit_Type
   GET(glo:Queue13,glo:Queue13.Pointer13)
  IF ERRORCODE()
     glo:Queue13.Pointer13 = trt:Transit_Type
     ADD(glo:Queue13,glo:Queue13.Pointer13)
    tmp:TransitTypeTag = '*'
  ELSE
    DELETE(glo:Queue13)
    tmp:TransitTypeTag = ''
  END
    Queue:Browse:12.tmp:TransitTypeTag = tmp:TransitTypeTag
  IF (tmp:TransitTypetag = '*')
    Queue:Browse:12.tmp:TransitTypeTag_Icon = 2
  ELSE
    Queue:Browse:12.tmp:TransitTypeTag_Icon = 1
  END
  PUT(Queue:Browse:12)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASTAGALL Routine
  ?List:13{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW22.Reset
  FREE(glo:Queue13)
  LOOP
    NEXT(BRW22::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue13.Pointer13 = trt:Transit_Type
     ADD(glo:Queue13,glo:Queue13.Pointer13)
  END
  SETCURSOR
  BRW22.ResetSort(1)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASUNTAGALL Routine
  ?List:13{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue13)
  BRW22.Reset
  SETCURSOR
  BRW22.ResetSort(1)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASREVTAGALL Routine
  ?List:13{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::37:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue13)
    GET(glo:Queue13,QR#)
    DASBRW::37:QUEUE = glo:Queue13
    ADD(DASBRW::37:QUEUE)
  END
  FREE(glo:Queue13)
  BRW22.Reset
  LOOP
    NEXT(BRW22::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::37:QUEUE.Pointer13 = trt:Transit_Type
     GET(DASBRW::37:QUEUE,DASBRW::37:QUEUE.Pointer13)
    IF ERRORCODE()
       glo:Queue13.Pointer13 = trt:Transit_Type
       ADD(glo:Queue13,glo:Queue13.Pointer13)
    END
  END
  SETCURSOR
  BRW22.ResetSort(1)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASSHOWTAG Routine
   CASE DASBRW::37:TAGDISPSTATUS
   OF 0
      DASBRW::37:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:13{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:13{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:13{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::37:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:13{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:13{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:13{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::37:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:13{PROP:Text} = 'Show All'
      ?DASSHOWTAG:13{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:13{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:13{PROP:Text})
   BRW22.ResetSort(1)
   SELECT(?List:13,CHOICE(?List:13))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::38:DASTAGONOFF Routine
  GET(Queue:Browse:13,CHOICE(?List:14))
  BRW23.UpdateBuffer
   glo:Queue14.Pointer14 = tur:Turnaround_Time
   GET(glo:Queue14,glo:Queue14.Pointer14)
  IF ERRORCODE()
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     ADD(glo:Queue14,glo:Queue14.Pointer14)
    tmp:TurnaroundTimeTag = '*'
  ELSE
    DELETE(glo:Queue14)
    tmp:TurnaroundTimeTag = ''
  END
    Queue:Browse:13.tmp:TurnaroundTimeTag = tmp:TurnaroundTimeTag
  IF (tmp:TurnaroundTimetag = '*')
    Queue:Browse:13.tmp:TurnaroundTimeTag_Icon = 2
  ELSE
    Queue:Browse:13.tmp:TurnaroundTimeTag_Icon = 1
  END
  PUT(Queue:Browse:13)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASTAGALL Routine
  ?List:14{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW23.Reset
  FREE(glo:Queue14)
  LOOP
    NEXT(BRW23::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     ADD(glo:Queue14,glo:Queue14.Pointer14)
  END
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASUNTAGALL Routine
  ?List:14{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue14)
  BRW23.Reset
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASREVTAGALL Routine
  ?List:14{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::38:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue14)
    GET(glo:Queue14,QR#)
    DASBRW::38:QUEUE = glo:Queue14
    ADD(DASBRW::38:QUEUE)
  END
  FREE(glo:Queue14)
  BRW23.Reset
  LOOP
    NEXT(BRW23::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::38:QUEUE.Pointer14 = tur:Turnaround_Time
     GET(DASBRW::38:QUEUE,DASBRW::38:QUEUE.Pointer14)
    IF ERRORCODE()
       glo:Queue14.Pointer14 = tur:Turnaround_Time
       ADD(glo:Queue14,glo:Queue14.Pointer14)
    END
  END
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASSHOWTAG Routine
   CASE DASBRW::38:TAGDISPSTATUS
   OF 0
      DASBRW::38:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:14{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:14{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:14{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::38:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:14{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:14{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:14{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::38:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:14{PROP:Text} = 'Show All'
      ?DASSHOWTAG:14{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:14{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:14{PROP:Text})
   BRW23.ResetSort(1)
   SELECT(?List:14,CHOICE(?List:14))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Export      Routine
    !if this is for a summary report just go through all the checks
    !and give a total at the end, just don't export anything.
    If tmp:OutputType = 1 And tmp:PaperReportType = 1
        JobsError# = 1
        AuditError# = 1
        PartsError# = 1
        WarPartsError# = 1

    Else !If tmp:OutputType = 1 And tmp:PaperReportType = 1

        local:FileNameJobs   = Clip(tmp:ExportPath) & '\JOBSDATA.CSV'
        local:FIlenameAudit  = Clip(tmp:ExportPath) & '\AUDITDATA.CSV'
        local:FileNameParts  = Clip(tmp:ExportPath) & '\PARTSDATA.CSV'
        local:FileNameWarParts = Clip(tmp:ExportPath) & '\WARPARTSDATA.CSV'

        Remove(local:FileNameJobs)
        Remove(local:FileNameAudit)
        Remove(Local:FileNameParts)
        Remove(Local:FileNameWarParts)

        JobsError# = 0
        If tmp:ExportJobs
            Open(ExportFileJobs)
            If Error()
                Create(ExportFileJobs)
                Open(ExportFileJobs)
                If Error()
                    JobsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportJobs
            JobsError# = 1
        End !If tmp:ExportJobs

        AuditError# = 0
        If tmp:ExportAudit
            Open(ExportFileAudit)
            If Error()
                Create(ExportFileAudit)
                Open(ExportFileAudit)
                IF Error()
                    AuditError# = 1
                End !IF Error()
            End !If Error()
        Else !If tmp:ExportAudit
            AuditError# = 1
        End !If tmp:ExportAudit

        PartsError# = 0
        If tmp:ExportParts
            Open(ExportFileParts)
            If Error()
                Create(ExportFileParts)
                Open(ExportFileParts)
                If Error()
                    PartsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportParts
            PartsError# = 1
        End !If tmp:ExportParts

        WarPartsError# = 0
        If tmp:ExportWarparts
            Open(ExportFileWarparts)
            If Error()
                Create(ExportFileWarParts)
                Open(ExportFileWarparts)
                If Error()
                    WarPartsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportWarparts
            WarPartsError# = 1
        End !If tmp:ExportWarparts
    End !If tmp:OutputType = 1 And tmp:PaperReportType = 1


    !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column C
    !some defaults want looking up
    LOC:RRCLocation         = GETINI('RRC','RRCLocation','AT FRANCHISE'   , '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
    LOC:ARCLocation         = GETINI('RRC','ARCLocation','RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
    !End TB1326

    If JobsError# = 0
        if Tmp:ShortReportFlag then
            do JobsFileTitleSimple
        Else
            Do NewFileTitle
        END
    End !If JobsError# = 0

    If AuditError# = 0
        Do AuditFileTitle
    End !If AuditError# = 0

    If PartsError# = 0
        Do PartsFileTitle
    End !If PartsError# = 0

    If WarPartsError# = 0
        Do WarPartsFileTitle
    End !If PartsError# = 0
    Count_Main# = 0
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    thiswindow.reset(1)
    open(progresswindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'

    recordstoprocess    = Records(JOBS)

    Save_job_ID = Access:JOBS.SaveFile()
    RecordCount# = 0
    FileCount# = 1

    Case tmp:ReportOrder
        Of 'JOB NUMBER'
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            set(job:ref_number_key,0)
        Of 'I.M.E.I. NUMBER'
            Access:JOBS.Clearkey(job:ESN_Key)
            set(job:esn_key,0)
        Of 'ACCOUNT NUMBER'
            Access:JOBS.Clearkey(job:AccountNumberKey)
            set(job:accountnumberkey,0)
        Of 'MODEL NUMBER'
            Access:JOBS.Clearkey(job:Model_Number_Key)
            set(job:model_number_key,0)
        Of 'STATUS'
            Case tmp:StatusType
                Of 0 !Job Status
                    Access:JOBS.Clearkey(job:By_Status)
                    set(job:by_status,0)
                Of 1 !Exchange Status
                    Access:JOBS.Clearkey(job:ExcStatusKey)
                    Set(job:ExcStatusKey)
                Of 2 !Loan Status
                    Access:JOBS.Clearkey(job:LoanStatusKey)
                    Set(job:LoanStatusKey)
            End !Case tmp:StatusType
        Of 'MSN'
            Access:JOBS.Clearkey(job:MSN_Key)
            set(job:msn_key,0)
        Of 'SURNAME'
            Access:JOBS.Clearkey(job:Surname_Key)
            set(job:surname_key,0)
        Of 'ENGINEER'
            Access:JOBS.Clearkey(job:Engineer_Key)
            set(job:engineer_key,0)
        Of 'MOBILE NUMBER'
            Access:JOBS.Clearkey(job:MobileNumberKey)
            set(job:mobilenumberkey,0)
        Of 'DATE BOOKED'
            Access:JOBS.Clearkey(job:Date_Booked_Key)
            Case tmp:DateRangeType
                Of 0 !Booking
                    job:date_booked = tmp:StartDate
                    set(job:date_booked_key,job:date_booked_key)
                Of 1 !Completed
                    Set(job:Date_Booked_Key,0)
            End !tmp:DateRangeType
        Of 'DATE COMPLETED'
            Access:JOBS.ClearKey(job:DateCompletedKey)
            Case tmp:DateRangeType
                Of 0 !Booking
                    Set(job:DateCompletedKey,0)
                Of 1 !Completed
                    job:date_completed = tmp:StartDate
                    set(job:datecompletedkey,job:datecompletedkey)
            End !Case tmp:DateRangeType
    End !Case tmp:ReportOrder

    Loop
        If Access:JOBS.NEXT()
           Break
        End !If

        Do GetNextRecord2
        If tmp:cancel = 1
            Break
        End!If tmp:cancel = 1

        Case tmp:ReportOrder
            Of 'DATE BOOKED'
                If tmp:DateRangeType = 0
                    If job:Date_Booked > tmp:EndDate
                        Break
                    End !If job:Date_Booked > tmp:EndDate
                End !If tmp:DateRangeType = 0
            Of 'DATE COMPLETED'
                If tmp:DateRangeType = 1
                    If job:Date_Completed > tmp:EndDate
                        Break
                    End !If job:Date_Completed > tmp:EndDate
                End !If tmp:DateRangeType = 1
        End !Case tmp:ReportOrder

        If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:JobBatchNumber,tmp:EDIBatchNumber)
            Cycle
        End !If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType)

        If JobsError# = 0
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            if Tmp:ShortReportFlag then
                do exportJobsSimple
            ELSE
                Do NewFileExport
                RecordCount# += 1
                If RecordCount# = 64000
                    Close(ExportFileJobs)
                    Rename(local:FileNameJobs,Clip(tmp:ExportPath) & '\JOBSDATA' & '(' & FileCount# & ').CSV')
                    FileCount# += 1
                    local:FileNameJobs = Clip(tmp:ExportPath) & '\JOBSDATA' & '(' & FileCount# & ').CSV'
                    Remove(local:FileNameJobs)
                    Create(ExportFileJobs)
                    Open(ExportFileJobs)
                    Do NewFileTitle
                    RecordCount# = 0
                End !If RecordCount# > 64000
            END
        End !If JobsError# = 0

        If AuditError# = 0

            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:TypeRefKey)
            aud:Ref_Number = job:Ref_Number
            Set(aud:TypeRefKey,aud:TypeRefKey)
            Loop
                If Access:AUDIT.NEXT()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number|
                    Then Break.  ! End If
                Do ExportAudit
            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)
        End !If AuditError# = 0

        If PartsError# = 0
            tmp:FirstPart = 0
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                Do ExportParts
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If PartsError# = 0

        If WarpartsError# = 0
            tmp:FirstPart = 0
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number |
                    Then Break.  ! End If
                Do ExportWarparts
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If WarpartsError# = 0

        Count_Main# += 1
        ?progress:userstring{prop:Text} = 'Records Found: ' & Count_Main#

    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

    Do EndPrintRun
    close(progresswindow)
    Case Missive('Routine Completed.'&|
      '<13,10>'&|
      '<13,10>' & Count_Main# & ' record(s) matched the criteria.','ServiceBase 3g',|
                   'midea.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive

    Close(ExportFileJobs)
    Close(ExportFileAudit)
    Close(ExportFileParts)
    Close(ExportFileWarParts)







NewFileTitle       Routine          !JOBS EXPORT TITLE LINE
        Clear(expjobs:Record)
        expjobs:Line1   = 'STATUS REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
        Add(ExportFileJobs)

        Clear(expjobs:Record)
        expjobs:Line1   = 'Job No'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Franchise Branch Code')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Franchise Job Number')
        !TB13200 - J - 26/02/14 - new column for booking agent
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Booking Agent')
        !End TB13200
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Unit Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Manufacturer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Model Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Telephone')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Customer Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Customer Telephone')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Order Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Authority Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Booked')

        !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column O (after the column Date Booked)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'Date Received From PUP'
        !end TB13236

        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Job Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Exch Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Loan Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Days in Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Total Days')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Fault Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Of Purchase')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Repair Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Repair Excludes EDI')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exclude From EDI')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Claim Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Rejection Verified By')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('EDI Batch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collected By')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Unit Exchanged')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Third Party Site')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Third Party Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3rd Party Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3rd Party V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3rd Party Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Estimate Ignore Default Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Estimate Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Estimate Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Estimate Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Estimate V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Estimate Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Ignore Default Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Lost Loan Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Chargeable Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Warranty Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Claim Exchange Fee')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Claim Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Claim Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Claim Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Claim V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Claim Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment Confirm')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment Exchange')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Adjustment Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Handling Fee Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Handling Fee Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Handling Fee')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Exchange Fee')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Estimate Ignore Default Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Estimate Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Estimate Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Estimate Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Estimate V.A.T.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Estimate Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Ignore Default Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Lost Loan Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Vat')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Chargeable Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Spares Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Spares Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Labour')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Vat')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC Warranty Total Value')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC Charge')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC MarkUp')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Main Out Fault Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 4')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 5')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 6')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 7')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 8')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 9')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 10')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 11')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 12')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 13')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 14')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 15')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 16')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 17')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 18')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 19')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 20')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineers Notes')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Part Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Quantity')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Price')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Cost Selling')
        Add(ExportFileJobs)
NewFileExport      Routine      !EXPORT JOBS

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber  = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign

    !TB13200 - J - 26/02/14 - new column for booking agent from jobse2
    Access:jobse2.clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = Job:Ref_number
    if access:jobse2.fetch(jobe2:RefNumberKey)
        !error
    END
    !End TB13200

    Clear(expjobs:Record)
    !Job Number
    expjobs:Line1   = StripComma(job:Ref_Number)
    !Franchise Branch Code
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tra:BranchIdentification)
    !Franchise Job Number
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:JobNumber)

    !TB13200 - J - 26/02/14 - new column for booking agent
    if Job:Who_Booked = 'WEB' then
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe2:SIDBookingName)
    ELSE
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = job:Who_Booked
        IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
        ELSE
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        END
    END !tb13200 web
    !End TB13200

    !Unit Type
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Unit_Type)
    !Manufacturer
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Manufacturer)
    !Model Number
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Model_Number)
    !Account Number
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Account_Number)
    !Account Name
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name)
    !Account Telephone
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Number)
    !Customer Name
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname))
    !Customer Telephone
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:EndUserTelNo)
    !Order Number
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Order_Number)
    !Authority Number
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Authority_Number)
    !Date Booked
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Booked,@d06b))

    !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column O
    LOC:RRCDateRecived = ''
    LOC:PUPflag = 0     !use to show it has been at the pup
    
    Access:LocatLog.ClearKey(lot:DateKey)
    lot:RefNumber = job:Ref_number
    lot:TheDate = 0
    SET(lot:DateKey,lot:DateKey)
    LOOP
        if Access:LocatLog.NEXT() then break.
        IF lot:RefNumber <> job:Ref_Number then break.

        IF instring('PUP',lot:PreviousLocation,1,1) then LOC:PUPflag = 1.    !it has been at the PUP
        IF lot:NewLocation <> LOC:RRCLocation then cycle.

        if lot:NewLocation = LOC:ARCLocation then break.  !has been sent to ARC, there will be a return date that we do not want

        if LOC:PUPflag = 1 then LOC:RRCDateRecived = Format(lot:TheDate,@d06).
        BREAK

    END !LOOP

    expjobs:Line1   = Clip(expjobs:Line1) & ',' & clip(LOC:RRCDateRecived)

    !end TB13236

    !Current Status
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Current_Status)
    ! Inserting (DBH 27/11/2006) # 8524 - Show all job statuses
    !Exchange Status
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Status)
    !Loan Status
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Status)
    ! End (DBH 27/11/2006) #8524
    !Location
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Location)
    !Days In Status
    statusdays" =  LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Current_Status_Date)    !was today() - wob:Current_Status_Date
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StatusDays")
    !Total Days
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,job:Date_Booked))    !was Today() - job:Date_Booked)
    !Incoming I.M.E.I. Number
    !Incoming M.S.N.

    !Check the JOBTHIRD to see if there is an entry for this job.
    !If there is then that means it's been sent to third party,
    !therefore get the original IMEI /MSN incase it has changed.
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefNumberKey)
    If Access:JOBTHIRD.Next() = Level:Benign
        If jot:RefNumber = job:Ref_Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalIMEI)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalMSN)
        Else !If jot:RefNumber = job:Ref_Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
        End !If jot:RefNumber = job:Ref_Number
    Else !If Access:JOBTHIRD.Next() = Level:Benign
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
    End !If Access:JOBTHIRD.Next() = Level:Benign
    !Incoming Fault Description
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Fault_Description))
    !Date Of Purchase
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:DOP,@d06b))

    !Repair Location
    !TB13236 - J - 18/02/14 - If exchange issued at RRC then this is RRC otherwise as before
    If job:Exchange_Unit_Number <> ''  and  jobe:ExchangedATRRC then
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
    ELSE !ELSE of TB13236
        If SentToHub(job:Ref_Number)
            If job:Third_Party_Site <> ''
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('3RD')
            Else !If job:Third_Party_Site <> ''
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC')
            End !If job:Third_Party_Site <> ''
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
        End !If SentToHub(job:Ref_Number)
    END  !END of  TB13236


    !Warranty Job
    If job:Warranty_Job = 'YES'
        !Warranty Job
        expjobs:Line1   = Clip(expjobs:Line1) & ',YES'
        !Warranty Charge Type
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Warranty_Charge_Type)
        !Warranty Repair Type
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type_Warranty)
        !Warranty Invoice Number
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number_Warranty)
        !Warranty Invoice Date
        If job:Invoice_Number_Warranty <> 0
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number  = job:Invoice_Number_Warranty
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(inv:Date_Created,@d06b))
            Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        Else !If job:Warranty_Invoice_Number <> 0
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        End !If job:Warranty_Invoice_Number <> 0
        !Repair Excludes EDI
        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
        rtd:Manufacturer = job:Manufacturer
        rtd:Warranty     = 'YES'
        rtd:Repair_Type  = job:Repair_Type_Warranty
        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
            If rtd:ExcludeFromEDI
                expjobs:Line1   = Clip(expjobs:Line1) & ',YES'
            Else !If rtd:ExcludeFromEDI
                expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
            End !If rtd:ExcludeFromEDI
        Else !If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey)
            expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
        End !If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey)
        !!Exclude From EDI
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty    = 'YES'
        cha:Charge_Type = job:Warranty_Charge_Type
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(cha:Exclude_EDI)
        Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !H/O Claim Status
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:WarrantyClaimStatus)
        !Rejection Verified By
        Found# = 0
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeActionKey)
        aud:Ref_Number = job:ref_Number
        aud:Type       = 'JOB'
        aud:Action     = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Ref_Number <> job:Ref_Number      |
            Or aud:Type       <> 'JOB'      |
            Or aud:Action     <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'      |
                Then Break.  ! End If
            Access:USERS.Clearkey(use:user_code_Key)
            use:User_Code   = aud:User
            If Access:USERS.Tryfetch(use:user_code_Key) = Level:Benign
                !Found
                Found# = 1
                Break
            Else ! If Access:USERS.Tryfetch(use:user_code_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:user_code_Key) = Level:Benign
        End !Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)
        If Found#
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
        Else !If Found#
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If Found#
        !EDI Batch Number
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:EDI_Batch_Number)
    Else !If job:Warranty_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
        Loop x# = 1 To 9
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 10
    End !If job:Warranty_Job = 'YES'
    !Chargeable Job
    If job:Chargeable_Job = 'YES'
        !Chargeable Job
        expjobs:Line1   = Clip(expjobs:Line1) & ',YES'
        !Chargeable Charge Type
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Charge_Type)
        !Chargeable Repair Type
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type)
        !Chargeable Invoice Number
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number)
        !Chargeable Invoice Date
        If job:Invoice_Number <> 0
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number  = job:Invoice_Number
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(inv:Date_Created,@d06b))
            Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        Else !If job:Warranty_Invoice_Number <> 0
            expjobs:Line1   = Clip(expjobs:Line1) & ','
        End !If job:Warranty_Invoice_Number <> 0
        !Collected By
        Found# = 0
        Save_jpt_ID = Access:JOBPAYMT.SaveFile()
        Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
        jpt:Ref_Number =  job:Ref_Number
        Set(jpt:All_Date_Key,jpt:All_Date_Key)
        Loop
            If Access:JOBPAYMT.NEXT()
               Break
            End !If
            If jpt:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            Found# = 1
        End !Loop
        Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
        !The last record, should be the last user
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code   = jpt:User_Code
        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign And Found#
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Error
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    Else !If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
        Loop x# = 1 To 5
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 5
    End !If job:Chargeable_Job = 'YES'
    !Unit Exchanged
    If job:Exchange_Unit_Number <> 0
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            !Exchange Location
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:Location)
            !Exchange I.M.E.I. Number
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:ESN)
            !Exchange M.S.N.
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:MSN)
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    Else !If job:Exchange_Unit_Number <> 0
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Exchange_Unit_Number <> 0
    !Third Party Site
    If job:Third_Party_Site <> ''
        !Third Party Site
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Third_Party_Site)
        !Third Party Invoice Number
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:ARC3rdPartyInvoiceNumber)
        !Third Party Charge
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:ARC3rdPartyCost)
        !Third Party V.A.T.
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:ARC3rdPartyVAT)
        !Third Party Total
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Round(jobe:ARC3rdPartyCost,.01) + Round(jobe:ARC3rdPartyVAT,.01))
    Else !If job:Third_Party_Site <> ''
        Loop x# =1 To 5
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# =1 To 5
    End !If job:Third_Party_Site <> ''

    tmp:ARCChargeableCostPrice = 0
    tmp:RRCChargeableCostPrice = 0

    Save_epr_ID = Access:ESTPARTS.SaveFile()
    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = job:Ref_Number
    Set(epr:Part_Number_Key,epr:Part_Number_Key)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCChargeableCostPrice += epr:AveragePurchaseCost
        tmp:RRCChargeableCostPrice += epr:RRCAveragePurchaseCost
    End !Loop

    Access:ESTPARTS.RestoreFile(Save_epr_ID)

    If job:Estimate = 'YES'
        If SentToHub(job:Ref_Number)
            !ARC Estimate Ignore Default Charge
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Ignore_Estimate_Charges)
            !ARC Estimate Spares Cost
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCChargeableCostPrice,@n_14.2))
            !ARC Estimate Spares Selling
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Parts_Cost_Estimate,@n_14.2))
            !ARC Estimate Labour
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Labour_Cost_Estimate,@n_14.2))
            !ARC V.A.T.
            tmp:Vat = (job:Labour_Cost_Estimate * VatRate(job:Account_Number,'L')/100) + |
                                (job:Parts_Cost_Estimate * VatRate(job:Account_Number,'P')/100) + |
                                (job:Courier_Cost_Estimate *VatRate(job:Account_Number,'L')/100)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
            !ARC Estimate Total Value
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Labour_Cost_Estimate + |
                                                                            job:Parts_Cost_Estimate + |
                                                                            job:Courier_Cost_Estimate +|
                                                                            tmp:VAT,@n_14.2))
        Else !If SentToHub(job:Ref_Number
            Loop x# = 1 To 6
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 6
        End !If SentToHub(job:Ref_Number
    Else !If job:Estimate = 'YES'
        Loop x# = 1 To 6
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 6
    End !If job:Estimate = 'YES'

    tmp:ARCChargeableCostPrice = 0
    tmp:RRCChargeableCostPrice = 0

    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCChargeableCostPrice += par:AveragePurchaseCost
        tmp:RRCChargeableCostPrice += par:RRCAveragePurchaseCost
    End !Loop

    Access:PARTS.RestoreFile(Save_par_ID)

    If job:Chargeable_Job = 'YES'
        !Don't show chargeable cots for an unaccepted/rejected estimate
        If job:Estimate = 'YES' And (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
            Loop x# = 1 To 7
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 7
        Else !If job:Estimate = 'YES' And (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
            !Show invoiced values, if the job has been
            tmp:CourierCost    = job:Courier_Cost
            tmp:PartsCost      = job:Parts_Cost
            tmp:LabourCost     = job:Labour_Cost
            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If job:Invoice_Number <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = job:Invoice_Number
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    tmp:CourierCost    = job:Invoice_Courier_Cost
                    tmp:PartsCost      = job:Invoice_Parts_Cost
                    tmp:LabourCost     = job:Invoice_Labour_Cost
                    tmp:LabourVatRate   = inv:Vat_Rate_Labour
                    tmp:PartsVatRate    = inv:Vat_Rate_Parts
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0
            If SentToHub(job:Ref_Number)
                !ARC Chargeable Ignore Default Charge
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Ignore_Chargeable_Charges)
                !ARC Chargeable Lost Loan Charge
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:CourierCost)
                !ARC Chargeable Spares Cost
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCChargeableCostPrice,@n_14.2))
                !ARC Chargeable Spares Selling
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n_14.2))
                !ARC Chargeable Labour
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n_14.2))
                tmp:Vat = (tmp:LabourCost * tmp:LabourVatRate/100) + |
                                    (tmp:partsCost * tmp:PartsVatRate/100) + |
                                    (tmp:CourierCost * tmp:LabourVatRate/100)
                !ARC Chargeable V.A.T.
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
                !ARC Chargeable Total Value
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost + |
                                                                tmp:LabourCost + |
                                                                tmp:CourierCost + |
                                                                tmp:VAT,@n_14.2))
            Else !If SentToHub(job:Ref_Number)
                Loop x# = 1 To 7
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
                End !Loop x# = 1 To 7
            End !If SentToHub(job:Ref_Number)
        End !If job:Estimate = 'YES' And (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
    Else !If job:Chargeable_Job = 'YES'
        Loop x# = 1 To 7
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 7
    End !If job:Chargeable_Job = 'YES'

    tmp:ARCWarrantyCostPrice = 0
    tmp:RRCWarrantyCostPrice = 0

    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCWarrantyCostPrice += wpr:AveragePurchaseCost
        tmp:RRCWarrantyCostPrice += wpr:RRCAveragePurchaseCost
    End !Loop

    Access:WARPARTS.RestoreFile(Save_wpr_ID)
    !ARC Warranty Spares Cost
    If job:Warranty_Job = 'YES'
        If SentToHub(job:Ref_Number)
            !Show invoiced values, if the job has been
            tmp:PartsCost      = job:Parts_Cost_Warranty
            tmp:LabourCost     = job:Labour_Cost_Warranty
            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If job:Invoice_Number_Warranty <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = job:Invoice_Number_Warranty
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    tmp:PartsCost      = job:WInvoice_Parts_Cost
                    tmp:LabourCost     = job:WInvoice_Labour_Cost
                    tmp:LabourVatRate   = inv:Vat_Rate_Labour
                    tmp:PartsVatRate    = inv:Vat_Rate_Parts
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0

            !ARC Warranty Spares Cost
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCWarrantyCostPrice,@n_14.2))
            !ARC Warranty Spares Selling
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n_14.2))
            !ARC Warranty Labour
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n_14.2))
            !ARC Warranty V.A.T.
            tmp:VAT = tmp:PartsCost * tmp:PartsVATRate/100 + |
                                                        tmp:LabourCost * tmp:LabourVatRate/100
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
            !ARC Warranty Total Value
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost + |
                                                                            tmp:LabourCost + |
                                                                             tmp:VAT,@n_14.2))
        Else !If SentToHub(job:Ref_Number)
            Loop x# = 1 To 5
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 6
        End !If SentToHub(job:Ref_Number)
        !Show invoiced values, if the job has been
        tmp:CourierCost     = job:Courier_Cost_Warranty
        tmp:PartsCost       = jobe:ClaimPartsCost
        tmp:LabourCost      = jobe:ClaimValue
        tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
        tmp:PartsVatRate    = VatRate(job:Account_Number,'P')


        If job:Invoice_Number_Warranty <> 0
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number  = job:Invoice_Number_Warranty
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                tmp:CourierCost     = job:WInvoice_Courier_Cost
                tmp:PartsCost       = jobe:InvClaimPartsCost
                tmp:LabourCost      = jobe:InvoiceClaimValue
                tmp:LabourVatRate   = inv:Vat_Rate_Labour
                tmp:PartsVatRate    = inv:Vat_Rate_Parts
            Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
            End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        End !If job:Invoice_Number <> 0


        !Warranty Claim Exchange Fee
        If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:CourierCost))
        Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeATRRC
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeATRRC
        !Warranty Claim Spares Cost
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCWarrantyCostPrice,@n_14.2))
        !Warranty Claim Selling Cost
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n_14.2))
        !Warranty Claim Labour
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n_14.2))
        !Warranty Claim V.A.T.
        tmp:VAT = tmp:PartsCost * tmp:PartsVatRate/100 + |
                                                    tmp:LabourCost * tmp:LabourVatRate/100 + |
                                                    tmp:CourierCost * tmp:LabourVatRate/100
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
        !Warranty Claim Total Value
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:PartsCost + |
                                                                tmp:LabourCost + |
                                                                tmp:CourierCost + |
                                                                tmp:VAT)
        !Warranty Adjustment Confirm
        If jobe:ConfirmClaimAdjustment
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
            !Warranty ADjustment Exchange
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:ExchangeAdjustment,@n_14.2))
            !Warranty Claim Spares Cost
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:PartsAdjustment,@n_14.2))
            !Warranty Claim Spares Selling
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCWarrantyCostPrice,@n_14.2))
            !Warranty Claim Labour
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:LabourAdjustment,@n_14.2))
            !Warranty Claim V.A.T.
            tmp:VAT = jobe:PartsADjustment * VatRate(job:Account_Number,'P')/100 +|
                        jobe:LabourAdjustment * VatRate(job:Account_Number,'L')/100 +|
                        jobe:ExchangeAdjustment * VatRate(job:Account_Number,'L')/100
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
            !Warranty Claim Total Value
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:SubTotalAdjustment + tmp:VAT,@n_14.2))

        Else !If jobe:ConfirmClaimAdjustment
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
            Loop x# = 1 To 6
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 6
        End !If jobe:ConfirmClaimAdjustment
    Else !If job:Warranty_Job = 'YES'
        Loop x# = 1 To 18
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 6
    End !If job:Warranty_Job = 'YES'

    tmp:HandlingFeeType = 'REPAIR'
    If SentToHub(job:Ref_Number)
        tmp:HandlingFeeLocation = 'ARC'
    Else !If SentToHub(job:Ref_Number)
        tmp:HandlingFeeLocation = 'RRC'
    End !If SentToHub(job:Ref_Number)

    If job:Exchange_Unit_Number <> ''
        tmp:HandlingFeeType = 'REPAIR'
        If jobe:ExchangedAtRRC
            tmp:HandlingFeeLocation = 'RRC'
        Else !If jobe:ExchangedAtRRC
            tmp:HandlingFeeLocation = 'ARC'
        End !If jobe:ExchangedAtRRC
    End !If job:Exchange_Unit_Number <> ''

    if jobe:OBFvalidateDate <> ''
        tmp:HandlingFeeType = 'OBF'
    End !if jobe:OBFvalidateDate <> ''
    !RRC Handling Fee Type
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:HandlingFeeType)
    !RRC Handling Fee Location
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:HandlingFeeLocation)
    !RRC Handling Fee
    If SentToHub(job:Ref_Number) and jobe:WebJob
        If job:Invoice_Number <> 0
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:InvoiceHandlingFee,@n_14.2))
        Else !If job:Invoice_Number <> 0
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:HandlingFee,@n_14.2))
        End !If job:Invoice_Number <> 0

        !Exchange Fee
        If job:Invoice_Number <> 0
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:InvoiceExchangeRate,@n_14.2))
        Else !If job:Invoice_Number <> 0
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:ExchangeRate,@n_14.2))
        End !If job:Invoice_Number <> 0

    Else !If SentToHub(job:Ref_Number) and jobe:WebJob
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If SentToHub(job:Ref_Number) and jobe:WebJob


    tmp:ARCChargeableCostPrice = 0
    tmp:RRCChargeableCostPrice = 0

    Save_epr_ID = Access:ESTPARTS.SaveFile()
    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = job:Ref_Number
    Set(epr:Part_Number_Key,epr:Part_Number_Key)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCChargeableCostPrice += epr:AveragePurchaseCost
        tmp:RRCChargeableCostPrice += epr:RRCAveragePurchaseCost
    End !Loop

    Access:ESTPARTS.RestoreFile(Save_epr_ID)

    !RRC Estimate Ignore Default Charge
    If job:Estimate = 'YES' and jobe:WebJob
        If jobe:IgnoreRRCEstCosts
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
        Else !If jobe:IgnoreRRCEstCosts
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
        End !If jobe:IgnoreRRCEstCosts
        !RRC Estimate Spares Cost
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:RRCChargeableCostPrice,@n_14.2))
        !RRC Estimate Spares Selling
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:RRCEPartsCost,@n_14.2))
        !RRC Estimate Labour
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:RRCELabourCost,@n_14.2))
        !RRC Estimate V.A.T.
        tmp:Vat = (jobe:RRCELabourCost * VatRate(job:Account_Number,'L')/100) + |
                            (jobe:RRCEPartsCost * VatRate(job:Account_Number,'P')/100)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
        !RRC Estimate Total Value
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:RRCELabourCost + |
                                                                            jobe:RRCEPartsCost +|
                                                                            tmp:VAT,@n_14.2))
    Else !If job:Estimate = 'YES' and jobe:WebJob
        Loop x# = 1 To 6
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 6
    End !If job:Estimate = 'YES' and jobe:WebJob

    tmp:ARCChargeableCostPrice = 0
    tmp:RRCChargeableCostPrice = 0

    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCChargeableCostPrice += par:AveragePurchaseCost
        tmp:RRCChargeableCostPrice += par:RRCAveragePurchaseCost
    End !Loop

    Access:PARTS.RestoreFile(Save_par_ID)

    !RRC Chargeable Costs
    If job:Chargeable_Job = 'YES'
        !Don't show chargeable cots for an unaccepted/rejected estimate
        If job:Estimate = 'YES' And (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
            Loop x# = 1 To 7
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 7
        Else !If job:Estimate = 'YES' And (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
            If jobe:WebJob
                !Show invoiced values, if the job has been
                tmp:CourierCost    = job:Courier_Cost
                tmp:PartsCost      = jobe:RRCCPartsCost
                tmp:LabourCost     = jobe:RRCCLabourCost
                tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
                tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

                If job:Invoice_Number <> 0
                    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                    inv:Invoice_Number  = job:Invoice_Number
                    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                        !Found
                        If inv:RRCInvoiceDate <> ''
                            tmp:CourierCost    = job:Invoice_Courier_Cost
                            tmp:PartsCost      = jobe:InvRRCCPartsCost
                            tmp:LabourCost     = jobe:InvRRCCLabourCost
                            tmp:LabourVatRate   = inv:Vat_Rate_Labour
                            tmp:PartsVatRate    = inv:Vat_Rate_Parts
                        End !If inv:RRCInvoiceDate <> ''
                    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                        !Error
                    End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                End !If job:Invoice_Number <> 0

                !RRC Chargeable Ignore Default Charge
                If jobe:IgnoreRRCChaCosts
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
                Else !If jobe:IgnoreRRCChaCosts
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
                End !If jobe:IgnoreRRCChaCosts
                !RRC Lost Loan Charge
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:CourierCost,@n_14.2))
                !RRC Chargeable Spares Cost
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:RRCChargeableCostPrice,@n_14.2))
                !RRC Chargeable Spares Selling
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n_14.2))
                !RRC Chargeable Labour
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n_14.2))
                !RRC Chargeable VAT
                tmp:Vat = (tmp:LabourCost * tmp:LabourVatRate/100) + |
                                    (tmp:PartsCost * tmp:PartsVatRate/100) + |
                                    (tmp:CourierCost * tmp:LabourVatRate/100)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
                !RRC Chargeable Total Value
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost + |
                                                                        tmp:PartsCost + |
                                                                        tmp:CourierCost + |
                                                                        tmp:VAT,@n_14.2))
            Else !If ~SentToHub(job:Ref_Number)
                Loop x# = 1 To 7
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
                End !Loop x# = 1 To 7
            End !If ~SentToHub(job:Ref_Number)
        End !If job:Estimate = 'YES' And (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
    Else !If job:Chargeable_Job = 'YES'
        Loop x# = 1 To 7
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop x# = 1 To 7
    End !If job:Chargeable_Job = 'YES'

    tmp:ARCWarrantyCostPrice = 0
    tmp:RRCWarrantyCostPrice = 0

    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ARCWarrantyCostPrice += wpr:AveragePurchaseCost
        tmp:RRCWarrantyCostPrice += wpr:RRCAveragePurchaseCost
    End !Loop

    !RRC Warranty Costs
    If job:Warranty_Job = 'YES'
        If ~SentToHub(job:Ref_Number)
            !Show invoiced values, if the job has been
            tmp:PartsCost      = jobe:RRCWPartsCost
            tmp:LabourCost     = jobe:RRCWLabourCost
            tmp:LabourVatRate   = VatRate(job:Account_Number,'L')
            tmp:PartsVatRate    = VatRate(job:Account_Number,'P')

            If wob:RRCWInvoiceNumber <> 0
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = wob:RRCWInvoiceNumber
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Found
                    If inv:RRCInvoiceDate <> ''
                        tmp:PartsCost      = jobe:InvRRCWPartsCost
                        tmp:LabourCost     = jobe:InvRRCWLabourCost
                        tmp:LabourVatRate   = inv:Vat_Rate_Labour
                        tmp:PartsVatRate    = inv:Vat_Rate_Parts
                    End !If inv:RRCInvoiceDate <> ''
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                    !Error
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            End !If job:Invoice_Number <> 0

            !RRC Warranty Spares Cost
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:RRCWarrantyCostPrice,@n_14.2))
            !RRC Warranty Parts Selling
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:PartsCost,@n_14.2))
            !RRC Warranty Labour
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost,@n_14.2))
            !RRC Warranty VAT
            tmp:Vat = (tmp:LabourCost * tmp:LabourVatRate/100) + |
                                (tmp:PartsCost * tmp:PartsVatRate/100)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:VAT,@n_14.2))
            !RRC Warranty Total Value
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:LabourCost + |
                                                                    tmp:PartsCost + |
                                                                    tmp:VAT,@n_14.2))
        Else !If ~SentToHub(job:Ref_Number)
            Loop x# = 1 To 5
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 5
        End !If ~SentToHub(job:Ref_Number)
    Else !If job:Warranty_Job = 'YES'
            Loop x# = 1 To 5
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            End !Loop x# = 1 To 5
    End !If job:Warranty_Job = 'YES'

    !ARC Charge and ARC Markup
    If job:Chargeable_Job = 'YES'
        If jobe:WebJob And SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format((job:Courier_Cost+job:Parts_Cost+job:Labour_Cost) + (jobe:ARC3rdPartyCost),@n_14.2))
            tmp:ARCMarkup = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost
            if tmp:ARCMarkup <> 0
                tmp:ARCMarkup = tmp:ARCMarkup - tmp:ARCCharge
                if tmp:ARCMarkup < 0
                    tmp:ARCMarkup = 0
                end
            end
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(tmp:ARCMarkup,@n_14.2))
        Else !If jobe:WebJob And SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !If jobe:WebJob And SentToHub(job:Ref_Number)

    Else !If job:Chargeable_Job = 'YES'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If job:Chargeable_Job = 'YES'
    !Engineer
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
    Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    !Main Out Fault Description
    !Lets hope that the main out fault is written in the fault code field.
    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:MainFault    = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        Case maf:Field_Number
            Of 1
                mfo:Field        = job:Fault_Code1
            Of 2
                mfo:Field        = job:Fault_Code2
            Of 3
                mfo:Field        = job:Fault_Code3
            Of 4
                mfo:Field        = job:Fault_Code4
            Of 5
                mfo:Field        = job:Fault_Code5
            Of 6
                mfo:Field        = job:Fault_Code6
            Of 7
                mfo:Field        = job:Fault_Code7
            Of 8
                mfo:Field        = job:Fault_Code8
            Of 9
                mfo:Field        = job:Fault_Code9
            Of 10
                mfo:Field        = job:Fault_Code10
            Of 11
                mfo:Field        = job:Fault_Code11
            Of 12
                mfo:Field        = job:Fault_Code12
        End !Case maf:Field_Number
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Found
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(mfo:Description)
        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    !Fault Code 1
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code1)
    !Fault Code 2
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code2)
    !Fault Code 3
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code3)
    !Fault Code 4
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code4)
    !Fault Code 5
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code5)
    !Fault Code 6
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code6)
    !Fault Code 7
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code7)
    !Fault Code 8
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code8)
    !Fault Code 9
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code9)
    !Fault Code 10
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code10)
    !Fault Code 11
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code11)
    !Fault Code 12
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code12)
    ! Inserting (DBH 09/11/2006) # 8457 - Add extra fault codes
    !Fault Code 13
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode13)
    !Fault Code 14
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode14)
    !Fault Code 15
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode15)
    !Fault Code 16
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode16)
    !Fault Code 17
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode17)
    !Fault Code 18
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode18)
    !Fault Code 19
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode19)
    !Fault Code 20
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wob:FaultCode20)
    ! End (DBH 09/11/2006) #8457
    !Engineers Notes
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Engineers_Notes))
    !Invoice Text
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Invoice_Text))
    !Repair Type
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
    !Outgoing I.M.E.I. Number
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
    !Out going M.S.N.
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
    !Job Completed
    !TB13249 - if job:completed is blank show 'NO' - J - 06/03/14
    if clip(job:completed) = '' then job:completed = 'NO'.  !will not be saved
     expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Completed)
    !Date Completed
    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Completed,@d06b))
    !Parts
    Count# = 0
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If

        Count# += 1

        If Count# > 5
            Break
        End !If Count# > 5

        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(par:Part_Number)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(par:Description)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(par:Quantity)

        If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:AveragePurchaseCost,@n_14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:Sale_Cost,@n_14.2))
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCAveragePurchaseCost,@n_14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCSaleCost,@n_14.2))
        End !If SentToHub(job:Ref_Number)

    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)

    If Count# < 5
        Loop
            Count# += 1
            If Count# > 5
                Break
            End !If Count# > 5
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop
    End !If Count# < 5

    Count# = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If

        Count# += 1

        If Count# > 5
            Break
        End !If Count# > 5

        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wpr:Part_Number)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wpr:Description)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(wpr:Quantity)

        If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:AveragePurchaseCost,@n_14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:Purchase_Cost,@n_14.2))
        Else !If SentToHub(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCAveragePurchaseCost,@n_14.2))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(par:RRCPurchaseCost,@n_14.2))
        End !If SentToHub(job:Ref_Number)

    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    If Count# < 5
        Loop
            Count# += 1
            If Count# > 5
                Break
            End !If Count# > 5
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('')
        End !Loop
    End !If Count# < 5
    expjobs:line1   = StripQuotes(expjobs:Line1)
    Add(ExportFileJobs)
JobsFileTitleSimple       Routine       !SHORT EXPORT TITLE LINE
        Clear(expjobs:Record)
        expjobs:Line1   = 'STATUS REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
        Add(ExportFileJobs)

        Clear(expjobs:Record)

        expjobs:Line1   = 'Job No'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Booked')
        !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column C
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'Date Received From PUP'
        !end TB13236
        !TB13200 - J - 26/02/14 - new column for booking agent
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Booking Agent')
        !End TB13200
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Telephone')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Customer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Phone')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Model Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Repair Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job/Exchange')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Days in Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Total Days')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Eng')

        Add(ExportFileJobs)
ExportJobsSimple      Routine !SHORT EXPORT FILE

            Clear(expjobs:Record)
            tmp:Job_Number = job:Ref_Number

            access:webjob.clearkey(wob:RefNumberKey)
            wob:refnumber = job:Ref_Number
            if not access:webjob.fetch(wob:RefNumberKey)
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = wob:HeadAccountNumber
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Job_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
            end
            access:jobse.clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:ref_number
            if access:jobse.fetch(jobe:RefnumberKey)
                !error
            END

            !TB13200 - J - 26/02/14 - new column for booking agent from jobse2
            Access:jobse2.clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = Job:Ref_number
            if access:jobse2.fetch(jobe2:RefNumberKey)
                !error
            END
            !End TB13200

            expjobs:Line1   = StripComma(tmp:Job_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Booked,@d06))

            !TB13236 - J - 18/02/14 - add new field  Date Received From PUP into column C
            LOC:RRCDateRecived = ''
            LOC:PUPflag = 0
            
            Access:LocatLog.ClearKey(lot:DateKey)
            lot:RefNumber = job:Ref_Number
            lot:TheDate = 0
            SET(lot:DateKey,lot:DateKey)
            LOOP
                if Access:LocatLog.NEXT() then break.
                IF lot:RefNumber <> job:Ref_Number then break.

                IF instring('PUP',lot:PreviousLocation,1,1) then LOC:PUPflag = 1.    !it has been at the PUP
                IF lot:NewLocation <> LOC:RRCLocation then cycle.

                if lot:NewLocation = LOC:ARCLocation then break.  !has been sent to ARC, there will be a return date that we do not want

                if LOC:PUPflag = 1 then LOC:RRCDateRecived = Format(lot:TheDate,@d06).
                BREAK

            END !LOOP

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & clip(LOC:RRCDateRecived)

            !end TB13236

            !TB13200 - J - 26/02/14 - new column for booking agent
            if Job:Who_Booked = 'WEB' then
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe2:SIDBookingName)
            ELSE
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code = job:Who_Booked
                IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
                ELSE
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                END
            END !tb13200 web

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Account_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(clip(job:Title)&' '&clip(job:Initial)&' '&clip(job:Surname))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:EndUserTelNo)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Model_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)

            !TB13236 - J - 18/02/14 - If exchange issued at RRC then this is RRC otherwise as before
            If job:Exchange_Unit_Number <> ''  and  jobe:ExchangedATRRC then
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
            ELSE !ELSE of TB13236
                    If SentToHub(job:Ref_Number)
                        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('ARC')
                    Else !If SentToHub(job:RefNumber)
                        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('RRC')
                    End !If SentToHub(job:RefNumber)
            END  !END of  TB13236

            if job:Exchange_Unit_Number = 0 then
                !exchange unit attached
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'J'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(WOB:Current_status)
                !work out days in status
                If wob:Current_Status_Date = 0
                    ! Show a blank column if the field is blank (i.e. an aborted job) - TrkBs: 6634 (DBH: 02-11-2005)
                    statusdays" = 0
                Else ! If wob:Current_Status_Date = 0
                    statusdays" = LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Current_Status_Date)   !was today() - wob:Current_Status_Date
                End ! If wob:Current_Status_Date = 0

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StatusDays")

            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'E'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(WOB:Exchange_status)
                !work out days in status
                If wob:Exchange_Status_Date = 0
                    statusdays" = 0
                Else !If wob:Exchange_Status_Date = 0
                    statusdays" = LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Exchange_Status_Date)   !today() - wob:Exchange_Status_Date
                End !If wob:Exchange_Status_Date = 0

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StatusDays")
            END

            statusdays" = LocalGetDays(tra:IncludeSaturday,tra:IncludeSunday,job:date_booked)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(statusdays")      !was today()- job:date_booked)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Location)

            !Work out job type
            if job:Warranty_Job = 'YES' then
                if job:Chargeable_Job = 'YES'
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'S'
                ELSE !if both
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'W'
                END !if both
            ELSE
                if job:Chargeable_Job = 'YES'
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'C'
                ELSE !if only chargeable
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'N'
                END !if NEither?
            END !if warranty job
            if job:Loan_Unit_Number = 0 then
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'NO'
            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & 'YES'
            END !if no loan
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Engineer)

            Add(ExportFileJobs)

SummaryStatusReport        Routine
    Status_Summary_Report(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:ReportOrder,tmp:PaperReportType,tmp:JobBatchNumber,tmp:EDIBatchNumber,tmp:CustomerStatus)
StatusReport        Routine
    StatusReport(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:ReportOrder,tmp:PaperReportType,tmp:JobBatchNumber,tmp:EDIBatchNumber,tmp:CustomerStatus)
SaveCriteria        Routine
    tmp:NewDescription = InsertCriteriaDescription()
    If tmp:NewDescription <> ''
        If Access:STATREP.PrimeRecord() = Level:Benign
            star:Description = tmp:NewDescription
            If Access:STATREP.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:STATREP..TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:STATREP..TryInsert() = Level:Benign

            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'HEAD ACCOUNT'
                    stac:FieldValue  = glo:Pointer
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue2)
                Get(glo:Queue2,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'SUB ACCOUNT'
                    stac:FieldValue  = glo:Pointer2
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue3)
                Get(glo:Queue3,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'CHA CHARGE TYPE'
                    stac:FieldValue  = glo:Pointer3
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue4)
                Get(glo:Queue4,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'WAR CHARGE TYPE'
                    stac:FieldValue  = glo:Pointer4
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue5)
                Get(glo:Queue5,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'CHA REPAIR TYPE'
                    stac:FieldValue  = glo:Pointer5
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue6)
                Get(glo:Queue6,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'WAR REPAIR TYPE'
                    stac:FieldValue  = glo:Pointer6
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue7)
                Get(glo:Queue7,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'STATUS'
                    stac:FieldValue  = glo:Pointer7
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue8)
                Get(glo:Queue8,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'LOCATION'
                    stac:FieldValue  = glo:Pointer8
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)


            Loop x# = 1 To Records(glo:Queue9)
                Get(glo:Queue9,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'ENGINEER'
                    stac:FieldValue  = glo:Pointer9
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue10)
                Get(glo:Queue10,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'MANUFACTURER'
                    stac:FieldValue  = glo:Pointer10
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue11)
                Get(glo:Queue11,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'MODEL NUMBER'
                    stac:FieldValue  = glo:Pointer11
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue12)
                Get(glo:Queue12,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'UNIT TYPE'
                    stac:FieldValue  = glo:Pointer12
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue13)
                Get(glo:Queue13,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'TRANSIT TYPE'
                    stac:FieldValue  = glo:Pointer13
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue14)
                Get(glo:Queue14,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'TURNAROUND TIME'
                    stac:FieldValue  = glo:Pointer14
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'OUTPUT TYPE'
                stac:FieldValue     = tmp:OutputType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'PAPER REPORT TYPE'
                stac:FieldValue     = tmp:PaperReportType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT JOBS'
                stac:FieldValue     = tmp:ExportJobs
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT AUDIT'
                stac:FieldValue     = tmp:ExportAudit
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT PARTS'
                stac:FieldValue     = tmp:ExportParts
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT WARPARTS'
                stac:FieldValue     = tmp:ExportWarParts
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'STATUS TYPE'
                stac:FieldValue     = tmp:StatusType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'SELECT MANUFACTURER'
                stac:FieldValue     = tmp:SelectManufacturer
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'PICK MANUFACTURER'
                stac:FieldValue     = tmp:Manufacturer
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'WORKSHOP'
                stac:FieldValue     = tmp:Workshop
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'JOBTYPE'
                stac:FieldValue     = tmp:JobType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'COMPLETED TYPE'
                stac:FieldValue     = tmp:CompletedType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'DESPATCHED TYPE'
                stac:FieldValue     = tmp:DespatchedType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'JOB BATCH NUMBER'
                stac:FieldValue     = tmp:JobBatchNumber
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EDI BATCH NUMBER'
                stac:FieldValue     = tmp:EDIBatchNumber
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'REPORT ORDER'
                stac:FieldValue     = tmp:ReportOrder
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'DATE RANGE TYPE'
                stac:FieldValue     = tmp:DateRangeType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

        End !If Access:STATREP..PrimeRecord() = Level:Benign
    End !If tmp:NewDescription <> ''
AuditFileTitle      Routine
        Clear(expaudit:Record)
        expaudit:Line1    = 'Job Number'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Date'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Time'
        expaudit:Line1    = Clip(expaudit:Line1) & ',User'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Action'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Notes'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Type'
        Add(ExportFileAudit)
ExportAudit      Routine
        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
        END ! IF

        Clear(expaudit:Record)
        expaudit:Line1    = StripComma(aud:Ref_Number)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(Format(aud:Date,@d06))
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(Format(aud:Time,@t01))
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:User)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Action)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud2:Notes)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Type)
        expaudit:Line1    = StripQuotes(expaudit:Line1)
        Add(ExportFileAudit)
PartsFileTitle      Routine
        Clear(expparts:Record)
        expparts:Line1    = 'Job Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Part Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Description'
        expparts:Line1    = Clip(expparts:Line1) & ',Supplier'
!        expparts:Line1    = Clip(expparts:Line1) & ',Purchase Cost'
!        expparts:Line1    = Clip(expparts:Line1) & ',Sale Cost'
!        expparts:Line1    = Clip(expparts:Line1) & ',Retail Cost'
        expparts:Line1    = Clip(expparts:Line1) & ',Quantity'
        expparts:Line1    = Clip(expparts:Line1) & ',Exclude From Order'
        expparts:Line1    = Clip(expparts:Line1) & ',Despatch Note Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Order Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Date Received'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Codes Checked'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 1'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 2'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 3'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 4'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 5'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 6'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 7'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 8'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 9'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 10'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 11'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 12'
        expparts:Line1    = Clip(expparts:Line1) & ',First Part'
        Add(ExportFileParts)
ExportParts      Routine
        Clear(expparts:Record)
        expparts:Line1    = Stripcomma(par:Ref_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Part_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Description)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Supplier)
!        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Purchase_Cost)
!        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Sale_Cost)
!        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(Par:Retail_Cost)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Quantity)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Exclude_From_Order)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Despatch_Note_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Order_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(Format(par:Date_Received,@d06))
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Codes_Checked)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code1)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code2)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code3)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code4)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code5)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code6)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code7)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code8)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code9)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code10)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code11)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code12)
        If tmp:FirstPart = 0
            expparts:Line1    = Clip(expparts:Line1) & ',1'
            tmp:FirstPart = 1
        Else !If tmp:FirstPart = 0
            expparts:Line1    = Clip(expparts:Line1) & ',0'
        End !If tmp:FirstPart = 0
        expparts:Line1      = StripQuotes(expparts:Line1)
        Add(ExportFileParts)
WarPartsFileTitle       Routine
        Clear(expwarparts:Record)
        expwarparts:Line1    = 'Job Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Part Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Description'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Supplier'
!        expwarparts:Line1    = Clip(expparts:Line1) & ',Purchase Cost'
!        expwarparts:Line1    = Clip(expparts:Line1) & ',Sale Cost'
!        expwarparts:Line1    = Clip(expparts:Line1) & ',Retail Cost'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Quantity'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Exclude From Order'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Despatch Note Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Order Number'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Date Received'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Codes Checked'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 1'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 2'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 3'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 4'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 5'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 6'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 7'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 8'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 9'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 10'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 11'
        expwarparts:Line1    = Clip(expparts:Line1) & ',Fault Code 12'
        expwarparts:Line1    = Clip(expparts:Line1) & ',First Part'
        Add(ExportFileWarParts)
ExportWarParts      Routine
        Clear(expwarparts:Record)
        expwarparts:Line1    = Stripcomma(wpr:Ref_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Part_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Description)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Supplier)
!        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Purchase_Cost)
!        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Sale_Cost)
!        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Retail_Cost)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Quantity)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Exclude_From_Order)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Despatch_Note_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Order_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(Format(wpr:Date_Received,@d06))
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Codes_Checked)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code1)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code2)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code3)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code4)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code5)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code6)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code7)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code8)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code9)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code10)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code11)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code12)
        If tmp:FirstPart = 0
            expwarparts:Line1    = Clip(expwarparts:Line1) & ',1'
            tmp:FirstPart = 1
        Else !If tmp:FirstPart = 0
            expwarparts:Line1    = Clip(expwarparts:Line1) & ',0'
        End !If tmp:FirstPart = 0
        expwarparts:Line1       = StripQuotes(expwarparts:Line1)
        Add(ExportFileWarParts)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    progresswindow{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                tmp:cancel = 1
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020177'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Status_Report_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt46
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBPAYMT.Open
  Relate:STATCRIT.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:STATREP.UseFile
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBSE.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:LOCATLOG.UseFile
  Access:REPTYDEF.UseFile
  Access:INVOICE.UseFile
  Access:AUDIT2.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  tmp:ExportPath  = def:ExportPath
  
  
  
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate = Today()
  tmp:ReportOrder = 'DATE BOOKED'
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:CHARTYPE,SELF)
  BRW7.Init(?List:4,Queue:Browse:3.ViewPosition,BRW7::View:Browse,Queue:Browse:3,Relate:CHARTYPE,SELF)
  BRW8.Init(?List:5,Queue:Browse:4.ViewPosition,BRW8::View:Browse,Queue:Browse:4,Relate:REPTYDEF,SELF)
  BRW9.Init(?List:6,Queue:Browse:5.ViewPosition,BRW9::View:Browse,Queue:Browse:5,Relate:REPTYDEF,SELF)
  BRW10.Init(?List:7,Queue:Browse:6.ViewPosition,BRW10::View:Browse,Queue:Browse:6,Relate:STATUS,SELF)
  BRW14.Init(?List:8,Queue:Browse:7.ViewPosition,BRW14::View:Browse,Queue:Browse:7,Relate:LOCINTER,SELF)
  BRW15.Init(?List:9,Queue:Browse:8.ViewPosition,BRW15::View:Browse,Queue:Browse:8,Relate:USERS,SELF)
  BRW16.Init(?List:10,Queue:Browse:9.ViewPosition,BRW16::View:Browse,Queue:Browse:9,Relate:MANUFACT,SELF)
  BRW17.Init(?List:11,Queue:Browse:10.ViewPosition,BRW17::View:Browse,Queue:Browse:10,Relate:MODELNUM,SELF)
  BRW21.Init(?List:12,Queue:Browse:11.ViewPosition,BRW21::View:Browse,Queue:Browse:11,Relate:UNITTYPE,SELF)
  BRW22.Init(?List:13,Queue:Browse:12.ViewPosition,BRW22::View:Browse,Queue:Browse:12,Relate:TRANTYPE,SELF)
  BRW23.Init(?List:14,Queue:Browse:13.ViewPosition,BRW23::View:Browse,Queue:Browse:13,Relate:TURNARND,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ?List:6{prop:vcr} = TRUE
  ?List:7{prop:vcr} = TRUE
  ?List:8{prop:vcr} = TRUE
  ?List:9{prop:vcr} = TRUE
  ?List:10{prop:vcr} = TRUE
  ?List:11{prop:vcr} = TRUE
  ?List:12{prop:vcr} = TRUE
  ?List:13{prop:vcr} = TRUE
  ?List:14{prop:vcr} = TRUE
  ?tmp:ReportOrder{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard13.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,tra:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,tra:Account_Number,1,BRW4)
  BIND('tmp:HeadAccountTag',tmp:HeadAccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:HeadAccountTag,BRW4.Q.tmp:HeadAccountTag)
  BRW4.AddField(tra:Account_Number,BRW4.Q.tra:Account_Number)
  BRW4.AddField(tra:Company_Name,BRW4.Q.tra:Company_Name)
  BRW4.AddField(tra:RecordNumber,BRW4.Q.tra:RecordNumber)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:SubAccountTag',tmp:SubAccountTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:SubAccountTag,BRW5.Q.tmp:SubAccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,cha:Warranty_Key)
  BRW6.AddRange(cha:Warranty,tmp:No)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,cha:Charge_Type,1,BRW6)
  BIND('tmp:CChargeTypeTag',tmp:CChargeTypeTag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:CChargeTypeTag,BRW6.Q.tmp:CChargeTypeTag)
  BRW6.AddField(cha:Charge_Type,BRW6.Q.cha:Charge_Type)
  BRW6.AddField(cha:Warranty,BRW6.Q.cha:Warranty)
  BRW7.Q &= Queue:Browse:3
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,cha:Warranty_Key)
  BRW7.AddRange(cha:Warranty,tmp:Yes)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,cha:Charge_Type,1,BRW7)
  BIND('tmp:WChargeType',tmp:WChargeType)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:WChargeType,BRW7.Q.tmp:WChargeType)
  BRW7.AddField(cha:Charge_Type,BRW7.Q.cha:Charge_Type)
  BRW7.AddField(cha:Warranty,BRW7.Q.cha:Warranty)
  BRW8.Q &= Queue:Browse:4
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,rtd:Chargeable_Key)
  BRW8.AddRange(rtd:Chargeable,tmp:Yes)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,rtd:Repair_Type,1,BRW8)
  BIND('tmp:CRepairTypeTag',tmp:CRepairTypeTag)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:CRepairTypeTag,BRW8.Q.tmp:CRepairTypeTag)
  BRW8.AddField(rtd:Repair_Type,BRW8.Q.rtd:Repair_Type)
  BRW8.AddField(rtd:Manufacturer,BRW8.Q.rtd:Manufacturer)
  BRW8.AddField(rtd:RecordNumber,BRW8.Q.rtd:RecordNumber)
  BRW8.AddField(rtd:Chargeable,BRW8.Q.rtd:Chargeable)
  BRW9.Q &= Queue:Browse:5
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,rtd:Warranty_Key)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,rtd:Warranty,1,BRW9)
  BIND('tmp:WRepairTypeTag',tmp:WRepairTypeTag)
  ?List:6{PROP:IconList,1} = '~notick1.ico'
  ?List:6{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:WRepairTypeTag,BRW9.Q.tmp:WRepairTypeTag)
  BRW9.AddField(rtd:Repair_Type,BRW9.Q.rtd:Repair_Type)
  BRW9.AddField(rtd:Manufacturer,BRW9.Q.rtd:Manufacturer)
  BRW9.AddField(rtd:RecordNumber,BRW9.Q.rtd:RecordNumber)
  BRW9.AddField(rtd:Warranty,BRW9.Q.rtd:Warranty)
  BRW10.Q &= Queue:Browse:6
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,sts:JobKey)
  BRW10.AddRange(sts:Job,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort1:Locator)
  BRW10::Sort1:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:ExchangeKey)
  BRW10.AddRange(sts:Exchange,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort2:Locator)
  BRW10::Sort2:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:LoanKey)
  BRW10.AddRange(sts:Loan,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort3:Locator)
  BRW10::Sort3:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:Status_Key)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,sts:Status,1,BRW10)
  BIND('tmp:StatusTag',tmp:StatusTag)
  ?List:7{PROP:IconList,1} = '~notick1.ico'
  ?List:7{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tmp:StatusTag,BRW10.Q.tmp:StatusTag)
  BRW10.AddField(sts:Status,BRW10.Q.sts:Status)
  BRW10.AddField(sts:Ref_Number,BRW10.Q.sts:Ref_Number)
  BRW10.AddField(sts:Job,BRW10.Q.sts:Job)
  BRW10.AddField(sts:Exchange,BRW10.Q.sts:Exchange)
  BRW10.AddField(sts:Loan,BRW10.Q.sts:Loan)
  BRW14.Q &= Queue:Browse:7
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,loi:Location_Key)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,loi:Location,1,BRW14)
  BIND('tmp:LocationTag',tmp:LocationTag)
  ?List:8{PROP:IconList,1} = '~notick1.ico'
  ?List:8{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(tmp:LocationTag,BRW14.Q.tmp:LocationTag)
  BRW14.AddField(loi:Location,BRW14.Q.loi:Location)
  BRW15.Q &= Queue:Browse:8
  BRW15.RetainRow = 0
  BRW15.AddSortOrder(,use:User_Type_Key)
  BRW15.AddRange(use:User_Type,tmp:ENGINEER)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,use:Surname,1,BRW15)
  BIND('tmp:UserTag',tmp:UserTag)
  ?List:9{PROP:IconList,1} = '~notick1.ico'
  ?List:9{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(tmp:UserTag,BRW15.Q.tmp:UserTag)
  BRW15.AddField(use:Surname,BRW15.Q.use:Surname)
  BRW15.AddField(use:Forename,BRW15.Q.use:Forename)
  BRW15.AddField(use:User_Code,BRW15.Q.use:User_Code)
  BRW15.AddField(use:User_Type,BRW15.Q.use:User_Type)
  BRW16.Q &= Queue:Browse:9
  BRW16.RetainRow = 0
  BRW16.AddSortOrder(,man:Manufacturer_Key)
  BRW16.AddLocator(BRW16::Sort0:Locator)
  BRW16::Sort0:Locator.Init(,man:Manufacturer,1,BRW16)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:10{PROP:IconList,1} = '~notick1.ico'
  ?List:10{PROP:IconList,2} = '~tick1.ico'
  BRW16.AddField(tmp:ManufacturerTag,BRW16.Q.tmp:ManufacturerTag)
  BRW16.AddField(man:Manufacturer,BRW16.Q.man:Manufacturer)
  BRW16.AddField(man:RecordNumber,BRW16.Q.man:RecordNumber)
  BRW17.Q &= Queue:Browse:10
  BRW17.RetainRow = 0
  BRW17.AddSortOrder(,mod:Model_Number_Key)
  BRW17.AddLocator(BRW17::Sort1:Locator)
  BRW17::Sort1:Locator.Init(,mod:Model_Number,1,BRW17)
  BRW17.AddSortOrder(,mod:Manufacturer_Key)
  BRW17.AddRange(mod:Manufacturer,tmp:Manufacturer)
  BRW17.AddLocator(BRW17::Sort2:Locator)
  BRW17::Sort2:Locator.Init(,mod:Model_Number,1,BRW17)
  BRW17.AddSortOrder(,mod:Manufacturer_Key)
  BRW17.AddLocator(BRW17::Sort0:Locator)
  BRW17::Sort0:Locator.Init(,mod:Manufacturer,1,BRW17)
  BIND('tmp:ModelNumberTag',tmp:ModelNumberTag)
  ?List:11{PROP:IconList,1} = '~notick1.ico'
  ?List:11{PROP:IconList,2} = '~tick1.ico'
  BRW17.AddField(tmp:ModelNumberTag,BRW17.Q.tmp:ModelNumberTag)
  BRW17.AddField(mod:Model_Number,BRW17.Q.mod:Model_Number)
  BRW17.AddField(mod:Manufacturer,BRW17.Q.mod:Manufacturer)
  BRW21.Q &= Queue:Browse:11
  BRW21.RetainRow = 0
  BRW21.AddSortOrder(,uni:Unit_Type_Key)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,uni:Unit_Type,1,BRW21)
  BIND('tmp:UnitTypeTag',tmp:UnitTypeTag)
  ?List:12{PROP:IconList,1} = '~notick1.ico'
  ?List:12{PROP:IconList,2} = '~tick1.ico'
  BRW21.AddField(tmp:UnitTypeTag,BRW21.Q.tmp:UnitTypeTag)
  BRW21.AddField(uni:Unit_Type,BRW21.Q.uni:Unit_Type)
  BRW22.Q &= Queue:Browse:12
  BRW22.RetainRow = 0
  BRW22.AddSortOrder(,trt:Transit_Type_Key)
  BRW22.AddLocator(BRW22::Sort0:Locator)
  BRW22::Sort0:Locator.Init(,trt:Transit_Type,1,BRW22)
  BIND('tmp:TransitTypeTag',tmp:TransitTypeTag)
  ?List:13{PROP:IconList,1} = '~notick1.ico'
  ?List:13{PROP:IconList,2} = '~tick1.ico'
  BRW22.AddField(tmp:TransitTypeTag,BRW22.Q.tmp:TransitTypeTag)
  BRW22.AddField(trt:Transit_Type,BRW22.Q.trt:Transit_Type)
  BRW23.Q &= Queue:Browse:13
  BRW23.RetainRow = 0
  BRW23.AddSortOrder(,tur:Turnaround_Time_Key)
  BRW23.AddLocator(BRW23::Sort0:Locator)
  BRW23::Sort0:Locator.Init(,tur:Turnaround_Time,1,BRW23)
  BIND('tmp:TurnaroundTimeTag',tmp:TurnaroundTimeTag)
  ?List:14{PROP:IconList,1} = '~notick1.ico'
  ?List:14{PROP:IconList,2} = '~tick1.ico'
  BRW23.AddField(tmp:TurnaroundTimeTag,BRW23.Q.tmp:TurnaroundTimeTag)
  BRW23.AddField(tur:Turnaround_Time,BRW23.Q.tur:Turnaround_Time)
  IF ?tmp:SelectManufacturer{Prop:Checked} = True
    ENABLE(?Prompt39)
    ENABLE(?tmp:Manufacturer)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = False
    DISABLE(?Prompt39)
    DISABLE(?tmp:Manufacturer)
  END
  FDCB18.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo
  FDCB18.AddSortOrder(man:Manufacturer_Key)
  FDCB18.AddField(man:Manufacturer,FDCB18.Q.man:Manufacturer)
  FDCB18.AddField(man:RecordNumber,FDCB18.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FileLookup34.Init
  FileLookup34.Flags=BOR(FileLookup34.Flags,FILE:LongName)
  FileLookup34.Flags=BOR(FileLookup34.Flags,FILE:Directory)
  FileLookup34.SetMask('All Files','*.*')
  FileLookup34.WindowTitle='Export Path'
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  BRW15.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW15.AskProcedure = 0
      CLEAR(BRW15.AskProcedure, 1)
    END
  END
  BRW16.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW16.AskProcedure = 0
      CLEAR(BRW16.AskProcedure, 1)
    END
  END
  BRW17.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW17.AskProcedure = 0
      CLEAR(BRW17.AskProcedure, 1)
    END
  END
  BRW21.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW21.AskProcedure = 0
      CLEAR(BRW21.AskProcedure, 1)
    END
  END
  BRW22.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW22.AskProcedure = 0
      CLEAR(BRW22.AskProcedure, 1)
    END
  END
  BRW23.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW23.AskProcedure = 0
      CLEAR(BRW23.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  ?DASSHOWTAG:5{PROP:Text} = 'Show All'
  ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue6)
  ?DASSHOWTAG:6{PROP:Text} = 'Show All'
  ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  ?DASSHOWTAG:7{PROP:Text} = 'Show All'
  ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue8)
  ?DASSHOWTAG:8{PROP:Text} = 'Show All'
  ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue9)
  ?DASSHOWTAG:9{PROP:Text} = 'Show All'
  ?DASSHOWTAG:9{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:9{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  ?DASSHOWTAG:10{PROP:Text} = 'Show All'
  ?DASSHOWTAG:10{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:10{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue11)
  ?DASSHOWTAG:11{PROP:Text} = 'Show All'
  ?DASSHOWTAG:11{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:11{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue12)
  ?DASSHOWTAG:12{PROP:Text} = 'Show All'
  ?DASSHOWTAG:12{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:12{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue13)
  ?DASSHOWTAG:13{PROP:Text} = 'Show All'
  ?DASSHOWTAG:13{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:13{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue14)
  ?DASSHOWTAG:14{PROP:Text} = 'Show All'
  ?DASSHOWTAG:14{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:14{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue6)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue8)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue9)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue11)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue12)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue13)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue14)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBPAYMT.Close
    Relate:STATCRIT.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard13.Validate()
        DISABLE(Wizard13.NextControl())
     ELSE
        ENABLE(Wizard13.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:OutputType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OutputType, Accepted)
      if tmp:OutputType=0 then
          hide(?Tmp:ShortReportFlag)
      ELSE
          unhide(?Tmp:ShortReportFlag)
      
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OutputType, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectCriteria
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseStatusCriteria
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectCriteria, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              tmp:SavedCriteria   =   star:Description
      
              Free(glo:Queue)
              Free(glo:Queue2)
              Free(glo:Queue3)
              Free(glo:Queue4)
              Free(glo:Queue5)
              Free(glo:Queue6)
              Free(glo:Queue7)
              Free(glo:Queue8)
              Free(glo:Queue9)
              Free(glo:Queue10)
              Free(glo:Queue11)
              Free(glo:Queue12)
              Free(glo:Queue13)
              Free(glo:Queue14)
      
      
              Setcursor(Cursor:Wait)
              Save_STAC_ID = Access:STATCRIT.SaveFile()
              Access:STATCRIT.ClearKey(STAC:DescriptionKey)
              STAC:Description = tmp:SavedCriteria
              Set(STAC:DescriptionKey,STAC:DescriptionKey)
              Loop
                  If Access:STATCRIT.NEXT()
                     Break
                  End !If
                  If STAC:Description <> tmp:SavedCriteria      |
                      Then Break.  ! End If
      
                  Case stac:OptionType
                      Of 'HEAD ACCOUNT'
                          glo:Pointer = stac:FieldValue
                          Add(glo:Queue)
                      Of 'SUB ACCOUNT'
                          glo:Pointer2 = stac:FieldValue
                          Add(glo:Queue2)
                      Of 'CHA CHARGE TYPE'
                          glo:Pointer3 = stac:FieldValue
                          Add(glo:Queue3)
                      Of 'WAR CHARGE TYPE'
                          glo:Pointer4 = stac:FieldValue
                          Add(glo:Queue4)
                      Of 'CHA REPAIR TYPE'
                          glo:Pointer5 = stac:FieldValue
                          Add(glo:Queue5)
                      Of 'WAR REPAIR TYPE'
                          glo:Pointer6 = stac:FieldValue
                          Add(glo:Queue6)
                      Of 'STATUS'
                          glo:Pointer7 = stac:FieldValue
                          Add(glo:Queue7)
                      Of 'LOCATION'
                          glo:Pointer8 = stac:FieldValue
                          Add(glo:Queue8)
                      Of 'ENGINEER'
                          glo:Pointer9 = stac:FieldValue
                          Add(glo:Queue9)
                      Of 'MANUFACTURER'
                          glo:Pointer10 = stac:FieldValue
                          Add(glo:Queue10)
                      Of 'MODEL NUMBER'
                          glo:Pointer11 = stac:FieldValue
                          Add(glo:Queue11)
                      Of 'UNIT TYPE'
                          glo:Pointer12 = stac:FieldValue
                          Add(glo:Queue12)
                      Of 'TRANSIT TYPE'
                          glo:Pointer13 = stac:FieldValue
                          Add(glo:Queue13)
                      Of 'TURNAROUND TIME'
                          glo:Pointer14 = stac:FieldValue
                          Add(glo:Queue14)
                      Of 'OUTPUT TYPE'
                          tmp:OutputType  = stac:FieldValue
                      Of 'PAPER REPORT TYPE'
                          tmp:PaperReportType = stac:FieldValue
                      Of 'EXPORT JOBS'
                          tmp:ExportJobs  = stac:FieldValue
                      Of 'EXPORT AUDIT'
                          tmp:ExportAudit = stac:FieldValue
                      Of 'EXPORT PARTS'
                          tmp:ExportParts = stac:FieldValue
                      Of 'EXPORT WARPARTS'
                          tmp:ExportWarParts = stac:FieldValue
                      Of 'STATUS TYPE'
                          tmp:StatusType = stac:FieldValue
                      Of 'SELECT MANUFACTURER'
                          tmp:SelectManufacturer  = stac:FieldValue
                      Of 'PICK MANUFACTURER'
                          tmp:Manufacturer    = stac:FieldValue
                      Of 'WORKSHOP'
                          tmp:Workshop = stac:FieldValue
                      Of 'JOBTYPE'
                          tmp:JobType = stac:FieldValue
                      Of 'COMPLETED TYPE'
                          tmp:Completedtype = stac:FieldValue
                      Of 'DESPATCHED TYPE'
                          tmp:DespatchedType = stac:FieldValue
                      Of 'JOB BATCH NUMBER'
                          tmp:JobBatchNumber  = stac:FieldValue
                      Of 'EDI BATCH NUMBER'
                          tmp:EDIBatchNumber  = stac:FieldValue
                      Of 'REPORT ORDER'
                          tmp:ReportOrder = stac:FieldValue
                      Of 'DATE RANGE TYPE'
                          tmp:DateRangeType = stac:FieldValue
      
                  End !Case stac:OptionType
              End !Loop
              Access:STATCRIT.RestoreFile(Save_STAC_ID)
              Setcursor()
      
              Post(Event:Accepted,?tmp:SelectManufacturer)
      
          Of Requestcancelled
      End!Case Globalreponse
      Display()
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectCriteria, Accepted)
    OF ?LookupExportPath
      ThisWindow.Update
      tmp:ExportPath = Upper(FileLookup34.Ask(1)  )
      DISPLAY
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:StatusType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
      BRW10.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
    OF ?DASREVTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:SelectManufacturer
      IF ?tmp:SelectManufacturer{Prop:Checked} = True
        ENABLE(?Prompt39)
        ENABLE(?tmp:Manufacturer)
      END
      IF ?tmp:SelectManufacturer{Prop:Checked} = False
        DISABLE(?Prompt39)
        DISABLE(?tmp:Manufacturer)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
      BRW17.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW17.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?DASREVTAG:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020177'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020177'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020177'&'0')
      ***
    OF ?VSBackButton
      ThisWindow.Update
         Wizard13.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      DoNext# = 1
      Case Choice(?Sheet1)
          Of 1
              Case tmp:OutputType
                  Of 0
                      ?Tab3{prop:Hide} = 1
                      ?Tab2{prop:Hide} = 0
                  Of 1
                      ?Tab2{prop:Hide} = 1
                      ?Tab3{prop:Hide} = 0
              End !Case tmp:OutputType
          Of 3
              If tmp:ExportJobs = 0 And |
                  tmp:ExportAudit = 0 And |
                  tmp:ExportParts = 0 And |
                  tmp:ExportWarPArts = 0
                  Case Missive('Please select at least ONE file to export.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  DoNext# = 0
              End !tmp:ExportWarPArts = 0
          Of 6 !Job Types etc
              If tmp:Workshop = 2
                  ?Tab11{prop:Hide} = 1
                  ?Tab12{prop:Hide} = 1
              Else !If tmp:Workshop = 2
                  ?Tab11{prop:Hide} = 0
                  ?Tab12{prop:Hide} = 0
              End !If tmp:Workshop = 2
      
              Case tmp:JobType
                  Of 1 !Warranty Only
                      ?Tab6{prop:Hide} = 1
                      ?Tab8{prop:Hide} = 1
                      ?Tab7{prop:Hide} = 0
                      ?Tab9{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 0
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 0
                  Of 4 !Chargeable Only
                      ?Tab7{prop:Hide} = 1
                      ?Tab9{prop:Hide} = 1
                      ?Tab6{prop:Hide} = 0
                      ?Tab8{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 1
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 1
                  Else
                      ?Tab7{prop:Hide} = 0
                      ?Tab9{prop:Hide} = 0
                      ?Tab6{prop:Hide} = 0
                      ?Tab8{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 0
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 0
              End !Case tmp:JobType
      
              If tmp:CompletedType = 2
                  tmp:ReportOrder = 2
                  ?tmp:DateRangeType:Radio2{prop:Disable} = 1
                  ?tmp:ReportOrder{prop:From} = 'DATE BOOKED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER'
              Else !If tmp:CompltedType = 2
                  ?tmp:DateRangeType:Radio2{prop:Disable} = 0
                  ?tmp:ReportOrder{prop:From} = 'DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER'
              End !If tmp:CompltedType = 2
      End !Case Choice(?CurrentTab)
      If DoNext# = 1
         Wizard13.TakeAccepted()
      End !If DoNext# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      Case Missive('Do you want to save your criteria for future use, or run the report/export?','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|Run Only|Save-Run|Save Only')
          Of 4 ! Save Only Button
              Do SaveCriteria
              Post(Event:CloseWindow)
          Of 3 ! Save-Run Button
              Do SaveCriteria
      
              Case tmp:OutputType
                  Of 0 !Report
                      Case tmp:PaperReportType
                          Of 1
                              Do SummaryStatusReport
                          Else
                              Do StatusReport
                      End !Case tmp:PaperReportType
                  Of 1 !Export
                      Do Export
              End !Case tmp:OutputType
          Of 2 ! Run Only Button
      
              Case tmp:OutputType
                  Of 0 !Report
                      Case tmp:PaperReportType
                          Of 1
                              Do SummaryStatusReport
                          Else
                              Do StatusReport
                      End !Case tmp:PaperReportType
                  Of 1 !Export
                      Do Export
              End !Case tmp:OutputType
          Of 1 ! Cancel Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:2)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:3, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:3)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:3, AlertKey)
    END
  OF ?List:4
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:4, AlertKey)
      If Keycode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:4)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:4, AlertKey)
    END
  OF ?List:5
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:5, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:Accepted,?DasTag:5)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:5, AlertKey)
    END
  OF ?List:6
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:6, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:Accepted,?DasTag:6)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:6, AlertKey)
    END
  OF ?List:7
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:7, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:7)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:7, AlertKey)
    END
  OF ?List:8
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:8, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:8)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:8, AlertKey)
    END
  OF ?List:9
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:9, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:9)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:9, AlertKey)
    END
  OF ?List:10
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:10, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:10)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:10, AlertKey)
    END
  OF ?List:11
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:11, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:11)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:11, AlertKey)
    END
  OF ?List:12
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:12, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:12)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:12, AlertKey)
    END
  OF ?List:13
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:13, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:13)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:13, AlertKey)
    END
  OF ?List:14
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:14, AlertKey)
      If Keycode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag:14)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:14, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:6
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:7
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:8
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:9
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:10
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:11
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:12
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:13
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:14
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalGetDays       Procedure(String func:Sat,string func:Sun,Date func:Start)
    
loc:Count       Long
loc:Days        Long

    Code

    loc:Days = 0
    loc:Count = 0

    Loop

        loc:Count += 1

        If (func:Start + loc:Count) %7 = 0 And func:Sun <> 'YES'
            Cycle
        End
        If (func:Start + loc:Count) %7 = 6 And func:Sat <> 'YES'
            Cycle
        End

        loc:Days += 1

        If func:Start + loc:Count >= Today()
            Break
        End !If aus:DateChanged + Count# = Today()

    End !Loop

    Return loc:Days
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:HeadAccountTag = ''
    ELSE
      tmp:HeadAccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:HeadAccounttag = '*')
    SELF.Q.tmp:HeadAccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:HeadAccountTag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::24:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sub:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:SubAccountTag = ''
    ELSE
      tmp:SubAccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:SubAccounttag = '*')
    SELF.Q.tmp:SubAccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:SubAccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sub:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::25:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = cha:Charge_Type
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:CChargeTypeTag = ''
    ELSE
      tmp:CChargeTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:CChargeTypetag = '*')
    SELF.Q.tmp:CChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:CChargeTypeTag_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = cha:Charge_Type
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::26:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = cha:Charge_Type
     GET(glo:Queue4,glo:Queue4.Pointer4)
    IF ERRORCODE()
      tmp:WChargeType = ''
    ELSE
      tmp:WChargeType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:WChargeType = '*')
    SELF.Q.tmp:WChargeType_Icon = 2
  ELSE
    SELF.Q.tmp:WChargeType_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = cha:Charge_Type
     GET(glo:Queue4,glo:Queue4.Pointer4)
    EXECUTE DASBRW::27:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = rtd:Repair_Type
     GET(glo:Queue5,glo:Queue5.Pointer5)
    IF ERRORCODE()
      tmp:CRepairTypeTag = ''
    ELSE
      tmp:CRepairTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:CRepairTypetag = '*')
    SELF.Q.tmp:CRepairTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:CRepairTypeTag_Icon = 1
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = rtd:Repair_Type
     GET(glo:Queue5,glo:Queue5.Pointer5)
    EXECUTE DASBRW::28:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue6.Pointer6 = rtd:Repair_Type
     GET(glo:Queue6,glo:Queue6.Pointer6)
    IF ERRORCODE()
      tmp:WRepairTypeTag = ''
    ELSE
      tmp:WRepairTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:WRepairTypetag = '*')
    SELF.Q.tmp:WRepairTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:WRepairTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue6.Pointer6 = rtd:Repair_Type
     GET(glo:Queue6,glo:Queue6.Pointer6)
    EXECUTE DASBRW::29:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW10.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:StatusType = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:StatusType = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF tmp:StatusType = 2
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = sts:Status
     GET(glo:Queue7,glo:Queue7.Pointer7)
    IF ERRORCODE()
      tmp:StatusTag = ''
    ELSE
      tmp:StatusTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Statustag = '*')
    SELF.Q.tmp:StatusTag_Icon = 2
  ELSE
    SELF.Q.tmp:StatusTag_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = sts:Status
     GET(glo:Queue7,glo:Queue7.Pointer7)
    EXECUTE DASBRW::30:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue8.Pointer8 = loi:Location
     GET(glo:Queue8,glo:Queue8.Pointer8)
    IF ERRORCODE()
      tmp:LocationTag = ''
    ELSE
      tmp:LocationTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Locationtag = '*')
    SELF.Q.tmp:LocationTag_Icon = 2
  ELSE
    SELF.Q.tmp:LocationTag_Icon = 1
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue8.Pointer8 = loi:Location
     GET(glo:Queue8,glo:Queue8.Pointer8)
    EXECUTE DASBRW::31:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue9.Pointer9 = use:User_Code
     GET(glo:Queue9,glo:Queue9.Pointer9)
    IF ERRORCODE()
      tmp:UserTag = ''
    ELSE
      tmp:UserTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Usertag = '*')
    SELF.Q.tmp:UserTag_Icon = 2
  ELSE
    SELF.Q.tmp:UserTag_Icon = 1
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW15::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW15::RecordStatus=ReturnValue
  IF BRW15::RecordStatus NOT=Record:OK THEN RETURN BRW15::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue9.Pointer9 = use:User_Code
     GET(glo:Queue9,glo:Queue9.Pointer9)
    EXECUTE DASBRW::32:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW15::RecordStatus
  RETURN ReturnValue


BRW16.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = man:Manufacturer
     GET(glo:Queue10,glo:Queue10.Pointer10)
    IF ERRORCODE()
      tmp:ManufacturerTag = ''
    ELSE
      tmp:ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Manufacturertag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW16.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW16.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW16::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW16::RecordStatus=ReturnValue
  IF BRW16::RecordStatus NOT=Record:OK THEN RETURN BRW16::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = man:Manufacturer
     GET(glo:Queue10,glo:Queue10.Pointer10)
    EXECUTE DASBRW::33:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW16::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW16::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW16::RecordStatus
  RETURN ReturnValue


BRW17.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:SelectManufacturer = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:SelectManufacturer = 1
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW17.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue11.Pointer11 = mod:Model_Number
     GET(glo:Queue11,glo:Queue11.Pointer11)
    IF ERRORCODE()
      tmp:ModelNumberTag = ''
    ELSE
      tmp:ModelNumberTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ModelNumbertag = '*')
    SELF.Q.tmp:ModelNumberTag_Icon = 2
  ELSE
    SELF.Q.tmp:ModelNumberTag_Icon = 1
  END


BRW17.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW17.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW17::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW17::RecordStatus=ReturnValue
  IF BRW17::RecordStatus NOT=Record:OK THEN RETURN BRW17::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue11.Pointer11 = mod:Model_Number
     GET(glo:Queue11,glo:Queue11.Pointer11)
    EXECUTE DASBRW::35:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW17::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW17::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW17::RecordStatus
  RETURN ReturnValue


BRW21.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue12.Pointer12 = uni:Unit_Type
     GET(glo:Queue12,glo:Queue12.Pointer12)
    IF ERRORCODE()
      tmp:UnitTypeTag = ''
    ELSE
      tmp:UnitTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:UnitTypetag = '*')
    SELF.Q.tmp:UnitTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:UnitTypeTag_Icon = 1
  END


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW21::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW21::RecordStatus=ReturnValue
  IF BRW21::RecordStatus NOT=Record:OK THEN RETURN BRW21::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue12.Pointer12 = uni:Unit_Type
     GET(glo:Queue12,glo:Queue12.Pointer12)
    EXECUTE DASBRW::36:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW21::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW21::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW21::RecordStatus
  RETURN ReturnValue


BRW22.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue13.Pointer13 = trt:Transit_Type
     GET(glo:Queue13,glo:Queue13.Pointer13)
    IF ERRORCODE()
      tmp:TransitTypeTag = ''
    ELSE
      tmp:TransitTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:TransitTypetag = '*')
    SELF.Q.tmp:TransitTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:TransitTypeTag_Icon = 1
  END


BRW22.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW22.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW22::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW22::RecordStatus=ReturnValue
  IF BRW22::RecordStatus NOT=Record:OK THEN RETURN BRW22::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue13.Pointer13 = trt:Transit_Type
     GET(glo:Queue13,glo:Queue13.Pointer13)
    EXECUTE DASBRW::37:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW22::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW22::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW22::RecordStatus
  RETURN ReturnValue


BRW23.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     GET(glo:Queue14,glo:Queue14.Pointer14)
    IF ERRORCODE()
      tmp:TurnaroundTimeTag = ''
    ELSE
      tmp:TurnaroundTimeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:TurnaroundTimetag = '*')
    SELF.Q.tmp:TurnaroundTimeTag_Icon = 2
  ELSE
    SELF.Q.tmp:TurnaroundTimeTag_Icon = 1
  END


BRW23.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW23.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW23::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW23::RecordStatus=ReturnValue
  IF BRW23::RecordStatus NOT=Record:OK THEN RETURN BRW23::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     GET(glo:Queue14,glo:Queue14.Pointer14)
    EXECUTE DASBRW::38:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW23::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW23::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW23::RecordStatus
  RETURN ReturnValue

Wizard13.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW6.Q &= NULL) ! Has Browse Object been initialized?
       BRW6.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW7.Q &= NULL) ! Has Browse Object been initialized?
       BRW7.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW10.Q &= NULL) ! Has Browse Object been initialized?
       BRW10.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW14.Q &= NULL) ! Has Browse Object been initialized?
       BRW14.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW15.Q &= NULL) ! Has Browse Object been initialized?
       BRW15.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW16.Q &= NULL) ! Has Browse Object been initialized?
       BRW16.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW17.Q &= NULL) ! Has Browse Object been initialized?
       BRW17.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW21.Q &= NULL) ! Has Browse Object been initialized?
       BRW21.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW22.Q &= NULL) ! Has Browse Object been initialized?
       BRW22.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW23.Q &= NULL) ! Has Browse Object been initialized?
       BRW23.ResetQueue(Reset:Queue)
    END

Wizard13.TakeBackEmbed PROCEDURE
   CODE

Wizard13.TakeNextEmbed PROCEDURE
   CODE

Wizard13.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()






ExchangeOrders PROCEDURE(DefModelQueue ModelQueue,LONG func:JobNumber,DATE func:ActivationDate,<LONG fOrderNumber>)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
!ModelQueue  Queue(DefModelQueue)
!            End
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
RejectRecord         LONG,AUTO
LineCost             DECIMAL(10,2)
TotalLineCost        DECIMAL(10,2)
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
address:Telephone_Number STRING(20)
address:Fax_No       STRING(20)
address:Email        STRING(50)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Total_No_Of_Lines    LONG
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:Manufacturer     STRING(30)
tmp:ModelNumber      STRING(30)
tmp:Quantity         LONG
tmp:JobNumber        LONG
tmp:ActivationDate   DATE
tmp:AccountNumber    STRING(30)
locExchangeCost      REAL
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDWEBPR)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@n_6),AT(5833,604,521,208),USE(eno:RecordNumber),TRN,HIDE,RIGHT(1),FONT(,9,,FONT:bold)
                         STRING(@s20),AT(52,833),USE(address:Post_Code),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4938,948),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D17),AT(5833,938),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Email:'),AT(52,1302),USE(?String33),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s50),AT(365,1302),USE(address:Email),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@d17),AT(5833,771),USE(eno:DateCreated),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Order No:'),AT(4938,604),USE(?strOrderNumber),TRN,HIDE,FONT('Arial',9,,,CHARSET:ANSI)
                         STRING('Fax:'),AT(52,1146),USE(?String34),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(52,990),USE(?String32),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4938,1104),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(5833,1094),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6198,1094),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6510,1094,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@t7),AT(6510,771),USE(eno:TimeCreated),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Ordered:'),AT(4938,792),USE(?strOrderDate),TRN,HIDE,FONT('Arial',8,,)
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,0),USE(Address:User_Name),TRN,FONT('Arial',14,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s8),AT(3958,0),USE(tmp:Quantity),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s20),AT(4531,0),USE(tmp:JobNumber),TRN,HIDE,FONT(,8,,)
                           STRING(@d6),AT(6615,0),USE(tmp:ActivationDate),HIDE,RIGHT,FONT(,9,,)
                           STRING(@n10.2),AT(5833,0),USE(locExchangeCost),TRN,HIDE,RIGHT(14),FONT(,8,,)
                           STRING(@s30),AT(104,0),USE(tmp:Manufacturer),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2031,0,,208),USE(tmp:ModelNumber),TRN,FONT('Arial',8,,)
                         END
                         FOOTER,AT(0,0),USE(?unnamed:4)
                           LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                           STRING('Total Quantity Ordered:'),AT(115,115),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14),AT(1635,115),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('EXCHANGES ORDERED'),AT(4896,52),USE(?String3),TRN,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity'),AT(4073,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Number'),AT(4563,1979),USE(?JobNumber),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Activation Date'),AT(6563,1979),USE(?ActivationDate),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Price'),AT(6094,1979),USE(?strPrice),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Manufacturer'),AT(156,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2063,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('ExchangeOrders')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'ExchangeOrd'
  If PrintOption(PreviewReq,glo:ExportReport,'Exchange Orders') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDWEBPR.Open
  Relate:DEFAULTS.Open
  Relate:EXCHORNO.Open
  Relate:GENSHORT.Open
  Relate:MODELNUM.Open
  Access:STOAUDIT.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = BYTES(ORDWEBPR)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDWEBPR,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(ModelQueue)
      
      !Set Stock File (Stock Type Key)
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        If glo:WebJob
            tmp:AccountNumber   = Clarionet:Global.Param2
        Else !glo:WebJob.
            tmp:AccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
        End !glo:WebJob.
        
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = tmp:AccountNumber
        IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
           !Found
           address:User_Name = tra:company_name
           address:Address_Line1 = tra:address_line1
           address:Address_Line2 = tra:address_line2
           address:Address_Line3 = tra:address_line3
           address:Post_code = tra:postcode
           address:Telephone_Number = tra:telephone_number
           address:Fax_No = tra:Fax_Number
           address:Email = tra:EmailAddress
        ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        SETTARGET()
        !Loop not here any more
        Loop x# = 1 To Records(ModelQueue)
            Get(ModelQueue,x#)
            tmp:Manufacturer    = ModelQueue.Manufacturer
            tmp:ModelNumber     = ModelQueue.ModelNumber
            tmp:Quantity        = ModelQueue.Quantity
            Total_No_Of_Lines += ModelQueue.Quantity
        
            Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
            mod:Manufacturer = ModelQueue.Manufacturer
            mod:Model_Number = ModelQueue.ModelNumber
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
                locExchangeCost = mod:ExchReplaceValue * ModelQueue.Quantity
            ELSE
                locExchangeCost = 0
            END
        
            Print(rpt:Detail)
        End !x# = 1 To Records(ModelQueue)
        LocalResponse = RequestCompleted
        BREAK
        
        
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDWEBPR,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHORNO.Close
    Relate:GENSHORT.Close
    Relate:MODELNUM.Close
    Relate:ORDWEBPR.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening The Report
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Total_No_Of_Lines = 0
  
  If func:JobNumber <> 0
      SetTarget(Report)
      ?tmp:JobNumber{prop:Hide} = 0
      ?tmp:ActivationDate{prop:Hide} = 0
      ?JobNumber{prop:Hide} = 0
      ?ActivationDate{prop:Hide} = 0
      SetTarget()
  End !func:JobNumber <> 0
  tmp:JobNUmber = func:JobNumber
  tmp:ActivationDate = func:ActivationDate
  
  ! #12127 Show order number if from a "stock" exchange order. (Bryan: 02/07/2011)
  IF (fOrderNumber > 0)
      Access:EXCHORNO.Clearkey(eno:RecordNumberKey)
      eno:RecordNumber = fORderNumber
      IF (Access:EXCHORNO.TryFetch(eno:RecordNumberKey) = Level:Benign)
          SETTARGET(Report)
          ?strOrderNumber{prop:Hide} = 0
          ?eno:RecordNumber{prop:Hide} = 0
          ?eno:DateCreated{prop:Hide} = 0
  
          !tb12539 - J - 14/02/14 - Time is now also wanted and is added to report
          ?strOrderDate{prop:hide} = 0
          ?eno:TimeCreated{prop:Hide} = 0
          !END tb12539
  
          ?strPrice{prop:Hide} = 0
          ?locExchangeCost{prop:hide} = 0
          SETTARGET()
      END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Exchange Unit Order'
  END
  Report{Prop:Preview} = PrintPreviewImage







