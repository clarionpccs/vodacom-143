

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03014.INC'),ONCE        !Local module procedure declarations
                     END


SMS_Response_Report PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
AutoMode             BYTE
Start_Date           DATE
End_Date             DATE
TagIcon              STRING(1)
EndBit               CSTRING(255)
TempExportPath       CSTRING(255)
FinalExportPath      CSTRING(255)
X                    LONG
ReportCancelled      BYTE
NextCRLF             LONG
tmp:InFault          STRING(30)
tmp:OutFault         STRING(30)
tmp:InFaultDescription STRING(60)
tmp:OutFaultDescription STRING(60)
Tmp:EmailAddress     STRING(50)
FullEmailAddress     STRING(255)
CurrentRow           LONG
Local                CLASS
DrawBox              Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
                     END
LocalFilename   cstring(500),STATIC


ExportFile    File,Driver('BASIC'),Pre(exx),Name(LocalFilename),Create,Bindable,Thread
Record                  Record
DateTime                String(30)
MSISND                  String(30)
Manufacturer            String(30)
Model                   String(30)
Message                 String(160)
JobNumber               String(30)
InFault                 String(100)
Outfault                String(100)
JobNotes                String(255)
FranchiseName           String(30)
                        End
                    End
BRW11::View:Browse   VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:BranchIdentification)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
TagIcon                LIKE(TagIcon)                  !List box control field - type derived from local data
TagIcon_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:BranchIdentification LIKE(tra:BranchIdentification) !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,4,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(165,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('SMS Response Report'),AT(169,71),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,82,351,248),USE(?PanelMain),FILL(09A6A7CH)
                       BUTTON,AT(416,100),USE(?PopCalendar),FLAT,ICON('lookupp.jpg')
                       PROMPT('Start Date:'),AT(227,106),USE(?Start_Date:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(277,106,124,10),USE(Start_Date),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('End Date:'),AT(227,126),USE(?End_Date:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(277,126,124,10),USE(End_Date)
                       GROUP,AT(177,135,327,191),USE(?GroupChoice)
                         LIST,AT(227,142,225,152),USE(?List),IMM,MSG('Browsing Records'),FORMAT('10L|MI@s1@66L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@0L(2)|M@s2@'),FROM(Queue:Browse)
                         BUTTON,AT(236,302),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                         BUTTON,AT(306,302),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                         BUTTON('sho&W tags'),AT(336,38,60,10),USE(?DASSHOWTAG),HIDE
                         BUTTON,AT(376,302),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         BUTTON('&Rev tags'),AT(405,298,1,1),USE(?DASREVTAG),HIDE
                       END
                       BUTTON,AT(416,122),USE(?PopCalendar:2),FLAT,ICON('lookupp.jpg')
                       PANEL,AT(165,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(451,332),USE(?ButtonCancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(376,332),USE(?ButtonOK),TRN,FLAT,ICON('okp.jpg')
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
MyExcel13            Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW11                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
Expo MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW11.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    TagIcon = '*'
  ELSE
    DELETE(glo:Queue)
    TagIcon = ''
  END
    Queue:Browse.TagIcon = TagIcon
  IF (TagIcon = '*')
    Queue:Browse.TagIcon_Icon = 2
  ELSE
    Queue:Browse.TagIcon_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::12:QUEUE = glo:Queue
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
TheOutput       Routine

    ReportCancelled = false     !Just to make sure
    !progress window only being used here
    if not AutoMode
        Expo.OpenProgressWindow()
        
        Expo.UpdateProgressText()
        
        Expo.UpdateProgressText('Report Started: ' & FORMAT(Expo.StartDate,@d06) & |
            ' ' & FORMAT(Expo.StartTime,@t01))
        
        Expo.UpdateProgressText()
    END



    TempExportPath = GetTempFolder()

    IF (TempExportPath = '')
        Case Missive('An error occurred creating the report.'&|
            '|Temp folder not set up'&|
            '|Please ty again.','ServiceBase',|
            'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT            
    END

    FinalExportPath = SetReportsFolder('ServiceBase Export','SMS Response Report',glo:WebJob)
    IF (FinalExportPath = '')
        Case Missive('An error occurred creating the report.'&|
            '|destination folder not set up'&|
            '|Please ty again.','ServiceBase',|
            'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT            
    END



    if glo:WebJob = 0
        !create the excel file and put in some titles
        if not AutoMode
            Expo.UpdateProgressText('',1)
            Expo.UpdateProgressText('Generating Excel file in process')
        END

        do CreateExcelFile
    END

    !message('Records in queue are '&records(Glo:Queue))

    Loop x = 1 to records(glo:Queue)

        get(glo:Queue,X)
        if error() then break.

        !glo:Queue.GLO:Pointer holds tra:Account_Number
        Access:TradeAcc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = glo:Queue.GLO:Pointer
        if access:TradeAcc.fetch(tra:Account_Number_Key)
            cycle
        END

        Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
        tra2:Account_Number = glo:Queue.GLO:Pointer
        if access:TradeAc2.fetch(TRA2:KeyAccountNumber)
            TRA2:SMS_ReportEmail = ''       !just to be sure it doesn't get used
        END



        if not AutoMode
            IF (Expo.CancelPressed = 2)
                Expo.UpdateProgressText('===============')
                Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                    ' ' & FORMAT(clock(),@t1))
                Expo.FinishProgress()
                ReportCancelled = true
                break
            ELSE
                Expo.UpdateProgressText('Adding: ' & CLIP(glo:Pointer))
            END
        END

        !Always create the csv files - these will be sent to the franchise
        !create and open the csv file
        LocalFilename = clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv'
        if exists(LocalFilename) then remove(LocalFilename).
        Create(ExportFile)
        open(ExportFile)

        exx:DateTime        = 'DATE TIME'
        exx:MSISND          = 'MSISDN'
        exx:Manufacturer    = 'MANUFACTURER'
        exx:Model           = 'MODEL NUMBER'
        exx:Message         = 'TEXT RECEIVED'
        exx:JobNumber       = 'JOB NUMBER'
        exx:InFault         = 'IN FAULT'
        exx:Outfault        = 'OUT FAULT'
        exx:JobNotes        = 'NOTES'
        exx:FranchiseName   = 'FRANCHISE'
        Add(ExportFile)

        If glo:WebJob = 0 then
            !Start a new sheet in the excel file
            MyExcel13.InsertWorksheet()
            MyExcel13.RenameWorkSheet(tra:Company_Name)
            MyExcel13.SelectWorkSheet(tra:Company_Name)

            !header
            MyExcel13.WriteToCell('SMS Response Report'     ,'A1')
            MyExcel13.WriteToCell('SMS Date From:'          ,'A3')
            MyExcel13.WriteToCell(Format(Start_Date,@d06)   ,'B3')
            MyExcel13.WriteToCell('SMS Date To:'            ,'A4')
            MyExcel13.WriteToCell(Format(End_Date,@d06)     ,'B4')
            MyExcel13.WriteToCell('Date Report Created:'    ,'A5')
            MyExcel13.WriteToCell(Format(today(),@D06)      ,'B5')

            !bold font and size for A1
            MyExcel13.SetCellFontSize(14,'A1')
            MyExcel13.SetCellFontStyle('Bold','A1')

            !colouring
            Local.DrawBox('A1', 'J1', 'A1', 'J1', color:Silver)
            Local.DrawBox('A3', 'J3', 'A6', 'J6', color:Silver)
            Local.DrawBox('A8', 'J8', 'A9', 'J9', color:Silver)


            MyExcel13.WriteToCell('DATE TIME'       ,'A9')
            MyExcel13.WriteToCell('MSISDN'          ,'B9')
            MyExcel13.WriteToCell('MANUFACTURER'    ,'C9')
            MyExcel13.WriteToCell('MODEL NUMBER'    ,'D9')
            MyExcel13.WriteToCell('TEXT RECEIVED'   ,'E9')
            MyExcel13.WriteToCell('JOB NUMBER'      ,'F9')
            MyExcel13.WriteToCell('IN FAULT'        ,'G9')
            MyExcel13.WriteToCell('OUT FAULT'       ,'H9')
            MyExcel13.WriteToCell('NOTES'           ,'I9')
            MyExcel13.WriteToCell('FRANCHISE'       ,'J9')

            MyExcel13.SetColumnWidth('A','','24')   !Datetime
            MyExcel13.SetColumnWidth('B','','15')   !MSISDN
            MyExcel13.SetColumnWidth('C','','30')   !Manufacturer
            MyExcel13.SetColumnWidth('D','','30')   !model
            MyExcel13.SetColumnWidth('E','','100')  !Text
            MyExcel13.SetColumnWidth('F','','20')   !Job number
            MyExcel13.SetColumnWidth('G','','20')   !infault
            MyExcel13.SetColumnWidth('H','','45')   !outfault
            MyExcel13.SetColumnWidth('I','','100')  !notes !
            MyExcel13.SetColumnWidth('J','','70')  !Franchise name - acutally useing the subaccount company name

            CurrentRow = 10

        END !if glo:Webjog

        !having set up the header for the franchise - we need to include all the sub accounts.
        Access:Subtracc.clearkey(sub:Main_Account_Key)
        sub:Main_Account_Number = tra:Account_Number
        set(sub:Main_Account_Key,sub:Main_Account_Key)
        Loop
            if access:Subtracc.next() then break.
            if sub:Main_Account_Number <> tra:Account_Number then break.

            Access:SMSRecvd.clearkey(SMR:KeyAccount_Date_Time)
            SMR:AccountNumber = sub:Account_Number
            SMR:DateReceived  = Start_Date
            SMR:TimeReceived  = 0
            set(SMR:KeyAccount_Date_Time,SMR:KeyAccount_Date_Time)
            Loop

                if access:SMSRecvd.next() then break.
                if SMR:AccountNumber <> sub:Account_Number then
                    break
                END
                if SMR:DateReceived > End_Date then
                    break
                END
                
                !found one
                Access:jobs.clearkey(job:Ref_Number_Key)
                job:Ref_Number = SMR:Job_Ref_Number
                if access:Jobs.fetch(job:Ref_Number_Key) then
                    cycle
                END

                !jobsnotes
                Access:jobnotes.clearkey(jbn:RefNumberKey)
                jbn:RefNumber = SMR:Job_ref_number
                if access:jobnotes.fetch(jbn:RefNumberKey) then
                    !error - make sure this is left blank
                    jbn:Fault_Description = ''
                END

                !now for the infault and outfault
                Do GetFaults
                !always write a line to the csv
                Do WriteToCSV

                !if fat client then also write to the excel file
                if glo:webjob = 0 then
                    Do WriteToExcel
                END

            END !look through SMSRecvd by account and datetime
        END !loop through sub accounts
        Close(ExportFile)

    END !loop through records in glo:Queue

    !message('Done export')

    if ReportCancelled = true then
        !message('Report cancelled')
        if not AutoMode
            Expo.UpdateProgressText('',1)
            Expo.UpdateProgressText('Clearing temp files')
        END

        if glo:webjob = 0 then
            !finish off the xls file properly
            MyExcel13.saveas(clip(TempExportPath)&'Summary'&clip(EndBit)&'.xls')
            MyExcel13.closeWorkbook()
        END
    Else
        !message('Not cancelled')
        if not AutoMode
            Expo.UpdateProgressText('',1)
            Expo.UpdateProgressText('Finalising reports')
        END

        if glo:Webjob = 0
            !Start a new sheet in the excel file
            MyExcel13.InsertWorksheet()
            MyExcel13.RenameWorkSheet('Summary')
            MyExcel13.SelectWorkSheet('Summary')

            !header
            MyExcel13.WriteToCell('SMS Response Report'     ,'A1')
            MyExcel13.WriteToCell('SMS Date From:'          ,'A3')
            MyExcel13.WriteToCell(Format(Start_Date,@d06)   ,'B3')
            MyExcel13.WriteToCell('SMS Date To:'            ,'A4')
            MyExcel13.WriteToCell(Format(End_Date,@d06)     ,'B4')
            MyExcel13.WriteToCell('Date Report Created:'    ,'A5')
            MyExcel13.WriteToCell(Format(today(),@D06)      ,'B5')

            !bold font and size for A1
            MyExcel13.SetCellFontSize(14,'A1')
            MyExcel13.SetCellFontStyle('Bold','A1')

            !colouring
            Local.DrawBox('A1', 'B1', 'A1', 'B1', color:Silver)
            Local.DrawBox('A3', 'B3', 'A6', 'B6', color:Silver)
            Local.DrawBox('A8', 'B8', 'A9', 'B9', color:Silver)

            MyExcel13.WriteToCell('Franchises'      ,'A9')

            MyExcel13.SetColumnWidth('A','','24')   !Datetime
            MyExcel13.SetColumnWidth('B','','15')   !MSISDN


            CurrentRow = 10

            Loop x =  records(glo:Queue) to 1 by -1

                get(glo:Queue,X)
                if error() then break.

                !glo:Queue.GLO:Pointer holds tra:Account_Number
                Access:TradeAcc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = glo:Queue.GLO:Pointer
                if access:TradeAcc.fetch(tra:Account_Number_Key)
                    cycle
                END

                MyExcel13.WriteToCell(tra:Company_Name,'A' & CurrentRow)
                CurrentRow += 1

            end !loop through queue

            !remove the first sheet not wanted
            MyExcel13.SelectWorkSheet('ExSummary')
            MyExcel13.DeleteWorkSheet()

            !reselect the first sheet so it is shown first
            MyExcel13.SelectWorkSheet('Summary')


            !Exit nicely
            MyExcel13.saveas(clip(TempExportPath)&clip(EndBit)&'.xls')
            MyExcel13.closeWorkbook()
        END !if Excell was in use

        !ensure the receiving directory does not have any identically named files
        
        Loop x = 1 to records(glo:Queue)

            get(glo:Queue,X)
            if error() then break.
            if exists(clip(FinalExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv') then
                remove(clip(FinalExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv')
            END
            

        END !loop though global queue

        if exists(clip(FinalExportPath)&clip(EndBit)&'.xls') then remove(clip(FinalExportPath)&clip(EndBit)&'.xls').

        !Now we can Copy all files to the final directory
        !message('About to copy files')
        Loop x = 1 to records(glo:Queue)

            get(glo:Queue,X)
            if error() then break.
            !message('Should be saving, '&clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv')
            copy(clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv',clip(FinalExportPath))

            !there will only be one csv if this is a remote call so
            !message('Should be sending to client, if glo:webjob = '&clip(glo:Webjob))
            if glo:Webjob then
                !message('sending '&clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv')
                SendFileToClient(clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv')
            END

        END !loop though global queue

        !and move the summary
        if exists(clip(TempExportPath)&clip(EndBit)&'.xls')
            copy(clip(TempExportPath)&clip(EndBit)&'.xls',Clip(FinalExportPath))
        END
    END !if cancelled

    !message('Remove temp files')
    !and remove all temp files
    Loop x = 1 to records(glo:Queue)

        get(glo:Queue,X)
        if error() then break.

        if exists(clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv') then
            remove(clip(TempExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv')
        END

    END !loop though global queue

    !and move the summary
    if exists(clip(TempExportPath)&clip(EndBit)&'.xls') then
        remove(clip(TempExportPath)&clip(EndBit)&'.xls')
    END


    EXIT
AutoSetup       Routine

    AutoMode = true               
    Start_Date = today()-1
    End_Date   = today()-1


    !build the queue from the defaults
    Access:Tradeacc.clearkey(tra:Account_Number_Key)
    Set(tra:Account_Number_Key)

    Loop

        If Access:TRADEACC.NEXT() then break.
        
        If GETINI('SMSRECEIVED',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            !this one is included
            glo:Queue.GLO:Pointer = tra:Account_Number
            Add(Glo:Queue)
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

    END !loop
    !message('After loop have records = '&clip(records(Glo:Queue))

    exit
WriteToCSV      Routine

    !Ready to output
    exx:DateTime        = format(SMR:DateReceived,@D06)&' '&format(SMR:TimeReceived,@t1)
    exx:MSISND          = SMR:MSISDN
    exx:Manufacturer    = job:Manufacturer
    exx:Model           = job:Model_Number
    exx:Message         = SMR:TextReceived
    exx:JobNumber       = stripComma(clip(job:Ref_Number))
    exx:InFault         = clip(tmp:InFault)&'-'&clip(tmp:InFaultDescription)
    exx:Outfault        = clip(tmp:OutFault)&'-'&clip(tmp:OutFaultDescription)
    exx:JobNotes        = StripReturn(stripComma(clip(jbn:Fault_Description)))
    exx:FranchiseName   = tra:Company_Name          !could be sub:Company_Name, but franchise required

    Add(ExportFile)

    EXIT
CreateExcelFile     routine

    If MyExcel13.Init(0,0) = 0
        Case Missive('An error has occurred finding your Excel document.'&|
            '<13,10>'&|
            '<13,10>Please quit and try again.','ServiceBase 3g',|
            'mstop.jpg','/OK')
        Of 1 ! OK Button
        End ! Case Missive
        Exit
    End !If MyExcel13.Init(0,0,1) = 0

    MyExcel13.NewWorkBook()
    MyExcel13.RenameWorkSheet('ExSummary')
    !this sheet will be deleted at the end

    EXIT

WriteToExcel        Routine


    MyExcel13.WriteToCell(format(SMR:DateReceived,@D06)&' '&format(SMR:TimeReceived,@t1)   ,'A' & CurrentRow)
    MyExcel13.SetCellNumberFormat(oix:NumberFormatText,,0,,'B'&CurrentRow)      !should format next cell as text
    MyExcel13.WriteToCell(SMR:MSISDN                                                       ,'B' & CurrentRow)
    MyExcel13.WriteToCell(job:Manufacturer                                                 ,'C' & CurrentRow)
    MyExcel13.WriteToCell(job:Model_Number                                                 ,'D' & CurrentRow)
    MyExcel13.WriteToCell(SMR:TextReceived                                                 ,'E' & CurrentRow)
    MyExcel13.WriteToCell(stripComma(clip(job:Ref_Number))                                 ,'F' & CurrentRow)
    MyExcel13.WriteToCell(clip(tmp:InFault)&'-'&clip(tmp:InFaultDescription)               ,'G' & CurrentRow)
    MyExcel13.WriteToCell(clip(tmp:OutFault)&'-'&clip(tmp:OutFaultDescription)             ,'H' & CurrentRow)
    MyExcel13.WriteToCell(StripReturn(stripComma(clip(jbn:Fault_Description)))             ,'I' & CurrentRow)
    MyExcel13.WriteToCell(tra:Company_Name                                                 ,'J' & CurrentRow)   !could be sub:Company_Name, but franchise requested

    CurrentRow += 1

    EXIT
GetFaults       Routine

    tmp:InFault = ''
    tmp:InFaultDescription = ''
    Access:MANFAULT.ClearKey(maf:InFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:InFault      = 1
    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
        !Found
        
        Case maf:Field_Number
            Of 1
                tmp:Infault = job:Fault_Code1
            Of 2
                tmp:Infault = job:Fault_Code2
            Of 3
                tmp:Infault = job:Fault_Code3
            Of 4
                tmp:Infault = job:Fault_Code4
            Of 5
                tmp:Infault = job:Fault_Code5
            Of 6
                tmp:Infault = job:Fault_Code6
            OF 7
                tmp:Infault = job:Fault_Code7
            Of 8
                tmp:Infault = job:Fault_Code8
            Of 9
                tmp:Infault = job:Fault_Code9
            Of 10
                tmp:Infault = job:Fault_Code10
            Of 11
                tmp:Infault = job:Fault_Code11
            Of 12
                tmp:Infault = job:Fault_Code12
        End !Case maf:Field_Number
        
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        mfo:Field        = tmp:Infault
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Found
            tmp:InFaultDescription  = mfo:Description
        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
    
    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign

    Do GetOutFault

    EXIT
GetOutFault        Routine

!this is a complex bit - so this is borrowed from Calculate_Billing in sbj02.

Data
local:CFaultCode        String(30)
local:CIndex            Byte(0)
local:CFaultDescription String(60)

local:WFaultCOde        String(30)
local:WIndex            Byte(0)
local:WFaultDescription String(60)

local:KeyRepair         Long()
Code

    local:CFaultCode = ''
    local:CFaultDescription = ''
    local:WFaultCOde = ''
    local:WFaultDescription = ''

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

        If man:UseInvTextForFaults = True
            If man:AutoRepairType = True
                ! Get the main out fault (DBH: 23/11/2007)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                    ! Check the out faults list (DBH: 23/11/2007)
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber = job:Ref_Number
                    Set(joo:JobNumberKey,joo:JobNumberKey)
                    Loop
                        If Access:JOBOUTFL.Next()
                            Break
                        End ! If Access:JOBOUTFL.Next()
                        If joo:JobNumber <> job:Ref_Number
                            Break
                        End ! If joo:JobNumber <> job:Ref_Number

                        ! Get the lookup details (e.g. index and repair types) (DBH: 23/11/2007)
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field = joo:FaultCode
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            ! Is there a repair type? (DBH: 23/11/2007)
                            If mfo:RepairType <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:CIndex
                                    local:CFaultCode = mfo:Field
                                    local:CIndex = mfo:ImportanceLevel
                                    
                                    local:CFaultDescription = mfo:Description
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairType <> ''
                            If mfo:RepairTypeWarranty <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:WIndex
                                    local:WFaultCode = mfo:Field
                                    local:WIndex = mfo:ImportanceLevel
                                    
                                    local:WFaultDescription = mfo:Description
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairTypeWarranty <> ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End ! Loop

                    ! Inserting (DBH 30/04/2008) # 9723 - Is there a "Key Repair" fault code? If so, that is counted as the OUT Fault
                    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
                    map:Manufacturer = job:Manufacturer
                    map:KeyRepair = 1
                    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        ! Found
                        local:KeyRepair = map:Field_Number
                    Else ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        local:KeyRepair = 0
                    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                    ! End (DBH 30/04/2008) #9723

! Changing (DBH 21/05/2008) # 9723 - Can be more than one part out fault
!                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
!                    map:Manufacturer = job:Manufacturer
!                    map:MainFault = 1
!                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
! to (DBH 21/05/2008) # 9723
                    ! Is the out fault held on the parts. If so, check for the highest repair index there too (DBH: 23/11/2007)
                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
                    map:Manufacturer = job:Manufacturer
                    map:MainFault = 1
                    Set(map:MainFaultKey,map:MainFaultKey)
                    Loop ! Begin Loop
                        If Access:MANFAUPA.Next()
                            Break
                        End ! If Access:MANFAUPA.Next()
                        If map:Manufacturer <> job:Manufacturer
                            Break
                        End ! If map:Manufacturer <> job:Manufacturer
                        If map:MainFault <> 1
                            Break
                        End ! If map:MainFault <> 1

! End (DBH 21/05/2008) #9723
                        ! Check the Warranty Parts (DBH: 23/11/2007)
                        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = job:Ref_Number
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.Next()
                                Break
                            End ! If Access:WARPARTS.Next()
                            If wpr:Ref_Number <> job:Ref_Number
                                Break
                            End ! If wpr:Ref_Number <> job:Ref_Number

                            ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                            Case local:KeyRepair
                            Of 1
                                If wpr:Fault_Code1 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code1 <> 1
                            Of 2
                                If wpr:Fault_Code2 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code2 <> 1
                            Of 3
                                If wpr:Fault_Code3 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 4
                                If wpr:Fault_Code4 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 5
                                If wpr:Fault_Code5 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 6
                                If wpr:Fault_Code6 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 7
                                If wpr:Fault_Code7 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 8
                                If wpr:Fault_Code8 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 9
                                If wpr:Fault_Code9 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 10
                                If wpr:Fault_Code10 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 11
                                If wpr:Fault_Code11 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 12
                                If wpr:Fault_Code12 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            End ! Case local:KeyRepair
                            ! End (DBH 30/04/2008) #9723

                            ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = job:Manufacturer
                            mfo:Field_Number = maf:Field_NUmber
                            Case map:Field_Number
                            Of 1
                                mfo:Field = wpr:Fault_Code1
                            Of 2
                                mfo:Field = wpr:Fault_Code2
                            Of 3
                                mfo:Field = wpr:Fault_Code3
                            Of 4
                                mfo:Field = wpr:Fault_Code4
                            Of 5
                                mfo:Field = wpr:Fault_Code5
                            Of 6
                                mfo:Field = wpr:Fault_Code6
                            Of 7
                                mfo:Field = wpr:Fault_Code7
                            Of 8
                                mfo:Field = wpr:Fault_Code8
                            Of 9
                                mfo:Field = wpr:Fault_Code9
                            Of 10
                                mfo:Field = wpr:Fault_Code10
                            Of 11
                                mfo:Field = wpr:Fault_Code11
                            Of 12
                                mfo:Field = wpr:Fault_Code12
                            End ! Case map:Field_Number
                            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                ! Is there a repair type? (DBH: 23/11/2007)
                                If mfo:RepairType <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:CIndex
                                        local:CFaultCode = mfo:Field
                                        local:CIndex = mfo:ImportanceLevel
                                        
                                        local:CFaultDescription = mfo:Description
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairType <> ''
                                If mfo:RepairTypeWarranty <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:WIndex
                                        local:WFaultCode = mfo:Field
                                        local:WIndex = mfo:ImportanceLevel
                                        
                                        local:WFaultDescription = mfo:Description
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairTypeWarranty <> ''
                            End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                        End ! Loop

                        ! Check the estimate parts (DBH: 23/11/2007)
                        If job:Estimate = 'YES' And job:Chargeable_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                            epr:Ref_Number = job:Ref_Number
                            Set(epr:Part_Number_Key,epr:Part_Number_Key)
                            Loop
                                If Access:ESTPARTS.Next()
                                    Break
                                End ! If Access:ESTPARTS.Next()
                                If epr:Ref_Number <> job:Ref_Number
                                    Break
                                End ! If epr:Ref_Number <> job:Ref_Number

                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If epr:Fault_Code1 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code1 <> 1
                                Of 2
                                    If epr:Fault_Code2 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code2 <> 1
                                Of 3
                                    If epr:Fault_Code3 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 4
                                    If epr:Fault_Code4 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 5
                                    If epr:Fault_Code5 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 6
                                    If epr:Fault_Code6 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 7
                                    If epr:Fault_Code7 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 8
                                    If epr:Fault_Code8 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 9
                                    If epr:Fault_Code9 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 10
                                    If epr:Fault_Code10 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 11
                                    If epr:Fault_Code11 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 12
                                    If epr:Fault_Code12 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = job:Manufacturer
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = epr:Fault_Code1
                                Of 2
                                    mfo:Field = epr:Fault_Code2
                                Of 3
                                    mfo:Field = epr:Fault_Code3
                                Of 4
                                    mfo:Field = epr:Fault_Code4
                                Of 5
                                    mfo:Field = epr:Fault_Code5
                                Of 6
                                    mfo:Field = epr:Fault_Code6
                                Of 7
                                    mfo:Field = epr:Fault_Code7
                                Of 8
                                    mfo:Field = epr:Fault_Code8
                                Of 9
                                    mfo:Field = epr:Fault_Code9
                                Of 10
                                    mfo:Field = epr:Fault_Code10
                                Of 11
                                    mfo:Field = epr:Fault_Code11
                                Of 12
                                    mfo:Field = epr:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            
                                            local:CFaultDescription = mfo:Description
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            
                                            local:WFaultDescription = mfo:Description
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                            End ! Loop
                        Else ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:PARTS.Clearkey(par:Part_Number_Key)
                            par:Ref_Number = job:Ref_Number
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.Next()
                                    Break
                                End ! If Access:PARTS.Next()
                                If par:Ref_Number <> job:Ref_Number
                                    Break
                                End ! If par:Ref_Number <> job:Ref_Number
                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If par:Fault_Code1 <> 1
                                        Cycle
                                    End ! If par:Fault_Code1 <> 1
                                Of 2
                                    If par:Fault_Code2 <> 1
                                        Cycle
                                    End ! If par:Fault_Code2 <> 1
                                Of 3
                                    If par:Fault_Code3 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 4
                                    If par:Fault_Code4 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 5
                                    If par:Fault_Code5 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 6
                                    If par:Fault_Code6 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 7
                                    If par:Fault_Code7 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 8
                                    If par:Fault_Code8 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 9
                                    If par:Fault_Code9 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 10
                                    If par:Fault_Code10 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 11
                                    If par:Fault_Code11 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 12
                                    If par:Fault_Code12 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = job:Manufacturer
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = par:Fault_Code1
                                Of 2
                                    mfo:Field = par:Fault_Code2
                                Of 3
                                    mfo:Field = par:Fault_Code3
                                Of 4
                                    mfo:Field = par:Fault_Code4
                                Of 5
                                    mfo:Field = par:Fault_Code5
                                Of 6
                                    mfo:Field = par:Fault_Code6
                                Of 7
                                    mfo:Field = par:Fault_Code7
                                Of 8
                                    mfo:Field = par:Fault_Code8
                                Of 9
                                    mfo:Field = par:Fault_Code9
                                Of 10
                                    mfo:Field = par:Fault_Code10
                                Of 11
                                    mfo:Field = par:Fault_Code11
                                Of 12
                                    mfo:Field = par:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            
                                            local:CFaultDescription = mfo:Description
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            
                                            local:WFaultDescription = mfo:Description
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign

                            End ! Loop
                        End ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            End ! If man:AutoRepairType = True
        End ! If man:UseInvTextForFaults = True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign


    !use the warranty out fault unless it does not exist
    if Local:WfaultCode = '' then
        tmp:OutFault = local:CFaultCode                 !Chargeable Fault code
        tmp:OutFaultDescription = local:CFaultDescription
    ELSE
        tmp:OutFault = local:WFaultCode      !warrentry fault code
        tmp:OutFaultDescription = local:WFaultDescription
    END
EmailEveryone       Routine

    !emails cannot be generated unless we are in fat client mode JC
    if glo:Webjob <> 0 then exit.

    !Emails will need the default sending address
    if de2:DefaultFromEmail = '' then
        !Say why we are not doing this
        if not AutoMode
            Expo.UpdateProgressText('',1)
            Expo.UpdateProgressText('Cannot generate Emails, no default "FROM" address')
        END !if not AutoMode

        exit !need the default from address

    end     !if default from mail is blank

    if not AutoMode
        Expo.UpdateProgressText('',1)
        Expo.UpdateProgressText('Generating Emails')
    END !if not AutoMode

    !have a number of .csv files and the .xls file to email to people
    Loop x = 1 to records(glo:Queue)

        get(glo:Queue,X)
        if error() then break.

        !glo:Queue.GLO:Pointer holds tra:Account_Number
        Access:TradeAcc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = glo:Queue.GLO:Pointer
        if access:TradeAcc.fetch(tra:Account_Number_Key)
            cycle
        END

        Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
        tra2:Account_Number = glo:Queue.GLO:Pointer
        if access:TradeAc2.fetch(TRA2:KeyAccountNumber)
            TRA2:SMS_ReportEmail = ''
        END


        !create and open the csv file
        LocalFilename = clip(FinalExportPath)&clip(EndBit)&' '&clip(glo:Queue.GLO:Pointer)&'.csv'

        if clip(tra:EmailAddress) & clip(tra2:SMS_ReportEmail) = '' then

            if not AutoMode
                Expo.UpdateProgressText('',1)
                Expo.UpdateProgressText('Cannot generate '&clip(glo:Queue.GLO:Pointer)&' Emails, no SMS email address')
            END !if not AutoMode

            Remove(localFilename)
            cycle
        END

        if exists(LocalFilename) then
            !create an email to each of the SMSreponse receipients
            if clip(Tra:EmailAddress) <> '' then
                Tmp:EmailAddress = tra:EmailAddress
                Do SingleEmailSend
            END
            
            if clip(tra2:SMS_ReportEmail) <> '' then
                FullEmailAddress = tra2:SMS_ReportEmail      !this is not empty but may contain up to five addresses
                Do MultipleEmailSend
            END
        END

    END !loop though global queue

    !now to find the AA20 account
    Access:Tradeacc.clearkey(tra:Account_Number_Key)
    tra:Account_Number = 'AA20'
    if access:TradeAcc.fetch(tra:Account_Number_Key)
        !error - eh?
    ELSE
        Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
        tra2:Account_Number = tra:Account_Number
        if access:TradeAc2.fetch(TRA2:KeyAccountNumber)
            TRA2:SMS_ReportEmail = ''
        END

        !swap to sending the xls to this account
        LocalFilename = clip(FinalExportPath)&clip(EndBit)&'.xls'

        if clip(Tra:EmailAddress) <> '' then
            Tmp:EmailAddress = tra:EmailAddress
            Do SingleEmailSend
        ELSE
            if not AutoMode
                Expo.UpdateProgressText('',1)
                Expo.UpdateProgressText('Cannot generate AA20 Emails, no email address')
            END !if not AutoMode

        END

        if clip(tra2:SMS_ReportEmail) <> '' then

            FullEmailAddress = tra2:SMS_ReportEmail
            Do MultipleEmailSend
        ELSE
            if not AutoMode
                Expo.UpdateProgressText('',1)
                Expo.UpdateProgressText('Cannot generate AA20 SMS Emails, no SMS email address')
            END !if not AutoMode
        END !if email exists

    END


    EXIT


MultipleEmailSend       Routine

    Loop

        NextCRLF = instring('<13,10>',FullEmailAddress,1,1)
        if NextCRLF = 0 then
            Tmp:EmailAddress = clip(FullEmailAddress)
            Do SingleEmailSend
            break
        END

        Tmp:EmailAddress = FullEmailAddress[ 1 : NextCRLF - 1 ]
        Do SingleEmailSend

        !now cut off the first bit
        if NextCRLF +2 >= len(clip(FullEmailAddress)) then break.

        FullEmailAddress = FullEmailAddress[ NextCRLF + 2 : len(clip(FullEmailAddress)) ]

    END


    EXIT

SingleEmailSend         Routine

    !message('TESTING:|Sending email to ['&clip(tmp:EmailAddress)&']')
    !tmp:EmailAddress = 'h.jackson@pccsuk.com'
    if clip(tmp:EmailAddress) = '' then exit.       !if they have a carriage return at the end of their address list this happens


    SendEmail(clip(de2:DefaultFromEmail),clip(Tmp:EmailAddress),|
                'SMS Response Report for '&format(Start_Date,@d06) & ' to '& format(End_Date,@d06),|
                '','',clip(LocalFilename),|
                'SMS Response Report for '&format(Start_Date,@d06) & ' to '& format(End_Date,@d06) &' is attached to this email')

    EXIT

!    SendEmail PROCEDURE (string pEmailFrom, string pEmailTo,
!                    string pEmailSubject,
!                    string pEmailCC,string pEmailBcc, string pEmailFileList,
!                    string pEmailMessageText) 
!
!============================================================
!old method replaced - but kept in case needed later JC
!    Access:SMSMail.primerecord()
!    sms:SendToSMS = 'F'
!    sms:SendToEmail = 'T'
!    sms:DateInserted = today()
!    sms:TimeInserted = clock()
!    sms:PathToEstimate = clip(LocalFilename)
!    sms:EmailSubject = 'SMS Response Report for '&format(Start_Date,@d06) & ' to '& format(End_Date,@d06)
!    sms:MSG = 'SMS Response Report for '&format(Start_Date,@d06) & ' to '& format(End_Date,@d06) &' is attached to this email'
!    sms:EmailAddress = Tmp:EmailAddress
!    Access:SMSMail.update()


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020776'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SMS_Response_Report')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:JOBNOTES.Open
  Relate:SMSMAIL.Open
  Relate:SMSRECVD.Open
  Relate:TRADEAC2.Open
  Access:JOBS.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:JOBOUTFL.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  BRW11.Init(?List,Queue:Browse.ViewPosition,BRW11::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !set up the system
  
  !for the temporary csv files
  EndBit = 'SMS Response Report '&format(Today(),@d12)
  !SMS Response Report yyyymmdd.xls
  
  Set(Default2)
  if access:Default2.next()
      de2:DefaultFromEmail = ''       !just to be sure
  END
  
  !Check to see if the report has been run with one of the auto switches
  If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
      
      Do AutoSetup
      Do TheOutput
      Do EmailEveryone
  
      post(Event:closewindow)
  
  ELSE
  
      AutoMode = false
      Start_Date = today()
      End_Date   = today()
  
      If Glo:webjob then
          Hide(?GroupChoice)
          hide(?PopCalendar)
          hide(?PopCalendar:2)
      END
  
  End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW11.Q &= Queue:Browse
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,tra:Account_Number_Key)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,tra:Account_Number,1,BRW11)
  BRW11.SetFilter('(clip(tra:BranchIdentification) <<> '''')')
  BIND('TagIcon',TagIcon)
  ?List{PROP:IconList,1} = '~off.ico'
  ?List{PROP:IconList,2} = '~on.ico'
  BRW11.AddField(TagIcon,BRW11.Q.TagIcon)
  BRW11.AddField(tra:Account_Number,BRW11.Q.tra:Account_Number)
  BRW11.AddField(tra:Company_Name,BRW11.Q.tra:Company_Name)
  BRW11.AddField(tra:BranchIdentification,BRW11.Q.tra:BranchIdentification)
  BRW11.AddField(tra:RecordNumber,BRW11.Q.tra:RecordNumber)
  BRW11.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW11.AskProcedure = 0
      CLEAR(BRW11.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULT2.Close
    Relate:JOBNOTES.Close
    Relate:SMSMAIL.Close
    Relate:SMSRECVD.Close
    Relate:TRADEAC2.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
          if records(Glo:queue) = 0 then
              !this may be being run thin client
              if glo:Webjob = true then
                  if clip(glo:UserAccountNumber) <> '' then
                      glo:Queue.GLO:Pointer = glo:UserAccountNumber
                      Add(Glo:Queue)
                  END
              ELSE
                  cycle
              END
          END !if no recoreds in global quque
      
      
          Do TheOutput
          
      
          !final bit of progess window changes if cancelled
          if  ReportCancelled then
              Expo.UpdateProgressText('===============')
              Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                  ' ' & FORMAT(clock(),@t1))
              Expo.FinishProgress()
      
          ELSE
              !send the emails
              Do EmailEveryone
      
              Expo.UpdateProgressText('===============')
              Expo.UpdateProgressText('Report Completed: ' & FORMAT(Today(),@d6) & |
                  ' ' & FORMAT(clock(),@t1))
              Expo.FinishProgress(FinalExportPath)
          END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020776'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020776'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020776'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Start_Date = TINCALENDARStyle1(Start_Date)
          Display(?Start_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          End_Date = TINCALENDARStyle1(End_Date)
          Display(?End_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    OF ?ButtonOK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    MyExcel13.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code

!TL = top left, TR = Top right, BL = bottom left BR = bottom Right

    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    MyExcel13.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    MyExcel13.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    MyExcel13.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    MyExcel13.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    MyExcel13.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
!---------------------------------------------------------------------------------
MyExcel13.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
MyExcel13.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
MyExcel13.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      TagIcon = ''
    ELSE
      TagIcon = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (TagIcon = '*')
    SELF.Q.TagIcon_Icon = 2
  ELSE
    SELF.Q.TagIcon_Icon = 1
  END


BRW11.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()






WIP_Audit_Report PROCEDURE(auditNumber, reportType)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Total_No_Of_Lines    LONG
loc:location         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
Make_Model           STRING(60)
tmp:Job_Number       STRING(20)
tmp:DefaultTelephone STRING(30)
tmp:DefaultFax       STRING(30)
TheAddress           GROUP,PRE(address)
TheName              STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
                     END
tmp:AuditNumber      LONG
tmp:DateTimeCompleted STRING(20)
qExport              QUEUE,PRE(qex)
JobNumber            LONG
IMEI                 STRING(20)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
StatusType           STRING(30)
                     END
scanStatus           STRING(30)
statusType           STRING(30)
i                    LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2688,7521,8104),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:TheName),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,469,3531,198),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,781),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Date Completed:'),AT(4948,969),USE(?StrDateCompleted),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(6208,969),USE(tmp:DateTimeCompleted),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel: '),AT(94,990),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(563,990),USE(address:Telephone),TRN,FONT(,9,,)
                         STRING('WIP Audit Number:'),AT(4948,1135),USE(?StrAuditNumber),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@n10),AT(6146,1135),USE(tmp:AuditNumber),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(94,1146),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(563,1146),USE(address:Fax),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3531,198),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16:9),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D06b),AT(6208,1302),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(6177,1458),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6458,1458),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6625,1458,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,1302),USE(?String16:4),TRN,FONT(,9,,)
                         STRING('WIP Audit Report'),AT(3656,0,3438,260),USE(?String21),TRN,RIGHT(10),FONT('Arial',14,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s30),AT(5490,0),USE(scanStatus),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s60),AT(2229,10),USE(Make_Model),FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n_12),AT(10,10),USE(qex:JobNumber),TRN,RIGHT(1),FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s18),AT(1052,10),USE(qex:IMEI),FONT('Arial',8,,,CHARSET:ANSI)
                         END
                       END
detail1                DETAIL,AT(,,,490),USE(?unnamed:6)
                         LINE,AT(94,63,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(94,135),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-10),AT(1635,135),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
detail2                DETAIL,AT(,,,333),USE(?unnamed:7)
                         STRING(@s30),AT(115,73,7094,208),USE(statusType),TRN,CENTER,FONT(,12,,FONT:bold)
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Job Number'),AT(104,1979),USE(?String23),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('IMEI'),AT(1083,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Make/Model'),AT(2260,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Scan Status'),AT(5521,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WIP_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'AuditWIP'
  If PrintOption(PreviewReq,glo:ExportReport,'WIP Audit Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STATUS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Relate:WIPEXC.Open
  Access:WIPAMF.UseFile
  Access:WIPAUI.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  
  
  RecordsToProcess = BYTES(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(Wipaui)*2
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        ! Okay, we need to go through , H, G, and X & Y
        
        ! Build the queue
        case reportType
            of 'not in location'
                scanStatus = 'SCANNED BUT NOT IN ' & clip(tra:Account_Number)
                do BuildSerialsScannedNotInLocation
            of 'not scanned'
                scanStatus = 'NOT SCANNED'
                do BuildSerialsNotScanned
            else ! default - scanned
                scanStatus = 'SCANNED'
                do BuildSerialsScanned
        end
        
        ! Sort by status type then job number
        sort(qExport, qExport.qex:StatusType, qExport.qex:JobNumber)
        
        ! Now export contents of the queue
        LOOP i = 1 TO RECORDS(qExport)
            GET(qExport, i)
        
            RecordsProcessed += 1
            Do DisplayProgress
        
            ! Write the status type header
            if statusType <> qExport.qex:StatusType
                if statusType <> ''
                    print(RPT:Detail1) ! Print total for this status
                    endpage(Report)
                    Total_No_Of_Lines = 0
                end
                statusType = qExport.qex:StatusType
                print(RPT:Detail2) ! Print status heading
            end
        
            Make_Model = clip(qExport.qex:Manufacturer) & '-' & clip(qExport.qex:ModelNumber)
            Total_No_Of_Lines += 1
        
            print(RPT:Detail)
        
        END
        
        if Total_No_Of_Lines > 0
            print(RPT:Detail1) ! Print total for this status
        end
        
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
    Relate:WIPEXC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
      tmp:AuditNumber = auditNumber
  
      Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
      wim:Audit_Number = auditNumber
      if Access:WIPAMF.Fetch(wim:Audit_Number_Key) = Level:Benign
          tmp:DateTimeCompleted = format(wim:Date_Completed, @d06) & ' ' & format(wim:Time_Completed, @t01)
      end
  
      !*************************set the display address to the head account if booked on as a web job***********************
      if glo:webjob
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = clarionet:global.param2
          IF access:tradeacc.fetch(tra:account_number_key)
          ! Error!
          END ! IF
          address:TheName      = tra:Company_Name
          address:AddressLine1 = tra:Address_Line1
          address:AddressLine2 = tra:Address_Line2
          address:AddressLine3 = tra:Address_Line3
          address:Postcode     = tra:Postcode
          address:Telephone    = tra:Telephone_Number
          address:Fax          = tra:Fax_Number
          address:EmailAddress = tra:EmailAddress
      Else
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = GETINI('BOOKING', 'HeadAccount',, CLIP(PATH()) & '\SB2KDEF.INI')
          IF access:tradeacc.fetch(tra:account_number_key)
          ! Error!
          END ! IF
          address:TheName      = tra:Company_Name
          address:AddressLine1 = tra:Address_Line1
          address:AddressLine2 = tra:Address_Line2
          address:AddressLine3 = tra:Address_Line3
          address:Postcode     = tra:Postcode
          address:Telephone    = tra:Telephone_Number
          address:Fax          = tra:Fax_Number
          address:EmailAddress = tra:EmailAddress
  
      end ! if
  
      loc:location =  tra:SiteLocation
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='WIP_Audit_Report'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
BuildSerialsScanned         ROUTINE

    Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
    wia:Audit_Number = wim:Audit_Number
    set(wia:Audit_Number_Key, wia:Audit_Number_Key)
    loop
        if Access:WIPAUI.Next() then break.
        if wia:Audit_Number <> wim:Audit_Number then break.

        RecordsProcessed += 1
        Do DisplayProgress

        qExport.qex:JobNumber = wia:Ref_Number

        Access:Jobs.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wia:Ref_Number
        if Access:Jobs.Fetch(Job:Ref_Number_Key) then cycle.

        if wia:IsExchange
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

            qExport.qex:IMEI = xch:ESN
            qExport.qex:Manufacturer = xch:Manufacturer
            qExport.qex:ModelNumber = xch:Model_Number
        else
            qExport.qex:IMEI = job:ESN
            qExport.qex:Manufacturer = job:Manufacturer
            qExport.qex:ModelNumber = job:Model_Number
        end

        qExport.qex:StatusType = wia:Status ! Status could be either exchange or job status

        add(qExport)
    end
BuildSerialsNotScanned      ROUTINE
    DATA
StartingRef     long
StatusDays      long


statusQ queue
statusType  string(30)
isExchange  byte
        end

jobsQ   queue
JobNumber   long
        end

i       long,auto

    CODE

    !TB12740 - J - 17/06/14 going to use location repeatedly - best just to look it up once
    !this was looked up on the trade account lookup

    !TB12740 - need a starting point of so many days ago - J - 16/06/14
    StatusDays = getini(loc:Location,'StatusDays',0,clip(path())&'\WIPAudit.ini')
    If StatusDays = 0 then
        StartingRef = 0
    ELSE
        Access:jobs.clearkey(job:Date_Booked_Key)
        job:date_booked =  today() - StatusDays
        set(job:Date_Booked_Key,job:Date_Booked_Key)
        if Access:jobs.next() then exit.            !No jobs in date limit - print blank report (just header)
        StartingRef = job:Ref_Number
    END
    !TB12740 - end of find a starting refnumber

    ! Fill the Export queue with jobs that have NOT been scanned.

    ! Go through each of the items on this audit
    ! Build a queue of status types used and a queue of jobs audited
    Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
    wia:Audit_Number = wim:Audit_Number
    set(wia:Audit_Number_Key, wia:Audit_Number_Key)
    loop

        if Access:WIPAUI.Next() then break.
        if wia:Audit_Number <> wim:Audit_Number then break.

        RecordsProcessed += 1
        Do DisplayProgress

        statusQ.statusType = wia:Status
        get(statusQ, statusQ.statusType)
        if error()
            statusQ.statusType = wia:Status
            statusQ.isExchange = wia:IsExchange
            add(statusQ, statusQ.statusType)
        end

        jobsQ.JobNumber = wia:Ref_Number
        get(jobsQ, jobsQ.JobNumber)
        if error()
            jobsQ.JobNumber = wia:Ref_Number
            add(jobsQ, jobsQ.JobNumber)
        end
    end

    !TB12740 - J - 17/06/14 - include all the statuses that should be included - check WIPEXC (this contains the excluded statuses)
    Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 0
    SET(sts:Ref_Number_Only_Key,sts:Ref_Number_Only_Key)
    LOOP UNTIL Access:STATUS.Next()
        !is this one exluded?
        Access:WIPEXC.ClearKey(wix:StatusTypeKey) 
        wix:Location = loc:Location
        wix:Status   = sts:Status
        if access:Wipexc.fetch(wix:StatusTypeKey)
            !not in excluded list - Is it already in the statuses list?
            statusQ.statusType = sts:Status
            get(statusQ, statusQ.statusType)
            if error()
                !not found in status list add it
                statusQ.statusType = sts:Status
                if sts:Exchange = 'YES' then
                    statusQ.isExchange = 1
                ELSE
                    statusQ.isExchange = 0
                END
                add(statusQ, statusQ.statusType)
            end
        END !if not in excluded list
    END ! LOOP


    ! Now process each status type used in the audit
    loop i = 1 to records(statusQ)

        get(statusQ, i)

        if glo:WebJob
            if statusQ.isExchange
                Access:WEBJOB.ClearKey(wob:HeadExchangeStatus)
                wob:HeadAccountNumber = Clarionet:Global.Param2
                wob:Exchange_Status   = statusQ.statusType
                wob:RefNumber         = StartingRef         !TB12740 - J - 17/06/14 adding a starting point
                set(wob:HeadExchangeStatus, wob:HeadExchangeStatus)
            else
                Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)
                wob:HeadAccountNumber = Clarionet:Global.Param2
                wob:Current_Status    = statusQ.statusType
                wob:RefNumber         = StartingRef         !TB12740 - J - 17/06/14 adding a starting point
                set(wob:HeadCurrentStatusKey, wob:HeadCurrentStatusKey)
            end
            loop
                if Access:WEBJOB.Next() then break.
                if wob:HeadAccountNumber <> Clarionet:Global.Param2 then break.
                if statusQ.isExchange
                    if wob:Exchange_Status <> statusQ.statusType then break.
                else
                    if wob:Current_Status <> statusQ.statusType then break.
                end

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = wob:RefNumber
                if Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign
                    if jobe:HubRepair = 1 then cycle.
                end

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                if Access:JOBS.Fetch(job:Ref_Number_Key) then cycle.

                !is this at the correct location
                if clip(job:Location) <> 'AT FRANCHISE' then cycle.

                ! Check if this job is already on the audit, we want jobs that have not been scanned
                jobsQ.JobNumber = job:Ref_Number
                get(jobsQ, jobsQ.JobNumber)
                if not error() then cycle.

                qExport.qex:JobNumber = job:Ref_Number

                if statusQ.isExchange
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

                    qExport.qex:IMEI = xch:ESN
                    qExport.qex:Manufacturer = xch:Manufacturer
                    qExport.qex:ModelNumber = xch:Model_Number
                    qExport.qex:StatusType = job:Exchange_Status
                else
                    qExport.qex:IMEI = job:ESN
                    qExport.qex:Manufacturer = job:Manufacturer
                    qExport.qex:ModelNumber = job:Model_Number
                    qExport.qex:StatusType = job:Current_Status
                end

                add(qExport)
            end ! loop

        else

            if statusQ.isExchange
                Access:JOBS.Clearkey(job:ExcStatusKey)
                job:Exchange_Status = statusQ.statusType
                job:Ref_Number      = StartingRef         !TB12740 - J - 17/06/14 adding a starting point by date
                set(job:ExcStatusKey, job:ExcStatusKey)
            else
                Access:JOBS.Clearkey(job:By_Status)
                job:Current_Status = statusQ.statusType
                job:Ref_Number      = StartingRef         !TB12740 - J - 17/06/14 adding a starting point by date
                set(job:By_Status, job:By_Status)
            end

            loop
                if Access:JOBS.Next() then break.
                if statusQ.isExchange
                    if job:Exchange_Status <> statusQ.statusType then break.
                else
                    if job:Current_Status <> statusQ.statusType then break.
                end


                !is this at the correct location
                if clip(job:Location) <> 'RECEIVED AT ARC' then cycle.

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                if Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign
                !    if jobe:HubRepair = 1 then cycle.
                end

                ! Check if this job is already on the audit, we want jobs that have not been scanned
                jobsQ.JobNumber = job:Ref_Number
                get(jobsQ, jobsQ.JobNumber)
                if not error() then cycle.

                qExport.qex:JobNumber = job:Ref_Number

                if statusQ.isExchange
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

                    qExport.qex:IMEI = xch:ESN
                    qExport.qex:Manufacturer = xch:Manufacturer
                    qExport.qex:ModelNumber = xch:Model_Number
                    qExport.qex:StatusType = job:Exchange_Status
                else
                    qExport.qex:IMEI = job:ESN
                    qExport.qex:Manufacturer = job:Manufacturer
                    qExport.qex:ModelNumber = job:Model_Number
                    qExport.qex:StatusType = job:Current_Status
                end

                add(qExport)

            end ! loop through jobs file

        end  !if glo:webjob
        
    end !loop through status queue
BuildSerialsScannedNotInLocation    ROUTINE

    ! This is the same as the BuildSerialsScanned routine but with a location filter
    Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
    wia:Audit_Number = wim:Audit_Number
    set(wia:Audit_Number_Key, wia:Audit_Number_Key)
    loop
        if Access:WIPAUI.Next() then break.
        if wia:Audit_Number <> wim:Audit_Number then break.

        Access:Jobs.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wia:Ref_Number
        if Access:Jobs.Fetch(Job:Ref_Number_Key) then cycle.

!        ! This is how the old version checked the location...
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)
!        wob:RefNumber = job:Ref_number
!        if Access:WEBJOB.Fetch(wob:refNumberKey) = Level:Benign
!            Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
!            tra_ali:Account_Number = wob:HeadAccountNumber
!            if Access:TRADEACC_ALIAS.Fetch(tra_ali:Account_Number_Key) = Level:Benign
!                if tra_ali:SiteLocation = wim:Site_Location then cycle. ! We want jobs not in the location
!            end
!        end

        ! #12491 Include on the report if job is CURRENTLY not in the right location (DBH: 23/05/2012)
        ! J - 01/07/14 - new version of checking location moved here
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
            CYCLE
        END
        IF glo:WEbjob = 1

            IF (jobe:HubRepair <> 1)
                ! Job is currently at the RRC. Don't include on the report
                CYCLE
            END

        ELSE

            IF (jobe:HubRepair = 1)
                ! Job is currently at the ARC. Don't include on the report
                CYCLE
            END

        END




        RecordsProcessed += 1
        Do DisplayProgress

        qExport.qex:JobNumber = wia:Ref_Number

        if wia:IsExchange
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.
            qExport.qex:IMEI = xch:ESN
            qExport.qex:Manufacturer = xch:Manufacturer
            qExport.qex:ModelNumber = xch:Model_Number
        else
            qExport.qex:IMEI = job:ESN
            qExport.qex:Manufacturer = job:Manufacturer
            qExport.qex:ModelNumber = job:Model_Number
        end

        qExport.qex:StatusType = wia:Status ! Status could be either exchange or job status

        add(qExport)
    end
! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()





WIPAuditReportCriteria PROCEDURE (auditNumber)        !Generated from procedure template - Window

loc:SerialsScanned   BYTE
loc:SerialsScannedNotInLocation BYTE
loc:SerialsNotScanned BYTE
qExport              QUEUE,PRE(qex)
JobNumber            LONG
IMEI                 STRING(20)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
StatusType           STRING(30)
                     END
scanStatus           STRING(100)
reportType           STRING(20)
CurrentLocation      STRING(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(850,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('WIP Audit Report'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION('Report Type'),AT(260,150,168,84),USE(?ReportOption),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           END
                           CHECK('Serials Scanned'),AT(270,166),USE(loc:SerialsScanned),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Serials Scanned But Not In Location'),AT(270,184),USE(loc:SerialsScannedNotInLocation),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Serials Not Scanned'),AT(270,202),USE(loc:SerialsNotScanned),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(312,332),USE(?Print),TRN,FLAT,ICON('printp.jpg')
                       BUTTON,AT(380,332),USE(?Export),TRN,FLAT,ICON('exportp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
EE MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
AddRecord       ROUTINE

    ! Job number
    EE.AddField(qExport.qex:JobNumber,1)
    ! IMEI Number
    EE.AddField(qExport.qex:IMEI)
    ! Manufacturer
    EE.AddField(qExport.qex:Manufacturer)
    ! Model Number
    EE.AddField(qExport.qex:ModelNumber)
    ! Scan Status - not part of the queue as it is hardcoded for every entry
    EE.AddField(scanStatus,,1)
    
       


CreateTitle     ROUTINE

    EE.AddField('WIP Audit',1)
    EE.AddField(clip(tra:Account_Number) & ' ' & clip(tra:Company_Name),,1)

    EE.AddField('Date Completed',1)
    EE.AddField(Format(wim:Date_Completed,@d06))
    EE.AddField(Format(wim:Time_Completed,@t01),,1)

    EE.AddField('WIP Audit Number',1)
    EE.AddField(wim:Audit_Number,,1)


    exit
CreateTitleExcel        ROUTINE

    E1.WriteToCell('WIP AUDIT','A1')
    E1.WriteToCell(clip(tra:Account_Number) & ' ' & clip(tra:Company_Name),'C1')

    E1.WriteToCell('Date Completed','A2')
    E1.WriteToCell(Format(wim:Date_Completed,@d06) & ' ' & Format(wim:Time_Completed,@t01),'C2')

    E1.WriteToCell('WIP Audit Number','A3')
    E1.WriteToCell(wim:Audit_Number,'C3')
  
    exit
Export      ROUTINE
DATA
kReportFolder       CSTRING(255)
kReportName         STRING(50)
kReportFolderName   STRING(50)
kFileName           STRING(255)
kTempFolder         CSTRING(255)
kTempFilename       CSTRING(255)
kLastDataColumn     STRING(1)
kPasteDataCell      STRING(30)
locClipBoard        ANY
lastStatus          STRING(30)
i                   LONG,AUTO
CODE

    ! Report type must be on of the following :-
    ! reportType = 'scanned'
    ! reportType = 'not in location'
    ! reportType = 'not scanned'

    ! Set Report Title
    Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
    wim:Audit_Number = auditNumber
    if Access:WIPAMF.Fetch(wim:Audit_Number_Key)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Cannot get details of this audit.','ServiceBase',|
                     'mstop.jpg','/&OK')
            of 1 ! &OK Button
        End!Case Message
        exit
    end

    ! Get the trade account
    Access:TRADEACC.ClearKey(tra:account_number_key)
    if glo:webjob
        tra:account_number = clarionet:global.param2
    else
        tra:account_number = GETINI('BOOKING', 'HeadAccount',, CLIP(PATH()) & '\SB2KDEF.INI')
    end

    if Access:TRADEACC.Fetch(tra:account_number_key)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Cannot get the account details of this audit.','ServiceBase',|
                     'mstop.jpg','/&OK')
            of 1 ! &OK Button
        End!Case Message
        exit
    end

    CurrentLocation = tra:SiteLocation

    case reportType
        of 'not in location'
            kReportName = 'SerialsScannedLocation_' & wim:Audit_Number
            kReportFolderName = 'SerialsScannedLocation'
        of 'not scanned'
            kReportName = 'SerialsNotScanned_' & wim:Audit_Number
            kReportFolderName = 'SerialsNotScanned'
        else ! default - scanned
            kReportName = 'SerialsScanned_' & wim:Audit_Number
            kReportFolderName = 'SerialsScanned'
    end

    ! Get Temp Folder For CSV Export
    kTempFolder = GetTempFolder()

    IF (kTempFolder = '')
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.'&|
            '|'&|
            '|Please ty again.','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT
    END

    ! Set Final Filename
    IF (glo:WebJob = 0)
        kFileName = clip(kReportName) & '.xls'
    ELSE ! IF (glo:WebJob = 1)
        kFileName = clip(kReportName) & '.csv'
    END ! IF (glo:WebJob = 1)

    kTempFilename = CLIP(kTempFolder) & 'WIPSSREP_' & CLOCK() & RANDOM(1,1000) & '.CSV'

    IF (EE.OpenDataFile(kTempFilename,,1))
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.','ServiceBase',|
                     'mstop.jpg','/&OK')
        Of 1 ! &OK Button
        End!Case Message
        RETURN
    END

    ! Start Progress Window
    EE.OpenProgressWindow()

    EE.UpdateProgressText('')

    EE.UpdateProgressText('Report Started: ' & FORMAT(EE.StartDate,@d6) & |
        ' ' & FORMAT(EE.StartTime,@t1))
    EE.UpdateProgressText('')

    ! Add Header
    Do CreateTitle

    free(qExport)
    clear(qExport)

    ! Build the queue
    case reportType
        of 'not in location'
            scanStatus = 'SCANNED BUT NOT IN ' & clip(tra:Account_Number)
            do BuildSerialsScannedNotInLocation
        of 'not scanned'
            scanStatus = 'NOT SCANNED'
            do BuildSerialsNotScanned

            ! #12491 A bodge to force report to stay as CSV regardless of where it is run.
            !  Ideally I would re-write the process to tidy it up, but I just don't have time. (DBH: 24/05/2012)
            EE.CloseDataFile()
            kFileName = clip(kReportName) & '.csv'
            IF (EE:CancelPressed = 3) then          !new special case of no jobs found - reverts to type 2 in a few lines J - 30/16/14
                Case Missive('Error: No jobs have been found in the specified date limit.','ServiceBase 3g','mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                EE:CancelPressed = 2
            END
            IF (EE:CancelPressed = 2)
                EE.UpdateProgressText('===============')
                EE.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                    ' ' & FORMAT(clock(),@t1))
                EE.FinishProgress()
                REMOVE(kTempFilename)        
            ELSE ! IF
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                RENAME(kTempFilename,CLIP(kTempFolder) & CLIP(kFileName))
                IF (glo:WebJob = 1)
                    EE.FinishProgress()
                    SendFileToClient(CLIP(kTempFolder) & CLIP(kFileName))

                    Beep(Beep:SystemAsterisk)  ;  Yield()
                    Case Missive('Export File Created.'&|
                        '|'&|
                        '|' & Clip(kFileName) & '','ServiceBase',|
                                   'midea.jpg','/&OK') 
                    Of 1 ! &OK Button
                    End!Case Message
                ELSE
                    EE.UpdateProgressText('===============')
                    EE.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

                    kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportFolderName),glo:WebJob)
                    IF (EE.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                        ! Couldn't save final file
                        EE.FinishProgress()
                    ELSE
                        EE.FinishProgress(kReportFolder)
                    END            
                END            
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))


            END ! IF

            EXIT
            ! #12491 =========================================
        else ! default - scanned
            scanStatus = 'SCANNED'
            do BuildSerialsScanned
    end

    IF (EE.CancelPressed <> 2)
        ! #12491 Didn't click cancel, so carry on (DBH: 23/05/2012)
        ! Sort by status type then job number
        sort(qExport, qExport.qex:StatusType, qExport.qex:JobNumber)

        ! Now export contents of the queue
        lastStatus = ''

        ! Build Temp File
        LOOP i = 1 TO RECORDS(qExport)
            GET(qExport, i)


            IF (EE.UpdateProgressWindow())
                BREAK
            END

            ! Write the status type header line
            if lastStatus <> qExport.qex:StatusType
                EE.UpdateProgressText('Exporting Status: ' & CLIP(qExport.qex:StatusType))
                EE.AddField(qExport.qex:StatusType,1,1)
                lastStatus = qExport.qex:StatusType
            end

            do AddRecord

            IF (EE.CancelPressed > 0)
                BREAK
            END

        END ! LOOP l# = 1 TO RECORDS(glo:Queue)
    END ! IF (EE.CancelPressed <> 2)

    EE.CloseDataFile()

    IF (EE:CancelPressed = 2)
        EE.UpdateProgressText('===============')
        EE.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        EE.FinishProgress()
        REMOVE(kTempFilename)
    ELSE
        IF (glo:WebJob = 1)
            EE.FinishProgress()
            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            RENAME(kTempFilename,CLIP(kTempFolder) & CLIP(kFileName))
            SendFileToClient(CLIP(kTempFolder) & CLIP(kFileName))
            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))

            Beep(Beep:SystemAsterisk)  ;  Yield()
            Case Missive('Export File Created.'&|
                '|'&|
                '|' & Clip(kFileName) & '','ServiceBase',|
                           'midea.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
        ELSE
            EE.UpdateProgressText()
            EE.UpdateProgressText('Formatting Document')
            EE.UpdateProgressText()
            ! Build Excel Document
            If E1.Init(0,0) = 0
                Case Missive('An error has occurred finding your Excel document.'&|
                  '<13,10>'&|
                  '<13,10>Please quit and try again.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If E1.Init(0,0,1) = 0

            E1.NewWorkBook()

            case reportType
                of 'not in location'
                    E1.RenameWorkSheet('Not In Location')
                of 'not scanned'
                    E1.RenameWorkSheet('Not Scanned')
                else ! default - scanned
                    E1.RenameWorkSheet('Scanned')
            end

            do CreateTitleExcel

            E1.SelectCells('A10')

            row# = 4
            lastStatus = ''

            LOOP i = 1 TO RECORDS(qExport)
                GET(qExport, i)

                if lastStatus <> qExport.qex:StatusType
                    row# += 1
                    E1.WriteToCell(qExport.qex:StatusType, 'A' & row#)
                    !El.MergeCells('A' & row#, 'E' & row#) - Not supported in this version of Office Inside
                    !E1.SetCellAlignment(oix:HorizontalAlignment, oix:xlCenter, 'A' & row#)
                    E1.SetCellFontStyle('Bold', 'A' & row#)

                    row# += 2
                    lastStatus = qExport.qex:StatusType
                end

                E1.WriteToCell(qExport.qex:JobNumber, 'A' & row#)
                E1.WriteToCell(qExport.qex:IMEI, 'B' & row#)
                E1.WriteToCell(qExport.qex:Manufacturer, 'C' & row#)
                E1.WriteToCell(qExport.qex:ModelNumber, 'D' & row#)
                E1.WriteToCell(scanStatus, 'E' & row#)

                E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,,'B' & row#)
                E1.SetCellNumberFormat(oix:NumberFormatText,,0,,'C' & row#,'E' & row#)

                row# += 1
            END

            EE.FinishFormat(E1,'E',row#,'A4')

            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            E1.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
            E1.CloseWorkBook(3)
            E1.Kill()

            EE.UpdateProgressText('===============')
            EE.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportFolderName),glo:WebJob)
            IF (EE.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                EE.FinishProgress()
            ELSE
                EE.FinishProgress(kReportFolder)
            END
        END
    END

    REMOVE(kTempFilename)

    exit
BuildSerialsScanned         ROUTINE

    Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
    wia:Audit_Number = wim:Audit_Number
    set(wia:Audit_Number_Key, wia:Audit_Number_Key)
    loop

        if Access:WIPAUI.Next() then break.
        if wia:Audit_Number <> wim:Audit_Number then break.

        EE.UpdateProgressText('',1)

        if (EE.UpdateProgressWindow()) then break.

        qExport.qex:JobNumber = wia:Ref_Number

        Access:Jobs.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wia:Ref_Number
        if Access:Jobs.Fetch(Job:Ref_Number_Key) then cycle.

        if wia:IsExchange
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

            qExport.qex:IMEI = xch:ESN
            qExport.qex:Manufacturer = xch:Manufacturer
            qExport.qex:ModelNumber = xch:Model_Number
        else
            qExport.qex:IMEI = job:ESN
            qExport.qex:Manufacturer = job:Manufacturer
            qExport.qex:ModelNumber = job:Model_Number
        end

        qExport.qex:StatusType = wia:Status ! Status could be either exchange or job status

        add(qExport)

    end
BuildSerialsNotScanned      ROUTINE
    DATA

statusQ queue
statusType  string(30)
isExchange  byte
        end
jobsQ   queue
JobNumber   long
        end
i       long,auto
totalStatus     LONG(0)
countStatus     LONG(0)
StartingRef     LONG(0)
StatusDays      Long(0)
showStatusHeader    BYTE(TRUE)
ExportGroup     GROUP,PRE()
expJobNumber        STRING(30)
expIMEINumber       STRING(30)
expManufacturer     STRING(30)
expModelNumber      STRING(30)
                END

    CODE
    ! Fill the Export queue with jobs that have not been scanned.

    !TB12740 - J - 17/06/14 going to use location repeatedly - best just to look it up once
    !Already looked up in tradeacc.siteloction

    !TB12740 - need a starting point of so many days ago - J - 16/06/14
    StatusDays = getini(CurrentLocation,'StatusDays',0,clip(path())&'\WIPAudit.ini')
    If StatusDays = 0 then
        StartingRef = 0
    ELSE
        Access:jobs.clearkey(job:Date_Booked_Key)
        job:date_booked =  today() - StatusDays
        set(job:Date_Booked_Key,job:Date_Booked_Key)
        if Access:jobs.next() then
            EE:CancelPressed = 3
            EXIT
        ELSE
            !found
            StartingRef = job:Ref_Number
        END
    END
    !TB12740 - end of find a starting refnumber


    ! Go through each of the items on this audit
    ! Build a queue of status types used and a queue of jobs audited  !TB12740 - revert to building and using the status queue
    EE.UpdateProgressText('')
    EE.UpdateProgressText('Building List Of Jobs Scanned....')
    EE.UpdateProgressText('')

    Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
    wia:Audit_Number = wim:Audit_Number
    set(wia:Audit_Number_Key, wia:Audit_Number_Key)
    loop
        if Access:WIPAUI.Next() then break.
        if wia:Audit_Number <> wim:Audit_Number then break.

        EE.UpdateProgressText('',1)

        if (EE.UpdateProgressWindow()) then break.

        !TB12740 - J - 17/06/14 - revert to using a queue of the statuses scanned, not "all statuses"
        !This adds all the statuses scanned to the list of statuses to be exported
        statusQ.statusType = wia:Status
        get(statusQ, statusQ.statusType)
        if error()
            statusQ.statusType = wia:Status
            statusQ.isExchange = wia:IsExchange
            add(statusQ, statusQ.statusType)
        end

        jobsQ.JobNumber = wia:Ref_Number
        get(jobsQ, jobsQ.JobNumber)
        if error()
            jobsQ.JobNumber = wia:Ref_Number
            add(jobsQ, jobsQ.JobNumber)
        end
    end

    IF (EE.CancelPressed > 0)
        ! Break out if cancel was pressed
        EXIT
    END


    !TB12740 - J - 17/06/14 - include all the statuses that should be included - check WIPEXC (this contains the excluded statuses)
    Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 0
    SET(sts:Ref_Number_Only_Key,sts:Ref_Number_Only_Key)
    LOOP UNTIL Access:STATUS.Next()
        !is this one exluded?
        Access:WIPEXC.ClearKey(wix:StatusTypeKey) 
        wix:Location = CurrentLocation
        wix:Status   = sts:Status
        if access:Wipexc.fetch(wix:StatusTypeKey)
            !not in excluded list - Is it already in the statuses list?
            statusQ.statusType = sts:Status
            get(statusQ, statusQ.statusType)
            if error()
                !not found in status list add it
                statusQ.statusType = sts:Status
                statusQ.isExchange = ''
                add(statusQ, statusQ.statusType)

            end
        END !if not in excluded list
    END ! LOOP

    ! Quick count of statuses  !TB12740 - J - 17/06/14 - revert to using a queue of the statuses used, not all statuses
    TotalStatus = records(StatusQ)
!    Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
!    sts:Ref_Number = 0
!    SET(sts:Ref_Number_Only_Key,sts:Ref_Number_Only_Key)
!    LOOP UNTIL Access:STATUS.Next()
!        IF sts:Loan = 'YES'
!            CYCLE
!        END
!        totalStatus += 1
!    END ! LOOP

    !TB12740 - J - 17/06/14 - revert to using a queue of the statuses used, not all statuses
    !Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
    !sts:Ref_Number = 0
    !SET(sts:Ref_Number_Only_Key,sts:Ref_Number_Only_Key)
    !LOOP UNTIL Access:STATUS.Next()
    !TB12740 - J - 17/06/14 - revert to using a queue of the statuses used, not all statuses

    Loop CountStatus = 1 to records(statusQ)      !TB12740 replacing loop through all statuses with loop through queue

        Get(StatusQ,CountStatus)
        if error() then break.
                                                            
        Access:Status.clearkey(sts:Status_Key)   !TB12740 fetch the status record so can use all existing code from here on
        sts:Status = statusQ.statusType
        if access:Status.fetch(sts:Status_Key) then cycle.

        IF sts:Loan = 'YES'
            CYCLE
        END

        showStatusHeader = TRUE ! Print the status header when first export line found

        !countStatus += 1

        EE.UpdateProgressText(countStatus & '/' & totalStatus & '. Counting Jobs For Status: ' & CLIP(sts:Status))

        if (EE.UpdateProgressWindow()) then break.

        if glo:WebJob
            if sts:Exchange = 'YES'
                Access:WEBJOB.ClearKey(wob:HeadExchangeStatus)     !note this key has ref_number as the final part - can use this to do the date()
                wob:HeadAccountNumber = Clarionet:Global.Param2
                wob:Exchange_Status   = sts:Status
                wob:RefNumber         = StartingRef                !TB12740 - adding the ref_number starting point
                set(wob:HeadExchangeStatus, wob:HeadExchangeStatus)
            else
                Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)   !note this key has ref_number as the final part - can use this to do the date()
                wob:HeadAccountNumber = Clarionet:Global.Param2
                wob:Current_Status    = sts:Status
                wob:RefNumber         = StartingRef                !TB12740 - adding the ref_number starting point
                set(wob:HeadCurrentStatusKey, wob:HeadCurrentStatusKey)
            end

            loop
                if Access:WEBJOB.Next() then break.
                if wob:HeadAccountNumber <> Clarionet:Global.Param2 then break.
                if sts:Exchange = 'YES'
                    if wob:Exchange_Status <> sts:Status then break.
                else
                    if wob:Current_Status <> sts:Status then break.
                end

                if (EE.UpdateProgressWindow()) then break.

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = wob:RefNumber
                if Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign
                    if jobe:HubRepair = 1 then cycle.
                end

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                if Access:JOBS.Fetch(job:Ref_Number_Key) then cycle.

                !Is this at the correct location still?
                if clip(job:Location) <> 'AT FRANCHISE' then cycle.

                ! Check if this job is already on the audit, we want jobs that have not been scanned
                jobsQ.JobNumber = job:Ref_Number
                get(jobsQ, jobsQ.JobNumber)
                if not error() then cycle.

                CLEAR(ExportGroup)

!                qExport.qex:JobNumber = job:Ref_Number

                if sts:Exchange = 'YES'
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

                    expIMEINumber = xch:ESN
                    expManufacturer = xch:Manufacturer
                    expModelNumber = xch:Model_Number

!                    qExport.qex:IMEI = xch:ESN
!                    qExport.qex:Manufacturer = xch:Manufacturer
!                    qExport.qex:ModelNumber = xch:Model_Number
!                    qExport.qex:StatusType = job:Exchange_Status
                else
                    expIMEINumber = job:ESN
                    expManufacturer = job:Manufacturer
                    expModelNumber = job:Model_Number

                end

!                IF (qExport.qex:IMEI = '') ! #12491 Do not show entries with no IMEI (DBH: 19/04/2012)
!                    CYCLE
!                END
!                add(qExport)

                IF (expIMEINumber = '')
                    CYCLE
                END

                IF (showStatusHeader = TRUE)
                    ! First job for this status, export the header
                    EE.AddField(sts:Status,1,1)
                    showStatusHeader = FALSE
                END ! IF

                ! Job Number
                EE.AddField(job:Ref_Number,1)
                ! IMEI Number
                EE.AddField('''' & expIMEINumber)
                ! Manufacturer
                EE.AddField(expManufacturer)
                ! Model Number
                EE.AddField(expModelNumber)
                ! Scan Status
                EE.AddField(scanStatus,,1)

            end ! loop

        else  !if webjob

            if sts:Exchange = 'YES'
                Access:JOBS.Clearkey(job:ExcStatusKey)  !note this key has ref_number as the final part - can use this to do the date()
                job:Exchange_Status = sts:Status
                job:Ref_Number      = StartingRef                !TB12740 - adding the ref_number starting point
                set(job:ExcStatusKey, job:ExcStatusKey)
            else
                Access:JOBS.Clearkey(job:By_Status)     !note this key has ref_number as the final part - can use this to do the date()
                job:Current_Status = sts:Status
                job:Ref_Number     = StartingRef                !TB12740 - adding the ref_number starting point
                set(job:By_Status, job:By_Status)
            end
            loop

                if Access:JOBS.Next() then break.

                if sts:Exchange = 'YES'
                    if job:Exchange_Status <> sts:Status then break.
                else
                    if job:Current_Status <> sts:Status then break.
                end

                IF (EE.UpdateProgressWindow())
                    BREAK
                END

                !Is this at the correct location still?
                if clip(job:Location) <> 'RECEIVED AT ARC' then cycle.

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                if Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign
                    !if jobe:HubRepair <> 1 then cycle.    ! #12491 I think the ARC version uses ALL jobs (DBH: 20/04/2012)
                end

                !Check if this job is already on the audit, we want jobs that have not been scanned
                jobsQ.JobNumber = job:Ref_Number
                get(jobsQ, jobsQ.JobNumber)
                if not error() then cycle.

                !qExport.qex:JobNumber = job:Ref_Number

                CLEAR(ExportGroup)

                !if statusQ.isExchange
                IF (sts:Exchange = 'YES')
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

                    expIMEINumber = xch:ESN
                    expManufacturer = xch:Manufacturer
                    expModelNumber = xch:Model_Number

!                    qExport.qex:IMEI = xch:ESN
!                    qExport.qex:Manufacturer = xch:Manufacturer
!                    qExport.qex:ModelNumber = xch:Model_Number
!                    qExport.qex:StatusType = job:Exchange_Status
                ELSE
!                    qExport.qex:IMEI = job:ESN
!                    qExport.qex:Manufacturer = job:Manufacturer
!                    qExport.qex:ModelNumber = job:Model_Number
!                    qExport.qex:StatusType = job:Current_Status

                    expIMEINumber = job:ESN
                    expManufacturer = job:Manufacturer
                    expModelNumber = job:Model_Number
                END

!                IF (qExport.qex:IMEI = '') ! #12491 Do not show entries with no IMEI (DBH: 19/04/2012)
!                    CYCLE
!                END
!
!                add(qExport)

                IF (expIMEINumber = '')
                    CYCLE
                END

                IF (showStatusHeader = TRUE)
                    ! First job for this status, export the header
                    EE.AddField(sts:Status,1,1)
                    showStatusHeader = FALSE
                END ! IF

                ! Job Number
                EE.AddField(job:Ref_Number,1)
                ! IMEI Number
                EE.AddField('''' & expIMEINumber)
                ! Manufacturer
                EE.AddField(expManufacturer)
                ! Model Number
                EE.AddField(expModelNumber)
                ! Scan Status
                EE.AddField(scanStatus,,1)
            end ! loop

        end

        IF (EE.CancelPressed > 0)
            BREAK
        END ! IF
    end

    
BuildSerialsScannedNotInLocation    ROUTINE

    ! This is the same as the BuildSerialsScanned routine but with a location filter
    Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
    wia:Audit_Number = wim:Audit_Number
    set(wia:Audit_Number_Key, wia:Audit_Number_Key)
    loop

        if Access:WIPAUI.Next() then break.
        if wia:Audit_Number <> wim:Audit_Number then break.

        Access:Jobs.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wia:Ref_Number
        if Access:Jobs.Fetch(Job:Ref_Number_Key) then cycle.

!        ! This is how the old version checked the location...
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)
!        wob:RefNumber = job:Ref_number
!        if Access:WEBJOB.Fetch(wob:refNumberKey) = Level:Benign
!            Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
!            tra_ali:Account_Number = wob:HeadAccountNumber
!            if Access:TRADEACC_ALIAS.Fetch(tra_ali:Account_Number_Key) = Level:Benign
!                if tra_ali:SiteLocation = wim:Site_Location then cycle. ! We want jobs not in the location
!            end
!        end

        ! #12491 Include on the report if job is CURRENTLY not in the right location (DBH: 23/05/2012)
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
            CYCLE
        END

        IF glo:WEbjob = 1

            IF (jobe:HubRepair <> 1)
                ! Job is currently at the RRC. Don't include on the report
                CYCLE
            END

        ELSE

            IF (jobe:HubRepair = 1)
                ! Job is currently at the ARC. Don't include on the report
                CYCLE
            END

        END

        EE.UpdateProgressText('',1)

        if (EE.UpdateProgressWindow()) then break.

        qExport.qex:JobNumber = wia:Ref_Number

        if wia:IsExchange
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

            qExport.qex:IMEI = xch:ESN
            qExport.qex:Manufacturer = xch:Manufacturer
            qExport.qex:ModelNumber = xch:Model_Number
        else
            qExport.qex:IMEI = job:ESN
            qExport.qex:Manufacturer = job:Manufacturer
            qExport.qex:ModelNumber = job:Model_Number
        end

        qExport.qex:StatusType = wia:Status ! Status could be either exchange or job status

        add(qExport)
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020800'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WIPAuditReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:STATUS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Relate:WIPEXC.Open
  Access:USERS.UseFile
  Access:WIPAMF.UseFile
  Access:WIPAUI.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
    Relate:WIPEXC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Print
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      if loc:SerialsScanned = 0 and loc:SerialsScannedNotInLocation = 0 and loc:SerialsNotScanned = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not selected a report type.','ServiceBase',|
                       'mstop.jpg','/&OK')
              of 1 ! &OK Button
          End!Case Message
          cycle
      end
      
      if loc:SerialsScanned
          reportType = 'scanned'
          WIP_Audit_Report(auditNumber, reportType)
      end
      
      if loc:SerialsScannedNotInLocation
          reportType = 'not in location'
          WIP_Audit_Report(auditNumber, reportType)
      end
      
      if loc:SerialsNotScanned
          reportType = 'not scanned'
          WIP_Audit_Report(auditNumber, reportType)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020800'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020800'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020800'&'0')
      ***
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      if loc:SerialsScanned = 0 and loc:SerialsScannedNotInLocation = 0 and loc:SerialsNotScanned = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not selected a report type.','ServiceBase',|
                       'mstop.jpg','/&OK')
              of 1 ! &OK Button
          End!Case Message
          cycle
      end
      
      if loc:SerialsScanned
          reportType = 'scanned'
          do Export
      end
      
      if loc:SerialsScannedNotInLocation
          reportType = 'not in location'
          do Export
      end
      
      if loc:SerialsNotScanned
          reportType = 'not scanned'
          do Export
      end
      
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Allocations_Report PROCEDURE                          !Generated from procedure template - Window

StartDate            DATE
LastDate             DATE
RememberDate         DATE
FinalExportPath      STRING(255)
CurrentRow           LONG
ReportCancelled      BYTE
Local                CLASS
Drawbox              Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,7,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Main Stores Spares Allocation Report'),AT(168,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,73),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,84,352,250),USE(?Panel3),FILL(09A6A7CH)
                       BUTTON,AT(432,121),USE(?PopCalendar),FLAT,ICON('lookupp.jpg')
                       PROMPT('Spares Requested From:'),AT(228,126),USE(?StartDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(324,126,92,10),USE(StartDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Spares Requested To:'),AT(228,161),USE(?LastDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(324,162,92,10),USE(LastDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(432,156),USE(?PopCalendar:2),FLAT,ICON('lookupp.jpg')
                       PANEL,AT(164,335,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(376,336),USE(?ButtonOK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(448,336),USE(?ButtonCancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
MyExcel9             Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
Expo MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
TheExport           Routine

   FinalExportPath = SetReportsFolder('ServiceBase Export','Main Stores Spares Allocation Report',glo:WebJob)
    IF (FinalExportPath = '')
        Case Missive('An error occurred creating the report.'&|
            '|destination folder not set up'&|
            '|Please ty again.','ServiceBase',|
            'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT            
    END

    If MyExcel9.Init(0,0) = 0
        Case Missive('An error has occurred starting your Excel document.'&|
            '<13,10>'&|
            '<13,10>Please quit and try again.','ServiceBase 3g',|
            'mstop.jpg','/OK')
        Of 1 ! OK Button
        End ! Case Missive
        Exit
    End !If MyExcel9.Init(0,0,1) = 0


    Expo.OpenProgressWindow()
    
    Expo.UpdateProgressText()
    
    Expo.UpdateProgressText('Report Started: ' & FORMAT(Expo.StartDate,@d06) & |
        ' ' & FORMAT(Expo.StartTime,@t01))

    Expo.UpdateProgressText('',1)
    Expo.UpdateProgressText('Generating Excel file in process')


    IF EXISTS(clip(FinalExportPath)&'Main Stores Spares Allocation Report '&format(Today(),@d12)&'.xls') THEN REMOVE(clip(FinalExportPath)&'Main Stores Spares Allocation Report '&format(Today(),@d12)&'.xls').

    MyExcel9.NewWorkBook()
    MyExcel9.RenameWorkSheet('Allocations')


    MyExcel9.WriteToCell('Main Stores Spares Allocation Report'     ,'A1')
    MyExcel9.WriteToCell('Request Date From:'    ,'A3')
    MyExcel9.WriteToCell(Format(StartDate,@d06)  ,'B3')
    MyExcel9.WriteToCell('Request Date To:'      ,'A4')
    MyExcel9.WriteToCell(Format(LastDate,@d06)   ,'B4')
    MyExcel9.WriteToCell('Date Report Created:'  ,'A5')
    MyExcel9.WriteToCell(Format(today(),@D06)    ,'B5')

    !bold font and size for A1
    MyExcel9.SetCellFontSize(14,'A1')
    MyExcel9.SetCellFontStyle('Bold','A1')

    !colouring
    Local.DrawBox('A1', 'G1', 'A1', 'G1', color:Silver)
    Local.DrawBox('A3', 'G3', 'A6', 'G6', color:Silver)
    Local.DrawBox('A8', 'G8', 'A9', 'G9', color:Silver)

    MyExcel9.WriteToCell('Job Number'                         ,'A9')
    MyExcel9.WriteToCell('Part Number'                        ,'B9')
    MyExcel9.WriteToCell('Description'                        ,'C9')
    MyExcel9.WriteToCell('Date and Time spares requested'     ,'D9')
    MyExcel9.WriteToCell('Date and Time spares allocated'     ,'E9')
    MyExcel9.WriteToCell('Name'                               ,'F9')
    MyExcel9.WriteToCell('Location'                           ,'G9')

    MyExcel9.SetColumnWidth('A','','20')   !job numner, needs this width due to "date Report Created:" above
    MyExcel9.SetColumnWidth('B','','15')   !part number
    MyExcel9.SetColumnWidth('C','','30')   !description
    MyExcel9.SetColumnWidth('D','','30')   !date and time
    MyExcel9.SetColumnWidth('E','','30')   !date and time
    MyExcel9.SetColumnWidth('F','','30')   !name
    MyExcel9.SetColumnWidth('G','','20')   !MAIN STORE

    CurrentRow = 10
    RememberDate = 0
    ReportCancelled = false

    Access:StockALX.clearkey(STLX:KeyRequestDateTime)
    STLX:RequestDate = StartDate
    STLX:RequestTime = 0
    set(STLX:KeyRequestDateTime,STLX:KeyRequestDateTime)
    Loop
    
        if access:StockALX.next() then break.
        if STLX:RequestDate > LastDate then break.
        if STLX:PartNo = 'EXCH' then cycle. !don't report on exchanges

        if STLX:RequestDate > RememberDate  !on to the next day

            RememberDate = STLX:RequestDate !reset the day

            !check for cancelation only at the end of every day
            IF (Expo.CancelPressed = 2)
                Expo.UpdateProgressText('===============')
                Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                    ' ' & FORMAT(clock(),@t1))
                ReportCancelled = true
                break
            ELSE
                Expo.UpdateProgressText('Adding: ' & format(STLX:RequestDate,@d06))
            END

        END

        Access:users.clearkey(use:User_Code_Key)
        use:User_Code =  STLX:AllocateUser
        if access:users.fetch(use:User_Code_Key)
            use:Forename = 'NOT'
            use:Surname  = 'KNOWN'
        END

        MyExcel9.WriteToCell(STLX:JobNo                                                       ,'A' & CurrentRow)
        MyExcel9.WriteToCell(STLX:PartNo                                                      ,'B' & CurrentRow)
        MyExcel9.WriteToCell(STLX:Description                                                 ,'C' & CurrentRow)
        MyExcel9.WriteToCell(format(STLX:RequestDate,@d06)&' '&format(STLX:RequestTime,@t1)   ,'D' & CurrentRow)
        MyExcel9.WriteToCell(format(STLX:AllocateDate,@d06)&' '&format(STLX:AllocateTime,@t1) ,'E' & CurrentRow)
        MyExcel9.WriteToCell(clip(use:Forename)&' '&clip(use:Surname)                         ,'F' & CurrentRow)
        MyExcel9.WriteToCell('MAIN STORE'                                                     ,'G' & CurrentRow)

        CurrentRow += 1

    END !loop through StockAlx
    

    MyExcel9.saveas(clip(FinalExportPath)&'Main Stores Spares Allocation Report '&format(Today(),@d12)&'.xls')
    MyExcel9.closeWorkbook()

!    message('Saved as: '& clip(FinalExportPath)&'Summary'&format(Today(),@d12)&'.xls')

    if  ReportCancelled then
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        Expo.FinishProgress()

    ELSE
        
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Report Completed: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        Expo.FinishProgress(FinalExportPath)
    END


    EXIT
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020780'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Allocations_Report')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STOCKALX.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  StartDate = today()
  LastDate  = today()
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCKALX.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
      Do TheExport
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020780'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020780'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020780'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          StartDate = TINCALENDARStyle1()
          Display(?StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LastDate = TINCALENDARStyle1()
          Display(?LastDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonOK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    MyExcel9.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code

!TL = top left, TR = Top right, BL = bottom left BR = bottom Right

    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    MyExcel9.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    MyExcel9.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    MyExcel9.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    MyExcel9.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    MyExcel9.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
!---------------------------------------------------------------------------------
MyExcel9.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
MyExcel9.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
MyExcel9.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Export_Bouncer_Hist  PROCEDURE                        ! Declare Procedure
Save_par_ID          USHORT
Save_war_ID          USHORT
Save_job_ID          USHORT
Save_not_ID          USHORT
tmp:Parts            STRING(30),DIM(12)
TempExportPath       STRING(255)
FinalExportPath      STRING(255)
ReportCancelled      BYTE
Print_Fault_Description STRING(255)
Print_Invoice_Text   STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
LocalFilename   cstring(500),STATIC

OUTFILE FILE,DRIVER('ASCII'),PRE(OUT),NAME(LocalFilename),CREATE
OUTFILERec RECORD
TextLine  STRING(255)
    end !Record  (Part of infile)
    end !infile

Expo MyExportClass
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:WARPARTS.Open
   Relate:PARTS.Open
   Relate:JOBNOTES_ALIAS.Open
   Relate:JOBS_ALIAS.Open
!Save places - just in case

    Save_par_ID = Access:PARTS.SaveFile()
    Save_war_ID = Access:WARPARTS.SaveFile()
    Save_job_ID = Access:Jobs_Alias.SaveFile()
    Save_not_ID = Access:JobNotes_Alias.SaveFile()

    Do TheExport            !put in a subroutine so I can use exit

    !finish off and tidy up
    Access:PARTS.RestoreFile(Save_par_ID)
    Access:WARPARTS.RestoreFile(Save_war_ID)
    Access:Jobs_Alias.RestoreFile(Save_job_ID)
    Access:JobNotes_Alias.RestoreFile(Save_not_ID)

   Relate:WARPARTS.Close
   Relate:PARTS.Close
   Relate:JOBNOTES_ALIAS.Close
   Relate:JOBS_ALIAS.Close
TheExport   Routine

    ReportCancelled = false

    TempExportPath = GetTempFolder()
    IF (TempExportPath = '')
        miss# =  Missive('An error occurred creating the report.'&|
            '|Temp folder not set up'&|
            '|Please ty again.','ServiceBase',|
            'mstop.jpg','/&OK') 
        EXIT            
    END

    FinalExportPath = SetReportsFolder('ServiceBase Export','Bouncer History Report',glo:WebJob)
    IF (FinalExportPath = '')
        Miss# = Missive('An error occurred creating the report.'&|
            '|destination folder not set up'&|
            '|Please ty again.','ServiceBase',|
            'mstop.jpg','/&OK') 
        EXIT            
    END

    !show the progress window
    Expo.OpenProgressWindow()
    Expo.UpdateProgressText()
    Expo.UpdateProgressText('Report Started: ' & FORMAT(Expo.StartDate,@d06) & ' ' & FORMAT(Expo.StartTime,@t01))
    Expo.UpdateProgressText()

    !created the ouput file
    LocalFilename = CLIP(TempExportPath)&'\Bouncer_History_'& clip(glo:Select2) &'.csv'
    if exists(localFilename) then remove(LocalFilename).
    create(outfile)
    Open(Outfile)

    !print job header - there is at least one entry
    out:Textline = '"Job Number","Date Booked","Date Completed","Date Despatched","Manufacturer","Model","Unit Type","IMEI",' & |
                    '"MSN","Mobile Number","Reported Fault","Repair Details","Chargeable Parts",,,,,,"Warranty Parts",,,,,,'
    Add(outfile)

    !set up criteria
    Access:Jobs_Alias.clearkey(job_ali:ESN_Key)
    Jobs_Alias:ESN = glo:Select2
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop

        if access:Jobs_alias.next() then break.
        if Job_Ali:ESN <> glo:Select2 then break.

        !Is this one in the tagged list?
        sort(glo:q_JobNumber,glo:q_JobNumber.GLO:Job_Number_Pointer)
        glo:q_JobNumber.GLO:Job_Number_Pointer = job_ali:Ref_Number
        get(glo:q_JobNumber,glo:q_JobNumber.GLO:Job_Number_Pointer)
        if error() then cycle.       !not found in the global queue

        !update the progress window
        IF (Expo.CancelPressed = 2)
            ReportCancelled = true
            break
        ELSE
            Expo.UpdateProgressText('Adding Job No: ' & CLIP(job_ali:Ref_Number))
        END

        Access:JOBNOTES_ALIAS.Clearkey(jbn_ali:RefNumberKey)
        jbn_ali:RefNumber   = job_ali:Ref_Number
        If Access:JOBNOTES_ALIAS.Tryfetch(jbn_ali:RefNumberKey) = Level:Benign
            ! Found
        End! If Access:JOBNOTES_ALIAS.Tryfetch(jbn_ali:RefNumberKey) = Level:Benign

       !original report allows for six chargeable and six warranty parts
       clear(tmp:parts)
        PartNumber#     = 0
       
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number  = job_ali:Ref_Number
        Set(par:Part_Number_Key, par:Part_Number_Key)
        Loop
           If Access:PARTS.NEXT() then break.
           If par:Ref_Number  <> job_ali:Ref_Number then break.

           PartNumber# += 1
           If PartNumber# > 6 then break.
           tmp:parts[PartNumber#] = par:Description
        End ! Loop
       

        PartNumber#     = 6
       
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job_ali:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
           If Access:WARPARTS.NEXT() then break.
           If wpr:Ref_Number  <>  job_ali:Ref_Number then break.

           PartNumber# += 1
           If PartNumber# > 12  then break.

           tmp:Parts[PartNumber#] = wpr:Description
        End ! Loop
       
        !Use bryan's function to strip out tabs, CR, LF etc
        Print_Fault_Description = BHStripNonAlphanum(jbn_ali:Fault_Description,' ')
        Print_Invoice_Text = BHStripNonAlphanum(jbn_ali:Invoice_Text,' ')

        !ready to export
        out:Textline =   '"' & clip(job_ali:Ref_Number)          &'","'&|   !Job Number,
                         format(job_ali:date_booked,@d06)        &'","'&|   !Date Booked,
                         format(job_ali:Date_Completed,@d06)     &'","'&|   !Date Completed,
                         format(job_ali:Date_Despatched,@d06)    &'","'&|   !Date Despatched,
                         clip(job_ali:Manufacturer)              &'","'&|   !Manufacturer,
                         clip(job_ali:Model_Number)              &'","'&|   !Model,
                         clip(job_ali:Unit_Type)                 &'","'&|   !Unit Type,
                         clip(job_ali:ESN)                       &'","'&|   !IMEI,
                         clip(job_ali:MSN)                       &'","'&|   !MSN,
                         clip(job_ali:Mobile_Number)             &'","'&|   !Mobile Number,
                         clip(Print_Fault_Description)           &'","'&|   !Reported Fault,
                         clip(Print_Invoice_Text)                &'","'&|   !Repair Details,
                         clip(tmp:Parts[1])                      &'","'&|   !Chargeable Parts,
                         clip(tmp:Parts[2])                      &'","'&|   !,
                         clip(tmp:Parts[3])                      &'","'&|   !,
                         clip(tmp:Parts[4])                      &'","'&|   !,
                         clip(tmp:Parts[5])                      &'","'&|   !,
                         clip(tmp:Parts[6])                      &'","'&|   !,
                         clip(tmp:Parts[7])                      &'","'&|   !Warranty Parts,
                         clip(tmp:Parts[8])                      &'","'&|   !,
                         clip(tmp:Parts[9])                      &'","'&|   !,
                         clip(tmp:Parts[10])                     &'","'&|   !,
                         clip(tmp:Parts[11])                     &'","'&|   !,
                         clip(tmp:Parts[12])                     &'"'
        Add(outfile)

    END !loop thorugh jobs by job_ali:ESN_Key

    Close(Outfile)

    if  ReportCancelled then

        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        Expo.FinishProgress()
        remove(LocalFilename)

    ELSE

        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Report Completed: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))

        if glo:Webjob then
            SendFileToClient(LocalFilename)
            Expo.FinishProgress()
            Remove(LocalFilename)
        ELSE
            copy(localFilename,FinalExportPath)
            Expo.FinishProgress(FinalExportPath)
        END  !if web job

    END !if report cancelled


    EXIT
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PreJobReport PROCEDURE                                !Generated from procedure template - Window

StartDate            DATE
EndDate              DATE
IncludeReceived      STRING('Y')
IncludeNotReceived   STRING('Y')
SelectedFranchise    STRING(20)
AllFranchises        STRING('Y')
tmp:ExportFileName   STRING(255),STATIC
FinalExportPath      STRING(255)
CurrentStatus        STRING(20)
CurrentBooking       STRING(20)
CurrentFranchise     STRING(30)
CurrentHeadNo        STRING(30)
CurrentHeadName      STRING(30)
CurrentTransit       STRING(30)
CurrentChargeable    STRING(30)
CurrentWarranty      STRING(30)
CurrentJobRef_number STRING(20)
ReportCancelled      BYTE
ProgressCount        LONG
FDB9::View:FileDrop  VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?SelectedFranchise
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(650,4,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(166,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Vodacom Portal Booking'),AT(170,71),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(466,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(166,84,352,244),USE(?Panel3),FILL(09A6A7CH)
                       GROUP,AT(182,97,330,73),USE(?GroupSelectFranchise),HIDE
                         STRING('Note: Selecting a Franchise is only relevant to Received jobs.'),AT(274,109),USE(?String1),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                         LIST,AT(274,130,210,10),USE(SelectedFranchise),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('38L(2)|M~Account~@s15@120L(2)|M~Company Name~@s30@'),DROP(5),FROM(Queue:FileDrop)
                         CHECK('All Franchises'),AT(274,151),USE(AllFranchises),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                       END
                       PROMPT('Start Date:'),AT(222,180),USE(?StartDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(274,180,124,10),USE(StartDate)
                       PROMPT('End Date:'),AT(222,206),USE(?EndDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(274,206,124,10),USE(EndDate)
                       CHECK('Include Received Jobs'),AT(274,238),USE(IncludeReceived),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                       CHECK('Include Not Received Jobs'),AT(274,260),USE(IncludeNotReceived),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                       PANEL,AT(166,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(451,332),USE(?ButtonClose),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(384,332),USE(?ButtonExport),TRN,FLAT,ICON('exportp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFileName),Create,Bindable,Thread
Record                  Record
ExportLine                    String(1000)
                        End
                    End

Expo MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
TheExport       Routine


    FinalExportPath = SetReportsFolder('ServiceBase Export','Vodacom Portal Report',glo:WebJob)
    IF (FinalExportPath = '')
        Miss# = Missive('An error occurred creating the report.'&|
            '|destination folder not set up'&|
            '|Please ty again.','ServiceBase',|
            'mstop.jpg','/&OK') 
        EXIT            
    END

    !progress window
    Expo.OpenProgressWindow()
    Expo.UpdateProgressText()
    Expo.UpdateProgressText('Export Started: ' & FORMAT(Expo.StartDate,@d06) & ' ' & FORMAT(Expo.StartTime,@t01))
    Expo.UpdateProgressText()
    ProgressCount = 0


                       !Vodacom_Portal_DDMMYYYY.csv.
    tmp:ExportFileName = 'Vodacom_Portal_'&format(today(),@D06-)&'.csv'
    Create(ExportFile)
    Open(ExportFile)

    !title
    expfil:ExportLine = 'Portal Ref,Job Number,Franchise,Date Pre-Booked,Date Received,Trade Acc,Trade Name,Transit Type,Chargeable,Warranty,Manufacturer,Model,IMEI,Status'
    Add(ExportFile)

    Access:PreJob.clearkey(PRE:KeyDateTime)
    PRE:Date_booked = StartDate
    PRE:Time_Booked = 0
    Set(PRE:KeyDateTime,PRE:KeyDateTime)
    Loop
        if access:Prejob.next() then break.
        if PRE:Date_booked > EndDate then break.

        if PRE:JobRef_number = -1 then cycle.       !don't iincluce the cancelled ones

        if PRE:JobRef_number < 1 and IncludeNotReceived = 'N' then cycle.
        if PRE:JobRef_number > 0 and IncludeReceived = 'N' then cycle.


        Case PRE:JobRef_number
            of 0

                CurrentStatus = 'NOT YET RECEIVED'
                CurrentFranchise    = ''
                CurrentHeadNo       = ''
                CurrentHeadName     = ''
                CurrentTransit      = ''
                CurrentChargeable   = ''
                CurrentWarranty     = ''
                CurrentBooking      = ''
                CurrentJobRef_number = ''

                !need to sort out warranty
                Access:Chartype.clearkey(cha:Charge_Type_Key)
                cha:Charge_Type = pre:ChargeType
                if access:Chartype.fetch(cha:Charge_Type_Key) = level:Benign
                    if cha:Warranty = 'YES' then
                        CurrentWarranty     = PRE:ChargeType
                    ELSE
                        CurrentChargeable   = PRE:ChargeType
                    END
                END


            else

                CurrentStatus = 'RECEIVED'
                CurrentJobRef_number = left(format(Pre:jobRef_number,@n_10))
            
                Access:jobs.clearkey(job:Ref_Number_Key)
                job:Ref_Number = PRE:JobRef_number
                if access:jobs.fetch(job:Ref_Number_Key)
                    !error
                    CurrentFranchise    = 'JOB NOT FOUND'
                    CurrentHeadNo       = 'JOB NOT FOUND'
                    CurrentHeadName     = 'JOB NOT FOUND'
                    CurrentTransit      = 'JOB NOT FOUND'
                    CurrentChargeable   = 'JOB NOT FOUND'
                    CurrentWarranty     = 'JOB NOT FOUND'
                    CurrentBooking      = 'JOB NOT FOUND'
                    
                ELSE
                    !Track down the trade account details
                    Access:SubTracc.clearkey(sub:Account_Number_Key)
                    sub:Account_Number = job:Account_number
                    if access:SubTracc.fetch(sub:Account_Number_Key)
                        CurrentHeadNo       = 'SUB NOT FOUND'
                        CurrentHeadName     = 'SUB NOT FOUND'
                    ELSE
                        Access:TradeAcc.clearkey(tra:Account_Number_Key)
                        tra:Account_Number = sub:Main_Account_number
                        if access:Tradeacc.fetch(tra:Account_Number_Key)
                            !error
                            CurrentHeadNo       = 'ACC NOT FOUND'
                            CurrentHeadName     = 'ACC NOT FOUND'
                            CurrentFranchise    = 'ACC NOT FOUND'      !Job:Account_Number
                        ELSe
                            CurrentHeadNo   = tra:Account_Number
                            CurrentHeadName = tra:Company_Name
                            CurrentFranchise = tra:BranchIdentification      !Job:Account_Number
                        END
                    END
                    
                    CurrentTransit      = job:Transit_type
                    CurrentChargeable   = job:Charge_Type
                    CurrentWarranty     = job:Warranty_charge_type
                    CurrentBooking      = format(PRE:Date_booked,@d06)
                END

        END !case pre:jobRef_number


        if AllFranchises = 'Y'
            !this goes through
        ELSE
            if PRE:JobRef_number > 0 then
                !only if this has been received does the limit apply
                if SelectedFranchise <> CurrentHeadNo then cycle.
            END
        END

        IF (Expo.CancelPressed = 2)
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Export Cancelled: ' & FORMAT(Today(),@d6) & |
                ' ' & FORMAT(clock(),@t1))
            Expo.FinishProgress()
            ReportCancelled = true
            break
        ELSE
            ProgressCount += 1
            !update display every ten records
            if progressCount % 10 = 0 then
                Expo.UpdateProgressText('Export At: ' & CLIP(ProgressCount))
            END
        END


        expfil:ExportLine = clip(PRE:VodacomPortalRef)         &','&|
                     clip(CurrentJobRef_number)         &','&|
                     clip(CurrentFranchise)             &','&|
                     format(PRE:Date_booked,@d06)       &','&|
                     clip(CurrentBooking)               &','&|
                     clip(CurrentHeadNo)                &','&|
                     clip(CurrentHeadName)              &','&|
                     clip(CurrentTransit)               &','&|
                     clip(CurrentChargeable)            &','&|
                     clip(CurrentWarranty)              &','&|
                     clip(PRE:Manufacturer)             &','&|
                     clip(PRE:Model_number)             &','&|
                     clip(PRE:ESN)                      &','&|
                     clip(CurrentStatus)     

        Add(ExportFile)

    END

    Close(ExportFile)

    if glo:Webjob then
        SendFileToClient(clip(tmp:ExportFileName))
    ELSE
        copy(clip(tmp:ExportFileName),clip(FinalExportPath))
    END


    if  ReportCancelled then
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Export Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        Expo.FinishProgress()

    ELSE
 
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Export Completed: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1)&' Records '&clip(ProgressCount))
        if glo:Webjob then
            Expo.FinishProgress()
        ELSE
            Expo.FinishProgress(FinalExportPath)
        END
    END


    exit
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020785'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PreJobReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:PREJOB.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !setup the defaults
  !dates month to day
  StartDate = today()-Day(today())+1
  EndDate   = today()
  
  
  If glo:WebJob
  
      !Track down the trade account details
      Access:SubTracc.clearkey(sub:Account_Number_Key)
      sub:Account_Number = ClarioNET:Global.Param2                    !job:Account_number
      if access:SubTracc.fetch(sub:Account_Number_Key)
          !error
      ELSE
          Access:TradeAcc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_number
          if access:Tradeacc.fetch(tra:Account_Number_Key)
              !error
          END
      END
  
      hide(?GroupSelectFranchise)
      AllFranchises = 'N'
  
  else
  
      Access:TRADEACC.Clearkey(tra:Account_Number_key)
      tra:Account_Number  = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
      Access:TRADEACC.Fetch(tra:Account_Number_key)
  
      unhide(?GroupSelectFranchise)
          
  END !if glo:Webjob
  
  SelectedFranchise = tra:Account_Number
  ?SelectedFranchise{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?AllFranchises{Prop:Checked} = True
    SelectedFranchise = ''
    HIDE(?SelectedFranchise)
  END
  IF ?AllFranchises{Prop:Checked} = False
    UNHIDE(?SelectedFranchise)
  END
  FDB9.Init(?SelectedFranchise,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:TRADEACC,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(tra:Account_Number_Key)
  FDB9.AddField(tra:Account_Number,FDB9.Q.tra:Account_Number)
  FDB9.AddField(tra:Company_Name,FDB9.Q.tra:Company_Name)
  FDB9.AddField(tra:RecordNumber,FDB9.Q.tra:RecordNumber)
  FDB9.AddUpdateField(tra:Account_Number,SelectedFranchise)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:PREJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020785'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020785'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020785'&'0')
      ***
    OF ?AllFranchises
      IF ?AllFranchises{Prop:Checked} = True
        SelectedFranchise = ''
        HIDE(?SelectedFranchise)
      END
      IF ?AllFranchises{Prop:Checked} = False
        UNHIDE(?SelectedFranchise)
      END
      ThisWindow.Reset
    OF ?ButtonClose
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonClose, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonClose, Accepted)
    OF ?ButtonExport
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonExport, Accepted)
      if IncludeReceived & IncludeNotReceived = 'NN' then
          miss# = missive('Please tick to include Received Jobs / Not Received Jobs or both','ServiceBase 3g','mexclam.jpg','OK')
      ELSE
          Do TheExport
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonExport, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDB9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(9, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if tra:Account_number <> 'AA20'
      if (tra:RemoteRepairCentre <> 1) or (tra:Account_Number = 'XXXRRC') then return(record:filtered).
      if tra:Stop_Account = 'YES' then return(Record:filtered).
  END !if not AA20 which should go throouh
  ! After Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(9, ValidateRecord, (),BYTE)
  RETURN ReturnValue

GetSBFolder          PROCEDURE  (SentSubFolder)       ! Declare Procedure
tmp:DesktopFolder    CSTRING(255)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!(SentSubFolder) if not blank include in the return string

    SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 5, FALSE )
    tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Export'
    If ~Exists(tmp:DesktopFolder)
        If ~MkDir(tmp:DesktopFolder)
            ! Can't create desktop folder
            Case Missive('An error has occured finding, or creating the ServiceBase folder.'&|
                '|' & Clip(Clip(tmp:DesktopFolder)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message

            Return('')

        End ! If ~MkDir(tmp:Desktop)
    End ! If ~Exists(tmp:Desktop)


    if clip(SentSubFolder) <> ''
        tmp:DesktopFolder = tmp:DesktopFolder & '\' & Clip(SentSubFolder)
        If ~Exists(tmp:DesktopFolder)
            If ~MkDir(tmp:DesktopFolder)
                ! Can't create desktop folder
                Case Missive('An error has occured finding, or creating the Subfolder for the Export.'&|
                    '|' & Clip(Clip(tmp:DesktopFolder)) & ''&|
                    '|'&|
                    '|Please quit and try again.','ServiceBase',|
                               'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                End!Case Message

                Return('')

            End ! If ~MkDir(tmp:Desktop)
        End ! If ~Exists(tmp:Desktop)
    END !if SentSubFolder not blank
    

    Return(tmp:DesktopFolder)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
