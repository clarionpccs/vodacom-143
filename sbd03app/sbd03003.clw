

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03003.INC'),ONCE        !Local module procedure declarations
                     END








Exchange_Unit_Audit_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
Unit_Group           GROUP,PRE()
stock_unit_temp      STRING(60)
replacement_unit_temp STRING(60)
stock_model_temp     STRING(30)
stock_status_temp    STRING(30)
replacement_model_temp STRING(30)
replacement_status_temp STRING(30)
                     END
lines_total_temp     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Stock Type:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5781,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?String32),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5781,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@n-7),AT(5781,781),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6250,781),USE(?String34),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6458,781,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,188),USE(?detailband)
                           STRING(@s8),AT(104,0),USE(exa:Audit_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s16),AT(729,0),USE(stock_unit_temp),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(3958,0),USE(replacement_unit_temp),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s18),AT(4844,0),USE(replacement_model_temp),TRN,FONT(,7,,)
                           STRING(@s24),AT(5833,0),USE(replacement_status_temp),TRN,LEFT,FONT(,7,,)
                           STRING(@s24),AT(2604,0),USE(stock_status_temp),TRN,FONT(,7,,)
                           STRING(@s18),AT(1615,0),USE(stock_model_temp),TRN,LEFT,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(219,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,156),USE(?String27),TRN
                           STRING(@s8),AT(1667,156),USE(lines_total_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE UNIT AUDIT REPORT'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Audit No'),AT(156,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Stock Unit'),AT(729,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Replacement Unit'),AT(3958,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Status'),AT(5833,2083),USE(?string24:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Status'),AT(2604,2083),USE(?string45:3),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Unit_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:EXCAUDIT.Open
  Relate:DEFAULTS.Open
  Access:EXCHANGE.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  
  
  RecordsToProcess = RECORDS(EXCAUDIT)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(EXCAUDIT,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(exa:Audit_Number_Key)
      Process:View{Prop:Filter} = |
      'UPPER(exa:Stock_Type) = UPPER(GLO:Select1)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 1
        Clear(Unit_Group)
        
        If exa:stock_unit_number <> ''
            access:exchange.clearkey(xch:ref_number_stock_key)
            xch:stock_type = glo:select1
            xch:ref_number = exa:stock_unit_number
            if access:exchange.tryfetch(xch:ref_number_stock_key) = Level:Benign
                stock_unit_temp  = xch:esn
                stock_model_temp = xch:model_number
                Case xch:available
                    Of 'AVL'
                        If glo:select2 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'AVAILABLE'
                    Of 'INC'
                        If glo:select3 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'INCOMING - JOB: ' & CLip(xch:job_number)
                    Of 'REP'
                        If glo:select4 <> 'YES'
                            print# = 0
                        End!If glo:select4 <> 'YES'
                        stock_status_temp = 'IN REPAIR - JOB: ' & Clip(xch:job_number)
                    Of 'EXC'
                        If glo:select5 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'EXCHANGED - JOB: ' & CLip(xch:job_number)
                    Of 'DES'
                        If glo:select6 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'DESPATCHED - JOB: ' & Clip(xch:job_number)
                    Of 'SUS'
                        If glo:select7 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'SUSPENDED'
                End!Case xch:available
        
            end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        
            If exa:replacement_unit_number <> ''
                access:exchange.clearkey(xch:ref_number_stock_key)
                xch:stock_type = glo:select1
                xch:ref_number = exa:replacement_unit_number
                if access:exchange.fetch(xch:ref_number_stock_key) = Level:Benign
                    replacement_unit_temp  = xch:esn
                    replacement_model_temp = xch:model_number
                    Case xch:available
                        Of 'AVL'
                            replacement_status_temp = 'AVAILABLE'
                        Of 'INC'
                            replacement_status_temp = 'INCOMING - JOB: ' & CLip(xch:job_number)
                        Of 'REP'
                            replacement_status_temp = 'IN REPAIR - JOB: ' & CLip(xch:Job_number)
                        Of 'EXC'
                            replacement_status_temp = 'EXCHANGED - JOB: ' & CLip(xch:job_number)
                        Of 'DES'
                            replacement_status_temp = 'DESPATCHED - JOB: ' & Clip(xch:job_number)
                        Of 'SUS'
                            replacement_status_temp = 'SUSPENDED'
                    End!Case xch:available
                end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
            Else
                replacement_status_temp = ''
            End!If exa:replacement_unit_number <> ''
        
            If print# = 1
                lines_total_temp += 1
                tmp:RecordsCount += 1
                Print(rpt:detail)
            End!If print# = 1
        End!If exa:stock_unit_number <> ''
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(EXCAUDIT,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCAUDIT.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'EXCAUDIT')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Exchange_Unit_Audit_Report'
  END
  report{Prop:Preview} = PrintPreviewImage







SetDefault PROCEDURE                                  !Generated from procedure template - Window

FilesOpened          BYTE
defaultPrinter       CSTRING(256)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Set Default Printer'),AT(,,291,64),FONT('MS Sans Serif',8,,FONT:regular),GRAY,RESIZE
                       PROMPT('Current Default Printer:'),AT(11,6),USE(?Prompt1)
                       STRING(@s255),AT(9,17,277,10),USE(defaultPrinter)
                       BUTTON('Pick a printer to set it as the Windows default Printer'),AT(57,37,176,14),USE(?Button1)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!EnumPtrcl        EnumPrtClType

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SetDefault')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
!  stop(3)
!  EnumPtrCl.init()
!  stop(4)
!  EnumPtrCl.GetDefaultPrinter(DefaultPrinter)

  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
!  EnumPtrCl.kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
!      If EnumPtrCl.SetDefaultPrinter(1)  then
!        Message('error setting default printer')
!      end
!      EnumPtrCl.GetDefaultPrinter(DefaultPrinter)
!      display(?defaultPrinter)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Job_Pending_Parts_Criteria PROCEDURE                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Model_Number_Pointer          LIKE(glo:Model_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
tag_temp             STRING(1)
trade_account_temp   STRING('ALL')
manfuacturer_temp    STRING(30)
single_page_temp     STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?manfuacturer_temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB4::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
BRW3::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Job Pending Parts Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Units In Channel Criteria'),USE(?Tab1)
                           OPTION('Trade Account'),AT(168,113,260,28),USE(trade_account_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(183,126),USE(?trade_account_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Individual'),AT(215,126),USE(?trade_account_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                           END
                           COMBO(@s40),AT(284,126,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,160),FROM(Queue:FileDropCombo)
                           SHEET,AT(168,145,340,181),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                             TAB('By Model Number'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                             TAB('By Manufacturer'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               COMBO(@s30),AT(304,161,124,10),USE(manfuacturer_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                             END
                           END
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Job Pending Parts Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(436,280),USE(?DASUNTAGALL),TRN,FLAT,LEFT,KEY(AltU),ICON('untagalp.jpg')
                       BUTTON('&Rev tags'),AT(284,258,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(332,258,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(436,244),USE(?DASTAGAll),TRN,FLAT,LEFT,KEY(AltA),ICON('tagallp.jpg')
                       BUTTON,AT(436,208),USE(?DASTAG),TRN,FLAT,LEFT,KEY(AltT),ICON('tagitemp.jpg')
                       ENTRY(@s30),AT(172,162,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       LIST,AT(172,174,256,148),USE(?List),IMM,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11LI@s1@116L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
   GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  IF ERRORCODE()
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Q_ModelNumber)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Q_ModelNumber)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_ModelNumber)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_ModelNumber)
    GET(GLO:Q_ModelNumber,QR#)
    DASBRW::6:QUEUE = GLO:Q_ModelNumber
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Q_ModelNumber)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Model_Number_Pointer = mod:Model_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Model_Number_Pointer)
    IF ERRORCODE()
       GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
       ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
show_hide       Routine
    Case trade_account_temp
        Of 'ALL'
            Disable(?glo:select1)
        Of 'IND'
            Enable(?glo:select1)
    End!Case trade_account_temp
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020165'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Job_Pending_Parts_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?trade_account_temp:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
    glo:select1 = ''
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,mod:Manufacturer_Key)
  BRW3.AddRange(mod:Manufacturer,manfuacturer_temp)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?MOD:Model_Number,mod:Model_Number,1,BRW3)
  BRW3.AddSortOrder(,mod:Model_Number_Key)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,mod:Model_Number,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(mod:Model_Number,BRW3.Q.mod:Model_Number)
  BRW3.AddField(mod:Manufacturer,BRW3.Q.mod:Manufacturer)
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(sub:Account_Number_Key)
  FDCB1.AddField(sub:Account_Number,FDCB1.Q.sub:Account_Number)
  FDCB1.AddField(sub:Company_Name,FDCB1.Q.sub:Company_Name)
  FDCB1.AddField(sub:RecordNumber,FDCB1.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB4.Init(manfuacturer_temp,?manfuacturer_temp,Queue:FileDropCombo:1.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:1
  FDCB4.AddSortOrder(man:Manufacturer_Key)
  FDCB4.AddField(man:Manufacturer,FDCB4.Q.man:Manufacturer)
  FDCB4.AddField(man:RecordNumber,FDCB4.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ModelNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?trade_account_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
    OF ?manfuacturer_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manfuacturer_temp, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manfuacturer_temp, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020165'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020165'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020165'&'0')
      ***
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If ~Records(glo:Q_ModelNumber)
          Case Missive('You must select at least ONE Model Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If ~Record(glo:Queue)
      IF error# = 0
          If trade_account_temp = 'ALL'
              glo:select1 = ''
          End
          Job_Pending_Parts_Report
      End!IF error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::6:DASUNTAGALL
      Do show_hide
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

Income_Report_Criteria PROCEDURE                      !Generated from procedure template - Window

FilesOpened          BYTE
tag_temp             STRING(1)
trade_account_temp   BYTE(0)
manfuacturer_temp    STRING(30)
single_page_temp     STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccountType      BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select6
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB8::View:FileDropCombo VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
window               WINDOW('Income Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Income Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Units In Channel Criteria'),USE(?Tab1)
                           OPTION('Trade Account'),AT(211,130,111,28),USE(trade_account_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(222,142),USE(?trade_account_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Individual'),AT(262,142),USE(?trade_account_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION,AT(210,154,20,32),USE(tmp:AccountType)
                             RADIO,AT(214,162),USE(?Option3:Radio1),VALUE('0')
                             RADIO,AT(214,174),USE(?Option3:Radio2),VALUE('1')
                           END
                           COMBO(@s40),AT(346,162,124,10),USE(GLO:Select6),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('67L(2)|M@s15@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Header Account Number'),AT(234,162),USE(?HeadAccountNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Sub Account Number'),AT(234,174),USE(?SubAccountNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s40),AT(346,174,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,160),FROM(Queue:FileDropCombo)
                           OPTION('Invoice Types To Include'),AT(210,198,236,48),USE(?Option2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           CHECK('Service Invoices'),AT(310,210),USE(GLO:Select4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Retail Invoices'),AT(310,226),USE(GLO:Select5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           PROMPT('Start Date'),AT(210,254),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(310,254,64,10),USE(GLO:Select1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(378,250),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(210,274),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(310,274,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(378,270),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
show_hide       Routine
    Case trade_account_temp
        Of 0
            ?tmp:AccountType{prop:Disable} = 1
            ?HeadAccountNumber:Prompt{prop:Disable} = 1
            ?SubAccountNumber:Prompt{prop:Disable} = 1
            ?glo:Select3{prop:Disable} = 1
            ?glo:Select6{prop:Disable} = 1
        Of 1
            ?tmp:AccountType{prop:Disable} = 0
            Post(Event:Accepted,?tmp:AccountType)
    End!Case trade_account_temp
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020163'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Income_Report_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select1 = Deformat('1/1/1998',@d6)
  glo:select2  = Today()
  glo:select3 = ''
  glo:select4 = 'YES'
  glo:select5 = 'YES'
  if ~glo:WebJob then
      ?glo:select6{prop:disable}=true
      trade_account_temp = 1
      ?trade_account_temp{Prop:disable}=true
      do show_hide
  End !if glo:WebJob
  Bryan.CompFieldColour()
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB2.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(sub:Account_Number_Key)
  FDCB2.AddField(sub:Account_Number,FDCB2.Q.sub:Account_Number)
  FDCB2.AddField(sub:Company_Name,FDCB2.Q.sub:Company_Name)
  FDCB2.AddField(sub:RecordNumber,FDCB2.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB8.Init(GLO:Select6,?GLO:Select6,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:TRADEACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(tra:Account_Number_Key)
  FDCB8.AddField(tra:Account_Number,FDCB8.Q.tra:Account_Number)
  FDCB8.AddField(tra:Company_Name,FDCB8.Q.tra:Company_Name)
  FDCB8.AddField(tra:RecordNumber,FDCB8.Q.tra:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020163'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020163'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020163'&'0')
      ***
    OF ?trade_account_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
    OF ?tmp:AccountType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountType, Accepted)
      Case tmp:AccountType
          Of 0
              ?HeadAccountNumber:Prompt{prop:Disable} = 0
              ?SubAccountNumber:Prompt{prop:Disable} = 1
              ?glo:Select6{prop:Disable} = 0
              ?glo:Select3{prop:Disable} = 1
      
          Of 1
              ?HeadAccountNumber:Prompt{prop:Disable} = 1
              ?SubAccountNumber:Prompt{prop:Disable} = 0
              ?glo:Select6{prop:Disable} = 1
              ?glo:Select3{prop:Disable} = 0
      
      End !tmp:AccountType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountType, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select4 = 'NO' And glo:select5 = 'NO'
          MsgExButton# = Missive('You must select at least ONE Invoice Type.','ServiceBase 3g',|
               'mstop.jpg','/OK')
          Error# = 1
      End!If glo:select4 = 'NO' And glo:select5 = 'NO'
      IF error# = 0
          Income_Report(Trade_Account_Temp,tmp:AccountType)
      End!IF error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select4 = ''
      glo:select5 = ''
      Post(event:Closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do show_hide
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






Exchange_Units_In_Channel_Fix_Later PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Exchange_Queue       QUEUE,PRE(excque)
model_number         STRING(30)
request_received     REAL
same_day             REAL
other_day            REAL
received_in_workshop REAL
                     END
In_Repair_Queue      QUEUE,PRE(repque)
model_number         STRING(30)
in_stock             REAL
in_workshop          REAL
awaiting_parts       REAL
RTM                  REAL
                     END
first_page_temp      BYTE(1)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
tmp:PrintedBy        STRING(60)
account_number_temp  STRING(60)
model_number_temp    STRING(30)
requests_received_temp STRING(6)
same_day_temp        STRING(6)
other_day_temp       STRING(6)
Failed_Exchange_Temp STRING(6)
Despatched_Temp      STRING(6)
In_Workshop_Temp     STRING(6)
Awaiting_Arrival_Temp STRING(6)
In_Stock_Temp        STRING(6)
Units_In_Workshop_Temp STRING(6)
Received_In_Workshop_Temp STRING(6)
Awaiting_Parts_Temp  STRING(6)
RTM_Temp             STRING(6)
In_Repair_Total      STRING(6)
In_Channel_Temp      STRING(6)
Removed_From_Stock_Temp STRING(6)
In_Scheme_Temp       STRING(6)
Manufacturer_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT('Exchange Units In Channel Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,750,7521,1771),USE(?unnamed)
                         STRING('Trade Account:'),AT(4948,208),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,208),USE(account_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(4948,469),USE(?String23),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,469),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6406,469),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4948,729),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,729),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,990),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1250),USE(?string59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5729,1250),USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(5729,990),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,7208),USE(?detailband)
                           STRING('MODEL NUMBER:'),AT(677,0),USE(?model_string),TRN,FONT(,9,,FONT:bold)
                           STRING(@s30),AT(1875,0),USE(model_number_temp),TRN,FONT(,9,,FONT:bold)
                           STRING('EXCHANGE TRANSACTION'),AT(677,365),USE(?String28),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,677),USE(requests_received_temp),TRN,RIGHT
                           STRING('IN TRANSIT'),AT(698,2135),USE(?String28:2),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,2396),USE(Despatched_Temp),TRN,RIGHT
                           STRING('IN REPAIR'),AT(698,3750),USE(?String28:3),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,3958),USE(In_Stock_Temp),TRN,RIGHT
                           STRING('GENERAL DETAILS'),AT(677,5729),USE(?String28:4),TRN,FONT(,9,,FONT:bold)
                           STRING('Exchange Units In Channel:'),AT(1875,5990),USE(?String41),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,5990),USE(In_Channel_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,6198,500,0),USE(?Line1:5),COLOR(COLOR:Black)
                           STRING('Exchange Units In Stock:'),AT(1875,3958),USE(?String36),TRN
                           STRING('Exchange Units In Workshop:'),AT(1875,4271),USE(?String37),TRN
                           STRING(@s6),AT(5906,4271),USE(In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Parts:'),AT(1875,4583),USE(?String38),TRN
                           STRING(@s6),AT(5906,4583),USE(Awaiting_Parts_Temp),TRN,RIGHT
                           STRING('Exchange Units R.T.M.:'),AT(1875,4896),USE(?String39),TRN
                           STRING(@s6),AT(5906,4896),USE(RTM_Temp),TRN,RIGHT
                           STRING('Total:'),AT(1875,5208),USE(?String45),TRN,FONT(,10,,FONT:bold)
                           STRING(@s6),AT(5906,5208),USE(In_Repair_Total),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Units Despatched:'),AT(1875,2396),USE(?String33),TRN
                           STRING('Exchange Units Received Into Workshop:'),AT(1875,2708),USE(?String34),TRN
                           STRING(@s6),AT(5906,2708),USE(Received_In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Arrival:'),AT(1875,3021),USE(?String35),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,3021),USE(Awaiting_Arrival_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Requests Received:'),AT(1875,677),USE(?String29),TRN
                           STRING('Exchange Units Despatched (Same Day):'),AT(1875,990),USE(?String30),TRN
                           STRING(@s6),AT(5906,990),USE(same_day_temp),TRN,RIGHT
                           STRING('Exchange Units Despatched (Not Same Day):'),AT(1875,1354),USE(?String31),TRN
                           STRING(@s6),AT(5906,1354),USE(other_day_temp),TRN,RIGHT
                           LINE,AT(5938,1667,500,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Failed Exchange Transactions:'),AT(1875,1719),USE(?String32),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,1719),USE(Failed_Exchange_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,2969,500,0),USE(?Line1:2),COLOR(COLOR:Black)
                           LINE,AT(5938,5156,500,0),USE(?Line1:3),COLOR(COLOR:Black)
                           LINE,AT(5938,5938,500,0),USE(?Line1:4),COLOR(COLOR:Black)
                         END
new_page                 DETAIL,PAGEBEFORE(-1),AT(,,,52),USE(?new_page)
                         END
first_page_title         DETAIL,AT(,,,656),USE(?first_page_title)
                           STRING('SELECTED MODEL NUMBERS'),AT(677,104),USE(?String28:5),TRN,FONT(,9,,FONT:bold)
                         END
first_page_detail        DETAIL,AT(,,,167),USE(?first_page_detail)
                           STRING(@s20),AT(1875,0),USE(GLO:Pointer),TRN,FONT(,8,,)
                           STRING(@s30),AT(3385,0),USE(Manufacturer_Temp),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE UNITS IN CHANNEL'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(135,250,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(135,406,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         IMAGE,AT(-21,-10,7521,11156),USE(?Image1)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,885),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1198,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1198),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Description'),AT(698,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Units In Channel'),AT(5906,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Units_In_Channel_Fix_Later')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:TRANTYPE.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(job:date_booked_key)  + Records(Exchange)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:date_booked_key)
        job:date_booked = glo:select1
        set(job:date_booked_key,job:date_booked_key)
        loop
            if access:jobs.next()
               break
            end !if
            if job:date_booked > glo:select2      |
                then break.  ! end if
        
            recordsprocessed += 1
            Do DisplayProgress
        
            error# = 0
            If glo:select3 <> ''
                If job:account_number <> glo:select3
                    error# = 1
                End!If job:account_number <> glo:select3
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = glo:select3
                if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                    account_number_temp = Clip(sub:account_number) & '-' & Clip(sub:company_name)
                end
            Else!If glo:select <> ''
                account_number_temp = 'ALL'
            End!If glo:select <> ''
        
            If error# = 0
            !IF glo:select4 = YES, i.e. one model per page. Don't print the first (summary) page.
            !Otherwise, show the model on top of each page
                If glo:select4 <> 'YES'
                    Settarget(report)
                    Hide(?model_string)
                    model_number_temp = ''
                    Settarget()
                    If first_page_temp = 1
                        first_page_temp = 0
                        Print(rpt:first_page_title)
                        Sort(glo:Queue,glo:pointer)
                        Loop x# = 1 To Records(glo:Queue)
                            Get(glo:Queue,x#)
                            access:modelnum.clearkey(mod:model_number_key)
                            mod:model_number = glo:pointer
                            if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                                manufacturer_temp = mod:manufacturer
                            end!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                            Print(rpt:first_page_detail)
                        End!Loop x# = 1 To Records(glo:Queue)
                        Print(rpt:new_page)
                    End!If first_page_temp = 1
                End!If glo:select4 = 'YES'
        
            !Looping through jobs in date order.
        
            !First If glo:select4 = 'YES', means one page per model.
        
            !Check the queue of models and see if the job model is in it
                Sort(glo:Queue,glo:pointer)
                glo:pointer  = job:model_number
                Get(glo:Queue,glo:pointer)
                If ~Error()
                    request# = 0
                    access:trantype.clearkey(trt:transit_type_key)                                      !Check the transit type to see if the
                    trt:transit_type = job:transit_type                                                 !default status is '108 Exchange Unit
                    if access:trantype.fetch(trt:transit_type_key) = Level:benign                       !Required'.
                        If sub(trt:initial_status,1,3) = '108'
                            request# = 1                                                                !Request Received
                        End!If sub(trt:initial_status,1,3) = '108'
                    end!if access:trantype.fetch(trt:transit_type_key) = Level:benign
                    If glo:select4 = 'YES'
                        Clear(exchange_queue)                                                       !to the job. Hopefully this should tie
                        Sort(exchange_queue,excque:model_number)                                    !up with the requests, but there may be
                        excque:model_number = job:model_number                                      !a mismatch due to transit types being
                        Get(exchange_queue,excque:model_number)                                     !changed on the job.
                        If Error()                                                                  !Model Number doesn't exist
                            excque:model_number = job:model_number
                            If request# = 1
                                excque:request_received = 1
                            Else!If request# = 1
                                excque:request_received = 0
                            End!If request# = 1
                            If job:exchange_unit_number <> ''
                                If job:exchange_despatched <> ''
                                    If job:exchange_despatched = job:date_booked
                                        excque:same_day = 1
                                        excque:other_day = 0
                                    Else!If job:exchange_despatched = job:date_booked
                                        excque:other_day = 1
                                        excque:same_day = 0
                                    End!If job:exchange_despatched = job:date_booked
                                End!If job:exchange_despatched <> ''
        
                                If job:workshop = 'YES'
                                    excque:received_in_workshop = 1
                                Else!If job:workshop = 'YES'
                                    excque:received_in_workshop = 0
                                End!If job:workshop = 'YES'
                            End!If job:exchange_unit_number <> ''
                            Add(Exchange_Queue)
        
                        Else!If Error()                                                             !Model number does exist
                            If request# = 1
                                excque:request_received += 1
                            End
                            If job:exchange_unit_number <> ''
                                If job:exchange_despatched <> ''
                                    If job:exchange_despatched = job:date_booked
                                        excque:same_day += 1
                                    Else!If job:exchange_despatched = job:date_booked
                                        excque:other_day += 1
                                    End!If job:exchange_despatched = job:date_booked
                                End!If job:exchange_despatched <> ''
        
                                If job:workshop = 'YES'
                                    excque:received_in_workshop += 1
                                End!If job:workshop = 'YES'
                            End!If job:exchange_unit_number <> ''
                            Put(Exchange_Queue)
                        End!If Error()
                    Else!If glo:select4 = 'YES'
                        If request# = 1
                            requests_received_temp += 1
                        End!If request# = 1
                        If job:exchange_unit_number <> ''
                            If job:exchange_despatched <> ''
                                If job:exchange_despatched = job:date_booked
                                    same_day_temp += 1
                                Else!If job:exchange_despatched = job:date_booked
                                    other_day_temp += 1
                                End!If job:exchange_despatched = job:date_booked
                            End!If job:exchange_despatched <> ''
                            If job:workshop = 'YES'
                                Received_In_Workshop_Temp += 1
                            End!If job:workshop = 'YES'
                        End!If job:exchange_unit_number <> ''
                    End!If glo:select4 = 'YES'
                End!If ~Error()
            End!If error# = 0
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        in_stock_temp = 0
        in_workshop_temp = 0
        Awaiting_Parts_Temp = 0
        RTM_Temp = 0
        setcursor(cursor:wait)
        
        save_xch_id = access:exchange.savefile()
        set(xch:ref_number_key)
        loop
            if access:exchange.next()
               break
            end !if
            RecordsProcessed += 1
            Do DisplayProgress
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            Sort(glo:Queue,glo:pointer)
            glo:pointer = xch:model_number
            Get(glo:Queue,glo:pointer)
            If error()
                Cycle
            End
            If glo:select4 = 'YES'                                                               !If One Model Per Page. Build up the
                Clear(in_repair_queue)
                Sort(in_repair_queue,repque:model_number)                                           !In Repair Queue.
                repque:model_number = xch:model_number
                Get(in_repair_queue,repque:model_number)
                If Error()                                                                          !Check the model number of the exchange
                    repque:model_number = xch:model_number                                          !unit. Add if not found, put if found.
                    If xch:available = 'AVL'
                        repque:in_stock = 1
                    Else
                        repque:in_stock = 0
                    End
                    Add(in_repair_queue)
                Else
                    If xch:available = 'AVL'
                        repque:in_stock += 1
                        Put(in_repair_queue)
                    End
                End!If Error()
            Else!If glo:select4 = 'YES'
                If xch:available = 'AVL'
                    in_stock_temp += 1
                End!If xch:job_number <> 0
            End!If glo:select4 = 'YES'
        
        
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = xch:job_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                If job:date_completed = ''
        !Model Dependent
                    workshop# = 0
                    If job:workshop = 'YES'
                        workshop# = 1
                    End!If job:workshop = 'YES'
                    setcursor(cursor:wait)
                    rtm# = 0
                    If job:third_party_site <> '' And job:workshop <> 'YES'
                        rtm# = 1
                    Else!If job:third_party_site <> '' And job:workshop <> 'YES'
                        found# = 0
                        save_wpr_id = access:warparts.savefile()
                        access:warparts.clearkey(wpr:part_number_key)
                        wpr:ref_number  = job:ref_number
                        set(wpr:part_number_key,wpr:part_number_key)
                        loop
                            if access:warparts.next()
                               break
                            end !if
                            if wpr:ref_number  <> job:ref_number      |
                                then break.  ! end if
                            yldcnt# += 1
                            if yldcnt# > 25
                               yield() ; yldcnt# = 0
                            end !if
                            If wpr:date_ordered <> '' and wpr:date_received = ''
                                found# = 1
                            Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                                If wpr:pending_ref_number <> '' and wpr:date_received = ''
                                    found# = 1
                                End!If wpr:pending_ref_number <> ''
                            End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                        end !loop
                        access:warparts.restorefile(save_wpr_id)
                        setcursor()
        
                        If found# = 0
                            setcursor(cursor:wait)
                            save_par_id = access:parts.savefile()
                            access:parts.clearkey(par:part_number_key)
                            par:ref_number  = job:ref_number
                            set(par:part_number_key,par:part_number_key)
                            loop
                                if access:parts.next()
                                   break
                                end !if
                                if par:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                yldcnt# += 1
                                if yldcnt# > 25
                                   yield() ; yldcnt# = 0
                                end !if
                                If par:date_ordered <> '' and par:date_received = ''
                                    found# = 1
                                Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                                    If par:pending_ref_number <> '' and par:date_received = ''
                                        found# = 1
                                    End!If wpr:pending_ref_number <> ''
                                End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                            end !loop
                            access:parts.restorefile(save_par_id)
                            setcursor()
                        End!If found# = 0
        
                    End!If job:third_party_site <> '' And job:workshop <> 'YES'
        
                    If glo:select4 = 'YES'
                        Clear(in_repair_queue)
                        Sort(in_repair_queue,repque:model_number)
                        repque:model_number = xch:model_number
                        Get(in_repair_queue,repque:model_number)
                        If workshop# = 1
                            repque:in_workshop += 1
                        End!If job:workshop = 'YES'
                        If rtm# = 1
                            repque:rtm += 1
                        End!If rtm# = 1
                        If found# = 1
                            repque:awaiting_parts += 1
                        End!If found# = 1
                        Put(in_repair_queue)
                    Else!If glo:select4 = 'YES'
                        If workshop# = 1
                            in_workshop_temp += 1
                        End!If job:workshop = 'YES'
                        If rtm# = 1
                            rtm_temp += 1
                        End!If rtm# = 1
                        If found# = 1
                            awaiting_parts_temp += 1
                        End!If found# = 1
        
                    End!If glo:select4 = 'YES'
        
                End!If job:date_completed = ''
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
        end !loop
        access:exchange.restorefile(save_xch_id)
        setcursor()
        
        RecordsToProcess += Records(glo:Queue)
        !If One Model Per Page
        If glo:select4 = 'YES'
        
            first_page# = 1
            Sort(glo:Queue,glo:pointer)                                                       !Loop through the global list of models
            Loop x# = 1 to Records(glo:Queue)                                                    !so that even if there aren't any records
                Get(glo:Queue,x#)
                recordsprocessed += 1
                Do DisplayProgress                                                   !in the exchange queue, a page will still
                in_stock_temp = 0
                in_workshop_temp = 0
                Awaiting_Parts_Temp = 0
                RTM_Temp = 0
                requests_received_temp  = 0
                same_day_temp           = 0
                other_day_temp          = 0
                received_in_workshop_temp = 0
                Clear(exchange_queue)
                Sort(exchange_queue,excque:model_number)                                            !be printed for the in stock bit.
                excque:model_number = glo:pointer
                Get(exchange_queue,excque:model_number)
                model_number_temp       = excque:model_number
                requests_received_temp  = excque:request_received
                same_day_temp           = excque:same_day
                other_day_temp          = excque:other_day
                received_in_workshop_temp = excque:received_in_workshop
                Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
                Despatched_Temp = same_day_temp + other_day_temp
                Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
                Clear(in_repair_queue)
                Sort(in_repair_queue,repque:model_number)
                Loop y# = 1 To Records(in_repair_queue)
                    Get(in_repair_queue,y#)
                    If repque:model_number <> excque:model_number
                        Cycle
                    End
                    in_stock_temp   += repque:in_stock
                    in_workshop_temp    += repque:in_workshop
                    awaiting_parts_temp += repque:awaiting_parts
                    rtm_temp    += repque:RTM
                End!Loop x# = 1 To Records(in_repair_queue)
                In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
                In_Channel_Temp = awaiting_arrival_temp + in_repair_total
                tmp:RecordsCount += 1
                Print(rpt:Detail)
                If x# <> Records(glo:Queue)
                    Print(rpt:new_page)
                End!If first_page# = 1
            End!Loop x# = 1 To Records(exchange_queue)
        
        Else!If glo:select4 = 'YES'
            Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
            Despatched_Temp = same_day_temp + other_day_temp
            Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
            In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
            In_Channel_Temp = awaiting_arrival_temp + in_repair_total
            tmp:RecordsCount += 1
            Print(rpt:Detail)
        End!If glo:select4 = 'YES'
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:TRANTYPE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Exchange_Units_In_Channel_Fix_Later'
  END
  report{Prop:Preview} = PrintPreviewImage













QA_Failed PROCEDURE
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
qa_reason_temp       STRING(255)
engineer_temp        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Repair_Type)
                       PROJECT(job_ali:Repair_Type_Warranty)
                       PROJECT(job_ali:Third_Party_Site)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:who_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(406,1042,7521,9781),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
endofreportbreak       BREAK(endofreport)
                         HEADER,AT(0,0,,583),USE(?unnamed)
                           STRING('3rd Party Repair'),AT(2604,0),USE(?Third_Party_Repair),TRN,HIDE,FONT(,22,,FONT:bold)
                         END
detail                   DETAIL,AT(,,,9115),USE(?detailband)
                           STRING('Job Number:'),AT(313,208),USE(?String2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(2917,313,1771,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(4792,313),USE(job_ali:Ref_Number,,?JOB_ALI:Ref_Number:2),TRN,LEFT,FONT(,12,,FONT:bold)
                           STRING('Unit Details:'),AT(313,656),USE(?String2:2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING('Date Booked:'),AT(2917,760),USE(?String6),TRN,FONT(,12,,FONT:bold)
                           STRING(@d6b),AT(4792,760),USE(job_ali:date_booked),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s3),AT(5781,760),USE(job_ali:who_booked),FONT(,12,,)
                           STRING('Manufacturer:'),AT(2917,1073),USE(?String6:2),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,1073),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Model Number:'),AT(2917,1385),USE(?String6:3),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,1385),USE(job_ali:Model_Number),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Unit Type:'),AT(2917,1698),USE(?String6:4),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,1698),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('I.M.E.I. Number:'),AT(2917,2010),USE(?String6:5),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,2010),USE(job_ali:ESN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('M.S.N.'),AT(2917,2323),USE(?String6:6),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,2323),USE(job_ali:MSN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Chargeable Type:'),AT(2917,2740),USE(?String6:7),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,2740),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s30),AT(4792,3000),USE(job_ali:Repair_Type),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Repair Type:'),AT(2917,3000),USE(?String6:8),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,3281),USE(job_ali:Warranty_Charge_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Warranty Type:'),AT(2917,3281),USE(?String6:9),TRN,FONT(,12,,FONT:bold)
                           STRING('Repair Type:'),AT(2917,3542),USE(?String6:10),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,3542),USE(job_ali:Repair_Type_Warranty),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s60),AT(4792,4552),USE(tmp:PrintedBy),FONT(,12,,)
                           STRING(@s60),AT(4792,4292),USE(engineer_temp),TRN,FONT(,12,,)
                           STRING(@s30),AT(4792,3927),USE(job_ali:Third_Party_Site),TRN,HIDE,FONT(,12,,)
                           STRING('3rd Party Agent:'),AT(2917,3927),USE(?Third_Party_Agent),TRN,HIDE,FONT(,12,,FONT:bold)
                           STRING('Original Engineer:'),AT(2917,4292),USE(?String6:13),TRN,FONT(,12,,FONT:bold)
                           STRING('Inspector:'),AT(2917,4552),USE(?String6:11),TRN,FONT(,12,,FONT:bold)
                           STRING('Failure Reason:'),AT(2917,4813),USE(?String6:12),TRN,FONT(,12,,FONT:bold)
                           TEXT,AT(4792,4844,2552,4167),USE(glo:Notes_Global),FONT(,8,,)
                           STRING('Repair Details:'),AT(260,2688),USE(?String2:3),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING('QA Details:'),AT(313,4292),USE(?String2:4),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('QA FAILURE'),AT(2031,52,3177,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('QA_Failed')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'QAFailure'
  If PrintOption(PreviewReq,glo:ExportReport,'QA Failure Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFPRINT.Open
  Access:AUDIT.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        ! Before Embed Point: %BeforePrintPreview) DESC(Before Print Preview) ARG()
        access:users.clearkey(use:user_code_key)
        use:user_code = job_ali:engineer
        access:users.fetch(use:user_code_key)
        engineer_temp = Clip(use:forename) & ' ' & Clip(use:surname)
        
        If job_ali:third_party_site <> ''
            Settarget(report)
            Unhide(?third_party_repair)
            Unhide(?third_party_agent)
            Unhide(?job_ali:third_party_site)
            Settarget()
        End!If job_ali:third_party_site <> ''
        ! After Embed Point: %BeforePrintPreview) DESC(Before Print Preview) ARG()
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFPRINT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(glo:select1)
  sequence2(code_temp,option_temp,bar_code_string_temp,bar_code_temp)
  
  
  setcursor(cursor:wait)
  save_aud_id = access:audit.savefile()
  access:audit.clearkey(aud:action_key)
  aud:ref_number = glo:select1
  aud:action     = 'QA REJECTION'
  set(aud:action_key,aud:action_key)
  loop
      !omemo(audit)
      if access:audit.next()
         break
      end !if
      if aud:ref_number <> glo:select1      |
      or aud:action     <> 'QA REJECTION'      |
          then break.  ! end if
      Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
      aud2:AUDRecordNumber = aud:Record_Number
      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
          qa_reason_temp = aud2:notes
          Break
      END ! IF
  
  end !loop
  access:audit.restorefile(save_aud_id)
  setcursor()
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:engineer
  access:users.fetch(use:user_code_key)
  engineer_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  
  If job_ali:third_party_site <> ''
      Settarget(report)
      Unhide(?third_party_repair)
      Unhide(?third_party_agent)
      Unhide(?job_ali:third_party_site)
      Settarget()
  End!If job_ali:third_party_site <> ''
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='QA_Failed'
  END
  report{Prop:Preview} = PrintPreviewImage













Retail_Sales_Valuation_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ret_id          USHORT,AUTO
save_res_id          USHORT,AUTO
retail_queue         QUEUE,PRE(retque)
User_Code            STRING(3)
type                 STRING(3)
record_number        LONG
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
item_cost_temp       REAL
line_cost_temp       REAL
quantity_total_temp  LONG
line_cost_total_temp REAL
item_cost_total_temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSTOCK)
                       PROJECT(res:Description)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Quantity)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,854,7521,1427),USE(?unnamed)
                         STRING('Date From:'),AT(5000,208),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,208),USE(GLO:Select1),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date To:'),AT(5000,417),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,625),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,625),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,833),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,833),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6458,833),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6),AT(5781,417),USE(GLO:Select2),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1042),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,1042),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6094,1042),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6302,1042,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s30),AT(990,-10),USE(res:Part_Number),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2917,0),USE(res:Description),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n8),AT(4844,0),USE(res:Quantity),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5885,0),USE(item_cost_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6708,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s3),AT(208,0),USE(retque:User_Code),TRN,FONT(,8,,,CHARSET:ANSI)
                         END
Back_Orders_Title        DETAIL,AT(,,,271),USE(?unnamed:5)
                           STRING('BACK ORDERS'),AT(104,52),USE(?String26),TRN,FONT(,9,,FONT:bold)
                         END
Sales_Transactions_Title DETAIL,AT(,,,260),USE(?unnamed:6)
                           STRING('SALES TRANSACTIONS'),AT(104,52),USE(?String26:2),TRN,FONT(,9,,FONT:bold)
                         END
totals                   DETAIL,USE(?unnamed:7)
                           LINE,AT(4792,52,2604,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n8),AT(4844,104),USE(quantity_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n10.2),AT(6708,104),USE(line_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('Totals (Ex V.A.T.)'),AT(3750,104),USE(?String37),TRN,FONT(,8,,FONT:bold)
                           STRING(@n10.2),AT(5885,104),USE(item_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('RETAIL SALES VALUATION REPORT'),AT(3854,0,3594,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('User Code'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(990,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2917,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Qty'),AT(5052,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost '),AT(5906,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6771,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Sales_Valuation_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSTOCK.Open
  Relate:DEFAULTS.Open
  Relate:USERS.Open
  Access:RETSALES.UseFile
  
  
  RecordsToProcess = RECORDS(RETSTOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETSTOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        setcursor(cursor:wait)
        save_ret_id = access:retsales.savefile()
        access:retsales.clearkey(ret:date_booked_key)
        ret:date_booked = glo:select1
        set(ret:date_booked_key,ret:date_booked_key)
        loop
            if access:retsales.next()
               break
            end !if
            if ret:date_booked > glo:select2      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            glo:pointer  = ret:who_booked
            Get(glo:Queue,glo:pointer)
            IF error()
                Cycle
            End!IF error()
        
            save_res_id = access:retstock.savefile()
            access:retstock.clearkey(res:part_number_key)
            res:ref_number  = ret:Ref_number
            set(res:part_number_key,res:part_number_key)
            loop
                if access:retstock.next()
                   break
                end !if
                if res:ref_number  <> ret:ref_number      |
                    then break.  ! end if
                retque:user_code    = ret:who_booked
                If res:despatched = 'YES'
                    retque:type = 'SAL'
                Else!If res:despatched = 'YES'
                    retque:type = 'BAK'
                End!If res:despatched = 'YES'
                retque:record_number    = res:record_number
                Add(retail_queue)
            end !loop
            access:retstock.restorefile(save_res_id)
        
        end !loop
        access:retsales.restorefile(save_ret_id)
        setcursor()
        
        Sort(retail_queue,retque:type,retque:user_code)
        
        back_order# = 0
        sales# = 0
        quantity_total_temp = 0
        line_cost_total_temp = 0
        item_cost_total_temp = 0
        
        Loop x# = 1 To Records(retail_queue)
            Get(retail_queue,x#)
            access:retstock.clearkey(res:record_number_key)
            res:record_number = retque:record_number
            If access:retstock.tryfetch(res:record_number_key) = Level:Benign
        
                If retque:type = 'BAK'
                    If back_order# = 0
                        Print(rpt:back_orders_title)
                        back_order# = 1
                        quantity_total_temp = 0
                        line_cost_total_temp = 0
                        item_cost_total_temp = 0
                    End!If back_order# = 0
                End!End!If retque:type = 'BAK'
        
                If retque:type = 'SAL'
                    If sales# = 0
                        Print(rpt:totals)
                        Print(rpt:sales_transactions_title)
                        sales# = 1
                        quantity_total_temp = 0
                        line_cost_total_temp = 0
                        item_cost_total_temp = 0
                    End!If back_order# = 0
                End!If retque:type = 'SAL'
                item_cost_temp  = res:item_cost
                line_cost_temp  = res:quantity * item_cost_temp
        
                quantity_total_temp += res:quantity
                item_cost_total_temp += res:item_cost
                line_cost_total_temp += (res:quantity * res:item_cost)
        
                Print(rpt:detail)
            end!If access:retstock.tryfetch(res:record_number_key) = Level:Benign
        
        End!Loop x# = 1 To Records(retail_queue)
        Print(rpt:totals)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSTOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:RETSALES.Close
    Relate:USERS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Sales_Valuation_Report'
  END
  report{Prop:Preview} = PrintPreviewImage







Retail_Sales_Valuation_Criteria PROCEDURE             !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
tag_temp             STRING(1)
trade_account_temp   STRING('ALL')
manfuacturer_temp    STRING(30)
single_page_temp     STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW3::View:Browse    VIEW(USERS)
                       PROJECT(use:User_Code)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Retail Sales Valuation Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(192,118,248,154),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),VCR,FORMAT('11LI@s1@41L(2)|M~User Code~@s3@94L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@'),FROM(Queue:Browse)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Units In Channel Criteria'),USE(?Tab1)
                           SHEET,AT(168,100,344,176),USE(?Sheet2),LEFT,COLOR(0D6E7EFH),SPREAD,UP
                             TAB('By User Code'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s3),AT(192,106,100,10),USE(use:User_Code),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             END
                             TAB('By Surname'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(192,106,124,10),USE(use:Surname),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             END
                           END
                           PROMPT('Select Date Range'),AT(172,284),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(272,288),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(332,288,64,10),USE(GLO:Select1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(400,282),USE(?PopCalendar),TRN,FLAT,FONT(,,COLOR:White,,CHARSET:ANSI),ICON('lookupp.jpg')
                           BUTTON,AT(400,300),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(272,304),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(332,306,64,10),USE(GLO:Select2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(444,202),USE(?DASUNTAGALL),TRN,FLAT,LEFT,KEY(AltU),ICON('untagalp.jpg')
                       BUTTON('&Rev tags'),AT(276,218,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(324,218,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('Select User'),AT(168,88),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Retail Sales Valuation Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(444,174),USE(?DASTAGAll),TRN,FLAT,LEFT,KEY(AltA),ICON('tagallp.jpg')
                       BUTTON,AT(444,146),USE(?DASTAG),TRN,FLAT,LEFT,KEY(AltT),ICON('tagitemp.jpg')
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW3::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = use:User_Code
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = use:User_Code
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = use:User_Code
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::6:QUEUE = GLO:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = use:User_Code
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = use:User_Code
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020173'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Retail_Sales_Valuation_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:USERS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select1 = Deformat('1/1/1990',@d6)
  glo:select2 = Today()
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,use:surname_key)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?USE:Surname,use:Surname,1,BRW3)
  BRW3.AddSortOrder(,use:User_Code_Key)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(?USE:User_Code,use:User_Code,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(use:User_Code,BRW3.Q.use:User_Code)
  BRW3.AddField(use:Surname,BRW3.Q.use:Surname)
  BRW3.AddField(use:Forename,BRW3.Q.use:Forename)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020173'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020173'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020173'&'0')
      ***
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If ~Records(glo:Queue)
          Case Missive('You must select at least ONE User.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If ~Record(glo:Queue)
      IF error# = 0
          Retail_Sales_Valuation_Report
      End!IF error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::6:DASUNTAGALL
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = use:User_Code
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = use:User_Code
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue







Third_Party_Consignment_Note PROCEDURE
RejectRecord         LONG,AUTO
tmp:Batch_Number     STRING(20)
tmp:Retained         BYTE(0)
save_aud_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:BatchNumber      LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
count_temp           LONG
tmp:accessories      STRING(100)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:AuthorisationNo)
                       PROJECT(trb:ESN)
                       PROJECT(trb:Ref_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3885,7521,7417),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,771,7521,2781),USE(?unnamed)
                         STRING('3rd Party Agent:'),AT(4896,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,313),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5885,469),USE(trb:AuthorisationNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Authorisation No:'),AT(4896,469),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Batch No:'),AT(4896,313),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4896,625),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,625),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4896,781),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,781),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6458,781),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,938),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,938),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,938),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,938,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Repair If Required: '),AT(156,1490),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s3),AT(1771,1490),USE(GLO:Select6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1563),USE(trd:Company_Name),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s3),AT(1771,1646),USE(GLO:Select7),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s3),AT(1771,1802),USE(GLO:Select8),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1719),USE(trd:Address_Line1),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s3),AT(1771,1958),USE(GLO:Select9),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s3),AT(1771,2115),USE(GLO:Select11),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Upgrade Software:'),AT(156,2271),USE(?String46:6),TRN,FONT(,8,,)
                         STRING(@s3),AT(1771,2271),USE(GLO:Select12),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1875),USE(trd:Address_Line2),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(trd:Address_Line3),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s15),AT(4063,2188),USE(trd:Postcode),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Screening:'),AT(156,1646),USE(?String46:2),TRN,FONT(,8,,)
                         STRING('Refurbish If Required:'),AT(156,1802),USE(?String46:3),TRN,FONT(,8,,)
                         STRING('Upgrade If Required: '),AT(156,1958),USE(?String46:4),TRN,FONT(,8,,)
                         STRING('Refurbish All Units:'),AT(156,2115),USE(?String46:5),TRN,FONT(,8,,)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,167),USE(?detailband)
                           STRING(@s16),AT(208,0),USE(trb:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s11),AT(1094,0),USE(job:MSN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(1719,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2813,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(6906,0),USE(trb:Ref_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s100),AT(3906,0,2448,156),USE(tmp:accessories),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@d6),AT(6354,0),USE(jot:DateDespatched),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(260,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n-14),AT(1667,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY CONSIGNMENT NOTE'),AT(3958,0,3490,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING('Unit Type'),AT(2813,3177),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Sent'),AT(6333,3177),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(6979,3177),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Accessories Despatched'),AT(3906,3177),USE(?AccessoriesDespatched),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,938),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,938),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1094),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1250),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1250),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('DELIVERY ADDRESS'),AT(4052,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         STRING('CONSIGNMENT DETAILS'),AT(146,1458),USE(?String42:2),TRN,FONT(,9,,FONT:bold)
                         STRING('I.M.E.I. No'),AT(208,3177),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(1094,3177),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1719,3177),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Consignment_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  tmp:BatchNumber   = glo:Select2
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = '3rdPartyCons'
  If PrintOption(PreviewReq,glo:ExportReport,'Third Party Consignment Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:BatchNumber',tmp:BatchNumber)
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:TRDBATCH.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:TRDPARTY.UseFile
  Access:JOBACC.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBS.UseFile
  Access:ACCESSOR.UseFile
  Access:AUDIT.UseFile
  Access:USERS.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(TRDBATCH)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(TRDBATCH,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(trb:Company_Batch_ESN_Key)
      Process:View{Prop:Filter} = |
      'UPPER(trb:Company_Name) = UPPER(GLO:Select1) AND (trb:Batch_Number = t' & |
      'mp:BatchNumber)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
    print# = 1

    If Upper(trb:status) <> 'OUT' AND GLO:Select10 = 'NO'
        print# = 0
    End!If Upper(trb:status) = 'OUT'

    If print# = 1
        access:JOBS.clearkey(job:Ref_Number_Key)
        job:Ref_Number  = trb:Ref_Number
        If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign

            !Found
            Access:JOBTHIRD.Clearkey(jot:ThirdPartyKey)
            jot:ThirdPartyNumber = trb:RecordNumber
            If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
                !Found

            Else! If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
                !Error
            End! If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign

            !Match as Despatched
            If glo:Select3 = 'YES'
                job:ThirdPartyDateDesp   = glo:Select4
                Access:JOBS.Update()

                IF (AddToAudit(job:ref_number,'JOB','3RD PARTY AGENT: CONSIGNMENT NOTE PRINTED','3RD PARTY AGENT: ' & Clip(trb:Company_Name) & |
                                        '<13,10>BATCH NUMBER: ' & Clip(trb:Batch_Number)))
                END ! IF


                !Write the turnaround time to the record
                trb:TurnTime    = glo:Select5
                !Record the date and time the paperwork was printed, i.e.
                !when the unit was despatched - L842 (DBH: 21-07-2003)
                trb:DateDespatched  = glo:Select4
                trb:TimeDespatched  = Clock()

                Access:TRDBATCH.Update()

                !Get the individual record in the JOBTHIRD table that relates to the TRDBATCH record
                access:JOBTHIRD.clearkey(jot:ThirdPartyKey)
                jot:ThirdPartyNumber    = trb:RecordNumber
                If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !Found
                    jot:DateDespatched  = glo:Select4
                    Access:JOBTHIRD.Update()
                Else! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !For some reason, a record cannot be found. I suppose I should add a record
                    !Hopefully this will recover any errors that might occur.
                    !See if a previous entry exists in the JOBTHIRD file for this job
                    !If so, then take the Original IMEI Number. So every entry has the same
                    !Original IMEI Number.
                    OriginalIMEI"   = job:ESN
                    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                    jot:RefNumber = job:Ref_Number
                    Set(jot:RefNumberKey,jot:RefNumberKey)
                    If Access:JOBTHIRD.NEXT() = Level:Benign
                        If jot:RefNumber = job:Ref_Number
                            error# = 0
                            OriginalIMEI"    = jot:OriginalIMEI
                        End!If jot:RefNumber = job:RefNumber
                    End!If Access:JOBTHIRD.NEXT() = Level:Benign

                    !Create a new entry in the JOBTHIRD file for this despatch
                    If Access:JOBTHIRD.Primerecord() = Level:Benign
                        jot:RefNumber       = job:Ref_Number
                        jot:OriginalIMEI    = OriginalIMEI"
                        jot:OutIMEI         = trb:ESN
                        jot:InIMEI          = ''
                        jot:DateOut         = Today()
                        jot:DateIn          = ''
                        jot:ThirdPartyNumber    = trb:RecordNumber
                        If Access:JOBTHIRD.Tryinsert()
                            Access:JOBTHIRD.Cancelautoinc()
                        End!If Access:JOBTHIRD.Tryinsert()
                    End!If Access:JOBTHIRD.Primerecord() = Level:Benign

                End! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
            End!If glo:Select3 = 'YES'
            tmp:Retained = 0
            x# = 0

            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:Ref_Number_Key)
            aud:Ref_Number = job:Ref_Number
            aud:Date       = TODAY()
            Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
            Loop
                If Access:AUDIT.Next()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number
                  BREAK
                END
                IF aud:Action     <> '3RD PARTY AGENT: UNIT SENT'
                  CYCLE
                END

                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                    x# = INSTRING('BATCH NUMBER:',aud2:Notes,1,1)
                    IF x# <> 0
                      LOOP y# = x#+14 TO LEN(CLIP(aud2:Notes))
                        IF SUB(aud2:Notes,y#,1) = CHR(13)
                          BREAK
                        END
                      END
                      tmp:batch_Number = SUB(aud2:Notes,x#+14,(y#)-(x#+14))
                      !Make sure the batch number doesn't contain any commas - TrkBs: 4826 (DBH: 21-02-2005)
                      IF FORMAT(tmp:batch_number,@n_10) <> trb:Batch_Number
                        CYCLE
                      END
                    END
                    x# = Instring('ACCESSORIES DESPATCHED WITH UNIT:',aud2:Notes,1,1)
                    If x# <> 0
                        tmp:Accessories = StripReturn(Clip(Sub(aud2:Notes,x# + 35,Len(Aud2:Notes))))
                        tmp:Retained = 0
                        Break
                    End !If x# <> 0
                    x# = Instring('ACCESSORIES RETAINED WITH UNIT:',aud2:Notes,1,1)
                    If x# <> 0
                        tmp:Accessories = StripReturn(Clip(Sub(aud2:Notes,x# + 33,Len(Aud2:Notes))))
                        tmp:Retained = 1
                        Break
                    End !If x# <> 0
                END ! IF



            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)

!            tmp:Accessories = ''
!            Save_jac_ID = Access:JOBACC.SaveFile()
!            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
!            jac:Ref_Number = job:Ref_Number
!            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
!            Loop
!                If Access:JOBACC.NEXT()
!                   Break
!                End !If
!                If jac:Ref_Number <> job:Ref_Number      |
!                    Then Break.  ! End If
!                If tmp:Accessories = ''
!                    tmp:Accessories = jac:Accessory
!                Else!If tmp:Accessories = ''
!                    tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
!                End!If tmp:Accessories = ''
!            End !Loop
!            Access:JOBACC.RestoreFile(Save_jac_ID)
!            If tmp:Accessories <> ''
                If tmp:Retained = 1
                    SetTarget(Report)
                    ?AccessoriesDespatched{prop:Text} = 'Accessories Retained'
                    SetTarget()
!                Else!If tmp:Retained = 1
!                    tmp:Accessories = '(DES) ' & Clip(tmp:Accessories)
                End!If tmp:Retained = 1
!            End!If tmp:Accessories <> ''
            Count_Temp += 1
            Print(rpt:Detail)
        Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
            Assert(0,'<13,10>Cannot find associate JOB Entry (' & Clip(trb:Ref_Number) & ').<13,10>')        !Error
        End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
    End!If Print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(TRDBATCH,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:STANTEXT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'TRDBATCH')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Set(Defaults)
  Access:DEFAULTS.Next()
  If GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      SetTarget(Report)
      ?String46{prop:Hide} = 1
      ?String46:2{prop:Hide} = 1
      ?String46:3{prop:Hide} = 1
      ?String46:4{prop:Hide} = 1
      ?String46:5{prop:Hide} = 1
      ?String46:6{prop:Hide} = 1
      ?glo:Select6{prop:Hide} = 1
      ?glo:Select7{prop:Hide} = 1
      ?glo:Select8{prop:Hide} = 1
      ?glo:Select9{prop:Hide} = 1
      ?glo:Select11{prop:Hide} = 1
      ?glo:Select12{prop:Hide} = 1
      ?String42:2{prop:Hide} = 1
      SetTarget()
  End !GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  ! Inserting (DBH 15/02/2008) # 9775 - Show who printed the document
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:Password_Key) = Level:benign
      tmp:PrintedBy = Clip(use:Forename) & ' ' & Clip(use:Surname)
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:benign
  ! End (DBH 15/02/2008) #9775
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Consignment_Note'
  END
  report{Prop:Preview} = PrintPreviewImage













Parts_Order PROCEDURE
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:ExchangeRate     STRING(30)
tmp:TotalValueForeign STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ShelfLocation    STRING(30)
tmp:CurrencyLineCost REAL
tmp:CurrencyItemCost REAL
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDERS)
                       PROJECT(ord:Date)
                       PROJECT(ord:Order_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Parts Order'),AT(250,4323,7750,4323),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),THOUS
                       HEADER,AT(250,250,7750,2958),USE(?unnamed)
                         STRING(@s30),AT(104,0),USE(def:OrderCompanyName),TRN,LEFT,FONT('Tahoma',16,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(104,260),USE(def:OrderAddressLine1),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(104,417),USE(def:OrderAddressLine2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(104,573),USE(def:OrderAddressLine3),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(104,729),USE(def:OrderPostcode),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5313,792),USE(?ReportDatePrompt),TRN,FONT(,,,FONT:bold)
                         STRING(@d6b),AT(6146,781),USE(ReportRunDate),TRN
                         STRING('Tel:'),AT(104,885),USE(?String22),TRN
                         STRING(@s30),AT(469,885),USE(def:OrderTelephoneNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(104,1042),USE(?String22:2),TRN
                         STRING(@s30),AT(469,1042),USE(def:OrderFaxNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Email:'),AT(104,1198),USE(?String22:3),TRN
                         STRING(@s255),AT(469,1198),USE(def:OrderEmailAddress),TRN,LEFT,FONT(,8,,)
                         STRING(@t3b),AT(6927,781),USE(ReportRunTime),TRN
                         STRING(@s30),AT(156,1771),USE(sup:Company_Name),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,1927),USE(sup:Address_Line1),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,2083),USE(sup:Address_Line2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4167,1771),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,2240),USE(sup:Address_Line3),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4167,1927),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s10),AT(156,2396),USE(sup:Postcode),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4167,2083),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Tel:'),AT(156,2552),USE(?Text:SupplierTelephone),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s15),AT(417,2552),USE(sup:Telephone_Number),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4167,2240),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Fax:'),AT(156,2708),USE(?Text:SupplierFax),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s15),AT(417,2708),USE(sup:Fax_Number),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4167,2396),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Tel:'),AT(4167,2552),USE(?Text:DeliveryTelephone),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4427,2552),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Fax:'),AT(4167,2708),USE(?Text:DeliveryFax),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4427,2708),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                       END
DETAIL                 DETAIL,AT(,,,156),USE(?DetailBand)
                         STRING(@s6),AT(156,0),USE(GLO:Q_Quantity),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(625,0),USE(GLO:Q_Part_Number),TRN,FONT(,7,,)
                         STRING(@s30),AT(2760,0),USE(GLO:Q_Description),TRN,FONT(,7,,)
                         STRING(@n7.2),AT(5365,0),USE(GLO:Q_Purchase_Cost),TRN,RIGHT,FONT(,7,,)
                         STRING(@n14.2),AT(6677,0),USE(total_cost_temp),TRN,RIGHT,FONT(,7,,)
                         STRING(@n7.2),AT(4573,0),USE(tmp:CurrencyItemCost),TRN,RIGHT,FONT(,7,,)
                         STRING(@n14.2),AT(5885,0),USE(tmp:CurrencyLineCost),TRN,RIGHT,FONT(,7,,)
                         STRING(@s30),AT(1667,0,833,208),USE(tmp:ShelfLocation),TRN,LEFT,FONT('Tahoma',7,,,CHARSET:ANSI)
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                       END
Totals                 DETAIL,AT(250,250,7750,11188),USE(?Text:TotalOrderValueCurrency:2),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ABSOLUTE
                         STRING('Total Items:'),AT(313,8958),USE(?String47),TRN,FONT(,8,,FONT:bold)
                         STRING('Total Lines: '),AT(313,9323),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING(@p<<<<<<<#p),AT(1302,9323),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6427,9375,,208),USE(tmp:TotalValueForeign),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Total Order Value (CUR):'),AT(5052,9375),USE(?Text:TotalOrderValueCurrency),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchange Rate:'),AT(5052,8854),USE(?Text:ExchangeRate),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6427,9115),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@p<<<<<<<#p),AT(1302,8958),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@n12.4),AT(6552,8854,,208),USE(tmp:ExchangeRate),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Total Order Value (R):'),AT(5052,9115),USE(?Text:TotalOrderValue),TRN,FONT(,8,,FONT:bold)
                       END
Continue               DETAIL,AT(250,9396),USE(?Continue),ABSOLUTE
                         STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                         STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                       END
Header:Titles          DETAIL,PAGEBEFORE(-1),AT(250,250,7750,),USE(?Header:Titles),ABSOLUTE
                         STRING(@s15),AT(156,3438),USE(sup:Account_Number),TRN
                         STRING(@n~SS~010),AT(1667,3438),USE(ord:Order_Number),TRN
                         STRING(@d6),AT(3177,3438),USE(ord:Date),TRN,LEFT
                         STRING(@s60),AT(4688,3438,1406,156),USE(sup:Contact_Name),TRN,FONT(,7,,)
                         STRING(@s15),AT(6198,3438),USE(sup:Telephone_Number,,?sup:Telephone_Number:2),TRN
                         STRING('Qty'),AT(188,3854),USE(?Text:Quantity),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(625,3854),USE(?Text:PartNumber),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(2760,3854),USE(?Text:Description),TRN,FONT(,7,,FONT:bold)
                         STRING('Item Cost (R)'),AT(5156,3854),USE(?Text:ItemCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Line Cost (CUR)'),AT(5990,3854),USE(?Text:CurrencyLineCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Line Cost (R)'),AT(6927,3854),USE(?Text:LineCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Item Cost (CUR)'),AT(4219,3854),USE(?Text:CurrencyItemCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Shelf Location'),AT(1667,3854),USE(?Text:ShelfLocation),TRN,FONT(,7,,FONT:bold)
                       END
                       FOOTER,AT(250,10156,7750,1125),USE(?unnamed:2)
                         TEXT,AT(104,104,7552,938),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         BOX,AT(5052,417,2646,1042),USE(?BoxGrey:TopDetails),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,1719,3604,1302),USE(?BoxGrey:Address1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,1719,3604,1302),USE(?BoxGrey:Address2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3177,7552,521),USE(?BoxGrey:Title),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3854,7552,4740),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,8750,2813,1042),USE(?BoxGrey:Total1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4896,8750,2813,1042),USE(?BoxGrey:Total2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3802,7552,260),USE(?Text:Quantity:2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,8698,2813,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4844,8698,2813,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         STRING('OFFICIAL PARTS ORDER'),AT(4948,52),USE(?Text:OfficialPartsOrder),TRN,FONT(,16,,FONT:bold)
                         STRING('SUPPLIER ADDRESS'),AT(156,1510),USE(?Text:SupplierAddress),TRN,FONT(,,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510),USE(?Text:DeliveryAddress),TRN,FONT(,,,FONT:bold)
                         STRING('Account Number'),AT(156,3177),USE(?Text:AccountNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Order Number'),AT(1667,3177),USE(?Text:OrderNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Order Date'),AT(3177,3177),USE(?Text:OrderDate),TRN,FONT(,,,FONT:bold)
                         STRING('Contact'),AT(4688,3177),USE(?Text:Contact),TRN,FONT(,,,FONT:bold)
                         STRING('Contact Number'),AT(6198,3177),USE(?Text:ContactNumber),TRN,FONT(,,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Parts_Order')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'PartsOrder'
  If PrintOption(PreviewReq,glo:ExportReport,'Parts Order') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('no_temp',no_temp)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDERS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:STOCK_ALIAS.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:ORDPARTS.UseFile
  
  
  RecordsToProcess = RECORDS(ORDERS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDERS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(ord:Supplier_Printed_Key)
      Process:View{Prop:Filter} = |
      'UPPER(ord:Printed) = UPPER(no_temp)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        !access:supplier.clearkey(sup:company_name_key)
        !sup:company_name = ord:supplier
        !if access:supplier.fetch(sup:company_name_key) = Level:Benign
        !    address_line1_temp = sup:address_line1
        !    address_line2_temp = sup:address_line2
        !    If sup:address_line3 = ''
        !        address_line3_temp = sup:postcode
        !        address_line4_temp = ''
        !    Else
        !        address_line3_temp = sup:address_line3
        !        address_line4_temp = sup:postcode
        !    End ! If
        !end! if access:supplier.fetch(sup:company_name_key) = Level:Benign
        !
        !multi_print# = 1
        !If glo:select1 <> ''
        !    If ord:order_number <> glo:select1
        !        multi_print# = 0
        !    End ! If
        !End! If glo:select1 <> ''
        !! Currency not setup, then hide the currency fields - TrkBs: 6264 (DBH: 08-09-2005)
        !SetTarget(Report)
        !If ord:OrderedCurrency = ''
        !    ?Text:CurrencyItemCost{prop:Hide} = True
        !    ?Text:CurrencyLineCost{prop:Hide} = True
        !    ?tmp:CurrencyItemCost{prop:Hide} = True
        !    ?tmp:CurrencyLineCost{prop:Hide} = True
        !Else ! If ord:OrderedCurrency = ''
        !    ?Text:CurrencyItemCost{prop:Text} = 'Item Cost (' & Clip(ord:OrderedCurrency) & ')'
        !    ?Text:CurrencyLineCost{prop:Text} = 'Line Cost (' & Clip(ord:OrderedCurrency) & ')'
        !End ! If ord:OrderedCurrency = ''
        !SetTarget()
        !
        !If multi_print# = 1
        !    Print(rpt:Header:Titles)
        !
        !    Sort(glo:Q_PartsOrder, glo:q_supplier, glo:q_order_number, glo:q_part_number)
        !
        !    count# = 0
        !    Loop x# = 1 To Records(glo:Q_PartsOrder)
        !        Get(glo:Q_PartsOrder, x#)
        !        If glo:q_supplier <> sup:company_name Then Cycle.
        !        total_cost_temp        = glo:q_purchase_cost * glo:q_quantity
        !        total_cost_total_temp += total_cost_temp
        !        total_quantity_temp   += glo:q_quantity
        !        total_lines_temp      += 1
        !
        !        ! Show the Main Store Shelf Location - TrkBs: 6264 (DBH: 07-09-2005)
        !        Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
        !        sto_ali:Location    = 'MAIN STORE'
        !        sto_ali:Part_Number = glo:Q_Part_Number
        !        If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
        !            ! Found
        !            tmp:ShelfLocation = sto_ali:Shelf_Location
        !        Else ! If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key)l = Level:Benign
        !            ! Error
        !            tmp:ShelfLocation = ''
        !        End ! If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key)l = Level:Benign
        !
        !        ! Show individual line currency, and use this to work out the total - TrkBs: 6264 (DBH: 08-09-2005)
        !        If ord:OrderedCurrency <> ''
        !            Case ord:OrderedDivideMultiply
        !            Of '*'
        !                tmp:CurrencyItemCost = glo:Q_Purchase_Cost / ord:OrderedDailyRate
        !            Of '/'
        !                tmp:CurrencyItemCost = glo:Q_Purchase_Cost * ord:OrderedDailyRate
        !            End ! Case ord:OrderedDivideMultiply
        !            tmp:CurrencyLineCost = tmp:CurrencyItemCost * glo:Q_Quantity
        !            tmp:TotalValueForeign += tmp:CurrencyLineCost
        !        End ! If ord:OrderedCurrency <> ''
        !
        !        If order_temp <> ord:order_number And first_page_temp <> 1
        !            !Print(rpt:detail1)
        !            Print(rpt:Header:Titles)
        !        End ! If
        !        tmp:RecordsCount += 1
        !        count#           += 1
        !        If count# > 20
        !            Print(rpt:continue)
        !            !Print(rpt:detail1)
        !            Print(rpt:Header:Titles)
        !            count# = 1
        !        End! If count# > 25
        !        Print(rpt:detail)
        !        order_temp      = ord:order_number
        !        first_page_temp = 0
        !        print#          = 1
        !    End! Loop x# = 1 To Records(glo:Q_PartsOrder)
        !
        !    ! Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
        !    SetTarget(Report)
        !    If ord:OrderedCurrency <> ''
        !        ! tmp:ExchangeRate = '(' & Clip(ord:OrderedCurrency) & ') ' & Format(ord:OrderedDailyRate,@n14.4)
        !        tmp:ExchangeRate             = ord:OrderedDailyRate
        !        ?tmp:ExchangeRate{PROP:Text} = '@n~(' & Clip(ord:OrderedCurrency) & ') ~14.4'
        !        ?tmp:TotalValueForeign{prop:Text} = '@n~(' & Clip(ord:OrderedCurrency) & ') ~24.2'
        !    Else ! ord:OrderedCurrency <> ''
        !        ?tmp:ExchangeRate{PROP:Hide}      = True
        !        ?tmp:TotalValueForeign{prop:Hide} = True
        !        ?Text:ExchangeRate{PROP:Hide}     = True
        !    End ! ord:OrderedCurrency <> ''
        !    SetTarget()
        !    ! End   - Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
        !
        !    If print# = 1
        !        Print(rpt:totals)
        !        total_cost_total_temp = 0
        !        total_quantity_temp   = 0
        !        total_Lines_temp = 0
        !        pos                   = Position(ord:supplier_printed_key)
        !        ord:printed           = 'YES'
        !        access:orders.update()
        !        Reset(ord:supplier_printed_key, pos)
        !    End! If print# = 1
        !
        !End! If print# = 1
        Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
        sup:Company_Name    = ord:Supplier
        If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
            ! Found
            Address_Line1_Temp = sup:Address_Line1
            Address_Line2_Temp = sup:Address_Line2
            If sup:Address_Line3 = ''
                Address_Line3_Temp = sup:Postcode
                Address_Line4_Temp = ''
            Else ! If sup:Address_Line3 = ''
                Address_Line3_Temp = sup:Address_Line3
                Address_Line4_Temp = sup:Postcode
            End ! If sup:Address_Line3 = ''
            Else ! If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
            ! Error
        End ! If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
        
        PrintReport# = 1
        Print#       = 0
        
        ! Has an order number been passed? (DBH: 04-10-2005)
        If glo:Select1 <> ''
            If ord:Order_Number <> glo:Select1
                PrintReport# = 0
            End ! If ord:Order_Number <> glo:Select1
        End ! If glo:Select1 <> ''
        
        If PrintReport# = 1 And Records(glo:Q_PartsOrder)
            ! Currency not setup, then hide the currency fields - TrkBs: 6264 (DBH: 08-09-2005)
            SetTarget(Report)
            If ord:OrderedCurrency = ''
                ?Text:CurrencyItemCost{prop:Hide} = True
                ?Text:CurrencyLineCost{prop:Hide} = True
                ?tmp:CurrencyItemCost{prop:Hide}  = True
                ?tmp:CurrencyLineCost{prop:Hide}  = True
            Else ! If ord:OrderedCurrency = ''
                ?Text:CurrencyItemCost{prop:Hide} = False
                ?Text:CurrencyLineCost{prop:Hide} = False
                ?tmp:CurrencyItemCost{prop:Hide}  = False
                ?tmp:CurrencyLineCost{prop:Hide}  = False
                ?Text:CurrencyItemCost{prop:Text} = 'Item Cost (' & Clip(ord:OrderedCurrency) & ')'
                ?Text:CurrencyLineCost{prop:Text} = 'Line Cost (' & Clip(ord:OrderedCurrency) & ')'
            End ! If ord:OrderedCurrency = ''
            SetTarget()
        
            FirstPage# = 1
        
            Sort(glo:Q_PartsOrder, glo:Q_Supplier, glo:Q_Order_Number, glo:Q_Part_Number)
        
            Count# = 0
            Loop x# = 1 To Records(glo:Q_PartsOrder)
                Get(glo:Q_PartsOrder, x#)
                If glo:Q_Supplier <> sup:Company_Name
                    Cycle
                End ! If glo:Q_Supplier <> sup:Company_Name
        
                ! Inserting (DBH 03/03/2008) # 9811 - Make sure only the correct orders are printed
                If glo:Q_Order_Number <> ord:Order_Number
                    Cycle
                End ! If glo:Q_Order_Number <> ord:Order_Number
        
                If FirstPage# = 1
                    Print(rpt:Header:Titles)
                    FirstPage# = 0
                End ! If FirstPage# = 1
                ! End (DBH 03/03/2008) #9811
        
                Total_Cost_Temp        = glo:Q_Purchase_Cost * glo:Q_Quantity
                Total_Cost_Total_Temp += Total_Cost_Temp
                Total_Quantity_Temp   += glo:Q_Quantity
                Total_Lines_Temp      += 1
        
                ! Show the Main Store Shelf Location - TrkBs: 6264 (DBH: 07-09-2005)
                SETTARGET(REPORT)
                ! #12127 If a free text Supplier. Don't show costs. (Bryan: 12/07/2011)
                ?GLO:Q_Purchase_Cost{prop:Hide} = 0
                ?total_cost_temp{prop:Hide} = 0
                IF (ord:OrderedCurrency <> '')
                    ?tmp:CurrencyItemCost{prop:Hide} = 0
                    ?tmp:CurrencyLineCost{prop:Hide} = 0
                END
        
                Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                sto_ali:Location    = MainStoreLocation()
                sto_ali:Part_Number = glo:Q_Part_Number
                If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                    ! Found
                    tmp:ShelfLocation = sto_ali:Shelf_Location
                    IF (sto_ali:ExchangeUnit = 'YES')
                        IF (sup:UseFreeStock)
                            ?tmp:CurrencyItemCost{prop:Hide} = 1
                            ?GLO:Q_Purchase_Cost{prop:Hide} = 1
                            ?tmp:CurrencyLineCost{prop:Hide} = 1
                            ?total_cost_temp{prop:Hide} = 1
                        END ! IF (sup:UseFreeStock)
                    END ! IF (sto_ali:Exchange_Unit = 'YES')
        
                Else ! If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key)l = Level:Benign
                    ! Error
                    tmp:ShelfLocation = ''
                End ! If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key)l = Level:Benign
                SETTARGET()
        
                ! Show individual line currency, and use this to work out the total - TrkBs: 6264 (DBH: 08-09-2005)
                If ord:OrderedCurrency <> ''
                    Case ord:OrderedDivideMultiply
                    Of '*'
                        tmp:CurrencyItemCost = glo:Q_Purchase_Cost / ord:OrderedDailyRate
                    Of '/'
                        tmp:CurrencyItemCost = glo:Q_Purchase_Cost * ord:OrderedDailyRate
                    End ! Case ord:OrderedDivideMultiply
                    tmp:CurrencyLineCost   = tmp:CurrencyItemCost * glo:Q_Quantity
                    tmp:TotalValueForeign += tmp:CurrencyLineCost
                End ! If ord:OrderedCurrency <> ''
        
                tmp:RecordsCount += 1
                Count#           += 1
        
                If count# > 20
                    ! New Page (DBH: 04-10-2005)
                    Print(rpt:Continue)
                    Print(rpt:Header:Titles)
                    Count# = 1
                End ! If count# > 20
                Print(rpt:Detail)
                Print# = 1
            End ! Loop x# = 1 To Records(glo:Q_PartsOrder)
        
            ! Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
            SetTarget(Report)
            If ord:OrderedCurrency <> ''
                ?tmp:ExchangeRate{PROP:Hide}      = False
                ?tmp:TotalValueForeign{prop:Hide} = False
                ?Text:ExchangeRate{PROP:Hide}     = False
                ?Text:TotalOrderValueCurrency{prop:Hide} = False
                tmp:ExchangeRate                  = ord:OrderedDailyRate
                ?Text:TotalOrderValueCurrency{prop:Text} = 'Total Order Value (' & Clip(ord:OrderedCurrency) & '):'
                ?Text:ExchangeRate{PROP:Text}           = 'Exchange Rate (' & Clip(ord:OrderedCurrency) & '):'
            Else ! ord:OrderedCurrency <> ''
                ?tmp:ExchangeRate{PROP:Hide}      = True
                ?tmp:TotalValueForeign{prop:Hide} = True
                ?Text:ExchangeRate{PROP:Hide}     = True
                ?Text:TotalOrderValueCurrency{prop:Hide} = True
            End ! ord:OrderedCurrency <> ''
            SetTarget()
            ! End   - Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
        
            If Print# = 1
                Print(rpt:Totals)
                Total_Cost_Total_Temp = 0
                Total_Quantity_Temp   = 0
                Total_Lines_Temp      = 0
                pos                   = Position(ord:Supplier_Printed_Key)
                ord:Printed           = 'YES'
                Access:ORDERS.TryUpdate()
                Reset(ord:Supplier_Printed_Key, pos)
            End ! If Print# = 1
        End ! If PrintReport# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDERS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:ORDERS.Close
    Relate:STANTEXT.Close
    Relate:STOCK_ALIAS.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'ORDERS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  ! Inserting (DBH 03/12/2007) # 9034 - Change the prefix of the order number
  Prefix" = GETINI('STOCK','OrderNumberPrefix',,Clip(Path()) & '\SB2KDEF.INI')
  If Clip(Prefix") = ''
      Prefix" = 'SS'
  End ! If Clip(Prefix") = ''
  
  SetTarget(Report)
  ?ord:Order_Number{prop:Text} = '@n~' & Clip(Prefix") & '~010'
  SetTarget()
  ! End (DBH 03/12/2007) #9034
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Parts_Order'
  END
  Report{Prop:Preview} = PrintPreviewImage







