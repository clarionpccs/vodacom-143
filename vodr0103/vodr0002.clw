

   MEMBER('vodr0103.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


ConsolidatedStockReport PROCEDURE                          ! Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGFLAG         BYTE(0)
DASBRW::10:TAGMOUSE        BYTE(0)
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locAllAccounts       BYTE                                  !
tmp:VersionNumber    STRING(30)                            !
locTag               STRING(1)                             !
locHeadAccountNumber STRING(30)                            !
locARCSiteLocation   STRING(30)                            !
locARCCompanyName    STRING(30)                            !
locMainStore         STRING(30)                            !
locUserName          STRING(60)                            !
locTrue              BYTE(1)                               !
Progress:Thermometer BYTE(0)                               !Moving Bar
StatusQueue          QUEUE,PRE(staque)                     !
StatusMessage        STRING(60)                            !Status Message
                     END                                   !
BRW9::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
locTag                 LIKE(locTag)                   !List box control field - type derived from local data
locTag_Icon            LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_Icon      LONG                           !Entry's icon ID
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('** Report Title **'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(64,38,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Consolidated Stock Report'),AT(68,39),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(563,39),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,363,552,26),USE(?PanelButton),FILL(09A6A7CH)
                       ENTRY(@d17),AT(335,78,60,10),USE(locStartDate),RIGHT(2),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ
                       PROMPT('Action End Date'),AT(248,98),USE(?locEndDate:Prompt),TRN,CENTER,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       PROMPT('Action Start Date'),AT(248,76),USE(?locStartDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@d17),AT(335,100,60,10),USE(locEndDate),RIGHT(2),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ
                       PANEL,AT(64,54,552,305),USE(?PANEL1),FILL(09A6A7CH),BEVEL(1)
                       LIST,AT(222,131,237,220),USE(?List),IMM,VSCROLL,COLOR(COLOR:White),MSG('Record Number'),TIP('Record Number'),FORMAT('12CJ@s1@120L(2)J~Location~@s30@'),FROM(Queue:Browse),GRID(0D3D3D3H)
                       BUTTON,AT(464,263),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(464,294),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(464,325),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON('xx'),AT(507,146,5,5),USE(?DASREVTAG),HIDE
                       BUTTON('xx'),AT(504,186,8,8),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(400,74),USE(?Calendar),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(400,97),USE(?Calendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(549,363),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       PROMPT('Report Version'),AT(70,370),USE(?ReportVersion),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(479,363),USE(?buttonPrint),TRN,FLAT,ICON('printp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                     END

!Progress Window
rejectrecord        long
recordstoprocess    long,auto
recordsprocessed    long,auto
recordspercycle     long,auto
recordsthiscycle    long,auto
percentprogress     byte
recordstatus        byte,auto
locCancel          byte

progresswindow      WINDOW('Progress...'),AT(,,680,428),CENTER,ICON('cellular3g.ico'),FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),IMM,COLOR(00D6EAEFh),Tiled,DOUBLE
                        PANEL, AT(164,68,352,12), USE(?ProgPanel), FILL(009A6A7Ch)
                        LIST, AT(208,88,268,206), USE(?List1), FONT(,, 00010101h,, CHARSET:ANSI), FORMAT('20L(2)|M'), |
                            COLOR(COLOR:White, COLOR:White, 009A6A7Ch), VSCROLL, GRID(COLOR:WHITE), FROM(StatusQueue)
                        PANEL, AT(164,84,352,246), USE(?Panel4), FILL(009A6A7Ch)
                        PROGRESS, AT(206,314,268,12), USE(progress:thermometer), RANGE(0,100), SMOOTH
                        STRING(''), AT(259,300,161,10), USE(?progress:userstring), FONT('Arial', 8, 0080FFFFh, FONT:bold), |
                            COLOR(009A6A7Ch), CENTER
                        STRING(''), AT(232,136,161,10), USE(?progress:pcttext), FONT('Arial', 8), HIDE, TRN, CENTER
                        PANEL, AT(164,332,352,28), USE(?Panel2), FILL(009A6A7Ch)
                        PROMPT('Report Progress'), AT(168,70), USE(?WindowTitle2), FONT('Tahoma', 8, COLOR:White, FONT:bold, CHARSET:ANSI), |
                            TRN
                        BUTTON, AT(444,332), USE(?ProgressCancel), ICON('Cancelp.jpg'), FLAT, LEFT, TRN
                        BUTTON, AT(376,332), USE(?Button:OpenReportFolder), ICON('openrepp.jpg'), HIDE, FLAT, TRN
                        BUTTON, AT(444,332), USE(?Finish), ICON('Finishp.jpg'), HIDE, FLAT, TRN
                    END

                    map
UpdateProgressWindow    PROCEDURE(STRING fText)
WriteLine               PROCEDURE(LONG fRecordNumber,LONG fCurrentRow,STRING fAction,BYTE fLastAction)
FinishFormat            PROCEDURE(STRING fLastColumn,LONG fLastRow)
GetMarkedDownCost       Procedure(Real fPurchaseCost,Real fMarkup),Real
                    end
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW9                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
Calendar12           CalendarClass
Calendar13           CalendarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

glo:ExportFile      STRING(255),STATIC
ExportFile          File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                       String(2000)
                        End
                    End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW9.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    locTag = '*'
  ELSE
    DELETE(glo:Queue)
    locTag = ''
  END
    Queue:Browse.locTag = locTag
  IF (locTag = '*')
    Queue:Browse.locTag_Icon = 2
  ELSE
    Queue:Browse.locTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::10:QUEUE = glo:Queue
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer = loc:Location
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reporting           ROUTINE
DATA
kTempPath   CSTRING(255) ! Temp Folder
kReportFolder       CString(255) ! Report Folder
kFilename   CSTRING(255) ! Excel Filename
kRecordCount        LONG()
kClipBoard  ANY()
kCurrentManufacturer        STRING(30)
kCurrentPartNumber  STRING(30)
kCurrentRow LONG()

kReportStartDate    DATE()
kReportStartTime    TIME()

kReportName EQUATE('Consolidated Stock Report') ! Report Name
kLastDetailColumn   EQUATE('AO')

    !---
qReportData QUEUE
Manufacturer    STRING(30)
RecordNumber    LONG()
PartNumber      STRING(30)
ReportAction    String(15)
LastAction      BYTE()
            END

qLastPart   QUEUE
QueueNumber     LONG()
            END

kUserLocation       STRING(30)
kMovements  Byte(0)
CODE
    ! Set temp folder for csv files
    IF (GetTempPathA(255,kTempPath))
        IF (sub(kTempPath,-1,1) <> '\')
            kTempPath = clip(kTempPath) & '\'
        END
    END

    ! Set the folder name for the excel file

    ! Create folder in My Documents
    SHGetSpecialFolderPathAA(GetDesktopWindow(),kReportFolder,5,0)
    kReportFolder = CLIP(kReportFolder) & '\ServiceBase Export'

    IF (NOT Exists(kReportFolder))
        IF (MKDir(kReportFolder))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(kReportFolder) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            EXIT
        END
    END

    kReportFolder = Clip(kReportFolder) & '\' & clip(kReportName)

    IF (NOT Exists(kReportFolder))
        IF (MKDir(kReportFolder))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(kReportFolder) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            EXIT
        END
    END

    kFileName = clip(kReportFolder) & '\' & clip(kReportName) & |
        ' ' & FORMAT(today(),@d12)

    IF (exists(kFileName & '.xls'))
        REMOVE(kFileName & '.xls')
        IF (exists(kFileName & '.xls'))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot get access to selected document:'&|
                '|'&|
                '|' & Clip(kFileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            EXIT
        END
    END

    DO OpenProgressWindow
    kReportStartDate = Today()
    kReportStartTime = Clock()

    UpdateProgressWindow('Report Started: ' & FORMAT(kReportStartDate,@d6) & |
        ' ' & FORMAT(kReportStartTime,@t1))
    UpdateProgressWindow('')
    UpdateProgressWindow('Searching Stock History...')
    UpdateProgressWindow('')


    LOOP main# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,main#)
        UpdateProgressWindow('')
        UpdateProgressWindow('Location: ' & glo:Pointer)
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location = glo:Pointer
        SET(sto:Location_key,sto:Location_Key)
        LOOP
            IF (Access:STOCK.Next())
                BREAK
            END
            IF (sto:Location <> glo:Pointer)
                BREAK
            END
            if (sto:Suspend)
                CYCLE
            END

            DO UpdateProgressWindow
            DO CancelCheck
            IF (locCancel)
                BREAK
            END

            ! Only include stock with value
!            IF (sto:Location = locMainStore)
!                IF (sto:Purchase_Cost = 0)
!                    CYCLE
!                END
!
!            ELSE
!                IF (sto:AveragePurchaseCost = 0)
!                    CYCLE
!                END
            !            END
!            IF (sto:Quantity_Stock = 0)
!                CYCLE
!            END


            ! Loop
            kMovements = 0
            Access:STOHIST.ClearKey(shi:Ref_Number_Key)
            shi:Ref_Number = sto:Ref_Number
            shi:Date = locStartDate
            SET(shi:Ref_Number_Key,shi:Ref_Number_Key)
            Loop
                IF (Access:STOHIST.Next())
                    BREAK
                END
                IF (shi:Ref_Number <> sto:Ref_Number)
                    BREAK
                END

                IF (shi:Date > locEndDate)
                    BREAK
                END

                DO UpdateProgressWindow
                DO CancelCheck
                IF (locCancel)
                    BREAK
                END

                IF (shi:Date > locEndDate)
                    Break
                END


                IF (shi:Notes = 'STOCK ADDED FROM ORDER')
                    qReportData.ReportAction = 'Purchase'
                ELSIF (shi:Notes = 'RETAIL ITEM SOLD')
                    qReportData.ReportAction = 'Sale'
                ELSIF (Instring('STOCK TRANSFERRED',shi:Notes,1,1))
                    IF (sto:Location = locMainStore OR sto:Location = locARCSiteLocation)
                        qReportData.ReportAction = 'Transfer'
                    ELSE
                        CYCLE
                    END
                ELSIF (shi:Notes = 'STOCK ADDED')
                    qReportData.ReportAction = 'Increment'
                ELSIF (shi:Notes = 'STOCK DECREMENTED')
                    qReportData.ReportAction = 'Decrement'
                ELSIF (shi:Notes = 'PART SCRAPPED')
                    qReportData.ReportAction = 'Scrap'
                ELSE
                    CYCLE
                END


                qReportData.Manufacturer = sto:Manufacturer
                qReportData.RecordNumber = shi:Record_Number
                qReportData.PartNumber = sto:Part_Number
                qReportData.LastAction = FALSE
                Add(qReportData)

                !            WriteLine()
                kRecordCount += 1
                kMovements += 1

            END
            IF (locCancel)
                BREAK
            END
            IF (kMovements = 0 AND sto:Quantity_Stock > 0) OR (kMovements > 0)
                ! #11728 Don't add a line if there is no movement ADD no stock (Bryan: 11/02/2011)
                qReportData.ReportAction = 'Initial'
                qReportData.Manufacturer = sto:Manufacturer
                qReportData.RecordNumber = sto:Ref_Number
                qReportData.PartNumber = sto:Part_Number
                qReportData.LastAction = FALSE
                Add(qReportData)
                kRecordCount += 1
            END


        END
        IF (locCancel)
            BREAK
        END
    End

    ! EndLoop
    !        CLOSE(ExportFile)

    IF (locCancel = 0 OR locCancel = 2)
        ?ProgressCancel{PROP:Hide} = 1

        IF (e1.init(0,0) = 0)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding your Excel Document.'&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            EXIT
        END
        E1.NewWorkbook()

        UpdateProgressWindow('')
        UpdateProgressWindow('Finding Last Action...')
        UpdateProgressWindow('')

        ! Find which is the last item for a particular part
        kCurrentPartNumber = ''
        kCurrentManufacturer = ''
        SORT(qReportData,qReportData.Manufacturer,qReportData.PartNumber)
        LOOP X# = 1 TO RECORDS(qReportData)
            GET(qReportData,X#)
            DO UpdateProgressWindow
            DO CancelCheck
            IF (locCancel)
                BREAK
            END
            IF (kCurrentManufacturer = '')
                kCurrentManufacturer = qReportData.Manufacturer
            END
            IF (kCurrentManufacturer <> qReportData.Manufacturer)
                kCurrentManufacturer = qReportData.Manufacturer
                qLastPart.QueueNumber = X# - 1
                ! New manufacturer, therefore the last record was the last action for that part
                Add(qLastPart)
                kCurrentPartNumber = qReportData.PartNumber
            END

            IF (kCurrentPartNumber = '')
                kCurrentPartNumber = qReportData.PartNumber
            END
            IF (kCurrentPartNumber <> qReportData.PartNumber)
                kCurrentPartNumber = qReportData.PartNumber
                qLastPart.QueueNumber = X# - 1
                ! New part, therefore the last record was the last action for that part
                Add(qLastPart)
            END
        END
        qLastPart.QueueNumber = X# - 1
        Add(qLastPart)

        ! Mark which of the parts are the "last action"
        SORT(qReportData,qReportData.Manufacturer,qReportData.PartNumber)
        LOOP X# = 1 TO RECORDS(qLastPart)
            GET(qLastPart,X#)
            GET(qReportData,qLastPart.QueueNumber)
            IF (NOT ERROR())
                qReportData.LastAction = TRUE
                PUT(qReportData)
            END
        END

        UpdateProgressWindow('===============')
        UpdateProgressWindow('Buiding Excel Document...')

        ! Create Export Documents
        UpdateProgressWindow('Creating Excel Sheets....')
        UpdateProgressWindow('')

        ! Print Out Queue
        SORT(qReportData,qReportData.Manufacturer,qReportData.PartNumber)
        LOOP X# = 1 TO RECORDS(qReportData)
            GET(qReportData,X#)
            DO UpdateProgressWindow
            DO CancelCheck
            IF (locCancel)
                BREAK
            END

            IF (kCurrentManufacturer <> qReportData.Manufacturer)
                IF (kCurrentManufacturer <> '')
                    FinishFormat(kLastDetailColumn,kCurrentRow)
                END

                UpdateProgressWindow('Exporting: ' & qReportData.Manufacturer)
                UpdateProgressWindow('')

                E1.InsertWorksheet()
                E1.RenameWorksheet(qReportData.Manufacturer)
                DO Title
                kCurrentManufacturer = qReportData.Manufacturer
                kCurrentRow = 2
            END

            WriteLine(qReportData.RecordNumber,kCurrentRow,qReportData.ReportAction,qReportData.LastAction)
            kCurrentRow += 1
        END

        FinishFormat(kLastDetailColumn,kCurrentRow)

        !            SetClipBoard('')
        !            E1.OpenWorkbook(glo:ExportFile)
        !            E1.Copy('A1',clip(kLastDetailColumn) & kRecordCount)
        !            kClipBoard = clipBoard()
        !            SetClipBoard('BLANK')
        !            E1.CloseWorkbook(3)
        !
        !            E1.NewWorkbook()
        !            E1.SelectCells('A2')
        !            SetClipboard(kClipboard)
        !            E1.Paste()
        !            SetClipBoard('BLANK')
        !            E1.RenameWorksheet(kReportName)



        E1.SaveAs(kFilename)
        E1.CloseWorkbook(3)
        E1.Kill()

        UpdatePRogressWindow('Finishing Off Formatting..')

        UpdateProgressWindow('===============')
        UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    ELSE
        UpdateProgressWindow('===============')
        UpdateProgressWindow('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
    END

    !REMOVE(ExportFile)

    DO EndProgress

    ?progress:userstring{PROP:Text} = 'Finished...'
    display()
    ?ProgressCancel{PROP:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{PROP:Hide} = 0
    ACCEPT
        CASE field()
        OF ?Finish
            CASE EVENT()
            OF EVENT:Accepted
                BREAK
            END
        OF ?Button:OpenReportFolder
            CASE EVENT()
            OF EVENT:Accepted
                run('explorer.exe ' & kReportFolder)
            END
        END
    END
    CLOSE(progresswindow)
    Post(EVENT:CloseWindow)




Title               ROUTINE
    DATA
locCol      LONG()
    CODE
        locCol = 1
        e1.writeToCell('Site ID',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('User ID',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Part Number',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Part Description',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Action',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Opening Balance Of Parts Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Cost Per Part',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Cost For Parts On Hand',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Purchases Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Purchase Price Per Unit',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Purchase Value',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Invoice Number',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Invoice Date',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Sales Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Sales Value',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Sales Invoice Number',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Sales Date',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Transfers In Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Transfers In Value',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Date Of Transfer In',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Site Transferred From',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Transfer Out Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Transers Out Value',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Date Of Transfer Out',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Site To Transfer',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Scrap Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Scrap Total Value',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Scrap Date',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Incremented Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Incremented Price',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Incremented Date',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Decremented Qty',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Decremented Price',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Decremented Date',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Decremented Job Number',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Decrement OOW/WAR',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Average Cost Per Unit',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Closing Quantity On Hand',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Value Of Closing Quantity On Hand',E1.ColumnName(locCol) & 1)
        locCol += 1
        e1.writeToCell('Last Action Line Monitory Value Closing Qty On Hand',E1.ColumnName(locCol) & 1)
UpdateProgressWindow      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
        percentprogress = (recordsprocessed / recordstoprocess)*100
        if percentprogress >= 100
            recordsprocessed        = 0
            percentprogress         = 0
            progress:thermometer    = 0
            recordsthiscycle        = 0
        end
        if percentprogress <> progress:thermometer then
            progress:thermometer = percentprogress
            ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

OpenProgressWindow  ROUTINE
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'

CancelCheck         routine
    cancel# = 0
    loccancel = 0
    accept
        Case Event()
        Of Event:Timer
            Break
        Of Event:CloseWindow
            cancel# = 1
            Break
        Of Event:accepted
            Yield()
            If Field() = ?ProgressCancel
                cancel# = 1
                Break
            End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Do you want to finish off building the excel document with the data you have compile so far, or just quit now?','ServiceBase',|
            Icon:Question,'&Finish Report|&Quit Now|&Cancel',3)
        Of 1 ! &Finish Report Button
            locCancel = 2
        Of 2 ! &Quit Now Button
            locCancel = 1
        Of 3 ! &Cancel Button
        End!Case Message
    End!If cancel# = 1


EndProgress         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()
RecolourWindow      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          Access:SRNTExt.Clearkey(srn:KeySRN)
          srn:SRNumber = '020742'&'0'
          If Access:SRNText.Fetch(srn:KeySRN)
              !Error
              SRN:TipText = 'Press F1 to access detailed help'
          End !Access:SRNText.Fetch(stn:KeySRN) = Level:Benign
          ?SRNNumber{Prop:Text}= 'SRN:020742'&'0'
          if clip(SRN:TipText)='' then SRN:TipText = 'Press F1 to access detailed help'.
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ConsolidatedStockReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('locTag',locTag)                                    ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:SRNText.Open()
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:RETSALES.SetOpenRelated()
  Relate:RETSALES.Open                                     ! File RETSALES used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:STOHISTE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATION.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOCK.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOHIST.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! Set Initial Variables
      locStartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
      locEndDate = Today()
  
      locHeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = locHeadAccountNumber
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          locARCSiteLocation = tra:SiteLocation
          locARCCompanyName = tra:Company_Name
      End
  
      Access:LOCATION.Clearkey(loc:Main_Store_Key)
      loc:Main_Store = 'YES'
      SET(loc:Main_Store_Key,loc:Main_Store_Key)
      LOOP
          IF (Access:LOCATION.Next())
              BREAK
          END
          IF (loc:Main_Store <> 'YES')
              BREAK
          END
          locMainStore = loc:Location
          BREAK
      END
  
  
      !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
      pos# = Instring('%',COMMAND(),1,1)
      IF (pos#)
          Access:USERS.ClearKey(use:Password_Key)
          use:Password = clip(sub(command(),POS# + 1,30))
          IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
              locUserName = clip(use:Forename) & ' ' & clip(use:Surname)
          END
      END
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:LOCATION,SELF) ! Initialize the browse manager
  SELF.Open(window)                                        ! Open window
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  ?List{Prop:LineHeight} = 10
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      Alert(0078H) !F9
      Alert(0070H) !F1
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('ConsolidatedStockReport',window)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,loc:ActiveLocationKey)                ! Add the sort order for loc:ActiveLocationKey for sort order 1
  BRW9.AddRange(loc:Active,locTrue)                        ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,loc:Location,1,BRW9)           ! Initialize the browse locator using  using key: loc:ActiveLocationKey , loc:Location
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(locTag,BRW9.Q.locTag)                      ! Field locTag is a hot field or requires assignment from browse
  BRW9.AddField(loc:Location,BRW9.Q.loc:Location)          ! Field loc:Location is a hot field or requires assignment from browse
  BRW9.AddField(loc:RecordNumber,BRW9.Q.loc:RecordNumber)  ! Field loc:RecordNumber is a hot field or requires assignment from browse
  BRW9.AddField(loc:Active,BRW9.Q.loc:Active)              ! Field loc:Active is a hot field or requires assignment from browse
  Do RecolourWindow
  BRW9.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  Relate:SRNText.Close()
    Relate:AUDIT.Close
    Relate:RETSALES.Close
    Relate:SRNTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('ConsolidatedStockReport',window)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Calendar
      ThisWindow.Update
      Calendar12.SelectOnClose = True
      Calendar12.Ask('Select a Date',locStartDate)
      IF Calendar12.Response = RequestCompleted THEN
      locStartDate=Calendar12.SelectedDate
      DISPLAY(?locStartDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar13.SelectOnClose = True
      Calendar13.Ask('Select a Date',locEndDate)
      IF Calendar13.Response = RequestCompleted THEN
      locEndDate=Calendar13.SelectedDate
      DISPLAY(?locEndDate)
      END
      ThisWindow.Reset(True)
    OF ?buttonPrint
      ThisWindow.Update
          DO Reporting
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020742'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020742'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020742'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::10:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
  !Of Event:AlertKey
      If KeyCode() = 0078H
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020742'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020742'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020742'&'0')
      ***
      End !If KeyCode() F9
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

FinishFormat        PROCEDURE(STRING fLastColumn,LONG fLastRow)
kLastSquare     CSTRING(20)
    CODE
        kLastSquare = fLastColumn & fLastRow
        E1.SetCellFontName('Tahoma','A1',kLastSquare)
        E1.SetCellFontSize(8,'A1',kLastSquare)
        E1.SelectCells('B2')
        E1.FreezePanes()
        E1.AutoFilter('A',fLastRow)
        E1.SetColumnWidth('A',fLastColumn)
WriteLine           Procedure(LONG fRecordNumber,LONG fCurrentRow,STRING fAction,BYTE fLastAction)
locCol                  LONG()
CODE
    locCol = 1

    IF (fAction = 'Initial')

        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = fRecordNumber
        IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
            RETURN
        END

        ! Site ID
        Access:TRADEACC.Clearkey(tra:SiteLocationKey)
        tra:SiteLocation = sto:Location
        IF (Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign)
            e1.WriteToCell(tra:Account_Number   ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            e1.WriteToCell(''   ,E1.ColumnName(locCol) & fCurrentRow)
        END
        ! User ID
        locCol += 1
        e1.WriteToCell(''             ,E1.ColumnName(locCol) & fCurrentRow)
        ! Part Number
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatText,,0,,E1.ColumnName(locCol) & fCurrentRow)
        e1.WriteToCell(sto:Part_Number      ,E1.ColumnName(locCol) & fCurrentRow)
        ! Part Description
        locCol += 1
        e1.WriteToCell(sto:Description      ,E1.ColumnName(locCol) & fCurrentRow)
        ! Action
        locCol += 1
        e1.WriteToCell(''              ,E1.ColumnName(locCol) & fCurrentRow)
        ! Opening Balance Of Parts Quantity
        locCol += 1
        e1.WriteToCell(sto:Quantity_Stock     ,E1.ColumnName(locCol) & fCurrentRow)
        ! Cost Per Part
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        IF (sto:Location = locMainStore)
            e1.WriteToCell(FORMAT(sto:Purchase_Cost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            e1.WriteToCell(FORMAT(sto:AveragePurchaseCost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
        END
        ! Cost For Parts On Hand
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        e1.WriteToCell('=(' & E1.ColumnName(locCol-2) & fCurrentRow & '*' & E1.ColumnName(locCol-1) & fCurrentRow & ')'    ,E1.ColumnName(locCol) & fCurrentRow)

        locCol += 28

        ! Average Cost Price Per Unit
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        IF (sto:Location = locMainStore)
            e1.WriteToCell(FORMAT(sto:Purchase_Cost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

        ELSE
            e1.WriteToCell(FORMAT(sto:AveragePurchaseCost,@n14.2), |
                e1.ColumnName(locCol) & fCurrentRow)

        END

        ! Closing Quantity On Hand
        locCol += 1
        e1.WriteToCell(sto:Quantity_Stock      ,e1.ColumnName(locCol) & fCurrentRow)
        ! Value Of Closing Quantity On Hand
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        e1.WriteToCell('=' & e1.ColumnName(locCol - 2) & fCurrentRow & '*' & |
            e1.ColumnName(locCol - 1) & fCurrentRow     , e1.ColumnName(locCol) & fCurrentRow)
        ! Last Action Monitory Value Cosing Quantity On Hand
        locCol += 1
        e1.WriteToCell('=' & e1.ColumnName(locCol - 1) & fCurrentRow, e1.ColumnName(locCol) & fCurrentRow)

    ELSE !IF (fAction = 'Initial')



        Access:STOHIST.Clearkey(shi:record_number_key)
        shi:Record_Number = fRecordNumber
        IF (Access:STOHIST.TryFetch(shi:record_number_key))
            RETURN
        END
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = shi:Ref_Number
        IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
            RETURN
        END

        Access:STOHISTE.Clearkey(stoe:SHIRecordNumberKey)
        stoe:SHIRecordNumber = shi:Record_Number
        IF (Access:STOHISTE.TryFetch(stoe:SHIRecordNumberKey))
        END

        ! Site ID
        Access:TRADEACC.Clearkey(tra:SiteLocationKey)
        tra:SiteLocation = sto:Location
        IF (Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign)
            e1.WriteToCell(tra:Account_Number   ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            e1.WriteToCell(''   ,E1.ColumnName(locCol) & fCurrentRow)
        END
        ! User ID
        locCol += 1
        e1.WriteToCell(shi:User             ,E1.ColumnName(locCol) & fCurrentRow)
        ! Part Number
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatText,,0,,E1.ColumnName(locCol) & fCurrentRow)
        e1.WriteToCell(sto:Part_Number      ,E1.ColumnName(locCol) & fCurrentRow)
        ! Part Description
        locCol += 1
        e1.WriteToCell(sto:Description      ,E1.ColumnName(locCol) & fCurrentRow)
        ! Action
        locCol += 1
        e1.WriteToCell(fAction              ,E1.ColumnName(locCol) & fCurrentRow)
        ! Opening Balance Of Parts Quantity
        locCol += 1
        IF (shi:Transaction_Type = 'DEC')
            e1.WriteToCell(shi:StockOnHand + shi:Quantity     ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            balance# = shi:StockOnHand - shi:Quantity
            IF (balance# < 0)
                balance# = 0
            END

            e1.WriteToCell(balance#     ,E1.ColumnName(locCol) & fCurrentRow)
        END
        ! Cost Per Part
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        IF (sto:Location = locMainStore)
            IF (stoe:PurchaseCost > 0)
                e1.WriteToCell(FORMAT(stoe:PurchaseCost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
            ELSE
                e1.WriteToCell(FORMAT(sto:Purchase_Cost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
            END

        ELSE
            IF (stoe:PreviousAveragePurchaseCost > 0)
                e1.WriteToCell(FORMAT(stoe:PreviousAveragePurchaseCost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
            ELSE
                e1.WriteToCell(FORMAT(sto:AveragePurchaseCost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
            END

        END
        ! Cost For Parts On Hand
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        e1.WriteToCell('=(' & E1.ColumnName(locCol-2) & fCurrentRow & '*' & E1.ColumnName(locCol-1) & fCurrentRow & ')'    ,E1.ColumnName(locCol) & fCurrentRow)


        IF (fAction = 'Purchase')
            ! Purchases Quantity
            locCol += 1
            e1.WriteToCell(shi:Quantity     ,E1.ColumnName(locCol) & fCurrentRow)
            ! Purchase Price Per Unit
            locCol += 1
            e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
            e1.WriteToCell(format(shi:purchase_Cost,@n14.2)  ,E1.ColumnName(locCol) & fCurrentRow)

            ! Purchase Value
            locCol += 1
            e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
            e1.WriteToCell('=(' & E1.ColumnName(locCol-2) & fCurrentRow & '*' & E1.ColumnName(locCol-1) & fCurrentRow & ')'    ,E1.ColumnName(locCol) & fCurrentRow)

            ! Invoice Number
            ! Invoice Date
            pos# = Instring('INVOICE NO:',Upper(shi:Information),1,1)
            IF (pos# > 0)
                locCol += 1
                e1.WriteToCell(sub(shi:Information,pos# + 12,12)               ,E1.ColumnName(locCol) & fCurrentRow)
                locCol += 1
                e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)
            ELSE
                locCol += 2
            END
        ELSE
            locCol += 5
        END


        IF (fAction = 'Sale')
            ! Sales Quantity
            locCol += 1
            e1.WriteToCell(shi:Quantity     ,E1.ColumnName(locCol) & fCurrentRow)
            ! Sales Value
            locCol += 1
            e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
            e1.WriteToCell(FORMAT(shi:Quantity * shi:Sale_Cost,@n14.2)                ,E1.ColumnName(locCol) & fCurrentRow)
            ! Sales Invoice Number
            locCol += 1
            IF (Instring('INVOICE NO: ',Upper(shi:Information),1,1))
                e1.WriteToCell(sub(shi:Information,34,12)               ,E1.ColumnName(locCol) & fCurrentRow)
            ELSE
                ! Lookup Retail Sales For Invoice
                Access:RETSALES.CLearkey(ret:Ref_Number_Key)
                ret:Ref_Number = shi:Job_Number
                IF (Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign)
                    e1.WriteToCell(ret:Invoice_Number   ,e1.ColumnName(locCol) & fCurrentRow)
                END
            END
            locCol += 1
            e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)

        ELSE
            locCol += 4
        END

        IF (fAction = 'Transfer')
            IF (shi:Transaction_Type = 'ADD')
                ! Transfers In Quantity
                locCol += 1
                e1.WriteToCell(shi:Quantity     ,E1.ColumnName(locCol) & fCurrentRow)
                ! Transfers Value
                locCol += 1
                e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
                IF (sto:Location = locMainStore)
                    e1.WriteToCell(FORMAT(sto:Purchase_Cost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

                ELSE
                    e1.WriteToCell(FORMAT(sto:AveragePurchaseCost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

                END

                ! Date Of Transfer In
                locCol += 1
                e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)
                ! Site Transferred From
                locCol += 1
                pos# = INSTRING('LOCATION: ',shi:Information,1,1)
                e1.WriteToCell(sub(shi:Information,pos# + 10,30) ,E1.ColumnName(locCol) & fCurrentRow)
            ELSE
                locCol += 4
            END
            IF (shi:Transaction_Type = 'DEC')
                ! Transfer Out Quantity
                locCol += 1
                e1.WriteToCell(shi:Quantity     ,E1.ColumnName(locCol) & fCurrentRow)
                ! Transfer Out Value
                locCol += 1
                e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
                IF (sto:Location = locMainStore)
                    IF (stoe:PurchaseCost > 0)
                        e1.WriteToCell(FORMAT(stoe:PurchaseCost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
                    ELSE
                        e1.WriteToCell(FORMAT(sto:Purchase_Cost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
                    END

                ELSE
                    IF (stoe:PreviousAveragePurchaseCost > 0)
                        e1.WriteToCell(FORMAT(stoe:PreviousAveragePurchaseCost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
                    ELSE
                        e1.WriteToCell(FORMAT(sto:AveragePurchaseCost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
                    END

                END
                ! Date Of Transfer Out
                locCol += 1
                e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)
                ! Site Transferred To
                locCol += 1
                pos# = INSTRING('LOCATION: ',shi:Information,1,1)
                e1.WriteToCell(sub(shi:Information,pos# + 10,30) ,E1.ColumnName(locCol) & fCurrentRow)
            ELSE
                locCol += 4
            END
        ELSE
            locCol += 8
        END

        IF (fAction = 'Scrap')
            ! Scrap Quantity
            locCol += 1
            pos# = INSTRING('QUANTITY:',shi:Information,1,1)
            qty# = SUB(shi:Information,pos# + 10,10)
            e1.WriteToCell( qty#, E1.ColumnName(locCol) & fCurrentRow)
            ! Scrap Total Value
            locCol += 1
            e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
            IF (sto:Location = locMainStore)
                e1.WriteToCell(FORMAT(sto:Purchase_Cost * qty#,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

            ELSE
                e1.WriteToCell(FORMAT(sto:AveragePurchaseCost * qty#,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

            END

            ! Scrap Date
            locCol += 1
            e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            locCol += 3
        END


        IF (fAction = 'Increment')
            ! Incremented Quantity
            locCol += 1
            e1.WriteToCell(shi:Quantity     ,e1.ColumnName(locCol) & fCurrentRow)
            ! Incremented Price
            locCol += 1
            e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
            IF (sto:Location = locMainStore)
                e1.WriteToCell(FORMAT(sto:Purchase_Cost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

            ELSE
                e1.WriteToCell(FORMAT(sto:AveragePurchaseCost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

            END
            ! Incremented Date
            locCol += 1
            e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            locCol += 3
        END
        IF (fAction = 'Decrement')
            ! Decremented Quantity
            locCol += 1
            e1.WriteToCell(shi:Quantity     ,e1.ColumnName(locCol) & fCurrentRow)
            ! Decremented Price
            locCol += 1
            e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
            IF (sto:Location = locMainStore)
                e1.WriteToCell(FORMAT(sto:Purchase_Cost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

            ELSE
                e1.WriteToCell(FORMAT(sto:AveragePurchaseCost * shi:Quantity,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)

            END

            ! Decremented Date
            locCol += 1
            e1.WriteToCell(FORMAT(shi:Date,@d06b)                   ,E1.ColumnName(locCol) & fCurrentRow)

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = shi:Job_Number
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Decremented Job Number
                locCol += 1
                e1.WriteToCell(shi:Job_Number   ,e1.ColumnName(locCol) & fCurrentRow)
                ! Decremented OOW/Warr
                locCol += 1
                IF (job:Warranty_Job = 'YES')
                    e1.WriteToCell('WAR',   e1.ColumnName(locCol) & fCurrentRow)
                ELSE
                    e1.WriteToCell('OOW',   e1.ColumnName(locCol) & fCurrentRow)
                END
            ELSE
                locCol += 2
            END
        ELSE
            locCol += 5
        END
        ! Average Cost Price Per Unit
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        IF (sto:Location = locMainStore)
            e1.WriteToCell(FORMAT(sto:Purchase_Cost,@n14.2)     ,E1.ColumnName(locCol) & fCurrentRow)
        ELSE
            e1.WriteToCell(FORMAT(GetMarkedDownCost(shi:Purchase_Cost,sto:PurchaseMarkUp),@n14.2), |
                e1.ColumnName(locCol) & fCurrentRow)
        END

        ! Closing Quantity On Hand
        locCol += 1
        e1.WriteToCell(shi:StockOnHand      ,e1.ColumnName(locCol) & fCurrentRow)
        ! Value Of Closing Quantity On Hand
        locCol += 1
        e1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,E1.ColumnName(locCol) & fCurrentRow)
        e1.WriteToCell('=' & e1.ColumnName(locCol - 2) & fCurrentRow & '*' & |
            e1.ColumnName(locCol - 1) & fCurrentRow     , e1.ColumnName(locCol) & fCurrentRow)
        ! Last Action Monitory Value Cosing Quantity On Hand
        IF (fLastAction = TRUE)
            locCol += 1
            e1.WriteToCell('=' & e1.ColumnName(locCol - 1) & fCurrentRow, e1.ColumnName(locCol) & fCurrentRow)
        END



    END


GetMarkedDownCost   Procedure(Real fPurchaseCost,Real fMarkup)
rtnValue REAL
Code
    rtnValue = (fPurchaseCost * 100) / (100 + fMarkup)
        Return rtnValue
UpdateProgressWindow        PROCEDURE(STRING fText)
    CODE
        staque:StatusMessage = clip(fText)
        ADD(StatusQueue)
        select(?List1,RECORDS(StatusQueue))
        display()

!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

BRW9.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      locTag = ''
    ELSE
      locTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (locTag = '*')
    SELF.Q.locTag_Icon = 2                                 ! Set icon from icon list
  ELSE
    SELF.Q.locTag_Icon = 1                                 ! Set icon from icon list
  END
  SELF.Q.loc:Location_Icon = 0


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

XFile PROCEDURE                                            ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,169),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RecolourWindow      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFile')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFile',QuickWindow)                        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFile',QuickWindow)                     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
RecolourWindow      Routine

ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
RecolourWindow      Routine

ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RecolourWindow      Routine

ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit


RecolourWindow      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('HelpBrowser',Window)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('HelpBrowser',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


