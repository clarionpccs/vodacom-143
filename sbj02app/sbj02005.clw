

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02005.INC'),ONCE        !Local module procedure declarations
                     END


WarrantyRepairLimitMessage PROCEDURE                  !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
chosenSelection      BYTE
returnedValue        BYTE
window               WINDOW('Warranty Repair Limit'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Repair Limit Reached'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('The value of this repair exceeds the authorised limit set by the manufacturer an' &|
   'd may require a swap.'),AT(248,178,184,28),USE(?Prompt3),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Do you wish to'),AT(268,212,144,40),USE(chosenSelection),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Authorise swap && B.E.R. the unit'),AT(276,224),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Authorise Repair'),AT(276,236),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           BUTTON,AT(248,258),USE(?ViewCostsButton),TRN,FLAT,LEFT,ICON('costsp.jpg')
                         END
                       END
                       BUTTON,AT(368,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(returnedValue)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020459'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WarrantyRepairLimitMessage')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?WindowTitle{prop:text} = 'Warranty Repair Limit: ' & mod:WarrantyRepairLimit
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELNUM.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      if chosenSelection = 0
          select(?chosenSelection)
          cycle
      end
      
      returnedValue = chosenSelection
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020459'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020459'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020459'&'0')
      ***
    OF ?ViewCostsButton
      ThisWindow.Update
      ViewCosts
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      if keycode() = EscKey then cycle.
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
WebJobEnquiry PROCEDURE                               !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LocalEnquiryType     STRING(1)
LocalJobNo           STRING(20)
LocalIMEINumber      STRING(20)
LocalMobileNumber    STRING(20)
LocalOrderNo         STRING(20)
FoundJobNo           LONG
LocalFindFlag        STRING(1)
window               WINDOW('ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Enquiry Type'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Select Enquiry Type'),USE(?Tab1)
                           OPTION('Select Enquiry Type'),AT(234,164,214,84),USE(LocalEnquiryType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Job No'),AT(242,180),USE(?LocalEnquiryType:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('IMEI Number'),AT(242,196),USE(?LocalEnquiryType:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Mobile Number'),AT(242,212),USE(?LocalEnquiryType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                             RADIO('Order Number'),AT(242,228),USE(?LocalEnquiryType:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('4')
                           END
                           ENTRY(@s20),AT(334,178,,10),USE(LocalJobNo),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           ENTRY(@s20),AT(334,194,,10),USE(LocalIMEINumber),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s20),AT(334,210,,10),USE(LocalMobileNumber),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s20),AT(334,226,,10),USE(LocalOrderNo),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?Close),DISABLE,TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020460'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WebJobEnquiry')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS_ALIAS.Open
  Relate:SUBTRACC_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
    Relate:SUBTRACC_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?LocalEnquiryType
      HIDE(?LocalJobNo)
      HIDE(?LocalIMEINumber)
      HIDE(?LocalMobileNumber)
      HIDE(?LocalMobileNumber)
      HIDE(?LocalOrderNo)
      CASE LocalEnquiryType
      OF 1
         UNHIDE(?LocalJobNo)
         SELECT(?LocalJobNo,1)
      OF 2
         UNHIDE(?LocalIMEINumber)
         SELECT(?LocalIMEINumber,1)
      OF 3
         UNHIDE(?LocalMobileNumber)
         SELECT(?LocalMobileNumber,1)
      OF 4
         UNHIDE(?LocalOrderNo)
         SELECT(?LocalOrderNo,1)
      END !CASE
      ENABLE(?Close)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020460'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020460'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020460'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
InsertAuthorisationNumber PROCEDURE                   !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AuthorisationNumber STRING(30)
window               WINDOW('Authorisation Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Authorisation Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Insert Authorisation Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,206,184,10),USE(tmp:AuthorisationNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Authorisation Number'),TIP('Authorisation Number'),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:AuthorisationNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020451'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertAuthorisationNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      tmp:AuthorisationNumber = ''
      Post(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020451'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020451'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020451'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
FieldChangeReason PROCEDURE (func:String)             !Generated from procedure template - Window

tmp:Close            BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Field Changed'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Field Changed Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('The following field has been changed. Please provide a reason.'),AT(238,116,204,16),USE(?Prompt1),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Field'),AT(240,160),USE(?FieldType),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           TEXT,AT(240,188,200,98),USE(glo:EDI_Reason),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Close)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020449'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('FieldChangeReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?FieldType{prop:Text} = Clip(func:String)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020449'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020449'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020449'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      If glo:EDI_Reason <> ''
          tmp:Close = 1
          Post(event:CloseWindow)
      End !glo:EDI_Reason <> ''
    OF ?Cancel
      ThisWindow.Update
      tmp:Close = 0
      Post(event:Closewindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AccessoryJobMatch PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccessoryNumber  STRING(30)
tmp:JobNumber        LONG
window               WINDOW('Accessory Job Match'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Accessory Job Match'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Accessory Number'),AT(248,194),USE(?tmp:AccessoryNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(328,194,104,10),USE(tmp:AccessoryNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Accessory Number'),TIP('Accessory Number'),UPR
                           STRING('Job Number'),AT(248,212),USE(?String1),FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(328,212),USE(tmp:JobNumber),FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020431'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AccessoryJobMatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBACCNO.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBACCNO.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020431'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020431'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020431'&'0')
      ***
    OF ?tmp:AccessoryNumber
      tmp:JobNumber = ''
      Access:JOBACCNO.Clearkey(joa:AccessoryNoOnlyKey)
      joa:AccessoryNumber = tmp:AccessoryNumber
      If Access:JOBACCNO.Tryfetch(joa:AccessoryNoOnlyKey) = Level:Benign
          !Found
          tmp:JobNumber   = joa:RefNumber
      Else ! If Access:JOBACCNO.Tryfetch(joa:AccessoryNoOnlyKey) = Level:Benign
          !Error
          Case Missive('Cannot find the selected job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      End !If Access:JOBACCNO.Tryfetch(joa:AccessoryNoOnlyKey) = Level:Benign
      Display()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Jobs_Rapid PROCEDURE                           !Generated from procedure template - Window

! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
CurrentTab           STRING(80)
tmp:JobNumber        STRING(20)
save_epr_id          USHORT,AUTO
save_jobsobf_id      USHORT,AUTO
save_exchange_id     USHORT,AUTO
save_joe_id          USHORT,AUTO
save_taf_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
tmp:RestockingNote   BYTE(0)
tmp:chargetype       STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:RepairType       STRING(30)
tmp:WarrantyRepairType STRING(30)
tmp:UnitType         STRING(30)
invoice_number_temp  LONG
tmp:CreateInvoice    BYTE(0)
save_job_ali_id      USHORT,AUTO
tmp:ConsignNo        STRING(20)
tmp:OldConsignNo     STRING(20)
tmp:LabelError       STRING('0 {29}')
tmp:DespatchClose    BYTE(0)
save_lac_id          USHORT,AUTO
save_job_id          USHORT,AUTO
chargeable_job_temp  STRING(3)
warranty_job_temp    STRING(3)
save_cha_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
Print_Despatch_Note_temp STRING(3)
tmp:accountnumber    STRING(15)
account_number2_temp STRING(15)
sav:path             STRING(255)
save_cou_ali_id      USHORT,AUTO
error_queue_temp     QUEUE,PRE(err)
field                STRING(30)
                     END
error_message_temp   STRING(255)
error_type_temp      STRING(1),DIM(50)
check_for_bouncers_temp STRING(3)
saved_ref_number_temp REAL
saved_esn_temp       STRING(16)
save_aud_id          USHORT,AUTO
saved_msn_temp       STRING(16)
engineer_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_ref_number_temp REAL
main_store_quantity_temp REAL
main_store_sundry_temp STRING(3)
engineer_sundry_temp STRING(3)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
Force_Fault_Codes_Temp STRING('NO {1}')
date_completed_temp  DATE
time_completed_temp  TIME
qa_passed_temp       STRING(3)
qa_rejected_temp     STRING(3)
qa_second_passed_temp STRING(3)
location_temp        STRING(30)
date_error_temp      BYTE
engineer_name_temp   STRING(40)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
title_text_temp      STRING(255)
trade_account_temp   STRING(90)
model_details_temp   STRING(90)
DisplayString        STRING(255)
title_text2_temp     STRING(255)
engineer_temp        STRING(30)
check_for_bouncer_temp BYTE(0)
qa_rejection_reason_temp STRING(10000)
exchange_loan_temp   STRING(60)
tmp:invoicetext      STRING(255)
SaveGroup            GROUP,PRE(sav)
ChargeType           STRING(30)
WarrantyChargeType   STRING(20)
RepairType           STRING(30)
WarrantyRepairType   STRING(30)
ChargeableJob        STRING(3)
WarrantyJob          STRING(3)
EstimateLabourCost   REAL
ChargeableLabourCost REAL
WarrantyLabourCost   REAL
EstimatePartsCost    REAL
ChargeablePartsCost  REAL
WarrantyPartsCost    REAL
EstimateCourierCost  REAL
ChargeableCourierCost REAL
WarrantyCourierCost  REAL
HubRepair            BYTE(0)
AccountNumber        STRING(30)
TransitType          STRING(30)
MSN                  STRING(30)
ModelNumber          STRING(30)
DOP                  DATE
OrderNumber          STRING(30)
FaultCode1           STRING(30)
FaultCode2           STRING(30)
FaultCode3           STRING(30)
FaultCode4           STRING(30)
FaultCode5           STRING(30)
FaultCode6           STRING(30)
FaultCode7           STRING(30)
FaultCode8           STRING(30)
FaultCode9           STRING(30)
FaultCode10          STRING(255)
FaultCode11          STRING(255)
FaultCode12          STRING(255)
InvoiceText          STRING(255)
FaultDescription     STRING(255)
                     END
ChangedGroup         GROUP,PRE(sav1)
cct                  BYTE(0)
wct                  BYTE(0)
crt                  BYTE(0)
wrt                  BYTE(0)
chj                  BYTE(0)
wrj                  BYTE(0)
elc                  BYTE(0)
clc                  BYTE(0)
wlc                  BYTE(0)
epc                  BYTE(0)
cpc                  BYTE(0)
wpc                  BYTE(0)
ecc                  BYTE(0)
ccc                  BYTE(0)
wcc                  BYTE(0)
hbr                  BYTE(0)
acc                  BYTE(0)
itt                  BYTE(0)
msn                  BYTE(0)
mdl                  BYTE(0)
dop                  BYTE(0)
ord                  BYTE(0)
fc1                  BYTE(0)
fc2                  BYTE(0)
fc3                  BYTE(0)
fc4                  BYTE(0)
fc5                  BYTE(0)
fc6                  BYTE(0)
fc7                  BYTE(0)
fc8                  BYTE(0)
fc9                  BYTE(0)
fc10                 BYTE(0)
fc11                 BYTE(0)
fc12                 BYTE(0)
ivt                  BYTE(0)
fdt                  BYTE(0)
                     END
tmp:ParcellineName   STRING(255),STATIC
tmp:WorkStationName  STRING(30)
tmp:SkillLevel       LONG
tmp:no               STRING('NO {1}')
tmp:yes              STRING('YES')
tmp:DateAllocated    DATE
tmp:Network          STRING(30)
tmp:HubRepair        BYTE(0)
tmp:HubRepairDate    DATE
tmp:HubRepairTime    TIME
tmp:SaveHubRepair    BYTE(0)
tmp:Charge_t         STRING(30)
tmp:OBFvalidated     BYTE
tmp:OBFvalidateDate  DATE
tmp:OBFvalidateTime  TIME
tmp:SaveOBFvalidated BYTE
tmp:warranty_t       STRING(30)
tmp:FaultCode_t      STRING(30)
tmp:FaultCodeW_T     STRING(30)
tmp:WColourFlag      BYTE(0)
tmp:CColourFlag      BYTE(0)
tmp:EColourFlag      BYTE(0)
tmp:COverwriteRepairType BYTE(0)
tmp:WOverwriteRepairType BYTE(0)
tmp:Claim            REAL
tmp:Handling         REAL
tmp:Exchange         REAL
tmp:IgnoreClaimCosts BYTE(0)
tmp:RRCELabourCost   REAL
tmp:RRCEPartsCost    REAL
tmp:RRCCLabourCost   REAL
tmp:RRCCPartsCost    REAL
tmp:RRCWLabourCost   REAL
tmp:RRCWPartsCost    REAL
Dummy_Q              QUEUE,PRE()
snurglefflumph       BYTE
                     END
PartsCounter         LONG
WarrantyPartsCounter LONG
Bouncer_Text         STRING(20)
tmp:DespatchType     STRING(3)
tmp:Despatched       STRING(3)
tmp:OKPressed        BYTE(0)
tmp:CompletedMode    BYTE(0)
TempFaultCode        STRING(255)
PromptForExcFlag     BYTE
TempFieldNo          BYTE
tmp:DisableLocation  BYTE(0)
tmp:ExchangeRate     LONG
tmp:POPType          STRING(30)
SaveWarrantyRepType  STRING(30)
SaveChargeRepairType STRING(30)
tmp:SaveAccessory    STRING(255),STATIC
tmp:PrintEstimateOnExit BYTE(9)
tmp:CompletedQA      BYTE(0)
SaveAccessoryQueue   QUEUE,PRE(savaccq)
Accessory            STRING(30)
Type                 BYTE(0)
                     END
tmp:DoNotUseAuthTable STRING(30)
tmp:RRCLocation      STRING(30)
tmp:InternalLocation STRING(30)
tmp:InternalEngLocation STRING(30)
tmp:HideNetwork      STRING(30)
tmp:RenameAutorityNo STRING(30)
tmp:AutorityNoName   STRING(30)
tmp:DespatchToCustomer STRING(30)
tmp:StatusSendToARC  STRING(30)
tmp:UseRapidStockAllocation STRING(30)
tmp:UseBERComparison STRING(30)
tmp:BERComparison    STRING(30)
tmp:CheckPartAvailable STRING(30)
tmp:ForceAccCheckComp STRING(30)
tmp:HeadAccount      STRING(30)
tmp:HideSkillLevel   STRING(30)
tmp:CurrentExchangeUnitNumber LONG
tmp:CurrentSecondExchangeUnitNumber LONG
tmp:CShelfLocation   STRING(60)
tmp:WShelfLocation   STRING(60)
tmp:EShelfLocation   STRING(60)
tmp:ARCSiteLocation  STRING(30)
tmp:Booking48HourOption BYTE(0)
tmp:Engineer48HourOption BYTE(0)
tmp:Saved48HourOption BYTE(0)
tmp:CurrentSiteLocation STRING(30)
tmp:BookingSiteLocation STRING(30)
save:Preview         STRING(30)
tmp:Close            BYTE(0)
tmp:CPartQuantity    STRING(4)
tmp:WPartQuantity    STRING(4)
tmp:LockJob          STRING(30)
count                LONG
locAuditNotes        STRING(255)
BRW9::View:Browse    VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Quantity)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Part_Number_NormalFG LONG                         !Normal forground color
par:Part_Number_NormalBG LONG                         !Normal background color
par:Part_Number_SelectedFG LONG                       !Selected forground color
par:Part_Number_SelectedBG LONG                       !Selected background color
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Description_NormalFG LONG                         !Normal forground color
par:Description_NormalBG LONG                         !Normal background color
par:Description_SelectedFG LONG                       !Selected forground color
par:Description_SelectedBG LONG                       !Selected background color
tmp:CPartQuantity      LIKE(tmp:CPartQuantity)        !List box control field - type derived from local data
tmp:CPartQuantity_NormalFG LONG                       !Normal forground color
tmp:CPartQuantity_NormalBG LONG                       !Normal background color
tmp:CPartQuantity_SelectedFG LONG                     !Selected forground color
tmp:CPartQuantity_SelectedBG LONG                     !Selected background color
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Quantity_NormalFG  LONG                           !Normal forground color
par:Quantity_NormalBG  LONG                           !Normal background color
par:Quantity_SelectedFG LONG                          !Selected forground color
par:Quantity_SelectedBG LONG                          !Selected background color
tmp:CShelfLocation     LIKE(tmp:CShelfLocation)       !List box control field - type derived from local data
tmp:CShelfLocation_NormalFG LONG                      !Normal forground color
tmp:CShelfLocation_NormalBG LONG                      !Normal background color
tmp:CShelfLocation_SelectedFG LONG                    !Selected forground color
tmp:CShelfLocation_SelectedBG LONG                    !Selected background color
tmp:CColourFlag        LIKE(tmp:CColourFlag)          !List box control field - type derived from local data
tmp:CColourFlag_NormalFG LONG                         !Normal forground color
tmp:CColourFlag_NormalBG LONG                         !Normal background color
tmp:CColourFlag_SelectedFG LONG                       !Selected forground color
tmp:CColourFlag_SelectedBG LONG                       !Selected background color
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Part_Number_NormalFG LONG                         !Normal forground color
wpr:Part_Number_NormalBG LONG                         !Normal background color
wpr:Part_Number_SelectedFG LONG                       !Selected forground color
wpr:Part_Number_SelectedBG LONG                       !Selected background color
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Description_NormalFG LONG                         !Normal forground color
wpr:Description_NormalBG LONG                         !Normal background color
wpr:Description_SelectedFG LONG                       !Selected forground color
wpr:Description_SelectedBG LONG                       !Selected background color
tmp:WPartQuantity      LIKE(tmp:WPartQuantity)        !List box control field - type derived from local data
tmp:WPartQuantity_NormalFG LONG                       !Normal forground color
tmp:WPartQuantity_NormalBG LONG                       !Normal background color
tmp:WPartQuantity_SelectedFG LONG                     !Selected forground color
tmp:WPartQuantity_SelectedBG LONG                     !Selected background color
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Quantity_NormalFG  LONG                           !Normal forground color
wpr:Quantity_NormalBG  LONG                           !Normal background color
wpr:Quantity_SelectedFG LONG                          !Selected forground color
wpr:Quantity_SelectedBG LONG                          !Selected background color
tmp:WShelfLocation     LIKE(tmp:WShelfLocation)       !List box control field - type derived from local data
tmp:WShelfLocation_NormalFG LONG                      !Normal forground color
tmp:WShelfLocation_NormalBG LONG                      !Normal background color
tmp:WShelfLocation_SelectedFG LONG                    !Selected forground color
tmp:WShelfLocation_SelectedBG LONG                    !Selected background color
tmp:WColourFlag        LIKE(tmp:WColourFlag)          !List box control field - type derived from local data
tmp:WColourFlag_NormalFG LONG                         !Normal forground color
tmp:WColourFlag_NormalBG LONG                         !Normal background color
tmp:WColourFlag_SelectedFG LONG                       !Selected forground color
tmp:WColourFlag_SelectedBG LONG                       !Selected background color
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW39::View:Browse   VIEW(ESTPARTS)
                       PROJECT(epr:Part_Number)
                       PROJECT(epr:Description)
                       PROJECT(epr:Quantity)
                       PROJECT(epr:Record_Number)
                       PROJECT(epr:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
epr:Part_Number        LIKE(epr:Part_Number)          !List box control field - type derived from field
epr:Part_Number_NormalFG LONG                         !Normal forground color
epr:Part_Number_NormalBG LONG                         !Normal background color
epr:Part_Number_SelectedFG LONG                       !Selected forground color
epr:Part_Number_SelectedBG LONG                       !Selected background color
epr:Description        LIKE(epr:Description)          !List box control field - type derived from field
epr:Description_NormalFG LONG                         !Normal forground color
epr:Description_NormalBG LONG                         !Normal background color
epr:Description_SelectedFG LONG                       !Selected forground color
epr:Description_SelectedBG LONG                       !Selected background color
epr:Quantity           LIKE(epr:Quantity)             !List box control field - type derived from field
epr:Quantity_NormalFG  LONG                           !Normal forground color
epr:Quantity_NormalBG  LONG                           !Normal background color
epr:Quantity_SelectedFG LONG                          !Selected forground color
epr:Quantity_SelectedBG LONG                          !Selected background color
tmp:EShelfLocation     LIKE(tmp:EShelfLocation)       !List box control field - type derived from local data
tmp:EShelfLocation_NormalFG LONG                      !Normal forground color
tmp:EShelfLocation_NormalBG LONG                      !Normal background color
tmp:EShelfLocation_SelectedFG LONG                    !Selected forground color
tmp:EShelfLocation_SelectedBG LONG                    !Selected background color
tmp:EColourFlag        LIKE(tmp:EColourFlag)          !List box control field - type derived from local data
tmp:EColourFlag_NormalFG LONG                         !Normal forground color
tmp:EColourFlag_NormalBG LONG                         !Normal background color
tmp:EColourFlag_SelectedFG LONG                       !Selected forground color
tmp:EColourFlag_SelectedBG LONG                       !Selected background color
epr:Record_Number      LIKE(epr:Record_Number)        !Primary key field - type derived from field
epr:Ref_Number         LIKE(epr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::job:Record  LIKE(job:RECORD),STATIC
QuickWindow          WINDOW('Rapid Job Update'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Options'),USE(?Options)
                           ITEM('Estimate'),USE(?OptionsEstimate)
                           ITEM,SEPARATOR
                           ITEM('Allocate Loan'),USE(?OptionsAllocateLoan)
                           ITEM('Allocate Exchange'),USE(?OptionsAllocateExchange)
                         END
                         MENU('Browse'),USE(?Browse)
                           ITEM('Amend Addresses'),USE(?BrowseAmendAddresses)
                           ITEM('Audit Trail'),USE(?BrowseAuditTrail)
                           ITEM('Status Change'),USE(?BrowseStatusChange)
                           ITEM,SEPARATOR
                           ITEM('Contact History'),USE(?BrowseContactHistory)
                           ITEM('Engineer History'),USE(?BrowseEngineerHistory),DISABLE
                           ITEM('Location History'),USE(?BrowseLocationHistory)
                           MENU('SMS Features'),USE(?BrowseSMSFeatures)
                             ITEM('SMS History'),USE(?BrowseSMSFeaturesSMSHistory)
                             ITEM('Send  Loan SMS'),USE(?BrowseSMSFeaturesSendLoanSMS)
                           END
                         END
                       END
                       PROMPT('Amend Job'),AT(8,9),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,7,644,13),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,9),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,21,344,144),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('JOB NO:'),AT(8,37),USE(?Prompt20),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Order Number:'),AT(8,66),USE(?job:Order_Number:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s20),AT(209,71,7,7),USE(?Combo3),DISABLE,HIDE,FORMAT('4L(2)|M~Dummy Q~@s1@'),DROP(10),FROM(Dummy_Q)
                           STRING(@s20),AT(64,37),USE(tmp:JobNumber),FONT(,12,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT(''),AT(228,45,116,12),USE(?ExchangeRepair),FONT('Tahoma',12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Trade Account: '),AT(8,58),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Model Details:'),AT(8,77),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s90),AT(84,77,196,10),USE(model_details_temp),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           ENTRY(@s30),AT(84,66,196,10),USE(job:Order_Number),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           BUTTON,AT(280,55),USE(?buttonCustomerClassification),TRN,FLAT,HIDE,ICON('cusclasp.jpg')
                           ENTRY(@s90),AT(84,58,196,10),USE(trade_account_temp),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           PROMPT('I.M.E.I. Number'),AT(8,87),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(84,87,80,10),USE(job:ESN),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           PROMPT('M.S.N.:'),AT(168,87),USE(?job:MSN:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(200,87,80,10),USE(job:MSN),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           PROMPT('Warranty Repair Type'),AT(204,130),USE(?JOB:Repair_Type_Warranty:Prompt),TRN,FONT(,7,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Warranty Charge Type'),AT(84,130),USE(?job:Warranty_Charge_Type:Prompt),TRN,FONT(,7,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Job Status:'),AT(8,98),USE(?Prompt13),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,98,196,10),USE(job:Current_Status),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           PROMPT('Repair Type'),AT(204,111),USE(?JOB:Repair_Type:Prompt),TRN,FONT(,7,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Charge Type'),AT(84,111),USE(?job:Charge_Type:Prompt),TRN,FONT(,7,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(204,138,116,8),USE(job:Repair_Type_Warranty),SKIP,HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR,READONLY
                           STRING(@s20),AT(8,146),USE(Bouncer_Text),LEFT,FONT(,12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(280,90),USE(?ResubmitJob),TRN,FLAT,HIDE,LEFT,ICON('rsubclap.jpg')
                           CHECK('Chargeable Job'),AT(8,119,72,8),USE(job:Chargeable_Job),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@s30),AT(84,119,116,8),USE(job:Charge_Type),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           ENTRY(@s30),AT(204,119,116,8),USE(job:Repair_Type),SKIP,HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR,READONLY
                           CHECK('Warranty Job'),AT(8,138,72,8),USE(job:Warranty_Job),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@s30),AT(84,138,116,8),USE(job:Warranty_Charge_Type),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       SHEET,AT(504,21,172,86),USE(?Sheet5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 7'),USE(?Tab7)
                           PROMPT('Date Booked'),AT(508,26),USE(?Prompt35),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('JOB COMPLETED'),AT(508,85,164,20),USE(?Completed),RIGHT,FONT(,16,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Date Completed'),AT(508,37),USE(?JOB:Date_Completed:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(588,39,56,10),USE(job:Date_Completed),SKIP,TRN,RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(644,39,28,10),USE(job:Time_Completed),SKIP,TRN,RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,COLOR:Black,COLOR:Silver),READONLY
                           PROMPT('Date QA Passed'),AT(508,50),USE(?Prompt29),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(588,50,56,10),USE(job:Date_QA_Passed),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                           ENTRY(@t1b),AT(644,50,28,10),USE(job:Time_QA_Passed),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                           ENTRY(@d6b),AT(588,23,56,10),USE(job:date_booked),SKIP,TRN,RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(644,23,28,10),USE(job:time_booked),SKIP,TRN,RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,COLOR:Black,COLOR:Silver),READONLY
                           ENTRY(@s3),AT(560,23,28,10),USE(job:who_booked),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                           CHECK('Hub Repair'),AT(508,61),USE(tmp:HubRepair),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hub Repair'),TIP('Hub Repair'),VALUE('1','0')
                           ENTRY(@d6),AT(588,61,56,10),USE(tmp:HubRepairDate),SKIP,TRN,RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hub Repair Date'),TIP('Hub Repair Date'),UPR,READONLY
                           ENTRY(@t1b),AT(644,61,28,10),USE(tmp:HubRepairTime),SKIP,TRN,RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hub Repair Time'),TIP('Hub Repair Time'),UPR,READONLY
                           CHECK('OBF Validated'),AT(508,74),USE(tmp:OBFvalidated),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(588,74,56,10),USE(tmp:OBFvalidateDate),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@t1b),AT(644,74,28,10),USE(tmp:OBFvalidateTime),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),UPR,READONLY
                         END
                       END
                       SHEET,AT(352,21,148,86),USE(?Sheet6),COLOR(0D6E7EFH),SPREAD
                         TAB('48 Hour/ARC Repair/7 Day TAT'),USE(?Tab8),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Booking Option'),AT(355,37,141,27),USE(?BookingOption),BOXED,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Booking Option'),AT(360,47,132,10),USE(?BookingOption:Prompt),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           END
                           OPTION('Engineering option'),AT(356,66,140,34),USE(tmp:Engineer48HourOption),BOXED,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Standard Repair'),AT(360,76),USE(?tmp:Engineer48HourOption:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('4')
                             RADIO('48 Hr Exch'),AT(440,77),USE(?tmp:Engineer48HourOption:Radio1),DISABLE,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('ARC Repair'),AT(360,87),USE(?tmp:Engineer48HourOption:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('7 Day TAT'),AT(440,87),USE(?tmp:Engineer48HourOption:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                           END
                         END
                       END
                       SHEET,AT(352,111,324,116),USE(?Sheet7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange / Loan Details'),USE(?Tab9)
                           PROMPT('Exchange Details'),AT(356,114),USE(?Prompt34),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Loan Details'),AT(560,114),USE(?Prompt34:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Not Issued'),AT(356,124),USE(?ExchangeUnitAttached),FONT(,9,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Not Issued'),AT(560,124),USE(?LoanUnitAttached),FONT(,9,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('2ND EXCHANGE Unit Attached'),AT(356,135),USE(?SecondExchangeUnitAttached),HIDE,FONT(,9,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Job Notes'),AT(356,151),USE(?jobNotes),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(648,146),USE(?engineers_notes),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Fault Description'),AT(356,159),USE(?JOB:Fault_Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Engineers Notes'),AT(516,159),USE(?JOB:Fault_Description:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(356,167,156,58),USE(jbn:Fault_Description),SKIP,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           TEXT,AT(516,167,156,58),USE(jbn:Engineers_Notes),SKIP,VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       SHEET,AT(4,167,344,214),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           BUTTON,AT(280,276),USE(?ChangeJobStatus),TRN,FLAT,HIDE,LEFT,ICON('jobstap.jpg')
                           STRING(@s60),AT(8,327,256,12),USE(exchange_loan_temp),LEFT,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('View Notes'),AT(19,343),USE(?Prompt:ViewNotes),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(8,353),USE(?Button:ContactHistoryEmpty),TRN,FLAT,ICON('conthisp.jpg')
                           BUTTON,AT(76,353),USE(?BouncerButton),TRN,FLAT,HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ICON('prevhisp.jpg')
                           BUTTON,AT(144,353),USE(?ValidatePOP),TRN,FLAT,LEFT,ICON('valpopp.jpg')
                           PROMPT('Authority Number'),AT(8,295),USE(?AuthorityNoPrompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Network'),AT(8,239),USE(?tmp:Network:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,239,124,10),USE(tmp:Network),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Network'),TIP('Network'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(212,236),USE(?LookupNetwork),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Repair Details'),AT(8,172),USE(?Prompt5),FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(280,252),USE(?AmendUnitType),TRN,FLAT,HIDE,LEFT,ICON('unittypp.jpg')
                           STRING('Customer has not replied to estimate SMS'),AT(60,259),USE(?StringNoReply),TRN,HIDE,FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           GROUP('Engineer Details'),AT(84,175,188,57),USE(?EngineerDetails),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('LIQUID DAMAGE'),AT(132,183),USE(?Prompt:LiquidDamage),HIDE,FONT(,12,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                             BUTTON,AT(236,207),USE(?Lookup_Engineer),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Name:'),AT(88,196),USE(?Prompt17),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(128,196,140,11),USE(engineer_name_temp),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Eng Level:'),AT(88,220),USE(?Prompt17:3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s3),AT(132,220),USE(tmp:SkillLevel),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Job Level:'),AT(164,220),USE(?Prompt17:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s3),AT(208,220),USE(jobe:SkillLevel),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Allocated:'),AT(88,207),USE(?Prompt17:4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@d6),AT(132,207),USE(tmp:DateAllocated),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           END
                           BUTTON,AT(280,170),USE(?AllocateJob),TRN,FLAT,HIDE,LEFT,ICON('alljobp.jpg')
                           BUTTON,AT(280,303),USE(?Costs),TRN,FLAT,LEFT,ICON('costsp.jpg')
                           BUTTON,AT(280,353),USE(?Invoice),TRN,FLAT,HIDE,LEFT,ICON('invoicep.jpg')
                           ENTRY(@s30),AT(84,295,124,10),USE(job:Authority_Number),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(608,330),USE(?ColourKey),TRN,FLAT,LEFT,ICON('colkeyp.jpg')
                           BUTTON,AT(208,353),USE(?Button:ResendXML),TRN,FLAT,HIDE,ICON('resnmsp.jpg')
                           BUTTON,AT(8,353),USE(?Button:ContactHistoryFilled),TRN,FLAT,HIDE,ICON('conthisb.jpg')
                           PROMPT('Location'),AT(8,281),USE(?job:Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,281,124,10),USE(job:Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight)
                           BUTTON,AT(280,223),USE(?Fault_Codes),TRN,FLAT,HIDE,LEFT,ICON('faucodep.jpg')
                           BUTTON,AT(280,196),USE(?ViewAccessories),TRN,FLAT,HIDE,LEFT,ICON('accessp.jpg')
                           BUTTON,AT(8,188),USE(?EngineerHistory),TRN,FLAT,HIDE,LEFT,ICON('enghistp.jpg')
                         END
                       END
                       SHEET,AT(352,233,324,148),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('Estimate Parts'),USE(?EstimateTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(356,354,199,27),USE(?EstimateGroup)
                             BUTTON,AT(356,354),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                             BUTTON,AT(424,354),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg')
                             BUTTON,AT(492,354),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           END
                           LIST,AT(356,251,248,100),USE(?List:3),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('108L(2)|FM*~Part Number~@s30@93L(2)|M*~Description~@s30@21R(2)|M*~Qty~@n6@120L(2' &|
   ')|M*~Shelf Location~@s60@0R(2)|M*~Estimate Colour Flag~@n1@'),FROM(Queue:Browse:2)
                         END
                         TAB('&Chargeable Parts'),USE(?ChargeableTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(352,249,320,130),USE(?Chargeable_Group),DISABLE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             LIST,AT(356,251,248,98),USE(?List),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('108L(2)|FM*~Part Number~@s30@95L(2)|M*~Description~@s30@20R(2)|M*~Qty~@s4@0R(2)|' &|
   'M*~Qty~@n4@120L(2)|M*~Shelf Location~@s60@0R(2)*~Chargeable Colour Flag~L@n1@'),FROM(Queue:Browse)
                             BUTTON,AT(356,351),USE(?Multi_Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                             BUTTON,AT(612,274),USE(?ChargeableAdjustment),TRN,FLAT,LEFT,ICON('adjustp.jpg')
                             BUTTON('&Insert '),AT(408,314,24,8),USE(?Insert),HIDE,LEFT,ICON('INSERT.ICO')
                             BUTTON,AT(608,303),USE(?ExchangeButton),TRN,FLAT,ICON('exchp.jpg')
                             BUTTON,AT(424,351),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                             BUTTON,AT(492,351),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           END
                         END
                         TAB('&Warranty Parts'),USE(?Warrantytab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(352,249,320,134),USE(?Warranty_Group),DISABLE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(356,354),USE(?Multi_Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                             BUTTON,AT(608,274),USE(?WarrantyAdjustment),TRN,FLAT,LEFT,ICON('adjustp.jpg')
                             BUTTON,AT(608,303),USE(?ExchangeButton:2),TRN,FLAT,LEFT,ICON('exchp.jpg')
                             LIST,AT(356,249,248,102),USE(?List:2),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('108L(2)|FM*~Part Number~@s30@95L(2)|M*~Description~@s30@20R(2)|M*~Qty~@s4@0R(2)|' &|
   'M*~Qty~@n4@120L(2)|M*~Shelf Location~@s60@0R(2)|M*~Warranty Colour Flag~L@n1@'),FROM(Queue:Browse:1)
                             BUTTON('&Insert'),AT(424,314,24,4),USE(?Insert:3),HIDE,LEFT,ICON('INSERT.ICO')
                             BUTTON,AT(424,354),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg')
                             BUTTON,AT(492,354),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           END
                         END
                       END
                       PROMPT('Claim Rejected'),AT(8,388,188,8),USE(?ClaimStatus),HIDE,FONT(,9,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(207,385),USE(?Button:ReceiptFromPUP),TRN,FLAT,ICON('recpupp.jpg')
                       BUTTON,AT(604,384),USE(?Close),TRN,FLAT,HIDE,ICON('closep.jpg')
                       BUTTON,AT(540,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Claim Rejected'),AT(8,398,188,8),USE(?ClaimStatus:2),HIDE,FONT(,9,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Prompt 40'),AT(356,392),USE(?Prompt:LoyaltyStatus),HIDE,FONT(,12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(280,384),USE(?CompleteButton),TRN,FLAT,LEFT,ICON('comprepp.jpg')
                       STRING(''),AT(0,64,680,320),USE(?CancelText),TRN,HIDE,CENTER,FONT(,48,,FONT:bold),ANGLE(350)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW39                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW39::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_wpr_id   ushort,auto
save_par_id   ushort,auto
save_orp_id   ushort,auto
save_maf_id   ushort,auto
save_map_id   ushort,auto
save_jea_id   ushort,auto
save_xch_ali_id ushort,auto
windowsdir      cstring(144)
systemdir       long
TempFilePath         CSTRING(255)

    Map
CheckWarrantyRepairLimit    Procedure(byte bMode = 0),byte,proc
LocalValidateAccessories    Procedure(),Byte
    End

local   Class
disableJob                  Procedure(byte fDisable)
AllocateExchangePart        Procedure(String func:Type,Byte func:Allocated,Byte func:SecondUnit)
AccessoryOutOfWarranty      Procedure(),Byte
Pricing                     Procedure(Byte func:Warranty)
PrintReturningWaybill       Procedure(Long func:RefNumber)
AllocateExchangeOrderPart   Procedure(String func:Status,Byte func:SecondUnit)
CorrectEngineer             Procedure(),Byte
        End
!Save Account Address Details
accsav:AccountNumber    String(30)

InvoiceDetails  Group,Pre(accsa1)
AccountNumber       String(30)
CompanyName         String(30)
AddressLine1        String(30)
AddressLine2        String(30)
AddressLine3        String(30)
Postcode            String(30)
TelephoneNumber     String(30)
FaxNumber           String(30)
                End
DeliveryDetails     Group,Pre(accsa2)
CompanyName      String(30)
AddressLine1     String(30)
AddressLine2     String(30)
AddressLine3     String(30)
Postcode         String(30)
TelephoneNumber  String(30)
                End
SaveAccessory    File,Driver('TOPSPEED'),Pre(savacc),Name(tmp:SaveAccessory),Create,Bindable,Thread
AccessoryKey            Key(savacc:Accessory),NOCASE,DUP
Record                  Record
Accessory               String(30)
                        End
                    End
!!temp declaratioin of a file to be used for debgging - JC TB 12905
!DebugExport    File,Driver('ASCII'),Pre(debx),Name(DebuggingFile),Create,Bindable,Thread
!Record              Record
!TheLine               String(100)
!                    End
!                End
!!this was only intended to be used in 12.3 and is not to be copied forward
PassFaultCodeQueue      Queue(DefFaultCodeQueue)
                        End

DefModelQueue   Queue,Type
Manufacturer        String(30)
ModelNumber         String(30)
Quantity            Long
                End

PassModelQueue  Queue(DefModelQueue)
                End

!Save Entry Fields Incase Of Lookup
look:job:Charge_Type                Like(job:Charge_Type)
look:job:Warranty_Charge_Type                Like(job:Warranty_Charge_Type)
look:tmp:Network                Like(tmp:Network)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! A
Add_Warranty_Part     Routine
    Clear(wpr:Record)
    get(warparts,0)
    if access:warparts.primerecord() = level:benign
        wpr:ref_number           = job:ref_number
        wpr:adjustment           = 'NO'
        wpr:part_number         = sto:part_number
        wpr:description         = sto:description
        wpr:supplier            = sto:supplier
        wpr:purchase_cost       = sto:purchase_cost
        wpr:sale_cost           = sto:sale_cost
        wpr:retail_cost         = sto:retail_cost
        wpr:quantity             = 1
        wpr:warranty_part        = 'NO'
        wpr:exclude_from_order   = 'NO'
        wpr:fault_codes_checked  = 'YES'
        wpr:part_ref_Number      = sto:ref_number
        wpr:date_ordered         = Today()
        If sto:assign_fault_codes = 'YES'
            Access:STOMODEL.ClearKey(stm:Model_Number_Key)
            stm:Ref_Number   = sto:Ref_Number
            stm:Manufacturer = sto:Manufacturer
            stm:Model_Number = job:Model_Number
            If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                wpr:fault_code1    = stm:Faultcode1
                wpr:fault_code2    = stm:Faultcode2
                wpr:fault_code3    = stm:Faultcode3
                wpr:fault_code4    = stm:Faultcode4
                wpr:fault_code5    = stm:Faultcode5
                wpr:fault_code6    = stm:Faultcode6
                wpr:fault_code7    = stm:Faultcode7
                wpr:fault_code8    = stm:Faultcode8
                wpr:fault_code9    = stm:Faultcode9
                wpr:fault_code10   = stm:Faultcode10
                wpr:fault_code11   = stm:Faultcode11
                wpr:fault_code12   = stm:Faultcode12
            End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
        End!If sto:assign_fault_codes = 'YES'

        !Get the default markups
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = sto:Location
        If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            !Found
            !wpr:OutWarrantyMarkUp   = loc:OutWarrantyMarkUp
        Else ! If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
            !Error
        End !If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
        !wpr:InWarrantyMarkup        = InWarrantyMarkup(job:Manufacturer,loc:Location)

        wpr:InWarrantyMarkup    = sto:PurchaseMarkup! InWarrantyMarkup(job:Manufacturer,loc:Location)
        wpr:OutWarrantyMarkUp   = sto:Percentage_Mark_Up!loc:OutWarrantyMarkUp

        If glo:WebJob
            !This parts is being added by the RRC.
            wpr:RRCAveragePurchaseCost  = sto:AveragePurchaseCost
            wpr:RRCPurchaseCost         = sto:Purchase_Cost     !Markup(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,wpr:InWarrantyMarkup)
            wpr:RRCSaleCost             = sto:Sale_Cost         !Markup(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,wpr:OutWarrantyMarkup)
            wpr:RRCInWarrantyMarkup     = wpr:InWarrantyMarkup
            wpr:RRCOutWarrantyMarkup    = wpr:OutWarrantyMarkup

            !Fill in purchase and sale cost, hopefull this should filter through
            !to every else these fields are used, and not break the program
            wpr:Purchase_Cost           = wpr:RRCPurchaseCost
            wpr:Sale_Cost               = wpr:RRCSaleCost
            wpr:AveragePurchaseCost  = wpr:RRCAveragePurchaseCost
        Else !If glo:WebJob
            !This part is being added by the ARC, but need to check
            !if it was originally booked in by the RRC

            wpr:AveragePurchaseCost = sto:AveragePurchaseCost!sto:Purchase_Cost
            wpr:Purchase_Cost       = sto:Purchase_Cost!Markup(wpr:Purchase_Cost,wpr:AveragePurchaseCost,wpr:InWarrantyMarkup)

            ! VP105
            wpr:Sale_Cost       = sto:Sale_Cost

            !These are blank, unless the part was origially
            !booked by an RRC
            wpr:RRCPurchaseCost     = 0
            wpr:RRCSaleCost         = 0

            !Was this job booked by an RRC?
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:WebJob
                    wpr:RRCAveragePurchaseCost  = wpr:Sale_Cost
                    wpr:RRCPurchaseCost         = Markups(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,InWarrantyMarkup(job:Manufacturer,sto:Location))
                    wpr:RRCSaleCost             = Markups(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,loc:OutWarrantyMarkup)
                    wpr:RRCInWarrantyMarkup     = wpr:InWarrantyMarkup
                    wpr:RRCOutWarrantyMarkup    = wpr:OutWarrantyMarkup
                End !If jobe:WebJob
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        End !If glo:WebJob

        ! Start - Is this part a "correction"? - TrkBs: 5365 (DBH: 26-04-2005)
        If glo:Select1 = 'CORRECT PART'
            wpr:Correction = True
            wpr:Adjustment = True
            wpr:PartAllocated = True
            ! Do not price a "Correction" - TrkBs: 5365 (DBH: 27-04-2005)
            wpr:Purchase_Cost = 0
            wpr:RRCPurchaseCost = 0
            wpr:Sale_Cost = 0
            wpr:RRCSaleCost = 0
            wpr:InWarrantyMarkup = 0
            wpr:RRCInWarrantyMarkup = 0
        Else
            wpr:Correction = False
        End ! If glo:Select1 = 'CORRECT PART'
        ! End   - Is this part a "correction"? - TrkBs: 5365 (DBH: 26-04-2005)

        if access:warparts.insert()
            access:warparts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign

Add_Chargeable_Part     Routine
    Clear(par:record)
    get(parts,0)
    if access:parts.primerecord() = level:benign
        par:ref_number           = job:ref_number
        par:adjustment           = 'NO'
        par:part_number     = sto:part_number
        par:description     = sto:description
        par:supplier        = sto:supplier
        par:purchase_cost   = sto:purchase_cost
        par:sale_cost       = sto:sale_cost
        par:retail_cost     = sto:retail_cost
        par:quantity             = 1
        par:warranty_part        = 'NO'
        par:exclude_from_order   = 'NO'
        par:fault_codes_checked  = 'YES'
        par:part_ref_Number      = sto:ref_number
        par:date_ordered         = Today()
        If sto:assign_fault_codes = 'YES'
            Access:STOMODEL.ClearKey(stm:Model_Number_Key)
            stm:Ref_Number   = sto:Ref_Number
            stm:Manufacturer = sto:Manufacturer
            stm:Model_Number = job:Model_Number
            If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                par:fault_code1    = stm:Faultcode1
                par:fault_code2    = stm:Faultcode2
                par:fault_code3    = stm:Faultcode3
                par:fault_code4    = stm:Faultcode4
                par:fault_code5    = stm:Faultcode5
                par:fault_code6    = stm:Faultcode6
                par:fault_code7    = stm:Faultcode7
                par:fault_code8    = stm:Faultcode8
                par:fault_code9    = stm:Faultcode9
                par:fault_code10   = stm:Faultcode10
                par:fault_code11   = stm:Faultcode11
                par:fault_code12   = stm:Faultcode12

            End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
        End!If sto:assign_fault_codes = 'YES'

        !Get the default markups
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = sto:Location
        If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            !Found
!            par:OutWarrantyMarkUp   = loc:OutWarrantyMarkUp
        Else ! If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
            !Error
        End !If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign

        par:InWarrantyMarkup        = sto:PurchaseMarkup! InWarrantyMarkup(job:Manufacturer,loc:Location)
        par:OutWarrantyMarkUp   = sto:Percentage_Mark_Up!loc:OutWarrantyMarkUp

        If glo:WebJob
            !This parts is being added by the RRC.
            par:RRCAveragePurchaseCost  = sto:AveragePurchaseCost
            par:RRCPurchaseCost         = sto:Purchase_Cost
            par:RRCSaleCost             = sto:Sale_Cost


            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:charge_type
            If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                If cha:Zero_Parts = 'YES'
                    par:RRCSaleCost             = 0
                End
            end !if


            par:RRCInWarrantyMarkup     = sto:PurchaseMarkUp
            par:RRCOutWarrantyMarkup    = sto:Percentage_Mark_Up

            !Fill in purchase and sale cost, hopefull this should filter through
            !to every else these fields are used, and not break the program
            par:Purchase_Cost           = par:RRCPurchaseCost
            par:Sale_Cost               = par:RRCSaleCost
            par:AveragePurchaseCost  = par:RRCAveragePurchaseCost
        Else !If glo:WebJob
            !This part is being added by the ARC, but need to check
            !if it was originally booked in by the RRC

            par:AveragePurchaseCost = sto:AveragePurchaseCost
            par:Purchase_Cost       = sto:Purchase_Cost
            ! VP105
            par:Sale_Cost           = sto:Sale_Cost
            !These are blank, unless the part was origially
            !booked by an RRC


            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:charge_type
            If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                If cha:Zero_Parts_ARC
                    par:Sale_Cost    = 0
                End
            end !if

            par:RRCPurchaseCost     = 0
            par:RRCSaleCost         = 0

            !Was this job booked by an RRC?
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:WebJob
                    par:RRCAveragePurchaseCost  = par:Sale_Cost
                    par:RRCPurchaseCost         = Markups(par:RRCPurchaseCost,par:RRCAveragePurchaseCost,InWarrantyMarkup(job:Manufacturer,sto:Location))
                    par:RRCSaleCost             = Markups(par:RRCSaleCost,par:RRCAveragePurchaseCost,loc:OutWarrantyMarkup)
                    par:RRCInWarrantyMarkup     = par:InWarrantyMarkup
                    par:RRCOutWarrantyMarkup    = par:OutWarrantyMarkup
                End !If jobe:WebJob
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        End !If glo:WebJob

        if access:parts.insert()
            access:parts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign
Add_Estimate_Part     Routine
    Clear(epr:record)
    get(estparts,0)
    if access:estparts.primerecord() = level:benign
        epr:ref_number           = job:ref_number
        epr:adjustment           = 'NO'
        epr:part_number     = sto:part_number
        epr:description     = sto:description
        epr:supplier        = sto:supplier
        epr:purchase_cost   = sto:purchase_cost
        epr:sale_cost       = sto:sale_cost
        epr:retail_cost     = sto:retail_cost
        epr:quantity             = 1
        epr:warranty_part        = 'NO'
        epr:exclude_from_order   = 'NO'
        epr:fault_codes_checked  = 'YES'
        epr:part_ref_Number      = sto:ref_number
        epr:date_ordered         = Today()
        If sto:assign_fault_codes = 'YES'
            epr:fault_code1    = sto:fault_code1
            epr:fault_code2    = sto:fault_code2
            epr:fault_code3    = sto:fault_code3
            epr:fault_code4    = sto:fault_code4
            epr:fault_code5    = sto:fault_code5
            epr:fault_code6    = sto:fault_code6
            epr:fault_code7    = sto:fault_code7
            epr:fault_code8    = sto:fault_code8
            epr:fault_code9    = sto:fault_code9
            epr:fault_code10   = sto:fault_code10
            epr:fault_code11   = sto:fault_code11
            epr:fault_code12   = sto:fault_code12
        End!If sto:assign_fault_codes = 'YES'

        !Get the default markups
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = sto:Location
        If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            !Found
         !   epr:OutWarrantyMarkUp   = loc:OutWarrantyMarkUp
        Else ! If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
            !Error
        End !If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
        !epr:InWarrantyMarkup        = InWarrantyMarkup(job:Manufacturer,loc:Location)
        epr:InWarrantyMarkup        = sto:PurchaseMarkup! InWarrantyMarkup(job:Manufacturer,loc:Location)
        epr:OutWarrantyMarkUp   = sto:Percentage_Mark_Up!loc:OutWarrantyMarkUp

        If glo:WebJob
            !This parts is being added by the RRC.
            epr:RRCAveragePurchaseCost  = sto:AveragePurchaseCost
            epr:RRCPurchaseCost         = sto:Purchase_Cost !Markup(epr:RRCAveragePurchaseCost,epr:RRCAveragePurchaseCost,epr:InWarrantyMarkup)
            epr:RRCSaleCost             = sto:Sale_Cost     !Markup(epr:RRCAveragePurchaseCost,epr:RRCAveragePurchaseCost,epr:OutWarrantyMarkup)
            epr:RRCInWarrantyMarkup     = epr:InWarrantyMarkup
            epr:RRCOutWarrantyMarkup    = epr:OutWarrantyMarkup

            !Fill in purchase and sale cost, hopefull this should filter through
            !to every else these fields are used, and not break the program
            epr:Purchase_Cost           = epr:RRCPurchaseCost
            epr:Sale_Cost               = epr:RRCSaleCost
            epr:AveragePurchaseCost  = epr:RRCAveragePurchaseCost
        Else !If glo:WebJob
            !This part is being added by the ARC, but need to check
            !if it was originally booked in by the RRC
            epr:AveragePurchaseCost = sto:AveragePurchaseCost
            epr:Purchase_Cost       = sto:Purchase_Cost
            ! VP105
            epr:Sale_Cost           = sto:Sale_Cost

            !These are blank, unless the part was origially
            !booked by an RRC
            epr:RRCPurchaseCost     = 0
            epr:RRCSaleCost         = 0
            !Was this job booked by an RRC?
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:WebJob
                    epr:RRCAveragePurchaseCost  = epr:Sale_Cost
                    epr:RRCPurchaseCost         = Markups(epr:RRCAveragePurchaseCost,epr:RRCAveragePurchaseCost,InWarrantyMarkup(job:Manufacturer,sto:Location))
                    epr:RRCSaleCost             = Markups(epr:RRCAveragePurchaseCost,epr:RRCAveragePurchaseCost,loc:OutWarrantyMarkup)
                    epr:RRCInWarrantyMarkup     = epr:InWarrantyMarkup
                    epr:RRCOutWarrantyMarkup    = epr:OutWarrantyMarkup
                End !If jobe:WebJob
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        End !If glo:WebJob


        if access:estparts.insert()
            access:estparts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign
AfterOutFaultList  Routine
Data
save:WChargeType    String(30)
save:WRepairType    String(30)
Code
    !now to see if we could change the repair types

    if SaveChargeRepairType <>  job:Repair_Type  and SaveChargeRepairtype <> ''
        access:reptydef.clearkey(rtd:ManRepairTypeKey)
        rtd:Repair_Type = SaveChargeRepairtype
        rtd:Manufacturer = job:manufacturer
        if access:reptydef.fetch(rtd:ManRepairTypeKey)
            !Can't find one? Let it be changed
        ELSE
            if rtd:BER = 10 or rtd:Ber = 11 then !we have exch or handlinge fee
                !let it be changed
            ELSE
                !reset to what it was before
                job:Repair_Type = SaveChargeRepairtype
            END !If 10 or 11
        ENd !if fetch
    END !If new repair type different

    if SaveWarrantyRepType <>  job:Repair_Type_Warranty and  SaveWarrantyRepType <>''
        access:reptydef.clearkey(rtd:ManRepairTypeKey)
        rtd:Repair_Type = SaveWarrantyRepType
        rtd:Manufacturer = job:manufacturer
        if access:reptydef.fetch(rtd:ManRepairTypeKey)
            !Can't find one? Let it be changed

        ELSE
            if rtd:BER = 10 or rtd:Ber = 11 then !we have exch or handlinge fee
                !let it be changed
            Else
                job:Repair_Type_Warranty = SaveWarrantyRepType
            END !If 10 or 11
        END
    END! if warranty

    !Ok if Charge_chang# - we can change the chargeable fee
    !   if War_change#   - we can change the warranty fee

    If glo:Preview <> 'V'

        If tmp:COverwriteRepairType
            tmp:charge_t = job:Repair_Type
        Else !tmp:COverwriteRepairType
            tmp:Charge_T = ''
        End !tmp:COverwriteRepairType

        If tmp:WOverwriteRepairType
            tmp:warranty_t = job:Repair_Type_Warranty
        Else !tmp:WOverwriteRepairType
            tmp:Warranty_T = ''
        End !tmp:WOverwriteRepairType


        tmp:FaultCode_t = ''
        tmp:FaultCodeW_t = ''

        !Check to see if the prices have changed,
        !if so, reprice the job - L945 (DBH: 05-09-2003)
        If job:Warranty_Job = 'YES'
            save:WChargeType    = job:Warranty_Charge_Type
            save:WRepairType    = job:Repair_Type_Warranty
        End !If job:Warranty_Job = 'YES'

        Calculate_Billing(tmp:charge_t,tmp:warranty_t,tmp:FaultCode_t,tmp:FaultCodeW_t)

        PaulsMessageGlobal = ''

        DO refresh_types

        If job:Warranty_Job = 'YES'
            !Have the charge/repair type changed?
            !If so, force a reprice - L945 (DBH: 05-09-2003)
            If save:WChargeType <> job:Warranty_Charge_Type Or |
                save:WRepairType <> job:Repair_Type_Warranty
                Local.Pricing(1)
            End !save:WRepairType <> job:Repair_Type_Warranty
        End !If job:Warranty_Job = 'YES'


        Access:Manufact.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = job:Manufacturer
        IF Access:Manufact.Fetch(man:Manufacturer_Key)
            !Error!
        ELSE
            IF man:BillingConfirmation = TRUE !Only show this screen if required!
                !Check to see if the prices have changed,
                !if so, reprice the job - L945 (DBH: 05-09-2003)
                If job:Warranty_Job = 'YES'
                    save:WChargeType    = job:Warranty_Charge_Type
                    save:WRepairType    = job:Repair_Type_Warranty
                End !If job:Warranty_Job = 'YES'
                !message('About to call billing confirmation - debug 0')
                Billing_Confirmation(job:Charge_Type,tmp:charge_t,job:Warranty_Charge_Type,tmp:warranty_t,|
                                      tmp:COverwriteRepairType,tmp:WOverwriteRepairType,0)
                !Message('job:Warranty_Charge_Type = '&job:Warranty_Charge_Type)
                DO refresh_types

                If job:Warranty_Job = 'YES'
                    !Have the charge/repair type changed?
                    !If so, force a reprice - L945 (DBH: 05-09-2003)
                    If save:WChargeType <> job:Warranty_Charge_Type Or |
                        save:WRepairType <> job:Repair_Type_Warranty
                        Local.Pricing(1)
                    End !save:WRepairType <> job:Repair_Type_Warranty
                End !If job:Warranty_Job = 'YES'

            END
        END
        DO refresh_types

        If PaulsMessageGlobal <> '' then ! ! #11871 Forgot to add the error message. (Bryan: 04/01/2011)
            Case Missive(PaulsMessageGlobal,'ServiceBase 3g','mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End

        !Write the JOBSE so can show the costs correctly
        Do SetJOBSE
        Access:JOBSE.TryUpdate()
        ?cancel{prop:Disable} = 1


    End !glo:Select21 <> 'RRCVIEWONLY'

    Do ShowCompletedText
    Do Enable_Disable

AddEngineer     Routine
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Error# = 0
        Set(DEFAULT2)
        Access:DEFAULT2.Next()
        If de2:UserSkillLevel
            If use:SkillLevel < jobe:SkillLevel
                Case Missive('The selected Engineer''s Skill level is NOT high enough to be allocated to this job.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Error# = 1
                job:Engineer    = Engineer_Temp
            End !If use:SkillLevel < jobe:SkillLevel
        End !If de2:UserSkillLevel
        If Error# = 0
            Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
            tmp:SkillLevel   = use:SkillLevel
            !Added by Neil at Joes Request

        End !If Error# = 0
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    Do_Status# = 0

    If job:Engineer <> Engineer_Temp
        IF job:Date_Completed <> ''

            If glo:WebJob And SentToHub(job:Ref_Number)
                Case Missive('Warning! This job has been completed. Are you sure you want to change the engineer?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                    Of 1 ! No Button
                        Error# = 1
                End ! Case Missive
            Else !If glo:WebJob
                Error# = 1
                Case Missive('Cannot change engineer. This job has been completed.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End !If glo:WebJob
        End !IF job:Date_Completed <> ''
        If Error# = 0
            If Engineer_Temp = ''
                ! Inserting (DBH 11/05/2006) #7597 - First allocation of engineer. Send SMS/Email Alert
                If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                    ! Do not send if ARC engineer being assign to RRC job (DBH: 11-05-2006)
                    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                    jobe2:RefNumber = job:Ref_Number
                    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                        ! Found
!                        !sms now covered by change of status routine - this is a duplicate
!                        If jobe2:SMSNotification
!
!                            !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','SMS',jobe2:SMSAlertNumber,'',0,'')
!                            SendSMSText('J','Y','N')
!
!                        End ! If jobe2:SMSNotification

                        If jobe2:EmailNotification
                            AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','EMAIL','',jobe2:EmailAlertAddress,0,'')
                        End ! If jobe2:EmailNotification
                    Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                        ! Error
                    End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                End ! If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                ! End (DBH 11/05/2006) #7597

                If AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))

                End ! If AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    joe:Status        = 'ALLOCATED'
                    joe:StatusDate    = Today()
                    joe:StatusTime    = Clock()
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = jobe:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign


            Else !If Engineer_Temp = ''
                Case Missive('Are you sure you want to change the engineer?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button

                        IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER CHANGED TO ' & CLip(job:engineer),'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)))
                        END ! IF

                        If glo:WebJob and job:Date_Completed <> '' And SentToHub(job:ref_Number)
                            !The engineer is being changed at the RRC after the job has come back from hub
                            If Access:JOBSENG.PrimeRecord() = Level:Benign
                                joe:JobNumber     = job:Ref_Number
                                joe:UserCode      = job:Engineer
                                joe:DateAllocated = Today()
                                joe:TimeAllocated = Clock()
                                joe:AllocatedBy   = use:User_Code
                                joe:Status        = 'ENGINEER QA'
                                joe:StatusDate    = Today()
                                joe:StatusTime    = Clock()
                                access:users.clearkey(use:User_Code_Key)
                                use:User_Code   = job:Engineer
                                access:users.fetch(use:User_Code_Key)

                                joe:EngSkillLevel = use:SkillLevel
                                joe:JobSkillLevel = jobe:SkillLevel

                                If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:JOBSENG.TryInsert() = Level:Benign
                            End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                        Else !If glo:WebJob and job:Date_Completed <> ''
                            !Lookup current engineer and mark as escalated
                            Save_joe_ID = Access:JOBSENG.SaveFile()
                            Access:JOBSENG.ClearKey(joe:UserCodeKey)
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = Engineer_Temp
                            joe:DateAllocated = Today()
                            Set(joe:UserCodeKey,joe:UserCodeKey)
                            Loop
                                If Access:JOBSENG.PREVIOUS()
                                   Break
                                End !If
                                If joe:JobNumber     <> job:Ref_Number       |
                                Or joe:UserCode      <> Engineer_Temp      |
                                Or joe:DateAllocated > Today()       |
                                    Then Break.  ! End If
                                If joe:Status = 'ALLOCATED'
                                    joe:Status = 'ESCALATED'
                                    joe:StatusDate = Today()
                                    joe:StatusTime = Clock()
                                    Access:JOBSENG.Update()
                                End !If joe:Status = 'ALLOCATED'
                                Break
                            End !Loop
                            Access:JOBSENG.RestoreFile(Save_joe_ID)

                            If Access:JOBSENG.PrimeRecord() = Level:Benign
                                joe:JobNumber     = job:Ref_Number
                                joe:UserCode      = job:Engineer
                                joe:DateAllocated = Today()
                                joe:TimeAllocated = Clock()
                                joe:AllocatedBy   = use:User_Code
                                joe:Status        = 'ALLOCATED'
                                joe:StatusDate    = Today()
                                joe:StatusTime    = Clock()
                                access:users.clearkey(use:User_Code_Key)
                                use:User_Code   = job:Engineer
                                access:users.fetch(use:User_Code_Key)

                                joe:EngSkillLevel = use:SkillLevel
                                joe:JobSkillLevel = jobe:SkillLevel

                                If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:JOBSENG.TryInsert() = Level:Benign
                            End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                        End !If glo:WebJob and job:Date_Completed <> ''

                        Engineer_Temp   = job:Engineer


                    Of 1 ! No Button
                        Job:Engineer    = Engineer_Temp
                End ! Case Missive
            End !If Engineer_Temp = ''
        Else !!If Error# = 0
            job:Engineer    = Engineer_Temp
        End !If Error# = 0

    End !If job:Engineer <> Engineer_Temp

    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
        tmp:SkillLevel   = use:SkillLevel
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
    End!If do_status# = 1

    !Save record, so the engineer is saved
    Access:JOBS.Update()
    ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
    UpdateDateTimeStamp(job:Ref_Number)
    ! End (DBH 16/09/2008) #10253

    Do Lookup_Engineer
! C
CheckEstimateReplies      Routine

    if job:Current_Status = '520 ESTIMATE SENT' then

        !find the relevant text definition
        Access:SMSText.clearkey(SMT:Key_StatusType_Location_Trigger)
        SMT:Status_Type = 'J'
        if glo:WebJob
            SMT:Location = 'A'
        ELSe
            SMT:Location = 'F'
        END
        SMT:Trigger_Status = '520 ESTIMATE SENT'
        if access:SMSText.fetch (SMT:Key_StatusType_Location_Trigger)
            !Error
            EXIT
        END

        !if the customer has already received the maximum number or emails then display the on screen prompt
        count = 0
        Access:SMSMail.clearkey(sms:DateTimeInsertedKey)
        sms:RefNumber = Job:Ref_number
        sms:DateInserted = 0
        sms:TimeInserted = 0
        Set(sms:DateTimeInsertedKey,sms:DateTimeInsertedKey)
        loop
            if access:SMSMail.next() then break.
            if sms:RefNumber <> Job:Ref_number then break.
            if sms:SMSType = 'E' then count += 1.
        END !loop

        if Count > SMT:Resend_Estimate_Days then
            unhide(?StringNoReply)
        END

    END !if 520 estiimate sent

    exit
CheckContactHistory             Routine
    Found# = 0
    Access:CONTHIST.Clearkey(cht:Ref_Number_Key)
    cht:Ref_Number = job:Ref_Number
    Set(cht:Ref_Number_Key,cht:Ref_Number_Key)
    Loop ! Begin Loop
        If Access:CONTHIST.Next()
            Break
        End ! If Access:CONTHIST.Next()
        If cht:Ref_Number <> job:Ref_Number
            Break
        End ! If cht:Ref_Number <> job:Ref_Number
        Found# = 1
    End ! Loop
    If Found# = 0
        ?Button:ContactHistoryEmpty{prop:Hide} = False
        ?Button:ContactHistoryFilled{prop:Hide} = True
        ?Prompt:ViewNotes{prop:Hide} = True
    Else ! If Found# = 0
        ?Button:ContactHistoryEmpty{prop:Hide} = True
        ?Button:ContactHistoryFilled{prop:Hide} = False
        ?Prompt:ViewNotes{prop:Hide} = False
    End ! If Found# = 0
CallViewCosts       Routine
Data
save:WChargeType    String(30)
save:WRepairType    String(30)
Code
    If glo:Preview <> 'V'

        If tmp:COverwriteRepairType
            tmp:charge_t = job:Repair_Type
        Else !tmp:COverwriteRepairType
            tmp:Charge_T = ''
        End !tmp:COverwriteRepairType

        If tmp:WOverwriteRepairType
            tmp:warranty_t = job:Repair_Type_Warranty
        Else !tmp:WOverwriteRepairType
            tmp:Warranty_T = ''
        End !tmp:WOverwriteRepairType


        tmp:FaultCode_t = ''
        tmp:FaultCodeW_t = ''

        !Check to see if the prices have changed,
        !if so, reprice the job - L945 (DBH: 05-09-2003)
        If job:Warranty_Job = 'YES'
            save:WChargeType    = job:Warranty_Charge_Type
            save:WRepairType    = job:Repair_Type_Warranty
        End !If job:Warranty_Job = 'YES'

        Calculate_Billing(tmp:charge_t,tmp:warranty_t,tmp:FaultCode_t,tmp:FaultCodeW_t)


        PaulsMessageGlobal = ''
        DO refresh_types

        If job:Warranty_Job = 'YES'
            !Have the charge/repair type changed?
            !If so, force a reprice - L945 (DBH: 05-09-2003)
            If save:WChargeType <> job:Warranty_Charge_Type Or |
                save:WRepairType <> job:Repair_Type_Warranty
                Local.Pricing(1)
            End !save:WRepairType <> job:Repair_Type_Warranty
        End !If job:Warranty_Job = 'YES'


        Access:Manufact.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = job:Manufacturer
        IF Access:Manufact.Fetch(man:Manufacturer_Key)
            !Error!
        ELSE
            IF man:BillingConfirmation = TRUE !Only show this screen if required!


                !Check to see if the prices have changed,
                !if so, reprice the job - L945 (DBH: 05-09-2003)
                If job:Warranty_Job = 'YES'
                    save:WChargeType    = job:Warranty_Charge_Type
                    save:WRepairType    = job:Repair_Type_Warranty
                End !If job:Warranty_Job = 'YES'
                !Message('About to call billing confirmation - debug 1')
                Billing_Confirmation(job:Charge_Type,tmp:charge_t,job:Warranty_Charge_Type,tmp:warranty_t,|
                                      tmp:COverwriteRepairType,tmp:WOverwriteRepairType,0)
                !Message('job:Chargeable_Job And job:Warranty_Job '&job:Chargeable_Job &' '&job:Warranty_Job)
                DO refresh_types
                !Message('job:Chargeable_Job And job:Warranty_Job - after call '&job:Chargeable_Job &' '&job:Warranty_Job)
                If job:Warranty_Job = 'YES'
                    !Have the charge/repair type changed?
                    !If so, force a reprice - L945 (DBH: 05-09-2003)
                    If save:WChargeType <> job:Warranty_Charge_Type Or |
                        save:WRepairType <> job:Repair_Type_Warranty
                        Local.Pricing(1)
                    End !save:WRepairType <> job:Repair_Type_Warranty
                End !If job:Warranty_Job = 'YES'

            END
        END
        DO refresh_types
        !Message('job:Chargeable_Job And job:Warranty_Job - debug 3 '&job:Chargeable_Job &' '&job:Warranty_Job)

        If PaulsMessageGlobal <> '' then
            Case Missive(PaulsMessageGlobal,'ServiceBase 3g','mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End


        !Write the JOBSE so can show the costs correctly
        Do SetJOBSE
        Access:JOBSE.TryUpdate()

    End !glo:Select21 <> 'RRCVIEWONLY'
ChargeableExchangeButton        Routine
Data
local:ExchangeAttached      Byte(0)
local:SecondExchangeAttached        Byte(0)
local:AllocateUnit          Byte(0)
local:AllocateSecondUnit    Byte(0)
local:SaveExchangeUnitNumber    Long
Code
    !Has the booking option been set?
    !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
    !If tmp:Booking48HourOption <> 0    !TB13298 - remove reliance on Booking Option - always error on Engineer Option
        If tmp:Engineer48HourOption = 0
            Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If tmp:Engineer48HourOption = 0
    !End !tmp:Booking48HourOption <> 0

    !Check for existing unit - then exchange
    !chargeable parts and partnumber = EXCH?
    If job:Engineer = ''
        Case Missive('You must allocate an engineer to this job.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else !job:Engineer = ''
        If Local.CorrectEngineer() = False
            Exit
        End !If Local.CorrectEnginer() = False

        !Check to see if ARC is adding a second exchange -  (DBH: 06-01-2004)
        Save_par_ID = Access:PARTS.SaveFile()
        Access:PARTS.Clearkey(par:Part_Number_Key)
        par:Ref_Number  = job:Ref_Number
        par:Part_Number = 'EXCH'
        Set(par:Part_Number_Key,par:Part_Number_Key)
        Loop
            If Access:PARTS.Next()
                Break
            End !If Access:PARTS.Next(0)
            If par:Ref_Number <> job:Ref_Number |
            Or par:Part_Number <> 'EXCH'|
                Then Break.
            !There is an Exchange Part Attached -  (DBH: 06-01-2004)
            local:ExchangeAttached = True
            If ~glo:WebJob
                If par:SecondExchangeUnit = True
                    local:SecondExchangeAttached = True
                End !If par:SecondExchangeUnit = True
            End !If ~glo:WebJob
        End !Loop
        Access:PARTS.RestoreFile(Save_par_ID)

        If local:ExchangeAttached
            If glo:WebJob
                Case Missive('This job already has an exchange unit attached.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            Else !If glo:WebJob
                If local:SecondExchangeAttached = False
                    Case Missive('This job already has ONE exchange unit attached. Do you wish to add a SECOND unit?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            local:AllocateUnit = True
                            local:AllocateSecondUnit = True
                        Of 1 ! No Button
                    End ! Case Missive
                Else !If local:SecondExchangeUnit = False
                    Case Missive('This job already has an exchange unit attached.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                End !If local:SecondExchangeUnit = False
            End !If glo:WebJob
        Else !If local:ExchangeAttached
            Case Missive('Are you sure you want to allocate an Exchange Unit?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    local:AllocateUnit = True
                Of 1 ! No Button
            End ! Case Missive
        End !If local:ExchangeAttached

        If local:AllocateUnit
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location    = use:Location
                sto:Part_Number = 'EXCH'
                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Found
                    If loc:UseRapidStock
                        Local.AllocateExchangePart('CHA',0,local:AllocateSecondUnit)
                    Else !If loc:UseRapidStock
                        !Call menu option - 3973 (DBH: 01-03-2004)
                        Post(Event:Accepted,?OptionsAllocateExchange)

                    End !If loc:UseRapidStock
                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Error
                    Case Missive('You must have a part setup in stock control with a Part Number of "EXCH" under the location ' & CLip(use:Location) & '.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        Else !If local:AllocateUnit

        End !If local:AllocateUnit
    End! If job:Engineer = ''
CheckPrefixLabType    Routine
Data
local:Level    Byte(0)
Code
    If job:Manufacturer <> 'SAMSUNG'
        Exit
    End ! If job:Manufacturer <> 'SAMSUNG'
    local:Level = 0
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number
        If wpr:Correction = 1
            Cycle
        End ! If wpr:Correction = 1
        Access:MANSAMP.Clearkey(msp:RecordNumberKey)
        msp:RecordNumber = 1
        Set(msp:RecordNumberKey,msp:RecordNumberKey)
        Loop
            If Access:MANSAMP.Next()
                Break
            End ! If Access:MANSAMP.Next()
            If Sub(wpr:Part_Number,1,Len(Clip(msp:PartNumberPrefix))) = msp:PartNumberPrefix
                Case msp:LabType
                Of 'L3'
                    local:Level = 3
                    Break
                Of 'L2'
                    If local:Level <> 3
                        local:Level = 2
                    End ! If local:Level > 2
                Of 'L1'
                    If local:Level = 0
                        local:Level = 1
                    End ! If local:Level < 3
                End ! Case msp:LabType

            End ! If Sub(wpr:Part_Number,1,Len(Clip(msp:PartNumberPrefix)) = msp:PartNumberPrefix
        End ! Loop (MANSAMP)
        If local:Level = 3
            Break
        End ! If local:Level = 1
    End ! Loop (WARPARTS)

    Case local:Level
    Of 1
        job:Fault_Code3 = 'L1'
    Of 2
        job:Fault_Code3 = 'L2'
    Of 3
        job:Fault_Code3 = 'L3'
    End ! Case local:Level
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Check_For_Despatch        Routine
    Set(defaults)
    access:defaults.next()

    despatch# = 0

    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        despatch# = 1
    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'

    If despatch# = 1
        tmp:DespatchClose   = 0
        tmp:RestockingNote  = 0
        If job:despatched <> 'YES'
            If ToBeLoaned() = 1
                restock# = 0
                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                    Case Missive('A Loan Unit has been issued to this job, but the Initial Transit Type has been set-up so that an Exchange Unit is required.'&|
                      '<13,10>Do you wish to continue and mark this unit for DESPATCH back to the Customer, or do you want to RESTOCK it?','ServiceBase 3g',|
                                   'mquest.jpg','Restock|Despatch')
                        Of 2 ! Despatch Button
                        Of 1 ! Restock Button
                            ForceDespatch()
                            tmp:restockingnote = 1
                            restock# = 1
                    End ! Case Missive
                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                If restock# = 0
                    If glo:WebJob
                        tmp:Despatched = 'REA'
                        tmp:DespatchType = 'JOB'
                    Else !If glo:WebJob
                        IF tmp:DoNotUseAuthTable = 1
                            job:despatched  = 'REA'
                        Else !IF GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                            job:despatched  = 'LAT'
                        End !IF GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI') = 1

                        job:despatch_type   = 'JOB'

                    End !If glo:WebJob
                End!If restock# = 0
            Else!IF ToBeLoaned() = 1
                If ToBeExchanged() ! Job Will Be Exchanged
                    ForceDespatch()
                    tmp:restockingnote = 1
                Else!If ToBeExchanged() ! Job Will Be Exchanged
                    If glo:WebJob
                        tmp:Despatched  = 'REA'
                        tmp:DespatchType = 'JOB'
                    Else !If glo:WebJob
                        job:date_Despatched = DespatchANC(job:courier,'JOB')
                    End !If glo:WebJob


                    access:courier.clearkey(cou:courier_key)
                    cou:courier = job:courier
                    if access:courier.tryfetch(cou:courier_key) = Level:Benign
                        If cou:despatchclose = 'YES'
                            tmp:DespatchClose   = 1
                        Else!If cou:despatchclose = 'YES'
                            tmp:DespatchClose   = 0
                        End!If cou:despatchclose = 'YES'
                    End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                End!If ToBeExchanged() ! Job Will Be Exchanged
            End!If ToBeLoaned() = 1
        End!If job:despatched <> 'YES'
    End!If despatch# = 1
CountParts      Routine
    ! Refresh The Browses (DBH: 18-10-2005)
    PartsCounter = 0
    Count# = 0
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
        If par:Part_Number <> 'EXCH'
            PartsCounter += 1
        End !If par:Part_Number <> 'EXCH'

    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)

    ?ChargeableTab{prop:Text} = 'Chargeable Parts (' & Count# & ')'

    Count# = 0
    WarrantyPartsCounter = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
        If wpr:Part_Number <> 'EXCH'
            WarrantyPartsCounter += 1
        End !If par:Part_Number <> 'EXCH'
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    ?WarrantyTab{prop:Text} = 'Warranty Parts (' & Count# & ')'

    Count# = 0
    Save_epr_ID = Access:ESTPARTS.SaveFile()
    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = job:Ref_Number
    Set(epr:Part_Number_Key,epr:Part_Number_Key)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
        If epr:Part_Number <> 'EXCH'
            PartsCounter += 1
        End !If par:Part_Number <> 'EXCH'
    End !Loop
    Access:ESTPARTS.RestoreFile(Save_epr_ID)

    ?EstimateTab{prop:Text} = 'Estimate Parts (' & Count# & ')'

    Display()

    ! Inserting (DBH 06/05/2008) # 9723 - Check the prefix of the part numbers, and set the " Lab Type" fault code
    Do CheckPrefixLabType
    ! End (DBH 06/05/2008) #9723
CheckExchangePrompt  ROUTINE
  Access:MANFAULO.CLEARKEY(mfo:Field_Key)
  mfo:Manufacturer = job:Manufacturer
  mfo:Field_Number = TempFieldNo
  mfo:Field = TempFaultCode
  IF ~Access:MANFAULO.Fetch(mfo:Field_Key) THEN
     IF mfo:PromptForExchange THEN
        PromptForExcFlag = 1
     END !IF
  END !IF
CheckRequiredFields     Routine

    If ForceRepairType('B')
!        If ?job:Repair_Type{prop:Hide} = 0
            ?job:Repair_Type{prop:color} = 0BDFFFFH
            ?job:Repair_Type{prop:Req} = 1
!        Else !If ?job:Repair_Type{prop:Hide} = 0
!            ?job:Repair_Type{prop:color} = color:White
!            ?job:Repair_Type{prop:Req} = 0
!        End !If ?job:Repair_Type{prop:Hide} = 0
!        If ?job:Repair_Type_Warranty{prop:Hide} = 0
            ?job:Repair_Type_Warranty{prop:color} = 0BDFFFFH
            ?job:Repair_Type_Warranty{prop:Req} = 1
!        Else !If ?job:Repair_Type_Warranty{prop:Hide} = 0
!            ?job:Repair_Type_Warranty{prop:color} = color:White
!            ?job:Repair_Type_Warranty{prop:Req} = 0
!        End !If ?job:Repair_Type_Warranty{prop:Hide} = 0
    Else !If ForceRepairType('B')
        ?job:Repair_Type{prop:color} = color:White
        ?job:Repair_Type{prop:Req} = 0
        ?job:Repair_Type_Warranty{prop:color} = color:White
        ?job:Repair_Type_Warranty{prop:Req} = 0
    End !If ForceRepairType('B')
Check_Parts     Routine

    !Call the new routine to check the status of the job, based on the spares - TrkBs: 4625 (DBH: 17-02-2005)
    Case CheckParts('C')
        Of 1 !Pending Order
            GetStatus(330,0,'JOB')
        Of 2 !On Order
            GetStatus(335,0,'JOB')
        Of 3 !Back Order spares
            GetStatus(340,0,'JOB')
        Of 4 !Spares Received
            GetStatus(345,0,'JOB')
        Else
            Case CheckParts('W')
                Of 1 !Pending Order
                    GetStatus(330,0,'JOB')
                Of 2 !On Order
                    GetStatus(335,0,'JOB')
                Of 3 !Back Order Spares
                    GetStatus(340,0,'JOB')
                Of 4 !Spares Received
                    GetStatus(345,0,'JOB')
                Else
                    If Sub(job:Current_Status,1,3) = '330' Or Sub(job:Current_Status,1,3) = '340'
                        !There are no parts on order, yet the status is 330/340. Change it back to the previous status - TrkBs: 4625 (DBH: 17-02-2005)
                        Save_aus_ID = Access:AUDSTATS.SaveFile()
                        Access:AUDSTATS.ClearKey(aus:StatusTimeKey)
                        aus:RefNumber   = job:Ref_Number
                        aus:Type        = 'JOB'
                        aus:DateChanged = Today() + 1
                        Set(aus:StatusTimeKey,aus:StatusTimeKey)
                        Loop
                            If Access:AUDSTATS.PREVIOUS()
                               Break
                            End !If
                            If aus:RefNumber   <> job:Ref_Number      |
                            Or aus:Type        <> 'JOB'      |
                            Or aus:DateChanged > Today() + 1       |
                                Then Break.  ! End If

                            If Sub(aus:NewStatus,1,3) = '330'
                                GetStatus(CLIP(SUB(aus:OldStatus,1,3)),0,'JOB') !Change to previous
                                Break
                            End !If Sub(aud:NewStatus,1,3) = '330'
                            If Sub(aus:NewStatus,1,3) = '340'
                                GetStatus(CLIP(SUB(aus:OldStatus,1,3)),0,'JOB') !Change to previous
                                Break
                            End ! If Sub(aus:NewStatus,1,3) = '340'
                        End !Loop
                        Access:AUDSTATS.RestoreFile(Save_aus_ID)
                    End ! If Sub(job:Current_Status,1,3) = '330'


            End ! Case CheckParts('W')
    End ! Case CheckParts('C')
    Display()
    !End   - Call the new routine to check the status of the job, based on the spares - TrkBs: 4625 (DBH: 17-02-2005)
CustomerClassificationButton        Routine
    Data
locLifeTimeValue        String(30)
locAveSpend             String(30)
locLoyaltyStatus        String(30)
locUpgradeDate          Date()
locIDNumber             String(30)
    Code
        CustClassificationFields(job:Ref_Number,locLifeTimeValue,locAveSpend,|
        locLoyaltyStatus,locUpgradeDate,locIDNumber)

        MobileNumberStatus(locLifeTimeValue,locAveSpend,locLoyaltyStatus |
                        ,locUpgradeDate,locIDNumber)
! D
DisplayCompleteButton Routine

    if job:completed = 'YES'
        ?CompleteButton{prop:Disable} = True
    else
        ?CompleteButton{prop:Disable} = False
    end

    display()
! E
Enable_Disable      Routine
    !Job Types
    If job:Chargeable_Job = 'YES'
        !Show estimate menu
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty    = 'NO'
        cha:Charge_Type = job:Charge_Type
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If cha:Allow_Estimate
                ?OptionsEstimate{prop:Disable} = 0
            End !If cha:Allow_Estimate
        Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign

        ?job:Charge_Type{prop:Hide} = 0
        ?job:Charge_Type:Prompt{prop:Hide} = 0

        ?job:Repair_Type{prop:Hide} = 0
        ?job:Repair_Type:Prompt{prop:Hide} = 0
        !?LookupChargeableRepairType{prop:Hide} = 0

        Estimate# = 0
        If job:Estimate = 'YES'
            ?EstimateTab{prop:Hide} = 0
            If job:Estimate_Rejected <> 'YES' and job:Estimate_Accepted <> 'YES'
                Estimate# = 1
            End !If job:Estimate_Rejected <> 'YES' and job:Estimate_Accepted <> 'YES'
        ELSE
            ?EstimateTab{prop:hide} = 1
        End !If job:Estimate = 'YES'

        If Estimate# = 1
            ?ChargeableTab{prop:Hide} = 1
        Else !If Estimate# = 1
            ?ChargeableTab{prop:Hide} = 0
        End !If Estimate# = 1

!        ?Chargeable_Group{prop:Disable} = 0
    Else !If job:Chargeable_Job = 'YES'
        ?EstimateTab{prop:hide} = 1
        ?job:Charge_Type{prop:Hide} = 1
        ?job:Charge_Type:Prompt{prop:Hide} = 1
        ?job:Repair_Type{prop:Hide} = 1
        ?job:Repair_Type:Prompt{prop:Hide} = 1
        !?LookupChargeableRepairType{prop:Hide} = 1
        ?ChargeableTab{prop:Hide} = 1

!        ?Chargeable_Group{prop:Disable} = 1
        ?OptionsEstimate{prop:Disable} = 1
    End !If job:Chargeable_Job = 'YES'

    If job:Warranty_Job = 'YES'

        ?job:Warranty_Charge_Type{prop:Hide} = 0
        ?job:Warranty_Charge_Type:prompt{prop:Hide} = 0
        ?job:Repair_Type_Warranty{prop:Hide} = 0
        ?job:Repair_Type_Warranty:Prompt{prop:Hide} = 0
        !?LookupWarrantyRepairType{prop:Hide} = 0
        ?WarrantyTab{prop:Hide} = 0
!        ?Warranty_Group{prop:Disable} = 0
    Else !If job:Warranty_Job = 'YES'
        ?job:Warranty_Charge_Type{prop:Hide} = 1
        ?job:Warranty_Charge_Type:prompt{prop:Hide} = 1
        ?job:Repair_Type_Warranty{prop:Hide} = 1
        ?job:Repair_Type_Warranty:Prompt{prop:Hide} = 1
        !?LookupWarrantyRepairType{prop:Hide} = 1
        ?WarrantyTab{prop:Hide} = 1

!        ?Warranty_Group{prop:Disable} = 1
    End !If job:Warranty_Job = 'YES'

    if job:Warranty_Job = 'YES' and job:Chargeable_Job <> 'YES'
        job:Charge_Type = ''
        job:Repair_Type = ''
    end

    if job:Chargeable_Job = 'YES' and job:Warranty_Job <> 'YES'
        job:Warranty_Charge_Type = ''
        job:Repair_Type_Warranty = ''
    end

    Access:JOBSE_ALIAS.Clearkey(jobe_ali:RefNumberKey)
    jobe_ali:RefNumber  = job:Ref_Number
    If Access:JOBSE_ALIAS.Tryfetch(jobe_ali:RefNumberKey) = Level:Benign
        !Found
        If job:Exchange_Unit_Number <> ''
            If jobe_ali:ExchangedATRRC
                ?ExchangeUnitAttached{prop:Text} = 'RRC EXCHANGE Unit Attached'
                ?ExchangeUnitAttached{prop:FontColor} = 080FFFFH
            Else !If jobe_ali:ExchangedATRRC
                ?ExchangeUnitAttached{prop:Text} = 'ARC EXCHANGE Unit Attached'
                ?ExchangeUnitAttached{prop:FontColor} = Color:White
            End !If jobe_ali:ExchangedATRRC
        Else !If job:Exchange_Unit_Number <> ''
            ?ExchangeUnitAttached{prop:Text} = 'Not Issued'
            ?ExchangeUnitAttached{prop:FontColor} = 0D0FFD0H
        End !If job:Exchange_Unit_Number <> ''

        If jobe_ali:SecondExchangeNumber <> ''
            ?SecondExchangeUnitAttached{prop:Hide} = False
        Else !If jobe_ali:SecondExchangeNumber <> ''
            ?SecondExchangeUnitAttached{prop:Hide} = True
        End !If jobe_ali:SecondExchangeNumber <> ''
    Else ! If Access:JOBSE_ALIAS.Tryfetch(jobe_ali:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE_ALIAS.Tryfetch(jobe_ali:RefNumberKey) = Level:Benign

    If job:Loan_Unit_Number <> ''
        ?LoanUnitAttached{prop:Text} = 'LOAN Unit Attached'
        ?LoanUnitAttached{prop:FontColor} = color:White
    Else
        ?LoanUnitAttached{prop:Text} = 'Not Issued'
        ?LoanUnitAttached{prop:FontColor} = 0D0FFD0H
    End !If job:Loan_Unit_Number

    exchange_loan_temp = ''
    if job:Estimate_Accepted = 'YES' and job:exchange_unit_number = 0
        exchangeFound# = false
        Save_par_ID = Access:PARTS.SaveFile()
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number  = job:Ref_Number
        set(par:Part_Number_Key,par:Part_Number_Key)
        loop until Access:PARTS.Next()
            if par:Ref_Number <> job:Ref_Number then break.
            if par:Part_Number = 'EXCH'
                exchangeFound# = true
            end
        end
        Access:PARTS.RestoreFile(Save_par_ID)

        if exchangeFound# = true
            exchange_loan_temp = 'Estimate Accepted: Exchange Req.'
        end
    end
    ! Refresh The Browses (DBH: 18-10-2005)

    ! Inserting (DBH 06/12/2006) # 8559 - Only enable the relevant part sections if the job isn't "view only"
    If glo:Preview = 'V'
        ?Warranty_Group{prop:Disable} = True
        ?Chargeable_Group{prop:Disable} = True
    Else ! If glo:Preview = 'V'
        ?Warranty_Group{prop:Disable} = False
        ?Chargeable_Group{prop:Disable} = False
    End ! If glo:Preview = 'V'
    ! End (DBH 06/12/2006) #8559



    Display()
! F
Fill_lists      Routine
   BRW39.ResetSort(1)
   BRW10.ResetSort(1)
   BRW9.ResetSort(1)
! L
LockPUPJob      Routine
    if (tmp:LockJob = 'LOCKJOB')
        local.disableJob(1)
        ?options{prop:disable} = 1
        ?browse{prop:disable} = 1
        ?allocateJob{prop:disable} = 0
        ?Fault_Codes{prop:disable} = 1
        ?Costs{prop:disable} = 1
        ?Button:ReceiptFromPUP{prop:disable} = 1
        ?BouncerButton{prop:disable} = 1
    else
        local.disableJob(0)
        ?options{prop:disable} = 0
        ?browse{prop:disable} = 0
        ?allocateJob{prop:disable} = 0
        ?Fault_Codes{prop:disable} = 0
        ?Costs{prop:disable} = 0
        ?Button:ReceiptFromPUP{prop:disable} = 0
        ?BouncerButton{prop:disable} = 0
    end !if (tmp:LockJob = 1)
Lookup_Engineer     Routine
    !Engineer
    Save_joe_ID = Access:JOBSENG.SaveFile()
    Access:JOBSENG.ClearKey(joe:UserCodeKey)
    joe:JobNumber     = job:Ref_Number
    joe:UserCode      = job:Engineer
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.PREVIOUS()
           Break
        End !If
        If joe:JobNumber     <> job:Ref_Number       |
        Or joe:UserCode      <> job:Engineer      |
        Or joe:DateAllocated > Today()       |
            Then Break.  ! End If

        tmp:SkillLevel      = joe:EngSkillLevel
        IF tmp:SkillLevel <> use:SkillLevel
          Access:users.Clearkey(use:user_code_key)
          use:user_code    = job:engineer
          If Access:users.Fetch(use:user_Code_Key) = Level:Benign
              engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
      !        tmp:SkillLevel  = use:SkillLevel
          End
          tmp:SkillLevel = use:SkillLevel
        END
        tmp:DateAllocated   = joe:DateAllocated
        Break
    End !Loop
    Access:JOBSENG.RestoreFile(Save_joe_ID)

    Access:users.Clearkey(use:user_code_key)
    use:user_code    = job:engineer
    If Access:users.Fetch(use:user_Code_Key) = Level:Benign
        engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
!        tmp:SkillLevel  = use:SkillLevel
    End
    Display()

! M
MakePartsOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_number
        ope:Job_Number      = job:Ref_number
        ope:Part_Type       = 'JOB'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = sto:Part_Number
        ope:Description     = sto:Description
        ope:Quantity        = 1
        ope:Account_Number  = job:Account_Number
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
MakeWarPartsOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_number
        ope:Job_Number      = job:Ref_number
        ope:Part_Type       = 'WAR'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = sto:Part_Number
        ope:Description     = sto:Description
        ope:Quantity        = 1
        ope:Account_Number  = job:Account_Number
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
MSNEmbeds          ROUTINE

    data
tmp:DateCodeChecked byte
    code

    IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
      !Ericsson MSN!
      Job:MSN = SUB(job:MSN,2,10)
      UPDATE()
      Display()
    END

    !End!
    Error# = 0
    If CheckLength('MSN',job:model_number,job:msn)
        job:MSN = ''
        Select(?job:msn)
        Error# = 1
    Else!If error# = 1
        Error# = 0
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = job:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            If man:ApplyMSNFormat
                If CheckFaultFormat(job:Msn,man:MSNFormat)
                    Case Missive('M.S.N. failed format validation.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Select(?job:msn)
                    Error# = 1
                End !If CheckFaultFormat(job:Msn,man:MSNFormat)
            End !If man:ApplyMSNFormat
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    End!If error# = 1

    !If it's an OBF, then don't do any validation
    If job:Warranty_job = 'YES' and job:Warranty_Charge_type = 'WARRANTY (OBF)'
        EXIT
    End !If job:Warranty_job = 'YES' and job:Warranty_Charge_type = 'WARRANTY (OBF)'

    job:Warranty_Job = ''
    job:Warranty_Charge_type = ''
    !these then have to be changed

    !Added By Neil!
    If job:Manufacturer = 'MOTOROLA' AND Error# = 0

        tmp:DateCodeChecked = true

        If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          do Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)
          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)


    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'SAMSUNG' AND Error# = 0

        tmp:DateCodeChecked = true

        If DateCodeValidation('SAMSUNG',job:MSN,job:Date_Booked)
          do Validate_Motorola


        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'PHILIPS' AND Error# = 0

        tmp:DateCodeChecked = true

        If DateCodeValidation('PHILIPS',job:MSN,job:Date_Booked)
          do Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)

    End !If job:Manufacturer = 'MOTOROLA'

    if tmp:DateCodeChecked = false And Error# = 0
         do Validate_Motorola
    end

    display()
! O
OutFaultsList     Routine
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

        If man:ForceParts
            ?ChargeableAdjustment{prop:Hide} = 0
            ?WarrantyAdjustment{prop:Hide} = 0
        Else !If man:ForceParts
            ?ChargeableAdjustment{prop:Hide} = 1
            ?WarrantyAdjustment{prop:Hide} = 1
        End !If man:ForceParts
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If AccessMANUFACT.Tryfetch(manManufacturer_Key) = LevelBenign
! P
PUPValidation      Routine
Data
locIMEINumber           String(30)
locMSN                  String(30)
locNetwork              String(30)
locInFault              String(255)
locAuditNotes           String(255)
locFaultCode            String(255)
locFaultNumber          String(255)
Code
    tmp:LockJob = ''
    locIMEINumber = job:ESN
    if (PUPValidation(locIMEINumber,locMSN,locNetwork,locInFault) = 0)
        !Lock Job
        tmp:LockJob = 'LOCKJOB'
        do LockPUPJob
    else !if (PUPValidation(locIMEINumber,locMSN,locNetwork,locInFault) = 0)
        tmp:LockJob = ''
        do LockPUPJob

        changeMade# = 0
        if (locIMEINumber <> job:ESN)
            locAuditNotes = clip(locAuditNotes) & '<13,10>OLD I.M.E.I.: ' & clip(job:ESN) & |
                                                    '<13,10>NEW I.M.E.I.: ' & clip(locIMEINumber)
            job:ESN = locIMEINumber
            changeMade# = 1
            do AfterOutFaultList
        end! if (locIMEINumber <> job:ESN)
        if (locMSN <> '' and locMSN <> job:MSN)
            locAuditNotes = clip(locAuditNotes) & '<13,10>OLD M.S.N.: ' & clip(job:MSN) & |
                                                    '<13,10>NEW M.S.N.: ' & clip(locMSN)
            changeMade# = 1
            job:MSN = locMSN
        end !if (locMSN <> '' and locMSN <> job:MSN)

        if (locNetwork <> tmp:Network)
            locAuditNotes = clip(locAuditNotes) & '<13,10>OLD NETWORK: ' & clip(tmp:Network) & |
                                                    '<13,10>NEW NETWORL: ' & clip(locNetwork)
            changeMade# = 1
            tmp:Network = locNetwork
        end !if (locNetwork <> tmp:Network)

        Access:MANFAULT.Clearkey(maf:InFaultKey)
        maf:Manufacturer    = job:Manufacturer
        maf:InFault    = 1
        if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
            ! Found
            locFaultNumber = maf:Field_Number
            case (maf:Field_Number)
            of 1
                locFaultCode = job:Fault_Code1
            of 2
                locFaultCode = job:Fault_Code2
            of 3
                locFaultCode = job:Fault_Code3
            of 4
                locFaultCode = job:Fault_Code4
            of 5
                locFaultCode = job:Fault_Code5
            of 6
                locFaultCode = job:Fault_Code6
            of 7
                locFaultCode = job:Fault_Code7
            of 8
                locFaultCode = job:Fault_Code8
            of 9
                locFaultCode = job:Fault_Code9
            of 10
                locFaultCode = job:Fault_Code10
            of 11
                locFaultCode = job:Fault_Code11
            of 12
                locFaultCode = job:Fault_Code12
            of 13
                locFaultCode = wob:FaultCode13
            of 14
                locFaultCode = wob:FaultCode14
            of 15
                locFaultCode = wob:FaultCode15
            of 16
                locFaultCode = wob:FaultCode16
            of 17
                locFaultCode = wob:FaultCode17
            of 18
                locFaultCode = wob:FaultCode18
            of 19
                locFaultCode = wob:FaultCode19
            of 20
                locFaultCode = wob:FaultCode20
            end ! case (maf:Field_Number)
        else ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)

        if (locInFault <> locFaultCode)
            changeMade# = 1
            locAuditNotes = clip(locAuditNotes) & '<13,10>OLD IN FAULT: ' & clip(locFaultCode) & |
                                                    '<13,10>NEW IN FAULT: ' & clip(locInFault)
            case (locFaultNumber)
            of 1
                job:Fault_Code1 = locInFault
            of 2
                job:Fault_Code2 = locInFault
            of 3
                job:Fault_Code3 = locInFault
            of 4
                job:Fault_Code4 = locInFault
            of 5
                job:Fault_Code5 = locInFault
            of 6
                job:Fault_Code6 = locInFault
            of 7
                job:Fault_Code7 = locInFault
            of 8
                job:Fault_Code8 = locInFault
            of 9
                job:Fault_Code9 = locInFault
            of 10
                job:Fault_Code10 = locInFault
            of 11
                job:Fault_Code11 = locInFault
            of 12
                job:Fault_Code12 = locInFault
            of 13
                wob:FaultCode13 = locInFault
            of 14
                wob:FaultCode14 = locInFault
            of 15
                wob:FaultCode15 = locInFault
            of 16
                wob:FaultCode16 = locInFault
            of 17
                wob:FaultCode17 = locInFault
            of 18
                wob:FaultCode18 = locInFault
            of 19
                wob:FaultCode19 = locInFault
            of 20
                wob:FaultCode20 = locInFault
            end ! case (maf:Field_Number)
        end !if (locInFault <> locFaultCode)
    end !if (PUPValidation(locIMEINumber,locMSN,locNetwork,locInFault) = 0)

    if (changeMade# = 1)
        locAuditNotes = 'DETAILS CHANGED:' & clip(locAuditNotes)
    end !if (changeMade# = 1)
    if (AddToAudit(job:Ref_Number,'JOB','PUP VALIDATION',locAuditNotes))
    end !if (AddToAudit(job:Ref_Number,'JOB','DETAILS CHANGED FROM PUP',sub(locAuditNotes,3,255)))
! Q
QA_Group        Routine
    !Does this really matter?
    !Are Vodacom not going to use QA? -  (DBH: 26-11-2003)
!    hide(?qa_group)
!    If def:qa_before_complete = 'YES'
!        If job:date_completed <> ''
!            Unhide(?qa_group)
!            Disable(?qa_group)
!        Else!If job:date_completed = 'YES'
!            Unhide(?qa_group)
!            Disable(?qa_group)
!        End!If job:date_completed = 'YES'
!    End
!    If def:qa_required = 'YES'
!        If job:date_completed <> ''
!            Unhide(?qa_group)
!        Else
!            Hide(?qa_group)
!        End
!    End
!    If def:rapidqa = 'YES'
!        Disable(?qa_group)
!    End!If def:rapidqa = 'YES'
! R
ReceiptFromPUP      Routine
Data
local:Site              String(3)
local:CurrentLocation   String(30)
local:CurrentStatus     String(3)
local:DefaultStatus     String(3)
local:AuditNotes        String(255)
Code

    If glo:WebJob
        ! Inserting (DBH 13/11/2006) # 8485 - Check that the logged in user matches the RRC of the job
        Access:USERS.ClearKey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            !Found
            If use:Location <> tmp:BookingSiteLocation
                Case Missive('You cannot receive a job from a different RRC.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End ! If use:Location <> tmp:BookingSiteLocation
        Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            !Error
        End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        ! End (DBH 13/11/2006) #8485
        local:Site = 'RRC'
        local:CurrentLocation = GETINI('RRC','RRCLocation',,Clip(Path()) & '\SB2KDEF.INI')
    Else ! If glo:WebJob
        local:Site = 'ARC'
        local:CurrentLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
    End ! If glo:WebJob
    local:DefaultStatus = GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI')

    If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
        If SecurityCheck('FORCE RECEIPT FROM PUP')
            Case Missive('Cannot receive this job!'&|
              '|The current location is NOT "In Transit From PUP".','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        Else ! If SecurityCheck('FORCE RECEIPT FROM PUP')
            Case Missive('Warning! The current location of this job is NOT "In Transit From PUP".'&|
              'Are you sure you still want to receive this job from the PUP?','ServiceBase 3g',|
                           'mexclam.jpg','\Cancel|/Receive')
                Of 2 ! Receive Button
                Of 1 ! Cancel Button
                    Exit
            End ! Case Missive
        End ! If SecurityCheck('FORCE RECEIPT FROM PUP')
! End (DBH 07/08/2006) #8021
    Else ! If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
        Case Missive('Are you sure you want to receive the PUP job into the ' & Clip(local:Site) & '?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
            Of 1 ! No Button
                Exit
        End ! Case Missive
    End ! If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')

    do PUPValidation

    local:AuditNotes = ''

    Rtn# = AccessoryCheck('JOB')

    If Rtn# = 1 Or Rtn# = 2
        local:AuditNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'

        Save_jac_ID = Access:JOBACC.SaveFile()
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        Loop ! Begin JOBACC Loop
            If Access:JOBACC.Next()
                Break
            End ! If !Access
            If jac:Ref_Number <> job:Ref_Number
                Break
            End ! If
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>' & Clip(jac:Accessory)
        End ! End JOBACC Loop
        Access:JOBACC.RestoreFile(Save_jac_ID)
        local:AuditNotes = Clip(local:AuditNotes) & '<13,10,13,10>ACCESSORIES RECEIVED:-'
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>' & Clip(glo:Pointer)
        End ! Loop x# = 1 To Records(glo:Queue)
    End ! If Rtn# = 1 Or Rtn# = 2

    If AddToAudit(job:Ref_Number,'JOB','UNIT RECEIVED AT ' & CLip(local:Site) & ' FROM PUP',Clip(local:AuditNotes))
        LocationChange(GETINI('RRC',local:Site & 'Location',,Clip(Path()) & '\SB2KDEF.INI'))
        GetStatus(local:DefaultStatus,0,'JOB')

        Case Missive('This job will now be saved.','ServiceBase 3g',|
                       'midea.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        ! Changing (DBH 03/07/2006) # 7149 - Do not do the OK validation
        ! Post(Event:Accepted,?OK)
        ! ! to (DBH 03/07/2006) # 7149
        job:Workshop = 'YES'
        Access:JOBS.Update()
        do SetJobse
        Access:JOBSE.Update()
        ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
        UpdateDateTimeStamp(job:Ref_Number)
        ! End (DBH 16/09/2008) #10253
        Post(Event:Accepted,?Close)
        ! End (DBH 03/07/2006) #7149
    End ! If AddToAuditEntry(job:Ref_Number,'JOB','UNIT RECEIVED AT ' & CLip(local:Site) & ' FROM PUP','')



ResendXML       Routine
Data
local:Seq        Long()
local:ToSendFolder  CString(255)
local:SentFolder    CString(255)
local:ReceivedFolder CString(255)
local:ProcessedFolder CString(255)
local:XMLFileName   CString(255)
Code
    Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
    trd:Company_Name = job:Third_Party_Site
    If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Found
        If trd:LHubAccount = 0
            Exit
        End ! If trd:LHubAccount = 0
    Else ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Error
    End ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

    Access:TRDBATCH.ClearKey(trb:JobStatusKey)
    trb:Status = 'OUT'
    trb:Ref_Number = job:Ref_Number
    If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
        !Found
    Else ! If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
        !Error
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Unable to find this job on any "Out" batches.','ServiceBase 3g',|
                       'mstop.jpg','/&OK')
            Of 1 ! &OK Button
        End!Case Message
    End ! If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign


    local:ToSendFolder = GETINI('NMSFTP','XMLFolder',,Clip(Path()) & '\SB2KDEF.INI')
    If Sub(Clip(local:ToSendFolder),-1,1) <> '\'
        local:ToSendFolder = Clip(local:ToSendFolder) & '\'
    End ! If Sub(Clip(local:ToSendFolder),-1,1) <> '\'

    local:SentFolder = Clip(local:ToSendFolder) & 'Sent'

    local:ReceivedFolder = Clip(local:ToSendFolder) & 'Received'

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Processed'

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Bad'

    local:ToSendFolder = Clip(local:ToSendFolder) & 'ToSend'

    local:XMLFileName =  'RR_' & trb:Batch_Number & Format(Today(),@d12) & Clock() & '.xml'


    If objXmlExport.FOpen(local:ToSendFolder & '\' & local:XMLFileName,1) = Level:Benign
        objXmlExport.OpenTag('ShipmentOrderRequest','Version="4.0"')
        objXmlExport.OpenTag('Authentication')
            objXmlExport.WriteTag('SiteId',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('UserId',GETINI('NMSFTP','UserID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('ToolType','CT')
            objXmlExport.WriteTag('AccessKey',GETINI('NMSFTP','AccessKey',,Clip(Path()) & '\SB2KDEF.INI'))
        objXmlExport.CloseTag('Authentication')
        objXmlExport.WriteTag('ClientReferenceNumber',trb:Batch_Number & '-' & Today() & Clock())
        objXmlExport.WriteTag('ShipmentType','001')
        objXmlExport.WriteTag('SourceSiteId',trd:ASCID)
        objXmlExport.OpenTag('Booking')
            objXmlExport.WriteTag('SequenceNo',1)
            objXmlExport.WriteTag('BookingType','001')
            objXmlExport.WriteTag('ItemType','PHONE')
            objXmlExport.WriteTag('ClientBookingNumber',job:Ref_Number)
            objXmlExport.WriteTag('FaultSymptomCode',Clip(job:Fault_Code1))
            objXmlExport.WriteTag('ExistenceOfSymptom',Clip(wob:FaultCode15))
            objXmlExport.WriteTag('OrderType','REPAIR')
            objXmlExport.WriteTag('ProductCode',Clip(job:ProductCode))
            objXmlExport.WriteTag('OriginalSerialNumber',Clip(job:ESN))
            objXmlExport.WriteTag('ClientBookingTimestamp',Format(Today(),@d05b) & ' ' & Format(Clock(),@t04b))
            If job:Warranty_Job = 'YES'
                objXmlExport.WriteTag('WarrantyOption','001')
            Else ! If job:Warranty_Job = 'YES'
                objXmlExport.WriteTag('WarrantyOption','002')
            End ! If job:Warranty_Job = 'YES'
            objXmlExport.WriteTag('WarrantyDatabaseDate',Format(job:DOP,@d05b) & ' ' & Format(Clock(),@t04b))
            objXmlExport.WriteTag('OwnershipCode','001')
            Found# = 0
            If job:Warranty_Job = 'YES'
                If Instring('SOFTWARE',Upper(job:Repair_Type_Warranty),1,1)
                    objXmlExport.WriteTag('TransactionType','002')
                    Found# = 1
                End ! If Instring('SOFTWARE',Upper(job:Warranty_Repair_Type),1,1)
            Else ! If job:Warranty_Job = 'YES'
                If Instring('SOFTWARE',Upper(job:Repair_Type),1,1)
                    objXmlExport.WriteTag('TransactionType','002')
                    Found# = 1
                End ! If Instring('SOFTWARE',Upper(job:Repair_Type),1,1)
            End ! If job:Warranty_Job = 'YES'
            If Found# = 0
                objXmlExport.WriteTag('TransactionType','001')
            End ! If Found# = 0
        objXmlExport.CloseTag('Booking')
        objXmlExport.CloseTag('ShipmentOrderRequest')
        objXMLExport.FClose()
    End ! If objXmlExport.FOpen(Filename.xml,1) = Level:Benign

    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Missive('Job Resent.','ServiceBase 3g',|
                   'midea.jpg','/&OK')
        Of 1 ! &OK Button
    End!Case Message

RRCHubRepairDisableScreen           Routine
    ?Sheet1{prop:Disable} = 1
    ?Sheet2{prop:Disable} = 1
    ?Sheet3{prop:Disable} = 1
    ?Cancel{prop:Disable} = 1
    If SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
        ?tmp:OBFvalidated{prop:Disable} = 1
    Else !SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
        ?tmp:HubRepair{prop:Disable} = True
        ?tmp:OBFValidated{prop:Disable} = True
    End !SecurityCheck('JOBS - HUB REPAIR') = Level:Benign

    ?CompleteButton{prop:Disable} = 1
    Disable(?BrowseAmendAddresses)
    Disable(?OptionsEstimate)
    Disable(?OptionsAllocateExchange)
Refresh_Types   ROUTINE

    !message('In refresh types Charge/War ='&job:Warranty_job &'/'& job:Warranty_job)

    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:MainFault    = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found

    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    !Added By Paul 01/11/2010 - Log No 11762
    !If the repair type has been changed for one that does not allow parts
    !AND
    !there are parts already on the job - then do no allow the change
    TempError# = 0

    If Clip(tmp:Charge_T) <> '' then
        If tmp:Charge_T <> job:Repair_Type then
            !chargetype wants to change - check if the new type is a restricted parts type
            access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
            rtd:Manufacturer    = job:Manufacturer
            rtd:Repair_Type     = tmp:Charge_T
            If Access:ReptyDef.fetch(rtd:ManRepairTypeKey) = level:benign then
                if rtd:NoParts = 'Y' then
                    Access:Parts.ClearKey(par:Part_Number_Key)
                    par:Ref_Number = job:Ref_Number
                    set(par:Part_Number_Key,par:Part_Number_Key)
                    loop
                        If Access:Parts.Next() then
                            Break
                        End
                        if par:Ref_Number <> job:Ref_Number then
                            break
                        End
                        !if we are still here - then there must be parts on the job
                        TempError# = 1
                        If PaulsMessageGlobal = '' then
                            PaulsMessageGlobal = 'Spares cannot be added on the current Repair Type. Please select a different Repair Type or remove the parts and then try again.'
                        End ! Case Missive
                        break
                    End
                End
            End
        End
    End

    If TempError# = 0 then
        !dont do checks twice if split jobs
        If clip(tmp:Warranty_T) <> '' then
            If tmp:Warranty_T <> job:Repair_Type_Warranty then
                !warranty repair type wants to change - check if the new type is restricted parts type
                access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
                rtd:Manufacturer    = job:Manufacturer
                rtd:Repair_Type     = tmp:Warranty_T
                If Access:RepTyDef.Fetch(rtd:ManRepairTypeKey) = level:benign then
                    If rtd:NoParts = 'Y' then
                        Access:WarParts.ClearKey(wpr:Part_Number_Key)
                        wpr:Ref_Number = job:Ref_Number
                        set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        loop
                            If Access:WarParts.Next() then
                                break
                            End
                            if wpr:Ref_Number <> job:Ref_Number then
                                break
                            End
                            !if we are still here - then there must be parts on the job
                            TempError# = 1
                            If PaulsMessageGlobal = '' then
                                PaulsMessageGlobal = 'Spares cannot be added on the current Repair Type. Please select a different Repair Type or remove the parts and then try again.'
                            End ! Case Missive
                            break
                        End
                    End
                End
            End
        End
    End



    If TempError# = 0 then
        !Message('TempError is zero')

        If tmp:Warranty_T <> '' And tmp:Charge_T <> ''
            If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
                job:Repair_Type = tmp:Charge_T
                job:Repair_Type_Warranty = tmp:Warranty_T
                Do WriteFaultCode
                Post(Event:Accepted,?job:Repair_Type)
                Post(Event:Accepted,?job:Repair_Type_Warranty)
            End !If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
            If job:Chargeable_Job = 'YES' and job:Warranty_Job <> 'YES'
                job:Repair_Type = tmp:Charge_T
                ! Inserting (DBH 13/06/2006) #6733 - Set Out Fault based on Chargeable fault codes
                Do WriteFaultCode
                ! End (DBH 13/06/2006) #6733
                Post(Event:Accepted,?job:Repair_Type)
            End !If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
            If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                job:Repair_Type_Warranty = tmp:Warranty_T
                Do WriteFaultCode
                Post(Event:Accepted,?job:Repair_Type_Warranty)
            End !If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
        End !tmp:Warranty_T <> '' And tmp:Charge <> ''

        If tmp:Warranty_T <> '' And tmp:Charge_T = ''
            If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'

                ! Should this be here? Lets find out (DBH: 29-09-2005)
                Do WriteFaultCode
                ! End   - Should this be here? Lets find out (DBH: 29-09-2005)
                job:Repair_Type = ''
                Post(Event:Accepted,?job:Repair_Type)
            End !If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
            If job:Chargeable_Job = 'YES' and job:Warranty_Job <> 'YES'
                job:Repair_Type = ''
                ! Inserting (DBH 13/06/2006) #6733 - Set Out Fault based on Chargeable fault codes
                Do WriteFaultCode
                ! End (DBH 13/06/2006) #6733
                Post(Event:Accepted,?job:Repair_Type)
            End !If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
            If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                job:Repair_Type_Warranty = tmp:Warranty_T
                Do WriteFaultCode
                Post(Event:Accepted,?job:Repair_Type_Warranty)
            End !If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
        End !tmp:Warranty_T <> '' And tmp:Charge_T = ''

        If tmp:Warranty_T = '' And tmp:Charge_T <> ''
            !message('This seems to be the change chargeable = yes and warranty = yes, to be over written because tmp:Warranty_T And tmp:Charge_T = '&clip(tmp:Warranty_T) & clip(tmp:Charge_T))
            If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
                job:Warranty_Job = 'NO'
                job:Repair_Type = tmp:Charge_T
                ! Inserting (DBH 13/06/2006) #6733 - Set Out Fault based on Chargeable fault codes
                Do WriteFaultCode
                ! End (DBH 13/06/2006) #6733
                job:Warranty_Job = 'YES'    !set the warranty typte back to yes 012717- BUG on Splitting - JC 06/08/2012
                Post(Event:Accepted,?job:Repair_Type)
                !TransferParts('C')         !no transfer of parts this is both warranty and chargeable 012717- BUG on Splitting - JC 06/08/2012
                brw9.ResetSort(1)
                brw10.ResetSort(1)
            End !If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
            If job:Chargeable_Job = 'YES' and job:Warranty_Job <> 'YES'
                job:Repair_Type = tmp:Charge_T
                ! Inserting (DBH 13/06/2006) #6733 - Set Out Fault based on Chargeable fault codes
                Do WriteFaultCode
                ! End (DBH 13/06/2006) #6733
                Post(Event:Accepted,?job:Repair_Type)
            End !If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
            If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                job:Warranty_Job = 'NO'
                job:Chargeable_Job = 'YES'
                job:Repair_Type = tmp:Charge_T
                ! Inserting (DBH 13/06/2006) #6733 - Set Out Fault based on Chargeable fault codes
                Do WriteFaultCode
                ! End (DBH 13/06/2006) #6733
                Post(Event:Accepted,?job:Repair_Type)
                TransferParts('C')
                brw9.ResetSort(1)
                brw10.ResetSort(1)

            End !If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
        End !tmp:Warranty_T = '' And tmp:Charge_T <> ''

        Display()
        Do Enable_Disable
    End


    !IF cahrgeable, leave it, unless no chargeable repair type
    !Ditto other

    !Modified yet again!


!    If tmp:Charge_T <> ''
!        !Chargeable Returned
!        If job:Chargeable_Job = 'YES'
!            !Chargeable already, everythings fine
!            job:Repair_Type = tmp:Charge_T
!            Post(Event:Accepted,?job:Repair_Type)
!        Else !If job:Chargeable_Job = 'YES'
!            !Not already chargeable.
!            If tmp:Warranty_T  '' and job:Warranty_Job <> 'YES'
!                !No warranty return, but it's a warranty job
!                !Turn off warranty.
!            End !If tmp:Warranty_T  '' and job:Warranty_Job <> 'YES'
!            job:Repair_Type = tmp:Charge_T
!            Post(Event:Accepted,?job:Repair_Type)
!        End !If job:Chargeable_Job = 'YES'
!    End !If tmp:Charge_t <> ''
!
!
!    IF tmp:charge_t <> ''
!      IF job:Chargeable_Job = 'NO'
!        IF tmp:warranty_t = ''
!          job:Chargeable_Job = 'YES'
!          POST(Event:Accepted,?job:Chargeable_Job)
!          job:Repair_Type = tmp:charge_t
!          POST(Event:Accepted,?job:Repair_Type)
!        END
!      ELSE
!        job:Repair_Type = tmp:charge_t
!        POST(Event:Accepted,?job:Repair_Type)
!      END
!    END
!
!
!    IF tmp:Warranty_t <> ''
!      IF job:Warranty_Job = 'NO'
!        IF job:Chargeable_Job = 'YES'
!          !Do nothing!
!        ELSE
!          IF tmp:charge_t = ''
!            job:Warranty_Job = 'YES'
!            POST(Event:Accepted,?job:Warranty_Job)
!            job:Repair_Type_Warranty = tmp:warranty_t
!            POST(Event:Accepted,?job:Repair_Type_Warranty)
!          END
!        END
!      ELSE
!        job:Repair_Type_Warranty = tmp:warranty_t
!        POST(Event:Accepted,?job:Repair_Type_Warranty)
!      END
!    END
! S
SetJOBSE        Routine
    jobe:Network            = tmp:Network
    jobe:HubRepair          = tmp:HubRepair
    jobe:HubRepairDate      = tmp:HubRepairDate
    jobe:HubRepairTime      = tmp:HubRepairTime
! Changing (DBH 25/04/2007) # 8938 - Save the "overwrite repair type" tick box
!!    jobe:COverwriteRepairType   = tmp:COverwriteRepairType
!!    jobe:WOverwriteRepairType   = tmp:WOverwriteRepairType
! to (DBH 25/04/2007) # 8938
    jobe:COverwriteRepairType   = tmp:COverwriteRepairType
    jobe:WOverwriteRepairType   = tmp:WOverwriteRepairType
! End (DBH 25/04/2007) #8938
!    jobe:ClaimValue         = tmp:Claim
!    jobe:HandlingFee        = tmp:Handling
!    jobe:ExchangeRate       = tmp:Exchange
!    jobe:IgnoreClaimCosts   = tmp:IgnoreClaimCosts
!    jobe:RRCCLabourCost     = tmp:RRCCLabourCost
!    jobe:RRCCPartsCost      = tmp:RRCCPartsCost
!    jobe:RRCWPartsCost      = tmp:RRCWPartsCost
!    jobe:RRCWLabourCost     = tmp:RRCWLabourCost
!    jobe:RRCELabourCost     = tmp:RRCELabourCost
!    jobe:RRCEPartsCost      = tmp:RRCEPartsCost
    jobe:OBFvalidated       = tmp:OBFvalidated
    jobe:OBFvalidateDate    = tmp:OBFvalidateDate
    jobe:OBFvalidateTime    = tmp:OBFvalidateTime
    jobe:Despatched         = tmp:Despatched
    jobe:DespatchType       = tmp:DespatchType
    jobe:ExchangeRate       = tmp:ExchangeRate
    jobe:POPType            = tmp:POPType
    jobe:Engineer48HourOption    = tmp:Engineer48HourOption
    jobe:Booking48HourOption     = tmp:Booking48HourOption
    ! Insert --- Trade Fault Codes not used. Will use to signify a job lock down (DBH: 30/04/2009) #10782
    jobe:TraFaultCode1           = tmp:LockJob
    ! end --- (DBH: 30/04/2009) #10782

GetJOBSE        Routine
    tmp:Network         = jobe:Network
    tmp:HubRepair       = jobe:HubRepair
    tmp:HubRepairDate   = jobe:HubRepairDate
    tmp:HubRepairTime   = jobe:HubRepairTime
    tmp:OBFvalidated    = jobe:OBFvalidated
    tmp:OBFvalidateDate = jobe:OBFvalidateDate
    tmp:OBFvalidateTime = jobe:OBFvalidateTime
! Changing (DBH 25/04/2007) # 8938 - Save the "overwrite repair type" tick box
!!    tmp:COverwriteRepairType    = jobe:COverwriteRepairType
!!    tmp:WOverwriteRepairType    = jobe:WOverwriteRepairType
! to (DBH 25/04/2007) # 8938
    tmp:COverwriteRepairType    = jobe:COverwriteRepairType
    tmp:WOverwriteRepairType    = jobe:WOverwriteRepairType
! End (DBH 25/04/2007) #8938
!    tmp:Claim           = jobe:ClaimValue
!    tmp:Handling        = jobe:HandlingFee
!    tmp:Exchange        = jobe:ExchangeRate
!    tmp:IgnoreClaimCosts    = jobe:IgnoreClaimCosts
!    tmp:RRCCLabourCost  = jobe:RRCCLabourCost
!    tmp:RRCCPartsCost   = jobe:RRCCPartsCost
!    tmp:RRCWPartsCost   = jobe:RRCWPartsCost
!    tmp:RRCWLabourCost  = jobe:RRCWLabourCost
!    tmp:RRCELabourCost  = jobe:RRCELabourCost
!    tmp:RRCEPartsCost   = jobe:RRCEPartsCost
    tmp:Despatched      = jobe:Despatched
    tmp:DespatchType    = jobe:DespatchType
    tmp:ExchangeRate    = jobe:ExchangeRate
    tmp:POPType         = jobe:POPType
    tmp:Booking48HourOption = jobe:Booking48HourOption
    tmp:Engineer48HourOption    = jobe:Engineer48HourOption
    ! Insert --- Trade Fault Codes not used. Will use to signify a job lock down (DBH: 30/04/2009) #10782
    tmp:LockJob         = jobe:TraFaultCode1
    ! end --- (DBH: 30/04/2009) #10782
ShowCompletedText       Routine
    If job:Date_Completed <> ''
        ?Completed{prop:hide} = 0
        ?Completed{prop:Text} = 'JOB COMPLETED'
        ?Completed{prop:FontSize} = 18
    Else !If job:Date_Completed <> ''
        !Estimate Details
        If job:Estimate = 'YES' and job:Chargeable_Job = 'YES'
            ?Completed{prop:Text} = 'Estimate If Over: ' & Clip(left(format(job:Estimate_If_Over,@N10.2))) & '  Status: '
            ?Completed{prop:Hide} = 0
            ?Completed{prop:FontSize} = 10 !10
            If job:Estimate_Accepted = 'YES'
                ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Accepted'
            Else !If job:EstimateAccepted = 'YES'
                If job:Estimate_Rejected = 'YES'
                    ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Rejected'
                Else !If job:Estimate_Rejected = 'YES'
                    If job:Estimate_Ready = 'YES'
                        if Sub(job:Current_Status,1,3) = '520'
                            ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Sent'
                        else
                            ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Ready'
                        end
                    Else !If job:Estimate_Ready = 'YES'
                        ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' In Progress'
                    End !If job:Estimate_Ready = 'YES'
                End !If job:Estimate_Rejected = 'YES'
            End !If job:EstimateAccepted = 'YES'
        Else
            ?Completed{prop:Hide} = 1
        End !If job:Estimate = 'YES'
    End !If job:Date_Completed <> ''
! V
ValidateDateCode Routine

    !Still validation even if POP entered.
    !In case user changes mind - L948 (DBH: 16-09-2003)
    !if job:POP <> '' then exit.

    !Warranty Check!
    IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
      !Ericsson MSN!
      Job:MSN = SUB(job:MSN,2,10)
      UPDATE()
      Display()
    END

    Error# = 0
    If CheckLength('MSN',job:model_number,job:msn)
        Select(?job:msn)
        Error# = 1
    Else!If error# = 1
        Error# = 0
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = job:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            If man:ApplyMSNFormat
                If CheckFaultFormat(job:Msn,man:MSNFormat)
                    Case Missive('M.S.N. Failed Format Validation.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Select(?job:msn)
                    Error# = 1
                End !If CheckFaultFormat(job:Msn,man:MSNFormat)
            End !If man:ApplyMSNFormat
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign


    End!If error# = 1

    If job:Manufacturer = 'MOTOROLA' AND Error# = 0

        If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          DO Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        !IF job:pop <> ''
          !UNHIDE(?Validate_POP)
        !ELSE
          !HIDE(?Validate_POP)
        !END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'SAMSUNG' AND Error# = 0

        If DateCodeValidation('SAMSUNG',job:MSN,job:Date_Booked)
          DO Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        !IF job:pop <> ''
        !  UNHIDE(?Validate_POP)
        !ELSE
        !  HIDE(?Validate_POP)
        !END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'PHILIPS' AND Error# = 0

        If DateCodeValidation('PHILIPS',job:MSN,job:Date_Booked)
          DO Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        !IF job:pop <> ''
        !  UNHIDE(?Validate_POP)
        !ELSE
        !  HIDE(?Validate_POP)
        !END
    End !If job:Manufacturer = 'MOTOROLA'

    !----------------------------------------------------------------------!
    If job:Manufacturer = 'ALCATEL' AND job:POP = ''
        IF job:Fault_Code3 <> ''
            If DateCodeValidation('ALCATEL',job:Fault_Code3,job:Date_Booked)
                DO Validate_Motorola

            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'ERICSSON' AND job:POP = ''
        IF job:fault_code7 <> ''
            If DateCodeValidation('ERICSSON',CLIP(CLIP(job:Fault_Code7)&CLIP(job:Fault_Code8)),job:Date_Booked)
                DO Validate_Motorola

            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'BOSCH' AND job:POP = ''
        IF job:fault_code7 <> ''
            If DateCodeValidation('BOSCH',job:Fault_Code7,job:Date_Booked)
                DO Validate_Motorola

            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        END
    End !If job:Manufacturer = 'MOTOROLA'


    If job:Manufacturer = 'SIEMENS' AND job:POP = ''
        IF job:Fault_Code3 <> ''
            If DateCodeValidation('SIEMENS',job:Fault_Code3,job:Date_Booked)
                DO Validate_Motorola
            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        End
    End !If job:Manufacturer = 'MOTOROLA'

    !If no DOP called the POP screen - L948 (DBH: 16-09-2003)
    If job:dop = ''
        Do Validate_Motorola
    End !If job:dop = ''
Validate_Motorola   ROUTINE

    IF job:POP = ''
        Glo:Select1 = ''
    ELSE
        Glo:Select1 = job:pop
    END ! IF
    IF job:dop = ''
        Glo:Select2 = ''
    ELSE
        Glo:Select2 = job:dop
    END ! IF
    ! Glo:Select2 = ''
    !glo:Select3 = 'NONE'
    ! #11912 Set POP Type (Bryan: 10/02/2011)
    glo:Select3 = tmp:POPType


    ! Inserting (DBH 02/10/2006) # 8275 - Only validate if the OK button is pressed
    If POP_Validate2()
    ! End (DBH 02/10/2006) #8275

        tmp:POPType = glo:Select3

        IF Glo:Select1 = ''
        ! Nothing done!
        ELSE
            ! If current status is DOP Query, then reset the status
            If Sub(job:Current_Status, 1, 3) = '130'
                If job:Engineer = ''
                    GetStatus(305, 0, 'JOB') ! Awaiting Allocation
                Else ! If job:Engineer = ''
                    GetStatus(Sub(job:PreviousStatus, 1, 3), 0, 'JOB')
                End ! If job:Engineer = ''
            End ! If Sub(job:Current_Status,1,3) = '130'
            job:POP = Glo:Select1
            IF Glo:Select1 = 'F'
                job:Warranty_Job = 'YES'
                Post(Event:Accepted, ?job:Warranty_Job)
                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
            ! Post(Event:Accepted,?job:Warranty_Charge_Type)

     ! No need to untick Chargeable - L948 (DBH: 16-09-2003)
     !       job:Chargeable_Job = 'NO'
     !       Post(Event:Accepted,?job:Chargeable_Job)
                !       job:Charge_Type = ''
                Job:DOP = Glo:Select2
                ! Post(Event:Accepted,?Job:DOP)
                UPDATE()
                DISPLAY()
            END ! IF

            IF Glo:Select1 = 'S'
                job:Warranty_Job = 'YES'
                Post(Event:Accepted, ?job:Warranty_Job)
                job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
            ! Post(Event:Accepted,?job:Warranty_Charge_Type)
     ! No need to untick Chargeable - L948 (DBH: 16-09-2003)
     !       job:Chargeable_Job = 'NO'
     !       Post(Event:Accepted,?job:Chargeable_Job)
                !       job:Charge_Type = ''
                Job:DOP = Glo:Select2
                ! Post(Event:Accepted,?Job:DOP)
                UPDATE()
                DISPLAY()

            END ! IF
            IF Glo:Select1 = 'O'
                job:Warranty_Job = 'YES'
                Post(Event:Accepted, ?job:Warranty_Job)
                job:Warranty_Charge_Type = 'WARRANTY (OBF)'
            ! Post(Event:Accepted,?job:Warranty_Charge_Type)
     ! No need to untick chargeable - L948 (DBH: 16-09-2003)
     !       job:Chargeable_Job = 'NO'
     !       Post(Event:Accepted,?job:Chargeable_Job)
                !       job:Charge_Type = ''
                Job:DOP = Glo:Select2
                ! Post(Event:Accepted,?Job:DOP)
                UPDATE()
                DISPLAY()
            END ! IF

            IF Glo:Select1 = 'C'
                job:Warranty_Job   = 'NO'
                job:Chargeable_Job = 'YES'
                Post(Event:Accepted, ?job:Chargeable_Job)
                Post(Event:Accepted, ?job:Warranty_Job)
                job:Warranty_Charge_Type = ''
                ! Post(Event:Accepted,?job:Warranty_Charge_Type)
                job:Charge_Type = 'NON-WARRANTY'
                ! Post(Event:Accepted,?job:Charge_Type)
                ! Inserting (DBH 03/10/2006) # 8275 - Save the DOP incase it has changed
                job:DOP = glo:Select2
                ! End (DBH 03/10/2006) #8275
                UPDATE()
                DISPLAY()
            END ! IF
        END ! IF
    End ! If POP_Validate2()

    Glo:Select1 = ''
    Glo:Select2 = ''
! W
WarrantyExchangeUnitButton      Routine
Data
local:ExchangeAttached      Byte(0)
local:SecondExchangeAttached    Byte(0)
local:AllocateUnit          Byte(0)
local:AllocateSecondUnit    Byte(0)
local:SaveExchangeUnitNumber    Long
Code

    !Has the booking option been set?
    !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
    !If tmp:Booking48HourOption <> 0    !TB13298 - remove reliance on Booking Option - always error on Engineer Option
        If tmp:Engineer48HourOption = 0
            Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If tmp:Engineer48HourOption = 0
    !End !tmp:Booking48HourOption <> 0

    !check for existing Exchange unit
    If job:Engineer = ''
        Case Missive('You must allocate an engineer to this job.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else !job:Engineer = ''
        If Local.CorrectEngineer() = False
            Exit
        End !If Local.CorrectEngineer() = False

        !Check to see if ARC is adding a second exchange -  (DBH: 15-12-2003)
        Save_wpr_ID = Access:WARPARTS.SaveFile()
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number  |
            Or wpr:Part_Number <> 'EXCH'|
                Then Break.  ! End If
            !There is an Exchange Part attached -  (DBH: 15-12-2003)
            local:ExchangeAttached = True
            If ~glo:WebJob
                If wpr:SecondExchangeUnit = True
                    local:SecondExchangeAttached = True
                End !If wpr:SecondExchangeUnit = True
            End !If glo:WebJob
        End !Loop
        Access:WARPARTS.RestoreFile(Save_wpr_ID)

        If local:ExchangeAttached
            If glo:WebJob
                Case Missive('This job already has an exchange unit attached.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            Else !If glo:WebJob
                If local:SecondExchangeAttached = False
                    Case Missive('This job already has ONE exchange unit attached. Do you wish to add a SECOND unit?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            local:AllocateUnit = True
                            local:AllocateSecondUnit = True
                        Of 1 ! No Button
                    End ! Case Missive
                Else !If local:SecondExchangeUnit = False
                    Case Missive('This job already has an exchange unit attached.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                End !If local:SecondExchangeUnit = False
            End !If glo:WebJob
        Else !If local:ExchangeAttached
            Case Missive('Are you sure you want to allocate and exchange unit?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    local:AllocateUnit = True
                Of 1 ! No Button
            End ! Case Missive
        End !If local:ExchangeAttached

        If local:AllocateUnit
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location    = use:Location
                sto:Part_Number = 'EXCH'
                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Found
                    If loc:UseRapidStock
                        Local.AllocateExchangePart('WAR',0,local:AllocateSecondUnit)
                    Else !If loc:UseRapidStock
                        !Call the menu option - 3973 (DBH: 01-03-2004)
                        Post(Event:Accepted,?OptionsAllocateExchange)

                    End !If loc:UseRapidStock
                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Error
                    Case Missive('You must have a part setup in Stock Control with a Part Number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If local:AllocateUnit
    End! If job:Engineer = ''
WriteFaultCode      Routine
Data
local:FaultCode     String(255)
Code
    ! Inserting (DBH 13/06/2006) #6733 - Work out if to use Warranty or Chargeable Fault Code
    local:FaultCode = ''
    If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
        local:FaultCode = tmp:FaultCodeW_T
    End ! If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
    If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
        local:FaultCode = tmp:FaultCode_T
    End ! If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
    If job:Chargeable_Job = 'YES' ANd job:Warranty_Job = 'YES'
        If tmp:FaultCodeW_T <> ''
            local:FaultCode = tmp:FaultCodeW_T
        Else
            local:FaultCode = tmp:FaultCode_T
        End ! If tmp:FaultCodeW_T <> ''
    End ! If job:Chargeable_Job = 'YES' ANd job:Warranty_Job = 'YES'
    ! End (DBH 13/06/2006) #6733

    Case maf:Field_Number
        Of 1
            ! Changing (DBH 13/06/2006) #6733 - Use Warranty/Chargeable Fault Code
            ! job:Fault_Code1 = tmp:FaultCodeW_T
            ! to (DBH 13/06/2006) #6733
            job:Fault_Code1 = local:FaultCode
            ! End (DBH 13/06/2006) #6733
        Of 2
            job:Fault_Code2 = local:FaultCode
        Of 3
            job:Fault_Code3 = local:FaultCode
        Of 4
            job:Fault_Code4 = local:FaultCode
        Of 5
            job:Fault_Code5 = local:FaultCode
        Of 6
            job:Fault_Code6 = local:FaultCode
        Of 7
            job:Fault_Code7 = local:FaultCode
        Of 8
            job:Fault_Code8 = local:FaultCode
        Of 9
            job:Fault_Code9 = local:FaultCode
        Of 10
            job:Fault_Code10 = local:FaultCode
        Of 11
            job:Fault_Code11 = local:FaultCode
        Of 12
            job:Fault_Code12 = local:FaultCode
    End !Case maf:Field_Number
Waybill:Check Routine
    ! Check if this job requires inserting into the 'waybill generation - awaiting processing' table

    if jobe:HubRepair
!        if job:Incoming_Consignment_Number <> ''                    ! Job must have an empty consignment number
!            do Waybill:CheckRemoveFromAwaitProcess
!            exit
!        end

        if job:Location <> tmp:RRCLocation
            do Waybill:CheckRemoveFromAwaitProcess
            exit
        end

        Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
        wob:RefNumber = job:Ref_Number
        if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.

        Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
        wya:AccountNumber = wob:HeadAccountNumber
        wya:JobNumber   = job:Ref_Number
        if Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
            if not Access:WAYBAWT.PrimeRecord()                     ! Insert new waybill 'awaiting processing' record
                wya:JobNumber     = job:Ref_Number
                wya:AccountNumber = wob:HeadAccountNumber
                wya:Manufacturer  = job:Manufacturer
                wya:ModelNumber   = job:Model_Number
                wya:IMEINumber    = job:ESN
                if Access:WAYBAWT.Update()
                    Access:WAYBAWT.CancelAutoInc()
                end
            end
        else

        end

    else
        do Waybill:CheckRemoveFromAwaitProcess
    end
Waybill:CheckRemoveFromAwaitProcess Routine
    ! Remove waybill 'awaiting processing' record (not actual waybill record(!))

    Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
    wob:RefNumber = job:Ref_Number
    if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.

    Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
    wya:AccountNumber = wob:HeadAccountNumber
    wya:JobNumber = job:Ref_Number
    if not Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
        Relate:WAYBAWT.Delete(0)                                ! Delete record if it exists
    end

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job'
  OF ChangeRecord
    ActionMessage = 'Changing A Job'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020425'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! #11678 Load SBOnline if selected on Trade Account. (Bryan: 09/09/2010)
  if (glo:WebJob and glo:Select20 <> 'NOSBONLINE') ! #11723 Nasty fudge to get around Vodacom's mistake. (Bryan: 07/10/2010)
      Relate:TRADEACC.Open()
      sbOnline# = 0
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = Clarionet:Global.Param2
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          If tra:SBOnlineJobProgress = 2    !tb13153 - SBOnlineJobProgress now has a force (2) setting as well as a use (1) setting - JC 18/09/2013
              sbOnline# = 1
          End ! If tra:UseSBOnline
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      if (sbOnline#)
          ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
          Return RequestCancelled
      end
  end
  GlobalErrors.SetProcedureName('Update_Jobs_Rapid')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(job:Record,History::job:Record)
  SELF.AddHistoryField(?job:Order_Number,42)
  SELF.AddHistoryField(?job:ESN,16)
  SELF.AddHistoryField(?job:MSN,17)
  SELF.AddHistoryField(?job:Current_Status,38)
  SELF.AddHistoryField(?job:Repair_Type_Warranty,90)
  SELF.AddHistoryField(?job:Chargeable_Job,13)
  SELF.AddHistoryField(?job:Charge_Type,36)
  SELF.AddHistoryField(?job:Repair_Type,89)
  SELF.AddHistoryField(?job:Warranty_Job,12)
  SELF.AddHistoryField(?job:Warranty_Charge_Type,37)
  SELF.AddHistoryField(?job:Date_Completed,83)
  SELF.AddHistoryField(?job:Time_Completed,84)
  SELF.AddHistoryField(?job:Date_QA_Passed,52)
  SELF.AddHistoryField(?job:Time_QA_Passed,53)
  SELF.AddHistoryField(?job:date_booked,6)
  SELF.AddHistoryField(?job:time_booked,7)
  SELF.AddHistoryField(?job:who_booked,5)
  SELF.AddHistoryField(?job:Authority_Number,25)
  SELF.AddHistoryField(?job:Location,24)
  SELF.AddUpdateFile(Access:JOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCSTAT.Open
  Relate:AUDIT.Open
  Relate:COMMONCP.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:EXCHOR48.Open
  Relate:JOBEXACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:JOBSE_ALIAS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:MANSAMP.Open
  Relate:MANUDATE.Open
  Relate:NETWORKS.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:ORDWEBPR.Open
  Relate:PARTS_ALIAS.Open
  Relate:SMSMAIL.Open
  Relate:SMSText.Open
  Relate:STOCKALL.Open
  Relate:STOCKALX.Open
  Relate:STOFAULT.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Relate:WAYBILLS.Open
  Relate:WAYITEMS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STAHEAD.UseFile
  Access:STATUS.UseFile
  Access:STOCK.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:MANFAULT.UseFile
  Access:MANUFACT.UseFile
  Access:EXCHHIST.UseFile
  Access:JOBEXHIS.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:USELEVEL.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:ORDERS.UseFile
  Access:STOMODEL.UseFile
  Access:COURIER.UseFile
  Access:JOBNOTES.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:ALLLEVEL.UseFile
  Access:REPAIRTY.UseFile
  Access:TRAFAULT.UseFile
  Access:CONTHIST.UseFile
  Access:CHARTYPE.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBSENG.UseFile
  Access:JOBSE.UseFile
  Access:LOCINTER.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:STDCHRGE.UseFile
  Access:MANFAULO.UseFile
  Access:JOBOUTFL.UseFile
  Access:JOBTHIRD.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATION.UseFile
  Access:MODELNUM.UseFile
  Access:WAYBAWT.UseFile
  Access:JOBSE2.UseFile
  Access:TRDPARTY.UseFile
  Access:TRDBATCH.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
      ! Initialize Before Opening The Window (DBH: 24-03-2005)
      ! Set INI Variables
      tmp:DoNotUseAuthTable = GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:RRCLocation = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:InternalLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:InternalEngLocation = GETINI('HIDE','InternalEngLocation',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:HideNetwork = GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:RenameAutorityNo = GETINI('RENAME','RenameAutorityNo',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:AutorityNoName = GETINI('RENAME','AutorityNoName','Authoity Number',clip(PATH())&'\SB2KDEF.INI')
      tmp:DespatchToCustomer = GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:StatusSendToARC = GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:UseRapidStockAllocation = GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:UseBERComparison = GETINI('ESTIMATE','UseBERComparison',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:BERComparison = GETINI('ESTIMATE','BERComparison',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:CheckPartAvailable = GETINI('ESTIMATE','CheckPartAvailable',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:ForceAccCheckComp = GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:HeadAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:HideSkillLevel = GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI')
      ! _____________________________________________________________________
  
      ! Get the ARC's Site Location (DBH: 24-03-2005)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = tmp:HeadAccount
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Found
          tmp:ARCSiteLocation = tra:SiteLocation
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Error
      End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      ! Get the site location of the current user's location (DBH: 24-03-2005)
      If glo:WebJob
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number    = Clarionet:Global.Param2
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
              tmp:CurrentSiteLocation = tra:SiteLocation
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
          End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Else
          tmp:CurrentSiteLocation = tmp:ARCSiteLocation
      End ! If glo:WebJob
  
          ! _____________________________________________________________________
      ! Prime Files (DBH: 24-03-2005)
  
      ! Get or insert the JOBNOTES file (DBH: 24-03-2005)
      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
      jbn:RefNumber   = job:Ref_Number
      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
          ! Found
  
      Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
          ! Error
          If Access:JOBNOTES.PrimeRecord() = Level:Benign
              jbn:RefNumber = job:Ref_Number
              If Access:JOBNOTES.TryInsert() = Level:Benign
                  ! Insert Successful
              Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
                  ! Insert Failed
              End ! If Access:JOBNOTES.TryInsert() = Level:Benign
          End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
      End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
  
      ! Get or insert the JOBSE file (DBH: 24-03-2005)
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Error
          If Access:JOBSE.PrimeRecord() = Level:Benign
              jobe:RefNumber = job:Ref_Number
              If Access:JOBSE.TryInsert() = Level:Benign
                  ! Insert Successful
  
              Else ! If Access:JOBSE.TryInsert() = Level:Benign
                  ! Insert Failed
                  Access:JOBSE.CancelAutoInc()
              End ! If Access:JOBSE.TryInsert() = Level:Benign
          End !If Access:JOBSE.PrimeRecord() = Level:Benign
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      Do GetJOBSE
  
      ! Inserting (DBH 10/04/2006) #7305 - Get or insert JOBSE2 record
      Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
      jobe2:RefNumber = job:Ref_Number
      If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
          ! Found
  
      Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
          ! Error
          If Access:JOBSE2.PrimeRecord() = Level:Benign
              jobse2:RefNumber = job:Ref_Number
              If Access:JOBSE2.TryInsert() = Level:Benign
                  ! Insert Successful
  
              Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                  ! Insert Failed
                  Access:JOBSE2.CancelAutoInc()
              End ! If Access:JOBSE2.TryInsert() = Level:Benign
          End !If Access:JOBSE2.PrimeRecord() = Level:Benign
      End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! End (DBH 10/04/2006) #7305
  
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
  
      Set(DEFAULT2)
      Access:DEFAULT2.Next()
  
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          ! Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              ! Found
  
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              ! Error
          End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          ! Error
      End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          ! Found
  
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          ! Error
      End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Found
  
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Error
      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
      ! Get the "Vodacom" job number (DBH: 24-03-2005)
      Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
      tra_ali:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
          !Found
  
      Else !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
  
      ! Inserting (DBH 11/10/2006) # 8326 - Save the site location of the booking location
      tmp:BookingSiteLocation = tra_ali:SiteLocation
      ! End (DBH 11/10/2006) #8326
  
      tmp:JobNumber = job:Ref_Number & '-' & tra_ali:BranchIdentification & wob:JobNumber
  
!      !TB12905 - JC 10/12/12
!      if clip(tra_ali:BranchIdentification) = '04'
!        DebugLogging = true
!        DebuggingFile = 'FA60LOG.TXT'
!        if not exists(DebuggingFile) then
!            Create(DebugExport)
!        END
!        Open(DebugExport)
!        debx:TheLine = '============================================================================'
!        Add(DebugExport)
!  
!        debx:TheLine = clip(job:Ref_Number)&' New entry in log file: '&Format(today(),@d06)&' '&format(Clock(),@t4)
!        Add(DebugExport)
!  
!      ELSE
!        DebugLogging = false
!        DebuggingFile = ''
!      END
  
      ! _____________________________________________________________________
      ! Save fields to record in the audit trail later (DBH: 24-03-2005)
      sav:ChargeType          = job:Charge_Type
      sav:WarrantyChargeType  = job:Warranty_Charge_Type
      sav:RepairType          = job:Repair_Type
      sav:WarrantyRepairType  = job:Repair_Type_Warranty
      sav:ChargeableJob       = job:Chargeable_Job
      sav:WarrantyJob         = job:Warranty_Job
      sav:EstimateLabourCost       = job:Labour_Cost_Estimate
      sav:ChargeableLabourCost       = job:Labour_Cost
      sav:WarrantyLabourCost       = job:Labour_Cost_Warranty
      sav:EstimatePartsCost       = job:Parts_Cost_Estimate
      sav:ChargeablePartsCost       = job:Parts_Cost
      sav:WarrantyPartsCost       = job:Parts_Cost_Warranty
      sav:EstimateCourierCost       = job:Courier_Cost_Estimate
      sav:ChargeableCourierCost       = job:Courier_Cost
      sav:WarrantyCourierCost       = job:Courier_Cost_Warranty
      sav:HubRepair               = tmp:HubRepair
      sav:TransitType         = job:Transit_Type
      sav:MSN                 = job:MSN
      sav:ModelNumber         = job:Model_Number
      sav:DOP                 = job:DOP
      sav:OrderNumber         = job:Order_Number
      sav:FaultCode1          = job:Fault_Code1
      sav:FaultCode2          = job:Fault_Code2
      sav:FaultCode3          = job:Fault_Code3
      sav:FaultCode4          = job:Fault_Code4
      sav:FaultCode5          = job:Fault_Code5
      sav:FaultCode6          = job:Fault_Code6
      sav:FaultCode7          = job:Fault_Code7
      sav:FaultCode8          = job:Fault_Code8
      sav:FaultCode9          = job:Fault_Code9
      sav:FaultCode10         = job:Fault_Code10
      sav:FaultCode11         = job:Fault_Code11
      sav:FaultCode12         = job:Fault_Code12
      sav:InvoiceText         = jbn:Invoice_Text
      sav:FaultDescription    = jbn:Fault_Description
  
          ! _____________________________________________________________________
  
      ! Save the current accessories incase they change (DBH: 24-03-2005)
      If GetTempPathA(255,TempFilePath)
          If Sub(TempFilePath,-1,1) = '\'
              tmp:SaveAccessory = Clip(TempFilePath) & 'SAVACC' & Clock() & '.TMP'
          Else !If Sub(TempFilePath,-1,1) = '\'
              tmp:SaveAccessory = Clip(TempFilePath) & '\SAVACC' & Clock() & '.TMP'
          End !If Sub(TempFilePath,-1,1) = '\'
          Remove(tmp:SaveAccessory)
  
          Create(SaveAccessory)
          Open(SaveAccessory)
          If ~Error()
              Save_jac_ID = Access:JOBACC.SaveFile()
              Access:JOBACC.ClearKey(jac:Ref_Number_Key)
              jac:Ref_Number = job:Ref_Number
              Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
              Loop
                  If Access:JOBACC.NEXT()
                     Break
                  End !If
                  If jac:Ref_Number <> job:Ref_Number      |
                      Then Break.  ! End If
                  savacc:Accessory = jac:Accessory
                  Add(SaveAccessory)
              End !Loop
              Access:JOBACC.RestoreFile(Save_jac_ID)
          End !If ~Error()
          Close(SaveAccessory)
      End
  
          ! _____________________________________________________________________
  
  
      tmp:ChargeType          = job:Charge_Type
      tmp:WarrantyChargeType  = job:Warranty_Charge_Type
      tmp:RepairType          = job:Repair_Type
      tmp:WarrantyRepairType  = job:Repair_Type_Warranty
      tmp:UnitType            = job:Unit_Type
      tmp:Saved48HourOption   = tmp:Engineer48HourOption !What is this here for??
  
      !Save Address/Account Details
      accsav:AccountNumber    = job:Account_Number
  
      accsa1:CompanyName      = job:Company_Name
      accsa1:AddressLine1     = job:Address_Line1
      accsa1:AddressLine2     = job:Address_Line2
      accsa1:AddressLine3     = job:Address_Line3
      accsa1:Postcode         = job:Postcode
      accsa1:TelephoneNumber  = job:Telephone_Number
      accsa1:FaxNumber        = job:Fax_Number
  
      accsa2:CompanyName      = job:Company_Name_Delivery
      accsa2:AddressLine1     = job:Address_Line1_Delivery
      accsa2:AddressLine2     = job:Address_Line2_Delivery
      accsa2:AddressLine3     = job:Address_Line3_Delivery
      accsa2:Postcode         = job:Postcode_Delivery
      accsa2:TelephoneNumber  = job:Telephone_Delivery
  
      Warranty_Job_Temp       = job:Warranty_Job
      Chargeable_Job_Temp     = job:Chargeable_Job
      Date_Completed_Temp     = job:Date_Completed
      Time_Completed_Temp     = job:Time_Completed
      Engineer_Temp           = job:Engineer
      tmp:SaveHubRepair       = tmp:HubRepair
      tmp:SaveOBFValidated    = tmp:OBFValidated
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:WARPARTS,SELF)
  BRW39.Init(?List:3,Queue:Browse:2.ViewPosition,BRW39::View:Browse,Queue:Browse:2,Relate:ESTPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! After window open initialization (DBH: 24-03-2005)
      ! Is this record in use? (DBH: 24-03-2005)
      If ThisWindow.Request <> InsertRecord
          if (jobInUse(job:ref_Number,0))
  ! Delete --- Use the routine as it continues the SBOnline lock check (DBH: 15/02/2010) #
  !        Pointer# = Pointer(JOBS)
  !        Hold(JOBS,1)
  !        Get(JOBS,Pointer#)
  !        If ErrorCode() = 43
  !
  ! end --- (DBH: 15/02/2010) #
              ! Inserting (DBH 03/07/2006) # 7149 - Do not show the receipt button if the job is in use
              ?Button:ReceiptFromPUP{prop:Hide} = True
              ! End (DBH 03/07/2006) #7149
              If SecurityCheck('EDIT LOCKED JOBS') = False
                  Case Missive('This job is currently in use by another station.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish to EDIT his job?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          Release(JOBS)
                      Of 1 ! No Button
                          ThisWindow.Request = ViewRecord
                          ?OK{prop:Hide} = True
                  End ! Case Missive
              Else ! If SecurityCheck('EDIT LOCKED JOBS') = False
                  Case Missive('This job is currently in use by another station.'&|
                    '<13,10>'&|
                    '<13,10>This screen will be VIEW ONLY.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  ThisWindow.Request = ViewRecord
                  ?OK{prop:Hide} = True
              End ! If SecurityCheck('EDIT LOCKED JOBS') = False
          End ! If ErrorCode() = 43
      End ! ThisWindow.Request <> InsertRecord
  
      ! _____________________________________________________________________
      ! Show and hide depending on Warranty/Chargeable Job Types (DBH: 24-03-2005)
      Do Enable_Disable
  
      !show line if estimate not replied to
      do CheckEstimateReplies  !JC 12477 03/12
  
      ! Show the Engineer Details (DBH: 24-03-2005)
      Do Lookup_Engineer
      ! Show completed, or estimate info text (DBH: 24-03-2005)
      Do ShowCompletedText
  
      ! Fill in the trade and model display fields (DBH: 24-03-2005)
      If job:Surname <> ''
          Trade_Account_Temp = Clip(job:Account_Number) & ' (' & Clip(job:Title) & ' ' & Clip(job:Initial) & |
                              ' ' & Clip(job:Surname) & ')'
      Else ! job:Surname <> ''
  
          Trade_Account_Temp = Clip(job:Account_Number) & ' (' & Clip(sub:Company_Name) & ')'
      End ! job:Surname <> ''
  
  
      Model_Details_Temp = Clip(job:Model_Number) & ' ' & Clip(job:Manufacturer) & ' - ' & Clip(job:Unit_Type)
  
      ! Show the job is completed (DBH: 24-03-2005)
      If job:Date_Completed <> ''
          ?Completed{prop:Hide} = False
      End ! job:Date_Completed <> ''
  
      ! Do not allow to allocate an engineer if job is not in workshop (DBH: 24-03-2005)
      If job:Workshop <> 'YES'
          ?Lookup_Engineer{prop:Disable} = True
          ?AllocateJob{prop:Disable} = True
      Else ! job:Workshop <> 'YES'
          ?Lookup_Engineer{prop:Disable} = False
          ?AllocateJob{prop:Disable} = False
      End ! job:Workshop <> 'YES'
  
      ! Hide the internal location (DBH: 24-03-2005)
      If def:HideLocation
          tmp:DisableLocation = tmp:InternalLocation
          If tmp:DisableLocation
              ?job:Location{prop:Disable} = True
          Else ! If tmp:DisableLocation
              ?job:Location{prop:Hide} = True
              ?job:Location:Prompt{prop:Hide} = True
          End ! If tmp:DisableLocation
      Else ! def:HideLocation
          If tmp:InternalEngLocation
              ?job:Location{prop:Hide} = True
              ?job:Location:Prompt{prop:Hide} = True
          End ! If tmp:InternalEngLocation
      End ! def:HideLocation
  
      ! Do not allow to change the engineer (DBH: 24-03-2005)
      If de2:AllocateEngPassword
          ?Lookup_Engineer{prop:Hide} = True
      End ! de2:AllocationEngPassword
  
      If man:ForceParts
          ?ChargeableAdjustment{prop:Hide} = False
          ?WarrantyAdjustment{prop:Hide} = False
      Else ! man:ForceParts
          ?ChargeableAdjustment{prop:Hide} = True
          ?WarrantyAdjustment{prop:Hide} = True
      End ! man:ForceParts
  
      IF tmp:HideNetwork = True
          ?tmp:Network{prop:Hide} = True
          ?tmp:Network:Prompt{prop:Hide} = True
          ?LookupNetwork{prop:Hide} = True
      Else ! tmp:HideNetwork = True
          ?tmp:Network{prop:Hide} = False
          ?tmp:Network:Prompt{prop:Hide} = False
          ?LookupNetwork{prop:Hide} = False
      End ! tmp:HideNetwork = True
  
      If tmp:InternalEngLocation = True
          ?job:Location{prop:Hide} = True
          ?job:Location:Prompt{prop:Hide} = True
      Else ! tmp:InternalEngLocation = True
          ?job:Location{prop:Hide} = False
          ?job:Location:Prompt{prop:Hide} = False
      End ! tmp:InternalEngLocation = True
  
      If def:Hide_Authority_Number = 'YES' And jobe:OBFValidated = False
          ?AuthorityNoPrompt{prop:Hide} = True
          ?job:Authority_NUmber{prop:Hide} = True
      End ! def:Hide_Authority_Number = 'YES' And jobe:OBFValidated = False
  
      If tmp:RenameAutorityNo = True
          ?AuthorityNoPrompt{prop:Text} = Clip(tmp:AutorityNoName)
      End ! tmp:RenameAuthorityNo = True
  
      ! Activate the 48 Hour option and show the booking option (DBH: 24-03-2005)
      If AccountActivate48Hour(wob:HeadAccountNumber) = True
          ?tmp:Engineer48HourOption:Radio1{prop:Hide} = False
          If Allow48Hour(job:ESN,job:Model_Number,wob:HeadAccountNumber) = True
              ?tmp:Engineer48HourOption:Radio1{prop:Disable} = False
          Else ! If Allow48Hour(job:ESN,job:Model_Number) = True
              ?tmp:Engineer48HourOption:Radio1{prop:Disable} = True
          End ! If Allow48Hour(job:ESN,job:Model_Number) = True
      Else ! AccountActivate48Hour(wob:HeadAccountNumber) = True
          ?tmp:Engineer48HourOption:Radio1{prop:Hide} = True
      End ! AccountActivate48Hour(wob:HeadAccountNumber) = True
  
      Case tmp:Booking48HourOption
          Of 1
              ?BookingOption:Prompt{prop:Text} = '48 Hour Exchange'
          Of 2
              ?BookingOption:Prompt{prop:Text} = 'ARC Repair'
          Of 3
              ?BookingOption:Prompt{prop:Text} = '7 Day TAT'
          of 4
              ! DBH #10544 - Add new booking option
              ?BookingOption:Prompt{prop:Text} = 'Liquid Damage'
          Else
              ?BookingOption:Prompt{prop:Text} = 'Not Selected'
      End ! tmp:Booking48HourOption
  
      ! Do not allow an ARC engineer to change an RRC's options (DBH: 24-03-2005)
      If jobe:WebJob And ~glo:WebJob
          ?tmp:Engineer48HourOption{prop:Disable} = True
      End ! jobe:WebJob And ~job:WebJob
  
          ! _____________________________________________________________________
  
      ! Show the warranty claim status (and date paid by RRC) (DBH: 24-03-2005)
      ! Show resubmit button, if rejected (DBH: 24-03-2005)
      If job:Warranty_Job = 'YES'
          If job:Date_Completed <> ''
              ?ClaimStatus{prop:Hide} = False
              ?ClaimStatus:2{prop:Hide} = False
              ?ClaimStatus{prop:Text} = 'H/O STATUS: ' & Clip(jobe:WarrantyClaimStatus)
              IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer) AND jobe:WebJob <> 1)
                  ?ClaimStatus:2{prop:Text} = 'Third Party Claimed'
              ELSE
                  ?ClaimStatus:2{prop:Text} = 'RRC STATUS: ' & Clip(WOBEDIStatus(wob:EDI))
              END
              If wob:EDI = 'PAY'
                  Save_aud_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.Clearkey(aud:Action_Key)
                  aud:Ref_Number = job:Ref_Number
                  aud:Action = 'WARRANTY CLAIM MARKED PAID (RRC)'
                  Set(aud:Action_Key,aud:Action_Key)
                  Loop ! Begin AUDIT Loop
                      If Access:AUDIT.Next()
                          Break
                      End ! If !Access
                      If aud:Ref_Number <> job:Ref_Number
                          Break
                      End ! If
                      If aud:Action <> 'WARRANTY CLAIM MARKED PAID (RRC)'
                          Break
                      End ! If
                      ?ClaimStatus{prop:Text} = Clip(?ClaimStatus{prop:Text}) & ' (' & Format(aud:Date,@d6) & ')'
                      Break
                  End ! End AUDIT Loop
                  Access:AUDIT.RestoreFile(Save_aud_ID)
              End ! If wob:EDI = 'PAY'
              If job:EDI = 'REJ'
                  ?ResubmitJob{prop:Hide} = False
              End ! If job:EDI = 'REJ'
          Else ! If job:Date_Completed <> ''
              IF (job:EDI <> 'XXX')
                  ?ClaimStatus{prop:Hide} = False   ! #12363 EDI should always be XXX for incomplete jobs (except for Third Party Despatches) (DBH: 15/02/2012)
                  ?ClaimStatus:2{prop:Hide} = False
                  ?ClaimStatus{prop:Text} = 'H/O STATUS: ' & Clip(jobe:WarrantyClaimStatus)
                  ?ClaimStatus:2{prop:Text} = 'Third Party Claimed'
              ELSE ! IF (job:EDI <> 'XXX')
                  ?ClaimStatus{prop:Hide} = True
                  ?ClaimStatus:2{prop:Hide} = True
              END ! IF (job:EDI <> 'XXX')
          End ! If job:Date_Completed <> ''
      Else ! job:Warranty_Job = 'YES'
          ?ClaimStatus{prop:Hide} = True
          ?ClaimStatus:2{prop:Hide} = True
      End ! job:Warranty_Job = 'YES'
  
      ! Hide engineer skill level (DBH: 24-03-2005)
      If Clip(tmp:HideSkillLevel) = 1
          ?Prompt17:2{prop:Hide} = True
          ?jobe:SkillLevel{prop:Hide} = True
      End ! Clip(tmp:HideSkillLevel) = 1
          ! _____________________________________________________________________
  
      ! Security checking (DBH: 24-03-2005)
      If SecurityCheck('RAPID ENG - CREATE INVOICE') = Level:Benign
          ?Invoice{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - CREATE INVOICE') = Level:Benign
  
  ! Deleting (DBH 12/10/2006) # 8060 - I don't think this does anything anymore
  !    If SecurityCheck('RAPID ENG - CONTACT HISTORY') = Level:Benign
  !        ?ContactHistory{prop:Hide} = 0
  !    End !SecurityCheck('RAPID ENG - CONTACT HISTORY') = Level:Benign
  ! End (DBH 12/10/2006) #8060
      If SecurityCheck('RAPID ENG - COSTS') = Level:Benign
          ?Costs{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - COSTS') = Level:Benign
  
      If SecurityCheck('RAPID ENG - CHANGE JOB STATUS') = Level:Benign
          ?ChangeJobStatus{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - CHANGE JOB STATUS') = Level:Benign
  
      If SecurityCheck('RAPID ENG - ALLOCATE JOB') = Level:Benign
          ?AllocateJob{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - ALLOCATE JOB') = Level:Benign
  
      If SecurityCheck('RAPID ENG - AMEND UNIT TYPE') = Level:Benign
          ?AmendUnitType{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - AMEND UNIT TYPE') = Level:Benign
  
      If SecurityCheck('RAPID ENG - ENGINEER HISTORY') = Level:Benign
          ?EngineerHistory{prop:Hide} = 0
          ?BrowseEngineerHistory{prop:Disable} = 0
      End !SecurityCheck('RAPID ENG - ENGINEER HISTORY') = Level:Benign
  
      If SecurityCheck('RAPID ENG - ACCESSORIES') = Level:Benign
          ?ViewAccessories{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - VIEW ACCESSORIES') = Level:Benign
  
      If SecurityCheck('RAPID ENG - AMEND FAULT CODES') = Level:Benign
          ?Fault_Codes{prop:Hide} = 0
      End !SecurityCheck('RAPID ENG - AMEND FAULT CODES') = Level:Benign
  
      If SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
          ?tmp:HubRepair{prop:Disable} = 0
      Else !SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
          ?tmp:HubRepair{prop:Disable} = 1
      End !SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
  
      If SecurityCheck('ADDRESS MANIPULATION') = Level:Benign
          ?BrowseAmendAddresses{prop:Disable} = 0
      Else
          ?BrowseAmendAddresses{prop:Disable} = 1
      End
  
      If SecurityCheck('JOBS - SHOW COSTS')
          ?Costs{prop:Hide} = True
          ?Invoice{prop:Hide} = True
      End ! SecurityCheck('JOBS - SHOW COSTS')
  
      If ?CompleteButton{prop:Disable} = False
          Do DisplayCompleteButton
      End ! ?CompleteButton{prop:Disable} = False
  
      ! Can the user access a completed/invoiced job? (DBH: 24-03-2005)
      If job:Date_Completed <> ''
          If SecurityCheck('AMEND COMPLETED JOBS')
              If job:Invoice_Date <> ''
                  If SecurityCheck('AMEND INVOICED JOBS')
                      ?Multi_Insert{prop:Disable} = True
                      ?Multi_Insert:2{prop:Disable} = True
                      ?Insert{prop:Disable} = True
                      ?Insert:3{prop:Disable} = True
                      ?Delete{prop:Disable} = True !28/11/02
                      ?Delete:2{prop:Disable} = True
                      ?Delete:3{prop:Disable} = True
                      ?ExchangeButton{prop:Disable} = True !3/12/02 (g155)
                      ?ExchangeButton:2{prop:Disable} = True
                  End ! If SecurityCheck('AMEND INVOICED JOBS')
              Else ! If job:Invoice_Date <> ''
                  ?Multi_Insert{prop:Disable} = True
                  ?Multi_Insert:2{prop:Disable} = True
                  ?Insert{prop:Disable} = True
                  ?Insert:3{prop:Disable} = True
                  ?Delete{prop:Disable} = True
                  ?Delete:2{prop:Disable} = True
                  ?Delete:3{prop:Disable} = True
                  ?ExchangeButton{prop:Disable} = True
                  ?ExchangeButton:2{prop:Disable} = True
              End ! If job:Invoice_Date <> ''
          End ! If SecurityCheck('AMEND COMPLETED JOBS')
      End ! job:Date_Completed <> ''
          ! _____________________________________________________________________
  
      ! Bouncer check (DBH: 24-03-2005)
      x# = CountHistory(job:ESN,job:Ref_Number,job:Date_Booked)
      If x# > 0
          ?BouncerButton{prop:Hide} = False
          ?Bouncer_Text{prop:FontColor} = color:Red
          Bouncer_Text = x# & ' PREV. JOB(S)'
      End ! x#
          ! _____________________________________________________________________
  
      ! Show number of parts on each tab (DBH: 24-03-2005)
      Do CountParts
          ! _____________________________________________________________________
  
      save_EXCHANGE_id = Access:EXCHANGE.SaveFile()
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN = job:ESN
      If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
          ! Found
          If xch:Available = 'REP' or xch:Available = 'INR'
              ?ExchangeRepair{prop:Text} = 'EXCHANGE REPAIR'
          End ! If xch:Available = 'REP' or xch:Available = 'INR'
      Else ! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
          ! Error
      End ! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      Access:EXCHANGE.RestoreFile(save_EXCHANGE_id)
          ! _____________________________________________________________________
  
      If job:Cancelled = 'YES'
          ?OK{prop:Hide} = True
          ?CancelText{prop:Hide} = False
          ?CancelText{prop:Text} = 'JOB CANCELLED'
          ?CancelText{prop:FontColor} = color:Red
      Else ! job:Cancelled = 'YES'
          If jobe:FailedDelivery
              ?CancelText{prop:Hide} = False
              ?CancelText{prop:Text} = 'FAILED DELIVERY'
              ?CancelText{prop:FontColor} = color:Green
          End ! If jobe:FailedDelivery
      End ! job:Cancelled = 'YES'
      ! _____________________________________________________________________
  
      ! Inserting (DBH 27/06/2006) #6420 - Show / Hide the liquid damage text
      If IsUnitLiquidDamaged(job:Ref_Number,job:ESN,TRUE) = True
          ?Prompt:LiquidDamage{prop:Hide} = False
      Else ! If IsUnitLiquidDamaged(job:Ref_Number,job:ESN) = True
          ?Prompt:LiquidDamage{prop:Hide} = True
      End ! If IsUnitLiquidDamaged(job:Ref_Number,job:ESN) = True
      ! End (DBH 27/06/2006) #6420
  
      ! Inserting (DBH 05/07/2006) # N/A - If MSN is required, but not filled in, allow the user to enter it
      If MSNRequired(job:Manufacturer) = Level:Benign
          ?job:MSN{prop:Hide} = False
          ?job:MSN:Prompt{prop:Hide} = False
          If job:MSN = ''
              ?job:MSN{prop:Boxed} = False
              ?job:MSN{prop:trn} = False
              ?job:MSN{prop:ReadOnly} = False
              ?job:MSN{prop:FontColor} = 0101010H
              ?job:MSN{prop:Color} = color:White
          End ! If job:MSN = ''
      Else ! If MSNRequired(job:Manufacturer) = Level:Benign
          ?job:MSN{prop:Hide} = True
          ?job:MSN:Prompt{prop:Hide} = True
      End ! If MSNRequired(job:Manufacturer) = Level:Benign
      ! End (DBH 05/07/2006) #N/A
  
      ! Inserting (DBH 12/10/2006) # 8060 - Make it obvious that contact history has been entered
      Do CheckContactHistory
      ! End (DBH 12/10/2006) #8060
  
      ! In an RRC engineer allowed to view a "Sent To Hub" job? (DBH: 24-03-2005)
  
      glo:Preview = ''
  
      !Moved from end of routine By Paul 18/09/09 Log no 11079
      do LockPUPJob
  
      ! If completed, do not allow to change engineer option (DBH: 24-03-2005)
      ! DBH #11429. Moved this line further down to ensure that the option is disabled.
      If job:Date_Completed <> ''
          ?tmp:Engineer48HourOption{prop:Disable} = True
      End ! job:Date_Completed <> ''
  
  
      If glo:WebJob
          !Does this user have the correct access level
          !to amend a job that was sent to the Hub? - 4348 (DBH: 21-07-2004  20)
          If SentToHub(job:Ref_Number)
              If SecurityCheck('JOBS - RRC AMEND ARC JOB')
                  !Disable the job and make the child windows "read only" - 4348 (DBH: 21-07-2004  20)
                  local.disableJob(1)
                  ?Sheet1{prop:Disable} = True
                  ?tmp:HubRepair{prop:Disable} = True
                  ?tmp:OBFValidated{prop:Disable} = True
                  ?Chargeable_Group{prop:Disable} = True
                  ?Warranty_Group{prop:Disable} = True
                  ?Multi_Insert{prop:Hide} = True
                  ?ChargeableAdjustment{prop:Hide} = True
                  ?ExchangeButton{prop:Hide} = True
                  ?Change{prop:Hide} = True
                  ?Delete{prop:Hide} = True
                  ?Multi_Insert:2{prop:Hide} = True
                  ?WarrantyAdjustment{prop:Hide} = True
                  ?ExchangeButton:2{prop:Hide} = True
                  ?Change:3{prop:Hide} = True
                  ?Delete:3{prop:Hide} = True
                  ?ValidatePOP{prop:Hide} = True
                  glo:Preview = 'V'
  
              End ! If SecurityCheck('JOBS - RRC AMEND ARC JOB')
          End ! If SentToHub(job:Ref_Number)
          ! _____________________________________________________________________
  
          !Check the see if viewing job was booked by the
          !RRC viewing it - L910 (DBH: 01-09-2003)
  ! Changing (DBH 18/07/2008) # 10220 - Add PUP locations
  !        If (job:Location <> tmp:RRCLocation And job:Location <> tmp:DespatchToCustomer) Or |
  ! to (DBH 18/07/2008) # 10220
          If (job:Location <> tmp:RRCLocation And job:Location <> tmp:DespatchToCustomer And |
              job:Location <> GETINI('RRC','InTransitToPUPLocation',,Clip(Path()) & '\SB2KDEF.INI') And |
              job:Location <> GETINI('RRC','AtPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')) Or |
              tmp:HubRepair = True Or |
              (wob:HeadAccountNumber <> Clarionet:Global.Param2)
              Case Missive('This job is not in this RRC''s control and cannot be amended.'&|
                '<13,10>'&|
                '<13,10>Click ''OK'' to VIEW the job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              If SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
                  ?tmp:OBFvalidated{prop:Disable} = 1
              Else !SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
                  ?tmp:HubRepair{prop:Disable} = True
                  ?tmp:OBFValidated{prop:Disable} = True
              End !SecurityCheck('JOBS - HUB REPAIR') = Level:Benign
  
              local.disableJob(1)
  
              glo:Preview = 'V'
          Else ! If (job:Location <> tmp:RRCLocation And job:Location <> tmp:DespatchToCustomer) Or |
              !Is this job a warranty job. If so has it been completed
              !at the RRC. If so, then it has been submitted and cannot be changed - 3788 (DBH: 14-04-2004)
              !DO CheckWarrantyJobEdit      !J - TB13265 - Don't allow to edit accepted Warranty jobs
              If job:Warranty_Job = 'YES' And wob:EDI <> 'XXX'
                  If job:Date_Completed <> '' And ~SentToHub(job:Ref_Number)
                      If wob:EDI = 'NO' Or wob:EDI = 'YES' Or wob:EDI = 'PAY' or wob:EDI = 'APP'
                          local.disableJob(1)
                          ?tmp:HubRepair{prop:Disable} = True
                          ?tmp:OBFValidated{prop:Disable} = True
                          ?OK{prop:Hide} = True
  
                          glo:Preview = 'V'
                      Else ! If wob:EDI = 'NO' Or wob:EDI = 'YES' Or wob:EDI = 'PAY' or wob:EDI = 'APP'
                          Error# = 0
                          Case Missive('The selected Warranty Job has been rejected.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to EDIT (password required), or just VIEW the job details?','ServiceBase 3g',|
                                         'mquest.jpg','View|Edit')
                              Of 2 ! Edit Button
                                  Access:USERS.Clearkey(use:User_Code_Key)
                                  use:User_Code   = InsertEngineerPassword()
                                  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                      ! Found
                                      Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
                                      acc:User_Level  = use:User_Level
                                      acc:Access_Area = 'EDIT REJECTED WARRANTY JOB'
                                      If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
                                          ! Found
  
                                      Else ! If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
                                          ! Error
                                          Error# = 1
                                      End ! If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
                                  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                      ! Error
                                      Error# = 1
                                  End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  If Error# = 1
                                      Case Missive('You do not have sufficient access to edit this job.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                  End ! If Error# = 1
                              Of 1 ! View Button
                                  Error# = 1
                          End ! Case Missive
                          If Error# = 1
                              local.disableJob(1)
                              ?tmp:HubRepair{prop:Disable} = True
                              ?tmp:OBFValidated{prop:Disable} = True
                              ?OK{prop:Hide} = True
                              glo:Preview = 'V'
                          End ! If Error# = 1
                      End ! If wob:EDI = 'NO' Or wob:EDI = 'YES' Or wob:EDI = 'PAY' or wob:EDI = 'APP'
                  End ! If job:Date_Completed <> '' And ~SentToHub(job:Ref_Number)
              End ! If job:Warranty_Job = 'YES' And wob:EDI <> 'XXX'
          End ! If (job:Location <> tmp:RRCLocation And job:Location <> tmp:DespatchToCustomer) Or |
      ELSE
          !ARC
          !DO CheckWarrantyJobEdit      !J - TB13265 - Don't allow to edit accepted Warranty jobs
      End ! glo:WebJob
  
      ! Inserting (DBH 07/11/2007) # 9200 - Show the loyalty status
      ?Prompt:LoyaltyStatus{prop:Hide} = 1
      Access:AUDIT.Clearkey(aud:Action_Key)
      aud:Ref_Number = job:Ref_Number
      aud:Action = 'MSISDN VALIDATION'
      aud:Date = Today()
      Set(aud:Action_Key,aud:Action_Key)
      Loop
          If Access:AUDIT.Next()
              Break
          End ! If Access:AUDIT.Next()
          If aud:Ref_Number <> job:Ref_Number
              Break
          End ! If aud:Ref_Number <> job:Ref_Number
          If aud:Action <> 'MSISDN VALIDATION'
              Break
          End ! If aud:Action <> 'MSISDN VALIDATION'
  
          Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
          aud2:AUDRecordNumber = aud:Record_Number
          IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
              If Instring('LOYALTY STATUS: VIP',aud2:Notes,1,1)
                  ?Prompt:LoyaltyStatus{prop:Hide} = 0
                  ?Prompt:LoyaltyStatus{prop:Text} = 'VIP Customer'
                  Break
              End ! If Instring('LOYALTY STATUS: VIP',aud:Notes,1,1)
              If Instring('LOYALTY STATUS: ONIX',aud2:Notes,1,1)
                  ?Prompt:LoyaltyStatus{prop:Hide} = 0
                  ?Prompt:LoyaltyStatus{prop:Text} = 'ONIX Customer'
                  Break
              End ! If Instring('LOYALTY STATUS: VIP',aud:Notes,1,1)
              If Instring('LOYALTY STATUS: PLATINUM',aud2:Notes,1,1)
                  ?Prompt:LoyaltyStatus{prop:Hide} = 0
                  ?Prompt:LoyaltyStatus{prop:Text} = 'PLATINUM Customer'
                  Break
              End ! If Instring('LOYALTY STATUS: VIP',aud:Notes,1,1)
          END ! IF
  
  
      End ! Loop
      ! End (DBH 07/11/2007) #9200
  
      IF (job:Third_Party_Site <> '')
          ! Job has been sent to third party
          claimMade# = 0
          hideParts# = 0
          IF (job:Warranty_Job = 'YES')
              ! #12363 Should a warranty claim of been made? (DBH: 16/02/2012)
              claimMade# = _PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer)
          END
  
              
  
          IF (job:Workshop <> 'YES')
              ! Job is still at third party
              Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
              trd:Company_Name = job:Third_Party_Site
              If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                  !Found
                  If trd:LHubAccount
                      ?Button:ResendXML{prop:Hide} = 0 ! #10394
                  End ! If trd:LHubAccount
              Else ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                  !Error
              End ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
  
              IF (claimMade# = 1)
                  IF (job:EDI <> 'XXX' AND job:EDI <> 'EXC')
                      ! #12363 A claim has been made, and not rejected. Disable the parts. (DBH: 16/02/2012)
                      hideParts# = 1
                  END ! IF (job:EDI <> 'XXX' AND job:EDI <> 'REJ')
              END ! IF (claimMade# = 1)
          ELSE! IF (job:Workshop <> 'YES')
              IF (claimMade# = 1)
                  IF (job:EDI <> 'XXX' AND job:EDI <> 'EXC') ! #12363 Allow to access parts of rejected jobs, after the job has been received (DBH: 05/04/2012)
                      ! #12363 A claim has been made, and not rejected. Disable the parts. (DBH: 16/02/2012)
                      hideParts# = 1
                  END ! IF (job:EDI <> 'XXX' AND job:EDI <> 'REJ')
              END
  
          END ! IF (job:Workshop <> 'YES')
  
          IF (hideParts# = 1)
              ?Chargeable_Group{prop:Disable} = True
              ?Warranty_Group{prop:Disable} = True
              ?Multi_Insert{prop:Hide} = True
              ?ChargeableAdjustment{prop:Hide} = True
              ?ExchangeButton{prop:Hide} = True
              ?Change{prop:Hide} = True
              ?Delete{prop:Hide} = True
              ?Multi_Insert:2{prop:Hide} = True
              ?WarrantyAdjustment{prop:Hide} = True
              ?ExchangeButton:2{prop:Hide} = True
              ?Change:3{prop:Hide} = True
              ?Delete:3{prop:Hide} = True
          END
      END ! IF (job:Third_Party_Site <> '')
  
      ! DBH #11388 - Show Customer Classification Button
      If (CustCollectionValidated(job:Ref_Number))
          ?buttonCustomerClassification{prop:Hide} = 0
      End
  
      ! DBH #10544 - Don't allow users to add parts to Liquid Damage Jobs
      If (?BookingOption:Prompt{prop:Text} = 'Liquid Damage')
          ?EstimateGroup{prop:Disable} = 1
          ?Warranty_Group{prop:Disable} = 1
          ?Chargeable_Group{prop:Disable} = 1
      end
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?job:Order_Number{prop:boxed} = False
  ?job:ESN{prop:boxed} = False
  ?job:MSN{prop:boxed} = False
  ?trade_account_temp{prop:boxed} = False
  ?model_details_temp{prop:boxed} = False
  ?job:Current_Status{prop:boxed} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Network{Prop:Tip} AND ~?LookupNetwork{Prop:Tip}
     ?LookupNetwork{Prop:Tip} = 'Select ' & ?tmp:Network{Prop:Tip}
  END
  IF ?tmp:Network{Prop:Msg} AND ~?LookupNetwork{Prop:Msg}
     ?LookupNetwork{Prop:Msg} = 'Select ' & ?tmp:Network{Prop:Msg}
  END
  IF SELF.Request = ViewRecord
    DISABLE(?ButtonHelp)
    DISABLE(?Combo3)
    DISABLE(?buttonCustomerClassification)
    DISABLE(?job:Repair_Type_Warranty)
    DISABLE(?ResubmitJob)
    DISABLE(?job:Chargeable_Job)
    DISABLE(?job:Charge_Type)
    DISABLE(?job:Repair_Type)
    DISABLE(?job:Warranty_Job)
    DISABLE(?job:Warranty_Charge_Type)
    ?job:Date_Completed{PROP:ReadOnly} = True
    ?job:Time_Completed{PROP:ReadOnly} = True
    ?job:Date_QA_Passed{PROP:ReadOnly} = True
    ?job:Time_QA_Passed{PROP:ReadOnly} = True
    ?job:date_booked{PROP:ReadOnly} = True
    ?job:time_booked{PROP:ReadOnly} = True
    ?job:who_booked{PROP:ReadOnly} = True
    DISABLE(?tmp:HubRepair)
    ?tmp:HubRepairDate{PROP:ReadOnly} = True
    ?tmp:HubRepairTime{PROP:ReadOnly} = True
    ?tmp:OBFvalidateDate{PROP:ReadOnly} = True
    ?tmp:OBFvalidateTime{PROP:ReadOnly} = True
    DISABLE(?engineers_notes)
    DISABLE(?ChangeJobStatus)
    DISABLE(?BouncerButton)
    DISABLE(?ValidatePOP)
    ?tmp:Network{PROP:ReadOnly} = True
    DISABLE(?LookupNetwork)
    DISABLE(?AmendUnitType)
    DISABLE(?Lookup_Engineer)
    ?engineer_name_temp{PROP:ReadOnly} = True
    DISABLE(?AllocateJob)
    DISABLE(?Costs)
    DISABLE(?Invoice)
    ?job:Authority_Number{PROP:ReadOnly} = True
    DISABLE(?ColourKey)
    DISABLE(?Button:ResendXML)
    DISABLE(?Button:ContactHistoryFilled)
    DISABLE(?job:Location)
    DISABLE(?Fault_Codes)
    DISABLE(?ViewAccessories)
    DISABLE(?EngineerHistory)
    DISABLE(?Insert:2)
    DISABLE(?Change:2)
    DISABLE(?Delete:2)
    DISABLE(?Multi_Insert)
    DISABLE(?ChargeableAdjustment)
    DISABLE(?Insert)
    DISABLE(?ExchangeButton)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Multi_Insert:2)
    DISABLE(?WarrantyAdjustment)
    DISABLE(?ExchangeButton:2)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Button:ReceiptFromPUP)
    DISABLE(?Close)
    HIDE(?OK)
    DISABLE(?CompleteButton)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,par:Part_Number_Key)
  BRW9.AddRange(par:Ref_Number,Relate:PARTS,Relate:JOBS)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,par:Part_Number,1,BRW9)
  BIND('tmp:CPartQuantity',tmp:CPartQuantity)
  BIND('tmp:CShelfLocation',tmp:CShelfLocation)
  BIND('tmp:CColourFlag',tmp:CColourFlag)
  BRW9.AddField(par:Part_Number,BRW9.Q.par:Part_Number)
  BRW9.AddField(par:Description,BRW9.Q.par:Description)
  BRW9.AddField(tmp:CPartQuantity,BRW9.Q.tmp:CPartQuantity)
  BRW9.AddField(par:Quantity,BRW9.Q.par:Quantity)
  BRW9.AddField(tmp:CShelfLocation,BRW9.Q.tmp:CShelfLocation)
  BRW9.AddField(tmp:CColourFlag,BRW9.Q.tmp:CColourFlag)
  BRW9.AddField(par:Record_Number,BRW9.Q.par:Record_Number)
  BRW9.AddField(par:Ref_Number,BRW9.Q.par:Ref_Number)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,wpr:Part_Number_Key)
  BRW10.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,wpr:Part_Number,1,BRW10)
  BIND('tmp:WPartQuantity',tmp:WPartQuantity)
  BIND('tmp:WShelfLocation',tmp:WShelfLocation)
  BIND('tmp:WColourFlag',tmp:WColourFlag)
  BRW10.AddField(wpr:Part_Number,BRW10.Q.wpr:Part_Number)
  BRW10.AddField(wpr:Description,BRW10.Q.wpr:Description)
  BRW10.AddField(tmp:WPartQuantity,BRW10.Q.tmp:WPartQuantity)
  BRW10.AddField(wpr:Quantity,BRW10.Q.wpr:Quantity)
  BRW10.AddField(tmp:WShelfLocation,BRW10.Q.tmp:WShelfLocation)
  BRW10.AddField(tmp:WColourFlag,BRW10.Q.tmp:WColourFlag)
  BRW10.AddField(wpr:Record_Number,BRW10.Q.wpr:Record_Number)
  BRW10.AddField(wpr:Ref_Number,BRW10.Q.wpr:Ref_Number)
  BRW39.Q &= Queue:Browse:2
  BRW39.AddSortOrder(,epr:Part_Number_Key)
  BRW39.AddRange(epr:Ref_Number,Relate:ESTPARTS,Relate:JOBS)
  BRW39.AddLocator(BRW39::Sort0:Locator)
  BRW39::Sort0:Locator.Init(,epr:Part_Number,1,BRW39)
  BIND('tmp:EShelfLocation',tmp:EShelfLocation)
  BIND('tmp:EColourFlag',tmp:EColourFlag)
  BRW39.AddField(epr:Part_Number,BRW39.Q.epr:Part_Number)
  BRW39.AddField(epr:Description,BRW39.Q.epr:Description)
  BRW39.AddField(epr:Quantity,BRW39.Q.epr:Quantity)
  BRW39.AddField(tmp:EShelfLocation,BRW39.Q.tmp:EShelfLocation)
  BRW39.AddField(tmp:EColourFlag,BRW39.Q.tmp:EColourFlag)
  BRW39.AddField(epr:Record_Number,BRW39.Q.epr:Record_Number)
  BRW39.AddField(epr:Ref_Number,BRW39.Q.epr:Ref_Number)
  BRW9.AskProcedure = 4
  BRW10.AskProcedure = 5
  BRW39.AskProcedure = 6
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  BRW39.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW39.AskProcedure = 0
      CLEAR(BRW39.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Fill In WebJobs again, just in case anything is lost - TrkBs: 6554 (DBH: 18-10-2005)
  ! Inserting (DBH 09/?6/2006) #7804 - Code moved here, so it still fills in even if cancel pressed
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Found
      wob:SubAcountNumber  = job:account_number
      wob:OrderNumber      = job:Order_Number
      wob:IMEINumber       = job:ESN
      wob:MSN              = job:MSN
      wob:Manufacturer     = job:Manufacturer
      wob:ModelNumber      = job:Model_Number
      wob:MobileNumber     = job:Mobile_Number
      wob:Current_Status       = job:Current_Status
      wob:Exchange_Status      = job:Exchange_Status
      wob:Loan_Status          = job:Loan_Status
      Access:WEBJOB.Update()
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  ! End (DBH 09/06/2006) #7804
  ! Inserting (DBH 12/03/2007) # 8718 - Keep the IMEI Number in the child files up to date
  Relate:JOBSOBF.Open()
  Save_JOBSOBF_ID = Access:JOBSOBF.SaveFile()
  Access:JOBSOBF.ClearKey(jof:RefNumberKey)
  jof:RefNumber = job:Ref_Number
  If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
      !Found
      If jobe:OBFValidated = 0 And jof:Status <> 2 And jof:Status <> 3
          ! If this is no longer and OBF then delete the entry in OBF Processing (DBH: 14/03/2007)
          Relate:JOBSOBF.Delete(0)
      Else ! If jof:OBFValidated = 0 And jof:Status <> 2 And jof:Status <> 3
          If jof:HeadAccountNumber <> wob:HeadAccountNumber
              jof:HeadAccountNumber = wob:HeadAccountNumber
              Access:JOBSOBF.Update()
          End ! If jof:HeadAccountNumber <> wob:HeadAccountNumber
          If jof:DateCompleted <> job:Date_Completed
              jof:DateCompleted = job:Date_Completed
              jof:TimeCompleted = job:Time_Completed
              Access:JOBSOBF.Update()
          End ! If jof:DateCompleted <> job:Date_Completed
          If jof:IMEINumber <> job:ESN
              jof:IMEINumber = job:ESN
              Access:JOBSOBF.Update()
          End ! If jof:IMEINumber <> job:ESN
      End ! If jof:OBFValidated = 0 And jof:Status <> 2 And jof:Status <> 3
  Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
  
  Access:JOBSOBF.RestoreFile(Save_JOBSOBF_ID)
  Relate:JOBSOBF.Close()
  ! End (DBH 12/03/2007) #8718
  Release(JOBS)
  !Clear Preview Flag
  glo:Preview = ''
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCSTAT.Close
    Relate:AUDIT.Close
    Relate:COMMONCP.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:EXCHOR48.Close
    Relate:JOBEXACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:JOBSE_ALIAS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:MANSAMP.Close
    Relate:MANUDATE.Close
    Relate:NETWORKS.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:ORDWEBPR.Close
    Relate:PARTS_ALIAS.Close
    Relate:SMSMAIL.Close
    Relate:SMSText.Close
    Relate:STOCKALL.Close
    Relate:STOCKALX.Close
    Relate:STOFAULT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
    Relate:WAYBILLS.Close
    Relate:WAYITEMS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ================================================================
  ! Initialise Xml Export Object
    objXmlExport.FInit(fileXmlExport)
  ! ================================================================
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
      continue# = 1
      If job:model_number = ''
          Case Missive('You must select a Model Number before you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          continue# = 0
      Else !If job:model_number = ''
          If job:charge_type = '' And (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
              Case Missive('You must select a Charge Type before you can insert any parts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              continue# = 0
          End
          If job:warranty_charge_type = '' And continue# = 1 And (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
              Case Missive('You must select a Warranty Charge Type before you can insert any parts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              continue# = 0
          End
  
      End !If job:model_number = ''
  
      If job:date_completed <> ''
          ! Inserting (DBH 07/12/2007) # 4486 - Do not check the deletion of exchange part lines. This is covered elsewhere
          If (Field() = ?Delete and par:Part_Number <> 'EXCH') Or |
              (Field() = ?Delete:3 and wpr:Part_Number <> 'EXCH')
          ! End (DBH 07/12/2007) #4486
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case Missive('Cannot Insert / Delete parts.'&|
                    '<13,10>'&|
                    '<13,10>This job has been completed.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  continue# = 0
              End
          End ! (Field() = ?Delete:3 and wpr:Part_Number <> 'EXCH')
      End
  
      If continue# = 1
  
          If Field() = ?Insert
  
              glo:select11 = job:manufacturer
              glo:select12 = job:model_number
              If IsJobInvoiced(job:Invoice_Number)
                  Case Missive('You cannot insert parts onto a job that has been invoiced.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Access:PARTS.CancelAutoInc()
                  Return Level:User
              End !If job:Invoice_Number <> 0
  
  
          End !If Field() = ?Insert
          If Field() = ?Delete
              !Stock Order
              NoDelete# = 0
  
              ! Inserting (DBH 07/12/2007) # 4486 - Deletion of exchange units is handled elsewhere
              If par:Part_Number <> 'EXCH'
              ! End (DBH 07/12/2007) #4486
                  !Check were the part was attached so that
                  !only the site that attached it, can delete it. - L882 (DBH: 22-07-2003)
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = par:Part_Ref_Number
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                  End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                  If glo:WebJob
                      If sto:Location = tmp:ARCSiteLocation
                          Case Missive('Cannot delete! This part was added at the ARC.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          NoDelete# = 1
                      Else !If sto:Location = tmp:ARCSiteLocation
                          If IsJobInvoiced(job:Invoice_Number)
                              !Job has been invoiced.. check access
                              Case Missive('You cannot remove parts for a job that has been invoiced.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              NoDelete# = 1
                          End !If job:Invoice_Number <> 0
                      End !If sto:Location = tmp:ARCSiteLocation
                  Else !If glo:WebJob
                      !message('In delete code have relocate store = '&clip(Glo:RelocateStore)&'|Stock location = '&clip(sto:location)&'|ARCSite ='&clip(tmp:ARCSiteLocation))
                      If sto:Location <> tmp:ARCSiteLocation
                          if glo:RelocateStore then
                              if sto:Location <> 'MAIN STORE' then
                                miss# = Missive('Cannot delete. This part was added at the RRC.','ServiceBase 3g','mstop.jpg','/OK')
                                NoDelete# = 1
                              END
                          ELSE
                              Case Missive('Cannot delete! This part was added at the RRC.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              NoDelete# = 1
                          END
                      Else !If sto:LOcation <> tmp:ARCSiteLocation
                          If IsJobInvoiced(job:Invoice_Number)
                              !Job has been invoiced.. check access
                              Case Missive('You cannot remove parts for a job that has been invoiced.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              NoDelete# = 1
                          End !If job:Invoice_Number <> 0
                      End !If sto:LOcation <> tmp:ARCSiteLocation
                  End !If glo:WebJob
              End ! If par:Part_Number <> 'EXCH'
              If NoDelete# = 0
                  If DeleteChargeablePart(0)
                      Delete(PARTS)
                  End !If DeleteChargeablePart()
              End !If NoDelete# = 0
  
  
          End!If Field() = ?Delete
  
          If Field() = ?Insert:3
              glo:select11 = job:manufacturer
              glo:select12 = job:model_number
              If job:Invoice_Number_Warranty <> 0
                  Case Missive('You cannot insert parts onto a job that has been invoiced.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Access:WARPARTS.CancelAutoInc()
                  Return Level:User
              End !If job:Invoice_Number <> 0
  
          End !If Field() = ?Insert:3
          If Field() = ?Delete:3
              NoDelete# = 0
              If wpr:Part_Number <> 'EXCH'
                  !Do not delete Warranty parts from completed jobs - L945 (DBH: 03-09-2003)
                  If job:Date_Completed <> '' and wpr:Part_Number <> 'EXCH'
                      Case Missive('You cannot remove parts from a Completed warranty job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      NoDelete# = 1
                  Else !If job:Date_Completed <> ''
  
                      !Check were the part was attached so that
                      !only the site that attached it, can delete it. - L882 (DBH: 22-07-2003)
                          !Was using par:Part_Ref_Number in error - L873 (DBH: 17-09-2003)
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = wpr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                      If glo:WebJob
                          If sto:Location = tmp:ARCSiteLocation
                              Case Missive('Cannot delete! This part was added at the ARC.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              NoDelete# = 1
                          Else !If sto:Location = tmp:ARCSiteLocation
                              If job:Invoice_Number_Warranty <> 0
                                  Case Missive('You cannot remove parts for a job that has been invoiced.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  NoDelete# = 1
                              End !If job:Invoice_Number <> 0
                          End !If sto:Location = tmp:ARCSiteLocation
                      Else !If glo:WebJob
                          !message('In delete code (2) have relocate store = '&clip(Glo:RelocateStore)&'|Stock location = '&clip(sto:location)&'|ARCSite ='&clip(tmp:ARCSiteLocation))
                          If sto:Location <> tmp:ARCSiteLocation
  
                              if glo:RelocateStore then
                                  if sto:Location <> 'MAIN STORE' then
                                    miss# = Missive('Cannot delete. This part was added at the RRC.','ServiceBase 3g','mstop.jpg','/OK')
                                    NoDelete# = 1
                                  END
                              ELSE
                                  Case Missive('Cannot delete! This part was added at the RRC.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  NoDelete# = 1
                              END
  
                          Else !If sto:LOcation <> tmp:ARCSiteLocation
                              If job:Invoice_Number_Warranty <> 0
                                  !Job has been invoiced.. check access
                                  Case Missive('You cannot remove parts from a job that has been invoiced.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  NoDelete# = 1
                              End !If job:Invoice_Number <> 0
                          End  !If sto:LOcation <> tmp:ARCSiteLocation
                      End !If glo:WebJob
                  End !If job:Date_Completed <> ''
              End ! If wpr:Part_Number <> 'EXCH'
              If NoDelete# = 0
                  If DeleteWarrantyPart(0)
                      Delete(WARPARTS)
                  End !If DeleteWarrantyPart()
              End !If NoDelete# = 0
  
          End !If Field() = Delete:3
  
          If Field() = ?Insert:2
              If job:Estimate_Accepted = 'YES'
                  Case Missive('You cannot add an Estimate Part. This estimate has been accepted.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Access:ESTPARTS.CancelAutoInc()
                  Return Level:User
  
              End !If job:Estimate_Accepted = 'YES'
              If job:Estimate_Rejected = 'YES'
                  Case Missive('You cannot add an Estimate Part. This estimate has been rejected.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Access:ESTPARTS.CancelAutoInc()
                  Return Level:User
  
              End !If job:Estimate_Rejected = 'YES'
  
  
              glo:Select11 = job:Manufacturer
              glo:Select12 = job:Model_Number
          End !If Field() = ?Insert:2
  
          If Field() = ?Delete:2
              If job:Estimate_Accepted = 'YES'
                  Case Missive('You cannot delete the Estimate Part. This estimate has been accepted.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Return Level:User
              End !If job:Estimate_Accepted = 'YES'
              If job:Estimate_Rejected = 'YES'
                  Case Missive('You cannot delete the Estimate Part. This estimate has been rejected.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Return Level:User
              End !If job:Estimate_Rejected = 'YES'
              If DeleteEstimatePart(0)
                  Delete(ESTPARTS)
              End !If DeleteEstimatePart(0)
          End !If Field() = ?Delete:2
  
          If request <> 3
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
      PickNetworks
      Update_Parts
      Update_Warranty_Part
      UpdateEstimatePart
    END
    ReturnValue = GlobalResponse
  END
      End !If request <> 3
      !message('Returning from update_parts/war/est, have glo:select1 as '&clip(glo:Select1))
      If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
          !message('past the field() checks')
          If glo:select1 = 'NEW PENDING' Or glo:Select1 = 'NEW PENDING WEB'
              !message('New pending found')
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = par:part_ref_number
              If access:stock.fetch(sto:ref_number_key)
                  Case Missive('An error has occurred retreiving the details of this Stock Part.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If access:stock.fetch(sto:ref_number_key)
                  sto:quantity_stock     = 0
                  access:stock.update()
  
                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'DEC', | ! Transaction_Type
                                     par:Despatch_Note_Number, | ! Depatch_Note_Number
                                     job:Ref_Number, | ! Job_Number
                                     0, | ! Sales_Number
                                     sto:Quantity_stock, | ! Quantity
                                     par:Purchase_Cost, | ! Purchase_Cost
                                     par:Sale_Cost, | ! Sale_Cost
                                     par:Retail_Cost, | ! Retail_Cost
                                     'STOCK DECREMENTED', | ! Notes
                                     '') ! Information
                    ! Added OK
                  Else ! AddToStockHistory
                    ! Error
                  End ! AddToStockHistory
  
                  !message('Trying to fetch parts record by job ref no='&clip(Job:Ref_Number)&' part_ref_number ='&clip(glo:Select2))
                  access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                  par_ali:ref_number      = job:ref_number
                  par_ali:part_ref_number = glo:select2
                  If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                      !message('Parts record found - recreating')
                      par:ref_number           = par_ali:ref_number
                      par:adjustment           = par_ali:adjustment
                      par:part_ref_number      = par_ali:part_ref_number
  
                      par:part_number     = par_ali:part_number
                      par:description     = par_ali:description
                      par:supplier        = par_ali:supplier
                      par:purchase_cost   = par_ali:purchase_cost
                      par:sale_cost       = par_ali:sale_cost
                      par:retail_cost     = par_ali:retail_cost
                      par:quantity             = glo:select3
                      par:warranty_part        = 'NO'
                      par:exclude_from_order   = 'NO'
                      par:despatch_note_number = ''
                      par:date_ordered         = ''
                      par:date_received        = ''
                      par:pending_ref_number   = glo:select4
                      par:order_number         = ''
                      par:fault_code1    = par_ali:fault_code1
                      par:fault_code2    = par_ali:fault_code2
                      par:fault_code3    = par_ali:fault_code3
                      par:fault_code4    = par_ali:fault_code4
                      par:fault_code5    = par_ali:fault_code5
                      par:fault_code6    = par_ali:fault_code6
                      par:fault_code7    = par_ali:fault_code7
                      par:fault_code8    = par_ali:fault_code8
                      par:fault_code9    = par_ali:fault_code9
                      par:fault_code10   = par_ali:fault_code10
                      par:fault_code11   = par_ali:fault_code11
                      par:fault_code12   = par_ali:fault_code12
                      !Message('Before the parts.insert add stock allocation '&par:record_number)
                      access:parts.insert()
                      If glo:Select1 = 'NEW PENDING WEB' or glo:RelocateStore then
                          par:WebOrder = 1
                          AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                          Access:parts.update()
                      End !If glo:Select1 = 'NEW PENDING WEB'
  
                      !message('Inserted part')
                      !Message('AFTER the parts.insert add stock allocation '&par:record_number)
                  ELSE
                      !message('Part number not found')
                  end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
              end !if access:stock.fetch(sto:ref_number_key = Level:Benign
          End!If glo:select1 = 'NEW PENDING'
      End
  
      If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
          If glo:select1 = 'NEW PENDING' or glo:Select1= 'NEW PENDING WEB'
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = wpr:part_ref_number
              If access:stock.fetch(sto:ref_number_key)
                  Case Missive('An error has occurred retreiving the details of this Stock Part.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If access:stock.fetch(sto:ref_number_key)
                  sto:quantity_stock     = 0
                  access:stock.update()
  
                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'DEC', | ! Transaction_Type
                                     wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                     job:Ref_Number, | ! Job_Number
                                     0, | ! Sales_Number
                                     sto:Quantity_Stock, | ! Quantity
                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                     wpr:Sale_Cost, | ! Sale_Cost
                                     wpr:Retail_Cost, | ! Retail_Cost
                                     'STOCK DECREMENTED', | ! Notes
                                     '') ! Information
                    ! Added OK
                  Else ! AddToStockHistory
                    ! Error
                  End ! AddToStockHistory
  
                  access:warparts_alias.clearkey(war_ali:RefPartRefNoKey)
                  war_ali:ref_number      = job:ref_number
                  war_ali:part_ref_number = glo:select2
                  If access:warparts_alias.fetch(war_ali:RefPartRefNoKey) = Level:Benign
                      wpr:ref_number           = war_ali:ref_number
                      wpr:adjustment           = war_ali:adjustment
                      wpr:part_ref_number      = war_ali:part_ref_number
                      wpr:part_number     = war_ali:part_number
                      wpr:description     = war_ali:description
                      wpr:supplier        = war_ali:supplier
                      wpr:purchase_cost   = war_ali:purchase_cost
                      wpr:sale_cost       = war_ali:sale_cost
                      wpr:retail_cost     = war_ali:retail_cost
                      wpr:quantity             = glo:select3
                      wpr:warranty_part        = 'YES'
                      wpr:exclude_from_order   = 'NO'
                      wpr:despatch_note_number = ''
                      wpr:date_ordered         = ''
                      wpr:date_received        = ''
                      wpr:pending_ref_number   = glo:select4
                      wpr:order_number         = ''
                      wpr:fault_code1    = war_ali:fault_code1
                      wpr:fault_code2    = war_ali:fault_code2
                      wpr:fault_code3    = war_ali:fault_code3
                      wpr:fault_code4    = war_ali:fault_code4
                      wpr:fault_code5    = war_ali:fault_code5
                      wpr:fault_code6    = war_ali:fault_code6
                      wpr:fault_code7    = war_ali:fault_code7
                      wpr:fault_code8    = war_ali:fault_code8
                      wpr:fault_code9    = war_ali:fault_code9
                      wpr:fault_code10   = war_ali:fault_code10
                      wpr:fault_code11   = war_ali:fault_code11
                      wpr:fault_code12   = war_ali:fault_code12
                      !Message('Before the WARparts.insert add stock allocation '&wpr:record_number)
  
                      access:warparts.insert()
                      !Message('AFTER the WARparts.insert add stock allocation '&wpr:record_number)
                      If glo:Select1 = 'NEW PENDING WEB' or glo:RelocateStore then
                          AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'WEB',job:Engineer)
                          wpr:WebOrder = 1
                          Access:WarParts.update()
                      End !If glo:Select1 = 'NEW WEB PENDING'
                  end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
              end !if access:stock.fetch(sto:ref_number_key = Level:Benign
          End!If glo:select1 = 'NEW PENDING'
  
          If glo:Select1 = 'MAKE CHARGEABLE'
              !message('Have the make chargeable command')
              Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
              wpr:Record_Number    = glo:Select4
              If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Found
  
                  If Access:PARTS.PrimeRecord() = Level:Benign
                      RecordNumber#   = par:Record_Number
                      par:Record  :=: wpr:Record
                      par:Record_Number    = RecordNumber#
                      If Access:PARTS.TryInsert() = Level:Benign
                          !Insert Successful
                          !message('Part insert worked')
                          RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                          Relate:WARPARTS.Delete(0)
                      Else !If Access:PARTS.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:PARTS.CancelAutoInc()
                      End !If Access:PARTS.TryInsert() = Level:Benign
                  End !If Access:PARTS.PrimeRecord() = Level:Benign
              Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                  !Error
              End !If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
          End !If glo:Select1 = 'MAKE CHARGEABLE'
          If glo:Select1 = 'REMOVE PART'
              Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
              wpr:Record_Number    = glo:Select4
              If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Found
                  RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                  Relate:WARPARTS.Delete(0)
                  !message('Remove part worked')
              Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                  !Error
              End !If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
  
          End !If glo:Select1 = 'REMOVE PART'
  
          !This should stop the message coming up, if the Chargeble Job tick has been ticked
          !But it should still hide and unhide the fields.
          Warranty_Job_Temp = job:Warranty_Job
          Chargeable_Job_Temp = job:Chargeable_Job
          Post(Event:Accepted,?job:Chargeable_Job)
          Post(Event:Accepted,?job:Warranty_Job)
  
      End!If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
  
      Else!!If continue# = 1
          If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
              access:parts.cancelautoinc()
          End!If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
          If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
              access:warparts.cancelautoinc()
          End!If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
          If (Field() = ?Insert:2 Or Field() = ?Change:2 Or Field() = ?Delete:2)
              Access:ESTPARTS.CancelAutoInc()
          End !If (Field() = ?Insert:2 Or Field() = ?Change:2 Or Field() = ?Delete:2)
  
  
      End !If continue# = 1
  
      Clear(glo:G_Select1)
      Do check_parts
      Local.Pricing(0)
  
      If Request = DeleteRecord
          ?Cancel{prop:Disable} = 1
      End !If Request = DeleteRecord
  
  Do CountParts
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OptionsEstimate
      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0  !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0
      
          Do SetJOBSE
          Access:JOBSE.TryUpdate()
    OF ?OptionsAllocateLoan
      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0  !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0
      
          Do SetJOBSE
          Access:JOBSE.TryUpdate()
          !Save the Loan Unit Number to see if it changes - L902 (DBH: 30-07-2003)
          tmp:CurrentExchangeUnitNumber = job:Loan_Unit_Number
    OF ?OptionsAllocateExchange
      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0  !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0
      
      
          Do SetJOBSE
          Access:JOBSE.TryUpdate()
          tmp:CurrentExchangeUnitNumber = job:Exchange_Unit_Number
          tmp:CurrentSecondExchangeUnitNumber = jobe:SecondExchangeNumber
    OF ?BrowseAuditTrail
      glo:select12 = job:ref_number
    OF ?BrowseContactHistory
      glo:select12 = job:ref_number
    OF ?job:Chargeable_Job
      If ~0{prop:Acceptall}
          If job:Chargeable_Job <> Chargeable_Job_Temp
              If job:Chargeable_Job = 'YES'
                  !Making the job chargeable
                  If job:Warranty_Job = 'YES'
                      !Is this job Warranty also?
                      Case Missive('This job has already been marked as a Warranty Job. If you continue it will be changed to SPLIT Warranty / Chargeable.'&|
                        '<13,10>Do you want to SPLIT the job or make it CHARGEABLE or WARRANTY only?','ServiceBase 3g',|
                                     'mquest.jpg','Warranty|Chargeable|Split')
                          Of 3 ! Split Button
                              Chargeable_Job_Temp = job:Chargeable_Job
                          Of 2 ! Chargeable Button
                              !Transfer Warranty Parts
                              Save_wpr_ID = Access:WARPARTS.SaveFile()
                              Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                              wpr:Ref_Number  = job:Ref_Number
                              Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                              Loop
                                  If Access:WARPARTS.NEXT()
                                     Break
                                  End !If
                                  If wpr:Ref_Number  <> job:Ref_Number      |
                                      Then Break.  ! End If
                                  If Access:PARTS.PrimeRecord() = Level:Benign
                                      RecordNumber# = par:Record_Number
                                      par:Record :=: wpr:Record
                                      par:Record_Number    = RecordNumber#
                                      If Access:PARTS.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:PARTS.TryInsert() = Level:Benign
                                          !Insert Failed
                                          Access:PARTS.CancelAutoInc()
                                      End !If Access:PARTS.TryInsert() = Level:Benign
                                  End !If Access:PARTS.PrimeRecord() = Level:Benign
                                  !Is this part in Stock Allocation? If so, change it to chargeable - L873 (DBH: 30-07-2003)
                                  Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
                                  stl:PartType         = 'WAR'
                                  stl:PartRecordNumber = wpr:Record_Number
                                  If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                      !Found
                                      stl:PartRecordNumber = par:Record_Number
                                      stl:PartType = 'CHA'
                                      Access:STOCKALL.Update()
                                  Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                      !Error
                                  End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                  Delete(WARPARTS)
                              End !Loop
                              Access:WARPARTS.RestoreFile(Save_wpr_ID)
                              !Disable Cancel button to make sure
                              !the changes are saved - L868 (DBH: 24-07-2003)
                              ?Cancel{prop:Disable} = 1
      
                              Chargeable_Job_Temp = job:Chargeable_Job
                              job:Warranty_Job = 'NO'
                              Warranty_job_Temp = 'NO'
                          Of 1 ! Warranty Button
                              job:Chargeable_Job = 'NO'
                      End ! Case Missive
                      brw9.resetsort(1)
                      brw10.resetsort(1)
                  Else !If job:Warranty_Job = 'YES'
                      Chargeable_job_Temp = job:Chargeable_Job
                  End !If job:Warranty_Job = 'YES'
              Else !If job:Chargeable_Job = 'YES'
                  !Turning Chartgeable Off. Check for parts
                  found# = 0
                  setcursor(cursor:wait)
                  save_par_id = access:parts.savefile()
                  access:parts.clearkey(par:part_number_key)
                  par:ref_number  = job:ref_number
                  set(par:part_number_key,par:part_number_key)
                  loop
                      if access:parts.next()
                         break
                      end !if
                      if par:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      found# = 1
                      Break
                  end !loop
                  access:parts.restorefile(save_par_id)
                  setcursor()
      
                  If job:estimate = 'YES'
                      found# = 2
                  End
      
                  Case found#
                      Of 1    !Parts
                          Case Missive('Chargeable Parts have been added to this job.'&|
                            '<13,10>'&|
                            '<13,10>You must remove these before you can mark this job as Warranty Only.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          job:chargeable_job  = chargeable_job_temp
      
                      Of 2    !Estimate
                          Case Missive('This job has been marked as an Estimate.'&|
                            '<13,10>'&|
                            '<13,10>You cannot make this a Warranty Only Job.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          job:chargeable_job  = chargeable_job_temp
                  Else !Case found#
                      chargeable_job_temp = job:chargeable_job
                  End !If found# <> 0
      
              End !If job:Chargeable_Job = 'YES'
          End !If job:Chargeable_Job <> Chargeable_Job_Temp
          brw9.resetsort(1)
          brw10.resetsort(1)
      
      End !0{prop:Acceptall}
      DO enable_Disable
      
    OF ?job:Warranty_Job
      If ~0{prop:Acceptall}
          If job:Warranty_Job <> Warranty_Job_Temp
              If job:Warranty_Job = 'YES'
                  !Making the job Warranty
                  If job:Chargeable_Job = 'YES'
                      !Is this job Chargeable also?
                      Case Missive('This job has already been marked as a Chargeable Job.'&|
                        '<13,10>If you continue it will be changed to a SPLIT Chargeable / Warranty job.'&|
                        '<13,10>'&|
                        '<13,10>Do you want to SPLIT the job or make it Chargeable or Warranty only?','ServiceBase 3g',|
                                     'mquest.jpg','Warranty|Chargeable|Split')
                          Of 3 ! Split Button
                              Warranty_Job_Temp = job:Warranty_Job
                          Of 2 ! Chargeable Button
                              job:Warranty_Job = 'NO'
                          Of 1 ! Warranty Button
                              If job:Estimate = 'YES'
                                  Case Missive('This job has been marked as an Estimate.'&|
                                    '<13,10>'&|
                                    '<13,10>You cannot make it a Warranty Only job.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  job:Warranty_Job = 'NO'
                              Else !If job:Estimate = 'YES'
                                  !Transfer Warranty Parts
                                  Save_par_ID = Access:PARTS.SaveFile()
                                  Access:PARTS.ClearKey(par:Part_Number_Key)
                                  par:Ref_Number  = job:Ref_Number
                                  Set(par:Part_Number_Key,par:Part_Number_Key)
                                  Loop
                                      If Access:PARTS.NEXT()
                                         Break
                                      End !If
                                      If par:Ref_Number  <> job:Ref_Number      |
                                          Then Break.  ! End If
                                      If Access:WARPARTS.PrimeRecord() = Level:Benign
                                          RecordNumber# = wpr:Record_Number
                                          wpr:Record :=: par:Record
                                          wpr:Record_Number    = RecordNumber#
                                          If Access:WARPARTS.TryInsert() = Level:Benign
                                              !Insert Successful
                                          Else !If Access:PARTS.TryInsert() = Level:Benign
                                              !Insert Failed
                                              Access:WARPARTS.CancelAutoInc()
                                          End !If Access:PARTS.TryInsert() = Level:Benign
                                      End !If Access:PARTS.PrimeRecord() = Level:Benign
                                      !Is this part in Stock Allocation? If so, change it to chargeable - L873 (DBH: 30-07-2003)
                                      Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
                                      stl:PartType         = 'CHA'
                                      stl:PartRecordNumber = par:Record_Number
                                      If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                          !Found
                                          stl:PartRecordNumber = wpr:Record_Number
                                          stl:PartType = 'WAR'
                                          Access:STOCKALL.Update()
                                      Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                          !Error
                                      End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
      
                                      !Added the Warranty Part, Delete the Chargeable Part
                                      Delete(PARTS)
                                  End !Loop
                                  Access:PARTS.RestoreFile(Save_par_ID)
                                  !Disable Cancel button to make sure
                                  !the changes are saved - L868 (DBH: 24-07-2003)
                                  ?Cancel{prop:Disable} = 1
      
                                  Warranty_Job_Temp = job:Warranty_Job
                                  job:Chargeable_Job = 'NO'
                                  Chargeable_Job_temp = 'NO'
                              End !If job:Estimate = 'YES'
                      End ! Case Missive
                      brw9.resetsort(1)
                      brw10.resetsort(1)
                  Else !If job:Warranty_Job = 'YES'
                      Warranty_job_Temp = job:Warranty_Job
                  End !If job:Warranty_Job = 'YES'
              Else !If job:Warranty_Job = 'YES'
                  !Turning Chartgeable Off. Check for parts
                  found# = 0
                  setcursor(cursor:wait)
                  save_wpr_id = access:warparts.savefile()
                  access:warparts.clearkey(wpr:part_number_key)
                  wpr:ref_number  = job:ref_number
                  set(wpr:part_number_key,wpr:part_number_key)
                  loop
                      if access:warparts.next()
                         break
                      end !if
                      if wpr:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      found# = 1
                      Break
                  end !loop
                  access:warparts.restorefile(save_wpr_id)
                  setcursor()
      
                  !Transfer the parts when trying to make Warranty To Chargeable - L948 (DBH: 16-09-2003)
                  Case found#
                      Of 1    !Parts
                          Case Missive('You are trying to make this job Chargeable Only, but there are still Warranty parts attached.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to transfer these Warranty parts to Chargeable?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  !Transfer Warranty Parts
                                  Save_wpr_ID = Access:WARPARTS.SaveFile()
                                  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                                  wpr:Ref_Number  = job:Ref_Number
                                  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                                  Loop
                                      If Access:WARPARTS.NEXT()
                                         Break
                                      End !If
                                      If wpr:Ref_Number  <> job:Ref_Number      |
                                          Then Break.  ! End If
                                      If Access:PARTS.PrimeRecord() = Level:Benign
                                          RecordNumber# = par:Record_Number
                                          par:Record :=: wpr:Record
                                          par:Record_Number    = RecordNumber#
                                          If Access:PARTS.TryInsert() = Level:Benign
                                              !Insert Successful
                                          Else !If Access:PARTS.TryInsert() = Level:Benign
                                              !Insert Failed
                                              Access:PARTS.CancelAutoInc()
                                          End !If Access:PARTS.TryInsert() = Level:Benign
                                      End !If Access:PARTS.PrimeRecord() = Level:Benign
                                      !Is this part in Stock Allocation? If so, change it to chargeable - L873 (DBH: 30-07-2003)
                                      Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
                                      stl:PartType         = 'WAR'
                                      stl:PartRecordNumber = wpr:Record_Number
                                      If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                          !Found
                                          stl:PartRecordNumber = par:Record_Number
                                          stl:PartType = 'CHA'
                                          Access:STOCKALL.Update()
                                      Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                          !Error
                                      End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
                                      Delete(WARPARTS)
                                  End !Loop
                                  Access:WARPARTS.RestoreFile(Save_wpr_ID)
                                  !Disable Cancel button to make sure
                                  !the changes are saved - L868 (DBH: 24-07-2003)
                                  ?Cancel{prop:Disable} = 1
                                  job:Chargeable_Job = 'YES'
                                  Chargeable_Job_Temp = job:Chargeable_Job
                                  job:Warranty_Job = 'NO'
                                  Warranty_job_Temp = 'NO'
      
                              Of 1 ! No Button
                                  job:Warranty_Job = Warranty_Job_Temp
                          End ! Case Missive
                  Else !Case found#
                      Warranty_job_temp = job:Warranty_job
                  End !If found# <> 0
      
              End !If job:Warranty_Job = 'YES'
      
              If job:Warranty_Job = 'YES' ! Check again, may no longer be warranty job
                  do ValidateDateCode
              End
      
          End !If job:Warranty_Job <> Warranty_Job_Temp
          brw9.resetsort(1)
          brw10.resetsort(1)
      
      End !0{prop:Acceptall}
      DO enable_Disable
    OF ?tmp:HubRepair
      If ~0{prop:AcceptAll}
          Error# = 0
      
          If tmp:HubRepair <> tmp:SaveHubRepair
              If job:Engineer = ''
                  Case Missive('You must allocate an Engineer to this job.', 'ServiceBase 3g', |
                               'mstop.jpg', '/OK')
                  Of 1 ! OK Button
                  End ! Case Missive
                  Error# = 1
              Else ! job:Engineer = ''
      
                  Case tmp:HubRepair
                  Of 0
                      Case Missive('You have de-selected "Hub Repair". This will credit this job to a RRC.' &|
                                   '<13,10>' &|
                                   '<13,10>Are you sure you want to continue?', 'ServiceBase 3g', |
                                   'mquest.jpg', '\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          Error# = 1
                      End ! Case Missive
                  Of 1
                      If ?Prompt:LiquidDamage{prop:Hide} = False
                          If glo:WebJob
                              Case Missive('Error! Cannot sent to ARC.'&|
                                '|This unit is Liquid Damaged.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Error# = 1
                          Else ! If glo:WebJob
                              Case Missive('Error! Cannot sent to ARC.'&|
                                '|This unit is Liquid Damaged.'&|
                                '|Do you wish to override?','ServiceBase 3g',|
                                             'mexclam.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                      Access:USERS.Clearkey(use:User_Code_Key)
                                      use:User_Code   = InsertEngineerPassword()
                                      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
                                          acc:User_Level  = use:User_Level
                                          acc:Access_Area = 'LIQUID DAMAGE ARC OVERRIDE'
                                          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
                                              !Found
                                              If AddToAudit(job:Ref_Number,'JOB','LIQUID DAMAGE OVERRIDE','HUB REPAIR SELECTED')
      
                                              End ! If AddToAudit(job:Ref_Number,'JOB','LIQUID DAMAGE OVERRIDE','HUB REPAIR SELECTED')
                                          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
                                              !Error
                                              Case Missive('You do not have sufficient access to amend this option.','ServiceBase 3g',|
                                                             'mstop.jpg','/OK')
                                                  Of 1 ! OK Button
                                              End ! Case Missive
                                              Error# = 1
                                          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      
                                      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
                                  Of 1 ! No Button
                                      Error# = 1
                              End ! Case Missive
                          End ! If glo:WebJob
                      End ! If ?Prompt:LiquidDamage{prop:Hide} = False
                      If Error# = 0
                          Case Missive('You have selected "Hub Repair". This will include this job in the next waybill generated for the ARC.' &|
                                       '<13,10>Are you sure you want to continue?', 'ServiceBase 3g', |
                                       'mquest.jpg', '\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Error# = 1
                          End ! Case Missive
      
                          do CountParts
                          if PartsCounter <> 0
                              Case Missive('This unit has parts attached you should remove them before sending it to the ARC.', 'ServiceBase 3g', |
                                           'mstop.jpg', '/OK')
                              Of 1 ! OK Button
                              End ! Case Missive
                              Error# = 1
                          Else
                              If WarrantyPartsCounter <> 0
                                      ! Always to keep the parts IF it's warranty and has an exchange.
                                      ! Ths should mean it's a RF SWap (hopefully)
                                  If job:Exchange_Unit_Number <> 0 And job:Warranty_Job = 'YES'
                                      Case Missive('Warning! This job has Warranty Parts attached.' &|
                                                   '<13,10>' &|
                                                   '<13,10>Do you wish to keep the parts attached to the job when it is sent to the ARC?', 'ServiceBase 3g', |
                                                   'mquest.jpg', '\No|/Yes')
                                      Of 2 ! Yes Button
                                      Of 1 ! No Button
                                      End ! Case Missive
                                      Error# = 1
                                  Else
                                      Case Missive('This unit has parts attached. You should remove them before sending it to the ARC.', 'ServiceBase 3g', |
                                                   'mstop.jpg', '/OK')
                                      Of 1 ! OK Button
                                      End ! Case Missive
                                      Error# = 1
                                  End ! If job:Exchange_Unit_Number <> 0 And job:Warranty_Job = 'YES'
                              End! If WarrantyPartsCounter <> 0
                          end ! if
                      End ! If Error# = 0
                  End ! Case tmp:HubRepair
              End ! job:Engineer = ''
              If Error#
                  tmp:HubRepair = tmp:SaveHubRepair
              Else ! If Error#
                  If tmp:HubRepair
                      tmp:HubRepairDate = Today()
                      tmp:HubRepairTime = Clock()
                      tmp:SaveHubRepair = tmp:HubRepair
                      stanum#           = Sub(tmp:StatusSendToARC, 1, 3)
                      GetStatus(stanum#, 0, 'JOB')
      
      
                      if glo:WebJob
                          ! - 3973 (DBH: 01-03-2004)
                          Do RRCHubRepairDisableScreen
      
                          ! change 28/11 28/11 ref:G152/L321 to add update of repair type
                          ! if there is a Handling Fee repair type use it
                          ! Do not overwrite it repair type is already filled in or it's an exchange unit
                          If job:Exchange_unit_Number = ''
                              access:reptydef.clearkey(rtd:ManRepairTypeKey)
                              rtd:Manufacturer = job:manufacturer
                              set(rtd:ManRepairTypeKey, rtd:ManRepairTypeKey)
                              loop
                                  if access:reptydef.next() then break.
                                  if rtd:Manufacturer <> job:manufacturer then break.
                                  if rtd:BER = 11 then
                                      ! we have an exchange fee
                                      if (job:Chargeable_Job = 'YES' and job:Repair_Type = '') then job:Repair_Type = rtd:Repair_Type.
                                      if (job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = '') |
                                          then job:Repair_Type_Warranty = rtd:Repair_Type
                                      end ! if
                                      break
                                  end ! if ber = 10
                              end! loop
      
                          End ! If job:Exchange_unit_Number = ''
      
                          ! Lookup current engineer and mark as HUB
                          Save_joe_ID = Access:JOBSENG.SaveFile()
                          Access:JOBSENG.ClearKey(joe:UserCodeKey)
                          joe:JobNumber     = job:Ref_Number
                          joe:UserCode      = job:Engineer
                          joe:DateAllocated = Today()
                          Set(joe:UserCodeKey, joe:UserCodeKey)
                          Loop
                              If Access:JOBSENG.PREVIOUS()
                                  Break
                              End ! If
                              If joe:JobNumber     <> job:Ref_Number   |
                                  Or joe:UserCode      <> job:Engineer |
                                  Or joe:DateAllocated > Today()       |
                                  Then Break   ! End If
                              End ! If
                              joe:Status     = 'HUB'
                              joe:StatusDate = Today()
                              joe:StatusTime = Clock()
                              Access:JOBSENG.Update()
                              Break
                          End ! Loop
                          Access:JOBSENG.RestoreFile(Save_joe_ID)
      
                          Case Missive('Your changes will now be saved.', 'ServiceBase 3g', |
                                       'mexclam.jpg', '/OK')
                          Of 1 ! OK Button
                          End ! Case Missive
      
                          ! Inserting (DBH 11/05/2006) #7597 - Add Email / SMS Alert
                          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                          jobe2:RefNumber = job:Ref_Number
                          If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                              ! Found
!                              If jobe2:SMSNotification
!                                  ! Insert --- If it's a VCP job, show the VCP account rather then RRC (DBH: 03/02/2010) #11014
!
!                                  if (job:who_Booked = 'WEB')
!                                      !AddEmailSMS(job:Ref_Number,job:Account_Number,'2ARC','SMS',jobe2:SMSAlertNumber,'',0,'')
!                                      !Send SMS text is now covered by change of status - this gives a duplicate
!                                      SendSMSText('J','Y','N')
!                                      if glo:ErrorText[1:5] = 'ERROR' then
!                                          miss# = missive('(1)Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!      
!                                      ELSE
!                                          !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                                          miss# = missive('(1)An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!                                      END 
!      
!      
!                                  ! end --- (DBH: 03/02/2010) #11014
!                                  else ! if (job:who_Booked = 'WEB')
!                                      !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ARC','SMS',jobe2:SMSAlertNumber,'',0,'')
!                                      SendSMSText('J','Y','N')
!                                      if glo:ErrorText[1:5] = 'ERROR' then
!      
!                                          miss# = missive('(3)Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!      
!                                      ELSE
!                                          !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                                          miss# = missive('(3)An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!                                      END 
!      
!                                  end ! if (job:who_Booked = 'WEB')
!      
!                              End ! If jobe2:SMSNotification

                              If jobe2:EmailNotification
                                  ! Insert --- If it's a VCP job, show the VCP acccount rather than RRC (DBH: 03/02/2010) #11014
                                  if (job:who_Booked = 'WEB')
                                      AddEmailSMS(job:Ref_Number,job:Account_Number,'2ARC','EMAIL','',jobe2:EmailAlertAddress,0,'')
      
                                  ! end --- (DBH: 03/02/2010) #11014
                                  else ! if (job:who_Booked = 'WEB')
                                      AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ARC','EMAIL','',jobe2:EmailAlertAddress,0,'')
                                  end ! if (job:who_Booked = 'WEB')
                              End ! If jobe2:EmailNotification
                          Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                              ! Error
                          End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                          ! End (DBH 11/05/2006) #7597
      
                          Post(Event:Accepted, ?OK)
      
                      ! end additions 28/11 ref:G152/L321
                      end! If glo:webjob
                  Else
                      tmp:HubRepairDate = ''
                      tmp:HubRepairTime = ''
                      ! set status to previous status and change location to RRC default
      
                      job:location = tmp:RRCLocation
                      stanum#      = Sub(job:previousStatus, 1, 3)
                      GetStatus(stanum#, 0, 'JOB')
      
                      if glo:WebJob
                          ! - 3973 (DBH: 01-03-2004)
                          Do RRCHubRepairDisableScreen
      
                          ! Lookup current engineer and mark as Allocated
                          Save_joe_ID = Access:JOBSENG.SaveFile()
                          Access:JOBSENG.ClearKey(joe:UserCodeKey)
                          joe:JobNumber     = job:Ref_Number
                          joe:UserCode      = job:Engineer
                          joe:DateAllocated = Today()
                          Set(joe:UserCodeKey, joe:UserCodeKey)
                          Loop
                              If Access:JOBSENG.PREVIOUS()
                                  Break
                              End ! If
                              If joe:JobNumber     <> job:Ref_Number   |
                                  Or joe:UserCode      <> job:Engineer |
                                  Or joe:DateAllocated > Today()       |
                                  Then Break   ! End If
                              End ! If
                              If joe:Status = 'HUB'
                                  joe:Status     = 'ALLOCATED'
                                  joe:StatusDate = Today()
                                  joe:StatusTime = Clock()
                                  Access:JOBSENG.Update()
                              End ! If joe:Status = 'HUB'
                              Break
                          End ! Loop
                          Access:JOBSENG.RestoreFile(Save_joe_ID)
      
      
                          ?Cancel{prop:Disable} = 1
                          Case Missive('Your changes will now be saved.', 'ServiceBase 3g', |
                                       'mexclam.jpg', '/OK')
                          Of 1 ! OK Button
                          End ! Case Missive
                          Post(Event:Accepted, ?OK)
                      end ! if
                  End ! If tmp:HubRepair
              End ! If Error# = 0
          End ! If tmp:HubRepair <> tmp:SavedHubRepair
      End ! 0{prop:AcceptAll}
      
      if ?CompleteButton{prop:Disable} = False
          do DisplayCompleteButton
      end ! if
      
      Display()
    OF ?tmp:OBFvalidated
      If ~0{prop:AcceptAll}
          Error# = 0
      
          If tmp:OBFvalidated <> tmp:SaveOBFvalidated
              Case tmp:OBFvalidated
                  Of 0
                      Case Missive('De-selecting Out Of Box Failure will change the status of this job.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure you want to continue','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Error# = 1
                      End ! Case Missive
                  Of 1
                      Case Missive('Validating Out Of Box Failure will change the status of this job.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Error# = 1
                      End ! Case Missive
      
              End !Case tmp:OBFvalidated
              If Error#
                  tmp:OBFvalidated = tmp:SaveOBFvalidated
              End !If Error# = 0
          End !If tmp:HubRepair <> tmp:SavedHubRepair
      
          If tmp:OBFvalidated
              tmp:OBFvalidateDate = Today()
              tmp:OBFvalidateTime = Clock()
              ?AuthorityNoPrompt{prop:hide}=0
              ?job:Authority_Number{prop:hide}=0
          Else
              tmp:OBFvalidateDate = ''
              tmp:OBFvalidateTime = ''
              if def:Hide_Authority_Number = 'YES'
                  ?AuthorityNoPrompt{prop:hide}=1
                  ?job:Authority_Number{prop:hide}=1
              END !if def hide authority number
          End !If tmp:HubRepair
      
      End !0{prop:AcceptAll}
      Display()
    OF ?ChangeJobStatus
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
      
          Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
          End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      Else !job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
      
          Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
          End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      End !job:engineer <> ''
      
      glo:Select1 = use:User_Level
    OF ?Button:ContactHistoryEmpty
      glo:select12    = job:ref_number
    OF ?ValidatePOP
      If SecurityCheck('JOBS - AMEND POP DETAILS')
          Case Missive('You do not have sufficient access to amend this details.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          !Validate Proof OF Purchase
          Glo:Select1 = job:pop
          Glo:Select2 = job:dop
      
          Error# = 0
          ! Check access level to determine if password screen should appear - TrkBs: 6685 (DBH: 11-11-2005)
          If SecurityCheck('VALIDATE POP WITHOUT PASSWORD')
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Found
                  UserCode" = use:User_Code
                  If InsertEngineerPassword() <> UserCode"
                      Case Missive('Incorrect password.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End ! If InsertEngineerPassword() <> UserCode"
              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Error
              End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          End ! If SecurityCheck('VALIDATE POP WITHOUT PASSWORD')
      
          If Error# = 0
              Do Validate_Motorola
          End ! If Error# = 0
      
          Glo:Select1 = ''
          Glo:Select2 = ''
      END
    OF ?Costs
      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0
          If tmp:Engineer48HourOption = 0   !TB13298 - remove reliance on Booking Option - always error on Engineer Option
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0
      
      
      Do CallViewCosts
      
    OF ?Button:ContactHistoryFilled
      glo:Select12 = job:Ref_Number
    OF ?ViewAccessories
      Do SetJOBSE
    OF ?Delete:2
      !Added By Paul - 01/10/2009 - log no - 11060
      !check to see if its a 'PINK' part before we delete it
      If tmp:EColourFlag = 7 then
          !we need to check the access level before we continue
          If SecurityCheck('JOB - DELETE PART AW RETURN') = Level:Benign
              !access level is present
              !so continue
          Else
              !access level is missing
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
              of 1
              End
              cycle
          End !SecurityCheck('RAPID ENG - CREATE INVOICE') = Level:Benign
      
      End !If tmp:WColourFlag = 7 then
      !End Change
    OF ?Change
      !look out for an exchange part
      if par:Part_Number = 'EXCH' then
          If FullAccess(glo:PassAccount,glo:Password) <> Level:Benign
              Case Missive('You cannot edit an exchange unit.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              cycle
         END !If full access
      END
    OF ?Delete
      !Added By Paul - 01/10/2009 - log no - 11060
      !check to see if its a 'PINK' part before we delete it
      If tmp:CColourFlag = 7 then
          !we need to check the access level before we continue
          If SecurityCheck('JOB - DELETE PART AW RETURN') = Level:Benign
              !access level is present
              !so continue
          Else
              !access level is missing
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
              of 1
              End
              cycle
          End !SecurityCheck('RAPID ENG - CREATE INVOICE') = Level:Benign
      
      End !If tmp:WColourFlag = 7 then
      !End Change
      
      !look out for an exchange part
      CallExchangeOrder# = False
      
      Access:PARTS.Clearkey(par:RecordNumberKey)
      par:Record_Number   = brw9.q.par:Record_Number
      If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
          !Found
      
      Else ! If Access:WARPARTS.Tryfetch(RecordNumberKey) = Level:Benign
          !Error
          Cycle
      
      End !If Access:WARPARTS.Tryfetch(RecordNumberKey) = Level:Benign
      
      
      If Local.CorrectEngineer() = False
          Cycle
      End !Local.CorrectEngineer() = False
      
      if par:Part_Number = 'EXCH' then
          ! Inserting (DBH 07/12/2007) # 4486 - Do not allow to delete exchanges on completed jobs without access level
          If SecurityCheck('AMEND EXCHANGE ON COMPLETE JOB') And job:Date_Completed <> ''
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If SecurityAccess('AMEND COMPLETED EXCHANGE')
          ! End (DBH 07/12/2007) #4486
      
          If glo:WebJob = False
              !Check to see if it's a Second Exchange Unit
              !if not, the ARC cannot delete RRC exchanges -  (DBH: 17-12-2003)
              If par:SecondExchangeUnit = False
                  if jobe:ExchangedATRRC = true
                      Case Missive('This unit cannot be deleted because it was exchanged at the RRC.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  end
              End !If wpr:SecondExchangeUnit = False
          Else !If glo:WebJob = False
              If par:SecondExchangeUnit = True
                  Case Missive('This part is for the Second Exchange Unit and can only be deleted at the ARC.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End !If wpr:SecondExchangeUnit = True
          End !If glo:WebJob = False
      
          Case Missive('Are you sure you want to remove an allocated exchange unit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  If par:SecondExchangeUnit = True
                      If jobe:SecondExchangeNumber <> 0
                          If par:PartAllocated = True
                              PartAllocatedFlag# = 1
      
                              Access:USERS.Clearkey(use:User_Code_Key)
                              use:User_Code   = job:Engineer
                              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found
      
                              Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                              End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                              Access:LOCATION.Clearkey(loc:Location_Key)
                              loc:Location    = use:Location
                              If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                                  !Found
      
                              Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                                  !Error
                              End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                              If loc:UseRapidStock = True
                                  par:Status = 'RTS'
                                  par:PartAllocated = 0
                                  Access:PARTS.Update()
                                  AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'RET',job:Engineer)
                              Else !If loc:UseRapidStock = True
                                  !Only call the Exchange Unit Screen if there 'is' an exchange unit attached -  (DBH: 06-01-2004)
                                  If jobe:SecondExchangeNumber <> 0
                                      tmp:CurrentExchangeUnitNumber = jobe:SecondExchangeNumber
                                      ViewExchangeUnit()
                                      If tmp:CurrentExchangeUnitNumber <> jobe:SecondExchangeNumber
                                          !Something about the exchange has changed. Save it -  (DBH: 06-01-2004)
                                          Access:JOBS.Update()
                                          ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
                                          UpdateDateTimeStamp(job:Ref_Number)
                                          ! End (DBH 16/09/2008) #10253
                                          ?Cancel{prop:Disable} = 1
                                      End !If tmp:CurrentExchangeUnitNumber <> jobe:SecondExchangeNumber
                                      If jobe:SecondExchangeNumber <> 0
                                          Cycle
                                      End !If jobe:SecondExchangeNumber <> 0
                                  End !If jobe:SecondExchangeNumber <> 0
                              End !If loc:UseRapidStock = True
                          End !If par:PartAllocated = True
                      End !If jobe:SecondExchangeNumber <> 0
                  Else !If par:SecondExchangeUnit = True
                      If job:Exchange_Unit_Number = 0
                          If tmp:Engineer48HourOption = 1
                              !Is there an exchange unit on the way? -  (DBH: 27-11-2003)
                              If Is48HourOrderCreated(tmp:CurrentSiteLocation,job:Ref_Number) = True
                                  Case Missive('A 48 Hour Exchange Unit has been ordered for this job.'&|
                                    '<13,10>'&|
                                    '<13,10>Are you sure you want to delete this order request?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                          Relate:EXCHOR48.Delete(0)
                                      Of 1 ! No Button
                                          Cycle
                                  End ! Case Missive
                              End !Is48HourOrderCreated(tmp:CurrentSiteLocation,job:RefNumber) = True
      
                              If Is48HourOrderProcessed(tmp:CurrentSiteLocation,job:Ref_Number) = True
                                  Case Missive('The exchange unit that was requested for this job has already been despatched from National Stores.'&|
                                    '<13,10>'&|
                                    '<13,10>You must receive this unit from Rapid Exchange Allocation before you can remove it.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Cycle
                              End !Is48HourOrderProcessed(tmp:CurrentSiteLocation,job:Ref_Number) = True
      
                          End !If tmp:Engineer48HourOption = 1
                          !There is no exchange unit attached, therefore must be on order
                          GetStatus(101,0,'EXC')
                          stanum# = Sub(job:previousStatus,1,3)
                          GetStatus(stanum#,0,'JOB')
                      Else !If job:Exchange_Unit_Number = 0
                          !get full record and mark it awaiting return
                          access:parts.clearkey(par:Part_Number_Key)
                          par:Ref_Number = job:ref_number
                          par:Part_Number = 'EXCH'
                          if access:parts.fetch(par:Part_NUmber_Key) = level:benign
                              !has this been allocated?
                              if par:PartAllocated = 1 then
                                  If tmp:Engineer48HourOption = 1
                                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                      xch:Ref_Number  = job:Exchange_Unit_Number
                                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                          !Found
                                      Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                          !Error
                                      End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
                                      If DeleteExchangeReason(xch:ESN)
                                          !Create a new 48 order and either mark it
                                          !as order required or not -  (DBH: 03-11-2003)
                                          Return# =  Missive('Would you like to request a replacement for the Exchange Unit you are deleting?','ServiceBase 3g',|
                                                         'mquest.jpg','\No|/Yes')
                                          !Does an entry already exist for this job?
                                          !If so, change the exchange unit to match the unit on this job]# -  (DBH: 27-11-2003)
                                          Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
                                          ex4:Received  = 0
                                          ex4:Returning = 1
                                          ex4:Location  = tmp:CurrentSiteLocation
                                          ex4:JobNumber = job:Ref_Number
                                          If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                                              !Found
                                              ex4:OrderUnitNumber = job:Exchange_Unit_Number
                                              ex4:Notes           = Clip(ex4:Notes) & '<13,10>' & Clip(Upper(glo:EDI_Reason))
                                              Access:EXCHOR48.Update()
                                          Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                                              !Error
                                              If Access:EXCHOR48.PrimeRecord() = Level:Benign
                                                  ex4:Location        = tmp:CurrentSiteLocation
                                                  ex4:Manufacturer    = job:Manufacturer
                                                  ex4:ModelNumber     = job:Model_Number
                                                  ex4:Received        = 0
                                                  ex4:Notes           = Clip(Upper(glo:EDI_Reason))
                                                  ex4:JobNumber       = job:Ref_Number
                                                  ex4:OrderUnitNumber = job:Exchange_Unit_Number
                                                  ex4:Returning       = 1
                                                  ex4:AttachedToJob   = 0
                                                  If Access:EXCHOR48.TryInsert() = Level:Benign
                                                      !Insert Successful
      
                                                  Else !If Access:EXCHOR48.TryInsert() = Level:Benign
                                                      !Insert Failed
                                                  End !If Access:EXCHOR48.TryInsert() = Level:Benign
      
      
                                              End !If Access:EXCHOR48.PrimeRecord() = Level:Benign
      
                                          End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
      
                                          locAuditNotes = ''
                                          Case Return#
                                              Of 1
                                                  locAuditNotes           = 'REPLACEMENT REQUESTED'
                                                  CallExchangeOrder# = True
                                              Of 2
                                                  locAuditNotes           = 'NO REPLACEMENT REQUESTED'
                                          End !Case Return#
                                          locAuditNotes = Clip(locAuditNotes) & '<13,10>UNIT NO: ' & job:Exchange_Unit_Number & |
                                                              '<13,10>REASON: ' & Clip(Upper(glo:EDI_Reason))
      
                                          IF (AddToAudit(job:Ref_Number,'JOB','48 HOUR EXCHANGE UNIT REMOVED FROM JOB',locAuditNotes))
                                          END ! IF
                                          Local.PrintReturningWaybill(job:Exchange_Unit_Number)
      
                                      Else !If DeleteExchangeReason(xch:ESN)
                                          Cycle
                                      End !If DeleteExchangeResaon(xch:ESN)
      
                                  End !If tmp:Engineer48HourOption = 1
      
                                  PartAllocatedFlag# = 1  !noted for later use
                                  !are we using RapidStock
                                  Access:USERS.Clearkey(use:User_Code_Key)
                                  use:User_Code  = job:Engineer
                                  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                     !Found
      
                                  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                     !Error
                                  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
                                  access:location.clearkey(loc:Location_Key)
                                  loc:Location = use:location
                                  access:location.fetch(loc:Location_Key)
                                  if loc:UseRapidStock And tmp:Engineer48HourOption <> 1
                                     par:Status = 'RTS'
                                     par:PartAllocated = 0
                                     Access:parts.update()
                                     AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'RET',job:Engineer)
                                  ELSE
                                     !Only call the exchange screen if there *is* as exchange
                                     !unit attached.
                                     If job:Exchange_Unit_Number <> 0
                                         tmp:CurrentExchangeUnitNumber = job:Exchange_Unit_Number
                                         ViewExchangeUnit()
                                         If tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
                                             !Something about the Exchange has changed. Save the record
                                             Access:JOBS.Update()
                                             ?Cancel{prop:Disable} = 1
                                         End !tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
                                         If job:Exchange_Unit_Number <> 0
                                             !The unit is still attached
                                             Cycle
                                         End !If job:Exchange_Unit_Number <> 0
                                     End !If job:Exchange_Unit_Number <> 0
                                     GetStatus(101,0,'EXC')
                                  END !if loc:useRapidStock
      
                                  If CallExchangeOrder# = True
                                      !Bring up the screen to create a new order request -  (DBH: 27-11-2003)
                                      If Exchange48HourOrder(job:Ref_Number,job:DOP,1) = True
                                          !Create EXCH Order Parts Line -  (DBH: 17-10-2003)
                                          local.AllocateExchangeOrderPart('ORD',0)
                                          !And this means it will appear in the Stock Allocation list
                                          GetStatus(360,0,'EXC') !Exchange Order Required
                                          GetStatus(355,0,'JOB') !Exchange Requested
                                          Brw9.ResetSort(1)
                                          Brw10.ResetSort(1)
                                      End !If Exchange48HourOrder(job:Ref_Number)
                                  End !If CallExchangeOrder# = True
                              ELSE
                                  PartAllocatedFlag# = 0
      
                              END !If partallocated
                          end !if parts.fetch worked
                      End !If job:Exchange_Unit_Number = 0
                  End !If par:SecondExchangeUnit = True
              Of 1 ! No Button
          End ! Case Missive
        if PartAllocatedFlag# =1  !this was an allocated unit - dealt with above
                                  !If not we let the delete procedure go ahead
            if job:Exchange_Unit_Number = 0 then exchange_loan_temp =''.
            BRW9.resetsort(1)
            cycle
        END !if part allocated flag
      END
    OF ?Change:3
      !look out for an exchange part
      if Wpr:Part_Number = 'EXCH' then
          If FullAccess(glo:PassAccount,glo:Password) <> Level:Benign
              Case Missive('You cannot edit an Exchange Unit.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              cycle
          END !if not superuser
      END
    OF ?Delete:3
      !Added By Paul - 01/10/2009 - log no - 11060
      !check to see if its a 'PINK' part before we delete it
      If tmp:WColourFlag = 7 then
          !we need to check the access level before we continue
          If SecurityCheck('JOB - DELETE PART AW RETURN') = Level:Benign
              !access level is present
              !so continue
          Else
              !access level is missing
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
              of 1
              End
              cycle
          End !SecurityCheck('RAPID ENG - CREATE INVOICE') = Level:Benign
      
      End !If tmp:WColourFlag = 7 then
      !End Change
      
      !look out for an exchange part
      CallExchangeORder# = False
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number   = brw10.q.wpr:Record_Number
      If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
          !Found
      
      Else ! If Access:WARPARTS.Tryfetch(RecordNumberKey) = Level:Benign
          !Error
          Cycle
      
      End !If Access:WARPARTS.Tryfetch(RecordNumberKey) = Level:Benign
      
      
      If Local.CorrectEngineer() = False
          Cycle
      End !Local.CorrectEngineer() = False
      
      if wpr:Part_Number = 'EXCH' then
          ! Inserting (DBH 07/12/2007) # 4486 - Do not allow to delete exchanges on completed jobs without access level
          If SecurityCheck('AMEND EXCHANGE ON COMPLETE JOB') And job:Date_Completed <> ''
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If SecurityAccess('AMEND COMPLETED EXCHANGE')
          ! End (DBH 07/12/2007) #4486
      
          ! VP119 - An ARC technician cannot delete RRC exchange units
          If glo:WebJob = False
              !Check to see if it's a Second Exchange Unit
              !if not, the ARC cannot delete RRC exchanges -  (DBH: 17-12-2003)
              If wpr:SecondExchangeUnit = False
                  if jobe:ExchangedATRRC = true
                      Case Missive('This unit cannot be deleted because it was exchanged at the RRC.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  end
              End !If wpr:SecondExchangeUnit = False
          Else !If glo:WebJob = False
              If wpr:SecondExchangeUnit = True
                  Case Missive('This part is for the Second Exchange Unit and can only deleted at the ARC.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End !If wpr:SecondExchangeUnit = True
          End !If glo:WebJob = False
      
          Case Missive('Are you sure you want to remove an allocated exchange unit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  If wpr:SecondExchangeUnit = True
                      If jobe:SecondExchangeNumber <> 0
                          If wpr:PartAllocated = True
                              PartAllocatedFlag# = 1
      
                              Access:USERS.Clearkey(use:User_Code_Key)
                              use:User_Code   = job:Engineer
                              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found
      
                              Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                              End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                              access:location.clearkey(loc:Location_Key)
                              loc:Location = use:Location
                              access:location.fetch(loc:Location_Key)
                              if loc:UseRapidStock
                                 wpr:Status = 'RTS'
                                 wpr:PartAllocated = 0
                                 Access:warparts.update()
                                 AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'RET',job:Engineer)
                              ELSE
                                 !Only call the exchange unit screen if there *is*
                                 !an exchange unit attached
                                 If jobe:SecondExchangeNumber <> 0
                                     tmp:CurrentExchangeUnitNumber = jobe:SecondExchangeNumber
                                     ViewExchangeUnit()
                                     If tmp:CurrentExchangeUnitNumber <> jobe:SecondExchangeNumber
                                         !Something about the Exchange has changed. Save the record
                                         Access:JOBS.Update()
                                         ?Cancel{prop:Disable} = 1
                                     End !tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
                                     !The exchange unit is still attached
                                     If jobe:SecondExchangeNumber <> 0
                                         Cycle
                                     End !If job:Exchange_Unit_Number <> 0
                                 End !If job:Exchange_Unit_Number <> 0
                              END !if loc:useRapidStock
                          End !If wpr:PartAllocated = True
                      End !If jobe:SecondExchangeNumber = 0
                  Else !If wpr:SecondExchangeUnit = True
      
                      If job:Exchange_Unit_Number = 0
                          If tmp:Engineer48HourOption = 1
                              !Is there an exchange unit on the way? -  (DBH: 27-11-2003)
                              If Is48HourOrderCreated(tmp:CurrentSiteLocation,job:Ref_Number) = True
                                  Case Missive('A 48 Hour Exchange  Unit has been ordered for this job.'&|
                                    '<13,10>'&|
                                    '<13,10>Are you sure you want to delete this order request?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                          Relate:EXCHOR48.Delete(0)
                                      Of 1 ! No Button
                                          Cycle
                                  End ! Case Missive
                              End !Is48HourOrderCreated(tmp:CurrentSiteLocation,job:RefNumber) = True
      
                              If Is48HourOrderProcessed(tmp:CurrentSiteLocation,job:Ref_Number) = True
                                  Case Missive('The exchange unit that was requested for this job has already been despatched from National Stores.'&|
                                    '<13,10>'&|
                                    '<13,10>You must receive this unit from Rapid Exchange Allocation before you can remove it.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Cycle
                              End !Is48HourOrderProcessed(tmp:CurrentSiteLocation,job:Ref_Number) = True
      
                          End !If tmp:Engineer48HourOption = 1
                          !There is no exchange unit attached, therefore must be on order
                          GetStatus(101,0,'EXC')
                          stanum# = Sub(job:previousStatus,1,3)
                          GetStatus(stanum#,0,'JOB')
                      Else
                           if wpr:PartAllocated = 1 then
                              If tmp:Engineer48HourOption = 1
                                  Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                  xch:Ref_Number  = job:Exchange_Unit_Number
                                  If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                      !Found
      
                                  Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                      !Error
                                  End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                  !Is there an exchange unit on the way? -  (DBH: 27-11-2003)
                                  Access:EXCHOR48.ClearKey(ex4:JobNumberKey)
                                  ex4:Location  = tmp:CurrentSiteLocation
                                  ex4:JobNumber = job:Ref_Number
                                  If Access:EXCHOR48.TryFetch(ex4:JobNumberKey)  = Level:Benign
                                      !Found
                                      If ex4:Received = 1 And ex4:AttachedToJob = 0 And ex4:DateOrdered <> 0
                                          Case Missive('The exchange unit that was requested for this job has already been despatched from National Stores.'&|
                                            '<13,10>'&|
                                            '<13,10>you must receive this unit from Rapid Exchange Allocation before you can remove it.','ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      End !If ex4:Received
                                  Else !If Access:EXCHOR48.TryFetch(ex4:JobNumberKey)  = Level:Benign
                                      !Error
                                  End !If Access:EXCHOR48.TryFetch(ex4:JobNumberKey)  = Level:Benign
      
                                  If DeleteExchangeReason(xch:ESN)
                                      !Create a new 48 order and either mark it
                                      !as order required or not -  (DBH: 03-11-2003)
                                      Return# =  Missive('Would you like to request a replacement for the Exchange Unit you are deleting?','ServiceBase 3g',|
                                                     'mquest.jpg','\No|/Yes')
                                      !Does an entry already exist for this job?
                                      !If so, change the exchange unit to match the unit on this job]# -  (DBH: 27-11-2003)
                                      Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
                                      ex4:Received  = 0
                                      ex4:Returning = 1
                                      ex4:Location  = tmp:CurrentSiteLocation
                                      ex4:JobNumber = job:Ref_Number
                                      If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                                          !Found
                                          ex4:OrderUnitNumber = job:Exchange_Unit_Number
                                          ex4:Notes           = Clip(ex4:Notes) & '<13,10>' & Clip(Upper(glo:EDI_Reason))
                                          Access:EXCHOR48.Update()
                                      Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                                          !Error
                                          If Access:EXCHOR48.PrimeRecord() = Level:Benign
                                              ex4:Location        = tmp:CurrentSiteLocation
                                              ex4:Manufacturer    = job:Manufacturer
                                              ex4:ModelNumber     = job:Model_Number
                                              ex4:Received        = 0
                                              ex4:Notes           = Clip(Upper(glo:EDI_Reason))
                                              ex4:JobNumber       = job:Ref_Number
                                              ex4:OrderUnitNumber = job:Exchange_Unit_Number
                                              ex4:Returning       = 1
                                              ex4:AttachedToJob   = 0
                                              If Access:EXCHOR48.TryInsert() = Level:Benign
                                                  !Insert Successful
      
                                              Else !If Access:EXCHOR48.TryInsert() = Level:Benign
                                                  !Insert Failed
                                              End !If Access:EXCHOR48.TryInsert() = Level:Benign
      
      
                                          End !If Access:EXCHOR48.PrimeRecord() = Level:Benign
      
                                      End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
      
                                      locAuditNotes = ''
                                      Case Return#
                                          Of 1
                                              locAuditNotes           = 'REPLACEMENT REQUESTED'
                                              CallExchangeOrder# = True
                                          Of 2
                                              locAuditNotes           = 'NO REPLACEMENT REQUESTED'
                                      End !Case Return#
                                      locAuditNotes = Clip(locAuditNotes) & '<13,10>UNIT NO: ' & job:Exchange_Unit_Number & |
                                                          '<13,10>REASON: ' & Clip(Upper(glo:EDI_Reason))
      
                                      IF (AddToAudit(job:Ref_Number,'JOB','48 HOUR EXCHANGE UNIT REMOVED FROM JOB',locAuditNotes))
                                      END ! IF
      
                                      Local.PrintReturningWaybill(job:Exchange_Unit_Number)
      
                                  Else !If DeleteExchangeReason(xch:ESN)
                                      Cycle
                                  End !If DeleteExchangeResaon(xch:ESN)
      
                              End !If tmp:Engineer48HourOption = 1
      
                               PartAllocatedFlag# = 1   !make a note of it for later
                               !deal with delete here
                               !are we using RapidStock
                               Access:USERS.Clearkey(use:User_Code_Key)
                               use:User_Code  = job:Engineer
                               If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found
      
                               Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                               End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                               access:location.clearkey(loc:Location_Key)
                               loc:Location = use:Location
                               access:location.fetch(loc:Location_Key)
                               if loc:UseRapidStock And tmp:Engineer48HourOption <> 1
                                  wpr:Status = 'RTS'
                                  wpr:PartAllocated = 0
                                  Access:warparts.update()
                                  AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'RET',job:Engineer)
                               ELSE
                                  !Only call the exchange unit screen if there *is*
                                  !an exchange unit attached
                                  If job:Exchange_Unit_Number <> 0
                                      tmp:CurrentExchangeUnitNumber = job:Exchange_Unit_Number
                                      ViewExchangeUnit()
                                      If tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
                                          !Something about the Exchange has changed. Save the record
                                          Access:JOBS.Update()
                                          ?Cancel{prop:Disable} = 1
                                      End !tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
                                      !The exchange unit is still attached
                                      If job:Exchange_Unit_Number <> 0
                                          Cycle
                                      End !If job:Exchange_Unit_Number <> 0
                                  End !If job:Exchange_Unit_Number <> 0
                                  GetStatus(101,0,'EXC')
                               END !if loc:useRapidStock
                              If CallExchangeOrder# = True
                                  !Bring up the screen to create a new order request -  (DBH: 27-11-2003)
                                  If Exchange48HourOrder(job:Ref_Number,job:DOP,1) = True
                                      !Create EXCH Order Parts Line -  (DBH: 17-10-2003)
                                      local.AllocateExchangeOrderPart('ORD',wpr:SecondExchangeUnit)
                                      !And this means it will appear in the Stock Allocation list
                                      GetStatus(360,0,'EXC') !Exchange Order Required
                                      GetStatus(355,0,'JOB') !Exchange Requested
                                      Brw9.ResetSort(1)
                                      Brw10.ResetSort(1)
                                  End !If Exchange48HourOrder(job:Ref_Number)
                              End !If CallExchangeOrder# = True
      
                          ELSE !if wpr:PartAllocated
                              PartAllocatedFlag# = 0    !Deal with delete normally
                          END !If wpr:PartAllocated
                      End !If job:Exchange_Unit_Number = 0
                  End !If wpr:SecondExchangeUnit = True
      
              Of 1 ! No Button
          End ! Case Missive
          if PartAllocatedFlag# = 1 then
              if job:Exchange_Unit_Number = 0 then exchange_loan_temp =''.
              BRW10.resetsort(1)
              cycle
         END !if part allocated
      END
    OF ?OK
      ! #11762 Repair Type does not allow parts.
      ! Check if there are any (Bryan: 24/11/2010)
      If (RepairTypesNoParts(job:Chargeable_Job, |
                                    job:Warranty_Job, |
                                    job:Manufacturer, |
                                    job:Repair_Type,  |
                                    job:Repair_Type_Warranty))
          If (JobHasSparesAttached(job:Ref_Number, |
                                        job:Estimate,  |
                                        job:Chargeable_Job, |
                                        job:Warranty_Job))
              Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type or remove the parts and then try again.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End
      End
!      !added by Paul 01/11/2010 - Log no 11762
!      !check to see if parts are allowed with the selected repair type
!      If job:Chargeable_Job = 'YES' Then
!          !check chargeable repair type
!          access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
!          rtd:Manufacturer    = job:Manufacturer
!          rtd:Repair_Type     = job:Repair_Type
!          If Access:ReptyDef.fetch(rtd:ManRepairTypeKey) = level:benign then
!              if rtd:NoParts = 'Y' then
!                  Access:Parts.ClearKey(par:Part_Number_Key)
!                  par:Ref_Number = job:Ref_Number
!                  set(par:Part_Number_Key,par:Part_Number_Key)
!                  loop
!                      If Access:Parts.Next() then
!                          Break
!                      End
!                      if par:Ref_Number <> job:Ref_Number then
!                          break
!                      End
!                      !if we are still here - then there must be parts on the job
!                      Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type or remove the parts and then try again.','ServiceBase 3g',|
!                                     'mstop.jpg','/OK')
!                          Of 1 ! OK Button
!                      End ! Case Missive
!                      Break
!                  End
!              End
!          End
!      ElsIf job:Warranty_Job  = 'YES' then
!          access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
!          rtd:Manufacturer    = job:Manufacturer
!          rtd:Repair_Type     = job:Repair_Type_Warranty
!          If Access:RepTyDef.Fetch(rtd:ManRepairTypeKey) = level:benign then
!              If rtd:NoParts = 'Y' then
!                  Access:WarParts.ClearKey(wpr:Part_Number_Key)
!                  wpr:Ref_Number = job:Ref_Number
!                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!                  loop
!                      If Access:WarParts.Next() then
!                          break
!                      End
!                      if wpr:Ref_Number <> job:Ref_Number then
!                          break
!                      End
!                      !if we are still here - then there must be parts on the job
!                      Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type or remove the parts and then try again.','ServiceBase 3g',|
!                                     'mstop.jpg','/OK')
!                          Of 1 ! OK Button
!                      End ! Case Missive
!                      Break
!                  End
!              End
!          End
!      End
    OF ?Cancel
      Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              tmp:Close = True
          Of 1 ! No Button
              Cycle
      End ! Case Missive
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OptionsEstimate
      ThisWindow.Update
      EstimateDetails
      ThisWindow.Reset
      Do GetJOBSE
      Local.Pricing(0)
      do SetJOBSE
      Do ShowCompletedText
      Do Enable_Disable
      Do CountParts
      Do Fill_Lists
    OF ?OptionsAllocateLoan
      ThisWindow.Update
      ViewLoanUnit
      ThisWindow.Reset
      Do Enable_Disable
      Do GetJobSE
      If tmp:CurrentExchangeUnitNumber <> job:Loan_Unit_Number
          !Something about the Exchange has changed. Save the record
          Access:JOBS.Update()
          If glo:Preview = 'V' And ?OK{prop:Hide} = True
              ! If screen is "read only" show a close button if the job changes - TrkBs: 6570 (DBH: 19-10-2005)
              ?Cancel{prop:Hide} = True
              ?Close{prop:Hide} = False
          Else ! If glo:Preview = 'V'
              ?Cancel{prop:Disable} = True
          End ! If glo:Preview = 'V'
      End !tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
    OF ?OptionsAllocateExchange
      ThisWindow.Update
      ViewExchangeUnit
      ThisWindow.Reset
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
      
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      Do GetJOBSE
      Do Enable_Disable
      
      Display()
      
      If tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number Or tmp:CurrentSecondExchangeUnitNumber <> jobe:SecondExchangeNumber
          !Something about the Exchange has changed. Save the record
          Access:JOBS.Update()
      
          If glo:Preview = 'V' And ?OK{prop:Hide} = True
              ! If screen is "read only" show a close button if the job changes - TrkBs: 6570 (DBH: 19-10-2005)
              ?Cancel{prop:Hide} = True
              ?Close{prop:Hide} = False
          Else ! If glo:Preview = 'V'
              ?Cancel{prop:Disable} = True
          End ! If glo:Preview = 'V'
      
          If glo:WebJob And tmp:HubRepair
              Do RRCHubRepairDisableScreen
          End !If glo:WebJob And tmp:HubRepair
      
          If tmp:CurrentExchangeUnitNumber = 0 And job:Exchange_Unit_Number <> 0
              If UseReplenishmentProcess(job:Model_Number,wob:HeadAccountNumber) = 1
                  Order# = 1
                  Beep(Beep:SystemQuestion);  Yield()
                  Case Missive('Do you wish to order a replacement exchange unit?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          Beep(Beep:SystemQuestion);  Yield()
                          Case Missive('Can you please confirm that you do NOT want to order a Replacement Unit.'&|
                              '|'&|
                              '|Otherwise, click "Order" to order a replacement unit.','ServiceBase 3g',|
                                         'mquest.jpg','/Order|\Confirm')
                              Of 2 ! Confirm Button
                                  Order# = 0
                              Of 1 ! Order Button
                          End ! Case Missive
                  End ! Case Missive
                  If Order# = 1
                      If Access:EXCHOR48.PrimeRecord() = Level:Benign
                          ex4:Location     = tmp:CurrentSiteLocation
                          ex4:Manufacturer = job:Manufacturer
                          ex4:ModelNUmber  = job:Model_Number
                          ex4:Received     = 0
                          ex4:JobNumber    = 0
                          ex4:OrderUnitNumber = job:Exchange_Unit_Number
                          If Access:EXCHOR48.TryInsert() = Level:Benign
                              Clear(PassModelQueue)
                              Free(PassModelQueue)
                              PassModelQueue.Manufacturer = job:Manufacturer
                              PassModelQueue.ModelNumber  = job:Model_Number
                              PassModelQUeue.Quantity     = 1
                              Add(PassModelQueue)
                              ExchangeOrders(PassModelQueue,0,0)
                              If AddToAudit(job:Ref_Number,'EXC','REPLENISHMENT EXCHANGE ORDER CREATED',|
                                          'MANUFACTURER: ' & Clip(job:Manufacturer) & |
                                          '<13,10>MODEL NUMBER: ' & Clip(job:Model_Number))
                              End ! If AddToAudit(job:Ref_Number,'EXC','REPLENISHMENT EXCHANGE ORDER CREATED',|
                          Else ! If Access:EXCHOR48.TryInsert() = Level:Benign
                              Access:EXCHOR48.CancelAutoInc()
                          End ! If Access:EXCHOR48.TryInsert() = Level:Benign
                      End ! If Access:EXCHOR48.PrimeRecord() = Level:Benign
                  End ! If Order# = 1
              End ! If UseReplenishmentProcess(job:Model_Number,wob:HeadAccountNumber) = 1
          End ! If tmp:CurrentExchangeUnitNumber = 0 And job:Exchange_Unit_Number <> 0
      End !tmp:CurrentExchangeUnitNumber <> job:Exchange_Unit_Number
      BRW10.ResetSort(1)
      BRW39.ResetSort(1)
      BRW9.ResetSort(1)
    OF ?BrowseAmendAddresses
      ThisWindow.Update
      Show_Addresses
      ThisWindow.Reset
    OF ?BrowseAuditTrail
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      glo:select12 = ''
    OF ?BrowseStatusChange
      ThisWindow.Update
      BrowseStatusChanges(job:Ref_Number)
      ThisWindow.Reset
    OF ?BrowseContactHistory
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:select12 = ''
    OF ?BrowseEngineerHistory
      ThisWindow.Update
      BrowseEngineersOnJob(job:Ref_Number)
      ThisWindow.Reset
    OF ?BrowseLocationHistory
      ThisWindow.Update
      BrowseLocationChanges(job:ref_number)
      ThisWindow.Reset
    OF ?BrowseSMSFeaturesSMSHistory
      ThisWindow.Update
      BrowseSMSHistory
      ThisWindow.Reset
    OF ?BrowseSMSFeaturesSendLoanSMS
      ThisWindow.Update
      !Sending sms if defined for current loan status
      
      SendSMSText('L','N','N')
      
      !If jobe2:SMSDate = 0 then
      !    !no loan sms sent previously
      !    jobe2:SMSDate = today() + 30
      !    access:JobSE2.update()
      !
      !End!If jobe2:SMSDate = 0 then
      !
      !!Format the date before sending
      !TempDate = Format(jobe2:SMSDate,@d10)
      !
      !now send the SMS message
      !If jobe2:SMSNotification
      !    !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'LOAN','SMS',jobe2:SMSAlertNumber,'','',clip(TempDate))
      !    SendSMSText('L','Y','N')
      !End ! If jobe2:SMSNotification
      !If jobe2:EmailNotification
      !    AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'LOAN','EMAIL','',jobe2:EmailAlertAddress,'',clip(TempDate))
      !End ! If jobe2:EmailNotification
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020425'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020425'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020425'&'0')
      ***
    OF ?buttonCustomerClassification
      ThisWindow.Update
          Do CustomerClassificationButton
    OF ?job:MSN
      if ~0{prop:Acceptall}
          Do MSNEmbeds
      end
    OF ?job:Repair_Type_Warranty
      If ~0{prop:acceptall}
      
              IF job:Repair_Type_Warranty
                  Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                  rtd:Manufacturer = job:Manufacturer
                  rtd:Warranty     = 'YES'
                  rtd:Repair_Type  = job:Repair_Type_Warranty
                  If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                      !Found
      
                  Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      job:Repair_Type_Warranty = PickRepairTypes(job:Model_Number,job:Account_Number,|
                                                            job:Warranty_Charge_Type,job:Unit_Type,'W',0,|
                                                            EngineerSkillLevel(job:Engineer),job:Repair_Type_Warranty)
                      If job:Repair_Type_Warranty = ''
                          job:Repair_Type = tmp:WarrantyRepairType
                      End !job:Repair_Type = ''
      
                  End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
              End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
              If job:Repair_Type_Warranty <> tmp:WarrantyRepairType
                  error# = 0
      
                  If job:date_completed <> ''
                      If SecurityCheck('AMEND COMPLETED JOBS')
                          Case Missive('Cannot change Warranty Repair Type! This job has been completed.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          error# = 1
                      End
                  End
                  If IsJobInvoiced(job:Invoice_Number) and Error# = 0
                      If SecurityCheck('AMEND INVOICED JOBS')
                          Case Missive('Cannot change Warranty Repair Type! This job has been invoiced.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                      End
                  End
      
                  If Error# = 0
                      Case CheckPricing('W')
                          Of 1
                              error# = 1
                          Else
                              If tmp:WarrantyRepairType <> ''
                                  Case Missive('Are you sure you want to change the Repair Type?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                      Of 1 ! No Button
                                          error# = 1
                                  End ! Case Missive
                              End!If repair_type_temp <> ''
                      End!Case CheckPricing('C')
                  End !If Error# = 0
                  If error# = 1
                      job:repair_type_Warranty = tmp:WarrantyRepairType
                  Else!If job:date_completed <> ''
                      tmp:WarrantyRepairType = job:repair_type_Warranty
      
                      !Do we want an "Exchange Prompt" message to appear?
                      Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                      rtd:Warranty    = 'YES'
                      rtd:Repair_Type = job:Repair_Type_Warranty
                      If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                          !Found
                          If rtd:PromptForExchange
                              Case Missive('You have requested an Exchange Unit for this repair.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to PROCESS this request, or CANCEL it?','ServiceBase 3g',|
                                             'mquest.jpg','\Cancel|/Process')
                                  Of 2 ! Process Button
      
                                      GetStatus(108,0,'EXC')
                                  Of 1 ! Cancel Button
                              End ! Case Missive
                          End !If rtd:ExchangePrompt
                      Else!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                      Local.Pricing(0)
                      Post(Event:accepted,?job:Warranty_Charge_Type)
                  End!If job:date_completed <> ''
      
              End!If job:repair_type <> repair_type_temp
              Display()
      End !0{prop:acceptall}
    OF ?ResubmitJob
      ThisWindow.Update
      If SecurityCheck('RESUBMIT WARRANTY JOB')
          NoAccess('RESUBMIT WARRANTY JOB')
      Else !SecurityCheck('RESUBMIT WARRANTY JOB')
          Case Missive('This will allow this job to be included in the next EDI Process.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  !get a reason
                  Glo:EDI_Reason = ''
                  Get_EDI_Reason   !writes to glo:EDI_Reason string 60
      
                  If glo:EDI_Reason <> ''
                      !update the records
                      job:edi = 'NO'
                      wob:edi = 'NO'
                      access:webjob.update()
                      if access:jobs.update() = level:benign
      
                          IF (AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','RESUBMITTED TO WARRANTY CLAIM<13,10>'&Glo:EDI_Reason))
                          END ! IF
      
                          access:jobnotes.clearkey(jbn:RefNumberKey)
                          jbn:RefNumber = job:ref_Number
                          if access:jobnotes.tryfetch(jbn:RefNumberKey)
                             jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) &'<13,10>'&Glo:EDI_Reason
                             access:jobnotes.update()
                          END !if jobnotes.tryfetch
      
                          Do SETJOBSE
                          jobe:WarrantyClaimStatus = 'RESUBMITTED'
                          jobe:WarrantyStatusDate  = Today()
                          Access:JOBSE.Update()
      
                          Do GETJOBSE
      
                          ! Inserting (DBH 10/04/2006) #7252 - Record the "in pending" date
                          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                          jobe2:RefNumber   = job:Ref_Number
                          If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                              ! Found
                              jobe2:InPendingDate = Today()
                              Access:JOBSE2.TryUpdate()
                          Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                            ! Error
                          End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                          ! End (DBH 10/04/2006) #7252
                          !Added by Paul 13/08/2009 Log No
                          ?Cancel{prop:disable} = True
                          display()
                      END !if access:jobs.update
                  End !If glo:EDI_Reason <> ''
              Of 1 ! No Button
          End ! Case Missive
      End !SecurityCheck('RESUBMIT WARRANTY JOB')
    OF ?job:Charge_Type
      IF job:Charge_Type OR ?job:Charge_Type{Prop:Req}
        cha:Charge_Type = job:Charge_Type
        cha:Warranty = 'NO'
        GLO:Select1 = 'NO'
        !Save Lookup Field Incase Of error
        look:job:Charge_Type        = job:Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Charge_Type = look:job:Charge_Type
            SELECT(?job:Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          error# = 0
          CheckEstimate# = 0
          If tmp:chargetype   <> job:Charge_Type
              If tmp:chargetype <> ''
                  Case Missive('Are you sure you want to change the Charge Type?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          error# = 1
                  End ! Case Missive
              End!If tmp:chargetype <> ''
              If error# = 0
                  If CheckPricing('C')
                      error# = 1
                  End!If CheckPricing('C')
              End!If error# = 0
      
              If error# = 0
                  tmp:chargetype  = job:Charge_Type
                  access:chartype.clearkey(cha:charge_type_key)
                  cha:charge_type = job:charge_type
                  If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      If cha:allow_estimate = 'YES'
                          If job:estimate <> 'YES' and cha:force_estimate = 'YES'
                              Case Missive('The selected Charge Type means that this job must be an Estimate.','ServiceBase 3g',|
                                             'mexclam.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:estimate = 'YES'
                          End!If job:estimate <> 'YES'
                      End
                  end !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
      
              Else!If error# = 0
                  job:Charge_Type = tmp:Chargetype
              End!If error# = 0
      
          End!If tmp:chargetype   <> job:Charge_Type
      
          Display()
          Do Enable_Disable
          Do ShowCompletedText
          ! To hopefully prevent Estimate part error - TrkBs: 6557 (DBH: 03-11-2005)
          brw9.ResetSort(1)
          brw10.ResetSort(1)
          brw39.ResetSort(1)
      End!If ~0{prop:acceptall}
    OF ?job:Repair_Type
      If ~0{prop:acceptall}
          IF job:Repair_Type
              Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
              rtd:Manufacturer = job:Manufacturer
              rtd:Chargeable   = 'YES'
              rtd:Repair_Type  = job:Repair_Type
              If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                  !Found
      
              Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,|
                                                    job:Charge_Type,job:Unit_Type,'C',1,|
                                                    EngineerSkillLevel(job:Engineer),job:Repair_Type)
                  If job:Repair_Type = ''
                      job:Repair_Type = tmp:RepairType
                  End !job:Repair_Type = ''
      
              End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
          End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
          If job:repair_type <> tmp:RepairType
              Error# = 0
              If job:date_completed <> ''
                  If SecurityCheck('AMEND COMPLETED JOBS')
                      Case Missive('Cannot change Chargeable Charge Types! This job has been completed.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End
              End
              If IsJobInvoiced(job:Invoice_Number) and Error# = 0
                  If SecurityCheck('AMEND INVOICED JOBS')
                      Case Missive('Cannot change Chargeable Charge Types! This job has been invoiced.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End
              End
      
              IF Error# = 0
                  Case CheckPricing('C')
                      Of 1
                          error# = 1
                      Else
                          If tmp:RepairType <> ''
                              Case Missive('Are you sure you want to change the Repair Type?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      error# = 1
                              End ! Case Missive
                          End!If tmp:RepairType <> ''
                  End!Case CheckPricing('C')
              End !IF Error# = 0
      
              If error# = 1
                  job:repair_type = tmp:RepairType
              Else!If job:date_completed <> ''
                  tmp:RepairType = job:repair_type
                  Local.Pricing(0)
                  Post(Event:accepted,?job:charge_type)
              End!If job:date_completed <> ''
          End!If job:repair_type <> tmp:RepairType
          Display()
      End !0{prop:acceptall}
    OF ?job:Warranty_Job
      If job:warranty_job <> 'YES'
          job:edi = 'XXX'
      End!If job:warranty_job = 'NO'
    OF ?job:Warranty_Charge_Type
      IF job:Warranty_Charge_Type OR ?job:Warranty_Charge_Type{Prop:Req}
        cha:Charge_Type = job:Warranty_Charge_Type
        cha:Warranty = 'YES'
        GLO:Select1 = 'YES'
        !Save Lookup Field Incase Of error
        look:job:Warranty_Charge_Type        = job:Warranty_Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            job:Warranty_Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Warranty_Charge_Type = look:job:Warranty_Charge_Type
            SELECT(?job:Warranty_Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?tmp:Engineer48HourOption
      If ~0{prop:Acceptall}
          If job:Engineer = ''
              Case Missive('You must allocate an engineer to this job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              tmp:Engineer48HourOption = tmp:Saved48HourOption
          Else !job:Engineer = ''
              !Is the option being changed, or inserted for the first time? -  (DBH: 24-10-2003)
              Error# = 0
              If tmp:Saved48HourOption <> 0
                  If tmp:Saved48HourOption <> tmp:Engineer48HourOption
                      Case Missive('Are you sure you want to change the selected option?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              Access:USERS.Clearkey(use:User_Code_Key)
                              use:User_Code   = InsertEngineerPassword()
                              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
                                  acc:User_Level  = use:User_Level
                                  acc:Access_Area = 'AMEND ENGINEERING OPTION'
                                  If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
                                      !Found
      
                                  Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
                                      !Error
                                      Case Missive('You do not have sufficient access to amend this option.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                      tmp:Engineer48HourOption = tmp:Saved48HourOption
                                      Error# = 1
                                  End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      
                              End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                          Of 1 ! No Button
                              tmp:Engineer48HourOption = tmp:Saved48HourOption
                              Error# = 1
                      End ! Case Missive
                  End !If tmp:Saved48HourOption <> tmp:Engineer48HourOption
              End !If tmp:Saved48HourOption <> 0
      
              If Error# = 0
                  Case tmp:Engineer48HourOption
                      Of 1
                          Case Missive('Are you sure you want to create an Exchange Order?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  If Exchange48HourOrder(job:Ref_Number,job:DOP,0) = True
                                      !Create EXCH Order Parts Line -  (DBH: 17-10-2003)
                                      local.AllocateExchangeOrderPart('ORD',0)
                                      !And this means it will appear in the Stock Allocation list
                                      GetStatus(360,0,'EXC') !Exchange Order Required
                                      GetStatus(355,0,'JOB') !Exchange Requested
                                      Brw9.ResetSort(1)
                                      Brw10.ResetSort(1)
                                      tmp:Saved48HourOption = 1
                                      If Access:AUDIT.PrimeRecord() = Level:Benign
                                          aud:Ref_Number    = job:ref_number
                                          aud:Date          = Today()
                                          aud:Time          = Clock()
                                          aud:Type          = 'JOB'
                                          Access:USERS.ClearKey(use:Password_Key)
                                          use:Password      = glo:Password
                                          Access:USERS.Fetch(use:Password_Key)
                                          aud:User          = use:User_Code
                                          aud:Action        = 'ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE'
                                          Access:AUDIT.Insert()
                                      End!If Access:AUDIT.PrimeRecord() = Level:Benign
      
                                      !If warranty job, auto change to spilt chargeable - 3876 (DBH: 29-03-2004)
                                      If job:Warranty_Job = 'YES' and job:Chargeable_Job <> 'YES'
                                          job:Chargeable_Job = 'YES'
                                          !Change to a "Fixed Price" RRC Charge Type - 3878 (DBH: 23-04-2004)
                                          If glo:WebJob
                                              job:Charge_Type = 'NON-WARR SERVICE FEE'
                                          Else !If glo:WebJob
                                              job:Charge_Type = 'NON-WARRANTY'
                                          End !If glo:WebJob
                                          job:Repair_Type = '48-HOUR SERVICE FEE'
                                          Post(Event:Accepted,?job:Chargeable_Job)
                                      End !If job:Warranty_Job = 'YES' and job:Chargeable_Job <> 'YES'
      
                                  Else !If Exchange48HourOrder(job:Ref_Number)
                                      !If exchange order canceled, prompt to chose another option - 3647 (DBH: 03-12-2003)
                                      tmp:Engineer48HourOption = tmp:Saved48HourOption
                                      Case Missive('You have chosen to cancel the 48 Hour Exchange Order. Please select another option.','ServiceBase 3g',|
                                                     'mexclam.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                  End !If Exchange48HourOrder(job:Ref_Number)
                              Of 1 ! No Button
                                  tmp:Engineer48HourOption = tmp:Saved48HourOption
                          End ! Case Missive
                      Of 2
                          !Automatically send the job to hub -  (DBH: 24-10-2003)
                          ! Inserting (DBH 27/06/2006) #6420 - Is this a liquid damage unit?
                          If ?Prompt:LiquidDamage{prop:Hide} = False And jobe:WebJob = True And jobe:HubRepair = ''
                              Case Missive('Error! Cannot sent to ARC.'&|
                                '|This Unit is Liquid Damaged.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              tmp:Engineer48HourOption = tmp:Saved48HourOption
                              Error# = True
                          Else ! If ?Prompt:LiquidDamage{prop:Hide} = False
                          ! End (DBH 27/06/2006) #6420
                              If tmp:HubRepair = 0 And glo:WebJob = 1
                                  tmp:HubRepair = 1
                                  Post(Event:Accepted,?tmp:HubRepair)
                              End !If tmp:HubRepair = 0
                              tmp:Saved48HourOption = 2
                              If Access:AUDIT.PrimeRecord() = Level:Benign
                                  aud:Ref_Number    = job:ref_number
                                  aud:Date          = Today()
                                  aud:Time          = Clock()
                                  aud:Type          = 'JOB'
                                  Access:USERS.ClearKey(use:Password_Key)
                                  use:Password      = glo:Password
                                  Access:USERS.Fetch(use:Password_Key)
                                  aud:User          = use:User_Code
                                  aud:Action        = 'ENGINEERING OPTION SELECTED: ARC REPAIR'
                                  Access:AUDIT.Insert()
                              End!If Access:AUDIT.PrimeRecord() = Level:Benign
      
                          End ! If ?Prompt:LiquidDamage{prop:Hide} = False
      
                      Of 3
                          !Automatically send the job to hub -  (DBH: 24-10-2003)
                          ! Inserting (DBH 27/06/2006) #6420 - Is this a liquid damage unit?
                          If ?Prompt:LiquidDamage{prop:Hide} = False And jobe:WebJob = True And jobe:HubRepair = ''
                              Case Missive('Error! Cannot sent to ARC.'&|
                                '|This Unit is Liquid Damaged.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              tmp:Engineer48HourOption = tmp:Saved48HourOption
                              Error# = True
                          Else ! If ?Prompt:LiquidDamage{prop:Hide} = False
                          ! End (DBH 27/06/2006) #6420
                              If tmp:HubRepair = 0 And glo:WebJob = 1
                                  tmp:HubRepair = 1
                                  Post(Event:Accepted,?tmp:HubRepair)
                              End !If tmp:HubRepair = 0
                              tmp:Saved48HourOption = 3
                              If Access:AUDIT.PrimeRecord() = Level:Benign
                                  aud:Ref_Number    = job:ref_number
                                  aud:Date          = Today()
                                  aud:Time          = Clock()
                                  aud:Type          = 'JOB'
                                  Access:USERS.ClearKey(use:Password_Key)
                                  use:Password      = glo:Password
                                  Access:USERS.Fetch(use:Password_Key)
                                  aud:User          = use:User_Code
                                  aud:Action        = 'ENGINEERING OPTION SELECTED: 7 DAY TAT'
                                  Access:AUDIT.Insert()
                              End!If Access:AUDIT.PrimeRecord() = Level:Benign
                          End ! If ?Prompt:LiquidDamage{prop:Hide} = False
                      Of 4
                          !Is this Model Excluded? -  (DBH: 26-11-2003)
                          If ExcludeFromRRCRepair(job:Model_Number) = True And Error# = 0
                              Case Missive('This model should not be repaired by an RRC and should be sent to the Hub.'&|
                                '<13,10>'&|
                                '<13,10>Do you want to send it directly to the HUB, or do you want to REPAIR it anyway?','ServiceBase 3g',|
                                             'mquest.jpg','Repair|Hub')
                                  Of 2 ! Hub Button
                                      If ?Prompt:LiquidDamage{prop:Hide} = False And jobe:WebJob = True and jobe:HubRepair = ''
                                          Case Missive('Error! Cannot sent to ARC.'&|
                                            '|This Unit is Liquid Damaged.','ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          tmp:Engineer48HourOption = tmp:Saved48HourOption
                                          Error# = True
                                      Else ! If ?Prompt:LiquidDamage{prop:Hide} = False And jobe:WebJob = True and jobe:HubRepair = ''
                                          !Automatically send the job to hub -  (DBH: 24-10-2003)
                                          If tmp:HubRepair = 0 And glo:WebJob = 1
                                              tmp:HubRepair = 1
                                              Post(Event:Accepted,?tmp:HubRepair)
                                          End !If tmp:HubRepair = 0
                                      End ! If ?Prompt:LiquidDamage{prop:Hide} = False And jobe:WebJob = True and jobe:HubRepair = ''
                                  Of 1 ! Repair Button
                              End ! Case Missive
                          End !ExcludeFromRRCRepair = True
      
                          If Error# = False
                              tmp:Saved48HourOption = 4
                              If Access:AUDIT.PrimeRecord() = Level:Benign
                                  aud:Ref_Number    = job:ref_number
                                  aud:Date          = Today()
                                  aud:Time          = Clock()
                                  aud:Type          = 'JOB'
                                  Access:USERS.ClearKey(use:Password_Key)
                                  use:Password      = glo:Password
                                  Access:USERS.Fetch(use:Password_Key)
                                  aud:User          = use:User_Code
                                  aud:Action        = 'ENGINEERING OPTION SELECTED: STANDARD REPAIR'
                                  Access:AUDIT.Insert()
                              End!If Access:AUDIT.PrimeRecord() = Level:Benign
                          End ! If Error# = False
                  End !tmp:Engineer48BookingOption = 1
              End !If Error# = 0
              !Do not allow the user to leave the screen without saving
              !once they have chosen an engineer job type - 3938 (DBH: 23-02-2004)
              If Error# = 0
                  ?Cancel{prop:Disable} = 1
              End ! If Error# = 0
          End !job:Engineer = ''
      End !0{prop:Acceptall}
      Display()
    OF ?engineers_notes
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Engineers_Notes
      ThisWindow.Reset
    OF ?ChangeJobStatus
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickAccessStatus
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              GetStatus(Sub(acs:Status,1,3),1,'JOB')
              !this now includes sendSMSText and the changes needed
          Of Requestcancelled
      End!Case Globalreponse
      glo:Select1 = ''
    OF ?Button:ContactHistoryEmpty
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:select12    = ''
      DO CheckContactHistory
    OF ?BouncerButton
      ThisWindow.Update
      If GetTempPathA(255,TempFilePath)
          If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & 'JOBBOUNCER' & Clock() & '.TMP'
          Else !If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & '\JOBBOUNCER' & Clock() & '.TMP'
          End !If Sub(TempFilePath,-1,1) = '\'
      End
      job:Account_Number = BrowseJobBouncers('')
      Remove(glo:FileName)
      Local.Pricing(0)
      
    OF ?tmp:Network
      IF tmp:Network OR ?tmp:Network{Prop:Req}
        net:Network = tmp:Network
        !Save Lookup Field Incase Of error
        look:tmp:Network        = tmp:Network
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:Network = net:Network
          ELSE
            !Restore Lookup On Error
            tmp:Network = look:tmp:Network
            SELECT(?tmp:Network)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupNetwork
      ThisWindow.Update
      net:Network = tmp:Network
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:Network = net:Network
          Select(?+1)
      ELSE
          Select(?tmp:Network)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Network)
    OF ?AmendUnitType
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Unit_Types
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
              error# = 0
              If job:unit_type <> tmp:unittype
                  Case Missive('Are you sure you want to change the Unit Type?'&|
                    '<13,10>'&|
                    '<13,10>Warning! The value of this job may change.','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          error# = 1
                  End ! Case Missive
              End!If job:unit_type <> tmp:unittype
              If error# = 0
                  If CheckPricing('C') Or CheckPricing('W')
                      error# = 1
                  End!If CheckPricing('C') Or CheckPricing('W')
              End!If error# = 0
      
              If error# = 0
                  tmp:unittype    = job:unit_type
                  job:unit_type = uni:unit_type
                  model_details_temp = Clip(job:model_number) & ' ' & Clip(job:manufacturer) & ' - ' & Clip(job:unit_type)
                  !Changing the unit type could reprice the job - L945 (DBH: 08-09-2003)
                  Local.Pricing(1)
              Else!If error# = 0
                  job:unit_type   = tmp:unittype
              End!If error# = 0
      
          of requestcancelled
      end!case globalreponse
      Display()
    OF ?Lookup_Engineer
      ThisWindow.Update
      SET(DEFAULT2)
      Access:DEFAULT2.Next()
      
      Globalrequest = SelectRecord
      
      If de2:UserSkillLevel
          PickEngineersSkillLevel(jobe:SkillLevel)
      Else !If de2:UserSkillLevel
          PickEngineers
      End !If de2:UserSkillLevel
      Case GlobalResponse
          Of Requestcompleted
              job:Engineer = use:User_Code
      
          Of Requestcancelled
      
      End!Case Globalreponse
      
      Do AddEngineer
    OF ?AllocateJob
      ThisWindow.Update
      ! Insert --- If the job is locked, recall the pup validation (DBH: 30/04/2009) #10782
          if (tmp:LockJob = 'LOCKJOB')
              do PUPValidation
          end !if (tmp:LockJob = 'LOCKJOB')
          if (tmp:LockJob = 'LOCKJOB')
              ! Still locked. Don't continue
              Cycle
          end !if (tmp:LockJob = 'LOCKJOB')
      ! end --- (DBH: 30/04/2009) #10782
      
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = InsertEngineerPassword()
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              If use:User_Code <> job:Engineer
                  If use:User_Type <> 'ENGINEER'
                      ! Only allowed to allocate Engineers
                      Case Missive('You are attempting to allocate an engineer that is not available for this site.','ServiceBase 3g',|
                          'mstop.jpg','/OK')
                      Of 1 ! OK Button
                      End ! Case Missive
                  Else !If use:User_Type <> 'ENGINEER'
                      Access:USERS_ALIAS.Clearkey(use_ali:Password_Key)
                      use_ali:Password = glo:Password
                      IF (Access:USERS_ALIAS.TryFetch(use_ali:Password_Key) = Level:Benign)
                          ! #11978 Get the current users location, and match with selected user. (Bryan: 14/02/2011)
      
                      END
                      IF (use:Location <> use_ali:Location)
                          ! #11978 Trying to allocate an engineer from another site to yours.(Bryan: 14/02/2011)
                          Case Missive('You are attempting to allocate an engineer that is not available for this site.','ServiceBase 3g',|
                              'mstop.jpg','/OK')
                          Of 1 ! OK Button
                          End ! Case Missive
                      ELSE
                          ! Inserting (DBH 11/10/2006) # 8326 - Make sure the allocated engineer matches the booked site
                          If use:Location <> tmp:BookingSiteLocation
                              If glo:WebJob Or (glo:WebJob = 0 And jobe:HubRepair = False)
                                  ! Ok to allocate if job is at the ARC (DBH: 11/10/2006)
                                  Case Missive('The selected engineer cannot be allocated to this job.','ServiceBase 3g',|
                                      'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                                  End ! Case Missive
                                  Error# = 1
                              Else
                                  ! DBH #10544 - Last check. If it's a booked Liquid Damage. Force to 3rd party site
                                  If (?BookingOption:Prompt{prop:Text} = 'Liquid Damage' And job:Third_Party_Site = '')
                                      Beep(Beep:SystemHand)  ;  Yield()
                                      Case Missive('Liquid Damaged Handset!'&|
                                          '|Send to 3rd Party for repairs.','ServiceBase',|
                                          'mstop.jpg','/&OK')
                                      Of 1 ! &OK Button
                                      End!Case Message
                                      ! Set status to "send To 3rd party"
                                      GetStatus(405,1,'JOB')
                                      if (addToAudit(job:Ref_Number,'JOB','STATUS CHANGED TO SEND TO 3RD PARTY',''))
                                      end
      
                                      Error# = 1
                                      Post(Event:Accepted,?OK)
                                  end ! If (jobe:Booking48HourOption = 4)
                              End ! If glo:WebJob Or (glo:WebJob = 0 And jobe:HubRepair = False)
                          Else ! If use:Location <> tmp:CurrentSIteLocation
                              ! End (DBH 11/10/2006) #8326
      
                              !If the job is at the ARC, check that it has been
                              !through Waybill confirmation - L921 (DBH: 13-08-2003)
                              Error# = 0
                              If use_ali:Location = tmp:ARCSiteLocation
                                  Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                                  lot:RefNumber   = job:Ref_Number
                                  lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                                  If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                                      !Found
                                      ! DBH #10544 - Last check. If it's a booked Liquid Damage. Force to 3rd party site
                                      If (?BookingOption:Prompt{prop:Text} = 'Liquid Damage' And job:Third_Party_Site = '')
                                          Beep(Beep:SystemHand)  ;  Yield()
                                          Case Missive('Liquid Damaged Handset!'&|
                                              '|Send to 3rd Party for repairs.','ServiceBase',|
                                              'mstop.jpg','/&OK')
                                          Of 1 ! &OK Button
                                          End!Case Message
                                          ! Set status to "send To 3rd party"
                                          GetStatus(405,1,'JOB')
                                          if (addToAudit(job:Ref_Number,'JOB','STATUS CHANGED TO SEND TO 3RD PARTY',''))
                                          end
      
                                          Error# = 1
                                          Post(Event:Accepted,?OK)
                                      end ! If (jobe:Booking48HourOption = 4)
      
      
                                  Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                                      !Error
                                      Case Missive('Error! This job is not at the ARC Location. Please ensure that the Waybill Confirmation has been completed.','ServiceBase 3g',|
                                          'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                      End ! Case Missive
                                      Error# = 1
                                  End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                              End !If use:Location = tmp:ARCSiteLocation
                          End ! If use:Location <> tmp:CUrrentSiteLocation
                          If Error# = 0
                              job:Engineer    = use:User_Code
      
                              Do AddEngineer
                          End !If Error# = 0
                      END
      
                  End !If use:User_Type <> 'ENGINEER'
              End !If use:User_Code <> job:Engineer
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
    OF ?Costs
      ThisWindow.Update
      ViewCosts
      ThisWindow.Reset
      !Get the temp variables back incase they are changed again
      Do GetJOBSE
      Do ShowCompletedText
      Do Enable_Disable
    OF ?Invoice
      ThisWindow.Update
      Case Missive('Are you sure you want to create/print an invoice?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              !Will print invoice now, so need to save the record#
              Do GetJOBSE
              Access:JOBSE.Update()
              Access:JOBS.Update()
      
              Set(defaults)
              access:defaults.next()
      
              If CanJobInvoiceBePrinted(1) = Level:Benign
      
                  JobPricingRoutine(0)
      
                  Access:JOBSE.Update() !Save again incase the costs have changed
                  Access:JOBS.Update() !Save again incase the costs have changed
      
                  PrintInvoice# = 0
                  If IsJobInvoiced(job:Invoice_Number) = Level:Benign
                      If CreateInvoice() = Level:Benign
                          PrintInvoice# = 1
                      End !If CreateInvoice()
                  Else !If job:Invoice_Number = 0
                      PrintInvoice# = 1
                  End !If job:Invoice_Number = 0
      
                  If PrintInvoice# = 1
      !                glo:Select1 = job:Invoice_Number
      !! Changing (DBH 25/05/2007) # 9024 - Allow to print normal invoices
      !!                IF glo:webjob
      !!                  Vodacom_Delivery_Note_Web
      !!                ELSE
      !!                  Single_Invoice
      !!                END
      !! to (DBH 25/05/2007) # 9024
      !                If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !                    If glo:WebJob
      !                        Vodacom_Delivery_Note_Web('','')
      !                    Else ! If glo:WebJob
      !                        Vodacom_Delivery_Note
      !                    End ! If glo:WebJob
      !                Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !                    Single_Invoice('','')
      !                End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !! End (DBH 25/05/2007) #9024
      !                glo:Select1 = 0
                      VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
      
                  End !If PrintInvoice# = 1
      
              End !If CanInvoiceBePrinted(1) = Level:Benign
              Do SetJOBSE
      
              !Do not allow to cancel out of the job -  (DBH: 12-01-2004)
              ?Cancel{prop:Disable} = 1
      
          Of 1 ! No Button
      End ! Case Missive
    OF ?ColourKey
      ThisWindow.Update
      !If tmp:UseRapidStockAllocation = 1
          PartsKey
      !Else !GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ! Don't see Vodacom would need to see the original colour key (DBH: 01-04-2005)
      !    PartsKeyNormal
      !End !GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF ?Button:ResendXML
      ThisWindow.Update
      Do ResendXML
    OF ?Button:ContactHistoryFilled
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:select12    = ''
      DO CheckContactHistory
    OF ?job:Location
      ! Amend Location Levels
      If ~0{prop:acceptall}
          If job:location <> location_temp
              error# = 0
      
              If error# = 0
                  If location_temp <> ''
              !Add To Old Location
                      access:locinter.clearkey(loi:location_key)
                      loi:location = location_temp
                      If access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces+= 1
                              access:locinter.update()
                          End
                      end !if
                  End
              !Take From New Location
                  access:locinter.clearkey(loi:location_key)
                  loi:location = job:location
                  If access:locinter.fetch(loi:location_key) = Level:Benign
                      If loi:allocate_spaces = 'YES'
                          loi:current_spaces -= 1
                          If loi:current_spaces< 1
                              loi:location_available = 'NO'
                          End
                          access:locinter.update()
                      End
                  end !if
      
                  !Write To Log
                  If Access:LOCATLOG.PrimeRecord() = Level:Benign
                      lot:RefNumber           = job:Ref_Number
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password    = glo:Password
                      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                          !Found
                          lot:UserCode    = use:User_code
                      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
                      lot:PreviousLocation    = Location_temp
                      lot:NewLocation         = job:Location
                      If Access:LOCATLOG.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:LOCATLOG.TryInsert() = Level:Benign
                          !Insert Failed
                      End !If Access:LOCATLOG.TryInsert() = Level:Benign
                  End !If Access:LOCATLOG.PrimeRecord() = Level:Benign
      
                  location_temp  = job:location
      
              End!If job:date_completed <> ''
      
          End
      End!If ~0{prop:acceptall}
    OF ?Fault_Codes
      ThisWindow.Update
      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0      !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0
      
      !If it's a completed warranty job, that hasn't been rejected
      !don't allow access to the Out Faults - 3712 (DBH: 07-01-2004)
      save:Preview = glo:Preview
      If job:Warranty_Job = 'YES' And glo:WebJob
          If job:Date_Completed <> ''
              If wob:EDI <> 'EXC'
                  glo:Preview = 'V'
              End !If wob:EDI <> 'EXC'
          End !If job:Date_Completed <> ''
      End !job:Warranty_Job = 'YES'
      
      !If the Out Faults aren't read-only already, check
      !to see if the user has access to change them - 4027 (DBH: 26-04-2004)
      If job:Date_Completed <> ''
          If glo:Preview <> 'V'
              If SecurityCheck('OUT FAULTS - AMEND')
                  glo:Preview = 'V'
              End !If SecurityAccess('OUT FAULTS - AMEND')
          End !glo:Preview <> 'V'
      End !job:Date_Completed <> ''
    Access:JOBNOTES.Update()
    If job:Chargeable_Job = 'YES'
        JobType"    = 'C'
        ChargeType"   = job:Charge_Type
        RepairType"   = job:Repair_Type
    End !If job:Chargeable_Job = 'YES'
    If job:Warranty_Job = 'YES'
        JobType"    = 'W'
        ChargeType"   = job:Warranty_Charge_Type
        RepairType"   = job:Repair_Type_Warranty
    End !If job:Warranty_Job = 'YES'



    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found
        ! Have to reget the webjob file, unfortunately this means the changes cannot be cancelled - TrkBs: 5904 (DBH: 23-09-2005)

    Else ! If Access:WEBJOB.Tryfetch(wbo:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wbo:RefNumberKey) = Level:Benign


    Clear(PassFaultCodeQueue)
    Free(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 1
    PassFaultCodeQueue.FaultCode = job:Fault_Code1
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 2
    PassFaultCodeQueue.FaultCode = job:Fault_Code2
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 3
    PassFaultCodeQueue.FaultCode = job:Fault_Code3
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 4
    PassFaultCodeQueue.FaultCode = job:Fault_Code4
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 5
    PassFaultCodeQueue.FaultCode = job:Fault_Code5
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 6
    PassFaultCodeQueue.FaultCode = job:Fault_Code6
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 7
    PassFaultCodeQueue.FaultCode = job:Fault_Code7
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 8
    PassFaultCodeQueue.FaultCode = job:Fault_Code8
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 9
    PassFaultCodeQueue.FaultCode = job:Fault_Code9
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 10
    PassFaultCodeQueue.FaultCode = job:Fault_Code10
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 11
    PassFaultCodeQueue.FaultCode = job:Fault_Code11
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 12
    PassFaultCodeQueue.FaultCode = job:Fault_Code12
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 13
    PassFaultCodeQueue.FaultCode = wob:FaultCode13
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 14
    PassFaultCodeQueue.FaultCode = wob:FaultCode14
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 15
    PassFaultCodeQueue.FaultCode = wob:FaultCode15
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 16
    PassFaultCodeQueue.FaultCode = wob:FaultCode16
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 17
    PassFaultCodeQueue.FaultCode = wob:FaultCode17
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 18
    PassFaultCodeQueue.FaultCode = wob:FaultCode18
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 19
    PassFaultCodeQueue.FaultCode = wob:FaultCode19
    Add(PassFaultCodeQueue)
    PassFaultCodeQueue.FaultNumber = 20
    PassFaultCodeQueue.FaultCode = wob:FaultCode20
    Add(PassFaultCodeQueue)

    JobFaultCodes(ThisWindow.Request, JobType", ChargeType", RepairType", PassFaultCodeQueue)

    Loop x# = 1 To Records(PassFaultCodeQueue)
        Get(PassFaultCodeQueue,x#)
        Case PassFaultCodeQueue.FaultNumber
        Of 1
            job:Fault_Code1 = Clip(PassFaultCodeQueue.FaultCode)
        Of 2
            job:Fault_Code2 = Clip(PassFaultCodeQueue.FaultCode)
        Of 3
            job:Fault_Code3 = Clip(PassFaultCodeQueue.FaultCode)
        Of 4
            job:Fault_Code4 = Clip(PassFaultCodeQueue.FaultCode)
        Of 5
            job:Fault_Code5 = Clip(PassFaultCodeQueue.FaultCode)
        Of 6
            job:Fault_Code6 = Clip(PassFaultCodeQueue.FaultCode)
        Of 7
            job:Fault_Code7 = Clip(PassFaultCodeQueue.FaultCode)
        Of 8
            job:Fault_Code8 = Clip(PassFaultCodeQueue.FaultCode)
        Of 9
            job:Fault_Code9 = Clip(PassFaultCodeQueue.FaultCode)
        Of 10
            job:Fault_Code10 = Clip(PassFaultCodeQueue.FaultCode)
        Of 11
            job:Fault_Code11 = Clip(PassFaultCodeQueue.FaultCode)
        Of 12
            job:Fault_Code12 = Clip(PassFaultCodeQueue.FaultCode)
        Of 13
            wob:FaultCode13 = Clip(PassFaultCodeQueue.FaultCode)
        Of 14
            wob:FaultCode14 = Clip(PassFaultCodeQueue.FaultCode)
        Of 15
            wob:FaultCode15 = Clip(PassFaultCodeQueue.FaultCode)
        Of 16
            wob:FaultCode16 = Clip(PassFaultCodeQueue.FaultCode)
        Of 17
            wob:FaultCode17 = Clip(PassFaultCodeQueue.FaultCode)
        Of 18
            wob:FaultCode18 = Clip(PassFaultCodeQueue.FaultCode)
        Of 19
            wob:FaultCode19 = Clip(PassFaultCodeQueue.FaultCode)
        Of 20
            wob:FaultCode20 = Clip(PassFaultCodeQueue.FaultCode)
        End ! Case PassFaultCodeQueue.FaultNumber
    End ! Loop x# = 1 To Records(PassFaultCodeQueue)

    Access:WEBJOB.Update()
    Access:JOBSE.Update()

!    Clear(glo:FaultCodeGroup)
!    glo:FaultCode1 = job:Fault_Code1
!    glo:FaultCode2 = job:Fault_Code2
!    glo:FaultCode3 = job:Fault_Code3
!    glo:FaultCode4 = job:Fault_Code4
!    glo:FaultCode5 = job:Fault_Code5
!    glo:FaultCode6 = job:Fault_Code6
!    glo:FaultCode7 = job:Fault_Code7
!    glo:FaultCode8 = job:Fault_Code8
!    glo:FaultCode9 = job:Fault_Code9
!    glo:FaultCode10 = job:Fault_Code10
!    glo:FaultCode11 = job:Fault_Code11
!    glo:FaultCode12 = job:Fault_Code12
!    glo:FaultCode13 = wob:FaultCode13
!    glo:FaultCode14 = wob:FaultCode14
!    glo:FaultCode15 = wob:FaultCode15
!    glo:FaultCode16 = wob:FaultCode16
!    glo:FaultCode17 = wob:FaultCode17
!    glo:FaultCode18 = wob:FaultCode18
!    glo:FaultCode19 = wob:FaultCode19
!    glo:FaultCode20 = wob:FaultCode20
!    GenericFaultCodes(job:Ref_Number,JobType",ThisWindow.Request,job:account_Number,job:Manufacturer,|
!                        ChargeType",RepairType",job:Date_Completed)
!    job:Fault_Code1   = glo:FaultCode1
!    job:Fault_Code2   = glo:FaultCode2
!    job:Fault_Code3   = glo:FaultCode3
!    job:Fault_Code4   = glo:FaultCode4
!    job:Fault_Code5   = glo:FaultCode5
!    job:Fault_Code6   = glo:FaultCode6
!    job:Fault_Code7   = glo:FaultCode7
!    job:Fault_Code8   = glo:FaultCode8
!    job:Fault_Code9   = glo:FaultCode9
!    job:Fault_Code10  = glo:FaultCode10
!    job:Fault_Code11  = glo:FaultCode11
!    job:Fault_Code12  = glo:FaultCode12
!    wob:FaultCode13   = glo:FaultCode13
!    wob:FaultCode14   = glo:FaultCode14
!    wob:FaultCode15   = glo:FaultCode15
!    wob:FaultCode16   = glo:FaultCode16
!    wob:FaultCode17   = glo:FaultCode17
!    wob:FaultCode18   = glo:FaultCode18
!    wob:FaultCode19   = glo:FaultCode19
!    wob:FaultCode20   = glo:FaultCode20
!    Clear(glo:FaultCodeGroup)
!    Access:WEBJOB.Update()
!    Access:JOBSE.Update()

    do ValidateDateCode
    PromptForExcFlag = 0
    TempFaultCode = job:Fault_Code1
    TempFieldNo = 1
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code2
    TempFieldNo = 2
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code3
    TempFieldNo = 3
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code4
    TempFieldNo = 4
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code5
    TempFieldNo = 5
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code6
    TempFieldNo = 6
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code7
    TempFieldNo = 7
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code8
    TempFieldNo = 8
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code9
    TempFieldNo = 9
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code10
    TempFieldNo = 10
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code11
    TempFieldNo = 11
    do CheckExchangePrompt
    TempFaultCode = job:Fault_Code12
    TempFieldNo = 12
    do CheckExchangePrompt
    IF PromptForExcFlag THEN
        Case Missive('You have requested an Exchange Unit for this repair.'&|
          '<13,10>'&|
          '<13,10>Do you wish to continue and PROCESS this request, or CANCEL  it?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|/Process')
            Of 2 ! Process Button
                IF job:Chargeable_Job = 'YES' AND job:Warranty_Job = 'YES'
                    Case Missive('Is this a Warranty or Chargeable Exchange?','ServiceBase 3g',|
                                   'mquest.jpg','Chargeable|Warranty')
                        Of 2 ! Warranty Button
                           If job:Engineer <> ''
                              Access:USERS.Clearkey(use:User_Code_Key)
                              use:User_Code   = job:Engineer
                              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                 !Found
                              Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                              End !If Access:USERS.Tryfetch(use:User_Code_Key) = LevelBenign
                           Else !job:Engineer <> ''
                               Access:USERS.Clearkey(use:Password_Key)
                               use:Password    = glo:Password
                               If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                  !Found
                               Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                   !Error
                               End !If AccessUSERS.Tryfetch(usePassword_Key) = LevelBenign
                           End !job:Engineer <> ''
                           Access:STOCK.Clearkey(sto:Location_Key)
                           sto:Location    = use:Location
                           sto:Part_Number = 'EXCH'
                           If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                              !Found
                              Local.AllocateExchangePart('WAR',0,0)
                              BRW10.RESETSORT(1)
                           Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                               !Error
                                Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                           End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                        Of 1 ! Chargeable Button
                           Access:USERS.Clearkey(use:User_Code_Key)
                           use:User_Code   = job:Engineer
                           If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                              !Found
                              Access:STOCK.Clearkey(sto:Location_Key)
                              sto:Location    = use:Location
                              sto:Part_Number = 'EXCH'
                              If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                 !Found
                                 Local.AllocateExchangePart('CHA',0,0)
                                 BRW9.RESETSORT(1)
                              Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                  !Error
                                    Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                              End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                           Else ! If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                               !Error
                           End !If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                    End ! Case Missive
                 ELSE
                     Access:USERS.Clearkey(use:User_Code_Key)
                     use:User_Code   = job:Engineer
                     If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = 'EXCH'
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                           !Found
                           Local.AllocateExchangePart('CHA',0,0)
                           BRW9.RESETSORT(1)
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                     Else ! If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                         !Error
                     End !If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                 END !IF job:Chargeable_Job = 'YES' AND job:Warranty_Job = 'YES'
            Of 1 ! Cancel Button
        End ! Case Missive
    END !IF PromptForExhFlag
      Do AfterOutFaultList
      !Reset the default glo:Preview setting - 3712 (DBH: 07-01-2004)
      glo:Preview = save:Preview
    OF ?ViewAccessories
      ThisWindow.Update
          glo:select1  = job:model_number
          glo:select2  = job:ref_number
      
          Browse_job_accessories
      
          glo:select1  = ''
          glo:select2  = ''
          Do GETJOBSE
    OF ?EngineerHistory
      ThisWindow.Update
      BrowseEngineersOnJob(Job:Ref_Number)
      ThisWindow.Reset
    OF ?Insert:2
      ThisWindow.Update
     !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)

      !If tmp:Booking48HourOption <> 0 !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0


      If job:Model_Number = ''
          Case Missive('You must select a Model Number before you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Model_Number = ''

      If job:Charge_Type = ''
          Case Missive('You must select a Charge Type before you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Charge_Type = ''

      Error# = 0
      If job:Engineer = ''
          Case Missive('You must select an engineer before you can add parts to the job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !If job:Engineer = ''
      ! #11762 Repair Type does not allow parts.
      ! Check if there are an (Bryan: 24/11/2010)
      if (RepairTypesNoParts(job:Chargeable_Job, |
                                 job:Warranty_Job,   |
                                 job:Manufacturer,   |
                                 job:Repair_Type,    |
                                 job:Repair_Type_Warranty))
            Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            cycle
      end

!      !added by Paul 01/11/2010 - Log no 11762
!      !check to see if parts are allowed with the selected repair type
!      If job:Chargeable_Job = 'YES' Then
!          !check chargeable repair type
!          access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
!          rtd:Manufacturer    = job:Manufacturer
!          rtd:Repair_Type     = job:Repair_Type
!          If Access:ReptyDef.fetch(rtd:ManRepairTypeKey) = level:benign then
!              if rtd:NoParts = 'Y' then
!                  Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
!                                 'mstop.jpg','/OK')
!                      Of 1 ! OK Button
!                  End ! Case Missive
!                  cycle
!              End
!          End
!      ElsIf job:Warranty_Job  = 'YES' then
!          access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
!          rtd:Manufacturer    = job:Manufacturer
!          rtd:Repair_Type     = job:Repair_Type_Warranty
!          If Access:RepTyDef.Fetch(rtd:ManRepairTypeKey) = level:benign then
!              If rtd:NoParts = 'Y' then
!                  Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
!                                 'mstop.jpg','/OK')
!                      Of 1 ! OK Button
!                  End ! Case Missive
!                  cycle
!              End
!          End
!      End
      display(?job:Repair_Type)   ! Repair type may have changed
    OF ?Multi_Insert
      ThisWindow.Update
!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!
!        debx:TheLine = clip(job:Ref_Number)&' Chargeable Insert selected'&Format(today(),@d06)&' '&format(Clock(),@t4)
!        Add(DebugExport)
!      END

      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0      !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0

      If job:Model_Number = ''
          Case Missive('You must select a Model Number before you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Model_Number = ''

      If job:Charge_Type = ''
          Case Missive('You must select a Charge Type before you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Charge_Type = ''

      If IsJobInvoiced(job:Invoice_Number)
          Case Missive('You cannot insert parts onto an invoiced jobs.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Invoice_Number <> 0


!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&' Debug one before No Part check'
!        Add(DebugExport)
!      END



      ! #11762 Repair Type does not allow parts.
      ! Check if there are an (Bryan: 24/11/2010)
      if (RepairTypesNoParts(job:Chargeable_Job, |
                                 job:Warranty_Job,   |
                                 job:Manufacturer,   |
                                 job:Repair_Type,    |
                                 job:Repair_Type_Warranty))
            Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            cycle
      end


!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&' Debug two after No Part check'
!        Add(DebugExport)
!      END


      Error# = 0
      If job:Engineer = ''
          Case Missive('You must select an engineer before you can insert parts on the job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          cycle
      ELSE
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              If glo:WebJob
                  !The engineer on the job, should match the location of the job -  (DBH: 06-08-2003)
                  If use:Location = tmp:ARCSiteLocation
                      Case Missive('The ARC Engineer attached to this job must be removed before you can insert any parts.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  End !If use:Location = tmp:ARCSiteLocation
              Else !If glo:WebJob
                  If use:Location <> tmp:ARCSiteLocation
                      Case Missive('The RRC Engineer attached to this job must be removed before you can insert any parts.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  End !If use:Location <> tmp:ARCSiteLocation
              End !If glo:WebJob
          Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              Case Missive('Error! Cannot find the details for the engineer attached to this job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      End !job:Engineer <> ''

!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&' Debug three after location check - error running at '&error#
!        Add(DebugExport)
!      END


      If Error# = 0
          Access:LOCATION.Clearkey(loc:Location_Key)
          if glo:RelocateStore then
            loc:Location = 'MAIN STORE'
          ELSE
            loc:Location    = use:Location
          END
          If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Case Missive('Error! Stock for this job will be picked from location ' & Clip(loc:Location) & ' by default.'&|
                    '<13,10>'&|
                    '<13,10>This location is NOT active and cannot be used.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Error# = 1
              End !If loc:Active = 0
          Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
              !Error
          End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign

          If Error# = 0
              Required# = 0
              If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
                              job:Charge_Type,job:Warranty_Charge_Type,|
                              job:Repair_Type,job:Repair_Type_Warranty,'C')

                  save_map_id = access:manfaupa.savefile()
                  access:manfaupa.clearkey(map:field_number_key)
                  map:manufacturer = job:manufacturer
                  set(map:field_number_key,map:field_number_key)
                  loop
                      if access:manfaupa.next()
                         break
                      end !if
                      if map:manufacturer <> job:manufacturer      |
                          then break.  ! end if
                      If MAP:Compulsory = 'YES'
                          required# = 1
                          Break
                      End!If MAP:Compulsory = 'YES'
                  end !loop
                  access:manfaupa.restorefile(save_map_id)

              End
              If Required#
                  Post(event:accepted,?Insert)
              Else!If required# = 1

                  Set(DEFAULTS)
                  Access:DEFAULTS.Next()
                  glo:select11 = job:manufacturer
                  glo:select12 = job:model_number
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  pick_model_stock(use:User_Code)
                  glo:select11 = ''
                  glo:select12 = ''
                  if globalresponse = requestcompleted
                      If Records(glo:Queue)
                          Loop x# = 1 To Records(glo:Queue)
                              Get(glo:Queue,x#)
                              access:stomodel.clearkey(stm:model_number_key)
                              stm:ref_number   = glo:pointer
                              stm:manufacturer = job:manufacturer
                              stm:model_number = job:model_number
                              if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                                  access:stock.clearkey(sto:ref_number_key)
                                  sto:ref_number = stm:ref_number
                                  if access:stock.fetch(sto:ref_number_key) = Level:Benign
                                      !Ignore EXCHANGE PART
                                      If sto:Part_Number = 'EXCH'
                                          Cycle
                                      End !If sto:Part_Number = 'EXCH'

                                      if sto:ExchangeUnit = 'YES'
                                          Case Missive('Error! An exchange unit must be picked from exchange stock. (' & Clip(sto:Part_Number) & ')','ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      end

                                      If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                          Case Missive('Error! Your skill level is not high enough to use the part ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.','ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      Else !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                          If sto:sundry_item = 'YES'
                                              access:parts.clearkey(par:part_number_key)
                                              par:ref_number  = job:ref_number
                                              par:part_number = sto:part_number
                                              if access:parts.fetch(par:part_number_key)
                                                  Do Add_Chargeable_Part

                                                  AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)
                                              End!if access:parts.fetch(par:part_number_key)
                                          Else!If sto:sundry_item = 'YES'
                                              If sto:quantity_stock < 1
                                                  If SecurityCheck('JOBS - ORDER PARTS')
                                                      Case Missive(Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                                        '<13,10>'&|
                                                        '<13,10>This part is out of stock.','ServiceBase 3g',|
                                                                     'mstop.jpg','/OK')
                                                          Of 1 ! OK Button
                                                      End ! Case Missive
                                                  Else !If SecurityCheck('JOBS - ORDER PARTS')
                                                      ! #10396 Not sure how this could happen. But stop using part that is suspended. (DBH: 16/07/2010)
                                                      If (MainStoreSuspended(sto:Part_Number))
                                                          Beep(Beep:SystemHand)  ;  Yield()
                                                          Case Missive('This part is out of stock. It has been suspended at Main Store and therefore cannot be ordered.','ServiceBase',|
                                                                         'mstop.jpg','/&OK')
                                                          Of 1 ! &OK Button
                                                          End!Case Message
                                                      else ! If (MainStoreSuspended(sto:Part_Number))
                                                          Case Missive('' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                                            '<13,10>'&|
                                                            '<13,10>This part is out of stock. Do you wish to place it on Back Order?','ServiceBase 3g',|
                                                                         'mquest.jpg','\No|/Yes')
                                                              Of 2 ! Yes Button
                                                                  If VirtualSite(sto:Location)
                                                                      Do Add_Chargeable_Part
                                                                      par:WebOrder = TRUE
                                                                      CreateWebOrder('C',1)
                                                                      Access:PARTS.Update()
                                                                      AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                                                                  Else !If VirtualSite(sto:Location)
                                                                      Case def:SummaryOrders
                                                                          Of 0 !Old Way
                                                                              Do MakePartsOrder
                                                                              Do Add_Chargeable_Part
                                                                              par:Pending_Ref_number  = ope:Ref_Number
                                                                              par:Date_Ordered    = ''
                                                                              Access:PARTS.Update()
                                                                          Of 1 !New Way
                                                                              Do Add_Chargeable_Part
                                                                              Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                                                              ope:Supplier    = par:Supplier
                                                                              ope:Part_Number = par:Part_Number
                                                                              If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                                                  ope:Quantity    += 1
                                                                                  Access:ORDPEND.Update()
                                                                              Else !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                                                  Do MakePartsOrder
                                                                              End !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                                              par:Pending_Ref_Number  = ope:Ref_Number
                                                                              par:Requested   = True
                                                                              par:Date_Ordered = ''
                                                                              Access:PARTS.Update()

                                                                      End !Case def:SummaryOrders
                                                                  End !Else !If VirtualSite(sto:Location)
                                                              Of 1 ! No Button
                                                          End ! Case Missive
                                                      End ! If (MainStoreSuspended(sto:Part_Number))
                                                  End !If SecurityCheck('JOBS - ORDER PARTS')
                                              Else!If sto:quantity_stock < 1
                                                  access:parts.clearkey(par:part_number_key)
                                                  par:ref_number  = job:ref_number
                                                  par:part_number = sto:part_number
                                                  if access:parts.fetch(par:part_number_key)
                                                      Do Add_Chargeable_Part
                                                      sto:quantity_stock -= 1
                                                      If sto:quantity_stock < 0
                                                          sto:quantity_stock = 0
                                                      End!If sto:quantity_stock < 0
                                                      access:stock.update()
                                                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                         'DEC', | ! Transaction_Type
                                                                         '', | ! Depatch_Note_Number
                                                                         job:Ref_Number, | ! Job_Number
                                                                         0, | ! Sales_Number
                                                                         1, | ! Quantity
                                                                         sto:Purchase_Cost, | ! Purchase_Cost
                                                                         sto:Sale_Cost, | ! Sale_Cost
                                                                         sto:Retail_Cost, | ! Retail_Cost
                                                                         'STOCK DECREMENTED', | ! Notes
                                                                         '') ! Information
                                                        ! Added OK
                                                      Else ! AddToStockHistory
                                                        ! Error
                                                      End ! AddToStockHistory
                                                      ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
                                                      SuspendedPartCheck()
                                                      !Add to Rapid Stock Allocation - L873 (DBH: 22-07-2003)
                                                      AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)
                                                  End!if access:parts.fetch(par:part_number_key)
                                              End!If sto:quantity_stock < 1
                                          End!If sto:sundry_item = 'YES'
                                      End !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                  end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                              end!if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:Queue)
                      End!If Records(glo:Queue)
                  End!if globalresponse = requestcompleted
              End !If required# = 1
              globalrequest     = saverequest#
          End !If Error# = 0
      Else
          Case Missive('Unable to find the User''s Details.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      End !If Error# = 0

!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine =clip(job:Ref_Number)&' Debug Four after long check '
!        Add(DebugExport)
!      END
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
      Do CountParts
    OF ?ChargeableAdjustment
      ThisWindow.Update
      If IsJobInvoiced(job:Invoice_Number)
          Case Missive('You cannot insert parts onto an invoiced jobs.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Invoice_Number <> 0
      ! #11762 Repair Type does not allow parts.
      ! Check if there are an (Bryan: 24/11/2010)
      if (RepairTypesNoParts(job:Chargeable_Job, |
                                 job:Warranty_Job,   |
                                 job:Manufacturer,   |
                                 job:Repair_Type,    |
                                 job:Repair_Type_Warranty))
            Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            cycle
      end

      Case Missive('Are you sure you want to add a Chargeable ADJUSTMENT?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button

          get(parts,0)
          if access:parts.primerecord() = level:benign
              par:ref_number            = job:ref_number
              par:adjustment            = 'YES'
              par:part_number           = 'ADJUSTMENT'
              par:description           = 'ADJUSTMENT'
              par:quantity              = 1
              par:warranty_part         = 'NO'
              par:exclude_from_order    = 'YES'
              if access:parts.insert()
                  access:parts.cancelautoinc()
              end
          end!if access:parts.primerecord() = level:benign
          Of 1 ! No Button
      End ! Case Missive

      BRW9.ResetSort(1)
      Do CountParts
    OF ?ExchangeButton
      ThisWindow.Update
      Do ChargeableExchangeButton
      BRW9.ResetSort(1)
      Do Enable_Disable
    OF ?Multi_Insert:2
      ThisWindow.Update
!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&'( Warranty insert selected '&format(today(),@d06)&' '&format(clock(),@t4)
!        Add(DebugExport)
!      END

      !Has the booking option been set?
      !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
      !If tmp:Booking48HourOption <> 0  !TB13298 - remove reliance on Booking Option - always error on Engineer Option
          If tmp:Engineer48HourOption = 0
              Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If tmp:Engineer48HourOption = 0
      !End !tmp:Booking48HourOption <> 0


      If job:Model_Number = ''
          Case Missive('You must select a Model Number before you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Model_Number = ''

      If job:Warranty_Charge_Type = ''
          Case Missive('You must select a Warranty Charge Type before  you can insert any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Charge_Type = ''

      !Do not add parts to completed warranty job - L945 (DBH: 03-09-2003)
      If job:Date_Completed <> 0
          Case Missive('You cannot insert parts onto a Completed warranty job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Date_Completed <> 0


      If job:Invoice_Number_Warranty <> 0
          Case Missive('You cannot insert parts onto an invoiced job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Invoice_Number <> 0

      !Are Parts Codes Required?
      Error# = 0
      If job:Engineer = ''
          Case Missive('You must select an engineer before you can add parts to the job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          cycle
      ELSE
          If Local.CorrectEngineer() = False
              Cycle
          End !If Local.CorrectEngineer() = False
      End !job:Engineer <> ''

!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&' Debug two after engineer check '
!        Add(DebugExport)
!      END


      ! #11762 Repair Type does not allow parts.
      ! Check if there are an (Bryan: 24/11/2010)
      if (RepairTypesNoParts(job:Chargeable_Job, |
                                 job:Warranty_Job,   |
                                 job:Manufacturer,   |
                                 job:Repair_Type,    |
                                 job:Repair_Type_Warranty))
            Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            cycle
      end

!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&' Debug three after location check - error running at '&error#
!        Add(DebugExport)
!      END

      If Error# = 0
          Access:LOCATION.Clearkey(loc:Location_Key)
          loc:Location    = use:Location
          If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Case Missive('Error! Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                    '<13,10>'&|
                    '<13,10>This location is NOT active and cannot be used.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Error# = 1
              End !If loc:Active = 0
          Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
              !Error
          End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign

          If Error# = 0

              Required# = False
              FoundCodes# = False

              Save_map_ID = Access:MANFAUPA.SaveFile()
              Access:MANFAUPA.Clearkey(map:Field_Number_Key)
              map:Manufacturer = job:Manufacturer
              Set(map:Field_Number_Key,map:Field_Number_Key)
              Loop ! Begin MANFAUPA Loop
                  If Access:MANFAUPA.Next()
                      Break
                  End ! If !Access
                  If map:Manufacturer <> job:Manufacturer
                      Break
                  End ! If
                  FoundCodes# = True
                  If map:Compulsory = 'YES'
                      Required# = True
                      Break
                  End ! If map:Compulsory = 'YES'
              End ! End MANFAUPA Loop
              Access:MANFAUPA.RestoreFile(Save_map_ID)

              If Required# = True
                  If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
                      job:Charge_Type,job:Warranty_Charge_Type,|
                      job:Repair_Type,job:Repair_Type_Warranty,'W')
                  Else ! If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
                      Required# = False
                  End ! If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
              End ! If Required# = True

              ! Start - Fault codes are used on this part. Check to see if the user just wants to "correct" the part - TrkBs: 5365 (DBH: 26-04-2005)
              glo:Select1 = ''
              If FoundCodes# = True
                  Case Missive('Do you wish to add a NEW part(s) or CORRECT an existing part(s)?','ServiceBase 3g',|
                                 'mquest.jpg','\Cancel|Correct|Add')
                      Of 3 ! Add Button
                      Of 2 ! Correct Button
                          glo:Select1 = 'CORRECT PART'
                      Of 1 ! Cancel Button
                          Cycle
                  End ! Case Missive
              End ! If FoundCodes# = True
              ! End   - Fault codes are used on this part. Check to see if the user just wants to "correct" the part - TrkBs: 5365 (DBH: 26-04-2005)

!              !TB12905 - JC 10/12/12
!              if DebugLogging then
!                debx:TheLine = clip(job:Ref_Number)&' Debug three.one after message - required running at '&required#
!                Add(DebugExport)
!              END


              If Required# = True
                  Post(event:accepted,?Insert:3)
              Else ! If Required# = True

                  glo:select11 = job:manufacturer
                  glo:select12 = job:model_number
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  pick_model_stock(use:User_Code)
                  glo:select11 = ''
                  glo:select12 = ''
                  if globalresponse = requestcompleted
                      If Records(glo:Queue)
                          Loop x# = 1 To Records(glo:Queue)
                              Get(glo:Queue,x#)
                              access:stomodel.clearkey(stm:model_number_key)
                              stm:ref_number   = glo:pointer
                              stm:manufacturer = job:manufacturer
                              stm:model_number = job:model_number
                              if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                                  access:stock.clearkey(sto:ref_number_key)
                                  sto:ref_number = stm:ref_number
                                  if access:stock.fetch(sto:ref_number_key) = Level:Benign
                                      !Ignore EXCHANGE PART
                                      If sto:Part_Number = 'EXCH'
                                          Cycle
                                      End !If sto:Part_Number = 'EXCH'

                                      ! Check if the part can be used on Level1/2 Repairs, for Nokia - TrkBs: 5904 (DBH: 23-09-2005)
                                      If job:Manufacturer = 'NOKIA'
                                          If sto:ExcludeLevel12Repair = 1 And (job:Repair_Type_Warranty = 'LEVEL 1' or job:Repair_Type_Warranty = 'LEVEL 2')
                                             Case Missive('Error! This part cannot be used on Level 1/2 Repairs:|'&|
                                              Clip(sto:Part_Number) & ' - ' & CLip(sto:Description) & '.','ServiceBase 3g',|
                                                            'mstop.jpg','/OK')
                                                 Of 1 ! OK Button
                                             End ! Case Missive
                                             Cycle
                                          End ! If sto:ExcludeLevel12Repair = 1
                                      End ! If job:Manufacturer = 'NOKIA'

                                      if sto:ExchangeUnit = 'YES'
                                          Case Missive('Error: An exchange unit must be picked from exchange stock. (' & Clip(sto:Part_Number) & ').','ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      end

                                      !Added By Paul 03/07/2009 Log No
                                      If sto:ChargeablePartOnly = True then
                                          Case Missive('The following part could not be added, it is a chargeable only part:|' & Clip(sto:Part_Number) & ' ' & clip(sto:Description),'ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      End
                                      !End Change

                                      If use:RestrictParts And use:RestrictWarranty And sto:SkillLevel > use:SkillLevel
                                          Case Missive('Error! Your skill level is not high enough to use the part ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.','ServiceBase 3g',|
                                                         'mstop.jpg','/OK')
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      Else !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                          If sto:Accessory = 'YES'
                                              If Records(glo:Queue) > 1
                                                  Case Missive('Warranty Accessories can only be added individually.','ServiceBase 3g',|
                                                                 'mstop.jpg','/OK')
                                                      Of 1 ! OK Button
                                                  End ! Case Missive
                                                  Cycle

                                              End !If Records(glo:Queue) > 1
                                              If Records(glo:Queue) = 1
                                                  !Do Warranty Check on Accessory

                                              End !If Records(glo:Queue) = 1
                                          End !If sto:Accessory = 'YES' And Records(glo:Queue) > 1

                                          If sto:sundry_item = 'YES' Or glo:Select1 = 'CORRECT PART'
                                              access:warparts.clearkey(wpr:part_number_key)
                                              wpr:ref_number  = job:ref_number
                                              wpr:part_number = sto:part_number
                                              if access:warparts.fetch(wpr:part_number_key)
                                                  Do Add_Warranty_Part
                                                  AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'',job:Engineer)
                                              End!if access:warparts.fetch(wpr:part_number_key)
                                          Else!If sto:sundry_item <> 'YES'
                                              If sto:quantity_stock < 1
                                                  If SecurityCheck('JOBS - ORDER PARTS')
                                                      Case Missive(Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                                        '<13,10>'&|
                                                        '<13,10>This part is out of stock.','ServiceBase 3g',|
                                                                     'mstop.jpg','/OK')
                                                          Of 1 ! OK Button
                                                      End ! Case Missive
                                                  Else !If SecurityCheck('JOBS - ORDER PARTS')
                                                      ! #10396 Not sure how this could happen. But stop using part that is suspended. (DBH: 16/07/2010)
                                                      If (MainStoreSuspended(sto:Part_Number))
                                                          Beep(Beep:SystemHand)  ;  Yield()
                                                          Case Missive('This part is out of stock. It has been suspended at Main Store and therefore cannot be ordered.','ServiceBase',|
                                                                         'mstop.jpg','/&OK')
                                                          Of 1 ! &OK Button
                                                          End!Case Message
                                                      else ! If (MainStoreSuspended(sto:Part_Number))
                                                          Case Missive('' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                                            '<13,10>'&|
                                                            '<13,10>This part is out of stock. Do you wish to place it on Back Order?','ServiceBase 3g',|
                                                                         'mquest.jpg','\No|/Yes')
                                                              Of 2 ! Yes Button
                                                                  If VirtualSite(sto:Location)
                                                                      !If its not a Rapid site, turn part allocated off
                                                                      Case Local.AccessoryOutOfWarranty()
                                                                          Of 1 !Chargeable Only
                                                                              Do Add_Chargeable_Part
                                                                              par:WebOrder = 1
                                                                              CreateWebOrder('C',1)
                                                                              Access:PARTS.Update()
                                                                              AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)

                                                                              If job:Chargeable_Job <> 'YES'
                                                                                  job:Chargeable_Job = 'YES'
                                                                              End !If job:Chargeable_Job <> 'YES'
                                                                              job:Warranty_job = 'NO'
                                                                          Of 2 !Split
                                                                              Do Add_Chargeable_Part
                                                                              par:WebOrder = 1
                                                                              CreateWebOrder('C',1)
                                                                              Access:PARTS.Update()
                                                                              AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)

                                                                              If job:Chargeable_Job <> 'YES'
                                                                                  job:Chargeable_Job = 'YES'
                                                                              End !If job:Chargeable_Job <> 'YES'
                                                                          Of 3 !Cancel
                                                                              !Do not continue - L943 (DBH: 02-09-2003)
                                                                              Cycle
                                                                          Else
                                                                              Do Add_Warranty_Part
                                                                              wpr:WebOrder = 1
                                                                              CreateWebOrder('W',1)
                                                                              Access:WARPARTS.Update()
                                                                              AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'WEB',job:Engineer)
                                                                      End !Case Local.AccessoryOutOfWarranty
                                                                  Else !If VirtualSite(sto:Location)
                                                                      wpr:PartAllocated = 1
                                                                      Case def:SummaryOrders
                                                                          Of 0 !Old Way
                                                                              Do MakeWarPartsOrder
                                                                              Do Add_Warranty_Part
                                                                              wpr:Pending_Ref_number  = ope:Ref_Number
                                                                              wpr:Date_Ordered    = ''
                                                                              Access:WARPARTS.Update()
                                                                          Of 1 !New Way
                                                                              Do Add_Warranty_Part
                                                                              Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                                                              ope:Supplier    = wpr:Supplier
                                                                              ope:Part_Number = wpr:Part_Number
                                                                              If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                                                  ope:Quantity    += 1
                                                                                  Access:ORDPEND.Update()
                                                                              Else !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                                                  Do MakeWarPartsOrder
                                                                              End !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                                              wpr:Pending_Ref_Number  = ope:Ref_Number
                                                                              wpr:Requested   = True
                                                                              wpr:Date_Ordered = ''
                                                                              Access:WARPARTS.Update()

                                                                      End !Case def:SummaryOrders

                                                                  End !If VirtualSite(sto:Location)

                                                              Of 1 ! No Button
                                                          End ! Case Missive
                                                      End ! If (MainStoreSuspended(sto:Part_Number))
                                                  End !If SecurityCheck('JOBS - ORDER PARTS')

                                              Else!If sto:quantity_stock < 1
                                                  !If its not a Rapid site, turn part allocated off
                                                  Case Local.AccessoryOutOfWarranty()
                                                      Of 1 !Chargeable Only
                                                          Do Add_Chargeable_Part
                                                          Access:PARTS.Update()
                                                          AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)

                                                          If job:Chargeable_Job <> 'YES'
                                                              job:Chargeable_Job = 'YES'
                                                          End !If job:Chargeable_Job <> 'YES'
                                                          job:Warranty_job = 'NO'
                                                      Of 2 !Split
                                                          Do Add_Chargeable_Part
                                                          Access:PARTS.Update()
                                                          AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)

                                                          If job:Chargeable_Job <> 'YES'
                                                              job:Chargeable_Job = 'YES'
                                                          End !If job:Chargeable_Job <> 'YES'
                                                      Of 3 !Cancel
                                                          !Do not continue - L943 (DBH: 02-09-2003)
                                                          Cycle
                                                      Else
                                                          Do Add_Warranty_Part
                                                          AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'',job:Engineer)
                                                  End !Case Local.AccessoryOutOfWarranty
                                                  sto:quantity_stock -= 1
                                                  If sto:quantity_stock < 0
                                                      sto:quantity_stock = 0
                                                  End!If sto:quantity_stock < 0
                                                  access:stock.update()
                                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                     'DEC', | ! Transaction_Type
                                                                     '', | ! Depatch_Note_Number
                                                                     job:Ref_Number, | ! Job_Number
                                                                     0, | ! Sales_Number
                                                                     1, | ! Quantity
                                                                     sto:Purchase_Cost, | ! Purchase_Cost
                                                                     sto:Sale_Cost, | ! Sale_Cost
                                                                     sto:Retail_Cost, | ! Retail_Cost
                                                                     'STOCK DECREMENTED', | ! Notes
                                                                     '') ! Information
                                                      ! Added OK
                                                  Else ! AddToStockHistory
                                                      ! Error
                                                  End ! AddToStockHistory
                                                  ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
                                                  SuspendedPartCheck()
                                              End!If sto:quantity_stock < 1
                                          End!If sto:sundry_item <> 'YES'
                                      End !If use:RestrictParts And use:RestrictWarranty And sto:SkillLevel > use:SkillLevel
                                  end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                              end!if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:Queue)
                      End!If Records(glo:Queue)
                  End!if globalresponse = requestcompleted
              End!If required# = 1
          End !If Error# = 0
          !This should stop the message coming up, if the Chargeble Job tick has been ticked
          !But it should still hide and unhide the fields.
          Warranty_Job_Temp = job:Warranty_Job
          Chargeable_Job_Temp = job:Chargeable_Job
          Post(Event:Accepted,?job:Chargeable_Job)
          Post(Event:Accepted,?job:Warranty_Job)
      Else
          Case Missive('Unable to find the User''s Details.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      End !Error# = 0


!      !TB12905 - JC 10/12/12
!      if DebugLogging then
!        debx:TheLine = clip(job:Ref_Number)&' Debug four after long checks '
!        Add(DebugExport)
!      END


      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
          Local.Pricing(0)
          Do CountParts
    OF ?WarrantyAdjustment
      ThisWindow.Update
      If job:Invoice_Number_Warranty <> 0
          Case Missive('You cannot insert parts onto an invoiced job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !job:Invoice_Number <> 0

      ! #11762 Repair Type does not allow parts.
      ! Check if there are an (Bryan: 24/11/2010)
      if (RepairTypesNoParts(job:Chargeable_Job, |
                                 job:Warranty_Job,   |
                                 job:Manufacturer,   |
                                 job:Repair_Type,    |
                                 job:Repair_Type_Warranty))
            Case Missive('Spares cannot be added on the current Repair Type. Please select a different Repair Type and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            cycle
      end

      Case Missive('Are you sure you want to add a Warranty ADJUSTMENT?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button

          found# = 0
          access:manufact.clearkey(man:manufacturer_key)
          man:manufacturer    = job:manufacturer
          If access:manufact.tryfetch(man:manufacturer_key) = Level:benign
              If man:includeadjustment = 'YES' and man:adjustpart = 1
                  found# = 1
              End!If job:skip_adjustment <> 'YES'
          End!If access:manufacturer.tryfetch(man:manufacturer_key)
          If found# = 0
              access:chartype.clearkey(cha:charge_type_key)
              cha:charge_type = job:warranty_charge_type
              if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                  If cha:force_warranty = 'YES'
                      setcursor(cursor:wait)
                      save_map_id = access:manfaupa.savefile()
                      access:manfaupa.clearkey(map:field_number_key)
                      map:manufacturer = job:manufacturer
                      set(map:field_number_key,map:field_number_key)
                      loop
                          if access:manfaupa.next()
                             break
                          end !if
                          if map:manufacturer <> job:manufacturer      |
                              then break.  ! end if
                          if map:compulsory = 'YES'
                              found# = 1
                              Break
                          End
                      end !loop
                      access:manfaupa.restorefile(save_map_id)
                      setcursor()
                  End!If cha:force_warranty = 'YES'
              end!if access:chartype.fetch(cha:charge_type_key) = Level:Benign
          End!If found# = 0
          If found# = 1
              Clear(wpr:Record)
              Globalrequest = Insertrecord
              glo:select1 = 'ADJUSTMENT'
              glo:select2 = job:ref_number
              Update_Warranty_Part
              glo:select1 = ''
              glo:select2 = ''
          End!If found# = 1
          If found# = 0
              get(warparts,0)
              if access:warparts.primerecord() = level:benign
                  wpr:ref_number           = job:ref_number
                  wpr:adjustment           = 'YES'
                  wpr:part_number          = 'ADJUSTMENT'
                  wpr:description          = 'ADJUSTMENT'
                  wpr:quantity             = 1
                  wpr:warranty_part        = 'YES'
                  wpr:exclude_from_order   = 'YES'
              IF job:manufacturer = 'SIEMENS'
                        wpr:Fault_Code1 = 'Y'
                wpr:Fault_Code2 = 'N'
                      END
                  if access:warparts.insert()
                      access:warparts.cancelautoinc()
                  end
              end!if access:warparts.primerecord() = level:benign
          End!!If found# = 0
          Of 1 ! No Button
      End ! Case Missive


      BRW10.ResetSort(1)
      Do CountParts
    OF ?ExchangeButton:2
      ThisWindow.Update
      Do WarrantyExchangeUnitButton
      BRW10.ResetSort(1)
      Do Enable_Disable
    OF ?Button:ReceiptFromPUP
      ThisWindow.Update
      ! Received From PUP - TrkBs: 7149 (DBH: 04-05-2006)
      Do ReceiptFromPUP
      
    OF ?Close
      ThisWindow.Update
      ! Close the window - TrkBs: 6570 (DBH: 19-10-2005)
      tmp:Close = True
      Post(Event:closeWindow)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?CompleteButton
      ThisWindow.Update
          !Has the booking option been set?
          !If so, then the engineer option MUST be set too - 3658 (DBH: 04-12-2003)
          !If tmp:Booking48HourOption <> 0      !TB13298 - remove reliance on Booking Option - always error on Engineer Option
              If tmp:Engineer48HourOption = 0
                  Case Missive('You must select an Engineering Option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End !If tmp:Engineer48HourOption = 0
          !End !tmp:Booking48HourOption <> 0
      
          !Work out Repair Types
          Error# = 0
      
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = job:Manufacturer
          If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
              !Found
              If man:AutoRepairType
                  If tmp:COverwriteRepairType
                      tmp:charge_t = job:Repair_Type
                  Else !tmp:COverwriteRepairType
                      tmp:Charge_T = ''
                  End !tmp:COverwriteRepairType
      
                  If tmp:WOverwriteRepairType
                      tmp:warranty_t = job:Repair_Type_Warranty
                  Else !tmp:WOverwriteRepairType
                      tmp:Warranty_T = ''
                  End !tmp:WOverwriteRepairType
      
                  Calculate_Billing(tmp:charge_t,tmp:warranty_t,tmp:FaultCode_t,tmp:FaultCodeW_t)
      
                  !J added G145 - if Fault code error starts '*' or is '0' then drop it
                  !Warranty fault codes if free texted will be 0 or *
                  !if SUB(tmp:FaultCodeW_t,1,1) = '*' or clip(tmp:FaultCodeW_t)='0'
                  !    tmp:FaultCodeW_t=''
                  !end
                  !end G145
      
                  glo:ErrorText = ''
      
                  If tmp:Warranty_T = '' And tmp:Charge_T = ''
                      glo:ErrorText = 'The fault codes allocated have not returned a repair type.'
                      Error# = 1
                  End !If tmp:Warranty_T = '' And tmp:Charge_T = ''
      
                  If job:Chargeable_Job = 'YES'
                      If tmp:Charge_T = '' and tmp:Warranty_T <> ''
                          glo:ErrorText = 'You have allocated a Warranty Fault Code to this job, but it is marked as Chargeable.' &|
                                          '<13,10>Please select another Fault Code.'
                          Error# = 1
                      End !If tmp:Charge_T = ''
                  End !If job:Chargeable_Job = 'YES'
      
                  If Error#
                      glo:ErrorText = 'Unable to complete the job:<13,10>' & Clip(glo:ErrorText)
                      Error_Text
                      glo:ErrorText = ''
                  Else!If Error#
                      If tmp:Warranty_T <> '' And tmp:Charge_T <> ''
                          If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
      
                          End !If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
                          If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
      
                          End !If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
                          If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
      
                          End !If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                      End !tmp:Warranty_T <> '' And tmp:Charge <> ''
      
                      If tmp:Warranty_T <> '' And tmp:Charge_T = ''
                          If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
      
                          End !If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
                          If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
      
                          End !If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
                          If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
      
                          End !If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                      End !tmp:Warranty_T <> '' And tmp:Charge_T = ''
      
                      If tmp:Warranty_T = '' And tmp:Charge_T <> ''
                          If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
                              glo:ErrorText = Clip(glo:ErrorText) & '<13,10>This job has not been allocated a Warranty Fault Code. ' &|
                                              'This job will become Chargeable Only, and any Warranty parts attached will be converted to Chargeable.'
                          End !If job:Chargeable_Job = 'YES' and job:Warranty_job = 'YES'
      
                          If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
                                  !hello - what are you looking at?
                          End !If job:Chargeable_Job = 'YES' and job:Warranty_Job = 'NO'
      
                          If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                              glo:ErrorText = Clip(glo:ErrorText) & '<13,10>This job has not been allocated a Warranty Fault Code. ' &|
                                              'This job will become Chargeable Only, and any Warranty parts attached will be converted to Chargeable.'
                          End !If job:Chargeable_Job <> 'YES' and job:Warranty_Job = 'YES'
                      End !tmp:Warranty_T = '' And tmp:Charge_T <> ''
      
                      If tmp:Warranty_T = '' And tmp:Charge_T = ''
                          If job:Chargeable_Job = 'YES' And job:Warranty_Job = 'YES'
                              glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Error! No repair types were returned. Cannot complete this job.'
                          End !If job:Chargeable_Job = 'YES' And job:Warranty_Job = 'YES'
                      End !tmp:Warranty_T = '' And tmp:Charge_T = ''
      
                      If glo:ErrorText
                          glo:ErrorText = 'Completion Information: <13,10>' & Clip(glo:ErrorText)
                          If Error_Text_Return() = 0
                              Error# = 1
                          End !If Error_Text_Return
                          glo:ErrorText = ''
                      Else
              !            Post(Event:Accepted,?Button40)
      
                      End !glo:ErrorText
      
                      If Error# = 0
                          DO refresh_types
                          IF man:BillingConfirmation = TRUE !Only show this screen if required!
                              !message('About to call billing confirmation - debug 2')
                              Billing_Confirmation(job:Charge_Type,tmp:charge_t,job:Warranty_Charge_Type,tmp:warranty_t,tmp:COverwriteRepairType,tmp:WOverwriteRepairType,1)
                              !Message('job:Warranty_Charge_Type = '&job:Warranty_Charge_Type)
                              Do SetJOBSE
                              Access:JOBSE.TryUpdate()
                              Do refresh_types
                          END
                      End !If Error# = 0
      
                  End !If Error#
      
              End !If man:AutoRepairType
          Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
              !Error
          End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Force Authorisation Number
      If job:Chargeable_Job = 'YES'
          Access:CHARTYPE.ClearKey(cha:Warranty_Key)
          cha:Warranty    = 'NO'
          cha:Charge_Type = job:Charge_Type
          If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
              If cha:ForceAuthorisation
                  If job:Authority_Number = ''
                      job:Authority_Number    = InsertAuthorisationNumber()
                  End !If job:Authorisation_Number = ''
              End !If cha:ForceAuthorisationNumber
          End
      End !job:Chargeable_Job = 'YES'
      ! Warranty Repair Limits
      If Error# = 0

          tmp:CompletedMode = True
          Local.Pricing(0)
          tmp:CompletedMode = False

          if CheckWarrantyRepairLimit(1) <> Level:Benign
              Error# = 1
          end

          If Error#
              glo:ErrorText = 'Unable to complete the job:<13,10>' & 'This repair exceeds the warranty repair limit. You must remove all ' & |
                              'outstanding warranty parts from this job.'
              Error_Text
              glo:ErrorText = ''
          End

      End
      !Estimate?
      tmp:CompletedMode = True
      Local.Pricing(0)
      tmp:CompletedMode = False

      tmp:PrintEstimateOnExit = 9

      If Error# = 0
          Estimate# = 0
          If job:Estimate = 'YES' and job:Chargeable_Job = 'YES'
              If job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES' And job:Estimate_Ready <> 'YES'
                  Total_Price('E',vat",total",balance")
                  Error# = 0

                  If total"+ vat" < job:estimate_if_over
                      Case Missive('The total value of this Estimate is less than the specified "Estimate If Over" value.'&|
                        '<13,10>'&|
                        '<13,10>Do you want to continue and COMPLETE the repair, or create an ESTIMATE anyway?','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|Estimate|Complete')
                          Of 3 ! Complete Button
                              !TB13024 - before allocating any stock check to ensure stock is clear to handle this   JC 12/03/13
                              Case EstimatePartsInUse()
                                of 0    !none in use
                                  IF (AddToAudit(job:Ref_Number,'JOB','ESTIMATE ACCEPTED FROM: ' & Clip(use:Forename) & ' ' & Clip(use:Surname),'COMMUNICATION METHOD: BELOW ESTIMATE THRESHOLD'))
                                  END ! IF

                                  job:Courier_Cost = job:Courier_Cost_Estimate
                                  job:Estimate_Ready  = 'YES'
                                  job:Estimate_Accepted = 'YES'
                                  ConvertEstimateParts()
                                  Error# = 1
                               of 1 !not found error
                                    miss# = Missive('The Estimate cannot be accepted at this time.'&|
                                    'The stock record is missing.'&|
                                    '|Ref No: '& Clip(epr:Part_Ref_Number),'ServiceBase 3g',|
                                    'midea.jpg','/OK')
                                    Error# = 1
                               of 2 !in use error
                                    miss# = Missive('The Estimate cannot be accepted at this time.'&|
                                    'The stock record is locked.'&|
                                    '|Part No: '& Clip(sto:Part_Number) & |
                                    '|Location: ' & Clip(sto:Location),'ServiceBase 3g',|
                                    'midea.jpg','/OK')
                                    Error# = 1
                             END !case EstimatePartsInUse

                          Of 2 ! Estimate Button
                          Of 1 ! Cancel Button
                              Error# = 1
                      End ! Case Missive
                  End!If total_estimate_temp < job:estimate_if_over

                  If Error# = 0
                      If tmp:UseBERComparison = 1
                          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                          mod:Model_Number    = job:Model_Number
                          If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                              !Found
                              If job:Sub_Total_Estimate > mod:ReplacementValue
                                  If ((job:Sub_Total_Estimate - mod:ReplacementValue) / mod:ReplacementValue) * 100 > tmp:BERComparison
                                      Case Missive('This repair exceeds ' & Clip(tmp:BERComparison) & '% of the unit replacement value.'&|
                                        '<13,10>You should now select a B.E.R. Repair Type.'&|
                                        '<13,10>Do you wish to CONTINUE with the repair, or make this job a B.E.R.?','ServiceBase 3g',|
                                                     'mexclam.jpg','B.E.R.|Continue')
                                          Of 2 ! Continue Button
                                          Of 1 ! B.E.R. Button
                                              job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C',1,EngineerSkillLevel(job:Engineer),job:Repair_Type)
                                              If job:Repair_Type = ''
                                                  job:Repair_Type = tmp:RepairType
                                              End !job:Repair_Type = ''
                                              Post(Event:Accepted,?job:Repair_Type)
                                              Error# = 1
                                      End ! Case Missive
                                  End !If ((job:Sub_Total_Estimate - mod:ReplacementValue) / mod:ReplacementValue) * 100 > GETINI('ESTIMATE','BERComparison',,CLIP(PATH())&'\SB2KDEF.INI')
                              End !If job:Sub_Total_Estimate > mod:ReplacementValue
                          Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

                      End !If GETINI('ESTIMATE','UseBERComparison',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                  End !If Error# = 0
                  If error# = 0
                      If tmp:CheckPartAvailable = 1
                          !Check if parts are available, or outside supplier period
                          FoundNotOverDue# = 0
                          Save_epr_ID = Access:ESTPARTS.SaveFile()
                          Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                          epr:Ref_Number  = job:Ref_Number
                          Set(epr:Part_Number_Key,epr:Part_Number_Key)
                          Loop
                              If Access:ESTPARTS.NEXT()
                                 Break
                              End !If
                              If epr:Ref_Number  <> job:Ref_Number      |
                                  Then Break.  ! End If
                              If epr:Exclude_From_Order <> 'YES'
                                  If epr:Part_Ref_Number <> ''
                                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                      sto:Ref_NUmber  = epr:Part_Ref_Number
                                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                          !Found
                                          If sto:Quantity_Stock = 0
                                              Save_orp_ID = Access:ORDPARTS.SaveFile()
                                              Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
                                              orp:All_Received = 'NO'
                                              orp:Part_Number  = epr:Part_Number
                                              Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
                                              Loop
                                                  If Access:ORDPARTS.NEXT()
                                                     Break
                                                  End !If
                                                  If orp:All_Received <> 'NO'      |
                                                  Or orp:Part_Number  <> epr:Part_Number      |
                                                      Then Break.  ! End If
                                                  Access:ORDERS.ClearKey(ord:Order_Number_Key)
                                                  ord:Order_Number = orp:Order_Number
                                                  If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
                                                      !Found
                                                      Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                                                      sup:Company_Name = ord:Supplier
                                                      If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                                          !Found
                                                          If Today() - ord:Date < sup:Normal_Supply_Period
                                                              FoundNotOverdue# = 1
                                                              Break
                                                          End !If Today() - ord:Date > sup:Normal_Supply_Period
                                                      Else!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                                                          !Error
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                      End!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign

                                                  Else!If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
                                                      !Error
                                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  End!If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
                                              End !Loop
                                              Access:ORDPARTS.RestoreFile(Save_orp_ID)
                                              If FoundNotOverDue# = 1
                                                  Break
                                              End !If FoundOverDue# = 1

                                          End !If sto:Quantity_stock = 0
                                      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key.) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key.) = Level:Benign
                                  End !If sto:Part_Ref_Number <> ''
                              End !If epr:Exclude_From_Order <> 'YES
                          End !Loop
                          Access:ESTPARTS.RestoreFile(Save_epr_ID)

                          If FoundNotOverDue# = 1
                              Case Missive('One or more of the parts attached to this job is on an overdue backorder.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to notarise this on the estimate?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                      Set(DEFAULT2)
                                      Access:DEFAULT2.Next()
                                      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                                      jbn:RefNumber   = job:Ref_Number
                                      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                                          !Found

                                      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign

                                      If jbn:Invoice_Text = ''
                                          jbn:Invoice_Text = de2:OverdueBackOrderText
                                      Else !If jbn:Invoice_Text = ''
                                          jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & de2:OverdueBackOrderText
                                      End !If jbn:Invoice_Text = ''
                                      Access:JOBNOTES.TryUpdate()
                                  Of 1 ! No Button
                              End ! Case Missive
                          End !If FoundOverDue# = 1
                      End !If GETINI('ESTIMATE','CheckPartAvailable',,CLIP(PATH())&'\SB2KDEF.INI') = 1

                      job:Estimate_Ready  = 'YES'
                      GetStatus(510,thiswindow.request,'JOB') !estimate ready

                      Case Missive('The estimate is now ready.'&|
                        '<13,10>'&|
                        '<13,10>Do you want to print the estimate?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              tmp:PrintEstimateOnExit = 1
                          Of 1 ! No Button
                              tmp:PrintEstimateOnExit = 0
                      End ! Case Missive

                      Post(Event:Accepted,?OK)
                      Estimate# = 1
                  End !If error# = 0

              End !If job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES'
          End !job:Estimate = 'YES'
      End !Error# = 0
      ! Parts validation
      if Error# = 0
          glo:ErrorText = ''
          ! #11762 Repair Type does not allow parts.
          ! Check if there are an (Bryan: 24/11/2010)
          if (RepairTypesNoParts(job:Chargeable_Job, |
                                     job:Warranty_Job,   |
                                     job:Manufacturer,   |
                                     job:Repair_Type,    |
                                     job:Repair_Type_Warranty))
              If (JobHasSparesAttached(job:Ref_Number, |
                                            job:Estimate,  |
                                            job:Chargeable_Job, |
                                            job:Warranty_Job))
                  if Error# = 0
                      Error# = 1
                      glo:ErrorText = 'Unable to complete the job:<13,10>'
                  end
                  glo:ErrorText = Clip(glo:ErrorText) & 'Parts not allowed on current Repair Type.<13,10>'
                  !Break  ! #11871 Don't break here. (Bryan: 04/01/2011)
              End
          end

!          !added by Paul 01/11/2010 - Log no 11762
!          !check to see if parts are allowed with the selected repair type
!          If job:Chargeable_Job = 'YES' Then
!              !check chargeable repair type
!              access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
!              rtd:Manufacturer    = job:Manufacturer
!              rtd:Repair_Type     = job:Repair_Type
!              If Access:ReptyDef.fetch(rtd:ManRepairTypeKey) = level:benign then
!                  if rtd:NoParts = 'Y' then
!                      Access:Parts.ClearKey(par:Part_Number_Key)
!                      par:Ref_Number = job:Ref_Number
!                      set(par:Part_Number_Key,par:Part_Number_Key)
!                      loop
!                          If Access:Parts.Next() then
!                              Break
!                          End
!                          if par:Ref_Number <> job:Ref_Number then
!                              break
!                          End
!                          !if we are still here - then there must be parts on the job
!                          if Error# = 0
!                              Error# = 1
!                              glo:ErrorText = 'Unable to complete the job:<13,10>'
!                          end
!                          glo:ErrorText = Clip(glo:ErrorText) & 'Parts not allowed on current Repair Type.<13,10>'
!                          Break
!                      End
!                  End
!              End
!          End
!          If job:Warranty_Job  = 'YES' then
!              access:Reptydef.Clearkey(rtd:ManRepairTypeKey)
!              rtd:Manufacturer    = job:Manufacturer
!              rtd:Repair_Type     = job:Repair_Type_Warranty
!              If Access:RepTyDef.Fetch(rtd:ManRepairTypeKey) = level:benign then
!                  If rtd:NoParts = 'Y' then
!                      Access:WarParts.ClearKey(wpr:Part_Number_Key)
!                      wpr:Ref_Number = job:Ref_Number
!                      set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!                      loop
!                          If Access:WarParts.Next() then
!                              break
!                          End
!                          if wpr:Ref_Number <> job:Ref_Number then
!                              break
!                          End
!                          !if we are still here - then there must be parts on the job
!                          if Error# = 0
!                              Error# = 1
!                              glo:ErrorText = 'Unable to complete the job:<13,10>'
!                          end
!                          glo:ErrorText = Clip(glo:ErrorText) & 'Parts not allowed on current Repair Type.<13,10>'
!                          Break
!                      End
!                  End
!              End
!          End

          If Error# = 0
              if job:Chargeable_Job = 'YES'
                  Access:Parts.ClearKey(par:Part_Number_Key)
                  par:Ref_Number = job:Ref_Number
                  set(par:Part_Number_Key,par:Part_Number_Key)
                  loop until Access:Parts.Next()
                      if par:Ref_Number <> job:Ref_Number then break.

                      if par:Part_Number = 'EXCH'
                          if par:Status = 'REQ'
                              ! Error message
                              if Error# = 0
                                  Error# = 1
                                  glo:ErrorText = 'Unable to complete the job:<13,10>'
                              end
                              glo:ErrorText = Clip(glo:ErrorText) & 'An exchange unit has been requested for this job - ' & |
                                              ' No exchange unit has been allocated.<13,10>'
                          end
                      end

                      Access:Stock.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = par:Part_Ref_Number
                      if not Access:Stock.Fetch(sto:Ref_Number_Key)
                          if ~(RapidLocation(sto:Location) And sto:Location <> '')
                              if (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '') Or par:WebOrder = 1
                                  ! Error message
                                  if Error# = 0
                                      Error# = 1
                                      glo:ErrorText = 'Unable to complete the job:<13,10>'
                                  end
                                  glo:ErrorText = Clip(glo:ErrorText) & 'Part ' & Clip(par:Part_Number) & ' - ' & |
                                                  Clip(par:Description) & ' has not been allocated to this repair.<13,10>'
                              end
                          else
                              if ~par:PartAllocated
                                  ! Error message
                                  if Error# = 0
                                      Error# = 1
                                      glo:ErrorText = 'Unable to complete the job:<13,10>'
                                  end
                                  glo:ErrorText = Clip(glo:ErrorText) & 'Part ' & Clip(par:Part_Number) & ' - ' & |
                                                  Clip(par:Description) & ' has not been allocated to this repair.<13,10>'
                              end
                          end
                      end

                  end

              end

              if job:Warranty_Job = 'YES'
                  Access:WarParts.ClearKey(wpr:Part_Number_Key)
                  wpr:Ref_Number = job:Ref_Number
                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  loop until Access:WarParts.Next()
                      if wpr:Ref_Number <> job:Ref_Number then break.

                      if wpr:Part_Number = 'EXCH'
                          if wpr:Status = 'REQ'
                              ! Error message
                              if Error# = 0
                                  Error# = 1
                                  glo:ErrorText = 'Unable to complete the job:<13,10>'
                              end
                              glo:ErrorText = Clip(glo:ErrorText) & 'An exchange unit has been requested for this job - ' & |
                                              ' No exchange unit has been allocated.<13,10>'
                          end
                      end

                      Access:Stock.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = wpr:Part_Ref_Number
                      if not Access:Stock.Fetch(sto:Ref_Number_Key)

                          if ~(RapidLocation(sto:Location) And sto:Location <> '')
                              If (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '') Or wpr:WebOrder = 1
                                  ! Error message
                                  if Error# = 0
                                      Error# = 1
                                      glo:ErrorText = 'Unable to complete the job:<13,10>'
                                  end
                                  glo:ErrorText = Clip(glo:ErrorText) & 'Part ' & Clip(wpr:Part_Number) & ' - ' & |
                                                  Clip(wpr:Description) & ' has not been allocated to this repair.<13,10>'
                              end
                          else
                              if ~wpr:PartAllocated
                                  ! Error message
                                  if Error# = 0
                                      Error# = 1
                                      glo:ErrorText = 'Unable to complete the job:<13,10>'
                                  end
                                  glo:ErrorText = Clip(glo:ErrorText) & 'Part ' & Clip(wpr:Part_Number) & ' - ' & |
                                                  Clip(wpr:Description) & ' has not been allocated to this repair.<13,10>'
                              end
                          end
                      end

                  end
              end
          end

          if Error# = 1 then Error_Text.
          glo:ErrorText = ''

      end
      !Complete the job?
      If Error# = 0 And Estimate# = 0
          FailAssessment# = 0
          Case Missive('You are about to complete this repair.'&|
            '<13,10>Warning! You will NOT be able to un-complete it if you continue.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  Set(DEFAULTS)
                  Access:DEFAULTS.Next()
                  !Save the notes field
                  Access:JOBNOTES.TryUpdate()
                  Do SetJOBSE
                  Access:JOBSE.Update()

                  glo:errortext = ''
                  Complete# = 0
                  If job:date_completed = ''
                      !Check to see that all the required fields are filled in
                      CompulsoryFieldCheck('C')
                      If glo:ErrorText <> ''
                          glo:errortext = 'You cannot complete this job due to the following error(s): <13,10>' & Clip(glo:errortext)
                          Error_Text
                          glo:errortext = ''
                          !thismakeover.SetWindow(win:form)
                      Else!If glo:ErrorText <> ''
                          Access:JOBS.Update()
                          If tmp:ForceAccCheckComp = 1
                              If LocalValidateAccessories() = Level:Benign
                                  Complete# = 1
                              Else !If LocalValidateAccessories() = Level:Benign

                              End !If LocalValidateAccessories() = Level:Benign
                          Else
                              Complete# = 1
                          End !If GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                      End!If glo:ErrorText <> ''
                  Else!If job:date_completed = ''
                      Case Missive('Are you sure you want to change the Completion Date?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              Complete# = 1
                          Of 1 ! No Button
                      End ! Case Missive
                  End!If job:date_completed = ''

                  If Complete# = 1
                      Print_Despatch_Note_Temp = ''
                      !Fill in the totals with the right costs from the
                      !Price Structure.
                      Local.Pricing(0)
                      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                      man:Manufacturer = job:Manufacturer
                      If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                          !Found
                          If man:QAAtCompletion
                              !If QA at completion is ticked, then don't actually
                              !complete the job. That is done at QA

                              !*Neil*! Log Vodacom 151
                              !Find out if the head account needs this!

                              !overwritten by L060 - use current account doing the complete
                              QA_Flag# = 0

                              !Access:SubTracc.ClearKey(sub:Account_Number_Key)
                              !sub:Account_Number = job:Account_Number
                              !IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                              !  !Error!
                              !ELSE

                                Access:TradeAcc.ClearKey(tra:Account_Number_Key)

                                if glo:webjob then
                                  tra:Account_Number = Clarionet:Global.Param2 !was - sub:Main_Account_Number
                                ELSE
                                  tra:account_number = tmp:HeadAccount
                                END !if glo:webjob

                                IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                                  !Error! leave qa_flag = 0
                                ELSE
                                  IF tra:AllocateQAEng = TRUE
                                    QA_Flag# = 1
                                  END
                                  If tra:RepairEngineerQA
                                      tmp:CompletedQA = 1
                                  End !If tra:RepairEngineerQA

                                END

                              !END

                              IF QA_Flag# = 1
                                GetStatus(605,1,'JOB')
                                Nominate_Engineer
                                GetStatus(606,1,'JOB')
                                job:On_Test = 'YES'
                                job:Date_On_Test = Today()
                                job:Time_On_Test = Clock()
                                Post(Event:Accepted,?OK)
                              ELSE
                                GetStatus(605,1,'JOB')
                                Case Missive('The repair has been completed. You must now QA this unit.','ServiceBase 3g',|
                                               'mexclam.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                job:On_Test = 'YES'
                                job:Date_On_Test = Today()
                                job:Time_On_Test = Clock()
                                Post(Event:Accepted,?OK)
                              END
                          Else !If man:QAAtCompletion
                              !To avoid not changing to the right status,
                              !change to completed, first, and then it should
                              !Change again to one of the below status's if necessary

                              !(for Vodcom's turnaround report, the status HAS to change
                              !to completed, first.)
                              GetStatus(705,1,'JOB')


                              If ExchangeAccount(job:Account_Number)
                                  !Is the Trade  Account used for Exchange Units
                                  !if so, then there's no need to despatch the
                                  !unit normal. Just auto fill the despatch fields
                                  !and mark the job as to return to exchange stock.
                                  ForceDespatch
                                  GetStatus(707,1,'JOB')
                              Else !If ExchangeAccount(job:Account_Number)

                                  !Will the job be restocked, i.e. has, or will have, an
                                  !Exchange Unit attached, or is it a normal despatch.
                                  restock# = 0
                                  If ExchangeAccount(job:Account_Number)
                                      restock# = 1
                                  Else !If ExchangeAccount(job:Account_Number)
                                      glo:select1  = job:ref_number
                                      If ToBeExchanged()
                                      !Has this unit got an Exchange, or is it expecting an exchange
                                          If jobe:Engineer48HourOption = 1 And ~glo:WebJob
                                              If PassExchangeAssessment() = Level:Benign
                                                  restock# = 1
                                              Else !If PassExchangeAssessment() = Level:Benign
                                                  Case Missive('This unit is being repaired under the 48 Hour Exchange Process and has failed assessment.'&|
                                                    '<13,10>'&|
                                                    '<13,10>The unit must now be returned to the originating ARC.','ServiceBase 3g',|
                                                                 'mstop.jpg','/OK')
                                                      Of 1 ! OK Button
                                                  End ! Case Missive
                                                  FailAssessment# = 1
                                                  restock# = 0
                                              End !If PassExchangeAssessment() = Level:Benign

                                          Else !If jobe:Engineer48HourOption = 1 And ~glo:WebJob
                                              Restock# = 1
                                          End !If jobe:Engineer48HourOption = 1 And ~glo:WebJob
                                      End!If job:exchange_unit_number <> ''
                                      !If there is an loan unit attached, assume that a despatch note is required.
                                      If job:loan_unit_number <> ''
                                          restock# = 0
                                      End!If job:loan_unit_number <> ''
                                  End !If ExchangeAccount(job:Account_Number)

                                  If CompletePrintDespatch(job:Account_Number) = Level:Benign
                                      Print_Despatch_Note_Temp = 'YES'
                                  End !CompletePrintDespatch(job:Account_Number) = Level:Benign

                                  If restock# = 0

                                      !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
                                      !If either of those are ticked then don't despatch yet
                                      !print_despatch_note_temp = ''

                                      If DespatchAtClosing(FailAssessment#)
                                          !Does the Trade Account have "Skip Despatch" set?
                                          If AccountSkipDespatch(job:Account_Number) = Level:Benign
                                              access:courier.clearkey(cou:courier_key)
                                              cou:courier = job:courier
                                              if access:courier.tryfetch(cou:courier_key)
                                                  Case Missive('Cannot find the Courier attached to this job.','ServiceBase 3g',|
                                                                 'mstop.jpg','/OK')
                                                      Of 1 ! OK Button
                                                  End ! Case Missive
                                              Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                                  !Give the job a unique Despatch Number
                                                  !then call the despsing.inc.
                                                  !This calls the relevant exports etc, and different
                                                  !despatch procedures depending on the Courier.
                                                  !It will then print a Despatch Note and change
                                                  !The status as required.
                                                  If access:desbatch.primerecord() = Level:Benign
                                                      If access:desbatch.tryinsert()
                                                          access:desbatch.cancelautoinc()
                                                      Else!If access:despatch.tryinsert()
                                                          !If completing at RRC, use it's despatch
                                                          !procedure.
                                                          If glo:WebJob
                                                              glo:select1 = wob:refNumber
                                                              glo:select2 = 'JOB'
                                                              RemoteDespatch(job:ref_Number,jobe:DespatchType)
                                                              glo:Select1 = ''
                                                              glo:Select2 = ''
                                                          Else !If glo:WebJob
                                                              DespatchSingle()
                                                          End !If glo:WebJob
                                                          Print_Despatch_Note_Temp = ''
                                                      End!If access:despatch.tryinsert()
                                                  End!If access:desbatch.primerecord() = Level:Benign
                                              End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                          End !If AccountSkipDespatch = Level:Benign
                                      Else !If DespatchAtClosing()
                                          If job:Loan_Unit_Number <> ''
                                              GetStatus(811,1,'JOB')
                                          End !If job:Loan_Unit_Number <> ''
                                      End !If DespatchAtClosing()
                                  Else !If restock# = 0
                                      If job:Exchange_Unit_Number <> ''
                                          !Are either of the Chargeable or Warranty repair types BERs?
                                          !If so, then don't change the status to 707 because it shouldn't
                                          !be returned to the Exchange Stock.
                                          Comp# = 0
                                          If job:Chargeable_Job = 'YES'
                                              If BERRepairType(job:Manufacturer,job:Repair_Type) = Level:Benign
                                                   Comp# = 1
                                              End !If BERRepairType(job:Repair_Type) = Level:Benign
                                          End !If job:Chageable_Job = 'YES'
                                          If job:Warranty_Job = 'YES' and Comp# = 0
                                              If BERRepairType(job:Manufacturer,job:Repair_Type_Warranty) = Level:Benign
                                                  Comp# = 1
                                              End !If BERRepairType(job:Repair_Type) = Level:Benign
                                          End !If job:Warranty_Job = 'YES'
                                          If Comp# = 1
                                              Case Missive('This unit has been exchanged but is a B.E.R. Repair.'&|
                                                '<13,10>'&|
                                                '<13,10>DO NOT place this unit into Exchange Stock.','ServiceBase 3g',|
                                                             'mexclam.jpg','/OK')
                                                  Of 1 ! OK Button
                                              End ! Case Missive
                                              GetStatus(705,1,'JOB')
                                              Print_Despatch_Note_Temp = 'NO'
                                          Else !If Comp# = 1
                                              Case Missive('This unit has been exchanged. It should now be placed into Exchange Stock.','ServiceBase 3g',|
                                                             'mexclam.jpg','/OK')
                                                  Of 1 ! OK Button
                                              End ! Case Missive
                                              GetStatus(707,1,'JOB')
                                          End !If Comp# = 1
                                      End !If job:Exchange_Unit_Number <> ''
                                  End !If restock# = 0
                                  !Does the Account care about Bouncers?
                                  Check_For_Bouncers_Temp = 1
                                  If CompleteCheckForBouncer(job:Account_Number)
                                      Check_For_Bouncers_Temp = 0
                                  End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign

                              End !If ExchangeAccount(job:Account_Number)

                              !Fill in the relevant fields

                              saved_ref_number_temp   = job:ref_number
                              saved_esn_temp  = job:esn
                              saved_msn_temp  = job:msn
                              job:date_completed = Today()
                              job:time_completed = Clock()

                              !Fill in WEBJOBS with completed info -  (DBH: 14-10-2003)
                              Access:WEBJOB.Clearkey(wob:RefNumberKey)
                              wob:RefNumber = job:Ref_Number
                              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                !Found
                                  wob:DateCompleted = job:Date_Completed
                                  wob:TimeCompleted = job:Time_Completed
                                  wob:Completed     = 'YES'
                                  Access:WEBJOB.Update()
                              Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                !Error
                              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                              date_completed_temp = job:date_completed
                              time_completed_temp = job:time_completed
                              job:completed = 'YES'

                              If Check_For_Bouncers_Temp
                                  !Check if the job is a Bouncer and add
                                  !it to the Bouncer List
                                  If CountBouncer()
                                      !Bouncer
                                      AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                                  Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

                                  End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

                              End !If Check_For_Bouncers_Temp

                              !Check if this job is an EDI job
                              Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                              mod:Model_Number    = job:Model_Number
                              If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                  !Found
                                  job:edi = PendingJob(mod:Manufacturer)
                                  job:Manufacturer    = mod:Manufacturer
                              Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

                              !Mark the job's exchange unit as being available
                              CompleteDoExchange(job:Exchange_Unit_Number)

                              Do QA_Group
                              !ThisMakeover.SetWindow(win:form)
                              Access:JOBS.TryUpdate()

                              If ExchangeAccount(job:Account_Number)
                                  Case Missive('This is an Exchange Repair.'&|
                                    '<13,10>'&|
                                    '<13,10>Please re-stock the unit.','ServiceBase 3g',|
                                                 'mexclam.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              End !If ExchangeAccount(job:Account_Number)

                              If print_despatch_note_temp = 'YES'
                                  If restock# = 1
                                      restocking_note
                                  Else!If restock# = 1
                                      despatch_note
                                  End!If restock# = 1
                                  glo:select1  = ''
                              End!If print_despatch_note_temp = 'YES'

                              !Auto print invoice?
                              If job:Chargeable_Job = 'YES'
                                  PrintInvoice# = 0
                                  If InvoiceSubAccounts(job:Account_Number)
                                      If sub:InvoiceAtCompletion
                                          Case sub:InvoiceTypeComplete
                                              Of 0 !Manual
                                                  If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                                      DespatchInvoice(1)
                                                  End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                              Of 1 !Automatic
                                                  If IsJobInvoiced(job:Invoice_Number) = Level:Benign
                                                      If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                                          If IsJobInvoiced(job:Invoice_Number) = Level:Benign
                                                              If CreateInvoice() = Level:Benign
                                                                  PrintInvoice# = 1
                                                              End !If CreateInvoice() = Level:Benign
                                                          Else !If job:Invoice_Number = ''
                                                              PrintInvoice# = 1
                                                          End !If job:Invoice_Number = ''
                                                      End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                                  End !If job:Invoice_Number = ''
                                          End !Case sub:InvoiceType
                                      End !If sub:InvoiceAtCompletion
                                  Else !If InvoiceSubAccounts(job:Account_Number)
                                      If tra:InvoiceAtCompletion
                                          Case tra:InvoiceTypeComplete
                                              Of 0 !Manual
                                                  If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                                      DespatchInvoice(1)
                                                  End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                              Of 1 !Automatic
                                                  If IsJobInvoiced(job:Invoice_Number) = Level:Benign
                                                      If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                                          If IsJobInvoiced(job:Invoice_Number) = Level:Benign
                                                              If CreateInvoice() = Level:Benign
                                                                  PrintInvoice# = 1
                                                              End !If CreateInvoice() = Level:Benign
                                                          Else !If job:Invoice_Number = ''
                                                              PrintInvoice# = 1
                                                          End !If job:Invoice_Number = ''
                                                      End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                                  End !If job:Invoice_Number = ''
                                          End !Case sub:InvoiceType
                                      End !If sub:InvoiceAtCompletion

                                  End !If InvoiceSubAccounts(job:Account_Number)
                                  If PrintInvoice#
                                      glo:Select1 = job:Invoice_Number
                                      !Single_Invoice('','')
                                      VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
                                      glo:Select1 = ''
                                  End !If PrintInvoice#
                              End !If job:Chargeable_Job = 'YES'
                              Do GetJOBSE
                              Post(Event:Accepted,?OK)
                          End !If man:QAAtCompletion
                      Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

                  End !Complete# = 1


              Of 1 ! No Button
          End ! Case Missive
          !Completed?
          Do ShowCompletedText
          Display()
      End !Error# = 0

      do DisplayCompleteButton
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Are warranty/chargeable types blank?
  if job:Warranty_Job = 'NO' then
      job:Warranty_Charge_Type = ''
  END
  
  if job:Chargeable_Job = 'NO' then
      job:Charge_Type = ''
  END
  jbn:Fault_Description   = Upper(jbn:Fault_Description)
  jbn:Engineers_Notes     = Upper(jbn:Engineers_Notes)
  jbn:Invoice_Text        = Upper(jbn:Invoice_Text)
  jbn:Collection_Text     = Upper(jbn:Collection_Text)
  jbn:Delivery_Text       = Upper(jbn:Delivery_Text)
  jbn:ColContatName       = Upper(jbn:ColContatName)
  jbn:ColDepartment       = Upper(jbn:ColDepartment)
  jbn:DelContactName      = Upper(jbn:DelContactName)
  jbn:DelDepartment       = Upper(jbn:DelDepartment)
  
  Access:JOBNOTES.Update()
  Do SetJOBSE
  
  !Reprice The Job
  !Add new prototype - L945 (DBH: 04-09-2003)
  JobPricingRoutine(0)
  
  job:EDI = PendingJob(job:Manufacturer)
  
  Access:JOBSE.Update()
  If job:Date_Completed <> ''
      CompulsoryFieldCheck('C')
  Else!If job:Date_Completed <> ''
      CompulsoryFieldCheck('B')
  End!If job:Date_Completed <> ''
  
  If glo:ErrorText <> ''
      glo:errortext = 'You cannot complete editing due to the following error(s): <13,10>' & Clip(glo:errortext)
      Error_Text
      glo:errortext = ''
      !thismakeover.SetWindow(win:form)
      Cycle
  Else!If error# =
      !Have fields changed? Ask for a reason
      glo:EDI_Reason = ''
      If job:Charge_Type <> sav:ChargeType And sav:ChargeType <> ''
          If FieldChangeReason('Chargeable Charge Type')
              IF (AddToAudit(job:Ref_Number,'JOB','DETAILS CHANGED: CHARGEABLE CHARGE TYPE','PREVIOUS: ' & Clip(sav:ChargeType) & '<13,10>REASON: ' & Clip(glo:EDI_Reason)))
              END ! IF
  
              sav:ChargeType = job:Charge_Type
          Else !If FieldChangeReason('Chargeable Charge Type')
              Cycle
          End !If FieldChangeReason('Chargeable Charge Type')
      End !job:Charge_Type <> sav:ChargeType
  
      glo:EDI_Reason = ''
      If job:Warranty_Charge_Type <> sav:WarrantyChargeType And sav:WarrantyChargeType <> ''
          If FieldChangeReason('Warranty Charge Type')
              IF (AddToAudit(job:Ref_Number,'JOB','DETAILS CHANGED: WARRANTY CHARGE TYPE','PREVIOUS: ' & Clip(sav:WarrantyChargeType) & '<13,10>REASON: ' & Clip(glo:EDI_Reason)))
              END ! IF
  
              sav:WarrantyChargeType = job:Warranty_Charge_Type
          Else !If FieldChangeReason('Chargeable Charge Type')
              Cycle
          End !If FieldChangeReason('Chargeable Charge Type')
      End !job:Charge_Type <> sav:ChargeType
  
      glo:EDI_Reason = ''
      If job:Repair_Type <> sav:RepairType And sav:RepairType <> ''
          If FieldChangeReason('Chargeable Repair Type')
              IF (AddToAudit(job:Ref_Number,'JOB','DETAILS CHANGED: CHARGEABLE REPAIR TYPE','PREVIOUS: ' & Clip(sav:RepairType) & '<13,10>REASON: ' & Clip(glo:EDI_Reason)))
              END ! IF
  
              sav:ChargeType = job:Repair_Type
          Else !If FieldChangeReason('Chargeable Charge Type')
              Cycle
          End !If FieldChangeReason('Chargeable Charge Type')
      End !job:Charge_Type <> sav:ChargeType
  
      glo:EDI_Reason = ''
      If job:Repair_Type_Warranty <> sav:WarrantyRepairType And sav:WarrantyRepairType <> ''
          If FieldChangeReason('Warranty Repair Type')
              IF (AddToAudit(job:Ref_Number,'JOB','DETAILS CHANGED: WARRANTY REPAIR TYPE','PREVIOUS: ' & Clip(sav:WarrantyRepairType) & '<13,10>REASON: ' & Clip(glo:EDI_Reason)))
              END ! IF
  
              sav:WarrantyChargeType = job:Repair_Type_Warranty
          Else !If FieldChangeReason('Chargeable Charge Type')
              Cycle
          End !If FieldChangeReason('Chargeable Charge Type')
      End !job:Charge_Type <> sav:ChargeType
  End
  
  ! Update job status to 'Spares Requested' if any spares on order
  tmp:OKPressed = 1
  do Check_Parts          ! 'Check parts' updates the job status
  tmp:OKPressed = 0
  
  do Waybill:Check
  !Did Anything Change?
  If thiswindow.request <> Insertrecord
      Clear(ChangedGroup)
      If sav:ChargeableJob <> job:Chargeable_Job
          sav1:chj = 1
      End!If sav:ChargeableJob <> job:Chargeable_Job
      If sav:WarrantyJob <> job:Warranty_Job
          sav1:wrj = 1
      End!If sav:WarrantyJob <> job:Warranty_Job
      If sav:HubRepair  <> tmp:HubRepair
          sav1:hbr = 1
      End !If sav:HubRepair  <> jobe:HubRepair
  
  
  
      If sav1:cct Or sav1:wct or sav1:crt or sav1:wrt or sav1:chj or |
          sav1:wrj Or sav1:elc Or sav1:clc Or sav1:wlc Or sav1:epc Or |
          sav1:cpc  Or sav1:Wpc Or sav1:ecc Or sav1:ccc Or sav1:wcc Or sav1:hbr Or sav1:acc
          !Something has changed, so add an audit entry
          locAuditNotes         = 'JOB DETAILS CHANGED: '
  
          If sav1:cct = 1
          !Chargeable Charge Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>CHAR. CHARGE TYPE: ' & CLip(job:Charge_Type)
          End!If sav1:cct
  
          If sav1:wct = 1
          !Warranty Charge Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>WARR. CHARGE TYPE: ' & CLip(job:Warranty_Charge_Type)
          End!If sav1:cct
  
          If sav1:crt = 1
          !Chargeable Repair Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>CHAR. REPAIR TYPE: ' & CLip(job:Repair_Type)
          End!If sav1:cct
  
          If sav1:wrt = 1
          !Warranty Repair Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>WARR. REPAIR TYPE: ' & CLip(job:Repair_Type_Warranty)
          End!If sav1:cct
  
          If sav1:chj = 1
          !Chargable Job
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>CHARGEABLE JOB: ' &  Clip(job:Chargeable_Job)
          End!If sav1:chj = 1
  
          If sav1:wrj = 1
          !Warranty Job
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
          End!If sav1:wrj = 1
  
          If sav1:hbr = 1
              Case tmp:HubRepair
                  Of 0
                      locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>HUB REPAIR DE-SELECTED'
                  Of 1
                      locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>HUB REPAIR SELECTED'
              End !Case jobe:HubRepair
          End !If sav1:hbr = 1
  
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED',locAuditNotes))
          END ! IF
  
      End!If ChangedGroup
  
      If sav:TransitType <> job:Transit_Type
          sav1:itt = 1
      End !If sav:TransitType <> job:Transit_Type
  
      !Do not record N/A in the notes - L900  (DBH: 29-07-2003)
      If job:MSN <> 'N/A'
          If sav:MSN <> job:MSN
              sav1:MSN = 1
          End !If sav:MSN <> job:MSN
      End !If job:MSN <> 'N/A' And job:MSN <> ''
  
      If sav:ModelNumber <> job:Model_Number
          sav1:Mdl = 1
      End !If sav:ModelNumber <> job:Model_Number
  
      If sav:DOP <> job:DOP
          sav1:DOP = 1
      End !If sav:DOP <> job:DOP
  
      If sav:OrderNumber <> job:Order_Number
          sav1:ord = 1
      End !If sav:OrderNumber <> job:Order_Number
  
      If sav:FaultCode1 <> job:Fault_Code1
          sav1:fc1 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode2 <> job:Fault_Code2
          sav1:fc2 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode3 <> job:Fault_Code3
          sav1:fc3 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode4 <> job:Fault_Code4
          sav1:fc4 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode5 <> job:Fault_Code5
          sav1:fc5 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode6 <> job:Fault_Code6
          sav1:fc6 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode7 <> job:Fault_Code7
          sav1:fc7 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode8 <> job:Fault_Code8
          sav1:fc8 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode9 <> job:Fault_Code9
          sav1:fc9 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode10 <> job:Fault_Code10
          sav1:fc10 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode11 <> job:Fault_Code11
          sav1:fc11 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode12 <> job:Fault_Code12
          sav1:fc12 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If job:Manufacturer = 'SAMSUNG'
          !Do not show Fault Code 12 for Samsung
          sav1:fc12 = 0
      End !If job:Manufacturer = 'SAMSUNG'
  
      !Do not show the Main Out Fault
      Access:MANFAULT.ClearKey(maf:MainFaultKey)
      maf:Manufacturer = job:Manufacturer
      maf:MainFault    = 1
      If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
          !Found
          Case maf:Field_Number
              Of 1
                  sav1:fc1 = 0
              Of 2
                  sav1:fc2 = 0
              Of 3
                  sav1:fc3 = 0
              Of 4
                  sav1:fc4 = 0
              Of 5
                  sav1:fc5 = 0
              Of 6
                  sav1:fc6 = 0
              Of 7
                  sav1:fc7 = 0
              Of 8
                  sav1:fc8 = 0
              Of 9
                  sav1:fc9 = 0
              Of 10
                  sav1:fc10 = 0
              Of 11
                  sav1:fc11 = 0
              Of 12
                  sav1:fc12 = 0
          End !Case maf:Field_Number
      Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End            !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
  
      If sav:InvoiceText <> jbn:Invoice_Text
          sav1:ivt = 1
      End !If sav:InvoiceText <> jbn:Invoice_Text
  
      If sav:FaultDescription <> jbn:Fault_Description
          sav1:fdt = 1
      End !If sav:FaultDescription <> jbn:Fault_Description
  
      If sav1:itt Or sav1:MSN Or sav1:mdl Or sav1:DOP Or sav1:Ord
          locAuditNotes         = 'JOB DETAILS CHANGED: '
  
          If sav1:itt = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>TRANS TYPE: ' & CLip(job:Transit_Type)
          End !If sav1:itt = 1
  
          If sav1:MSN = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>MSN: ' & CLip(job:MSN)
          End !If sav1:MSN = 1
  
          If sav1:mdl = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>MODEL NO: ' & CLip(job:Model_Number)
          End !If sav1:mdl = 1
  
          If sav1:DOP = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>DOP: ' & CLip(Format(job:DOP,@d6))
          End !If sav1:DOP = 1
  
          If sav1:ord = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>ORDER NO: ' & CLip(job:Order_Number)
          End !If sav1:ord = 1
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED',locAuditNotes))
          END ! IF
  
      End !sav1:ivt Or sav1:fd1
  
      If sav1:FC1 Or sav1:FC2 Or sav1:FC3 Or sav1:FC4 Or |
          sav1:FC5 Or sav1:FC6 Or sav1:FC7 Or sav1:fC8 Or |
          sav1:FC9 Or sav1:FC10 Or sav1:FC11 Or sav1:FC12
          locAuditNotes         = 'JOB DETAILS CHANGED: '
  
          If sav1:fc1 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE1: ' & CLip(job:Fault_Code1)
          End !If sav1:fc1 = 1
          If sav1:fc2 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE2: ' & CLip(job:Fault_Code2)
          End !If sav1:fc1 = 1
          If sav1:fc3 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE3: ' & CLip(job:Fault_Code3)
          End !If sav1:fc1 = 1
          If sav1:fc4 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE4: ' & CLip(job:Fault_Code4)
          End !If sav1:fc1 = 1
          If sav1:fc5 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE5: ' & CLip(job:Fault_Code5)
          End !If sav1:fc1 = 1
          If sav1:fc6 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE6: ' & CLip(job:Fault_Code6)
          End !If sav1:fc1 = 1
          If sav1:fc7 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE7: ' & CLip(job:Fault_Code7)
          End !If sav1:fc1 = 1
          If sav1:fc8 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE8: ' & CLip(job:Fault_Code8)
          End !If sav1:fc1 = 1
          If sav1:fc9 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE9: ' & CLip(job:Fault_Code9)
          End !If sav1:fc1 = 1
          If sav1:fc10 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE10: ' & CLip(job:Fault_Code10)
          End !If sav1:fc1 = 1
          If sav1:fc11 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE11: ' & CLip(job:Fault_Code11)
          End !If sav1:fc1 = 1
          If sav1:fc12 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE12: ' & CLip(job:Fault_Code12)
          End !If sav1:fc1 = 1
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED: FAULT CODES',locAuditNotes))
          END ! IF
  
      End !sav1:FC9 Or sav1:FC10 Or sav1:FC11 Or sav1:FC12
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:UseInvTextForFaults
              !Does not show Invoice Text
              sav1:ivt = 0
          End !If man:UseInvTextForFaults
      End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      If sav1:ivt
          locAuditNotes         = 'JOB DETAILS CHANGED: '
          If sav1:ivt = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>' & CLip(jbn:Invoice_Text)
          End !If sav1:fc1 = 1
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED: INVOICE TEXT',locAuditNotes))
          END ! IF
  
      End !If sav1:ivt
      If sav1:fdt
          locAuditNotes         = 'JOB DETAILS CHANGED: '
          If sav1:fdt = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>' & CLip(jbn:Fault_Description)
          End !If sav1:fc1 = 1
  
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED: FAULT DESCRIPTION',locAuditNotes))
          END ! IF
  
      End !If sav1:ivt
  
  
  End!If thiswindow.request <> Insertrecord
  
  
  saved_esn_temp = job:esn
  saved_Ref_number_temp = job:Ref_Number
  
  
  Access:JOBNOTES.Update()
  
  !Write Address/Account Details
  If job:Account_Number <> accsav:AccountNumber
      IF (AddToAudit(job:Ref_Number,'JOB','ACCOUNT NUMBER CHANGED','PREVIOUS ACCOUNT: ' & Clip(accsav:AccountNumber) & |
                          '<13,10>NEW ACCOUNT: ' & Clip(job:Account_Number)))
      END ! IF
  End !job:Account_Number <> accsav:AccountNumber
  
  If job:Company_Name <> accsa1:CompanyName Or |
      job:Address_Line1 <> accsa1:AddressLine1 Or |
      job:Address_Line2 <> accsa1:AddressLine2 Or |
      job:Address_Line3 <> accsa1:AddressLine3 Or |
      job:Postcode <> accsa1:Postcode Or |
      job:Telephone_Number <> accsa1:TelephoneNumber Or |
      job:Fax_Number <> accsa1:FaxNumber
  
      IF (AddToAudit(job:Ref_Number,'JOB','ADDRESS DETAILS CHANGED - CUSTOMER','PREVIOUS: <13,10>' & Clip(accsa1:CompanyName) & |
                      '<13,10>' & Clip(accsa1:AddressLine1) & |
                      '<13,10>' & Clip(accsa1:AddressLine2) & |
                      '<13,10>' & Clip(accsa1:AddressLine3) & |
                      '<13,10>' & Clip(accsa1:Postcode) & |
                      '<13,10>TEL: ' & Clip(accsa1:TelephoneNumber) & |
                      '<13,10>FAX: ' & Clip(accsa1:FaxNumber)))
      END ! IF
  
  End !job:Fax_Number <> accsa1:FaxNumber
  
  If job:Company_Name_Delivery <> accsa2:CompanyName Or |
      job:Address_Line1_Delivery <> accsa2:AddressLine1 Or |
      job:Address_Line2_Delivery <> accsa2:AddressLine2 Or |
      job:Address_Line3_Delivery <> accsa2:AddressLine3 Or |
      job:Postcode_Delivery <> accsa2:Postcode Or |
      job:Telephone_Delivery <> accsa2:TelephoneNumber
  
      IF (AddToAudit(job:Ref_Number,'JOB','ADDRESS DETAILS CHANGED - DESPATCH','PREVIOUS: <13,10>' & Clip(accsa2:CompanyName) & |
                      '<13,10>' & Clip(accsa2:AddressLine1) & |
                      '<13,10>' & Clip(accsa2:AddressLine2) & |
                      '<13,10>' & Clip(accsa2:AddressLine3) & |
                      '<13,10>' & Clip(accsa2:Postcode) & |
                      '<13,10>TEL: ' & Clip(accsa1:TelephoneNumber)))
      END ! IF
  
  End !job:Fax_Number <> accsa1:FaxNumber
  
  !Check Accesories incase they change
  Free(SaveAccessoryQueue)
  Clear(SaveAccessoryQueue)
  
  Open(SaveAccessory)
  If ~Error()
      Save_jac_ID = Access:JOBACC.SaveFile()
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
             Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          Clear(savacc:Record)
          savacc:Accessory    = jac:Accessory
          Get(SaveAccessory,savacc:AccessoryKey)
          If Error()
              !New Accessory Added
              savaccq:Accessory   = jac:Accessory
              savaccq:Type        = 1
              Add(SaveAccessoryQueue)
          End !If Error()
      End !Loop
      Access:JOBACC.RestoreFile(Save_jac_ID)
  
      Clear(savacc:record)
      Set(SaveAccessory,0)
      Loop
          Next(SaveAccessory)
          If Error()
              Break
          End !If Error()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          jac:Accessory  = savacc:Accessory
          If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Found
  
          Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              !Accessory Deleted
              savaccq:Accessory   = savacc:Accessory
              savaccq:Type        = 0
              Add(SaveAccessoryQueue)
          End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
      End !Loop
  
      If Records(SaveAccessoryQueue)
          !Some accessories have been changed
          locAuditNotes = ''
          Sort(SaveAccessoryQueue,savaccq:Accessory,savaccq:Type)
          Loop x# = 1 To Records(SaveAccessoryQueue)
              Get(SaveAccessoryQueue,x#)
              locAuditNotes   = Clip(locAuditNotes) & '<13,10>' & Clip(savaccq:Accessory)
               Case savaccq:Type
                  Of 0
                      locAuditNotes = Clip(locAuditNotes) & ' -DELETED'
                  Of 1
                      locAuditNotes = Clip(locAuditNotes) & ' -ADDED'
               End !Case savaccq:Type
          End !Loop x# = 1 To Records(SaveAccessoryQueue)
  
          !Cut out the line break at the beginning
          locAuditNotes         = Sub(locAuditNotes,3,Len(locAuditNotes))
          IF (AddToAudit(job:Ref_Number,'JOB','JOB ACCESSORIES AMENDED',locAuditNotes))
          END ! IF
  
      End !If Records(SaveAccessoryQueue)
      Free(SaveAccessoryQueue)
  End !Error()
  Close(SaveAccessory)
  Remove(SaveAccessory)
  
  
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(job:Ref_Number)
  ! End (DBH 16/09/2008) #10253
  
  
  
  tmp:Close = True
  ReturnValue = PARENT.TakeCompleted()
  If tmp:PrintEstimateOnExit = 1
      glo:Select1 = job:Ref_Number
      AlertEst# = False
      Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
      jobe2:RefNumber = job:Ref_Number
      If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
          ! Found
          ! Reget the jobs information for the pricing (DBH: 18-05-2006)
          If jobe2:SMSNotification Or jobe2:EmailNotification
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = glo:Select1
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  ! Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
                      Total_Price('E',vat",total",balance")
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  ! Error
              End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          End ! If jobe2:SMSNotification Or jobe2:EmailNotification
!          !SMS notification now goes on change of status - this is a duplicate
!          If jobe2:SMSNotification
!              !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'ESTM','SMS',jobe2:SMSAlertNumber,'',total",'')
!              SendSMSText('J','Y','N')
!              if glo:ErrorText[1:5] = 'ERROR' then
!  
!                  miss# = missive('(4)Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!  
!              ELSE
!                  !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                  miss# = missive('(4)An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!              END 
!  
!          End ! If jobe2:SMSNotification
          If jobe2:EmailNotification
              AlertEst# = True
              glo:EstimateReportName = ''
              Estimate('ALERT')
              AddEmailSMS(glo:Select1,wob:HeadAccountNumber,'ESTM','EMAIL','',jobe2:EmailAlertAddress,total",Clip(glo:EstimateReportName))
              glo:EstimateReportName = ''
          End ! If jobe2:EmailNotification
      Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
          ! Error
  
      End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      If AlertEst# = False
          ! An "Email" estimate has not been printed... do a normal one (DBH: 18-05-2006)
          Estimate('')
      End ! If AlertEst# = False
      glo:Select1 = ''
      tmp:PrintEstimateOnExit = 9
  
  Else
      !Paul 15/06/2009 Log No 10321
      If tmp:PrintEstimateOnExit = 0
          !do the same routine but dont print the estimate
          glo:Select1 = job:Ref_Number
          !AlertEst# = False
          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
          jobe2:RefNumber = job:Ref_Number
          If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
              ! Found
              ! Reget the jobs information for the pricing (DBH: 18-05-2006)
              If jobe2:SMSNotification Or jobe2:EmailNotification
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = glo:Select1
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      ! Found
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          ! Found
                          Total_Price('E',vat",total",balance")
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          ! Error
                      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      ! Error
                  End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              End ! If jobe2:SMSNotification Or jobe2:EmailNotification
!              !SMS notification now goes on change of status - this is a duplicate
!              If jobe2:SMSNotification
!                  !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'ESTM','SMS',jobe2:SMSAlertNumber,'',total",'')
!                  SendSMSText('J','Y','N')
!                  if glo:ErrorText[1:5] = 'ERROR' then
!  
!                      miss# = missive('(5)Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!  
!                  ELSE
!                      !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                      miss# = missive('(5)An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!                  END 
!  
!              End ! If jobe2:SMSNotification
              If jobe2:EmailNotification
                  !AlertEst# = True
                  glo:EstimateReportName = ''
                  !Estimate('ALERT')
                  AddEmailSMS(glo:Select1,wob:HeadAccountNumber,'ESTM','EMAIL','',jobe2:EmailAlertAddress,total",Clip(glo:EstimateReportName))
                  glo:EstimateReportName = ''
              End ! If jobe2:EmailNotification
          Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
              ! Error
  
          End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
          glo:Select1 = ''
          tmp:PrintEstimateOnExit = 9
      End !If tmp:PrintEstimateOnExit = 0
  End !tmp:PrintEstimateOnExit
  !Call QA
  If tmp:CompletedQA
      if (SecurityCheck('RAPID - QA PROCEDURE') = FALSE)  ! #11682 Check access. (Bryan: 07/09/2010)
          Update_QA('PRI')
      End
  End !tmp:CompletedQA
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      !Do not allow to close via 'X' -  (DBH: 24-02-2004)
      If tmp:Close <> True
          Cycle
      End !tmp:Close <> True
      
!      !JC TB12905 13/02/13
!      if DebugLogging then
!          debx:TheLine = clip(job:Ref_Number)&' Closed log file: '&Format(today(),@d06)&' '&format(Clock(),@t4)
!          Add(DebugExport)
!          close(DebugExport)
!      END
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F8Key
              Post(Event:Accepted,?AmendUnitType)
          Of F3Key
              If ?AllocateJob{prop:Disable} = 0
                  Post(Event:Accepted,?AllocateJob)
              End !If ?AllocateJob{prop:Disable} = 0
          Of F5Key
              Post(Event:Accepted,?EngineerHistory)
          Of F6Key
              Post(Event:Accepted,?ViewAccessories)
          Of F7Key
              Post(Event:Accepted,?fault_Codes)
          Of F9Key
              Post(Event:Accepted,?ChangeJobStatus)
          Of F10Key
              Post(Event:Accepted,?CompleteButton)
      End !KeyCode()
    OF EVENT:GainFocus
      Do fill_lists
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.CorrectEngineer       Procedure()
Code
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        If glo:WebJob
            !The engineer on the job, should match the location of the job -  (DBH: 06-08-2003)
            If use:Location = tmp:ARCSiteLocation
                Case Missive('The ARC Engineer attached to this job must be removed before you can continue.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return False
            End !If use:Location = tmp:ARCSiteLocation
        Else !If glo:WebJob
            If use:Location <> tmp:ARCSiteLocation
                Case Missive('The RRC Engineer attached to this job must be removed before you can continue.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return False
            End !If use:Location <> tmp:ARCSiteLocation
        End !If glo:WebJob
    Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        Case Missive('Error! Cannot find the details for the engineer attached to this job.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return False
    End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    Return True
local.disableJob      Procedure(byte fDisable)
Code
    ?CompleteButton{prop:Disable} = fDisable
    ?AllocateJob{prop:Disable} = fDisable
    ?AmendUnitType{prop:Disable} = fDisable
    ?ChangeJobStatus{prop:Disable} = fDisable
    ?Invoice{prop:Disable} = fDisable
    ?Engineers_Notes{prop:Disable} = fDisable
    ?Warranty_Group{prop:Disable} = fDisable
    ?Chargeable_Group{prop:Disable} = fDisable
    ?tmp:Network{prop:Disable} = fDisable
    ?LookupNetwork{prop:Disable} = fDisable
    ?job:Location{prop:Disable} = fDisable
    ?job:Authority_Number{prop:Disable} = fDisable
    ! Inserting (DBH 06/fDisable2/2006) # 8559 - Do not allow to change when in view only mode
    ?job:Chargeable_Job{prop:Disable} = fDisable
    ?job:Warranty_Job{prop:Disable} = fDisable
    ?tmp:HubRepair{prop:Disable} = fDisable
    ?tmp:Engineer48HourOption{prop:Disable} = fDisable
    ?ValidatePOP{prop:Disable} = fDisable
    ?EstimateGroup{prop:Disable} = fDisable
    ! End (DBH 06/fDisable2/2006) #8559
! Deleting (DBH fDisable3/fDisablefDisable/2006) # 8485 - Can't willy nilly disable this button
!    ?Button:ReceiptFromPUP{prop:Disable} = fDisable
! End (DBH fDisable3/fDisablefDisable/2006) #8485
    ! Insert --- Make sure that the complete button is always disable for completed jobs (DBH: 19/08/2009) #11036
    !Changed by Paul 30/09/2009 - log no 10079
    !dont re-enable the complete button if it has already been diabled
    If ?CompleteButton{prop:Disable} = false
        do displayCompleteButton
    End
    ! end --- (DBH: 19/08/2009) #11036
Local.PrintReturningWaybill   Procedure(Long func:RefNumber)
local:WayBillNo       string(30)
local:CurrentSite   String(30)
local:Courier       String(30)
code
    If glo:WebJob
        local:CurrentSite   = Clarionet:Global.Param2
    Else !If glo:WebJob
        local:CurrentSite   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
    End !If glo:WebJob
    local:courier = getini('DESPATCH','RETcourier','',clip(path())&'\SB2KDEF.INI')

    access:courier.clearkey(cou:Courier_Key)
    cou:Courier = local:Courier
    if access:courier.fetch(cou:Courier_Key)
        !tmp:WayBillNo = Clip(tmp:AccountNumber) & |
        local:WayBillNo = Format(Month(Today()),@n02) & Format(Day(Today()),@n02) & Format(Year(Today()),@n04) & |
                    Format(Clock(),@t2)
    ELSE
        !Get the next waybill number - 3384 (DBH: 10-10-2003)
        local:WaybillNo = NextWayBillNumber()
        !Do not continue if waybill error - 3432 (DBH: 27-10-2003)
        If local:WayBillNo = 0
            Return
        End !If tmp:WayBillNo = 0
    END !If could find a courier



    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = local:WayBillNo
    If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !Found
        way:AccountNumber = local:CurrentSite
        way:WaybillID = 100 ! Exchange waybill
        way:FromAccount = local:CurrentSite
        way:ToAccount = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
        if Access:WAYBILLS.TryUpdate()
            !Access:WAYBILLS.CancelAutoInc()
        else
            ! Add items onto waybill
            xch:Ref_Number = func:RefNumber
            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then return.

            if not Access:WAYITEMS.PrimeRecord()
                wai:WayBillNumber = way:WayBillNumber
                wai:IsExchange = true
                wai:Ref_Number = xch:Ref_Number
                wai:JobNumber48Hour = job:Ref_Number
                if Access:WAYITEMS.TryUpdate()
                    Access:WAYITEMS.CancelAutoInc()
                end
            end
        end
    end

    WayBillExchange(local:CurrentSite,'TRA',|
                    local:CurrentSite,'DEF',|
                    local:WayBillNo,local:Courier,1)
Local.Pricing     Procedure(Byte func:Warranty)
Code
    Do SetJOBSE

    !Add new prototype - L945 (DBH: 04-09-2003)
    JobPricingRoutine(func:Warranty)
    Access:JOBSE.Update()

    Do GetJOBSE

    Display()

    if tmp:CompletedMode = False
        CheckWarrantyRepairLimit()
    end
CheckWarrantyRepairLimit Procedure(byte bMode = 0)

savedPassword   like(use:Password)
authoriseRepair byte(0)
netParts        real
netLabour       real
    code
    !/**
    !* Check the repair limit for warranty jobs.
    !* If repair limit exceeds warranty parts/labour value then :-
    !*      exchange unit / BER  - or -
    !*      authorise repair
    !*
    !*      bMode = 0 (Displays message box, call after costs are recalculated)
    !*      bMode = 1 (Called on 'Complete Repair' button, checks if parts require removing)
    !*/

    if job:Warranty_Job <> 'YES' then return Level:Benign.
    if jobe:ExceedWarrantyRepairLimit then return Level:Benign. ! This can be reset if the job costs change (see pricing routine)

    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    if Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        if mod:WarrantyRepairLimit = 0 then return Level:Benign.
    else ! Error
        return Level:Benign
    end

    ! return Level:Benign    ! ** placed here by Gary until procedure finished **

    if jobe:HubRepair
        netParts   = job:Parts_Cost_Warranty
        netLabour  = job:Labour_Cost_Warranty
    else
        netParts  = jobe:RRCWPartsCost
        netLabour = jobe:RRCWLabourCost
    end

    if (netParts + netLabour) >= mod:WarrantyRepairLimit
        if bMode = 0
            !----------------------------------------------------------------------!
            case WarrantyRepairLimitMessage()
                of 1 ! Authorise swap & BER the unit

                    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                    wpr:Ref_Number = job:Ref_Number
                    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    loop until Access:WARPARTS.Next()
                        if wpr:Ref_Number <> job:Ref_Number then break.
                        if wpr:Adjustment = 'YES' then cycle.
                        If wpr:Part_Ref_Number = '' then cycle.
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number = wpr:Part_Ref_Number
                        if not Access:STOCK.Fetch(sto:Ref_Number_Key)
                            if sto:AttachBySolder               ! Zero cost of parts attached by solder
                                wpr:Purchase_Cost = 0
                                Access:WARPARTS.TryUpdate()
                            end
                        end
                    end

                    ViewExchangeUnit()                          ! Select exchange unit

                    Case Missive('You should now select a B.E.R. Repair Type.','ServiceBase 3g',|
                                   'mexclam.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C',1,EngineerSkillLevel(job:Engineer),job:Repair_Type)
                    If job:Repair_Type = ''
                        job:Repair_Type = tmp:RepairType
                    End !job:Repair_Type = ''
                    Post(Event:Accepted,?job:Repair_Type)

                of 2 ! Authorise Repair

                    if SecurityCheck('JOBS - AUTHORISE REPAIR') ! Check security access level
                        Case Missive('You do not have sufficient access to authorise this repair.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        savedPassword = glo:Password            ! Save current user
                        Access:USERS.ClearKey(use:User_Code_Key)
                        use:User_Code = InsertEngineerPassword()! Allow a second user to authorise this repair
                        if Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                            glo:Password = use:Password
                            if SecurityCheck('JOBS - AUTHORISE REPAIR')
                                Case Missive('You do not have sufficient access to authorise this repair.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                            else
                                authoriseRepair = True          ! User has correct access level
                            end
                        end
                        glo:Password = savedPassword            ! Restore current user
                    else
                        authoriseRepair = True                  ! Current user has correct access level
                    end

                    if authoriseRepair
                        jobe:ExceedWarrantyRepairLimit = True
                    else
                        return Level:Fatal
                    end
            end
            !----------------------------------------------------------------------!
        else    ! bMode <> 0
            ! if not authorised
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            loop until Access:WARPARTS.Next()
                if wpr:Ref_Number <> job:Ref_Number then break.
                if wpr:Adjustment = 'YES' then cycle.           ! Ignore adjustments
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number = wpr:Part_Ref_Number
                if not Access:STOCK.Fetch(sto:Ref_Number_Key)
                    if sto:AttachBySolder then cycle.           ! Ignore parts attached by solder
                end
                return Level:Fatal                              ! Error - found a part that requires removing
            end

        end
    end

    return Level:Benign
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
locAction STRING(30)
    Code

    local:ErrorCode  = 0
    If AccessoryCheck('JOB')
        If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 1
        Else!If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 2
        End !If AccessoryMismatch(job:Ref_Number,'JOB')
    End !If AccessoryCheck('JOB')

    locAuditNotes = ''
    Case local:Errorcode
        Of 0 Orof 2
            locAuditNotes         = 'ACCESSORIES:<13,10>'
        Of 1
            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
    End !Case local:Errorcode

    !Add the list of accessories to the Audit Trail
    If job:Despatch_Type <> 'LOAN'
        Save_jac_ID = Access:JOBACC.SaveFile()
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        Loop
            If Access:JOBACC.NEXT()
               Break
            End !If
            If jac:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
        End !Loop
        Access:JOBACC.RestoreFile(Save_jac_ID)
    Else !If job:Despatch_Type <> 'LOAN'

        Save_lac_ID = Access:LOANACC.SaveFile()
        Access:LOANACC.ClearKey(lac:Ref_Number_Key)
        lac:Ref_Number = job:Ref_Number
        Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        Loop
            If Access:LOANACC.NEXT()
               Break
            End !If
            If lac:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
        End !Loop
        Access:LOANACC.RestoreFile(Save_lac_ID)

    End !If job:Despatch_Type <> 'LOAN'

    Case local:Errorcode
        Of 1
            !If Error show the Accessories that were actually tagged
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).
    End!Case local:Errorcode

    Case local:ErrorCode
        Of 0
            locAction        = 'COMPLETION VALIDATION: PASSED ACCESSORIES'
        Of 1
            locAction        = 'COMPLETION VALIDATION: FAILED ACCESSORIES'
        Of 2
            locAction        = 'COMPLETION VALIDATION: PASSED ACCESSORIES (FORCED)'
    End !Case local:ErrorCode

    IF (AddToAudit(job:Ref_Number,'JOB',locAction,locAuditNotes))
    END ! IF
    !Do not complete
    If local:ErrorCode = 1
        Return Level:Fatal
    End !If local:ErrorCode = 1

    Return Level:Benign
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
local:SecondExchangeUnit        Byte(0)
local:ExchangePartAttached      Byte(0)
local:AttachUnit                Byte(0)
local:AttachSecondUnit          Byte(0)
local:FoundExchange             Byte(0)
Code
        Case func:Type
            Of 'WAR'
                !Check to see if an Exchange Part line already exists -  (DBH: 19-12-2003)
                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                wpr:Part_Number = 'EXCH'
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                    Or wpr:Part_Number <> 'EXCH'      |
                        Then Break.  ! End If
                    If func:SecondUnit = True
                        If wpr:SecondExchangeUnit
                            Return
                        End !If func:SecondUnit And wpr:SecondExchangeUnit
                    Else !If func:SecondUnit = True
                        Return
                    End !If func:SecondUnit = True
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)
                If Access:WARPARTS.PrimeRecord() = Level:Benign
                    wpr:Part_Ref_Number = sto:Ref_Number
                    wpr:Ref_Number      = job:Ref_Number
                    wpr:Adjustment      = 'YES'
                    wpr:Part_Number     = 'EXCH'
                    wpr:Description     = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                    wpr:Quantity        = 1
                    wpr:Warranty_Part   = 'NO'
                    wpr:Exclude_From_Order = 'YES'
                    wpr:PartAllocated   = func:Allocated
                    If func:Allocated = 0
                        wpr:Status  = 'REQ'
                    Else !If func:Allocated = 0
                        wpr:Status  = 'PIK'
                    End !If func:Allocated = 0
                    wpr:ExchangeUnit        = True
                    wpr:SecondExchangeUnit = func:SecondUnit

                    If sto:Assign_Fault_Codes = 'YES'
                        !Try and get the fault codes. This key should get the only record
                        Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                        stm:Ref_Number  = sto:Ref_Number
                        stm:Part_Number = sto:Part_Number
                        stm:Location    = sto:Location
                        stm:Description = sto:Description
                        If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                            !Found
                            wpr:Fault_Code1  = stm:FaultCode1
                            wpr:Fault_Code2  = stm:FaultCode2
                            wpr:Fault_Code3  = stm:FaultCode3
                            wpr:Fault_Code4  = stm:FaultCode4
                            wpr:Fault_Code5  = stm:FaultCode5
                            wpr:Fault_Code6  = stm:FaultCode6
                            wpr:Fault_Code7  = stm:FaultCode7
                            wpr:Fault_Code8  = stm:FaultCode8
                            wpr:Fault_Code9  = stm:FaultCode9
                            wpr:Fault_Code10 = stm:FaultCode10
                            wpr:Fault_Code11 = stm:FaultCode11
                            wpr:Fault_Code12 = stm:FaultCode12
                        Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign

                    End !If sto:Assign_Fault_Codes = 'YES'
                    If Access:WARPARTS.TryInsert() = Level:Benign
                        !Insert Successful
                        AddToStockAllocation(wpr:Record_Number,'WAR',1,'',job:Engineer)
                    Else !If Access:WARPARTS.TryInsert() = Level:Benign
                        !Insert Failed
                        Access:WARPARTS.CancelAutoInc()
                    End !If Access:WARPARTS.TryInsert() = Level:Benign
                End !If Access:WARPARTS.PrimeRecord() = Level:Benign

            Of 'CHA'

                Save_par_ID = Access:PARTS.SaveFile()
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                par:Part_Number = 'EXCH'
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                       Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number      |
                    Or par:Part_Number <> 'EXCH'      |
                        Then Break.  ! End If
                    If func:SecondUnit = True
                        If par:SecondExchangeUnit
                            Return
                        End !If func:SecondUnit And wpr:SecondExchangeUnit
                    Else !If func:SecondUnit = True
                        Return
                    End !If func:SecondUnit = True
                End !Loop
                Access:PARTS.RestoreFile(Save_par_ID)

                if access:parts.primerecord() = level:benign
                    par:PArt_Ref_Number      = sto:Ref_Number
                    par:ref_number            = job:ref_number
                    par:adjustment            = 'YES'
                    par:part_number           = 'EXCH'
                    par:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                    par:quantity              = 1
                    par:warranty_part         = 'NO'
                    par:exclude_from_order    = 'YES'
                    par:PartAllocated         = func:Allocated
                    par:ExchangeUnit        = True
                    par:SecondExchangeUnit = func:SecondUnit

                    !see above for confusion
                    if func:allocated = 0 then
                        par:Status            = 'REQ'
                    ELSE
                        par:status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           par:Fault_Code1  = stm:FaultCode1
                           par:Fault_Code2  = stm:FaultCode2
                           par:Fault_Code3  = stm:FaultCode3
                           par:Fault_Code4  = stm:FaultCode4
                           par:Fault_Code5  = stm:FaultCode5
                           par:Fault_Code6  = stm:FaultCode6
                           par:Fault_Code7  = stm:FaultCode7
                           par:Fault_Code8  = stm:FaultCode8
                           par:Fault_Code9  = stm:FaultCode9
                           par:Fault_Code10 = stm:FaultCode10
                           par:Fault_Code11 = stm:FaultCode11
                           par:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Error
                           !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    End !If sto:Assign_Fault_Codes = 'YES'
                    if access:parts.insert()
                        access:parts.cancelautoinc()
                    end
                    AddToStockAllocation(par:Record_Number,'CHA',1,'',job:Engineer)
                End !If access:Prime
        End !Case func:Type
Local.AllocateExchangeOrderPart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundUnit         Byte(0)
Code

    !Check to see if a part already exists

    If job:Warranty_Job = 'YES'
        Save_wpr_ID = Access:WARPARTS.SaveFile()
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
                If wpr:SecondExchangeUnit
                    local:FoundUnit = True
                End !If wpr:SecondExchangeUnit
            Else !If func:SecondUnit
                local:FoundUnit = True
            End !If func:SecondUnit
            If local:FoundUnit
                wpr:Status  = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = True
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundUnit
        End !Loop
        Access:WARPARTS.RestoreFile(Save_wpr_ID)
    End !If job:Warranty_Job = 'YES'

    If local:FoundUnit = True
        If job:Chargeable_Job = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                    If par:SecondExchangeUnit
                        local:FoundUnit = True
                    End !If wpr:SecondExchangeUnit
                Else !If func:SecondUnit
                    local:FoundUnit = True
                End !If func:SecondUnit
                If local:FoundUnit
                    par:Status  = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = True
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundUnit
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'
    End !If FoundEXCHPart# = 0

    If local:FoundUnit = False
        If job:Engineer = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = glo:Password
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If job:Engineer = ''

         !Found
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If job:Warranty_Job = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = job:ref_number
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If job:Chargeable_Job = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = job:ref_number
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
            Case Missive('You must have a part set-up in Stock Control with a Part Number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If FoundEXCHPart# = 0
Local.AccessoryOutOfWarranty        Procedure()
Code
    If sto:Accessory = 'YES'
        !Is this job outside the Accessory Warranty Period?
        If job:DOP <> ''
            !Count the warranty period from the date the job was booked - L880 (DBH: 2/10/2003)
            If job:Date_Booked > job:DOP + sto:AccWarrantyPeriod
                !Outside the Accessory Period
                Case Missive('This accessory is Out Of Warranty. It must be a Chargeable Part.'&|
                  '<13,10>'&|
                  '<13,10>Do you wish to make the job CHARGEABLE only, or SPLIT?','ServiceBase 3g',|
                               'mquest.jpg','\Cancel|Split|Chargeable')
                    Of 3 ! Chargeable Button
                        !Are there Warranty Parts already attached?
                        FoundOtherParts# = 0
                        Access:WARPARTS_ALIAS.ClearKey(war_ali:Part_Number_Key)
                        war_ali:Ref_Number  = job:Ref_Number
                        Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                        Loop
                            If Access:WARPARTS_ALIAS.NEXT()
                               Break
                            End !If
                            If war_ali:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            If war_ali:Record_Number <> wpr:Record_Number
                                FoundOtherParts# = 1
                                Break
                            End !If war_ali:RecordNumber <> war:RecordNumber
                        End !Loop
                        If FoundOtherParts#
                            Case Missive('Cannot make Chargeable Only, there are other Warranty Parts attached to this job.'&|
                              '<13,10>'&|
                              '<13,10>Do you wish to make this job SPLIT?','ServiceBase 3g',|
                                           'mquest.jpg','\Cancel|/Split')
                                Of 2 ! Split Button
                                    Return 2 !Make split job
                                Of 1 ! Cancel Button
                                    Return 3 !Cancel, but do not cancel screen
                            End ! Case Missive
                        Else
                            Return 1 !Make Chargeable Only
                        End !If FoundOtherParts#
                    Of 2 ! Split Button
                        Return 2 !Make Split Job
                    Of 1 ! Cancel Button
                        Return 3 !Cancel screen
                End ! Case Missive
            End !If Today() > job:DOP + sto:AccWarrantyPeriod
        End !If job:DOP <> ''
    End !If sto:Accessory = 'YES'
    Return 0
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.SetQueueRecord PROCEDURE

  CODE
  !Work out the colour
  tmp:CColourFlag = 0
  Access:STOCK.Clearkey(sto:Ref_Number_Key)
  sto:Ref_Number  = par:Part_Ref_Number
  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Found
      tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
  Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Error
      tmp:CShelfLocation  = ''
  End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
  
  If par:Part_Number = 'EXCH'
      Case par:Status
          Of 'ORD'    !Order
              tmp:CColourFlag = 6
          Of 'REQ'    !Allocate
              tmp:CColourFlag = 1
          Of 'RTS'
              tmp:CColourFlag = 7
      End !Case par:Status
  Else !par:Part_Number = 'EXCH'
      If par:WebOrder = 1
          tmp:CColourFlag = 6
      Else
  
          If par:PartAllocated Or ~(RapidLocation(sto:Location) And sto:Location <> '')
              If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
                  tmp:CColourFlag = 1 !Green
              End !If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
  
              If par:order_number <> '' And par:date_received =''
                  tmp:CColourFlag = 3 !Red
              End !If par:order_number <> '' And par:date_received =''
  
              If par:Order_Number <> '' And par:Date_Received <> ''
                  tmp:CColourFlag = 0 !was 4 Gray   changed to black by Harvey by MSN conversation 28/05/
              End !If par:Order_Number <> '' And par:Date_Received <> ''
          Else !par:PartAllocated
              If par:Status = ''
                  tmp:CColourFlag = 1 !Green
              End !If par:Status = ''
  
              Case par:Status
                  Of 'PRO'
                      tmp:CColourFlag = 5 !Yellow
                  Of 'PIK'
                      tmp:CColourFlag = 2 !Navy
                  Of 'RET'
                      tmp:CColourFlag = 7 !Pink?
              End !Case par:Status
          End !par:PartAllocated
  
      End !par:WebOrder = 1
  End !par:Part_Number = 'EXCH'
  ! Start - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  If par:Correction = True
      tmp:CPartQuantity = 'COR'
  Else ! If par:Correction = True
      tmp:CPartQuantity = par:Quantity
  End ! If par:Correction = True
  ! End   - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  PARENT.SetQueueRecord
  SELF.Q.par:Part_Number_NormalFG = -1
  SELF.Q.par:Part_Number_NormalBG = -1
  SELF.Q.par:Part_Number_SelectedFG = -1
  SELF.Q.par:Part_Number_SelectedBG = -1
  SELF.Q.par:Description_NormalFG = -1
  SELF.Q.par:Description_NormalBG = -1
  SELF.Q.par:Description_SelectedFG = -1
  SELF.Q.par:Description_SelectedBG = -1
  SELF.Q.tmp:CPartQuantity_NormalFG = -1
  SELF.Q.tmp:CPartQuantity_NormalBG = -1
  SELF.Q.tmp:CPartQuantity_SelectedFG = -1
  SELF.Q.tmp:CPartQuantity_SelectedBG = -1
  SELF.Q.par:Quantity_NormalFG = -1
  SELF.Q.par:Quantity_NormalBG = -1
  SELF.Q.par:Quantity_SelectedFG = -1
  SELF.Q.par:Quantity_SelectedBG = -1
  SELF.Q.tmp:CShelfLocation_NormalFG = -1
  SELF.Q.tmp:CShelfLocation_NormalBG = -1
  SELF.Q.tmp:CShelfLocation_SelectedFG = -1
  SELF.Q.tmp:CShelfLocation_SelectedBG = -1
  SELF.Q.tmp:CColourFlag_NormalFG = -1
  SELF.Q.tmp:CColourFlag_NormalBG = -1
  SELF.Q.tmp:CColourFlag_SelectedFG = -1
  SELF.Q.tmp:CColourFlag_SelectedBG = -1
   
   
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Part_Number_NormalFG = 32768
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Part_Number_NormalFG = 8388608
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Part_Number_NormalFG = 255
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Part_Number_NormalFG = 8421504
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Part_Number_NormalFG = 16711935
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Part_Number_NormalFG = 8388736
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Part_Number_NormalFG = 8421631
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Part_Number_NormalFG = 0
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Description_NormalFG = 32768
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Description_NormalFG = 8388608
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Description_NormalFG = 255
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Description_NormalFG = 8421504
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Description_NormalFG = 16711935
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Description_NormalFG = 8388736
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Description_NormalFG = 8421631
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Description_NormalFG = 0
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.tmp:CPartQuantity_NormalFG = 32768
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8388608
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.tmp:CPartQuantity_NormalFG = 255
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8421504
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.tmp:CPartQuantity_NormalFG = 16711935
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8388736
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8421631
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:CPartQuantity_NormalFG = 0
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Quantity_NormalFG = 32768
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Quantity_NormalFG = 8388608
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Quantity_NormalFG = 255
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Quantity_NormalFG = 8421504
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Quantity_NormalFG = 16711935
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Quantity_NormalFG = 8388736
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Quantity_NormalFG = 8421631
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Quantity_NormalFG = 0
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.tmp:CShelfLocation_NormalFG = 32768
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.tmp:CShelfLocation_NormalFG = 8388608
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.tmp:CShelfLocation_NormalFG = 255
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.tmp:CShelfLocation_NormalFG = 8421504
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.tmp:CShelfLocation_NormalFG = 16711935
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.tmp:CShelfLocation_NormalFG = 8388736
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.tmp:CShelfLocation_NormalFG = 8421631
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:CShelfLocation_NormalFG = 0
     SELF.Q.tmp:CShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:CShelfLocation_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.tmp:CColourFlag_NormalFG = 32768
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.tmp:CColourFlag_NormalFG = 8388608
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.tmp:CColourFlag_NormalFG = 255
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.tmp:CColourFlag_NormalFG = 8421504
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.tmp:CColourFlag_NormalFG = 16711935
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.tmp:CColourFlag_NormalFG = 8388736
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.tmp:CColourFlag_NormalFG = 8421631
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:CColourFlag_NormalFG = 0
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 0
   END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW10.SetQueueRecord PROCEDURE

  CODE
  !Work out the colour
  tmp:WColourFlag = 0
  Access:STOCK.Clearkey(sto:Ref_Number_Key)
  sto:Ref_Number  = wpr:Part_Ref_Number
  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Found
      tmp:WShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
  Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Error
      tmp:WShelfLocation  = ''
  End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
  If wpr:Part_Number = 'EXCH'
      Case wpr:Status
          Of 'ORD'    !Order
              tmp:WColourFlag = 6
          Of 'REQ'    !Allocate
              tmp:WColourFlag = 1
          Of 'RTS'
              tmp:WColourFlag = 7
      End !Case par:Status
  Else !par:Part_Number = 'EXCH'
      If wpr:WebOrder = 1
          tmp:WColourFlag = 6
      Else
  
          If wpr:PartAllocated Or ~(RapidLocation(sto:Location) And sto:Location <> '')
              If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
                  tmp:WColourFlag = 1 !Green
              End !If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
  
              If wpr:order_number <> '' And wpr:date_received =''
                  tmp:WColourFlag = 3 !Red
              End !If wpr:order_number <> '' And wpr:date_received =''
  
              If wpr:Order_Number <> '' And wpr:Date_Received <> ''
                  tmp:WColourFlag = 0 !was  4 Gray
              End !If wpr:Order_Number <> '' And wpr:Date_Received <> ''
          Else !wpr:PartAllocated
              If wpr:Status = ''
                  tmp:WColourFlag = 1 !Green
              End !If wpr:Status = ''
  
              Case wpr:Status
                  Of 'PRO'
                      tmp:WColourFlag = 5 !Yellow
                  Of 'PIK'
                      tmp:WColourFlag = 2 !Navy
                  Of 'RET'
                      tmp:WColourFlag = 7 !Pink?
              End !Case wpr:Status
          End !wpr:PartAllocated
  
      End !wpr:WebOrder = 1
  End !If par:Part_Number = 'EXCH'
  ! Start - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  If wpr:Correction = True
      tmp:WPartQuantity = 'COR'
  Else ! If par:Correction = True
      tmp:WPartQuantity = wpr:Quantity
  End ! If par:Correction = True
  ! End   - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  PARENT.SetQueueRecord
  SELF.Q.wpr:Part_Number_NormalFG = -1
  SELF.Q.wpr:Part_Number_NormalBG = -1
  SELF.Q.wpr:Part_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Number_SelectedBG = -1
  SELF.Q.wpr:Description_NormalFG = -1
  SELF.Q.wpr:Description_NormalBG = -1
  SELF.Q.wpr:Description_SelectedFG = -1
  SELF.Q.wpr:Description_SelectedBG = -1
  SELF.Q.tmp:WPartQuantity_NormalFG = -1
  SELF.Q.tmp:WPartQuantity_NormalBG = -1
  SELF.Q.tmp:WPartQuantity_SelectedFG = -1
  SELF.Q.tmp:WPartQuantity_SelectedBG = -1
  SELF.Q.wpr:Quantity_NormalFG = -1
  SELF.Q.wpr:Quantity_NormalBG = -1
  SELF.Q.wpr:Quantity_SelectedFG = -1
  SELF.Q.wpr:Quantity_SelectedBG = -1
  SELF.Q.tmp:WShelfLocation_NormalFG = -1
  SELF.Q.tmp:WShelfLocation_NormalBG = -1
  SELF.Q.tmp:WShelfLocation_SelectedFG = -1
  SELF.Q.tmp:WShelfLocation_SelectedBG = -1
  SELF.Q.tmp:WColourFlag_NormalFG = -1
  SELF.Q.tmp:WColourFlag_NormalBG = -1
  SELF.Q.tmp:WColourFlag_SelectedFG = -1
  SELF.Q.tmp:WColourFlag_SelectedBG = -1
   
   
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Part_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Part_Number_NormalFG = 8388608
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Part_Number_NormalFG = 255
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Part_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Part_Number_NormalFG = 16711935
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Part_Number_NormalFG = 8388736
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Part_Number_NormalFG = 8421631
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Part_Number_NormalFG = 0
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Description_NormalFG = 32768
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Description_NormalFG = 8388608
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Description_NormalFG = 255
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Description_NormalFG = 8421504
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Description_NormalFG = 16711935
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Description_NormalFG = 8388736
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Description_NormalFG = 8421631
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Description_NormalFG = 0
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.tmp:WPartQuantity_NormalFG = 32768
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8388608
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.tmp:WPartQuantity_NormalFG = 255
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8421504
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.tmp:WPartQuantity_NormalFG = 16711935
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8388736
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8421631
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:WPartQuantity_NormalFG = 0
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Quantity_NormalFG = 32768
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Quantity_NormalFG = 8388608
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Quantity_NormalFG = 255
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Quantity_NormalFG = 8421504
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Quantity_NormalFG = 16711935
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Quantity_NormalFG = 8388736
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Quantity_NormalFG = 8421631
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Quantity_NormalFG = 0
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.tmp:WShelfLocation_NormalFG = 32768
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.tmp:WShelfLocation_NormalFG = 8388608
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.tmp:WShelfLocation_NormalFG = 255
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.tmp:WShelfLocation_NormalFG = 8421504
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.tmp:WShelfLocation_NormalFG = 16711935
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.tmp:WShelfLocation_NormalFG = 8388736
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.tmp:WShelfLocation_NormalFG = 8421631
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:WShelfLocation_NormalFG = 0
     SELF.Q.tmp:WShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:WShelfLocation_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.tmp:WColourFlag_NormalFG = 32768
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.tmp:WColourFlag_NormalFG = 8388608
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.tmp:WColourFlag_NormalFG = 255
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.tmp:WColourFlag_NormalFG = 8421504
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.tmp:WColourFlag_NormalFG = 16711935
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.tmp:WColourFlag_NormalFG = 8388736
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.tmp:WColourFlag_NormalFG = 8421631
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:WColourFlag_NormalFG = 0
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 0
   END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW39.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW39.SetQueueRecord PROCEDURE

  CODE
  !Work out the colour
  tmp:EColourFlag = 0

!  If epr:WebOrder = 1
!      tmp:EColourFlag = 6
!  Else
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = epr:Part_Ref_Number
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
            tmp:EShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          tmp:EShelfLocation    = ''
      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

      If epr:PartAllocated Or ~(RapidLocation(sto:Location) And sto:Location <> '')
          If epr:pending_ref_number <> '' And epr:order_number = '' And epr:date_ordered = ''
              tmp:EColourFlag = 1 !Green
          End !If epr:pending_ref_number <> '' And epr:order_number = '' And epr:date_ordered = ''

          If epr:order_number <> '' And epr:date_received =''
              tmp:EColourFlag = 3 !Red
          End !If epr:order_number <> '' And epr:date_received =''

          If epr:Order_Number <> '' And epr:Date_Received <> ''
              tmp:EColourFlag = 0 !was 4 Gray
          End !If epr:Order_Number <> '' And epr:Date_Received <> ''
      Else !epr:PartAllocated
          If epr:Status = ''
              tmp:EColourFlag = 1 !Green
          End !If epr:Status = ''

          Case epr:Status
              Of 'PRO'
                  tmp:EColourFlag = 5 !Yellow
              Of 'PIK'
                  tmp:EColourFlag = 2 !Navy
              Of 'RET'
                  tmp:EColourFlag = 7 !Pink?
          End !Case epr:Status
      End !epr:PartAllocated

!  End !epr:WebOrder = 1
  PARENT.SetQueueRecord
  SELF.Q.epr:Part_Number_NormalFG = -1
  SELF.Q.epr:Part_Number_NormalBG = -1
  SELF.Q.epr:Part_Number_SelectedFG = -1
  SELF.Q.epr:Part_Number_SelectedBG = -1
  SELF.Q.epr:Description_NormalFG = -1
  SELF.Q.epr:Description_NormalBG = -1
  SELF.Q.epr:Description_SelectedFG = -1
  SELF.Q.epr:Description_SelectedBG = -1
  SELF.Q.epr:Quantity_NormalFG = -1
  SELF.Q.epr:Quantity_NormalBG = -1
  SELF.Q.epr:Quantity_SelectedFG = -1
  SELF.Q.epr:Quantity_SelectedBG = -1
  SELF.Q.tmp:EShelfLocation_NormalFG = -1
  SELF.Q.tmp:EShelfLocation_NormalBG = -1
  SELF.Q.tmp:EShelfLocation_SelectedFG = -1
  SELF.Q.tmp:EShelfLocation_SelectedBG = -1
  SELF.Q.tmp:EColourFlag_NormalFG = -1
  SELF.Q.tmp:EColourFlag_NormalBG = -1
  SELF.Q.tmp:EColourFlag_SelectedFG = -1
  SELF.Q.tmp:EColourFlag_SelectedBG = -1
   
   
   IF (tmp:EColourFlag = 1)
     SELF.Q.epr:Part_Number_NormalFG = 32768
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 32768
   ELSIF(tmp:EColourFlag = 2)
     SELF.Q.epr:Part_Number_NormalFG = 8388608
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:EColourFlag = 3)
     SELF.Q.epr:Part_Number_NormalFG = 255
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 255
   ELSIF(tmp:EColourFlag = 4)
     SELF.Q.epr:Part_Number_NormalFG = 8421504
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:EColourFlag = 5)
     SELF.Q.epr:Part_Number_NormalFG = 16711935
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 16711935
   ELSIF(tmp:EColourFlag = 6)
     SELF.Q.epr:Part_Number_NormalFG = 8388736
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 8388736
   ELSIF(tmp:EColourFlag = 7)
     SELF.Q.epr:Part_Number_NormalFG = 8421631
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.epr:Part_Number_NormalFG = -1
     SELF.Q.epr:Part_Number_NormalBG = -1
     SELF.Q.epr:Part_Number_SelectedFG = -1
     SELF.Q.epr:Part_Number_SelectedBG = -1
   END
   IF (tmp:EColourFlag = 1)
     SELF.Q.epr:Description_NormalFG = 32768
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 32768
   ELSIF(tmp:EColourFlag = 2)
     SELF.Q.epr:Description_NormalFG = 8388608
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 8388608
   ELSIF(tmp:EColourFlag = 3)
     SELF.Q.epr:Description_NormalFG = 255
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 255
   ELSIF(tmp:EColourFlag = 4)
     SELF.Q.epr:Description_NormalFG = 8421504
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 8421504
   ELSIF(tmp:EColourFlag = 5)
     SELF.Q.epr:Description_NormalFG = 16711935
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 16711935
   ELSIF(tmp:EColourFlag = 6)
     SELF.Q.epr:Description_NormalFG = 8388736
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 8388736
   ELSIF(tmp:EColourFlag = 7)
     SELF.Q.epr:Description_NormalFG = 8421631
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 8421631
   ELSE
     SELF.Q.epr:Description_NormalFG = -1
     SELF.Q.epr:Description_NormalBG = -1
     SELF.Q.epr:Description_SelectedFG = -1
     SELF.Q.epr:Description_SelectedBG = -1
   END
   IF (tmp:EColourFlag = 1)
     SELF.Q.epr:Quantity_NormalFG = 32768
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 32768
   ELSIF(tmp:EColourFlag = 2)
     SELF.Q.epr:Quantity_NormalFG = 8388608
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 8388608
   ELSIF(tmp:EColourFlag = 3)
     SELF.Q.epr:Quantity_NormalFG = 255
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 255
   ELSIF(tmp:EColourFlag = 4)
     SELF.Q.epr:Quantity_NormalFG = 8421504
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 8421504
   ELSIF(tmp:EColourFlag = 5)
     SELF.Q.epr:Quantity_NormalFG = 16711935
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 16711935
   ELSIF(tmp:EColourFlag = 6)
     SELF.Q.epr:Quantity_NormalFG = 8388736
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 8388736
   ELSIF(tmp:EColourFlag = 7)
     SELF.Q.epr:Quantity_NormalFG = 8421631
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 8421631
   ELSE
     SELF.Q.epr:Quantity_NormalFG = -1
     SELF.Q.epr:Quantity_NormalBG = -1
     SELF.Q.epr:Quantity_SelectedFG = -1
     SELF.Q.epr:Quantity_SelectedBG = -1
   END
   IF (tmp:EColourFlag = 1)
     SELF.Q.tmp:EShelfLocation_NormalFG = 32768
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 32768
   ELSIF(tmp:EColourFlag = 2)
     SELF.Q.tmp:EShelfLocation_NormalFG = 8388608
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 8388608
   ELSIF(tmp:EColourFlag = 3)
     SELF.Q.tmp:EShelfLocation_NormalFG = 255
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 255
   ELSIF(tmp:EColourFlag = 4)
     SELF.Q.tmp:EShelfLocation_NormalFG = 8421504
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 8421504
   ELSIF(tmp:EColourFlag = 5)
     SELF.Q.tmp:EShelfLocation_NormalFG = 16711935
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 16711935
   ELSIF(tmp:EColourFlag = 6)
     SELF.Q.tmp:EShelfLocation_NormalFG = 8388736
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 8388736
   ELSIF(tmp:EColourFlag = 7)
     SELF.Q.tmp:EShelfLocation_NormalFG = 8421631
     SELF.Q.tmp:EShelfLocation_NormalBG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedFG = 16777215
     SELF.Q.tmp:EShelfLocation_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:EShelfLocation_NormalFG = -1
     SELF.Q.tmp:EShelfLocation_NormalBG = -1
     SELF.Q.tmp:EShelfLocation_SelectedFG = -1
     SELF.Q.tmp:EShelfLocation_SelectedBG = -1
   END
   IF (tmp:EColourFlag = 1)
     SELF.Q.tmp:EColourFlag_NormalFG = 32768
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 32768
   ELSIF(tmp:EColourFlag = 2)
     SELF.Q.tmp:EColourFlag_NormalFG = 8388608
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 8388608
   ELSIF(tmp:EColourFlag = 3)
     SELF.Q.tmp:EColourFlag_NormalFG = 255
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 255
   ELSIF(tmp:EColourFlag = 4)
     SELF.Q.tmp:EColourFlag_NormalFG = 8421504
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 8421504
   ELSIF(tmp:EColourFlag = 5)
     SELF.Q.tmp:EColourFlag_NormalFG = 16711935
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 16711935
   ELSIF(tmp:EColourFlag = 6)
     SELF.Q.tmp:EColourFlag_NormalFG = 8388736
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 8388736
   ELSIF(tmp:EColourFlag = 7)
     SELF.Q.tmp:EColourFlag_NormalFG = 8421631
     SELF.Q.tmp:EColourFlag_NormalBG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:EColourFlag_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:EColourFlag_NormalFG = -1
     SELF.Q.tmp:EColourFlag_NormalBG = -1
     SELF.Q.tmp:EColourFlag_SelectedFG = -1
     SELF.Q.tmp:EColourFlag_SelectedBG = -1
   END


BRW39.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

DeleteExchangeReason PROCEDURE (func:String)          !Generated from procedure template - Window

tmp:Close            BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Field Changed'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Field Changed Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('The following Exchange Unit has been returned. Please provide a reason why.'),AT(238,115,204,16),USE(?Prompt1),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Field'),AT(244,144),USE(?FieldType),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           TEXT,AT(238,176,204,120),USE(glo:EDI_Reason),VSCROLL,FONT(,,0101010H,,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Close)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020446'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DeleteExchangeReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?FieldType{prop:Text} = Clip(func:String)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020446'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020446'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020446'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      If glo:EDI_Reason <> ''
          tmp:Close = 1
          Post(event:CloseWindow)
      End !glo:EDI_Reason <> ''
    OF ?Cancel
      ThisWindow.Update
      tmp:Close = 0
      Post(event:Closewindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AllocateBookingEngineerToJob PROCEDURE                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Engineer_Temp        STRING(3)
Engineer_Name_Temp   STRING(30)
engineer_code_temp   STRING(3)
window               WINDOW('Allocate Engineer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Allocate Engineer'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Select Engineer'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(268,208,124,10),USE(Engineer_Name_Temp),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(400,204),USE(?Engineer_Lookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(300,258),USE(?Ok),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

AddEngineer     Routine
    Do_Status# = 0

    If job:Engineer <> Engineer_Temp

        IF job:Date_Completed <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !IF job:Date_Completed <> ''
        If job:Invoice_Number <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If job:Invoice_Number <> ''
        If Error# = 0
            If Engineer_Temp = ''
                IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)))
                END ! IF

                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = use:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign

            Else !If Engineer_Temp = ''
                Case Missive('Are you sure you want to change the Engineer?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                        IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER CHANGED TO ' & CLip(job:engineer),'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)))
                        END ! IF

                        Engineer_Temp   = job:Engineer

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                    Of 1 ! No Button
                        job:Engineer    = Engineer_Temp
                End ! Case Missive
            End !If Engineer_Temp = ''
        Else !If Error# = 0
            job:Engineer    = engineer_temp
        End !If Error# = 0

    End !If job:Engineer <> Engineer_Temp

    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
        !Do time_remaining                      ! Turnaround time skipped
    End!If do_status# = 1

    Access:Jobs.TryUpdate()                     ! Added because of Joe's requirement...
show_engineer       Routine
    access:users.clearkey(use:user_code_key)
    use:user_code = engineer_code_temp
    if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    Else!if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = ''
    end!!if access:users.fetch(use:user_code_key) = Level:Benign
    Display(?engineer_name_temp)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020444'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AllocateBookingEngineerToJob')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Engineer_Temp = job:Engineer
  
  engineer_code_temp = job:Engineer
  
  do show_engineer
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Engineer_Lookup
      Access:Users.ClearKey(use:password_key)
      use:password = glo:password
      if not Access:Users.Fetch(use:password_key)
          GLO:Select1 = use:Location          ! Only show engineers with same site location as the current user
      end
      
      GlobalRequest   = SelectRecord
      PickLocationEngineers
      glo:Select1 = ''
      
      case globalresponse
          of requestcompleted
              engineer_code_temp = use:user_code
          of requestcancelled
              engineer_code_temp = ''
      end!case globalreponse
      Do show_engineer
    OF ?Ok
      ! Allocate Engineer
      
      job:Engineer = engineer_code_temp
      
      if job:Engineer = ''            ! Field validation
          select(?Engineer_Name_Temp)
          cycle
      else
          Access:Users.Clearkey(use:User_Code_Key)
          use:User_Code = job:Engineer
          if Access:Users.Fetch(use:User_Code_Key)
              select(?Engineer_Name_Temp)
              cycle
          end
      end
      
      !If the job is at the ARC, check that it has been
      !through Waybill confirmation - L921 (DBH: 13-08-2003)
      Error# = 0
      
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
      
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
      
      If use:Location = tra:SiteLocation
          Access:LOCATLOG.ClearKey(lot:NewLocationKey)
          lot:RefNumber   = job:Ref_Number
          lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
          If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
              !Found
          Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
              !Error
              Case Missive('Error! This job is not at the ARC Location. Please ensure that the Waybill Confirmation procedure has been completed.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Error# = 1
          End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
      End !If use:Location = tmp:ARCSiteLocation
      If Error# = 0
          Do AddEngineer
      End !If Error# = 0
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020444'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020444'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020444'&'0')
      ***
    OF ?Ok
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?Close
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
GenericFaultCodes_WithTradeCodes PROCEDURE (Long f:Job,String f:type,String f:Req,String f:Acc,String f:Man,String f:WCrg,String f:WRep,String f:Comp) !Generated from procedure template - Window

FilesOpened          BYTE
tmp:Required         BYTE(0)
save_taf_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_tfo_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
ActionMessage        CSTRING(40)
JobFaultCodes        GROUP,PRE(jfc)
JF1                  STRING(30)
JF2                  STRING(30)
JF3                  STRING(30)
JF4                  STRING(30)
JF5                  STRING(30)
JF6                  STRING(30)
JF7                  STRING(30)
JF8                  STRING(30)
JF9                  STRING(30)
JF10                 STRING(255)
JF11                 STRING(255)
JF12                 STRING(255)
                     END
TradeFaultCodes      GROUP,PRE(tfc)
TF1                  STRING(30)
TF2                  STRING(30)
TF3                  STRING(30)
TF4                  STRING(30)
TF5                  STRING(30)
TF6                  STRING(30)
TF7                  STRING(30)
TF8                  STRING(30)
TF9                  STRING(30)
TF10                 STRING(30)
TF11                 STRING(30)
TF12                 STRING(30)
                     END
FaultCodesDescription GROUP,PRE(jfd)
Description1         STRING(255)
Description2         STRING(255)
Description3         STRING(255)
Description4         STRING(255)
Description5         STRING(255)
Description6         STRING(255)
Description7         STRING(255)
Description8         STRING(255)
Description9         STRING(255)
Description10        STRING(255)
Description11        STRING(255)
Description12        STRING(255)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Fault Codes'),AT(8,6),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,4,672,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(4,20,672,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(8,24,664,36),USE(?GroupTip),BOXED,TRN,HIDE
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(624,6),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,68,672,312),USE(?Sheet1),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Manufacturers Fault Codes'),USE(?Tab1),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(136,81,420,248),USE(?FieldsGroup)
                             PROMPT('Fault Code 1:'),AT(8,84),USE(?jfc:JF1:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,84,124,10),USE(jfc:JF1),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,81),USE(?Lookup),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,84,168,10),USE(jfd:Description1),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             ENTRY(@s255),AT(312,105,168,10),USE(jfd:Description2),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             BUTTON,AT(276,81),USE(?PopCalendar),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             PROMPT('Fault Code 2:'),AT(8,105),USE(?jfc:JF2:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,105,124,10),USE(jfc:JF2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,100),USE(?Lookup:2),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,100),USE(?PopCalendar:2),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             PROMPT('Fault Code 3:'),AT(8,124),USE(?jfc:JF3:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,124,124,10),USE(jfc:JF3),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,121),USE(?Lookup:3),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,124,168,10),USE(jfd:Description3),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             BUTTON,AT(276,121),USE(?PopCalendar:3),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             PROMPT('Fault Code 4:'),AT(8,145),USE(?jfc:JF4:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,145,124,10),USE(jfc:JF4),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,140),USE(?PopCalendar:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,140),USE(?Lookup:4),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,145,168,10),USE(jfd:Description4),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 5:'),AT(8,164),USE(?jfc:JF5:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,164,124,10),USE(jfc:JF5),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,161),USE(?PopCalendar:5),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,161),USE(?Lookup:5),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,164,168,10),USE(jfd:Description5),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 6:'),AT(8,185),USE(?jfc:JF6:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,185,124,10),USE(jfc:JF6),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,180),USE(?PopCalendar:6),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,182),USE(?Lookup:6),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,185,168,10),USE(jfd:Description6),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 7:'),AT(8,204),USE(?jfc:JF7:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,204,124,10),USE(jfc:JF7),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,201),USE(?PopCalendar:7),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,201),USE(?Lookup:7),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,204,168,10),USE(jfd:Description7),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 8:'),AT(8,225),USE(?jfc:JF8:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,225,124,10),USE(jfc:JF8),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,220),USE(?PopCalendar:8),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,222),USE(?Lookup:8),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,225,168,10),USE(jfd:Description8),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 9:'),AT(8,244),USE(?jfc:JF9:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(140,244,124,10),USE(jfc:JF9),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,241),USE(?PopCalendar:9),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,241),USE(?Lookup:9),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,244,168,10),USE(jfd:Description9),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 10:'),AT(8,265),USE(?jfc:JF10:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(140,265,124,10),USE(jfc:JF10),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,260),USE(?PopCalendar:10),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,262),USE(?Lookup:10),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,265,168,10),USE(jfd:Description10),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 11:'),AT(8,284),USE(?jfc:JF11:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(140,284,124,10),USE(jfc:JF11),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,281),USE(?PopCalendar:11),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,281),USE(?Lookup:11),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             ENTRY(@s255),AT(312,284,168,10),USE(jfd:Description11),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             ENTRY(@s255),AT(312,305,168,10),USE(jfd:Description12),SKIP,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Fault Code 12:'),AT(8,305),USE(?jfc:JF12:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(140,305,124,10),USE(jfc:JF12),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(276,300),USE(?PopCalendar:12),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             BUTTON,AT(276,302),USE(?Lookup:12),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           END
                         END
                         TAB('Trade Fault Codes'),USE(?Tab2),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(134,81,149,197),USE(?FieldsGroup2)
                             PROMPT('Fault Code 1'),AT(8,84),USE(?tfc:TF1:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,84,124,10),USE(tfc:TF1),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 1'),TIP('Fault Code 1'),UPR
                             BUTTON,AT(208,84,10,10),USE(?PopCalendar:13),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,84,10,10),USE(?tralookup:1),SKIP,HIDE,ICON('list3.ico')
                             PROMPT('Fault Code 2'),AT(8,100),USE(?tfc:TF2:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,100,124,10),USE(tfc:TF2),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 2'),TIP('Fault Code 2'),UPR
                             BUTTON,AT(208,100,10,10),USE(?PopCalendar:14),HIDE,ICON('CALENDA2.ICO')
                             PROMPT('Fault Code 3'),AT(8,116),USE(?tfc:TF3:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,116,124,10),USE(tfc:TF3),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 3'),TIP('Fault Code 3'),UPR
                             BUTTON,AT(208,116,10,10),USE(?PopCalendar:15),HIDE,ICON('CALENDA2.ICO')
                             PROMPT('Fault Code 4'),AT(8,132),USE(?tfc:TF4:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,132,124,10),USE(tfc:TF4),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 4'),TIP('Fault Code 4'),UPR
                             BUTTON,AT(208,132,10,10),USE(?PopCalendar:16),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,132,10,10),USE(?tralookup:4),SKIP,HIDE,ICON('list3.ico')
                             PROMPT('Fault Code 5'),AT(8,148),USE(?tfc:TF5:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,148,124,10),USE(tfc:TF5),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 5'),TIP('Fault Code 5'),UPR
                             BUTTON,AT(208,148,10,10),USE(?PopCalendar:17),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,148,10,10),USE(?tralookup:5),SKIP,HIDE,ICON('list3.ico')
                             PROMPT('Fault Code 6'),AT(8,164),USE(?tfc:TF6:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,164,124,10),USE(tfc:TF6),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 6'),TIP('Fault Code 6'),UPR
                             BUTTON,AT(208,164,10,10),USE(?PopCalendar:18),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,100,10,10),USE(?tralookup:2),SKIP,HIDE,ICON('list3.ico')
                             BUTTON,AT(268,116,10,10),USE(?tralookup:3),SKIP,HIDE,ICON('list3.ico')
                             PROMPT('Fault Code 7'),AT(8,180),USE(?tfc:TF7:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,180,124,10),USE(tfc:TF7),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 7'),TIP('Fault Code 7'),UPR
                             BUTTON,AT(208,180,10,10),USE(?PopCalendar:19),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,180,10,10),USE(?tralookup:7),SKIP,HIDE,ICON('list3.ico')
                             PROMPT('Fault Code 8'),AT(8,196),USE(?tfc:TF8:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,196,124,10),USE(tfc:TF8),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 8'),TIP('Fault Code 8'),UPR
                             BUTTON,AT(208,196,10,10),USE(?PopCalendar:20),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,196,10,10),USE(?tralookup:8),SKIP,HIDE,ICON('list3.ico')
                             PROMPT('Fault Code 9'),AT(8,212),USE(?tfc:TF9:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,212,124,10),USE(tfc:TF9),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 9'),TIP('Fault Code 9'),UPR
                             BUTTON,AT(208,212,10,10),USE(?PopCalendar:21),HIDE,ICON('CALENDA2.ICO')
                             PROMPT('Fault Code 10'),AT(8,228),USE(?tfc:TF10:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,228,124,10),USE(tfc:TF10),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 10'),TIP('Fault Code 10'),UPR
                             BUTTON,AT(208,228,10,10),USE(?PopCalendar:22),HIDE,ICON('CALENDA2.ICO')
                             PROMPT('Fault Code 11'),AT(8,244),USE(?tfc:TF11:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,244,124,10),USE(tfc:TF11),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 11'),TIP('Fault Code 11'),UPR
                             BUTTON,AT(208,244,10,10),USE(?PopCalendar:23),HIDE,ICON('CALENDA2.ICO')
                             PROMPT('Fault Code 12'),AT(8,260),USE(?tfc:TF12:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(140,260,124,10),USE(tfc:TF12),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code 12'),TIP('Fault Code 12'),UPR
                             BUTTON,AT(208,260,10,10),USE(?PopCalendar:24),HIDE,ICON('CALENDA2.ICO')
                             BUTTON,AT(268,212,10,10),USE(?tralookup:9),SKIP,HIDE,ICON('list3.ico')
                             BUTTON,AT(268,244,10,10),USE(?tralookup:11),SKIP,HIDE,ICON('list3.ico')
                             BUTTON,AT(268,164,10,10),USE(?tralookup:6),SKIP,HIDE,ICON('list3.ico')
                             BUTTON,AT(268,228,10,10),USE(?tralookup:10),SKIP,HIDE,ICON('list3.ico')
                             BUTTON,AT(268,260,10,10),USE(?tralookup:12),SKIP,HIDE,ICON('list3.ico')
                           END
                         END
                         TAB('Fault Codes'),USE(?Tab3),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('NO Fault Codes are required for this job.'),AT(84,178),USE(?Prompt25),CENTER,FONT(,22,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(608,384),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Fault_Coding        Routine   ! Manufacturers Fault Codes

    tmp:Required = 0
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = f:WCrg
    If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        If cha:force_warranty = 'YES'
            If f:WRep <> ''
                Case f:Type
                    Of 'C'
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = f:Man
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = f:WRep
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                tmp:Required = 1
                            End !If rtd:CompFaultCoding

                        Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    Of 'W'
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = f:Man
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = f:WRep
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                tmp:Required = 1
                            End !If rtd:CompFaultCoding

                        Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                End !Case f:Type

            End !If f:WRep <> ''
        End
    end !if

    found# = 0
    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = f:Man
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> f:Man      |
            then break.  ! end if

        If maf:MainFault
            If man:UseInvTextForFaults
                Cycle
            End !If man:UseInvTextForFaults
        End !If maf:MainFault
        IF found# = 0
            found# = 1
            Unhide(?tab1)
        End!IF found# = 0
        hide# = 0
        req#  = 0


        If maf:Compulsory_At_Booking = 'YES'
            If tmp:Required
                req# = 1
            End!If tmp:Required
        Else !If maf:Compulsory_At_Booking = 'YES' and tmp:Required
            If f:req = InsertRecord
                Hide# = 1
            End !If f:req = InsertRecord
        End !If maf:Compulsory_At_Booking = 'YES' and tmp:Required

        If maf:Compulsory = 'YES' or tmp:Required  ! Changed from AND to OR (27th Aug 2002)
            If f:Req <> InsertRecord
                req# = 1
            End !If f:Req <> InsertRecord
        End !If maf:Compulsory = 'YES' and tmp:Required

        If maf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_mfo_id = access:manfaulo.savefile()
            access:manfaulo.clearkey(mfo:field_key)
            mfo:manufacturer = maf:manufacturer
            mfo:field_number = maf:field_number
            set(mfo:field_key,mfo:field_key)
            loop
                if access:manfaulo.next()
                   break
                end !if
                if mfo:manufacturer <> maf:manufacturer      |
                or mfo:field_number <> maf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = mfo:field
            end !loop
            access:manfaulo.restorefile(save_mfo_id)
            Case maf:field_number
                of 1
                    If jfc:JF1 = ''
                        jfc:JF1 = field"
                    End!If jfc:JF = ''
                of 2
                    If jfc:JF2 = ''
                        jfc:JF2 = field"
                    End!If jfc:JF = ''
                of 3
                    If jfc:JF3 = ''
                        jfc:JF3 = field"
                    End!If jfc:JF = ''
                of 4
                    If jfc:JF4 = ''
                        jfc:JF4 = field"
                    End!If jfc:JF = ''
                of 5
                    If jfc:JF5 = ''
                        jfc:JF5 = field"
                    End!If jfc:JF = ''
                of 6
                    If jfc:JF6 = ''
                        jfc:JF6 = field"
                    End!If jfc:JF = ''
                of 7
                    If jfc:JF7 = ''
                        jfc:JF7 = field"
                    End!If jfc:JF = ''
                of 8
                    If jfc:JF8 = ''
                        jfc:JF8 = field"
                    End!If jfc:JF = ''
                of 9
                    If jfc:JF9 = ''
                        jfc:JF9 = field"
                    End!If jfc:JF = ''
                of 10
                    If jfc:JF10 = ''
                        jfc:JF10 = field"
                    End!If jfc:JF = ''
                of 11
                    If jfc:JF11 = ''
                        jfc:JF11 = field"
                    End!If jfc:JF = ''
                of 12
                    If jfc:JF12 = ''
                        jfc:JF12 = field"
                    End!If jfc:JF = ''
            End!Case maf:field_number
        End!If maf:lookup = 'YES'

        Case maf:field_number
            Of 1
                If hide# = 0
                    Unhide(?jfc:JF1:prompt)
                    Unhide(?jfc:JF1)
                    ?jfc:JF1:prompt{prop:text} = maf:field_name
                    If req# = 1
                        ?jfc:JF1{prop:req} = 1
                    End!If req# = 1

                    Case maf:field_type
                        Of 'DATE'
                            ?jfc:JF1{prop:text} = Clip(maf:DateType)
                            Unhide(?popcalendar)

                        Of 'STRING'
                            If maf:restrictlength
                                ?jfc:JF1{prop:text} = '@s' & maf:lengthto
                            Else!If maf:restrictlength
                                ?jfc:JF1{prop:text} = '@s30'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 1
                                mfo:Field        = jfc:JF1
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description1{prop:Hide} = 0
                                    jfd:Description1    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            End!If maf:lookup = 'YES'
                        Of 'NUMBER'
                            If maf:restrictlength
                                ?jfc:JF1{prop:text} = '@n_' & maf:lengthto
                            Else!If maf:restrictlength
                                ?jfc:JF1{prop:text} = '@n_9'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup)
                            End!If maf:lookup = 'YES'
                    End!Case maf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    Unhide(?jfc:JF2:prompt)
                    Unhide(?jfc:JF2)
                    ?jfc:JF2:prompt{prop:text} = maf:field_name
                    If req# = 1
                        ?jfc:JF2{prop:req} = 1
                    End!If req# = 1
                    Case maf:field_type
                        Of 'DATE'
                            ?jfc:JF2{prop:text} = Clip(maf:DateType)
                            Unhide(?popcalendar:2)

                        Of 'STRING'
                            If maf:restrictlength
                                ?jfc:JF2{prop:text} = '@s' & maf:lengthto
                            Else!If maf:restrictlength
                                ?jfc:JF2{prop:text} = '@s30'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup:2)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 2
                                mfo:Field        = jfc:JF2
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description2{prop:Hide} = 0
                                    jfd:Description2    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            End!If maf:lookup = 'YES'
                        Of 'NUMBER'

                            If maf:restrictlength
                                ?jfc:JF2{prop:text} = '@n_' & maf:lengthto
                            Else!If maf:restrictlength
                                ?jfc:JF2{prop:text} = '@n_9'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup:2)
                            End!If maf:lookup = 'YES'
                    End!Case maf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    unhide(?jfc:JF3:prompt)
                    unhide(?jfc:JF3)
                    ?jfc:JF3:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF3{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF3{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:3)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF3{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF3{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:3)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 3
                                mfo:Field        = jfc:JF3
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description3{prop:Hide} = 0
                                    jfd:Description3    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF3{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF3{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:3)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    unhide(?jfc:JF4:prompt)
                    unhide(?jfc:JF4)
                    ?jfc:JF4:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF4{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF4{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:4)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF4{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF4{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:4)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 4
                                mfo:Field        = jfc:JF4
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description4{prop:Hide} = 0
                                    jfd:Description4    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF4{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF4{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:4)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    unhide(?jfc:JF5:prompt)
                    unhide(?jfc:JF5)
                    ?jfc:JF5:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF5{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF5{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:5)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF5{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF5{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:5)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 5
                                mfo:Field        = jfc:JF5
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description5{prop:Hide} = 0
                                    jfd:Description5    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF5{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF5{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:5)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    unhide(?jfc:JF6:prompt)
                    unhide(?jfc:JF6)
                    ?jfc:JF6:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF6{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF6{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:6)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF6{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF6{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:6)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 6
                                mfo:Field        = jfc:JF6
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description6{prop:Hide} = 0
                                    jfd:Description6    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF6{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF6{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:6)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    unhide(?jfc:JF7:prompt)
                    unhide(?jfc:JF7)
                    ?jfc:JF7:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF7{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF7{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:7)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF7{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF7{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:7)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 7
                                mfo:Field        = jfc:JF7
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description7{prop:Hide} = 0
                                    jfd:Description7    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF7{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF7{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:7)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    unhide(?jfc:JF8:prompt)
                    unhide(?jfc:JF8)
                    ?jfc:JF8:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF8{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF8{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:8)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF8{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF8{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:8)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 8
                                mfo:Field        = jfc:JF8
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description8{prop:Hide} = 0
                                    jfd:Description8    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF8{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF8{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:8)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    unhide(?jfc:JF9:prompt)
                    unhide(?jfc:JF9)
                    ?jfc:JF9:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF9{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF9{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:9)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF9{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF9{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:9)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 9
                                mfo:Field        = jfc:JF9
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description9{prop:Hide} = 0
                                    jfd:Description9    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF9{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF9{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:9)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    unhide(?jfc:JF10:prompt)
                    unhide(?jfc:JF10)
                    ?jfc:JF10:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF10{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF10{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:10)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF10{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF10{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:10)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 10
                                mfo:Field        = jfc:JF10
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description10{prop:Hide} = 0
                                    jfd:Description10    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF10{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF10{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:10)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    unhide(?jfc:JF11:prompt)
                    unhide(?jfc:JF11)
                    ?jfc:JF11:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF11{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF11{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:11)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF11{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF11{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:11)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 11
                                mfo:Field        = jfc:JF11
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description11{prop:Hide} = 0
                                    jfd:Description11    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF11{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF11{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:11)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    unhide(?jfc:JF12:prompt)
                    unhide(?jfc:JF12)
                    ?jfc:JF12:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jfc:JF12{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jfc:JF12{prop:text} = Clip(maf:DateType)
                            unhide(?popcalendar:12)

                        of 'STRING'
                            if maf:restrictlength
                                ?jfc:JF12{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF12{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:12)
                                Access:MANFAULO.ClearKey(mfo:Field_Key)
                                mfo:Manufacturer = f:Man
                                mfo:Field_Number = 12
                                mfo:Field        = jfc:JF12
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Found
                                    ?jfd:Description12{prop:Hide} = 0
                                    jfd:Description12    = mfo:Description
                                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            if maf:restrictlength
                                ?jfc:JF12{prop:text} = '@n_' & maf:lengthto
                            else!if maf:restrictlength
                                ?jfc:JF12{prop:text} = '@n_9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:12)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
        End
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()
    Display()

    found# = 0
    setcursor(cursor:wait)
    save_taf_id = access:trafault.savefile()
    access:trafault.clearkey(taf:field_number_key)
    taf:accountnumber = f:Acc
    set(taf:field_number_key,taf:field_number_key)
    loop
        if access:trafault.next()
           break
        end !if
        if taf:accountnumber <> f:Acc      |
            then break.  ! end if
        if found# = 0
            found# = 1
            unhide(?Tab2)
        End!if found# = 0
        hide# = 0
        req#  = 0

        If taf:Compulsory_At_Booking = 'YES'
            req# = 1
        Else !If taf:Compulsory_At_Booking = 'YES'
            If f:Req = InsertRecord
                Hide# = 1
            End !If f:Req = InsertRecord
        End !If taf:Compulsory_At_Booking = 'YES'

        If taf:Compulsory = 'YES' and f:Req <> InsertRecord
            req# = 1
        End !If taf:Compulsory = 'YES' and f:Req <> InsertRecord

        If taf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_tfo_id = access:trafaulo.savefile()
            access:trafaulo.clearkey(tfo:field_key)
            tfo:accountnumber = taf:accountnumber
            tfo:field_number = taf:field_number
            set(tfo:field_key,tfo:field_key)
            loop
                if access:trafaulo.next()
                   break
                end !if
                if tfo:accountnumber <> taf:accountnumber      |
                or tfo:field_number <> taf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = tfo:field
            end !loop
            access:trafaulo.restorefile(save_tfo_id)
            Case taf:field_number
                of 1
                    If tfc:TF1 = ''
                        tfc:TF1 = field"
                    End!If tfc:TF = ''
                of 2
                    If tfc:TF2 = ''
                        tfc:TF2 = field"
                    End!If tfc:TF = ''
                of 3
                    If tfc:TF3 = ''
                        tfc:TF3 = field"
                    End!If tfc:TF = ''
                of 4
                    If tfc:TF4 = ''
                        tfc:TF4 = field"
                    End!If tfc:TF = ''
                of 5
                    If tfc:TF5 = ''
                        tfc:TF5 = field"
                    End!If tfc:TF = ''
                of 6
                    If tfc:TF6 = ''
                        tfc:TF6 = field"
                    End!If tfc:TF = ''
                of 7
                    If tfc:TF7 = ''
                        tfc:TF7 = field"
                    End!If tfc:TF = ''
                of 8
                    If tfc:TF8 = ''
                        tfc:TF8 = field"
                    End!If tfc:TF = ''
                of 9
                    If tfc:TF9 = ''
                        tfc:TF9 = field"
                    End!If tfc:TF = ''
                of 10
                    If tfc:TF10 = ''
                        tfc:TF10 = field"
                    End!If tfc:TF = ''
                of 11
                    If tfc:TF11 = ''
                        tfc:TF11 = field"
                    End!If tfc:TF = ''
                of 12
                    If tfc:TF12 = ''
                        tfc:TF12 = field"
                    End!If tfc:TF = ''
            End!Case taf:field_number
        End!If taf:lookup = 'YES'

        Case taf:field_number
            Of 1
                If hide# = 0
                    Unhide(?tfc:TF1:prompt)
                    Unhide(?tfc:TF1)
                    ?tfc:TF1:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?tfc:TF1{prop:req} = 1
                    End!If req# = 1

                    Case taf:field_type
                        Of 'DATE'
                            ?tfc:TF1{prop:text} = Clip(maf:DateType)
                            ?tfc:TF1{prop:width} = 64
                            Unhide(?popcalendar:13)

                        Of 'STRING'
                            ?tfc:TF1{prop:width} = 124
                            If taf:restrictlength
                                ?tfc:TF1{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?tfc:TF1{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'
                            ?tfc:TF1{prop:width} = 64
                            If taf:restrictlength
                                ?tfc:TF1{prop:text} = '@n_' & taf:lengthto
                            Else!If taf:restrictlength
                                ?tfc:TF1{prop:text} = '@n_9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    Unhide(?tfc:TF2:prompt)
                    Unhide(?tfc:TF2)
                    ?tfc:TF2:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?tfc:TF2{prop:req} = 1
                    End!If req# = 1
                    Case taf:field_type
                        Of 'DATE'
                            ?tfc:TF2{prop:text} = Clip(maf:DateType)
                            ?tfc:TF2{prop:width} = 64
                            Unhide(?popcalendar:14)

                        Of 'STRING'
                            ?tfc:TF2{prop:width} = 124
                            If taf:restrictlength
                                ?tfc:TF2{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?tfc:TF2{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'

                            ?tfc:TF2{prop:width} = 64
                            If taf:restrictlength
                                ?tfc:TF2{prop:text} = '@n_' & taf:lengthto
                            Else!If taf:restrictlength
                                ?tfc:TF2{prop:text} = '@n_9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    unhide(?tfc:TF3:prompt)
                    unhide(?tfc:TF3)
                    ?tfc:TF3:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF3{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF3{prop:text} = Clip(maf:DateType)
                            ?tfc:TF3{prop:width} = 64
                            unhide(?popcalendar:15)

                        of 'STRING'
                            ?tfc:TF3{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF3{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF3{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF3{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF3{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF3{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    unhide(?tfc:TF4:prompt)
                    unhide(?tfc:TF4)
                    ?tfc:TF4:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF4{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF4{prop:text} = Clip(maf:DateType)
                            ?tfc:TF4{prop:width} = 64
                            unhide(?popcalendar:16)

                        of 'STRING'
                            ?tfc:TF4{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF4{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF4{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF4{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF4{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF4{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    unhide(?tfc:TF5:prompt)
                    unhide(?tfc:TF5)
                    ?tfc:TF5:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF5{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF5{prop:text} = Clip(maf:DateType)
                            ?tfc:TF5{prop:width} = 64
                            unhide(?popcalendar:17)

                        of 'STRING'
                            ?tfc:TF5{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF5{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF5{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF5{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF5{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF5{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    unhide(?tfc:TF6:prompt)
                    unhide(?tfc:TF6)
                    ?tfc:TF6:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF6{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF6{prop:text} = Clip(maf:DateType)
                            ?tfc:TF6{prop:width} = 64
                            unhide(?popcalendar:18)

                        of 'STRING'
                            ?tfc:TF6{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF6{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF6{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF6{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF6{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF6{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    unhide(?tfc:TF7:prompt)
                    unhide(?tfc:TF7)
                    ?tfc:TF7:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF7{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF7{prop:text} = Clip(maf:DateType)
                            ?tfc:TF7{prop:width} = 64
                            unhide(?popcalendar:19)

                        of 'STRING'
                            ?tfc:TF7{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF7{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF7{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF7{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF7{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF7{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    unhide(?tfc:TF8:prompt)
                    unhide(?tfc:TF8)
                    ?tfc:TF8:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF8{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF8{prop:text} = Clip(maf:DateType)
                            ?tfc:TF8{prop:width} = 64
                            unhide(?popcalendar:20)

                        of 'STRING'
                            ?tfc:TF8{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF8{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF8{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF8{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF8{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF8{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    unhide(?tfc:TF9:prompt)
                    unhide(?tfc:TF9)
                    ?tfc:TF9:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF9{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF9{prop:text} = Clip(maf:DateType)
                            ?tfc:TF9{prop:width} = 64
                            unhide(?popcalendar:21)

                        of 'STRING'
                            ?tfc:TF9{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF9{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF9{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF9{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF9{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF9{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    unhide(?tfc:TF10:prompt)
                    unhide(?tfc:TF10)
                    ?tfc:TF10:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF10{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF10{prop:text} = Clip(maf:DateType)
                            ?tfc:TF10{prop:width} = 64
                            unhide(?popcalendar:22)

                        of 'STRING'
                            ?tfc:TF10{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF10{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF10{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF10{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF10{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF10{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    unhide(?tfc:TF11:prompt)
                    unhide(?tfc:TF11)
                    ?tfc:TF11:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF11{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF11{prop:text} = Clip(maf:DateType)
                            ?tfc:TF11{prop:width} = 64
                            unhide(?popcalendar:23)

                        of 'STRING'
                            ?tfc:TF11{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF11{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF11{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF11{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF11{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF11{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    unhide(?tfc:TF12:prompt)
                    unhide(?tfc:TF12)
                    ?tfc:TF12:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?tfc:TF12{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?tfc:TF12{prop:text} = Clip(maf:DateType)
                            ?tfc:TF12{prop:width} = 64
                            unhide(?popcalendar:24)

                        of 'STRING'
                            ?tfc:TF12{prop:width} = 124
                            if taf:restrictlength
                                ?tfc:TF12{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF12{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?tfc:TF12{prop:width} = 64
                            if taf:restrictlength
                                ?tfc:TF12{prop:text} = '@n_' & taf:lengthto
                            else!if taf:restrictlength
                                ?tfc:TF12{prop:text} = '@n_9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
        End
    end !loop
    access:trafault.restorefile(save_taf_id)
    setcursor()

    Display()



PutTradeNotes       Routine
    If f:Job
        access:jobnotes.clearkey(jbn:refnumberkey)
        jbn:refnumber = f:Job
        if access:jobnotes.tryfetch(jbn:refnumberkey)
            access:jobnotes.primerecord()
            access:jobnotes.tryinsert()
            access:jobnotes.clearkey(jbn:refnumberkey)
            jbn:refnumber   = f:Job
            access:jobnotes.tryfetch(jbn:refnumberkey)
        end!if access:jobnotes.tryfetch(jbn:refnumberkey)

        if taf:replicatefault = 'YES'
            if jbn:fault_description = ''
                jbn:fault_description = clip(tfo:description)
            else!if jbn:fault_description = ''
                jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(tfo:description)
            end!if jbn:fault_description = ''
        end!if mfo:replicatefault = 'YES'
        if taf:replicateinvoice = 'YES'
            if jbn:invoice_text = ''
                jbn:invoice_text = clip(tfo:description)
            else!if jbn:fault_description = ''
                jbn:invoice_text = clip(jbn:invoice_text) & '<13,10>' & clip(tfo:description)
            end!if jbn:fault_description = ''
        end!if mfo:replicatefault = 'YES'

        access:jobnotes.update()
    End !If f:Job

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020450'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GenericFaultCodes_WithTradeCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:JOBNOTES.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:JOBSE.UseFile
  Access:REPTYDEF.UseFile
  SELF.FilesOpened = True
  !Fill in the fields from the passed variables
!  jfc:JF1     = jfr:JF1
!  jfc:JF2     = jfr:JF2
!  jfc:JF3     = jfr:JF3
!  jfc:JF4     = jfr:JF4
!  jfc:JF5     = jfr:JF5
!  jfc:JF6     = jfr:JF6
!  jfc:JF7     = jfr:JF7
!  jfc:JF8     = jfr:JF8
!  jfc:JF9     = jfr:JF9
!  jfc:JF10    = jfr:JF10
!  jfc:JF11    = jfr:JF11
!  jfc:JF12    = jfr:JF12
!  tfc:TF1     = tfr:TF1
!  tfc:TF2     = tfr:TF2
!  tfc:TF3     = tfr:TF3
!  tfc:TF4     = tfr:TF4
!  tfc:TF5     = tfr:TF5
!  tfc:TF6     = tfr:TF6
!  tfc:TF7     = tfr:TF7
!  tfc:TF8     = tfr:TF8
!  tfc:TF9     = tfr:TF9
!  tfc:TF10    = tfr:TF10
!  tfc:TF11    = tfr:TF11
!  tfc:TF12    = tfr:TF12
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !View Only
  If glo:Preview = 'V'
      ?FieldsGroup{prop:Disable} = 1
      ?FieldsGroup2{prop:Disable} = 1
  End !glo:Select21 = 'RRCVIEWONLY'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  Globalrequest = Request
  Browse_Manufacturer_Fault_Lookup
  ReturnValue   = Globalresponse
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tralookup:1
      glo:Select1 = tra:Account_Number
      glo:Select2 = 1
    OF ?tralookup:4
      glo:Select1 = tra:Account_Number
      glo:Select2 = 4
    OF ?tralookup:5
      glo:Select1 = tra:Account_Number
      glo:Select2 = 5
    OF ?tralookup:2
      glo:Select1 = tra:Account_Number
      glo:Select2 = 2
    OF ?tralookup:3
      glo:Select1 = tra:Account_Number
      glo:Select2 = 3
    OF ?tralookup:7
      glo:Select1 = tra:Account_Number
      glo:Select2 = 7
    OF ?tralookup:8
      glo:Select1 = tra:Account_Number
      glo:Select2 = 8
    OF ?tralookup:9
      glo:Select1 = tra:Account_Number
      glo:Select2 = 9
    OF ?tralookup:11
      glo:Select1 = tra:Account_Number
      glo:Select2 = 11
    OF ?tralookup:6
      glo:Select1 = tra:Account_Number
      glo:Select2 = 6
    OF ?tralookup:10
      glo:Select1 = tra:Account_Number
      glo:Select2 = 10
    OF ?tralookup:12
      glo:Select1 = tra:Account_Number
      glo:Select2 = 12
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020450'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020450'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020450'&'0')
      ***
    OF ?jfc:JF1
      If jfc:JF1 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 1
          if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 1
              mfo:field        = jfc:JF1
              if access:manfaulo.tryfetch(mfo:field_key)
                  If maf:force_lookup = 'YES'
                      Post(event:accepted,?Lookup)
                  End!If maf:force_lookup = 'YES'
              Else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
                  If Maf:ReplicateFault = 'YES'
                      If jbn:fault_description = ''
                          jbn:fault_description = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_Text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
      
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
      End!If jfc:JF1 <> ''
      Do Fault_Coding
    OF ?Lookup
      ThisWindow.Update
      If ?lookup{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 1
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              jfc:JF1 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      If Maf:ReplicateFault = 'YES'
      
                          If jbn:fault_description = ''
                              jbn:fault_description = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          End!If jbn:fault_description = ''
      
                      End!If Mfo:ReplicateFault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
      
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                          End!If jbn:fault_description = ''
      
                      End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
              End !If j:Job
              display()
          Else
         !     jfc:JF1 = ''
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup{prop:hide} = false
      
      
      
      
      Display()
      Do Fault_Coding
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF1 = TINCALENDARStyle1(jfc:JF1)
          Display(?jfc:JF1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jfc:JF2
      if jfc:JF2 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 2
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 2
              mfo:field        = jfc:JF2
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:2)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF2 <> ''
      Do Fault_Coding
    OF ?Lookup:2
      ThisWindow.Update
      If ?lookup:2{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 2
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              jfc:JF2 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      
              End !If f:Job
              display()
          Else
           !! IF jfc:JF2 = ''
           !   jfc:JF2 = ''
           ! END
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:2{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF2 = TINCALENDARStyle1(jfc:JF2)
          Display(?jfc:JF2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jfc:JF3
      if jfc:JF3 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 3
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 3
              mfo:field        = jfc:JF3
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:3)
                  end!if maf:force_lookup = 'YES'
              else
                   access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF3 <> ''
      Do Fault_Coding
    OF ?Lookup:3
      ThisWindow.Update
      If ?lookup:3{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 3
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF3 = mfo:field
      
              If f:job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      
              End !If f:job
              display()
          Else
          !    jfc:JF3 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:3{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF3 = TINCALENDARStyle1(jfc:JF3)
          Display(?jfc:JF3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jfc:JF4
      if jfc:JF4 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 4
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 4
              mfo:field        = jfc:JF4
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:4)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF4 <> ''
      Do Fault_Coding
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF4 = TINCALENDARStyle1(jfc:JF4)
          Display(?jfc:JF4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      If ?lookup:4{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 4
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF4 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      
              End !If f:Job
              display()
          Else
         !     jfc:JF4 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:4{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF5
      if jfc:JF5 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 5
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 5
              mfo:field        = jfc:JF5
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:5)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF5 <> ''
      Do Fault_Coding
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF5 = TINCALENDARStyle1(jfc:JF5)
          Display(?jfc:JF5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      If ?lookup:5{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 5
          glo:select3  = ''
          If Self.Run(1,Selectrecord) = RequestCompleted
              jfc:JF5 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              End !If f:Job
      
              display()
          Else
             ! jfc:JF5 = ''
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:5{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF6
      if jfc:JF6 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 6
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 6
              mfo:field        = jfc:JF6
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:6)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF6 <> ''
      Do Fault_Coding
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF6 = TINCALENDARStyle1(jfc:JF6)
          Display(?jfc:JF6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      If ?lookup:6{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 6
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF6 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
         !     jfc:JF6 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:6{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF7
      if jfc:JF7 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 7
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 7
              mfo:field        = jfc:JF7
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:7)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF7 <> ''
      Do Fault_Coding
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF7 = TINCALENDARStyle1(jfc:JF7)
          Display(?jfc:JF7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      If ?lookup:7{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 7
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              jfc:JF7 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
          !    jfc:JF7 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:7{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF8
      if jfc:JF8 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 8
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 8
              mfo:field        = jfc:JF8
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:8)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF8 <> ''
      Do Fault_Coding
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF8 = TINCALENDARStyle1(jfc:JF8)
          Display(?jfc:JF8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      If ?lookup:8{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 8
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF8 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
          !    jfc:JF8  = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:8{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF9
      if jfc:JF9 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 9
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 9
              mfo:field        = jfc:JF9
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:9)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF9 <> ''
      Do Fault_Coding
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF9 = TINCALENDARStyle1(jfc:JF9)
          Display(?jfc:JF9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      If ?lookup:9{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 9
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF9 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
          !    jfc:JF9  = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:9{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF10
      if jfc:JF10 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 10
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 10
              mfo:field        = jfc:JF10
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:10)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF10 <> ''
      Do Fault_Coding
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF10 = TINCALENDARStyle1(jfc:JF10)
          Display(?jfc:JF10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      If ?lookup:10{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 10
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF10 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
         !     jfc:JF10 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:10{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF11
      if jfc:JF11 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 11
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 11
              mfo:field        = jfc:JF11
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:11)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF11 <> ''
      Do Fault_Coding
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF11 = TINCALENDARStyle1(jfc:JF11)
          Display(?jfc:JF11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      If ?lookup:11{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 11
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF11 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
          !    jfc:JF11 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:11{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?jfc:JF12
      if jfc:JF12 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 12
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 12
              mfo:field        = jfc:JF12
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:12)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF12 <> ''
      Do Fault_Coding
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc:JF12 = TINCALENDARStyle1(jfc:JF12)
          Display(?jfc:JF12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      If ?lookup:12{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 12
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jfc:JF12 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                              End !If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  If ~Instring(CLip(mfo:Description),jbn:Invoice_Text,1,1)
                                      jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                                  End !If ~Instring(CLip(mfo:Description),jbn:Fault_Description,1,1)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
          !    jfc:JF12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      Display()
      Do Fault_Coding
    OF ?tfc:TF1
      if tfc:TF1 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 1
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 1
              tfo:Field         = tfc:TF1
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 1      |
                  Or tfo:Field         <> tfc:TF1
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:1)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  !Message(taf:ReplicateFault,'Before Routine')
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF1 = TINCALENDARStyle1(tfc:TF1)
          Display(?tfc:TF1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:1
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF1 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 1
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 1
              tfo:Field         = tfc:TF1
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 1      |
                  Or tfo:Field         <> tfc:TF1
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:1)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF1 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF1)
      
    OF ?tfc:TF2
      if tfc:TF2 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 2
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 2
              tfo:Field         = tfc:TF2
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 2      |
                  Or tfo:Field         <> tfc:TF2
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:2)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF2 = TINCALENDARStyle1(tfc:TF2)
          Display(?tfc:TF2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc:TF3
      if tfc:TF3 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 3
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 3
              tfo:Field         = tfc:TF3
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 3      |
                  Or tfo:Field         <> tfc:TF3
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:3)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF3 = TINCALENDARStyle1(tfc:TF3)
          Display(?tfc:TF3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc:TF4
      if tfc:TF4 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 4
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 4
              tfo:Field         = tfc:TF4
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 4      |
                  Or tfo:Field         <> tfc:TF4
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:4)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:16
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF4 = TINCALENDARStyle1(tfc:TF4)
          Display(?tfc:TF4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:4
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF4 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 4
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 4
              tfo:Field         = tfc:TF4
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 4      |
                  Or tfo:Field         <> tfc:TF4
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:4)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF4 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF4)
      
    OF ?tfc:TF5
      if tfc:TF5 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 5
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 5
              tfo:Field         = tfc:TF5
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 5      |
                  Or tfo:Field         <> tfc:TF5
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:5)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:17
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF5 = TINCALENDARStyle1(tfc:TF5)
          Display(?tfc:TF5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:5
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF5 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 5
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 5
              tfo:Field         = tfc:TF5
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 5      |
                  Or tfo:Field         <> tfc:TF5
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:5)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF5 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF5)
      
    OF ?tfc:TF6
      if tfc:TF6 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 6
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 6
              tfo:Field         = tfc:TF6
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 6      |
                  Or tfo:Field         <> tfc:TF6
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:6)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:18
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF6 = TINCALENDARStyle1(tfc:TF6)
          Display(?tfc:TF6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF2 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 2
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 2
              tfo:Field         = tfc:TF2
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 2      |
                  Or tfo:Field         <> tfc:TF2
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:2)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF2 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF2)
      
    OF ?tralookup:3
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF3 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 3
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 3
              tfo:Field         = tfc:TF3
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 3      |
                  Or tfo:Field         <> tfc:TF3
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:3)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF3 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF3)
      
    OF ?tfc:TF7
      if tfc:TF7 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 7
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 7
              tfo:Field         = tfc:TF7
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 7      |
                  Or tfo:Field         <> tfc:TF7
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:7)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:19
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF7 = TINCALENDARStyle1(tfc:TF7)
          Display(?tfc:TF7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:7
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF7 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 7
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 7
              tfo:Field         = tfc:TF7
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 7      |
                  Or tfo:Field         <> tfc:TF7
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:7)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF7 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF7)
      
    OF ?tfc:TF8
      if tfc:TF8 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 8
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 8
              tfo:Field         = tfc:TF8
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 8      |
                  Or tfo:Field         <> tfc:TF8
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:8)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:20
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF8 = TINCALENDARStyle1(tfc:TF8)
          Display(?tfc:TF8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:8
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF8 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 8
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 8
              tfo:Field         = tfc:TF8
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 8      |
                  Or tfo:Field         <> tfc:TF8
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:8)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF8 = ''
      !        tfc:TF9 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF8)
      
    OF ?tfc:TF9
      if tfc:TF9 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 9
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 9
              tfo:Field         = tfc:TF9
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 9      |
                  Or tfo:Field         <> tfc:TF9
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:9)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:21
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF9 = TINCALENDARStyle1(tfc:TF9)
          Display(?tfc:TF9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc:TF10
      if tfc:TF10 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 10
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 10
              tfo:Field         = tfc:TF10
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 10      |
                  Or tfo:Field         <> tfc:TF10
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:10)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:22
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF10 = TINCALENDARStyle1(tfc:TF10)
          Display(?tfc:TF10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc:TF11
      if tfc:TF11 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 11
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 11
              tfo:Field         = tfc:TF11
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 11      |
                  Or tfo:Field         <> tfc:TF11
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:11)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:23
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF11 = TINCALENDARStyle1(tfc:TF11)
          Display(?tfc:TF11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc:TF12
      if tfc:TF12 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 12
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 12
              tfo:Field         = tfc:TF12
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 12      |
                  Or tfo:Field         <> tfc:TF12
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:12)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if jfc:JF1 <> ''
    OF ?PopCalendar:24
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc:TF12 = TINCALENDARStyle1(tfc:TF12)
          Display(?tfc:TF12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:9
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF9 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 9
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 9
              tfo:Field         = tfc:TF9
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 9      |
                  Or tfo:Field         <> tfc:TF9
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:9)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF9 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF9)
      
    OF ?tralookup:11
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF11 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 11
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 11
              tfo:Field         = tfc:TF11
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 11      |
                  Or tfo:Field         <> tfc:TF11
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:1)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF11 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF11)
      
    OF ?tralookup:6
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF6 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 6
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 6
              tfo:Field         = tfc:TF6
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 6      |
                  Or tfo:Field         <> tfc:TF6
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:6)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF6 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF6)
      
    OF ?tralookup:10
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF10 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 10
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 10
              tfo:Field         = tfc:TF10
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 10      |
                  Or tfo:Field         <> tfc:TF10
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:10)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF10 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF10)
      
    OF ?tralookup:12
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tfc:TF12 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 12
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 12
              tfo:Field         = tfc:TF12
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 12      |
                  Or tfo:Field         <> tfc:TF12
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:12)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              tfc:TF12 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tfc:TF12)
      
    OF ?OkButton
      ThisWindow.Update
      continue# = 1
      If f:comp <> ''
          continue# = 0
          If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF1:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF2{prop:hide} = 0 and jfc:JF2 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF2:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF3{prop:hide} = 0 and jfc:JF3 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF3:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?job:fault_ode1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF4{prop:hide} = 0 and jfc:JF4 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF4:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive


          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF5{prop:hide} = 0 and jfc:JF5 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF5:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive


          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF6{prop:hide} = 0 and jfc:JF6 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF6:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF7{prop:hide} = 0 and jfc:JF7 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF7:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?job:fault_ode1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF8{prop:hide} = 0 and jfc:JF8 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF8:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF9{prop:hide} = 0 and jfc:JF9 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF9:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF10{prop:hide} = 0 and jfc:JF10 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF10:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF11{prop:hide} = 0 and jfc:JF11 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF11:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?job:fault_ode1{prop:hide} = 0 and jfc:JF1 = ''
          If ?jfc:JF12{prop:hide} = 0 and jfc:JF12 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?jfc:JF12:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive

          End!If ?jfc:JF1{prop:hide} = 0 and jfc:JF1 = ''
          If error# = 0
              continue# = 1
          End!If error# = 0
      End!If job:date_completed <> ''
      If continue# = 1
          !Fill in the returning variables
!          jfr:JF1     = jfc:JF1
!          jfr:JF2     = jfc:JF2
!          jfr:JF3     = jfc:JF3
!          jfr:JF4     = jfc:JF4
!          jfr:JF5     = jfc:JF5
!          jfr:JF6     = jfc:JF6
!          jfr:JF7     = jfc:JF7
!          jfr:JF8     = jfc:JF8
!          jfr:JF9     = jfc:JF9
!          jfr:JF10    = jfc:JF10
!          jfr:JF11    = jfc:JF11
!          jfr:JF12    = jfc:JF12
!          tfr:TF1     = tfc:TF1
!          tfr:TF2     = tfc:TF2
!          tfr:TF3     = tfc:TF3
!          tfr:TF4     = tfc:TF4
!          tfr:TF5     = tfc:TF5
!          tfr:TF6     = tfc:TF6
!          tfr:TF7     = tfc:TF7
!          tfr:TF8     = tfc:TF8
!          tfr:TF9     = tfc:TF9
!          tfr:TF10    = tfc:TF10
!          tfr:TF11    = tfc:TF11
!          tfr:TF12    = tfc:TF12
          Post(event:closewindow)
      End

    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?jfc:JF1
    CASE EVENT()
    OF EVENT:AlertKey
      If ?lookup{prop:hide} = 0
          Post(Event:Accepted,?Lookup)
      End!If ?lookup{prop:hide} = 0
      
    END
  OF ?jfc:JF2
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:2{prop:hide} = 0
          Post(Event:Accepted,?Lookup:2)
      End
      
    END
  OF ?jfc:JF3
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:3{prop:hide} = 0
          Post(Event:Accepted,?Lookup:3)
      End
    END
  OF ?jfc:JF4
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:4{prop:hide} = 0
          Post(Event:Accepted,?Lookup:4)
      End
    END
  OF ?jfc:JF5
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:5{prop:hide} = 0
          Post(Event:Accepted,?Lookup:5)
      End
    END
  OF ?jfc:JF6
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:6{prop:hide} = 0
          Post(Event:Accepted,?Lookup:6)
      End
    END
  OF ?jfc:JF7
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:7{prop:hide} = 0
          Post(Event:Accepted,?Lookup:7)
      End
    END
  OF ?jfc:JF8
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:8{prop:hide} = 0
          Post(Event:Accepted,?Lookup:8)
      End
    END
  OF ?jfc:JF9
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:9{prop:hide} = 0
          Post(Event:Accepted,?Lookup:9)
      End
    END
  OF ?jfc:JF10
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:10{prop:hide} = 0
          Post(Event:Accepted,?Lookup:10)
      End
    END
  OF ?jfc:JF11
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:11{prop:hide} = 0
          Post(Event:Accepted,?Lookup:11)
      End
    END
  OF ?jfc:JF12
    CASE EVENT()
    OF EVENT:AlertKey
      If ?Lookup:12{prop:hide} = 0
          Post(Event:Accepted,?Lookup:12)
      End
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = f:Acc
      access:subtracc.tryfetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.tryfetch(tra:account_number_key)
      access:jobse.clearkey(jobe:refnumberkey)
      jobe:refnumber  = f:Job
      If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
          !Found
      Else! If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
          !Error
          If access:jobse.primerecord() = Level:Benign
              jobe:refnumber  = f:Job
              If access:jobse.tryinsert()
                  access:jobse.cancelautoinc()
              End!If access:jobse.tryinsert()
          End!If access:jobse.primerecord() = Level:Benign
      End! If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
      
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
      
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
      End !If AccessMANUFACT.Tryfetch(manManufacturer_Key) = LevelBenign
      
      
      Do Fault_Coding
          If ?tab1{prop:hide} = 1 And ?tab2{prop:hide} = 1
              ?tab3{prop:hide} = 0
          End!If ?tab1{prop:hide} = 0 Or ?tab2{prop:hide} = 0
          !ThisMakeOver.SetWindow(Win:FORM)
          Display()
      
      Select(?+1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
SingleIMEISearchWindow PROCEDURE                      !Generated from procedure template - ViewOnlyForm

tmp:IMEINumber       STRING(30)
tmp:JobNumber        LONG
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('To identify further jobs use the Global I.M.E.I. Search or use the Job Progress ' &|
   'Screen.'),AT(178,170),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('I.M.E.I. Number'),AT(231,216),USE(?tmp:IMEINumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(318,216,124,10),USE(tmp:IMEINumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR
                       BUTTON,AT(235,250),USE(?Button:CustomerService),TRN,FLAT,ICON('custserp.jpg')
                       BUTTON,AT(371,250),USE(?Button:EngineeringDetails),TRN,FLAT,ICON('engdetp.jpg'),DEFAULT
                       PROMPT('This search will return the most recent job with the Job I.M.E.I. Number entered' &|
   '.'),AT(187,156),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Single IMEI Search'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020668'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SingleIMEISearchWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:CustomerService
      ThisWindow.Update
      tmp:JobNumber = 0
      Access:JOBS.Clearkey(job:ESN_Key)
      job:ESN = tmp:IMEINumber
      Set(job:ESN_Key,job:ESN_Key)
      Loop ! Begin Loop
          If Access:JOBS.Next()
              Break
          End ! If Access:JOBS.Next()
          If job:ESN <> tmp:IMEINumber
              Break
          End ! If job:ESN <> tmp:IMEINumber
          If job:Ref_Number > tmp:JobNumber
              tmp:JobNumber = job:Ref_Number
          End ! If job:Ref_Number > tmp:JobNumber
      End ! Loop
      
      If tmp:JobNumber > 0
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = tmp:JobNumber
          If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
              !Found
              ! Inserting (DBH 06/12/2006) # 8559 - Do not allow access to the customer service screen if the user has not booked the job
              If glo:WebJob
                  Access:WEBJOB.ClearKey(wob:RefNumberKey)
                  wob:RefNumber = job:Ref_Number
                  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                      !Found
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = wob:HeadAccountNumber
                      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          Access:USERS.ClearKey(use:Password_Key)
                          use:Password = glo:Password
                          If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                              !Found
                              If tra:SiteLocation <> use:Location
                                  Case Missive('Error! You cannot view the Customer Service screen of a job booked by another RRC.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  tmp:IMEINumber = ''
                                  ?tmp:IMEINumber{prop:Touched} = 1
                                  Select(?tmp:IMEINumber)
                                  Cycle
                              End ! If wob:HeadACcountNumber <> use:Location
                          Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                              !Error
                          End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
              End ! If glo:WebJob
              ! End (DBH 06/12/2006) #8559
      
              GlobalRequest = ChangeRecord
              UpdateJobs
              Select(?tmp:IMEINumber)
          Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      Else ! If tmp:JobNumber > 0
           Case Missive('Cannot find a job that matches the selected I.M.E.I. Number.','ServiceBase 3g',|
                          'mstop.jpg','/OK')
               Of 1 ! OK Button
           End ! Case Missive
           tmp:IMEINumber = ''
           ?tmp:IMEINumber{prop:Touched} = 1
           Select(?tmp:IMEINumber)
      
      End ! If tmp:JobNumber > 0
    OF ?Button:EngineeringDetails
      ThisWindow.Update
      tmp:JobNumber = 0
      Access:JOBS.Clearkey(job:ESN_Key)
      job:ESN = tmp:IMEINumber
      Set(job:ESN_Key,job:ESN_Key)
      Loop ! Begin Loop
          If Access:JOBS.Next()
              Break
          End ! If Access:JOBS.Next()
          If job:ESN <> tmp:IMEINumber
              Break
          End ! If job:ESN <> tmp:IMEINumber
          If job:Ref_Number > tmp:JobNumber
              tmp:JobNumber = job:Ref_Number
          End ! If job:Ref_Number > tmp:JobNumber
      End ! Loop
      
      If tmp:JobNumber > 0
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = tmp:JobNumber
          If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
              !Found
              GlobalRequest = ChangeRecord
              Update_Jobs_Rapid
              Select(?tmp:IMEINumber)
          Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      Else ! If tmp:jobNumber > 09
          Case Missive('Cannot find a job that matches the selected I.M.E.I. Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          tmp:IMEINumber = ''
          ?tmp:IMEINumber{prop:Touched} = 1
          Select(?tmp:IMEINumber)
      End ! If tmp:JobNumber > 0
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020668'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020668'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020668'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
