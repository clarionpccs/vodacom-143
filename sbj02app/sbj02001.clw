

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02001.INC'),ONCE        !Local module procedure declarations
                     END


UpdateContactHistoryText PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::noc:Record  LIKE(noc:RECORD),STATIC
QuickWindow          WINDOW('Update the NOTESINV File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Note'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Reference'),AT(248,196),USE(?NOE:Reference:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(296,196,136,10),USE(noc:Reference),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Notes'),AT(248,212),USE(?NOI:Notes:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s80),AT(296,212,136,10),USE(noc:Notes),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Engineers Text Note'
  OF ChangeRecord
    ActionMessage = 'Chaning An Engineers Text Note'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020443'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateContactHistoryText')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(noc:Record,History::noc:Record)
  SELF.AddHistoryField(?noc:Reference,2)
  SELF.AddHistoryField(?noc:Notes,3)
  SELF.AddUpdateFile(Access:NOTESCON)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:NOTESCON.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:NOTESCON
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:NOTESCON.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020443'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020443'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020443'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseContactHistoryNotes PROCEDURE                   !Generated from procedure template - Window

CurrentTab           STRING(80)
select_temp          STRING('''N''')
FilesOpened          BYTE
manufacturer_temp    STRING(30)
notes_tag            STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW8::View:Browse    VIEW(NOTESCON)
                       PROJECT(noc:Reference)
                       PROJECT(noc:Notes)
                       PROJECT(noc:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
noc:Reference          LIKE(noc:Reference)            !List box control field - type derived from field
noc:Notes              LIKE(noc:Notes)                !List box control field - type derived from field
noc:RecordNumber       LIKE(noc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Standard Contact History Notes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Standard Contact History Notes File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,276,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Notes'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,86,268,242),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('72L(2)|M~Reference~@s30@320L(2)|M~Notes~@s80@'),FROM(Queue:Browse),DRAGID('FromList')
                           ENTRY(@s30),AT(68,70,72,10),USE(noc:Reference),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(68,332),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(136,332),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(204,332),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(272,332),USE(?add),TRN,FLAT,LEFT,ICON('addtextp.jpg')
                         END
                       END
                       SHEET,AT(344,54,272,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Text To Appear On Contact History'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(348,86,184,160),USE(?List2),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('320L(2)~Notes~@s80@'),FROM(glo:Q_Notes),DROPID('FromList')
                           BUTTON,AT(348,250),USE(?remove_text),TRN,FLAT,LEFT,ICON('remtextp.jpg')
                         END
                       END
                       BUTTON,AT(548,136),USE(?Select_Button),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020442'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseContactHistoryNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  Relate:NOTESCON.Open
  Relate:NOTESENG.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:NOTESCON,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Clear(glo:Q_Notes)
  Free(glo:Q_Notes)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,noc:Notes_Key)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?noc:Reference,noc:Reference,1,BRW8)
  BRW8.AddField(noc:Reference,BRW8.Q.noc:Reference)
  BRW8.AddField(noc:Notes,BRW8.Q.noc:Notes)
  BRW8.AddField(noc:RecordNumber,BRW8.Q.noc:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
    Relate:NOTESCON.Close
    Relate:NOTESENG.Close
  If select_Temp <> 'Y'
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
  End
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('ENGINEERS NOTES - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('ENGINEERS NOTES - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of deleterecord
            check_access('ENGINEERS NOTES - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateContactHistoryText
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020442'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020442'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020442'&'0')
      ***
    OF ?add
      ThisWindow.Update
      glo:notes_pointer = noc:notes
      Add(glo:Q_Notes)
      Select(?List)
    OF ?remove_text
      ThisWindow.Update
      glo:notes_pointer = noc:notes
      Get(glo:Q_Notes,glo:notes_pointer)
      Delete(glo:Q_Notes)
      Select(?List)
    OF ?Select_Button
      ThisWindow.Update
      select_temp = 'Y'
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:Drag
      get(queue:browse,noc:notes)
      if dragid()
          setdropid(brw8.q.noc:notes)
      end!if dragid()
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List2
    CASE EVENT()
    OF EVENT:Drop
      if dropid()
          glo:notes_pointer = noc:notes
          Add(glo:Q_Notes)
          Select(?List)
      end!if dropid()
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?noc:Reference
      Select(?List)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW8.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_Contact_History PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::cht:Record  LIKE(cht:RECORD),STATIC
QuickWindow          WINDOW('Update the CONTHIST File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Contact History Entry'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:2)
                           PROMPT('Ref Number'),AT(168,126),USE(?CHT:Ref_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s9),AT(244,126,64,10),USE(cht:Ref_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Ref Number')
                           TEXT,AT(244,176,134,10),USE(cht:Action,,?cht:Action:2),SKIP,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Date'),AT(168,142),USE(?CHT:Date:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(244,142,64,10),USE(cht:Date),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(312,142,64,10),USE(cht:Time),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('User'),AT(168,158),USE(?CHT:User:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(244,158,40,10),USE(cht:User),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON,AT(396,171),USE(?FldLookup),SKIP,FLAT,ICON('lookupp.jpg')
                           PROMPT('Action'),AT(168,174),USE(?cht:action:prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Notes'),AT(168,194),USE(?CHT:Notes:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(244,190,268,80),USE(cht:Notes),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Sticky Note'),AT(244,278),USE(cht:SN_StickyNote),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                           GROUP,AT(194,276,316,51),USE(?GroupSticky),TRN,HIDE
                             CHECK('Completed'),AT(350,278),USE(cht:SN_Completed),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                             CHECK('Engineer Allocation'),AT(244,291),USE(cht:SN_EngAlloc),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                             CHECK('Customer Service'),AT(244,304),USE(cht:SN_CustService),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                             CHECK('Waybill Confirmation'),AT(350,304),USE(cht:SN_WaybillConf),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                             CHECK('Despatch to 3rd Party'),AT(244,317),USE(cht:SN_Despatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                             CHECK('Engineer Update'),AT(350,291),USE(cht:SN_EngUpdate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                           END
                           BUTTON,AT(212,190),USE(?LookupNotes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Contact'
  OF ChangeRecord
    ActionMessage = 'Changing A Contact'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020434'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Contact_History')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(cht:Record,History::cht:Record)
  SELF.AddHistoryField(?cht:Ref_Number,2)
  SELF.AddHistoryField(?cht:Action:2,6)
  SELF.AddHistoryField(?cht:Date,3)
  SELF.AddHistoryField(?cht:Time,4)
  SELF.AddHistoryField(?cht:User,5)
  SELF.AddHistoryField(?cht:Notes,7)
  SELF.AddHistoryField(?cht:SN_StickyNote,9)
  SELF.AddHistoryField(?cht:SN_Completed,10)
  SELF.AddHistoryField(?cht:SN_EngAlloc,11)
  SELF.AddHistoryField(?cht:SN_CustService,13)
  SELF.AddHistoryField(?cht:SN_WaybillConf,14)
  SELF.AddHistoryField(?cht:SN_Despatch,15)
  SELF.AddHistoryField(?cht:SN_EngUpdate,12)
  SELF.AddUpdateFile(Access:CONTHIST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACTION.Open
  Relate:CONTHIST.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CONTHIST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If thiswindow.request = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      cht:user = use:user_code
         
      if GLO:Select1 = 'STICKY' then
          cht:Action = 'STICKY NOTE'
          cht:SN_StickyNote = 'Y'
          GLO:Select1 = ''
          hide(?FldLookup)
      END
  End!If thiswindow.request = Insertrecord
  
  If cht:SystemHistory
      ?CurrentTab{prop:Disable} = 1
  End !cht:SystemHistory
  
  If SecurityCheck('STICKY NOTES') = level:benign then
      Unhide(?cht:SN_StickyNote)
      if cht:SN_StickyNote = 'Y' then
          unhide(?GroupSticky)
          hide(?FldLookup)
      ELSE
          hide(?GroupSticky)
          UNhide(?FldLookup)
      END
  ELSE
      hide(?cht:SN_StickyNote)
      hide(?GroupSticky)
      if cht:SN_StickyNote = 'Y' then
          ?CurrentTab{prop:Disable} = 1
      END
  End
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?cht:Action:2{Prop:Tip} AND ~?FldLookup{Prop:Tip}
     ?FldLookup{Prop:Tip} = 'Select ' & ?cht:Action:2{Prop:Tip}
  END
  IF ?cht:Action:2{Prop:Msg} AND ~?FldLookup{Prop:Msg}
     ?FldLookup{Prop:Msg} = 'Select ' & ?cht:Action:2{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACTION.Close
    Relate:CONTHIST.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    cht:Date = Today()
    cht:Time = Clock()
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?cht:SN_StickyNote
      if cht:SN_StickyNote = 'Y' then
          unhide(?GroupSticky)
          cht:Action = 'STICKY NOTE'
          hide(?FldLookup)
      ELSE
          hide(?GroupSticky)
          unhide(?FldLookup)
          if cht:Action = 'STICKY NOTE' THEN
              cht:Action = ''
          end
      END
      
      display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020434'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020434'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020434'&'0')
      ***
    OF ?FldLookup
      ThisWindow.Update
      ! Lookup button pressed. Call select procedure if required
      act:Action = cht:Action                         ! Move value for lookup
      IF Access:ACTION.TryFetch(act:Action_Key)       ! IF record not found
         act:Action = cht:Action                      ! Move value for lookup
         ! Based on ?cht:Action:2
         SET(act:Action_Key,act:Action_Key)           ! Prime order for browse
         Access:ACTION.TryNext()
      END
      GlobalRequest = SelectRecord
      Browse_Action                                   ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?FldLookup)                           ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?cht:Action:2,act:Action)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?FldLookup+1)                            ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
    OF ?LookupNotes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseContactHistoryNotes
      ThisWindow.Reset
      If Records(glo:Q_Notes)
          Loop x# = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x#)
              If cht:Notes = ''
                  cht:Notes = glo:Notes_Pointer
              Else!If con:Notes = ''
                  cht:Notes = Clip(cht:Notes) & '<13,10>' & glo:Notes_Pointer
              End!If con:Notes = ''
          End!Loop x# = 1 To Records(glo:Q_Notes).
      End!If Records(glo:Q_Notes)
      Display(?cht:Notes)
    OF ?OK
      ThisWindow.Update
      IF CLIP(cht:Action) = '' then cycle.
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Contact_History PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:User             STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(CONTHIST)
                       PROJECT(cht:Date)
                       PROJECT(cht:Time)
                       PROJECT(cht:Action)
                       PROJECT(cht:User)
                       PROJECT(cht:SN_StickyNote)
                       PROJECT(cht:Notes)
                       PROJECT(cht:Record_Number)
                       PROJECT(cht:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
cht:Date               LIKE(cht:Date)                 !List box control field - type derived from field
cht:Date_NormalFG      LONG                           !Normal forground color
cht:Date_NormalBG      LONG                           !Normal background color
cht:Date_SelectedFG    LONG                           !Selected forground color
cht:Date_SelectedBG    LONG                           !Selected background color
cht:Time               LIKE(cht:Time)                 !List box control field - type derived from field
cht:Time_NormalFG      LONG                           !Normal forground color
cht:Time_NormalBG      LONG                           !Normal background color
cht:Time_SelectedFG    LONG                           !Selected forground color
cht:Time_SelectedBG    LONG                           !Selected background color
tmp:User               LIKE(tmp:User)                 !List box control field - type derived from local data
tmp:User_NormalFG      LONG                           !Normal forground color
tmp:User_NormalBG      LONG                           !Normal background color
tmp:User_SelectedFG    LONG                           !Selected forground color
tmp:User_SelectedBG    LONG                           !Selected background color
cht:Action             LIKE(cht:Action)               !List box control field - type derived from field
cht:Action_NormalFG    LONG                           !Normal forground color
cht:Action_NormalBG    LONG                           !Normal background color
cht:Action_SelectedFG  LONG                           !Selected forground color
cht:Action_SelectedBG  LONG                           !Selected background color
cht:User               LIKE(cht:User)                 !List box control field - type derived from field
cht:User_NormalFG      LONG                           !Normal forground color
cht:User_NormalBG      LONG                           !Normal background color
cht:User_SelectedFG    LONG                           !Selected forground color
cht:User_SelectedBG    LONG                           !Selected background color
cht:SN_StickyNote      LIKE(cht:SN_StickyNote)        !List box control field - type derived from field
cht:Notes              LIKE(cht:Notes)                !Browse hot field - type derived from field
cht:Record_Number      LIKE(cht:Record_Number)        !Primary key field - type derived from field
cht:Ref_Number         LIKE(cht:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Contact History File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Contact History File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,84,344,178),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('50R(2)|M*~Date~L@d6b@26R(2)|M*~Time~@t1@130L(2)|M*~User~@s50@155L(2)|M*~Action~@' &|
   's80@37L(2)|M*~Usercode~@s3@0L(2)|M@s1@'),FROM(Queue:Browse:1)
                       PROMPT('Notes'),AT(168,266),USE(?AUD:Notes:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(548,134),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       TEXT,AT(168,278,344,80),USE(cht:Notes),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(548,162),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(548,192),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(546,102),USE(?ButtonSticky),FLAT,ICON('StickyNp.jpg')
                         END
                         TAB('By Action'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s80),AT(168,70,124,10),USE(cht:Action),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         END
                         TAB('By User'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(168,72,64,10),USE(cht:User),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         END
                       END
                       BUTTON,AT(552,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(544,230),USE(?Button:ResendSMS),TRN,FLAT,HIDE,ICON('resmsp.jpg')
                       BUTTON,AT(544,260),USE(?Button:ResendEmail),TRN,FLAT,HIDE,ICON('reemailp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020433'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Contact_History')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CONTHIST.Open
  Access:JOBSE2.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:CONTHIST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
      If Sub(job:Current_Status,1,3) > '704' And Sub(job:Current_Status,1,3) < '901'
          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
          jobe2:RefNumber = job:Ref_Number
          If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
              ! Found
              If jobe2:SMSNotification
                  ?Button:ResendSMS{prop:Hide} = False
              End ! If jobe2:SMSNotification
              If jobe2:EmailNotification
                  ?Button:ResendEmail{prop:Hide} = False
              End ! If jobe2:EmailNotification
          Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
              ! Error
          End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      End ! If Sub(job:Current_Status,1,3) > '704' And Sub(job:Current_Status,1,3) < '901'
  End ! If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
  
  If SecurityCheck('STICKY NOTES') = level:benign then
      Unhide(?ButtonSticky)
  ELSE
      hide(?ButtonSticky)
  End ! If SecurityCheck('AMEND RRC PART')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,cht:Action_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?cht:Action,cht:Action,1,BRW1)
  BRW1.AddSortOrder(,cht:User_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?cht:User,cht:User,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,cht:Ref_Number_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,cht:Date,1,BRW1)
  BIND('tmp:User',tmp:User)
  BRW1.AddField(cht:Date,BRW1.Q.cht:Date)
  BRW1.AddField(cht:Time,BRW1.Q.cht:Time)
  BRW1.AddField(tmp:User,BRW1.Q.tmp:User)
  BRW1.AddField(cht:Action,BRW1.Q.cht:Action)
  BRW1.AddField(cht:User,BRW1.Q.cht:User)
  BRW1.AddField(cht:SN_StickyNote,BRW1.Q.cht:SN_StickyNote)
  BRW1.AddField(cht:Notes,BRW1.Q.cht:Notes)
  BRW1.AddField(cht:Record_Number,BRW1.Q.cht:Record_Number)
  BRW1.AddField(cht:Ref_Number,BRW1.Q.cht:Ref_Number)
  QuickWindow{PROP:MinWidth}=529
  QuickWindow{PROP:MinHeight}=272
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Date'
    ?Tab2{PROP:TEXT} = 'By Action'
    ?Tab3{PROP:TEXT} = 'By User'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#155L(2)|M*~Action~@s80@#16#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTHIST.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('CONTACT HISTORY - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('CONTACT HISTORY - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of deleterecord
            check_access('CONTACT HISTORY - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Contact_History
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonSticky
      GLO:Select1 = 'STICKY'
      Post(Event:Accepted,?Insert)
      
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020433'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020433'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020433'&'0')
      ***
    OF ?Button:ResendSMS
      ThisWindow.Update
      Case Missive('Are you sure you want to resend an SMS?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              SendSMSText('J','N','N')
              !Debug erroring turned off for distruibution
!              if glo:ErrorText[1:5] = 'ERROR' then
!      
!                  miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!      
!              ELSE
!                  !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                  miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!              END
              glo:ErrorText = ''
      !
      !        !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'COMP','SMS',jobe2:SMSAlertNumber,'',0,'')
      !        Case Missive('An SMS has been scheduled to be sent.','ServiceBase 3g',|
      !                       'midea.jpg','/OK')
      !            Of 1 ! OK Button
      !        End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
    OF ?Button:ResendEmail
      ThisWindow.Update
      Case Missive('Are you sure you want to resend an Email?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'COMP','EMAIL','',jobe2:EmailAlertAddress,0,'')
              Case Missive('An Email has been scheduled to be sent.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#155L(2)|M*~Action~@s80@#16#'
          ?Tab:2{PROP:TEXT} = 'By Date'
        OF 2
          ?Browse:1{PROP:FORMAT} ='155L(2)|M*~Action~@s80@#16#50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#'
          ?Tab2{PROP:TEXT} = 'By Action'
        OF 3
          ?Browse:1{PROP:FORMAT} ='37L(2)|M*~Usercode~@s3@#21#50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#155L(2)|M*~Action~@s80@#16#'
          ?Tab3{PROP:TEXT} = 'By User'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?cht:Action
      Select(?Browse:1)
    OF ?cht:User
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Inserting (DBH 18/05/2006) #7597 - Display system or full user name
  If cht:User = '&SB'
      tmp:User = '* SYSTEM *'
  Else ! If cht:User = '&SB'
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = cht:User
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          ! Found
          tmp:User = CLip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          ! Error
          tmp:USer = '* ERROR - USER CODE: ' * Clip(cht:User) & ' *'
      End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  End ! If cht:User = '&SB'
  ! End (DBH 18/05/2006) #7597
  PARENT.SetQueueRecord
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:Date_NormalFG = 255
    SELF.Q.cht:Date_NormalBG = 16777215
    SELF.Q.cht:Date_SelectedFG = 16777215
    SELF.Q.cht:Date_SelectedBG = 255
  ELSE
    SELF.Q.cht:Date_NormalFG = -1
    SELF.Q.cht:Date_NormalBG = -1
    SELF.Q.cht:Date_SelectedFG = -1
    SELF.Q.cht:Date_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:Time_NormalFG = 255
    SELF.Q.cht:Time_NormalBG = 16777215
    SELF.Q.cht:Time_SelectedFG = 16777215
    SELF.Q.cht:Time_SelectedBG = 255
  ELSE
    SELF.Q.cht:Time_NormalFG = -1
    SELF.Q.cht:Time_NormalBG = -1
    SELF.Q.cht:Time_SelectedFG = -1
    SELF.Q.cht:Time_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.tmp:User_NormalFG = 255
    SELF.Q.tmp:User_NormalBG = 16777215
    SELF.Q.tmp:User_SelectedFG = 16777215
    SELF.Q.tmp:User_SelectedBG = 255
  ELSE
    SELF.Q.tmp:User_NormalFG = -1
    SELF.Q.tmp:User_NormalBG = -1
    SELF.Q.tmp:User_SelectedFG = -1
    SELF.Q.tmp:User_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:Action_NormalFG = 255
    SELF.Q.cht:Action_NormalBG = 16777215
    SELF.Q.cht:Action_SelectedFG = 16777215
    SELF.Q.cht:Action_SelectedBG = 255
  ELSE
    SELF.Q.cht:Action_NormalFG = -1
    SELF.Q.cht:Action_NormalBG = -1
    SELF.Q.cht:Action_SelectedFG = -1
    SELF.Q.cht:Action_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:User_NormalFG = 255
    SELF.Q.cht:User_NormalBG = 16777215
    SELF.Q.cht:User_SelectedFG = 16777215
    SELF.Q.cht:User_SelectedBG = 255
  ELSE
    SELF.Q.cht:User_NormalFG = -1
    SELF.Q.cht:User_NormalBG = -1
    SELF.Q.cht:User_SelectedFG = -1
    SELF.Q.cht:User_SelectedBG = -1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?AUD:Notes:Prompt, Resize:FixRight+Resize:FixTop, Resize:LockSize)

BrowseEngineersOnJob PROCEDURE (func:JobNumber)       !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:EngineersName    STRING(60)
tmp:JobNumber        LONG
Hide_Skill           BYTE
tmp:Skill_Level      STRING(30)
BRW1::View:Browse    VIEW(JOBSENG)
                       PROJECT(joe:DateAllocated)
                       PROJECT(joe:AllocatedBy)
                       PROJECT(joe:EngSkillLevel)
                       PROJECT(joe:Status)
                       PROJECT(joe:StatusDate)
                       PROJECT(joe:StatusTime)
                       PROJECT(joe:RecordNumber)
                       PROJECT(joe:JobNumber)
                       PROJECT(joe:TimeAllocated)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tmp:EngineersName      LIKE(tmp:EngineersName)        !List box control field - type derived from local data
joe:DateAllocated      LIKE(joe:DateAllocated)        !List box control field - type derived from field
joe:AllocatedBy        LIKE(joe:AllocatedBy)          !List box control field - type derived from field
joe:EngSkillLevel      LIKE(joe:EngSkillLevel)        !List box control field - type derived from field
tmp:Skill_Level        LIKE(tmp:Skill_Level)          !List box control field - type derived from local data
joe:Status             LIKE(joe:Status)               !List box control field - type derived from field
joe:StatusDate         LIKE(joe:StatusDate)           !List box control field - type derived from field
joe:StatusTime         LIKE(joe:StatusTime)           !List box control field - type derived from field
joe:RecordNumber       LIKE(joe:RecordNumber)         !Primary key field - type derived from field
joe:JobNumber          LIKE(joe:JobNumber)            !Browse key field - type derived from field
joe:TimeAllocated      LIKE(joe:TimeAllocated)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Engineers On Job History'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Engineers On Job History File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,70,544,290),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('164L(2)|M~Engineers Name~@s60@51R(2)|M~Date Allocated~@d6@46L(2)|M~Allocated By~' &|
   '@s3@51D(4)|M~Eng Skill Level~C(0)@s8@50L(2)|M~Job Skill Level~@s30@120L(2)|M~Sta' &|
   'tus~@s30@46L(2)|M~Status Date~@d6@20L(2)|M~Status Time~@t1b@'),FROM(Queue:Browse:1)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020591'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseEngineersOnJob')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBSENG.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  
  If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      Hide_Skill = 1
  Else !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      Hide_Skill = 0
  End !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBSENG,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,joe:JobNumberKey)
  BRW1.AddRange(joe:JobNumber,tmp:JobNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,joe:DateAllocated,1,BRW1)
  BIND('tmp:EngineersName',tmp:EngineersName)
  BIND('tmp:Skill_Level',tmp:Skill_Level)
  BRW1.AddField(tmp:EngineersName,BRW1.Q.tmp:EngineersName)
  BRW1.AddField(joe:DateAllocated,BRW1.Q.joe:DateAllocated)
  BRW1.AddField(joe:AllocatedBy,BRW1.Q.joe:AllocatedBy)
  BRW1.AddField(joe:EngSkillLevel,BRW1.Q.joe:EngSkillLevel)
  BRW1.AddField(tmp:Skill_Level,BRW1.Q.tmp:Skill_Level)
  BRW1.AddField(joe:Status,BRW1.Q.joe:Status)
  BRW1.AddField(joe:StatusDate,BRW1.Q.joe:StatusDate)
  BRW1.AddField(joe:StatusTime,BRW1.Q.joe:StatusTime)
  BRW1.AddField(joe:RecordNumber,BRW1.Q.joe:RecordNumber)
  BRW1.AddField(joe:JobNumber,BRW1.Q.joe:JobNumber)
  BRW1.AddField(joe:TimeAllocated,BRW1.Q.joe:TimeAllocated)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSENG.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020591'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020591'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020591'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = joe:UserCode
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:EngineersName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
  Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  IF Hide_Skill = 1
     tmp:Skill_Level = 'N/A'
  ELSE
     tmp:Skill_Level = joe:JobSkillLevel
  END
  PARENT.SetQueueRecord


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Engineers_Notes PROCEDURE                             !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Engineers Notes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Engineers Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Engineers Notes'),AT(240,146),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(208,158),USE(?Lookup_Engineers_Notes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           TEXT,AT(240,158,200,90),USE(jbn:Engineers_Notes),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Changing Engineers Notes'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020447'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Engineers_Notes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020447'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020447'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020447'&'0')
      ***
    OF ?Lookup_Engineers_Notes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Engineers_Notes
      ThisWindow.Reset
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If jbn:engineers_notes = ''
                  jbn:engineers_notes = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  jbn:engineers_notes = Clip(jbn:engineers_notes) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
    OF ?jbn:Engineers_Notes
      If Len(Clip(jbn:Engineers_Notes)) > 240
          jbn:Engineers_Notes = Sub(jbn:Engineers_Notes,1,240)
          Display()
      End !Len(Clip(jbn:Engineers_Notes)) > 240
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Parts PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
save_loc_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
save_ori_id          USHORT,AUTO
fault_codes_required_temp STRING('NO {1}')
pos                  STRING(255)
Part_Details_Group   GROUP,PRE(TMP)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
                     END
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
tmp:location         STRING(30)
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
sav:ExcludeFromOrder STRING(3)
sav:Supplier         STRING(30)
tmp:CreateNewOrder   BYTE(0)
tmp:StockNumber      LONG
tmp:PendingPart      LONG
tmp:NewQuantity      LONG
tmp:CurrentState     LONG
tmp:WebOrderNumber   LONG
UnHide_Tab           BYTE(0)
tmp:PurchaseCost     REAL
tmp:InWarrantyCost   REAL
tmp:OutWarrantyCost  REAL
tmp:InWarrantyMarkup LONG
tmp:OutWarrantyMarkup LONG
tmp:MaxMarkup        LONG
tmp:Unallocate       BYTE(0)
tmp:FromEstimate     BYTE(0)
tmp:ARCPart          BYTE(0)
FieldNumbersQueue    QUEUE,PRE(field)
ScreenOrder          LONG
FieldNumber          LONG
                     END
PromptNumbersQueue   QUEUE,PRE(prompt)
ScreenOrder          LONG
FieldNumber          LONG
                     END
LookupNumbersQueue   QUEUE,PRE(lookup)
ScreenOrder          LONG
FieldNumber          LONG
                     END
DescriptionNumbersQueue QUEUE,PRE(description)
ScreenOrder          LONG
FieldNumber          LONG
                     END
tmp:FaultCode        STRING(30),DIM(12)
FaultCodeQueue       QUEUE,PRE(fault)
FaultNumber          LONG
FaultCode            STRING(30)
                     END
History::par:Record  LIKE(par:RECORD),STATIC
QuickWindow          WINDOW('Chargeable Part'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Insert / Amend Chargeable Part'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(185,42,1,1),USE(?PanelMain),HIDE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,284,354),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Chargeable Part'),AT(8,39),USE(?Title),FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(8,52),USE(?par:Part_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,52,124,10),USE(par:Part_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(228,49),USE(?browse_stock_button),SKIP,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Description'),AT(8,68),USE(?par:Description:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,68,124,10),USE(par:Description),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Location'),AT(8,84),USE(?tmp:location:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,84,124,10),USE(tmp:location),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Shelf/Second Location'),AT(8,97),USE(?tmp:location:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,97,60,10),USE(tmp:ShelfLocation),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@s30),AT(164,97,60,10),USE(tmp:SecondLocation),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Purchase Cost'),AT(8,116),USE(?tmp:PurchaseCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(100,116,60,10),USE(tmp:PurchaseCost),SKIP,DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Purchase Cost'),TIP('Purchase Cost'),UPR
                           PROMPT('% Markup'),AT(164,116),USE(?Prompt32),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Out Warranty Cost'),AT(8,129),USE(?tmp:OutWarrantyCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(100,129,60,10),USE(tmp:OutWarrantyCost),SKIP,DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Out Warranty Cost'),TIP('Out Warranty Cost'),UPR
                           SPIN(@s8),AT(164,129,24,10),USE(tmp:OutWarrantyMarkup),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),RANGE(0,99),STEP(5)
                           STRING('FIXED PRICE'),AT(192,129),USE(?String1),TRN,HIDE,FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Supplier'),AT(8,148),USE(?par:Supplier:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,148,124,10),USE(par:Supplier),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(228,145),USE(?LookupSupplier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Quantity'),AT(8,164),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@p<<<<<<<#p),AT(100,164,64,10),USE(par:Quantity),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,RANGE(1,99999999),STEP(1)
                           PROMPT('Despatch Note No'),AT(8,180),USE(?par:Despatch_Note_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,180,124,10),USE(par:Despatch_Note_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           CHECK('Exclude From Ordering / Decrementing'),AT(100,193),USE(par:Exclude_From_Order),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Part Status'),AT(8,212),USE(?Prompt24),FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Date Ordered'),AT(8,239),USE(?par:Date_Ordered:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(100,239,64,10),USE(par:Date_Ordered),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Order Number'),AT(8,225),USE(?par:Order_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n012b),AT(100,225,64,10),USE(par:Order_Number),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),READONLY,MSG('Order Number')
                           PROMPT('Date Received'),AT(8,252),USE(?PAR:Date_Received:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(100,252,64,10),USE(par:Date_Received),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON,AT(220,244),USE(?UnallocatePart),TRN,FLAT,HIDE,LEFT,ICON('unalparp.jpg')
                           PROMPT('Out Fault Code'),AT(8,273),USE(?OutFaultCode:Prompt),HIDE,FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Part Allocated'),AT(100,204),USE(par:PartAllocated),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Part Allocated'),TIP('Part Allocated'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(292,28,384,354),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Fault Coding'),USE(?FaultCodeTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Fault Codes Checked'),AT(408,33),USE(par:Fault_Codes_Checked),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Fault Code'),AT(296,52,110,20),USE(?FaultCode:Prompt:1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,52,124,10),USE(tmp:FaultCode[1]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,52,108,20),USE(?FaultCode:Description:1),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(296,76,110,20),USE(?FaultCode:Prompt:2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Description'),AT(564,76,108,20),USE(?FaultCode:Description:2),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,49),USE(?Button:Lookup:1),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(408,76,124,10),USE(tmp:FaultCode[2]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(536,73),USE(?Button:Lookup:2),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(296,100,110,20),USE(?FaultCode:Prompt:3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,100,124,10),USE(tmp:FaultCode[3]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(536,97),USE(?Button:Lookup:3),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Description'),AT(564,100,108,20),USE(?FaultCode:Description:3),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,124,124,10),USE(tmp:FaultCode[4]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,124,108,20),USE(?FaultCode:Description:4),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,121),USE(?Button:Lookup:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(296,124,110,20),USE(?FaultCode:Prompt:4),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,148,124,10),USE(tmp:FaultCode[5]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,148,108,20),USE(?FaultCode:Description:5),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(296,148,110,20),USE(?FaultCode:Prompt:5),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,145),USE(?Button:Lookup:5),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(296,172,110,20),USE(?FaultCode:Prompt:6),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,172,124,10),USE(tmp:FaultCode[6]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,172,108,20),USE(?FaultCode:Description:6),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(296,196,110,20),USE(?FaultCode:Prompt:7),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,193),USE(?Button:Lookup:7),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Description'),AT(564,196,108,20),USE(?FaultCode:Description:7),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,169),USE(?Button:Lookup:6),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(408,196,124,10),USE(tmp:FaultCode[7]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,220,108,20),USE(?FaultCode:Description:8),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(296,220,110,20),USE(?FaultCode:Prompt:8),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,217),USE(?Button:Lookup:8),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(408,220,124,10),USE(tmp:FaultCode[8]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,244,108,20),USE(?FaultCode:Description:9),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,241),USE(?Button:Lookup:9),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(296,244,110,20),USE(?FaultCode:Prompt:9),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,244,124,10),USE(tmp:FaultCode[9]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,268,108,20),USE(?FaultCode:Description:10),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(296,268,110,20),USE(?FaultCode:Prompt:10),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Description'),AT(564,292,108,20),USE(?FaultCode:Description:11),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(296,292,110,20),USE(?FaultCode:Prompt:11),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,265),USE(?Button:Lookup:10),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(408,268,124,10),USE(tmp:FaultCode[10]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(536,289),USE(?Button:Lookup:11),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(408,292,124,10),USE(tmp:FaultCode[11]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(564,316,108,20),USE(?FaultCode:Description:12),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,313),USE(?Button:Lookup:12),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(296,316,110,20),USE(?FaultCode:Prompt:12),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(408,316,124,10),USE(tmp:FaultCode[12]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                         END
                       END
                       BUTTON,AT(544,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(612,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(216,384),USE(?Button31),TRN,FLAT,ICON('parhistp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_map_id   ushort,auto
save_par_ali_id     ushort,auto

local       Class
GetJobFaultCode        Procedure(Long f:FaultCodeNumber),String
LookupFaultCode        Procedure(Long func:FieldNumber, String f:FaultCode),String
LookupFaultCodeField   Procedure(Long func:FieldNumber,String func:Field),Byte
            End

PassLinkedFaultCodeQueue    Queue(DefLinkedFaultCodeQueue)
                            End
!Save Entry Fields Incase Of Lookup
look:par:Supplier                Like(par:Supplier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

BuildNumbersQueue        Routine
    Free(FieldNumbersQueue)
    Free(PromptNumbersQueue)
    Free(LookupNumbersQueue)
    Free(DescriptionNumbersQueue)
    Free(FaultCodeQueue)

    fault:FaultNumber   = 1
    fault:FaultCode     = par:Fault_Code1
    Add(FaultCodeQueue)
    fault:FaultNumber   = 2
    fault:FaultCode     = par:Fault_Code2
    Add(FaultCodeQueue)
    fault:FaultNumber   = 3
    fault:FaultCode     = par:Fault_Code3
    Add(FaultCodeQueue)
    fault:FaultNumber   = 4
    fault:FaultCode     = par:Fault_Code4
    Add(FaultCodeQueue)
    fault:FaultNumber   = 5
    fault:FaultCode     = par:Fault_Code5
    Add(FaultCodeQueue)
    fault:FaultNumber   = 6
    fault:FaultCode     = par:Fault_Code6
    Add(FaultCodeQueue)
    fault:FaultNumber   = 7
    fault:FaultCode     = par:Fault_Code7
    Add(FaultCodeQueue)
    fault:FaultNumber   = 8
    fault:FaultCode     = par:Fault_Code8
    Add(FaultCodeQueue)
    fault:FaultNumber   = 9
    fault:FaultCode     = par:Fault_Code9
    Add(FaultCodeQueue)
    fault:FaultNumber   = 10
    fault:FaultCode     = par:Fault_Code10
    Add(FaultCodeQueue)
    fault:FaultNumber   = 11
    fault:FaultCode     = par:Fault_Code11
    Add(FaultCodeQueue)
    fault:FaultNumber   = 12
    fault:FaultCode     = par:Fault_Code12
    Add(FaultCodeQueue)

    field:ScreenOrder = 1
    field:FieldNumber = ?tmp:FaultCode_1
    Add(FieldNumbersQueue)
    field:ScreenOrder = 2
    field:FieldNumber = ?tmp:FaultCode_2
    Add(FieldNumbersQueue)
    field:ScreenOrder = 3
    field:FieldNumber = ?tmp:FaultCode_3
    Add(FieldNumbersQueue)
    field:ScreenOrder = 4
    field:FieldNumber = ?tmp:FaultCode_4
    Add(FieldNumbersQueue)
    field:ScreenOrder = 5
    field:FieldNumber = ?tmp:FaultCode_5
    Add(FieldNumbersQueue)
    field:ScreenOrder = 6
    field:FieldNumber = ?tmp:FaultCode_6
    Add(FieldNumbersQueue)
    field:ScreenOrder = 7
    field:FieldNumber = ?tmp:FaultCode_7
    Add(FieldNumbersQueue)
    field:ScreenOrder = 8
    field:FieldNumber = ?tmp:FaultCode_8
    Add(FieldNumbersQueue)
    field:ScreenOrder = 9
    field:FieldNumber = ?tmp:FaultCode_9
    Add(FieldNumbersQueue)
    field:ScreenOrder = 10
    field:FieldNumber = ?tmp:FaultCode_10
    Add(FieldNumbersQueue)
    field:ScreenOrder = 11
    field:FieldNumber = ?tmp:FaultCode_11
    Add(FieldNumbersQueue)
    field:ScreenOrder = 12
    field:FieldNumber = ?tmp:FaultCode_12
    Add(FieldNumbersQueue)

    lookup:ScreenOrder = 1
    lookup:FieldNumber = ?Button:Lookup:1
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 2
    lookup:FieldNumber = ?Button:Lookup:2
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 3
    lookup:FieldNumber = ?Button:Lookup:3
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 4
    lookup:FieldNumber = ?Button:Lookup:4
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 5
    lookup:FieldNumber = ?Button:Lookup:5
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 6
    lookup:FieldNumber = ?Button:Lookup:6
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 7
    lookup:FieldNumber = ?Button:Lookup:7
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 8
    lookup:FieldNumber = ?Button:Lookup:8
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 9
    lookup:FieldNumber = ?Button:Lookup:9
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 10
    lookup:FieldNumber = ?Button:Lookup:10
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 11
    lookup:FieldNumber = ?Button:Lookup:11
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 12
    lookup:FieldNumber = ?Button:Lookup:12
    Add(LookupNumbersQueue)

    prompt:ScreenOrder = 1
    prompt:FieldNumber = ?FaultCode:Prompt:1
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 2
    prompt:FieldNumber = ?FaultCode:Prompt:2
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 3
    prompt:FieldNumber = ?FaultCode:Prompt:3
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 4
    prompt:FieldNumber = ?FaultCode:Prompt:4
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 5
    prompt:FieldNumber = ?FaultCode:Prompt:5
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 6
    prompt:FieldNumber = ?FaultCode:Prompt:6
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 7
    prompt:FieldNumber = ?FaultCode:Prompt:7
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 8
    prompt:FieldNumber = ?FaultCode:Prompt:8
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 9
    prompt:FieldNumber = ?FaultCode:Prompt:9
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 10
    prompt:FieldNumber = ?FaultCode:Prompt:10
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 11
    prompt:FieldNumber = ?FaultCode:Prompt:11
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 12
    prompt:FieldNumber = ?FaultCode:Prompt:12
    Add(PromptNumbersQueue)

    description:ScreenOrder = 1
    description:FieldNumber = ?FaultCode:Description:1
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 2
    description:FieldNumber = ?FaultCode:Description:2
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 3
    description:FieldNumber = ?FaultCode:Description:3
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 4
    description:FieldNumber = ?FaultCode:Description:4
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 5
    description:FieldNumber = ?FaultCode:Description:5
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 6
    description:FieldNumber = ?FaultCode:Description:6
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 7
    description:FieldNumber = ?FaultCode:Description:7
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 8
    description:FieldNumber = ?FaultCode:Description:8
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 9
    description:FieldNumber = ?FaultCode:Description:9
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 10
    description:FieldNumber = ?FaultCode:Description:10
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 11
    description:FieldNumber = ?FaultCode:Description:11
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 12
    description:FieldNumber = ?FaultCode:Description:12
    Add(DescriptionNumbersQueue)

    Loop x# = 1 To 12
        Get(PromptNumbersQueue,x#)
        prompt:FieldNumber{prop:Text} = ''
        Get(LookupNumbersQueue,x#)
        lookup:FieldNumber{prop:Hide} = 1
        lookup:FieldNumber{prop:Disable} = 0
        Get(DescriptionNumbersQueue,x#)
        description:FieldNumber{prop:Text} = ''
        Get(FieldNumbersQueue,x#)
        field:FieldNumber{prop:Hide} = 1
        field:FieldNumber{prop:ReadOnly} = 0

    End ! Loop x# = 1 To 20

FaultCode        Routine
Data
local:Field        String(255)
local:Required     Byte(0)
local:MainFaultOnly    Byte(0)
local:FoundFault   Byte(0)
Code
    ! If "Force Accessory Fault Codes Only" ticked on Manufacturer, only show Main Fault on non accessories.
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        If sto:Accessory <> 'YES' And man:ForceAccessoryCode
            local:MainFaultOnly = 1
        End ! If sto:Accessory <> 'YES' And man:ForceAcessoryCode
    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = job:Manufacturer
    map:ScreenOrder = 0
    Set(map:ScreenOrderKey,map:ScreenOrderKey)
    Loop
        If Access:MANFAUPA.Next()
            Break
        End ! If Access:MANFAULT.Next()
        If map:Manufacturer <> job:Manufacturer
            Break
        End ! If map:Manufacturer <> f:Manfacturer
        If map:ScreenOrder = 0
            Cycle
        End ! If map:ScreenOrder = 0

        If local:MainFaultOnly
            If map:MainFault = 0
                Cycle
            End ! If map:MainFault = 0
        End ! If local:MainFaultOnly

        prompt:ScreenOrder = map:ScreenOrder
        Get(PromptNumbersQueue,prompt:ScreenOrder)
        field:ScreenOrder = map:ScreenOrder
        Get(FieldNumbersQueue,field:ScreenOrder)
        lookup:ScreenOrder = map:ScreenOrder
        Get(LookupNumbersQueue,lookup:ScreenOrder)
        description:ScreenOrder = map:ScreenOrder
        Get(DescriptionNumbersQueue,lookup:ScreenOrder)

        field:FieldNumber{prop:Hide} = 0
        prompt:FieldNumber{prop:Text} = map:Field_Name

        If local:Required And map:Compulsory = 'YES'
            If par:Adjustment = 'YES'
                If map:CompulsoryForAdjustment
                    field:FieldNumber{prop:Req} = 1
                End ! If map:CompulsoryForAdjustment
            Else ! If par:Adjustment = 'YES'
                field:FieldNumber{prop:Req} = 1
            End !If par:Adjustment = 'YES'

        Else ! If local:Required
            field:FieldNumber{prop:Req} = 0
        End ! If local:Required

        local:FoundFault = 1

        If map:MainFault
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                Case maf:Field_Type
                Of 'DATE'
                    field:FieldNumber{prop:Text} = Clip(maf:DateType)
                    If ~glo:WebJob
                        ! Show Lookup Calendar
                        lookup:FieldNumber{prop:Hide} = 0
                    End ! If ~glo:WebJob
                Of 'STRING'
                    If maf:RestrictLength
                        field:FieldNumber{prop:Text} = '@s' & maf:LengthTo
                    Else ! If map:RestrictLength
                        field:FieldNumber{prop:Text} = '@s30'
                    End ! If map:RestrictLength

                    If maf:Lookup = 'YES'
                        lookup:FieldNumber{prop:Hide} = 0
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode[map:ScreenOrder]
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            description:FieldNumber{prop:Text} = mfo:Description
                        Else ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            description:FieldNumber{prop:Text} = ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    Else ! If map:Lookup = 'YES'
                        description:FieldNumber{prop:Text} = ''
                        lookup:FieldNumber{prop:Hide} = 1
                    End ! If map:Lookup = 'YES'
                Of 'NUMBER'
                    If maf:RestrictLength
                        field:FieldNumber{prop:Text} = '@n_' & maf:LengthTo
                    Else ! If map:RestrictLength
                        field:FieldNumber{prop:Text} = '@n_9'
                    End ! If map:RestrictLength
                    If maf:Lookup
                        lookup:FieldNumber{prop:Hide} = 0
                    Else ! If map:Lookup
                        lookup:FieldNumber{prop:Hide} = 1
                    End ! If map:Lookup
                End ! Case map:Field_Type
            End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

        Else ! If map:MainFault
            Case map:Field_Type
            Of 'DATE'
                field:FieldNumber{prop:Text} = Clip(map:DateType)
                If ~glo:WebJob
                    ! Show Lookup Calendar
                    lookup:FieldNumber{prop:Hide} = 0
                End ! If ~glo:WebJob
            Of 'STRING'
                If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@s' & map:LengthTo
                Else ! If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@s30'
                End ! If map:RestrictLength

                If map:Lookup = 'YES'
                    lookup:FieldNumber{prop:Hide} = 0
                    Access:MANFPALO.Clearkey(mfp:Field_Key)
                    mfp:Manufacturer = job:Manufacturer
                    mfp:Field_Number = map:Field_Number
                    mfp:Field        = tmp:FaultCode[map:ScreenOrder]
                    If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                        description:FieldNumber{prop:Text} = mfp:Description
                    Else ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        description:FieldNumber{prop:Text} = ''
                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                Else ! If map:Lookup = 'YES'
                    description:FieldNumber{prop:Text} = ''
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If map:Lookup = 'YES'
            Of 'NUMBER'
                If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@n_' & map:LengthTo
                Else ! If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@n_9'
                End ! If map:RestrictLength
                If map:Lookup
                    lookup:FieldNumber{prop:Hide} = 0
                Else ! If map:Lookup
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If map:Lookup
            End ! Case map:Field_Type

        End ! If map:MainFault

        If map:NotAvailable = 1
            If tmp:FaultCode[map:ScreenOrder] = ''
                field:FieldNumber{prop:Hide} = 1
                prompt:FieldNumber{prop:Text} = ''
                description:FieldNumber{prop:Text} = ''
            Else ! If tmp:FaultCode[map:ScreenOrder] = ''
                field:FieldNumber{prop:ReadOnly} = 1
            End ! If tmp:FaultCode[map:ScreenOrder] = ''
            lookup:FieldNumber{prop:Hide} = 1
        Else ! If map:NotAvailable = 1
            field:FieldNumber{prop:ReadOnly} = 0
        End ! If map:NotAvailable = 1

        If map:Lookup = 'YES'
            ! Autofill fault code if only one entry in lookup
            local:Field = ''
            Count# = 0

            If ~map:MainFault
                ! There should never be only 1 out fault, so no need to check
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer = job:Manufacturer
                mfp:Field_Number = map:Field_Number
                Set(mfp:Field_Key,mfp:Field_Key)
                Loop
                    If Access:MANFPALO.Next()
                        Break
                    End ! If Access:MANFAULO.Next()
                    If mfp:Manufacturer <> job:Manufacturer
                        Break
                    End ! If mfo:Manufacturer <> f:Man
                    If mfp:Field_Number <> map:Field_Number
                        Break
                    End ! If mfo:Field_Number <> map:Field_Number
                    Count# += 1
                    If Count# > 1
                        local:Field = ''
                        Break
                    End ! If Count# > 1
                    local:Field = mfp:Field
                End ! Loop
            End  ! If ~map:MainFault
            If local:Field <> ''
                If tmp:FaultCode[map:ScreenOrder] = '' And field:FieldNumber{prop:Hide} = 0 And field:FieldNumber{prop:ReadOnly} = 0
                    tmp:FaultCode[map:ScreenOrder] = local:Field
                End ! If tmp:FaultCode[map:ScreenOrder] = '' And field:FieldNumber{prop:Hide} = 0 And field:FieldNumber{prop:Disable} = 0
            End ! If local:Field <> ''
        End ! If map:Lookup = 'YES'

        If map:NAForAccessory And sto:Accessory = 'YES' And tmp:FaultCode[map:ScreenOrder] = ''
            tmp:FaultCode[map:ScreenOrder] = 'N/A'
        End ! If map:NAForAccessory And sto:Accessory = 'YES' And Contents(field:FieldNumber) = ''

        If map:CopyFromJobFaultCode And tmp:FaultCode[map:ScreenOrder] = ''
            tmp:FaultCode[map:ScreenOrder] = local.GetJobFaultCode(map:CopyJobFaultCode)
        End ! If map:CopyFromJobFaultCode And Contents(field:FieldNumber) = ''

        ! Check if fault codes have been assigned against the stock item (DBH: 29/10/2007)
        If tmp:FaultCode[map:ScreenOrder] = '' And sto:Assign_Fault_Codes = 'YES'
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number = sto:Ref_Number
            stm:Manufacturer = job:Manufacturer
            stm:Model_Number = job:Model_Number
            If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode1
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode2
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode3
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode4
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode5
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode6
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode7
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode8
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode9
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode10
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode11
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode12
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
        End ! If tmp:FaultCode[map:ScreenOrder] = ''
    End ! Loop

    ! Check if a part main fault is set to assign a value to another fault code (DBH: 25/10/2007)
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer = job:Manufacturer
        mfp:Field_Number = map:Field_Number
        mfp:Field = tmp:FaultCode[map:ScreenOrder]
        If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            If mfp:SetPartFaultCode
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer = job:Manufacturer
                map:Field_Number = mfp:SelectPartFaultCode
                If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                    fault:FaultNumber = map:Field_Number
                    Get(FaultCodeQueue,fault:FaultNumber)
                    If ~Error()
                        If tmp:FaultCode[map:ScreenOrder] = ''
                            tmp:FaultCode[map:ScreenOrder] = mfp:PartFaultCodeValue
                        End ! If tmp:FaultCode[field:ScreenOrder] <> ''
                    End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
            End ! If mfp:SetPartFaultCode
        End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Bryan.CompFieldColour()
    Display()

    If local:FoundFault
        ?par:Fault_Codes_Checked{prop:Hide} = 0
    End ! If local:FoundFault
ShowCosts       Routine
    !Do not allow ARC to edit RRC parts and vice versa - L789 (DBH: 17-07-2003)
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  =  GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        !Get the Part's Location - L789 (DBH: 17-07-2003)
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = par:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:Location <> tra:SiteLocation
                !message('have Relocate store ='&glo:RelocateStore&' Stock location ='&sto:location)
                if glo:RelocateStore = true and Sto:location = 'MAIN STORE' then
                    !this is permitted
                    !message('This should be OK')
                    tmp:ARCPart = 1
                ELSE
                    !This part is NOT from ARC - L789 (DBH: 17-07-2003)
                    tmp:ARCPart = 0
                    If ~glo:WebJob
                        ! Changing (DBH 04/01/2006) #6645 - Allow access to certain users (to change fault codes)
                        ! ?OK{prop:Hide} = 1
                        ! to (DBH 04/01/2006) #6645
                        If SecurityCheck('AMEND RRC PART')
                            !message('Hide at security check')
                            ?OK{prop:Hide} = 1
                        End ! If SecurityCheck('AMEND RRC PART')
                        ! End (DBH 04/01/2006) #6645
                    End !If glo:WebJob
                END
            Else ! !If sto:Location <> tra:SiteLocation
                tmp:ARCPart = 1
                If glo:WebJob
                    !message('Hide at second try')
                    ?OK{prop:Hide} = 1
                End !If glo:WebJob
            End !If sto:Location <> tra:SiteLocation
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Else ! If Access:TRADE.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADE.Tryfetch(tra:Account_Number_Key) = Level:Benign
    If tmp:ARCPart
        tmp:PurchaseCost    = par:AveragePurchaseCost
        tmp:InWarrantyCost  = par:Purchase_cost
        tmp:OutWarrantyCost = par:Sale_Cost
        tmp:InWarrantyMarkup    = par:InWarrantyMarkup
        tmp:OutWarrantyMarkup   = par:OutWarrantyMarkup

    Else !If tmp:ARCPart
        If par:RRCAveragePurchaseCost > 0
            tmp:PurchaseCost    = par:RRCAveragePurchaseCost
        Else !If par:RRCAvereagePurchaseCost > 0
            tmp:PurchaseCost    = par:RRCPurchaseCost
        End !If par:RRCAvereagePurchaseCost > 0
        tmp:InWarrantyCost  = par:RRCPurchaseCost
        tmp:OutWarrantyCost = par:RRCSaleCost
        tmp:InWarrantyMarkup    = par:RRCInWarrantyMarkup
        tmp:OutWarrantyMarkup   = par:RRCOutWarrantyMarkup
    End !If tmp:ARCPart

EnableDisableCosts      Routine

!TB13351 - In the case of a warranty job the user must not be able to change any of the costs or percentage markup as warranty pricing for parts cannot be changed.
!J - 11/08/14 - Percentage markup is now available - but nothing else may be changed
!    If tmp:OutWarrantyMarkup = 0
!        Global.ReadOnly(?tmp:OutWarrantyCost,0)
!    Else !If tmp:OutWarrantyMarkup <> 0
!        Global.ReadOnly(?tmp:OutWarrantyCost,1)
!    End !If tmp:OutWarrantyMarkup <> 0
!TB13351 - END

    !Can change part cost up until the job is invoiced - L789 (DBH: 17-07-2003)
    If job:Invoice_Number = 0
        If tmp:InWarrantyMarkup <> 0
            tmp:InWarrantyCost  = Markups(tmp:InWarrantyCost,tmp:PurchaseCost,tmp:InWarrantyMarkup)
        End !If tmp:InWarrantyMarkup <> 0

        If tmp:OutWarrantyMarkup <> 0
            tmp:OutWarrantyCost = Markups(tmp:OutWarrantyCost,tmp:PurchaseCost,tmp:OutWarrantyMarkup)
        End !If tmp:OutWarrantyMarkup <> 0
    End !If ThisWindow.Request <> InsertRecord

    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = job:charge_type
    If access:chartype.fetch(cha:charge_type_key) = Level:Benign
        !Zero Parts RRC
        If cha:Zero_Parts_ARC
            If tmp:ARCPart
                UNHIDE(?String1)
                ?tmp:OutWarrantyMarkup{prop:Disable} = 1
                ?tmp:OutWarrantyCost{prop:Disable} = 1
                tmp:OutWarrantyCost = 0
            End !If tmp:ARCPart
        End
        If cha:Zero_Parts = 'YES'
            If ~tmp:ARCPart
                UNHIDE(?String1)
                ?String1{prop:text} = 'FIXED PRICE'
                ?tmp:OutWarrantyMarkup{prop:Disable} = 1
                ?tmp:OutWarrantyCost{prop:Disable} = 1
                tmp:OutWarrantyCost = 0
            End !If ~tmp:ARCPart
        End
    end !if

    Display()
GetMarkupCosts      Routine
    !Get the default markups
    If ThisWindow.Request = InsertRecord
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = sto:Location
        If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            !Found
!            par:OutWarrantyMarkUp   = loc:OutWarrantyMarkUp
        Else ! If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
            !Error
        End !If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign


        par:InWarrantyMarkup    = sto:PurchaseMarkup        ! InWarrantyMarkup(job:Manufacturer,loc:Location)
        par:OutWarrantyMarkUp   = sto:Percentage_Mark_Up    !loc:OutWarrantyMarkUp

        If glo:WebJob
            !This parts is being added by the RRC.
            par:RRCAveragePurchaseCost  = sto:AveragePurchaseCost
            par:RRCPurchaseCost         = sto:Purchase_Cost
            par:RRCSaleCost             = sto:Sale_Cost

            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:charge_type
            If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                If cha:Zero_Parts = 'YES'
                    par:RRCSaleCost             = 0
                End
            end !if

            par:RRCInWarrantyMarkup     = sto:PurchaseMarkUp
            par:RRCOutWarrantyMarkup    = sto:Percentage_Mark_Up

            !Fill in purchase and sale cost, hopefull this should filter through
            !to every else these fields are used, and not break the program
            par:Purchase_Cost           = par:RRCPurchaseCost
            par:Sale_Cost               = par:RRCSaleCost
            par:AveragePurchaseCost  = par:RRCAveragePurchaseCost
        Else !If glo:WebJob
            !This part is being added by the ARC, but need to check
            !if it was originally booked in by the RRC
            if glo:RelocateStore then
                par:AveragePurchaseCost = sto:Sale_Cost
                par:Purchase_Cost       = sto:Sale_Cost
                ! VP105
                par:Sale_Cost           = sto:Sale_Cost*(1+glo:RelocatedMark_Up/100)
                par:InWarrantyMarkup    = InWarrantyMarkup(job:Manufacturer,glo:RelocatedFrom)
                par:OutWarrantyMarkUp   = glo:RelocatedMark_Up      !sto:Percentage_Mark_Up    !loc:OutWarrantyMarkUp

            ELSE

                par:AveragePurchaseCost = sto:AveragePurchaseCost
                par:Purchase_Cost       = sto:Purchase_Cost
                ! VP105
                par:Sale_Cost           = sto:Sale_Cost

            END
            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:charge_type
            If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                If cha:Zero_Parts_ARC
                    par:Sale_Cost    = 0
                End
            end !if

           !These are blank, unless the part was origially
            !booked by an RRC
            par:RRCPurchaseCost     = 0
            par:RRCSaleCost         = 0

            !Was this job booked by an RRC?
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:WebJob
                    par:RRCAveragePurchaseCost  = par:Sale_Cost
                    par:RRCPurchaseCost         = Markups(par:RRCPurchaseCost,par:RRCAveragePurchaseCost,InWarrantyMarkup(job:Manufacturer,sto:Location))
                    par:RRCSaleCost             = Markups(par:RRCSaleCost,par:RRCAveragePurchaseCost,loc:OutWarrantyMarkup)
                    par:RRCInWarrantyMarkup     = par:InWarrantyMarkup
                    par:RRCOutWarrantyMarkup    = par:OutWarrantyMarkup
                End !If jobe:WebJob
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        End !If glo:WebJob

    End !If ThisWindow.Request <> InsertRecord
LookupMainFault     Routine
    ?OutFaultCode:Prompt{prop:Hide} = 1
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Found
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            Case map:Field_Number
                Of 1
                    mfo:Field        = par:Fault_Code1
                Of 2
                    mfo:Field        = par:Fault_Code2
                Of 3
                    mfo:Field        = par:Fault_Code3
                Of 4
                    mfo:Field        = par:Fault_Code4
                Of 5
                    mfo:Field        = par:Fault_Code5
                Of 6
                    mfo:Field        = par:Fault_Code6
                Of 7
                    mfo:Field        = par:Fault_Code7
                Of 8
                    mfo:Field        = par:Fault_Code8
                Of 9
                    mfo:Field        = par:Fault_Code9
                Of 10
                    mfo:Field        = par:Fault_Code10
                Of 11
                    mfo:Field        = par:Fault_Code11
                Of 12
                    mfo:Field        = par:Fault_Code12
            End !Case map:Field_Number
            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Found
                ?OutFaultCode:Prompt{prop:Hide} = 0
                ?OutFaultCode:Prompt{prop:Text} = 'Out Fault Code: ' & Clip(mfo:Description)
            Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
CreateWebOrder      Routine
    !Find if there is a pending order already for this job
    tmp:WebOrderNumber = 0
    Save_orh_ID = Access:ORDHEAD.SaveFile()
    Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
    orh:procesed    = 0
    Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
    Loop
        If Access:ORDHEAD.NEXT()
           Break
        End !If
        If orh:procesed    <> 0      |
            Then Break.  ! End If
        If orh:Account_No = job:Account_Number
            tmp:WebOrderNumber = orh:Order_no
            Break
        End !If orh:Account_No = job:Account_Number
    End !Loop
    Access:ORDHEAD.RestoreFile(Save_orh_ID)

    If tmp:WebOrderNumber
        !Found an existing order. Add to it
        !Has this part already been ordered?
        Found# = 0
        Save_ori_ID = Access:ORDITEMS.SaveFile()
        Access:ORDITEMS.ClearKey(ori:Keyordhno)
        ori:ordhno = tmp:WebOrderNumber
        Set(ori:Keyordhno,ori:Keyordhno)
        Loop
            If Access:ORDITEMS.NEXT()
               Break
            End !If
            If ori:ordhno <> tmp:WebOrderNumber      |
                Then Break.  ! End If
            If ori:partno = par:Part_Number And |
                ori:partdiscription = par:Description
                ori:qty += par:Quantity

                !TB12359 - J - 13/02/14 - update the date time stamp of this order
                ori:OrderDate = today()
                ori:OrderTime = clock()
                !end TB12359
                
                Access:ORDITEMS.Update()
                Found# = 1
                Break
            End !ori:partdiscription = par:Description
        End !Loop
        Access:ORDITEMS.RestoreFile(Save_ori_ID)

        If Found# = 0
            !Make a new part line
            If Access:ORDITEMS.PrimeRecord() = Level:Benign
                ori:ordhno          = tmp:WebOrderNumber
                ori:manufact        = job:Manufacturer
                ori:partno          = par:Part_Number
                ori:partdiscription = par:Description
                ori:qty             = par:Quantity
                ori:itemcost        = par:Sale_Cost
                ori:totalcost       = par:Sale_Cost * par:Quantity
                !TB12359 - J - 13/02/14 - update the date time stamp of this order
                ori:OrderDate = today()
                ori:OrderTime = clock()
                !end TB12359
                
                If Access:ORDITEMS.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:ORDITEMS.TryInsert() = Level:Benign
            End !If Access:ORDITEMS.PrimeRecord() = Level:Benign

        End !If Found# = 0
    Else !tmp:WebOrderNumber
        !Cannot find order. Make a new one
        If Access:ORDHEAD.PrimeRecord() = Level:Benign
            orh:account_no      = job:Account_Number
            orh:CustName        = job:Company_Name
            orh:CustAdd1        = job:Address_Line1
            orh:CustAdd2        = job:Address_Line2
            orh:CustAdd3        = job:Address_Line3
            orh:CustPostCode    = job:Postcode
            orh:CustTel         = job:Telephone_Number
            orh:CustFax         = job:Fax_Number
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                orh:dName           = sub:Company_Name
                orh:dAdd1           = sub:Address_Line1
                orh:dAdd2           = sub:Address_Line2
                orh:dAdd3           = sub:Address_Line3
                orh:dPostCode       = sub:Postcode
                orh:dTel            = sub:Telephone_Number
                orh:dFax            = sub:Fax_Number

            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

            orh:thedate         = Today()
            orh:thetime         = Clock()
            orh:procesed        = 0
            orh:WhoBooked       = job:Who_Booked
            orh:Department      = ''
            orh:CustOrderNumber = job:Order_Number
            If Access:ORDHEAD.TryInsert() = Level:Benign
                !Insert Successful
                !Make a new part line
                If Access:ORDITEMS.PrimeRecord() = Level:Benign
                    ori:ordhno          = orh:Order_No
                    ori:manufact        = job:Manufacturer
                    ori:partno          = par:Part_Number
                    ori:partdiscription = par:Description
                    ori:qty             = par:Quantity
                    ori:itemcost        = par:Sale_Cost
                    ori:totalcost       = par:Sale_Cost * par:Quantity

                    !TB12359 - J - 13/02/14 - update the date time stamp of this order
                    ori:OrderDate = today()
                    ori:OrderTime = clock()
                    !end TB12359
                    
                    If Access:ORDITEMS.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:ORDITEMS.TryInsert() = Level:Benign
                End !If Access:ORDITEMS.PrimeRecord() = Level:Benign

            Else !If Access:ORDHEAD.TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:ORDHEAD.TryInsert() = Level:Benign
        End !If Access:ORDHEAD.PrimeRecord() = Level:Benign
    End !tmp:WebOrderNumber
    par:WebOrder = 1
CreateNewOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number  = tmp:StockNumber
        ope:Job_Number       = job:Ref_Number
        ope:Part_Type        = 'JOB'
        ope:Supplier         = par:Supplier
        ope:Part_Number      = par:Part_Number
        ope:Description      = par:Description
        ope:Quantity         = tmp:NewQuantity
        ope:Account_Number   = job:Account_Number
        ope:PartRecordNumber = par:Record_Number

        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
Adjustment          Routine
    If par:adjustment = 'YES'
        par:Part_Number = 'ADJUSTMENT'
        par:Description = 'ADJUSTMENT'
        par:Purchase_Cost = ''
        par:Sale_Cost     = ''
        par:quantity = 1


       ?par:Part_Number{prop:readonly} = 0
       ?par:Part_Number{prop:skip} = 0
       ?par:Part_Number{prop:disable} = 1


       ?par:Description{prop:readonly} = 0
       ?par:Description{prop:skip} = 0
       ?par:Description{prop:disable} = 1


       ?par:Quantity{prop:readonly} = 0
       ?par:Quantity{prop:skip} = 0
       ?par:Quantity{prop:disable} = 1


!       ?par:Purchase_Cost{prop:readonly} = 0
!       ?par:Purchase_Cost{prop:skip} = 0
!       ?par:Purchase_Cost{prop:disable} = 1
!
!
!       ?par:Sale_Cost{prop:readonly} = 0
!       ?par:Sale_Cost{prop:skip} = 0
!       ?par:Sale_Cost{prop:disable} = 1


       ?par:Supplier{prop:readonly} = 0
       ?par:Supplier{prop:skip} = 0
       ?par:Supplier{prop:disable} = 1

       ?par:Exclude_From_Order{prop:readonly} = 0
       ?par:Exclude_From_Order{prop:skip} = 0
       ?par:Exclude_From_Order{prop:disable} = 1

    Else


       ?par:Part_Number{prop:readonly} = 0
       ?par:Part_Number{prop:skip} = 0
       ?par:Part_Number{prop:disable} = 0


       ?par:Description{prop:readonly} = 0
       ?par:Description{prop:skip} = 0
       ?par:Description{prop:disable} = 0


       ?par:Quantity{prop:readonly} = 0
       ?par:Quantity{prop:skip} = 0
       ?par:Quantity{prop:disable} = 0


!       ?par:Purchase_Cost{prop:readonly} = 0
!       ?par:Purchase_Cost{prop:skip} = 0
!       ?par:Purchase_Cost{prop:disable} = 0
!
!
!       ?par:Sale_Cost{prop:readonly} = 0
!       ?par:Sale_Cost{prop:skip} = 0
!       ?par:Sale_Cost{prop:disable} = 0
!
       ?par:Supplier{prop:readonly} = 0
       ?par:Supplier{prop:skip} = 0
       ?par:Supplier{prop:disable} = 0


       ?par:Exclude_From_Order{prop:readonly} = 0
       ?par:Exclude_From_Order{prop:skip} = 0
       ?par:Exclude_From_Order{prop:disable} = 0

    End
    Display()

Lookup_Location     Routine
    If par:part_ref_number <> ''
        access:stock.clearkey(sto:ref_number_key)
        sto:ref_number  = par:part_ref_number
        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = sto:location
            tmp:shelflocation   = sto:shelf_location
            tmp:secondlocation  = sto:second_location
        Else!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = ''
            tmp:shelflocation   = ''
            tmp:secondlocation  = ''
        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    Else!If par:part_ref_number <> ''
        tmp:location    = ''
        tmp:shelflocation   = ''
        tmp:secondlocation  = ''
    End!If par:part_ref_number <> ''
    Display()
MaxMarkup_Message Routine

    Case Missive('You cannot mark-up parts more than ' & Format(tmp:MaxMarkup,@n3) & '%.','ServiceBase 3g',|
                   'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Chargeable Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Chargeable Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020426'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Parts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(par:Record,History::par:Record)
  SELF.AddHistoryField(?par:Part_Number,5)
  SELF.AddHistoryField(?par:Description,6)
  SELF.AddHistoryField(?par:Supplier,7)
  SELF.AddHistoryField(?par:Quantity,11)
  SELF.AddHistoryField(?par:Despatch_Note_Number,14)
  SELF.AddHistoryField(?par:Exclude_From_Order,13)
  SELF.AddHistoryField(?par:Date_Ordered,15)
  SELF.AddHistoryField(?par:Order_Number,17)
  SELF.AddHistoryField(?par:Date_Received,19)
  SELF.AddHistoryField(?par:PartAllocated,38)
  SELF.AddHistoryField(?par:Fault_Codes_Checked,21)
  SELF.AddUpdateFile(Access:PARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:MANFAUPA_ALIAS.Open
  Relate:MANFPALO_ALIAS.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:ORDWEBPR.Open
  Relate:PARTS_ALIAS.Open
  Relate:STOCKALL.Open
  Relate:STOCKALX.Open
  Relate:STOCK_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:SUPPLIER.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:PARTS.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  Access:STOMODEL.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:ESTPARTS.UseFile
  Access:MANFPALO.UseFile
  Access:MANFPARL.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Security Check
  If SecurityCheck('JOB PART COSTS - EDIT')
      !Disable# = 1
      ?tmp:OutWarrantyMarkup{prop:Disable} = 1
  Else !SecurityCheck('JOB PART COSTS - EDIT')
      !Disable# = 0
      ?tmp:OutWarrantyMarkup{prop:Disable} = 0
  End !SecurityCheck('JOB PART COSTS - EDIT')
  !TB13351 - J - 11/08/14 - only the spinner is available to change the cost
  !Global.ReadOnly(?tmp:PurchaseCost,Disable#)
  !Global.ReadOnly(?tmp:OutWarrantyCost,Disable#)
  
  If SecurityCheck('PART ALLOCATED')
      ?par:PartAllocated{prop:Disable} = 1
  Else !SecurityAccess('PART ALLOCATED')
      ?par:PartAllocated{prop:Disable} = 0
  End !SecurityAccess('PART ALLOCATED')
  !! Fault Coding (Hopefully)
  !
  !fault_codes_required_temp = 'NO'
  !required# = 0
  !access:chartype.clearkey(cha:charge_type_key)
  !cha:charge_type = job:charge_type
  !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
  !    If cha:force_warranty = 'YES'
  !        required# = 1
  !    End
  !end !if
  !! Inserting (DBH 13/06/2006) #6733 - Quick fix to make sure Chargable Fault Codes are never forced
  !required# = 0
  !! End (DBH 13/06/2006) #6733
  !
  !!Ok, workaround!
  !access:manfaupa.clearkey(map:MainFaultKey)
  !map:manufacturer = job:manufacturer
  !map:MainFault = TRUE
  !IF Access:MANFAUPA.Fetch(map:MainFaultKey)
  !  !No main code, carry on a s normal!
  !ELSE
  !  If sto:Accessory <> 'YES' and man:ForceAccessoryCode
  !    !Only unhide the main fault!
  !    unhide_tab = TRUE
  !  END
  !END
  !
  !
  !found# = 0
  !setcursor(cursor:wait)
  !save_map_id = access:manfaupa.savefile()
  !access:manfaupa.clearkey(map:field_number_key)
  !map:manufacturer = job:manufacturer
  !set(map:field_number_key,map:field_number_key)
  !loop
  !    if access:manfaupa.next()
  !       break
  !    end !if
  !    if map:manufacturer <> job:manufacturer      |
  !        then break.  ! end if
  !
  !    IF unhide_tab = TRUE
  !      IF map:MainFault = FALSE
  !        CYCLE
  !      END
  !    END
  !
  !    Case map:field_number
  !        Of 1
  !            found# = 1
  !            Unhide(?par:fault_code1:prompt)
  !            ?par:fault_code1:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar)
  !                Unhide(?par:fault_code1:2)
  !            Else
  !                Unhide(?par:fault_code1)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code1{prop:req} = 1
  !                ?par:fault_code1:2{prop:req} = 1
  !            else
  !                ?par:fault_code1{prop:req} = 0
  !                ?par:fault_code1:2{prop:req} = 0
  !            End
  !        Of 2
  !            found# = 1
  !            Unhide(?par:fault_code2:prompt)
  !            ?par:fault_code2:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:2)
  !                Unhide(?par:fault_code2:2)
  !            Else
  !                Unhide(?par:fault_code2)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:2)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code2{prop:req} = 1
  !                ?par:fault_code2:2{prop:req} = 1
  !            else
  !                ?par:fault_code2{prop:req} = 0
  !                ?par:fault_code2:2{prop:req} = 0
  !            End
  !        Of 3
  !            found# = 1
  !            Unhide(?par:fault_code3:prompt)
  !            ?par:fault_code3:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:3)
  !                Unhide(?par:fault_code3:2)
  !            Else
  !                Unhide(?par:fault_code3)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:3)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code3{prop:req} = 1
  !                ?par:fault_code3:2{prop:req} = 1
  !            else
  !                ?par:fault_code3{prop:req} = 0
  !                ?par:fault_code3:2{prop:req} = 0
  !            End
  !        Of 4
  !            found# = 1
  !            Unhide(?par:fault_code4:prompt)
  !            ?par:fault_code4:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:4)
  !                Unhide(?par:fault_code4:2)
  !            Else
  !                Unhide(?par:fault_code4)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:4)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code4{prop:req} = 1
  !                ?par:fault_code4:2{prop:req} = 1
  !            else
  !                ?par:fault_code4{prop:req} = 0
  !                ?par:fault_code4:2{prop:req} = 0
  !            End
  !        Of 5
  !            found# = 1
  !            Unhide(?par:fault_code5:prompt)
  !            ?par:fault_code5:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:5)
  !                Unhide(?par:fault_code5:2)
  !            Else
  !                Unhide(?par:fault_code5)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:5)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code5{prop:req} = 1
  !                ?par:fault_code5:2{prop:req} = 1
  !            else
  !                ?par:fault_code5{prop:req} = 0
  !                ?par:fault_code5:2{prop:req} = 0
  !            End
  !        Of 6
  !            found# = 1
  !            Unhide(?par:fault_code6:prompt)
  !            ?par:fault_code6:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:6)
  !                Unhide(?par:fault_code6:2)
  !            Else
  !                Unhide(?par:fault_code6)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:6)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code6{prop:req} = 1
  !                ?par:fault_code6:2{prop:req} = 1
  !            else
  !                ?par:fault_code6{prop:req} = 0
  !                ?par:fault_code6:2{prop:req} = 0
  !            End
  !        Of 7
  !            found# = 1
  !            Unhide(?par:fault_code7:prompt)
  !            ?par:fault_code7:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:7)
  !                Unhide(?par:fault_code7:2)
  !            Else
  !                Unhide(?par:fault_code7)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:7)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code7{prop:req} = 1
  !                ?par:fault_code7:2{prop:req} = 1
  !            else
  !                ?par:fault_code7{prop:req} = 0
  !                ?par:fault_code7:2{prop:req} = 0
  !            End
  !
  !        Of 8
  !            found# = 1
  !            Unhide(?par:fault_code8:prompt)
  !            ?par:fault_code8:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:8)
  !                Unhide(?par:fault_code8:2)
  !            Else
  !                Unhide(?par:fault_code8)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:8)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code8{prop:req} = 1
  !                ?par:fault_code8:2{prop:req} = 1
  !            else
  !                ?par:fault_code8{prop:req} = 0
  !                ?par:fault_code8:2{prop:req} = 0
  !            End
  !
  !        Of 9
  !            found# = 1
  !            Unhide(?par:fault_code9:prompt)
  !            ?par:fault_code9:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:9)
  !                Unhide(?par:fault_code9:2)
  !            Else
  !                Unhide(?par:fault_code9)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:9)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code9{prop:req} = 1
  !                ?par:fault_code9:2{prop:req} = 1
  !            else
  !                ?par:fault_code9{prop:req} = 0
  !                ?par:fault_code9:2{prop:req} = 0
  !            End
  !
  !        Of 10
  !            found# = 1
  !            Unhide(?par:fault_code10:prompt)
  !            ?par:fault_code10:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:10)
  !                Unhide(?par:fault_code10:2)
  !            Else
  !                Unhide(?par:fault_code10)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:10)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code10{prop:req} = 1
  !                ?par:fault_code10:2{prop:req} = 1
  !            else
  !                ?par:fault_code10{prop:req} = 0
  !                ?par:fault_code10:2{prop:req} = 0
  !            End
  !
  !        Of 11
  !            found# = 1
  !            Unhide(?par:fault_code11:prompt)
  !            ?par:fault_code11:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:11)
  !                Unhide(?par:fault_code11:2)
  !            Else
  !                Unhide(?par:fault_code11)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:11)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code11{prop:req} = 1
  !                ?par:fault_code11:2{prop:req} = 1
  !            else
  !                ?par:fault_code11{prop:req} = 0
  !                ?par:fault_code11:2{prop:req} = 0
  !            End
  !
  !        Of 12
  !            found# = 1
  !            Unhide(?par:fault_code12:prompt)
  !            ?par:fault_code12:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:12)
  !                Unhide(?par:fault_code12:2)
  !            Else
  !                Unhide(?par:fault_code12)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:12)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?par:fault_code12{prop:req} = 1
  !                ?par:fault_code12:2{prop:req} = 1
  !            else
  !                ?par:fault_code12{prop:req} = 0
  !                ?par:fault_code12:2{prop:req} = 0
  !            End
  !
  !    End !Case map:field_number
  !end !loop
  !access:manfaupa.restorefile(save_map_id)
  !setcursor()
  !If found# = 1
  !    ?FaultCodeTab{prop:Hide} = 0
  !    ?NoFaultCodeTab{prop:Hide} = 1
  !Else
  !    ?FaultCodeTab{prop:Hide} = 1
  !    ?NoFaultCodeTab{prop:Hide} = 0
  !End
  
  Do Lookup_Location
  !ThisMakeOver.SetWindow(Win:Form)
  Display()
  If ThisWindow.Request = Insertrecord
      par:quantity = 1
  Else
      Disable(?browse_stock_button)
  End
  Do Adjustment
  If par:order_number <> ''
      ?title{prop:text} = 'Chargeable Part - On Order'
  End
  If par:pending_ref_number <> ''
      ?title{prop:text} = 'Chargeable Part - Pending Order'
  End
  If par:date_received <> ''
      ?title{prop:text} = 'Chargeable Part - Received'
  End
  If PAR:Exclude_From_Order = 'YES'
      ?title{prop:text} = 'Chargeable Part - Excluded From Order'
  End!If PAR:Exclude_From_Order = 'YES'
  If par:WebOrder
      ?title{prop:Text} = 'Chargeable Part - Retail Order Raised'
      ?par:Quantity{prop:Disable} = 1
  End !par:WebOrder
  
  If Thiswindow.request <> Insertrecord
      Disable(?par:exclude_from_order)
  End!If THiswindow.request <> Insertrecord
  If ThisWindow.Request <> InsertRecord
      !There was never any code to allow a user to change
      !the details of a part, apart from the quantity,
      !once it is inserted. So I'm going to disable the fields.
      ?browse_stock_button{prop:Hide} = 1
      Global.ReadOnly(?par:Part_Number,1)
      Global.ReadOnly(?par:Description,1)
      Global.Readonly(?par:Supplier,1)
      ?LookupSupplier{prop:Hide} = 1
      Global.ReadOnly(?tmp:PurchaseCost,1)
  
  
  
      if job:Date_Completed <> ''
          Global.ReadOnly(?tmp:OutWarrantyCost,1)
          ?tmp:OutWarrantyMarkup{prop:Disable} = 1
          ?par:Quantity{prop:Disable} = 1
          !new check for security to overwrite disabling added by J
  
          !I think this is dangerous - but then I shouldn't think!
          !If job:invoice_Number <> '' should be here!
  
  ! #11682 Seems to be an irrelevant access level. (Bryan: 15/09/2010)
  !        If SecurityCheck('JOB COSTS - EDIT POST COMPLETE')
  !            !no access - no changes
  !        ELSE
  !            Global.ReadOnly(?tmp:OutWarrantyCost,0)
  !            ?tmp:OutWarrantyMarkup{prop:Disable} = 0
  !            ?par:Quantity{prop:Disable} = 0
  !        END !security check
      end
  
      ! Insert --- Blame me, but added as part of SBonline because the code to create an order for an amended part does NOT work. (DBH: 18/08/2009) #
      ?par:Quantity{prop:Disable} = 1
      ! end --- (DBH: 18/08/2009) #
  
      !Do not allow to change the part if it's from an estimate - L789 (DBH: 17-07-2003)
      If job:Estimate = 'YES'
          Access:ESTPARTS.ClearKey(epr:Part_Ref_Number_Key)
          epr:Ref_Number      = par:Ref_Number
          epr:Part_Ref_Number = par:Part_Ref_Number
          If Access:ESTPARTS.TryFetch(epr:Part_Ref_Number_Key) = Level:Benign
              !Found
              !This same part is on the Estimate Tab - L789 (DBH: 17-07-2003)
              tmp:FromEstimate = 1
              Global.ReadOnly(?tmp:OutWarrantyCost,1)
              ?tmp:OutWarrantyMarkup{prop:Disable} = 1
              !Still allow to save record incase Fault Codes are required - L927 (DBH: 15-08-2003)
              !?OK{prop:Hide} = 1
          Else !If Access:ESTPARTS.TryFetch(epr:Part_Ref_Number2_Key) = Level:Benign
              !Error
          End !If Access:ESTPARTS.TryFetch(epr:Part_Ref_Number2_Key) = Level:Benign
  
      End !If job:Estimate = 'YES'
  
  
  End !ThisWindow.Request <> InsertRecord
  tmp:MaxMarkup = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  
  Do Lookup_Location
  Do ShowCosts
  
  sav:ExcludeFromOrder    = par:Exclude_From_Order
  sav:Supplier = par:Supplier
  do EnableDisableCosts
  
  If ThisWindow.Request <> InsertRecord
      If par:Part_Ref_Number <> '' And par:PartAllocated And ~par:WebOrder
          !Don't show the Unallocate Part button
          !if the OK button is hidden - 3310 (DBH: 25-09-2003)
          !message('Hideing at enablecosts')
          If ?OK{prop:Hide} = 0
              ?UnallocatePart{prop:Hide} = 0
          End !If ?OK{prop:Hide} = 0
      End !If wpr:Part_Ref_Number <> '' And wpr:PartAllocated
  End !ThisWindow.LocalRequest <> InsertRecord
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?par:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?par:Supplier{Prop:Tip}
  END
  IF ?par:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?par:Supplier{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:MANFAUPA_ALIAS.Close
    Relate:MANFPALO_ALIAS.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:ORDWEBPR.Close
    Relate:PARTS_ALIAS.Close
    Relate:STOCKALL.Close
    Relate:STOCKALX.Close
    Relate:STOCK_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    par:Adjustment = 'NO'
    par:Warranty_Part = 'NO'
    par:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSuppliers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?browse_stock_button
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  Case Missive('Error! Cannot access the stock file.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:Part_Number = 'EXCH'
                      Case Missive('Cannot attach an "Exchange Part".','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      par:Part_Number = ''
                      Select(?par:Part_Number)
                  Else !If sto:Part_Number = 'EXCH'
      
                      if sto:ExchangeUnit = 'YES'
                          Case Missive('Error! An exchange unit must be picked from Exchange Stock.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          par:Part_Number = ''
                          select(?par:Part_Number)
                      else
                          If sto:Suspend
                              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              par:Part_Number = ''
                              Select(?par:Part_Number)
                          Else !If sto:Suspend
                              If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                  Case Missive('Error! You skill level is not high enough to use the selected part.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  par:Part_Number = ''
                                  Select(?par:Part_Number)
                              Else !If sto:SkillLevel > use:SkillLevel
                                  par:part_number     = sto:part_number
                                  par:description     = sto:description
                                  par:supplier        = sto:supplier
                                  par:purchase_cost   = sto:purchase_cost
                                  par:sale_cost       = sto:sale_cost
                                  par:Retail_Cost     = sto:Retail_Cost
                                  par:part_ref_number = sto:ref_number
      
                                  Do GetMarkupCosts
      
                                  If sto:assign_fault_codes = 'YES'
                                      stm:Manufacturer = sto:Manufacturer
                                      stm:Model_Number = job:Model_Number
                                      If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                                          par:fault_code1    = stm:Faultcode1
                                          par:fault_code2    = stm:Faultcode2
                                          par:fault_code3    = stm:Faultcode3
                                          par:fault_code4    = stm:Faultcode4
                                          par:fault_code5    = stm:Faultcode5
                                          par:fault_code6    = stm:Faultcode6
                                          par:fault_code7    = stm:Faultcode7
                                          par:fault_code8    = stm:Faultcode8
                                          par:fault_code9    = stm:Faultcode9
                                          par:fault_code10   = stm:Faultcode10
                                          par:fault_code11   = stm:Faultcode11
                                          par:fault_code12   = stm:Faultcode12
      
                                      End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                                  End!If sto:assign_fault_codes = 'YES'
                                  Select(?par:quantity)
                              End !If sto:SkillLevel > use:SkillLevel
                          End !If sto:Suspend
                      end
      
      
                  End !If sto:Part_Number = 'EXCH'
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
          end
          globalrequest     = saverequest#
          do lookup_location
      End !Error# = 0
      
      Do ShowCosts
      Do EnableDisableCosts
      do LookupMainFault
    OF ?tmp:OutWarrantyMarkup
      if ~0{prop:AcceptAll}
          if tmp:MaxMarkup <> 0
              if tmp:OutWarrantyMarkup > tmp:MaxMarkup
                  tmp:OutWarrantyMarkup = tmp:MaxMarkup
                  do MaxMarkup_Message
                  select(?tmp:OutWarrantyMarkup)
                  cycle
              end
          end
      end
    OF ?par:Exclude_From_Order
      If ~0{prop:acceptall}
          If sav:ExcludeFromOrder <> par:Exclude_From_Order
              If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  par:Exclude_From_Order = sav:ExcludeFromOrder
              Else!If SecurityCheck('EXCLUDE FROM ORDER')
                  If thiswindow.request = Insertrecord
                      If PAR:Exclude_From_Order = 'YES' And par:part_ref_number <> ''
                          Case Missive('You have selected to "Exclude From Order".'&|
                            '<13,10>This stock part will NOT be decremented from stock.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
      
                                  sav:ExcludeFromOrder    = par:Exclude_From_Order
                              Of 1 ! No Button
      
                                  par:Exclude_From_Order  = sav:ExcludeFromOrder
                          End ! Case Missive
                      End!If PAR:Exclude_From_Order = 'YES'
                  End!If thiswindow.request = Insertrecord
              End!If SecurityCheck('EXCLUDE FROM ORDER')
          End!If sav:ExcludeFromOrder <> par:Exclude_From_Order
      End!If ~0{prop:acceptall}
      Display()
    OF ?Cancel
      Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Cycle
      End ! Case Missive
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020426'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020426'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020426'&'0')
      ***
    OF ?par:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = par:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign And par:Part_Number <> 'EXCH'
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                  Error# = 4
                              Else !If sto:SkillLevel > use:SkillLevel
                                  error# = 0
                              End !If sto:SkillLevel > use:SkillLevel
      
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
      
              End !If Error# = 0
      
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = par:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          error# = 0
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
      
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
              par:part_number     = sto:part_number
              par:description     = sto:description
              par:supplier        = sto:supplier
              par:purchase_cost   = sto:purchase_cost
              par:sale_cost       = sto:sale_cost
              par:Retail_Cost     = sto:Retail_Cost
              par:part_ref_number = sto:ref_number
      
              Do GetMarkupCosts
      
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      par:fault_code1    = stm:Faultcode1
                      par:fault_code2    = stm:Faultcode2
                      par:fault_code3    = stm:Faultcode3
                      par:fault_code4    = stm:Faultcode4
                      par:fault_code5    = stm:Faultcode5
                      par:fault_code6    = stm:Faultcode6
                      par:fault_code7    = stm:Faultcode7
                      par:fault_code8    = stm:Faultcode8
                      par:fault_code9    = stm:Faultcode9
                      par:fault_code10   = stm:Faultcode10
                      par:fault_code11   = stm:Faultcode11
                      par:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
              Select(?par:quantity)
              display()
      
          Of 1
              Case Missive('Cannot find the selected part.'&|
                '<13,10>It does either not exist in the Engineer''s Location, or is NOT in stock.'&|
                '<13,10>'&|
                '<13,10>Do you wish to SELECT another part, or CONTINUE using the entered part number?','ServiceBase 3g',|
                             'mquest.jpg','Continue|Select')
                  Of 2 ! Select Button
      
                      Post(event:accepted,?browse_stock_button)
                  Of 1 ! Continue Button
                      Select(?par:description)
              End ! Case Missive
          Of 2
              Case Missive('This part cannot be used. It has been suspended.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              par:Part_Number = ''
              Select(?par:Part_Number)
          Of 3
              Case Missive('Error! Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                '<13,10>'&|
                '<13,10>This location is NOT active and cannot be used.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              par:Part_Number = ''
              Select(?par:Part_Number)
          Of 4
              Case Missive('Error! Your skill level is not high enough to use the selected part.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              par:Part_Number = ''
              Select(?par:Part_Number)
      End!If error# = 0
      Do Lookup_Location
      Display()
      End!If ~0{prop:acceptall}
    OF ?browse_stock_button
      ThisWindow.Update
      Do FaultCode
    OF ?tmp:PurchaseCost
      Do EnableDisableCosts
    OF ?tmp:OutWarrantyCost
      Do EnableDisableCosts
    OF ?tmp:OutWarrantyMarkup
      Do EnableDisableCosts
    OF ?par:Supplier
      IF par:Supplier OR ?par:Supplier{Prop:Req}
        sup:Company_Name = par:Supplier
        !Save Lookup Field Incase Of error
        look:par:Supplier        = par:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            par:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            par:Supplier = look:par:Supplier
            SELECT(?par:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If sav:Supplier <> par:Supplier
              If SecurityCheck('PARTS - AMEND SUPPLIER')
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  par:Supplier    = sav:Supplier
              Else!If SecurityCheck('PARTS - AMEND SUPPLIER')
                  sav:Supplier    = par:Supplier
              End!If SecurityCheck('PARTS - AMEND SUPPLIER')
          End!If sav:Supplier <> par:Supplier
      End!If ~0{prop:acceptall}
      Display(?Par:Supplier)
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = par:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          par:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?par:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?par:Supplier)
    OF ?UnallocatePart
      ThisWindow.Update
      If SecurityCheck('UNALLOCATE PART')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('UNALLOCATE PART')
          Case Missive('This part will be returned to stock and will appear in the "Parts On Order" section of Stock Allocation.'&|
            '<13,10>'&|
            '<13,10>Are you sure you want to unallocate this part?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  tmp:UnAllocate = 1
                  Post(Event:Accepted,?OK)
              Of 1 ! No Button
          End ! Case Missive
      End !SecurityCheck('UNALLOCATE PART')
    OF ?tmp:FaultCode_1
      x# = 1
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:1)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:1
      ThisWindow.Update
      x# = 1
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_2
      x# = 2
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:2)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:2
      ThisWindow.Update
      x# = 2
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_3
      x# = 3
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:3)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:3
      ThisWindow.Update
      x# = 3
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_4
      x# = 4
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:4)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:4
      ThisWindow.Update
      x# = 4
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_5
      x# = 5
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:5)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:5
      ThisWindow.Update
      x# = 5
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_6
      x# = 6
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:6)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:7
      ThisWindow.Update
      x# = 7
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?Button:Lookup:6
      ThisWindow.Update
      x# = 6
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_7
      x# = 7
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:7)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:8
      ThisWindow.Update
      x# = 8
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_8
      x# = 8
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:8)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:9
      ThisWindow.Update
      x# = 9
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_9
      x# = 9
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:9)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:10
      ThisWindow.Update
      x# = 10
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_10
      x# = 10
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:10)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:11
      ThisWindow.Update
      x# = 11
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_11
      x# = 11
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:11)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:12
      ThisWindow.Update
      x# = 12
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_12
      x# = 12
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:12)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button31
      ThisWindow.Update
      BrowsePartNumberHistory(par:Part_Ref_Number)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Fault Codes Checked
  If par:fault_codes_checked <> 'YES'
      If fault_codes_required_temp = 'YES'
          Case Missive('You must tick the "Fault Codes Checked" box before you can can continue.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?par:fault_codes_checked)
          Cycle
      End!If fault_codes_required_temp = 'YES'
  End!If par:fault_codes_checked <> 'YES'
  
  ! Inserting (DBH 29/11/2007) # 9585 - Double check that all the fault codes are still correct
  Error# = 0
  Loop x# = 1 To 12
      Get(FieldNumbersQueue,x#)
      If field:FieldNumber{prop:Hide} = 0
          If local.LookupFaultCodeField(x#,Contents(field:FieldNumber))
              field:FieldNumber{prop:Color} = color:Red
              Error# = 1
          End ! If local.LookupFaultCodeField(x#,Contents(field:FieldNumber))
      End ! If field:FieldNumber{prop:Hide} = 0
  End ! Loop x# = 1 To 12
  
  If Error#
      Cycle
  End ! If Error#
  ! End (DBH 29/11/2007) #9585
  
  ! Write back fault codes (DBH: 19/10/2007)
  Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
  map:Manufacturer = job:Manufacturer
  map:ScreenOrder = 0
  Set(map:ScreenOrderKey,map:ScreenOrderKey)
  Loop
      If Access:MANFAUPA.Next()
          Break
      End ! If Access:MANFAUPA.Next()
      If map:Manufacturer <> job:Manufacturer
          Break
      End ! If map:Manufacturer <> job:Manufacturer
      If map:ScreenOrder = 0
          Cycle
      End ! If map:ScreenOrder = 0
      Case map:Field_Number
      Of 1
          par:Fault_Code1  = tmp:FaultCode[map:ScreenOrder]
      Of 2
          par:Fault_Code2  = tmp:FaultCode[map:ScreenOrder]
      Of 3
          par:Fault_Code3  = tmp:FaultCode[map:ScreenOrder]
      Of 4
          par:Fault_Code4  = tmp:FaultCode[map:ScreenOrder]
      Of 5
          par:Fault_Code5  = tmp:FaultCode[map:ScreenOrder]
      Of 6
          par:Fault_Code6  = tmp:FaultCode[map:ScreenOrder]
      Of 7
          par:Fault_Code7  = tmp:FaultCode[map:ScreenOrder]
      Of 8
          par:Fault_Code8  = tmp:FaultCode[map:ScreenOrder]
      Of 9
          par:Fault_Code9  = tmp:FaultCode[map:ScreenOrder]
      Of 10
          par:Fault_Code10 = tmp:FaultCode[map:ScreenOrder]
      Of 11
          par:Fault_Code11 = tmp:FaultCode[map:ScreenOrder]
      Of 12
          par:Fault_Code12 = tmp:FaultCode[map:ScreenOrder]
      End ! Case map:Field_Number
  End ! Loop
  !Check max markup
  if tmp:MaxMarkup <> 0
      if tmp:OutWarrantyMarkup > tmp:MaxMarkup
          do MaxMarkup_Message
          select(?tmp:OutWarrantyMarkup)
          cycle
      end
  end
  !Write Costs Back
      If tmp:ARCPart
          par:AveragePurchaseCost = tmp:PurchaseCost
          par:Purchase_Cost       = tmp:InWarrantyCost
          par:Sale_Cost           = tmp:OutWarrantyCost
          par:InWarrantyMarkup    = tmp:InWarrantyMarkup
          par:OutWarrantyMarkup   = tmp:OutWarrantyMarkup
      Else !If tmp:ARCPart
          par:RRCAveragePurchaseCost  = tmp:PurchaseCost
          par:RRCPurchaseCost     = tmp:InWarrantyCost
          par:RRCSaleCost         = tmp:OutWarrantyCost
          par:RRCInWarrantyMarkup = tmp:InWarrantyMarkup
          par:RRCOutWarrantyMarkup   = tmp:OutWarrantyMarkup
          par:Purchase_Cost       = par:RRCPurchaseCost
          par:Sale_Cost           = par:RRCSaleCost
      End !If tmp:ARCPart
  If thiswindow.Request = InsertRecord
      par:PartAllocated = 1
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      !Is this a stock part?
      tmp:StockNumber = 0
      If par:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = par:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              !Need to check for duplicates?
              tmp:StockNumber = sto:Ref_Number
              Found# = 0
              If sto:AllowDuplicate = 0
                  Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
                  Access:PARTS_ALIAS.ClearKey(par_ali:Part_Number_Key)
                  par_ali:Ref_Number  = job:Ref_Number
                  par_ali:Part_Number = par:Part_Number
                  Set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
                  Loop
                      If Access:PARTS_ALIAS.NEXT()
                         Break
                      End !If
                      If par_ali:Ref_Number  <> job:Ref_Number      |
                      Or par_ali:Part_Number <> par:Part_Number     |
                          Then Break.  ! End If
                      If par:Record_Number <> par_ali:Record_Number
                          If par_ali:Date_Received = ''
                              Found# = 1
                              Break
                          End !If par_ali:Date_Received
                      End !If par:Record_Number <> par_ali:Record_Number
                  End !Loop
                  Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
                  If Found# = 1
                      Case Missive('This part is already attached to this job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Select(?par:part_number)
                      Cycle
                  End !If Found# = 1
              End !If sto:AllowDuplicate = 0
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      End !par:Part_Ref_Number <> ''
  
      If par:Part_Number <> 'ADJUSTMENT'
          !The part is not an adjustment
          If par:Exclude_From_Order <> 'YES'
              !The part can be ordered
              If tmp:StockNumber
                  If sto:Sundry_Item <> 'YES'
  
                      !This is from stock
                      If par:Quantity > sto:Quantity_Stock
                          !There isn't enough in stock
                          Case Missive('There are insufficient items in stock.'&|
                            '<13,10>Do you wish to ORDER the remaining items, or RE-ENTER the quantity required?','ServiceBase 3g',|
                                         'mquest.jpg','Re-Enter|Order')
                              Of 2 ! Order Button
                                  If SecurityCheck('JOBS - ORDER PARTS')
                                      Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                  Else !If SecurityCheck('JOBS - ORDER PARTS')
                                      ! #10396 Not sure how this could happen. But stop using part that is suspended. (DBH: 16/07/2010)
                                      If (MainStoreSuspended(sto:Part_Number))
                                          Beep(Beep:SystemHand)  ;  Yield()
                                          Case Missive('This part is out of stock. It has been suspended at Main Store and therefore cannot be ordered.','ServiceBase',|
                                                         'mstop.jpg','/&OK')
                                          Of 1 ! &OK Button
                                          End!Case Message
                                      else ! If (MainStoreSuspended(sto:Part_Number))
  
                                          !Is this a web job?
                                          If VirtualSite(sto:Location)
                                              !message('This is a virtual site')
                                              CreateWebOrder('C',par:Quantity - sto:Quantity_Stock)
                                              If sto:Quantity_Stock > 0
                                                  glo:Select1 = 'NEW PENDING WEB'
                                                  glo:Select2 = par:Part_Ref_Number
                                                  glo:Select3 = par:Quantity - sto:Quantity_Stock
                                                  !glo:Select4 =
                                                  par:Quantity    = sto:Quantity_Stock
                                                  par:Date_Ordered    = Today()
                                              Else !If sto:Quantity_Stock > 0
                                                  par:WebOrder    = 1
                                                  par:PartAllocated = 0
                                                  ! Inserting (DBH 19/06/2006) #6733 - Add to Stock Allocation
                                                  ! Inserting (DBH 12/09/2006) # 8188 - Save record
                                                  Access:PARTS.Update()
                                                  ! End (DBH 12/09/2006) #8188
                                                  AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                                                  ! End (DBH 19/06/006) #6733
                                              End !If sto:Quantity_Stock > 0
                                          Else !If job:WebJob
                                              !message('this is NOT a virtual site')
                                              tmp:CreateNewOrder = 0
                                              Case def:SummaryOrders
                                                  Of 0 !Old Way
                                                      tmp:CreateNewOrder = 1
                                                  Of 1 !New Way
                                                      !Check to see if a pending order already exists.
                                                      !If so, add to that.
                                                      !If not, create a new pending order
                                                      Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                                      ope:Supplier    = par:Supplier
                                                      ope:Part_Number = par:Part_Number
                                                      If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                          !Found
                                                          ope:Quantity += par:Quantity
                                                          Access:ORDPEND.Update()
  
                                                          tmp:CreateNewOrder = 0
                                                          !If there is some in stock, then use that for this part
                                                          !then pass the variables, to create another part line
                                                          !for the ordered part.
                                                          If sto:Quantity_Stock > 0
                                                              glo:Select1 = 'NEW PENDING'
                                                              glo:Select2 = par:Part_Ref_Number
                                                              glo:Select3 = par:Quantity-sto:quantity_stock
                                                              glo:Select4 = ope:Ref_Number
                                                              par:Quantity    = sto:Quantity_Stock
                                                              par:Date_Ordered    = Today()
                                                          Else !If sto:Quantity_Stock > 0
                                                              par:Pending_Ref_Number  =  ope:Ref_Number
                                                              par:Requested   = True
  
                                                          End !If sto:Quantity_Stock > 0
                                                      Else! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                                          tmp:CreateNewOrder  = 1
                                                          par:Requested   = True
                                                          !Error
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                      End! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
  
                                              End !Case def:SummaryOrders
  
                                              If tmp:CreateNewOrder = 1
                                                  If Access:ORDPEND.PrimeRecord() = Level:Benign
                                                      ope:Part_Ref_Number = sto:Ref_Number
                                                      ope:Job_Number      = job:Ref_Number
                                                      ope:Part_Type       = 'JOB'
                                                      ope:Supplier        = par:Supplier
                                                      ope:Part_Number     = par:Part_Number
                                                      ope:Description     = par:Description
  
                                                      If sto:Quantity_Stock <=0
                                                          par:Pending_Ref_Number  = ope:Ref_Number
                                                          ope:Quantity    = par:Quantity
                                                          ope:PartRecordNumber    = par:Record_Number
                                                      Else !If sto:Quantity_Stock <=0
                                                          glo:Select1 = 'NEW PENDING'
                                                          glo:Select2 = par:Part_Ref_Number
                                                          glo:Select3 = par:Quantity - sto:Quantity_Stock
                                                          glo:Select4 = ope:Ref_Number
                                                          ope:Quantity    = par:Quantity - sto:Quantity_Stock
                                                          par:Quantity    = sto:Quantity_Stock
                                                          par:Date_Ordered = Today()
                                                      End !If sto:Quantity_Stock <=0
                                                      If Access:ORDPEND.TryInsert() = Level:Benign
                                                          !Insert Successful
                                                      Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                                          !Insert Failed
                                                      End !If Access:ORDPEND.TryInsert() = Level:Benign
                                                  End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                                              End !If tmp:CreateNewOrder = 1
  
                                              !If the stock is relocated then we jump the gun a bit at this point
                                              !Log Number 012031 ( ARC To Request Spares directly) - JC - 25/05/12
                                              if glo:RelocateStore and sto:Quantity_Stock <= 0 then
                                                  !this could not be met - it becomes the order
                                                  par:WebOrder    = 1
                                                  par:PartAllocated = 0
                                                  Access:PARTS.Update()
                                                  AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
  
                                              END !if glo:RelocateStore
                                          End !If glo:WebJob
                                      End ! If (MainStoreSuspended(sto:Part_Number))
                                  End !If SecurityCheck('JOBS - ORDER PARTS')
                              Of 1 ! Re-Enter Button
                                  Select(?par:Quantity)
                                  Cycle
                          End ! Case Missive
                      Else !If par:Quantity > sto:Quantity
                          !There is sufficient in stock
                          par:Date_Ordered    = Today()
                          !Reget Stock
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              !If its not a Rapid site, turn part allocated off
                              If RapidLocation(sto:Location)
                                  par:PartAllocated = 0
                                  ! Inserting (DBH 19/06/2?06) #6733 - Add to stock allocation
                                  Access:PARTS.Update()
                                  AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)
                                  ! End (DBH 19/06/2006) #6733
                              Else !If RapidLocation(sto:Location)
                                  !message('This is NOT rapid location thing')
                              End !If RapidLocation(sto:Location) = Level:Benign
                              sto:Quantity_Stock  -= par:Quantity
                              If sto:Quantity_Stock < 0
                                  sto:Quantity_Stock = 0
                              End !If sto:Quantity_Stock < 0
                              If Access:STOCK.Update() = Level:Benign
                                  If def:Add_Stock_Label = 'YES'
                                      glo:Select1 = sto:Ref_Number
                                      Case def:Label_Printer_Type
                                          of 'TEC B-440 / B-442'
                                              stock_request_label(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
                                          of 'TEC B-452'
                                              stock_request_label_B452(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
                                      End !Case def:Label_Printer_Type
                                  End !If def:Add_Stock_Label = 'YES'
  
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'DEC', | ! Transaction_Type
                                                     par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                     job:Ref_Number, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     par:Quantity, | ! Quantity
                                                     tmp:InWarrantyCost, | ! Purchase_Cost
                                                     tmp:OutWarrantyCost, | ! Sale_Cost
                                                     par:Retail_Cost, | ! Retail_Cost
                                                     'STOCK DECREMENTED', | ! Notes
                                                     '') ! Information
                                    ! Added OK
                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
                                  ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
                                  SuspendedPartCheck()
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      End !If par:Quantity > sto:Quantity
                  Else !If sto:Sundry_Item <> 'YES'
                      !Sundry Item, do nothing
                      par:Date_Ordered    = Today()
                  End !If sto:Sundry_Item <> 'YES'
              Else !If tmp:StockNumber
                  !A non stock part
                  Case Missive('This is a NON-STOCK item.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish it to appear on the next parts order?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          If VirtualSite(sto:Location)
                              !message('This is vitual site 2')
                              CreateWebOrder('C',par:Quantity)
                              par:WebOrder = 1
                              par:PartAllocated = 0
                              ! Inserting (DBH 19/06/2006) #6733 - Add to Stock Allocations
                              Access:PARTS.Update()
                              AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                              ! End (DBH 19/06/2006) #6733
                          Else !If glo:WebJob
                              !message('This is NOT virtual site 2')
                              If Access:ORDPEND.PrimeRecord() = Level:Benign
                                  ope:Job_Number      = job:Ref_Number
                                  ope:Part_Type       = 'JOB'
                                  ope:Supplier        = par:Supplier
                                  ope:Part_Number     = par:Part_Number
                                  ope:Description     = par:Description
                                  ope:Quantity        = par:Quantity
                                  ope:PartRecordNumber    = par:Record_Number
                                  par:Pending_Ref_Number  = ope:Ref_Number
                                  If Access:ORDPEND.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:ORDPEND.TryInsert() = Level:Benign
                              End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                          End !If glo:WebJob
                      Of 1 ! No Button
                          par:Exclude_From_Order  = 'YES'
                          par:Date_Ordered    = Today()
                  End ! Case Missive
  
              End !If tmp:StockNumber
          Else !If par:Exclude_From_Order <> 'YES'
              !excluded, do nothing
              par:Date_Ordered = Today()
          End !If par:Exclude_From_Order <> 'YES'
  
      Else !par:Part_Number <> 'ADJUSTMENT'
          !An ajustment, do nothing
          par:Date_Ordered    = Today()
      End !par:Part_Number <> 'ADJUSTMENT'
  End !If thiswindow.Request = InsertRecord
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If thiswindow.Request <> Insertrecord
      !Is this a part from stock?
      If par:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = par:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              tmp:StockNumber = sto:Ref_Number
  
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
      End !If par:Part_Ref_Number <> ''
  
      If tmp:Unallocate
  
          Pointer# = Pointer(STOCK)
          Hold(STOCK,1)
          Get(STOCK,Pointer#)
          If Errorcode() = 43
              Case Missive('The stock entry is in use by another station.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If Errorcode() = 43
          sto:Quantity_Stock += par:Quantity
          If Access:STOCK.Update() = Level:Benign
              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                 'REC', | ! Transaction_Type
                                 par:Despatch_Note_Number, | ! Depatch_Note_Number
                                 job:Ref_Number, | ! Job_Number
                                 0, | ! Sales_Number
                                 par:Quantity, | ! Quantity
                                 par:Purchase_Cost, | ! Purchase_Cost
                                 par:Sale_Cost, | ! Sale_Cost
                                 par:Retail_Cost, | ! Retail_Cost
                                 'STOCK UNALLOCATED', | ! Notes
                                 '') ! Information
                ! Added OK
  
              Else ! AddToStockHistory
                ! Error
              End ! AddToStockHistory
              par:Status = 'WEB'
              par:PartAllocated = 0
              par:WebOrder = 1
          End !If Access:STOCK.Update() = Level:Benign
  
      Else !If tmp:Unallocate
          !Normal part
          If par:Date_Ordered <> '' And par:Order_Number = '' and par:Pending_Ref_Number = ''
              If par:Quantity > Quantity_Temp
                  !Stock part?
                  If tmp:StockNumber
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = tmp:StockNumber
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Found
                          Pointer# = Pointer(STOCK)
                          Hold(STOCK,1)
                          Get(STOCK,Pointer#)
                          If Errorcode() = 43
                              Case Missive('The stock entry is in use by another station.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Cycle
                          End !If Errorcode() = 43
  
                          If sto:Quantity_Stock < (par:Quantity - Quantity_Temp)
                              !There isn't enough in stock
                              Case Missive('You have increases the quantity required, but there are insufficient items in stock.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to ORDER the remaining items, or RE-ENTER the quantity required?','ServiceBase 3g',|
                                             'mquest.jpg','Re-Enter|Order')
                                  Of 2 ! Order Button
                                      !Is there any existing pending part to add to?
                                      tmp:PendingPart = 0
                                      Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
                                      Access:PARTS_ALIAS.ClearKey(par_ali:RefPartRefNoKey)
                                      par_ali:Ref_Number      = job:Ref_Number
                                      par_ali:Part_Ref_Number = tmp:StockNumber
                                      Set(par_ali:RefPartRefNoKey,par_ali:RefPartRefNoKey)
                                      Loop
                                          If Access:PARTS_ALIAS.NEXT()
                                             Break
                                          End !If
                                          If par_ali:Ref_Number      <> job:Ref_Number      |
                                          Or par_ali:Part_Ref_Number <> tmp:StockNumber      |
                                              Then Break.  ! End If
                                          If par_ali:Record_Number = par:Record_Number
                                              Cycle
                                          End !If par_ali:Record_Number = par:Record_Number
                                          If par_ali:Pending_Ref_Number <> ''
                                              tmp:PendingPart = par_ali:Record_Number
                                              Break
                                          End !If par:Pending_Ref_Number <> ''
                                      End !Loop
                                      Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
  
                                      tmp:NewQuantity     = (par:Quantity - Quantity_Temp) - sto:Quantity_Stock
  
                                      If tmp:PendingPart
                                          tmp:CurrentState    = Getstate(PARTS)
  
                                          Access:PARTS.Clearkey(par:RecordNumberKey)
                                          par:Record_Number   = tmp:PendingPart
                                          If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                                              !Found
                                              par:Quantity += tmp:NewQuantity
                                              Access:PARTS.Update()
  
                                              tmp:CreateNewOrder = 0
                                              !Find pending order
                                              Case def:SummaryOrders
                                                  Of 0 !Old Wauy
                                                      Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                                                      ope:Ref_Number  = par:Pending_Ref_Number
                                                      If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                          !Found
                                                          ope:Quantity += tmp:NewQuantity
                                                          Access:ORDPEND.Update()
  
                                                      Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                          !Error
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          Do CreateNewOrder
                                                      End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                  Of 1 !New Way
                                                      Access:ORDPEND.ClearKey(ope:Supplier_Key)
                                                      ope:Supplier    = par:Supplier
                                                      ope:Part_Number = par:Part_Number
                                                      If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                                          ope:Quantity += tmp:NewQuantity
                                                          Access:ORDPEND.Update()
                                                      Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Lev !Found
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          Do CreateNewOrder
  
                                                      End !If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                              End !Case def:SummaryOrders
  
                                              RestoreState(PARTS,tmp:CurrentState)
                                              FreeState(PARTS,tmp:CurrentState)
                                          Else! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
  
                                      Else !If tmp:PendingPart
                                          Do CreateNewOrder
  
                                          !Create new pending part
                                          glo:Select1 = 'NEW PENDING'
                                          glo:Select2 = par:Part_Ref_Number
                                          glo:Select3 = (par:Quantity - Quantity_Temp) - sto:Quantity_Stock
                                          glo:Select4 = ope:Ref_Number
                                          par:Quantity    = Quantity_Temp + sto:Quantity_Stock
  
                                      End !If tmp:PendingPart
                                  Of 1 ! Re-Enter Button
                                      par:Quantity    = Quantity_Temp
                                      Select(?par:Quantity)
                                      Cycle
                              End ! Case Missive
  
                          End !If sto:Quantity_Stock < (par:Quantity - Quantity_Temp)
  
                          !There is enough in stock, take it.
                          If sto:Quantity_Stock > (par:Quantity - Quantity_Temp)
                              Access:STOCK.Clearkey(sto:Ref_Number_Key)
                              sto:Ref_Number  = tmp:StockNumber
                              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Found
                                  sto:Quantity_Stock -= (par:Quantity - Quantity_Temp)
                                  If Access:STOCK.Update() = Level:Benign
                                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                         'DEC', | ! Transaction_Type
                                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                         job:Ref_Number, | ! Job_Number
                                                         0, | ! Sales_Number
                                                         par:Quantity - Quantity_Temp, | ! Quantity
                                                         tmp:InWarrantyCost, | ! Purchase_Cost
                                                         tmp:OutWarrantyCost, | ! Sale_Cost
                                                         par:Retail_Cost, | ! Retail_Cost
                                                         'STOCK DECREMENTED', | ! Notes
                                                         '') ! Information
                                        ! Added OK
  
                                      Else ! AddToStockHistory
                                        ! Error
                                      End ! AddToStockHistory
                                  End !If Access:STOCK.Update() = Level:Benign
                              Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                          End !If sto:Quantity_Stock > (par:Quantity - Quantity_Temp)
                      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                  End !If tmp:StockNumber
              End !If quantity increased
              !Quantity decreased
              If Quantity_Temp > par:Quantity
                  If tmp:StockNumber
                      !A Stock part. Put back into stock?
                      Case Missive('You have decreased the quantity required.'&|
                        '<13,10>'&|
                        '<13,10>What do you wish to do with the excess? RETURN it to stock, or SCRAP it?','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|Scrap|Restock')
                          Of 3 ! Restock Button
                              Case Missive('This item was original taken from location ' & Clip(sto:Location) & '.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to return it to it''s ORIGINAL location, or to a NEW location?','ServiceBase 3g',|
                                             'mquest.jpg','\Cancel|New|Original')
                                  Of 3 ! Original Button
  
                                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                      sto:Ref_Number  = tmp:StockNumber
                                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                          !Found
                                          Pointer# = Pointer(STOCK)
                                          Hold(STOCK,1)
                                          Get(STOCK,Pointer#)
                                          If Errorcode() = 43
                                              Case Missive('The stock entry is in use by another station.','ServiceBase 3g',|
                                                             'mstop.jpg','/OK')
                                                  Of 1 ! OK Button
                                              End ! Case Missive
                                              Cycle
                                          End !If Errorcode() = 43
                                          sto:Quantity_Stock += Quantity_Temp - par:Quantity
                                          If Access:STOCK.Update() = Level:Benign
                                              If access:stock.update() = Level:Benign
                                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                     'ADD', | ! Transaction_Type
                                                                     par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                     job:Ref_Number, | ! Job_Number
                                                                     0, | ! Sales_Number
                                                                     Quantity_Temp - par:Quantity, | ! Quantity
                                                                     par:Purchase_Cost, | ! Purchase_Cost
                                                                     par:Sale_Cost, | ! Sale_Cost
                                                                     par:Retail_Cost, | ! Retail_Cost
                                                                     'CHARGEABLE PART AMENDED ON JOB', | ! Notes
                                                                     '') ! Information
                                                    ! Added OK
  
                                                  Else ! AddToStockHistory
                                                    ! Error
                                                  End ! AddToStockHistory
                                              End!If access:stock.update = Level:Benign
                                          End !If Access:STOCK.Update() = Level:Benign
                                      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                                  Of 2 ! New Button
  
                                      glo:select1 = ''
                                      Pick_New_Location
                                      If glo:select1 <> ''
                                          access:stock.clearkey(sto:location_part_description_key)
                                          sto:location    = glo:select1
                                          sto:part_number = par:part_number
                                          sto:description = par:description
                                          If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                              sto:quantity_stock   += par:quantity
                                              access:stock.update()
                                              do_delete# = 1
                                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'ADD', | ! Transaction_Type
                                                                 par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                 job:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 par:Quantity, | ! Quantity
                                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                                 par:Sale_Cost, | ! Sale_Cost
                                                                 par:Retail_Cost, | ! Retail_Cost
                                                                 'CHARGEABLE PART AMENDED ON JOB', | ! Notes
                                                                 '') ! Information
                                                ! Added OK
  
                                              Else ! AddToStockHistory
                                                ! Error
                                              End ! AddToStockHistory
                                          Else!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                              Case Missive('Cannot find the selected part in location ' & Clip(glo:Select1) & '.'&|
                                                '<13,10>'&|
                                                '<13,10>Do you wish to add this part as a NEW item, or SCRAP it?','ServiceBase 3g',|
                                                             'mquest.jpg','\Cancel|Scrap|New')
                                                  Of 3 ! New Button
  
                                                      glo:select2  = ''
                                                      glo:select3  = ''
                                                      Pick_Locations
                                                      If glo:select1 <> ''
                                                          do_delete# = 1
                                                          Get(stock,0)
                                                          If access:stock.primerecord() = Level:Benign
                                                              sto:part_number = par:part_number
                                                              sto:description = par:description
                                                              sto:supplier    = par:supplier
                                                              sto:purchase_cost   = par:purchase_cost
                                                              sto:sale_cost   = par:sale_cost
                                                              sto:shelf_location  = glo:select2
                                                              sto:manufacturer    = job:manufacturer
                                                              sto:location    = glo:select1
                                                              sto:second_location = glo:select3
                                                              If access:stock.insert() = Level:Benign
                                                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                     'ADD', | ! Transaction_Type
                                                                                     par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                                     job:Ref_Number, | ! Job_Number
                                                                                     0, | ! Sales_Number
                                                                                     par:Quantity, | ! Quantity
                                                                                     par:Purchase_Cost, | ! Purchase_Cost
                                                                                     par:Sale_Cost, | ! Sale_Cost
                                                                                     par:Retail_Cost, | ! Retail_Cost
                                                                                     'CHARGEABLE PART AMENDED ON JOB', | ! Notes
                                                                                     '') ! Information
                                                                    ! Added OK
  
                                                                  Else ! AddToStockHistory
                                                                    ! Error
                                                                  End ! AddToStockHistory
                                                              End!If access:stock.insert() = Level:Benign
                                                          End!If access:stock.primerecord() = Level:Benign
                                                      End!If glo:select1 = ''
                                                  Of 2 ! Scrap Button
                                                  Of 1 ! Cancel Button
                                              End ! Case Missive
                                          End !If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                      End !If glo:select1 <> ''
                                  Of 1 ! Cancel Button
                              End ! Case Missive
                          Of 2 ! Scrap Button
                          Of 1 ! Cancel Button
                      End ! Case Missive
                  Else !If tmp:StockNumber
  
                  End !If tmp:StockNumber
              End !If Quantity_Temp > par:Quantity
          End !If par:Date_Ordered <> '' And par:Order_Number = '' and par:Pending_Ref_Number = ''
  
          !This is a pending part
          If par:Pending_Ref_Number <> '' and par:Order_Number = ''
              !Quantity increased
              If par:Quantity > Quantity_Temp
                  !Find a pending order
                  Case def:SummaryOrders
                      Of 0 !Old Way
                          Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                          ope:Ref_Number  = par:Pending_Ref_Number
                          If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Found
  
                              ope:Quantity += par:Quantity - Quantity_Temp
                              Access:ORDPEND.Update()
                          Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                              !Cannot find a pending order, lets' make one
                              tmp:NewQuantity = par:Quantity - Quantity_Temp
                              Do CreateNewOrder
                              !Create new pending part
                              glo:Select1 = 'NEW PENDING'
                              glo:Select2 = par:Part_Ref_Number
                              glo:Select3 = (par:Quantity - Quantity_Temp)
                              glo:Select4 = ope:Ref_Number
                              par:Quantity   = Quantity_Temp
  
                          End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
  
                      Of 1 !New Way
                          Access:ORDPEND.ClearKey(ope:Supplier_Key)
                          ope:Supplier    = par:Supplier
                          ope:Part_Number = par:Part_Number
                          If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                              !Found
                              ope:Quantity += par:Quantity - Quantity_Temp
                              Access:ORDPEND.Update()
                          Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                              tmp:NewQuantity = par:Quantity - Quantity_Temp
                              Do CreateNewOrder
                          End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                  End !Case def:SummaryOrders
              End !If par:Quantity > Quantity_Temp
              !Quantity decreased
              If par:Quantity < Quantity_Temp
                  !Find a pending order
                  Case def:SummaryOrders
                      Of 0 !Old Way
                          Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                          ope:Ref_Number  = par:Pending_Ref_Number
                          If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Found
                              ope:Quantity -= (Quantity_Temp - par:Quantity)
                              If ope:Quantity <= 0
                                  Delete(ORDPEND)
                              Else !If ope:Quantity <= 0
                                  Access:ORDPEND.Update()
                              End !If ope:Quantity <= 0
  
                          Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
  
                      Of 1 !New Way
                          Access:ORDPEND.Clearkey(ope:Supplier_Key)
                          ope:Supplier    = par:Supplier
                          ope:Part_Number = par:Part_Number
                          If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                              !Found
                              ope:Quantity -= (Quantity_Temp - par:Quantity)
                              If ope:Quantity <= 0
                                  Delete(ORDPEND)
                              Else !If ope:Quantity <= 0
                                  Access:ORDPEND.Update()
                              End !If ope:Quantity <= 0
                          Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
  
                  End !Case def:SummaryOrders
              End !If par:Quantity < Quantity_Temp
          End !If par:Pending_Ref_Number <> '' and par:Order_Number = ''
      End !If tmp:Unallocate
  End !If thiswindow.Request <> Insertrecord
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:OutWarrantyMarkup
    Do EnableDisableCosts
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
      !Is this An Adjustment?
      If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
          par:adjustment = 'YES'
          par:part_number = 'ADJUSTMENT'
          par:description = 'ADJUSTMENT'
      
      End!If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
      adjustment_temp = par:Adjustment
      !Save Fields
      quantity_temp = par:quantity
      tmp:part_number     = par:part_number
      tmp:description     = par:description
      tmp:supplier        = par:supplier
      tmp:purchase_cost   = par:purchase_cost
      tmp:sale_cost       = par:sale_cost
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Popup Stock Button
      If par:Part_Number = ''
          Post(Event:Accepted,?Browse_Stock_Button)
      End !wpr:Part_Number = ''
      Do BuildNumbersQueue
      
      ! Fill In Fault Codes
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = job:Manufacturer
      map:ScreenOrder = 0
      Set(map:ScreenOrderKey,map:ScreenOrderKey)
      Loop
          If Access:MANFAUPA.Next()
              Break
          End ! If Access:MANFAUPA.Next()
          If map:Manufacturer <> job:Manufacturer
              Break
          End ! If map:Manufacturer <> job:Manufacturer
          If map:ScreenOrder = 0
              Cycle
          End ! If map:ScreenOrder = 0
      
          fault:FaultNumber = map:Field_Number
          Get(FaultCodeQueue,fault:FaultNumber)
          tmp:FaultCode[map:ScreenOrder] = fault:FaultCode
      End ! Loop
      
      Do FaultCode
      Do LookupMainFault
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.GetJobFaultCode        Procedure(Long f:FaultCodeNumber)
Code
    Case f:FaultCodeNumber
    Of 1
        Return job:Fault_Code1
    Of 2
        Return job:Fault_Code2
    Of 3
        Return job:Fault_Code3
    Of 4
        Return job:Fault_Code4
    Of 5
        Return job:Fault_Code5
    Of 6
        Return job:Fault_Code6
    Of 7
        Return job:Fault_Code7
    Of 8
        Return job:Fault_code8
    Of 9
        Return job:Fault_Code9
    Of 10
        Return job:Fault_code10
    Of 11
        Return job:Fault_Code11
    Of 12
        Return job:Fault_Code12
    Of 13
        Return wob:FaultCode13
    Of 14
        Return wob:FaultCode14
    Of 15
        Return wob:FaultCode15
    Of 16
        Return wob:FaultCode16
    Of 17
        Return wob:FaultCode17
    Of 18
        Return wob:FaultCode18
    Of 19
        Return wob:FaultCode19
    Of 20
        Return wob:FaultCode20
    End ! Case f:FaultCodeNumber
local.LookupFaultCode        Procedure(Long f:FieldNumber, String f:FaultCode)
local:JobType       Byte(0)
local:PartType      Byte(0)
!BlankQueue          Queue(DefLinkedFaultCodeQueue)
!                    End
local:ReturnValue   String(30)
local:LinkedRecordNumber    Long()
Code
    local:ReturnValue = f:FaultCode

    If par:Adjustment = 'YES'
        local:PartType = 1
    Else ! If par:Adjustment = 'YES'
        If par:Correction
            local:PartType = 2
        End ! If par:Correction
    End ! If par:Adjustment = 'YES'

    ! Used to restrict the lookup to char/warr (DBH: 23/10/2007)
    If job:Chargeable_Job = 'YES'
        local:JobType = 1
        If job:Warranty_Job = 'YES'
            local:JobType = 0
        End ! If job:Warranty_Job = 'YES'
    Else ! If job:Chargeable_Job = 'YES'
        local:JobType = 2
    End ! If job:Chargeable_Job = 'YES'

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = job:Manufacturer
    map:ScreenOrder = f:FieldNumber
    If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign

        If map:MainFault
            ! This is the main fault, validate against the Job Main Fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault = 1
            If Access:MANFAULT.Tryfetch(maf:MainFaultKey) = Level:Benign

                Request# = GlobalRequest
                GlobalRequest = SelectRecord

                ! Use restricted Browse?
                Free(PassLinkedFaultCodeQueue)
                If map:UseRelatedJobCode <> 0
! Changing (DBH 15/09/2008) # 10442. - Using wrong part number
!                    BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,f:FieldNumber,local:PartType,PassLinkedFaultCodeQueue)
! to (DBH 15/09/2008) # 10442.
                    BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,map:Field_Number,local:PartType,PassLinkedFaultCodeQueue)
! End (DBH 15/09/2008) #10442.

                Else ! If map:UseRelatedJobCode <> 0
                    BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,'',local:PartType,PassLinkedFaultCodeQueue)
                End ! If map:UseRelatedJobCode <> 0

                GlobalRequest = Request#

                If GlobalResponse = RequestCompleted
                    local:ReturnValue = mfo:Field
                End ! If GlobalResponse = RequestCompleted
            Else ! If Access:MANFAULT.Clearkey(maf:MainFaultKey) = Level:Benign
                !Error
                Case Missive('This fault code is set as the "Main Fault" but there is no corresponding "Main Fault" for the job fault codes.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If Access:MANFAULT.Clearkey(maf:MainFaultKey) = Level:Benign
        Else ! If map:MainFault
            Found# = 0
            ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
            If sto:Assign_Fault_Codes = 'YES'
                Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                stm:Ref_Number = sto:Ref_Number
                stm:Manufacturer = job:Manufacturer
                stm:Model_Number = job:Model_Number
                If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                    Case map:Field_Number
                    Of 1
                        If stm:FaultCode1 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 2
                        If stm:FaultCode2 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 3
                        If stm:FaultCode3 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 4
                        If stm:FaultCode4 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 5
                        If stm:FaultCode5 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 6
                        If stm:FaultCode6 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 7
                        If stm:FaultCode7 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 8
                        If stm:FaultCode8 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 9
                        If stm:FaultCode9 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 10
                        If stm:FaultCode10 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 11
                        If stm:FaultCode11 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 12
                        If stm:FaultCode12 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    End ! Case map:Field_Number
                    If Found# = 1
                        Request# = GlobalRequest
                        GlobalRequest = SelectRecord
                        BrowseStockFaultCodeLookup(stm:RecordNumber,map:Field_Number)
                        GlobalRequest = Request#
                        If GlobalResponse = RequestCompleted
                            local:ReturnValue = stu:Field
                        End ! If GlobalReponse = RequestCompleted
                    End ! If Found# = 1
                End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
            End ! If sto:Assign_Fault_Codes = 'YES'

            If Found# = 0
! Changing (DBH 24/04/2008) # 9723 - Lookup for linked fault codes
!                ! Validate against Part Codes
!                Request# = GlobalRequest
!                GlobalRequest = SelectRecord
!                BrowsePartFaultCodeLookup(job:Manufacturer,map:Field_Number,local:PartType,local:JobType)
!                GlobalRequest = Request#
!                If GlobalResponse = RequestCompleted
!                    local:ReturnValue = mfp:Field
!                End ! If GlobalResponse = RequestCompleted
! to (DBH 24/04/2008) # 9723
                ! Go through all the fault codes to see if THIS fault code is a secondary linked code (DBH: 24/04/2008)
                Free(PassLinkedFaultCodeQueue)
                Loop num# = 1 to 12
                    If num# = f:FieldNumber
                        Cycle
                    End ! If num# = f:FieldNumber

                    ! Only count visible fault codes (DBH: 24/04/2008)
                    field:ScreenOrder = num#
                    Get(FieldNumbersQueue,field:ScreenOrder)
                    If ~Error()
                        If field:FieldNumber{prop:Hide} = 1
                            Cycle
                        End ! If field:FieldNumber{prop:Hide} = 1
                    End ! If ~Error()

                    If tmp:FaultCode[num#] <> ''
                        ! Get the record number of the fault code (DBH: 24/04/2008)
                        Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
                        map_ali:Manufacturer = job:Manufacturer
                        map_ali:ScreenOrder = num#
                        If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                            ! OK
                            Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
                            mfp_ali:Manufacturer = job:Manufacturer
                            mfp_ali:Field_Number = map_ali:Field_Number
                            mfp_ali:Field = tmp:FaultCode[num#]
                            If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign
                                ! OK
                                Found# = 0
                                Access:MANFPARL.Clearkey(mpr:FieldKey)
                                mpr:MANFPALORecordNumber = mfp_ali:RecordNumber
                                mpr:FieldNumber = map:Field_Number
                                Set(mpr:FieldKey,mpr:FieldKey)
                                Loop
                                    If Access:MANFPARL.Next()
                                        Break
                                    End ! If Access:MANFPARL.Next()
                                    If mpr:MANFPALORecordNumber <> mfp_ali:RecordNumber
                                        Break
                                    End ! If mpr:MANFPALORecordNumber <> mfp_ali:RecordNumber
                                    If mpr:FieldNumber <> map:Field_Number
                                        Break
                                    End ! If mpr:FieldNumber <> map:Field_Number
                                    Found# = 1
                                    Break
                                End ! Loop
                                If Found# = 1
                                    PassLinkedFaultCodeQueue.RecordNumber = mfp_ali:RecordNumber
                                    PassLinkedFaultCodeQueue.FaultType = 'P'
                                    Add(PassLinkedFaultCodeQueue)
                                End ! If Found# = 1
                            Else ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign
                                ! Error
                            End ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign
                        Else ! If Access:MANFAUPA.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                            ! Error
                        End ! If Access:MANFAUPA.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                    End ! If tmp:FaultCode[num#] <> ''
                End ! Loop num# = 1 to 12

                Do LookupRelatedJobFaultCodes

                Request# = GlobalRequest
                GlobalRequest = SelectRecord
                BrowsePartFaultCodeLookup(job:Manufacturer,map:Field_Number,local:PartType,local:JobType,PassLinkedFaultCodeQueue)
                GlobalRequest = Request#
                If GlobalResponse = RequestCompleted

                    ! A new fault code has been picked. Check the secondary fault codes (DBH: 24/04/2008)
                    If f:FaultCode <> mfp:Field
                        Loop num# = 1 To 12
                            If num# = f:FieldNumber
                                Cycle
                            End ! If num# = f:FieldNumber

                            field:ScreenOrder = num#
                            Get(FieldNumbersQueue,field:ScreenOrder)
                            If ~Error()
                                If field:FieldNumber{prop:Hide} = 1
                                    Cycle
                                End ! If field:FieldNumber{prop:Hide} = 1
                            End ! If ~Error()

                            Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
                            map_ali:Manufacturer = job:Manufacturer
                            map_ali:ScreenOrder = num#
                            If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey)  = Level:Benign
                                ! OK

                            Else ! If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                                ! Error
                                Cycle
                            End ! If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign

                            ! Count how many secondary values. And if only one, auto fill (DBH: 24/04/2008)
                            Count# = 0
                            Access:MANFPARL.Clearkey(mpr:FieldKey)
                            mpr:MANFPALORecordNumber = mfp:RecordNumber
                            mpr:FieldNumber = map_ali:Field_Number
                            Set(mpr:FieldKey,mpr:FieldKey)
                            Loop
                                If Access:MANFPARL.Next()
                                    Break
                                End ! If Access:MANFPARL.Next()
                                If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                    Break
                                End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                If mpr:FieldNumber <> map_ali:Field_Number
                                    Break
                                End ! If mpr:FieldNumber <> map_ali:Field_Number

                                local:LinkedRecordNumber = mpr:LinkedRecordNumber
                                Count# += 1
                                If Count# > 1
                                    Break
                                End ! If Count# > 1
                            End ! Loop

                            If Count# = 1
                                Access:MANFPALO_ALIAS.Clearkey(mfp_ali:RecordNumberKey)
                                mfp_ali:RecordNumber = local:LinkedRecordNumber
                                If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:RecordNumberKey) = Level:Benign
                                    ! Found
                                    tmp:FaultCode[num#] = mfp_ali:Field
                                Else ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:RecordNumberKey) = Level:Benign
                                End ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:RecordNumberKey) = Level:Benign
                            Elsif Count# > 1
                                ! Blank, the user must select a new code
                                tmp:FaultCode[num#] = ''
                                Display()
                            End ! If Count# = 1
                        End ! Loop num# = 1 To 12
                    End ! If f:Field <> mfp:Field

                    local:ReturnValue = mfp:Field
                End ! If GlobalReponse = RequestCompleted
! End (DBH 24/04/2008) #9723
            End ! If Found# = 0
        End ! If map:MainFault

        If map:KeyRepair
            If local:ReturnValue = 1
                ! This is a key repair fault code. Check no other parts have been marked as Key Repair (DBH: 09/11/2007)
                Found# = 0
                Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
                par_ali:Ref_Number = job:Ref_Number
                Set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
                Loop
                    If Access:PARTS_ALIAS.Next()
                        Break
                    End ! If Access:PARTS_ALIAS.Next()
                    If par_ali:Ref_Number <> job:Ref_Number
                        Break
                    End ! If par_ali:Ref_Number <> job:Ref_Number
                    If par_ali:Record_Number = par:Record_Number
                        Cycle
                    End ! If par_ali:RecordNumber = war:RecordNumber
                    Case map:Field_Number
                    Of 1
                        If par_ali:Fault_Code1 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 2
                        If par_ali:Fault_Code2 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 3
                        If par_ali:Fault_Code3 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 4
                        If par_ali:Fault_Code4 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 5
                        If par_ali:Fault_Code5 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 6
                        If par_ali:Fault_Code6 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 7
                        If par_ali:Fault_Code7 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 8
                        If par_ali:Fault_Code8 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 9
                        If par_ali:Fault_Code9 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 10
                        If par_ali:Fault_Code10 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 11
                        If par_ali:Fault_Code11 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    Of 12
                        If par_ali:Fault_Code12 = 1
                            Found# = 1
                        End ! If par:Fault_Code1 = 1
                    End ! Case map:Field_Number
                    If Found# = 1
                        Case Missive('Part Number ' & Clip(par_ali:Part_Number) & ' has already been marked as "Key Repair".','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        local:ReturnValue = f:FaultCode
                        Break
                    End ! If Found# = 1
                End ! Loop
                If Found# = 0
                    Access:WARPARTS_ALIAS.Clearkey(war_ali:Part_Number_Key)
                    war_ali:Ref_Number = job:Ref_Number
                    Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                    Loop
                        If Access:WARPARTS_ALIAS.Next()
                            Break
                        End ! If Access:WARPARTS_ALIAS.Next()
                        If war_ali:Ref_Number <> job:Ref_Number
                            Break
                        End ! If war_ali:Ref_Number <> job:Ref_Number
                        If war_ali:Record_Number = wpr:Record_Number
                            Cycle
                        End ! If war_ali:RecordNumber = war:RecordNumber
                        Case map:Field_Number
                        Of 1
                            If war_ali:Fault_Code1 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 2
                            If war_ali:Fault_Code2 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 3
                            If war_ali:Fault_Code3 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 4
                            If war_ali:Fault_Code4 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 5
                            If war_ali:Fault_Code5 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 6
                            If war_ali:Fault_Code6 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 7
                            If war_ali:Fault_Code7 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 8
                            If war_ali:Fault_Code8 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 9
                            If war_ali:Fault_Code9 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 10
                            If war_ali:Fault_Code10 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 11
                            If war_ali:Fault_Code11 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        Of 12
                            If war_ali:Fault_Code12 = 1
                                Found# = 1
                            End ! If wpr:Fault_Code1 = 1
                        End ! Case map:Field_Number
                        If Found# = 1
                            Case Missive('Part Number ' & Clip(war_ali:Part_Number) & ' has already been marked as "Key Repair".','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            local:ReturnValue = f:FaultCode
                            Break
                        End ! If Found# = 1
                    End ! Loop
                End ! If Found# = 0
            End ! If local:ReturnValue = 1
        End ! If map:KeyRepair
    End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
    Return local:ReturnValue

LookupRelatedJobFaultCodes       Routine
Data
local:FaultCodeValue            String(255)
Code
                Loop f# = 1 To 20
                    Case f#
                    Of 1
                        If job:Fault_Code1 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code1
                    Of 2
                        If job:Fault_Code2 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code2
                    Of 3
                        If job:Fault_Code3 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code3
                    Of 4
                        If job:Fault_Code4 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code4
                    Of 5
                        If job:Fault_Code5 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code5
                    Of 6
                        If job:Fault_Code6 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code6
                    Of 7
                        If job:Fault_Code7 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code7
                    Of 8
                        If job:Fault_Code8 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code8
                    Of 9
                        If job:Fault_Code9 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code9
                    Of 10
                        If job:Fault_Code10 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code10
                    Of 11
                        If job:Fault_Code11 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code11
                    Of 12
                        If job:Fault_Code12 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = job:Fault_Code12
                    Of 13
                        If wob:FaultCode13 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode13
                    Of 14
                        If wob:FaultCode14 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode14
                    Of 15
                        If wob:FaultCode15 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode15
                    Of 16
                        If wob:FaultCode16 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode16
                    Of 17
                        If wob:FaultCode17 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode17
                    Of 18
                        If wob:FaultCode18 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode18
                    Of 19
                        If wob:FaultCode19 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode19
                    Of 20
                        If wob:FaultCode20 = ''
                            Cycle
                        End ! If job:Fault_Code1 = ''
                        local:FaultCodeValue = wob:FaultCode20
                    End ! Case f#

                    Access:MANFAULT.Clearkey(maf:Field_Number_Key)
                    maf:Manufacturer = job:Manufacturer
                    maf:Field_Number = f#
                    If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
                        ! Found
                        If maf:Lookup = 'YES'
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = job:Manufacturer
                            mfo:Field_Number = maf:Field_Number
                            mfo:Field = local:FaultCodeValue
                            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                ! Found
                                Access:MANFAURL.Clearkey(mnr:FieldKey)
                                mnr:MANFAULORecordNumber = mfo:RecordNumber
                                mnr:PartFaultCode = 1
                                mnr:FieldNumber = map:Field_Number
                                Set(mnr:FieldKey,mnr:FieldKey)
                                Loop
                                    If Access:MANFAURL.Next()
                                        Break
                                    End ! If Access:MANFAURL.Next()
                                    If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                                        Break
                                    End ! If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                                    If mnr:PartFaultCode <> 1
                                        Break
                                    End ! If mnr:PartFaultCode <> 1
                                    If mnr:FieldNumber <> map:Field_Number
                                        Break
                                    End ! If mnr:FieldNumber <> map:Field_Number
                                    Found# = 1
                                    Break
                                End ! Loop (MANFAURL)
                                If Found# = 1
                                    PassLinkedFaultCodeQueue.RecordNumber = mfo:RecordNumber
                                    PassLinkedFaultCodeQueue.FaultType = 'J'
                                    Add(PassLinkedFaultCodeQueue)
                                End ! If Found# = 1
                            Else ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        End ! If maf:Lookup = 'YES'
                    Else ! If Access:MANFAUL.TryFetch(maf:Field_Number_Key) = Level:Benign
                    End ! If Access:MANFAUL.TryFetch(maf:Field_Number_Key) = Level:Benign

                End ! Loop f# = 1 To 20
Local.LookupFaultCodeField      Procedure(Long func:FieldNumber,String func:Field)
Code
    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = job:Manufacturer
    map:ScreenOrder  = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
        !Found
        If map:Lookup <> 'YES'
            Return Level:Benign
        End ! If map:Lookup <> 'YES'
        ! Inserting (DBH 28/11/2007) # 9585 - Don't check lookup, if not forced
        If map:Force_Lookup <> 'YES'
            Return Level:Benign
        End ! If map:Force_Lookup <> 'YES'
        ! End (DBH 28/11/2007) #9585
        If map:MainFault
            !Ok, validate the field against the Job Main Fault
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                If map:Force_Lookup = 'YES'
                    If map:UseRelatedJobCode <> 0
                        Access:MANFAULO.ClearKey(mfo:RelatedFieldKey)
                        mfo:Manufacturer    = job:Manufacturer
                        mfo:RelatedPartCode = map:Field_Number
                        mfo:Field_Number    = maf:Field_Number
                        mfo:Field           = func:Field
                        If Access:MANFAULO.TryFetch(mfo:RelatedFieldKey) = Level:Benign
                            !Found
                        Else!If Access:MANFAULO.TryFetch(mfo:RelatedFieldKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            Return Level:Fatal
                        End!If Access:MANFAULO.TryFetch(mfo:RelatedFieldKey) = Level:Benign
                    Else !If map:UseRelatedJobCode <> 0
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = func:Field

                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Lev   !Found
                            Return Level:Fatal
                        End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End !If map:UseRelatedJobCode <> 0

                End !If map:Force_Lookup = 'YES'

            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                Case Missive('This fault code is set as the "Main Fault" but there is no corresponding "Main Fault" for the job fault codes.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault
            Access:MANFPALO.Clearkey(mfp:Field_Key)
            mfp:Manufacturer    = job:Manufacturer
            mfp:Field_Number    = map:Field_Number
            mfp:Field           = func:Field
            If Access:MANFPALO.Tryfetch(mfp:Field_Key) = Level:Benign
                !Found

            Else ! If Access:MANFPALO.Tryfetch(mfp:Field_Key) = Level:Benign
                !Error
                Return Level:Fatal
            End !If Access:MANFPALO.Tryfetch(mfp:Field_Key) = Level:Benign
        End !If map:MainFault
    Else ! If Access:MANFAUPA.Tryfetch(map:Field_Number_Key) = Level:Benign
        !Error
    End !If Access:MANFAUPA.Tryfetch(map:Field_Number_Key) = Level:Benign
    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseStockNotification PROCEDURE (func:JobNumber,func:PartNumber,func:Description,func:Location) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_loc_id          USHORT,AUTO
tmp:JobDetails       STRING(60)
tmp:PartDetails      STRING(60)
PartsQueue           QUEUE,PRE(parque)
Location             STRING(30)
ShelfLocation        STRING(30)
Quantity             LONG
                     END
tmp:Return           BYTE(0)
window               WINDOW('Browse Stock Notification'),AT(,,299,195),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,292,40),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Job Number: '),AT(8,12),USE(?Prompt1),FONT(,10,,)
                           STRING(@s60),AT(64,12),USE(tmp:JobDetails),FONT(,10,,FONT:bold)
                           STRING(@s60),AT(64,28),USE(tmp:PartDetails),FONT(,10,,FONT:bold)
                           PROMPT('Part Details:'),AT(8,28),USE(?Prompt2),FONT(,10,,)
                         END
                       END
                       SHEET,AT(4,48,292,116),USE(?Sheet2),COLOR(0D6E7EFH),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('The selected part is not in stock at the selected location, but is available in ' &|
   'the locations below.'),AT(8,52,284,16),USE(?Prompt3)
                           LIST,AT(8,72,284,84),USE(?List1),VSCROLL,COLOR(COLOR:White),FORMAT('120L(2)|M~Location~@s30@120L(2)|M~Shelf Location~@s30@32L(2)|M~Quantity~@s8@'),FROM(PartsQueue)
                         END
                       END
                       PANEL,AT(4,168,292,24),USE(?ButtonPanel),FILL(0D6E7EFH)
                       BUTTON('Print Report'),AT(8,172,56,16),USE(?PrintReport),LEFT,ICON('print.gif')
                       BUTTON('Cancel Part Request'),AT(224,172,68,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('&Continue'),AT(168,172,56,16),USE(?OK),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseStockNotification')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Access:LOCATION.UseFile
  SELF.FilesOpened = True
  Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number = func:JobNumber
  If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
      tmp:JobDetails = job:Ref_Number & '  -  ' & Clip(job:Manufacturer) & ' ' &Clip(job:Model_Number)
      tmp:PartDetails = Clip(func:PartNumber) & ' ' & Clip(func:Description)
  Else!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
  
  Clear(PartsQueue)
  Free(PartsQueue)
  Save_loc_ID = Access:LOCATION.SaveFile()
  Access:LOCATION.ClearKey(loc:Location_Key)
  Set(loc:Location_Key,loc:Location_Key)
  Loop
      If Access:LOCATION.NEXT()
         Break
      End !If
      If loc:Location = func:Location
          Cycle
      End !If loc:Location = func:Location
      Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Part_Description_Key)
      sto_ali:Location    = loc:Location
      sto_ali:Part_Number = func:PartNumber
      sto_ali:Description = func:Description
      If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
          !Found
          If sto_ali:Quantity_Stock <> 0
              parque:Location = loc:Location
              parque:ShelfLocation    = sto_ali:Shelf_Location
              parque:Quantity = sto_ali:Quantity_Stock
              Add(PartsQueue)
          End !If sto_ali:Quantity_Stock <> ''
      Else!If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
  End !Loop
  Access:LOCATION.RestoreFile(Save_loc_ID)
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
    Relate:STOCK_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PrintReport
      ThisWindow.Update
      StockNotification(func:JobNumber,func:PartNumber,func:Description,func:Location,1)
      ThisWindow.Reset
    OF ?Cancel
      ThisWindow.Update
      tmp:Return = 1
      Post(Event:CloseWindow)
    OF ?OK
      ThisWindow.Update
      tmp:Return = 0
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Pick_Model_Stock PROCEDURE (func:UserCode)            !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
tag_temp             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:One              BYTE(1)
tmp:SkillLevel       LONG
InWarr               DECIMAL(7,2)
OutWarr              DECIMAL(7,2)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Description)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:RecordNumber)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Location)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Purchase_Cost)
                         PROJECT(sto:Sale_Cost)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Suspend)
                         PROJECT(sto:Location)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
InWarr                 LIKE(InWarr)                   !List box control field - type derived from local data
OutWarr                LIKE(OutWarr)                  !List box control field - type derived from local data
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !List box control field - type derived from field
stm:RecordNumber       LIKE(stm:RecordNumber)         !Primary key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Browse key field - type derived from field
stm:Location           LIKE(stm:Location)             !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::stm:Location         LIKE(stm:Location)
HK9::stm:Model_Number     LIKE(stm:Model_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(68,58),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(144,58,124,10),USE(location_temp),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       BUTTON,AT(144,330),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(212,330),USE(?DASTAGAll),TRN,FLAT,HIDE,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(280,330),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       LIST,AT(144,106,392,218),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@124L(2)|M~Description~@s30@124L(2)|M~Part Number~@s30@36R(2)|M~Purch~' &|
   'L@n7.2@36R(2)|M~Sale~L@n7.2@40R(2)|M~In Warr~L@n-10.2@40R(2)|M~Out Warr~L@n-10.2' &|
   '@47R(2)|M~Qty In Stock~L@N8@48L(2)|M~Ref Number~@n012@0L(2)|M~Reference Number~@' &|
   's8@0L(2)|M~Suspend Part~@n1@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(616,330,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(608,354,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(548,154),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(64,76,552,288),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,92,124,10),USE(stm:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Part Number'),USE(?Tab:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,92,124,10),USE(stm:Part_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = stm:Ref_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = stm:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = stm:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::12:QUEUE = glo:Queue
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer = stm:Ref_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = stm:Ref_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020440'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Model_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  error# = 1
  tmp:SkillLevel = 0
  If job:Engineer <> ''
      access:users.clearkey(use:user_code_key)
      use:user_code = job:Engineer
      if access:users.fetch(use:user_code_key) = Level:Benign
          If use:location <> ''
              error# = 0
              location_temp = use:location
          End!If use:location <> ''
          tmp:SkillLevel = use:SkillLevel
      End!if access:users.fetch(use:user_code_key) = Level:Benign
  End!If glo:select11 <> ''
  
  
  !TB12669 - relocate store
  if glo:RelocateStore then
      !relocated this to the main store
      Location_Temp = 'MAIN STORE'        !was 'Main Store'
      error# = 0
      !will use glo:RelocatedFrom and glo:RelocatedMark_Up
  END !if relocated to main store
  
  
  If error# = 1
      Set(defstock)
      Access:defstock.next()
      If dst:site_location <> ''
          location_temp = dst:site_location
      End
  End!If error# = 1
  
  
  Access:LOCATION.ClearKey(loc:ActiveLocationKey)
  loc:Active   = 1
  loc:Location = Location_Temp
  If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Found
  
  Else!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      Location_Temp = ''
  End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
  
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = func:UserCode
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly
          ?Location_Temp{prop:Disable} = 1
      End !If use:StockFromLocationOnly
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?stm:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stm:Location_Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?stm:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BIND('tag_temp',tag_temp)
  BIND('InWarr',InWarr)
  BIND('OutWarr',OutWarr)
  BIND('GLO:Select12',GLO:Select12)
  BIND('location_temp',location_temp)
  BIND('no_temp',no_temp)
  BIND('model_number_temp',model_number_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(InWarr,BRW1.Q.InWarr)
  BRW1.AddField(OutWarr,BRW1.Q.OutWarr)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Suspend,BRW1.Q.sto:Suspend)
  BRW1.AddField(stm:RecordNumber,BRW1.Q.stm:RecordNumber)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:ActiveLocationKey)
  FDCB6.AddRange(loc:Active,tmp:One)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,location_temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Browse:1{PROP:FORMAT} ='11L(2)I@s1@#1#124L(2)|M~Description~@s30@#3#124L(2)|M~Part Number~@s30@#4#40R(2)|M~In Warr~L@n-10.2@#7#40R(2)|M~Out Warr~L@n-10.2@#8#47R(2)|M~Qty In Stock~L@N8@#9#0L(2)|M~Reference Number~@s8@#11#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULT2.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Select:2
      IF Records(Glo:Queue)
        CLEAR(glo:Queue2)
        FREE(glo:Queue2)
        LOOP x# = 1 TO RECORDS(Glo:Queue)
          GET(Glo:Queue,x#)
          access:stomodel.clearkey(stm:model_number_key)
          stm:ref_number   = glo:pointer
          stm:manufacturer = job:manufacturer
          stm:model_number = job:model_number
          if access:stomodel.fetch(stm:model_number_key) = Level:Benign
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
              if access:stock.fetch(sto:ref_number_key) = Level:Benign
                 !Record in Use?
                 IF sto:Quantity_Stock > 0
                   Pointer# = Pointer(Stock)
                   Hold(Stock,1)
                   Get(Stock,Pointer#)
                   If Errorcode() = 43
                      Case Missive('The part ' & Clip(sto:Part_Number) & ' is currently is use by another station.'&|
                        '<13,10>'&|
                        '<13,10>This part will not be added. Please try again later.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                   ELSE
                     !Fine
                     Glo:Queue2.GLO:Pointer2 = Glo:Queue.Pointer
                     Add(glo:Queue2)
                   End !If Errorcode() = 43
                 ELSE
                     !Fine
                     Glo:Queue2.GLO:Pointer2 = Glo:Queue.Pointer
                     Add(glo:Queue2)
                 End
              End !ThisWindow.Request <> Insertrecord
          End
        End
        CLEAR(glo:Queue)
        FREE(glo:Queue)
        LOOP c# = 1 TO RECORDS(glo:Queue2)
          GET(glo:Queue2,c#)
          glo:Queue.GLO:Pointer = glo:Queue2.GLO:Pointer2
          ADD(glo:Queue)
        END
        CLEAR(glo:Queue2)
        FREE(glo:Queue2)
      End
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020440'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020440'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020440'&'0')
      ***
    OF ?location_temp
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      if keycode() = mouseleft2
         post(event:accepted,?DasTag)
         CYCLE
      end
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='11L(2)I@s1@#1#124L(2)|M~Description~@s30@#3#124L(2)|M~Part Number~@s30@#4#40R(2)|M~In Warr~L@n-10.2@#7#40R(2)|M~Out Warr~L@n-10.2@#8#47R(2)|M~Qty In Stock~L@N8@#9#0L(2)|M~Reference Number~@s8@#11#'
          ?Tab2{PROP:TEXT} = 'By Description'
        OF 2
          ?Browse:1{PROP:FORMAT} ='11L(2)I@s1@#1#124L(2)|M~Part Number~@s30@#4#124L(2)|M~Description~@s30@#3#40R(2)|M~In Warr~L@n-10.2@#7#40R(2)|M~Out Warr~L@n-10.2@#8#47R(2)|M~Qty In Stock~L@N8@#9#48L(2)|M~Ref Number~@n012@#10#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?DASTAG)
          Of F6Key
              Post(Event:Accepted,?DASTAGALL)
          Of F7Key
              Post(Event:Accepted,?DASUNTAGALL)
          Of F10Key
              Post(Event:Accepted,?Select:2)
      End !KeyCode()
    OF EVENT:OpenWindow
      ?WindowTitle{prop:text} = 'Available Stock For Model: ' & Clip(glo:select12)
      !Title
      DO DASBRW::12:DASUNTAGALL
      
      Select(?Browse:1)
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  ELSE
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stm:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !And engineer should only see parts that are equal or lower to his skill level
  !THIS MAY SLOW DOWN THE BROWSE TOO MUCH!
  
  If tmp:SkillLevel <> 0
      Case tmp:SkillLevel
          Of 1
              If ~sto:E1
                  Return Record:Filtered
              End !If sto:E1
          Of 2
              If ~(sto:E1 Or sto:E2)
                  Return Record:Filtered
              End !If ~sto:E1 and ~sto:E2
          Of 3
              If ~(sto:E1 Or sto:E2 Or sto:E3)
                  Return Record:Filtered
              End !If ~sto:E1 And ~sto:E2 And ~sto:E3
      End !Case tmp:SkillLevel
  End !tmp:SkillLevel <> 0
  
  !Do not show suspended parts - L913 (DBH: 07-08-2003)
  If sto:Suspend
      Return Record:Filtered
  End !sto:Suspend
  
  
  if glo:RelocateStore then
      inWarr  = sto:Sale_Cost * (1 + InWarrantyMarkup(sto:Manufacturer,glo:RelocatedFrom)/100)
      outWarr = sto:Sale_Cost * (1 + glo:RelocatedMark_Up/100)
  ELSE
      InWarr  = sto:purchase_cost
      OutWarr = sto:Sale_Cost
  END
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stm:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Update_Warranty_Part PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
Unhide_Tab           BYTE
save_orh_id          USHORT,AUTO
save_ori_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
tmp:exclude          STRING(3)
main_part_temp       STRING(3)
fault_codes_required_temp STRING('NO {1}')
Part_Details_Group   GROUP,PRE(TMP)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
                     END
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
tmp:location         STRING(30)
tmp:shelflocation    STRING(30)
tmp:secondlocation   STRING(30)
tmp:CreateNewOrder   BYTE(0)
save_loc_id          USHORT,AUTO
tmp:StockNumber      LONG
tmp:PendingPart      LONG
tmp:NewQuantity      LONG
tmp:CurrentState     LONG
tmp:WebOrderNumber   LONG
tmp:PurchaseCost     REAL
tmp:InWarrantyCost   REAL
tmp:OutWarrantyCost  REAL
tmp:InWarrantyMarkup LONG
tmp:OutWarrantyMarkup LONG
tmp:MaxMarkup        LONG
tmp:Unallocate       BYTE(0)
tmp:OutOfWarrantyAccessory BYTE(0)
tmp:DontOK           BYTE(0)
tmp:ARCPart          BYTE(0)
sav:Supplier         STRING(30)
FieldNumbersQueue    QUEUE,PRE(field)
ScreenOrder          LONG
FieldNumber          LONG
                     END
PromptNumbersQueue   QUEUE,PRE(prompt)
ScreenOrder          LONG
FieldNumber          LONG
                     END
LookupNumbersQueue   QUEUE,PRE(lookup)
ScreenOrder          LONG
FieldNumber          LONG
                     END
DescriptionNumbersQueue QUEUE,PRE(description)
ScreenOrder          LONG
FieldNumber          LONG
                     END
tmp:FaultCode        STRING(30),DIM(12)
FaultCodeQueue       QUEUE,PRE(fault)
FaultNumber          LONG
FaultCode            STRING(30)
                     END
History::wpr:Record  LIKE(wpr:RECORD),STATIC
QuickWindow          WINDOW('Update the WARPARTS File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Insert / Amend Warranty Part'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,280,354),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Warranty Part'),AT(8,33),USE(?title),FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(8,49),USE(?WPR:Part_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(100,49,124,10),USE(wpr:Part_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,SINGLE
                           BUTTON,AT(228,46),USE(?browse_stock_button),SKIP,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Description'),AT(8,60),USE(?WPR:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Despatch Note No'),AT(8,76),USE(?WPR:Dewpatch_Note_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(100,62,124,10),USE(wpr:Description),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,SINGLE
                           CHECK('Main Part'),AT(100,33),USE(wpr:Main_Part),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           TEXT,AT(100,76,124,10),USE(wpr:Despatch_Note_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,SINGLE
                           PROMPT('Location'),AT(8,92),USE(?tmp:location:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(100,92,124,10),USE(tmp:location),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,SINGLE,READONLY
                           PROMPT('Second/Shelf Location'),AT(8,105),USE(?tmp:location:Prompt:2),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(100,105,60,10),USE(tmp:shelflocation),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,SINGLE,READONLY
                           TEXT,AT(164,105,60,10),USE(tmp:secondlocation),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,SINGLE,READONLY
                           PROMPT('Purchase Cost'),AT(8,124),USE(?tmp:PurchaseCost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(100,124,64,10),USE(tmp:PurchaseCost),SKIP,DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Purchase Cost'),TIP('Purchase Cost'),UPR
                           PROMPT('% Markup'),AT(168,132),USE(?Prompt31),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('In Warranty Cost'),AT(8,140),USE(?tmp:InWarrantyCost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(100,140,64,10),USE(tmp:InWarrantyCost),SKIP,DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('In Warranty Cost'),TIP('In Warranty Cost'),UPR
                           SPIN(@s3),AT(168,140,24,10),USE(tmp:InWarrantyMarkup),DISABLE,HIDE,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),RANGE(0,99),STEP(5)
                           PROMPT('Supplier'),AT(8,161),USE(?wpr:Supplier:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(100,161,124,10),USE(wpr:Supplier),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(228,156),USE(?LookupSupplier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Quantity'),AT(8,180),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@p<<<<<<<#p),AT(100,180,64,10),USE(wpr:Quantity),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,RANGE(1,99999999),STEP(1)
                           CHECK('Exclude From Ordering / Decrementing'),AT(100,196),USE(wpr:Exclude_From_Order),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Part Status'),AT(8,220),USE(?Prompt24),TRN,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Date Ordered'),AT(8,233),USE(?WPR:Date_Ordered:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(100,233,64,10),USE(wpr:Date_Ordered),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Order Number'),AT(8,244),USE(?WPR:Order_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n012b),AT(100,244,64,10),USE(wpr:Order_Number),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),READONLY,MSG('Order Number')
                           BUTTON,AT(216,246),USE(?UnallocatePart),TRN,FLAT,HIDE,LEFT,ICON('unalparp.jpg')
                           PROMPT('Out Fault Code'),AT(8,273),USE(?OutFaultCode:Prompt),HIDE,FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Date Received'),AT(8,257),USE(?WPR:Date_Received:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(100,257,64,10),USE(wpr:Date_Received),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           CHECK('Part Allocated'),AT(100,212),USE(wpr:PartAllocated),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Part Allocated'),TIP('Part Allocated'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(288,28,384,354),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Required Fault Codes'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Fault Codes Checked'),AT(396,34),USE(wpr:Fault_Codes_Checked),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Prompt 32'),AT(292,48,100,19),USE(?FaultCode:Prompt:1),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,72,116,18),USE(?FaultCode:Description:2),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(524,44),USE(?Button:Lookup:1),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 33'),AT(552,49,116,18),USE(?FaultCode:Description:1),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,72,124,10),USE(tmp:FaultCode[2]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(396,48,124,10),USE(tmp:FaultCode[1]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Prompt 32'),AT(292,72,100,19),USE(?FaultCode:Prompt:2),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(524,68),USE(?Button:Lookup:2),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(524,94),USE(?Button:Lookup:3),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 32'),AT(292,96,100,19),USE(?FaultCode:Prompt:3),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,96,116,18),USE(?FaultCode:Description:3),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,96,124,10),USE(tmp:FaultCode[3]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,116),USE(?Button:Lookup:4),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 32'),AT(292,168,100,19),USE(?FaultCode:Prompt:6),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 32'),AT(292,144,100,19),USE(?FaultCode:Prompt:5),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 32'),AT(292,120,100,19),USE(?FaultCode:Prompt:4),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,120,116,18),USE(?FaultCode:Description:4),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,120,124,10),USE(tmp:FaultCode[4]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,140),USE(?Button:Lookup:5),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(524,164),USE(?Button:Lookup:6),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 33'),AT(552,144,116,18),USE(?FaultCode:Description:5),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 32'),AT(292,192,100,19),USE(?FaultCode:Prompt:7),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,168,116,18),USE(?FaultCode:Description:6),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,168,124,10),USE(tmp:FaultCode[6]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(396,144,124,10),USE(tmp:FaultCode[5]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,188),USE(?Button:Lookup:7),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(524,214),USE(?Button:Lookup:8),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 32'),AT(292,216,100,19),USE(?FaultCode:Prompt:8),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,192,116,18),USE(?FaultCode:Description:7),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,218,124,10),USE(tmp:FaultCode[8]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(396,192,124,10),USE(tmp:FaultCode[7]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,238),USE(?Button:Lookup:9),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 32'),AT(292,240,100,19),USE(?FaultCode:Prompt:9),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,264,116,18),USE(?FaultCode:Description:10),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,264,124,10),USE(tmp:FaultCode[10]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Prompt 33'),AT(552,216,116,18),USE(?FaultCode:Description:8),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,242,124,10),USE(tmp:FaultCode[9]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Prompt 33'),AT(552,240,116,18),USE(?FaultCode:Description:9),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 32'),AT(292,264,100,19),USE(?FaultCode:Prompt:10),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(524,260),USE(?Button:Lookup:10),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 32'),AT(292,290,100,19),USE(?FaultCode:Prompt:11),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 33'),AT(552,290,116,18),USE(?FaultCode:Description:11),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,290,124,10),USE(tmp:FaultCode[11]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,286),USE(?Button:Lookup:11),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Prompt 32'),AT(292,314,100,19),USE(?FaultCode:Prompt:12),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,314,124,10),USE(tmp:FaultCode[12]),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Prompt 33'),AT(552,314,116,18),USE(?FaultCode:Description:12),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(524,310),USE(?Button:Lookup:12),FLAT,HIDE,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(540,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       STRING(@s40),AT(8,398),USE(GLO:Select11),HIDE,COLOR(0D6E7EFH)
                       STRING(@s40),AT(8,388),USE(GLO:Select12),HIDE,COLOR(0D6E7EFH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_map_id   ushort,auto
save_war_ali_id     ushort,auto
!Save Entry Fields Incase Of Lookup
look:wpr:Supplier                Like(wpr:Supplier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
Local           Class
LookupFaultCode         Procedure(Long func:FieldNumber, String f:FaultCode),String
LookupFaultCodeField    Procedure(Long func:FieldNumber,String func:Field),Byte
AccessoryOutOfWarranty  Procedure(),Byte
GetJobFaultCode         Procedure(Long f:FaultCodeNumber),String
                End

PassLinkedFaultCodeQueue    Queue(DefLinkedFaultCodeQueue)
                            End

  CODE
  GlobalResponse = ThisWindow.Run()

BuildNumbersQueue        Routine
    Free(FieldNumbersQueue)
    Free(PromptNumbersQueue)
    Free(LookupNumbersQueue)
    Free(DescriptionNumbersQueue)
    Free(FaultCodeQueue)

    fault:FaultNumber   = 1
    fault:FaultCode     = wpr:Fault_Code1
    Add(FaultCodeQueue)
    fault:FaultNumber   = 2
    fault:FaultCode     = wpr:Fault_Code2
    Add(FaultCodeQueue)
    fault:FaultNumber   = 3
    fault:FaultCode     = wpr:Fault_Code3
    Add(FaultCodeQueue)
    fault:FaultNumber   = 4
    fault:FaultCode     = wpr:Fault_Code4
    Add(FaultCodeQueue)
    fault:FaultNumber   = 5
    fault:FaultCode     = wpr:Fault_Code5
    Add(FaultCodeQueue)
    fault:FaultNumber   = 6
    fault:FaultCode     = wpr:Fault_Code6
    Add(FaultCodeQueue)
    fault:FaultNumber   = 7
    fault:FaultCode     = wpr:Fault_Code7
    Add(FaultCodeQueue)
    fault:FaultNumber   = 8
    fault:FaultCode     = wpr:Fault_Code8
    Add(FaultCodeQueue)
    fault:FaultNumber   = 9
    fault:FaultCode     = wpr:Fault_Code9
    Add(FaultCodeQueue)
    fault:FaultNumber   = 10
    fault:FaultCode     = wpr:Fault_Code10
    Add(FaultCodeQueue)
    fault:FaultNumber   = 11
    fault:FaultCode     = wpr:Fault_Code11
    Add(FaultCodeQueue)
    fault:FaultNumber   = 12
    fault:FaultCode     = wpr:Fault_Code12
    Add(FaultCodeQueue)

    field:ScreenOrder = 1
    field:FieldNumber = ?tmp:FaultCode_1
    Add(FieldNumbersQueue)
    field:ScreenOrder = 2
    field:FieldNumber = ?tmp:FaultCode_2
    Add(FieldNumbersQueue)
    field:ScreenOrder = 3
    field:FieldNumber = ?tmp:FaultCode_3
    Add(FieldNumbersQueue)
    field:ScreenOrder = 4
    field:FieldNumber = ?tmp:FaultCode_4
    Add(FieldNumbersQueue)
    field:ScreenOrder = 5
    field:FieldNumber = ?tmp:FaultCode_5
    Add(FieldNumbersQueue)
    field:ScreenOrder = 6
    field:FieldNumber = ?tmp:FaultCode_6
    Add(FieldNumbersQueue)
    field:ScreenOrder = 7
    field:FieldNumber = ?tmp:FaultCode_7
    Add(FieldNumbersQueue)
    field:ScreenOrder = 8
    field:FieldNumber = ?tmp:FaultCode_8
    Add(FieldNumbersQueue)
    field:ScreenOrder = 9
    field:FieldNumber = ?tmp:FaultCode_9
    Add(FieldNumbersQueue)
    field:ScreenOrder = 10
    field:FieldNumber = ?tmp:FaultCode_10
    Add(FieldNumbersQueue)
    field:ScreenOrder = 11
    field:FieldNumber = ?tmp:FaultCode_11
    Add(FieldNumbersQueue)
    field:ScreenOrder = 12
    field:FieldNumber = ?tmp:FaultCode_12
    Add(FieldNumbersQueue)

    lookup:ScreenOrder = 1
    lookup:FieldNumber = ?Button:Lookup:1
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 2
    lookup:FieldNumber = ?Button:Lookup:2
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 3
    lookup:FieldNumber = ?Button:Lookup:3
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 4
    lookup:FieldNumber = ?Button:Lookup:4
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 5
    lookup:FieldNumber = ?Button:Lookup:5
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 6
    lookup:FieldNumber = ?Button:Lookup:6
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 7
    lookup:FieldNumber = ?Button:Lookup:7
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 8
    lookup:FieldNumber = ?Button:Lookup:8
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 9
    lookup:FieldNumber = ?Button:Lookup:9
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 10
    lookup:FieldNumber = ?Button:Lookup:10
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 11
    lookup:FieldNumber = ?Button:Lookup:11
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 12
    lookup:FieldNumber = ?Button:Lookup:12
    Add(LookupNumbersQueue)

    prompt:ScreenOrder = 1
    prompt:FieldNumber = ?FaultCode:Prompt:1
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 2
    prompt:FieldNumber = ?FaultCode:Prompt:2
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 3
    prompt:FieldNumber = ?FaultCode:Prompt:3
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 4
    prompt:FieldNumber = ?FaultCode:Prompt:4
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 5
    prompt:FieldNumber = ?FaultCode:Prompt:5
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 6
    prompt:FieldNumber = ?FaultCode:Prompt:6
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 7
    prompt:FieldNumber = ?FaultCode:Prompt:7
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 8
    prompt:FieldNumber = ?FaultCode:Prompt:8
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 9
    prompt:FieldNumber = ?FaultCode:Prompt:9
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 10
    prompt:FieldNumber = ?FaultCode:Prompt:10
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 11
    prompt:FieldNumber = ?FaultCode:Prompt:11
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 12
    prompt:FieldNumber = ?FaultCode:Prompt:12
    Add(PromptNumbersQueue)

    description:ScreenOrder = 1
    description:FieldNumber = ?FaultCode:Description:1
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 2
    description:FieldNumber = ?FaultCode:Description:2
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 3
    description:FieldNumber = ?FaultCode:Description:3
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 4
    description:FieldNumber = ?FaultCode:Description:4
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 5
    description:FieldNumber = ?FaultCode:Description:5
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 6
    description:FieldNumber = ?FaultCode:Description:6
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 7
    description:FieldNumber = ?FaultCode:Description:7
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 8
    description:FieldNumber = ?FaultCode:Description:8
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 9
    description:FieldNumber = ?FaultCode:Description:9
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 10
    description:FieldNumber = ?FaultCode:Description:10
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 11
    description:FieldNumber = ?FaultCode:Description:11
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 12
    description:FieldNumber = ?FaultCode:Description:12
    Add(DescriptionNumbersQueue)

    Loop x# = 1 To 12
        Get(PromptNumbersQueue,x#)
        prompt:FieldNumber{prop:Text} = ''
        Get(LookupNumbersQueue,x#)
        lookup:FieldNumber{prop:Hide} = 1
        lookup:FieldNumber{prop:Disable} = 0
        Get(DescriptionNumbersQueue,x#)
        description:FieldNumber{prop:Text} = ''
        Get(FieldNumbersQueue,x#)
        field:FieldNumber{prop:Hide} = 1
        field:FieldNumber{prop:ReadOnly} = 0

    End ! Loop x# = 1 To 20

CreateNewOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number  = tmp:StockNumber
        ope:Job_Number       = job:Ref_Number
        ope:Part_Type        = 'JOB'
        ope:Supplier         = wpr:Supplier
        ope:Part_Number      = wpr:Part_Number
        ope:Description      = wpr:Description
        ope:Quantity         = tmp:NewQuantity
        ope:Account_Number   = job:Account_Number
        ope:PartRecordNumber = wpr:Record_Number

        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
CreateWebOrder      Routine
    !Find if there is a pending order already for this job
    tmp:WebOrderNumber = 0
    Save_orh_ID = Access:ORDHEAD.SaveFile()
    Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
    orh:procesed    = 0
    Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
    Loop
        If Access:ORDHEAD.NEXT()
           Break
        End !If
        If orh:procesed    <> 0      |
            Then Break.  ! End If
        If orh:Account_No = job:Account_Number
            tmp:WebOrderNumber = orh:Order_no
            Break
        End !If orh:Account_No = job:Account_Number
    End !Loop
    Access:ORDHEAD.RestoreFile(Save_orh_ID)

    If tmp:WebOrderNumber
        !Found an existing order. Add to it
        !Has this part already been ordered?
        Found# = 0
        Save_ori_ID = Access:ORDITEMS.SaveFile()
        Access:ORDITEMS.ClearKey(ori:Keyordhno)
        ori:ordhno = tmp:WebOrderNumber
        Set(ori:Keyordhno,ori:Keyordhno)
        Loop
            If Access:ORDITEMS.NEXT()
               Break
            End !If
            If ori:ordhno <> tmp:WebOrderNumber      |
                Then Break.  ! End If
            If ori:partno = wpr:Part_Number And |
                ori:partdiscription = wpr:Description
                ori:qty += wpr:Quantity
                Access:ORDITEMS.Update()
                Found# = 1
                Break
            End !ori:partdiscription = wpr:Description
        End !Loop
        Access:ORDITEMS.RestoreFile(Save_ori_ID)

        If Found# = 0
            !Make a new part line
            If Access:ORDITEMS.PrimeRecord() = Level:Benign
                ori:ordhno          = tmp:WebOrderNumber
                ori:manufact        = job:Manufacturer
                ori:partno          = wpr:Part_Number
                ori:partdiscription = wpr:Description
                ori:qty             = wpr:Quantity
                ori:itemcost        = wpr:Sale_Cost
                ori:totalcost       = wpr:Sale_Cost * wpr:Quantity
                If Access:ORDITEMS.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:ORDITEMS.TryInsert() = Level:Benign
            End !If Access:ORDITEMS.PrimeRecord() = Level:Benign

        End !If Found# = 0
    Else !tmp:WebOrderNumber
        !Cannot find order. Make a new one
        If Access:ORDHEAD.PrimeRecord() = Level:Benign
            orh:account_no      = job:Account_Number
            orh:CustName        = job:Company_Name
            orh:CustAdd1        = job:Address_Line1
            orh:CustAdd2        = job:Address_Line2
            orh:CustAdd3        = job:Address_Line3
            orh:CustPostCode    = job:Postcode
            orh:CustTel         = job:Telephone_Number
            orh:CustFax         = job:Fax_Number
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                orh:dName           = sub:Company_Name
                orh:dAdd1           = sub:Address_Line1
                orh:dAdd2           = sub:Address_Line2
                orh:dAdd3           = sub:Address_Line3
                orh:dPostCode       = sub:Postcode
                orh:dTel            = sub:Telephone_Number
                orh:dFax            = sub:Fax_Number

            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

            orh:thedate         = Today()
            orh:thetime         = Clock()
            orh:procesed        = 0
            orh:WhoBooked       = job:Who_Booked
            orh:Department      = ''
            orh:CustOrderNumber = job:Order_Number
            If Access:ORDHEAD.TryInsert() = Level:Benign
                !Insert Successful
                !Make a new part line
                If Access:ORDITEMS.PrimeRecord() = Level:Benign
                    ori:ordhno          = orh:Order_No
                    ori:manufact        = job:Manufacturer
                    ori:partno          = wpr:Part_Number
                    ori:partdiscription = wpr:Description
                    ori:qty             = wpr:Quantity
                    ori:itemcost        = wpr:Sale_Cost
                    ori:totalcost       = wpr:Sale_Cost * wpr:Quantity
                    If Access:ORDITEMS.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:ORDITEMS.TryInsert() = Level:Benign
                End !If Access:ORDITEMS.PrimeRecord() = Level:Benign

            Else !If Access:ORDHEAD.TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:ORDHEAD.TryInsert() = Level:Benign
        End !If Access:ORDHEAD.PrimeRecord() = Level:Benign
    End !tmp:WebOrderNumber
    !message('Setting weborder to 1')
    wpr:WebOrder = 1
EnableDisableCosts      Routine


!TB13351 - In the case of a warranty job the user must not be able to change any of the costs or percentage markup as warranty pricing for parts cannot be changed.
!J - 11/08/14 - Percentage markup is now hidden - nothing else may be changed
!    If tmp:InWarrantyMarkup = 0
!        Global.ReadOnly(?tmp:InWarrantyCost,0)
!    Else !If tmp:InWarrantyMarkup = 0
!        Global.ReadOnly(?tmp:InWarrantyCost,1)
!    End !If tmp:InWarrantyMarkup <> 0
!
!    If ThisWindow.Request = InsertRecord
!        If tmp:InWarrantyMarkup <> 0
!            tmp:InWarrantyCost  = Markups(tmp:InWarrantyCost,tmp:PurchaseCost,tmp:InWarrantyMarkup)
!        End !If tmp:InWarrantyMarkup <> 0
!    End !If ThisWindow.Request = InsertRecord
!
!    ! Do not allow to change the costs of a "Correction" part - TrkBs: 5365 (DBH: 27-04-2005)
!    If wpr:Correction
!        Global.ReadOnly(?tmp:InWarrantyCost,1)
!        Global.ReadOnly(?tmp:InWarrantyMarkup,1)
!    End ! If wpr:Correction
!TB13351 END

    !Do not allow to change the quantity of a completed job - L945 (DBH: 03-09-2003)
    If job:Date_Completed <> ''
        ?wpr:Quantity{prop:Disable} = 1
    End !If job:Date_Completed <> ''


    Display()
FaultCode        Routine
Data
local:Field        String(255)
local:Required     Byte(0)
local:MainFaultOnly    Byte(0)
local:FoundFault   Byte(0)
local:SoftwareUpgrade Byte(0)
Code

    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type = job:Warranty_Charge_Type
    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
        If cha:Force_Warranty = 'YES'
            local:Required = 1
        End ! If cha:Force_Warranty = 'YES'
    End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:BenignI

    ! If "Force Accessory Fault Codes Only" ticked on Manufacturer, only show Main Fault on non accessories.
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        If sto:Accessory <> 'YES' And man:ForceAccessoryCode
            local:MainFaultOnly = 1
        End ! If sto:Accessory <> 'YES' And man:ForceAcessoryCode
    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    ! Inserting (DBH 30/04/2008) ' 9723 - Has this part been marked as a "Software Upgrade"
    local:SoftwareUpgrade = 0
    Loop x# = 1 To 12
        field:ScreenOrder = x#
        Get(FieldNumbersQueue,field:ScreenOrder)
        If field:FieldNumber{prop:Hide} = 0
            If tmp:FaultCode[x#] = 'SW'
                local:SoftwareUpgrade = 1
                Break
            End ! If tmp:FaultCode[x#] = 'SW'
        End ! If field:FieldNumber{prop:Hide} = 0
    End ! Loop x# = 1 To 12
    ! End (DBH 30/04/2008) '9723

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = job:Manufacturer
    map:ScreenOrder = 0
    Set(map:ScreenOrderKey,map:ScreenOrderKey)
    Loop
        If Access:MANFAUPA.Next()
            Break
        End ! If Access:MANFAULT.Next()
        If map:Manufacturer <> job:Manufacturer
            Break
        End ! If map:Manufacturer <> f:Manfacturer
        If map:ScreenOrder = 0
            Cycle
        End ! If map:ScreenOrder = 0

        If local:MainFaultOnly
            If map:MainFault = 0
                Cycle
            End ! If map:MainFault = 0
        End ! If local:MainFaultOnly

        prompt:ScreenOrder = map:ScreenOrder
        Get(PromptNumbersQueue,prompt:ScreenOrder)
        field:ScreenOrder = map:ScreenOrder
        Get(FieldNumbersQueue,field:ScreenOrder)
        lookup:ScreenOrder = map:ScreenOrder
        Get(LookupNumbersQueue,lookup:ScreenOrder)
        description:ScreenOrder = map:ScreenOrder
        Get(DescriptionNumbersQueue,lookup:ScreenOrder)

        field:FieldNumber{prop:Hide} = 0
        prompt:FieldNumber{prop:Text} = map:Field_Name

        If local:Required And map:Compulsory = 'YES'
            If wpr:Adjustment = 'YES'
                If map:CompulsoryForAdjustment
                    field:FieldNumber{prop:Req} = 1
                End ! If map:CompulsoryForAdjustment
            Else ! If wpr:Adjustment = 'YES'
                field:FieldNumber{prop:Req} = 1
            End !If wpr:Adjustment = 'YES'

        Else ! If local:Required
            field:FieldNumber{prop:Req} = 0
        End ! If local:Required

        local:FoundFault = 1

        If map:MainFault
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                Case maf:Field_Type
                Of 'DATE'
                    field:FieldNumber{prop:Text} = Clip(maf:DateType)
                    If ~glo:WebJob
                        ! Show Lookup Calendar
                        lookup:FieldNumber{prop:Hide} = 0
                    End ! If ~glo:WebJob
                Of 'STRING'
                    If maf:RestrictLength
                        field:FieldNumber{prop:Text} = '@s' & maf:LengthTo
                    Else ! If map:RestrictLength
                        field:FieldNumber{prop:Text} = '@s30'
                    End ! If map:RestrictLength

                    If maf:Lookup = 'YES'
                        lookup:FieldNumber{prop:Hide} = 0
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode[map:ScreenOrder]
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            description:FieldNumber{prop:Text} = mfo:Description
                        Else ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            description:FieldNumber{prop:Text} = ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                        If maf:Force_Lookup = 'YES'
                            field:FieldNumber{prop:ReadOnly} = 1
                        Else ! If maf:Force_Lookup = 'YES'
                            field:FieldNumber{prop:ReadOnly} = 0
                        End ! If maf:Force_Lookup = 'YES'
                    Else ! If map:Lookup = 'YES'
                        description:FieldNumber{prop:Text} = ''
                        lookup:FieldNumber{prop:Hide} = 1
                    End ! If map:Lookup = 'YES'
                Of 'NUMBER'
                    If maf:RestrictLength
                        field:FieldNumber{prop:Text} = '@n_' & maf:LengthTo
                    Else ! If map:RestrictLength
                        field:FieldNumber{prop:Text} = '@n_9'
                    End ! If map:RestrictLength
                    If maf:Lookup
                        lookup:FieldNumber{prop:Hide} = 0
                    Else ! If map:Lookup
                        lookup:FieldNumber{prop:Hide} = 1
                    End ! If map:Lookup
                End ! Case map:Field_Type
            End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

        Else ! If map:MainFault
            Case map:Field_Type
            Of 'DATE'
                field:FieldNumber{prop:Text} = Clip(map:DateType)
                If ~glo:WebJob
                    ! Show Lookup Calendar
                    lookup:FieldNumber{prop:Hide} = 0
                End ! If ~glo:WebJob
            Of 'STRING'
                If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@s' & map:LengthTo
                Else ! If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@s30'
                End ! If map:RestrictLength

                If map:Lookup = 'YES'
                    lookup:FieldNumber{prop:Hide} = 0
                    Access:MANFPALO.Clearkey(mfp:Field_Key)
                    mfp:Manufacturer = job:Manufacturer
                    mfp:Field_Number = map:Field_Number
                    mfp:Field        = tmp:FaultCode[map:ScreenOrder]
                    If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                        description:FieldNumber{prop:Text} = mfp:Description
                    Else ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        description:FieldNumber{prop:Text} = ''
                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    If map:Force_Lookup = 'YES'
                        field:FieldNumber{prop:ReadOnly} = 1
                    Else ! If maf:Force_Lookup = 'YES'
                        field:FieldNumber{prop:ReadOnly} = 0
                    End ! If maf:Force_Lookup = 'YES'

                Else ! If map:Lookup = 'YES'
                    description:FieldNumber{prop:Text} = ''
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If map:Lookup = 'YES'
            Of 'NUMBER'
                If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@n_' & map:LengthTo
                Else ! If map:RestrictLength
                    field:FieldNumber{prop:Text} = '@n_9'
                End ! If map:RestrictLength
                If map:Lookup
                    lookup:FieldNumber{prop:Hide} = 0
                Else ! If map:Lookup
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If map:Lookup
            End ! Case map:Field_Type

        End ! If map:MainFault

        If map:NotAvailable = 1
            If tmp:FaultCode[map:ScreenOrder] = ''
                field:FieldNumber{prop:Hide} = 1
                prompt:FieldNumber{prop:Text} = ''
                description:FieldNumber{prop:Text} = ''
            Else ! If tmp:FaultCode[map:ScreenOrder] = ''
                field:FieldNumber{prop:ReadOnly} = 1
            End ! If tmp:FaultCode[map:ScreenOrder] = ''
            lookup:FieldNumber{prop:Hide} = 1
!        Else ! If map:NotAvailable = 1
!            field:FieldNumber{prop:ReadOnly} = 0
        End ! If map:NotAvailable = 1

        If map:Lookup = 'YES'
            ! Autofill fault code if only one entry in lookup
            local:Field = ''
            Count# = 0

            If ~map:MainFault
                ! There should never be only 1 out fault, so no need to check
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer = job:Manufacturer
                mfp:Field_Number = map:Field_Number
                Set(mfp:Field_Key,mfp:Field_Key)
                Loop
                    If Access:MANFPALO.Next()
                        Break
                    End ! If Access:MANFAULO.Next()
                    If mfp:Manufacturer <> job:Manufacturer
                        Break
                    End ! If mfo:Manufacturer <> f:Man
                    If mfp:Field_Number <> map:Field_Number
                        Break
                    End ! If mfo:Field_Number <> map:Field_Number
                    Count# += 1
                    If Count# > 1
                        local:Field = ''
                        Break
                    End ! If Count# > 1
                    local:Field = mfp:Field
                End ! Loop
            End  ! If ~map:MainFault
            If local:Field <> ''
                If tmp:FaultCode[map:ScreenOrder] = '' And field:FieldNumber{prop:Hide} = 0 And field:FieldNumber{prop:ReadOnly} = 0
                    tmp:FaultCode[map:ScreenOrder] = local:Field
                End ! If tmp:FaultCode[map:ScreenOrder] = '' And field:FieldNumber{prop:Hide} = 0 And field:FieldNumber{prop:Disable} = 0
            End ! If local:Field <> ''
        End ! If map:Lookup = 'YES'

        If map:NAForAccessory And sto:Accessory = 'YES' And tmp:FaultCode[map:ScreenOrder] = ''
            tmp:FaultCode[map:ScreenOrder] = 'N/A'
        End ! If map:NAForAccessory And sto:Accessory = 'YES' And Contents(field:FieldNumber) = ''

        ! Inserting (DBH 30/04/2008) # 9723 - Force to N/A if Software Upgrade
        If map:NAForSW And local:SoftwareUpgrade And tmp:FaultCode[map:ScreenOrder] = ''
            tmp:FaultCode[map:ScreenOrder] = 'N/A'
        End ! If map:NAForSW And local:SoftwareUpgrade And tmp:FaultCode[map:ScreenOrder] = ''
        ! End (DBH 30/04/2008) #9723

        If map:CopyFromJobFaultCode And tmp:FaultCode[map:ScreenOrder] = ''
            tmp:FaultCode[map:ScreenOrder] = local.GetJobFaultCode(map:CopyJobFaultCode)
        End ! If map:CopyFromJobFaultCode And Contents(field:FieldNumber) = ''

        ! Check if fault codes have been assigned against the stock item (DBH: 29/10/2007)
        If tmp:FaultCode[map:ScreenOrder] = '' And sto:Assign_Fault_Codes = 'YES'
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number = sto:Ref_Number
            stm:Manufacturer = job:Manufacturer
            stm:Model_Number = job:Model_Number
            If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode1
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode2
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode3
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode4
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode5
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode6
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode7
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode8
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode9
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode10
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode11
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        tmp:FaultCode[map:ScreenOrder] = stm:FaultCode12
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
        End ! If tmp:FaultCode[map:ScreenOrder] = ''
    End ! Loop

    ! Check if a part main fault is set to assign a value to another fault code (DBH: 25/10/2007)
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer = job:Manufacturer
        mfp:Field_Number = map:Field_Number
        mfp:Field = tmp:FaultCode[map:ScreenOrder]
        If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            If mfp:SetPartFaultCode
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer = job:Manufacturer
                map:Field_Number = mfp:SelectPartFaultCode
                If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                    fault:FaultNumber = map:Field_Number
                    Get(FaultCodeQueue,fault:FaultNumber)
                    If ~Error()
                        If tmp:FaultCode[map:ScreenOrder] = ''
                            tmp:FaultCode[map:ScreenOrder] = mfp:PartFaultCodeValue
                        End ! If tmp:FaultCode[field:ScreenOrder] <> ''
                    End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
            End ! If mfp:SetPartFaultCode
        End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Bryan.CompFieldColour()
    Display()

    If local:FoundFault
        ?wpr:Fault_Codes_Checked{prop:Hide} = 0
    End ! If local:FoundFault
GetMarkupCosts      Routine
    If ThisWindow.Request = InsertRecord
    !Get the default markups
        !Get the default markups
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = sto:Location
        If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            !Found
            !wpr:OutWarrantyMarkUp   = loc:OutWarrantyMarkUp
        Else ! If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
            !Error
        End !If Access:LOCATION.Tryfetch(loc:Location) = Level:Benign
        !wpr:InWarrantyMarkup        = InWarrantyMarkup(job:Manufacturer,loc:Location)


        If glo:WebJob
            !This parts is being added by the RRC.
            wpr:RRCAveragePurchaseCost  = sto:AveragePurchaseCost
            wpr:RRCPurchaseCost         = sto:Purchase_Cost !Markup(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,wpr:InWarrantyMarkup)
            wpr:RRCSaleCost             = sto:Sale_Cost     !Markup(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,wpr:OutWarrantyMarkup)
            wpr:RRCInWarrantyMarkup     = wpr:InWarrantyMarkup
            wpr:RRCOutWarrantyMarkup    = wpr:OutWarrantyMarkup

            !Fill in purchase and sale cost, hopefull this should filter through
            !to every else these fields are used, and not break the program
            wpr:Purchase_Cost           = wpr:RRCPurchaseCost
            wpr:Sale_Cost               = wpr:RRCSaleCost
            wpr:AveragePurchaseCost  = wpr:RRCAveragePurchaseCost
        Else !If glo:WebJob
            !This part is being added by the ARC, but need to check
            !if it was originally booked in by the RRC
            if glo:RelocateStore then
                wpr:AveragePurchaseCost = sto:Sale_Cost
                wpr:Purchase_Cost       = sto:Sale_Cost
                wpr:Sale_Cost           = sto:Sale_Cost*(1+glo:RelocatedMark_Up/100)
                wpr:InWarrantyMarkup    = InWarrantyMarkup(job:Manufacturer,glo:RelocatedFrom)
                wpr:OutWarrantyMarkUp   = glo:RelocatedMark_Up      !sto:Percentage_Mark_Up    !loc:OutWarrantyMarkUp
            ELSE

                wpr:AveragePurchaseCost = sto:AveragePurchaseCost!sto:Purchase_Cost
                wpr:Purchase_Cost       = sto:Purchase_Cost!Markup(wpr:Purchase_Cost,wpr:AveragePurchaseCost,wpr:InWarrantyMarkup)
                wpr:Sale_Cost           = sto:Sale_Cost
                wpr:InWarrantyMarkup    = sto:PurchaseMarkup! InWarrantyMarkup(job:Manufacturer,loc:Location)
                wpr:OutWarrantyMarkUp   = sto:Percentage_Mark_Up!loc:OutWarrantyMarkUp

            END
            !These are blank, unless the part was origially
            !booked by an RRC
            wpr:RRCPurchaseCost     = 0
            wpr:RRCSaleCost         = 0

            !Was this job booked by an RRC?
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:WebJob
                    wpr:RRCAveragePurchaseCost  = wpr:Sale_Cost
                    wpr:RRCPurchaseCost         = Markups(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,InWarrantyMarkup(job:Manufacturer,sto:Location))
                    wpr:RRCSaleCost             = Markups(wpr:RRCAveragePurchaseCost,wpr:RRCAveragePurchaseCost,loc:OutWarrantyMarkup)
                    wpr:RRCInWarrantyMarkup     = wpr:InWarrantyMarkup
                    wpr:RRCOutWarrantyMarkup    = wpr:OutWarrantyMarkup
                End !If jobe:WebJob
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        End !If glo:WebJob
        ! Do not price "Correction" parts - TrkBs: 5365 (DBH: 27-04-2005)
        Do ResetCorrectionCosts
    End !If ThisWindow.Request <> InsertRecord
Lookup_Location     Routine
    If wpr:part_ref_number <> ''
        access:stock.clearkey(sto:ref_number_key)
        sto:ref_number  = wpr:part_ref_number
        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location        = sto:location
            tmp:shelflocation   = sto:shelf_location
            tmp:secondlocation  = sto:second_location
        Else!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = ''
            tmp:shelflocation   = ''
            tmp:secondlocation  = ''
        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
        Access:MANUFACT.Clearkey(man:Manufacturer_Key)
        man:Manufacturer    = job:Manufacturer
        If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            !Found

        Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            !Error
        End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
! Deleting (DBH 18/10/2007) # 9278 - This should be covered by the new fault code routine
!        If (sto:Accessory <> 'YES' and man:ForceAccessoryCode) And Unhide_tab = FALSE
!            ?Tab3{prop:Hide} = 1
!            ?Tab4{prop:Hide} = 0
!        Else !If sto:Accessory <> 'YES' and man:ForceAccessoryCode
!            ?Tab3{prop:Hide} = 0
!            ?Tab4{prop:Hide} = 1
!        End !If sto:Accessory = 'YES' and man:ForceAccessoryCode
! End (DBH 18/10/2007) #9278
    Else!If par:part_ref_number <> ''
        tmp:location    = ''
        tmp:shelflocation   = ''
        tmp:secondlocation  = ''
    End!If par:part_ref_number <> ''

    Display()
LookupMainFault     Routine

    ?OutFaultCode:Prompt{prop:Hide} = 1
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Found
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            Case map:Field_Number
                Of 1
                    mfo:Field        = wpr:Fault_Code1
                Of 2
                    mfo:Field        = wpr:Fault_Code2
                Of 3
                    mfo:Field        = wpr:Fault_Code3
                Of 4
                    mfo:Field        = wpr:Fault_Code4
                Of 5
                    mfo:Field        = wpr:Fault_Code5
                Of 6
                    mfo:Field        = wpr:Fault_Code6
                Of 7
                    mfo:Field        = wpr:Fault_Code7
                Of 8
                    mfo:Field        = wpr:Fault_Code8
                Of 9
                    mfo:Field        = wpr:Fault_Code9
                Of 10
                    mfo:Field        = wpr:Fault_Code10
                Of 11
                    mfo:Field        = wpr:Fault_Code11
                Of 12
                    mfo:Field        = wpr:Fault_Code12
            End !Case map:Field_Number
            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Found
                !Does another fault code need forcing?
!                Case mfo:ForcePartCode
!                    Of 1
!                        ?wpr:Fault_Code1{prop:Req} = 1
!                    Of 2
!                        ?wpr:Fault_Code2{prop:Req} = 1
!                    Of 3
!                        ?wpr:Fault_Code3{prop:Req} = 1
!                    Of 4
!                        ?wpr:Fault_Code4{prop:Req} = 1
!                    Of 5
!                        ?wpr:Fault_Code5{prop:Req} = 1
!                    Of 6
!                        ?wpr:Fault_Code6{prop:Req} = 1
!                    Of 7
!                        ?wpr:Fault_Code7{prop:Req} = 1
!                    Of 8
!                        ?wpr:Fault_Code8{prop:Req} = 1
!                    Of 9
!                        ?wpr:Fault_Code9{prop:Req} = 1
!                    Of 10
!                        ?wpr:Fault_Code10{prop:Req} = 1
!                    Of 11
!                        ?wpr:Fault_Code11{prop:Req} = 1
!                    Of 12
!                        ?wpr:Fault_Code12{prop:Req} = 1
!                End !Case mfo:ForcePartCode

                ?OutFaultCode:Prompt{prop:Hide} = 0
                ?OutFaultCode:Prompt{prop:Text} = 'Out Fault Code: ' & Clip(mfo:Description)
            Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    Bryan.CompFieldColour()
NonOrderPart    Routine
    !Create entry for the quantity from stock
    If Access:PARTS.PrimeRecord() = Level:Benign
        RecordNumber#   = par:Record_Number
        par:Record     :=: wpr:Record
        par:Record_Number = RecordNumber#
        par:Quantity = sto:Quantity_Stock
        par:Date_Ordered = Today()
        If Access:PARTS.TryInsert() = Level:Benign
            !Insert Successful
            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)
            sto:Quantity_Stock = 0
            If Access:STOCK.Update() = Level:Benign
                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'DEC', | ! Transaction_Type
                                     wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                     job:Ref_Number, | ! Job_Number
                                     0, | ! Sales_Number
                                     wpr:Quantity, | ! Quantity
                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                     wpr:Sale_Cost, | ! Sale_Cost
                                     wpr:Retail_Cost, | ! Retail_Cost
                                     'STOCK DECREMENTED', | ! Notes
                                     '') ! Information
                    ! Added OK
                Else ! AddToStockHistory
                    ! Error
                End ! AddToStockHistory
            End !If Access:STOCK.Update() = Level:Benign
        Else !If Access:PARTS.TryInsert() = Level:Benign
            !Insert Failed
            Access:PARTS.CancelAutoInc()
        End !If Access:PARTS.TryInsert() = Level:Benign
    End !If Access:PARTS.PrimeRecord() = Level:Benign
MaxMarkup_Message Routine

    Case Missive('You cannot mark-up part by more than ' & Format(tmp:MaxMarkup,@n3) & '%.','ServiceBase 3g',|
                   'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
OrderPart       Routine
    !Create entry for the order
    If Access:PARTS.PrimeRecord() = Level:Benign
        RecordNumber#   = par:Record_Number
        par:Record     :=: wpr:Record
        par:Record_Number = RecordNumber#
        par:Quantity = wpr:Quantity - sto:Quantity_Stock
        par:PartAllocated = 0
        par:WebOrder    = 1
        If Access:PARTS.TryInsert() = Level:Benign
            !Insert Successful
            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
        Else !If Access:PARTS.TryInsert() = Level:Benign
            !Insert Failed
            Access:PARTS.CancelAutoInc()
        End !If Access:PARTS.TryInsert() = Level:Benign
    End !If Access:PARTS.PrimeRecord() = Level:Benign
ResetCorrectionCosts        Routine
    ! Inserting (DBH 16/08/2006) # 8132 - Always blank the costs for a Correction. This allows for errors.
    If wpr:Correction
        wpr:Purchase_Cost = 0
        wpr:Sale_Cost = 0
        wpr:RRCPurchaseCost = 0
        wpr:InWarrantyMarkup = 0
        wpr:OutWarrantyMarkup = 0
        wpr:RRCInWarrantyMarkup = 0
        wpr:RRCOutWarrantyMarkup = 0
    End ! If wpr:Correction
    ! End (DBH 16/08/2006) #8132

ShowCosts       Routine

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  =  GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        !Get the Part's Location - L789 (DBH: 17-07-2003)
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = wpr:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:Location <> tra:SiteLocation
                !This part is NOT from ARC - L789 (DBH: 17-07-2003)
                tmp:ARCPart = 0
                If ~glo:WebJob
                    ! Changing (DBH 04/01/2006) #6645 - Allow access to certain users (to change fault codes)
                    ! ?OK{prop:Hide} = 1
                    ! to (DBH 04/01/2006) #6645
                    if glo:RelocateStore = true and Sto:location = 'MAIN STORE' then
                        !this is permitted
                        !message('This should be OK')
                        tmp:ARCPart = 1
                    ELSE
                        If SecurityCheck('AMEND RRC PART')
                            ?OK{prop:Hide} = 1
                        End ! If SecurityCheck('AMEND RRC PART')
                    END !if relocated store
                    ! End (DBH 04/01/2006) #6645
                End !If glo:WebJob
            Else ! !If sto:Location <> tra:SiteLocation
                tmp:ARCPart = 1
                If glo:WebJob
                    ?OK{prop:Hide} = 1
                End !If glo:WebJob
            End !If sto:Location <> tra:SiteLocation
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Else ! If Access:TRADE.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADE.Tryfetch(tra:Account_Number_Key) = Level:Benign
    If tmp:ARCPart
        tmp:PurchaseCost        = wpr:AveragePurchaseCost
        tmp:InWarrantyCost      = wpr:Purchase_Cost
        tmp:OutWarrantyCost     = wpr:Sale_Cost
        tmp:InWarrantyMarkup    = wpr:InWarrantyMarkup
        tmp:OutWarrantyMarkup   = wpr:OutWarrantyMarkup

    Else !If tmp:ARCPart
        If wpr:RRCAveragePurchaseCost > 0
            tmp:PurchaseCost        = wpr:RRCAveragePurchaseCost
        Else !If wpr:RRCAveragePurchaseCost > 0
            tmp:PurchaseCost        = wpr:RRCPurchaseCost
        End !If wpr:RRCAveragePurchaseCost > 0
        tmp:InWarrantyCost      = wpr:RRCPurchaseCost
        tmp:OutWarrantyCost     = wpr:Sale_Cost
        tmp:InWarrantyMarkup    = wpr:RRCInWarrantyMarkup
        tmp:OutWarrantyMarkup   = wpr:RRCOutWarrantyMarkup
    End !If tmp:ARCPart

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Warranty Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Warranty Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020427'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Warranty_Part')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlShiftS
  SELF.AddHistoryFile(wpr:Record,History::wpr:Record)
  SELF.AddHistoryField(?wpr:Part_Number,5)
  SELF.AddHistoryField(?wpr:Description,6)
  SELF.AddHistoryField(?wpr:Main_Part,21)
  SELF.AddHistoryField(?wpr:Despatch_Note_Number,14)
  SELF.AddHistoryField(?wpr:Supplier,7)
  SELF.AddHistoryField(?wpr:Quantity,11)
  SELF.AddHistoryField(?wpr:Exclude_From_Order,13)
  SELF.AddHistoryField(?wpr:Date_Ordered,15)
  SELF.AddHistoryField(?wpr:Order_Number,17)
  SELF.AddHistoryField(?wpr:Date_Received,19)
  SELF.AddHistoryField(?wpr:PartAllocated,39)
  SELF.AddHistoryField(?wpr:Fault_Codes_Checked,22)
  SELF.AddUpdateFile(Access:WARPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULT2.Open
  Relate:LOCATION_ALIAS.Open
  Relate:MANFAULO_ALIAS.Open
  Relate:MANFAULT_ALIAS.Open
  Relate:MANFAUPA_ALIAS.Open
  Relate:MANFPALO_ALIAS.Open
  Relate:ORDWEBPR.Open
  Relate:PARTS_ALIAS.Open
  Relate:STOCKALX.Open
  Relate:STOCK_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:WARPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFPALO.UseFile
  Access:LOCATION.UseFile
  Access:STOMODEL.UseFile
  Access:STOMPFAU.UseFile
  Access:MANFPARL.UseFile
  Access:MANFAURL.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WARPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Save Fields
  quantity_temp = wpr:quantity
  tmp:part_number   = wpr:part_number
  tmp:description   = wpr:description
  tmp:supplier      = wpr:supplier
  tmp:purchase_cost = wpr:purchase_cost
  tmp:sale_cost     = wpr:sale_cost
  main_part_temp  = wpr:main_part
  adjustment_temp = wpr:adjustment
  tmp:exclude = wpr:exclude_from_order
  sav:Supplier    = wpr:Supplier
      !Security Check
      If SecurityCheck('PART ALLOCATED')
          ?wpr:PartAllocated{prop:Disable} = 1
      Else !SecurityAccess('PART ALLOCATED')
          ?wpr:PartAllocated{prop:Disable} = 0
      End !SecurityAccess('PART ALLOCATED')
  ! Set Adjustment and Correction Settings
  If ThisWindow.Request = InsertRecord
      wpr:Quantity = 1
      If glo:Select1 = 'ADJUSTMENT'
          wpr:Adjustment = 'YES'
          wpr:Exclude_From_Order = 'YES'
          wpr:Ref_Number = glo:Select2
          wpr:Purchase_Cost = ''
          wpr:Sale_Cost = ''
          If job:Manufacturer = 'SIEMENS'
              wpr:Fault_Code1 = 'Y'
              wpr:Fault_Code2 = 'N'
          End ! If job:Manufacturer = 'SIEMENS'
  
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              If man:IncludeAdjustment = 'YES' and man:AdjustPart <> 1
                  wpr:Part_Number = 'ADJUSTMENT'
                  wpr:Description = 'ADJUSTMENT'
                  ?wpr:Part_Number{prop:ReadOnly} = 1
                  ?wpr:Part_Number{prop:Skip} = 1
                  ?wpr:Description{prop:ReadOnly} = 1
                  ?wpr:Description{prop:Skip} = 1
                  ?Browse_Stock_Button{prop:Hide} = 1
              End ! If man:IncludeAdjustment = 'YES' and man:AdjustPart <> 1
          End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      End ! If glo:Select1 = 'ADJUSTMENT'
  
      ! Is this part a correction? (DBH 24/10/2007 13:33:41) - TrkBs: 5365
      If glo:Select1 = 'CORRECT PART'
          wpr:Correction = 1
          wpr:Adjustment = 'YES'
          wpr:Exclude_From_Order = 'YES'
          wpr:PartAllocated = 1
          ?wpr:PartAllocated{prop:Disable} = 1
          ?wpr:Exclude_From_Order{prop:Disable} = 1
          glo:Select1 = ''
      End ! If glo:Select1 = 'CORRECT PART'
  Else ! If ThisWindow.Request = InsertRecord
      ?Browse_Stock_Button{prop:Disable} = 1
      ?wpr:Exclude_From_Order{prop:Disable} = 1
      ?wpr:Part_Number{prop:ReadOnly} = 1
      ?wpr:Part_Number{prop:Skip} = 1
      ?wpr:Description{prop:ReadOnly} = 1
      ?wpr:Description{prop:Skip} = 1
      ?wpr:Supplier{prop:ReadOnly} = 1
      ?wpr:Supplier{prop:Skip} = 1
      ?LookupSupplier{prop:Hide} = 1
      ?tmp:PurchaseCost{prop:ReadOnly} = 1
      ?tmp:PurchaseCost{prop:Skip} = 1
  
      ! Disable costs for completed jobs
      If job:Date_Completed <> ''
          ?tmp:InWarrantyCost{prop:ReadOnly} = 1
          ?tmp:InWarrantyCost{prop:Skip} = 1
          ?tmp:InWarrantyMarkup{prop:ReadOnly} = 1
          ?tmp:InWarrantyMarkup{prop:Skip} = 1
      End ! If job:Date_Completed <> ''
  End ! If ThisWindow.Request = InsertRecord
  
  ! Costs are required for Adjustments
  If wpr:Adjustment = 'YES'
      ?tmp:PurchaseCost{prop:ReadOnly} = 1
      ?tmp:PurchaseCost{prop:Skip} = 1
      ?tmp:InWarrantyCost{prop:ReadOnly} = 1
      ?tmp:InWarrantyCost{prop:Skip} = 1
  End ! If wpr:Adjustment = 'YES'
  ?Title{prop:text} = 'Warranty Part'
  If wpr:Correction = True
      ?Title{prop:Text} = Clip(?Title{prop:Text}) & ' (Correction)'
  End ! wpr:Correction = True
  
  If wpr:order_number <> ''
      ?Title{prop:Text} = Clip(?Title{prop:Text}) & ' - On Order'
  End
  If wpr:pending_ref_number <> ''
      ?Title{prop:Text} = Clip(?Title{prop:Text}) & ' - Pending Order'
  End
  If wpr:date_received <> ''
      ?Title{prop:Text} = Clip(?Title{prop:Text}) & ' - Received'
  End
  IF wpr:exclude_from_order = 'YES'
      ?Title{prop:Text} = Clip(?Title{prop:Text}) & ' - Excluded From Order'
  End!IF wpr:exclude_from_order = 'YES'
  If wpr:WebOrder
      ?Title{prop:Text} = Clip(?Title{prop:Text}) & ' - Retail Order Raised'
      ?OK{prop:Hide} = 1
  End !par:WebOrder
  ! Inserting (DBH 16/08/2006) # 8132 - Blank the correction costs again, just incase of errors
  Do ResetCorrectionCosts
  ! End (DBH 16/08/2006) #8132
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?wpr:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?wpr:Supplier{Prop:Tip}
  END
  IF ?wpr:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?wpr:Supplier{Prop:Msg}
  END
  tmp:MaxMarkup = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  
  Do lookup_location
  Do ShowCosts
  Do EnableDisableCosts
  
  If ThisWindow.Request <> InsertRecord
      If wpr:Part_Ref_Number <> '' And wpr:PartAllocated And ~wpr:WebOrder
          !Don't show the Unallocate Part button
          !if the OK button is hidden - 3310 (DBH: 25-09-2003)
          If ?OK{prop:Hide} = 0
              ?UnallocatePart{prop:Hide} = 0
          End !If ?OK{prop:Hide} = 0
      End !If wpr:Part_Ref_Number <> '' And wpr:PartAllocated
  End !ThisWindow.LocalRequest <> InsertRecord
  !Fault Codes
  Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
  map:Manufacturer = job:Manufacturer
  map:ScreenOrder = 0
  If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Case Missive('One or more of the Manufacturer Part Fault Codes have not been assigned a "Screen Order".'&|
        '|The fault codes screen will not function correctly until this is done.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Post(Event:CloseWindow)
  End ! If Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign
  
  Do BuildNumbersQueue
  
  ! Fill In Fault Codes
  Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
  map:Manufacturer = job:Manufacturer
  map:ScreenOrder = 0
  Set(map:ScreenOrderKey,map:ScreenOrderKey)
  Loop
      If Access:MANFAUPA.Next()
          Break
      End ! If Access:MANFAUPA.Next()
      If map:Manufacturer <> job:Manufacturer
          Break
      End ! If map:Manufacturer <> job:Manufacturer
      If map:ScreenOrder = 0
          Cycle
      End ! If map:ScreenOrder = 0
  
      fault:FaultNumber = map:Field_Number
      Get(FaultCodeQueue,fault:FaultNumber)
      tmp:FaultCode[map:ScreenOrder] = fault:FaultCode
  End ! Loop
  Do FaultCode
  
  ! Deleting (DBH 18/10/2007) # 9278 - New procedure to work out fault codes
  !! Fault Coding (Hopefully)
  !If job:manufacturer = 'NEC'
  !    Unhide(?wpr:main_part)
  !End!If job:manufacturer = 'NEC'
  !fault_codes_required_temp = 'NO'
  !required# = 0
  !access:chartype.clearkey(cha:charge_type_key)
  !cha:charge_type = job:warranty_charge_type
  !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
  !    If cha:force_warranty = 'YES'
  !        required# = 1
  !    End
  !end !if
  !
  !!Ok, workaround!
  !access:manfaupa.clearkey(map:MainFaultKey)
  !map:manufacturer = job:manufacturer
  !map:MainFault = TRUE
  !IF Access:MANFAUPA.Fetch(map:MainFaultKey)
  !  !No main code, carry on a s normal!
  !ELSE
  !  If sto:Accessory <> 'YES' and man:ForceAccessoryCode
  !    !Only unhide the main fault!
  !    unhide_tab = TRUE
  !  END
  !END
  !
  !
  !found# = 0
  !setcursor(cursor:wait)
  !save_map_id = access:manfaupa.savefile()
  !access:manfaupa.clearkey(map:field_number_key)
  !map:manufacturer = job:manufacturer
  !set(map:field_number_key,map:field_number_key)
  !loop
  !    if access:manfaupa.next()
  !       break
  !    end !if
  !    if map:manufacturer <> job:manufacturer      |
  !        then break.  ! end if
  !    IF unhide_tab = TRUE
  !      IF map:MainFault = FALSE
  !        CYCLE
  !      END
  !    END
  !    Case map:field_number
  !        Of 1
  !            found# = 1
  !            Unhide(?wpr:fault_code1:prompt)
  !            ?wpr:fault_code1:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar)
  !                ?popcalendar{prop:xpos} = 212
  !                Unhide(?wpr:fault_code1:2)
  !                ?wpr:fault_code1:2{prop:xpos} = 84
  !            Else
  !                Unhide(?wpr:fault_code1)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4)
  !                End
  !            End
  !
  !            If required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code1{prop:req} = 1
  !                ?wpr:fault_code1:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code1{prop:req} = 0
  !                ?wpr:fault_code1:2{prop:req} = 0
  !            End
  !
  !            ! Inserting (DBH 04/01/2006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code1{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code1{prop:Hide} = True
  !                    ?wpr:Fault_code1:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4{prop:Hide} = True
  !                ?PopCalendar{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/2006) #6645
  !        Of 2
  !            found# = 1
  !            Unhide(?wpr:fault_code2:prompt)
  !            ?wpr:fault_code2:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:2)
  !                Unhide(?wpr:fault_code2:2)
  !            Else
  !                Unhide(?wpr:fault_code2)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:2)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code2{prop:req} = 1
  !                ?wpr:fault_code2:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code2{prop:req} = 0
  !                ?wpr:fault_code2:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/2006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code2 <> ''
  !                    ?wpr:Fault_Code2{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code2{prop:Hide} = True
  !                    ?wpr:Fault_code2:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:2{prop:Hide} = True
  !                ?PopCalendar:2{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/2006) #6645
  !        Of 3
  !            found# = 1
  !            Unhide(?wpr:fault_code3:prompt)
  !            ?wpr:fault_code3:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:3)
  !                Unhide(?wpr:fault_code3:2)
  !            Else
  !                Unhide(?wpr:fault_code3)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:3)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code3{prop:req} = 1
  !                ?wpr:fault_code3:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code3{prop:req} = 0
  !                ?wpr:fault_code3:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/3006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code3 <> ''
  !                    ?wpr:Fault_Code3{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code3{prop:Hide} = True
  !                    ?wpr:Fault_code3:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:3{prop:Hide} = True
  !                ?PopCalendar:3{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/3006) #6645
  !        Of 4
  !            found# = 1
  !            Unhide(?wpr:fault_code4:prompt)
  !            ?wpr:fault_code4:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:4)
  !                Unhide(?wpr:fault_code4:2)
  !            Else
  !                Unhide(?wpr:fault_code4)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:4)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code4{prop:req} = 1
  !                ?wpr:fault_code4:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code4{prop:req} = 0
  !                ?wpr:fault_code4:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/2006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code4 <> ''
  !                    ?wpr:Fault_Code4{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code4{prop:Hide} = True
  !                    ?wpr:Fault_code4:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:4{prop:Hide} = True
  !                ?PopCalendar:4{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/4006) #6645
  !        Of 5
  !            found# = 1
  !            Unhide(?wpr:fault_code5:prompt)
  !            ?wpr:fault_code5:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:5)
  !                Unhide(?wpr:fault_code5:2)
  !            Else
  !                Unhide(?wpr:fault_code5)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:5)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code5{prop:req} = 1
  !                ?wpr:fault_code5:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code5{prop:req} = 0
  !                ?wpr:fault_code5:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/2006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code5 <> ''
  !                    ?wpr:Fault_Code5{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code5{prop:Hide} = True
  !                    ?wpr:Fault_code5:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:5{prop:Hide} = True
  !                ?PopCalendar:5{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/5006) #6645
  !        Of 6
  !            found# = 1
  !            Unhide(?wpr:fault_code6:prompt)
  !            ?wpr:fault_code6:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:6)
  !                Unhide(?wpr:fault_code6:2)
  !            Else
  !                Unhide(?wpr:fault_code6)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:6)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code6{prop:req} = 1
  !                ?wpr:fault_code6:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code6{prop:req} = 0
  !                ?wpr:fault_code6:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/6006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code6 <> ''
  !                    ?wpr:Fault_Code6{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code6{prop:Hide} = True
  !                    ?wpr:Fault_code6:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:6{prop:Hide} = True
  !                ?PopCalendar:6{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/6006) #6645
  !        Of 7
  !            found# = 1
  !            Unhide(?wpr:fault_code7:prompt)
  !            ?wpr:fault_code7:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:7)
  !                Unhide(?wpr:fault_code7:2)
  !            Else
  !                Unhide(?wpr:fault_code7)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:7)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code7{prop:req} = 1
  !                ?wpr:fault_code7:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code7{prop:req} = 0
  !                ?wpr:fault_code7:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/7006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code7 <> ''
  !                    ?wpr:Fault_Code7{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code7{prop:Hide} = True
  !                    ?wpr:Fault_code7:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:7{prop:Hide} = True
  !                ?PopCalendar:7{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/7006) #6645
  !        Of 8
  !            found# = 1
  !            Unhide(?wpr:fault_code8:prompt)
  !            ?wpr:fault_code8:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:8)
  !                Unhide(?wpr:fault_code8:2)
  !            Else
  !                Unhide(?wpr:fault_code8)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:8)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code8{prop:req} = 1
  !                ?wpr:fault_code8:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code8{prop:req} = 0
  !                ?wpr:fault_code8:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/8006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code8 <> ''
  !                    ?wpr:Fault_Code8{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code8{prop:Hide} = True
  !                    ?wpr:Fault_code8:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:8{prop:Hide} = True
  !                ?PopCalendar:8{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/8006) #6645
  !        Of 9
  !            found# = 1
  !            Unhide(?wpr:fault_code9:prompt)
  !            ?wpr:fault_code9:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:9)
  !                Unhide(?wpr:fault_code9:2)
  !            Else
  !                Unhide(?wpr:fault_code9)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:9)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code9{prop:req} = 1
  !                ?wpr:fault_code9:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code9{prop:req} = 0
  !                ?wpr:fault_code9:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/9006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code9 <> ''
  !                    ?wpr:Fault_Code9{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code9{prop:Hide} = True
  !                    ?wpr:Fault_code9:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:9{prop:Hide} = True
  !                ?PopCalendar:9{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/9006) #6645
  !        Of 10
  !            found# = 1
  !            Unhide(?wpr:fault_code10:prompt)
  !            ?wpr:fault_code10:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:10)
  !                Unhide(?wpr:fault_code10:2)
  !            Else
  !                Unhide(?wpr:fault_code10)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:10)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code10{prop:req} = 1
  !                ?wpr:fault_code10:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code10{prop:req} = 0
  !                ?wpr:fault_code10:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/10006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code10 <> ''
  !                    ?wpr:Fault_Code10{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code10{prop:Hide} = True
  !                    ?wpr:Fault_code10:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:10{prop:Hide} = True
  !                ?PopCalendar:10{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/10006) #6645
  !        Of 11
  !            found# = 1
  !            Unhide(?wpr:fault_code11:prompt)
  !            ?wpr:fault_code11:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:11)
  !                Unhide(?wpr:fault_code11:2)
  !            Else
  !                Unhide(?wpr:fault_code11)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:11)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code11{prop:req} = 1
  !                ?wpr:fault_code11:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code11{prop:req} = 0
  !                ?wpr:fault_code11:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/11006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code11 <> ''
  !                    ?wpr:Fault_Code11{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code11{prop:Hide} = True
  !                    ?wpr:Fault_code11:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:11{prop:Hide} = True
  !                ?PopCalendar:11{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/11006) #6645
  !        Of 12
  !            found# = 1
  !            Unhide(?wpr:fault_code12:prompt)
  !            ?wpr:fault_code12:prompt{prop:text} = map:field_name
  !            If map:field_type = 'DATE'
  !                Unhide(?popcalendar:12)
  !                Unhide(?wpr:fault_code12:2)
  !            Else
  !                Unhide(?wpr:fault_code12)
  !                If map:lookup = 'YES'
  !                    Unhide(?button4:12)
  !                End
  !            End
  !            if required# = 1 And map:compulsory = 'YES'
  !                fault_codes_required_temp = 'YES'
  !                ?wpr:fault_code12{prop:req} = 1
  !                ?wpr:fault_code12:2{prop:req} = 1
  !            else
  !                ?wpr:fault_code12{prop:req} = 0
  !                ?wpr:fault_code12:2{prop:req} = 0
  !            End
  !            ! Inserting (DBH 04/01/12006) #6645 - Disable, or hide if blank, "not available" fault codes
  !            If map:NotAvailable = True
  !                If wpr:Fault_code12 <> ''
  !                    ?wpr:Fault_Code12{prop:Disable} = True
  !                Else ! If wpr:Fault_code1 <> ''
  !                    ?wpr:Fault_Code12{prop:Hide} = True
  !                    ?wpr:Fault_code12:Prompt{prop:Hide} = True
  !                End ! If wpr:Fault_code1 <> ''
  !                ?Button4:12{prop:Hide} = True
  !                ?PopCalendar:12{prop:Hide} = True
  !            End ! If map:NotAvailable = True
  !            ! End (DBH 04/01/12006) #6645
  !    End !Case map:field_number
  !end !loop
  !access:manfaupa.restorefile(save_map_id)
  !setcursor()
  !If found# = 1
  !    unhide(?WPR:Fault_Codes_Checked)
  !Else
  !    Unhide(?FaultCodesText)
  !End!If found# = 1
  !IF job:Manufacturer = 'SIEMENS'
  !  IF wpr:Adjustment = 'YES'
  !    ?wpr:fault_code3{prop:req} = True
  !    ?wpr:fault_code3:2{prop:req} = 1
  !  END
  !END
  ! End (DBH 18/10/2007) #9278
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULT2.Close
    Relate:LOCATION_ALIAS.Close
    Relate:MANFAULO_ALIAS.Close
    Relate:MANFAULT_ALIAS.Close
    Relate:MANFAUPA_ALIAS.Close
    Relate:MANFPALO_ALIAS.Close
    Relate:ORDWEBPR.Close
    Relate:PARTS_ALIAS.Close
    Relate:STOCKALX.Close
    Relate:STOCK_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    wpr:Adjustment = 'NO'
    wpr:Warranty_Part = 'YES'
    wpr:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSuppliers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?browse_stock_button
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          Case Missive('The selected user is not allocated to an Active Site Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  Case Missive('Error! Cannot access the stock file.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!if access:stock.fetch(sto:ref_number_key)
                  ! Insert --- Allow for chargeable parts only (DBH: 12/08/2009) #11010
                  If sto:ChargeablePartOnly = True
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('Error! The selected part is Chargeable Only.','ServiceBase',|
                                     'mstop.jpg','/&OK')
                      Of 1 ! &OK Button
                      End!Case Message
                      Cycle
                  End
                  ! end --- (DBH: 12/08/2009) #11010
      
                  If sto:part_Number = 'EXCH'
                      Case Missive('Cannot attach an "Exchange Part".','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      wpr:Part_Number = ''
                      Select(?wpr:Part_Number)
                  Else !If sto:part_Number = 'EXCH'
                      ! Check if the part can be used on Level1/2 Repairs, for Nokia - TrkBs: 5904 (DBH: 23-09-2005)
                      If job:Manufacturer = 'NOKIA'
                          If sto:ExcludeLevel12Repair = 1 And (job:Repair_Type_Warranty = 'LEVEL 1' or job:Repair_Type_Warranty = 'LEVEL 2')
                             Case Missive('Error! This part cannot be used on Level 1/2 Repairs:|'&|
                              Clip(sto:Part_Number) & ' - ' & CLip(sto:Description) & '.','ServiceBase 3g',|
                                            'mstop.jpg','/OK')
                                 Of 1 ! OK Button
                             End ! Case Missive
                             wpr:Part_Number = ''
                             Select(?wpr:Part_Number)
                             Cycle
                          End ! If sto:ExcludeLevel12Repair = 1
                      End ! If job:Manufacturer = 'NOKIA'
                      if sto:ExchangeUnit = 'YES'
                          Case Missive('Error! An exchange unit must be picked from exchange stock.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          wpr:Part_Number = ''
                          select(?wpr:Part_Number)
                      else
                          If sto:Suspend
                              Case Missive('This part cannot be used, it has been suspended.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              wpr:Part_Number = ''
                              Select(?wpr:Part_Number)
                          Else !If sto:Suspend
      
                              If use:RestrictParts And use:RestrictWarranty And sto:SkillLevel > use:SkillLevel
                                  Case Missive('Error! Your skill level is not high enough to use the selected part.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  wpr:Part_Number = ''
                                  Select(?wpr:Part_Number)
                              Else !If sto:SkillLevel > use:SkillLevel
                                  wpr:part_number     = sto:part_number
                                  wpr:description     = sto:description
                                  wpr:supplier        = sto:supplier
                                  wpr:purchase_cost   = sto:purchase_cost
                                  wpr:sale_cost       = sto:sale_cost
                                  wpr:Retail_Cost     = sto:Retail_Cost
                                  wpr:part_ref_number = sto:ref_number
      
                                  Do GetMarkupCosts
      
                                  If sto:assign_fault_codes = 'YES'
                                      stm:Manufacturer = sto:Manufacturer
                                      stm:Model_Number = job:Model_Number
                                      If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                                          wpr:fault_code1    = stm:Faultcode1
                                          wpr:fault_code2    = stm:Faultcode2
                                          wpr:fault_code3    = stm:Faultcode3
                                          wpr:fault_code4    = stm:Faultcode4
                                          wpr:fault_code5    = stm:Faultcode5
                                          wpr:fault_code6    = stm:Faultcode6
                                          wpr:fault_code7    = stm:Faultcode7
                                          wpr:fault_code8    = stm:Faultcode8
                                          wpr:fault_code9    = stm:Faultcode9
                                          wpr:fault_code10   = stm:Faultcode10
                                          wpr:fault_code11   = stm:Faultcode11
                                          wpr:fault_code12   = stm:Faultcode12
      
                                      End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                                  End!If sto:assign_fault_codes = 'YES'
                                  Select(?wpr:quantity)
                              End !If sto:SkillLevel > use:SkillLevel
                          End !If sto:Suspend
                      end
                  End !If sto:part_Number = 'EXCH'
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
          end
          globalrequest     = saverequest#
          do lookup_location
      End !Error# = 0
      
      Do ShowCosts
      Do EnableDisableCosts
      
      do LookupMainFault
      DO FaultCode
    OF ?wpr:Main_Part
      If wpr:main_part <> main_part_temp
          If wpr:main_part = 'YES'
              found# = 0
              setcursor(cursor:wait)
              save_war_ali_id = access:warparts_alias.savefile()
              clear(war_ali:record, -1)
              war_ali:ref_number  = job:ref_number
              set(war_ali:part_number_key,war_ali:part_number_key)
              loop
                  next(warparts_alias)
                  if errorcode()                     |
                     or war_ali:ref_number  <> job:ref_number      |
                     then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  If war_ali:record_number <> wpr:record_number
                      If war_ali:main_part = 'YES'
                          found# = 1
                          Case Missive('One of the other parts attached to this job has already been marked as the "Main Part".','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Break
                      End!If war_ali:main_part = 'YES'
                  End!If war_ali:record_number <> wpr:record_number
              end !loop
              access:warparts_alias.restorefile(save_war_ali_id)
              setcursor()
              If found# = 0
                  main_part_temp = wpr:main_part
              Else!If found# = 0
                  wpr:main_part = main_part_temp
              End!If found# = 0
          Else!If wpr:main_part = 'YES'
              Case Missive('Please ensure ONE of the other parts attached to this job is marked as "Main Part".','ServiceBase 3g',|
                             'mexclam.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              main_part_temp  = wpr:main_part
          End!If wpr:main_part = 'YES'
      End!If wpr:main_part <> main_part_temp
    OF ?tmp:InWarrantyMarkup
      if ~0{prop:AcceptAll}
          if tmp:MaxMarkup <> 0
              if tmp:InWarrantyMarkup > tmp:MaxMarkup
                  tmp:InWarrantyMarkup = tmp:MaxMarkup
                  do MaxMarkup_Message
                  select(?tmp:InWarrantyMarkup)
                  cycle
              end
          end
      end
    OF ?wpr:Exclude_From_Order
      If ~0{prop:acceptall}
          If thiswindow.request = Insertrecord
              If wpr:exclude_from_order <> tmp:exclude
                  If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                      Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      wpr:Exclude_From_Order = tmp:Exclude
                  Else !SecurityCheck('EXCLUDE FROM ORDER')
                      If wpr:Exclude_From_Order = 'YES' And wpr:part_ref_number <> ''
                          Case Missive('You have selected to "Exclude From Order".'&|
                            '<13,10>'&|
                            '<13,10>This part''s stock quantity will not be decremented.'&|
                            '<13,10>Are you sure?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
      
                                  tmp:exclude = wpr:Exclude_from_order
                              Of 1 ! No Button
      
                                  wpr:exclude_from_order = tmp:exclude
                          End ! Case Missive
                      Else
                          tmp:exclude = wpr:exclude_From_order
                      End!If PAR:Exclude_From_Order = 'YES'
                  End !SecurityCheck('EXCLUDE FROM ORDER')
              End !If wpr:exclude_from_order <> tmp:exclude
          End!If thiswindow.request = Insertrecord
      End!If ~0{prop:acceptall}
      Display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020427'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020427'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020427'&'0')
      ***
    OF ?wpr:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          !Can't find engineer's location, so use the user logged on
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
      
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = wpr:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign And wpr:Part_Number <> 'EXCH'
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              error# = 0
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
              End !If Error# = 0
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
      
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = wpr:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          If use:RestrictParts And use:RestrictWarranty And sto:SkillLevel > use:SkillLevel
                              Error# = 4
                          Else !If sto:SkillLevel > use:SkillLevel
                              error# = 0
                          End !If sto:SkillLevel > use:SkillLevel
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
      
              wpr:part_number     = sto:part_number
              wpr:description     = sto:description
              wpr:supplier        = sto:supplier
              wpr:purchase_cost   = sto:purchase_cost
              wpr:sale_cost       = sto:sale_cost
              wpr:Retail_Cost     = sto:Retail_Cost
              wpr:part_ref_number = sto:ref_number
      
              Do GetMarkupCosts
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      wpr:fault_code1    = stm:Faultcode1
                      wpr:fault_code2    = stm:Faultcode2
                      wpr:fault_code3    = stm:Faultcode3
                      wpr:fault_code4    = stm:Faultcode4
                      wpr:fault_code5    = stm:Faultcode5
                      wpr:fault_code6    = stm:Faultcode6
                      wpr:fault_code7    = stm:Faultcode7
                      wpr:fault_code8    = stm:Faultcode8
                      wpr:fault_code9    = stm:Faultcode9
                      wpr:fault_code10   = stm:Faultcode10
                      wpr:fault_code11   = stm:Faultcode11
                      wpr:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
              Select(?wpr:quantity)
              display()
      
          Of 1
              Case Missive('Cannot find the selected part.'&|
                '<13,10>'&|
                '<13,10>If either does not exist in the Engineer''s Location, or is not in stock.'&|
                '<13,10>Do you wish to SELECT another part, or CONTINUE to use the entered part number?','ServiceBase 3g',|
                             'mquest.jpg','Continue|Select')
                  Of 2 ! Select Button
                      Post(event:accepted,?browse_stock_button)
                  Of 1 ! Continue Button
                      Select(?wpr:description)
              End ! Case Missive
          Of 2
              Case Missive('This part cannot be used. It has been suspended.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              wpr:Part_Number = ''
              Select(?wpr:Part_Number)
          Of 3
              Case Missive('Error! Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                '<13,10>'&|
                '<13,10>This location is NOT active and cannot be used.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              wpr:Part_Number = ''
              Select(?wpr:Part_Number)
          Of 4
              Case Missive('Error! Your skill level is not high enough to use the selected part.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              wpr:Part_Number = ''
              Select(?wpr:Part_Number)
      End!If error# = 0
      Do Lookup_Location
      Display()
      
      Do ShowCosts
      Do EnableDisableCosts
      End!If ~0{prop:acceptall}
    OF ?tmp:PurchaseCost
      Do EnableDisableCosts
    OF ?tmp:InWarrantyCost
      Do EnableDisableCosts
    OF ?tmp:InWarrantyMarkup
      Do EnableDisableCosts
    OF ?wpr:Supplier
      If ~0{prop:acceptall}
          If sav:Supplier <> wpr:Supplier
              If SecurityCheck('PARTS - AMEND SUPPLIER')
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  wpr:Supplier    = sav:Supplier
              Else!If SecurityCheck('PARTS - AMEND SUPPLIER')
                  sav:Supplier    = wpr:Supplier
              End!If SecurityCheck('PARTS - AMEND SUPPLIER')
          End!If sav:Supplier <> par:Supplier
      End!If ~0{prop:acceptall}
      Display(?wpr:Supplier)
      
      IF wpr:Supplier OR ?wpr:Supplier{Prop:Req}
        sup:Company_Name = wpr:Supplier
        !Save Lookup Field Incase Of error
        look:wpr:Supplier        = wpr:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            wpr:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            wpr:Supplier = look:wpr:Supplier
            SELECT(?wpr:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = wpr:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          wpr:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?wpr:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?wpr:Supplier)
    OF ?UnallocatePart
      ThisWindow.Update
      If SecurityCheck('UNALLOCATE PART')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('UNALLOCATE PART')
          Case Missive('This part will be returned to stock and will appear in the "Parts On Order" section of Stock Allocation.'&|
            '<13,10>'&|
            '<13,10>Are you sure you want to unallocate this part?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  tmp:UnAllocate = 1
                  Post(Event:Accepted,?OK)
              Of 1 ! No Button
          End ! Case Missive
      End !SecurityCheck('UNALLOCATE PART')
    OF ?Button:Lookup:1
      ThisWindow.Update
      x# = 1
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_2
      x# = 2
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:2)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?tmp:FaultCode_1
      x# = 1
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:1)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:2
      ThisWindow.Update
      x# = 2
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?Button:Lookup:3
      ThisWindow.Update
      x# = 3
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_3
      x# = 3
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:3)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:4
      ThisWindow.Update
      x# = 4
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_4
      x# = 4
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:4)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:5
      ThisWindow.Update
      x# = 5
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?Button:Lookup:6
      ThisWindow.Update
      x# = 6
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_6
      x# = 6
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:6)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?tmp:FaultCode_5
      x# = 5
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:5)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:7
      ThisWindow.Update
      x# = 7
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?Button:Lookup:8
      ThisWindow.Update
      x# = 8
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_8
      x# = 8
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:8)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?tmp:FaultCode_7
      x# = 7
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:7)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:9
      ThisWindow.Update
      x# = 9
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_10
      x# = 10
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:10)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?tmp:FaultCode_9
      x# = 9
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:9)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:10
      ThisWindow.Update
      x# = 10
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_11
      x# = 11
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:11)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:11
      ThisWindow.Update
      x# = 11
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?tmp:FaultCode_12
      x# = 12
      If tmp:FaultCode[x#] <> ''
          If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
              Post(Event:Accepted,?Button:Lookup:12)
          End ! If local.LookupFaultCodeField(x#,tmp:FaultCode[x#])
          Do FaultCode
      End ! If tmp:FaultCode[x#] <> ''
    OF ?Button:Lookup:12
      ThisWindow.Update
      x# = 12
      tmp:FaultCode[x#] = Local.LookupFaultCode(x#,tmp:FaultCode[x#])
      Do LookupMainFault
      Do FaultCode
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Fault Codes Checked
  If ?wpr:Fault_Codes_Checked{prop:Hide} = 0 And wpr:Fault_Codes_Checked <> 'YES'
      Case Missive('You must tick the "Fault Codes Checked" box before you can continue.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Select(?wpr:Fault_Codes_Checked)
      Cycle
  End ! If ?wpr:Fault_Codes_Check{prop:Hide} = 0 And wpr:Fault_Codes_Checked <> 'YES'
  
  
  ! Inserting (DBH 29/11/2007) # 9585 - Double check that all the fault codes are still correct
  Error# = 0
  Loop x# = 1 To 12
      Get(FieldNumbersQueue,x#)
      If field:FieldNumber{prop:Hide} = 0
          If local.LookupFaultCodeField(x#,Contents(field:FieldNumber))
              field:FieldNumber{prop:Color} = color:Red
              Error# = 1
  !            Break
  
          End ! If local.LookupFaultCodeField(x#,Contents(field:FieldNumber))
      End ! If field:FieldNumber{prop:Hide} = 0
  End ! Loop x# = 1 To 12
  
  If Error#
      Cycle
  End ! If Error#
  ! End (DBH 29/11/2007) #9585
  
  
  ! Write back fault codes (DBH: 19/10/2007)
  Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
  map:Manufacturer = job:Manufacturer
  map:ScreenOrder = 0
  Set(map:ScreenOrderKey,map:ScreenOrderKey)
  Loop
      If Access:MANFAUPA.Next()
          Break
      End ! If Access:MANFAUPA.Next()
      If map:Manufacturer <> job:Manufacturer
          Break
      End ! If map:Manufacturer <> job:Manufacturer
      If map:ScreenOrder = 0
          Cycle
      End ! If map:ScreenOrder = 0
      Case map:Field_Number
      Of 1
          wpr:Fault_Code1  = tmp:FaultCode[map:ScreenOrder]
      Of 2
          wpr:Fault_Code2  = tmp:FaultCode[map:ScreenOrder]
      Of 3
          wpr:Fault_Code3  = tmp:FaultCode[map:ScreenOrder]
      Of 4
          wpr:Fault_Code4  = tmp:FaultCode[map:ScreenOrder]
      Of 5
          wpr:Fault_Code5  = tmp:FaultCode[map:ScreenOrder]
      Of 6
          wpr:Fault_Code6  = tmp:FaultCode[map:ScreenOrder]
      Of 7
          wpr:Fault_Code7  = tmp:FaultCode[map:ScreenOrder]
      Of 8
          wpr:Fault_Code8  = tmp:FaultCode[map:ScreenOrder]
      Of 9
          wpr:Fault_Code9  = tmp:FaultCode[map:ScreenOrder]
      Of 10
          wpr:Fault_Code10 = tmp:FaultCode[map:ScreenOrder]
      Of 11
          wpr:Fault_Code11 = tmp:FaultCode[map:ScreenOrder]
      Of 12
          wpr:Fault_Code12 = tmp:FaultCode[map:ScreenOrder]
      End ! Case map:Field_Number
  End ! Loop
  !Check max markup
  if tmp:MaxMarkup <> 0
      if tmp:InWarrantyMarkup > tmp:MaxMarkup
          do MaxMarkup_Message
          select(?tmp:InWarrantyMarkup)
          cycle
      end
  end
  !Write Costs Back
      If tmp:ARCPart
          wpr:AveragePurchaseCost = tmp:PurchaseCost
          wpr:Purchase_Cost       = tmp:InWarrantyCost
          wpr:Sale_Cost           = tmp:OutWarrantyCost
          wpr:InWarrantyMarkup    = tmp:InWarrantyMarkup
          wpr:OutWarrantyMarkup   = tmp:OutWarrantyMarkup
      Else !If tmp:ARCPart
          wpr:RRCAveragePurchaseCost  = tmp:PurchaseCost
          wpr:RRCPurchaseCost     = tmp:InWarrantyCost
          wpr:RRCSaleCost         = tmp:OutWarrantyCost
          wpr:RRCInWarrantyMarkup = tmp:InWarrantyMarkup
          wpr:RRCOutWarrantyMarkup   = tmp:OutWarrantyMarkup
          wpr:Purchase_Cost       = wpr:RRCPurchaseCost
          wpr:Sale_Cost           = wpr:RRCSaleCost
      End !If tmp:ARCPart
  If thiswindow.Request = InsertRecord
      tmp:DontOK = 0
  
      wpr:PartAllocated = 1
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      !Is this a stock part?
      tmp:StockNumber = 0
      If wpr:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = wpr:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              !Need to check for duplicates?
              tmp:StockNumber = sto:Ref_Number
              Found# = 0
              If sto:AllowDuplicate = 0
                  Save_war_ali_ID = Access:WARPARTS_ALIAS.SaveFile()
                  Access:WARPARTS_ALIAS.ClearKey(war_ali:Part_Number_Key)
                  war_ali:Ref_Number  = job:Ref_Number
                  war_ali:Part_Number = wpr:Part_Number
                  Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                  Loop
                      If Access:WARPARTS_ALIAS.NEXT()
                         Break
                      End !If
                      If war_ali:Ref_Number  <> job:Ref_Number      |
                      Or war_ali:Part_Number <> wpr:Part_Number     |
                          Then Break.  ! End If
                      If wpr:Record_Number <> war_ali:Record_Number
                          If war_ali:Date_Received = ''
                              Found# = 1
                              Break
                          End !If par_ali:Date_Received
                      End !If wpr:Record_Number <> par_ali:Record_Number
                  End !Loop
                  Access:WARPARTS_ALIAS.RestoreFile(Save_war_ali_ID)
                  If Found# = 1
                      Case Missive('This part is already attached to this job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Select(?wpr:part_number)
                      Cycle
                  End !If Found# = 1
              End !If sto:AllowDuplicate = 0
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      End !wpr:Part_Ref_Number <> ''
  
      tmp:OutOfWarrantyAccessory = 0
  
      If wpr:Part_Number <> 'ADJUSTMENT'
          !The part is not an adjustment
          If wpr:Exclude_From_Order <> 'YES'
              !The part can be ordered
              If tmp:StockNumber
                  If sto:Sundry_Item <> 'YES'
                      !This is from stock
                      If wpr:Quantity > sto:Quantity_Stock
                          !There isn't enough in stock
                          Case Missive('There are insufficient items in stock.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to ORDER the remaining items, or RE-ENTER the quantity required?','ServiceBase 3g',|
                                         'mquest.jpg','Re-Enter|Order')
                              Of 2 ! Order Button
                                  If SecurityCheck('JOBS - ORDER PARTS')
                                      Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                  Else !If SecurityCheck('JOBS - ORDER PARTS')
                                      ! #10396 Not sure how this could happen. But stop using part that is suspended. (DBH: 16/07/2010)
                                      If (MainStoreSuspended(sto:Part_Number))
                                          Beep(Beep:SystemHand)  ;  Yield()
                                          Case Missive('This part is out of stock. It has been suspended at Main Store and therefore cannot be ordered.','ServiceBase',|
                                                         'mstop.jpg','/&OK')
                                          Of 1 ! &OK Button
                                          End!Case Message
                                      else ! If (MainStoreSuspended(sto:Part_Number))
                                          If VirtualSite(sto:Location)
                                              !message('virtual site')
                                              tmp:DontOK = 0
                                              Case Local.AccessoryOutOfWarranty()
                                                  Of 1
                                                      Do OrderPart
                                                      If sto:Quantity_Stock > 0
                                                          Do NonOrderPart
                                                      End !If sto:Quantity_Stock > 0
  
  
                                                      glo:Select1 = 'REMOVE PART'
                                                      glo:Select4 = wpr:Record_Number
                                                      If job:Chargeable_Job <> 'YES'
                                                          job:Chargeable_Job = 'YES'
                                                      End !If job:Chargeable_Job <> 'YES'
                                                      job:Warranty_Job = 'NO'
                                                  Of 2
                                                      Do OrderPart
                                                      IF sto:Quantity_stock > 0
                                                          Do NonOrderPart
                                                      End !IF sto:Quantity_stock > 0
  
  
  
                                                      If job:Chargeable_Job <> 'YES'
                                                          job:Chargeable_Job = 'YES'
                                                      End !If job:Chargeable_Job <> 'YES'
                                                      glo:Select1 = 'REMOVE PART'
                                                      glo:Select4 = wpr:Record_Number
                                                  Of 3
                                                      Cycle
                                                  Else
  
                                                      If sto:Quantity_Stock > 0
                                                          glo:Select1 = 'NEW PENDING WEB'
                                                          glo:Select2 = wpr:Part_Ref_Number
                                                          glo:Select3 = wpr:Quantity - sto:Quantity_Stock
                                                          wpr:Quantity    = sto:Quantity_Stock
                                                          wpr:Date_Ordered    = Today()
  
                                                      Else !If sto:Quantity_Stock > 0
                                                          
                                                          wpr:PartAllocated = 0    !TB12796 - these were the wrong way round
                                                          wpr:WebOrder    = 1
                                                          !Save the record, so it can be added to Stock Allocation - L873 (DBH: 30-07-2003)
                                                          Access:WARPARTS.Update()
                                                          AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'WEB',job:Engineer)
                                                      End !If sto:Quantity_Stock > 0
                                              End !Case Local.AccessoryOutOfWarranty
  
                                              CreateWebOrder('W',wpr:Quantity - sto:Quantity_Stock)
  
                                          Else !If job:WebJob
  
                                              tmp:CreateNewOrder = 0
                                              Case def:SummaryOrders
                                                  Of 0 !Old Way
                                                      tmp:CreateNewOrder = 1
                                                  Of 1 !New Way
                                                      !Check to see if a pending order already exists.
                                                      !If so, add to that.
                                                      !If not, create a new pending order
                                                      Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                                      ope:Supplier    = wpr:Supplier
                                                      ope:Part_Number = wpr:Part_Number
                                                      If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                          !Found
                                                          ope:Quantity += wpr:Quantity - sto:Quantity_Stock
                                                          Access:ORDPEND.Update()
  
                                                          tmp:CreateNewOrder = 0
                                                          !If there is some in stock, then use that for this part
                                                          !then pass the variables, to create another part line
                                                          !for the ordered part.
                                                          If sto:Quantity_Stock > 0
                                                              glo:Select1 = 'NEW PENDING'
                                                              glo:Select2 = wpr:Part_Ref_Number
                                                              glo:Select3 = wpr:Quantity-sto:quantity_stock
                                                              glo:Select4 = ope:Ref_Number
                                                              wpr:Quantity    = sto:Quantity_Stock
                                                              wpr:Date_Ordered    = Today()
                                                          Else !If sto:Quantity_Stock > 0
                                                              wpr:Pending_Ref_Number  =  ope:Ref_Number
                                                              wpr:Requested   = True
  
                                                          End !If sto:Quantity_Stock > 0
                                                      Else! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                                          tmp:CreateNewOrder  = 1
                                                          wpr:Requested   = True
                                                          !Error
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                      End! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
  
                                              End !Case def:SummaryOrders
  
                                              If tmp:CreateNewOrder = 1
                                                  If Access:ORDPEND.PrimeRecord() = Level:Benign
                                                      ope:Part_Ref_Number = sto:Ref_Number
                                                      ope:Job_Number      = job:Ref_Number
                                                      ope:Part_Type       = 'WAR'
                                                      ope:Supplier        = wpr:Supplier
                                                      ope:Part_Number     = wpr:Part_Number
                                                      ope:Description     = wpr:Description
  
                                                      If sto:Quantity_Stock <=0
                                                          wpr:Pending_Ref_Number  = ope:Ref_Number
                                                          ope:Quantity    = wpr:Quantity
                                                          ope:PartRecordNumber    = wpr:Record_Number
                                                      Else !If sto:Quantity_Stock <=0
                                                          glo:Select1 = 'NEW PENDING'
                                                          glo:Select2 = wpr:Part_Ref_Number
                                                          glo:Select3 = wpr:Quantity - sto:Quantity_Stock
                                                          glo:Select4 = ope:Ref_Number
                                                          ope:Quantity    = wpr:Quantity - sto:Quantity_Stock
                                                          wpr:Quantity    = sto:Quantity_Stock
                                                          wpr:Date_Ordered = Today()
                                                      End !If sto:Quantity_Stock <=0
                                                      If Access:ORDPEND.TryInsert() = Level:Benign
                                                          !Insert Successful
                                                      Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                                          !Insert Failed
                                                      End !If Access:ORDPEND.TryInsert() = Level:Benign
                                                  End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                                              End !If tmp:CreateNewOrder = 1
                                              !If the stock is relocated then we jump the gun a bit at this point
                                              !Log Number 012031 ( ARC To Request Spares directly) - JC - 25/05/12
                                              if glo:RelocateStore  and sto:Quantity_Stock <= 0 then
                                                  !message('At glo:relocate stores setting web order to true')
                                                  wpr:WebOrder    = 1
                                                  wpr:PartAllocated = 0
                                                  Access:warPARTS.Update()
                                                  AddToStockAllocation(wpr:Record_Number,'WAR',par:Quantity,'WEB',job:Engineer)
                                              END !if glo:RelocateStore
  
                                          End !If job:WebJob
                                      End
                                  End !If SecurityCheck('JOBS - ORDER PARTS')
                              Of 1 ! Re-Enter Button
                                  Select(?wpr:Quantity)
                                  Cycle
                          End ! Case Missive
                      Else !If wpr:Quantity > sto:Quantity
                          !There is sufficient in stock
                          wpr:Date_Ordered    = Today()
                          !Reget Stock
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              !If its not a Rapid site, turn part allocated off
                              Case Local.AccessoryOutOfWarranty()
                                  Of 1 !Chargeable Only
                                      glo:Select1 = 'MAKE CHARGEABLE'
                                      glo:Select4 = wpr:Record_Number
                                      If job:Chargeable_Job <> 'YES'
                                          job:Chargeable_Job = 'YES'
                                      End !If job:Chargeable_Job <> 'YES'
                                      job:Warranty_job = 'NO'
                                  Of 2 !Split
                                      glo:Select1 = 'MAKE CHARGEABLE'
                                      glo:Select4 = wpr:Record_Number
                                      If job:Chargeable_Job <> 'YES'
                                          job:Chargeable_Job = 'YES'
                                      End !If job:Chargeable_Job <> 'YES'
                                  Of 3 !Cancel
                                      Cycle
                                  Else
                                      glo:Select1 = ''
                                      glo:Select4 = ''
                              End !Case Local.AccessoryOutOfWarranty
  
                              If RapidLocation(sto:Location)
                                  wpr:PartAllocated = 0
                                  !Save the record, so it can be added to Stock Allocation - L873 (DBH: 30-07-2003)
                                  Access:WARPARTS.Update()
  
                                  AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'',job:Engineer)
                              End !If RapidLocation(sto:Location) = Level:Benign
  
                              sto:Quantity_Stock  -= wpr:Quantity
                              If sto:Quantity_Stock < 0
                                  sto:Quantity_Stock = 0
                              End !If sto:Quantity_Stock < 0
                              If Access:STOCK.Update() = Level:Benign
                                  If def:Add_Stock_Label = 'YES'
                                      glo:Select1 = sto:Ref_Number
                                      Case def:Label_Printer_Type
                                          of 'TEC B-440 / B-442'
                                              stock_request_label(Today(),Clock(),wpr:quantity,job:ref_number,job:engineer,wpr:sale_cost)
                                          of 'TEC B-452'
                                              stock_request_label_B452(Today(),Clock(),wpr:quantity,job:ref_number,job:engineer,wpr:sale_cost)
                                      End !Case def:Label_Printer_Type
                                  End !If def:Add_Stock_Label = 'YES'
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'DEC', | ! Transaction_Type
                                                     wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                     job:Ref_Number, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     wpr:Quantity, | ! Quantity
                                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                                     wpr:Sale_Cost, | ! Sale_Cost
                                                     wpr:Retail_Cost, | ! Retail_Cost
                                                     'STOCK DECREMENTED', | ! Notes
                                                     '') ! Information
                                    ! Added OK
  
                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
                                  ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
                                  SuspendedPartCheck()
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      End !If wpr:Quantity > sto:Quantity
                  Else !If sto:Sundry_Item <> 'YES'
                      !Sundry Item, do nothing
                      wpr:Date_Ordered    = Today()
                  End !If sto:Sundry_Item <> 'YES'
              Else !If tmp:StockNumber
                  !A non stock part
                  Case Missive('This is a NON-STOCK item.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish it to appear on the Next Parts Order?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          If glo:WebJob
                              CreateWebOrder('W',wpr:Quantity)
                              wpr:WebOrder = 1
                              wpr:PartAllocated = 0
                              !Save the record, so it can be added to Stock Allocation - L873 (DBH: 30-07-2003)
                              Access:WARPARTS.Update()
  
                              AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'WEB',job:Engineer)
                          Else !If glo:WebJob
                              If Access:ORDPEND.PrimeRecord() = Level:Benign
                                  ope:Job_Number      = job:Ref_Number
                                  ope:Part_Type       = 'WAR'
                                  ope:Supplier        = wpr:Supplier
                                  ope:Part_Number     = wpr:Part_Number
                                  ope:Description     = wpr:Description
                                  ope:Quantity        = wpr:Quantity
                                  ope:PartRecordNumber    = wpr:Record_Number
                                  wpr:Pending_Ref_Number  = ope:Ref_Number
                                  If Access:ORDPEND.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:ORDPEND.TryInsert() = Level:Benign
                              End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                          End !If glo:WebJob
                      Of 1 ! No Button
                          wpr:Exclude_From_Order  = 'YES'
                          wpr:Date_Ordered    = Today()
                  End ! Case Missive
  
              End !If tmp:StockNumber
          Else !If wpr:Exclude_From_Order <> 'YES'
              !excluded, do nothing
              wpr:Date_Ordered = Today()
          End !If wpr:Exclude_From_Order <> 'YES'
  
      Else !wpr:Part_Number <> 'ADJUSTMENT'
          !An ajustment, do nothing
          wpr:Date_Ordered    = Today()
      End !wpr:Part_Number <> 'ADJUSTMENT'
  End !If thiswindow.Request = InsertRecord
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If thiswindow.Request <> Insertrecord
      !Is this a part from stock?
      If wpr:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = wpr:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              tmp:StockNumber = sto:Ref_Number
  
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
      End !If wpr:Part_Ref_Number <> ''
  
      If tmp:Unallocate
  
          Pointer# = Pointer(STOCK)
          Hold(STOCK,1)
          Get(STOCK,Pointer#)
          If Errorcode() = 43
              Case Missive('The stock entry is in use by another station.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If Errorcode() = 43
          sto:Quantity_Stock += wpr:Quantity
          If Access:STOCK.Update() = Level:Benign
              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                 'REC', | ! Transaction_Type
                                 wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                 job:Ref_Number, | ! Job_Number
                                 0, | ! Sales_Number
                                 wpr:Quantity, | ! Quantity
                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                 wpr:Sale_Cost, | ! Sale_Cost
                                 wpr:Retail_Cost, | ! Retail_Cost
                                 'STOCK UNALLOCATED', | ! Notes
                                 '') ! Information
                ! Added OK
  
              Else ! AddToStockHistory
                ! Error
              End ! AddToStockHistory
              wpr:Status = 'WEB'
              wpr:PartAllocated = 0
              wpr:WebOrder = 1
              !message('Debug 3 setting weborder to true')
          End !If Access:STOCK.Update() = Level:Benign
  
      Else !If tmp:Unallocate
  
  
          !Normal part
          If wpr:Date_Ordered <> '' And wpr:Order_Number = '' and wpr:Pending_Ref_Number = ''
              If wpr:Quantity > Quantity_Temp
                  !Stock part?
                  If tmp:StockNumber
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = tmp:StockNumber
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Found
  
                          Pointer# = Pointer(STOCK)
                          Hold(STOCK,1)
                          Get(STOCK,Pointer#)
                          If Errorcode() = 43
                              Case Missive('The stock entry is in use by another station.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Cycle
                          End !If Errorcode() = 43
  
                          If sto:Quantity_Stock < (wpr:Quantity - Quantity_Temp)
                              !There isn't enough in stock
                              Case Missive('You have increased the quantity required, but there are insufficient items in stock.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to ORDER the remaining items, or RE-ENTER the quantity required?','ServiceBase 3g',|
                                             'mquest.jpg','Re-Enter|Order')
                                  Of 2 ! Order Button
                                      !Is there any existing pending part to add to?
                                      tmp:PendingPart = 0
                                      Save_war_ali_ID = Access:WARPARTS_ALIAS.SaveFile()
                                      Access:WARPARTS_ALIAS.ClearKey(war_ali:RefPartRefNoKey)
                                      war_ali:Ref_Number      = job:Ref_Number
                                      war_ali:Part_Ref_Number = tmp:StockNumber
                                      Set(war_ali:RefPartRefNoKey,war_ali:RefPartRefNoKey)
                                      Loop
                                          If Access:WARPARTS_ALIAS.NEXT()
                                             Break
                                          End !If
                                          If war_ali:Ref_Number      <> job:Ref_Number      |
                                          Or war_ali:Part_Ref_Number <> tmp:StockNumber      |
                                              Then Break.  ! End If
                                          If war_ali:Record_Number = wpr:Record_Number
                                              Cycle
                                          End !If war_ali:Record_Number = wpr:Record_Number
                                          If war_ali:Pending_Ref_Number <> ''
                                              tmp:PendingPart = war_ali:Record_Number
                                              Break
                                          End !If wpr:Pending_Ref_Number <> ''
                                      End !Loop
                                      Access:WARPARTS_ALIAS.RestoreFile(Save_war_ali_ID)
  
                                      tmp:NewQuantity     = (wpr:Quantity - Quantity_Temp) - sto:Quantity_Stock
  
                                      If tmp:PendingPart
                                          tmp:CurrentState    = Getstate(WARPARTS)
  
                                          Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                                          wpr:Record_Number   = tmp:PendingPart
                                          If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                                              !Found
                                              wpr:Quantity += tmp:NewQuantity
                                              Access:WARPARTS.Update()
  
                                              tmp:CreateNewOrder = 0
                                              !Find pending order
                                               Case def:SummaryOrders
                                                  Of 0 !Old Wauy
                                                      Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                                                      ope:Ref_Number  = wpr:Pending_Ref_Number
                                                      If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                          !Found
                                                          ope:Quantity += tmp:NewQuantity
                                                          Access:ORDPEND.Update()
  
                                                      Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                          !Error
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          Do CreateNewOrder
                                                      End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                  Of 1 !New Way
                                                      Access:ORDPEND.ClearKey(ope:Supplier_Key)
                                                      ope:Supplier    = wpr:Supplier
                                                      ope:Part_Number = wpr:Part_Number
                                                      If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                                          ope:Quantity += tmp:NewQuantity
                                                          Access:ORDPEND.Update()
                                                      Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Lev !Found
                                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          Do CreateNewOrder
  
                                                      End !If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                              End !Case def:SummaryOrders
                                              RestoreState(WARPARTS,tmp:CurrentState)
                                              FreeState(WARPARTS,tmp:CurrentState)
                                          Else! If Access:PARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:PARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
  
                                      Else !If tmp:PendingPart
                                          Do CreateNewOrder
  
                                          !Create new pending part
                                          glo:Select1 = 'NEW PENDING'
                                          glo:Select2 = wpr:Part_Ref_Number
                                          glo:Select3 = (wpr:Quantity - Quantity_Temp) - sto:Quantity_Stock
                                          glo:Select4 = ope:Ref_Number
                                          wpr:Quantity    = Quantity_Temp + sto:Quantity_Stock
  
                                      End !If tmp:PendingPart
                                  Of 1 ! Re-Enter Button
                                      wpr:Quantity    = Quantity_Temp
                                      Select(?wpr:Quantity)
                                      Cycle
                              End ! Case Missive
  
                          End !If sto:Quantity_Stock < (wpr:Quantity - Quantity_Temp)
  
                          !There is enough in stock, take it.
                          If sto:Quantity_Stock > (wpr:Quantity - Quantity_Temp)
                              Access:STOCK.Clearkey(sto:Ref_Number_Key)
                              sto:Ref_Number  = tmp:StockNumber
                              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Found
                                  sto:Quantity_Stock -= (wpr:Quantity - Quantity_Temp)
                                  If Access:STOCK.Update() = Level:Benign
                                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                         'DEC', | ! Transaction_Type
                                                         wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                         job:Ref_Number, | ! Job_Number
                                                         0, | ! Sales_Number
                                                         wpr:Quantity - Quantity_Temp, | ! Quantity
                                                         wpr:Purchase_Cost, | ! Purchase_Cost
                                                         wpr:Sale_Cost, | ! Sale_Cost
                                                         wpr:Retail_Cost, | ! Retail_Cost
                                                         'STOCK DECREMENTED', | ! Notes
                                                         '') ! Information
                                        ! Added OK
                                      Else ! AddToStockHistory
                                        ! Error
                                      End ! AddToStockHistory
                                  End !If Access:STOCK.Update() = Level:Benign
                              Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                          End !If sto:Quantity_Stock > (wpr:Quantity - Quantity_Temp)
                      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
                  End !If tmp:StockNumber
              End !If quantity increased
              !Quantity decreased
              If Quantity_Temp > wpr:Quantity
                  If tmp:StockNumber
                      !A Stock part. Put back into stock?
                      Case Missive('You have decreased the quantity required.'&|
                        '<13,10>'&|
                        '<13,10>What do you wish to do with the excess? RETURN it to stock, or SCRAP it?','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|Scrap|Return')
                          Of 3 ! Return Button
                              Case Missive('This item was originally taken from location ' & Clip(sto:Location) & '.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to return it to it''s ORIGINAL Location or to a NEW location?','ServiceBase 3g',|
                                             'mquest.jpg','\Cancel|New|Original')
                                  Of 3 ! Original Button
                                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                      sto:Ref_Number  = tmp:StockNumber
                                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                          !Found
                                          Pointer# = Pointer(STOCK)
                                          Hold(STOCK,1)
                                          Get(STOCK,Pointer#)
                                          If Errorcode() = 43
                                              Case Missive('The stock entry is in use by another station.','ServiceBase 3g',|
                                                             'mstop.jpg','/OK')
                                                  Of 1 ! OK Button
                                              End ! Case Missive
                                              Cycle
                                          End !If Errorcode() = 43
  
                                          sto:Quantity_Stock += Quantity_Temp - wpr:Quantity
                                          If Access:STOCK.Update() = Level:Benign
                                              If access:stock.update() = Level:Benign
                                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                     'ADD', | ! Transaction_Type
                                                                     wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                     job:Ref_Number, | ! Job_Number
                                                                     0, | ! Sales_Number
                                                                     Quantity_Temp - wpr:Quantity, | ! Quantity
                                                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                                                     wpr:Sale_Cost, | ! Sale_Cost
                                                                     wpr:Retail_Cost, | ! Retail_Cost
                                                                     'WARRANTY PART AMENDED ON JOB', | ! Notes
                                                                     '') ! Information
                                                    ! Added OK
  
                                                  Else ! AddToStockHistory
                                                    ! Error
                                                  End ! AddToStockHistory
                                              End!If access:stock.update = Level:Benign
                                          End !If Access:STOCK.Update() = Level:Benign
                                      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  Of 2 ! New Location
                                      glo:select1 = ''
                                      Pick_New_Location
                                      If glo:select1 <> ''
                                          access:stock.clearkey(sto:location_part_description_key)
                                          sto:location    = glo:select1
                                          sto:part_number = wpr:part_number
                                          sto:description = wpr:description
                                          If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                              sto:quantity_stock   += wpr:quantity
                                              access:stock.update()
                                              do_delete# = 1
                                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'ADD', | ! Transaction_Type
                                                                 wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                 job:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 wpr:Quantity, | ! Quantity
                                                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                                                 wpr:Sale_Cost, | ! Sale_Cost
                                                                 wpr:Retail_Cost, | ! Retail_Cost
                                                                 'WARRANTY PART AMENDED ON JOB', | ! Notes
                                                                 '') ! Information
                                                ! Added OK
                                              Else ! AddToStockHistory
                                                ! Error
                                              End ! AddToStockHistory
                                          Else!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                              Case Missive('Cannot find the selected part in location ' & Clip(glo:Select1) & '.'&|
                                                '<13,10>'&|
                                                '<13,10>Do you wish to add this part as a NEW item, or SCRAP it?','ServiceBase 3g',|
                                                             'mquest.jpg','\Cancel|Scrap|New')
                                                  Of 3 ! New Button
                                                      glo:select2  = ''
                                                      glo:select3  = ''
                                                      Pick_Locations
                                                      If glo:select1 <> ''
                                                          do_delete# = 1
                                                          Get(stock,0)
                                                          If access:stock.primerecord() = Level:Benign
                                                              sto:part_number = wpr:part_number
                                                              sto:description = wpr:description
                                                              sto:supplier    = wpr:supplier
                                                              sto:purchase_cost   = wpr:purchase_cost
                                                              sto:sale_cost   = wpr:sale_cost
                                                              sto:shelf_location  = glo:select2
                                                              sto:manufacturer    = job:manufacturer
                                                              sto:location    = glo:select1
                                                              sto:second_location = glo:select3
                                                              If access:stock.insert() = Level:Benign
                                                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                     'ADD', | ! Transaction_Type
                                                                                     wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                                     job:Ref_Number, | ! Job_Number
                                                                                     0, | ! Sales_Number
                                                                                     wpr:Quantity, | ! Quantity
                                                                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                                                                     wpr:Sale_Cost, | ! Sale_Cost
                                                                                     wpr:Retail_Cost, | ! Retail_Cost
                                                                                     'WARRANTY PART AMENDED ON JOB', | ! Notes
                                                                                     '') ! Information
                                                                    ! Added OK
                                                                  Else ! AddToStockHistory
                                                                    ! Error
                                                                  End ! AddToStockHistory
                                                              End!If access:stock.insert() = Level:Benign
                                                          End!If access:stock.primerecord() = Level:Benign
                                                      End!If glo:select1 = ''
                                                  Of 2 ! Scrap Button
                                                  Of 1 ! Cancel Button
                                              End ! Case Missive
                                          End !If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                      End !If glo:select1 <> ''
                                  Of 1 ! Cancel Button
                              End ! Case Missive
                          Of 2 ! Scrap Button
                          Of 1 ! Cancel Button
                      End ! Case Missive
                  Else !If tmp:StockNumber
  
                  End !If tmp:StockNumber
              End !If Quantity_Temp > wpr:Quantity
          End !If wpr:Date_Ordered <> '' And wpr:Order_Number = '' and wpr:Pending_Ref_Number = ''
  
          !This is a pending part
          If wpr:Pending_Ref_Number <> '' and wpr:Order_Number = ''
              !Quantity increased
              If wpr:Quantity > Quantity_Temp
                  !Find a pending order
                  Case def:SummaryOrders
                      Of 0 !Old Way
                          Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                          ope:Ref_Number  = wpr:Pending_Ref_Number
                          If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Found
  
                              ope:Quantity += wpr:Quantity - Quantity_Temp
                              Access:ORDPEND.Update()
                          Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                              !Cannot find a pending order, lets' make one
                              tmp:NewQuantity = wpr:Quantity - Quantity_Temp
                              Do CreateNewOrder
                              !Create new pending part
                              glo:Select1 = 'NEW PENDING'
                              glo:Select2 = wpr:Part_Ref_Number
                              glo:Select3 = (wpr:Quantity - Quantity_Temp)
                              glo:Select4 = ope:Ref_Number
                              wpr:Quantity   = Quantity_Temp
  
                          End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
  
                      Of 1 !New Way
                          Access:ORDPEND.ClearKey(ope:Supplier_Key)
                          ope:Supplier    = wpr:Supplier
                          ope:Part_Number = wpr:Part_Number
                          If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                              !Found
                              ope:Quantity += wpr:Quantity - Quantity_Temp
                              Access:ORDPEND.Update()
                          Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                              tmp:NewQuantity = wpr:Quantity - Quantity_Temp
                              Do CreateNewOrder
                          End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                  End !Case def:SummaryOrders
              End !If wpr:Quantity > Quantity_Temp
              !Quantity decreased
              If wpr:Quantity < Quantity_Temp
                  !Find a pending order
                  Case def:SummaryOrders
                      Of 0 !Old Way
                          Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                          ope:Ref_Number  = wpr:Pending_Ref_Number
                          If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Found
                              ope:Quantity -= (Quantity_Temp - wpr:Quantity)
                              If ope:Quantity <= 0
                                  Delete(ORDPEND)
                              Else !If ope:Quantity <= 0
                                  Access:ORDPEND.Update()
                              End !If ope:Quantity <= 0
  
                          Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
  
                      Of 1 !New Way
                          Access:ORDPEND.Clearkey(ope:Supplier_Key)
                          ope:Supplier    = wpr:Supplier
                          ope:Part_Number = wpr:Part_Number
                          If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                              !Found
                              ope:Quantity -= (Quantity_Temp - wpr:Quantity)
                              If ope:Quantity <= 0
                                  Delete(ORDPEND)
                              Else !If ope:Quantity <= 0
                                  Access:ORDPEND.Update()
                              End !If ope:Quantity <= 0
                          Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
  
                  End !Case def:SummaryOrders
              End !If wpr:Quantity < Quantity_Temp
          End !If wpr:Pending_Ref_Number <> '' and wpr:Order_Number = ''
  
      End !If tmp:Unallocate
  
  End !If thiswindow.Request <> Insertrecord
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:InWarrantyMarkup
    Do EnableDisableCosts
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Do Lookup_Location
      Do LookupMainFault
      Display()
      !Popup Stock Button - lookupmainfault
      If wpr:Part_Number = ''
          Post(Event:Accepted,?Browse_Stock_Button)
      End !wpr:Part_Number = ''
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.GetJobFaultCode        Procedure(Long f:FaultCodeNumber)
Code
    Case f:FaultCodeNumber
    Of 1
        Return job:Fault_Code1
    Of 2
        Return job:Fault_Code2
    Of 3
        Return job:Fault_Code3
    Of 4
        Return job:Fault_Code4
    Of 5
        Return job:Fault_Code5
    Of 6
        Return job:Fault_Code6
    Of 7
        Return job:Fault_Code7
    Of 8
        Return job:Fault_code8
    Of 9
        Return job:Fault_Code9
    Of 10
        Return job:Fault_code10
    Of 11
        Return job:Fault_Code11
    Of 12
        Return job:Fault_Code12
    Of 13
        Return wob:FaultCode13
    Of 14
        Return wob:FaultCode14
    Of 15
        Return wob:FaultCode15
    Of 16
        Return wob:FaultCode16
    Of 17
        Return wob:FaultCode17
    Of 18
        Return wob:FaultCode18
    Of 19
        Return wob:FaultCode19
    Of 20
        Return wob:FaultCode20
    End ! Case f:FaultCodeNumber
local.LookupFaultCode        Procedure(Long f:FieldNumber, String f:FaultCode)
local:JobType       Byte(0)
local:PartType      Byte(0)
! Deleting (DBH 24/04/2008) # 9723 - Not needed
!BlankQueue          Queue(DefLinkedFaultCodeQueue)
!                    End
! End (DBH 24/04/2008) #9723
local:ReturnValue   String(30)
local:LinkedRecordNumber      Long(0)
local:MainJobFaultNumber      Long(0)
Code
    local:ReturnValue = f:FaultCode

! Changing (DBH 21/05/2008) # 9723 - If a part is a correct, then it's also an adjustment
!    If wpr:Adjustment = 'YES'
!        local:PartType = 1
!    Else ! If wpr:Adjustment = 'YES'
!        If wpr:Correction
!            local:PartType = 2
!        End ! If wpr:Correction
!    End ! If wpr:Adjustment = 'YES'
! to (DBH 21/05/2008) # 9723
    If wpr:Correction
        local:PartType = 2
    Else ! If wpr:Correction
        If wpr:Adjustment = 'YES'
            local:PartType = 1
        End ! If wpr:Adjustment = 'YES'
    End ! If wpr:Correction
! End (DBH 21/05/2008) #9723

    ! Used to restrict the lookup to char/warr (DBH: 23/10/2007)
    If job:Chargeable_Job = 'YES'
        local:JobType = 1
        If job:Warranty_Job = 'YES'
            local:JobType = 0
        End ! If job:Warranty_Job = 'YES'
    Else ! If job:Chargeable_Job = 'YES'
        local:JobType = 2
    End ! If job:Chargeable_Job = 'YES'

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = job:Manufacturer
    map:ScreenOrder = f:FieldNumber
    If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign

        If map:MainFault
            ! This is the main fault, validate against the Job Main Fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault = 1
            If Access:MANFAULT.Tryfetch(maf:MainFaultKey) = Level:Benign
                local:MainJobFaultNumber = maf:Field_Number
                Request# = GlobalRequest
                GlobalRequest = SelectRecord

                ! Use restricted Browse?
                ! Inserting (DBH 14/05/2008) # 9723 - There may be restrictions
                Do LookupRelatedFaultCodes
                ! End (DBH 14/05/2008) #9723

                If map:UseRelatedJobCode <> 0
                    BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,map:Field_Number,local:PartType,PassLinkedFaultCodeQueue)
                Else ! If map:UseRelatedJobCode <> 0
                    BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,'',local:PartType,PassLinkedFaultCodeQueue)
                End ! If map:UseRelatedJobCode <> 0

                GlobalRequest = Request#

                If GlobalResponse = RequestCompleted
                    local:ReturnValue = mfo:Field
                End ! If GlobalResponse = RequestCompleted
            Else ! If Access:MANFAULT.Clearkey(maf:MainFaultKey) = Level:Benign
                !Error
                Case Missive('This fault code is set as the "Main Fault" but there is no corresponding "Main Fault" for the job fault codes.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If Access:MANFAULT.Clearkey(maf:MainFaultKey) = Level:Benign
        Else ! If map:MainFault
            Found# = 0
            ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
            If sto:Assign_Fault_Codes = 'YES'
                Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                stm:Ref_Number   = sto:Ref_Number
                stm:Manufacturer = job:Manufacturer
                stm:Model_Number = job:Model_Number
                If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                    Case map:Field_Number
                    Of 1
                        If stm:FaultCode1 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 2
                        If stm:FaultCode2 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 3
                        If stm:FaultCode3 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 4
                        If stm:FaultCode4 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 5
                        If stm:FaultCode5 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 6
                        If stm:FaultCode6 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 7
                        If stm:FaultCode7 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 8
                        If stm:FaultCode8 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 9
                        If stm:FaultCode9 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 10
                        If stm:FaultCode10 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 11
                        If stm:FaultCode11 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    Of 12
                        If stm:FaultCode12 = '** MULTIPLE VALUES **'
                            Found# = 1
                        End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                    End ! Case map:Field_Number
                    If Found# = 1
                        Request# = GlobalRequest
                        GlobalRequest = SelectRecord
                        BrowseStockFaultCodeLookup(stm:RecordNumber,map:Field_Number)
                        GlobalRequest = Request#
                        If GlobalResponse = RequestCompleted
                            local:ReturnValue = stu:Field
                        End ! If GlobalReponse = RequestCompleted
                    End ! If Found# = 1
                End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
            End ! If sto:Assign_Fault_Codes = 'YES'

            If Found# = 0
! Changing (DBH 24/04/2008) # 9723 - Lookup for linked fault codes
!                ! Validate against Part Codes
!                Request# = GlobalRequest
!                GlobalRequest = SelectRecord
!                BrowsePartFaultCodeLookup(job:Manufacturer,map:Field_Number,local:PartType,local:JobType)
!                GlobalRequest = Request#
!                If GlobalResponse = RequestCompleted
!                    local:ReturnValue = mfp:Field
!                End ! If GlobalResponse = RequestCompleted
! to (DBH 24/04/2008) # 9723
                Do LookupRelatedFaultCodes

                Request# = GlobalRequest
                GlobalRequest = SelectRecord
                BrowsePartFaultCodeLookup(job:Manufacturer,map:Field_Number,local:PartType,local:JobType,PassLinkedFaultCodeQueue)
                GlobalRequest = Request#
                If GlobalResponse = RequestCompleted

                    ! A new fault code has been picked. Check the secondary fault codes (DBH: 24/04/2008)
                    If f:FaultCode <> mfp:Field
                        Loop num# = 1 To 12
                            If num# = f:FieldNumber
                                Cycle
                            End ! If num# = f:FieldNumber

                            field:ScreenOrder = num#
                            Get(FieldNumbersQueue,field:ScreenOrder)
                            If ~Error()
                                If field:FieldNumber{prop:Hide} = 1
                                    Cycle
                                End ! If field:FieldNumber{prop:Hide} = 1
                            End ! If ~Error()

                            Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
                            map_ali:Manufacturer = job:Manufacturer
                            map_ali:ScreenOrder = num#
                            If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey)  = Level:Benign
                                ! OK

                            Else ! If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                                ! Error
                                Cycle
                            End ! If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign

                            ! Count how many secondary values. And if only one, auto fill (DBH: 24/04/2008)
                            Count# = 0
                            Access:MANFPARL.Clearkey(mpr:FieldKey)
                            mpr:MANFPALORecordNumber = mfp:RecordNumber
                            mpr:FieldNumber = map_ali:Field_Number
                            Set(mpr:FieldKey,mpr:FieldKey)
                            Loop
                                If Access:MANFPARL.Next()
                                    Break
                                End ! If Access:MANFPARL.Next()
                                If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                    Break
                                End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                If mpr:FieldNumber <> map_ali:Field_Number
                                    Break
                                End ! If mpr:FieldNumber <> map_ali:Field_Number

                                local:LinkedRecordNumber = mpr:LinkedRecordNumber
                                Count# += 1
                                If Count# > 1
                                    Break
                                End ! If Count# > 1
                            End ! Loop

                            If Count# = 1
                                Access:MANFPALO_ALIAS.Clearkey(mfp_ali:RecordNumberKey)
                                mfp_ali:RecordNumber = local:LinkedRecordNumber
                                If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:RecordNumberKey) = Level:Benign
                                    ! Found
                                    tmp:FaultCode[num#] = mfp_ali:Field
                                Else ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:RecordNumberKey) = Level:Benign
                                End ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:RecordNumberKey) = Level:Benign
                            Elsif Count# > 1
                                ! Blank, the user must select a new code
                                tmp:FaultCode[num#] = ''
                                Display()
                            End ! If Count# = 1
                        End ! Loop num# = 1 To 12
                    End ! If f:Field <> mfp:Field

                    local:ReturnValue = mfp:Field
                End ! If GlobalReponse = RequestCompleted
! End (DBH 24/04/2008) #9723
            End ! If Found# = 0
        End ! If map:MainFault

        If map:KeyRepair
            If local:ReturnValue = 1
                ! This is a key repair fault code. Check no other parts have been marked as Key Repair (DBH: 09/11/2007)
                Found# = 0
                Access:WARPARTS_ALIAS.Clearkey(war_ali:Part_Number_Key)
                war_ali:Ref_Number = job:Ref_Number
                Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                Loop
                    If Access:WARPARTS_ALIAS.Next()
                        Break
                    End ! If Access:WARPARTS_ALIAS.Next()
                    If war_ali:Ref_Number <> job:Ref_Number
                        Break
                    End ! If war_ali:Ref_Number <> job:Ref_Number
                    If war_ali:Record_Number = wpr:Record_Number
                        Cycle
                    End ! If war_ali:RecordNumber = war:RecordNumber
                    Case map:Field_Number
                    Of 1
                        If war_ali:Fault_Code1 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 2
                        If war_ali:Fault_Code2 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 3
                        If war_ali:Fault_Code3 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 4
                        If war_ali:Fault_Code4 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 5
                        If war_ali:Fault_Code5 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 6
                        If war_ali:Fault_Code6 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 7
                        If war_ali:Fault_Code7 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 8
                        If war_ali:Fault_Code8 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 9
                        If war_ali:Fault_Code9 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 10
                        If war_ali:Fault_Code10 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 11
                        If war_ali:Fault_Code11 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    Of 12
                        If war_ali:Fault_Code12 = 1
                            Found# = 1
                        End ! If wpr:Fault_Code1 = 1
                    End ! Case map:Field_Number
                    If Found# = 1
                        Case Missive('Part Number ' & Clip(war_ali:Part_Number) & ' has already been marked as "Key Repair".','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        local:ReturnValue = f:FaultCode
                        Break
                    End ! If Found# = 1
                End ! Loop
                If Found# = 0
                    Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
                    par_ali:Ref_Number = job:Ref_Number
                    Set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
                    Loop
                        If Access:PARTS_ALIAS.Next()
                            Break
                        End ! If Access:PARTS_ALIAS.Next()
                        If par_ali:Ref_Number <> job:Ref_Number
                            Break
                        End ! If par_ali:Ref_Number <> job:Ref_Number
                        If par_ali:Record_Number = par:Record_Number
                            Cycle
                        End ! If par_ali:RecordNumber = war:RecordNumber
                        Case map:Field_Number
                        Of 1
                            If par_ali:Fault_Code1 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 2
                            If par_ali:Fault_Code2 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 3
                            If par_ali:Fault_Code3 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 4
                            If par_ali:Fault_Code4 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 5
                            If par_ali:Fault_Code5 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 6
                            If par_ali:Fault_Code6 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 7
                            If par_ali:Fault_Code7 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 8
                            If par_ali:Fault_Code8 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 9
                            If par_ali:Fault_Code9 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 10
                            If par_ali:Fault_Code10 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 11
                            If par_ali:Fault_Code11 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        Of 12
                            If par_ali:Fault_Code12 = 1
                                Found# = 1
                            End ! If par:Fault_Code1 = 1
                        End ! Case map:Field_Number
                        If Found# = 1
                            Case Missive('Part Number ' & Clip(par_ali:Part_Number) & ' has already been marked as "Key Repair".','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            local:ReturnValue = f:FaultCode
                            Break
                        End ! If Found# = 1
                    End ! Loop
                End ! If Found# = 0

            End ! If local:ReturnValue = 1
        End ! If map:KeyRepair
    End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
    Return local:ReturnValue


LookupRelatedFaultCodes       Routine
Data
local:FaultCodeValue            String(255)
Code
    ! Go through all the fault codes to see if THIS fault code is a secondary linked code (DBH: 24/04/2008)
    Free(PassLinkedFaultCodeQueue)
    Loop num# = 1 to 12
        If num# = f:FieldNumber
            Cycle
        End ! If num# = f:FieldNumber

        ! Only count visible fault codes (DBH: 24/04/2008)
        field:ScreenOrder = num#
        Get(FieldNumbersQueue,field:ScreenOrder)
        If ~Error()
            If field:FieldNumber{prop:Hide} = 1
                Cycle
            End ! If field:FieldNumber{prop:Hide} = 1
        End ! If ~Error()

        If tmp:FaultCode[num#] <> ''
            ! Get the record number of the fault code (DBH: 24/04/2008)
            Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
            map_ali:Manufacturer = job:Manufacturer
            map_ali:ScreenOrder = num#
            If Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                ! OK
                If map_ali:UseRelatedJobCode
                    Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
                    mfo_ali:Manufacturer = job:Manufacturer
                    mfo_ali:RelatedPartCode = map_ali:Field_Number
                    mfo_ali:Field_Number = maf:Field_Number
                    mfo_ali:Field = tmp:FaultCode[num#]
                    If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                        ! Found
                        Found# = 0
                        Access:MANFAURL.Clearkey(mnr:FieldKey)
                        mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                        mnr:PartFaultCode = 0
                        mnr:FieldNumber = maf:Field_Number
                        Set(mnr:FieldKey,mnr:FieldKey)
                        Loop
                            If Access:MANFAURL.Next()
                                Break
                            End ! If Access:MANFAURL.Next()
                            If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                Break
                            End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                            If mnr:PartFaultCode <> 0
                                Break
                            End ! If mnr:PartFaultCOde <> 1
                            If mnr:FIeldNUmber <> maf:Field_Number
                                Break
                            End ! If mnr:FIeldNUmber <> maf:Field_Number
                            If mnr:RelatedPartFaultCode > 0
                                If mnr:RelatedPartFaultCode <> map:Field_Number
                                    Cycle
                                End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
                            End ! If mnr:RelaterdPartFaultCode > 0
                            Found# = 1
                            Break
                        End ! Loop (MANFAURL)
                        If Found# = 1
                            PassLinkedFaultCodeQueue.RecordNumber = mfo_ali:RecordNumber
                            PassLinkedFaultCodeQueue.FaultType = 'J'
                            Add(PassLinkedFaultCodeQueue)
                        End ! If Found# = 1
                    Else ! If Access:MANFALO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                    End ! If Access:MANFALO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                Else ! If map_ali:UseRelatedJobCode
                    Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
                    mfp_ali:Manufacturer = job:Manufacturer
                    mfp_ali:Field_Number = map_ali:Field_Number
                    mfp_ali:Field = tmp:FaultCode[num#]
                    If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign
                        ! OK
                        Found# = 0
                        Access:MANFPARL.Clearkey(mpr:FieldKey)
                        mpr:MANFPALORecordNumber = mfp_ali:RecordNumber
                        mpr:JobFaultCode = 0
                        mpr:FieldNumber = map:Field_Number
                        Set(mpr:FieldKey,mpr:FieldKey)
                        Loop
                            If Access:MANFPARL.Next()
                                Break
                            End ! If Access:MANFPARL.Next()
                            If mpr:MANFPALORecordNumber <> mfp_ali:RecordNumber
                                Break
                            End ! If mpr:MANFPALORecordNumber <> mfp_ali:RecordNumber
                            If mpr:JobFaultCode <> 0
                                Break
                            End ! If mpr:JobFaultCode <> 0
                            If mpr:FieldNumber <> map:Field_Number
                                Break
                            End ! If mpr:FieldNumber <> map:Field_Number
                            Found# = 1
                            Break
                        End ! Loop
                        If Found# = 1
                            PassLinkedFaultCodeQueue.RecordNumber = mfp_ali:RecordNumber
                            PassLinkedFaultCodeQueue.FaultType = 'P'
                            Add(PassLinkedFaultCodeQueue)
                        End ! If Found# = 1

                    Else ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign
                        ! Error
                    End ! If Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign
                End ! If map_ali:UseRelatedJobCode
            Else ! If Access:MANFAUPA.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
                ! Error
            End ! If Access:MANFAUPA.TryFetch(map_ali:ScreenOrderKey) = Level:Benign
        End ! If tmp:FaultCode[num#] <> ''
    End ! Loop num# = 1 to 12

    Loop f# = 1 To 20
        Case f#
        Of 1
            If job:Fault_Code1 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code1
        Of 2
            If job:Fault_Code2 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code2
        Of 3
            If job:Fault_Code3 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code3
        Of 4
            If job:Fault_Code4 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code4
        Of 5
            If job:Fault_Code5 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code5
        Of 6
            If job:Fault_Code6 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code6
        Of 7
            If job:Fault_Code7 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code7
        Of 8
            If job:Fault_Code8 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code8
        Of 9
            If job:Fault_Code9 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code9
        Of 10
            If job:Fault_Code10 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code10
        Of 11
            If job:Fault_Code11 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code11
        Of 12
            If job:Fault_Code12 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = job:Fault_Code12
        Of 13
            If wob:FaultCode13 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode13
        Of 14
            If wob:FaultCode14 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode14
        Of 15
            If wob:FaultCode15 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode15
        Of 16
            If wob:FaultCode16 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode16
        Of 17
            If wob:FaultCode17 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode17
        Of 18
            If wob:FaultCode18 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode18
        Of 19
            If wob:FaultCode19 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode19
        Of 20
            If wob:FaultCode20 = ''
                Cycle
            End ! If job:Fault_Code1 = ''
            local:FaultCodeValue = wob:FaultCode20
        End ! Case f#

        save_maf_id = Access:MANFAULT.SaveFile()
        Access:MANFAULT.Clearkey(maf:Field_Number_Key)
        maf:Manufacturer = job:Manufacturer
        maf:Field_Number = f#
        If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
            ! Found
            If maf:Lookup = 'YES'
                save_mfo_id = Access:MANFAULO.SaveFile()
                Access:MANFAULO.Clearkey(mfo:Field_Key)
                mfo:Manufacturer = job:Manufacturer
                mfo:Field_Number = maf:Field_Number
                mfo:Field = local:FaultCodeValue
                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    ! Found
                    Found# = 0
                    Access:MANFAURL.Clearkey(mnr:FieldKey)
                    mnr:MANFAULORecordNumber = mfo:RecordNumber
                    If map:UseRelatedJobCode
                        mnr:PartFaultCode = 0
                        mnr:FieldNumber = local:MainJobFaultNumber
                    Else ! If map:UseRelatedJobCode
                        mnr:PartFaultCode = 1
                        mnr:FieldNumber = map:Field_Number
                    End ! If map:UseRelatedJobCode
                    Set(mnr:FieldKey,mnr:FieldKey)
                    Loop
                        If Access:MANFAURL.Next()
                            Break
                        End ! If Access:MANFAURL.Next()
                        If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                            Break
                        End ! If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                        If map:UseRelatedJobCode
                            If mnr:PartFaultCode <> 0
                                Break
                            End ! If mnr:PartFaultCode <> 0
                            If mnr:FieldNumber <> local:MainJobFaultNumber
                                Break
                            End ! If mnr:FieldNumber <> local:MainFaultCodeNumber
                            If mnr:RelatedPartFaultCode <> map:Field_Number
                                Cycle
                            End ! If mnr:RelatedJobCode <> map:Field_Number
                        Else ! If map:UseRelatedJobCode
                            If mnr:PartFaultCode <> 1
                                Break
                            End ! If mnr:PartFaultCode <> 1
                            If mnr:FieldNumber <> map:Field_Number
                                Break
                            End ! If mnr:FieldNumber <> map:Field_Number
                        End ! If map:UseRelatedJobCode
                        Found# = 1
                        Break
                    End ! Loop (MANFAURL)
                    If Found# = 1
                        PassLinkedFaultCodeQueue.RecordNumber = mfo:RecordNumber
                        PassLinkedFaultCodeQueue.FaultType = 'J'
                        Add(PassLinkedFaultCodeQueue)
                    End ! If Found# = 1
                End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                Access:MANFAULO.RestoreFile(save_mfo_id)
            End ! If maf:Lookup = 'YES'
        Else ! If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
        End ! If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
        Access:MANFAULT.RestoreFile(save_maf_id)
    End ! Loop f# = 1 To 20

Local.LookupFaultCodeField      Procedure(Long func:FieldNumber,String func:Field)
Code
    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = job:Manufacturer
    map:ScreenOrder  = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
        !Found
        If map:Lookup <> 'YES'
            Return Level:Benign
        End ! If map:Lookup <> 'YES'
        ! Inserting (DBH 28/11/2007) # 9585 - Don't check lookup, if not forced
        If map:Force_Lookup <> 'YES'
            Return Level:Benign
        End ! If map:Force_Lookup <> 'YES'
        ! End (DBH 28/11/2007) #9585
        If map:MainFault
            !Ok, validate the field against the Job Main Fault
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                If map:Force_Lookup = 'YES'
                    If map:UseRelatedJobCode <> 0
                        Access:MANFAULO.ClearKey(mfo:RelatedFieldKey)
                        mfo:Manufacturer    = job:Manufacturer
                        mfo:RelatedPartCode = map:Field_Number
                        mfo:Field_Number    = maf:Field_Number
                        mfo:Field           = func:Field
                        If Access:MANFAULO.TryFetch(mfo:RelatedFieldKey) = Level:Benign
                            !Found
                        Else!If Access:MANFAULO.TryFetch(mfo:RelatedFieldKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            Return Level:Fatal
                        End!If Access:MANFAULO.TryFetch(mfo:RelatedFieldKey) = Level:Benign
                    Else !If map:UseRelatedJobCode <> 0
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = func:Field

                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Lev   !Found
                            Return Level:Fatal
                        End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End !If map:UseRelatedJobCode <> 0

                End !If map:Force_Lookup = 'YES'

            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                Case Missive('This fault code is set as the "Main Fault" but there is no corresponding "Main Fault" for the job fault codes.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault
            Access:MANFPALO.Clearkey(mfp:Field_Key)
            mfp:Manufacturer    = job:Manufacturer
            mfp:Field_Number    = map:Field_Number
            mfp:Field           = func:Field
            If Access:MANFPALO.Tryfetch(mfp:Field_Key) = Level:Benign
                !Found

            Else ! If Access:MANFPALO.Tryfetch(mfp:Field_Key) = Level:Benign
                !Error
                Return Level:Fatal
            End !If Access:MANFPALO.Tryfetch(mfp:Field_Key) = Level:Benign
        End !If map:MainFault
    Else ! If Access:MANFAUPA.Tryfetch(map:Field_Number_Key) = Level:Benign
        !Error
    End !If Access:MANFAUPA.Tryfetch(map:Field_Number_Key) = Level:Benign
    Return Level:Benign
Local.AccessoryOutOfWarranty        Procedure()
Code
    If sto:Accessory = 'YES'
        !Is this job outside the Accessory Warranty Period?
        If job:DOP <> ''
            !Count the warranty period from the date the job was booked - L880 (DBH: 17-07-2003)
            If job:Date_Booked > job:DOP + sto:AccWarrantyPeriod
                !Outside the Accessory Period
                Case Missive('This accessory is out of warranty. It must be a Chargeable Part.'&|
                  '<13,10>'&|
                  '<13,10>Do you wish to mark the job CHARGEABLE only or SPLIT?','ServiceBase 3g',|
                               'mquest.jpg','\Cancel|Split|Chargeable')
                    Of 3 ! Chargeable Button
                        !Are there Warranty Parts already attached?
                        FoundOtherParts# = 0
                        Access:WARPARTS_ALIAS.ClearKey(war_ali:Part_Number_Key)
                        war_ali:Ref_Number  = job:Ref_Number
                        Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                        Loop
                            If Access:WARPARTS_ALIAS.NEXT()
                               Break
                            End !If
                            If war_ali:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            If war_ali:Record_Number <> wpr:Record_Number
                                FoundOtherParts# = 1
                                Break
                            End !If war_ali:RecordNumber <> war:RecordNumber
                        End !Loop
                        If FoundOtherParts#
                            Case Missive('Cannot mark Chargeable Only. There are other Warranty Parts attached to this job.'&|
                              '<13,10>'&|
                              '<13,10>Do you wish to mark this job SPLIT?','ServiceBase 3g',|
                                           'mquest.jpg','\Cancel|/Split')
                                Of 2 ! Split Button
                                    Return 2 !Make split job
                                Of 1 ! Cancel Button
                                    Return 3 !Cancel, but do not cancel screen
                            End ! Case Missive
                        Else
                            Return 1 !Make Chargeable Only
                        End !If FoundOtherParts#
                    Of 2 ! Split Button
                        Return 2 !Make Split Job
                    Of 1 ! Cancel Button
                        Return 3 !Cancel screen
                End ! Case Missive
            End !If Today() > job:DOP + sto:AccWarrantyPeriod
        End !If job:DOP <> ''
    End !If sto:Accessory = 'YES'
    Return 0
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

