

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01014.INC'),ONCE        !Local module procedure declarations
                     END


StatusEmailDefaults PROCEDURE                         !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW4::View:Browse    VIEW(STARECIP)
                       PROJECT(str:RecipientType)
                       PROJECT(str:RecordNumber)
                       PROJECT(str:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
str:RecipientType      LIKE(str:RecipientType)        !List box control field - type derived from field
str:RecordNumber       LIKE(str:RecordNumber)         !Primary key field - type derived from field
str:RefNumber          LIKE(str:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Email Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Sender Email Address'),AT(68,58),USE(?sts:SenderEmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(160,58,124,10),USE(sts:SenderEmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Sender Email Address'),TIP('Sender Email Address')
                           PROMPT('Email Subject'),AT(68,74),USE(?sts:EmailSubject:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(160,74,184,10),USE(sts:EmailSubject),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Subject'),TIP('Email Subject')
                           LIST,AT(160,90,148,102),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Recipient Type~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(312,114),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(312,140),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                           BUTTON,AT(312,166),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                           GROUP('Reference Fields To Include On Email'),AT(428,84,172,108),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Manufacturer'),AT(464,97),USE(sts:RefManufacturer,,?sts:RefManufacturer:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Manufacturer'),TIP('Manufacturer'),VALUE('1','0')
                             CHECK('Model Number'),AT(464,113),USE(sts:RefManufacturer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Model Number'),TIP('Model Number'),VALUE('1','0')
                             CHECK('Labour Cost'),AT(464,129),USE(sts:RefModelNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Labour Cost'),TIP('Labour Cost'),VALUE('1','0')
                             CHECK('Parts Cost'),AT(464,145),USE(sts:RefLabourCost),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Parts Cost'),TIP('Parts Cost'),VALUE('1','0')
                             CHECK('Total Cost'),AT(464,161),USE(sts:RefPartsCost),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Total Cost'),TIP('Total Cost'),VALUE('1','0')
                             CHECK('Estimated Cost'),AT(464,177),USE(sts:RefTotalCost),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Estimated Cost'),TIP('Estimated Cost'),VALUE('1','0')
                           END
                           PROMPT('Email Body'),AT(68,196),USE(?Prompt3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(160,196,304,68),USE(sts:EmailBody),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Email Body')
                           PROMPT('Email Footer'),AT(68,268),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(160,268,304,40),USE(sts:EmailFooter),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Email Footer')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Email Defaults'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020261'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StatusEmailDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sts:SenderEmailAddress:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:RECIPTYP.Open
  Relate:STARECIP.Open
  Access:STATUS.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:STARECIP,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,str:RecipientTypeKey)
  BRW4.AddRange(str:RefNumber,sts:Ref_Number)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,str:RecipientType,1,BRW4)
  BRW4.AddField(str:RecipientType,BRW4.Q.str:RecipientType)
  BRW4.AddField(str:RecordNumber,BRW4.Q.str:RecordNumber)
  BRW4.AddField(str:RefNumber,BRW4.Q.str:RefNumber)
  BRW4.AskProcedure = 1
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
    Relate:STARECIP.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateStatusRecipients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      SaveRequest#      = GlobalRequest
      GlobalResponse    = RequestCancelled
      GlobalRequest     = SelectRecord
      PickRecipientTypes
      If Globalresponse = RequestCompleted
          If Access:STARECIP.PrimeRecord() = Level:Benign
              str:RefNumber   = sts:Ref_Number
              str:RecipientType   = rec:RecipientType
              If Access:STARECIP.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:STARECIP.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:STARECIP.TryInsert() = Level:Benign
              Brw4.ResetSort(1)
          End !If Access:STARECIP.PrimeRecord() = Level:Benign
      End
      GlobalRequest     = SaveRequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020261'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020261'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020261'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Insert_STATUS PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
tmp:status           STRING(26)
tmp:heading          STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sts:Heading_Ref_Number
sth:Ref_Number         LIKE(sth:Ref_Number)           !List box control field - type derived from field
sth:Heading            LIKE(sth:Heading)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(STAHEAD)
                       PROJECT(sth:Ref_Number)
                       PROJECT(sth:Heading)
                     END
History::sts:Record  LIKE(sts:RECORD),STATIC
QuickWindow          WINDOW('Update the STATUS File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           GROUP,AT(236,100,244,124),USE(?SystemGroup),DISABLE
                             PROMPT('Heading Ref Number'),AT(168,108),USE(?STS:Heading:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             COMBO(@n3),AT(264,108,64,10),USE(sts:Heading_Ref_Number),IMM,VSCROLL,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,FORMAT('19L(2)|M@s3@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                             PROMPT('Ref Number'),AT(168,124),USE(?Prompt9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             SPIN(@n3),AT(264,124,64,10),USE(sts:Ref_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                             PROMPT('Do NOT include a number prefix'),AT(396,140),USE(?Prompt8),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Status Name'),AT(168,140,72,16),USE(?STS:Status:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s26),AT(264,140,127,10),USE(tmp:status),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                             PROMPT('Description Of Status'),AT(168,156),USE(?STS:Notes:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             TEXT,AT(264,156,232,52),USE(sts:Notes),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                             PROMPT('Type Of Status'),AT(168,212),USE(?STS:Notes:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             CHECK('Loan Status'),AT(264,212),USE(sts:Loan),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Exchange Status'),AT(328,212),USE(sts:Exchange),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Job Status'),AT(404,212),USE(sts:Job),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           END
                           CHECK('Display In Engineer''s Browse'),AT(264,228),USE(sts:EngineerStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Display In Engineer''s Browse'),TIP('Display In Engineer''s Browse'),VALUE('1','0')
                           CHECK('Use Turnaround Time'),AT(264,242,,12),USE(sts:Use_Turnaround_Time),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Include In Turnaround Time Report'),AT(364,242),USE(sts:TurnaroundTimeReport),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Include In Turnaround Time Report'),TIP('Include In Turnaround Time Report'),VALUE('1','0')
                           GROUP,AT(164,256,168,24),USE(?turnaround_time_group)
                             PROMPT('Days'),AT(264,256),USE(?sts:turnaround_days:prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Hours'),AT(300,256),USE(?sts:turnaround_hours:prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Turnaround Time'),AT(168,264),USE(?sts:use_turnaround_time:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@n3b),AT(264,264,24,10),USE(sts:Turnaround_Days),RIGHT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                             ENTRY(@n2b),AT(300,264,24,10),USE(sts:Turnaround_Hours),RIGHT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           END
                           BUTTON,AT(448,270),USE(?EmailDefaults),TRN,FLAT,HIDE,LEFT,ICON('edefsp.jpg')
                           CHECK('Enable Email'),AT(264,282),USE(sts:EnableEmail),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Enable Email'),TIP('Enable Email'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Status Type'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Status Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Status Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020234'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_STATUS')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STS:Heading:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sts:Record,History::sts:Record)
  SELF.AddHistoryField(?sts:Heading_Ref_Number,1)
  SELF.AddHistoryField(?sts:Ref_Number,3)
  SELF.AddHistoryField(?sts:Notes,9)
  SELF.AddHistoryField(?sts:Loan,10)
  SELF.AddHistoryField(?sts:Exchange,11)
  SELF.AddHistoryField(?sts:Job,12)
  SELF.AddHistoryField(?sts:EngineerStatus,14)
  SELF.AddHistoryField(?sts:Use_Turnaround_Time,4)
  SELF.AddHistoryField(?sts:TurnaroundTimeReport,26)
  SELF.AddHistoryField(?sts:Turnaround_Days,6)
  SELF.AddHistoryField(?sts:Turnaround_Hours,8)
  SELF.AddHistoryField(?sts:EnableEmail,15)
  SELF.AddUpdateFile(Access:STATUS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STAHEAD.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STATUS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?sts:Job{Prop:Checked} = True
    UNHIDE(?sts:EngineerStatus)
  END
  IF ?sts:Job{Prop:Checked} = False
    HIDE(?sts:EngineerStatus)
  END
  IF ?sts:Use_Turnaround_Time{Prop:Checked} = True
    ENABLE(?turnaround_time_group)
  END
  IF ?sts:Use_Turnaround_Time{Prop:Checked} = False
    DISABLE(?turnaround_time_group)
  END
  IF ?sts:EnableEmail{Prop:Checked} = True
    UNHIDE(?EmailDefaults)
  END
  IF ?sts:EnableEmail{Prop:Checked} = False
    HIDE(?EmailDefaults)
  END
  FDCB7.Init(sts:Heading_Ref_Number,?sts:Heading_Ref_Number,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:STAHEAD,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(sth:Ref_Number_Key)
  FDCB7.AddField(sth:Ref_Number,FDCB7.Q.sth:Ref_Number)
  FDCB7.AddField(sth:Heading,FDCB7.Q.sth:Heading)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STAHEAD.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    sts:Heading_Ref_Number = glo:select2
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  sth:Ref_Number = sts:Heading_Ref_Number             ! Assign linking field value
  Access:STAHEAD.Fetch(sth:Ref_Number_Key)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sts:Heading_Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sts:Heading_Ref_Number, Accepted)
      ?sts:ref_number{prop:rangelow} = sts:heading_ref_number + 1
      ?sts:ref_number{prop:rangehigh} = sts:heading_ref_number + 99
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sts:Heading_Ref_Number, Accepted)
    OF ?sts:Job
      IF ?sts:Job{Prop:Checked} = True
        UNHIDE(?sts:EngineerStatus)
      END
      IF ?sts:Job{Prop:Checked} = False
        HIDE(?sts:EngineerStatus)
      END
      ThisWindow.Reset
    OF ?sts:Use_Turnaround_Time
      IF ?sts:Use_Turnaround_Time{Prop:Checked} = True
        ENABLE(?turnaround_time_group)
      END
      IF ?sts:Use_Turnaround_Time{Prop:Checked} = False
        DISABLE(?turnaround_time_group)
      END
      ThisWindow.Reset
    OF ?sts:Turnaround_Hours
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sts:Turnaround_Hours, Accepted)
      If sts:Turnaround_Hours < 0 Or sts:Turnaround_Hours > 23
          Case Missive('The "Hours" value must be between 0 and 23.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          sts:Turnaround_Hours = 0
          Select(?sts:Turnaround_Hours)
      End !sts:Turnaround_Hours < 0 Or sts:Turnaround_Hours > 23
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sts:Turnaround_Hours, Accepted)
    OF ?EmailDefaults
      ThisWindow.Update
      StatusEmailDefaults
      ThisWindow.Reset
    OF ?sts:EnableEmail
      IF ?sts:EnableEmail{Prop:Checked} = True
        UNHIDE(?EmailDefaults)
      END
      IF ?sts:EnableEmail{Prop:Checked} = False
        HIDE(?EmailDefaults)
      END
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020234'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020234'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020234'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sts:loan <> 'YES' And sts:exchange <> 'YES' And sts:job <> 'YES'
      Case Missive('You must select at least one "Type Of Status".','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Cycle
  End!If sts:loan <> 'YES' or sts:exchange <> 'YES' or sts:job <> 'YES'
  IF sts:heading_ref_number = 0
      Case Missive('You cannot insert any statuses under this Heading Reference Number.'&|
        '<13,10>'&|
        '<13,10>Please select another.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Cycle
  End!IF sts:heading_ref_number = 0
  sts:status = sts:ref_number & ' ' & CLip(tmp:status)
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      tmp:status = Sub(sts:status,5,26)
      
      If sts:systemstatus <> 'YES'
          Enable(?SystemGroup)
      End!If sts:systemstatus <> 'YES'
      !ThisMakeOver.SetWindow(win:form)
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Available_Locations PROCEDURE                  !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
Current_Spaces_Temp  STRING(10)
yes_temp             STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOCINTER)
                       PROJECT(loi:Location)
                       PROJECT(loi:Location_Available)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Current_Spaces_Temp    LIKE(Current_Spaces_Temp)      !List box control field - type derived from local data
loi:Location_Available LIKE(loi:Location_Available)   !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Available Internal Locations File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('127L(2)|M~Location~@s30@0L(2)|M~Current Spaces~@s10@'),FROM(Queue:Browse:1)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Internal Location File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Location'),USE(?Tab:2)
                           ENTRY(@s30),AT(264,98,124,10),USE(loi:Location),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020214'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Available_Locations')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCINTER.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCINTER,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,loi:Location_Available_Key)
  BRW1.AddRange(loi:Location_Available,yes_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?loi:Location,loi:Location,1,BRW1)
  BIND('Current_Spaces_Temp',Current_Spaces_Temp)
  BRW1.AddField(loi:Location,BRW1.Q.loi:Location)
  BRW1.AddField(Current_Spaces_Temp,BRW1.Q.Current_Spaces_Temp)
  BRW1.AddField(loi:Location_Available,BRW1.Q.loi:Location_Available)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCINTER.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020214'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020214'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020214'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?loi:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loi:Location, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loi:Location, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (loi:Allocate_Spaces = 'YES')
    Current_Spaces_Temp = loi:Current_Spaces
  ELSE
    Current_Spaces_Temp = 'N/A'
  END
  PARENT.SetQueueRecord
  SELF.Q.Current_Spaces_Temp = Current_Spaces_Temp    !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateREPTYDEF PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
save_sta_id          USHORT,AUTO
save_trc_id          USHORT,AUTO
pos                  STRING(255)
sav:RepairType       STRING(30)
save_job_id          USHORT,AUTO
save_suc_id          USHORT,AUTO
save_tuc_id          USHORT,AUTO
sav:Chargeable       STRING('''YES''')
sav:Warranty         STRING('''YES''')
save_man_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
tmp:replicate        BYTE(0)
tmp:Manufacturer     STRING(30)
tmp:RepairType       STRING(30)
tmp:SaveState        LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::rtd:Record  LIKE(rtd:RECORD),STATIC
QuickWindow          WINDOW('Update the REPTYDEF File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(168,87),USE(?rtd:Manufacturer:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(240,90,124,8),USE(rtd:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Manufacturer'),TIP('Manufacturer'),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(368,86),USE(?LookupManufacturer),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Repair Type'),AT(168,103),USE(?RTD:Repair_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(240,103,124,8),USE(rtd:Repair_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,MSG('Repair Type')
                           OPTION('Type'),AT(420,87,92,182),USE(rtd:BER),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Type Of Repair Type')
                             RADIO('Repair'),AT(424,100),USE(?rtd:BER:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('Modification'),AT(424,114),USE(?rtd:BER:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             RADIO('Exchange'),AT(424,127),USE(?rtd:BER:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('3')
                             RADIO('Liquid Damage'),AT(424,142),USE(?rtd:BER:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('4')
                             RADIO('N.F.F.'),AT(424,156),USE(?rtd:BER:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),TIP('No Fault Found'),VALUE('5')
                             RADIO('B.E.R.'),AT(424,170),USE(?rtd:BER:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),TIP('Beyond Economical Repair'),VALUE('1')
                             RADIO('R.T.M.'),AT(424,183),USE(?rtd:BER:Radio7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),TIP('Return To Manufacturer'),VALUE('6')
                             RADIO('R.N.R.'),AT(424,198),USE(?rtd:BER:Radio9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('8')
                             RADIO('Excluded'),AT(424,212),USE(?rtd:BER:Radio8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('7')
                             RADIO('Exchange Estimate'),AT(424,226),USE(?rtd:BER:Radio10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('9')
                             RADIO('Exchange Fee'),AT(424,239),USE(?rtd:BER:Radio11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('10')
                             RADIO('Handling Fee'),AT(424,254),USE(?rtd:BER:Radio12),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('11')
                           END
                           CHECK('Chargeable'),AT(240,116),USE(rtd:Chargeable),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO'),MSG('Chargeable Repair Type')
                           CHECK('Warranty'),AT(240,127),USE(rtd:Warranty),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO'),MSG('Warranty Repair Type')
                           PROMPT('Warranty Code'),AT(300,119),USE(?RTD:WarrantyCode:Prompt),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s5),AT(300,127,64,8),USE(rtd:WarrantyCode),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Code For Warranty (Sagem)'),TIP('Code For Warranty (Sagem)'),UPR
                           CHECK('Compulsory Fault Coding'),AT(240,140),USE(rtd:CompFaultCoding),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Compulsory Fault Coding'),TIP('Compulsory Fault Coding'),VALUE('1','0')
                           CHECK('Exclude From EDI'),AT(240,154),USE(rtd:ExcludeFromEDI),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Exclude From EDI'),TIP('Exclude From EDI'),VALUE('1','0')
                           CHECK('Force Adjustment If No Parts Used'),AT(240,166),USE(rtd:ForceAdjustment),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Adjustment If No Parts Used'),TIP('Force Adjustment If No Parts Used'),VALUE('1','0')
                           CHECK('Exclude From Invoicing'),AT(240,178),USE(rtd:ExcludeFromInvoicing),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Exclude From Invoicing'),TIP('Exclude From Invoicing'),VALUE('1','0')
                           CHECK('Exclude From Bouncer Table'),AT(240,191),USE(rtd:ExcludeFromBouncer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Exclude From Bouncer Table'),TIP('Exclude From Bouncer Table'),VALUE('1','0')
                           CHECK('Prompt For Exchange'),AT(240,206),USE(rtd:PromptForExchange),DISABLE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Prompt For Exchange'),TIP('Prompt For Exchange'),VALUE('1','0')
                           CHECK('No Parts'),AT(240,218),USE(rtd:NoParts),FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('Y','N')
                           PROMPT('Job Weighting'),AT(168,230),USE(?rtd:JobWeighting:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(240,230,64,8),USE(rtd:JobWeighting),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Weighting'),TIP('Job Weighting'),UPR,RANGE(0,99),STEP(1)
                           PROMPT('Points'),AT(308,230),USE(?rtd:JobWeighting:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Repair Index'),AT(168,242),USE(?rtd:RepairLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(240,242,64,8),USE(rtd:RepairLevel),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Repair Index'),TIP('Repair Index'),UPR,RANGE(0,10),STEP(1)
                           PROMPT('Skill Level'),AT(312,242),USE(?rtd:SkillLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(352,242,64,8),USE(rtd:SkillLevel),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Skill Level'),TIP('Skill Level'),UPR,STEP(1)
                           CHECK('Scrap Exchange'),AT(240,254),USE(rtd:ScrapExchange),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Exclude RRC Handling Fee'),AT(240,266),USE(rtd:ExcludeHandlingFee),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Exclude RRC Handling Fee'),TIP('Exclude RRC Handling Fee'),VALUE('1','0')
                           OPTION('SMS to Send'),AT(420,274,92,50),USE(rtd:SMSSendType),BOXED,TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             RADIO('None'),AT(424,284),USE(?rtd:SMSSendType:Radio1),FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('N')
                             RADIO('BER'),AT(424,295),USE(?rtd:SMSSendType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('B')
                             RADIO('Liquid Damage '),AT(424,306),USE(?rtd:SMSSendType:Radio3),FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('L')
                           END
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Repair Type'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       CHECK('Unavailable'),AT(240,342),USE(rtd:NotAvailable),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Available'),TIP('Not Available'),VALUE('1','0')
                       BUTTON,AT(168,332),USE(?Replicate),TRN,FLAT,LEFT,ICON('repmanap.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:rtd:Manufacturer                Like(rtd:Manufacturer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CheckFaultCodes        Routine
Data
local:FaultCodeString        String(5000)
Code
    If rtd:NotAvailable = 1
        Found# = 0
        Access:MANFAULO.Clearkey(mfo:HideDescriptionKey)
        mfo:NotAvailable = 0
        mfo:Manufacturer = rtd:Manufacturer
        Set(mfo:HideDescriptionKey,mfo:HideDescriptionKey)
        Loop !MANFAULO
            If Access:MANFAULO.Next()
                Break
            End ! If Access:MANFAULO.Next()
            If mfo:NotAvailable <> 0
                Break
            End ! If mfo:NotAvailable <> 0
            If mfo:Manufacturer <> rtd:Manufacturer
                Break
            End ! If mfo:Manufacturer <> rtd:Manufacturer
            If rtd:Chargeable
                If mfo:RepairType = rtd:Repair_Type
                    Access:MANFAULT.Clearkey(maf:Field_Number_Key)
                    maf:Manufacturer = rtd:Manufacturer
                    maf:Field_Number = mfo:Field_Number
                    If Access:MANFAULT.TryFetch(Maf:Field_Number_Key) = Level:Benign
                        If maf:NotAvailable = 0
                            Found# = 1
                            local:FaultCodeString = Clip(local:FaultCodeString) & '|Fault Code: "' & Clip(maf:Field_Name) & '".    Lookup Value: "' & Clip(mfo:Field) & '"'
                            Cycle
                        End ! If maf:NotAvailable = 0
                    End ! If Access:MANFAULT.TryFetch(Maf:Field_Number_Key) = Level:Benign
                End ! If mfo:RepairType = rtd:RepairType
            End ! If rtd:Chargeable
            If rtd:Warranty
                If mfo:RepairTypeWarranty = rtd:Repair_Type
                    Access:MANFAULT.Clearkey(maf:Field_Number_Key)
                    maf:Manufacturer = rtd:Manufacturer
                    maf:Field_Number = mfo:Field_Number
                    If Access:MANFAULT.TryFetch(Maf:Field_Number_Key) = Level:Benign
                        If maf:NotAvailable = 0
                            Found# = 1
                            local:FaultCodeString = Clip(local:FaultCodeString) & '|Fault Code: "' & Clip(maf:Field_Name) & '".    Lookup Value: "' & Clip(mfo:Field) & '"  (Warranty)'
                        End ! If maf:NotAvailable = 0
                    End ! If Access:MANFAULT.TryFetch(Maf:Field_Number_Key) = Level:Benign
                End ! If mfo:RepairTypeWarranty = rtd:RepairTypoe
            End ! If rtd:Warranty
        End ! Loop !MANFAULO

        If Found# = 1
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Error. The selected repair type has been assigned to the fault codes below. '&|
                '|'&|
                '|You cannot make it "unavailable" until you have assigned a different repair type to this fault codes:' & |
                '|' & Clip(local:FaultCodeString),'ServiceBase',|
                           icon:Hand,'&OK',1,1)
                Of 1 ! &OK Button
            End!Case Message
            rtd:NotAvailable = 0
        End ! If Found# = 1
    End ! If rtd:NotAvailable = 1
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Repair Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Repair Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020231'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateREPTYDEF')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?rtd:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rtd:Record,History::rtd:Record)
  SELF.AddHistoryField(?rtd:Manufacturer,14)
  SELF.AddHistoryField(?rtd:Repair_Type,2)
  SELF.AddHistoryField(?rtd:BER,9)
  SELF.AddHistoryField(?rtd:Chargeable,3)
  SELF.AddHistoryField(?rtd:Warranty,4)
  SELF.AddHistoryField(?rtd:WarrantyCode,5)
  SELF.AddHistoryField(?rtd:CompFaultCoding,6)
  SELF.AddHistoryField(?rtd:ExcludeFromEDI,7)
  SELF.AddHistoryField(?rtd:ForceAdjustment,16)
  SELF.AddHistoryField(?rtd:ExcludeFromInvoicing,8)
  SELF.AddHistoryField(?rtd:ExcludeFromBouncer,10)
  SELF.AddHistoryField(?rtd:PromptForExchange,11)
  SELF.AddHistoryField(?rtd:NoParts,20)
  SELF.AddHistoryField(?rtd:JobWeighting,12)
  SELF.AddHistoryField(?rtd:RepairLevel,15)
  SELF.AddHistoryField(?rtd:SkillLevel,13)
  SELF.AddHistoryField(?rtd:ScrapExchange,17)
  SELF.AddHistoryField(?rtd:ExcludeHandlingFee,18)
  SELF.AddHistoryField(?rtd:SMSSendType,21)
  SELF.AddHistoryField(?rtd:NotAvailable,19)
  SELF.AddUpdateFile(Access:REPTYDEF)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Relate:REPTYDEF_ALIAS.Open
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:REPAIRTY.UseFile
  Access:STDCHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPTYDEF
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?rtd:Manufacturer{Prop:Tip} AND ~?LookupManufacturer{Prop:Tip}
     ?LookupManufacturer{Prop:Tip} = 'Select ' & ?rtd:Manufacturer{Prop:Tip}
  END
  IF ?rtd:Manufacturer{Prop:Msg} AND ~?LookupManufacturer{Prop:Msg}
     ?LookupManufacturer{Prop:Msg} = 'Select ' & ?rtd:Manufacturer{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?rtd:Warranty{Prop:Checked} = True
    ENABLE(?RTD:WarrantyCode)
    ENABLE(?rtd:PromptForExchange)
  END
  IF ?rtd:Warranty{Prop:Checked} = False
    rtd:WarrantyCode = ''
    DISABLE(?RTD:WarrantyCode)
    DISABLE(?rtd:PromptForExchange)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:REPTYDEF_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Manufacturers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?rtd:Manufacturer
      IF rtd:Manufacturer OR ?rtd:Manufacturer{Prop:Req}
        man:Manufacturer = rtd:Manufacturer
        !Save Lookup Field Incase Of error
        look:rtd:Manufacturer        = rtd:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            rtd:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            rtd:Manufacturer = look:rtd:Manufacturer
            SELECT(?rtd:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupManufacturer
      ThisWindow.Update
      man:Manufacturer = rtd:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          rtd:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?rtd:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?rtd:Manufacturer)
    OF ?rtd:BER
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtd:BER, Accepted)
      if rtd:BER = 10 or rtd:ber = 11 then
         !this is exchange fee or handling fee - we can only have one of each
         match# = 0
         access:reptydef_alias.clearkey(rtd_ali:ManRepairTypeKey)
         rtd_ali:Manufacturer = rtd:Manufacturer
         set(rtd_ali:ManRepairTypeKey,rtd_ali:ManRepairTypeKey)
         loop
              if access:Reptydef_alias.next() then break.
              if rtd_ali:Manufacturer <> rtd:Manufacturer then break.
              if rtd_ali:BER = rtd:BER and rtd_ali:RecordNumber<>rtd:RecordNumber
                  !we have a different record of this type
                  match# = 1
                  break
              END!if bers match
         END!loop
      
         if match# = 1 then
              Case Missive('You cannot have more than one repair type for Exchange Fee or Handling Fee. Please make another choice.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              rtd:ber = 0
      
         ELSE
              !no matches - check both chargeable and warranty
              rtd:Chargeable = 'YES'
              rtd:Warranty   = 'YES'
         END !if match is true
      
         thiswindow.update()
         display()
      END
      
      if rtd:BER = 1 or rtd:BER = 4 then
          unhide(?rtd:SMSSendType)
      ELSE
          hide(?rtd:SMSSendType)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtd:BER, Accepted)
    OF ?rtd:Warranty
      IF ?rtd:Warranty{Prop:Checked} = True
        ENABLE(?RTD:WarrantyCode)
        ENABLE(?rtd:PromptForExchange)
      END
      IF ?rtd:Warranty{Prop:Checked} = False
        rtd:WarrantyCode = ''
        DISABLE(?RTD:WarrantyCode)
        DISABLE(?rtd:PromptForExchange)
      END
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020231'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020231'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020231'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?rtd:NotAvailable
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtd:NotAvailable, Accepted)
      If ~0{prop:AcceptAll}
          Do CheckFaultCodes
          Display()
      End ! If ~0{prop:AcceptAll}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtd:NotAvailable, Accepted)
    OF ?Replicate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
      Case Missive('Are you sure you want to replicate this Repair Type to ALL Manufacturers?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              tmp:Replicate = 1
              Post(Event:Accepted,?OK)
          Of 1 ! No Button
              tmp:Replicate = 0
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If tmp:Replicate
      Access:REPTYDEF.Update()
      tmp:Manufacturer    = rtd:Manufacturer
      tmp:RepairType      = rtd:Repair_Type
      tmp:SaveState   = GetState(REPTYDEF)
  
      Access:REPTYDEF_ALIAS.ClearKey(rtd_ali:ManRepairTypeKey)
      rtd_ali:Manufacturer = tmp:Manufacturer
      rtd_ali:Repair_Type  = tmp:RepairType
      If Access:REPTYDEF_ALIAS.TryFetch(rtd_ali:ManRepairTypeKey) = Level:Benign
          !Found
          Save_man_ID = Access:MANUFACT.SaveFile()
          Set(man:Manufacturer_Key)
          Loop
              If Access:MANUFACT.NEXT()
                 Break
              End !If
              If man:Manufacturer = tmp:Manufacturer
                  Cycle
              End !If man:Manufacturer = tmp:Manufacturer
  
              Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
              rtd:Manufacturer = man:Manufacturer
              rtd:Repair_Type  = tmp:RepairType
              If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
                  !Found
                  RecordNumber$   = rtd:RecordNumber
                  RepairType"     = rtd:Repair_Type
                  rtd:Record      :=: rtd_ali:Record
                  rtd:RecordNumber    = RecordNumber$
                  rtd:Repair_Type = RepairType"
                  rtd:Manufacturer    = man:Manufacturer
                  Access:REPTYDEF.TryUpdate()
              Else!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  If Access:REPTYDEF.PrimeRecord() = Level:Benign
                      RecordNumber$   = rtd:RecordNumber
                      rtd:Record :=: rtd_ali:Record
                      rtd:RecordNumber    = RecordNumber$
                      rtd:Manufacturer    = man:Manufacturer
                      If Access:REPTYDEF.TryInsert() = Level:Benign
                          !Insert Successful
  
                      Else !If Access:REPTYDEF.TryInsert() = Level:Benign
                          !Insert Failed
  
                          Access:REPTYDEF.Cancelautoinc()
                      End !If Access:REPTYDEF.TryInsert() = Level:Benign
                  End !If Access:REPTYDEF.PrimeRecord() = Level:Benign
              End!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
  
          End !Loop
          Access:MANUFACT.RestoreFile(Save_man_ID)
  
      Else!If Access:REPTYDEF_ALIAS.TryFetch(rtd_ali:ManRepairTypeKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:REPTYDEF_ALIAS.TryFetch(rtd_ali:ManRepairTypeKey) = Level:Benign
  
      RestoreState(REPTYDEF,tmp:SaveState)
      FreeState(REPTYDEF,tmp:SaveState)
  
  End !tmp:Replicate
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?rtd:Manufacturer
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtd:Manufacturer, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtd:Manufacturer, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(Event:Accepted,?rtd:WarrantyCode)
      if rtd:BER = 1 or rtd:BER = 4 then
          unhide(?rtd:SMSSendType)
      ELSE
          hide(?rtd:SMSSendType)
      END
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Default_Repair_Types PROCEDURE                 !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Type             STRING(3)
tmp:Manufacturer     STRING(30)
tmp:FilterBrowse     BYTE(0)
BRW1::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Chargeable)
                       PROJECT(rtd:Warranty)
                       PROJECT(rtd:Manufacturer)
                       PROJECT(rtd:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
tmp:Type               LIKE(tmp:Type)                 !List box control field - type derived from local data
rtd:Chargeable         LIKE(rtd:Chargeable)           !List box control field - type derived from field
rtd:Warranty           LIKE(rtd:Warranty)             !List box control field - type derived from field
rtd:Manufacturer       LIKE(rtd:Manufacturer)         !List box control field - type derived from field
rtd:RecordNumber       LIKE(rtd:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Repair Type File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,134,276,190),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('97L(2)|M~Repair Type~@s30@21L(2)|M~Type~@s3@20L(2)|M~Cha~@s3@20L(2)|M~War~@s3@12' &|
   '0L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Default Repair Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,244),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(448,270),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(448,296),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Repair Type'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,106,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Manufacturer'),TIP('Manufacturer'),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(296,103),USE(?LookupManufacturer),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Manufacturer'),AT(168,98),USE(?Prompt1),TRN,FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                         END
                         TAB('All Repair Types'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       ENTRY(@s30),AT(168,120,124,10),USE(rtd:Repair_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Repair Type')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       OPTION('Filter Browse'),AT(336,98,176,28),USE(tmp:FilterBrowse),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Available'),AT(340,110),USE(?tmp:FilterBrowse:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('Unavailable'),AT(404,110,58,10),USE(?tmp:FilterBrowse:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('All'),AT(480,110),USE(?tmp:FilterBrowse:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('9')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020230'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Default_Repair_Types')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:REPTYDEF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?LookupManufacturer{Prop:Tip}
     ?LookupManufacturer{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?LookupManufacturer{Prop:Msg}
     ?LookupManufacturer{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,rtd:Repair_Type_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?rtd:Repair_Type,rtd:Repair_Type,1,BRW1)
  BRW1.AddSortOrder(,rtd:ManRepairTypeKey)
  BRW1.AddRange(rtd:Manufacturer,tmp:Manufacturer)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?rtd:Repair_Type,rtd:Repair_Type,1,BRW1)
  BRW1.AddResetField(tmp:Manufacturer)
  BIND('tmp:Type',tmp:Type)
  BRW1.AddField(rtd:Repair_Type,BRW1.Q.rtd:Repair_Type)
  BRW1.AddField(tmp:Type,BRW1.Q.tmp:Type)
  BRW1.AddField(rtd:Chargeable,BRW1.Q.rtd:Chargeable)
  BRW1.AddField(rtd:Warranty,BRW1.Q.rtd:Warranty)
  BRW1.AddField(rtd:Manufacturer,BRW1.Q.rtd:Manufacturer)
  BRW1.AddField(rtd:RecordNumber,BRW1.Q.rtd:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 2
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('REPAIR TYPES - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('REPAIR TYPES - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
        of deleterecord
            check_access('REPAIR TYPES - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      UpdateREPTYDEF
    END
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020230'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020230'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020230'&'0')
      ***
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW1.ResetSort(1)
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?LookupManufacturer
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:FilterBrowse
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FilterBrowse, Accepted)
      Brw1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FilterBrowse, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:Manufacturer
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  ! Inserting (DBH 29/11/2007) # 9002 - Show available and unavailable repair types
  Case tmp:FilterBrowse
  Of 0
      If rtd:Notavailable = 1
          Return Record:Filtered
      End ! If rtd:Unavailable = 1
  Of 1
      If rtd:Notavailable = 0
          Return Record:Filtered
      End ! If rtd:Unavailable = 0
  Else
  End ! Case tmp:FilterBrowse
  ! End (DBH 29/11/2007) #9002
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_Teams PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
TempAccount_Name     STRING(30)
History::tea:Record  LIKE(tea:RECORD),STATIC
QuickWindow          WINDOW('Update the TEAMS File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Team'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Team'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,178,184,10),USE(tea:Team),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           CHECK('Linked Account'),AT(248,198),USE(tea:Associated),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N'),MSG('Linked to trade account Y/N')
                           GROUP,AT(240,210,216,42),USE(?GroupAssociate)
                             STRING('Associated Trade Account'),AT(248,214),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             BUTTON,AT(396,223),USE(?CallLookup),FLAT,ICON('lookupp.jpg')
                             ENTRY(@s30),AT(248,228,124,10),USE(TempAccount_Name),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver,,COLOR:Silver),READONLY
                           END
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:TempAccount_Name                Like(TempAccount_Name)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Team'
  OF ChangeRecord
    ActionMessage = 'Changing A Team'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020257'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Teams')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tea:Record,History::tea:Record)
  SELF.AddHistoryField(?tea:Team,2)
  SELF.AddHistoryField(?tea:Associated,4)
  SELF.AddUpdateFile(Access:TEAMS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TEAMS.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TEAMS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:Auto
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if tea:Associated = 'Y' then
      Access:TradeAcc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = tea:TradeAccount_Number
      if access:TradeAcc.fetch(tra:Account_Number_Key)
          Tea:Associated = 'N'
      ELSE
          TempAccount_Name = tra:Company_Name
      END
  END
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tea:Associated{Prop:Checked} = True
    UNHIDE(?GroupAssociate)
    ENABLE(?CallLookup)
  END
  IF ?tea:Associated{Prop:Checked} = False
    HIDE(?GroupAssociate)
    DISABLE(?CallLookup)
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TEAMS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Pick_trade_Account
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020257'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020257'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020257'&'0')
      ***
    OF ?tea:Associated
      IF ?tea:Associated{Prop:Checked} = True
        UNHIDE(?GroupAssociate)
        ENABLE(?CallLookup)
      END
      IF ?tea:Associated{Prop:Checked} = False
        HIDE(?GroupAssociate)
        DISABLE(?CallLookup)
      END
      ThisWindow.Reset
    OF ?CallLookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup, Accepted)
      tra:Company_Name = TempAccount_Name
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        TempAccount_Name = tra:Company_Name
      END
      ThisWindow.Reset(1)
      tea:TradeAccount_Number = tra:Account_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup, Accepted)
    OF ?TempAccount_Name
      IF TempAccount_Name OR ?TempAccount_Name{Prop:Req}
        tra:Company_Name = TempAccount_Name
        !Save Lookup Field Incase Of error
        look:TempAccount_Name        = TempAccount_Name
        IF Access:TRADEACC.TryFetch(tra:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            TempAccount_Name = tra:Company_Name
          ELSE
            !Restore Lookup On Error
            TempAccount_Name = look:TempAccount_Name
            SELECT(?TempAccount_Name)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Teams PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LocalView            STRING('A')
BRW1::View:Browse    VIEW(TEAMS)
                       PROJECT(tea:Team)
                       PROJECT(tea:TradeAccount_Number)
                       PROJECT(tea:Associated)
                       PROJECT(tea:Record_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tea:Team               LIKE(tea:Team)                 !List box control field - type derived from field
tea:TradeAccount_Number LIKE(tea:TradeAccount_Number) !List box control field - type derived from field
tea:Associated         LIKE(tea:Associated)           !List box control field - type derived from field
tea:Record_Number      LIKE(tea:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Teams File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Team~@s30@0L(2)|M@s15@0L(2)|M@s1@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Team'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,148,10),USE(tea:Team),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           OPTION('View'),AT(172,112,84,72),USE(LocalView),BOXED,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             RADIO('Active Teams'),AT(176,126),USE(?LocalView:Radio1)
                             RADIO('Inactive Teams'),AT(176,148),USE(?LocalView:Radio2),VALUE('I')
                             RADIO('All Teams'),AT(176,168),USE(?LocalView:Radio3),VALUE('X')
                           END
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,196),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,223),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,250),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Team File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020253'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Teams')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TEAMS.Open
  Relate:TRADEACC_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TEAMS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,tea:Team_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,tea:Team,1,BRW1)
  BRW1.AddSortOrder(,tea:Team_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?TEA:Team,tea:Team,1,BRW1)
  BRW1.AddField(tea:Team,BRW1.Q.tea:Team)
  BRW1.AddField(tea:TradeAccount_Number,BRW1.Q.tea:TradeAccount_Number)
  BRW1.AddField(tea:Associated,BRW1.Q.tea:Associated)
  BRW1.AddField(tea:Record_Number,BRW1.Q.tea:Record_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TEAMS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('TEAMS - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('TEAMS - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of deleterecord
            check_access('TEAMS - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Teams
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LocalView
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LocalView, Accepted)
      BRW1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LocalView, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020253'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020253'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020253'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tea:Team
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tea:Team, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tea:Team, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  if tea:Associated = 'Y'
      access:Tradeacc_Alias.clearkey(tra_ali:Account_Number_Key)
      tra_ali:Account_Number = tea:TradeAccount_Number
      if access:Tradeacc_Alias.fetch(tra_ali:Account_Number_Key)
          !eh?? - let it show as active
          If LocalView = 'I' then return(Record:Filtered).
  
      ELSE
          !found trade record
          Case LocalView
              of 'A'  !Active
                  if  tra_ali:Stop_Account = 'YES' then return(Record:filtered).
              of 'I'  !inactive
                  if  tra_ali:Stop_Account <> 'YES' then return(Record:filtered).
              !ELSE = all so no filering
          END
  
      END
  
  ELSE
      !not associated - call it active
      If LocalView = 'I' then return(Record:Filtered).
  
  END !if is associated
  
  
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateUSERS PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
job_assignment_temp  STRING(15)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
FrameTitle           CSTRING(128)
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::use:Record  LIKE(use:RECORD),STATIC
QuickWindow          WINDOW('Update the USERS File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,280,310),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('General Details'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('User Code'),AT(68,70),USE(?USE:User_Code:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s3),AT(144,70,64,10),USE(use:User_Code),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           CHECK('Active User'),AT(212,70),USE(use:Active),FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           PROMPT('Forename'),AT(68,86),USE(?USE:Forename:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,86,124,10),USE(use:Forename),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Surname'),AT(68,102),USE(?USE:Surname:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,102,124,10),USE(use:Surname),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Password'),AT(68,118),USE(?USE:Password:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s20),AT(144,118,124,10),USE(use:Password),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,PASSWORD
                           PROMPT('Job Type'),AT(68,134),USE(?USE:User_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           COMBO(@s15),AT(144,134,124,10),USE(use:User_Type),LEFT(2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,DROP(10),FROM('ENGINEER|ADMINISTRATOR')
                           PROMPT('Primary Stock Loc.'),AT(68,150),USE(?use:location:prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,150,124,10),USE(use:Location),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(272,146),USE(?LookupStockLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Use Primary Stock Location Only'),AT(144,166),USE(use:StockFromLocationOnly),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Pick Stock From Users Location Only'),TIP('Pick Stock From Users Location Only'),VALUE('1','0')
                           GROUP('Engineer Defaults'),AT(68,178,248,72),USE(?EngineerDefaults),DISABLE,BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Skill Level'),AT(72,190),USE(?use:SkillLevel:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             SPIN(@s8),AT(144,190,64,10),USE(use:SkillLevel),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR,RANGE(0,10),STEP(1),MSG('Skill Level')
                             PROMPT('Team'),AT(72,206),USE(?USE:Team:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(144,206,124,10),USE(use:Team),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                             BUTTON,AT(272,202),USE(?LookupTeams),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Job Weighting'),AT(72,222),USE(?USE:Repair_Target:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             SPIN(@s8),AT(144,222,64,10),USE(use:Repair_Target),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(0,999999),STEP(1)
                             PROMPT('Points'),AT(212,222),USE(?Prompt12),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Include In Engineer Status'),AT(144,238),USE(use:IncludeInEngStatus),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(0D6E7EFH),MSG('Include In Engineer Status'),TIP('Include In Engineer Status'),VALUE('1','0')
                           END
                           PROMPT('Mobile Number:'),AT(68,262),USE(?use:MobileNumber:Prompt)
                           ENTRY(@s20),AT(144,262,124,10),USE(use:MobileNumber),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         END
                       END
                       SHEET,AT(348,54,268,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Security Details'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(428,70,124,10),USE(use:User_Level),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(556,66),USE(?LookupUserLevel),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Access Level'),AT(352,70),USE(?Prompt8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           CHECK('Restrict Logins'),AT(428,86),USE(use:Restrict_Logins),FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           PROMPT('Concurrent Logins'),AT(352,102),USE(?use:concurrent_logins:prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@n4b),AT(428,102,64,10),USE(use:Concurrent_Logins),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),RANGE(1,9999),STEP(1)
                           PROMPT('Password Expiry'),AT(352,118),USE(?use:RenewPassword:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@n8),AT(428,118,64,10),USE(use:RenewPassword),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Renew Password'),TIP('Renew Password'),UPR,STEP(1)
                           PROMPT('Days'),AT(496,118),USE(?Prompt14),FONT(,,,FONT:bold,CHARSET:ANSI)
                           CHECK('Restrict Parts Usage To Skill Level'),AT(428,142),USE(use:RestrictParts),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Restrict Parts Usage To Skill Level'),TIP('Restrict Parts Usage To Skill Level'),VALUE('1','0')
                           GROUP('Parts To Restrict'),AT(352,158,216,44),USE(?RestrictGroup),DISABLE,BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Chargeable && Estimate Parts'),AT(428,170),USE(use:RestrictChargeable),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Chargeable'),TIP('Chargeable'),VALUE('1','0')
                             CHECK('Warranty Parts'),AT(428,186),USE(use:RestrictWarranty),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Warranty'),TIP('Warranty'),VALUE('1','0')
                           END
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend User'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:use:Location                Like(use:Location)
look:use:Team                Like(use:Team)
look:use:User_Level                Like(use:User_Level)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Count_Job_Assignments       Routine
    found_job_assignments# = 0
    If job_assignment_temp    = 'MODEL NUMBER'
        setcursor(cursor:wait)
        clear(usm:record, -1)
        usm:user_code    = use:user_code
        set(usm:model_number_key,usm:model_number_key)
        loop
            next(usmassig)
            if errorcode()                  |
               or usm:user_code    <> use:user_code      |
               then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            found_job_assignments# = 1
            Break
        end !loop
        setcursor()
    End
    If job_assignment_temp   = 'UNIT TYPE'
        setcursor(cursor:wait)
        clear(usu:record, -1)
        usu:user_code = use:user_code
        set(usu:unit_type_key,usu:unit_type_key)
        loop
            next(usuassig)
            if errorcode()               |
               or usu:user_code <> use:user_code      |
               then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            found_job_assignments# = 1
            Break
        end !loop
        setcursor()
    End
Delete_Job_Assignments       Routine
    If job_assignment_temp = 'MODEL NUMBER'
        setcursor(cursor:wait)
        logout(2,usmassig)
        if errorcode()
           Stop(Error())
        end
        clear(usm:record, -1)
        usm:user_code    = use:user_code
        set(usm:model_number_key,usm:model_number_key)
        loop
            next(usmassig)
            if errorcode()                  |
               or usm:user_code    <> use:user_code      |
               then break.  ! end if
            delete(usmassig)
            case errorcode()
            of noerror
            else
                stop('DELETE '&clip(errorfile())&' Error: '&error() )
            end
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        end !loop
        commit()
        if errorcode()
           Stop(Error())
        end
        setcursor()
    End
    If job_assignment_temp   = 'UNIT TYPE'
        setcursor(cursor:wait)
        logout(2,usuassig)
        if errorcode()
           Stop(Error())
        end
        clear(usu:record, -1)
        usu:user_code = use:user_code
        set(usu:unit_type_key,usu:unit_type_key)
        loop
            next(usuassig)
            if errorcode()               |
               or usu:user_code <> use:user_code      |
               then break.  ! end if
            delete(usuassig)
            case errorcode()
            of noerror
            else
                stop('DELETE '&clip(errorfile())&' Error: '&error() )
            end
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        end !loop
        commit()
        if errorcode()
           Stop(Error())
        end
        setcursor()
    End
Concurrent_Field        Routine
    If use:restrict_logins <> 'YES'
        Disable(?use:concurrent_logins)
        Disable(?use:concurrent_logins:prompt)
    Else
        Enable(?use:concurrent_logins)
        Enable(?use:concurrent_logins:prompt)
    End
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A User'
  OF ChangeRecord
    ActionMessage = 'Changing A User'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020256'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateUSERS')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?USE:User_Code:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(use:Record,History::use:Record)
  SELF.AddHistoryField(?use:User_Code,1)
  SELF.AddHistoryField(?use:Active,14)
  SELF.AddHistoryField(?use:Forename,2)
  SELF.AddHistoryField(?use:Surname,3)
  SELF.AddHistoryField(?use:Password,4)
  SELF.AddHistoryField(?use:User_Type,5)
  SELF.AddHistoryField(?use:Location,16)
  SELF.AddHistoryField(?use:StockFromLocationOnly,19)
  SELF.AddHistoryField(?use:SkillLevel,18)
  SELF.AddHistoryField(?use:Team,15)
  SELF.AddHistoryField(?use:Repair_Target,17)
  SELF.AddHistoryField(?use:IncludeInEngStatus,26)
  SELF.AddHistoryField(?use:MobileNumber,27)
  SELF.AddHistoryField(?use:User_Level,8)
  SELF.AddHistoryField(?use:Restrict_Logins,12)
  SELF.AddHistoryField(?use:Concurrent_Logins,13)
  SELF.AddHistoryField(?use:RenewPassword,21)
  SELF.AddHistoryField(?use:RestrictParts,23)
  SELF.AddHistoryField(?use:RestrictChargeable,24)
  SELF.AddHistoryField(?use:RestrictWarranty,25)
  SELF.AddUpdateFile(Access:USERS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:LOCATION.Open
  Relate:USELEVEL.Open
  Access:TEAMS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:USERS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?use:Team{Prop:Tip} AND ~?LookupTeams{Prop:Tip}
     ?LookupTeams{Prop:Tip} = 'Select ' & ?use:Team{Prop:Tip}
  END
  IF ?use:Team{Prop:Msg} AND ~?LookupTeams{Prop:Msg}
     ?LookupTeams{Prop:Msg} = 'Select ' & ?use:Team{Prop:Msg}
  END
  IF ?use:Location{Prop:Tip} AND ~?LookupStockLocation{Prop:Tip}
     ?LookupStockLocation{Prop:Tip} = 'Select ' & ?use:Location{Prop:Tip}
  END
  IF ?use:Location{Prop:Msg} AND ~?LookupStockLocation{Prop:Msg}
     ?LookupStockLocation{Prop:Msg} = 'Select ' & ?use:Location{Prop:Msg}
  END
  IF ?use:User_Level{Prop:Tip} AND ~?LookupUserLevel{Prop:Tip}
     ?LookupUserLevel{Prop:Tip} = 'Select ' & ?use:User_Level{Prop:Tip}
  END
  IF ?use:User_Level{Prop:Msg} AND ~?LookupUserLevel{Prop:Msg}
     ?LookupUserLevel{Prop:Msg} = 'Select ' & ?use:User_Level{Prop:Msg}
  END
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If DE2:UserSkillLevel
      ?use:SkillLevel{prop:req} = 1
  Else !DE2:UserSkillLevel
      ?use:SkillLevel{prop:req} = 0
  End !DE2:UserSkillLevel
  !Web Job?
  If glo:WebJob
      ?LookupTeams{prop:Hide} = 1
      ?use:Team{prop:readonly} = 1
      ?use:Location{prop:Readonly} = 1
      ?LookupStockLocation{prop:Hide} = 1
      ?use:StockFromLocationOnly{prop:Disable} = 1
      ?use:User_Level{prop:readonly} = 1
      ?LookupUserLevel{prop:Hide} = 1
      ?use:Restrict_Logins{prop:Hide} = 1
      ?use:Concurrent_Logins{prop:Hide} = 1
      ?use:Concurrent_Logins:Prompt{prop:Hide} = 1
      ?use:user_Type{Prop:from}='ENGINEER'
      If ThisWindow.Request = InsertRecord
          use:User_Level = 'REMOTE REPAIR CENTRE'
          use:Team       = Clarionet:Global.Param2
          use:StockFromLocationOnly = 1
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = Clarionet:Global.Param2
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              use:Location        = tra:SiteLocation
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  
      End !If ThisWindow.Request = InsertRecord
  
  End !glo:WebJob
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?use:RestrictParts{Prop:Checked} = True
    ENABLE(?RestrictGroup)
  END
  IF ?use:RestrictParts{Prop:Checked} = False
    DISABLE(?RestrictGroup)
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:LOCATION.Close
    Relate:USELEVEL.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    use:User_Type = 'ENGINEER'
    use:Job_Assignment = 'MODEL NUMBER'
    use:Supervisor = 'NO'
    use:User_Level = '** UNKNOWN **'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      PickTeams
      PickUserLevels
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?use:Active
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Active, Accepted)
      if use:User_Type = 'ENGINEER' then
          if use:Active = 'YES' then
              use:IncludeInEngStatus = 1
          ELSE
              use:IncludeInEngStatus = 0
          END
      END !if engineer
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Active, Accepted)
    OF ?use:User_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Type, Accepted)
      If use:User_Type = 'ENGINEER'
          Enable(?EngineerDefaults)
      Else !use:User_Type = 'ENGINEER'
          Disable(?EngineerDefaults)
          if glo:webJob
              Enable(?EngineerDefaults)
              use:user_type = 'ENGINEER'
          END
      End !use:User_Type = 'ENGINEER'
      Bryan.CompFieldCOlour()
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Type, Accepted)
    OF ?use:Location
      IF use:Location OR ?use:Location{Prop:Req}
        loc:Location = use:Location
        !Save Lookup Field Incase Of error
        look:use:Location        = use:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            use:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            use:Location = look:use:Location
            SELECT(?use:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStockLocation
      ThisWindow.Update
      loc:Location = use:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          use:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?use:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?use:Location)
    OF ?use:Team
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Team, Accepted)
      If ?EngineerDefaults{prop:Disable} = 0
      IF use:Team OR ?use:Team{Prop:Req}
        tea:Team = use:Team
        !Save Lookup Field Incase Of error
        look:use:Team        = use:Team
        IF Access:TEAMS.TryFetch(tea:Team_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            use:Team = tea:Team
          ELSE
            !Restore Lookup On Error
            use:Team = look:use:Team
            SELECT(?use:Team)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      End !If ~0{prop:AcceptAll}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Team, Accepted)
    OF ?LookupTeams
      ThisWindow.Update
      tea:Team = use:Team
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          use:Team = tea:Team
          Select(?+1)
      ELSE
          Select(?use:Team)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?use:Team)
    OF ?use:MobileNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:MobileNumber, Accepted)
        If PassMobileNumberFormat(use:MobileNumber,1) = False
            use:MobileNumber = ''
            Select(?use:MobileNumber)
            Cycle
        End ! If PassMobileNumberFormat(jobe2:SMSAlertNumber,1) = False
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:MobileNumber, Accepted)
    OF ?use:User_Level
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Level, Accepted)
      access:uselevel.clearkey(lev:user_level_key)
      lev:user_level = use:user_level
      if access:uselevel.fetch(lev:user_level_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_access_levels
          if globalresponse = requestcompleted
              use:user_level = lev:user_level
          Else
              use:user_level = '**UNKNOWN**'
          end
          globalrequest     = saverequest#
          Display(?use:user_level)
      end!if access:uselevel.fetch(lev:user_level_key)
      IF use:User_Level OR ?use:User_Level{Prop:Req}
        lev:User_Level = use:User_Level
        !Save Lookup Field Incase Of error
        look:use:User_Level        = use:User_Level
        IF Access:USELEVEL.TryFetch(lev:User_Level_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            use:User_Level = lev:User_Level
          ELSE
            !Restore Lookup On Error
            use:User_Level = look:use:User_Level
            SELECT(?use:User_Level)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Level, Accepted)
    OF ?LookupUserLevel
      ThisWindow.Update
      lev:User_Level = use:User_Level
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          use:User_Level = lev:User_Level
          Select(?+1)
      ELSE
          Select(?use:User_Level)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?use:User_Level)
    OF ?use:Restrict_Logins
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Restrict_Logins, Accepted)
      Do Concurrent_Field
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Restrict_Logins, Accepted)
    OF ?use:RestrictParts
      IF ?use:RestrictParts{Prop:Checked} = True
        ENABLE(?RestrictGroup)
      END
      IF ?use:RestrictParts{Prop:Checked} = False
        DISABLE(?RestrictGroup)
      END
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020256'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020256'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020256'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Concurrent_Field
      Post(Event:Accepted,?use:User_Type)
      job_assignment_temp = use:job_assignment
      !Disable usercode if not inserting - 4314 (DBH: 27-05-2004)
      If Thiswindow.Request <> InsertRecord
          ?use:User_Code{prop:disable} = True
      End !Thiswindow.Request <> InsertRecord
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Pick_Unit_Type PROCEDURE                              !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::2:TAGFLAG          BYTE(0)
DASBRW::2:TAGMOUSE         BYTE(0)
DASBRW::2:TAGDISPSTATUS    BYTE(0)
DASBRW::2:QUEUE           QUEUE
Unit_Type_Pointer             LIKE(glo:Unit_Type_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
select_temp          STRING(1)
LocalRequest         LONG
FilesOpened          BYTE
Unit_Type_Tick       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:One              BYTE(1)
BRW1::View:Browse    VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                       PROJECT(uni:Active)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
Unit_Type_Tick         LIKE(Unit_Type_Tick)           !List box control field - type derived from local data
Unit_Type_Tick_Icon    LONG                           !Entry's icon ID
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
uni:Active             LIKE(uni:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Unit Type File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('10L(2)I@s1@120L(2)~Unit Type~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Unit Type'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(uni:Unit_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,111),USE(?Button11),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON('&Rev tags'),AT(320,162,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(316,178,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(448,188),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON('&Tag'),AT(316,196,76,20),USE(?Change:3),HIDE,LEFT,ICON('edit.ico'),DEFAULT
                           BUTTON,AT(448,218),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,246),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Unit Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::2:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
   GET(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
  IF ERRORCODE()
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     ADD(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    Unit_Type_Tick = '*'
  ELSE
    DELETE(GLO:Q_UnitType)
    Unit_Type_Tick = ''
  END
    Queue:Browse:1.Unit_Type_Tick = Unit_Type_Tick
  IF (unit_type_tick = '*')
    Queue:Browse:1.Unit_Type_Tick_Icon = 2
  ELSE
    Queue:Browse:1.Unit_Type_Tick_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::2:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Q_UnitType)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     ADD(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::2:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_UnitType)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::2:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::2:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_UnitType)
    GET(GLO:Q_UnitType,QR#)
    DASBRW::2:QUEUE = GLO:Q_UnitType
    ADD(DASBRW::2:QUEUE)
  END
  FREE(GLO:Q_UnitType)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::2:QUEUE.Unit_Type_Pointer = uni:Unit_Type
     GET(DASBRW::2:QUEUE,DASBRW::2:QUEUE.Unit_Type_Pointer)
    IF ERRORCODE()
       GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
       ADD(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::2:DASSHOWTAG Routine
   CASE DASBRW::2:TAGDISPSTATUS
   OF 0
      DASBRW::2:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::2:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::2:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020224'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Unit_Type')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:UNITTYPE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,uni:ActiveKey)
  BRW1.AddRange(uni:Active,tmp:One)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?uni:Unit_Type,uni:Unit_Type,1,BRW1)
  BIND('Unit_Type_Tick',Unit_Type_Tick)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(Unit_Type_Tick,BRW1.Q.Unit_Type_Tick)
  BRW1.AddField(uni:Unit_Type,BRW1.Q.uni:Unit_Type)
  BRW1.AddField(uni:Active,BRW1.Q.uni:Active)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_UnitType)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MODELNUM.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  If Request = ChangeRecord
    Do Dasbrw::2:dastagonoff
  Else
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateUNITTYPE
    ReturnValue = GlobalResponse
  END
  End
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020224'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020224'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020224'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::2:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change:3
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     GET(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    IF ERRORCODE()
      Unit_Type_Tick = ''
    ELSE
      Unit_Type_Tick = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (unit_type_tick = '*')
    SELF.Q.Unit_Type_Tick_Icon = 2
  ELSE
    SELF.Q.Unit_Type_Tick_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     GET(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    EXECUTE DASBRW::2:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Users PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
user_type_temp       STRING(15),DIM(2)
LocalRequest         LONG
FilesOpened          BYTE
active_temp          STRING('YES')
Team_Temp            STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Team_Temp
tea:Team               LIKE(tea:Team)                 !List box control field - type derived from field
tea:Associated         LIKE(tea:Associated)           !List box control field - type derived from field
tea:TradeAccount_Number LIKE(tea:TradeAccount_Number) !List box control field - type derived from field
tea:Record_Number      LIKE(tea:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Type)
                       PROJECT(use:User_Code)
                       PROJECT(use:Team)
                       PROJECT(use:Active)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Type          LIKE(use:User_Type)            !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:Team               LIKE(use:Team)                 !Browse key field - type derived from field
use:Active             LIKE(use:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(TEAMS)
                       PROJECT(tea:Team)
                       PROJECT(tea:Associated)
                       PROJECT(tea:TradeAccount_Number)
                       PROJECT(tea:Record_Number)
                     END
QuickWindow          WINDOW('Browse the Users File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       OPTION,AT(408,98,104,20),USE(active_temp),BOXED,FONT(,,,,CHARSET:ANSI)
                         RADIO('Active Only'),AT(416,106),USE(?active_temp:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                         RADIO('All'),AT(480,106),USE(?active_temp:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                       END
                       ENTRY(@s30),AT(168,114,124,10),USE(use:Surname),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       LIST,AT(168,126,344,170),USE(?Browse:1),IMM,HSCROLL,COLOR(0408000H),MSG('Browsing Records'),FORMAT('110L(2)|M~Surname~@s30@110L(2)|M~Forename~@s30@62L(2)|M~User Type~@s15@77L(2)|M~' &|
   'User Code~@s3@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The User File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       STRING('Surname'),AT(296,114),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(232,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(296,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,080FFFFH,,CHARSET:ANSI),COLOR(COLOR:Black),SPREAD
                         TAB('All Users'),USE(?Tab:2),FONT(,,0804000H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Lime)
                         END
                         TAB('Engineers Only'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Administrators Only'),USE(?Tab3),FONT(,,,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('By Team'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(168,100,124,10),USE(Team_Temp),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@0L(2)|M@s1@0L(2)|M@s15@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Team'),AT(296,100),USE(?Prompt1),TRN,FONT(,,08000FFH,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Black)
                         END
                         TAB('By User Code'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('User Code'),AT(300,114),USE(?use:User_Code:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(168,114,128,10),USE(use:User_Code),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort11:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(active_temp) = 'ALL' And GLO:WebJob = 1
BRW1::Sort12:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(active_temp) = 'YES' And GLO:WebJob = 1
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(active_temp) = 'ALL' and glo:webjob=0
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(active_temp) = 'YES' and glo:webjob=0
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'ALL'
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'YES'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'ALL'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'YES'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(active_temp) = 'ALL'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(active_temp) = 'YES'
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(active_temp) = 'ALL'
BRW1::Sort10:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(active_temp) = 'YES'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'ALL'
BRW1::Sort5:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'YES'
BRW1::Sort2:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'ALL'
BRW1::Sort6:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'YES'
BRW1::Sort8:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(active_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Refresh_Browse      Routine
    thiswindow.reset(1)
    If active_temp = 'YES'
        If Choice(?CurrentTab) = 4
            use:team    = team_temp
        End!If Choice(?CurrentTab) = 4
        brw1.addrange(use:active,active_temp)
        brw1.applyrange
        thiswindow.reset(1)
    End!If active_temp = 'YES'
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020254'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Users')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?active_temp:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TEAMS.Open
  Relate:TRADEACC_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:USERS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  user_type_temp[1] = 'ENGINEER'
  user_type_temp[2] = 'ADMINISTRATOR'
  !Web Job?
  If glo:WebJob
      ?tab:2{prop:Hide}       = 1
      ?tab2{prop:hide}        = 1
      ?tab3{prop:hide}        = 1
      ?tab5{prop:Hide}        = 1
      Team_Temp   = Clarionet:Global.Param2
      ?team_temp{prop:hide}   = 1
      ?prompt1{prop:hide}     = 1
      select(?Tab4)
      hide(?active_temp)
      Hide(?Insert:3)
      Hide(?Change:3)
      Hide(?Delete:3)

      BRW1.InsertControl = 0
      BRW1.ChangeControl = 0
      BRW1.DeleteControl = 0
  End !glo:WebJob


  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,use:Team_Surname)
  BRW1.AddRange(use:Team,Team_Temp)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?use:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:Active_Team_Surname)
  BRW1.AddRange(use:Team,Team_Temp)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?use:Surname,use:Active,1,BRW1)
  BRW1.SetFilter('(Use:Active=''YES'')')
  BRW1.AddSortOrder(,use:surname_key)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:Surname_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:User_Type_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?USE:Surname,use:User_Type,1,BRW1)
  BRW1.SetFilter('(Upper(use:user_type) = ''ENGINEER'')')
  BRW1.AddSortOrder(,use:User_Type_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddResetField(active_temp)
  BRW1.AddSortOrder(,use:User_Type_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?USE:Surname,use:User_Type,1,BRW1)
  BRW1.SetFilter('(Upper(use:user_type) = ''ADMINISTRATOR'')')
  BRW1.AddSortOrder(,use:User_Type_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:Team_Surname)
  BRW1.AddRange(use:Team,Team_Temp)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?use:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:Active_Team_Surname)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?use:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:User_Code_Key)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?use:User_Code,use:User_Code,1,BRW1)
  BRW1.AddSortOrder(,use:User_Code_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?use:User_Code,use:User_Code,1,BRW1)
  BRW1.AddSortOrder(,use:surname_key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?use:Surname,use:Surname,1,BRW1)
  BRW1.AddResetField(Team_Temp)
  BRW1.AddResetField(active_temp)
  BIND('active_temp',active_temp)
  BIND('user_type_temp_1',user_type_temp[1])
  BIND('user_type_temp_2',user_type_temp[2])
  BIND('Team_Temp',Team_Temp)
  BRW1.AddField(use:Surname,BRW1.Q.use:Surname)
  BRW1.AddField(use:Forename,BRW1.Q.use:Forename)
  BRW1.AddField(use:User_Type,BRW1.Q.use:User_Type)
  BRW1.AddField(use:User_Code,BRW1.Q.use:User_Code)
  BRW1.AddField(use:Team,BRW1.Q.use:Team)
  BRW1.AddField(use:Active,BRW1.Q.use:Active)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=192
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB10.Init(Team_Temp,?Team_Temp,Queue:FileDropCombo.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo,Relate:TEAMS,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo
  FDCB10.AddSortOrder(tea:Team_Key)
  FDCB10.AddField(tea:Team,FDCB10.Q.tea:Team)
  FDCB10.AddField(tea:Associated,FDCB10.Q.tea:Associated)
  FDCB10.AddField(tea:TradeAccount_Number,FDCB10.Q.tea:TradeAccount_Number)
  FDCB10.AddField(tea:Record_Number,FDCB10.Q.tea:Record_Number)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'All Users'
    ?Tab2{PROP:TEXT} = 'Engineers Only'
    ?Tab3{PROP:TEXT} = 'Administrators Only'
    ?Tab4{PROP:TEXT} = 'By Team'
    ?Tab5{PROP:TEXT} = 'By User Code'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TEAMS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('USERS - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('USERS - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of deleterecord
            check_access('USERS - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateUSERS
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?active_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?active_temp, Accepted)
      Do refresh_browse
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?active_temp, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020254'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020254'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020254'&'0')
      ***
    OF ?Team_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Team_Temp, Accepted)
      Do refresh_browse
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Team_Temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      Do refresh_browse
      Select(?Browse:1)
      IF CHOICE(?CurrentTab) = 5
        HIDE(?USE:Surname)
        HIDE(?String1)
        UNHIDE(?USE:User_Code)
        UNHIDE(?use:User_Code:Prompt)
      ELSE
        HIDE(?USE:User_Code)
        HIDE(?use:User_Code:Prompt)
        UNHIDE(?USE:Surname)
        UNHIDE(?String1)
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab:2{PROP:TEXT} = 'All Users'
        OF 2
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab2{PROP:TEXT} = 'Engineers Only'
        OF 3
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab3{PROP:TEXT} = 'Administrators Only'
        OF 4
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab4{PROP:TEXT} = 'By Team'
        OF 5
          ?Browse:1{PROP:FORMAT} ='77L(2)|M~User Code~@s3@#4#125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#'
          ?Tab5{PROP:TEXT} = 'By User Code'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?use:Surname
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Surname, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Surname, Selected)
    OF ?use:User_Code
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    DBHControl{prop:Color} = color:Silver
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        DBHControl{prop:Color} = 080FFFFH
                    Else ! If DBHControl{prop:Req} = 1
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                DBHControl{prop:FontColor} = 01010101H
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    DBHControl{prop:Color} = color:Silver
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    DBHControl{prop:Color} = color:White
                End ! If ReqControl#{prop:ReadOnly} = 1
                DBHControl{prop:FontColor} = 01010101H
                DBHControl{prop:Color,2} = color:White
                DBHControl{prop:Color,3} = 09A6A7CH
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    DBHControl{prop:Color} = color:Silver
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    DBHControl{prop:Color} = color:White
                End ! If ReqControl#{prop:ReadOnly} = 1
                DBHControl{prop:FontColor} = 01010101H
                DBHControl{prop:Color,2} = color:White
                DBHControl{prop:Color,3} = 09A6A7CH
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    DBHControl{prop:FontColor} = 010101H
                    DBHControl{prop:Color} = 0D6E7EFH
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    DBHControl{prop:Color} = 09A6A7CH
                    DBHControl{prop:FontColor} = 080FFFFH
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                DBHControl{prop:Color} = 09A6A7CH
                DBHControl{prop:FontColor} = color:White
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                DBHControl{prop:FontColor} = color:White
                DBHControl{prop:Color} = 09A6A7CH
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:FontColor} = color:White
                DBHControl{prop:Color,1} = 09A6A7CH
            Of Create:Panel
                DBHControl{prop:Fill} = 09A6A7CH
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(active_temp) = 'ALL' And GLO:WebJob = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(active_temp) = 'YES' And GLO:WebJob = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(active_temp) = 'ALL' and glo:webjob=0
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(active_temp) = 'YES' and glo:webjob=0
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(12,Force)
  ELSE
    RETURN SELF.SetSort(13,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?USE:Surname, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?String1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?active_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?active_temp:Radio1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?active_temp:Radio2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(10, ValidateRecord, (),BYTE)
  if tea:Associated = 'Y'
      access:Tradeacc_Alias.clearkey(tra_ali:Account_Number_Key)
      tra_ali:Account_Number = tea:TradeAccount_Number
      if access:Tradeacc_Alias.fetch(tra_ali:Account_Number_Key)
          !eh?? - let it show
      ELSE
          if tra_ali:Stop_Account = 'YES' then return(record:filtered).
      END
  END !if is associated
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(10, ValidateRecord, (),BYTE)
  RETURN ReturnValue

