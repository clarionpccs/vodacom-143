

   MEMBER('vodr0056.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


StockAdjustmentReport PROCEDURE                       !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
LocationValue                 LIKE(glo:LocationValue)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Automatic            BYTE
Parameter_Group      GROUP,PRE(param)
StartDate            DATE
EndDate              DATE
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG(False)
Count                LONG(0)
                     END
ExcelStuff           GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Local_Stuff          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CompanyName          STRING(30)
Courier              STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
JobNumber            LONG
LastColumn           STRING('L')
LocationName         STRING(30)
LineCost             DECIMAL(7,2)
MainStore            STRING(3)
Path                 STRING('C:\ {252}')
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
VATCode              STRING(2)
Version              STRING(30)
                     END
ProgressBar_Stuff    GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
Result               BYTE
AccountChange        BYTE
tmp:FirstModel       STRING(30)
StockOK              BYTE
InvoiceOK            BYTE
                     END
Worksheet_Stuff      GROUP,PRE(sheet)
TitleSummaryRow      LONG(9)
TempLastCol          STRING(1)
HeadLastCol          STRING('Q')
DataLastCol          STRING('Q')
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
LocationBrowse_Group GROUP,PRE()
LocationVal          STRING(30)
LocationTag          STRING(1)
LocationCount        LONG
LocationChange       BYTE
                     END
Calculation_Group    GROUP,PRE(calc)
StockAudAddQty       LONG
StockAudAddCost      DECIMAL(10,2)
StockAudDecQty       LONG
StockAudDecCost      DECIMAL(10,2)
StockAddQty          LONG
StockAddCost         DECIMAL(10,2)
StockDecQty          LONG
StockDecCost         DECIMAL(10,2)
StockAdjTotalQty     LONG
StockAdjTotalCost    DECIMAL(10,2)
StockAudTotalQty     LONG
StockAudTotalCost    DECIMAL(10,2)
TotalQty             LONG
TotalCost            DECIMAL(10,2)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
LocalTag             STRING(1)
LocalTimeOut         LONG
DoAll                STRING(1)
GUIMode              BYTE
Counter              LONG
LocalTranType        STRING(30)
ActiveRow            LONG
FinalExportPath      STRING(255)
ReportCancelled      BYTE(0)
RecordsToProcess     LONG
TmpPos               LONG
CommandLine          STRING(255)
BRW1::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
LocationQueue:Browse QUEUE                            !Queue declaration for browse/combo box using ?LocationList
LocationTag            LIKE(LocationTag)              !List box control field - type derived from local data
LocationTag_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(632,6,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Adjustment Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,56,552,306),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('Start Date'),AT(265,92),USE(?StrStartDate),TRN,FONT('TAHOMA',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(322,92,64,10),USE(param:StartDate),FONT('TAHOMA',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(389,88),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(322,112,64,10),USE(param:EndDate),FONT('TAHOMA',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(390,108),USE(?PopCalendar:2),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           STRING('End Date'),AT(265,112),USE(?StrEndDate),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(260,130,176,220),USE(?LocationList),IMM,VSCROLL,LEFT,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)~Location~@s30@'),FROM(LocationQueue:Browse)
                           BUTTON,AT(440,224),USE(?LocationDASTAG),IMM,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(440,256),USE(?LocationDASTAGAll),IMM,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(440,288),USE(?LocationDASUNTAGALL),IMM,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON('&Rev tags'),AT(548,394,20,16),USE(?LocationDASREVTAG),DISABLE,HIDE
                       BUTTON('sho&W tags'),AT(556,394,12,16),USE(?LocationDASSHOWTAG),DISABLE,HIDE
                       BUTTON,AT(548,366),USE(?CancelButton),FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRWLocation          CLASS(BrowseClass)               !Browse using ?LocationList
Q                      &LocationQueue:Browse          !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
Expo    MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(LocationQueue:Browse,CHOICE(?LocationList))
  BRWLocation.UpdateBuffer
   LocationQueue.LocationValue = loc:Location
   GET(LocationQueue,LocationQueue.LocationValue)
  IF ERRORCODE()
     LocationQueue.LocationValue = loc:Location
     ADD(LocationQueue,LocationQueue.LocationValue)
    LocationTag = '*'
  ELSE
    DELETE(LocationQueue)
    LocationTag = ''
  END
    LocationQueue:Browse.LocationTag = LocationTag
  IF (LocationTag = '*')
    LocationQueue:Browse.LocationTag_Icon = 2
  ELSE
    LocationQueue:Browse.LocationTag_Icon = 1
  END
  PUT(LocationQueue:Browse)
  SELECT(?LocationList,CHOICE(?LocationList))
  LocationCount = RECORDS(LocationQueue) !DAS Taging
  LocationChange = 1  !DAS Taging
DASBRW::6:DASTAGALL Routine
  ?LocationList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRWLocation.Reset
  FREE(LocationQueue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     LocationQueue.LocationValue = loc:Location
     ADD(LocationQueue,LocationQueue.LocationValue)
  END
  SETCURSOR
  BRWLocation.ResetSort(1)
  SELECT(?LocationList,CHOICE(?LocationList))
  LocationCount = RECORDS(LocationQueue) !DAS Taging
  LocationChange = 1  !DAS Taging
DASBRW::6:DASUNTAGALL Routine
  ?LocationList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(LocationQueue)
  BRWLocation.Reset
  SETCURSOR
  BRWLocation.ResetSort(1)
  SELECT(?LocationList,CHOICE(?LocationList))
  LocationCount = RECORDS(LocationQueue) !DAS Taging
  LocationChange = 1  !DAS Taging
DASBRW::6:DASREVTAGALL Routine
  ?LocationList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(LocationQueue)
    GET(LocationQueue,QR#)
    DASBRW::6:QUEUE = LocationQueue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(LocationQueue)
  BRWLocation.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.LocationValue = loc:Location
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.LocationValue)
    IF ERRORCODE()
       LocationQueue.LocationValue = loc:Location
       ADD(LocationQueue,LocationQueue.LocationValue)
    END
  END
  SETCURSOR
  BRWLocation.ResetSort(1)
  SELECT(?LocationList,CHOICE(?LocationList))
  LocationCount = RECORDS(LocationQueue) !DAS Taging
  LocationChange = 1  !DAS Taging
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?LocationDASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?LocationDASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?LocationDASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?LocationDASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?LocationDASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?LocationDASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?LocationDASSHOWTAG{PROP:Text} = 'Show All'
      ?LocationDASSHOWTAG{PROP:Msg}  = 'Show All'
      ?LocationDASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?LocationDASSHOWTAG{PROP:Text})
   BRWLocation.ResetSort(1)
   SELECT(?LocationList,CHOICE(?LocationList))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
OKButtonPressed          Routine


    !progress window only being used here
    if not Automatic

        Expo.OpenProgressWindow(0,1000)

        Expo.UpdateProgressText()          !blank line
        
        Expo.UpdateProgressText('Report Started: ' & FORMAT(Expo.StartDate,@d06) & |
            ' ' & FORMAT(Expo.StartTime,@t04))
        
        Expo.UpdateProgressText()
        Expo.UpdateProgressText('Locations to process: '&Records(LocationQueue))
        Expo.UpdateProgressText()

    END

    FinalExportPath = SetReportsFolder('ServiceBase Export','Stock Adjustment Report',0)
    IF (FinalExportPath = '')

        If not automatic
            Expo.UpdateProgressText('Unable to trace export path for export. Please try again')
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Report Abandoned: ' & FORMAT(Today(),@d06)& |
                    ' ' & FORMAT(clock(),@t04))
            Expo.FinishProgress()
        END

        EXIT
    END

    !start up the excel interface  "E1"
    If E1.Init(0,0) = 0

        If not automatic
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Unable to create connection to Excel for export. Please try again')
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Report Abandoned: ' & FORMAT(Today(),@d06) & |
                    ' ' & FORMAT(clock(),@t04))
            Expo.FinishProgress()
        END

        EXIT

    End ! If E1.Init(0,0) = False


    !column headings
    Do SetUpSheets

    !turning of the calculations should seriously improve performance "Provides a dramatic performance increase."
    E1.SetCalculations(oix:CalculationManual)

    ! Screen updating can be turned off to provide a small performance boost
    ! in cases where the window is hidden or the user does not need to see the 
    ! document being updated
    E1.SetScreenUpdating(false)


    !--------------------------------------------------
    !RUN THE MAIN LOOP OF THE EXPORT
    ReportCancelled = false
    Do LocationLoop      !for the relevant workbook
    !--------------------------------------------------


    if not Automatic
        
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Closing Operations ')
        Expo.UpdateProgressText()

    END

    if ReportCancelled = true then
        E1.Kill()
        exit
    END !if report has been cancelled



    E1.CloseWorkBook(2)
    E1.Kill

    !say goodbye nicely
    if not AutoMatic

        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Report Completed: ' & FORMAT(Today(),@d06) & |
            ' ' & FORMAT(clock(),@t04))
        Expo.FinishProgress(FinalExportPath)

    END

    exit
SetUpSheets          Routine

    !start creating stuff in excel
    E1.NewWorkBook()

    !default sheet
    e1.RenameWorkSheet('Details')
    e1.SelectWorkSheet('Details')

    !some bits are the same on both pages
    do SetupCommon

    !extra rows of section on details page
    e1.WriteToCell('Section Name',  'A9')
    e1.WriteToCell('location name', 'B9')
    e1.WriteToCell('Total Records', 'D9')
    e1.WriteToCell('0',             'E9')
    e1.WriteToCell('Showing',       'F9')
    e1.WriteToCell('0',             'E9')

    !Extra row of titles on details page
    e1.WriteToCell( 'Trn Number',         'A11')
    e1.WriteToCell( 'Trn Date',           'B11')
    e1.WriteToCell( 'Part Number',        'C11')
    e1.WriteToCell( 'Description',        'D11')
    e1.WriteToCell( 'Trn Type',           'E11')
    e1.WriteToCell( 'Reasons / Comments', 'F11')
    e1.WriteToCell( 'Qty Adj',            'G11')
    e1.WriteToCell( 'Unit Amount',        'H11')
    e1.WriteToCell( 'Line Amount',        'I11')

    !formats on details page
    e1.SelectColumns ('H','I')
    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, ,,)
    e1.SelectColumns('C')
    e1.SetCellNumberFormat(oix:NumberFormatText,,0,,)


    !extra row of titles on details page
    e1.SetCellBackgroundColor(color:silver,'A11','I11')
    e1.SetCellBorders('A11','I11',oix:BorderEdgeBottom ,oix:LineStyleContinuous)
    e1.SetCellBorders('A11','I11',oix:BorderEdgeTop    ,oix:LineStyleContinuous)
    e1.SetCellBorders('A11','I11',oix:BorderEdgeRight  ,oix:LineStyleContinuous)


    e1.SelectRows ('12')   
    e1.FreezePanes ()


!According to Harvey there should be no summary sheet - all this is to be removed
!
!    e1.InsertWorksheet()
!    e1.RenameWorkSheet('Summary')
!    e1.SelectWorkSheet('Summary')
!
!    !where we start
!    ActiveRow = 12
!
!    !down to criteria this is the same as details
!    do SetupCommon
!
!    e1.WriteToCell('Summary',                   'A9')
!    e1.WriteToCell('Stock Audit Inc. Qty',      'A10')
!    e1.WriteToCell('Stock Audit Inc. Cost',     'A11')
!    e1.WriteToCell('Stock Audit Dec. Qty',      'A12')
!    e1.WriteToCell('Stock Audit Dec. Cost',     'A13')
!    e1.WriteToCell('Stock Adj Inc. Qty',        'A14')
!    e1.WriteToCell('Stock Adj Inc. Cost',       'A15')
!    e1.WriteToCell('Stock Adj Dec. Qty',        'A16')
!    e1.WriteToCell('Stock Adj Dec. Cost',       'A17')
!    e1.WriteToCell('Stock Audit Total Qty',     'A18')
!    e1.WriteToCell('Stock Audit Total Cost',    'A19')
!    e1.WriteToCell('Stock Adj. Total Qty',      'A20')
!    e1.WriteToCell('Stock Adj. Total Cost',     'A21')
!    e1.WriteToCell('Total Qty',                 'A22')
!    e1.WriteToCell('Total Cost',                'A23')
!
!    !formats on summary page
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B11',)
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B13',)
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B15',)
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B17',)
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B19',)
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B21',)
!    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'B23',)
!
!    !Reset Column Widths on summary page
!    e1.SetColumnWidth('A','','22')
!    e1.SetColumnWidth('F','','15')

    EXIT


SetupCommon             Routine

    e1.WriteToCell('Stock Adjustment ',  'A1')
    e1.WriteToCell(tmp:VersionNumber,    'G1')
    e1.WriteToCell('Criteria',           'A3')
    e1.WriteToCell('Start Date:',        'A4')
    e1.WriteToCell(format(param:StartDate,@d08),   'B4')
    e1.WriteToCell('End Date:',          'A5')
    e1.WriteToCell(format(param:EndDate,@d08),     'B5')
    e1.WriteToCell('Created By:',        'A6')
    e1.WriteToCell(LOC:UserName,         'B6')
    e1.WriteToCell('Created Date:',      'A7')
    e1.WriteToCell(format(today(),@d08), 'B7')

    !set some font sizes and boldness
    e1.SetCellFontSize(14,'A1')             !bold font and size for A1
    e1.SetCellFontSize(8,'G1')
    e1.SetCellFontStyle('Bold','A1')
    e1.SetCellFontStyle('Bold','A3')
    e1.SetCellFontStyle('Bold','B9')

    !Column Widths 
    e1.SetColumnWidth('A','','15')
    e1.SetColumnWidth('B','','15')
    e1.SetColumnWidth('C','','15')
    e1.SetColumnWidth('D','','15')
    e1.SetColumnWidth('E','','15')
    e1.SetColumnWidth('F','','22')
    e1.SetColumnWidth('G','','15')
    e1.SetColumnWidth('H','','15')
    e1.SetColumnWidth('I','','15')

    !Colour rows 1,3,4-7,9,11 and add borders
    e1.SetCellBackgroundColor(color:silver,'A1','I1')
    e1.SetCellBorders('A1','I1',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A3','I3')
    e1.SetCellBorders('A3','I3',oix:BorderEdgeTop   ,oix:LineStyleContinuous)
    e1.SetCellBorders('A3','I3',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A4','I7')
    e1.SetCellBorders('A7','I7',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A9','I9')
    e1.SetCellBorders('A9','I9',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    e1.SetCellBorders('A9','I9',oix:BorderEdgeTop,oix:LineStyleContinuous)

    EXIT
LocationLoop        Routine

    LOOP Counter = 1 TO RECORDS(LocationQueue)
        !-------------------------------------------------------------
        GET(LocationQueue, Counter)
        LOC:LocationName = LocationQueue.LocationValue


        if not Automatic

            Expo.UpdateProgressText('======================')          !blank line
            Expo.UpdateProgressText('Start on location ' & clip(LOC:LocationName))
            Expo.UpdateProgressText('Location: '&clip(counter)&' of '&Records(LocationQueue))
            Expo.UpdateProgressText()

        END

        !clear old data on the details sheets
!        E1.SelectWorkSheet('Details')                      !Harvey - there is no summary sheet
        E1.SelectCells ('A12', 'I'&clip(ActiveRow))
        E1.DeleteSelection (oix:Up)

        !write new data in headings
        e1.WriteToCell('Stock Adjustment Export '&clip(Loc:LocationName),'A1')
        e1.WriteToCell(clip(Loc:LocationName),'B9')

        !reset the start point
        E1.SelectCells ('A12')
        ActiveRow = 12

        !fill in data on Stock loop
        Do StockLoop

        !write the records / showing
        e1.WriteToCell(clip(ActiveRow-12),'E9')
        e1.WriteToCell('=SUBTOTAL(2, A12:A'&clip(ActiveRow-1),'G9')
        !e1.WriteToCell(clip(ActiveRow-12),'G9')

!!According to Harvey there should be no summary sheet - all this to remove
!        !Move to the summary sheet
!        E1.SelectWorkSheet('Summary')
!
!        !write new data in headings
!        e1.WriteToCell('Stock Adjustment Export '&clip(Loc:LocationName), 'A1')
!
!        !write the new data to the data section
!        e1.WriteToCell( calc:StockAudAddQty     ,'B10' )
!        e1.WriteToCell( calc:StockAudAddCost    ,'B11' )
!        e1.WriteToCell( calc:StockAudDecQty     ,'B12' )
!        e1.WriteToCell( calc:StockAudDecCost    ,'B13' )
!        e1.WriteToCell( calc:StockAddQty        ,'B14' )
!        e1.WriteToCell( calc:StockAddCost       ,'B15' )
!        e1.WriteToCell( calc:StockDecQty        ,'B16' )
!        e1.WriteToCell( calc:StockDecCost       ,'B17' )
!        e1.WriteToCell( calc:StockAudTotalQty   ,'B18' )
!        e1.WriteToCell( calc:StockAudTotalCost  ,'B19' )
!        e1.WriteToCell( calc:StockAdjTotalQty   ,'B20' )
!        e1.WriteToCell( calc:StockAdjTotalCost  ,'B21' )
!        e1.WriteToCell( calc:TotalQty           ,'B22' )
!        e1.WriteToCell( calc:TotalCost          ,'B23' )

        !Save this book
        E1.SaveAs(clip(FinalExportPath)&'Stock Adjustment Export '&clip(Loc:LocationName)&' '&format(Today(),@d12)&'_'&format(clock(),@t5)&'.xlsx',oix:xlWorkbookDefault )

    END !LOOP

    EXIT

StockLoop       Routine


        CLEAR(Calculation_Group)
        loopcount# = 30

        !Save_sto_ID = Access:STOCK.SaveFile()
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = loc:LocationName
        Set(sto:Location_Key,sto:Location_Key)
        Loop

            If Access:STOCK.NEXT() then break.
            If sto:Location    <> loc:LocationName  Then Break.

            if sto:Suspend = true then cycle.

            if not Automatic

                if loopCount# > 25 then
                    if Expo.UpdateProgressWindow()
                        Expo.UpdateProgressText('===============')
                        Expo.UpdateProgressText('Report interrupted ' & FORMAT(Today(),@d6) & |
                            ' ' & FORMAT(clock(),@t1))
                        Expo.FinishProgress()
                        !ReportCancelled = true
                        break
                    END

                    IF (Expo.CancelPressed = 2)
                        Expo.UpdateProgressText('===============')
                        Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                            ' ' & FORMAT(clock(),@t1))
                        Expo.FinishProgress()
                        !ReportCancelled = true
                        break
                    END

                    !expo.UpdateProgresstext('Start work on invoice number '&clip(inv:Invoice_Number))
                    loopcount# = 0
                END !if loopcount# > 25

            loopcount# += 1

            END


            Access:STOHIST.ClearKey(shi:Ref_Number_Key)
            shi:Ref_Number = sto:Ref_Number
            shi:Date       = param:StartDate
            Set(shi:Ref_Number_Key,shi:Ref_Number_Key)
            Loop

                If Access:STOHIST.NEXT() then break.
                If shi:Ref_Number <> sto:Ref_Number then break.
                if shi:Date   > param:EndDate  Then Break.

                IF shi:Job_Number <> 0 THEN cycle.
                
                IF shi:Transaction_Type = 'ADD' THEN
                  IF shi:Notes = 'STOCK ADDED' THEN
                     !This is an add in!
                     IF INSTRING('STOCK AUDIT',shi:Despatch_Note_Number,1,1) THEN
                        LocalTranType = 'Stock Audit Incrememnt'
!                        calc:StockAudAddQty += shi:Quantity                        !Harvey there is no summary sheet
!                        calc:StockAudAddCost += shi:Quantity * shi:Purchase_Cost   !Harvey there is no summary sheet
                     ELSE
                         LocalTranType = 'Stock Increment'
!                         calc:StockAddQty += shi:Quantity                            !Harvey there is no summary sheet
!                         calc:StockAddCost += shi:Quantity * shi:Purchase_Cost       !Harvey there is no summary sheet
                     END !IF
                     DO WriteStockRow       !below in this embed
                  END !IF 'STOCK ADDED'
                   
                END !IF type = 'ADD'
                IF shi:Transaction_Type = 'DEC' THEN

                  IF shi:Notes = 'STOCK DECREMENTED'
                     !This is an add in!
                     IF INSTRING('STOCK AUDIT',shi:Despatch_Note_Number,1,1)
                        LocalTranType = 'Stock Audit Decrement'
!                        calc:StockAudDecQty += shi:Quantity                          !harvey there is no summary sheet
!                        calc:StockAudDecCost += shi:Quantity * shi:Purchase_Cost     !Harvey there is no summary sheet
                     ELSE
                         LocalTranType = 'Stock Decrement'
!                         calc:StockDecQty += shi:Quantity
!                         calc:StockDecCost += shi:Quantity * shi:Purchase_Cost
                     END !IF
                     DO WriteStockRow       !below in this embed
                  END !IF 'Stock decremented

                END !IF type = 'DEC'

            End !Loop through stohist
        End !Loop through stock


!According to Harvey there is no summary sheet so this is not needed
!        !keep the totals up to date
!        calc:StockAdjTotalQty  = calc:StockAddQty + calc:StockDecQty
!        calc:StockAdjTotalCost = calc:StockAddCost + calc:StockDecCost
!        calc:StockAudTotalQty  = calc:StockAudAddQty + calc:StockAudDecQty
!        calc:StockAudTotalCost = calc:StockAudAddCost + calc:StockAudDecCost
!        calc:TotalQty          = calc:StockAudTotalQty + calc:StockAdjTotalQty
!        calc:TotalCost         = calc:StockAudTotalCost + calc:StockAdjTotalCost

        

        EXIT

WriteStockRow        Routine

    !write new data in the body
    e1.WriteToCell(shi:Record_Number                 ,'A'&clip(ActiveRow))
    e1.WriteToCell(FORMAT(shi:Date,@D08)             ,'B'&clip(ActiveRow))
    e1.WriteToCell(sto:Part_Number                   ,'C'&clip(ActiveRow))
    e1.WriteToCell(sto:Description                   ,'D'&clip(ActiveRow))
    e1.WriteToCell(LocalTranType                     ,'E'&clip(ActiveRow))
    e1.WriteToCell(shi:Information                   ,'F'&clip(ActiveRow))
    e1.WriteToCell(shi:Quantity                      ,'G'&clip(ActiveRow))
    e1.WriteToCell(shi:Purchase_Cost                 ,'H'&clip(ActiveRow))
    e1.WriteToCell((shi:Quantity * shi:Purchase_Cost),'I'&clip(ActiveRow))

    ActiveRow += 1

    EXIT
GetUserName2                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        If Command('/SCHEDULE')
            Automatic = False
            Exit
        End ! If Command('/SCHEDULE')
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE
        tmpPos = INSTRING('%', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
!-------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020613'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StockAdjustmentReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:REPSCHCR.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLC.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  BRWLocation.Init(?LocationList,LocationQueue:Browse.ViewPosition,BRW1::View:Browse,LocationQueue:Browse,Relate:LOCATION,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5006'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Stock Adjustment Export' !          Job=1919        Cust=
  
      param:EndDate   = TODAY()
      param:StartDate = DATE(MONTH(param:EndDate), 1, YEAR(param:EndDate))
  
      excel:Visible   = False
      debug:Active    = True
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName2
      IF GUIMode = 1 THEN
         LocalTimeOut             = 500
         MainWindow{PROP:ICONIZE} = TRUE
         DoAll = 'Y'
      END !IF
  
      !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
      If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
          LOC:UserName = 'AUTOMATED PROCEDURE'
          Automatic = True
      End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        param:StartDate       = DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        param:EndDate       = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
      END
  ?LocationList{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?param:StartDate{Prop:Alrt,255} = MouseLeft2
  ?param:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRWLocation.Q &= LocationQueue:Browse
  BRWLocation.AddSortOrder(,loc:Location_Key)
  BRWLocation.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,loc:Location,1,BRWLocation)
  BIND('LocationTag',LocationTag)
  ?LocationList{PROP:IconList,1} = '~NoTick1.ico'
  ?LocationList{PROP:IconList,2} = '~Tick1.ico'
  BRWLocation.AddField(LocationTag,BRWLocation.Q.LocationTag)
  BRWLocation.AddField(loc:Location,BRWLocation.Q.loc:Location)
  BRWLocation.AddField(loc:RecordNumber,BRWLocation.Q.loc:RecordNumber)
      IF Automatic = TRUE
  !      LOC:StartDate = TODAY()
  !      LOC:EndDate = TODAY()
  !      DoAll = 'Y'
          DO DASBRW::6:DASTAGALL
  
          !Daily and Weekly Report not specified  (DBH: 07-05-2004)
          If Command('/DAILY')
              param:StartDate = Today()
              param:EndDate = Today()
          End !If Command('/DAILY')
  
          !Monthly Report to run from 1st to last day of month  (DBH: 07-05-2004)
          If Command('/WEEKLY')
              Loop day# = Today() To Today()-7 By -1
                  If day# %7 = 1
                      param:StartDate = day#
                      Break
                  End !If day# %7 = 2
              End !Loop day# = Today() To Today()-7 By -1
              Loop day# = Today() To Today() + 7
                  IF day# %7 = 0
                      param:EndDate = day#
                      Break
                  End !IF day# %7 = 0
              End !Loop day# = Today() To Today() + 7 By -1
          End !If Command('/WEEKLY')
  
          !Monthly - Start Date is 1st of Month, End Date is last of month  (DBH: 07-05-2004)
          If Command('/MONTHLY')
              param:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              param:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          End !If Command('/MONTHLY')
  
          DO OKButtonPressed
  !      !POST(Event:Closewindow)
  !      !Call email program!
  !      PUTINI('MAIN','InputDir',CLIP(Loc:Path),CLIP(PATH()) & '\AUTOEMAIL.INI')
  !      PUTINI('MAIN','ExportDir',CLIP(Loc:Path)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
  !      RUN('EMAILDIR',1)
        POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              If rpc:AllLocations
                  DO DASBRW::6:DASTAGALL
              Else ! If rpc:AllLocations
                  Access:REPSCHLC.Clearkey(rpl:LocationKey)
                  rpl:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpl:LocationKey,rpl:LocationKey)
                  Loop ! Begin Loop
                      If Access:REPSCHLC.Next()
                          Break
                      End ! If Access:REPSCHLC.Next()
                      If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
                      LocationValue = rpl:Location
                      Add(LocationQueue)
                  End ! Loop
              End ! If rpc:AllLocations
  
  
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  param:StartDate = Today()
                  param:EndDate = Today()
              Of 2 ! 1st Of Month
                  param:StartDate = Date(Month(Today()),1,Year(Today()))
                  param:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  param:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  param:StartDate = Date(Month(param:EndDate),1,Year(param:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do OKButtonPressed
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?LocationDASSHOWTAG{PROP:Text} = 'Show All'
  ?LocationDASSHOWTAG{PROP:Msg}  = 'Show All'
  ?LocationDASSHOWTAG{PROP:Tip}  = 'Show All'
  LocationCount = RECORDS(LocationQueue) !DAS Taging
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?LocationList{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:REPSCHCR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
 DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020613'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020613'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020613'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:StartDate = TINCALENDARStyle1(param:StartDate)
          Display(?param:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:EndDate = TINCALENDARStyle1(param:EndDate)
          Display(?param:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LocationDASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?param:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?param:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?LocationList
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?LocationDASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LocationList
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?LocationList{PROPLIST:MouseDownRow} > 0) 
        CASE ?LocationList{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?LocationDASTAG)
               ?LocationList{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRWLocation.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     LocationQueue.LocationValue = loc:Location
     GET(LocationQueue,LocationQueue.LocationValue)
    IF ERRORCODE()
      LocationTag = ''
    ELSE
      LocationTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocationTag = '*')
    SELF.Q.LocationTag_Icon = 2
  ELSE
    SELF.Q.LocationTag_Icon = 1
  END


BRWLocation.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRWLocation.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRWLocation.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     LocationQueue.LocationValue = loc:Location
     GET(LocationQueue,LocationQueue.LocationValue)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

