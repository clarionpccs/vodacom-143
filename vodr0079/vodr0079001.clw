

   MEMBER('vodr0079.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0079001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Version Number: 5004
!!! </summary>
The48HourTATReport PROCEDURE 

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:Clipboard        ANY                                   !
tmp:VersionNumber    STRING(30)                            !Version Number
tmp:UserName         STRING(61)                            !User Name
save_aus_id          USHORT                                !
Local                CLASS                                 !
DrawBox              Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour) !
UpdateProgressWindow Procedure(String  func:Text,String func:AccountNumber) !
                     END                                   !
StatusQueue          QUEUE,PRE(staque)                     !
StatusMessage        STRING(60)                            !Status Message
AccountNumber        STRING(30)                            !Account Number
                     END                                   !
SummaryGroup         GROUP,PRE(tmp)                        !
TATCP48Hours         LONG                                  !TAT CP 48 Hours
TATMainStore         LONG                                  !TAT Main Store
TATRRC               LONG                                  !TAT RRC
TATCourier           LONG                                  !TAT Courier
TATARC               LONG                                  !TAT ARC
                     END                                   !
TempFilePath         CSTRING(255)                          !
tmp:StartDate        DATE                                  !Jobs Completed From
tmp:EndDate          DATE                                  !Jobs Completed To
tmp:HeadAccountNumber STRING(30)                           !Head Account Number
tmp:ARCLocation      STRING(30)                            !ARC Location
Progress:Thermometer BYTE(0)                               !Moving Bar
tmp:ARCAccount       BYTE(0)                               !Reporting on ARC?
tmp:ARCCompanyName   STRING(30)                            !Company Name
tmp:ARCUser          BYTE(0)                               !ARC User
TempFileQueue        QUEUE,PRE(tmpque)                     !
AccountNumber        STRING(30)                            !Account Number
CompanyName          STRING(30)                            !Company Name
Count                LONG                                  !Count
FileName             STRING(255)                           !FileName
TATCP48Hours         LONG                                  !TAT CP 48 Hours
TATMainStore         LONG                                  !TAT Main Store
TATRRC               LONG                                  !TAT RRC
TATCourier           LONG                                  !TAT Courier
TATARC               LONG                                  !TAT ARC
                     END                                   !
tmp:Tag              STRING(1)                             !Tag
tmp:AllAccounts      BYTE(0)                               !All Accounts
BRW8::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('48 Hour TAT Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),DOUBLE, |
  TILED,COLOR(00D6EAEFh),GRAY,WALLPAPER('sbback.jpg')
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(009A6A7Ch)
                       BUTTON,AT(648,2),USE(?ButtonHelp),ICON('F1Helpsw.jpg'),FLAT,TRN
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI), |
  TRN
                       PROMPT('48 Hour TAT Report Criteria'),AT(68,42),USE(?WindowTitle),FONT('Tahoma',8,COLOR:White, |
  FONT:bold,CHARSET:ANSI),TRN
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(009A6A7Ch)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(009A6A7Ch),SPREAD, |
  WIZARD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Exchange Issued From'),AT(247,64),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold, |
  CHARSET:ANSI),COLOR(009A6A7Ch)
                           ENTRY(@d6),AT(339,64,64,10),USE(tmp:StartDate),FONT('Tahoma',8,00010101h,FONT:bold,CHARSET:ANSI), |
  LEFT,UPR,COLOR(COLOR:White),MSG('Start Date'),REQ,TIP('Start Date')
                           PROMPT('Exchange Issued To'),AT(247,84),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold, |
  CHARSET:ANSI),COLOR(009A6A7Ch)
                           ENTRY(@d6),AT(339,84,64,10),USE(tmp:EndDate),FONT('Tahoma',8,00010101h,FONT:bold,CHARSET:ANSI), |
  LEFT,UPR,COLOR(COLOR:White),MSG('Jobs Completed To'),REQ,TIP('Jobs Completed To')
                           CHECK('All Accounts'),AT(240,110),USE(tmp:AllAccounts),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI), |
  COLOR(009A6A7Ch),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           PROMPT(''),AT(247,96),USE(?StatusText)
                           LIST,AT(240,124,206,232),USE(?List),FONT(,,00010101h,,CHARSET:ANSI),VSCROLL,COLOR(COLOR:White, |
  COLOR:White,009A6A7Ch),FORMAT('11L(2)I@s1@64L(2)|M~Account Number~@s15@120L(2)|M~Comp' & |
  'any Name~@s30@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Rev tags'),AT(129,177,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(133,201,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(456,174),USE(?DASTAG),ICON('TagItemp.jpg'),FLAT,TRN
                           BUTTON,AT(456,214),USE(?DASTAGAll),ICON('TagAllp.jpg'),FLAT,TRN
                           BUTTON,AT(456,252),USE(?DASUNTAGALL),ICON('Untagalp.jpg'),FLAT,TRN
                           BUTTON,AT(405,59),USE(?Calendar),ICON('lookupp.jpg'),FLAT,TRN
                           BUTTON,AT(405,81),USE(?Calendar:2),ICON('lookupp.jpg'),FLAT,TRN
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),ICON('Printp.jpg'),DEFAULT,FLAT,TRN
                       BUTTON,AT(544,366),USE(?Cancel),ICON('Cancelp.jpg'),FLAT,TRN
                       PROMPT('Report Version'),AT(68,372),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI), |
  COLOR(009A6A7Ch)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('BASIC'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
JobNumber               String(30)
FranchiseNumber         String(30)
DateBooked              String(30)
TimeBooked              String(30)
DOP                     String(30)
Age                     String(30)
Hour48Band              String(30)
IncomingIMEI            String(30)
IncomingMSN             String(30)
IncomingManaufacturer   String(30)
IncomingModelNumber     String(30)
ExchangeIMEI            String(30)
ExchangeMSN             String(30)
ExchangeManufacturer    String(30)
ExchangeModelNumber     String(30)
DateExchangeOrdered     String(30)
TimeExchangeOrdered     String(30)
DateExchangeDespatched  String(30)
TimeExchangeDespatched  String(30)
WaybillNumberMainStore  String(30)
DateExchangeAttached    String(30)
TimeExchangeAttached    String(30)
DateReadyToDespatch     String(30)
TimeReadyToDespatch     String(30)
WaybillNumberRRC        String(30)
DateWaybillGenerated    String(30)
DateARCConfirmWaybill   String(30)
TimeARCConfirmWaybill   String(30)
DateRTSToARC            String(30)
TimeRTSToARC            String(30)
DateRTSToMainStore      String(30)
TimeRTSToMainStore      String(30)
TATMainStore            String(30)
TATRRC                  String(30)
TATCourier              String(30)
TATARC                  String(30)
TATCP48Hours            String(30)
                        End
                    End
!ARCExportFile    File,Driver('BASIC'),Pre(arcexp),Name(glo:ARCExportFile),Create,Bindable,Thread
!Record                  Record
!
!                        End
!                    End
Bryan       Class
CompFieldColour       Procedure()
            End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
progressThermometer Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.UserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                      ! Default Locator
Calendar14           CalendarClass
Calendar15           CalendarClass
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:TotalTATCP48Hours     Long()
local:TotalTATMainStore     Long()
local:TotalTATRRC           Long()
local:TotalTATCourier       Long()
local:TotalTATARC           Long()
local:TotalCount            Long()

local:SummaryRow            Long()
local:Desktop               CString(255)
Code
    !Include('version.inc')
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = '48 Hour TAT'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1) 
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1) 
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot get access to the selected document.'&|
                '|'&|
                '|' & Clip(excel:Filename) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                Icon:Hand,'&OK',1) 
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========','')
        local.UpdateProgressWindow('*** DEBUG MODE ***','')
        local.UpdateProgressWindow('========','')
        local.UpdateProgressWindow('','')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b),'')
    local.UpdateProgressWindow('','')
    local.UpdateProgressWindow('Creating Export Files...','')
    local.UpdateProgressWindow('','')


    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    Access:AUDIT.ClearKey(aud:ActionOnlyKey)
    aud:Action     = 'EXCHANGE UNIT ATTACHED TO JOB'
    aud:Date       = tmp:StartDate
    Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
    Loop
        If Access:AUDIT.NEXT()
           Break
        End !If
        If aud:Date        > tmp:EndDate      |
        Or aud:Action     <> 'EXCHANGE UNIT ATTACHED TO JOB'      |
            Then Break.  ! End If

        Do GetNextRecord2

        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel

        If aud:Type <> 'EXC'
            Cycle
        End !If aud:Type <> 'EXC'

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = aud:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            !Found
            If tmp:AllAccounts = False
                glo:Pointer = wob:HeadAccountNumber
                Get(glo:Queue,glo:Pointer)
                If Error()
                    Cycle
                End !If Error()
            End !If tmp:AllAccounts = False
        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = aud:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Cycle
        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        If job:Exchange_Unit_Number = 0
            Cycle
        End !If job:Exchange_Unit_Number = 0

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
            !Only include jobs that are ticked "48 Hour" (DBH: 22-03-2004)
            If jobe:Engineer48HourOption <> 1
                Cycle
            End !If jobe:Engineer48HourOption <> 1
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        !Make sure the audit entry is for the unit attached (DBH: 22-03-2004)
        If ~Instring('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud:Notes,1,1)
            Cycle
        End !If Instring('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud:Notes,1,1)


        !Create Export File (DBH: 22-03-2004)
        glo:ExportFile = Clip(local:LocalPath) & '48HRTAT-' & Clip(wob:HeadAccountNumber) & Clip(local:ReportStartDate) & Clip(local:ReportStartTime) & '.csv'
        Open(ExportFile)
        If Error()
            Create(ExportFile)
            Open(ExportFile)
            If Error()
                Cycle
            End !If Error()
        End !If Error()

        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found

        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            Cycle
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

        If Command('/DEBUG')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Attached',Format(aud:Date,@d18),'c:\debug.ini')
        End !If Command('/DEBUG')

        !Write Lines Into Export File (DBH: 22-03-2004)
        Do WriteLine

        tmpque:AccountNumber = tra:Account_Number
        Get(TempFileQueue,tmpque:AccountNumber)
        If Error()
            tmpque:AccountNumber = tra:Account_Number
            tmpque:CompanyName  = tra:Company_Name
            tmpque:FileName     = glo:ExportFile
            tmpque:Count        = 1
            tmpque:TATCP48Hours = tmp:TATCP48Hours
            tmpque:TATMainStore = tmp:TATMainStore
            tmpque:TATRRC       = tmp:TATRRC
            tmpque:TATCourier   = tmp:TATCourier
            tmpque:TATARC       = tmp:TATARC
            Add(TempFileQueue)
            local.UpdateProgressWindow('Account: ' & Clip(tra:Account_Number) & '-' & Clip(tra:Company_Name) & '. Jobs Found: ' & tmpque:Count,tra:Account_Number)
        Else !If Error()
            tmpque:FileName = glo:ExportFile
            tmpque:Count    += 1
            tmpque:TATCP48Hours += tmp:TATCP48Hours
            tmpque:TATMainStore += tmp:TATMainStore
            tmpque:TATRRC       += tmp:TATRRC
            tmpque:TATCourier   += tmp:TATCourier
            tmpque:TATARC       += tmp:TATARC
            Put(TempFileQueue)

            staque:AccountNumber = tra:Account_Number
            Get(StatusQueue,staque:AccountNumber)
            staque:StatusMessage = 'Account: ' & Clip(tra:Account_Number) & '-' & Clip(tra:Company_Name) & '. Jobs Found: ' & tmpque:Count
            Put(StatusQueue)
        End !If Error()
        Close(ExportFile)

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel
    End !Loop
    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================','')
        local.UpdateProgressWindow('Building Excel Document..','')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        ExcelSetup(0)
        ExcelMakeWorkBook('48 Hour TAT','','48 Hours TAT')
        ExcelSheetName('Summary')

        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            Do GetNextRecord2
            If tmpque:Count = 0
                Cycle
            End !If tmpque:Count = 0
            ExcelMakeSheet()
            ExcelSheetName(SUB(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName),1,30))
            ExcelSelectRange('A11')
            ExcelCell('Job Number',1)
            ExcelCell('Franchise Branch No',1)
            ExcelCell('Date Booked',1)
            ExcelCell('Time Booked',1)
            ExcelCell('Activation Incoming IMEI',1)
            ExcelCell('Age Of Incoming IMEI',1)
            ExcelCell('48 Hour Band',1)
            ExcelCell('Incoming IMEI',1)
            ExcelCell('Incoming MSN',1)
            ExcelCell('Incoming Make',1)
            ExcelCell('Incoming Model',1)
            ExcelCell('48H Exchange IMEI',1)
            ExcelCell('48H Exchange MSN',1)
            ExcelCell('48H Exchange Make',1)
            ExcelCell('48H Exchange Model',1)
            ExcelCell('Date 48H Exch Ordered',1)
            ExcelCell('Time 48H Exch Ordered',1)
            ExcelCell('Date Exch Despatched Main Store',1)
            ExcelCell('Time Exch Despatched Main Store',1)
            ExcelCell('Waybill Number Main Store',1)
            ExcelCell('Date Exch Attached',1)
            ExcelCell('Time Exch Attached',1)
            ExcelCell('Date Ready To Despatch',1)
            ExcelCell('Time Ready To Despatch',1)
            ExcelCell('Waybill Number RRC',1)
            ExcelCell('Date RRC Waybill Gen',1)
            ExcelCell('Date ARC Waybill Confirm',1)
            ExcelCell('Time ARC Waybill Confirm',1)
            ExcelCell('Date RTS To ARC',1)
            ExcelCell('Time RTS To ARC',1)
            ExcelCell('Date RTS To MS',1)
            ExcelCell('Time RTS To MS',1)
            ExcelCell('TAT Main Store',1)
            ExcelCell('TAT RRC',1)
            ExcelCell('TAT Courier',1)
            ExcelCell('TAT ARC',1)
            ExcelCell('TAT CP 48 Hour',1)
            ExcelAutoFilter('A11:AK11')
            ExcelFreeze('B12')
        End !Loop x# = 1 To Records(TempFileQueue)

        ExcelSaveWorkBook(excel:FileName)
        ExcelClose()

        local.UpdateProgressWindow('Creating Excel Sheets...','')
        local.UpdateProgressWindow('','')

        If E1.Init(0,1) = 0
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred with your Excel document. Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1) 
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If E1.Init(0,0,1) = 0


        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            Do GetNextRecord2
            Yield()
            If tmpque:Count = 0
                Cycle
            End !If tmpque:Count = 0

            local.UpdateProgressWindow('Writing Account: ' & Clip(tmpque:AccountNumber) & ' - ' & Clip(tmpque:CompanyName),'')

            !Open account csv file. Copy the data (DBH: 22-03-2004)
            E1.OpenWorkBook(Clip(tmpque:FileName))
            E1.Copy('A1','AK' & tmpque:Count)

            tmp:Clipboard = ClipBoard()
            SetClipBoard('')
            E1.CloseWorkBook(3)

            !Open "final" document and paste the data (DBH: 22-03-2004)
            E1.OpenWorkBook(excel:FileName)
            E1.SelectWorkSheet(SUB(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName),1,30))
            E1.SelectCells('A12')
            SetClipBoard(tmp:ClipBoard)
            E1.Paste()
            E1.SelectCells('B12')
            !MUST Clear the clipboard (DBH: 22-03-2004)
            SetClipBoard('')
            E1.CloseWorkBook(3)
            local.UpdateProgressWindow('Done.','')
            local.UpdateProgressWindow('','')
        End !Loop x# = 1 To Records(TempFileQueue)
        SetClipBoard('')
        E1.OpenWorkBook(excel:FileName)
    !_____________________________________________________________________

    !SUMMARY
    !_____________________________________________________________________

        !Fill In Summary Sheet  (DBH: 03-03-2004)
        local.UpdateProgressWindow('================','')
        local.UpdateProgressWindow('Building Summary..','')
        local.UpdateProgressWindow('','')

        E1.SelectWorkSheet('Summary')

        Do DrawSummaryTitle

        local:SummaryRow = 11
        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            If tmpque:Count = 0
                Cycle
            End !If tmpque:Count = 0
            E1.WriteToCell(tmpque:AccountNumber,'A' & local:SummaryRow)

            E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)

            tmpque:TATCP48Hours = ABS(tmpque:TATCP48Hours / tmpque:Count)
            BHReturnDaysHoursMins(tmpque:TATCP48Hours,Days#,Hours#,Mins#)
            E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'C' & local:SummaryRow)

            tmpque:TATMainStore = ABS(tmpque:TATMainStore / tmpque:Count)
            BHReturnDaysHoursMins(tmpque:TATMainStore,Days#,Hours#,Mins#)
            E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'D' & local:SummaryRow)

            tmpque:TATRRC = ABS(tmpque:TATRRC / tmpque:Count)
            BHReturnDaysHoursMins(tmpque:TATRRC,Days#,Hours#,Mins#)
            E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'E' & local:SummaryRow)

            tmpque:TATCourier = ABS(tmpque:TATCourier / tmpque:Count)
            BHReturnDaysHoursMins(tmpque:TATCourier,Days#,Hours#,Mins#)
            E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'F' & local:SummaryRow)

            tmpque:TATARC = ABS(tmpque:TATARC / tmpque:Count)
            BHReturnDaysHoursMins(tmpque:TATARC,Days#,Hours#,Mins#)
            E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'G' & local:SummaryRow)

            local:SummaryRow += 1

            local:TotalTATCP48Hours += tmpque:TATCP48Hours
            local:TotalTATMainSTore += tmpque:TATMainStore
            local:TotalTATRRC += tmpque:TATRRC
            local:TotalTATCourier += tmpque:TATCourier
            local:TotalTATARC += tmpque:TATARC
            local:TotalCount += tmpque:Count

        End !Loop x# = 1 To Records(TempFileQueue)

        !Write Summary Total Line (DBH: 25-03-2004)
        E1.WriteToCell('Total','A' & local:SummaryRow)
        E1.WriteToCell(local:TotalCount,'B' & local:SummaryRow)

        BHReturnDaysHoursMins(local:TotalTATCP48Hours,Days#,Hours#,Mins#)
        E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'C' & local:SummaryRow)

        BHReturnDaysHoursMins(local:TotalTATMainStore,Days#,Hours#,Mins#)
        E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'D' & local:SummaryRow)

        BHReturnDaysHoursMins(local:TotalTATRRC,Days#,Hours#,Mins#)
        E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'E' & local:SummaryRow)

        BHReturnDaysHoursMins(local:TotalTATCourier,Days#,Hours#,Mins#)
        E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'F' & local:SummaryRow)

        BHReturnDaysHoursMins(local:TotalTATARC,Days#,Hours#,Mins#)
        E1.WriteToCell('''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02),'G' & local:SummaryRow)

        E1.SetCellFontStyle('Bold','A' & local:SummaryRow,'G' & local:SummaryRow)
        Local.DrawBox('A' & local:SummaryRow,'G' & local:SummaryRow,'A' & local:SummaryRow,'G' & local:SummaryRow,color:Silver)


    !_____________________________________________________________________

    !FORMATTING
    !_____________________________________________________________________

        local.UpdatePRogressWindow('Finishing Off Formatting..','')

        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            If tmpque:Count = 0
                Cycle
            End !If tmpque:Count = 0
            Do GetNextRecord2
            E1.SelectWorkSheet(SUB(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName),1,30))
            If tmpque:AccountNumber = tmp:HeadAccountNumber
                tmp:ARCAccount = 1
            Else !If tmpque:AccountNumber = tmp:HeadAccountNumber
                tmp:ARCAccount = 0
            End !If tmpque:AccountNumber = tmp:HeadAccountNumber
            Do DrawDetailTitle

            E1.SetCellFontSize(12,'A1')
            E1.SetCellFontStyle('Bold','A1')
            E1.WriteToCell('48 HOUR TAT (' & Clip(tmpque:CompanyName) & ')'   ,'A1')

            E1.SetCellFontSize(8,'A11','AK11')

            !Records and Records Shown (DBH: 22-03-2004)
            E1.WriteToCell('=SUBTOTAL(2, A12:A' & tmpque:Count + 11,'E5')
            E1.WriteToCell(tmpque:Count,'D5')

            E1.SetCellFontStyle('Bold','A' & tmpque:Count + 12,'Z' & tmpque:Count + 13)
            E1.SetCellFontStyle('Bold','K4','Z5')

            E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,,'H1','I' & tmpque:Count + 11)
            E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,,'L1','M' & tmpque:Count + 11)

            E1.SetCellNumberFormat(oix:NumberFormatTime,,,6,'AG1','AK' & tmpque:Count + 11)

        End !Loop x# = 1 To Records(TempFileQueue)
        SetClipBoard('')
        E1.SelectWorkSheet('Summary')
        E1.CloseWorkBook(3)
        E1.Kill()


        local.UpdateProgressWindow('================','')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b),'')

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)
    
    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02),'')
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    Else
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End ! If ~Commend('/SCHEDULE')
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
WriteLine       Routine !Write CSV Files (DBH: 22-03-2004)
Data
local:Time          Long()

local:DateExchangeOrdered           Date()
local:TimeExchangeOrdered           Time()

local:DateReadyToDespatch           Date()
local:TimeReadyToDespatch           Time()

local:DateARCConfirmwaybill         Date()
local:TimeARCConfirmwaybill         Time()

local:DateRTSToARC                  Date()
local:TimeRTSToARC                  Time()

local:DateRTSToMainStore            Date()
local:TimeRTSToMainStore            Time()

local:DateExchangeDespatched        Date()
local:TimeExchangeDespatched        Time()
Code
    Clear(SummaryGroup)

    Clear(exp:Record)
    !Job Number
    exp:JobNumber                 = job:Ref_Number
    !Franchise Branch Number
    exp:FranchiseNumber           = wob:HeadAccountNumber
    !Date Booked
    exp:DateBooked                = Format(job:Date_Booked,@d18)
    !Time Booked
    exp:TimeBooked                = Format(job:Time_Booked,@t01)
    !Date Of Activation
    exp:DOP                       = Format(job:DOP,@d18)
    !Age
    If job:DOP > 0
        exp:Age                 = job:DOP - job:Date_Booked
    Else !If job:DOP > 0
        exp:Age                 = ''
    End !If job:DOP > 0
    !48 Hour Band
    If IsAlpha(Sub(job:Model_Number,Len(Clip(job:Model_Number)),1))
        exp:Hour48Band          = Sub(job:Model_Number,Len(Clip(job:Model_Number)),1)
    Else !If IsAlpha(Sub(job:Model_Number,1,1))
        exp:Hour48Band          = ''
    End !If IsAlpha(Sub(job:Model_Number,1,1))
    
    !Incoming IMEI
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''
    If IMEIError# = 1
        exp:IncomingIMEI            = job:ESN
        exp:IncomingMSN             = job:MSN
    Else !IMEIError# = 1
        exp:IncomingIMEI            = jot:OriginalIMEI
        exp:IncomingMSN             = jot:OriginalMSN
    End !IMEIError# = 1
    If exp:IncomingMSN = 'N/A'
        exp:IncomingMSN = ''
    End !If exp:IncomingMSN <> 'N/A'
    exp:IncomingIMEI = '''' & exp:IncomingIMEI

    If exp:IncomingMSN <> ''
        exp:IncomingMSN  = '''' & exp:IncomingMSN
    End !If exp:IncomingMSN = ''
    !Incoming Manufacturer
    exp:IncomingManaufacturer     = job:Manufacturer
    !Incoming Model
    exp:IncomingModelNumber       = job:Model_Number
    !Exchange IMEI
    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number  = job:Exchange_Unit_Number
    If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        !Found

    Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    exp:ExchangeIMEI              = '''' & xch:ESN
    !Exchange MSN
    If xch:MSN = 'N/A'
        exp:ExchangeMSN = ''
    End !If exp:ExchageMSN = 'N/A'

    If xch:MSN <> ''
        exp:ExchangeMSN = '''' & xch:MSN
    End !If exp:ExchangeMSN <> ''

    !Exchange Manufacturer
    exp:ExchangeManufacturer      = xch:Manufacturer
    !Exchange Model Number
    exp:ExchangeModelNumber       = xch:Model_Number
    !Date/Time Exchange Ordered
    FoundDateOrdered# = 0
    FoundDateDespatched# = 0
    FoundReceivedAtARC# = 0
    FoundRTSToARC# = 0
    Save_aus_ID = Access:AUDSTATS.SaveFile()
    Access:AUDSTATS.ClearKey(aus:RefDateRecordKey)
    aus:RefNumber    = job:Ref_Number
    aus:DateChanged  = 0
    Set(aus:RefDateRecordKey,aus:RefDateRecordKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber    <> job:Ref_Number      |
            Then Break.  ! End If
        If Sub(aus:NewStatus,1,3) = '360' And aus:Type = 'EXC'
            local:DateExchangeOrdered   = aus:DateChanged
            local:TimeExchangeOrdered   = aus:TimeChanged
        End !If Sub(aus:NewStatus,1,3) = '360'
        If Sub(aus:NewStatus,1,3) = '110' And aus:Type = 'EXC'
            !Date Ready To Despatch
            local:DateReadyToDespatch   = aus:DateChanged
            local:TimeReadyToDespatch   = aus:TimeChanged
        End !If Sub(aus:NewStatus,1,3) = '110' And aus:Type = 'EXC'
        If Sub(aus:NewStatus,1,3) = '452' And aus:Type = 'JOB'
            !Date Ready To Despatch
            local:DateARCConfirmWaybill = aus:DateChanged
            local:TimeARCConfirmWaybill = aus:TimeChanged
        End !If Sub(aus:NewStatus,1,3) = '110' And aus:Type = 'EXC'
        If Sub(aus:NewStatus,1,3) = '815' And aus:Type = 'JOB'
            !Date RTS To ARC
            If jobe:SecondExchangeNumber <> 0
                !Date RTS To ARC shows the original unit, but
                !only if a second exchange was attached - 4424 (DBH: 22-07-2004  28)
                local:DateRTSToARC      = aus:DateChanged
                local:TimeRTSToARC      = aus:TimeChanged

            Else !If jobe:SecondExchangeNumber <> 0
                !Date RTS to MS shows the original unit
                !if no second exchange attached. - 4424 (DBH: 22-07-2004  28)
                local:DateRTSToMainStore    = aus:DateChanged
                local:TimeRTSToMainStore    = aus:TimeChanged
            End !If jobe:SecondExchangeNumber <> 0
        End !If Sub(aus:NewStatus,1,3) = '815' And aus:Type = 'JOB'

        IF Sub(aus:NewStatus,1,3) = '815' And aus:Type = '2NE' And jobe:SecondExchangeNumber <> 0
            !Date RTS To MS shows the second exchange
            !date if attached. - 4424 (DBH: 22-07-2004  28)
            local:DateRTSToMainStore    = aus:DateChanged
            local:TimeRTSToMainStore    = aus:TimeChanged
        End !IF Sub(aus:NewStatus,1,3) = '815' And aus:Type = '2NE'
    End !Loop
    Access:AUDSTATS.RestoreFile(Save_aus_ID)

    !Date/Time Exchange
    !Waybill Number Main Store
    FoundTransfer# = 0
    FoundWaybill# = 0
    !_____________________________________________________________________

    !Do not use the fast method for looking up the waybill.
    !Use the waybill file instead. (But there is no key) - 4163 (DBH: 28-04-2004)

!    Access:EXCHHIST.ClearKey(exh:Ref_Number_Key)
!    exh:Ref_Number = xch:Ref_Number
!    exh:Date        = Today()
!    Set(exh:Ref_Number_Key,exh:Ref_Number_Key)
!    Loop
!        If Access:EXCHHIST.NEXT()
!           Break
!        End !If
!        If exh:Ref_Number <> xch:Ref_Number      |
!            Then Break.  ! End If
!        If FoundTransfer# = 0
!            If Instring('48 HOUR UNIT TRANSFERRED TO ' & Clip(tra:SiteLocation),exh:Status,1,1)
!                local:DateExchangeDespatched = exh:Date
!                local:TimeExchangeDespatched = exh:Time
!                FoundTransfer# = 1
!            End !If Instring('48 HOUR UNIT TRANSFERRED TO ' & Clip(wob:HeadAccount),exh:Status,1,1)
!        End !If FoundTransfer# = 0
!        If FoundWaybill# =0
!            x# = Instring('UNIT DESPATCHED ON WAYBILL: ',exh:Status,1,1)
!            If x# > 0
!                exp:WaybillNumberMainStore    = SUB(exh:Status,x# + 28,8)
!                FoundWaybill# = 1
!            End !If x# > 0
!        End !If FoundWaybill# =0
!    End !Loop

    !Loop through the waybills to try and find this job.
    !Then try and work out which is the exchange waybill -  (DBH: 28-04-2004)
    Access:WAYBILLS.ClearKey(way:TypeAccountRecNumberKey)
    way:WayBillType   = 0
    way:AccountNumber = tmp:HeadAccountNumber
    Set(way:TypeAccountRecNumberKey,way:TypeAccountRecNumberKey)
    Loop
        If Access:WAYBILLS.NEXT()
           Break
        End !If
        If way:WayBillType   <> 0      |
        Or way:AccountNumber <> tmp:HeadAccountNumber      |
            Then Break.  ! End If

        Do GetNextRecord2

        If way:WayBillID = '100' And |
            way:FromAccount = tmp:HeadAccountNumber And |
            way:ToAccount   = wob:HeadAccountNumber

            Access:WAYITEMS.ClearKey(wai:WayBillNumberKey)
            wai:WayBillNumber = way:WaybillNumber
            Set(wai:WayBillNumberKey,wai:WayBillNumberKey)
            Loop
                If Access:WAYITEMS.NEXT()
                   Break
                End !If
                If wai:WayBillNumber <> way:WaybillNumber      |
                    Then Break.  ! End If
                If wai:IsExchange      = True And |
                    wai:JobNumber48Hour = job:Ref_Number
                    local:DateExchangeDespatched    = way:TheDate
                    local:TimeExchangeDespatched    = way:TheTime
                    exp:WaybillNumberMainStore      = way:WayBillNumber
                    FoundTransfer# = 1
                    FoundWaybill# = 1
                    Break

                End !wai:JobNumber48Hour = job:Ref_Number
            End !Loop
            If FoundTransfer# = 1
                Break
            End !If FoundTransfer# = 1
        End !way:ToAccount   = wob:HeadAccountNumber
    End !Loop

    !_____________________________________________________________________


    !Date Exchange Attached
    exp:DateExchangeAttached      = Format(aud:Date,@d18)
    exp:TimeExchangeAttached      = Format(aud:Time,@t01)
    !Date Exchange Despatched Main Store
    exp:DateExchangeDespatched = Format(local:DateExchangeDespatched,@d18)
    exp:TimeExchangeDespatched = Format(local:TimeExchangeDespatched,@t01)
    !Date Exchange Ordered
    exp:DateExchangeOrdered       = Format(local:DateExchangeOrdered,@d18)
    exp:TimeExchangeOrdered       = Format(local:TimeExchangeOrdered,@t01)
    !Date Ready To Despatch
    exp:DateReadyToDespatch       = Format(local:DateReadyToDespatch,@d18)
    exp:TimeReadyToDespatch       = Format(local:TimeReadyToDespatch,@t01)
    !Date ARC Confirm Waybill
    exp:DateARCConfirmWaybill     = Format(local:DateARCConfirmWaybill,@d18)
    exp:TimeARCConfirmWaybill     = Format(local:TimeARCConfirmWaybill,@t01)
    !Date RTS To ARC
    exp:DateRTSToARC              = Format(local:DateRTSToARC,@d18)
    exp:TimeRTSToARC              = Format(local:TimeRTSToARC,@t01)
    !Date RTS To Main Store
    exp:DateRTSToMainStore        = Format(local:DateRTSToMainStore,@d18)
    exp:TimeRTSToMainStore        = Format(local:TimeRTSToMainStore,@t01)

    !Waybill Number RRC
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    joc:TheDate   = 0
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFrom = 'RRC' And joc:DespatchTo = 'ARC'
            exp:WaybillNumberRRC          = joc:ConsignmentNumber
            exp:DateWaybillGenerated      = Format(joc:TheDate,@d18)
            Break
        End !If joc:DespatchFrom = 'RRC' And job:DespatchTo = 'ARC'
    End !Loop
    !
    If local:DateExchangeDespatched <> 0 And local:DateExchangeOrdered <> 0
        local:Time  = BHTimeDifference24Hr5DaysMonday(local:DateExchangeOrdered,local:DateExchangeDespatched,local:TimeExchangeOrdered,local:TimeExchangeDespatched,'08:00:00')
        BHReturnDaysHoursMins(local:Time,Days#,Hours#,Mins#)
        exp:TATMainStore              = '''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02)

        tmp:TATMainStore = local:Time

        If Command('/DEBUG')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Exchange Despatched',Format(local:DateExchangeDespatched,@d6) & ' ' & Format(local:TimeExchangeDespatched,@t1),'c:\debug.ini')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Exchange Ordered',Format(local:DateExchangeOrdered,@d6) & ' ' & Format(local:TimeExchangeOrdered,@t1),'c:\debug.ini')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'TAT Main Store',local:Time,'c:\debug.ini')
        End !If Command('/DEBUG')
    Else
        exp:TATMainStore = '''00:00'
    End !If local:DateExchangeDespatched <> 0 And local:DateExchangeOrdered <> 0

    If local:DateExchangeOrdered <> 0
        local:Time  = BHTimeDifference24Hr5DaysMonday(job:Date_Booked,local:DateExchangeOrdered,job:Time_Booked,local:TimeExchangeOrdered,'08:00:00')
        !Include Exchange Attached to Exchange Despatched - 4301 (DBH: 24-05-2004)
        local:Time  += BHTimeDifference24Hr5DaysMonday(aud:Date,local:DateReadyToDespatch,aud:Time,local:TimeReadyToDespatch,'08:00:00')
        BHReturnDaysHoursMins(local:Time,Days#,Hours#,Mins#)
        exp:TATRRC                    = '''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02)

        tmp:TATRRC = local:Time

        If Command('/DEBUG')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Exchange Ordered',Format(local:DateExchangeOrdered,@d6) & ' ' & Format(local:TimeExchangeOrdered,@t1),'c:\debug.ini')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Booked',Format(job:Date_Booked,@d6) & ' ' & Format(job:Time_Booked,@t1),'c:\debug.ini')

            PUTINI('Job Number: ' & Clip(job:Ref_Number),'TAT RRC',local:Time,'c:\debug.ini')
        End !If Command('/DEBUG')
    Else
        exp:TATRRC = '''00:00'
    End !If local:DateExchangeOrdered <> 0 And exp:DateBooked <> 0

    If aud:Date <> 0 And local:DateExchangeDespatched <> 0
        local:Time = BHTimeDifference24Hr5DaysMonday(local:DateExchangeDespatched,aud:Date,local:TimeExchangeDespatched,aud:Time,'08:00:00')
        BHReturnDaysHoursMins(local:Time,Days#,Hours#,Mins#)
        exp:TATCourier                = '''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02)

        tmp:TATCourier = local:Time

        If Command('/DEBUG')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Exchange Attached',Format(exp:DateExchangeAttached,@d6) & ' ' & Format(exp:TimeExchangeAttached,@t1),'c:\debug.ini')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Exchange Despatched',Format(local:DateExchangeDespatched,@d6) & ' ' & Format(local:TimeExchangeDespatched,@t1),'c:\debug.ini')

            PUTINI('Job Number: ' & Clip(job:Ref_Number),'TAT Courier',local:Time,'c:\debug.ini')
        End !If Command('/DEBUG')
    Else
        exp:TATCourier  = '''00:00'
    End !If exp:DateExchangeAttached <> 0 And local:DateExchangeDespatched <> 0

    If local:DateRTSToMainStore <> 0 And local:DateARCConfirmWaybill <> 0
        local:Time =BHTimeDifference24Hr5Days(local:DateARCConfirmWayBill,local:DateRTStoMainStore,local:TimeARCConfirmWaybill,local:TimeRTSToMainStore)
        BHReturnDaysHoursMins(local:Time,Days#,Hours#,Mins#)
        exp:TATARC                    = '''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02)

        tmp:TATARC = local:Time

        If Command('/DEBUG')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date RTS To MS',Format(local:DateRTStoMainStore,@d6) & ' ' & Format(local:TimeRTSToMainStore,@t1),'c:\debug.ini')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date ARC Confirm WayBill',Format(local:DateARCConfirmWaybill,@d6) & ' ' & Format(local:TimeARCConfirmWaybill,@t1),'c:\debug.ini')

            PUTINI('Job Number: ' & Clip(job:Ref_Number),'TAT ARC',local:Time,'c:\debug.ini')
        End !If Command('/DEBUG')
    Else
        !Do not show anything if the unit has not got to ARC - 4301 (DBH: 24-05-2004)
        exp:TATARC = ''
    End !If local:DateRTSToMainStore <> 0 And local:DateARCConfirmWaybill <> 0
    
    If local:DateReadyToDespatch <> 0
        local:Time = BHTimeDifference24Hr5DaysMonday(job:Date_Booked,local:DateReadyToDespatch,job:Time_Booked,local:TimeReadyToDespatch,'08:00:00')
        BHReturnDaysHoursMins(local:Time,Days#,Hours#,Mins#)
        exp:TATCP48Hours              = '''' & (Days# * 24) + Hours# & ':' & Format(Mins#,@n02)

        tmp:TATCP48Hours = local:Time

        If Command('/DEBUG')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Ready To Despatch',Format(local:DateReadyToDespatch,@d6) & ' ' & Format(local:TimeReadyToDespatch,@t1),'c:\debug.ini')
            PUTINI('Job Number: ' & Clip(job:Ref_Number),'Date Booked',Format(job:Date_Booked,@d6) & ' ' & Format(job:Time_Booked,@t1),'c:\debug.ini')

            PUTINI('Job Number: ' & Clip(job:Ref_Number),'TAT CP',local:Time,'c:\debug.ini')
        End !If Command('/DEBUG')
    Else
        exp:TATCP48Hours = '''00:00'
    End !If local:DateReadyToDespatch <> 0 And local:TimeReadyToDespatch <> 0



    Add(ExportFile)
DrawSummaryTitle        Routine   !Set Summary Screen (DBH: 22-03-2004)
    E1.SetColumnWidth('A','','23')
    E1.SetColumnWidth('B','','16')
    E1.SetColumnWidth('C','Z','15')
    E1.WriteToCell('48 Hour TAT Report')
    E1.SetCellFontSize(12,'A1')
    E1.SetCellFontStyle('Bold','A1',)
    
    E1.WriteToCell('Criteria','A3')
    E1.SetCellFontStyle('Bold','A3',)
    E1.WriteToCell('Start Exchange Issue Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')
    E1.WriteToCell('End Exchange Issue Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B5')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B5')

    If tmp:UserName <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(Clip(use:Forename) & ' ' & Clip(use:Surname),'B6')
    End !If tmp:UserName <> ''

    E1.WriteTocell('Date Created','A7')
    E1.WriteToCell(Format(Today(),@d18),'B7')
    E1.WriteToCell('Summary','A9')
    E1.SetCellFontStyle('Bold','B3','G7')
    E1.SetCellFontStyle('Bold','A9','G10')

    !Colour
    Local.DrawBox('A1','G1','A1','G1',color:Silver)
    Local.DrawBox('A3','G3','A3','G3',color:Silver)
    Local.DrawBox('A4','G4','A7','G7',color:Silver)
    Local.DrawBox('A9','G9','A9','G9',color:Silver)
    Local.DrawBox('A10','G10','A10','G10',color:Silver)

    E1.WriteToCell(Clip(tmp:VersionNumber)  ,'D3')

    E1.WriteToCell('Location','A10')
    E1.WriteToCell('No Of 48 Hr Jobs','B10')
    E1.WriteToCell('TAT CP 48 Hr','C10')
    E1.WriteToCell('TAT Main Store','D10')
    E1.WriteToCell('TAT RRC','E10')
    E1.WriteToCell('TAT Courier','F10')
    E1.WriteToCell('TAT ARC','G10')


DrawDetailTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.SetColumnWidth('A','','20')
    E1.SetColumnWidth('B','','18')
    E1.SetColumnWidth('C','','12')
    E1.SetColumnWidth('D','','13')
    E1.SetColumnWidth('E','','21')
    E1.SetColumnWidth('F','','19')
    E1.SetColumnWidth('G','','12')
    E1.SetColumnWidth('H','I','17')
    E1.SetColumnWidth('J','','14')
    E1.SetColumnWidth('K','','15')
    E1.SetColumnWidth('L','M','17')
    E1.SetColumnWidth('N','O','18')
    E1.SetColumnWidth('P','Q','20')
    E1.SetColumnWidth('R','S','29')
    E1.SetColumnWidth('T','','23')
    E1.SetColumnWidth('U','V','18')
    E1.SetColumnWidth('W','X','22')
    E1.SetColumnWidth('Y','Z','19')
    E1.SetColumnWidth('AA','AB','23')
    E1.SetColumnWidth('AC','AF','16')
    E1.SetColumnWidth('AG','AK','15')

    E1.WriteToCell('Criteria','A3')
    E1.SetCellFontStyle('Bold','A3',)
    E1.WriteToCell('Jobs Completed From','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')
    E1.WriteToCell('Jobs Completed To','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B5')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B5')
    If tmp:UserName <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(Clip(use:Forename) & ' ' & Clip(use:Surname),'B6')
    End !If tmp:UserName <> ''
    E1.WriteTocell('Date Created','A7')
    E1.WriteToCell(Format(Today(),@d18),'B7')
    E1.SetCellFontStyle('Bold','B3','E7')

    E1.WriteToCell('Section Name','A9')
    E1.WriteTocell('Detailed','B9')

    Local.DrawBox('A1','AK1','A1','AK1',color:Silver)
    Local.DrawBox('A3','AK3','A3','AK3',color:Silver)
    Local.DrawBox('A4','AK4','A7','AK7',color:Silver)
    Local.DrawBox('A9','AK9','A9','AK9',color:Silver)
    Local.DrawBox('A11','AK11','A11','AK11',color:Silver)

    E1.WriteToCell('In Report','D4')
    E1.WriteToCell('Shown','E4')

    E1.SetCellFontStyle('Bold','A8','AK11')


    


getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Do you want to finish building the Excel document with the data you have compiled so far, or just quit now?','ServiceBase',|
            Icon:Question,'&Finish Off|&Quit Now|&Cancel',3) 
        Of 1 ! &Finish Off Button
            tmp:Cancel = 2
        Of 2 ! &Quit Now Button
            tmp:cancel = 1
        Of 3 ! &Cancel Button
        End!Case Message        
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020624'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('The48HourTATReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('tmp:Tag',tmp:Tag)                                  ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHHIST.Open                                     ! File EXCHHIST used by this procedure, so make sure it's RelationManager is open
  Relate:REPSCHAC.SetOpenRelated()
  Relate:REPSCHAC.Open                                     ! File REPSCHAC used by this procedure, so make sure it's RelationManager is open
  Relate:WAYBILLS.SetOpenRelated()
  Relate:WAYBILLS.Open                                     ! File WAYBILLS used by this procedure, so make sure it's RelationManager is open
  Relate:WAYITEMS.Open                                     ! File WAYITEMS used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDSTATS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSCONS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHCR.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHED.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHLG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:TRADEACC,SELF) ! Initialize the browse manager
  SELF.Open(window)                                        ! Open window
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5004'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,tra:Account_Number_Key)               ! Add the sort order for tra:Account_Number_Key for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(,tra:Account_Number,1,BRW8)     ! Initialize the browse locator using  using key: tra:Account_Number_Key , tra:Account_Number
  BRW8.SetFilter('(Upper(tra:BranchIdentification) <<> '''')') ! Apply filter expression to browse
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:Tag,BRW8.Q.tmp:Tag)                    ! Field tmp:Tag is a hot field or requires assignment from browse
  BRW8.AddField(tra:Account_Number,BRW8.Q.tra:Account_Number) ! Field tra:Account_Number is a hot field or requires assignment from browse
  BRW8.AddField(tra:Company_Name,BRW8.Q.tra:Company_Name)  ! Field tra:Company_Name is a hot field or requires assignment from browse
  BRW8.AddField(tra:RecordNumber,BRW8.Q.tra:RecordNumber)  ! Field tra:RecordNumber is a hot field or requires assignment from browse
  INIMgr.Fetch('The48HourTATReport',window)                ! Restore window settings from non-volatile store
  IF ?tmp:AllAccounts{Prop:Checked}
    DISABLE(?List)
  END
  IF NOT ?tmp:AllAccounts{PROP:Checked}
    ENABLE(?List)
  END
  BRW8.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              tmp:Allaccounts = rpc:AllAccounts
  
              Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
              rpa:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
              Loop ! Begin Loop
                  If Access:REPSCHAC.Next()
                      Break
                  End ! If Access:REPSCHAC.Next()
                  If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                  glo:Pointer = rpa:AccountNumber
                  Add(glo:Queue)
              End ! Loop
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do Reporting
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EXCHHIST.Close
    Relate:REPSCHAC.Close
    Relate:WAYBILLS.Close
    Relate:WAYITEMS.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('The48HourTATReport',window)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020624'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020624'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020624'&'0')
      ***
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{PROP:Checked}
        DISABLE(?List)
      END
      IF NOT ?tmp:AllAccounts{PROP:Checked}
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Calendar
      ThisWindow.Update
      Calendar14.SelectOnClose = True
      Calendar14.Ask('Select a Date',tmp:StartDate)
      IF Calendar14.Response = RequestCompleted THEN
      tmp:StartDate=Calendar14.SelectedDate
      DISPLAY(?tmp:StartDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar15.SelectOnClose = True
      Calendar15.Ask('Select a Date',tmp:EndDate)
      IF Calendar15.Response = RequestCompleted THEN
      tmp:EndDate=Calendar15.SelectedDate
      DISPLAY(?tmp:EndDate)
      END
      ThisWindow.Reset(True)
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = color:White
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text,String    func:AccountNumber)
Code
    staque:StatusMessage = Clip(func:Text)
    staque:AccountNumber = Clip(func:AccountNumber)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
Window
Prog.ProgressSetup        Procedure(Long func:Records)
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
Code
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}
        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.percentText = '0% Completed'

Prog.InsideLoop         Procedure(<String func:String>)
Code
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
    Prog.UserText = Clip(func:String)
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWinow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Prog.ProgressThermometer = 100
    Prog.PercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
    Code
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0


BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2                                ! Set icon from icon list
  ELSE
    SELF.Q.tmp:Tag_Icon = 1                                ! Set icon from icon list
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

