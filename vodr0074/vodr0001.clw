

   MEMBER('vodr0074.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END








WarrantyClaimReport PROCEDURE(func:JobNumber,func:StartDate,func:EndDate,func:Manufacturer)
save_joo_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_job_id          USHORT,AUTO
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:JobNumber        LONG
DefaultAddress       GROUP,PRE(address)
Name                 STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
ReportGroup          GROUP,PRE()
tmp:InFault          STRING(30)
tmp:OutFault1        STRING(30)
tmp:OutFault2        STRING(30)
tmp:OutFault3        STRING(30)
tmp:OutFault4        STRING(30)
tmp:OutFault5        STRING(30)
tmp:PartsTotal       REAL
tmp:ValidationType   STRING(30)
tmp:Labour           REAL
tmp:Parts            REAL
tmp:InvoiceTotal     REAL
tmp:VAT              REAL
tmp:InvoiceGrandTotal REAL
tmp:FullJobNumber    STRING(30)
                     END
tmp:Manufacturer     STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:ARCLocation      STRING(30)
tmp:FirstPage        BYTE(1)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                     END
report               REPORT,AT(406,6979,7521,1813),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1156,7521,10323),USE(?unnamed),FONT('Arial',10,,)
                         STRING('Claim Details'),AT(156,104),USE(?String8),TRN,FONT(,12,,FONT:bold+FONT:underline)
                         STRING('Job Details'),AT(156,1198),USE(?String8:2),TRN,FONT(,12,,FONT:bold+FONT:underline)
                         STRING('Manufacturer: '),AT(156,417),USE(?String9),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,417),USE(job:Manufacturer),TRN,LEFT
                         STRING('Job Number:'),AT(156,1510),USE(?String9:4),TRN,FONT(,,,FONT:bold)
                         STRING('Name:'),AT(4115,1510),USE(?String9:12),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(4948,1510),USE(address:Name),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(1719,1510),USE(tmp:FullJobNumber),TRN,LEFT,FONT('Arial',10,,,CHARSET:ANSI)
                         STRING('Franchise Details'),AT(4115,1198),USE(?String8:3),TRN,FONT(,12,,FONT:bold+FONT:underline)
                         STRING('Fault Codes'),AT(156,3438),USE(?String8:4),TRN,FONT(,12,,FONT:bold+FONT:underline)
                         STRING('Spares Used'),AT(156,5260),USE(?String8:5),TRN,FONT(,12,,FONT:bold+FONT:underline)
                         STRING('Qty'),AT(927,5521),USE(?String61),TRN,FONT(,,,FONT:bold)
                         STRING('Part Number'),AT(1406,5521),USE(?String61:2),TRN,FONT(,,,FONT:bold)
                         STRING('Description'),AT(3646,5521),USE(?String61:3),TRN,FONT(,,,FONT:bold)
                         STRING('Total'),AT(6354,5521),USE(?String61:4),TRN,FONT(,,,FONT:bold)
                         STRING('Labour:'),AT(4313,7979),USE(?String9:24),TRN,FONT(,,,FONT:bold)
                         STRING(@n14.2),AT(6240,7979),USE(tmp:Labour),TRN,RIGHT
                         STRING('Parts:'),AT(4313,8188),USE(?String9:25),TRN,FONT(,,,FONT:bold)
                         STRING(@n14.2),AT(6240,8188),USE(tmp:Parts),TRN,RIGHT
                         STRING('Invoice Total:'),AT(4313,8396),USE(?String9:26),TRN,FONT(,,,FONT:bold)
                         STRING(@n14.2),AT(6240,8396),USE(tmp:InvoiceTotal),TRN,RIGHT
                         LINE,AT(156,7865,7188,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('V.A.T.'),AT(4313,8656),USE(?String9:27),TRN,FONT(,,,FONT:bold)
                         STRING(@n14.2),AT(6240,8656),USE(tmp:VAT),TRN,RIGHT
                         STRING('Invoice Total incl. V.A.T.'),AT(4313,8865),USE(?String9:28),TRN,FONT(,,,FONT:bold)
                         STRING(@n14.2),AT(6240,8865),USE(tmp:InvoiceGrandTotal),TRN,RIGHT
                         BOX,AT(198,9125,7083,1042),USE(?Box1),COLOR(COLOR:Black)
                         STRING('Warranty Validation'),AT(302,9177),USE(?String8:6),TRN,FONT(,12,,FONT:bold+FONT:underline)
                         STRING('Type:'),AT(302,9490),USE(?String9:29),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(979,9490),USE(tmp:ValidationType),TRN,LEFT,FONT('Arial',10,,,CHARSET:ANSI)
                         STRING('Date Booked:'),AT(156,1719),USE(?String9:5),TRN,FONT(,,,FONT:bold)
                         STRING('Date Completed:'),AT(156,1927),USE(?String9:6),TRN,FONT(,,,FONT:bold)
                         STRING(@D6b),AT(1719,1927),USE(job:Date_Completed),TRN,LEFT
                         STRING(@s30),AT(4948,1875),USE(address:AddressLine2),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Address:'),AT(4115,1719),USE(?String9:13),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(4948,1719),USE(address:AddressLine1),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('In Fault:'),AT(156,3750),USE(?String9:18),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,3750),USE(tmp:InFault),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Out Fault 1:'),AT(156,4063),USE(?String9:19),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,4063),USE(tmp:OutFault1),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Out Fault 2:'),AT(156,4271),USE(?String9:20),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,4271),USE(tmp:OutFault2),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Out Fault 3:'),AT(156,4479),USE(?String9:21),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,4479),USE(tmp:OutFault3),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Out Fault 4:'),AT(156,4688),USE(?String9:22),TRN,FONT(,,,FONT:bold)
                         STRING('Out Fault 5:'),AT(156,4896),USE(?String9:23),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,4688),USE(tmp:OutFault4),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(1719,4896),USE(tmp:OutFault5),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@d6b),AT(1719,1719),USE(job:date_booked)
                         STRING('Model Number:'),AT(156,2135),USE(?String9:7),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,2135),USE(job:Model_Number),TRN,LEFT
                         STRING(@s30),AT(4948,2031),USE(address:AddressLine3),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('I.M.E.I. No:'),AT(156,2344),USE(?String9:8),TRN,FONT(,,,FONT:bold)
                         STRING('Serial/M.S.N. No:'),AT(156,2552),USE(?String9:9),TRN,FONT(,,,FONT:bold)
                         STRING(@s20),AT(1719,2344),USE(job:ESN),TRN,LEFT
                         STRING('V.A.T. No:'),AT(4115,2500),USE(?String9:17),TRN,FONT(,,,FONT:bold)
                         STRING('Reg No:'),AT(4115,2292),USE(?String9:14),TRN,FONT(,,,FONT:bold)
                         STRING('Swap I.M.E.I. No:'),AT(156,2760),USE(?String9:10),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,2760),USE(xch:ESN)
                         STRING(@s30),AT(4948,2760),USE(address:Telephone),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(1719,2552),USE(job:MSN),TRN,LEFT
                         STRING(@s30),AT(4948,2500),USE(address:VatNumber),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel No:'),AT(4115,2760),USE(?String9:15),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(4948,2292),USE(address:Postcode),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Swap Serial/M.S.N. No:'),AT(156,2969),USE(?String9:11),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,2969),USE(xch:MSN)
                         STRING('Fax No:'),AT(4115,2969),USE(?String9:16),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(4948,2969),USE(address:Fax),TRN,LEFT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Charge Type: '),AT(156,625),USE(?String9:2),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,625),USE(job:Warranty_Charge_Type),TRN,LEFT
                         STRING('Repair Type:'),AT(156,833),USE(?String9:3),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(1719,833),USE(job:Repair_Type_Warranty)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,177),USE(?detailband)
                           STRING(@n8b),AT(625,0),USE(wpr:Quantity),DECIMAL(14),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(1406,0),USE(wpr:Part_Number),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(3646,0),USE(wpr:Description),FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2b),AT(5938,0),USE(tmp:PartsTotal),RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
NewPage                  DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:5)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(417,9135,7521,208),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         STRING('Warranty Claim Report'),AT(104,52,2604,260),USE(?string20),TRN,LEFT,FONT(,16,,FONT:bold)
                         IMAGE('Vodlogo.gif'),AT(5156,365,2188,1146),USE(?Image1)
                         STRING('File Copy'),AT(4792,52,2604,260),USE(?string20:2),TRN,RIGHT,FONT(,16,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WarrantyClaimReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:JobNumber   = func:JobNumber
  tmp:StartDate  = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:Manufacturer    = func:Manufacturer
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:WARPARTS.UseFile
  Access:USERS.UseFile
  Access:JOBSE.UseFile
  Access:JOBOUTFL.UseFile
  Access:CHARTYPE.UseFile
  Access:REPTYDEF.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      RecordsToProcess = Records(JOBS)
      DO OpenReportRoutine
    OF Event:Timer
        tmp:ARCLocation = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
        
        tmp:FirstPage = 1
        
        If tmp:JobNumber <> 0
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = tmp:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                Do Export
        
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        Else !End !tmp:JobNumber
        
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateCompletedKey)
            job:Date_Completed = tmp:StartDate
            Set(job:DateCompletedKey,job:DateCompletedKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Date_Completed > tmp:EndDate      |
                    Then Break.  ! End If
                RecordsProcessed += 1
                Do DisplayProgress
        
                Do Export
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
        End !tmp:JobNumber
        
        
        LocalResponse = RequestCompleted
        BREAK
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Warranty Claim Report'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


Export      Routine
    Clear(ReportGroup)

    If job:Warranty_Job <> 'YES'
        Exit
    End !If job:Warranty_Job <> 'YES'

    If job:Date_Completed = ''
        Exit
    End !If job:Date_Completed = ''

    If tmp:Manufacturer <> ''
        If job:Manufacturer <> tmp:Manufacturer
            Exit
        End !If job:Manufacturer <> tmp:Manufacturer
    End !If tmp:Manufacturer <> ''

    Access:CHARTYPE.ClearKey(cha:Warranty_Key)
    cha:Warranty    = 'YES'
    cha:Charge_Type = job:Warranty_Charge_Type
    If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !Found
        If cha:Exclude_EDI = 'YES'
            Exit
        End !If cha:Exclude_EDI = 'YES'
    Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign

    Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
    rtd:Manufacturer = job:Manufacturer
    rtd:Warranty     = 'YES'
    rtd:Repair_Type  = job:Repair_Type_Warranty
    If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        !Found
        IF rtd:ExcludeFromEDI
            Exit
        End !IF rtd:ExcludeFromEDI
    Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found
        If tmp:JobNumber = 0
            !If single Job not selected, check booking account
            !against the tag list
            Sort(glo:Queue,glo:Pointer)
            glo:Pointer = wob:HeadAccountNumber
            Get(glo:Queue,glo:Pointer)
            If Error()
                Exit
            End !If Error()
        End !If tmp:JobNumber = 0
    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        tmp:FullJobNumber = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
        address:Name            = tra:Company_Name
        address:AddressLine1    = tra:Address_Line1
        address:AddressLine2    = tra:Address_Line2
        address:AddressLine3    = tra:Address_Line3
        address:Postcode        = tra:Postcode
        address:Telephone       = tra:Telephone_Number
        address:Fax             = tra:Fax_Number
        address:VatNumber       = tra:VAT_Number
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    !Find the In Fault
    Access:MANFAULT.ClearKey(maf:InFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:InFault      = 1
    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
        !Found
        Case maf:Field_Number
            Of 1
                tmp:InFault = job:Fault_Code1
            Of 2
                tmp:InFault = job:Fault_Code2
            Of 3
                tmp:InFault = job:Fault_Code3
            Of 4
                tmp:InFault = job:Fault_Code4
            Of 5
                tmp:InFault = job:Fault_Code5
            Of 6
                tmp:InFault = job:Fault_Code6
            Of 7
                tmp:InFault = job:Fault_Code7
            Of 8
                tmp:InFault = job:Fault_Code8
            Of 9
                tmp:InFault = job:Fault_Code9
            Of 10
                tmp:InFault = job:Fault_Code10
            Of 11
                tmp:InFault = job:Fault_Code11
            Of 12
                tmp:InFault = job:Fault_Code12
        End !Case maf:Field_Number
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        mfo:Field        = tmp:InFault
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            tmp:InFault = Clip(tmp:InFault) & ' - ' & mfo:Description
        Else !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

        End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign

    !List 5 Out Faults
    Count# = 0
    Save_joo_ID = Access:JOBOUTFL.SaveFile()
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
           Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
        Case Count#
            Of 1
                tmp:OutFault1   = Clip(joo:FaultCode) & ' - ' & joo:Description
            Of 2
                tmp:OutFault2   = Clip(joo:FaultCode) & ' - ' & joo:Description
            Of 3
                tmp:OutFault3   = Clip(joo:FaultCode) & ' - ' & joo:Description
            Of 4
                tmp:OutFault4   = Clip(joo:FaultCode) & ' - ' & joo:Description
            Of 5
                tmp:OutFault5   = Clip(joo:FaultCode) & ' - ' & joo:Description
            Else
                Break

        End !Case Count#
    End !Loop
    Access:JOBOUTFL.RestoreFile(Save_joo_ID)

    tmp:Labour  = jobe:ClaimValue
    tmp:Parts   = jobe:ClaimPartsCost
    tmp:InvoiceTotal    = jobe:ClaimValue + jobe:ClaimPartsCost + job:Courier_Cost_Warranty
    tmp:VAT     = jobe:ClaimValue * VatRate(job:Account_Number,'L')/100 + |
                    jobe:ClaimPartsCost * VatRate(job:Account_Number,'P')/100 + |
                    job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
    tmp:InvoiceGrandTotal = tmp:InvoiceTotal + tmp:VAT

    !Get Exchange Details
    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    If job:Exchange_Unit_Number <> 0
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found

        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign

    End !If job:Exchange_Unit_Number <> 0

    !Warranty Validation
    If job:Manufacturer = 'MOTOROLA' Or |
        job:Manufacturer = 'SAMSUNG' Or |
        job:Manufacturer = 'PHILIPS'
        tmp:ValidationType = 'BASTION'
    End !job:Manufacturer = 'PHILIPS'

    If job:Manufacturer = 'NOKIA'
        tmp:ValidationType = 'NOKIA'
    End !If job:Manufacturer = 'NOKIA'

    If tmp:FirstPage = 0
        Print(rpt:NewPage)
    End !If FirstPage# = 1

    tmp:FirstPage = 0

    AnyParts# = 0
    tmp:PartsTotal = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:PartsTotal = wpr:Quantity * wpr:Purchase_Cost
        AnyParts# = 1
        Print(rpt:Detail)
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)
    If AnyParts# = 0
        Print(rpt:Detail)
    End !If AnyParts# = 0





