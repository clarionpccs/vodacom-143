

   MEMBER('impcurr.clw')                                   ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMPCURR001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main                 PROCEDURE                             ! Declare Procedure
prog PW ! ProgressBarLocal(BryanTemplate)

prog:ProgressWindow      WINDOW('Progress...'),AT(,,210,64),CENTER,FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),DOUBLE
                        PROGRESS, AT(17,15,175,12), USE(?prog:ProgressThermometer), RANGE(0,100),SMOOTH
                        STRING(''), AT(0,3,211,10), USE(?prog:UserString), CENTER
                        STRING(''), AT(0,30,208,10), USE(?prog:PercentString), CENTER
                        BUTTON('Cancel'), AT(80,44,50,15), USE(?prog:ProgressCancel)
                        END
                        
locImportFile       Cstring(255),static 
ImportFile          File,Driver('ASCII'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record                  Record
Space1                  String(8)
CurrencyCode            String(10) ! Allow extra space for future
Rate                    String(17) ! 6 decimal places
                        End
                    End

                    MAP
WriteToLog              Procedure(String fText)
                    END
FilesOpened     BYTE(0)

  CODE
  prog.PWProgressWindow &= prog:ProgressWindow
  prog.CancelButtonFEQ = ?prog:ProgressCancel
  prog.UserStringFEQ = ?prog:UserString
  prog.ThermometerFEQ = ?prog:ProgressThermometer
  prog.PercentStringFEQ = ?prog:PercentString
  
  
    WriteToLog('== Started Import ==')
    
    locImportFile = clip(GETINI('EXCHANGERATE','ImportPath',,Clip(Path()) & '\SB2KDEF.INI'))
    
    if (not exists(locImportFile))
        WriteToLog('Import File Doesn''t Exist')
        RETURN
    END
    
    Open(ImportFile)
    If (Error())
        WriteToLog('Error Opening File - ' & clip(error()))
        RETURN
    END
    
    count# = 0
    set(ImportFile)
    LOOP
        next(ImportFile)
        if (Error())
            BREAK
        END
        count# += 1
       
    END
    prog.init(count#)

    do OpenFiles
    
    updated# = 0
    read# = 0
    set(ImportFile,0)
    LOOP
        next(importfile)
        if (error())
            BREAK
        END
        if (prog.UpdateProgressBar())
            BREAK
        END
        
        If (impfil:CurrencyCode = '' OR |
            Instring('Currency',impfil:CurrencyCode,1,1) or | 
            instring('Valuation',impfil:CurrencyCode,1,1))
            CYCLE
        END
        
        read# += 1
        Access:CURRENCY.Clearkey(cur:CurrencyCodeKey)
        cur:CurrencyCode = Clip(Upper(impfil:CurrencyCode))
        If (Access:CURRENCY.TryFetch(cur:CurrencyCodeKey))
            Access:CURRENCY.Clearkey(cur:CorrelationCodeKey)
            cur:CorrelationCode = Clip(Upper(impfil:CurrencyCode))
            If (Access:CURRENCY.TryFetch(cur:CorrelationCodeKey))
                CYCLE
            END
        END
        if (cur:DailyRate <> impfil:Rate)
            cur:DailyRate = impfil:Rate
            cur:LastUpdateDate = Today()
            Access:CURRENCY.TryUpdate()
            updated# += 1
        END
            
    END
    
    Close(ImportFile)
    prog.kill()
    
    do CloseFiles
    WriteToLog('Read: ' & read# & ' / Updated: ' & updated#)
    
    Remove(locImportFile)
    if (Exists(locImportFile))
        WriteToLog('Error: Import File Not Deleted - ' & Error())
    END
    WriteToLog('== Finished Import ==')
    
!--------------------------------------
OpenFiles  ROUTINE
  Access:CURRENCY.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CURRENCY.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:CURRENCY.Close
     FilesOpened = False
  END
WriteToLog          Procedure(String fText)
CODE
    LinePrint(Format(Today(),@d06) & ' ' & Format(clock(), @t01) & ': ' & Clip(fText),'IMPCURR.LOG')
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)

  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)

  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

