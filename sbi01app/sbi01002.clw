

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBI01002.INC'),ONCE        !Local module procedure declarations
                     END


ServiceHistoryExchReport PROCEDURE (func:start_date,func:end_date) !Generated from procedure template - Process

Progress:Thermometer BYTE
tmp:Desktop          CSTRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:FileName         STRING(255)
tmp:Path             STRING(255)
tmp:Count            LONG
save_wpr_id          USHORT,AUTO
pos                  STRING(255)
save_joo_id          USHORT,AUTO
SheetDesc            CSTRING(41)
tmp:DateIn           STRING(30)
tmp:Franchise        STRING(30)
tmp:InvoiceTotal     REAL
tmp:InvoiceDate      DATE
tmp:DateCode         STRING(30)
tmp:Infault          STRING(30)
tmp:OutFault         STRING(30)
tmp:EDI              STRING(3)
tmp:SwapIMEI         STRING(30)
tmp:SwapMSN          STRING(30)
tmp:ProcessBeforeDate DATE
tmp:Comment          STRING(255)
tmp:SparesUsed       STRING(60),DIM(10)
tmp:Quantity         LONG,DIM(10)
tmp:TotalValue       REAL,DIM(10)
tmp:RowStart         LONG(2)
tmp:InFaultDescription STRING(30)
tmp:OutFaultDescription STRING(30)
tmp:BranchCode       STRING(30)
tmp:MostParts        LONG
Start_Date           LONG
End_Date             LONG
Process:View         VIEW(JOBS)
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ServiceHistoryExchReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
!  tmp:Manufacturer    = func:Manufacturer
!  tmp:EDIBatchNumber  = func:BatchNumber
!  tmp:ProcessBeforeDate   = func:ProcessDate
  Start_Date = func:Start_Date
  End_Date = func:End_Date
!  If tmp:EDIBatchNumber = 0
!      tmp:EDI = 'NO'
!  Else !tmp:EDIBatchNumber = 0
!      tmp:EDI = 'YES'
!  End !tmp:EDIBatchNumber = 0
  
  
  glo:Select1 = 0
  
  
    BIND('tmp:Comment',tmp:Comment)
    BIND('tmp:DateCode',tmp:DateCode)
    BIND('tmp:DateIn',tmp:DateIn)
    BIND('tmp:EDI',tmp:EDI)
    BIND('tmp:Franchise',tmp:Franchise)
    BIND('tmp:Infault',tmp:Infault)
    BIND('tmp:InvoiceDate',tmp:InvoiceDate)
    BIND('tmp:InvoiceTotal',tmp:InvoiceTotal)
    BIND('tmp:OutFault',tmp:OutFault)
    BIND('tmp:SparesUsed_10',tmp:SparesUsed[10])
    BIND('tmp:SparesUsed_1',tmp:SparesUsed[1])
    BIND('tmp:SparesUsed_2',tmp:SparesUsed[2])
    BIND('tmp:SparesUsed_3',tmp:SparesUsed[3])
    BIND('tmp:SparesUsed_4',tmp:SparesUsed[4])
    BIND('tmp:SparesUsed_5',tmp:SparesUsed[5])
    BIND('tmp:SparesUsed_6',tmp:SparesUsed[6])
    BIND('tmp:SparesUsed_7',tmp:SparesUsed[7])
    BIND('tmp:SparesUsed_8',tmp:SparesUsed[8])
    BIND('tmp:SparesUsed_9',tmp:SparesUsed[9])
    BIND('tmp:SwapIMEI',tmp:SwapIMEI)
    BIND('tmp:SwapMSN',tmp:SwapMSN)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  Access:INVOICE.UseFile
  Access:MANFAULT.UseFile
  Access:JOBOUTFL.UseFile
  Access:TRADEACC.UseFile
  Access:MANUFACT.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ExcelSetup(0)
      ExcelMakeWorkBook('','ServiceBase 2000 Cellular','Exchange Jobs Report')
      !ExcelMakeSheet()
      ExcelSheetName('Exchange Jobs Report')
      ExcelSelectRange('A1:A1')
      ExcelCellWidth(12)
      ExcelCell('Date Booked',1)
      ExcelCellWidth(14)
      ExcelCell('Date Completed',1)
      ExcelCellWidth(14)
      ExcelCell('Purchase Date',1)
      ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
      ExcelCell('Administrator',1)
      ExcelCellWidth(30)
      ExcelCell('Date Claim Vetted',1)
      ExcelCellWidth(30)
      ExcelCell('Time Claim Vetted',1)
      ! End (DBH 26/06/2007) #9061
      ExcelCellWidth(30)
      ExcelCell('Franchise',1)
      ExcelCellWidth(12)
      ExcelCell('Job Number',1)
      !An Engineer Option to report - 3788 (DBH: 15-04-2004)
      ExcelCellWidth(15)
      ExcelCell('Engineer Option',1)
      ExcelCellWidth(12)
      ExcelCell('Branch Code',1)
      ExcelCellWidth(30)
      ExcelCell('Customer Name',1)
      ExcelCellWidth(12)
      ExcelCell('Invoice Total',1)
      ExcelCellWidth(12)
      ExcelCell('Labour Total',1)
      ExcelCellWidth(12)
      ExcelCell('Spares Total',1)
      ExcelCellWidth(12)
      ExcelCell('Exchange Cost',1)
      ExcelCellWidth(15)
      ExcelCell('Make',1)
      ExcelCellWidth(15)
      ExcelCell('Model',1)
      ExcelCellWidth(20)
      ExcelCell('I.M.E.I. Number',1)
      ExcelCellWidth(20)
      ExcelCell('Serial Number',1)
      ExcelCellWidth(20)
      ExcelCell('Swap I.M.E.I. Number',1)
      ExcelCellWidth(20)
      ExcelCell('Swap Serial Number',1)
      ExcelCellWidth(10)
      ExcelCell('Date Code',1)
      ExcelCell('In Fault Code',1)
      ExcelCellWidth(30)
      ExcelCell('In Fault Description',1)
      ExcelCellWidth(10)
      ExcelCell('Out Fault Code',1)
      ExcelCellWidth(30)
      ExcelCell('Out Fault Description',1)
      ExcelCellWidth(20)
      ExcelCell('Labour Level',1)
      ExcelCellWidth(30)
      ExcelCell('Comment',1)
  
      ExcelSelectRange('A2:A2')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Date_Completed)
  ThisProcess.AddSortOrder(job:DateCompletedKey)
  ThisProcess.AddRange(job:Date_Completed,Start_Date,End_Date)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  ExcelSelectRange('X1:X1')
  Loop x# = 1 To tmp:MostParts
      ExcelCellWidth(30)
      ExcelCell('Part Number',1)
      ExcelCell('Description',1)
      ExcelCellWidth(12)
      ExcelCell('Quantity',1)
      ExcelCellWidth(12)
      ExcelCell('Total Value',1)
  End !Loop
  !SHGetSpecialFolderPath( GetDesktopWindow(), tmp:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
  tmp:FileName = PAth()&'\ExchangedJobsReport_'&FORMAT(TODAY(),@d12)&'.xls'
  !MESSAGE(tmp:Filename)
  ExcelDeleteSheet('Sheet3')
  ExcelSaveWorkBook(tmp:FileName)
  ExcelClose()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:WEBJOB.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  !Before Print
  IF job:Exchange_Unit_Number = 0
    Return Record:Filtered
  End !If man:POPRequired
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      tmp:InvoiceTotal = jobe:ClaimValue + job:Parts_Cost_Warranty + job:Courier_Cost_Warranty
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      tmp:InvoiceTotal = 0
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
  inv:Invoice_Number  = job:Invoice_Number_Warranty
  If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      !Found
      tmp:InvoiceDate = inv:Date_Created
  Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      !Error
      tmp:InvoiceDate = ''
  End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
  
  
  tmp:Franchise = ''
  tmp:BranchCode = ''
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = wob:HeadAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          
          tmp:Franchise   = tra:Company_Name
          tmp:BranchCode  = tra:BranchIdentification
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  tmp:SwapIMEI = ''
  tmp:SwapMSN = ''
  If job:Exchange_unit_Number <> 0
      !RRC exchanges for normal jobs appear on the report
      !48 Hour RRC exchanges do not
      !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
      If jobe:Engineer48HourOption = 1
          If jobe:ExchangedATRRC = True
              If jobe:SecondExchangeNumber <> 0
                  ExchangeNumber# = jobe:SecondExchangeNumber            
              Else !If jobe:SecondExchangeNumber <> 0
                  ExchangeNumber# = 0
              End !If jobe:SecondExchangeNumber <> 0
          End !If jobe:ExchangeATRRC = True
      Else !jobe:Exchange48HourOption = 1
          ExchangeNumber# = job:Exchange_Unit_Number
      End !jobe:Exchange48HourOption = 1
          
      If ExchangeNumber# <> 0
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = ExchangeNumber#
          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Found
              tmp:SwapIMEI    = xch:ESN
              tmp:SwapMSN     = xch:MSN
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      End !If ShowExchange# = True
  End !job:Exchange_unit_Number <> 0
  
  
  tmp:InFault = ''
  tmp:InFaultDescription = ''
  Access:MANFAULT.ClearKey(maf:InFaultKey)
  maf:Manufacturer = job:Manufacturer
  maf:InFault      = 1
  If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Found
      
      Case maf:Field_Number
          Of 1
              tmp:Infault = job:Fault_Code1
          Of 2
              tmp:Infault = job:Fault_Code2
          Of 3
              tmp:Infault = job:Fault_Code3
          Of 4
              tmp:Infault = job:Fault_Code4
          Of 5
              tmp:Infault = job:Fault_Code5
          Of 6
              tmp:Infault = job:Fault_Code6
          OF 7
              tmp:Infault = job:Fault_Code7
          Of 8
              tmp:Infault = job:Fault_Code8
          Of 9
              tmp:Infault = job:Fault_Code9
          Of 10
              tmp:Infault = job:Fault_Code10
          Of 11
              tmp:Infault = job:Fault_Code11
          Of 12
              tmp:Infault = job:Fault_Code12
      End !Case maf:Field_Number
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
      mfo:Field        = tmp:Infault
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          !Found
          tmp:InFaultDescription  = mfo:Description
      Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
  Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
  
  tmp:OutFault = ''
  tmp:OutFaultDescription = ''
  
  Access:MANFAULT.ClearKey(maf:MainFaultKey)
  maf:Manufacturer = job:Manufacturer
  maf:MainFault      = 1
  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
      !Found
      Case maf:Field_Number
          Of 1
              tmp:Outfault = job:Fault_Code1
          Of 2
              tmp:Outfault = job:Fault_Code2
          Of 3
              tmp:Outfault = job:Fault_Code3
          Of 4
              tmp:Outfault = job:Fault_Code4
          Of 5
              tmp:Outfault = job:Fault_Code5
          Of 6
              tmp:Outfault = job:Fault_Code6
          OF 7
              tmp:Outfault = job:Fault_Code7
          Of 8
              tmp:Outfault = job:Fault_Code8
          Of 9
              tmp:Outfault = job:Fault_Code9
          Of 10
              tmp:Outfault = job:Fault_Code10
          Of 11
              tmp:Outfault = job:Fault_Code11
          Of 12
              tmp:Outfault = job:Fault_Code12
      End !Case maf:Field_Number
      !If there is no out fault, just get the first one in the OutFault List and use that description
      If tmp:OutFault = ''
          Save_joo_ID = Access:JOBOUTFL.SaveFile()
          Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          joo:FaultCode = '0'
          Set(joo:JobNumberKey,joo:JobNumberKey)
          Loop
              If Access:JOBOUTFL.NEXT()
                 Break
              End !If
              If joo:JobNumber <> job:Ref_Number      |
              Or joo:FaultCode <> '0'      |
                  Then Break.  ! End If
              tmp:OutFaultDescription = joo:Description
              Break
          End !Loop
          Access:JOBOUTFL.RestoreFile(Save_joo_ID)
      Else !If tmp:OutFault = ''
          !There could be duplicate fault codes, so check
          !the out fault list for the selected fault code, and use that description
          !If not, then check the fault code file, and hope you get the right one.
          Access:joboutfl.clearkey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          joo:FaultCode = tmp:outFault
          if access:joboutfl.fetch(joo:jobNumberKey) = Level:Benign
              tmp:OutFaultDescription  = joo:Description
          ELSE
          !for previous jobs try from manfaulo - but this may give errors
              Access:MANFAULO.ClearKey(mfo:Field_Key)
              mfo:Manufacturer = job:Manufacturer
              mfo:Field_Number = maf:Field_Number
              mfo:Field        = tmp:Outfault
              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                  !Found
                  tmp:OutFaultDescription  = mfo:Description
              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          END !If access:joboutfl
      End !If tmp:OutFault = ''
  
  Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
  
  tmp:Comment = ''
  Save_joo_ID = Access:JOBOUTFL.SaveFile()
  Access:JOBOUTFL.ClearKey(joo:LevelKey)
  joo:JobNumber = job:Ref_Number
  joo:Level     = 0
  Set(joo:LevelKey,joo:LevelKey)
  Loop
      If Access:JOBOUTFL.NEXT()
         Break
      End !If
      If joo:JobNumber <> job:Ref_Number      |
      Or joo:Level     <> 0      |
          Then Break.  ! End If
      If tmp:Comment <> ''
          tmp:Comment = Clip(tmp:Comment) & ', '
      End !If tmp:Comment <> ''
      tmp:Comment = Clip(tmp:Comment) & joo:Description
  End !Loop
  Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  
  !Work out Date Code
  Case job:Manufacturer
      Of 'SIEMENS'
          tmp:DateCode    = job:Fault_Code3
      Of 'ALCATEL'
          tmp:DateCode    = job:Fault_Code3
      Of 'ERICSSON'
          tmp:DateCode    = ''''&Clip(job:Fault_Code7) & Clip(job:Fault_Code8)
      Else
          tmp:DateCode    = '0'
  End !job:Manufacturer
  
  
  ExcelSelectSheet('Exchange Jobs Report')
  ExcelSelectRange('A' & tmp:RowStart)
  ExcelCell(Format(job:Date_Booked,@d06),0)
  ExcelCell(Format(job:Date_Completed,@d06),0)
  ExcelCell(Format(job:DOP,@d06),0)
  ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
  Found# = 0
  Access:AUDIT.Clearkey(aud:TypeRefKey)
  aud:Ref_Number = job:Ref_Number
  aud:Type = 'JOB'
  aud:Date = Today()
  Set(aud:TypeRefKey,aud:TypeRefKey)
  Loop ! Begin Loop
      If Access:AUDIT.Next()
          Break
      End ! If Access:AUDIT.Next()
      If aud:Ref_Number <> job:Ref_Number
          Break
      End ! If aud:Ref_Number <> job:Ref_Number
      If aud:Type <> 'JOB'
          Break
      End ! If aud:Type <> 'JOB'
      If aud:Date > Today()
          Break
      End ! If aud:Date > Today()
  ! Changing (DBH 03/08/2007) # 9246 - Also check for "Warranty Claim Rejected" entry
  !    If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
  ! to (DBH 03/08/2007) # 9246
      If Instring('CLAIM VETTED',Upper(aud:Action),1,1) Or Upper(aud:Action) = 'WARRANTY CLAIM REJECTED'
  ! End (DBH 03/08/2007) #9246
          Access:USERS.ClearKey(use:User_Code_Key)
          use:User_Code = aud:User
          If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Found
              Found# = 1
              ExcelCell(Clip(use:Forename) & ' ' & Clip(use:Surname),0)
              ExcelCell(Format(aud:Date,@d06b),0)
              ExcelCell(Format(aud:Time,@t01b),0)
              Break
          Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Error
  
          End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      End ! If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
  End ! Loop
  If Found# = 0
      ExcelCell('',0)
      ExcelCell('',0)
      ExcelCEll('',0)
  End ! If Found# = 0
  ! End (DBH 26/06/2007) #9061
  ExcelCell(tmp:Franchise,0)
  ExcelFormatCell('########')
  ExcelCell(job:Ref_Number,0)
  ExcelFormatCell('<64>')
  !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
  Case jobe:Engineer48HourOption
      Of 1
          ExcelCell('48H',0)
      Of 2
          ExcelCell('ARC',0)
      Of 3
          ExcelCell('7DT',0)
      Of 4
          ExcelCell('STD',0)
      Else
          ExcelCell('',0)
  End !Case jobe:Engineer48HourOption
  
  ExcelCell(tmp:BranchCode,0)
  ExcelCell(job:Company_Name,0)
  ExcelFormatCell('##00.00')
  ExcelCell(tmp:InvoiceTotal,0)
  ExcelFormatCell('##00.00')
  ExcelCell(jobe:ClaimValue,0)
  ExcelFormatCell('##00.00')
  ExcelCell(jobe:ClaimPartsCost,0)
  ExcelFormatCell('##00.00')
  ExcelCell(job:Courier_Cost_Warranty,0)
  ExcelCell(job:Manufacturer,0)
  ExcelCell(job:Model_Number,0)
  ExcelFormatCell('<64>')
  ExcelCell(job:ESN,0)
  ExcelFormatCell('<64>')
  If job:MSN = 'N/A'
      ExcelCell('',0)
  Else !job:MSN = 'N/A'
      ExcelCell(job:MSN,0)
  End !job:MSN = 'N/A'
  ExcelFormatCell('<64>')
  ExcelCell(tmp:SwapIMEI,0)
  ExcelFormatCell('<64>')
  If tmp:SwapMSN = ''
      ExcelCell('',0)
  Else !tmp:SwapMSN = ''
      ExcelCell(tmp:SwapMSN,0)
  End !tmp:SwapMSN = ''
  If tmp:DateCode = '0'
      ExcelCell('',0)
  Else !tmp:DateCode = 0
      ExcelCell(tmp:DateCode,0)
  End !tmp:DateCode = 0
  ExcelCell(tmp:InFault,0)
  ExcelCell(tmp:InFaultDescription,0)
  ExcelCell(tmp:OutFault,0)
  ExcelCell(tmp:OutFaultDescription,0)
  ExcelCell(job:Repair_Type_Warranty,0)
  ExcelCell(tmp:Comment,0)
  
  Count# = 0
  Save_wpr_ID = Access:WARPARTS.SaveFile()
  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
  wpr:Ref_Number  = job:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.NEXT()
         Break
      End !If
      If wpr:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
      Count# += 1
      ExcelCellWidth(30)
      ExcelCell(wpr:Part_Number,0)
      ExcelCell(wpr:Description,0)
      ExcelCellWidth(12)
      ExcelCell(wpr:Quantity,0)
      ExcelCellWidth(12)
      ExcelFormatCell('##00.00')
      Excelcell(wpr:Quantity * wpr:Purchase_Cost,0)
      
  End !Loop
  Access:WARPARTS.RestoreFile(Save_wpr_ID)
  If Count# > tmp:MostParts
      tmp:MostParts = Count#
  End !Count# > tmp:MostParts
  
  ExcelNewLine(1)
  tmp:RowStart += 1
  
  ReturnValue = PARENT.TakeRecord()
  !After Print
  
  
  tmp:Count += 1
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
ServiceHistoryDates PROCEDURE                         !Generated from procedure template - Window

tmp:date             DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ReturnDate       DATE
End_Date             LONG
window               WINDOW('Only Jobs completed Up To the selected date will be Claimed.'),AT(,,220,100),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,64),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Process Claims Before Date'),USE(?Tab1)
                           PROMPT('Please select the completed Date Range for this report'),AT(8,8,204,20),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Start Date'),AT(8,36),USE(?tmp:date:Prompt)
                           ENTRY(@d6b),AT(84,36,64,10),USE(tmp:date),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                           PROMPT('End Date'),AT(8,52),USE(?End_Date:Prompt)
                           ENTRY(@d6b),AT(84,52,64,10),USE(End_Date),LEFT,FONT(,,,FONT:bold)
                           BUTTON,AT(152,53,10,10),USE(?PopCalendar:2),LEFT,ICON('calenda2.ico')
                         END
                       END
                       PANEL,AT(4,72,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,76,56,16),USE(?Button2),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,76,56,16),USE(?Button1),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ServiceHistoryDates')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?tmp:date{Prop:Alrt,255} = MouseLeft2
  ?End_Date{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:date = TINCALENDARStyle1(tmp:date)
          Display(?tmp:date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          End_Date = TINCALENDARStyle1(End_Date)
          Display(?End_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      ServiceHistoryExchReport(tmp:Date,End_Date)
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?End_Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      tmp:date = Today()-30
      End_Date = TODAY()
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ServiceHistoryReportOBF PROCEDURE                     !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:FileName         STRING(255)
tmp:Path             STRING(255)
tmp:Count            LONG
save_wpr_id          USHORT,AUTO
pos                  STRING(255)
save_joo_id          USHORT,AUTO
SheetDesc            CSTRING(41)
tmp:DateIn           STRING(30)
tmp:Franchise        STRING(30)
tmp:InvoiceTotal     REAL
tmp:InvoiceDate      DATE
tmp:DateCode         STRING(30)
tmp:Infault          STRING(30)
tmp:OutFault         STRING(30)
tmp:EDIBatchNumber   LONG
tmp:Manufacturer     STRING(30)
tmp:EDI              STRING(3)
tmp:SwapIMEI         STRING(30)
tmp:SwapMSN          STRING(30)
tmp:ProcessBeforeDate DATE
tmp:Comment          STRING(255)
tmp:SparesUsed       STRING(60),DIM(10)
tmp:Quantity         LONG,DIM(10)
tmp:TotalValue       REAL,DIM(10)
tmp:RowStart         LONG(2)
tmp:InFaultDescription STRING(30)
tmp:OutFaultDescription STRING(30)
tmp:BranchCode       STRING(30)
tmp:MostParts        LONG
savepath             STRING(255)
Process:View         VIEW(LOGTEMP)
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ServiceHistoryReportOBF')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  !tmp:Manufacturer    = func:Manufacturer
  !tmp:EDIBatchNumber  = func:BatchNumber
  !tmp:ProcessBeforeDate   = func:ProcessDate
  !If tmp:EDIBatchNumber = 0
  !    tmp:EDI = 'NO'
  !Else !tmp:EDIBatchNumber = 0
  !    tmp:EDI = 'YES'
  !End !tmp:EDIBatchNumber = 0
  
  
  glo:Select1 = 0
  
  
    BIND('tmp:Comment',tmp:Comment)
    BIND('tmp:DateCode',tmp:DateCode)
    BIND('tmp:DateIn',tmp:DateIn)
    BIND('tmp:EDI',tmp:EDI)
    BIND('tmp:Franchise',tmp:Franchise)
    BIND('tmp:Infault',tmp:Infault)
    BIND('tmp:InvoiceDate',tmp:InvoiceDate)
    BIND('tmp:InvoiceTotal',tmp:InvoiceTotal)
    BIND('tmp:Manufacturer',tmp:Manufacturer)
    BIND('tmp:OutFault',tmp:OutFault)
    BIND('tmp:SparesUsed_10',tmp:SparesUsed[10])
    BIND('tmp:SparesUsed_1',tmp:SparesUsed[1])
    BIND('tmp:SparesUsed_2',tmp:SparesUsed[2])
    BIND('tmp:SparesUsed_3',tmp:SparesUsed[3])
    BIND('tmp:SparesUsed_4',tmp:SparesUsed[4])
    BIND('tmp:SparesUsed_5',tmp:SparesUsed[5])
    BIND('tmp:SparesUsed_6',tmp:SparesUsed[6])
    BIND('tmp:SparesUsed_7',tmp:SparesUsed[7])
    BIND('tmp:SparesUsed_8',tmp:SparesUsed[8])
    BIND('tmp:SparesUsed_9',tmp:SparesUsed[9])
    BIND('tmp:SwapIMEI',tmp:SwapIMEI)
    BIND('tmp:SwapMSN',tmp:SwapMSN)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:LOGTEMP.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  Access:INVOICE.UseFile
  Access:MANFAULT.UseFile
  Access:JOBOUTFL.UseFile
  Access:TRADEACC.UseFile
  Access:MANUFACT.UseFile
  Access:WARPARTS.UseFile
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      Excel:ProgramName = '2nd Yr Warranty OBF Invoice'
      tmp:FileName = GETINI('OBFDEFAULTS','Path',,CLIP(PATH())&'\SB2KDEF.INI')
  
      If Sub(tmp:FileName,-1,1) = '\'
          tmp:filename = Clip(tmp:FileName) & 'SHREPORT ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(Today()),@n04) & Format(Clock(),@t2)&'.xls'
      Else !If Sub(tmp:FileName,-1,1) = '\'
          tmp:filename = Clip(tmp:FileName) & '\SHREPORT ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(Today()),@n04) & Format(Clock(),@t2)&'.xls'
      End !If Sub(tmp:FileName,-1,1) = '\'
      
      ExcelSetup(0)
      ExcelMakeWorkBook('OBF','ServiceBase 2000 Cellular','Service History Report')
      !ExcelMakeSheet()
      ExcelSheetName('Service History Report')
      ExcelSelectRange('A1:A1')
      ExcelCellWidth(12)
      ExcelCell('Date Booked',1)
      ExcelCellWidth(14)
      ExcelCell('Date Completed',1)
      ExcelCellWidth(14)
      ExcelCell('Purchase Date',1)
      ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
      ExcelCell('Administrator',1)
      ExcelCellWidth(30)
      ExcelCell('Date Claim Vetted',1)
      ExcelCellWidth(30)
      ExcelCell('Time Claim Vetted',1)
      ! End (DBH 26/06/2007) #9061
      ExcelCellWidth(30)
      ExcelCell('Franchise',1)
      ExcelCellWidth(12)
      ExcelCell('Job Number',1)
      !An Engineer Option to report - 3788 (DBH: 15-04-2004)
      ExcelCellWidth(15)
      ExcelCell('Engineer Option',1)
      ExcelCellWidth(12)
      ExcelCell('Branch Code',1)
      ExcelCellWidth(30)
      ExcelCell('Customer Name',1)
      ExcelCellWidth(12)
      ExcelCell('Invoice Total',1)
      ExcelCellWidth(12)
      ExcelCell('Labour Total',1)
      ExcelCellWidth(12)
      ExcelCell('Spares Total',1)
      ExcelCellWidth(12)
      ExcelCell('Exchange Cost',1)
      ExcelCellWidth(15)
      ExcelCell('Make',1)
      ExcelCellWidth(15)
      ExcelCell('Model',1)
      ExcelCellWidth(20)
      ExcelCell('I.M.E.I. Number',1)
      ExcelCellWidth(20)
      ExcelCell('Serial Number',1)
      ExcelCellWidth(20)
      ExcelCell('Swap I.M.E.I. Number',1)
      ExcelCellWidth(20)
      ExcelCell('Swap Serial Number',1)
      ExcelCellWidth(10)
      ExcelCell('Date Code',1)
      ExcelCell('In Fault Code',1)
      ExcelCellWidth(30)
      ExcelCell('In Fault Description',1)
      ExcelCellWidth(10)
      ExcelCell('Out Fault Code',1)
      ExcelCellWidth(30)
      ExcelCell('Out Fault Description',1)
      ExcelCellWidth(20)
      ExcelCell('Labour Level',1)
      ExcelCellWidth(30)
      ExcelCell('Comment',1)
  
      ExcelSelectRange('A2:A2')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisProcess.Init(Process:View, Relate:LOGTEMP, ?Progress:PctText, Progress:Thermometer, ProgressMgr, logtmp:IMEI)
  ThisProcess.CaseSensitiveValue = FALSE
  ThisProcess.AddSortOrder(logtmp:IMEIKey)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Cancel, RequestCancelled)
  SEND(LOGTEMP,'QUICKSCAN=on')
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  ExcelSelectRange('X1:X1')
  Loop x# = 1 To tmp:MostParts
      ExcelCellWidth(30)
      ExcelCell('Part Number',1)
      ExcelCell('Description',1)
      ExcelCellWidth(12)
      ExcelCell('Quantity',1)
      ExcelCellWidth(12)
      ExcelCell('Total Value',1)
  End !Loop
  
  ExcelDeleteSheet('Sheet3')
  ExcelSaveWorkBook(tmp:FileName)
  ExcelClose()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:LOGTEMP.Close
    Relate:WEBJOB.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  !Before Print
  
  Access:JOBS.Clearkey(job:Ref_Number_Key)
  job:Ref_Number  = logtmp:IMEI
  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Found
  
  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Error
      Return Record:Filtered
  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  
  !If tmp:EDIBatchNumber = 0
  !    EDIPricing()
  !    WriteOutFault(job:Manufacturer)
  !    If job:Date_Completed > tmp:ProcessBeforeDate
  !        Return Record:Filtered
  !    End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
  !
  !    If job:Warranty_Job <> 'YES'
  !        Return Record:Filtered
  !    End !If job:Warranty_Job <> 'YES'
  !    !POP Required?
  !    If man:POPRequired
  !        If ~jobe:POPConfirmed
  !            Return Record:Filtered
  !        End !If ~jobe:POPReceived
  !    End !If man:POPRequired
  !End !tmp:EDIBatchNumber = 0
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      tmp:InvoiceTotal = jobe:ClaimValue + jobe:ClaimPartsCost + job:Courier_Cost_Warranty
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      tmp:InvoiceTotal = 0
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
  inv:Invoice_Number  = job:Invoice_Number_Warranty
  If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      !Found
      tmp:InvoiceDate = inv:Date_Created
  Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      !Error
      tmp:InvoiceDate = ''
  End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
  
  
  tmp:Franchise = ''
  tmp:BranchCode = ''
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = wob:HeadAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          
          tmp:Franchise   = tra:Company_Name
          tmp:BranchCode  = tra:BranchIdentification
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  tmp:SwapIMEI = ''
  tmp:SwapMSN = ''
  If job:Exchange_unit_Number <> 0
      !RRC exchanges for normal jobs appear on the report
      !48 Hour RRC exchanges do not
      !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
      If jobe:Engineer48HourOption = 1
          If jobe:ExchangedATRRC = True
              If jobe:SecondExchangeNumber <> 0
                  ExchangeNumber# = jobe:SecondExchangeNumber            
              Else !If jobe:SecondExchangeNumber <> 0
                  ExchangeNumber# = 0
              End !If jobe:SecondExchangeNumber <> 0
          End !If jobe:ExchangeATRRC = True
      Else !jobe:Exchange48HourOption = 1
          ExchangeNumber# = job:Exchange_Unit_Number
      End !jobe:Exchange48HourOption = 1
          
      If ExchangeNumber# <> 0
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = ExchangeNumber#
          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Found
              tmp:SwapIMEI    = xch:ESN
              tmp:SwapMSN     = xch:MSN
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      End !If ShowExchange# = True
  End !job:Exchange_unit_Number <> 0
  
  tmp:InFault = ''
  tmp:InFaultDescription = ''
  Access:MANFAULT.ClearKey(maf:InFaultKey)
  maf:Manufacturer = job:Manufacturer
  maf:InFault      = 1
  If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Found
      
      Case maf:Field_Number
          Of 1
              tmp:Infault = job:Fault_Code1
          Of 2
              tmp:Infault = job:Fault_Code2
          Of 3
              tmp:Infault = job:Fault_Code3
          Of 4
              tmp:Infault = job:Fault_Code4
          Of 5
              tmp:Infault = job:Fault_Code5
          Of 6
              tmp:Infault = job:Fault_Code6
          OF 7
              tmp:Infault = job:Fault_Code7
          Of 8
              tmp:Infault = job:Fault_Code8
          Of 9
              tmp:Infault = job:Fault_Code9
          Of 10
              tmp:Infault = job:Fault_Code10
          Of 11
              tmp:Infault = job:Fault_Code11
          Of 12
              tmp:Infault = job:Fault_Code12
      End !Case maf:Field_Number
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
      mfo:Field        = tmp:Infault
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          !Found
          tmp:InFaultDescription  = mfo:Description
      Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
  Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
  
  tmp:OutFault = ''
  tmp:OutFaultDescription = ''
  
  Access:MANFAULT.ClearKey(maf:MainFaultKey)
  maf:Manufacturer = job:Manufacturer
  maf:MainFault      = 1
  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
      !Found
      Case maf:Field_Number
          Of 1
              tmp:Outfault = job:Fault_Code1
          Of 2
              tmp:Outfault = job:Fault_Code2
          Of 3
              tmp:Outfault = job:Fault_Code3
          Of 4
              tmp:Outfault = job:Fault_Code4
          Of 5
              tmp:Outfault = job:Fault_Code5
          Of 6
              tmp:Outfault = job:Fault_Code6
          OF 7
              tmp:Outfault = job:Fault_Code7
          Of 8
              tmp:Outfault = job:Fault_Code8
          Of 9
              tmp:Outfault = job:Fault_Code9
          Of 10
              tmp:Outfault = job:Fault_Code10
          Of 11
              tmp:Outfault = job:Fault_Code11
          Of 12
              tmp:Outfault = job:Fault_Code12
      End !Case maf:Field_Number
      !If there is no out fault, just get the first one in the OutFault List and use that description
      If tmp:OutFault = ''
          Save_joo_ID = Access:JOBOUTFL.SaveFile()
          Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          joo:FaultCode = '0'
          Set(joo:JobNumberKey,joo:JobNumberKey)
          Loop
              If Access:JOBOUTFL.NEXT()
                 Break
              End !If
              If joo:JobNumber <> job:Ref_Number      |
              Or joo:FaultCode <> '0'      |
                  Then Break.  ! End If
              tmp:OutFaultDescription = joo:Description
              Break
          End !Loop
          Access:JOBOUTFL.RestoreFile(Save_joo_ID)
      Else !If tmp:OutFault = ''
          !There could be duplicate fault codes, so check
          !the out fault list for the selected fault code, and use that description
          !If not, then check the fault code file, and hope you get the right one.
          Access:joboutfl.clearkey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          joo:FaultCode = tmp:outFault
          if access:joboutfl.fetch(joo:jobNumberKey) = Level:Benign
              tmp:OutFaultDescription  = joo:Description
          ELSE
          !for previous jobs try from manfaulo - but this may give errors
              Access:MANFAULO.ClearKey(mfo:Field_Key)
              mfo:Manufacturer = job:Manufacturer
              mfo:Field_Number = maf:Field_Number
              mfo:Field        = tmp:Outfault
              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                  !Found
                  tmp:OutFaultDescription  = mfo:Description
              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          END !If access:joboutfl
      End !If tmp:OutFault = ''
  
  
  Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
  
  tmp:Comment = ''
  Save_joo_ID = Access:JOBOUTFL.SaveFile()
  Access:JOBOUTFL.ClearKey(joo:LevelKey)
  joo:JobNumber = job:Ref_Number
  joo:Level     = 0
  Set(joo:LevelKey,joo:LevelKey)
  Loop
      If Access:JOBOUTFL.NEXT()
         Break
      End !If
      If joo:JobNumber <> job:Ref_Number      |
      Or joo:Level     <> 0      |
          Then Break.  ! End If
      If tmp:Comment <> ''
          tmp:Comment = Clip(tmp:Comment) & ', '
      End !If tmp:Comment <> ''
      tmp:Comment = Clip(tmp:Comment) & joo:Description
  End !Loop
  Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  
  !Work out Date Code
  Case job:Manufacturer
      Of 'SIEMENS'
          tmp:DateCode    = job:Fault_Code3
      Of 'ALCATEL'
          tmp:DateCode    = job:Fault_Code3
      Of 'ERICSSON'
          tmp:DateCode    = ''''&Clip(job:Fault_Code7) & Clip(job:Fault_Code8)
      Else
          tmp:DateCode    = '0'
  End !job:Manufacturer
  
  ExcelSelectSheet('Service History Report')
  ExcelSelectRange('A' & tmp:RowStart)
  ExcelCell(Format(job:Date_Booked,@d06),0)
  ExcelCell(Format(job:Date_Completed,@d06),0)
  ExcelCell(Format(job:DOP,@d06),0)
  ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
  Found# = 0
  Access:AUDIT.Clearkey(aud:TypeRefKey)
  aud:Ref_Number = job:Ref_Number
  aud:Type = 'JOB'
  aud:Date = Today()
  Set(aud:TypeRefKey,aud:TypeRefKey)
  Loop ! Begin Loop
      If Access:AUDIT.Next()
          Break
      End ! If Access:AUDIT.Next()
      If aud:Ref_Number <> job:Ref_Number
          Break
      End ! If aud:Ref_Number <> job:Ref_Number
      If aud:Type <> 'JOB'
          Break
      End ! If aud:Type <> 'JOB'
      If aud:Date > Today()
          Break
      End ! If aud:Date > Today()
  ! Changing (DBH 03/08/2007) # 9246 - Also check for "Warranty Claim Rejected" entry
  !    If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
  ! to (DBH 03/08/2007) # 9246
      If Instring('CLAIM VETTED',Upper(aud:Action),1,1) Or Upper(aud:Action) = 'WARRANTY CLAIM REJECTED'
  ! End (DBH 03/08/2007) #9246
          Access:USERS.ClearKey(use:User_Code_Key)
          use:User_Code = aud:User
          If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Found
              Found# = 1
              ExcelCell(Clip(use:Forename) & ' ' & Clip(use:Surname),0)
              ExcelCell(Format(aud:Date,@d06b),0)
              ExcelCell(Format(aud:Time,@t01b),0)
              Break
          Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Error
  
          End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      End ! If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
  End ! Loop
  If Found# = 0
      ExcelCell('',0)
      ExcelCell('',0)
      ExcelCEll('',0)
  End ! If Found# = 0
  ! End (DBH 26/06/2007) #9061
  ExcelCell(tmp:Franchise,0)
  ExcelFormatCell('########')
  ExcelCell(job:Ref_Number,0)
  ExcelFormatCell('<64>')
  !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
  Case jobe:Engineer48HourOption
      Of 1
          ExcelCell('48H',0)
      Of 2
          ExcelCell('ARC',0)
      Of 3
          ExcelCell('7DT',0)
      Of 4
          ExcelCell('STD',0)
      Else
          ExcelCell('',0)
  End !Case jobe:Engineer48HourOption
  ExcelCell(tmp:BranchCode,0)
  ExcelCell(job:Company_Name,0)
  ExcelFormatCell('##00.00')
  ExcelCell(tmp:InvoiceTotal,0)
  ExcelFormatCell('##00.00')
  ExcelCell(jobe:ClaimValue,0)
  ExcelFormatCell('##00.00')
  ExcelCell(jobe:ClaimPartsCost,0)
  ExcelFormatCell('##00.00')
  ExcelCell(job:Courier_Cost_Warranty,0)
  ExcelCell(job:Manufacturer,0)
  ExcelCell(job:Model_Number,0)
  ExcelFormatCell('<64>')
  ExcelCell(job:ESN,0)
  ExcelFormatCell('<64>')
  If job:MSN = 'N/A'
      ExcelCell('',0)
  Else !job:MSN = 'N/A'
      ExcelCell(job:MSN,0)
  End !job:MSN = 'N/A'
  ExcelFormatCell('<64>')
  ExcelCell(tmp:SwapIMEI,0)
  ExcelFormatCell('<64>')
  If tmp:SwapMSN = ''
      ExcelCell('',0)
  Else !tmp:SwapMSN = ''
      ExcelCell(tmp:SwapMSN,0)
  End !tmp:SwapMSN = ''
  If tmp:DateCode = 0
      ExcelCell('',0)
  Else !tmp:DateCode = 0
      ExcelCell(tmp:DateCode,0)
  End !tmp:DateCode = 0
  ExcelCell(tmp:InFault,0)
  ExcelCell(tmp:InFaultDescription,0)
  ExcelCell(tmp:OutFault,0)
  ExcelCell(tmp:OutFaultDescription,0)
  ExcelCell(job:Repair_Type_Warranty,0)
  ExcelCell(tmp:Comment,0)
  
  Count# = 0
  Save_wpr_ID = Access:WARPARTS.SaveFile()
  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
  wpr:Ref_Number  = job:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.NEXT()
         Break
      End !If
      If wpr:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
      Count# += 1
      ExcelCellWidth(30)
      ExcelCell(wpr:Part_Number,0)
      ExcelCell(wpr:Description,0)
      ExcelCellWidth(12)
      ExcelCell(wpr:Quantity,0)
      ExcelCellWidth(12)
      ExcelFormatCell('##00.00')
      Excelcell(wpr:Quantity * wpr:Purchase_Cost,0)
      
  End !Loop
  Access:WARPARTS.RestoreFile(Save_wpr_ID)
  If Count# > tmp:MostParts
      tmp:MostParts = Count#
  End !Count# > tmp:MostParts
  
  ExcelNewLine(1)
  tmp:RowStart += 1
  
  ReturnValue = PARENT.TakeRecord()
  !After Print
  !If tmp:EDIBatchNumber = 0 and ~man:CreateEDIFile
  !
  !    pos = Position(job:EDI_Key)
  !
  !    job:EDI = 'YES'
  !    job:EDI_Batch_Number    = Man:Batch_Number
  !    AddOracleStatus
  !    AddToAudit
  !
  !    Access:JOBS.Update()
  !
  !    Reset(job:EDI_Key,pos)
  !
  !End !tmp:EDIBatchNumber = 0
  
  tmp:Count += 1
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
ServiceHistoryReport2 PROCEDURE (func:Manufacturer,func:BatchNumber,func:Path,func:SecondYear) !Generated from procedure template - Process

Progress:Thermometer BYTE
tmp:FileName         STRING(255)
tmp:Path             STRING(255)
tmp:Count            LONG
save_wpr_id          USHORT,AUTO
pos                  STRING(255)
save_joo_id          USHORT,AUTO
SheetDesc            CSTRING(41)
tmp:DateIn           STRING(30)
tmp:Franchise        STRING(30)
tmp:InvoiceTotal     REAL
tmp:InvoiceDate      DATE
tmp:DateCode         STRING(30)
tmp:Infault          STRING(30)
tmp:OutFault         STRING(30)
tmp:EDIBatchNumber   LONG
tmp:Manufacturer     STRING(30)
tmp:EDI              STRING(3)
tmp:SwapIMEI         STRING(30)
tmp:SwapMSN          STRING(30)
tmp:ProcessBeforeDate DATE
tmp:Comment          STRING(255)
tmp:SparesUsed       STRING(60),DIM(10)
tmp:Quantity         LONG,DIM(10)
tmp:TotalValue       REAL,DIM(10)
tmp:RowStart         LONG(2)
tmp:InFaultDescription STRING(30)
tmp:OutFaultDescription STRING(30)
tmp:BranchCode       STRING(30)
tmp:MostParts        LONG
tmp:Site_Location    STRING(30)
tmp:Branch_Code      STRING(20)
tmp:Stock_Type       STRING(20)
savepath             STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS)
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ServiceHistoryReport2')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  tmp:Manufacturer    = func:Manufacturer
  tmp:EDIBatchNumber  = func:BatchNumber
  tmp:EDI = 'JJJ'
  tmp:RowStart = 2
  
  glo:Select1 = 0
  
  
    BIND('tmp:Comment',tmp:Comment)
    BIND('tmp:DateCode',tmp:DateCode)
    BIND('tmp:DateIn',tmp:DateIn)
    BIND('tmp:EDI',tmp:EDI)
    BIND('tmp:EDIBatchNumber',tmp:EDIBatchNumber)
    BIND('tmp:Franchise',tmp:Franchise)
    BIND('tmp:Infault',tmp:Infault)
    BIND('tmp:InvoiceDate',tmp:InvoiceDate)
    BIND('tmp:InvoiceTotal',tmp:InvoiceTotal)
    BIND('tmp:Manufacturer',tmp:Manufacturer)
    BIND('tmp:OutFault',tmp:OutFault)
    BIND('tmp:SparesUsed_10',tmp:SparesUsed[10])
    BIND('tmp:SparesUsed_1',tmp:SparesUsed[1])
    BIND('tmp:SparesUsed_2',tmp:SparesUsed[2])
    BIND('tmp:SparesUsed_3',tmp:SparesUsed[3])
    BIND('tmp:SparesUsed_4',tmp:SparesUsed[4])
    BIND('tmp:SparesUsed_5',tmp:SparesUsed[5])
    BIND('tmp:SparesUsed_6',tmp:SparesUsed[6])
    BIND('tmp:SparesUsed_7',tmp:SparesUsed[7])
    BIND('tmp:SparesUsed_8',tmp:SparesUsed[8])
    BIND('tmp:SparesUsed_9',tmp:SparesUsed[9])
    BIND('tmp:SwapIMEI',tmp:SwapIMEI)
    BIND('tmp:SwapMSN',tmp:SwapMSN)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  Access:INVOICE.UseFile
  Access:MANFAULT.UseFile
  Access:JOBOUTFL.UseFile
  Access:TRADEACC.UseFile
  Access:MANUFACT.UseFile
  Access:WARPARTS.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      ExcelSetup(0)
      ExcelMakeWorkBook(func:Manufacturer,'ServiceBase 2000 Cellular','Service History Report')
      !ExcelMakeSheet()
      ExcelSheetName('Service History Report')
      ExcelSelectRange('A1:A1')
      ExcelCellWidth(12)
      ExcelCell('Date Booked',1)
      ExcelCellWidth(14)
      ExcelCell('Date Completed',1)
      ExcelCellWidth(14)
      ExcelCell('Purchase Date',1)
      ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
      ExcelCell('Administrator',1)
      ExcelCellWidth(30)
      ExcelCell('Date Claim Vetted',1)
      ExcelCellWidth(30)
      ExcelCell('Time Claim Vetted',1)
      ! End (DBH 26/06/2007) #9061
      ExcelCellWidth(30)
      ExcelCell('Franchise',1)
      ExcelCellWidth(30)
      ExcelCell('Repair Location',1)
      ExcelCellWidth(12)
      ExcelCell('Job Number',1)
      !An Engineer Option to report - 3788 (DBH: 15-04-2004)
      ExcelCellWidth(15)
      ExcelCell('Engineer Option',1)
      ExcelCellWidth(12)
      ExcelCell('Branch Code',1)
      ExcelCellWidth(30)
      ExcelCell('Customer Name',1)
      ExcelCellWidth(12)
      ExcelCell('Invoice Total',1)
      ExcelCellWidth(12)
      ExcelCell('Labour Total',1)
      ExcelCellWidth(12)
      ExcelCell('Spares Total',1)
      ExcelCellWidth(12)
      ExcelCell('Exchange Cost',1)
      ExcelCellWidth(15)
      ExcelCell('Make',1)
      ExcelCellWidth(15)
      ExcelCell('Model',1)
      ExcelCellWidth(20)
      ExcelCell('I.M.E.I. Number',1)
      ExcelCellWidth(20)
      ExcelCell('Serial Number',1)
      ExcelCellWidth(20)
      ExcelCell('Swap I.M.E.I. Number',1)
      ExcelCellWidth(20)
      ExcelCell('Swap Serial Number',1)
      ExcelCellWidth(10)
      ExcelCell('Date Code',1)
      ExcelCell('In Fault Code',1)
      ExcelCellWidth(30)
      ExcelCell('In Fault Description',1)
      ExcelCellWidth(10)
      ExcelCell('Out Fault Code',1)
      ExcelCellWidth(30)
      ExcelCell('Out Fault Description',1)
      ExcelCellWidth(20)
      ExcelCell('Labour Level',1)
      ExcelCellWidth(30)
      ExcelCell('Comment',1)
  
      !Added By Neil!
      ExcelCellWidth(30)
      ExcelCell('Exchange Site Location',1)
      ExcelCellWidth(30)
      ExcelCell('Exchange Branch Code',1)
      ExcelCellWidth(30)
      ExcelCell('Exchange Stock Type',1)
  
      !Only need 10 parts now
      Loop parts# = 1 To 10
          ExcelCellWidth(30)
          ExcelCell('Part Number',1)
          ExcelCell('Description',1)
          ExcelCellWidth(12)
          ExcelCell('Quantity',1)
          ExcelCellWidth(12)
          ExcelCell('Total Value',1)
      End !Loop parts# = 1 To 10
  
      ExcelSelectRange('A2:A2')
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:EDI)
  ThisProcess.CaseSensitiveValue = FALSE
  ThisProcess.AddSortOrder(job:EDI_Key)
  ThisProcess.AddRange(job:Manufacturer,tmp:Manufacturer)
  ThisProcess.SetFilter('Upper(job:EDI) = ''JJJ'' And Upper(job:EDI_Batch_Number) = Upper(tmp:EDIBatchNumber)')
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = tmp:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Found
      !Pick a path, or set one?
      If func:Path
          savepath = path()
          tmp:FileName = Clip(tmp:Manufacturer)
          If func:SecondYear
              tmp:FileName = Clip(tmp:FileName) & ' 2yr'
          End !If func:SecondYear
          tmp:FileName = Clip(tmp:FileName) & ' ' & Clip(tmp:EDIBatchNumber) & '.xls'
          If not filedialog ('Choose File',tmp:FileName,'Excel Files|*.XLS', |
                      file:keepdir + file:noerror + file:longname)
              !Failed
              setpath(savepath)
              !Put the file in the normal default path anyway
              If Sub(Clip(man:EDI_Path),-1,1) = '\'
                  
              Else !If Sub(man:EDI_Path,-1,1) = '\'
                  man:EDI_Path = Clip(man:EDI_Path) & '\'
              End !If Sub(man:EDI_Path,-1,1) = '\'
  
              tmp:FileName = Clip(man:EDI_Path) & Clip(tmp:Manufacturer)
              If func:SecondYear
                  tmp:FileName = Clip(tmp:FileName) & ' 2Yr'
              End !If func:SecondYear
              tmp:FileName = Clip(tmp:FileName) & ' ' & Clip(tmp:EDIBatchNumber) & '.xls'
          else!If not filedialog
              !Found
              setpath(savepath)
          End!If not filedialog
      Else !func:Path
          If Sub(Clip(man:EDI_Path),-1,1) = '\'
              
          Else !If Sub(man:EDI_Path,-1,1) = '\'
              man:EDI_Path = Clip(man:EDI_Path) & '\'
          End !If Sub(man:EDI_Path,-1,1) = '\'
  
          tmp:FileName = Clip(man:EDI_Path) & Clip(tmp:Manufacturer)
          If func:SecondYear
              tmp:FileName = Clip(tmp:FileName) & ' 2Yr'
          End !If func:SecondYear
          tmp:FileName = Clip(tmp:FileName) & ' ' & Clip(tmp:EDIBatchNumber) & '.xls'
      End !func:Path
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Error
  End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  
  ExcelDeleteSheet('Sheet3')
  ExcelSaveWorkBook(tmp:FileName)
  ExcelClose()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:WEBJOB.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  !Before Print
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      tmp:InvoiceTotal = jobe:ClaimValue + jobe:ClaimPartsCost + job:Courier_Cost_Warranty
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      tmp:InvoiceTotal = 0
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  IF (Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign)
      tmp:InvoiceTotal += jobe2:ThirdPartyHandlingFee  ! #12363 Include Third Party Value (DBH: 17/02/2012)
  END ! IF (Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign)
  
  Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
  inv:Invoice_Number  = job:Invoice_Number_Warranty
  If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      !Found
      tmp:InvoiceDate = inv:Date_Created
  Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      !Error
      tmp:InvoiceDate = ''
  End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
  
  
  tmp:Franchise = ''
  tmp:BranchCode = ''
  tmp:Branch_Code = ''
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = wob:HeadAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          
          tmp:Franchise   = tra:Company_Name
          tmp:BranchCode  = tra:BranchIdentification
          !tmp:Branch_Code = tra:BranchIdentification
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  tmp:SwapIMEI = ''
  tmp:SwapMSN = ''
  tmp:Site_Location = ''
  tmp:Stock_Type = ''
  
  If job:Exchange_unit_Number <> 0
      !RRC exchanges for normal jobs appear on the report
      !48 Hour RRC exchanges do not
      !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
      If jobe:Engineer48HourOption = 1
          If jobe:ExchangedATRRC = True
              If jobe:SecondExchangeNumber <> 0
                  ExchangeNumber# = jobe:SecondExchangeNumber            
              Else !If jobe:SecondExchangeNumber <> 0
                  ExchangeNumber# = 0
              End !If jobe:SecondExchangeNumber <> 0
          End !If jobe:ExchangeATRRC = True
      Else !jobe:Exchange48HourOption = 1
          ExchangeNumber# = job:Exchange_Unit_Number
      End !jobe:Exchange48HourOption = 1
          
      If ExchangeNumber# <> 0
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = ExchangeNumber#
          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Found
              tmp:SwapIMEI    = xch:ESN
              tmp:SwapMSN     = xch:MSN
              tmp:Site_Location = xch:Location
              tmp:Stock_Type = xch:Stock_Type
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      End !If ShowExchange# = True
  End !job:Exchange_unit_Number <> 0
  
  Access:TradeAcc.ClearKey(tra:Account_Number_Key)
  SET(tra:Account_Number_Key,tra:Account_Number_Key)
  LOOP
    IF Access:TradeAcc.Next()
      BREAK
    END
    IF tra:SiteLocation = tmp:Site_Location
      tmp:Branch_code = tra:BranchIdentification
      BREAK
    END
  END
  
  
  tmp:InFault = ''
  tmp:InFaultDescription = ''
  Access:MANFAULT.ClearKey(maf:InFaultKey)
  maf:Manufacturer = job:Manufacturer
  maf:InFault      = 1
  If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Found
      
      Case maf:Field_Number
          Of 1
              tmp:Infault = job:Fault_Code1
          Of 2
              tmp:Infault = job:Fault_Code2
          Of 3
              tmp:Infault = job:Fault_Code3
          Of 4
              tmp:Infault = job:Fault_Code4
          Of 5
              tmp:Infault = job:Fault_Code5
          Of 6
              tmp:Infault = job:Fault_Code6
          OF 7
              tmp:Infault = job:Fault_Code7
          Of 8
              tmp:Infault = job:Fault_Code8
          Of 9
              tmp:Infault = job:Fault_Code9
          Of 10
              tmp:Infault = job:Fault_Code10
          Of 11
              tmp:Infault = job:Fault_Code11
          Of 12
              tmp:Infault = job:Fault_Code12
      End !Case maf:Field_Number
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
      mfo:Field        = tmp:Infault
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          !Found
          tmp:InFaultDescription  = mfo:Description
      Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
  Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
  
  tmp:OutFault = ''
  tmp:OutFaultDescription = ''
  
  Access:MANFAULT.ClearKey(maf:MainFaultKey)
  maf:Manufacturer = job:Manufacturer
  maf:MainFault      = 1
  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
      !Found
      Case maf:Field_Number
          Of 1
              tmp:Outfault = job:Fault_Code1
          Of 2
              tmp:Outfault = job:Fault_Code2
          Of 3
              tmp:Outfault = job:Fault_Code3
          Of 4
              tmp:Outfault = job:Fault_Code4
          Of 5
              tmp:Outfault = job:Fault_Code5
          Of 6
              tmp:Outfault = job:Fault_Code6
          OF 7
              tmp:Outfault = job:Fault_Code7
          Of 8
              tmp:Outfault = job:Fault_Code8
          Of 9
              tmp:Outfault = job:Fault_Code9
          Of 10
              tmp:Outfault = job:Fault_Code10
          Of 11
              tmp:Outfault = job:Fault_Code11
          Of 12
              tmp:Outfault = job:Fault_Code12
      End !Case maf:Field_Number
  
      !If there is no out fault, just get the first one in the OutFault List and use that description
      If tmp:OutFault = ''
          Save_joo_ID = Access:JOBOUTFL.SaveFile()
          Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          joo:FaultCode = '0'
          Set(joo:JobNumberKey,joo:JobNumberKey)
          Loop
              If Access:JOBOUTFL.NEXT()
                 Break
              End !If
              If joo:JobNumber <> job:Ref_Number      |
              Or joo:FaultCode <> '0'      |
                  Then Break.  ! End If
              tmp:OutFaultDescription = joo:Description
              Break
          End !Loop
          Access:JOBOUTFL.RestoreFile(Save_joo_ID)
      Else !If tmp:OutFault = ''
          !There could be duplicate fault codes, so check
          !the out fault list for the selected fault code, and use that description
          !If not, then check the fault code file, and hope you get the right one.
          Access:joboutfl.clearkey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          joo:FaultCode = tmp:outFault
          if access:joboutfl.fetch(joo:jobNumberKey) = Level:Benign
              tmp:OutFaultDescription  = joo:Description
          ELSE
          !for previous jobs try from manfaulo - but this may give errors
              Access:MANFAULO.ClearKey(mfo:Field_Key)
              mfo:Manufacturer = job:Manufacturer
              mfo:Field_Number = maf:Field_Number
              mfo:Field        = tmp:Outfault
              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                  !Found
                  tmp:OutFaultDescription  = mfo:Description
              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          END !If access:joboutfl
      End !If tmp:OutFault = ''
  
  
  Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
  
  tmp:Comment = ''
  Save_joo_ID = Access:JOBOUTFL.SaveFile()
  Access:JOBOUTFL.ClearKey(joo:LevelKey)
  joo:JobNumber = job:Ref_Number
  joo:Level     = 0
  Set(joo:LevelKey,joo:LevelKey)
  Loop
      If Access:JOBOUTFL.NEXT()
         Break
      End !If
      If joo:JobNumber <> job:Ref_Number      |
      Or joo:Level     <> 0      |
          Then Break.  ! End If
      If tmp:Comment <> ''
          tmp:Comment = Clip(tmp:Comment) & ', '
      End !If tmp:Comment <> ''
      tmp:Comment = Clip(tmp:Comment) & joo:Description
  End !Loop
  Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  
  !Work out Date Code
  Case job:Manufacturer
      Of 'SIEMENS'
          tmp:DateCode    = job:Fault_Code3
      Of 'ALCATEL'
          tmp:DateCode    = job:Fault_Code3
      Of 'ERICSSON'
          tmp:DateCode    = ''''&Clip(job:Fault_Code7) & Clip(job:Fault_Code8)
      Else
          tmp:DateCode    = '0'
  End !job:Manufacturer
  
  
  ExcelSelectSheet('Service History Report')
  ExcelSelectRange('A' & tmp:RowStart)
  ExcelCell(Format(job:Date_Booked,@d06),0)
  ExcelCell(Format(job:Date_Completed,@d06),0)
  ExcelCell(Format(job:DOP,@d06),0)
  ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
  Found# = 0
  Access:AUDIT.Clearkey(aud:TypeRefKey)
  aud:Ref_Number = job:Ref_Number
  aud:Type = 'JOB'
  aud:Date = Today()
  Set(aud:TypeRefKey,aud:TypeRefKey)
  Loop ! Begin Loop
      If Access:AUDIT.Next()
          Break
      End ! If Access:AUDIT.Next()
      If aud:Ref_Number <> job:Ref_Number
          Break
      End ! If aud:Ref_Number <> job:Ref_Number
      If aud:Type <> 'JOB'
          Break
      End ! If aud:Type <> 'JOB'
      If aud:Date > Today()
          Break
      End ! If aud:Date > Today()
  ! Changing (DBH 03/08/2007) # 9246 - Also check for "Warranty Claim Rejected" entry
  !    If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
  ! to (DBH 03/08/2007) # 9246
      If Instring('CLAIM VETTED',Upper(aud:Action),1,1) Or Upper(aud:Action) = 'WARRANTY CLAIM REJECTED'
  ! End (DBH 03/08/2007) #9246
          Access:USERS.ClearKey(use:User_Code_Key)
          use:User_Code = aud:User
          If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Found
              Found# = 1
              ExcelCell(Clip(use:Forename) & ' ' & Clip(use:Surname),0)
              ExcelCell(Format(aud:Date,@d06b),0)
              ExcelCell(Format(aud:Time,@t01b),0)
              Break
          Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Error
  
          End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      End ! If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
  End ! Loop
  If Found# = 0
      ExcelCell('',0)
      ExcelCell('',0)
      ExcelCEll('',0)
  End ! If Found# = 0
  ! End (DBH 26/06/2007) #9061
  ExcelCell(tmp:Franchise,0)
  if jobe:WebJob AND SentToHub(job:ref_number) = FALSE
     ExcelCell('RRC',0)
  ELSE
     ExcelCell('ARC',0)
  END
  ExcelFormatCell('########')
  ExcelCell(job:Ref_Number,0)
  ExcelFormatCell('<64>')
  !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
  Case jobe:Engineer48HourOption
      Of 1
          ExcelCell('48H',0)
      Of 2
          ExcelCell('ARC',0)
      Of 3
          ExcelCell('7DT',0)
      Of 4
          ExcelCell('STD',0)
      Else
          ExcelCell('',0)
  End !Case jobe:Engineer48HourOption
  ExcelCell(tmp:BranchCode,0)
  ExcelCell(job:Company_Name,0)
  ExcelFormatCell('##00.00')
  ExcelCell(tmp:InvoiceTotal,0)
  ExcelFormatCell('##00.00')
  ExcelCell(jobe:ClaimValue,0)
  ExcelFormatCell('##00.00')
  ExcelCell(jobe:ClaimPartsCost,0)
  ExcelFormatCell('##00.00')
  ExcelCell(job:Courier_Cost_Warranty,0)
  ExcelCell(job:Manufacturer,0)
  ExcelCell(job:Model_Number,0)
  ExcelFormatCell('<64>')
  ExcelCell(job:ESN,0)
  ExcelFormatCell('<64>')
  If job:MSN = 'N/A'
      ExcelCell('',0)
  Else !job:MSN = 'N/A'
      ExcelCell(job:MSN,0)
  End !job:MSN = 'N/A'
  ExcelFormatCell('<64>')
  ExcelCell(tmp:SwapIMEI,0)
  ExcelFormatCell('<64>')
  If tmp:SwapMSN = ''
      ExcelCell('',0)
  Else !tmp:SwapMSN = ''
      ExcelCell(tmp:SwapMSN,0)
  End !tmp:SwapMSN = ''
  If tmp:DateCode = '0'
      ExcelCell('',0)
  Else !tmp:DateCode = 0
      ExcelCell(tmp:DateCode,0)
  End !tmp:DateCode = 0
  ExcelCell(tmp:InFault,0)
  ExcelCell(tmp:InFaultDescription,0)
  ExcelCell(tmp:OutFault,0)
  ExcelCell(tmp:OutFaultDescription,0)
  ExcelCell(job:Repair_Type_Warranty,0)
  ExcelCell(tmp:Comment,0)
  ExcelCell(tmp:Site_Location,0)
  ExcelCell(tmp:Branch_Code,0)
  ExcelCell(tmp:Stock_Type,0)
  Count# = 0
  Save_wpr_ID = Access:WARPARTS.SaveFile()
  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
  wpr:Ref_Number  = job:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.NEXT()
         Break
      End !If
      If wpr:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
      Count# += 1
      !Only allow 10 parts
      If Count# > 10
          Break
      End !If Count# > 10
  
      ExcelCellWidth(30)
      ExcelCell(wpr:Part_Number,0)
      ExcelCell(wpr:Description,0)
      ExcelCellWidth(12)
      ExcelCell(wpr:Quantity,0)
      ExcelCellWidth(12)
      ExcelFormatCell('##00.00')
      Excelcell(wpr:Quantity * wpr:Purchase_Cost,0)
      
  End !Loop
  Access:WARPARTS.RestoreFile(Save_wpr_ID)
  
  ExcelNewLine(1)
  tmp:RowStart += 1
  
  ReturnValue = PARENT.TakeRecord()
  !After Print
  tmp:Count += 1
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
DateRange PROCEDURE (func:StartDate,func:EndDate)     !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:Cancel           BYTE(0)
window               WINDOW('Out Of Warranty Date Range'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Out Of Warranty Date Range'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Date Range'),USE(?Tab1)
                           PROMPT('Include all Out Of Warranty Jobs between selected completed date range'),AT(238,170,204,20),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(238,199),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(314,199,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(382,194),USE(?LookupStartDate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(238,223),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(314,223,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(382,218),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Cancel)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020384'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DateRange')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:StartDate = Deformat('1/1/2000',@d6)
  tmp:endDate   = Today()
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020384'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020384'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020384'&'0')
      ***
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      func:StartDate  = tmp:StartDate
      func:EndDate    = tmp:EndDate
      tmp:Cancel      = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:cancel = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ServiceHistoryReport PROCEDURE (func:Manufacturer,func:BatchNumber,func:Path,func:EDI,func:Date,func:SecondYear) !Generated from procedure template - Window

tmp:FileName         STRING(255)
tmp:Path             STRING(255)
tmp:Count            LONG
save_wpr_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_joo_id          USHORT,AUTO
pos                  STRING(255)
SheetDesc            CSTRING(41)
tmp:DateIn           STRING(30)
tmp:Franchise        STRING(30)
tmp:InvoiceTotal     REAL
tmp:InvoiceDate      DATE
tmp:DateCode         STRING(30)
tmp:Infault          STRING(30)
tmp:OutFault         STRING(30)
tmp:EDIBatchNumber   LONG
tmp:Manufacturer     STRING(30)
tmp:EDI              STRING(3)
tmp:SwapIMEI         STRING(30)
tmp:SwapMSN          STRING(30)
tmp:ProcessBeforeDate DATE
tmp:Comment          STRING(255)
tmp:SparesUsed       STRING(60),DIM(10)
tmp:Quantity         LONG,DIM(10)
tmp:TotalValue       REAL,DIM(10)
tmp:RowStart         LONG(2)
tmp:InFaultDescription STRING(30)
tmp:OutFaultDescription STRING(30)
tmp:BranchCode       STRING(30)
tmp:MostParts        LONG
tmp:Site_Location    STRING(30)
tmp:Branch_Code      STRING(20)
tmp:Stock_Type       STRING(20)
savepath             STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
RejectRecord         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE(0)
RecordStatus         BYTE(0)
Progress:Thermometer BYTE(0)
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('Service History Report'),AT(,,155,38),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       PROMPT(''),AT(4,4,148,10),USE(?ProgressPrompt),TRN,CENTER
                       PROGRESS,USE(Progress:Thermometer),AT(4,16,148,10),RANGE(0,100)
                       PROMPT(''),AT(19,26,116,12),USE(?Progress:PctText),CENTER
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
GetNextRecord2      Routine
  RecordsProcessed += 1
  RecordsThisCycle += 1
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
    IF PercentProgress <> Progress:Thermometer THEN
      Progress:Thermometer = PercentProgress
      ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & '% Completed'
      DISPLAY()
    END
  END
EndPrintRun         Routine
    Progress:Thermometer = 100
    ?Progress:PctText{Prop:Text} = '100% Completed'
    DISPLAY()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ServiceHistoryReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ProgressPrompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:INVOICE.UseFile
  Access:MANFAULT.UseFile
  Access:JOBOUTFL.UseFile
  Access:TRADEACC.UseFile
  Access:WARPARTS.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
          ?ProgressPrompt{prop:Text} = Clip(func:Manufacturer) & ': Creating Report'
          tmp:Manufacturer    = func:Manufacturer
          tmp:EDIBatchNumber  = func:BatchNumber
          tmp:EDI = func:EDI
          tmp:RowStart = 2
          tmp:ProcessBeforeDate   = func:Date
      
          glo:Select1 = 0
      
          ExcelSetup(0)
          ExcelMakeWorkBook(func:Manufacturer,'ServiceBase 2000 Cellular','Service History Report')
          !ExcelMakeSheet()
          ExcelSheetName('Service History Report')
          ExcelSelectRange('A1:A1')
          ExcelCellWidth(12)
          ExcelCell('Date Booked',1)
          ExcelCellWidth(14)
          ExcelCell('Date Completed',1)
          ExcelCellWidth(14)
          ExcelCell('Purchase Date',1)
          ExcelCellWidth(30)
          ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
          ExcelCell('Administrator',1)
          ExcelCellWidth(30)
          ExcelCell('Date Claim Vetted',1)
          ExcelCellWidth(30)
          ExcelCell('Time Claim Vetted',1)
          ! End (DBH 26/06/2007) #9061
          ExcelCellWidth(30)
          ExcelCell('Franchise',1)
          ExcelCellWidth(30)
          ExcelCell('Repair Location',1)
          ExcelCellWidth(12)
          ExcelCell('Job Number',1)
          !An Engineer Option to report - 3788 (DBH: 15-04-2004)
          ExcelCellWidth(15)
          ExcelCell('Engineer Option',1)
          ExcelCellWidth(12)
          ExcelCell('Branch Code',1)
          ExcelCellWidth(30)
          ExcelCell('Customer Name',1)
          ExcelCellWidth(12)
          ExcelCell('Invoice Total',1)
          ExcelCellWidth(12)
          ExcelCell('Labour Total',1)
          ExcelCellWidth(12)
          ExcelCell('Spares Total',1)
          ExcelCellWidth(12)
          ExcelCell('Exchange Cost',1)
          ExcelCellWidth(15)
          ExcelCell('Make',1)
          ExcelCellWidth(15)
          ExcelCell('Model',1)
          ExcelCellWidth(20)
          ExcelCell('I.M.E.I. Number',1)
          ExcelCellWidth(20)
          ExcelCell('Serial Number',1)
          ExcelCellWidth(20)
          ExcelCell('Swap I.M.E.I. Number',1)
          ExcelCellWidth(20)
          ExcelCell('Swap Serial Number',1)
          ExcelCellWidth(10)
          ExcelCell('Date Code',1)
          ExcelCell('In Fault Code',1)
          ExcelCellWidth(30)
          ExcelCell('In Fault Description',1)
          ExcelCellWidth(10)
          ExcelCell('Out Fault Code',1)
          ExcelCellWidth(30)
          ExcelCell('Out Fault Description',1)
          ExcelCellWidth(20)
          ExcelCell('Labour Level',1)
          ExcelCellWidth(30)
          ExcelCell('Comment',1)
      
          !Added By Neil!
          ExcelCellWidth(30)
          ExcelCell('Exchange Site Location',1)
          ExcelCellWidth(30)
          ExcelCell('Exchange Branch Code',1)
          ExcelCellWidth(30)
          ExcelCell('Exchange Stock Type',1)
      
          !Only need 10 parts now
          Loop parts# = 1 To 10
              ExcelCellWidth(30)
              ExcelCell('Part Number',1)
              ExcelCell('Description',1)
              ExcelCellWidth(12)
              ExcelCell('Quantity',1)
              ExcelCellWidth(12)
              ExcelCell('Total Value',1)
          End !Loop parts# = 1 To 10
      
          ExcelSelectRange('A2:A2')
          RecordsPerCycle     = 25
          RecordsProcessed    = 0
          PercentProgress     = 0
          Progress:Thermometer    = 0
          ?Progress:PctText{prop:text} = '0% Completed'
      
          RecordsToProcess    = 500
      
      
          Save_job_ID = Access:JOBS.SaveFile()
          Access:JOBS.ClearKey(job:EDI_Key)
          job:Manufacturer     = func:Manufacturer
          job:EDI              = func:EDI
          job:EDI_Batch_Number = func:BatchNumber
          Set(job:EDI_Key,job:EDI_Key)
          Loop
              If Access:JOBS.NEXT()
                 Break
              End !If
              If job:Manufacturer     <> func:Manufacturer      |
              Or job:EDI              <> func:EDI      |
              Or job:EDI_Batch_Number <> func:BatchNumber      |
                  Then Break.  ! End If
      
              !Before Print
              Do GetNextRecord2
      
              If tmp:ProcessBeforeDate <> 0
                  If job:Date_Completed > tmp:ProcessBeforeDate
                      Cycle
                  End !If job:Date_Completed > tmp:ProcessBeforeDate
              End !tmp:ProcessBeforeDate <> 0
      
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Found
                  tmp:InvoiceTotal = jobe:ClaimValue + jobe:ClaimPartsCost + job:Courier_Cost_Warranty
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Error
                  tmp:InvoiceTotal = 0
              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
      
              Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
              jobe2:RefNumber = job:Ref_Number
              IF (Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign)
                  tmp:InvoiceTotal += jobe2:ThirdPartyHandlingFee  ! #12363 Include Third Party Value (DBH: 17/02/2012)
              END ! IF (Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign)
      
              Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
              inv:Invoice_Number  = job:Invoice_Number_Warranty
              If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                  !Found
                  tmp:InvoiceDate = inv:Date_Created
              Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                  !Error
                  tmp:InvoiceDate = ''
              End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      
      
              tmp:Franchise = ''
              tmp:BranchCode = ''
              tmp:Branch_Code = ''
              Access:WEBJOB.ClearKey(wob:RefNumberKey)
              wob:RefNumber = job:Ref_Number
              If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                  !Found
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number  = wob:HeadAccountNumber
                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      
                      tmp:Franchise   = tra:Company_Name
                      tmp:BranchCode  = tra:BranchIdentification
                      !tmp:Branch_Code = tra:BranchIdentification
                  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
              tmp:SwapIMEI = ''
              tmp:SwapMSN = ''
              tmp:Site_Location = ''
              tmp:Stock_Type = ''
      
              If job:Exchange_unit_Number <> 0
                  !RRC exchanges for normal jobs appear on the report
                  !48 Hour RRC exchanges do not
                  !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
                  If jobe:Engineer48HourOption = 1
                      If jobe:ExchangedATRRC = True
                          If jobe:SecondExchangeNumber <> 0
                              ExchangeNumber# = jobe:SecondExchangeNumber            
                          Else !If jobe:SecondExchangeNumber <> 0
                              ExchangeNumber# = 0
                          End !If jobe:SecondExchangeNumber <> 0
                      End !If jobe:ExchangeATRRC = True
                  Else !jobe:Exchange48HourOption = 1
                      ExchangeNumber# = job:Exchange_Unit_Number
                  End !jobe:Exchange48HourOption = 1
                      
                  If ExchangeNumber# <> 0
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number  = ExchangeNumber#
                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Found
                          tmp:SwapIMEI    = xch:ESN
                          tmp:SwapMSN     = xch:MSN
                          tmp:Site_Location = xch:Location
                          tmp:Stock_Type = xch:Stock_Type
                      Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Error
                      End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                  End !If ShowExchange# = True
              End !job:Exchange_unit_Number <> 0
      
              Access:TradeAcc.ClearKey(tra:Account_Number_Key)
              SET(tra:Account_Number_Key,tra:Account_Number_Key)
              LOOP
                IF Access:TradeAcc.Next()
                  BREAK
                END
                IF tra:SiteLocation = tmp:Site_Location
                  tmp:Branch_code = tra:BranchIdentification
                  BREAK
                END
              END
      
      
              tmp:InFault = ''
              tmp:InFaultDescription = ''
              Access:MANFAULT.ClearKey(maf:InFaultKey)
              maf:Manufacturer = job:Manufacturer
              maf:InFault      = 1
              If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
                  !Found
                  
                  Case maf:Field_Number
                      Of 1
                          tmp:Infault = job:Fault_Code1
                      Of 2
                          tmp:Infault = job:Fault_Code2
                      Of 3
                          tmp:Infault = job:Fault_Code3
                      Of 4
                          tmp:Infault = job:Fault_Code4
                      Of 5
                          tmp:Infault = job:Fault_Code5
                      Of 6
                          tmp:Infault = job:Fault_Code6
                      OF 7
                          tmp:Infault = job:Fault_Code7
                      Of 8
                          tmp:Infault = job:Fault_Code8
                      Of 9
                          tmp:Infault = job:Fault_Code9
                      Of 10
                          tmp:Infault = job:Fault_Code10
                      Of 11
                          tmp:Infault = job:Fault_Code11
                      Of 12
                          tmp:Infault = job:Fault_Code12
                  End !Case maf:Field_Number
                  Access:MANFAULO.ClearKey(mfo:Field_Key)
                  mfo:Manufacturer = job:Manufacturer
                  mfo:Field_Number = maf:Field_Number
                  mfo:Field        = tmp:Infault
                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                      !Found
                      tmp:InFaultDescription  = mfo:Description
                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
      
              Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      
              tmp:OutFault = ''
              tmp:OutFaultDescription = ''
      
              Access:MANFAULT.ClearKey(maf:MainFaultKey)
              maf:Manufacturer = job:Manufacturer
              maf:MainFault      = 1
              If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                  !Found
                  Case maf:Field_Number
                      Of 1
                          tmp:Outfault = job:Fault_Code1
                      Of 2
                          tmp:Outfault = job:Fault_Code2
                      Of 3
                          tmp:Outfault = job:Fault_Code3
                      Of 4
                          tmp:Outfault = job:Fault_Code4
                      Of 5
                          tmp:Outfault = job:Fault_Code5
                      Of 6
                          tmp:Outfault = job:Fault_Code6
                      OF 7
                          tmp:Outfault = job:Fault_Code7
                      Of 8
                          tmp:Outfault = job:Fault_Code8
                      Of 9
                          tmp:Outfault = job:Fault_Code9
                      Of 10
                          tmp:Outfault = job:Fault_Code10
                      Of 11
                          tmp:Outfault = job:Fault_Code11
                      Of 12
                          tmp:Outfault = job:Fault_Code12
                  End !Case maf:Field_Number
      
                  !If there is no out fault, just get the first one in the OutFault List and use that description
                  If tmp:OutFault = ''
                      Save_joo_ID = Access:JOBOUTFL.SaveFile()
                      Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
                      joo:JobNumber = job:Ref_Number
                      joo:FaultCode = '0'
                      Set(joo:JobNumberKey,joo:JobNumberKey)
                      Loop
                          If Access:JOBOUTFL.NEXT()
                             Break
                          End !If
                          If joo:JobNumber <> job:Ref_Number      |
                          Or joo:FaultCode <> '0'      |
                              Then Break.  ! End If
                          tmp:OutFaultDescription = joo:Description
                          Break
                      End !Loop
                      Access:JOBOUTFL.RestoreFile(Save_joo_ID)
                  Else !If tmp:OutFault = ''
                      !There could be duplicate fault codes, so check
                      !the out fault list for the selected fault code, and use that description
                      !If not, then check the fault code file, and hope you get the right one.
                      Access:joboutfl.clearkey(joo:JobNumberKey)
                      joo:JobNumber = job:Ref_Number
                      joo:FaultCode = tmp:outFault
                      if access:joboutfl.fetch(joo:jobNumberKey) = Level:Benign
                          tmp:OutFaultDescription  = joo:Description
                      ELSE
                      !for previous jobs try from manfaulo - but this may give errors
                          Access:MANFAULO.ClearKey(mfo:Field_Key)
                          mfo:Manufacturer = job:Manufacturer
                          mfo:Field_Number = maf:Field_Number
                          mfo:Field        = tmp:Outfault
                          If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                              !Found
                              tmp:OutFaultDescription  = mfo:Description
                          Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                      END !If access:joboutfl
                  End !If tmp:OutFault = ''
      
              Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
      
              tmp:Comment = ''
              Save_joo_ID = Access:JOBOUTFL.SaveFile()
              Access:JOBOUTFL.ClearKey(joo:LevelKey)
              joo:JobNumber = job:Ref_Number
              joo:Level     = 0
              Set(joo:LevelKey,joo:LevelKey)
              Loop
                  If Access:JOBOUTFL.NEXT()
                     Break
                  End !If
                  If joo:JobNumber <> job:Ref_Number      |
                  Or joo:Level     <> 0      |
                      Then Break.  ! End If
                  If tmp:Comment <> ''
                      tmp:Comment = Clip(tmp:Comment) & ', '
                  End !If tmp:Comment <> ''
                  tmp:Comment = Clip(tmp:Comment) & joo:Description
              End !Loop
              Access:JOBOUTFL.RestoreFile(Save_joo_ID)
      
              !Work out Date Code
              Case job:Manufacturer
                  Of 'SIEMENS'
                      tmp:DateCode    = job:Fault_Code3
                  Of 'ALCATEL'
                      tmp:DateCode    = job:Fault_Code3
                  Of 'ERICSSON'
                      tmp:DateCode    = ''''&Clip(job:Fault_Code7) & Clip(job:Fault_Code8)
                  Else
                      tmp:DateCode    = '0'
              End !job:Manufacturer
      
              ExcelSelectSheet('Service History Report')
              ExcelSelectRange('A' & tmp:RowStart)
              ExcelCell(Format(job:Date_Booked,@d06b),0)
              ExcelCell(Format(job:Date_Completed,@d06b),0)
              ExcelCell(Format(job:DOP,@d06b),0)
      
              ! Inserting (DBH 26/06/2007) # 9061 - Add vetting details from audit trail
              Found# = 0
              Access:AUDIT.Clearkey(aud:TypeRefKey)
              aud:Ref_Number = job:Ref_Number
              aud:Type = 'JOB'
              aud:Date = Today()
              Set(aud:TypeRefKey,aud:TypeRefKey)
              Loop ! Begin Loop
                  If Access:AUDIT.Next()
                      Break
                  End ! If Access:AUDIT.Next()
                  If aud:Ref_Number <> job:Ref_Number
                      Break
                  End ! If aud:Ref_Number <> job:Ref_Number
                  If aud:Type <> 'JOB'
                      Break
                  End ! If aud:Type <> 'JOB'
                  If aud:Date > Today()
                      Break
                  End ! If aud:Date > Today()
      ! Changing (DBH 03/08/2007) # 9246 - Also check for "Warranty Claim Rejected" entry
      !           If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
      ! to (DBH 03/08/2007) # 9246
                  If Instring('CLAIM VETTED',Upper(aud:Action),1,1) Or Upper(aud:Action) = 'WARRANTY CLAIM REJECTED'
      ! End (DBH 03/08/2007) #9246
                      Access:USERS.ClearKey(use:User_Code_Key)
                      use:User_Code = aud:User
                      If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                          !Found
                          Found# = 1
                          ExcelCell(Clip(use:Forename) & ' ' & Clip(use:Surname),0)
                          ExcelCell(Format(aud:Date,@d06b),0)
                          ExcelCell(Format(aud:Time,@t01b),0)
                          Break
                      Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                          !Error
                          
                      End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                  End ! If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
              End ! Loop
              If Found# = 0
                  ExcelCell('',0)
                  ExcelCell('',0)
                  ExcelCEll('',0)
              End ! If Found# = 0
              ! End (DBH 26/06/2007) #9061
      
              ExcelCell(tmp:Franchise,0)
              if jobe:WebJob AND SentToHub(job:ref_number) = FALSE
                 ExcelCell('RRC',0)
              ELSE
                 ExcelCell('ARC',0)
              END
              ExcelFormatCell('########')
              ExcelCell(job:Ref_Number,0)
              ExcelFormatCell('<64>')
              !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
              Case jobe:Engineer48HourOption
                  Of 1
                      ExcelCell('48H',0)
                  Of 2
                      ExcelCell('ARC',0)
                  Of 3
                      ExcelCell('7DT',0)
                  Of 4
                      ExcelCell('STD',0)
                  Else
                      ExcelCell('',0)
              End !Case jobe:Engineer48HourOption
              ExcelCell(tmp:BranchCode,0)
              ExcelCell(job:Company_Name,0)
              ExcelFormatCell('##00.00')
              ExcelCell(tmp:InvoiceTotal,0)
              ExcelFormatCell('##00.00')
              ExcelCell(jobe:ClaimValue,0)
              ExcelFormatCell('##00.00')
              ExcelCell(jobe:ClaimPartsCost,0)
              ExcelFormatCell('##00.00')
              ExcelCell(job:Courier_Cost_Warranty,0)
              ExcelCell(job:Manufacturer,0)
              ExcelCell(job:Model_Number,0)
              ExcelFormatCell('<64>')
              ExcelCell(job:ESN,0)
              ExcelFormatCell('<64>')
              If job:MSN = 'N/A'
                  ExcelCell('',0)
              Else !job:MSN = 'N/A'
                  ExcelCell(job:MSN,0)
              End !job:MSN = 'N/A'
              ExcelFormatCell('<64>')
              ExcelCell(tmp:SwapIMEI,0)
      
              ExcelFormatCell('<64>')
      
              If tmp:SwapMSN = ''
                  ExcelCell('',0)
              Else !tmp:SwapMSN = ''
                  ExcelCell(tmp:SwapMSN,0)
              End !tmp:SwapMSN = ''
      
              If tmp:DateCode = '0'
                  ExcelCell('',0)
              Else !tmp:DateCode = 0
                  ExcelCell(tmp:DateCode,0)
              End !tmp:DateCode = 0
      
              ExcelCell(tmp:InFault,0)
              ExcelCell(tmp:InFaultDescription,0)
              ExcelCell(tmp:OutFault,0)
              ExcelCell(tmp:OutFaultDescription,0)
              ExcelCell(job:Repair_Type_Warranty,0)
              ExcelCell(tmp:Comment,0)
              ExcelCell(tmp:Site_Location,0)
              ExcelCell(tmp:Branch_Code,0)
              ExcelCell(tmp:Stock_Type,0)
      
      
              Count# = 0
              Save_wpr_ID = Access:WARPARTS.SaveFile()
              Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
              wpr:Ref_Number  = job:Ref_Number
              Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
              Loop
                  If Access:WARPARTS.NEXT()
                     Break
                  End !If
                  If wpr:Ref_Number  <> job:Ref_Number      |
                      Then Break.  ! End If
                  Count# += 1
                  !Only allow 10 parts
                  If Count# > 10
                      Break
                  End !If Count# > 10
      
                  ExcelCellWidth(30)
                  ExcelCell(wpr:Part_Number,0)
                  ExcelCell(wpr:Description,0)
                  ExcelCellWidth(12)
                  ExcelCell(wpr:Quantity,0)
                  ExcelCellWidth(12)
                  ExcelFormatCell('##00.00')
                  Excelcell(wpr:Quantity * wpr:Purchase_Cost,0)
                  
              End !Loop
              Access:WARPARTS.RestoreFile(Save_wpr_ID)
      
              ExcelNewLine(1)
              tmp:RowStart += 1
              !After Print
              tmp:Count += 1
      
          End !Loop
          Access:JOBS.RestoreFile(Save_job_ID)
      
          Do EndPrintRun
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = func:Manufacturer
          If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
              !Found
              !Pick a path, or set one?
              If func:Path
                  savepath = path()
                  If tmp:EDIBatchNumber = 0
                      !Report is being run before claim is made
                      tmp:FileName = Clip(func:Manufacturer) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(Today()),@n04) & Format(Clock(),@t2)
                      If func:SecondYear
                          tmp:FileName    = Clip(tmp:FileName) & ' 2yr'
                      End !If func:SecondYear
                  Else !If tmp:ProcessBeforeDate <> 0
                      tmp:FileName = Clip(func:Manufacturer)
                      If func:SecondYear
                          tmp:FileName    = Clip(tmp:FileName) & ' 2yr'
                      End !If func:SecondYear
                      tmp:FileName = Clip(tmp:FileName) & ' ' & Clip(tmp:EDIBatchNumber)
                  End !If tmp:ProcessBeforeDate <> 0
      
                  tmp:FileName    = Clip(tmp:FileName) & '.xls'
                  
                  If not filedialog ('Choose File',tmp:FileName,'Excel Files|*.XLS', |
                              file:keepdir + file:noerror + file:longname)
                      !Failed
                      setpath(savepath)
                      !Put the file in the normal default path anyway
                      If Sub(Clip(man:EDI_Path),-1,1) = '\'
                          
                      Else !If Sub(man:EDI_Path,-1,1) = '\'
                          man:EDI_Path = Clip(man:EDI_Path) & '\'
                      End !If Sub(man:EDI_Path,-1,1) = '\'
      
                      tmp:FileName = Clip(man:EDI_Path) & Clip(func:Manufacturer)
                      If func:SecondYear
                          tmp:FileName = Clip(tmp:FileName) & ' 2Yr'
                      End !If func:SecondYear
                      tmp:FileName = Clip(tmp:FileName) & ' ' & Clip(tmp:EDIBatchNumber) & '.xls'
                  else!If not filedialog
                      !Found
                      setpath(savepath)
                  End!If not filedialog
              Else !func:Path
                  If Sub(Clip(man:EDI_Path),-1,1) = '\'
                      
                  Else !If Sub(man:EDI_Path,-1,1) = '\'
                      man:EDI_Path = Clip(man:EDI_Path) & '\'
                  End !If Sub(man:EDI_Path,-1,1) = '\'
      
                  tmp:FileName = Clip(man:EDI_Path) & Clip(func:Manufacturer)
                  If func:SecondYear
                      tmp:FileName = Clip(tmp:FileName) & ' 2Yr'
                  End !If func:SecondYear
                  tmp:FileName = Clip(tmp:FileName) & ' ' & Clip(tmp:EDIBatchNumber) & '.xls'
              End !func:Path
          Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
              !Error
          End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
      
          ExcelDeleteSheet('Sheet3')
          ExcelSaveWorkBook(tmp:FileName)
          ExcelClose()
      
          Post(Event:CloseWindow)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
GenericEDI PROCEDURE (f:Manufacturer,f:StartDate,f:EndDate,f:Silent,f:SHist,f:Type,f:FileList) !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
save_jobswarr_id     USHORT,AUTO
DateCompleted        DATE
TimeCompleted        TIME
tmp:Return           BYTE(0)
tmp:RecordsCount     LONG
tmp:ExportFile       STRING(255),STATIC
tmp:DebugFile        STRING(255),STATIC
tmp:DesktopFolder    CSTRING(255)
tmp:FirstCount       LONG
tmp:SecondCount      LONG
tmp:InboundSerial    STRING(20)
tmp:InboundIMEI      STRING(20)
tmp:OutboundSerial   STRING(20)
tmp:OutboundIMEI     STRING(20)
tmp:Engineerdate     DATE
tmp:EngineerTime     TIME
tmp:CustomerName     STRING(50)
tmp:CurrentRow       LONG
HTCQueue1            QUEUE,PRE(HTCQ1)
PartNumber           STRING(30)
PFC1                 STRING(30)
PFC2                 STRING(30)
                     END
HTCQueue2            QUEUE,PRE(HTCQ2)
PFC1                 STRING(30)
PFC2                 STRING(30)
Used                 LONG
Full                 STRING(1)
WarPart              STRING(30),DIM(7)
                     END
tmp:EDIASVCode       STRING(30)
FileNameQueue        QUEUE,PRE(FNQ)
ASVCode              STRING(100)
TempAccType          BYTE
Filename             STRING(255)
                     END
tmp:asvcode          STRING(100)
tmp:Charger          STRING(3)
tmp:Battery          STRING(3)
tmp:Other            STRING(3)
tmp:ReceiveDate      DATE
tmp:FinishDate       DATE
tmp:PickupDate       DATE
tmp:InFault          STRING(30)
tmp:InFaultDescription STRING(30)
tmp:OutFault         STRING(30)
tmp:OutFaultDescription STRING(30)
Tmp:Wpart            STRING(30),DIM(3)
Tmp:Wqty             LONG,DIM(3)
tmp:ExchangeIMEI     STRING(30)
tmp:ExchangeMSN      STRING(30)
Try:Date             DATE
try:Time             TIME
local       CLASS
AlcatelExport           Procedure(Byte f:FirstSecondYear)
BlackberryExport        Procedure()
BoschExport             Procedure(Byte f:FirstSecondYear)
EricssonExport          Procedure(Byte f:FirstSecondYear)
HTCExport               Procedure(Byte f:FirstSecondYear)
HUAWEIExport            Procedure(Byte f:FirstSecondYear)
LGExport2               Procedure()
MaxonExport             Procedure(Byte f:FirstSecondYear)
MitsubishiExport        Procedure(Byte f:FirstSecondYear)
MotorolaExport          Procedure(Byte f:FirstSecondYear)
NECExport               Procedure(Byte f:FirstSecondYear)
NokiaExport             Procedure(Byte f:FirstSecondYear)
PanasonicExport         Procedure(Byte f:FirstSecondYear)
PhilipsExport           Procedure(Byte f:FirstSecondYear)
SagemExport             Procedure()
SamsungSAExport         Procedure()
SiemensExport           Procedure(Byte f:FirstSecondYear)
SonyExport              Procedure(Byte f:FirstSecondYear)
TelitalExport           Procedure(Byte f:FirstSecondYear)
ZTEExport               Procedure(Byte f:FirstSecondYear)
ServiceHistoryReport    Procedure(Long f:Records,Byte f:FirstSecondYear)
            End ! local       CLASS
tmp:CompStartDate        Date()
tmp:CompEndDate          Date()

    MAP
ExportLine          Procedure(STRING fString,STRING fSep,<String fType>,<Byte fForceEnd>,<Long fRestrict>)
GetBookingDate      PROCEDURE(LONG fJobNumber,*DATE fDateBooked,*TIME fTimeBooked,LONG fExchange,BYTE fRRCExchange)
GetAuditDateTime        PROCEDURE(LONG fJobNumber,STRING fAction,*DATE fDate,*TIME fTIME,<STRING fType>)
    END

ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record              Record
Line                 String(2000)
                    End
                End



!DebugFile    File,Driver('ASCII'),Pre(debug),Name(tmp:DebugFile),Create,Bindable,Thread
!Record              Record
!Line                 String(2000)
!                    End
!                End
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto

Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
Prog:ProgressWindow  WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
                       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
                       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,HIDE,CENTER,FONT('Tahoma',8,,)
                       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico')
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:RecordsProcessed   = 0
    Prog:PercentProgress    = 0
    Prog:Thermometer        = 0
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    !Open(Prog:ProgressWindow)

    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    !Close(Prog:ProgressWindow)
    Display()

!routines for Blackberry - BBColumnHeadings, BBFinishSheet, BBwriteLine TB13053 JC 25/05/13
!moved here because of space requirements - they are long!
BBColumnHeadings    Routine

    !defaults
    e1.SetColumnWidth ('A', 'EZ', '2.86')

    !Default headers - top line
    e1.writeToCell('B2X WSC Lite - Request Template v2.0 - BlackBerry Standard','B1')

    !Centre the text in the next two header lines - they will be grouped
    e1.SetCellAlignment(oix:VerticalAlignment, oix:xlCenter, 'A2', 'EZ3')
    e1.SetCellAlignment(oix:HorizontalAlignment, oix:xlCenter, 'A2', 'EZ3')

    !first Row of headings
    e1.writeToCell('Ref.','A2')
    e1.MergeCells ('A2', 'B3')
    e1.writeToCell('Type of Repair','C2')
    e1.MergeCells ('C2', 'E3')
    e1.writeToCell('Customer','F2')
    e1.MergeCells ('F2', 'T3')
    e1.writeToCell('Product','U2')
    e1.MergeCells ('U2', 'W3')          !TB13130 = JC was Y3 not W3
    e1.writeToCell('Customer Issues - Issue Resolutions','X2')
    e1.MergeCells ('X2', 'AM2')
    e1.writeToCell('Trouble','AN2')
    e1.MergeCells ('AN2', 'AP3')
    e1.writeToCell('Level','AQ2')
    e1.MergeCells ('AQ2', 'AR3')
    e1.writeToCell('Customer Interaction','AS2')
    e1.MergeCells ('AS2', 'AX3')
    e1.writeToCell('Symptoms','AY2')
    e1.MergeCells ('AY2', 'BR2')
    e1.writeToCell('Actions','BS2')
    e1.MergeCells ('BS2','EX2')
    e1.writeToCell('Swap Device','EY2')
    e1.SetCellAlignment(oix:WrapText,1,'EY2','EZ3')
    e1.MergeCells ('EY2','EZ3')
    
    !Second row of headings
    e1.writeToCell('Issue 1','X3')
    e1.MergeCells ('X3','Y3')
    e1.writeToCell('Issue 2','Z3')
    e1.MergeCells ('Z3','AA3')
    e1.writeToCell('Issue 3','AB3')
    e1.MergeCells ('AB3','AC3')
    e1.writeToCell('Issue 4','AD3')
    e1.MergeCells ('AD3','AE3')
    !e1.writeToCell('AF3','Res 1')  TB13130 - JC
    e1.writeToCell('Res 1','AF3')
    e1.MergeCells ('AF3','AG3')
    e1.writeToCell('Res 2','AH3')
    e1.MergeCells ('AH3','AI3')
    e1.writeToCell('Res 3','AJ3')
    e1.MergeCells ('AJ3','AK3')
    e1.writeToCell('Res 4','AL3')
    e1.MergeCells ('AL3','AM3')
    e1.writeToCell('Symptom 1','AY3')
    e1.MergeCells ('AY3','BB3')
    e1.writeToCell('Symptom 2','BC3')
    e1.MergeCells ('BC3','BF3')
    e1.writeToCell('Symptom 3','BG3')
    e1.MergeCells ('BG3','BJ3')
    e1.writeToCell('Symptom 4','BK3')
    e1.MergeCells ('BK3','BN3')
    e1.writeToCell('Symptom 5','BO3')
    e1.MergeCells ('BO3','BR3')
    e1.writeToCell('Part 1','BS3')
    e1.MergeCells ('BS3','BY3')
    e1.writeToCell('Part 2','BZ3')
    e1.MergeCells ('BZ3','CF3')
    e1.writeToCell('Part 3','CG3')
    e1.MergeCells ('CG3','CM3')
    e1.writeToCell('Part 4','CN3')
    e1.MergeCells ('CN3','CT3')
    e1.writeToCell('Part 5','CU3')
    e1.MergeCells ('CU3','DA3')
    e1.writeToCell('Part 6','DB3')
    e1.MergeCells ('DB3','DH3')
    e1.writeToCell('Part 7','DI3')
    e1.MergeCells ('DI3','DO3')
    e1.writeToCell('Part 8','DP3')
    e1.MergeCells ('DP3','DV3')
    e1.writeToCell('Part 9','DW3')
    e1.MergeCells ('DW3','EC3')
    e1.writeToCell('Part 10','ED3')
    e1.MergeCells ('ED3','EJ3')
    e1.writeToCell('Part 11','EK3')
    e1.MergeCells ('EK3','EQ3')
    e1.writeToCell('Part 12','ER3')
    e1.MergeCells ('ER3','EX3')

    !Now the column titles - these run up the screen
    e1.SetCellAlignment(oix:Orientation, oix:Upward, 'A4', 'EZ4')
    e1.writeToCell('WSC Site ID','A4')
    e1.writeToCell('Customer Reference Number','B4')
    e1.writeToCell('Repair Program','C4')
    e1.writeToCell('Repair Process Status','D4')
    e1.writeToCell('Warranty Status','E4')
    e1.writeToCell('Title','F4')
    e1.writeToCell('First Name','G4')
    e1.writeToCell('Last Name','H4')
    e1.writeToCell('Preferred Contact Method','I4')
    e1.writeToCell('E-mail Address','J4')
    e1.writeToCell('Phone','K4')
    e1.writeToCell('Mobile Phone','L4')
    e1.writeToCell('Address 1','M4')
    e1.writeToCell('Address 2','N4')
    e1.writeToCell('City','O4')
    e1.writeToCell('Zip / Postal Code','P4')
    e1.writeToCell('Country','Q4')
    e1.writeToCell('State / Province','R4')
    e1.writeToCell('Customer Arrival Date Time','S4')
    e1.writeToCell('Customer Interaction End Date Time','T4')
    e1.writeToCell('Serial Number','U4')
    e1.writeToCell('Repair Service Offering','V4')
    e1.writeToCell('Purchase Date','W4')
    e1.writeToCell('Customer Issue','X4')
    e1.writeToCell('Issue Comments','Y4')
    e1.writeToCell('Customer Issue','Z4')
    e1.writeToCell('Issue Comments','AA4')
    e1.writeToCell('Customer Issue','AB4')
    e1.writeToCell('Issue Comments','AC4')
    e1.writeToCell('Customer Issue','AD4')
    e1.writeToCell('Issue Comments','AE4')
    e1.writeToCell('Issue Resolution','AF4')
    e1.writeToCell('Resolution Comments','AG4')
    e1.writeToCell('Issue Resolution','AH4')
    e1.writeToCell('Resolution Comments','AI4')
    e1.writeToCell('Issue Resolution','AJ4')
    e1.writeToCell('Resolution Comments','AK4')
    e1.writeToCell('Issue Resolution','AL4')
    e1.writeToCell('Resolution Comments','AM4')
    e1.writeToCell('Trouble Code','AN4')
    e1.writeToCell('Trouble Code Comments','AO4')
    e1.writeToCell('Trouble Code Verified?','AP4')
    e1.writeToCell('Service Level','AQ4')
    e1.writeToCell('BER','AR4')
    e1.writeToCell('Customer Data Backed Up?','AS4')
    e1.writeToCell('Personal Data Wipe Completed?','AT4')
    e1.writeToCell('Reviewed with Customer?','AU4')
    e1.writeToCell('Customer Satisfied?','AV4')
    e1.writeToCell('Contact for Customer Survey?','AW4')
    e1.writeToCell('Customer Survey Email Address','AX4')
    e1.writeToCell('Symptom Group','AY4')
    e1.writeToCell('Symptom Mode','AZ4')
    e1.writeToCell('Billing Type','BA4')
    e1.writeToCell('Failure Type','BB4')
    e1.writeToCell('Symptom Group','BC4')
    e1.writeToCell('Symptom Mode','BD4')
    e1.writeToCell('Billing Type','BE4')
    e1.writeToCell('Failure Type','BF4')
    e1.writeToCell('Symptom Group','BG4')
    e1.writeToCell('Symptom Mode','BH4')
    e1.writeToCell('Billing Type','BI4')
    e1.writeToCell('Failure Type','BJ4')
    e1.writeToCell('Symptom Group','BK4')
    e1.writeToCell('Symptom Mode','BL4')
    e1.writeToCell('Billing Type','BM4')
    e1.writeToCell('Failure Type','BN4')
    e1.writeToCell('Symptom Group','BO4')
    e1.writeToCell('Symptom Mode','BP4')
    e1.writeToCell('Billing Type','BQ4')
    e1.writeToCell('Failure Type','BR4')
    e1.writeToCell('Part Number','BS4')
    e1.writeToCell('Action Code','BT4')
    e1.writeToCell('Reason Code','BU4')
    e1.writeToCell('Part Serial','BV4')
    e1.writeToCell('Linked Symptom Group','BW4')
    e1.writeToCell('Linked Symptom Mode','BX4')
    e1.writeToCell('Quantity','BY4')
    e1.writeToCell('Part Number','BZ4')
    e1.writeToCell('Action Code','CA4')
    e1.writeToCell('Reason Code','CB4')
    e1.writeToCell('Part Serial','CC4')
    e1.writeToCell('Linked Symptom Group','CD4')
    e1.writeToCell('Linked Symptom Mode','CE4')
    e1.writeToCell('Quantity','CF4')
    e1.writeToCell('Part Number','CG4')
    e1.writeToCell('Action Code','CH4')
    e1.writeToCell('Reason Code','CI4')
    e1.writeToCell('Part Serial','CJ4')
    e1.writeToCell('Linked Symptom Group','CK4')
    e1.writeToCell('Linked Symptom Mode','CL4')
    e1.writeToCell('Quantity','CM4')
    e1.writeToCell('Part Number','CN4')
    e1.writeToCell('Action Code','CO4')
    e1.writeToCell('Reason Code','CP4')
    e1.writeToCell('Part Serial','CQ4')
    e1.writeToCell('Linked Symptom Group','CR4')
    e1.writeToCell('Linked Symptom Mode','CS4')
    e1.writeToCell('Quantity','CT4')
    e1.writeToCell('Part Number','CU4')
    e1.writeToCell('Action Code','CV4')
    e1.writeToCell('Reason Code','CW4')
    e1.writeToCell('Part Serial','CX4')
    e1.writeToCell('Linked Symptom Group','CY4')
    e1.writeToCell('Linked Symptom Mode','CZ4')
    e1.writeToCell('Quantity','DA4')
    e1.writeToCell('Part Number','DB4')
    e1.writeToCell('Action Code','DC4')
    e1.writeToCell('Reason Code','DD4')
    e1.writeToCell('Part Serial','DE4')
    e1.writeToCell('Linked Symptom Group','DF4')
    e1.writeToCell('Linked Symptom Mode','DG4')
    e1.writeToCell('Quantity','DH4')
    e1.writeToCell('Part Number','DI4')
    e1.writeToCell('Action Code','DJ4')
    e1.writeToCell('Reason Code','DK4')
    e1.writeToCell('Part Serial','DL4')
    e1.writeToCell('Linked Symptom Group','DM4')
    e1.writeToCell('Linked Symptom Mode','DN4')
    e1.writeToCell('Quantity','DO4')
    e1.writeToCell('Part Number','DP4')
    e1.writeToCell('Action Code','DQ4')
    e1.writeToCell('Reason Code','DR4')
    e1.writeToCell('Part Serial','DS4')
    e1.writeToCell('Linked Symptom Group','DT4')
    e1.writeToCell('Linked Symptom Mode','DU4')
    e1.writeToCell('Quantity','DV4')
    e1.writeToCell('Part Number','DW4')
    e1.writeToCell('Action Code','DX4')
    e1.writeToCell('Reason Code','DY4')
    e1.writeToCell('Part Serial','DZ4')
    e1.writeToCell('Linked Symptom Group','EA4')
    e1.writeToCell('Linked Symptom Mode','EB4')
    e1.writeToCell('Quantity','EC4')
    e1.writeToCell('Part Number','ED4')
    e1.writeToCell('Action Code','EE4')
    e1.writeToCell('Reason Code','EF4')
    e1.writeToCell('Part Serial','EG4')
    e1.writeToCell('Linked Symptom Group','EH4')
    e1.writeToCell('Linked Symptom Mode','EI4')
    e1.writeToCell('Quantity','EJ4')
    e1.writeToCell('Part Number','EK4')
    e1.writeToCell('Action Code','EL4')
    e1.writeToCell('Reason Code','EM4')
    e1.writeToCell('Part Serial','EN4')
    e1.writeToCell('Linked Symptom Group','EO4')
    e1.writeToCell('Linked Symptom Mode','EP4')
    e1.writeToCell('Quantity','EQ4')
    e1.writeToCell('Part Number','ER4')
    e1.writeToCell('Action Code','ES4')
    e1.writeToCell('Reason Code','ET4')
    e1.writeToCell('Part Serial','EU4')
    e1.writeToCell('Linked Symptom Group','EV4')
    e1.writeToCell('Linked Symptom Mode','EW4')
    e1.writeToCell('Quantity','EX4')
    e1.writeToCell('Swap Serial Number','EY4')
    e1.writeToCell('Swap Product code','EZ4')

    !colourings
    e1.SetCellBackgroundColor (Color:Gray  , 'A1' , 'EZ3')  !set all to gray
    e1.SetCellBackgroundColor (Color:Silver, 'C2' , 'T3' )  !then recolour the five exceptions
    e1.SetCellBackgroundColor (Color:Silver, 'X2' , 'AM3')
    e1.SetCellBackgroundColor (Color:Silver, 'AQ2', 'AR3')
    e1.SetCellBackgroundColor (Color:Silver, 'AY2', 'BR3')
    e1.SetCellBackgroundColor (Color:Silver, 'EY2', 'EZ3')
    E1.SetCellFontColor (color:WHITE, 'B1')

    EXIT


BBFinishSheet   Routine    !now we know how big the sheet is - tmp:currentRow is the last line

    !everything in "Calibri" 10 point ...
    E1.SetCellFontName('Calibri','A1','EZ' & tmp:CurrentRow)
    E1.SetCellFontSize(10,'A1','EZ' & tmp:CurrentRow)
    !special case of one with size 9
    E1.SetCellFontSize(9,'C2')
    !and top row is big
    E1.SetCellFontSize(22,'A1','EZ1')
    !top three rows bold italic
    E1.SetCellFontStyle ('Bold Italic', 'A1','EZ3')
    !titles just bold
    E1.SetCellFontStyle ('Bold', 'A4','EZ4')
    !rest is roman

    !colour every other line after the titles
    Loop x# = 6 to tmp:CurrentRow by 2
        e1.SetCellBackgroundColor (Color:Silver, 'A'&clip(x#), 'EZ'&clip(x#))
    END !loop to colour every other line

    !Add borders round everything after row 2                          
    e1.SetCellBorders ('A2', 'EZ'&tmp:CurrentRow+1, oix:BorderEdgeTop, oix:LineStyleNone, oix:BorderWeightThin)
    e1.SetCellBorders ('A2', 'EZ'&tmp:CurrentRow, oix:BorderEdgeRight, oix:LineStyleNone, oix:BorderWeightThin)
    e1.SetCellBorders ('A2', 'EZ'&tmp:CurrentRow, oix:BorderInsideHorizontal, oix:LineStyleNone, oix:BorderWeightThin)
    e1.SetCellBorders ('A2', 'EZ'&tmp:CurrentRow, oix:BorderInsideVertical, oix:LineStyleNone, oix:BorderWeightThin)

    !make a "few" (72) of these medium weight top and right
    e1.SetCellBorders ('A2' ,'B4 ',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('C2' ,'E4 ',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('F2' ,'T4 ',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('U2' ,'W4 ',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('X2' ,'AM2',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('X3' ,'Y4' ,oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('Z3' ,'AA4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AB3','AC4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AD3','AE4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('Af3','AG4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AH3','AI4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AJ3','AK4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AL3','AM4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AN2','AP4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AQ2','AR4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AS2','AX4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('Ay2','BR2',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AY3','BB4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BC3','BF4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BG3','BJ4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BK3','BN4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BO3','BR4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BS2','EX2',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BS3','BY4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BZ3','CF4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('CG3','CM4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('CN3','CT4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('CU3','DA4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DB3','DH4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DI3','DO4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DP3','DV4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DW3','EC4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('ED3','EJ4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('EK3','EQ4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('ER3','EX4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('EY2','EZ4',oix:BorderEdgeTop   , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('A2' ,'B4' ,oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('C2' ,'E4' ,oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('F2' ,'T4' ,oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('U2' ,'W4' ,oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('X2' ,'AM2',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('X3' ,'Y4' ,oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('Z3' ,'AA4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AB3','AC4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AD3','AE4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('Af3','AG4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AH3','AI4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AJ3','AK4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AL3','AM4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AN2','AP4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AQ2','AR4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AS2','AX4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('Ay2','BR2',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('AY3','BB4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BC3','BF4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BG3','BJ4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BK3','BN4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BO3','BR4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BS2','EX2',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BS3','BY4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('BZ3','CF4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('CG3','CM4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('CN3','CT4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('CU3','DA4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DB3','DH4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DI3','DO4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DP3','DV4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('DW3','EC4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('ED3','EJ4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('EK3','EQ4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('ER3','EX4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)
    e1.SetCellBorders ('EY2','EZ4',oix:BorderEdgeRight , oix:LineStyleNone, oix:BorderWeightMedium)

    !RowHeight = 12.75  (Except row 1 and 4 which will remain default)
    e1.SetRowHeight(2,3,'12.75')
    e1.SetRowHeight(5,tmp:CurrentRow,'12.75')

    exit


BBwriteLine     Routine

    !New line - set this line to format text
    e1.SetCellNumberFormat(oix:NumberFormatText, , , ,'A'&tmp:CurrentRow, 'EZ'&tmp:CurrentRow)


    !tb13221 - J - 24/01/14 - : "if a job booked at RRC was repaired at ARC then the "WSC Site ID" field is filled with the MRRC Code for
    !                            Blackberry taken from the AA20 Trade Account."
    !                            Otherwise we use the ASVCode where ASV:TradeAccNo = Tra:Account_number = wob:HeadAccountNumber

    Access:ASVACC.clearkey(ASV:TradeACCManufKey)       
    if jow:RepairedAt = 'RRC' then
        ASV:TradeAccNo = wob:HeadAccountNumber        !where booked
    ELSE
        ASV:TradeAccNo = 'AA20'                       !ARC trade account
    END
    ASV:Manufacturer = 'BLACKBERRY'
    if access:ASVACC.fetch(ASV:TradeACCManufKey)
        !error
        ASV:ASVCode = ' '
    END

    e1.writeToCell(clip(ASV:ASVCode),'A'&tmp:CurrentRow)            !WSC Site ID
    e1.writeToCell(clip(job:Ref_Number),'B'&tmp:CurrentRow)         !Customer Reference Number
    e1.writeToCell(clip(job:Fault_Code6),'C'&tmp:CurrentRow)        !Repair Programme
    e1.writeToCell(clip(job:Fault_Code8),'D'&tmp:CurrentRow)        !REPAIR PROCESS STATUS
    e1.writeToCell('1','E'&tmp:CurrentRow)                          !Warranty Status (blank)
    !e1.writeToCell('Title','F'&tmp:CurrentRow)                     !Title (blank)
    !e1.writeToCell('First Name','G'&tmp:CurrentRow)                !First Name (blank)
    e1.writeToCell('N/A','G'&tmp:CurrentRow)                        !TB not allocated -Voda000700 First Name should now always be 'N/A'    JC - 28/08/13

    e1.writeToCell(clip(job:company_Name),'H'&tmp:CurrentRow)       !'Last Name'
    e1.writeToCell('Walk-In','I'&tmp:CurrentRow)                    !Preferred Contact Method
    !e1.writeToCell('E-mail Address','J'&tmp:CurrentRow)            !email always blank
    !e1.writeToCell('Phone','K'&tmp:CurrentRow)                     !Phone always blank
    !e1.writeToCell('Mobile Phone','L'&tmp:CurrentRow)              !mobile always blank
    !e1.writeToCell('Address 1','M'&tmp:CurrentRow)                 !address 1 always blank
    !e1.writeToCell('Address 2','N'&tmp:CurrentRow)                 !Address 2 always blank
    e1.writeToCell(clip(job:Address_line3),'O'&tmp:CurrentRow)      !City (for vodacom = Suburb)
    e1.writeToCell(clip(job:postcode),'P'&tmp:CurrentRow)           !Zip / Postal Code
    e1.writeToCell('ZA','Q'&tmp:CurrentRow)                         !country
    !e1.writeToCell('State / Province','R'&tmp:CurrentRow)          !province always blank
    e1.writeToCell(format(Job:Date_Booked,@d06)&' ' & |
                   format(job:Time_booked,@t01),'S'&tmp:CurrentRow) !Customer Arrival Date Time

    if job:Exchange_unit_Number = 0 then
        if job:Date_QA_Passed = '' then
            Do GetDateCompleted                !something = DateCompleted, TimeCompleted
            e1.writeToCell(Format(DateCompleted,@d06)&' '&|        !Customer Interaction End Date Time
                           Format(TimeCompleted,@t01),'T'&tmp:CurrentRow)

        ELSE

            e1.writeToCell(Format(job:Date_QA_Passed,@d06)&' '&|        !Customer Interaction End Date Time
                           Format(job:Time_QA_Passed,@t01),'T'&tmp:CurrentRow)
        END
    ELSE
        !need to find exchange despatch date and time in audit trail
        Access:Audit.clearkey(Aud:TypeActionKey)
        aud:Ref_Number = Job:Ref_number
        aud:Type = 'EXC'
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop
            If access:Audit.next() then break.
            if aud:Ref_Number <> Job:Ref_number then break.
            if aud:Type <> 'EXC' then break.

            if instring('DESPATCH',aud:Action,1,1) then break.      !this should be found in here so ignore the others

        END !loop

        e1.writeToCell(Format(aud:Date,@D06) &' '&|
                       format(aud:Time,@T01),'T'&tmp:CurrentRow)     !Customer Interaction End Date Time
    END

    !get incoming IMEI
    tmp:InboundIMEI    = job:Esn
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    if access:jobthird.fetch(jot:RefNumberKey) = level:benign    
        tmp:InboundIMEI    = jot:OriginalIMEI
    End !If Access:JOBTHIRD.fetch() = Level:Benign
    e1.writeToCell(clip(tmp:InboundIMEI),'U'&tmp:CurrentRow)        !Serial Number


    e1.writeToCell(clip(job:Fault_Code7),'V'&tmp:CurrentRow)        !Repair Service Offering
    e1.writeToCell(format(Job:DOP,@d06),'W'&tmp:CurrentRow)         !Purchase Date
    !=======================================================
    !Customer issues 1 - 4
    e1.writeToCell(clip(job:Fault_Code1),'X'&tmp:CurrentRow)        !Customer Issue
    Access:Manfaulo.clearkey(mfo:Field_Key)                         !and get next column from Manfaulo
    mfo:Manufacturer = 'BLACKBERRY'
    mfo:Field_Number = 1
    mfo:Field = Job:Fault_Code1
    If access:ManFaulo.fetch(mfo:Field_Key)
        !error
    END
    e1.writeToCell(clip(mfo:Description),'Y'&tmp:CurrentRow)        !Issue Comments

    e1.writeToCell(clip(wob:FaultCode19),'Z'&tmp:CurrentRow)        !Customer Issue
    Access:Manfaulo.clearkey(mfo:Field_Key)
    mfo:Manufacturer = 'BLACKBERRY'
    mfo:Field_Number = 19
    mfo:Field = wob:FaultCode19
    If access:ManFaulo.fetch(mfo:Field_Key)
        !error
    END
    e1.writeToCell(clip(mfo:Description),'AA'&tmp:CurrentRow)        !Issue Comments

    !e1.writeToCell('Customer Issue','AB'&tmp:CurrentRow)           !third issue always blank
    !e1.writeToCell('Issue Comments','AC'&tmp:CurrentRow)
    !e1.writeToCell('Customer Issue','AD'&tmp:CurrentRow)           !forth issue always blank
    !e1.writeToCell('Issue Comments','AE'&tmp:CurrentRow)
    !=======================================================
    !issue resolutions 1 - 4
    e1.writeToCell(clip(Job:Fault_code2),'AF'&tmp:CurrentRow)       !Issue Resolution
    Access:Manfaulo.clearkey(mfo:Field_Key)
    mfo:Manufacturer = 'BLACKBERRY'
    mfo:Field_Number = 2
    mfo:Field = job:Fault_Code2
    If access:ManFaulo.fetch(mfo:Field_Key)
        !error
    END
    e1.writeToCell(clip(mfo:Description),'AG'&tmp:CurrentRow)       !Resolution Comments

    e1.writeToCell(clip(Wob:FaultCode20),'AH'&tmp:CurrentRow)       !Issue Resolution
    Access:Manfaulo.clearkey(mfo:Field_Key)
    mfo:Manufacturer = 'BLACKBERRY'
    mfo:Field_Number = 20
    mfo:Field = wob:FaultCode20
    If access:ManFaulo.fetch(mfo:Field_Key)
        !error
    END
    e1.writeToCell(clip(mfo:Description),'AI'&tmp:CurrentRow)       !Resolution Comments

    !e1.writeToCell('Issue Resolution','AJ'&tmp:CurrentRow)         !Third resolution always blank
    !e1.writeToCell('Resolution Comments','AK'&tmp:CurrentRow)
    !e1.writeToCell('Issue Resolution','AL'&tmp:CurrentRow)         !forth resolution always blank
    !e1.writeToCell('Resolution Comments','AM'&tmp:CurrentRow)
    !=======================================================
    e1.writeToCell(clip(Job:Fault_Code9),'AN'&tmp:CurrentRow)       !Trouble Code
    Access:ManFaulo.clearkey(mfo:Field_Key)
    mfo:Manufacturer = 'BLACKBERRY'
    mfo:Field_Number = 9
    mfo:Field = Job:Fault_Code9
    If access:ManFaulo.fetch(mfo:Field_Key)
        !error
    END
    e1.writeToCell(clip(mfo:Description),'AO'&tmp:CurrentRow)       !Trouble Code Comments
    e1.writeToCell('1','AP'&tmp:CurrentRow)                         !Trouble Code Verified? - always 1!
    !look up the service level
    Access:Reptydef.clearkey(rtd:WarManRepairTypeKey)
    rtd:Manufacturer    = f:Manufacturer
    rtd:Warranty        = 'YES'
    rtd:Repair_Type     = job:Repair_Type_Warranty
    If Access:Reptydef.fetch(rtd:WarManRepairTypeKey) = level:benign then
        e1.writeToCell(clip(rtd:WarrantyCode),'AQ'&tmp:CurrentRow)  !Service Level
    end !IF REPTYDEF found

    e1.writeToCell('0','AR'&tmp:CurrentRow)                         !BER - always 0
    e1.writeToCell(clip(Job:Fault_Code10),'AS'&tmp:CurrentRow)      !Customer Data Backed Up?
    e1.writeToCell('1','AT'&tmp:CurrentRow)                         !Personal Data Wipe Completed? always 1
    e1.writeToCell('1','AU'&tmp:CurrentRow)                         !Reviewed with Customer? Always 1
    e1.writeToCell('1','AV'&tmp:CurrentRow)                         !Customer Satisfied? Always 1
    e1.writeToCell('0','AW'&tmp:CurrentRow)                         !Contact for Customer Survey? Always 0
    !e1.writeToCell('','AX'&tmp:CurrentRow)                         !Customer Survey Email Address always blank
    !=======================================================
    !Sympton groups 1-5
    e1.writeToCell(clip(Job:Fault_code11),'AY'&tmp:CurrentRow)      !Symptom Group
    e1.writeToCell(clip(Job:Fault_Code12),'AZ'&tmp:CurrentRow)      !Symptom Mode
    e1.writeToCell(clip(Wob:faultCode13),'BA'&tmp:CurrentRow)       !Billing Type
    e1.writeToCell(clip(Wob:FaultCode14),'BB'&tmp:CurrentRow)       !Failure Type

    e1.writeToCell(clip(Wob:FaultCode15),'BC'&tmp:CurrentRow)       !Symptom Group
    e1.writeToCell(clip(Wob:FaultCode16),'BD'&tmp:CurrentRow)       !Symptom Mode
    e1.writeToCell(clip(Wob:FaultCode17),'BE'&tmp:CurrentRow)       !Billing Type
    e1.writeToCell(clip(Wob:FaultCode18),'BF'&tmp:CurrentRow)       !Failure Type

    !e1.writeToCell('Symptom Group','BG'&tmp:CurrentRow)            !Third symptom always blank
    !e1.writeToCell('Symptom Mode','BH'&tmp:CurrentRow)
    !e1.writeToCell('Billing Type','BI'&tmp:CurrentRow)
    !e1.writeToCell('Failure Type','BJ'&tmp:CurrentRow)
    !e1.writeToCell('Symptom Group','BK'&tmp:CurrentRow)            !forth symptom always blank
    !e1.writeToCell('Symptom Mode','BL'&tmp:CurrentRow)
    !e1.writeToCell('Billing Type','BM'&tmp:CurrentRow)
    !e1.writeToCell('Failure Type','BN'&tmp:CurrentRow)
    !e1.writeToCell('Symptom Group','BO'&tmp:CurrentRow)            !fifth symptom always blank
    !e1.writeToCell('Symptom Mode','BP'&tmp:CurrentRow)
    !e1.writeToCell('Billing Type','BQ'&tmp:CurrentRow)
    !e1.writeToCell('Failure Type','BR'&tmp:CurrentRow)
    !=======================================================
    !part numbers 1 - 12
    tmp:RecordsCount = 0        !long variable is available for reuse in this procedure - was used to count number of jobs to process at start
    Access:Warparts.clearkey(wpr:Part_Number_Key)  !this key is actually on ref_number first
    wpr:Ref_Number = job:Ref_number
    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        if access:WarParts.next() then break.
        if wpr:Ref_Number <> job:Ref_number then break.

        !got one now where to put it?
        Tmp:RecordsCount += 1
        if Tmp:RecordsCount > 12 then break. !only space for 12 parts to be added

        Case Tmp:RecordsCount
            of 1
                e1.writeToCell(clip(wpr:Part_Number),'BS'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'BT'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'BU'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','BV'&tmp:CurrentRow)            !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'BW'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'BX'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'BY'&tmp:CurrentRow)        !Quantity
            of 2
                e1.writeToCell(clip(wpr:Part_Number),'BZ'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'CA'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'CB'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','CC'&tmp:CurrentRow)            !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'CD'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'CE'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'CF'&tmp:CurrentRow)        !Quantity
            of 3
                e1.writeToCell(clip(wpr:Part_Number),'CG'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'CH'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'CI'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','CJ'&tmp:CurrentRow)            !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'CK'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'CL'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'CM'&tmp:CurrentRow)        !Quantity
            of 4
                e1.writeToCell(clip(wpr:Part_Number),'CN'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'CO'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'CP'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','CQ'&tmp:CurrentRow)            !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'CR'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'CS'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'CT'&tmp:CurrentRow)        !Quantity
            of 5
                e1.writeToCell(clip(wpr:Part_Number),'CU'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'CV'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'CW'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','CX'&tmp:CurrentRow)            !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'CY'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'CZ'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'DA'&tmp:CurrentRow)        !Quantity
            of 6
                e1.writeToCell(clip(wpr:Part_Number),'DB'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'DC'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'DD'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','DE'&tmp:CurrentRow)            !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'DF'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'DG'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'DH'&tmp:CurrentRow)        !Quantity
            of 7
                e1.writeToCell(clip(wpr:Part_Number),'DI'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'DJ'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'DK'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','DL&tmp:CurrentRow)             !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'DM'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'DN'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'DO'&tmp:CurrentRow)        !Quantity
            of 8
                e1.writeToCell(clip(wpr:Part_Number),'DP'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'DQ'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'DR'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','DS&tmp:CurrentRow)             !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'DT'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'DU'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'DV'&tmp:CurrentRow)        !Quantity
            of 9
                e1.writeToCell(clip(wpr:Part_Number),'DW'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'DX'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'DY'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','DZ&tmp:CurrentRow)             !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'EA'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'EB'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'EC'&tmp:CurrentRow)        !Quantity
            of 10
                e1.writeToCell(clip(wpr:Part_Number),'ED'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'EE'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'EF'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','EG&tmp:CurrentRow)             !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'EH'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'EI'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'EJ'&tmp:CurrentRow)        !Quantity
            of 11
                e1.writeToCell(clip(wpr:Part_Number),'EK'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'EL'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'EM'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','EN&tmp:CurrentRow)             !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'EO'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'EP'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'EQ'&tmp:CurrentRow)        !Quantity
            of 12
                e1.writeToCell(clip(wpr:Part_Number),'ER'&tmp:CurrentRow)     !Part Number
                e1.writeToCell(clip(wpr:Fault_Code1),'ES'&tmp:CurrentRow)     !Action Code
                e1.writeToCell(clip(wpr:Fault_Code2),'ET'&tmp:CurrentRow)     !Reason Code
                !e1.writeToCell('Part Serial','EU&tmp:CurrentRow)             !part Serial always blank
                e1.writeToCell(clip(wpr:Fault_Code3),'EV'&tmp:CurrentRow)     !Linked Symptom Group
                e1.writeToCell(clip(wpr:Fault_Code4),'EW'&tmp:CurrentRow)     !Linked Symptom Mode
                e1.writeToCell(clip(wpr:Quantity),'EX'&tmp:CurrentRow)        !Quantity
        END !Case Tmp:Records count
    END !loop

    !=======================================================
    !exchange unit
    if job:Exchange_unit_Number <> 0 then
        Access:EXCHANGE.Clearkey(xch:ref_Number_Key)
        xch:ref_Number    = job:Exchange_Unit_Number
        if (Access:EXCHANGE.Fetch(xch:ref_Number_Key) = Level:Benign)
            ! Found
            e1.writeToCell(clip(xch:ESN),'EY'&tmp:CurrentRow)           !Swap Serial Number
        end ! if fetch worked

        e1.writeToCell(clip(job:ProductCode),'EZ'&tmp:CurrentRow)       !Swap Product code

    END !if job:Exchange_unit_Number = 0 then
    !=======================================================

    !that is it - get ready for the next row
    tmp:CurrentRow += 1

    EXIT
GetEDIPath      Routine
    !Set FileName
    If Sub(Clip(man:EDI_Path),-1,1) = '\'
    Else ! If Sub(Clip(man:EDI_Path),-1,1) = '\'
        man:EDI_Path = Clip(man:EDI_Path) & '\'
    End ! If Sub(Clip(man:EDI_Path),-1,1) = '\'

    If ~Exists(man:EDI_path)
        SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
        tmp:DesktopFolder = tmp:DesktopFolder & '\Technical Reports\'
        If ~Exists(tmp:DesktopFolder)
            If ~MkDir(tmp:DesktopFolder)
                ! Can't create desktop folder
            End ! If ~MkDir(tmp:DesktopFolder)
        End ! If ~Exists(tmp:DesktopFolder)

        man:EDI_Path = tmp:DesktopFolder
        If f:Silent = 0
            Beep(Beep:SystemAsterisk)  ;  Yield()
            Case Missive('The export path for this manufacturer does not exist.'&|
                '|'&|
                '|The files will be created on your desktop.','ServiceBase 3g',|
                           'midea.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
        End ! If f:Silent = 0
    End ! If ~Exists(man:EDI_path)

UpdateJOBSE     Routine

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
        If jobe:WarrantyClaimStatus = 'PENDING' Or jobe:WarrantyCLaimStatus = ''
            jobe:WarrantyClaimStatus = 'ON TECHNICAL REPORT'
            Access:JOBSE.Update()
            If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM ON TECHNICAL REPORT','')

            End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM ON TECHNICAL REPORT','')
        End ! If jobe:WarrantyClaimStatus <> 'ON TECHNICAL REPORT'
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
GetInFault      Routine      !returns in tmp:Infault and tmp:InFaultDescription

    tmp:InFault = ''
    tmp:InFaultDescription = ''
    Access:MANFAULT.ClearKey(maf:InFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:InFault      = 1
    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
        !Found

        Case maf:Field_Number
            Of 1
                tmp:Infault = job:Fault_Code1
            Of 2
                tmp:Infault = job:Fault_Code2
            Of 3
                tmp:Infault = job:Fault_Code3
            Of 4
                tmp:Infault = job:Fault_Code4
            Of 5
                tmp:Infault = job:Fault_Code5
            Of 6
                tmp:Infault = job:Fault_Code6
            OF 7
                tmp:Infault = job:Fault_Code7
            Of 8
                tmp:Infault = job:Fault_Code8
            Of 9
                tmp:Infault = job:Fault_Code9
            Of 10
                tmp:Infault = job:Fault_Code10
            Of 11
                tmp:Infault = job:Fault_Code11
            Of 12
                tmp:Infault = job:Fault_Code12
        End !Case maf:Field_Number
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        mfo:Field        = tmp:Infault
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Found
            tmp:InFaultDescription  = mfo:Description
        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign

    EXIT
GetOutFault         Routine      !writes values to tmp:OutFault and Tmp:outFaultDecsription

    tmp:OutFault = ''
    tmp:OutFaultDescription = ''

    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:MainFault      = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Case maf:Field_Number
            Of 1
                tmp:Outfault = job:Fault_Code1
            Of 2
                tmp:Outfault = job:Fault_Code2
            Of 3
                tmp:Outfault = job:Fault_Code3
            Of 4
                tmp:Outfault = job:Fault_Code4
            Of 5
                tmp:Outfault = job:Fault_Code5
            Of 6
                tmp:Outfault = job:Fault_Code6
            OF 7
                tmp:Outfault = job:Fault_Code7
            Of 8
                tmp:Outfault = job:Fault_Code8
            Of 9
                tmp:Outfault = job:Fault_Code9
            Of 10
                tmp:Outfault = job:Fault_Code10
            Of 11
                tmp:Outfault = job:Fault_Code11
            Of 12
                tmp:Outfault = job:Fault_Code12
        End !Case maf:Field_Number

        !If there is no out fault, just get the first one in the OutFault List and use that description
        If tmp:OutFault = ''
            Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
            joo:JobNumber = job:Ref_Number
            joo:FaultCode = '0'
            Set(joo:JobNumberKey,joo:JobNumberKey)
            Loop
                If Access:JOBOUTFL.NEXT()
                   Break
                End !If
                If joo:JobNumber <> job:Ref_Number      |
                Or joo:FaultCode <> '0'      |
                    Then Break.  ! End If
                tmp:OutFaultDescription = joo:Description
                Break
            End !Loop
        Else !If tmp:OutFault = ''
            !There could be duplicate fault codes, so check
            !the out fault list for the selected fault code, and use that description
            !If not, then check the fault code file, and hope you get the right one.
            Access:joboutfl.clearkey(joo:JobNumberKey)
            joo:JobNumber = job:Ref_Number
            joo:FaultCode = tmp:outFault
            if access:joboutfl.fetch(joo:jobNumberKey) = Level:Benign
                tmp:OutFaultDescription  = joo:Description
            ELSE
            !for previous jobs try from manfaulo - but this may give errors
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = job:Manufacturer
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = tmp:Outfault
                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Found
                    tmp:OutFaultDescription  = mfo:Description
                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            END !If access:joboutfl
        End !If tmp:OutFault = ''

    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign

    EXIT
GetDateCompleted     Routine

    !012656 Completed date on Tech reps, complex sorting

    !usage
    !Do GetDateCompleted
    !something = DateCompleted
    !something = TimeCompleted


    !debug:line = 'GetDateCompleted called for job:'&clip(job:Ref_number)
    !Add(DebugFile)

    !First option - the date it was completed
    DateCompleted = job:Date_Completed
    TimeCompleted = job:Time_Completed
    Try:Date      = 1
    try:Time      = 1


    If job:Third_Party_Site <> ''  !this has gone to a third party

        !debug:line = 'This job has a third party site'
        !Add(DebugFile)

        IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))     !using the pre-claim system
            !Use DateCompleted = DateDispatched to 3rd party
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                !Not found
                !debug:line = 'JOBThird record NOT found'
                !Add(DebugFile)
            Else !If Access:JOBTHIRD.NEXT()

                !debug:line = 'JOBThird (JOT) record FOUND'
                !Add(DebugFile)

                If jot:RefNumber <> job:Ref_Number

                    !not found
                    !debug:line = 'JOT ref number does not match'
                    !Add(DebugFile)
                Else !If jot:RefNumber <> job:Ref_Number
                    !this needs to be changed

                    !debug:line = 'About to start looking at the dates. Date despatched is '&format(jot:DateDespatched,@d06)
                    !Add(DebugFile)

                    if jot:DateDespatched <> '' then
                        !debug:line = 'Not blank setting date to '&format(jot:DateDespatched,@d06)
                        !Add(DebugFile)

                        DateCompleted = jot:DateDespatched

                        !Need the time sometimes so
                        GetAuditDateTime(job:Ref_Number,'3RD PARTY AGENT: CONSIGNMENT NOTE PRINTED',try:Date,try:Time,'JOB')
                        if try:Time < 2 then
                            !did not work - Have a fourth option - look for the date it was put into 3RD PARTY AGENT: UNIT SENT
                            !debug:line = '3RD PARTY AGENT: CONSIGNMENT did not work trying for 3RD PARTY AGENT: UNIT SENT '
                            !Add(DebugFile)
                            GetAuditDateTime(job:Ref_Number,'3RD PARTY AGENT: UNIT SENT',try:Date,try:Time,'JOB')
                        END
                        if try:time > 1 then
                            !debug:line = 'Setting time to '&format(Try:time,@t1)
                            !Add(DebugFile)

                            TimeCompleted = try:Time
                        ELSE
                            !debug:line = 'UNIT SENT DID NOT WORK EITHER'
                            !Add(DebugFile)

                        END

                    ELSE
                        !debug:line = 'JOT:Date is blank - looking for audit date and time'
                        !Add(DebugFile)

                        !have a third option  - look for the date it was put into 3RD PARTY AGENT: CONSIGNMENT NOTE PRINTED
                        GetAuditDateTime(job:Ref_Number,'3RD PARTY AGENT: CONSIGNMENT NOTE PRINTED',try:Date,try:Time,'JOB')
                        if try:Date < 2 or try:Time < 2 then
                            !did not work - Have a fourth option - look for the date it was put into 3RD PARTY AGENT: UNIT SENT
                            GetAuditDateTime(job:Ref_Number,'3RD PARTY AGENT: UNIT SENT',try:Date,try:Time,'JOB')
                        END
                        if try:Date > 1 then
                            DateCompleted = try:date

                            !debug:line = 'Setting date to '&format(try:date,@d06)
                            ! Add(DebugFile)

                        END
                        if try:time > 1 then
                            TimeCompleted = try:Time
                            !debug:line = 'Setting time to '&format(Try:time,@t1)
                            !Add(DebugFile)

                        END
                    END !if jot:DateDespatched completed
                End !If jot:RefNumber <> job:Ref_Number
            End !If Access:JOBTHIRD.NEXT()
        ELSE
            !debug:line = 'This manufacturer does not do preclaim'
            !Add(DebugFile)

        END !if _preclain
    ELSE

        !debug:line = 'This job does not have a third party'
        !Add(DebugFile)

    END ! If job:Third_Party_Site <> ''


    !debug:line = 'Returning Date:'&format(DateCompleted,@d06)&' time '&format(TimeCompleted,@t1)
    !Add(DebugFile)


    EXIT
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, Prog:ProgressWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('GenericEDI')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prog:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ASVACC.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFEDI.Open
  Relate:DEFEDI2.Open
  Relate:EXCHANGE.Open
  Relate:LOCATION_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSWARR.UseFile
  Access:WARPARTS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:MANFPALO.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBOUTFL.UseFile
  Access:JOBSE.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBRPNOT.UseFile
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:LOCATION.UseFile
  Access:STDCHRGE.UseFile
  Access:JOBSE2.UseFile
  Access:REPTYDEF.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSENG.UseFile
  Access:TRADEACC.UseFile
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:LOCATLOG.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  OPEN(Prog:ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  SetCursor(Cursor:Wait)
  tmp:FirstCount = 0
  tmp:SecondCount = 0
  tmp:RecordsCount = 0
  Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
  Access:JOBSWARR.Clearkey(jow:ClaimStatusManKey)
  If f:Type <> ''
      jow:Status = f:Type
  Else ! If f:Type <> ''
      jow:Status = 'NO'
  End ! If f:Type <> ''
  jow:Manufacturer = f:Manufacturer
  jow:ClaimSubmitted = f:StartDate
  Set(jow:ClaimStatusManKey,jow:ClaimStatusManKey)
  Loop ! Begin Loop
      If Access:JOBSWARR.Next()
          Break
      End ! If Access:JOBSWARR.Next()
      If f:Type <> ''
          If jow:Status <> f:Type
              Break
          End ! If jow:Status <> 'NO'
      Else ! If f:Type <> ''
          If jow:Status <> 'NO'
              Break
          End ! If jow:Status <> 'NO'
      End ! If f:Type <> ''
      If jow:Manufacturer <> f:Manufacturer
          Break
      End ! If jow:Manufacturer <> f:Manufacturer
      If jow:ClaimSubmitted > f:EndDate
          Break
      End ! If jow:ClaimSubmitted > f:EndDate
      If jow:FirstSecondYear = 1
          tmp:SecondCount += 1
      Else ! If jow:FirstSecondYear = 1
          tmp:FirstCount += 1
      End ! If jow:FirstSecondYear = 1
  End ! Loop
  Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
  SetCursor()
  
  !debugging for the time fetch thing
      !tmp:DebugFile = clip(Path())&'\DebugEDI'&format(clock(),@t5)&'.txt'
      !create(DebugFile)
      !Open(DebugFile)
      !debug:line = 'Open file '
      !Add(DebugFile)
  
  
  !test code added
  tmp:FirstCount += 1
  
  
  Access:MANUFACT.ClearKey(man:Manufacturer_Key)
  man:Manufacturer = f:Manufacturer
  If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Found
  Else ! If Access:MANUFACTURER.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Error
  End ! If Access:MANUFACTURER.TryFetch(man:Manufacturer_Key) = Level:Benign
  
  
  If f:SHist = 0
      ! If not set to do Service History Report only (DBH: 16/06/2008)
      Case man:EDIFileType
      Of 'ALCATEL'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.AlcatelExport(0)
          End ! If tmp:RecordsCount > 0
          
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.AlcatelExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'BLACKBERRY'
          tmp:RecordsCount = tmp:FirstCount + tmp:SecondCount
          !message('About to do blackberry trial')
          If tmp:RecordsCount > 0
              local.BlackberryExport()
          End ! If tmp:RecordsCount > 0
      Of 'BOSCH'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.BoschExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.BoschExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'ERICSSON'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.EricssonExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.EricssonExport(1)
          End ! If tmp:RecordsCount > 0
      !added 29/04/09 by PS Log No 10769
      Of 'HTC'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.HTCExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.HTCExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'LG'
  ! Change --- Only call the 1 LG export as it includes both types (DBH: 08/12/2009) #11167
  !        tmp:RecordsCount = tmp:FirstCount
  !        If tmp:RecordsCount > 0
  !            local.LGExport(0)
  !        End ! If tmp:RecordsCount > 0
  !        tmp:RecordsCount = tmp:SecondCount
  !        If tmp:RecordsCount > 0
  !            local.LGExport(1)
  !        End ! If tmp:RecordsCount > 0
  ! To --- (DBH: 08/12/2009) #11167
          tmp:RecordsCount = tmp:FirstCount + tmp:SecondCount
          If tmp:RecordsCount > 0
              local.LGExport2()
          End ! If tmp:RecordsCount > 0
  ! end --- (DBH: 08/12/2009) #11167
      of 'HUAWEI'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.HUAWEIExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.HUAWEIExport(1)
          End ! If tmp:RecordsCount > 0
  
      Of 'MAXON'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.MaxonExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.MaxonExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'MITSUBISHI'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.MitsubishiExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.MitsubishiExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'MOTOROLA'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.MotorolaExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.MotorolaExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'NEC'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.NECExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.NECExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'NOKIA'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.NokiaExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.NokiaExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'PANASONIC'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.PanasonicExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.PanasonicExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'PHILIPS'
          Error# = 0
          If man:IncludeCharJobs
              If ~DateRange(tmp:CompStartDate,tmp:CompEndDate)
                  Error# = 1
              End ! If ~DateRange(tmp:CompStartDate,tmp:CompEndDate)
          End ! If man:IncludeCharJobs
          If Error# = 0
              tmp:RecordsCount = tmp:FirstCount
              If tmp:RecordsCount > 0
                  local.PhilipsExport(0)
              End ! If tmp:RecordsCount > 0
              tmp:RecordsCount = tmp:SecondCount
              If tmp:RecordsCount > 0
                  local.PhilipsExport(1)
              End ! If tmp:RecordsCount > 0
          End ! If Error# = 0
      Of 'SAGEM'
  !        tmp:RecordsCount = tmp:FirstCount
  !        If tmp:RecordsCount > 0
  !            local.SagemExport(0)
  !        End ! If tmp:RecordsCount > 0
  !        tmp:RecordsCount = tmp:SecondCount
  !        If tmp:RecordsCount > 0
  !            local.SagemExport(1)
  !        End ! If tmp:RecordsCount > 0
          ! #11673 Combined files. (Bryan: 10/02/2011)
          tmp:RecordsCount = tmp:FirstCount + tmp:SecondCount
          IF (tmp:RecordsCount > 0)
              local.SagemExport()
          END ! If (tmp:RecordsCount > 0)
      Of 'SAMSUNG SA'
  !        tmp:RecordsCount = tmp:FirstCount
  !        If tmp:RecordsCount > 0
  !            local.SamsungSAExport(0)
  !        End ! If tmp:RecordsCount > 0
  !        tmp:RecordsCount = tmp:SecondCount
  !        If tmp:RecordsCount > 0
  !            local.SamsungSAExport(1)
  !        End ! If tmp:RecordsCount > 0
          ! #11813 New process. 1st and 2nd in same file. (Bryan: 30/11/2010)
          tmp:RecordsCount = tmp:FirstCount + tmp:SecondCount
          If (tmp:RecordsCount > 0)
              local.SamsungSAExport()
          End
      Of 'SIEMENS'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.SiemensExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.SiemensExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'SONY'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.SonyExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.SonyExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'TELITAL'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.TelitalExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.TelitalExport(1)
          End ! If tmp:RecordsCount > 0
      Of 'ZTE'
          tmp:RecordsCount = tmp:FirstCount
          If tmp:RecordsCount > 0
              local.ZTEExport(0)
          End ! If tmp:RecordsCount > 0
          tmp:RecordsCount = tmp:SecondCount
          If tmp:RecordsCount > 0
              local.ZTEExport(1)
          End ! If tmp:RecordsCount > 0
      End ! Case f:Manufacturer
  
  End ! If ~f:SHist
  
  tmp:RecordsCount = tmp:FirstCount
  If tmp:RecordsCount > 0
      local.ServiceHistoryReport(tmp:RecordsCount,0)
  End ! If tmp:RecordsCount > 0
  tmp:RecordsCount = tmp:SecondCount
  If tmp:RecordsCount > 0
      local.ServiceHistoryReport(tmp:RecordsCount,1)
  End ! If tmp:RecordsCount > 0
  
  
  !close(Debugfile)
  
  
  Post(Event:CloseWindow)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ASVACC.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFEDI.Close
    Relate:DEFEDI2.Close
    Relate:EXCHANGE.Close
    Relate:LOCATION_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ================================================================
  ! Initialise Xml Export Object
    objXmlExport.FInit(fileXmlExport)
  ! ================================================================
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
GetAuditDateTime        PROCEDURE(LONG fJobNumber,STRING fAction,*DATE fDate,*TIME fTIME,<String fType>)
    CODE
        IF (fType = '')
            fType = 'JOB'
        END
        Access:AUDIT.Clearkey(aud:TypeActionKey)
        aud:Ref_Number = fJobNumber
        aud:Type = fType
        aud:Action = fAction
        SET(aud:TypeActionKey,aud:TypeActionKey)
        LOOP UNTIL Access:AUDIT.Next()
            IF (aud:Ref_Number <> fJobNumber OR |
                aud:Type <> fType OR |
                aud:Action <> fAction)
                BREAK
            END
            fDate = aud:Date
            fTime = aud:Time
            BREAK
        END
GetBookingDate      PROCEDURE(LONG fJobNumber,*DATE fDateBooked,*TIME fTimeBooked,LONG fExchange,BYTE fRRCExchange)
    CODE
        IF (fExchange > 0 AND fRRCExchange = 1) OR (NOT SentToHub(fJobNumber))
            IF (job:Who_Booked = 'WEB')
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:Ref_Number = fJobNumber
                aud:Type = 'JOB'
                aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
                SET(aud:TypeActionKey,aud:TypeActionKey)
                LOOP UNTIL Access:AUDIT.Next()
                    IF (aud:Ref_Number <> fJobNumber OR |
                        aud:Type <> 'JOB' OR |
                        aud:ACtion <> 'UNIT RECEIVED AT RRC FROM PUP')
                        BREAK
                    END
                    fDateBooked = aud:Date
                    fTimeBooked = aud:Time
                    BREAK
                END
            END
        ELSE
            IF (SentToHub(fJobNumber))
                found# = 0
                Access:AUDSTATS.Clearkey(aus:DateChangedKey)
                aus:RefNumber = fJobNumber
                aus:Type = 'JOB'
                Set(aus:DateChangedKey,aus:DateChangedKey)
                LOOP UNTIL Access:AUDSTATS.Next()
                    IF (aus:RefNumber <> fJobNumber OR |
                        aus:Type <> 'JOB')
                        BREAK
                    END
                    IF (Sub(Upper(aus:NewStatus),1,3) = '452')
                        fDateBooked = aus:DateChanged
                        fTimeBooked = aus:TimeChanged
                        found# = 1
                        BREAK
                    END
                END

                IF (found# = 0)
                    Access:LOCATLOG.Clearkey(lot:DateKey)
                    lot:RefNumber = fJobNumber
                    SET(lot:DateKey,lot:DateKey)
                    LOOP UNTIL Access:LOCATLOG.Next()
                        IF (lot:RefNumber <> fJobNumber)
                            BREAK
                        END
                        IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                            fDateBooked = lot:TheDate
                            fTimeBooked = lot:TheTime
                            BREAK
                        END
                    END
                END
            END
        END
        RETURN
        
        
ExportLine     Procedure(STRING fString,STRING fSep,<String fType>,<Byte fForceEnd>,<Long fRestrict>)
    CODE
        IF (fRestrict > 0)
            fString = SUB(fString,1,fRestrict)
        END
        Case fType
        OF 'BEGIN'
            Clear(expfil:Record)
            expfil:Line = Clip(fString)
            IF (fForceEnd = 1)
                ADD(ExportFile)
            END
            
        OF 'END'
            IF (fSep <> '')
                expfil:Line = Clip(expfil:Line) & Clip(fSep) & Clip(fString) & CLip(fSep)
            ELSE
                expfil:Line = Clip(expfil:Line) & Clip(fString)
            END
            ADD(ExportFile)
        OF 'BLANK'
            Clear(expfil:Record)
            expfil:Line = ''
            ADD(ExportFile)
        ELSE
            IF (fSep <> '')
                expfil:Line = Clip(expfil:Line) & Clip(fSep) & Clip(fString)
            ELSE
                expfil:Line = Clip(expfil:Line) & Clip(fString)
            END
            IF (fForceEnd = 1)
                ADD(ExportFile)
            END
        END
local.AlcatelExport        Procedure(Byte f:FirstSecondYear)
FileAlcatel    File,Driver('ASCII'),Pre(alcatel),Name(Filename),Create,Bindable,Thread
Record              Record
serv_code     STRING(4)
month         STRING(3)
repair_date   STRING(9)
serial_no     STRING(17)
total_labour  STRING(14)
total_parts   STRING(14)
currency      STRING(11)
user          STRING(20)
                    End
                End

locBasicFilename        String(255)
Code
    Do GetEDIPath

    locBasicFilename = '\CF' & Sub(man:EDI_Account_Number,1,3) & Format(Month(Today()),@n02) & '.DAT'
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.DAT'
    End !If func:SecondYear

    Filename = Clip(man:EDI_Path) & locBasicFilename

    Remove(FileAlcatel)
    Create(FileAlcatel)
    Open(FileAlcatel)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportAlcatel

                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileAlcatel)

ExportAlcatel        Routine
      alcatel:serv_code = SUB(MAN:EDI_Account_Number,1,4)&'!'
      alcatel:month = FORMAT(MONTH(today()),@n02)&'!'
      alcatel:repair_date = FORMAT(YEAR(Job:Date_Completed),@n04)&FORMAT(MONTH(Job:Date_Completed),@n02)&FORMAT(DAY(Job:Date_Completed),@n02)&'!'

        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 0
                End !If jot:RefNumber <> job:Ref_Number
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            alcatel:serial_no = SUB(job:ESN,1,15)&'!'
        Else !IMEIError# = 1
            alcatel:serial_no = SUB(jot:OriginalIMEI,1,15)&'!'
        End !IMEIError# = 1

      alcatel:total_labour = SUB(FORMAT(job:labour_cost_warranty,@n013`3),1,12)&'!'
      alcatel:total_parts      = SUB(FORMAT(job:parts_cost_warranty,@n013`3),1,12)&'!'
      alcatel:currency = 'STERLING!'
      alcatel:user = Format(SUB(job:Ref_Number,1,10),@s10) &'!'
      ADD(FileAlcatel)

local.BlackberryExport    Procedure()            !TB13053 JC 25/05/13
locBasicFilename    String(255)
code

    do getEDIPath

    Access:ASVACC.clearkey(ASV:TradeACCManufKey)
    ASV:TradeAccNo = 'AA20'
    ASV:Manufacturer = 'BLACKBERRY'
    If access:ASVACC.fetch(ASV:TradeACCManufKey)
        !error
        If f:Silent = 0
            Case Missive('Unable to fetch Blackberry defaults.'&|
                'Please check your ASV Defaults for this manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    END

    !create the name by which the file will be saved later
    locBasicFilename =  clip(ASV:ASVCode) & '_VCOM_BB_B2XWSCL_FUPLF_' &|
                        format(day(today()),@n02) & |
                        format(month(today()),@n02) & |
                        year(today()) & |
                        format(Clock(),@T5) & '.xlsx'

    filename = clip(man:edi_Path) & locBasicFileName
    if exists(Filename) then remove(Filename).

    !start up the excel interface
    If E1.Init(0,0) = False
        If f:Silent = 0
            miss# = Missive('Unable to create EDI File.|Please try again','ServiceBase 3g','mstop.jpg','/OK')
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
        Error# = 1
    End ! If E1.Init(0,0) = False

    E1.NewWorkBook()
    E1.RenameWorkSheet('Request Template')
    
    !column headings
    Do BBColumnHeadings
    tmp:CurrentRow = 5  !first four rows are the headers

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManKey,jow:ClaimStatusManKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                      Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Access:WEBJOB.Clearkey(wob:refNumberKey)
                    wob:refNumber    = job:Ref_Number
                    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
                        ! Found
                    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
                        ! Error
                    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

                    Do BBWriteLine
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    SetTarget()
                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept loop

    tmp:CurrentRow += 1
    Do BBFinishSheet

    !save and exit
    E1.SelectCells('A1')        !put cursor back to the beginning
    E1.SaveAs(FileName,oix:xlWorkbookDefault )  !this has to be an xlsx file because of the number of columns
    E1.CloseWorkBook(2)
    E1.Kill

    Do Prog:ProgressFinished

    If f:Silent = 0
         Case Missive('Technical Report Created.'&|
             '|'&|
             '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                        'midea.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
     ELSE
         f:FileList = Clip(f:FileList) & '|' & locBasicFilename
     End ! If f:Silent = 0


local.BoschExport            Procedure(Byte f:FirstSecondYear)
FileBosch    File,Driver('ASCII'),Pre(bosch),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(1000)
                    End
                End
locBasicFilename        String(255)
Code
    Do GetEDIPath

    locBasicFileName = 'BOS.CSV'

    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear

    Filename = Clip(man:EDI_Path) & '\' & locBasicFileName

    Remove(FileBosch)
    Create(FileBosch)
    Open(FileBosch)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Do ExportBosch1

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportBosch

                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:Filelist = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileBosch)

ExportBosch        Routine
    Clear(bosch:record)
    bosch:line1   = '"#H",""'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:ref_number,@s8) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(Day(job:date_booked),@n02) & |
                                            Format(Month(job:date_booked),@n02) & |
                                            Format(year(job:date_Booked),@n04) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(Day(Job:Date_Completed),@N02) & |
                                            Format(Month(Job:Date_Completed),@n02) & |
                                            Format(year(Job:Date_Completed),@n04) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"Y"'
    bosch:line1   = Clip(bosch:line1) & ',"N"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:fault_code4,@s1) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:fault_code2,@s1) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:fault_code6,@s14) & '"'
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:esn,@s15) & '"'
    Else !IMEIError# = 1
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(jot:OriginalIMEI,@s15) & '"'
    End !IMEIError# = 1

    bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:fault_code1,@s10) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(Month(job:fault_code5),@n02) & Format(Sub(Year(job:Fault_Code5),3,2),@n02) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:fault_code3,@s4) & '"'
    bosch:line1   = Clip(bosch:line1) & ',""'
    bosch:line1   = Clip(bosch:line1) & ',""'
    If job:dop <> ''
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(Day(job:dop),@n02) & |
                                                Format(Month(job:dop),@n02) & |
                                                Format(year(job:dop),@n04) & '"'
    Else!If job:dop <> ''
        bosch:line1   = Clip(bosch:line1) & ',""'
    End!If job:dop <> ''
    if job:exchange_unit_number <> ''
        bosch:line1   = Clip(bosch:line1) & ',"Y"'
    Else!if job:exchange_unit_number <> ''
        bosch:line1   = Clip(bosch:line1) & ',"N"'
    End!if job:exchange_unit_number <> ''
    Add(FileBosch)

    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:Ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:Ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'
        Clear(bosch:record)
        bosch:line1   = '"#L"'
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(wpr:fault_code2,@s9) & '"'
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(wpr:fault_code3,@s2) & '"'
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(wpr:fault_code1,@s6) & '"'
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(wpr:fault_code4,@s2) & '"'
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(wpr:part_number,@s9) & '"'
        bosch:line1   = Clip(bosch:line1) & ',"' & Format(job:who_booked,@s3) & '"'
        bosch:line1   = Clip(bosch:line1) & ',""'
        Add(FileBosch)
    end !loop

ExportBosch1        Routine
    Clear(bosch:record)
    bosch:line1   = '"#B","1"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(man:edi_account_number,@s6) & '"'
    bosch:line1   = Clip(bosch:line1) & ',"' & Format(Day(Today()),@n02) & |
                                            Format(Month(Today()),@n02) & |
                                            Format(Year(Today()),@n04) & '"'
! Changing (DBH 05/06/2008) # 9792 - No batch numbers anymore
!    bosch:line1   = Clip(bosch:line1) & ',"' & Format(f:BatchNumber,@s6) & '"'
! to (DBH 05/06/2008) # 9792
    bosch:line1   = Clip(bosch:line1) & ',""'
! End (DBH 05/06/2008) #9792

    bosch:line1   = Clip(bosch:line1) & ',"' & Format(tmp:RecordsCount,@s3) & '"'

    Add(FileBosch)
local.EricssonExport        Procedure(Byte f:FirstSecondYear)
FileEricsson    File,Driver('ASCII'),Pre(Ericsson),Name(Filename),Create,Bindable,Thread
Record          Record
Line1                String(2000)
                    End
                End
locBasicFilename        String(255)
locDate Date()
locTime Time()

Code

    Do GetEDIPath

    locBasicFilename = 'FI1.PRT'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.PRT'
    End ! If func:SecondYear

    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Set(DEFEDI2)
    Access:DEFEDI2.Next()

    Remove(FileEricsson)
    Create(FileEricsson)
    Open(FileEricsson)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Found
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Error
                    End ! If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                    ! Inserting (DBH 06/05/2008) # 9723 - WEBJOB needed for fault code 13
                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        ! Found

                    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        Cycle
                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    ! End (DBH 06/05/2008) #9723


                    !Change Added By Paul 06/10/2009 - Log No 11083
                    !use the manufaturers ASV code if it exists for the account
                    !look up code depending on what type of job it is
                    If jow:RepairedAt = 'ARC' then
                        !this is an ARC job - so look up th head account
                        tmpheadaccount" = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')

                        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
                        ASV:TradeAccNo      = tmpheadaccount"
                        ASV:Manufacturer    = f:Manufacturer
                        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
                            !entry exists - does it contain a code?
                            If clip(ASV:ASVCode) <> '' then
                                tmp:EDIASVCode = clip(ASV:ASVCode)
                            Else
                                tmp:EDIASVCode = clip(man:EDI_Account_Number)
                            End !If clip(ASV:ASVCode) <> '' then
                        Else
                            !no entry - use EDI code
                            tmp:EDIASVCode = clip(man:EDI_Account_Number)
                        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

                    Else
                        !this is an RRC job - so look up the RRC account
                        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
                        ASV:TradeAccNo      = wob:HeadAccountNumber
                        ASV:Manufacturer    = f:Manufacturer
                        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
                            !entry exists - does it contain a code?
                            If clip(ASV:ASVCode) <> '' then
                                tmp:EDIASVCode = clip(ASV:ASVCode)
                            Else
                                tmp:EDIASVCode = clip(man:EDI_Account_Number)
                            End !If clip(ASV:ASVCode) <> '' then
                        Else
                            !no entry - use EDI code
                            tmp:EDIASVCode = clip(man:EDI_Account_Number)
                        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

                    End !If x# = True then
                   

                    ! Insert --- Add job notes to remarks entry (DBH: 24/02/2009) #10616
                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber    = job:Ref_Number
                    if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                        ! Found
                    else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                        ! Error
                    end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                    ! end --- (DBH: 24/02/2009) #10616

                    Do ExportEricsson
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileEricsson)

ExportEricsson        Routine
Data
local:Remarks           String(150)
Code
   CLEAR(ericsson:RECORD)
   count_spares# = 0
   access:warparts.clearkey(wpr:part_number_key)
   wpr:ref_number  = job:Ref_number
   set(wpr:part_number_key, wpr:part_number_key)
   loop
       if access:warparts.next()
           break
       end ! if
       if wpr:ref_number  <> job:Ref_number      |
           then break   ! end if
       end ! if
       If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
           Cycle
       End! If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
       If wpr:Part_Number = 'EXCH'
           Cycle
       End ! If wpr:Part_Number = 'EXCH'
       count_spares# += 1
       ! Work Order Number
       ericsson:Line1   = Format(job:Ref_Number, @s20)      !ColA
       ! Service Location ID
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(tmp:EDIASVCode, @s8)    !ColB
       ! POP Date
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:DOP, @d12)          !ColC
            ! POP Supplier
            ! Vodacom do not have a POP supplier
            ! so this field should always be "VSPC" - L887 (DBH: 24-07-2003)
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('VSPC', @s50)           !ColD
       ! Date Received
       !ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Date_Booked, @d12)
       ! Time Received
       !ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Time_Booked, @t1)
       locDate = job:Date_Booked
       locTime = job:Time_Booked
       GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)

       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(locDate, @d12)          !ColE
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(locTime, @t1)           !Colf
       ! Repair Completion Date
       ! Repair Completion Time
!       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Date_Completed, @d12)
!       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Time_Completed, @t1)

        !012656 Completed date on Tech reps, complex sorting
       Do GetDateCompleted
       LocDate = DateCompleted
       LocTime = TimeCompleted
       !Replaces the following
       !locDate = job:Date_Completed
       !locTime = job:Time_Completed
       IF (job:Exchange_Unit_Number > 0)
           GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
       END !IF (job:Exchange_Unit_Number > 0)
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(locDate, @d12)          !ColG
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(locTime, @t1)           !Colh

       ! Phone Returned Date
       !TB13300 - if the despatched date is blank then replace these with column G + 1
       if job:Date_Despatched = '' then

           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(locDate+1, @d12)        !ColI
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(locTime, @t1)           !ColJ

       ELSE

           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Date_Despatched, @d12)  !ColI

           ! Phone Returned Time
           Found#      = 0
           Access:AUDIT.ClearKey(aud:TypeRefKey)
           aud:Ref_Number = job:Ref_Number
           aud:Type       = 'JOB'
           aud:Date       = Today()
           Set(aud:TypeRefKey, aud:TypeRefKey)
           Loop
               If Access:AUDIT.NEXT()
                   Break
               End ! If
               If aud:Ref_Number <> job:Ref_Number |
                   Or aud:Type       <> 'JOB'      |
                   Then Break   ! End If
               End ! If
               If Instring('JOB DESPATCHED', aud:Action, 1, 1)
                   Found#    = 1
                   ericsson:Line1 = Clip(ericsson:Line1) & '<9>' & Format(aud:Time, @t1)        !ColJ
               End ! If Instring('JOB DESPATCHED',aud:Action,1,1)
           End ! Loop
           If Found# = 0
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & ''
           End ! If Found# = 0
       END !if job:DateDespatched is blank TB13300

       ! Product Type
       ! Comes from the fault code which Vodacom populate with T and A
       ! Need to replaice T with P and A with O - L887 (DBH: 24-07-2003)
       If job:Fault_Code4 = 'T'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('P', @s1)
       Elsif job:Fault_Code4 = 'A'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('O', @s1)
       Else ! If job:Fault_Code4 = 'T'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_Code4, @s1)
       End ! If job:Fault_Code4 = 'T'
       ! Model Number
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Model_Number, @s20)
       ! SSN In
       IMEIError# = 0
       If job:Third_Party_Site <> ''
           Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
           jot:RefNumber = job:Ref_Number
           Set(jot:RefNumberKey, jot:RefNumberKey)
           If Access:JOBTHIRD.NEXT()
               IMEIError# = 1
           Else ! If Access:JOBTHIRD.NEXT()
               If jot:RefNumber <> job:Ref_Number
                   IMEIError# = 1
               Else ! If jot:RefNumber <> job:Ref_Number
                   IMEIError# = 0
               End ! If jot:RefNumber <> job:Ref_Number
           End ! If Access:JOBTHIRD.NEXT()
       Else ! job:Third_Party_Site <> ''
           IMEIError# = 1
       End ! job:Third_Party_Site <> ''

       If IMEIError# = 1
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:ESN, @s30)
       Else ! IMEIError# = 1
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(jot:OriginalIMEI, @s30)
       End ! IMEIError# = 1
        ! SSN Out
        ! Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
       ExchangeAttached# = False
       If job:Exchange_unit_Number <> 0
           Access:JOBSE.Clearkey(jobe:RefNumberKey)
           jobe:RefNumber  = job:Ref_Number
           If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
           ! Found
           Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
           ! Error
           End ! If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            ! RRC exchanges for normal jobs appear on the report
            ! 48 Hour RRC exchanges do not
            ! 2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
           If jobe:Engineer48HourOption = 1
               If jobe:ExchangedATRRC = True
                   If jobe:SecondExchangeNumber <> 0
                       ExchangeNumber# = jobe:SecondExchangeNumber
                   Else ! If jobe:SecondExchangeNumber <> 0
                       ExchangeNumber# = 0
                   End ! If jobe:SecondExchangeNumber <> 0
               End ! If jobe:ExchangeATRRC = True
           Else ! jobe:Exchange48HourOption = 1
               ExchangeNumber# = job:Exchange_Unit_Number
           End ! jobe:Exchange48HourOption = 1

           If ExchangeNumber# <> 0
               ! Exchange Made!
               Access:Exchange.ClearKey(xch:Ref_Number_Key)
               xch:Ref_Number = ExchangeNumber#
               IF Access:Exchange.Fetch(xch:Ref_Number_Key) = Level:Benign
                   ExchangeAttached# = True
                   ericsson:Line1         = Clip(ericsson:Line1) & '<9>' & Format(xch:ESN, @s30)
               END ! IF
           End ! If ShowExchange# = True
       End ! job:Exchange_unit_Number <> 0

       If ExchangeAttached# = False
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & ''
       End ! If ExchangeAttached# = False
       ! Software Revision State
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_Code6, @s10)
            ! Fault Codes ID (P)
            ! Fault Code ID should be the symptom code
            ! i.e. Fault Code 11 not 3 - L887 (DBH: 24-07-2003)

       ! Actually this SHOULD be fault code 3 - L938 (DBH: 01-09-2003)
       ! Inserting (DBH 01/05/2008) # 9723 - Strip 'F' prefix from "Fault Code" field
       If Sub(wpr:Fault_Code3,1,1) = 'F'
           wpr:Fault_Code3 = Sub(wpr:Fault_Code3,2,2)
       End ! If Sub(wpr:Fault_Code3,1,1) = 'F'
       ! End (DBH 01/05/2008) #9723
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(wpr:Fault_code3, @s2)
       ! Action Code ID  (Q)
       If Sub(wpr:Fault_Code2,1,1) = 'A'
           wpr:Fault_Code2 = Sub(wpr:Fault_Code2,2,2)
       End ! If Sub(wpr:Fault_Code2,1,1) = 'A'
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(wpr:Fault_Code2, @s2)
            ! Material Quantity (R)
       ! Quantity must be zero if the part number is "ADJUSTMENT" - L938 (DBH: 01-09-2003)
       If wpr:Part_Number = 'ADJUSTMENT'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('0', @s3)
       Else ! If wpr:Part_Number = 'ADJUSTMENT'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(wpr:Quantity, @s3)
       End ! If wpr:Part_Number = 'ADJUSTMENT'
            ! Material Number (S)
       ! Part Number must be blank if it is an ADJUSTMENT - L938 (DBH: 01-09-2003)
       If wpr:Part_Number = 'ADJUSTMENT'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('', @s18)
       Else ! If wpr:Part_Number = 'ADJUSTMENT'
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(wpr:Part_Number, @s18)
       End ! If wpr:Part_Number = 'ADJUSTMENT'

       ! Reused Indicator
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & '0'
       ! Symptom Code
       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_Code11, @s30)
       ! Remarks
! Changing (DBH 01/05/2008) # 9723 - Show the repair notes unless the Repair Type is 'ESCALATED'
!       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & 'N/A'
! to (DBH 01/05/2008) # 9723
        local:Remarks = ''
        If job:Repair_Type_Warranty = 'ESCALATED'
            local:Remarks = wob:FaultCode13
        Else ! If job:Repair_Type_Warranty = 'ESCALATED'
            ! Insert --- Add Fault Description & Engineers Notes to the end of Repair Notes (DBH: 24/02/2009) #10616
            local:Remarks = Clip(BHStripNonAlphaNum(jbn:Fault_Description,' ' )) & ' ' & Clip(BHStripNonAlphaNum(jbn:Engineers_Notes,' '))
            ! end --- (DBH: 24/02/2009) #10616
            Access:JOBRPNOT.Clearkey(jrn:TheDateKey)
            jrn:RefNumber = job:Ref_Number
            jrn:TheDate = 0
            Set(jrn:TheDateKey,jrn:TheDateKey)
            Loop
                If Access:JOBRPNOT.Next()
                    Break
                End ! If Access:JOBRPNOT.Next()
                If jrn:RefNumber <> job:Ref_Number
                    Break
                End ! If jrn:RefNuber <> job:Ref_Number
                local:Remarks = Clip(local:Remarks) & '. ' & Clip(BHStripNonAlphaNum(jrn:Notes,' '))
                If Len(Clip(local:Remarks)) > 149
                    Break
                End ! If Len(Clip(local:Remarks)) > 149
            End ! Loop (JOBRPNOT)

            if (len(clip(local:Remarks)) > 149)
                local:Remarks = sub(local:Remarks,1,149)
            end ! if (len(clip(local:Remarks)) > 149)


        End ! If job:Repair_Type_Warranty = 'ESCALATED'
        If local:Remarks = ''
            local:Remarks = 'N/A'
        End ! If local:Remarks = ''
        ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Clip(local:Remarks)
! End (DBH 01/05/2008) #9723

       Append(FileEricsson)
       Clear(Ericsson:Record)
   end ! loop

    ! Do not show a blank parts line - TrkBs: 5888 (DBH: 06-06-2005)

   ! If this a scrap AND an exchange, add two lines for each - TrkBs: 5365 (DBH: 27-04-2005)
   If Instring('SCRAP', job:Repair_Type_Warranty, 1, 1) And job:Exchange_Unit_NUmber <> 0
       Loop x# = 1 To 2
           ! Work Order Number
           ericsson:Line1   = Format(job:Ref_Number, @s20)
           ! Service Location ID
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(tmp:EDIASVCode, @s8)
           ! POP Date
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:DOP, @d12)
                ! POP Supplier
                ! Vodacom do not have a POP supplier
                ! so this field should always be "VSPC" - L887 (DBH: 24-07-2003)
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('VSPC', @s50)
           ! Date Received
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Date_Booked, @d12)
           ! Time Received
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Time_Booked, @t1)
           ! Repair Completion Date
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Date_Completed, @d12)
           ! Repair Completion Time
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Time_Completed, @t1)
           ! Phone Returned Date
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Date_Despatched, @d12)
           ! Phone Returned Time
           Found#      = 0
           Access:AUDIT.ClearKey(aud:TypeRefKey)
           aud:Ref_Number = job:Ref_Number
           aud:Type       = 'JOB'
           aud:Date       = Today()
           Set(aud:TypeRefKey, aud:TypeRefKey)
           Loop
               If Access:AUDIT.NEXT()
                   Break
               End ! If
               If aud:Ref_Number <> job:Ref_Number |
                   Or aud:Type       <> 'JOB'      |
                   Then Break   ! End If
               End ! If
               If Instring('JOB DESPATCHED', aud:Action, 1, 1)
                   Found#    = 1
                   ericsson:Line1 = Clip(ericsson:Line1) & '<9>' & Format(aud:Time, @t1)
               End ! If Instring('JOB DESPATCHED',aud:Action,1,1)
           End ! Loop
           If Found# = 0
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & ''
           End ! If Found# = 0
                ! Product Type
                ! Comes from the fault code which Vodacom populate with T and A
                ! Need to replaice T with P and A with O - L887 (DBH: 24-07-2003)
           If job:Fault_Code4 = 'T'
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('P', @s1)
           Elsif job:Fault_Code4 = 'A'
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format('O', @s1)
           Else ! If job:Fault_Code4 = 'T'
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_Code4, @s1)
           End ! If job:Fault_Code4 = 'T'
           ! Model Number
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Model_Number, @s20)
           ! SSN In
           IMEIError# = 0
           If job:Third_Party_Site <> ''
               Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
               jot:RefNumber = job:Ref_Number
               Set(jot:RefNumberKey, jot:RefNumberKey)
               If Access:JOBTHIRD.NEXT()
                   IMEIError# = 1
               Else ! If Access:JOBTHIRD.NEXT()
                   If jot:RefNumber <> job:Ref_Number
                       IMEIError# = 1
                   Else ! If jot:RefNumber <> job:Ref_Number
                       IMEIError# = 0
                   End ! If jot:RefNumber <> job:Ref_Number
               End ! If Access:JOBTHIRD.NEXT()
           Else ! job:Third_Party_Site <> ''
               IMEIError# = 1
           End ! job:Third_Party_Site <> ''

           If IMEIError# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:ESN, @s30)
           Else ! IMEIError# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(jot:OriginalIMEI, @s30)
           End ! IMEIError# = 1
            ! SSN Out
            ! Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
           ExchangeAttached# = False
           If job:Exchange_unit_Number <> 0
               Access:JOBSE.Clearkey(jobe:RefNumberKey)
               jobe:RefNumber  = job:Ref_Number
               If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
               ! Found
               Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
               ! Error
               End ! If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                ! RRC exchanges for normal jobs appear on the report
                ! 48 Hour RRC exchanges do not
                ! 2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
               If jobe:Engineer48HourOption = 1
                   If jobe:ExchangedATRRC = True
                       If jobe:SecondExchangeNumber <> 0
                           ExchangeNumber# = jobe:SecondExchangeNumber
                       Else ! If jobe:SecondExchangeNumber <> 0
                           ExchangeNumber# = 0
                       End ! If jobe:SecondExchangeNumber <> 0
                   End ! If jobe:ExchangeATRRC = True
               Else ! jobe:Exchange48HourOption = 1
                   ExchangeNumber# = job:Exchange_Unit_Number
               End ! jobe:Exchange48HourOption = 1

               If ExchangeNumber# <> 0
                   ! Exchange Made!
                   Access:Exchange.ClearKey(xch:Ref_Number_Key)
                   xch:Ref_Number = ExchangeNumber#
                   IF Access:Exchange.Fetch(xch:Ref_Number_Key) = Level:Benign
                       ExchangeAttached# = True
                       ericsson:Line1         = Clip(ericsson:Line1) & '<9>' & Format(xch:ESN, @s30)
                   END ! IF
               End ! If ShowExchange# = True
           End ! job:Exchange_unit_Number <> 0

           If ExchangeAttached# = False
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & ''
           End ! If ExchangeAttached# = False

           ! Software Revision State
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_Code6, @s10)
                ! Fault Codes ID
                ! Fault Code ID should be the symptom code
                ! i.e. Fault Code 11 not 3 - L887 (DBH: 24-07-2003)
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_code11, @s2)
            ! Action Code ID
            ! If this is an exchange AND a scrap. - TrkBs: 5365 (DBH: 27-04-2005)
           If x# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & '8'
           Else ! If x# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & '11'
           End ! If x# = 1

           ! Material Quantity
           If x# = 1
               ! Quantity is zero for the scrap line - TrkBs: 5365 (DBH: 27-04-2005)
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & '0'
           Else ! If x# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & '1'
           End ! If x# = 1

            ! Material Number
            ! Only show the Material Part Number on the "exchange" line - TrkBs: 5365 (DBH: 27-04-2005)
           If x# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & ''
           Else ! If x# = 1
               ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Clip(jobe:ExchangeProductCode)
           End ! If x# = 1

           ! Reused Indicator
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & '0'
           ! Symptom Code
           ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Format(job:Fault_Code11, @s30)
           ! Remarks
    ! Changing (DBH 01/05/2008) # 9723 - Show the repair notes unless the Repair Type is 'ESCALATED'
    !       ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & 'N/A'
    ! to (DBH 01/05/2008) # 9723
            local:Remarks = ''
            If job:Repair_Type_Warranty = 'ESCALATED'
                local:Remarks = wob:FaultCode13
            Else ! If job:Repair_Type_Warranty = 'ESCALATED'
                ! Insert --- Add Fault Description & Engineers Notes to the end of Repair Notes (DBH: 24/02/2009) #10616
                local:Remarks = Clip(BHStripNonAlphaNum(jbn:Fault_Description,' ' )) & ' ' & Clip(BHStripNonAlphaNum(jbn:Engineers_Notes,' '))
                ! end --- (DBH: 24/02/2009) #10616
                Access:JOBRPNOT.Clearkey(jrn:TheDateKey)
                jrn:RefNumber = job:Ref_Number
                jrn:TheDate = 0
                Set(jrn:TheDateKey,jrn:TheDateKey)
                Loop
                    If Access:JOBRPNOT.Next()
                        Break
                    End ! If Access:JOBRPNOT.Next()
                    If jrn:RefNumber <> job:Ref_Number
                        Break
                    End ! If jrn:RefNuber <> job:Ref_Number
                    local:Remarks = Clip(local:Remarks) & '. ' & Clip(BHStripNonAlphaNum(jrn:Notes,' '))
                    If Len(Clip(local:Remarks)) > 149
                        Break
                    End ! If Len(Clip(local:Remarks)) > 149
                End ! Loop (JOBRPNOT)
                ! Insert --- Add Fault Description & Engineers Notes to the end of Repair Notes (DBH: 24/02/2009) #10616
                if (len(clip(local:Remarks)) > 149)
                    local:Remarks = sub(local:Remarks,1,149)
                end ! if (len(clip(local:Remarks)) > 149)
                ! end --- (DBH: 24/02/2009) #10616
            End ! If job:Repair_Type_Warranty = 'ESCALATED'
            If local:Remarks = ''
                local:Remarks = 'N/A'
            End ! If local:Remarks = ''
            ericsson:Line1   = Clip(ericsson:Line1) & '<9>' & Clip(local:Remarks)
    ! End (DBH 01/05/2008) #9723
           Append(FileEricsson)
       End ! Loop x# = 1 To 2
   End ! If Instring('SCRAP',job:Repair_Type_Warranty,1,1) And job:Exchange_Unit_NUmber <> 0

local.HTCExport                 Procedure(Byte f:FirstSecondYear)
FileHTC    File,Driver('ASCII'),Pre(HTC),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
tmp:FirstPage        Byte(1)
locBasicFilename        String(255)
locDate Date()
locTime Time()
Code

    !Paul - Added comments to this export so we know which line is which when exporting
    !Date - 01/09/2009 - log no 10990
    Do GetEDIPath

    locBasicFilename = 'VOD' & format(clock(),@T5) & format(Day(today()),@n02) & format(month(today()),@n02) & format(year(today()),@n04) & '.CSV'

    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear
    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(FileHTC)
    Create(FileHTC)
    Open(FileHTC)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    !access the jobs record
                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    !now get the JOBSE record
                    Access:JobSE.clearkey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    If Access:JobSE.fetch(jobe:RefNumberKey) = level:notify then
                        !error
                    end

                    !now get the JOSE2 record
                    Access:JobSE2.clearkey(jobe2:RefNumberKey)
                    jobe2:RefNumber = job:Ref_Number
                    if Access:JobSE2.fetch(jobe2:RefNumberKey) = level:notify then
                        !error
                    end
                    !find Warranty parts
                    Free(HTCQueue1)
                    free(HTCQueue2)
                    Do HTCParts

                    Sort(HTCQueue2,HTCQ2:PFC1,HTCQ2:PFC2)
!                    message(records(HTCQueue2))
!
!                    loop x# = 1 to records(HTCQueue2)
!                        get(HTCQueue2,x#)
!                        message(clip(HTCQ2:PFC1) & ' / ' & clip(HTCQ2:PFC2) & ' / ' & HTCQ2:Used & ' / ' & clip(HTCQ2:WarPart[1]) & ' / ' & clip(HTCQ2:WarPart[2]) & ' / ' & clip(HTCQ2:WarPart[3]) & ' / ' & clip(HTCQ2:WarPart[4]) & ' / ' & clip(HTCQ2:WarPart[5]) & ' / ' & clip(HTCQ2:WarPart[6]) & ' / ' & clip(HTCQ2:WarPart[7]))
!                    end


                    !now run the report for each set of Part Fault Codes
                    loop x# = 1 to records(HTCQueue2)
                        Get(HTCQueue2,x#)
                        Do ExportHTC
                    End
                    !Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileHTC)

HTCParts         ROUTINE
    !loop through all parts for this job and write them into a queue
    Access:Warparts.clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:Warparts.next() then
            break
        End !If Access:Warparts.next() then
        If wpr:Ref_Number <> job:Ref_Number then
            Break
        End !If wpr:Ref_Number <> job:Ref_Number then
        !add all parts to the queue
        Loop x# = 1 to wpr:Quantity
            !part does not exist so add it
            HTCQ1:PartNumber = wpr:Part_Number
            HTCQ1:PFC1 = wpr:Fault_Code1
            HTCQ1:PFC2 = wpr:Fault_Code2
            add(HTCQueue1)
        End !If Error() then
    End !Loop

!    !**************************************************************************************
!    !******************************** TEST DATA ADDED *************************************
!
!    Free(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'A'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'A'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'A'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'A'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'A'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'B'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Z'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'C'
!    HTCQ1:PFC1          = 'W'
!    HTCQ1:PFC2          = 'Z'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'D'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'D'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'D'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'D'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Y'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'E'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Z'
!    Add(HTCQueue1)
!
!    HTCQ1:PartNumber    = 'A'
!    HTCQ1:PFC1          = 'X'
!    HTCQ1:PFC2          = 'Z'
!    Add(HTCQueue1)
!
!
!    !**************************************************************************************
!    !**************************************************************************************
    !Now we need to sort the parts for export
    Loop x# = 1 to records(HTCQueue1)
        Get(HTCQueue1,x#)
        !check to see if the PFC1 AND PFC2 on this part have been used
        HTCQ2:PFC1 = HTCQ1:PFC1
        HTCQ2:PFC2 = HTCQ1:PFC2
        HTCQ2:Full = 'N'
        Get(HTCQueue2,HTCQ2:PFC1,HTCQ2:PFC2,HTCQ2:Full)
        If error() then
            !PFC1 has not been added to the second queue - so add an entry
            HTCQ2:PFC1          = HTCQ1:PFC1
            HTCQ2:PFC2          = HTCQ1:PFC2
            HTCQ2:Used          = 1
            HTCQ2:Full          = 'N'
            HTCQ2:WarPart[1]    = HTCQ1:PartNumber
            HTCQ2:WarPart[2]    = ''
            HTCQ2:WarPart[3]    = ''
            HTCQ2:WarPart[4]    = ''
            HTCQ2:WarPart[5]    = ''
            HTCQ2:WarPart[6]    = ''
            HTCQ2:WarPart[7]    = ''
            add(HTCQueue2)
        Else
            !this combination has been used before - add up to a maximum of 7

            !add this part anyway
            HTCQ2:Used += 1

            If HTCQ2:Used = 7 then
                HTCQ2:Full = 'Y'
            End !If HTCQ2:Used = 7 then

            HTCQ2:WarPart[HTCQ2:Used] = clip(HTCQ1:PartNumber)
            Put(HTCQueue2)

        End !If error() then
    End !Loop x# = 1 to records(HTCQueue1)


ExportHTC        Routine
    If tmp:FirstPage = 1
        Clear(HTC:record)
        HTC:line1   = 'Work Order,Ticket Number,Device Description,Inbound Serial Number,Inbound IMEI Number,Outbound Serial Number,' &|
                        'Outbound IMEI Number,Warranty Status,Failure Description,Status Created,Status Diagnose,Status Repair,' &|
                        'Status Hold for Quotation,Status Quotation Accepted,Status Repaired,Status in testing,Status Ready for packing,' &|
                        'Status ready for collection,Status Closed,Return condition,Failure code,Failure Description,Repair code,' &|
                        'Repair Description,Repair Level,Warranty,Engineer Badge,' &|
                        'Material used_1,Material used_2,Material used_3,Material used_4,Material used_5,Material used_6,Material used_7,' &|
                        'Customer Name,Customer Email,Customer Phone'
        Append(FileHTC)
        tmp:FirstPage = 0
    End!If tmp:FirstPage = 1
    Clear(HTC:record)
    !Work Order
    HTC:line1   = job:Ref_Number & ','
    !Ticket Number
    HTC:line1   = clip(HTC:line1) & format(today(),@D06) & ' ' & format(clock(),@T01) & ','
    !Device Description
    HTC:line1   = clip(HTC:line1) & clip(job:Model_Number) & ','

    !work out the IMEI and Serial Numbers
    If job:Third_Party_Site = '' then
        !not a third party job
        If job:Exchange_unit_Number = 0 then
            !not an exchange
            !so in and out IMEI and Serial same
            tmp:InboundSerial   = clip(job:MSN) !InBound
            tmp:InboundIMEI     = clip(job:ESN) !InBound
            tmp:OutboundSerial  = clip(job:MSN) !OutBound
            tmp:OutboundIMEI    = clip(job:ESN) !OutBound
        End !If job:Exchange_unit_Number = 0 then
    End !If job:Third_Party_Site = '' then


    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        !error
        !already set
    Else !IMEIError# = 1
        tmp:InboundSerial   = clip(jot:OriginalMSN) !InBound
        tmp:InboundIMEI     = clip(jot:OriginalIMEI) !InBound
        tmp:OutboundSerial  = clip(job:MSN) !OutBound
        tmp:OutboundIMEI    = clip(job:ESN) !OutBound
    End !IMEIError# = 1

    !Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
    If job:Exchange_unit_Number <> 0
        !RRC exchanges for normal jobs appear on the report
        !48 Hour RRC exchanges do not
        !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
        If jobe:Engineer48HourOption = 1
            If jobe:ExchangedATRRC = True
                If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = jobe:SecondExchangeNumber
                Else !If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = 0
                End !If jobe:SecondExchangeNumber <> 0
            End !If jobe:ExchangeATRRC = True
        Else !jobe:Exchange48HourOption = 1
            ExchangeNumber# = job:Exchange_Unit_Number
        End !jobe:Exchange48HourOption = 1

        If ExchangeNumber# <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = ExchangeNumber#
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                tmp:InboundSerial   = clip(job:MSN) !InBound
                tmp:InboundIMEI     = clip(job:ESN) !InBound
                tmp:OutboundSerial  = clip(xch:MSN) !OutBound
                tmp:OutboundIMEI    = clip(xch:ESN) !OutBound
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        End !If ShowExchange# = True
    End !job:Exchange_unit_Number <> 0

    !we should now have the Serials And IMEI's sorted
    !Inbound Serial Number
    HTC:line1   = clip(HTC:line1) & clip(tmp:InboundSerial) & ','
    !Inbound IMEI Number
    HTC:line1   = clip(HTC:line1) & clip(tmp:InboundIMEI) & ','
    !Outbound Serial Number
    HTC:line1   = clip(HTC:line1) & clip(tmp:OutboundSerial) & ','
    !Outbound IMEI Number
    HTC:line1   = clip(HTC:line1) & clip(tmp:OutboundIMEI) & ','
    !Warranty Status
    HTC:line1   = clip(HTC:line1) & '0' & ','
    !Failure Description
    HTC:line1   = clip(HTC:line1) & clip(job:Fault_Code2) & ','
    !Status Created
    !HTC:line1   = clip(HTC:line1) & Format(job:date_booked,@D06) & ','
    locDate = job:Date_Booked
    locTime = job:Time_Booked
    GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)
    HTC:line1   = clip(HTC:line1) & Format(locDate,@D06) & ','

!Changed by Paul - 01/09/2009 Log no 10990
!    !search the audstat file for awaiting allocation status
!    StatusError# = 1
!    Access:AudStats.clearkey(aus:RefRecordNumberKey)
!    aus:RefNumber = job:Ref_Number
!    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
!    loop
!        If Access:AudStats.Next() then
!            Break
!        End !If Access:AudStats.Next() then
!        If aus:RefNumber <> job:Ref_Number then
!            Break
!        End !!If Access:AudStats.Next() then
!        If clip(aus:NewStatus) = '305 AWAITING ALLOCATION' then
!            !got the status
!            StatusError# = 0
!            Break
!        End !If clip(aus:NewStatus) = '305 AWAITING ALLOCATION' then
!    End !loop

!    !Status Diagnose
!    If StatusError# = 0 then
!        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
!    Else
!        HTC:line1   = clip(HTC:line1) & '' & ','
!    End !If StatusError# = 0 then

    !search the audstat file for Allocated to engineer status
    StatusError# = 1
    Access:AudStats.clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    loop
        If Access:AudStats.Next() then
            Break
        End !If Access:AudStats.Next() then
        If aus:RefNumber <> job:Ref_Number then
            Break
        End !!If Access:AudStats.Next() then
        If clip(aus:NewStatus) = '310 ALLOCATED TO ENGINEER' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '310 ALLOCATED TO ENGINEER' then
    End !loop

    !Status Diagnose + Status Repair
    If StatusError# = 0 then
        !Status Diagnose
        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
        !Status Repair
        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
        tmp:Engineerdate    = aus:DateChanged
        tmp:EngineerTime    = aus:TimeChanged
    Else
        !Status Diagnose
        HTC:line1   = clip(HTC:line1) & '' & ','
        !Status Repair
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If StatusError# = 0 then

!End Change

    !search the audstat file for Estimate Sent status
    StatusError# = 1
    Access:AudStats.clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    loop
        If Access:AudStats.Next() then
            Break
        End !If Access:AudStats.Next() then
        If aus:RefNumber <> job:Ref_Number then
            Break
        End !!If Access:AudStats.Next() then
        If clip(aus:NewStatus) = '520 ESTIMATE SENT' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '520 ESTIMATE SENT' then
    End !loop

    !Status Hold for Quotation
    If StatusError# = 0 then
        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
    Else
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If StatusError# = 0 then

    !search the audstat file for Estimate Accepted status
    StatusError# = 1
    Access:AudStats.clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    loop
        If Access:AudStats.Next() then
            Break
        End !If Access:AudStats.Next() then
        If aus:RefNumber <> job:Ref_Number then
            Break
        End !!If Access:AudStats.Next() then
        If clip(aus:NewStatus) = '535 ESTIMATE ACCEPTED' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '535 ESTIMATE ACCEPTED' then
    End !loop

    !Status Quotation Accepted
    If StatusError# = 0 then
        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
    Else
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If StatusError# = 0 then

!Changed by Paul - 01/09/2009 log no 10990
    !Status Repaired
    If job:Completed = 'YES' then

        !012656 Completed date on Tech reps, complex sorting
        Do GetDateCompleted
        LocDate = DateCompleted
        LocTime = TimeCompleted
        !Replaces the following
        !locDate = job:Date_Completed
        !locTime = job:Time_Completed
        IF (job:Exchange_Unit_Number > 0)
            GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
        END !IF (job:Exchange_Unit_Number > 0)
        !Status Repaired
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
        !Status in testing
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
        !Status Ready for packing
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
        !Status ready for collection
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
    Else

        !012656 Completed date on Tech reps, complex sorting
        Do GetDateCompleted
        LocDate = DateCompleted
        LocTime = TimeCompleted

        IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))     !this job is not completed - can we preclaim?
            IF (job:Exchange_Unit_Number > 0)
                GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
            END !IF (job:Exchange_Unit_Number > 0)
        END !if preclaiming

        !Status Repaired
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
        !Status in testing
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
        !Status Ready for packing
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','
        !Status ready for collection
        HTC:line1   = clip(HTC:line1) & format(locDate,@D06) & ' ' & format(locTime,@T01) & ','

!Was previously
!        !Status Repaired
!        HTC:line1   = clip(HTC:line1) & '' & ','
!        !Status in testing
!        HTC:line1   = clip(HTC:line1) & '' & ','
!        !Status Ready for packing
!        HTC:line1   = clip(HTC:line1) & '' & ','
!        !Status ready for collection
!        HTC:line1   = clip(HTC:line1) & '' & ','

    End !If job:Completed = 'YES' then

!    !Status Repaired
!    If job:Completed = 'YES' then
!        HTC:line1   = clip(HTC:line1) & format(job:Date_Completed,@D06) & ' ' & format(job:Time_Completed,@T01) & ','
!    Else
!        HTC:line1   = clip(HTC:line1) & '' & ','
!    End !If job:Completed = 'YES' then


!    !Status in testing
!    HTC:line1   = clip(HTC:line1) & format(tmp:Engineerdate,@D06) & ' ' & format(tmp:EngineerTime,@T01) & ','
!
!    !search the audstat file for Ready to Despatch/Ready for collection status
!    StatusError# = 1
!    Access:AudStats.clearkey(aus:RefRecordNumberKey)
!    aus:RefNumber = job:Ref_Number
!    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
!    loop
!        If Access:AudStats.Next() then
!            Break
!        End !If Access:AudStats.Next() then
!        If aus:RefNumber <> job:Ref_Number then
!            Break
!        End !!If Access:AudStats.Next() then
!        If clip(aus:NewStatus) = '810 READY TO DESPATCH' then
!            !got the status
!            StatusError# = 0
!            Break
!        End !If clip(aus:NewStatus) = '810 READY TO DESPATCH' then
!    End !loop
!
!    If StatusError# = 0 then
!        !Status Ready for packing
!        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
!        !Status ready for collection
!        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
!    Else
!        !Status Ready for packing
!        HTC:line1   = clip(HTC:line1) & '' & ','
!        !Status ready for collection
!        HTC:line1   = clip(HTC:line1) & '' & ','
!    End !If StatusError# = 0 then
!Change End

    !search the audstat file for a 'DESPATCHED' status
    StatusError# = 1
    Access:AudStats.clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    loop
        If Access:AudStats.Next() then
            Break
        End !If Access:AudStats.Next() then
        If aus:RefNumber <> job:Ref_Number then
            Break
        End !!If Access:AudStats.Next() then
        If clip(aus:NewStatus) = '901 DESPATCHED' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '810 READY TO DESPATCH' then
        If clip(aus:NewStatus) = '905 DESPATCHED UNPAID' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '810 READY TO DESPATCH' then
        If clip(aus:NewStatus) = '910 DESPATCH PAID' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '810 READY TO DESPATCH' then
        If clip(aus:NewStatus) = '902 DESPATCHED TO CUSTOMER' then
            !got the status
            StatusError# = 0
            Break
        End !If clip(aus:NewStatus) = '810 READY TO DESPATCH' then
    End !loop

    !Status Closed
    If StatusError# = 0 then
        HTC:line1   = clip(HTC:line1) & format(aus:DateChanged,@D06) & ' ' & format(aus:TimeChanged,@T01) & ','
    Else
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If StatusError# = 0 then

    !Look up the return condition
    !Return condition
    If clip(job:Estimate_Rejected) = 'YES' then
        HTC:line1   = clip(HTC:line1) & '3' & ','
    Else
        Access:Reptydef.clearkey(rtd:WarManRepairTypeKey)
        rtd:Manufacturer    = f:Manufacturer
        rtd:Warranty        = 'YES'
        rtd:Repair_Type     = job:Repair_Type_Warranty
        If Access:Reptydef.fetch(rtd:WarManRepairTypeKey) = level:benign then
            !got the record
            If rtd:BER = 8 then
                !this is a R.N.R. job
                HTC:line1   = clip(HTC:line1) & '2' & ','
            Else
                HTC:line1   = clip(HTC:line1) & '1' & ','
            End !If rtd:BER = 8 then
        Else
            !error
            HTC:line1   = clip(HTC:line1) & '' & ','
        End !If Access:Reptydef.fetch(rtd:WarManRepairTypeKey) = level:benign then
    End !If clip(job:Estimate_Rejected) = 'YES' then

    !Failure code
    HTC:line1   = clip(HTC:line1) & clip(HTCQ2:PFC2) & ','

    !look up the description of this fault
    !Failure Description
    Access:ManFaupa.clearkey(map:Field_Number_Key)
    map:Manufacturer    = f:Manufacturer
    map:Field_Number    = 2
    If access:ManFaupa.fetch(map:Field_Number_Key) = level:benign then
        If map:UseRelatedJobCode = 1 then
            !need to use the related codes table
            Access:ManFaulo.clearkey(mfo:Field_Key)
            mfo:Manufacturer    = f:Manufacturer
            mfo:Field_Number    = 2
            mfo:Field           = clip(HTCQ2:PFC2)
            If Access:ManFaulo.fetch(mfo:Field_Key) = level:benign then
                HTC:line1   = clip(HTC:line1) & clip(mfo:Description) & ','
            else
                HTC:line1   = clip(HTC:line1) & '' & ','
            End !If Access:ManFaulo.fetch(mfo:Field_Key) = level:benign then
        Else
            !Use standard code table
            Access:Manfpalo.clearkey(mfp:Field_Key)
            mfp:Manufacturer    = f:Manufacturer
            mfp:Field_Number    = 2
            mfp:Field           = clip(HTCQ2:PFC2)
            If Access:Manfpalo.fetch(mfp:Field_Key) = level:Benign then
                HTC:line1   = clip(HTC:line1) & clip(mfp:Description) & ','
            else
                HTC:line1   = clip(HTC:line1) & '' & ','
            End !If Access:Manfpalo.fetch(mfp:Field_Key) = level:Benign then
        End !If map:UseRelatedJobCode = 1 then
    Else
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If access:ManFaupa.fetch(map:Field_Number_Key) = level:benign then

    !Repair code
    HTC:line1   = clip(HTC:line1) & clip(HTCQ2:PFC1) & ','

    !look up the description of this fault
    !Repair Description
    Access:ManFaupa.clearkey(map:Field_Number_Key)
    map:Manufacturer    = f:Manufacturer
    map:Field_Number    = 1
    If access:ManFaupa.fetch(map:Field_Number_Key) = level:benign then
        If map:UseRelatedJobCode = 1 then
            !need to use the related codes table
            Access:ManFaulo.clearkey(mfo:Field_Key)
            mfo:Manufacturer    = f:Manufacturer
            mfo:Field_Number    = 1
            mfo:Field           = clip(HTCQ2:PFC1)
            If Access:ManFaulo.fetch(mfo:Field_Key) = level:benign then
                HTC:line1   = clip(HTC:line1) & clip(mfo:Description) & ','
            else
                HTC:line1   = clip(HTC:line1) & '' & ','
            End !If Access:ManFaulo.fetch(mfo:Field_Key) = level:benign then
        Else
            !Use standard code table
            Access:Manfpalo.clearkey(mfp:Field_Key)
            mfp:Manufacturer    = f:Manufacturer
            mfp:Field_Number    = 1
            mfp:Field           = clip(HTCQ2:PFC1)
            If Access:Manfpalo.fetch(mfp:Field_Key) = level:Benign then
                HTC:line1   = clip(HTC:line1) & clip(mfp:Description) & ','
            else
                HTC:line1   = clip(HTC:line1) & '' & ','
            End !If Access:Manfpalo.fetch(mfp:Field_Key) = level:Benign then
        End !If map:UseRelatedJobCode = 1 then
    Else
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If access:ManFaupa.fetch(map:Field_Number_Key) = level:benign then


    !Find the repair level for the job
    !Repair Level
    Access:Reptydef.clearkey(rtd:WarManRepairTypeKey)
    rtd:Manufacturer    = f:Manufacturer
    rtd:Warranty        = 'YES'
    rtd:Repair_Type     = job:Repair_Type_Warranty
    If Access:Reptydef.fetch(rtd:WarManRepairTypeKey) = level:benign then
        !got the record
        HTC:line1   = clip(HTC:line1) & clip(rtd:WarrantyCode) & ','
    Else
        !error
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If Access:Reptydef.fetch(rtd:WarManRepairTypeKey) = level:benign then

    !Warranty
    HTC:line1   = clip(HTC:line1) & '0' & ','

    !find the engineer assigned at completion
    StatusError# = 1
    Access:jobseng.clearkey(joe:JobNumberKey)
    joe:JobNumber       = job:Ref_Number
    joe:DateAllocated   = job:Date_Completed
    Set(joe:JobNumberKey,joe:JobNumberKey)
    loop
        If Access:jobseng.next() then
            break
        End !If Access:jobseng.next() then
        If joe:JobNumber <> job:Ref_Number then
            break
        End !
        !this should be the record i am looking for so break
        StatusError# = 0
        Break
    End

    !Engineer Badge
    If StatusError# = 0 then
        HTC:line1   = clip(HTC:line1) & clip(joe:UserCode) & ','
    Else
        HTC:line1   = clip(HTC:line1) & '' & ','
    End !If StatusError# = 0 then

    !Material used_1 - Material used_7
    loop z# = 1 to 7
        HTC:line1   = clip(HTC:line1) & clip(HTCQ2:WarPart[z#]) & ','
    End !Loop

    tmp:CustomerName = Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)
    if clip(tmp:CustomerName) = '' then
        tmp:CustomerName = clip(job:Company_Name)
    end
    !Customer Name
    HTC:line1   = clip(HTC:line1) & Clip(tmp:CustomerName) & ','
    !Customer Email
    HTC:line1   = clip(HTC:line1) & clip(jobe:EndUserEmailAddress) & ','
    !Customer Phone
    HTC:line1   = clip(HTC:line1) & clip(job:Mobile_Number) & ','




    Append(FileHTC)
    Clear(HTC:Record)
local.HUAWEIExport        Procedure(Byte f:FirstSecondYear)
FileHUAWEI    File,Driver('ASCII'),Pre(HUAWEI),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
locBasicFilename    String(255)
locDateString       String(12)
Code
    Do GetEDIPath

    !huawei want this as ddmmyyhhmmss
    locBasicFilename = format(Today(),@D5) !gives dd/mm/yy in temporary use of filename
    LocDateString = locBasicFilename[1:2]&locBasicFilename[4:5]&locBasicFilename[7:8]&format(clock(),@t05)

    !now back to using filename for the intended purpose
    locBasicFilename = 'HUAWEITR'&LocDateString&'.CSV'   !follows EeiTRDDMMYYHHSS.CSV

    If f:FirstSecondYear
        locBasicFilename = 'HUAWEITR'&locdatestring&'2y.CSV'  !Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear

    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(FileHUAWEI)
    Create(FileHUAWEI)
    Open(FileHUAWEI)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If

                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    !ready to export a record - will need the job open
                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! 
                        !Error
                        Cycle
                    End ! 

                    Access:Webjob.clearkey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_number
                    if access:webjob.fetch(wob:RefNumberKey)
                        !error
                    END

                    Access:TradeAcc.clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    if access:TradeAcc.fetch(tra:Account_Number_Key)
                        clear(tra:Record)
                        tra:Company_Name = 'NOT FOUND'
                    END

                    Access:Asvacc.clearkey(ASV:TradeACCManufKey)
                    ASV:TradeAccNo = tra:Account_Number
                    ASV:Manufacturer = 'HUAWEI'
                    if access:AsvAcc.fetch(ASV:TradeACCManufKey)
                        clear(ASV:record)
                    END

                    !need jobse2 for the customer mobile number
                    Access:Jobse2.clearkey(jobe2:RefNumberKey)
                    jobe2:RefNumber = job:Ref_Number
                    if access:jobse2.fetch(jobe2:RefNumberKey)
                        !error make sure this is blank
                        clear(Jobe2:record)
                    END

                    !job notes for the engineers notes
                    Access:jobnotes.clearkey(jbn:RefNumberKey)
                    jbn:RefNumber = Job:Ref_number
                    if access:jobnotes.fetch(jbn:RefNumberKey)
                        clear(jbn:Record)
                    END

                    do GetInFault   !returns in tmp:Infault and tmp:InFaultDescription
                    Do GetOutFault  !returns in tmp:outfault and tmp:outFaultDescription

                    !Look up the accessries
                    !default to NO
                    tmp:Charger = 'NO'
                    tmp:Battery = 'NO'
                    tmp:Other   = 'NO'
                    Access:JobAcc.clearkey(jac:Ref_Number_Key)
                    jac:Ref_Number = jow:RefNumber
                    set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                    Loop
                        if access:jobAcc.next() then break.
                        if jac:Ref_Number <> jow:RefNumber then break.
                        Case(jac:Accessory)
                            of 'BATTERY'
                                tmp:Battery = 'YES'
                            of 'CHARGER'
                                tmp:Charger = 'YES'
                            else
                                tmp:Other   = 'YES'
                        END !Case jac:Accessory
                    END

                    !need three warenty parts
                    Clear(Tmp:Wpart)
                    clear(Tmp:WQty)
                    x# = 1
                    Access:WARPARTS.Clearkey(wpr:part_Number_Key)
                    wpr:ref_Number    = job:Ref_Number
                    set(wpr:part_Number_Key,wpr:part_Number_Key)
                    Loop 
                        if access:Warparts.next() then break.
                        if wpr:ref_Number <> job:Ref_Number then break.
                        if wpr:Part_Number = 'EXCH' then cycle.
                        Tmp:Wpart[x#] = wpr:Part_Number
                        Tmp:Wqty[x#]  = wpr:Quantity
                        x# += 1
                        if x# > 3 then break.
                    END


                    !need details of the fourth jobfault
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = 4        !maf:Field_Number
                    mfo:Field        = job:Fault_Code4
                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        !found mfo:Description
                    ELSE
                        mfo:Description = ''
                    END


                    tmp:ExchangeIMEI = 'NA'
                    tmp:ExchangeMSN  = 'NA'
                    If job:Exchange_Unit_Number <> 0

                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                            !Found
                            tmp:ExchangeIMEI   = xch:ESN
                            tmp:ExchangeMSN    = xch:MSN
                        Else!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                            !Error
                        End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                    End !If job:Exchange_Unit_Number <> 0

                    !need to look up some dates
                    tmp:ReceiveDate = job:date_booked       !can we find a better one, if it was sent to ARC?
                    !first try to replace this with recevied at RRC from PUP
                    IF (job:Who_Booked = 'WEB')
                         Access:AUDIT.Clearkey(aud:TypeActionKey)
                         aud:Ref_Number = job:Ref_Number
                         aud:Type = 'JOB'
                         aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
                         SET(aud:TypeActionKey,aud:TypeActionKey)
                         LOOP UNTIL Access:AUDIT.Next()
                             IF (aud:Ref_Number <> job:Ref_Number OR |
                                 aud:Type <> 'JOB' OR |
                                 aud:Action <> 'UNIT RECEIVED AT RRC FROM PUP')
                                 BREAK
                             END
                             tmp:ReceiveDate = aud:Date
                             BREAK
                         END
                         
                     END ! IF (job:Who_Booked = 'WEB')

                    !Second try to replace receivedate - Has it been sent off to an ARC for repair
                    Access:Locatlog.clearkey(lot:NewLocationKey)
                    lot:RefNumber   = job:Ref_Number
                    lot:NewLocation = 'RECEIVED AT ARC'
                    If access:Locatlog.tryfetch(lot:NewLocationKey) = level:benign then
                        !put the date in
                        tmp:ReceiveDate = lot:TheDate
                    End

                    !012656 Completed date on Tech reps, complex sorting
                    Do GetDateCompleted
                    tmp:FinishDate = DateCompleted
                    !replaces
                    !tmp:FinishDate = job:Date_Completed

                    !tmp:PickUpDate to only be when goes to 810
                    tmp:PickUpDate = ''
                    !tmp:PickupDate  Set here - 
                    !GetAuditDateTime(job:Ref_Number,'READY TO DESPATCH',tmp:PickupDate,tmp:EngineerTime,'JOB')

                    !try and ammend this to the date it went into 810
                    Access:AUDSTATS.Clearkey(aus:DateChangedKey)
                    aus:RefNumber = job:Ref_Number
                    aus:Type = 'JOB'
                    Set(aus:DateChangedKey,aus:DateChangedKey)
                    LOOP UNTIL Access:AUDSTATS.Next()
                        IF (aus:RefNumber <> job:Ref_Number OR |
                            aus:Type <> 'JOB')
                            BREAK
                        END
                        IF (Sub(Upper(aus:NewStatus),1,3) = '810')
                            tmp:PickupDate = aus:DateChanged
                            BREAK
                        END
                    END
                    !end of setting the three dates needed

                    Access:users.clearkey(use:User_Code_Key)
                    use:User_Code = job:Engineer
                    if access:users.fetch(use:User_Code_Key)
                        clear(use:record)
                    END

                    Do ExportHUAWEI
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('HUAWEI Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileHUAWEI)

ExportHUAWEI        Routine

    !job and jow open at the right place already

    Clear(HUAWEI:Record)
    HUAWEI:line1   = '"'& Clip(job:ref_number)                                  &'","'&|        !A  Title: Paper Work Sheet No
                     CLIP(job:Company_Name)                                     &'","'&|        !B  Title: Customer
                     clip(jobe2:SMSAlertNumber)                                 &'","'&|        !C  Title: Customer Mobile
                     clip(job:Telephone_Number)                                 &'","'&|        !D  Title: Customer telephone
                     clip(job:Transit_Type)                                     &'","'&|        !E  Title: Initial Transit type
                     clip(tra:Company_Name)                                     &'","'&|        !F  Title: Sender - Name of Franchise that booked the repair.
                     ''                                                         &'","'&|        !G  Title: Sender Mobile (BLANK)
                     ''                                                         &'","'&|        !H  Title: Sender Tel (BLANK)
                     clip(ASV:ASVCode)                                          &'","'&|        !I  Title: Operator Name
                     Format(job:dop,@d02)                                       &'","'&|        !J  Title: Purchase Date (MM/DD/YYYY)
                     Format(tmp:ReceiveDate,@d02)                               &'","'&|        !K  Title: Receive Date
                     Format(tmp:FinishDate,@d02)                                &'","'&|        !L  Title: Finish Date
                     Format(tmp:PickUpDate,@d02)                                &'","'&|        !M  Title: Pick up phone date
                     clip(Job:Ref_number)                                       &'","'&|        !N  Title: POP No - confirmed same as A
                     clip(Job:Model_number)                                     &'","'&|        !O  Title: Model
                     clip(job:ProductCode)                                      &'","'&|        !P  Title: Item Code
                     clip(job:ESN)                                              &'","'&|        !Q  Title: IMEI/ESN/MSID
                     clip(job:MSN)                                              &'","'&|        !R  Title: MSN Number
                     clip(tmp:Charger)                                          &'","'&|        !S  Title: with Charger
                     clip(tmp:Battery)                                          &'","'&|        !T  Title: With Battery
                     clip(tmp:Other)                                            &'","'&|        !U  Title: Other Accessory
                     clip(tmp:InFaultDescription)&'('&clip(tmp:InFault)&')'     &'","'&|        !V  Title: Fault Description
                     clip(job:Fault_Code4)                                      &'","'&|        !W  Title: Symptom detail 1
                     clip(mfo:Description)                                      &'","'&|        !X  Title: Symptom detail 2
                     ''                                                         &'","'&|        !Y  Title: Symptom detail 3 confirmed as blank
                     clip(jbn:Engineers_Notes)                                  &'","'&|        !Z  Title: Other Fault
                     clip(job:Warranty_Charge_Type)                             &'","'&|        !AA Title: Warranty Status
                     'Self Repair'                                              &'","'&|        !AB Title: Maintenance Mode - confirmes as "Self Repair"
                     clip(tmp:OutFault)                                         &'","'&|        !AC Title: Maintenace Method
                     clip(Tmp:Wpart[1])                                         &'","'&|        !AD Title: Spare Code 1
                     clip(Tmp:Wqty[1])                                          &'","'&|    `   !AE Title:Spare num 1
                     'NA'                                                       &'","'&|        !AF Title:Spare SN 1 confirmed as NA
                     'NA'                                                       &'","'&|        !AG Title:Spare new SN 1 - confirmed as NA
                     clip(Tmp:Wpart[2])                                         &'","'&|        !AH Title: Spare code 2
                     clip(Tmp:WQty[2])                                          &'","'&|        !AI Title: Spare num 2
                     'NA'                                                       &'","'&|        !AJ Title: Spare SN2 - confirmed as NA
                     'NA'                                                       &'","'&|        !AK Title: Spare New SN 2 - confirmed as NA
                     clip(Tmp:Wpart[3])                                         &'","'&|        !AL Title: Spare Code 3
                     clip(Tmp:Wqty[3])                                          &'","'&|        !AM Title: Spare Num 3
                     'NA'                                                       &'","'&|        !AN Title: Spare SN3 - confirmed as NA
                     'NA'                                                       &'","'&|        !AO Title: Spare New SN3 - confirmed as NA
                     clip(tmp:ExchangeIMEI)                                     &'","'&|        !AP Title: IMEI of exchange unit attached on job
                     clip(tmp:ExchangeMSN)                                      &'","'&|        !AQ Title: MSN of exchange unit attached on job
                     ''                                                         &'","'&|        !AR Title: Original Mainboard item (BLANK)
                     ''                                                         &'","'&|        !AS Title: New Mainboard item (BLANK)
                     clip(use:Forename)&' '&clip(use:Surname)                   &'","'&|        !AT Title: Engineers name
                     clip(jbn:Engineers_Notes)                                  &'"'            !AU Title: Engineers notes (confirmed duplicate of Z)


    Append(FileHUAWEI)

    EXIT
!    IMEIError# = 0
!    If job:Third_Party_Site <> ''
!        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
!        jot:RefNumber = job:Ref_Number
!        Set(jot:RefNumberKey,jot:RefNumberKey)
!        If Access:JOBTHIRD.NEXT()
!            IMEIError# = 1
!        Else !If Access:JOBTHIRD.NEXT()
!            If jot:RefNumber <> job:Ref_Number
!                IMEIError# = 1
!            Else !If jot:RefNumber <> job:Ref_Number
!                IMEIError# = 0
!            End !If jot:RefNumber <> job:Ref_Number
!        End !If Access:JOBTHIRD.NEXT()
!    Else !job:Third_Party_Site <> ''
!        IMEIError# = 1
!    End !job:Third_Party_Site <> ''
!
!    If IMEIError# = 1
!        HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(job:esn)
!    Else !IMEIError# = 1
!        HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(jot:OriginalIMEI)
!    End !IMEIError# = 1
!
!    IF job:Exchange_Unit_Number <> ''
!        access:exchange.clearkey(xch:ref_number_key)
!        XCH:Ref_Number = job:Exchange_Unit_Number
!        If access:exchange.fetch(xch:ref_number_key) = Level:Benign
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(xch:esn)
!        ELSE!If access:exchange.fetch(xch:ref_number_key) = Level:Benign
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ','
!        END!If access:exchange.fetch(xch:ref_number_key) = Level:Benign
!    ELSE!IF job:Exchange_Unit_Number <> ''
!        HUAWAI:line1   = Clip(HUAWAI:line1) & ','
!    END!IF job:Exchange_Unit_Number <> ''
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(Format(job:date_booked,@d6))
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(format(job:date_completed,@d6))
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(job:model_number)
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ','
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(job:fault_code1)
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(job:fault_code3)
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(job:fault_code4)
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(job:fault_code2)
!    !spares!
!    cnt#=0
!
!    access:warparts.clearkey(wpr:part_number_key)
!    wpr:ref_number  = job:ref_number
!    set(wpr:part_number_key,wpr:part_number_key)
!    loop
!        if access:warparts.next()
!           break
!        end !if
!        if wpr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        yldcnt# += 1
!        if yldcnt# > 25
!           yield() ; yldcnt# = 0
!        end !if
!        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!            Cycle
!        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!        If wpr:Part_Number = 'EXCH'
!            Cycle
!        End !If wpr:Part_Number = 'EXCH'
!
!        cnt#+=1
!        CASE cnt#
!          OF 1
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(WPR:Part_Number)
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
!          OF 2
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(WPR:Part_Number)
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
!          OF 3
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(WPR:Part_Number)
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
!          OF 4
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(WPR:Part_Number)
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
!          OF 5
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(WPR:Part_Number)
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
!
!            BREAK
!        END
!    end !loop
!
!    If cnt# < 5
!        Loop x# = cnt# To 4
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',None Given'
!            HUAWAI:line1   = Clip(HUAWAI:line1) & ',0.00'
!        End!Loop x# = cnt# To 5
!
!    End!If cnt# < 5
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(Format(job:parts_cost_warranty,@n8.2))
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(Format(job:labour_cost_warranty,@n8.2))
!    HUAWAI:line1   = Clip(HUAWAI:line1) & ',' & Clip(Format(job:sub_total_warranty,@n8.2))


local.LGExport2    Procedure()
!tmp:CurrentRow      Long()
locFileNumber       Long()
locBasicFilename    String(255)
locDate Date()
locTime Time()
code


    do getEDIPath

    locFileNumber = getini('LGEDI','LastNumber',0,path() & '\SB2KDEF.INI') + 1

    locBasicFilename = 'GFS_' & format(day(today()),@n02) & |
                                            format(month(today()),@n02) & |
                                            year(today()) & '_' & |
                                            locFileNumber & '.xls'

    filename = clip(man:edi_Path) & locBasicFileName
    !stop(fileName)

    If E1.Init(0,0) = False
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
                'Please check your EDI Defaults for this manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
        Error# = 1
    End ! If E1.Init(0,0) = False

    E1.NewWorkBook()
    E1.RenameWorkSheet('LG')
    e1.writeToCell('','A1')
    e1.writeToCell('Customer Name','B1')
    e1.writeToCell('Address1','C1')
    e1.writeToCell('Address2','D1')
    e1.writeToCell('Address3','E1')
    e1.writeToCell('Post Code','F1')
    e1.writeToCell('City','G1')
    e1.writeToCell('State','H1')
    e1.writeToCell('Phone No','I1')
    e1.writeToCell('Cellular No','J1')
    e1.writeToCell('Social Security No','K1')
    e1.writeToCell('ASC Claim No','L1')
    e1.writeToCell('Service Department Code','M1')
    e1.writeToCell('Service Type Code','N1')
    e1.writeToCell('Model No','O1')
    e1.writeToCell('Serial No','P1')
    e1.writeToCell('New Serial No','Q1')
    e1.writeToCell('ESN/IMEI No','R1')
    e1.writeToCell('New ESN/IMEI No','S1')
    e1.writeToCell('Software Inbound Version','T1')
    e1.writeToCell('Software Outbound Version','U1')
    e1.writeToCell('Repair Level','V1')
    e1.writeToCell('Dealer Name','W1')
    e1.writeToCell('Warranty Flag','X1')
    e1.writeToCell('Date Of Purchase','Y1')
    e1.writeToCell('Dealer Receipt Date','Z1')
    e1.writeToCell('Promise Date','AA1')
    e1.writeToCell('Receipt Date','AB1')
    e1.writeToCell('Repair Start Date','AC1')
    e1.writeToCell('Repair End Date','AD1')
    e1.writeToCell('Return Date','AE1')
    e1.writeToCell('Repair Remark','AF1')
    e1.writeToCell('Customer Remark','AG1')
    e1.writeToCell('Service Engineer Code','AH1')
    e1.writeToCell('Mobile Operator Code','AI1')
    e1.writeToCell('Condition Code','AJ1')
    e1.writeToCell('Secondary Condition Code','AK1')
    e1.writeToCell('Primary Symptom1 Code','AL1')
    e1.writeToCell('Primary Symptom2 Code','AM1')
    e1.writeToCell('Secondary Symptom1 Code','AN1')
    e1.writeToCell('Seconday Symtpom2 Code','AO1')
    e1.writeToCell('Defect Code','AP1')
    e1.writeToCell('Seconday Defect Code','AQ1')
    e1.writeToCell('Section Code','AR1')
    e1.writeToCell('Secondary Section Code','AS1')
    e1.writeToCell('Repair Code','AT1')
    e1.writeToCell('Secondary Repair Code','AU1')
    e1.writeToCell('Labor Amount','AV1')
    e1.writeToCell('Freight Amount','AW1')
    e1.writeToCell('Others Amount','AX1')
    e1.writeToCell('Visit Amount','AY1')
    e1.writeToCell('Material Amount','AZ1')
    e1.writeToCell('Parts Amount','BA1')
    e1.writeToCell('Receipt Type Code','BB1')
    e1.writeToCell('Part No1','BC1')
    e1.writeToCell('Part Qty1','BD1')
    e1.writeToCell('Invoice','BE1')
    e1.writeToCell('Circuit No','BF1')
    e1.writeToCell('Part No2','BG1')
    e1.writeToCell('Part Qty2','BH1')
    e1.writeToCell('Invoice','BI1')
    e1.writeToCell('Circuit No','BJ1')
    e1.writeToCell('Part No3','BK1')
    e1.writeToCell('Part Qty3','BL1')
    e1.writeToCell('Invoice','BM1')
    e1.writeToCell('Circuit No','BN1')
    e1.writeToCell('Part No4','BO1')
    e1.writeToCell('Part Qty4','BP1')
    e1.writeToCell('Invoice','BQ1')
    e1.writeToCell('Circuit No','BR1')
    e1.writeToCell('Part No5','BS1')
    e1.writeToCell('Part Qty5','BT1')
    e1.writeToCell('Invoice','BU1')
    e1.writeToCell('Circuit No','BV1')
    e1.writeToCell('Part No6','BW1')
    e1.writeToCell('Part Qty6','BX1')
    e1.writeToCell('Invoice','BY1')
    e1.writeToCell('Circuit No','BZ1')
    e1.writeToCell('Part No7','CA1')
    e1.writeToCell('Part Qty7','CB1')
    e1.writeToCell('Invoice','CC1')
    e1.writeToCell('Circuit No','CD1')
    e1.writeToCell('Part No8','CE1')
    e1.writeToCell('Part Qty8','CF1')
    e1.writeToCell('Invoice','CG1')
    e1.writeToCell('Circuit No','CH1')
    e1.writeToCell('Part No9','CI1')
    e1.writeToCell('Part Qty9','CJ1')
    e1.writeToCell('Invoice','CK1')
    e1.writeToCell('Circuit No','CL1')
    e1.writeToCell('Part No10','CM1')
    e1.writeToCell('Part Qty10','CN1')
    e1.writeToCell('Invoice','CO1')
    e1.writeToCell('Circuit No','CP1')

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManKey,jow:ClaimStatusManKey)

    tmp:CurrentRow = 2

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                      Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Access:WEBJOB.Clearkey(wob:refNumberKey)
                    wob:refNumber    = job:Ref_Number
                    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
                        ! Found
                    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
                        ! Error
                    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

                    Do ExportLG2
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    SetTarget()
                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished
    tmp:CurrentRow += 1

    E1.SetCellFontName('Tahoma','A1','EA' & tmp:CurrentRow)
    E1.SetCellFontSize(8,'A1','EA' & tmp:CurrentRow)
!    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,'D2','E' & tmp:CurrentRow)
    E1.SetColumnWidth('A','EA')
    E1.SelectCells('A2')
    E1.SaveAs(FileName)
    E1.CloseWorkBook(2)
    E1.Kill

    putini('LGEDI','LastNumber',locFileNumber,path() & '\SB2KDEF.INI')

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0



ExportLG2           routine
    col# = 1
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 2. Customer Name
    e1.writeToCell(job:Company_Name,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 3. Address1
    e1.writeToCell(job:Address_Line1,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 4. Address2
    e1.writeToCell(job:Address_Line2,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 5. Address3
    e1.writeToCell(job:Address_Line3,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 6. Postcode
    e1.writeToCell(job:PostCode,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 7. City
    Access:TRADEACC.Clearkey(tra:account_Number_Key)
    tra:account_Number    = wob:headAccountNumber
    if (Access:TRADEACC.TryFetch(tra:account_Number_Key) = Level:Benign)
        ! Found
        e1.writeToCell(tra:Address_Line3,e1.columnName(col#) & tmp:currentRow); col# += 1
    else ! if (Access:TRADEACC.TryFetch(tra:account_Number_Key) = Level:Benign)
        ! Error
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    end ! if (Access:TRADEACC.TryFetch(tra:account_Number_Key) = Level:Benign)
    ! 8. State
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 9. Phone No
    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,e1.columnName(col#) & tmp:currentRow)
    e1.writeToCell(job:Mobile_Number,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 10 Cellular No.
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 11 Social Security No
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 12 ASC Claim No
    e1.writeToCell(job:Ref_Number,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 13 Service Deparment Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 14 Service Type Code
    e1.writeToCell('CI',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 15 Model No
    e1.writeToCell(job:Model_Number,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 16 Serial Number
    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,e1.columnName(col#) & tmp:currentRow)
    e1.writeToCell(job:MSN,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 17 New Serial No
    Access:EXCHANGE.Clearkey(xch:ref_Number_Key)
    xch:ref_Number    = job:Exchange_Unit_Number
    if (Access:EXCHANGE.TryFetch(xch:ref_Number_Key) = Level:Benign)
        ! Found
        E1.SetCellNumberFormat(oix:NumberFormatText,,0,,e1.columnName(col#) & tmp:currentRow)
        e1.writeToCell(xch:MSN,e1.columnName(col#) & tmp:currentRow); col# += 1
    else ! if (Access:EXCHANGE.TryFetch(xch:ref_Number_Key) = Level:Benign)
        ! Error
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    end ! if (Access:EXCHANGE.TryFetch(xch:ref_Number_Key) = Level:Benign)
    ! 18 ESN/IMEI No
    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,e1.columnName(col#) & tmp:currentRow)
    e1.writeToCell(job:ESN,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 19 New ESN/IMEI No
    if (job:exchange_Unit_Number > 0)
        E1.SetCellNumberFormat(oix:NumberFormatText,,0,,e1.columnName(col#) & tmp:currentRow)
        e1.writeToCell(xch:ESN,e1.columnName(col#) & tmp:currentRow); col# += 1
    else ! if (job:exchange_Unit_Number > 0)
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    end ! if (job:exchange_Unit_Number > 0)
    ! 20 Software Inbound Version
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 21 Software Outbound Version
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 22 Repair Level
    e1.writeToCell(job:Fault_Code4,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 23 Dealer Name
    e1.writeToCell(tra:Company_Name,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 24 Warranty Flag
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 25 Date Of Purchase
    e1.writeToCell(format(day(job:DOP),@n02) & format(month(job:DOP),@n02) & year(job:DOP),e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 26 Dealer Receipt Date
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 27 Promise Date
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 28 Receipt Date
!    if (job:who_Booked = 'WEB')
!        found# = 0
!        Access:AUDIT.Clearkey(aud:TypeActionKey)
!        aud:ref_Number    = job:Ref_Number
!        aud:type    = 'JOB'
!        aud:action    = 'UNIT RECEIVED AT RRC FROM PUP'
!        set(aud:TypeActionKey,aud:TypeActionKey)
!        loop
!            if (Access:AUDIT.Next())
!                Break
!            end ! if (Access:AUDIT.Next())
!            if (aud:ref_Number    <> job:Ref_Number)
!                Break
!            end ! if (aud:ref_Number    <> job:Ref_Number)
!            if (aud:type    <> 'JOB')
!                Break
!            end ! if (aud:type    <> 'JOB')
!            if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
!                Break
!            end ! if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
!
!            e1.writeToCell(format(day(aud:Date),@n02) & format(month(aud:Date),@n02) & year(aud:Date),e1.columnName(col#) & tmp:currentRow); col# += 1
!            found# = 1
!            break
!        end ! loop
!        if (found# = 0)
!            e1.writeToCell(format(day(job:date_Booked),@n02) & format(month(job:date_Booked),@n02) & year(job:date_Booked),e1.columnName(col#) & tmp:currentRow); col# += 1
!        end ! if (found# = 0)
!    else ! if (job:who_Booked = 'WEB')
!        e1.writeToCell(format(day(job:date_Booked),@n02) & format(month(job:date_Booked),@n02) & year(job:date_Booked),e1.columnName(col#) & tmp:currentRow); col# += 1
!    end !if (job:who_Booked = 'WEB')
    locDate = job:Date_Booked
    locTime = job:Time_Booked
    GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)
    e1.writeToCell(format(day(locDate),@n02) & format(month(locDate),@n02) & year(locDate),e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 29 Repair Start Date
    found# = 0
    Access:JOBSENG.Clearkey(joe:jobNumberKey)
    joe:jobNumber       = job:Ref_Number
    joe:dateAllocated   = today()
    set(joe:jobNumberKey,joe:jobNumberKey)
    loop
        if (Access:JOBSENG.Next())
            Break
        end ! if (Access:JOBSENG.Next())
        if (joe:jobNumber    <> job:Ref_Number)
            Break
        end ! if (joe:jobNumber    <> job:Ref_Number)
        e1.writeToCell(format(day(joe:dateAllocated),@n02) & format(month(joe:dateAllocated),@n02) & year(joe:dateAllocated),e1.columnName(col#) & tmp:currentRow); col# += 1
        found# = 1
        break
    end ! loop
    if (found# = 0)
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    end !
    ! 30 Repair End Date
!    found# = 0
!    Access:AUDIT.Clearkey(aud:typeActionKey)
!    aud:ref_Number    = job:Ref_Number
!    aud:type    = 'JOB'
!    aud:action    = 'RAPID QA UPDATE: MANUAL QA PASSED'
!    aud:date    = Today()
!    set(aud:typeActionKey,aud:typeActionKey)
!    loop
!        if (Access:AUDIT.Next())
!            Break
!        end ! if (Access:AUDIT.Next())
!        if (aud:ref_Number    <> job:Ref_Number)
!            Break
!        end ! if (aud:ref_Number    <> job:Ref_Number)
!        if (aud:type    <> 'JOB')
!            Break
!        end ! if (aud:type    <> 'JOB')
!        if (aud:action    <> 'RAPID QA UPDATE: MANUAL QA PASSED')
!            Break
!        end ! if (aud:action    <> 'RAPID QA UPDATE: MANUAL QA PASSED')
!        e1.writeToCell(format(day(aud:Date),@n02) & format(month(aud:Date),@n02) & year(aud:Date),e1.columnName(col#) & tmp:currentRow); col# += 1
!        found# = 1
!        break
!    end ! loop
!    if (found# = 0)
!        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
!    end ! if (found# = 0)

    !012656 Completed date on Tech reps, complex sorting
    Do GetDateCompleted
    LocDate = DateCompleted
    LocTime = TimeCompleted
    !Replaces the following
    !locDate = job:Date_Completed
    !locTime = job:Time_Completed

    IF (job:Exchange_Unit_Number > 0)
        GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
    END !IF (job:Exchange_Unit_Number > 0)
    e1.writeToCell(format(day(locDate),@n02) & format(month(locDate),@n02) & year(locDate),e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 31. Return Date
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 32. Repair Remark
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 33. Customer Remark
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 34. Service Engineer Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 35. Mobile Operator Code
    found# = 0
    If jow:RepairedAt = 'ARC' then
        !this is an ARC job - so look up th head account
        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
        ASV:TradeAccNo      = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
        ASV:Manufacturer    = 'LG'
        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
            !entry exists - does it contain a code?
            If clip(ASV:ASVCode) <> '' then
                e1.writeToCell(asv:ASVCode,e1.columnName(col#) & tmp:currentRow); col# += 1
                found# = 1
            End !If clip(ASV:ASVCode) <> '' then
        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

    Else
        !this is an RRC job - so look up the RRC account
        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
        ASV:TradeAccNo      = wob:HeadAccountNumber
        ASV:Manufacturer    = 'LG'
        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
            !entry exists - does it contain a code?
            If clip(ASV:ASVCode) <> '' then
                e1.writeToCell(asv:ASVCode,e1.columnName(col#) & tmp:currentRow); col# += 1
                found# = 1
            End !If clip(ASV:ASVCode) <> '' then
        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
    End !If x# = True then
    if (found# = 0)
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    end ! if (found# = 0)
    ! 36. Condition Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 37. Secondcary Condition Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 38. Primary Symptom1 Code
    e1.writeToCell(job:fault_Code1,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 39. Primary Symptom2 Code
    e1.writeToCell(job:fault_Code9,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 40. Secondary Symtpom1 Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 41. Secondary Symptom2 Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 42. Defect Code
    e1.writeToCell(job:fault_Code2,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 43. Secondary Defect Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 44. Section Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 45. Secondary Section Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 46. Repair Code
    e1.writeToCell(job:fault_Code3,e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 47. Secondary Repair Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 48. Labour Amount
    e1.writeToCell(format(jobe:ClaimValue,@n_14.2),e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 49. Freight Amount
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 50. Others Amount
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 51. Visit Amount
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 52. Material Amount
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 53. Parts Amount
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 54. Receipt Type Code
    e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    ! 55. Part No1
    countParts# = 0
    Access:WARPARTS.Clearkey(wpr:part_Number_Key)
    wpr:ref_Number    = job:Ref_Number
    set(wpr:part_Number_Key,wpr:part_Number_Key)
    loop
        if (Access:WARPARTS.Next())
            Break
        end ! if (Access:WARPARTS.Next())
        if (wpr:ref_Number    <> job:Ref_Number)
            Break
        end ! if (wpr:ref_Number    <> job:Ref_Number)
        countParts# += 1
        if (countParts# > 10)
            break
        end !if (countParts# > 10)
        E1.SetCellNumberFormat(oix:NumberFormatText,,0,,e1.columnName(col#) & tmp:currentRow)
        e1.writeToCell(wpr:part_Number,e1.columnName(col#) & tmp:currentRow); col# += 1
        e1.writeToCell(wpr:quantity,e1.columnName(col#) & tmp:currentRow); col# += 1
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
        e1.writeToCell('',e1.columnName(col#) & tmp:currentRow); col# += 1
    end ! loop
    tmp:CurrentRow += 1
local.MaxonExport        Procedure(Byte f:FirstSecondYear)
FileMaxon    File,Driver('ASCII'),Pre(Maxon),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
locBasicFilename    String(255)
Code
    Do GetEDIPath

    locBasicFilename = 'MAX.CSV'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear

    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(FileMaxon)
    Create(FileMaxon)
    Open(FileMaxon)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportMaxon
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileMaxon)

ExportMaxon        Routine
    Clear(Maxon:Record)
    Maxon:line1   = ',' & Clip(job:ref_number)
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(Format(job:dop,@d6))
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(job:esn)
    Else !IMEIError# = 1
        Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(jot:OriginalIMEI)
    End !IMEIError# = 1

    IF job:Exchange_Unit_Number <> ''
        access:exchange.clearkey(xch:ref_number_key)
        XCH:Ref_Number = job:Exchange_Unit_Number
        If access:exchange.fetch(xch:ref_number_key) = Level:Benign
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(xch:esn)
        ELSE!If access:exchange.fetch(xch:ref_number_key) = Level:Benign
            Maxon:line1   = Clip(Maxon:line1) & ','
        END!If access:exchange.fetch(xch:ref_number_key) = Level:Benign
    ELSE!IF job:Exchange_Unit_Number <> ''
        Maxon:line1   = Clip(Maxon:line1) & ','
    END!IF job:Exchange_Unit_Number <> ''
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(Format(job:date_booked,@d6))
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(format(job:date_completed,@d6))
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(job:model_number)
    Maxon:line1   = Clip(Maxon:line1) & ','
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(job:fault_code1)
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(job:fault_code3)
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(job:fault_code4)
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(job:fault_code2)
    !spares!
    cnt#=0

    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'

        cnt#+=1
        CASE cnt#
          OF 1
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(WPR:Part_Number)
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
          OF 2
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(WPR:Part_Number)
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
          OF 3
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(WPR:Part_Number)
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
          OF 4
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(WPR:Part_Number)
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))
          OF 5
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(WPR:Part_Number)
            Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(FORMAT(WPR:Purchase_Cost,@n8.2))

            BREAK
        END
    end !loop

    If cnt# < 5
        Loop x# = cnt# To 4
            Maxon:line1   = Clip(Maxon:line1) & ',None Given'
            Maxon:line1   = Clip(Maxon:line1) & ',0.00'
        End!Loop x# = cnt# To 5

    End!If cnt# < 5
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(Format(job:parts_cost_warranty,@n8.2))
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(Format(job:labour_cost_warranty,@n8.2))
    Maxon:line1   = Clip(Maxon:line1) & ',' & Clip(Format(job:sub_total_warranty,@n8.2))

    Append(FileMaxon)
local.MitsubishiExport        Procedure(Byte f:FirstSecondYear)
FileMitsubishi    File,Driver('ASCII'),Pre(Mitsubishi),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
tmp:FirstPage        Byte(1)
locBasicFilename    String(255)
Code
    Do GetEDIPath

    locBasicFilename = 'MIT.CSV'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear
    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(FileMitsubishi)
    Create(FileMitsubishi)
    Open(FileMitsubishi)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportMitsubishi
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileMitsubishi)

ExportMitsubishi        Routine
    If tmp:FirstPage = 1
        Clear(Mitsubishi:record)
        Mitsubishi:line1   = 'RegionCode,CustomerID,RepairNo,ModelNo,IMEI,DatePurchase,DateReceived,DateShipped,' &|
                        'WarrantyCode,ConditionCode,SymptomCode,DefectCode,1stPartNo,1stPartQty,' &|
                        '2ndPartNo,2ndPartQty,3rdPartNo,3rdPartQty,4thPartNo,4thPartQty,' &|
                        '5thPartNo,5thPartQty,LabourClaim,PartsClaim,TotalClaim'
        Append(FileMitsubishi)
        tmp:FirstPage = 0
    End!If tmp:FirstPage = 1
    Clear(Mitsubishi:Record)
    Mitsubishi:line1   = 'IP1,'
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(man:edi_account_number) & ','
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:ref_number) & ','
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:model_number) & ','
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:esn) & ','
    Else !IMEIError# = 1
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(jot:OriginalIMEI) & ','
    End !IMEIError# = 1

    If job:dop <> ''
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(Day(job:dop),@n02) & '.' & Format(Month(job:dop),@n02) &|
                                        '.' & Format(Year(job:dop),@n04) & ','

    Else!If job:dop <> ''
        Mitsubishi:line1   = Clip(Mitsubishi:Line1) & ','
    End!If job:dop <> ''
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(Day(job:date_booked),@n02) & '.' & Format(Month(job:date_booked),@n02) &|
                                    '.' & Format(Year(job:date_booked),@n04) & ','
    If job:exchange_unit_number <> ''
        If job:exchange_despatched <> ''
            Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(Day(job:Exchange_Despatched),@n02) & '.' & Format(Month(job:Exchange_Despatched),@n02) &|
                                        '.' & Format(Year(job:Exchange_Despatched),@n04) & ','
        Else!If job:exchange_despatched <> ''
            Mitsubishi:line1   = Clip(Mitsubishi:line1) & ','
        End!If job:exchange_despatched <> ''
    Else!If job:exchange_unit_number <> ''
        If job:date_despatched <> ''
            Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(Day(job:Date_Despatched),@n02) & '.' & Format(Month(job:Date_Despatched),@n02) &|
                                        '.' & Format(Year(job:Date_Despatched),@n04) & ','
        Else!If job:date_despatched <> ''
            Mitsubishi:line1   = Clip(Mitsubishi:line1) & ','
        End!If job:date_despatched <> ''
    End!If job:exchange_unit_number <> ''

    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:fault_code1) & ','     !Warranty Code
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:fault_code2) & ','     !Condition Code
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:fault_code3) & ','     !Symptom Code
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(job:fault_code4) & ','     !Defect Code

    count_parts# = 0

    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        count_parts# += 1
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(wpr:part_number) & ','
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & Clip(wpr:quantity) & ','
        If count_parts# => 5
            Break
        End!If count_parts# => 5
    end !loop

    count_parts# += 1
    Loop x# = count_parts# To 5
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & ','
        Mitsubishi:line1   = Clip(Mitsubishi:line1) & ','
    End!Loop x# = count_parts# To 5

    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(job:labour_cost_warranty,@n10.2) & ','
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(job:parts_cost_warranty,@n10.2) & ','
    Mitsubishi:line1   = Clip(Mitsubishi:line1) & Format(job:sub_total_warranty,@n10.2) & ','
    Append(FileMitsubishi)
    Clear(Mitsubishi:Record)
local.MotorolaExport        Procedure(Byte f:FirstSecondYear)
FileMotorola FILE,DRIVER('ASCII'),PRE(NOUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
NewOutGroup     GROUP
Line1           STRING(3000)
          . . .

HeaderLine1 GROUP,OVER(nouf:NewOutGroup),PRE(H1)
RecordIdentifier        String(4)
FileType                String(6)
DateGenerated           String(16)
MASCCode                String(8)
SiteCode                String(3)
Reserved1               String(50)
            End

HeaderLine2 GROUP,OVER(nouf:NewOutGroup),PRE(H2)
RecordIdentifier        String(4)
DateSent                String(16)
PackageVersion          String(12)
DataVersion             String(12)
Reserved1               String(50)
            End

HeaderLine3 Group,Over(nouf:NewOutGroup),Pre(h3)
RecordIdentifier        String(4)
Reserved1               String(20)
Reserved2               String(50)
Reserved3               String(50)
            End

ClaimDetail Group,Over(nouf:NewOutGroup),Pre(cd)
RecordIdentifier                    STRING(3)
ConsumerTitle                       STRING(3)
ConsumerFirstName                   STRING(40)
ConsumerSurname                     STRING(40)
Address1                            STRING(50)
Address2                            STRING(50)
Address3                            STRING(50)
Address4                            STRING(50)
Address5                            STRING(50)
Address6                            STRING(40)
Address7                            STRING(40)
Address8                            STRING(10)
ConsumerPhoneNumber                 STRING(30)
ConsumerEmailAddress                STRING(100)
FieldBulletinNumber                 STRING(6)
ConsumerGender                      STRING(1)
ConsumerAgeGroup                    STRING(2)
CountryCode                         STRING(3)
CourierTrackingNumberIn             STRING(20)
CourierTrackingNumberOut            STRING(20)
WarrantyClaim                       STRING(12)
CallCentreNumber                    STRING(20)
AirtimeCarrierCode                  STRING(6)
TransactionCode                     STRING(3)
ProductCode                         STRING(4)
TransceiverCode                     STRING(15)
ModelCode                           STRING(15)
FactoryCode                         STRING(10)
MSNIn                               STRING(14)
MSNOut                              STRING(14)
IMEIIn                              STRING(18)
IMEIOut                             STRING(18)
FaultCode                           STRING(3)
ComsumerPerceivedFaultCode          STRING(3)
RepairCode                          STRING(3)
RepairStatus                        STRING(3)
PartsNotAvailable1                  STRING(20)
PartsNotAvailable2                  STRING(20)
DateReceived                        STRING(10)
TimeReceived                        STRING(5)
ExpectedShipDate                    STRING(10)
ExpectedShipTime                    STRING(5)
DateDespatched                      STRING(10)
TimeDespatched                      STRING(5)
RepairDate                          STRING(10)
RepairTime                          STRING(8)
RepairCycleTime                     STRING(8)
ConsumerCycleTime                   STRING(8)
POPWarrantyClaim                    STRING(1)
DateOfPurchase                      STRING(10)
Reserved1                          STRING(11)
Reserved2                          STRING(11)
SoftwareVersionIn                   STRING(10)
SoftwareVersionOut                  STRING(10)
GlobalCustomerComplaintCode         STRING(8)
Reserved3                           STRING(9)
GlobalPrimaryProblemFound           STRING(8)
GlobalSecondaryProblemFound         STRING(8)
GlobalPrimaryRepairCode             STRING(8)
GlobalSecondaryRepairCode           STRING(8)
Reserved4                           STRING(11)
Airtime                             STRING(20)
Reserved5                           STRING(10)
SpecialProjectNumber                STRING(10)
BillingAccountNumber                STRING(20)
Reserved6                           STRING(10)
Reserved7                           STRING(10)
Reserved8                           STRING(15)
AccessoryDateCode                   STRING(5)
MobileNumber                        STRING(12)
Reserved9                           STRING(10)
CustomerContactDate                 STRING(10)
CustomerContactTime                 STRING(5)
Reserved10                          STRING(23)
Reserved11                          STRING(10)
Reserved12                          STRING(2)
Reserved13                          STRING(8)
Reserved14                          STRING(10)
Reserved15                          STRING(10)
Reserved16                          STRING(10)
Reserved17                          STRING(10)
Reserved18                          STRING(10)
                        End

ComponentDetail  Group,Over(nouf:NewOutGroup),Pre(cdd)
RecordIdentifier                    STRING(3)
MASCCode                            STRING(8)
WarrantyClaimNumber                 STRING(12)
PartReplaced                        STRING(20)
QuantityReplaced                    STRING(2)
QuantityExchanged                   STRING(2)
RepairOrRefurbish                   STRING(1)
ReferenceDesignator                 STRING(6)
ReferenceDesignatorNumber           STRING(6)
PartFailureCode                     STRING(5)
Reserved1                           STRING(5)
Reserved2                           STRING(28)
Reserved3                           STRING(50)
Reserved4                           STRING(50)
Reserved5                           STRING(20)
Reserved6                           STRING(10)
                End

TrailerRecord   Group,Over(nouf:NewOutGroup),Pre(tr)
RecordIdentifier        String(3)
NumberOfClaims          String(20)
NumberOfCompenentsLines String(20)
Reserved1               String(20)
                End

tmp:TotalClaims       Long()
tmp:TotalLines        Long()
tmp:CountRecords      Long()
tmp:Loop        Byte(0)
!tmp:CurrentRow        Long()
tmp:ExcelFileName      String(255),STATIC

locBasicFilename        String(255)
locDate Date()
locTime Time()
Code
    Do GetEDIPath

    locBasicFilename = 'SIFT.xls'

    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' Technical 2y.xls'
    End ! If f:FirstSecondYear

    tmp:ExcelFilename = Clip(man:EDI_Path) & locBasicFilename

    f:FileList = Clip(f:FileList) & '|' & locBasicFilename

    If E1.Init(0,0) = 0
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If E1.Init(0,0) = 0

    Remove(tmp:ExcelFilename)

    E1.NewWorkBook()
    tmp:CurrentRow = 1

    Do NewHeaderExcel

    tmp:CurrentRow = 4

    Loop tmp:Loop = 1 To 2
        Case tmp:Loop
        Of 1
            locBasicFileName = 'SIFT1'

            If f:FirstSecondYear
                locBasicFileName = Clip(f:Manufacturer) & ' 2y1'
            End !If func:SecondYear

        Of 2
            locBasicFileName = 'SIFT2'

            If f:FirstSecondYear
                locBasicFileName = Clip(f:Manufacturer) & ' 2y2'
            End !If func:SecondYear
        End ! Case tmp:Loop

        Filename = Clip(man:EDI_Path) & locBasicFileName

        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
        !Set FileName

        Remove(FileMotorola)
        Create(FileMotorola)
        Open(FileMotorola)
        If Error()
            If f:Silent = 0
                Case Missive('Unable to create EDI File.'&|
                  '<13,10>'&|
                  '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If f:Silent = 0
            tmp:Return = 1
            Return
        End ! If Error()

        Do NewHeader

        If tmp:Loop = 1
            Do Prog:ProgressSetup
            Prog:TotalRecords = tmp:RecordsCount
            Prog:ShowPercentage = 1 !Show Percentage Figure

 
            Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
            jow:Status = 'NO'
            jow:Manufacturer = f:Manufacturer
            jow:FirstSecondYear = f:FirstSecondYear
            jow:ClaimSubmitted = f:StartDate
            Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

            Accept
                Case Event()
                    Of Event:Timer
                        Loop 25 Times
                            !Inside Loop
                            If Access:JOBSWARR.NEXT()
                                Prog:Exit = 1
                                Break
                            End !If
                            If jow:Status <> 'NO'   |
                            Or jow:Manufacturer <> f:Manufacturer   |
                            Or jow:FirstSecondYear <> f:FirstSecondYear |
                            Or jow:ClaimSubmitted > f:EndDate
                                Prog:Exit = 1
                                Break
                             End ! If jow:ClaimSubmitted > f:EndDate

                            Access:JOBS.ClearKey(job:Ref_Number_Key)
                            job:Ref_Number = jow:RefNumber
                            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                !Found
                            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                !Error
                                Cycle
                            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                            Do NewExport
                            Do UpdateJOBSE

                            Prog:RecordCount += 1

                            ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                            Do Prog:UpdateScreen
                        End ! Loop 25 Times
                    Of Event:CloseWindow
            !            Prog:Exit = 1
            !            Prog:Cancelled = 1
            !            Break
                    Of Event:Accepted
                        If Field() = ?Prog:Cancel
                            Beep(Beep:SystemQuestion)  ;  Yield()
                            Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                           icon:Question,'&Yes|&No',2,2)
                                Of 1 ! &Yes Button
                                    tmp:Return = 1
                                    Prog:Exit = 1
                                    Prog:Cancelled = 1
                                    Break
                                Of 2 ! &No Button
                            End!Case Message
                        End ! If Field() = ?ProgressCancel
                End ! Case Event()
                If Prog:Exit
                    Break
                End ! If Prog:Exit
            End ! Accept
            Do Prog:ProgressFinished

        End ! If tmp:Loop = 1
        If tmp:Loop = 2
            !Ok now do accessory.
            Do Prog:ProgressSetup
            Prog:TotalRecords = tmp:RecordsCount
            Prog:ShowPercentage = 1 !Show Percentage Figure

            Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
            jow:Status = 'NO'
            jow:Manufacturer = f:Manufacturer
            jow:FirstSecondYear = f:FirstSecondYear
            jow:ClaimSubmitted = f:StartDate
            Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

            Accept
                Case Event()
                    Of Event:Timer
                        Loop 25 Times
                            !Inside Loop
                            If Access:JOBSWARR.NEXT()
                                Prog:Exit = 1
                                Break
                            End !If
                            If jow:Status <> 'NO'   |
                            Or jow:Manufacturer <> f:Manufacturer   |
                            Or jow:FirstSecondYear <> f:FirstSecondYear |
                            Or jow:ClaimSubmitted > f:EndDate
                                Prog:Exit = 1
                                Break
                            End ! If jow:ClaimSubmitted > f:EndDate

                            Access:JOBS.ClearKey(job:Ref_Number_Key)
                            job:Ref_Number = jow:RefNumber
                            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                !Found
                            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                !Error
                                Cycle
                            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                            wpr:Ref_Number  = job:Ref_Number
                            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                            Loop
                                If Access:WARPARTS.NEXT()
                                   Break
                                End !If
                                If wpr:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                sto:Ref_Number  = wpr:Part_Ref_Number
                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Found
                                    If sto:Accessory = 'YES'
                                        Do NewExport
                                        ! Inserting (DBH 05/01/2006) #6645 - There should only be one accessory per job
                                        Break
                                        ! End (DBH 05/01/2006) #6645
                                    End !If sto:Accessory = 'YES'
                                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            End !Loop

                            Do UpdateJOBSE
                            Prog:RecordCount += 1

                            ?Prog:UserString{Prop:Text} = 'Accessory Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                            Do Prog:UpdateScreen
                        End ! Loop 25 Times
                    Of Event:CloseWindow
            !            Prog:Exit = 1
            !            Prog:Cancelled = 1
            !            Break
                    Of Event:Accepted
                        If Field() = ?Prog:Cancel
                            Beep(Beep:SystemQuestion)  ;  Yield()
                            Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                           icon:Question,'&Yes|&No',2,2)
                                Of 1 ! &Yes Button
                                    Prog:Exit = 1
                                    Prog:Cancelled = 1
                                    Break
                                Of 2 ! &No Button
                            End!Case Message
                        End ! If Field() = ?ProgressCancel
                End ! Case Event()
                If Prog:Exit
                    Break
                End ! If Prog:Exit
            End ! Accept
            Do Prog:ProgressFinished
        End ! If tmp:Loop = 2

        Do NewTrailer

! Deleting (DBH 29/04/2008) # 9806 - Not Needed
!        If tmp:Loop = 2
!            Case Missive('Created EDI Batch File ' & Clip(f:BatchNumber) & '.'&|
!              '<13,10>'&|
!              '<13,10>File: ' & Clip(filename) & '.','ServiceBase 3g',|
!                           'midea.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
!        End !If tmp:Loop = 2
! End (DBH 29/04/2008) #9806!
        Close(FileMotorola)

    End ! Loop tmp:Loop = 1 To 2

    Do NewTrailerExcel

    E1.SetColumnWidth('A','BS')
    E1.SelectCells('A1')

    E1.SaveAs(tmp:ExcelFileName)
    E1.CloseWorkBook(2)
    E1.Kill()
    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(tmp:ExcelFileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive

    End ! If f:Silent = 0



NewExport       Routine
    ! Inserting (DBH 26/04/2006) #6645 - If only accessory, don't show on Sift1, otherwise parts on Sift1, accessories on Sift 2
    If tmp:Loop = 1
        FoundPart# = False
        FoundAccessory# = False

        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop ! Begin WARPARTS Loop
            If Access:WARPARTS.Next()
                Break
            End ! If !Access
            If wpr:Ref_Number <> job:Ref_Number
                Break
            End ! If
            If man:IncludeAdjustment <> 'YES' and wpr:Part_Number = 'ADJUSTMENT'
                Cycle
            End ! If man:IncludeAdjustment <> 'YES' and wpr:Part_Number = 'ADJUSTMENT'
            If wpr:Part_Number = 'EXCH'
                Cycle
            End ! If wpr:Part_Number = 'EXCH'
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = wpr:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                ! Found
                If job:Date_Completed <> ''
                    If stock:Accessory = 'YES'
                        FoundAccessory# = True
                        Cycle
                    End ! If job:Date_Completed <> ''
                End ! If job:Date_Completed <> ''
                FoundPart# = True
                Cycle
            Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                ! Error
            End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        End ! End WARPARTS Loop

        If FoundPart# = False And FoundAccessory# = True
            Exit
        End ! If FoundPart# = False
    End ! If tmp:Loop = 1
    ! End (DBH 26/04/2006) #6645

    Clear(nouf:Record)
    cd:RecordIdentifier = 'CLM'
    cd:ConsumerTitle    = job:Title
    cd:ConsumerFirstName   = job:Initial
    cd:ConsumerSurname  = job:Surname
    cd:Address1         = job:Address_Line1
    cd:Address2         = job:Address_Line2
    cd:Address3         = job:Address_Line3
    cd:Address4         = ''
    cd:Address5         = ''
    cd:Address6         = ''
    cd:Address7         = job:Postcode
    cd:Address8         = ''
    cd:ConsumerPhoneNumber  = job:Telephone_Number
    cd:ConsumerEmailAddress = ''
    cd:FieldBulletinNumber  = ''
    cd:ConsumerGender   = ''
    cd:ConsumerAgeGroup = ''
    cd:CountryCode      = edi:Country_Code
    cd:CourierTrackingNumberIn  = job:Incoming_Consignment_Number
    cd:CourierTrackingNumberOut = job:Consignment_Number
    ! Inserting (DBH 06/04/2006) #7478 - Append "SY" for second year claims
    If f:FirstSecondYear
        Case tmp:Loop
            Of 1 !Normal Job
                cd:WarrantyClaim    = Clip(job:Ref_Number) & 'SY'
            Of 2 !Accessory
                cd:WarrantyClaim    = Clip(job:Ref_Number) & 'SY/1'
        End !Case tmp:Loop
    Else ! If func:SecondYear
    ! End (DBH 06/04/2006) #7478
        Case tmp:Loop
            Of 1 !Normal Job
                cd:WarrantyClaim    = job:Ref_Number
            Of 2 !Accessory
                cd:WarrantyClaim    = Clip(job:Ref_Number) & '/1'
        End !Case tmp:Loop
    End ! If func:SecondYear
    cd:CallCentreNumber = ''
    cd:AirtimeCarrierCode   = ''
! Changing (DBH 12/04/2006) #6645 - Now use a fault code if not "Y"
!     If job:Exchange_Unit_Number <> ''
!         cd:TransactionCode  = 'PR'
!     Else !If job:Exchange_Unit_Number <> ''
!         cd:TransactionCode  = 'RE'
!     End !If job:Exchange_Unit_Number <> ''
! to (DBH 12/04/2006) #6645
    If job:Fault_Code4 = 'Y'
        If job:Exchange_Unit_Number <> ''
            cd:TransactionCode  = 'PR'
        Else !If job:Exchange_Unit_Number <> ''
            cd:TransactionCode  = 'RE'
        End !If job:Exchange_Unit_Number <> ''
    Else ! If job:Fault_Code4 = 'Y'
        cd:TransactionCode = job:Fault_Code4
    End ! If job:Fault_Code4 = 'Y'
    ! End (DBH 12/04/2006) #6645
    Case tmp:Loop
        Of 1 !Normal Job
            cd:ProductCode      = job:Fault_Code1
        Of 2 !Accesory
            cd:ProductCode      = wpr:Fault_Code1
    End !Case tmp:Loop
    cd:TransceiverCode  = ''
    cd:ModelCode        = ''
    cd:FactoryCode      = ''

    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        cd:MSNIn   = job:MSN
        cd:IMEIIn   = job:ESN
    Else !IMEIError# = 1
        cd:IMEIIn   = jot:OriginalIMEI
        cd:MSNIn   =  jot:OriginalMSN
    End !IMEIError# = 1

    !Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
    ExchangeAttached# = False
    If job:Exchange_unit_Number <> 0
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        !RRC exchanges for normal jobs appear on the report
        !48 Hour RRC exchanges do not
        !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
        If jobe:Engineer48HourOption = 1
            If jobe:ExchangedATRRC = True
                If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = jobe:SecondExchangeNumber
                Else !If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = 0
                End !If jobe:SecondExchangeNumber <> 0
            End !If jobe:ExchangeATRRC = True
        Else !jobe:Exchange48HourOption = 1
            ExchangeNumber# = job:Exchange_Unit_Number
        End !jobe:Exchange48HourOption = 1

        If ExchangeNumber# <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = ExchangeNumber#
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                ExchangeAttached# = True
                cd:MSNOut  = xch:MSN
                cd:IMEIOut  = xch:ESN
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        End !If ShowExchange# = True
    End !job:Exchange_unit_Number <> 0

    If ExchangeAttached# = False
        cd:MSNOut  = job:MSN
        cd:IMEIOut  = job:ESN
    End !If ExchangeAttached# = False

    cd:FaultCode            = ''!job:Fault_Code2
    cd:ComsumerPerceivedFaultCode   = ''!job:Fault_Code2

    Case tmp:Loop
        Of 1
            cd:RepairCode                   = ''!job:Fault_Code3
        Of 2
            cd:RepairCode                   = ''!wpr:Fault_Code2
    End !Case tmp:Loop

    If job:Date_Completed <> ''
        cd:RepairStatus = 'DES'
    Else !If job:Date_Completed <> ''
        If job:Cancelled = 'YES'
            cd:RepairStatus = 'CAN'
        Else !If job:Cancelled = 'YES'
            If CheckParts('W')
                cd:RepairStatus         = 'AWP'
            Else !If CheckParts('W')
                If job:Engineer <> ''
                    cd:RepairStatus     = 'INR'
                Else !If job:Engineer <> ''
                    cd:RepairStatus     = 'AWR'
                End !If job:Engineer <> ''
            End !If CheckParts('W')
        End !If job:Cancelled = 'YES'
    End !If job:Date_Completed <> ''

    cd:PartsNotAvailable1   = ''
    cd:PartsNotAvailable2   = ''
!    cd:DateReceived         = Format(job:Date_Booked,@d06)
!    cd:TimeReceived         = format(job:Time_Booked,@t01)
    locDate = job:Date_Booked
    locTime = job:Time_Booked
    GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)
    cd:DateReceived         = Format(locDate,@d06)
    cd:TimeReceived         = format(locTime,@t01)
    cd:ExpectedShipDate     = ''
    cd:ExpectedShipTime     = ''
!    cd:DateDespatched       = Format(job:Date_Completed,@d06)
!    cd:TimeDespatched       = Format(job:Time_Completed,@t01)

    !012656 Completed date on Tech reps, complex sorting
    Do GetDateCompleted
    LocDate = DateCompleted
    LocTime = TimeCompleted
    !Replaces the following
    !locDate = job:Date_Completed
    !locTime = job:Time_Completed
    IF (job:Exchange_Unit_Number > 0)
        GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
    END !IF (job:Exchange_Unit_Number > 0)
    cd:DateDespatched       = Format(locDate,@d06)
    cd:TimeDespatched       = Format(locTime,@t01)
    cd:RepairDate           = Format(job:Date_Completed,@d06)
    count# = 0
    loop
        count# += 1
        date$   = job:date_booked + count#
        If def:include_saturday <> 'YES'
            If date$    % 7 = 6
                Cycle
            End!If date$    % 7 - 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If date$ % 7 = 0
                Cycle
            End!If date$ % 7 = 0
        End!If def:include_sunday <> 'YES'
        If date$ >= job:date_completed
            Break
        End!If date$ >= job:date_completed
    End!loop
!    If job:time_completed > job:time_booked
!        cd:RepairTime      = Format(count#,@n02) & ':' & Format(job:time_completed - job:time_booked,@t01) !Leadtime
!    End!If job:time_completed > job:time_booked
!    If job:time_completed < job:time_booked
!        time$   = def:end_work_hours - job:time_booked + job:time_completed - def:start_work_hours
!        cd:RepairTime      = Format(count#-1,@n02) & ':' & Format(time$,@t1)                              !Leadtime
!    End!If job:time_completed < job:time_booked
!    If job:time_completed = job:time_booked
    if (count# > 100)
        count# = 100
    end ! if (count# > 99)
     cd:RepairTime      = Format(count#-1,@n02) & ':00:00'                                             !Leadtime
!    End!If job:time_completed - job:time_booked

    ! Insert --- Forgot to reset count (DBH: 22/05/2009) #10721
    count# = 0
    ! end --- (DBH: 22/05/2009) #10721
    loop time# = job:Date_Booked to Today()
        count# += 1
        date$   = job:date_booked + count#
        If def:include_saturday <> 'YES'
            If date$    % 7 = 6
                Cycle
            End!If date$    % 7 - 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If date$ % 7 = 0
                Cycle
            End!If date$ % 7 = 0
        End!If def:include_sunday <> 'YES'
        If job:Exchange_Unit_Number
            !Job has been exchanged
            If job:Exchange_Despatched = ''
                !If job has not been despatched
                ! Change --- Keep counting, if not despatched. (DBH: 22/05/2009) #10721
                !Count# = 1
                !Break
                ! To --- (DBH: 22/05/2009) #10721
                cycle
                ! end --- (DBH: 22/05/2009) #10721
            Else !If job:Exchange_Despatched = ''
                If date$ >= job:Exchange_Despatched
                    Break
                End!If date$ >= job:date_completed
            End !If job:Exchange_Despatched = ''
        Else !If job:Exchange_Unit_Number
! Insert --- If it's an RRC job, use it's despatch date (DBH: 10/03/2009) #10721
            if (jobe:WebJob)
                if (wob:DateJobDespatched = '')
                    ! Change --- Keep counting, if not despatched. (DBH: 22/05/2009) #10721
                    !Count# = 1
                    !Break
                    ! To --- (DBH: 22/05/2009) #10721
                    cycle
                    ! end --- (DBH: 22/05/2009) #10721
                else ! if (wob:DateJobDespatched = '')
                    if (date$ >= wob:DateJobDespatched)
                        break
                    end ! if (date$ >= wob:DateJobDespatched)
                end ! if (wob:DateJobDespatched = '')
            else ! if (jobe:WebJob)
! end --- (DBH: 10/03/2009) #10721
                If job:Date_Despatched = ''
                    !If job has not been despatched
                    ! Change --- Keep counting, if not despatched. (DBH: 22/05/2009) #10721
                    !Count# = 1
                    !Break
                    ! To --- (DBH: 22/05/2009) #10721
                    cycle
                    ! end --- (DBH: 22/05/2009) #10721
                Else !If job:Date_Despatched = ''
                    If date$ >= job:Date_Despatched
                        Break
                    End!If date$ >= job:date_completed

                End !If job:Date_Despatched = ''

            end ! if (jobe:WebJob)
        End !If job:Exchange_Unit_Number
    End!loop

!    If job:time_completed > job:time_booked
!        cd:RepairTime      = Format(count#,@n02) & ':' & Format(job:time_completed - job:time_booked,@t01) !Leadtime
!    End!If job:time_completed > job:time_booked
!    If job:time_completed < job:time_booked
!        time$   = def:end_work_hours - job:time_booked + job:time_completed - def:start_work_hours
!        cd:RepairTime      = Format(count#-1,@n02) & ':' & Format(time$,@t1)                              !Leadtime
!    End!If job:time_completed < job:time_booked
!    If job:time_completed = job:time_booked
    if (count# > 100)
        count# = 100
    end ! if (count# > 99)
    cd:RepairCycleTime      = Format(count#-1,@n02) & ':00:00'                                             !Leadtime
!    End!If job:time_completed - job:time_booked

    cd:ConsumerCycleTime    = ''
    If job:DOP <> ''
        cd:POPWarrantyClaim = 'Y'
    Else !If job:DOP <> ''
        cd:POPWarrantyClaim = 'N'
    End !If job:DOP <> ''
    cd:DateOfPurchase   = Format(job:DOP,@d06)

    cd:Reserved1 = ''
    cd:Reserved2 = ''
    cd:SoftwareVersionIn    = job:Fault_Code6
    cd:SoftwareVersionOut   = job:Fault_Code7
    cd:GlobalCustomerComplaintCode  = job:Fault_Code5
    cd:Reserved3 = ''
    cd:GlobalPrimaryProblemFound    = job:Fault_Code2
    ! Changing (DBH 04/01/2006) #6645 - No longer needed
    ! cd:GlobalSecondaryProblemFound  = job:Fault_Code9
    ! cd:GlobalSecondaryRepairCode    = job:Fault_Code10
    ! to (DBH 04/01/2006) #6645
    cd:GlobalSecondaryProblemFound  = ''
    cd:GlobalSecondaryRepairCode    = ''
    ! End (DBH 04/01/2006) #6645
    ! Changing (DBH 05/01/2006) #6645 - Show the part fault code for the accessory line
    ! cd:GlobalPrimaryRepairCode      = job:Fault_Code3
    ! to (DBH 05/01/2006) #6645
    Case tmp:Loop
    Of 1 ! Normal
        cd:GlobalPrimaryRepairCode      = job:Fault_Code3
    Of 2 ! Accessory
        cd:GlobalPrimaryRepairCode      = wpr:Fault_Code2
    End ! Case tmp:Loop
    ! End (DBH 05/01/2006) #6645

    cd:Reserved4 = ''
    cd:Airtime  = job:Fault_Code8
    cd:Reserved5 = ''
    cd:SpecialProjectNumber = ''
    cd:Reserved6 = ''
    cd:Reserved7 = ''
    cd:Reserved8 = ''
    ! Changing (DBH 05/01/2006) #6645 - Don't show if N/A
    ! cd:AccessoryDateCode    = FORMAT(job:Fault_Code11, @d13b)
    ! to (DBH 05/01/2006) # 6645
    If job:Fault_code11 = 'N/A'
        cd:AccessoryDateCode = ''
    Else ! If job:Fault_code11 = ''
        cd:AccessoryDateCode    = Clip(job:Fault_Code11)
    End ! If job:Fault_code11 = ''
    ! End (DBH 05/01/2006) # 6645
    If job:Mobile_Number = ''
        cd:MobileNumber = 'NA'
    Else !If job:Mobile_Number = ''
        cd:MobileNumber = job:Mobile_Number
    End !If job:Mobile_Number = ''
    cd:Reserved9 = ''
    cd:CustomerContactDate  = FORMAT(job:Date_Booked, @d06b)
    cd:CustomerContactTime  = FORMAT(job:Time_Booked, @t01)
    cd:Reserved10 = ''
    cd:Reserved11 = ''
    cd:Reserved12 = ''
    cd:Reserved13 = ''
    cd:Reserved14 = ''
    cd:Reserved15 = ''
    cd:Reserved16 = ''
    cd:Reserved17 = ''
    cd:Reserved18 = ''

    Add(FileMotorola)

    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,'A' & tmp:CurrentRow,'BT' & tmp:CurrentRow)

    E1.WriteToCell('CLM'    ,'A' & tmp:CurrentRow)
    E1.WriteToCell(Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)  ,'D' & tmp:CurrentRow)
    E1.WriteToCell(edi:Country_Code ,'R' & tmp:CurrentRow)
    E1.WriteToCell(job:Incoming_Consignment_Number   ,'S' & tmp:CurrentRow)
    E1.WriteToCell(job:Consignment_Number   ,'T' & tmp:CurrentRow)
    E1.WriteToCell(job:Ref_Number   ,'U' & tmp:CurrentRow)

    If job:Fault_Code4 = 'X'
        If job:Exchange_Unit_Number <> ''
            E1.WriteToCell('PR' ,'X' & tmp:CurrentRow)
        Else !If job:Exchange_Unit_Number <> ''
            E1.WriteToCell('RE' ,'X' & tmp:CurrentRow)
        End !If job:Exchange_Unit_Number <> ''
    Else ! If job:Fault_Code4 = 'Y'
        E1.WriteToCell(job:Fault_Code4  ,'X' & tmp:CurrentRow)
    End ! If job:Fault_Code4 = 'Y'

    Case tmp:Loop
        Of 1 !Normal Job
            E1.WriteToCell(job:Fault_Code1  ,'Y' & tmp:CurrentRow)
        Of 2 !Accesory
            E1.WriteToCell(wpr:Fault_Code1  ,'Y' & tmp:CurrentRow)
    End !Case tmp:Loop

    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        E1.WriteToCell(job:MSN  ,'AC' & tmp:CurrentRow)
        E1.WriteToCell(job:ESN  ,'AE' & tmp:CurrentRow)
    Else !IMEIError# = 1
        E1.WriteToCell(jot:OriginalMSN  ,'AC' & tmp:CurrentRow)
        E1.WriteToCell(jot:OriginalIMEI  ,'AE' & tmp:CurrentRow)
    End !IMEIError# = 1

    !Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
    ExchangeAttached# = False
    If job:Exchange_unit_Number <> 0
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        !RRC exchanges for normal jobs appear on the report
        !48 Hour RRC exchanges do not
        !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
        If jobe:Engineer48HourOption = 1
            If jobe:ExchangedATRRC = True
                If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = jobe:SecondExchangeNumber
                Else !If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = 0
                End !If jobe:SecondExchangeNumber <> 0
            End !If jobe:ExchangeATRRC = True
        Else !jobe:Exchange48HourOption = 1
            ExchangeNumber# = job:Exchange_Unit_Number
        End !jobe:Exchange48HourOption = 1

        If ExchangeNumber# <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = ExchangeNumber#
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                ExchangeAttached# = True
                E1.WriteToCell(xch:MSN  ,'AD' & tmp:CurrentRow)
                E1.WriteToCell(xch:ESN  ,'AF' & tmp:CurrentRow)
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        End !If ShowExchange# = True
    End !job:Exchange_unit_Number <> 0

    If ExchangeAttached# = False
        E1.WriteToCell(job:MSN  ,'AD' & tmp:CurrentRow)
        E1.WriteToCell(job:ESN  ,'AF' & tmp:CurrentRow)
    End !If ExchangeAttached# = False

    If job:Date_Completed <> ''
        E1.WriteToCell('DES'    ,'AJ' & tmp:CurrentRow)
    Else !If job:Date_Completed <> ''
        If job:Cancelled = 'YES'
            E1.WriteToCell('CAN'    ,'AJ' & tmp:CurrentRow)
        Else !If job:Cancelled = 'YES'
            If CheckParts('W')
                E1.WriteToCell('AWP'    ,'AJ' & tmp:CurrentRow)
            Else !If CheckParts('W')
                If job:Engineer <> ''
                    E1.WriteToCell('INR'    ,'AJ' & tmp:CurrentRow)
                Else !If job:Engineer <> ''
                    E1.WriteToCell('AWR'    ,'AJ' & tmp:CurrentRow)
                End !If job:Engineer <> ''
            End !If CheckParts('W')
        End !If job:Cancelled = 'YES'
    End !If job:Date_Completed <> ''

!    E1.WriteToCell(Format(job:Date_Booked,@d06) ,'AM' & tmp:CurrentRow)
!    E1.WriteToCell(Format(job:Time_Booked,@t01) ,'AN' & tmp:CurrentRow)
    locDate = job:Date_Booked
    locTime = job:Time_Booked
    GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)
    E1.WriteToCell(Format(locDate,@d06) ,'AM' & tmp:CurrentRow)
    E1.WriteToCell(Format(locTime,@t01) ,'AN' & tmp:CurrentRow)

!    E1.WriteToCell(Format(job:Date_Completed,@d06)  ,'AQ' & tmp:CurrentRow)
!    E1.WriteToCell(Format(job:Time_Completed,@t01)  ,'AR' & tmp:CurrentRow)


    !012656 Completed date on Tech reps, complex sorting
    Do GetDateCompleted
    LocDate = DateCompleted
    LocTime = TimeCompleted
    !Replaces the following
    !locDate = job:Date_Completed
    !locTime = job:Time_Completed

    IF (job:Exchange_Unit_Number > 0)
        GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
    END !IF (job:Exchange_Unit_Number > 0)

    E1.WriteToCell(Format(locDate,@d06)  ,'AQ' & tmp:CurrentRow)
    E1.WriteToCell(Format(locTime,@t01)  ,'AR' & tmp:CurrentRow)
    E1.WriteToCell(Format(job:Date_Completed,@d06)  ,'AS' & tmp:CurrentRow)

    count# = 0
    loop
        count# += 1
        date$   = job:date_booked + count#
        If def:include_saturday <> 'YES'
            If date$    % 7 = 6
                Cycle
            End!If date$    % 7 - 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If date$ % 7 = 0
                Cycle
            End!If date$ % 7 = 0
        End!If def:include_sunday <> 'YES'
        If date$ >= job:date_completed
            Break
        End!If date$ >= job:date_completed
    End!loop
    if (count# > 100)
        count# = 100
    end ! if (count# > 99)
    E1.WriteToCell(Format(Count#-1,@n02) & ':00:00' ,'AT' & tmp:CurrentRow)

    ! Insert --- Forgot to reset count (DBH: 22/05/2009) #10721
    count# = 0
    ! end --- (DBH: 22/05/2009) #10721
    loop times# = job:Date_Booked To Today()
        count# += 1
        date$   = job:date_booked + count#
        If def:include_saturday <> 'YES'
            If date$    % 7 = 6
                Cycle
            End!If date$    % 7 - 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If date$ % 7 = 0
                Cycle
            End!If date$ % 7 = 0
        End!If def:include_sunday <> 'YES'
        If job:Exchange_Unit_Number
            !Job has been exchanged
            If job:Exchange_Despatched = ''
                !If job has not been despatched
                ! Change --- Keep counting, if not despatched. (DBH: 22/05/2009) #10721
                !Count# = 1
                !Break
                ! To --- (DBH: 22/05/2009) #10721
                cycle
                ! end --- (DBH: 22/05/2009) #10721
            Else !If job:Exchange_Despatched = ''
                If date$ >= job:Exchange_Despatched
                    Break
                End!If date$ >= job:date_completed
            End !If job:Exchange_Despatched = ''
        Else !If job:Exchange_Unit_Number
! Insert --- If it's an RRC job, use it's despatch date (DBH: 10/03/2009) #10721
            if (jobe:WebJob)
                if (wob:DateJobDespatched = '')
                    ! Change --- Keep counting, if not despatched. (DBH: 22/05/2009) #10721
                    !Count# = 1
                    !Break
                    ! To --- (DBH: 22/05/2009) #10721
                    cycle
                    ! end --- (DBH: 22/05/2009) #10721
                else ! if (wob:DateJobDespatched = '')
                    if (date$ >= wob:DateJobDespatched)
                        break
                    end ! if (date$ >= wob:DateJobDespatched)
                end ! if (wob:DateJobDespatched = '')
            else ! if (jobe:WebJob)
! end --- (DBH: 10/03/2009) #10721
                If job:Date_Despatched = ''
                    !If job has not been despatched
                    ! Change --- Keep counting, if not despatched. (DBH: 22/05/2009) #10721
                    !Count# = 1
                    !Break
                    ! To --- (DBH: 22/05/2009) #10721
                    cycle
                    ! end --- (DBH: 22/05/2009) #10721
                Else !If job:Date_Despatched = ''
                    If date$ >= job:Date_Despatched
                        Break
                    End!If date$ >= job:date_completed

                End !If job:Date_Despatched = ''
            end ! if (jobe:WebJob)
        End !If job:Exchange_Unit_Number
    End!loop
    if (count# > 100)
        count# = 100
    end ! if (count# > 99)

    E1.WriteToCell(Format(count#-1,@n02) & ':00:00' ,'AU' & tmp:CurrentRow)

    If job:DOP <> ''
        E1.WriteToCell('Y'  ,'AW' & tmp:CurrentRow)
    Else ! If job:DOP <> ''
        E1.WriteToCell('N'  ,'AW' & tmp:CurrentRow)
    End ! If job:DOP <> ''

    E1.WriteToCell(Format(job:DOP,@d06) ,'AX' & tmp:CurrentRow)

    E1.WriteToCell(job:Fault_Code6  ,'BA' & tmp:CurrentRow)
    E1.WriteToCell(job:Fault_Code7  ,'BB' & tmp:CurrentRow)
    E1.WriteToCell(job:Fault_Code5  ,'BC' & tmp:CurrentRow)
    E1.WriteToCell(job:Engineer ,'BD' & tmp:CurrentRow)
    E1.WriteToCell(job:Fault_Code2  ,'BE' & tmp:CurrentRow)

    Case tmp:Loop
    Of 1 ! Normal
        E1.WriteToCell(job:Fault_Code3  ,'BG' & tmp:CurrentRow)
    Of 2 ! Accessory
        E1.WriteToCell(wpr:Fault_Code2 ,'BG' & tmp:CurrentRow)
    End ! Case tmp:Loop
    E1.WriteToCell(job:Fault_Code8  ,'BJ' & tmp:CurrentRow)
    If job:Fault_code11 = 'N/A'

    Else ! If job:Fault_code11 = ''
        E1.WriteToCell(job:Fault_Code11 ,'BQ' & tmp:CurrentRow)
    End ! If job:Fault_code11 = ''
    ! End (DBH 05/01/2006) # 6645
    If job:Mobile_Number = ''
        E1.WriteToCell('NA' ,'BR' & tmp:CurrentRow)
    Else !If job:Mobile_Number = ''
        E1.WriteToCell(job:Mobile_Number    ,'BR' & tmp:CurrentRow)
    End !If job:Mobile_Number = ''

    tmp:CurrentRow += 1

    tmp:TotalClaims += 1

    Case tmp:Loop
        Of 1 !Normal
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                    Cycle
                End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                IF Wpr:part_number = 'EXCH'
                  CYCLE
                END

                Case tmp:Loop
                    Of 1 !No Accessories
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = wpr:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            !Only exclude accessories on the complete jobs - L935 (DBH: 02-09-2003)
                            If job:Date_Completed <> ''
                                If sto:Accessory = 'YES'
                                    Cycle
                                End !If sto:Accessory = 'YES'
                            End !If job:Date_Completed <> ''

                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Of 2 !Accessories Only
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = wpr:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Accessory <> 'YES'
                                Cycle
                            End !If sto:Accessory = 'YES'

                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


                End !Case tmp:Loop

                Clear(nouf:Record)
                cdd:RecordIdentifier    = 'CMP'
                cdd:MASCCode            = man:EDI_Account_Number
                ! Inserting (DBH 06/04/2006) #7478 - Append "SY" for second year claims
                If f:FirstSecondYear
                    cdd:WarrantyClaimNumber = Clip(job:Ref_Number) & 'SY'
                Else ! If func:SecondYear
                ! End (DBH 06/04/2006) #7478
                    cdd:WarrantyClaimNumber = job:Ref_Number
                End ! If func:SecondYear
                cdd:PartReplaced        = wpr:Part_Number
                cdd:QuantityReplaced    = wpr:Quantity
                cdd:QuantityExchanged = '0'
                cdd:RepairOrRefurbish = 'R'
                cdd:ReferenceDesignator = wpr:Fault_Code4
                cdd:ReferenceDesignatorNumber   = wpr:Fault_Code5
                cdd:PartFailureCode     = wpr:Fault_Code6
                cdd:Reserved1           = ''
                cdd:Reserved2           = ''
                cdd:Reserved3           = ''
                cdd:Reserved4           = ''
                cdd:Reserved5           = ''
                Add(FileMotorola)

                E1.WriteToCell('CMP'    ,'A' & tmp:CurrentRow)
                E1.WriteToCell(man:EDI_Account_Number   ,'B' & tmp:CurrentRow)
                If f:FirstSecondYear
                    E1.WriteToCell(job:Ref_Number & 'SY'   ,'C' & tmp:CurrentRow)
                Else ! If f:FirstSecondYear
                    E1.WriteToCell(job:Ref_Number   ,'C' & tmp:CurrentRow)
                End ! If f:FirstSecondYear

                E1.WriteToCell(wpr:Part_Number  ,'D' & tmp:CurrentRow)
                E1.WriteToCell(wpr:Quantity ,'E' & tmp:CurrentRow)
                E1.WriteToCell('0'  ,'F' & tmp:CurrentRow)
                E1.WriteToCell('R'  ,'G' & tmp:CurrentRow)
                E1.WriteToCell(wpr:Fault_Code4  ,'H' & tmp:CurrentRow)
                E1.WriteToCell(wpr:Fault_Code5  ,'I' & tmp:CurrentRow)
                E1.WriteToCell(wpr:Fault_Code6  ,'J' & tmp:CurrentRow)
                tmp:CurrentRow += 1

                tmp:TotalLines += 1
            End !Loop

        Of 2 !Accessory
            Clear(nouf:Record)
            cdd:RecordIdentifier    = 'CMP'
            cdd:MASCCode            = man:EDI_Account_Number
            ! Inserting (DBH 06/04/2006) #7478 - Append "SY" for second year claims
            If f:FirstSecondYear
                cdd:WarrantyClaimNumber = Clip(job:Ref_Number) & 'SY/1'
            Else ! If func:SecondYear
            ! End (DBH 06/04/2006) #7478
                cdd:WarrantyClaimNumber = Clip(job:Ref_Number) & '/1'
            End !If func:SecondYear
            cdd:PartReplaced        = wpr:Part_Number
            cdd:QuantityReplaced    = wpr:Quantity
            cdd:QuantityExchanged = '0'
            cdd:RepairOrRefurbish = 'R'
            cdd:ReferenceDesignator = wpr:Fault_Code4
            cdd:ReferenceDesignatorNumber   = wpr:Fault_Code5
            cdd:PartFailureCode     = wpr:Fault_Code6
            cdd:Reserved1           = ''
            cdd:Reserved2           = ''
            cdd:Reserved3           = ''
            cdd:Reserved4           = ''
            cdd:Reserved5           = ''
            Add(FileMotorola)

            E1.WriteToCell('CMP'    ,'A' & tmp:CurrentRow)
            E1.WriteToCell(man:EDI_Account_Number   ,'B' & tmp:CurrentRow)
            If f:FirstSecondYear
                E1.WriteToCell(job:Ref_Number & 'SY'   ,'C' & tmp:CurrentRow)
            Else ! If f:FirstSecondYear
                E1.WriteToCell(job:Ref_Number   ,'C' & tmp:CurrentRow)
            End ! If f:FirstSecondYear

            E1.WriteToCell(wpr:Part_Number  ,'D' & tmp:CurrentRow)
            E1.WriteToCell(wpr:Quantity ,'E' & tmp:CurrentRow)
            E1.WriteToCell('0'  ,'F' & tmp:CurrentRow)
            E1.WriteToCell('R'  ,'G' & tmp:CurrentRow)
            E1.WriteToCell(wpr:Fault_Code4  ,'H' & tmp:CurrentRow)
            E1.WriteToCell(wpr:Fault_Code5  ,'I' & tmp:CurrentRow)
            E1.WriteToCell(wpr:Fault_Code6  ,'J' & tmp:CurrentRow)

            tmp:TotalLines += 1
            tmp:CurrentRow += 1
    End !Case tmp:Loop

NewHeader       Routine
    Clear(nouf:Record)
    h1:RecordIdentifier = 'HDR'
    h1:FileType         = 'CLMCRT'
    h1:DateGenerated    = Format(Today(),@d06) & ' ' & Format(Clock(),@t01)
    h1:MASCCode         = man:EDI_Account_Number
    h1:SiteCode         = man:Account_number
    h1:Reserved1         = ''
    Add(FileMotorola)

    Clear(nouf:Record)
    h2:RecordIdentifier = 'HDR2'
    h2:DateSent         = ''
    h2:PackageVersion   = ''
    h2:DataVersion      = ''
    h2:Reserved1        = ''
    Add(FileMotorola)

    Clear(nouf:Record)
    h3:RecordIdentifier = 'HDR3'
    h3:Reserved1        = ''
    h3:Reserved2        = ''
    h3:Reserved3        = ''
    Add(FileMotorola)

NewHeaderExcel      Routine

    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,'A' & tmp:CurrentRow,'F' & tmp:CurrentRow)

    tmp:CurrentRow = 1
    E1.WriteToCell('HDR'    ,'A' & tmp:CurrentRow)
    E1.WriteToCell('CLMCRT' ,'B' & tmp:CurrentRow)
    E1.WriteToCell(Format(Today(),@d06) ,'C' & tmp:CurrentRow)
    E1.WriteToCell(Format(Clock(),@t01)    ,'D' & tmp:CurrentRow)
    E1.WriteToCell(man:EDI_Account_Number   ,'E' & tmp:CurrentRow)
! Changing (DBH 06/06/2008) # 10115 - Hardcode to 002
!    E1.WriteToCell(man:Account_Number   ,'F' & tmp:CurrentRow)
! to (DBH 06/06/2008) # 10115
    E1.WriteToCell('002'   ,'F' & tmp:CurrentRow)
! End (DBH 06/06/2008) #10115

    tmp:CurrentRow = 2
    E1.WriteToCell('HDR2'   ,'A' & tmp:CurrentRow)

    tmp:CurrentRow = 3
    E1.WriteToCell('HDR3',  'A' & tmp:CurrentRow)

NewTrailer      Routine
    Clear(nouf:Record)
    tr:RecordIdentifier = 'TRA'
    tr:NumberOfClaims   = tmp:TotalClaims
    tr:NumberOfCompenentsLines = tmp:TotalLines
    tr:Reserved1 = ''
    Add(FileMotorola)

NewTrailerExcel Routine

    E1.SetCellNumberFormat(oix:NumberFormatText,,0,,'A' & tmp:CurrentRow,'C' & tmp:CurrentRow)
    E1.WriteToCell('TRA'    ,'A' & tmp:CurrentRow)
    E1.WriteToCell(tmp:TotalClaims  ,'B' & tmp:CurrentRow)
    E1.WriteToCell(tmp:TotalLines   ,'C' & tmp:CurrentRow)


local.NECExport        Procedure(Byte f:FirstSecondYear)
Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(255)
line2           STRING(255)
line3           STRING(2)
          . . .
Out_File2 FILE,DRIVER('ASCII'),PRE(OU2),NAME(Filename2),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group2     GROUP
Line1           STRING(76)
          . . .

Out_Part GROUP,OVER(Ou2:Out_Group2),PRE(L2)
job_number       String(8)
description      String(30)
part_number      String(30)
retail_cost       String(8)
            .
!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
serial_no         STRING(18)
exch_sn           STRING(18)
eqpt_type         STRING(4)
eqpt_no           STRING(18)
damage1           STRING(4)
damage2           STRING(4)
damage3           STRING(4)
damage4           STRING(4)
damage5           STRING(4)
Activity1         STRING(4)
Activity2         STRING(4)
Activity3         STRING(4)
Activity4         STRING(4)
Activity5         STRING(4)
fault             STRING(40)
job_no            STRING(20)
date_in           STRING(8)
date_out          STRING(8)
material1         STRING(18)
material2         STRING(18)
material3         STRING(18)
material4         STRING(18)
material5         STRING(18)
qty1              STRING(13)
qty2              STRING(13)
qty3              STRING(13)
qty4              STRING(13)
qty5              STRING(13)
circ_ref1         STRING(30)
circ_ref2         STRING(30)
circ_ref3         STRING(30)
circ_ref4         STRING(30)
circ_ref5         STRING(30)
eng_surn          STRING(30)
eng_code          STRING(3)
           .
locBasicFilename    String(255)
Code
    Do GetEDIPath

    locBasicFilename = FORMAT(today(),@d11)&CLIP(MAN:EDI_Account_Number)

    
    If f:FirstSecondYear
        locBasicFileName = Clip(f:Manufacturer) & ' 2y'
    End !If func:SecondYear
    Filename = CLIP(MAN:EDI_Path)&'\'& locBasicFilename

    Filename2 = Clip(man:EDI_Path) & '\NEC.CSV'

    Remove(Out_File)
    Create(Out_File)
    Open(Out_File)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()

    Remove(Out_File2)
    Create(Out_File2)
    Open(Out_File2)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportNEC
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(Out_File)
    Close(Out_File2)

ExportNEC        Routine
      CLEAR(ouf:RECORD)
      Clear(ou2:record)
        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 0
                End !If jot:RefNumber <> job:Ref_Number
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            l1:serial_no = UPPER(stripcomma(stripreturn(job:ESN)))
        Else !IMEIError# = 1
            l1:serial_no = UPPER(stripcomma(stripreturn(JOt:OriginalIMEI)))
        End !IMEIError# = 1

      IF job:Exchange_Unit_Number <> ''

          access:exchange.clearkey(job:ref_number_key)
          XCH:Ref_Number = job:Exchange_Unit_Number
          If access:exchange.fetch(job:ref_number_key) = Level:Benign
              l1:exch_sn   = UPPER(stripcomma(stripreturn(XCH:ESN)))
          ELSE!If access:exchange.fetch(job:ref_number_key) = Level:Benign
              l1:exch_sn   = ''
          END!If access:exchange.fetch(job:ref_number_key) = Level:Benign
      ELSE
          l1:exch_sn   = ''
      END
      l1:eqpt_type   = UPPER(stripcomma(stripreturn(job:Fault_Code2)))
      IF job:Authority_Number = ''
          l1:eqpt_no   = UPPER(stripcomma(stripreturn(job:Fault_Code1)))
      ELSE
          l1:eqpt_no   = UPPER(stripcomma(stripreturn(job:Authority_Number)))
      .
      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
      jbn:RefNumber = job:Ref_Number
      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey.) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey.) = Level:Benign

      l1:fault       = UPPER(stripcomma(stripreturn(JBN:Fault_Description)))
      l1:job_no      = FORMAT(job:Ref_Number,@n_20)
      l1:date_in     = FORMAT(job:date_booked,@d12)
      l1:date_out    = FORMAT(job:Date_Completed,@d12)
      l1:eng_code    = UPPER(SUB(job:Engineer,1,1))
      access:users.clearkey(use:user_code_key)
      USE:User_Code = job:engineer
      access:users.fetch(use:user_code_key)
      l1:eng_surn    = UPPER(stripcomma(stripreturn(USE:Surname)))

      found_main# = 0
      setcursor(cursor:wait)

      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job:ref_number      |
              then break.  ! end if
          If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
              Cycle
          End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'
          If wpr:main_part = 'YES'
              l1:damage1     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
              l1:Activity1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
              l1:material1   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
              l1:qty1        = FORMAT(WPR:Quantity,@n_13)
              l1:circ_ref1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
              found_main# = 1
              Break
          End!          If wpr:main_part = 'YES'
      end !loop

      setcursor()

      spa_cnt#=0

      setcursor(cursor:wait)
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job:ref_number      |
              then break.  ! end if

          If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
              Cycle
          End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

          If found_main# = 0
              spa_cnt#+=1
               If spa_cnt# > 5
                    l2:job_number   = stripcomma(stripreturn(job:ref_number))
                    l2:description  = ',' & stripcomma(stripreturn(wpr:description))
                    l2:part_number  = ',' & stripcomma(stripreturn(wpr:part_number))
                    l2:retail_cost   = ',' & Format(wpr:sale_cost,@n10.2)
                    Append(out_file2)
               End!If spa_cnt# > 5

              CASE spa_cnt#
                OF 1
                  l1:damage1     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material1   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty1        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 2
                  l1:damage2     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material2   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty2        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
               OF 3
                  l1:damage3     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity3   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material3   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty3        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref3   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 4
                  l1:damage4     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity4   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material4   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty4        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref4   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 5
                  l1:damage5     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity5   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material5   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty5        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref5   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
               End!CASE spa_cnt#
          Else!If found_main# = 0
              If wpr:main_part = 'YES'
                Cycle
              End!If wpr:main_part = 'YES'
              spa_cnt#+=1
               If spa_cnt# > 4
                    l2:job_number   = stripcomma(stripreturn(job:ref_number))
                    l2:description  = ',' & stripcomma(stripreturn(wpr:description))
                    l2:part_number  = ',' & stripcomma(stripreturn(wpr:part_number))
                    l2:retail_cost   = ',' & Format(wpr:sale_cost,@n10.2)
                    Append(out_file2)
               End!If spa_cnt# > 5

              CASE spa_cnt#
                OF 1
                  l1:damage2     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material2   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty2        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 2
                  l1:damage3    = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity3  = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material3  = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty3       = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref3  = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 3
                  l1:damage4    = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity4  = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material4  = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty4       = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref4  = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 4
                  l1:damage5    = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity5  = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material5  = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty5       = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref5  = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
               End!CASE spa_cnt#
          End!If found_main# = 0
      end !loop
      setcursor()
      Append(out_file)

local.NokiaExport        Procedure(Byte f:FirstSecondYear)
tmp:Accessory                Byte(0)
locBasicFilename        String(255)
locDate   Date()
locTime   Time()
Code
    Do GetEDIPath

    Free(FileNameQueue)
    clear(FileNameQueue)

    Loop tmp:Accessory = 0 To 1

        If f:FirstSecondYear = 1 And tmp:Accessory = 1
            ! Don't do accessory export if secondyear (DBH: 28/02/2007)
            Break
        End ! If f:FirstSecondYear = 1 And tmp:Accessory = 1


        Do Prog:ProgressSetup
        Prog:TotalRecords = tmp:RecordsCount
        Prog:ShowPercentage = 1 !Show Percentage Figure

        Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
        jow:Status = 'NO'
        jow:Manufacturer = f:Manufacturer
        jow:FirstSecondYear = f:FirstSecondYear
        jow:ClaimSubmitted = f:StartDate
        Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

        Accept
            Case Event()
                Of Event:Timer
                    Loop 25 Times
                        !Inside Loop
                        If Access:JOBSWARR.NEXT()
                            Prog:Exit = 1
                            Break
                        End !If
                        If jow:Status <> 'NO'   |
                        Or jow:Manufacturer <> f:Manufacturer   |
                        Or jow:FirstSecondYear <> f:FirstSecondYear |
                        Or jow:ClaimSubmitted > f:EndDate
                            Prog:Exit = 1
                            Break
                        End ! If jow:ClaimSubmitted > f:EndDate

                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = jow:RefNumber
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                        Do ExportNokia
                        Do UpdateJOBSE

                        Prog:RecordCount += 1

                        Case tmp:Accessory
                        Of 0 !Normal
                            ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords
                        Of 1 !Accessory
                            ?Prog:UserString{Prop:Text} = 'Accessory Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords
                        End ! Case tmp:Accessory

                        Do Prog:UpdateScreen
                    End ! Loop 25 Times
                Of Event:CloseWindow
        !            Prog:Exit = 1
        !            Prog:Cancelled = 1
        !            Break
                Of Event:Accepted
                    If Field() = ?Prog:Cancel
                        Beep(Beep:SystemQuestion)  ;  Yield()
                        Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                       icon:Question,'&Yes|&No',2,2)
                            Of 1 ! &Yes Button
                                tmp:Return = 1
                                Prog:Exit = 1
                                Prog:Cancelled = 1
                                Break
                            Of 2 ! &No Button
                        End!Case Message
                    End ! If Field() = ?ProgressCancel
            End ! Case Event()
            If Prog:Exit
                Break
            End ! If Prog:Exit
        End ! Accept
        Do Prog:ProgressFinished


        If f:Silent = 0
            Beep(Beep:SystemAsterisk);  Yield()
            Case Missive('Technical Report Created.'&|
                '|'&|
                '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                           'midea.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0

    End ! Loop tmp:Accessory = 0 To 1

    !Changed by Paul - 07/10/2009 - log no 11082
    !loop through all the files in the filenamequeue and close the xml off
!    stop(records(FileNameQueue))
    loop x# = 1 to records(FileNameQueue)
        get(FileNameQueue,x#)
        If error() then
            break
        End !If error() then
!        stop(x# & ':' & clip(FNQ:Filename))
        Filename = clip(FNQ:Filename)

        z#=objXmlExport.FOpen(Clip(FileName), False)

        !File open - now close it off
        objXmlExport.CloseTag('SERVICECENTER')
        objXmlExport.CloseTag('REPAIRDATA')

        objXmlExport.FClose()
    End !Loop

ExportNokia        Routine
Data
local:CountParts        Byte()
local:ExchangeDate      Date()
local:ExchangeTime      Time()
local:SoftwareUpgrade   Byte(0)
local:KeyRepairNumber   Byte(0)
local:ServiceCode       String(30)
local:GenericServiceCode String(30)
Local:2ndIMEI           String(30)
Code

    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
    map:Manufacturer = job:Manufacturer
    map:KeyRepair    = 1
    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
        local:KeyRepairNumber = map:Field_Number
    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign

    Case tmp:Accessory
    Of 0 !Normal
        ! Quick check to see if there are any parts to export. If not, ignore - TrkBs: 5904 (DBH: 26-09-2005)
        local:CountParts = 0
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number      |
                Then Break.  ! End If

            ! Inserting (DBH 06/11/2007) # 9278 - Get the service code from the key repair part
            ! Check all of the service code fields, incase there is no key repair (DBH: 06/11/2007)
            local:GenericServiceCode = wpr:Fault_Code5

            Case local:KeyRepairNumber
            Of 1
                If wpr:Fault_Code1 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 2
                If wpr:Fault_Code2 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 3
                If wpr:Fault_Code3 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 4
                If wpr:Fault_Code4 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 5
                If wpr:Fault_Code5 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 6
                If wpr:Fault_Code6 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 7
                If wpr:Fault_Code7 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 8
                If wpr:Fault_Code8 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 9
                If wpr:Fault_Code9 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 10
                If wpr:Fault_Code10 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 11
                If wpr:Fault_Code11 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            Of 12
                If wpr:Fault_Code12 = 1
                    local:ServiceCode = wpr:Fault_Code5
                End ! If wpr:Fault_Code1 = 1
            End ! Case local:KeyRepairNumber
            ! End (DBH 06/11/2007) #9278

            ! Inserting (DBH 10/04/2006) #7519 - Don't check stock for "Software Upgrade" adjustments
            If wpr:Part_Number <> 'ADJUSTMENT'
            ! End (DBH 10/04/2006) #7519
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    ! Found
                    If sto:Accessory = 'YES'
                        Cycle
                    End ! If sto:Accessory = 'YES'
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    ! Error
                    Cycle
                End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            End ! If wpr:Part_Number <> 'ADJUSTMENT'
            local:CountParts = 1
            Break
        End !Loop

        If local:ServiceCode = ''
            local:ServiceCode = local:GenericServiceCode
        End ! If local:ServiceCode = ''

        If local:CountParts = 0
            Exit
        End ! if local:CountParts = 0

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Error
        End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
            !Found

        Else !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber   = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found
        Else ! If Access:JOBSE.Tryfetch(job:RefNumberKey) = Level:Benign
            ! Error
        End ! If Access:JOBSE.Tryfetch(job:RefNumberKey) = Level:Benign

        ! Inserting (DBH 01/12/2005) #6788 - Is this job a "Software Upgrade"
        Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
        joo:JobNumber   = job:Ref_Number
        joo:FaultCode   = '110'
        If Access:JOBOUTFL.Tryfetch(joo:JobNumberKey) = Level:Benign
            ! Found
            local:SoftwareUpgrade = True
        Else ! If Access:JOBOUTFL.Tryfetch(joo:JobNumberKey) = Level:Benign
            ! Error
        End ! If Access:JOBOUTFL.Tryfetch(joo:JobNumberKey) = Level:Benign
        ! End (DBH 01/12/2005) #6788

        !Changed by Paul - 07/10/2009 - log no 11082
        !need to make a new file for each service centre
        !first - look up the new ASV number
        Filename = ''
        if jow:RepairedAt = 'ARC' then
            !ARC account number

            tmpheadaccount" = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')

            Access:ASVACC.clearkey(ASV:TradeACCManufKey)
            ASV:TradeAccNo      = tmpheadaccount"
            ASV:Manufacturer    = job:Manufacturer
            If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
                !record found
                If clip(ASV:ASVCode) <> '' then
                    !construct the new file format
                    locBasicFilename = 'R' & clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & '.xml'
                    
                    tmp:asvcode = clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number)
                Else
                    !no ASV code present - so use old file format
                    locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & '.xml'
                    tmp:asvcode = Clip(man:EDI_Account_Number)
                End !If clip(ASV:ASVCode) <> '' then
            Else
                !no record exists for this Account - so use the old file format
                locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & '.xml'
                tmp:asvcode = Clip(man:EDI_Account_Number)
            End !If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then


        Else
            !RRC account number

            Access:ASVACC.clearkey(ASV:TradeACCManufKey)
            ASV:TradeAccNo      = wob:HeadAccountNumber
            ASV:Manufacturer    = job:Manufacturer
            If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
                !record found
                If clip(ASV:ASVCode) <> '' then
                    !construct the new file format
                    locBasicFilename = 'R' & clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & '.xml'
                    tmp:asvcode = clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number)
                Else
                    !no ASV code present - so use old file format
                    locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & '.xml'
                    tmp:asvcode = Clip(man:EDI_Account_Number)
                End !If clip(ASV:ASVCode) <> '' then
            Else
                !no record exists for this Account - so use the old file format
                locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & '.xml'
                tmp:asvcode = Clip(man:EDI_Account_Number)
            End !If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

        End !if jow:RepairedAt = 'ARC' then
        Filename = CLip(Man:EDI_Path) & '\' & locBasicFilename

        !now - has this filename been used before?
        FNQ:ASVCode     = clip(tmp:asvcode)
        FNQ:TempAccType = 0
        get(FileNameQueue,FNQ:ASVCode,FNQ:TempAccType)
        If Error() then
            !this filename does not esist in the queue - so add it and create the file
            FNQ:ASVCode     = tmp:asvcode
            FNQ:TempAccType = 0
            FNQ:Filename    = clip(Filename)
            add(FileNameQueue)
            f:FileList = Clip(f:FileList) & '|' & locBasicFilename
            !remove the file if it exists on the disk
            Remove(FileName)

            !now open the file and add the file header information
            If objXmlExport.FOpen(Clip(FileName), true)
                If Error()
                    If f:Silent = 0
                        Case Missive('Unable to create EDI File.'&|
                          '<13,10>'&|
                          '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If f:Silent = 0
                    tmp:Return = 1
                    Return
                End ! If Error()
            End ! If objXmlExport.FOpen(Clip(FileName), true)

            objXmlExport.OpenTag('REPAIRDATA','TIMESTAMP="' & Format(Today(),@d06.) & ' ' & Format(Clock(),@t4) & '" FILECREATOR="Vodacom" CLAIMGROUPID="" ')
            objXmlExport.OpenTag('SERVICECENTER')
            !ASC Number
            objXmlExport.WriteTag('SC01',clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number))
            !Country Code
            objXmlExport.WriteTag('SC02','ZA')
            !Region Code
            objXmlExport.WriteTag('SC03','30')
            !PT Versions
            objXmlExport.WriteTag('SC04','2.1')
            !Currency
            ! Changing (DBH 01/12/2005) #6778 - Show currency as ZAR
            ! objXmlExport.WriteTag('SC05','EUR')
            ! to (DBH 01/12/2005) #6778
            objXmlExport.WriteTag('SC05','ZAR')
            ! End (DBH 01/12/2005) #6778

        Else
            !file exists - so just open it
            Filename = clip(FNQ:Filename)
            If objXmlExport.FOpen(Clip(FileName), false)
                If Error()
                    If f:Silent = 0
                        Case Missive('Unable to open EDI File.'&|
                          '<13,10>'&|
                          '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If f:Silent = 0
                    tmp:Return = 1
                    Return
                End ! If Error()
            End ! If objXmlExport.FOpen(Clip(FileName), true)
        End
        !End of changes - Paul


        objXmlExport.OpenTag('CUSTOMER')
        !Customer Type
        objXmlExport.WriteTag('CU01','EP')
        !Last / Business Name
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        objXmlExport.WriteTag('CU02',tra:Company_Name)

        !First Name
        ! Changing (DBH 09/01/2006) #6967 - Use the trade account's address
        ! If job:Surname <> '' And job:Initial <> ''
        !                         objXmlExport.WriteTag('CU03',job:Initial)
        !                     Else ! If job:Surname <> '' And job:Initial <> ''
        !                         objXmlExport.WriteTag('CU03','')
        !                     End ! If job:Surname <> '' And job:Initial <> ''
        ! to (DBH 09/01/2006) #6967
        objXmlExport.WriteTag('CU03',Format(Clip(tra:Address_Line1) & ' ' & Clip(tra:Address_Line2) & ' ' & Clip(tra:Address_Line3) & ' ' & CLip(tra:Postcode),@s50))
        ! End (DBH 09/01/2006) #6967
        !Phone Number
        objXmlExport.WriteTag('CU04',tra:Telephone_Number)
        !Fax Number
        objXmlExport.WriteTag('CU05',job:Fax_Number)
        !EMail Address
        objXmlExport.WriteTag('CU06',jobe:EndUserEmailAddress)
        !Club Nokia ID
        objXmlExport.WriteTag('CU07','')

        objXmlExport.OpenTag('WORKORDER')
        !Return Type
        objXmlExport.WriteTag('WO01','SR')
        !Work Order ID
        objXmlExport.WriteTag('WO02',job:Ref_Number)
        !Customer Symptom Code
        objXmlExport.WriteTag('WO03',job:Fault_Code1)
        !Customer Symptom Free Text
        ! Inserting (DBH 20/06/2006) #7707 - Truncate the text
! Changing (DBH 30/04/2008) # 9723 - Trucate again
!        objXmlExport.WriteTag('WO04',Sub(BHStripNonAlphaNum(jbn:Fault_Description,' '),1,220))
! to (DBH 30/04/2008) # 9723
                objXmlExport.WriteTag('WO04',Sub(BHStripNonAlphaNum(jbn:Fault_Description,' '),1,150))
! End (DBH 30/04/2008) #9723
        ! End (DBH 20/06/2006) #7707
        !Work Order Status
        objXmlExport.WriteTag('WO05','')
        !Service Code
! Changing (DBH 06/11/2007) # 9278 - Check the job fault code, then check the part
!        objXmlExport.WriteTag('WO06',wob:FaultCode13)
! to (DBH 06/11/2007) # 9278
        If wob:FaultCode13 <> ''
            objXmlExport.WriteTag('WO06',wob:FaultCode13)
        Else ! If wob:FaultCode13 <> ''
            objXmlExport.WriteTag('WO06',local:ServiceCode)
        End ! If wob:FaultCode13 <> ''
! End (DBH 06/11/2007) #9278

        !Technician Name
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code   = job:Engineer
        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            ! Found
            objXmlExport.WriteTag('WO07',Clip(use:Forename) & ' ' & Clip(use:Surname))
        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            ! Error
            objXmlExport.WriteTag('WO07','')
        End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Technician Comments
        ! Changing (DBH 20/06/2006) #7707 - Truncate the text
        ! objXmlExport.WriteTag('WO08',BHStripNonAlphaNum(jbn:Engineers_Notes,' '))
        ! to (DBH 20/06/2006) #7707
        objXmlExport.WriteTag('WO08',Sub(BHStripNonAlphaNum(jbn:Engineers_Notes,' '),1,220))
        ! End (DBH 20/06/2006) #7707
        !Labour Cost
        objXmlExport.WriteTag('WO09',Format(jobe:ClaimValue,@n_14.2))

        !Other Cost
        objXmlExport.WriteTag('WO10','')
        !Queuing Time
        objXmlExport.WriteTag('WO11','')
        !Walk-in Access Time
        objXmlExport.WriteTag('WO12','')
        !Sender Country
        objXmlExport.WriteTag('WO13','ZA')

        objXmlExport.OpenTag('PHONE')
        !Primary Serial Number
        objXmlExport.WriteTag('PH01',job:ESN)
        !Additional Serial Number
        objXmlExport.WriteTag('PH02',job:Fault_Code9)
        !Sales Model
        objXmlExport.WriteTag('PH03','')
        !Product Type
        objXmlExport.WriteTag('PH04','')
        !Product Code
        objXmlExport.WriteTag('PH05',job:ProductCode)
        !Receive Items
        objXmlExport.WriteTag('PH06','')
        !SW Version
        objXmlExport.WriteTag('PH07',job:Fault_Code5)
        !New SW Version
        objXmlExport.WriteTag('PH08',job:Fault_Code6)
        !HW Version
        objXmlExport.WriteTag('PH09','')
        !New HW Version
        objXmlExport.WriteTag('PH10','')
        !Manufacturing Date
        objXmlExport.WriteTag('PH11',Format(job:Fault_Code8,@d06.b))
        !Label Date Code
        objXmlExport.WriteTag('PH12','')
        !POP Date
        objXmlExport.WriteTag('PH13',Format(job:DOP,@d06.b))
        !Warranty Status
        objXmlExport.WriteTag('PH14','1')
        !Warranty Override Reason Code
        objXmlExport.WriteTag('PH15',job:Fault_Code4)
        !Warranty Ref#
        objXmlExport.WriteTag('PH16',wob:FaultCode16)
        !Receiving Date/Time
        !objXmlExport.WriteTag('PH17',Format(job:Date_Booked,@d06.b) & ' ' & Format(job:Time_Booked,@t04b))
        locDate = job:Date_Booked
        locTime = job:Time_Booked
        GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)
        objXmlExport.WriteTag('PH17',Format(locDate,@d06.b) & ' ' & Format(locTime,@t04b))
        !Expected Return Time
        objXmlExport.WriteTag('PH18','')
        !Expected Return Time Reason Code
        objXmlExport.WriteTag('PH19','')
        !Quote Status
        objXmlExport.WriteTag('PH20','')
        !Shipping Date / Time

        !objXmlExport.WriteTag('PH21',Format(job:Date_Completed,@d06.b) & ' ' & Format(job:Time_Completed,@t04b))
        !012656 Completed date on Tech reps, complex sorting
        Do GetDateCompleted
        LocDate = DateCompleted
        LocTime = TimeCompleted
        !Replaces the following
        !locDate = job:Date_Completed
        !locTime = job:Time_Completed

        IF (job:Exchange_Unit_Number > 0)
            GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
        END !IF (job:Exchange_Unit_Number > 0)
        objXmlExport.WriteTag('PH21',Format(locDate,@d06.b) & ' ' & Format(locTime,@t04b))
        !Special Warranty Ref IF
        objXmlExport.WriteTag('PH22',job:Fault_Code3)
        !Bluetooth Address
        objXmlExport.WriteTag('PH23',job:Fault_Code11)
        !Bluetooth HW
        objXmlExport.WriteTag('PH24',job:Fault_Code4)

        If job:Exchange_Unit_Number <> 0

            !TB12935 - J - 26/10/12 changes made to <SWAP> part of export - needs detail of Second Exchange Number fetch this first
            Local:2ndIMEI = ''
            If jobe:SecondExchangeNumber <> ''
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = jobe:SecondExchangeNumber
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    Local:2ndIMEI = xch:ESN
                END !if tryfetch worked
            END !if jobe:SecondExchangeNumber
            
            !NOW LOOK UP THE FIRST exchange and keep in buffer
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number   = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                ! Found
                local:ExchangeDate = ''
                Access:AUDIT.ClearKey(aud:TypeActionKey)
                aud:Ref_Number = job:Ref_Number
                aud:Type       = 'EXC'
                aud:Action     = 'EXCHANGE UNIT ATTACHED TO JOB'
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job:Ref_Number      |
                    Or aud:Type       <> 'EXC'      |
                    Or aud:Action     <> 'EXCHANGE UNIT ATTACHED TO JOB'      |
                        Then Break.  ! End If
                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                    aud2:AUDRecordNumber = aud:Record_Number
                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                        If Instring(job:Exchange_Unit_Number,aud2:Notes,1,1)
                            local:ExchangeDate    = aud:Date
                            local:ExchangeTime    = aud:Time
                            Break
                        End !If Instring(job:Exchange_Unit_Number,aud:Notes,1,1)
                    END ! IF

                End !Loop

                objXmlExport.OpenTag('SWAP')
                !Swap Date/Time
                objXmlExport.WriteTag('SW01',Format(local:ExchangeDate,@d06.b)&' '&format(local:ExchangeTime,@t04)) !TB12835 - J - 26/10/12 adds time
                !Swap Reason Code
                objXmlExport.WriteTag('SW02',job:Fault_Code2)   !TB12835 - preexisting
                !New Primary Serail Number
                objXmlExport.WriteTag('SW03',xch:ESN)           !TB12835 - preexisting
                !New Additional Serial Number
                objXmlExport.WriteTag('SW04',Local:2ndIMEI)     !TB12835 - J - 26/10/12
                !New Sales Model
                objXmlExport.WriteTag('SW05',xch:Model_Number)  !TB12835 - J - 26/10/12
                !New Product Type
                objXmlExport.WriteTag('SW06',wob:FaultCode18)   !TB12835 - J - 26/10/12
                !New Product Code
                objXmlExport.WriteTag('SW07',wob:FaultCode19)   !TB12835 - J - 26/10/12
                !New Software Version
                objXmlExport.WriteTag('SW08',JOB:Fault_Code6)   !TB12835 - J - 26/10/12
                !Repair Action
                objXmlExport.WriteTag('SW09',job:Fault_Code7)   !TB12835 - preexisting
                objXmlExport.CloseTag('SWAP')

            Else ! If Access:EXCHANGE.Tryfetch(xch:RefNumberKey) = Level:Benign
                ! Error
            End ! If Access:EXCHANGE.Tryfetch(xch:RefNumberKey) = Level:Benign
        End ! If job:Exchange_Unit_Number <> 0

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number      |
                Then Break.  ! End If

            ! Inserting (DBH 08/12/2005) #6838 - Do not check for a stock part if it's an adjustment
            If wpr:Part_Number = 'ADJUSTMENT'
            Else ! If wpr:Part_Number = 'ADJUSTMENT'
            ! End (DBH 08/12/2005) #6838
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    ! Found
                    If sto:Accessory = 'YES'
                        Cycle
                    End ! If sto:Accessory = 'YES'
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    ! Error
                End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            End ! If wpr:Part_Number <> 'ADJUSTMENT'

            ! Inserting (DBH 30/04/2008) ' 9723 - Don`t show @N/A@
            If wpr:Fault_Code1 = 'N/A'
                wpr:Fault_Code1 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code2 = 'N/A'
                wpr:Fault_Code2 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code3 = 'N/A'
                wpr:Fault_Code3 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code4 = 'N/A'
                wpr:Fault_Code4 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code5 = 'N/A'
                wpr:Fault_Code5 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code6 = 'N/A'
                wpr:Fault_Code6 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code7 = 'N/A'
                wpr:Fault_Code7 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code8 = 'N/A'
                wpr:Fault_Code8 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code9 = 'N/A'
                wpr:Fault_Code9 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code10 = 'N/A'
                wpr:Fault_Code10 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code11 = 'N/A'
                wpr:Fault_Code11 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code12 = 'N/A'
                wpr:Fault_Code12 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            ! End (DBH 30/04/2008) '9723

            objXmlExport.OpenTag('REPAIRROW')
            !Repair Symptom Code
            objXmlExport.WriteTag('RL01',wpr:Fault_Code10)
            !Fault Code
            objXmlExport.WriteTag('RL02',wpr:Fault_Code3)
            !Key Repair
            objXmlExport.WriteTag('RL03',wpr:Fault_Code4)
            !Part Number
            If wpr:Part_Number = 'ADJUSTMENT'
                objXmlExport.WriteTag('RL04','')
            Else ! If wpr:Part_Number = 'ADJUSTMENT'
                objXmlExport.WriteTag('RL04',wpr:Part_Number)
            End ! If wpr:Part_Number = 'ADJUSTMENT'
            !Field Service Bulletin
            objXmlExport.WriteTag('RL05','')
            !CCT Ref Number
            objXmlExport.WriteTag('RL06',wpr:Fault_Code1)
            !Repair Module
            objXmlExport.WriteTag('RL07',wpr:Fault_Code2)
            !Part Replaced
            If wpr:Correction
                objXmlExport.WriteTag('RL08','0')
            Else ! If wpr:Correction
                If local:SoftwareUpgrade And wpr:Part_Number = 'ADJUSTMENT'
                    objXmlExport.WriteTag('RL08','0')
                Else ! If local:SoftwareUpgrade And wpr:Part_Number = 'ADJUSTMENT'
                    objXmlExport.WriteTag('RL08','1')
                End ! If local:SoftwareUpgrade And wpr:Part_Number = 'ADJUSTMENT'
            End ! If wpr:Correction

            !Part Cost
            If wpr:Correction
                objXmlExport.WriteTag('RL09','0')
            Else ! If wpr:Correction
                objXmlExport.WriteTag('RL09',Format(wpr:Purchase_Cost,@n_14.2))
            End ! If wpr:Correction
            !Price Group
            objXmlExport.WriteTag('RL10','')
            !Warranty Status
            objXmlExport.WriteTag('RL11','')

            objXmlExport.CloseTag('REPAIRROW')
        End !Loop
        objXmlExport.CloseTag('PHONE')
        objXmlExport.CloseTag('WORKORDER')
        objXmlExport.CloseTag('CUSTOMER')

        objXmlExport.FClose()

    Of 1 !Accessory
        ! Quick check to see if there are any parts to export. If not, ignore - TrkBs: 5904 (DBH: 26-09-2005)
        local:CountParts = 0
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number      |
                Then Break.  ! End If

            ! Inserting (DBH 08/12/2005) #6838 - Do not check for a stock part if it's an adjustment
            If wpr:Part_Number = 'ADJUSTMENT'
                ! Do not show adjustments on the accessory file - TrkBs: 6838 (DBH: 08-12-2005)
                Cycle
            Else ! If wpr:Part_Number = 'ADJUSTMENT'
            ! End (DBH 08/12/2005) #6838
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    ! Found
                    If sto:Accessory <> 'YES'
                        Cycle
                    End ! If sto:Accessory = 'YES'
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    ! Error
                End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            End ! If wpr:Part_Number <> 'ADJUSTMENT'

            ! Inserting (DBH 30/04/2008) ' 9723 - Don`t show @N/A@
            If wpr:Fault_Code1 = 'N/A'
                wpr:Fault_Code1 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code2 = 'N/A'
                wpr:Fault_Code2 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code3 = 'N/A'
                wpr:Fault_Code3 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code4 = 'N/A'
                wpr:Fault_Code4 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code5 = 'N/A'
                wpr:Fault_Code5 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code6 = 'N/A'
                wpr:Fault_Code6 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code7 = 'N/A'
                wpr:Fault_Code7 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code8 = 'N/A'
                wpr:Fault_Code8 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code9 = 'N/A'
                wpr:Fault_Code9 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code10 = 'N/A'
                wpr:Fault_Code10 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code11 = 'N/A'
                wpr:Fault_Code11 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            If wpr:Fault_Code12 = 'N/A'
                wpr:Fault_Code12 = ''
            End ! If wpr:Fault_Code1 = 'N/A'
            ! End (DBH 30/04/2008) '9723

            local:CountParts += 1

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                ! Found

            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                ! Error
            End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                !Found

            Else !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber   = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                ! Found

            Else ! If Access:JOBSE.Tryfetch(job:RefNumberKey) = Level:Benign
                ! Error
            End ! If Access:JOBSE.Tryfetch(job:RefNumberKey) = Level:Benign

            ! Inserting (DBH 01/12/2005) #6788 - Is this job a "Software Upgrade"
            Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
            joo:JobNumber   = job:Ref_Number
            joo:FaultCode   = '110'
            If Access:JOBOUTFL.Tryfetch(joo:JobNumberKey) = Level:Benign
                ! Found
                local:SoftwareUpgrade = True
            Else ! If Access:JOBOUTFL.Tryfetch(joo:JobNumberKey) = Level:Benign
                ! Error
            End ! If Access:JOBOUTFL.Tryfetch(joo:JobNumberKey) = Level:Benign
            ! End (DBH 01/12/2005) #6788

            !Changed by Paul - 07/10/2009 - log no 11082
            !need to make a new file for each service centre
            !first - look up the new ASV number
            Filename = ''
            if jow:RepairedAt = 'ARC' then
                !ARC account number

                tmpheadaccount" = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')

                Access:ASVACC.clearkey(ASV:TradeACCManufKey)
                ASV:TradeAccNo      = tmpheadaccount"
                ASV:Manufacturer    = job:Manufacturer
                If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
                    !record found
                    If clip(ASV:ASVCode) <> '' then
                        !construct the new file format
                        locBasicFilename = 'R' & clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & 'A.xml'
                        tmp:asvcode = clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number)
                    Else
                        !no ASV code present - so use old file format
                        locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & 'A.xml'
                        tmp:asvcode = Clip(man:EDI_Account_Number)
                    End !If clip(ASV:ASVCode) <> '' then
                Else
                    !no record exists for this Account - so use the old file format
                    locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & 'A.xml'
                    tmp:asvcode = Clip(man:EDI_Account_Number)
                End !If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then


            Else
                !RRC account number

                Access:ASVACC.clearkey(ASV:TradeACCManufKey)
                ASV:TradeAccNo      = wob:HeadAccountNumber
                ASV:Manufacturer    = job:Manufacturer
                If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
                    !record found
                    If clip(ASV:ASVCode) <> '' then
                        !construct the new file format
                        locBasicFilename = 'R' & clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & 'A.xml'
                        tmp:asvcode = clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number)
                    Else
                        !no ASV code present - so use old file format
                        locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & 'A.xml'
                        tmp:asvcode = Clip(man:EDI_Account_Number)
                    End !If clip(ASV:ASVCode) <> '' then
                Else
                    !no record exists for this Account - so use the old file format
                    locBasicFilename = 'R' & Clip(man:EDI_Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2) & FOrmat(clock(),@t05) & 'A.xml'
                    tmp:asvcode = Clip(man:EDI_Account_Number)
                End !If Access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

            End !if jow:RepairedAt = 'ARC' then

            FileName = Clip(man:EDI_Path) & locBasicFileName

            !now - has this filename been used before?
            FNQ:ASVCode     = clip(tmp:asvcode)
            FNQ:TempAccType = 1
            get(FileNameQueue,FNQ:ASVCode,FNQ:TempAccType)
            If Error() then
                !this filename does not esist in the queue - so add it and create the file
                FNQ:ASVCode     = tmp:asvcode
                FNQ:TempAccType = 1
                FNQ:Filename    = clip(Filename)
                add(FileNameQueue)
                !remove the file if it exists on the disk
                Remove(FileName)
                f:FileList = Clip(f:FileList) & '|' & locBasicFilename

                !now open the file and add the file header information
                If objXmlExport.FOpen(Clip(FileName), true)
                    If Error()
                        If f:Silent = 0
                            Case Missive('Unable to create EDI File.'&|
                              '<13,10>'&|
                              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        End ! If f:Silent = 0
                        tmp:Return = 1
                        Return
                    End ! If Error()
                End ! If objXmlExport.FOpen(Clip(FileName), true)

                objXmlExport.OpenTag('REPAIRDATA','TIMESTAMP="' & Format(Today(),@d06.) & ' ' & Format(Clock(),@t4) & '" FILECREATOR="Vodacom" CLAIMGROUPID="" ')
                objXmlExport.OpenTag('SERVICECENTER')
                !ASC Number
                objXmlExport.WriteTag('SC01',clip(ASV:ASVCode) & '_' & Clip(man:EDI_Account_Number))
                !Country Code
                objXmlExport.WriteTag('SC02','ZA')
                !Region Code
                objXmlExport.WriteTag('SC03','30')
                !PT Versions
                objXmlExport.WriteTag('SC04','2.1')
                !Currency
                ! Changing (DBH 01/12/2005) #6778 - Show currency as ZAR
                ! objXmlExport.WriteTag('SC05','EUR')
                ! to (DBH 01/12/2005) #6778
                objXmlExport.WriteTag('SC05','ZAR')
                ! End (DBH 01/12/2005) #6778

            Else
                !file exists - so just open it
                FileName = clip(FNQ:Filename)
                If objXmlExport.FOpen(Clip(FileName), False)
                    If Error()
                        If f:Silent = 0
                            Case Missive('Unable to open EDI File.'&|
                              '<13,10>'&|
                              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        End ! If f:Silent = 0
                        tmp:Return = 1
                        Return
                    End ! If Error()
                End ! If objXmlExport.FOpen(Clip(FileName), true)
            End
            !End of changes - Paul


            objXmlExport.OpenTag('CUSTOMER')
            !Customer Type
            objXmlExport.WriteTag('CU01','EP')
            !Last / Business Name
            ! Changing (DBH 08/12/2005) #6838 - Use the booking franchise's details
            ! If job:Surname <> ''
            !                 objXmlExport.WriteTag('CU02',job:Surname)
            !             Else ! If job:Surname <> ''
            !                 objXmlExport.WriteTag('CU02',job:Company_Name)
            !             End ! If job:Surname <> ''
            ! to (DBH 08/12/2005) #6838
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                ! Found

            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                ! Error
            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            objXmlExport.WriteTag('CU02',tra:Company_Name)
            ! End (DBH 08/12/2005) #6838

            !First Name
            ! Changing (DBH 09/01/2006) #6967 - Use the trade account's address
            ! If job:Surname <> '' And job:Initial <> ''
            !                         objXmlExport.WriteTag('CU03',job:Initial)
            !                     Else ! If job:Surname <> '' And job:Initial <> ''
            !                         objXmlExport.WriteTag('CU03','')
            !                     End ! If job:Surname <> '' And job:Initial <> ''
            ! to (DBH 09/01/2006) #6967
            objXmlExport.WriteTag('CU03',Format(Clip(tra:Address_Line1) & ' ' & Clip(tra:Address_Line2) & ' ' & Clip(tra:Address_Line3) & ' ' & CLip(tra:Postcode),@s50))
            ! End (DBH 09/01/2006) #6967

            !Phone Number
            ! Changing (DBH 08/12/2005) #6838 - Use the booking franchise's telephone number
            ! If job:Mobile_Number <> ''
            !                 objXmlExport.WriteTag('CU04',job:Mobile_Number)
            !             Else ! If job:Mobile_Number <> ''
            !                 objXmlExport.WriteTag('CU04',job:Telephone_Number)
            !             End ! If job:Mobile_Number <> ''
            ! to (DBH 08/12/2005) #6838
            objXmlExport.WriteTag('CU04',tra:Telephone_Number)
            ! End (DBH 08/12/2005) #6838

            !Fax Number
            objXmlExport.WriteTag('CU05',job:Fax_Number)
            !EMail Address
            objXmlExport.WriteTag('CU06',jobe:EndUserEmailAddress)
            !Club Nokia ID
            objXmlExport.WriteTag('CU07','')

            objXmlExport.OpenTag('WORKORDER')
            !Return Type
            objXmlExport.WriteTag('WO01','SR')
            !Work Order ID
            If local:CountParts = 1
                objXmlExport.WriteTag('WO02',job:Ref_Number)
            Else ! If local:CountJobs = 1
                ! Inserting (DBH 21/12/2005) #6838 - Suffix the part number, if more than one accessory
                objXmlExport.WriteTag('WO02',job:Ref_Number & '-' & local:CountParts)
                ! End (DBH 21/12/2005) #6838
            End ! If local:CountJobs = 1

            !Customer Symptom Code
            objXmlExport.WriteTag('WO03',job:Fault_Code1)
            !Customer Symptom Free Text
            ! Changing (DBH 20/06/2006) #7707 - Truncate the text
            ! objXmlExport.WriteTag('WO04',BHStripNonAlphaNum(jbn:Fault_Description,' '))
            ! to (DBH 20/06/2006) #7707
! Changing (DBH 30/04/2008) # 9723 - Truncate again
!            objXmlExport.WriteTag('WO04',Sub(BHStripNonAlphaNum(jbn:Fault_Description,' '),1,220))
! to (DBH 30/04/2008) # 9723
            objXmlExport.WriteTag('WO04',Sub(BHStripNonAlphaNum(jbn:Fault_Description,' '),1,150))
! End (DBH 30/04/2008) #9723
            ! End (DBH 20/06/2006) #7707
            !Work Order Status
            objXmlExport.WriteTag('WO05','')
            !Service Code
            objXmlExport.WriteTag('WO06','AR')
            !Technician Name
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Found
                objXmlExport.WriteTag('WO07',Clip(use:Forename) & ' ' & Clip(use:Surname))
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Error
                objXmlExport.WriteTag('WO07','')
            End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Technician Comments
            ! Changing (DBH 20/06/2006) #7707 - Truncate the text
            ! objXmlExport.WriteTag('WO08',BHStripNonAlphaNum(jbn:Engineers_Notes,' '))
            ! to (DBH 20/06/2006) #7707
            objXmlExport.WriteTag('WO08',Sub(BHStripNonAlphaNum(jbn:Engineers_Notes,' '),1,220))
            ! End (DBH 20/06/2006) #7707
            !Labour Cost
            ! Changing (DBH 01/12/2005) #6778 - Show the ARC claim cost
            ! objXmlExport.WriteTag('WO09',Format(job:Labour_Cost_Warranty,@n_14.2))
            ! to (DBH 01/12/2005) #6778
            objXmlExport.WriteTag('WO09',Format(jobe:ClaimValue,@n_14.2))
            ! End (DBH 01/12/2005) #6778

            !Other Cost
            objXmlExport.WriteTag('WO10','')
            !Queuing Time
            objXmlExport.WriteTag('WO11','')
            !Walk-in Access Time
            objXmlExport.WriteTag('WO12','')
            !Sender Country
            objXmlExport.WriteTag('WO13','ZA')

            objXmlExport.OpenTag('PHONE')
            !Primary Serial Number
! Changing (DBH 08/11/2006) # 8452 - Clear PH01 for the accessory file
!                    objXmlExport.WriteTag('PH01',job:ESN)
! to (DBH 08/11/2006) # 8452
            objXmlExport.WriteTag('PH01','')
! End (DBH 08/11/2006) #8452
            !Additional Serial Number
            objXmlExport.WriteTag('PH02',job:Fault_Code9)
            !Sales Model
            objXmlExport.WriteTag('PH03','')
            !Product Type
            objXmlExport.WriteTag('PH04','')
            !Product Code
            objXmlExport.WriteTag('PH05',wpr:Part_Number)
            !Receive Items
            objXmlExport.WriteTag('PH06','')
            !SW Version
            objXmlExport.WriteTag('PH07',job:Fault_Code5)
            !New SW Version
            objXmlExport.WriteTag('PH08',job:Fault_Code6)
            !HW Version
            objXmlExport.WriteTag('PH09','')
            !New HW Version
            objXmlExport.WriteTag('PH10','')
            !Manufacturing Date
            objXmlExport.WriteTag('PH11',Format(job:Fault_Code8,@d06.b))
            !Label Date Code
            objXmlExport.WriteTag('PH12','')
            !POP Date
            objXmlExport.WriteTag('PH13',Format(job:DOP,@d06.b))
            !Warranty Status
            objXmlExport.WriteTag('PH14','1')
            !Warranty Override Reason Code
            objXmlExport.WriteTag('PH15',job:Fault_Code4)
            !Warranty Ref#
            objXmlExport.WriteTag('PH16',wob:FaultCode16)
            !Receiving Date/Time
            objXmlExport.WriteTag('PH17',Format(job:Date_Booked,@d06.b) & ' ' & Format(job:Time_Booked,@t04b))
            !Expected Return Time
            objXmlExport.WriteTag('PH18','')
            !Expected Return Time Reason Code
            objXmlExport.WriteTag('PH19','')
            !Quote Status
            objXmlExport.WriteTag('PH20','')
            !Shipping Date / Time

            !012656 Completed date on Tech reps, complex sorting
            Do GetDateCompleted
            LocDate = DateCompleted
            LocTime = TimeCompleted
            objXmlExport.WriteTag('PH21',Format(LocDate,@d06.b) & ' ' & Format(LocTime,@t04b))
            !Special Warranty Ref IF
            objXmlExport.WriteTag('PH22',job:Fault_Code3)
            !Bluetooth Address
            objXmlExport.WriteTag('PH23',job:Fault_Code11)
            !Bluetooth HW
            objXmlExport.WriteTag('PH24',job:Fault_Code4)

            objXmlExport.OpenTag('REPAIRROW')
            !Repair Symptom Code
            objXmlExport.WriteTag('RL01',wpr:Fault_Code10)
            !Fault Code
            objXmlExport.WriteTag('RL02',wpr:Fault_Code3)
            !Key Repair
            objXmlExport.WriteTag('RL03',wpr:Fault_Code4)
            !Part Number
            If wpr:Part_Number = 'ADJUSTMENT'
                objXmlExport.WriteTag('RL04','')
            Else ! If wpr:Part_Number = 'ADJUSTMENT'
                objXmlExport.WriteTag('RL04',wpr:Part_Number)
            End ! If wpr:Part_Number = 'ADJUSTMENT'
            !Field Service Bulletin
            objXmlExport.WriteTag('RL05','')
            !CCT Ref Number
            objXmlExport.WriteTag('RL06',wpr:Fault_Code1)
            !Repair Module
            objXmlExport.WriteTag('RL07',wpr:Fault_Code2)
            !Part Replaced
            If wpr:Correction
                objXmlExport.WriteTag('RL08','0')
            Else ! If wpr:Correction
                If local:SoftwareUpgrade And wpr:Part_Number = 'ADJUSTMENT'
                    objXmlExport.WriteTag('RL08','0')
                Else ! If local:SoftwareUpgrade And wpr:Part_Number = 'ADJUSTMENT'
                    objXmlExport.WriteTag('RL08','1')
                End ! If local:SoftwareUpgrade And wpr:Part_Number = 'ADJUSTMENT'
            End ! If wpr:Correction

            !Part Cost
            If wpr:Correction
                objXmlExport.WriteTag('RL09','0')
            Else ! If wpr:Correction
                objXmlExport.WriteTag('RL09',Format(wpr:Purchase_Cost,@n_14.2))
            End ! If wpr:Correction

            !Price Group
            objXmlExport.WriteTag('RL10','')
            !Warranty Status
            objXmlExport.WriteTag('RL11','')

            objXmlExport.CloseTag('REPAIRROW')
            objXmlExport.CloseTag('PHONE')
            objXmlExport.CloseTag('WORKORDER')
            objXmlExport.CloseTag('CUSTOMER')

            objXmlExport.FClose()
        End !Loop

    End ! Case tmp:Accessory
local.PanasonicExport        Procedure(Byte f:FirstSecondYear)
Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group       Group
Line1           STRING(2000)
          . ..

Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
type         STRING(2)
date1        String(4)
date3        String(2)
date4        String(2)
jobno        String(6)
accountno    String(4)
filler       String(48)
modelno      String(14)
serialno     String(14)
servcode     String(1)
dop          String(6)
faultdate    String(6)
compdate     String(6)
filler2      String(124)
faulttext    String(70)
repairtext   String(70)
filler3      String(10)
condcode     String(1)
symptom1     String(1)
symptom2     String(1)
extencode    String(1)
filler4      String(26)
cust_name    STRING(30)
cust_add1    STRING(30)
cust_add2    STRING(30)
cust_town    STRING(30)
cust_cnty    STRING(20)
cust_pcod    STRING(10)
cust_tel     STRING(15)
DealerName   String(30)
filler5      String(60)
DealerCity   String(20)
filler6      String(54)
fillerx      String(1)
          .
!out_fil2    DOS,PRE(ou2),NAME(filename)
Out_Part   GROUP,OVER(Ouf:Out_Group),PRE(L2)
type1        STRING(2)
jobno1       String(6)
part_no      STRING(14)
qty          STRING(6)
filler1      String(14)
sect_code    STRING(3)
defect       STRING(1)
repair       STRING(1)
PanaInvoice  String(9)
Filler2      String(32)
Fillery      String(1)
         .
locBasicFilename        String(255)
Code
    Do GetEDIPath

    locBasicFilename = Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.DAT'
    End !If func:SecondYear

    Filename = CLIP(MAN:EDI_Path) & '\' & locBasicFilename

    Remove(Out_File)
    Create(Out_File)
    Open(Out_File)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportPanasonic
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(Out_File)

ExportPanasonic        Routine
    Clear(Ouf:Record)
    l1:type    = 'A1'
    l1:date1    = Format(Year(Today()),@n04)
    l1:date3    = Format(month(Today()),@n02)
    l1:date4    = Format(day(Today()),@n02)
    l1:jobno    = Format(job:ref_number,@n06)
    l1:AccountNo    = Format(man:EDI_Account_Number,@s4)
    l1:filler   = Format('',@s48)
    l1:modelno  = Format(job:model_number,@s14)
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        l1:serialno = Format(job:esn,@s14)
    Else !IMEIError# = 1
        l1:serialno = Format(jot:OriginalIMEI,@s14)
    End !IMEIError# = 1

    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

    Else! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign


    l1:servcode = Format(' ',@s1)
    l1:dop      = Format(job:dop,@d11)
    l1:faultdate = Format(job:date_booked,@d11)
    l1:compdate = Format(job:date_completed,@d11)
    l1:filler2  = Format('',@s124)
    l1:faulttext = Format(Stripcomma(Stripreturn(jbn:fault_description)),@s70)
    l1:repairtext = Format(Stripcomma(Stripreturn(jbn:invoice_Text)),@s70)
    l1:filler3  = Format('',@s10)
    l1:condcode = Format(job:fault_code1,@s1)
    l1:symptom1 = Format(job:fault_code2,@s1)
    l1:symptom2 = Format(job:fault_code3,@s1)
    l1:extencode = Format(job:fault_code4,@s1)
    l1:filler4  = Format('',@s26)
    l1:cust_name    =  Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),@s30) !Cust_Name
    l1:cust_add1    =  Format(job:address_line1,@s30)                           !Cust_Add1
    l1:cust_add2    =  Format(job:address_line2,@s30)                           !Cust_Add2
    l1:cust_town    =  Format(job:address_line3,@s30)                           !Cust_Add3
    l1:cust_cnty    =  Format('',@s20)                         !Cust_Cnty
    l1:cust_pcod    =  Format(job:postcode,@s10)                                !Cust_PCod
    l1:cust_tel    =  Format(job:telephone_number,@s15)                        !Cust_Tel
    l1:DealerName   = Format('',@s30)
    l1:Filler5      = Format('',@s60)
    l1:DealerCity   = Format('',@s20)
    l1:Filler6      = Format('',@s54)
    l1:Fillerx      = 'X'
    Append(out_file)

    Clear(ouf:Record)
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'
        l2:type1    = 'A2'
        l2:jobno1    = Format(job:ref_number,@n06)
        l2:part_no    =  Format(wpr:part_number,@s14)
        l2:qty    =  Format(wpr:quantity,@n06)
        l2:filler1  = Format('',@s14)
        l2:sect_code    =  Format(wpr:Fault_Code2,@s3)
        l2:defect    =  Format(wpr:Fault_Code3,@s1)
        l2:repair    =  Format(wpr:Fault_Code4,@s1)
        l2:PanaInvoice  = Format('',@s9)
        l2:Filler2      = Format('',@s32)
        l2:Fillery      = 'Y'
        Append(out_file)
    end !loop
    Clear(Ouf:Record)

local.PhilipsExport        Procedure(Byte f:FirstSecondYear)
FilePhilips    File,Driver('ASCII'),Pre(Philips),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
locBasicFilename        String(255)
Code

    Do GetEDIPath

    locBasicFilename = 'PHI.CSV'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear
    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(FilePhilips)
    Create(FilePhilips)
    Open(FilePhilips)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportPhilips
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If man:IncludeCharJobs
        Do Prog:ProgressSetup
        Prog:TotalRecords = Records(JOBS)
        Prog:ShowPercentage = 1 !Show Percentage Figure

        Access:JOBS.Clearkey(job:DateCompletedKey)
        job:Date_Completed = tmp:CompStartDate
        Set(job:DateCompletedKey,job:DateCompletedKey)

        ?Prog:UserString{Prop:Text} = 'Exporting Chargeabe Jobs'

        Accept
            Case Event()
                Of Event:Timer
                    Loop 25 Times
                        !Inside Loop
                        If Access:JOBS.Next()
                            Prog:Exit = 1
                            Break
                        End ! If Access:JOBS.Next()
                        If job:Date_Completed > tmp:CompEndDate
                            Prog:Exit = 1
                            Break
                        End ! If job:Date_Completed > tmp:CompEndDate

                        If job:Manufacturer <> f:Manufacturer
                            Cycle
                        End ! If job:Manufacturer <> f:Manufacturer
                        If job:Chargeable_Job <> 'YES'
                            Cycle
                        End ! If job:Chargeable_Job <> 'YES'

                        Do ExportPhilips

                        Prog:RecordCount += 1

                        Do Prog:UpdateScreen
                    End ! Loop 25 Times
                Of Event:CloseWindow
        !            Prog:Exit = 1
        !            Prog:Cancelled = 1
        !            Break
                Of Event:Accepted
                    If Field() = ?Prog:Cancel
                        Beep(Beep:SystemQuestion)  ;  Yield()
                        Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                       icon:Question,'&Yes|&No',2,2)
                            Of 1 ! &Yes Button
                                Prog:Exit = 1
                                Prog:Cancelled = 1
                                Break
                            Of 2 ! &No Button
                        End!Case Message
                    End ! If Field() = ?ProgressCancel
            End ! Case Event()
            If Prog:Exit
                Break
            End ! If Prog:Exit
        End ! Accept
        Do Prog:ProgressFinished

    End ! If man:IncludeCharJobs

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FilePhilips)

ExportPhilips        Routine
    Clear(Philips:record)
    Philips:line1   = '"SAF","VOD","G"'
    !Model
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        Philips:line1   = Clip(Philips:line1) & ',"' & Format(mod:Product_Type,@s13) & '"'
    Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        Philips:line1   = Clip(Philips:line1) & ',"' &  '"'
    End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

    !Date of deposit
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:Date_Booked,@d06b) & '"'
    !Date In
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:Date_Booked,@d06b) & '"'
    !Date Out
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:Date_Completed,@d06b) & '"'
    !Transport
    Philips:line1   = Clip(Philips:line1) & ',"NO"'
    !Customer Type
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:fault_code2,@s1) & '"'
    !Purchase Date
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:dop,@d06b) & '"'

    !Warranty Status
    If job:Warranty_Job = 'YES'
        Philips:line1   = Clip(Philips:line1) & '," Y"'
    Else !If job:Warranty_Job = 'YES'
        Philips:line1   = Clip(Philips:line1) & '," N"'
    End !If job:Warranty_Job = 'YES'

    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    !I.M.E.I.
    If IMEIError# = 1
        Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:esn,@s15) & '"'
    Else !IMEIError# = 1
        Philips:line1   = Clip(Philips:line1) & ',"' & Format(jot:OriginalIMEI,@s15) & '"'
    End !IMEIError# = 1

    !Serial Number
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:msn,@s14) & '"'
    !Customer IRIS Symtpom Code
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:fault_code1,@s4) & '"'
    !NSC Iris Symptom Code
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:fault_code3,@s4) & '"'
    !Model if swap
    !New I.M.E.I.
    !Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
    ExchangeAttached# = False
    If job:Exchange_unit_Number <> 0
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        !RRC exchanges for normal jobs appear on the report
        !48 Hour RRC exchanges do not
        !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
        If jobe:Engineer48HourOption = 1
            If jobe:ExchangedATRRC = True
                If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = jobe:SecondExchangeNumber
                Else !If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = 0
                End !If jobe:SecondExchangeNumber <> 0
            End !If jobe:ExchangeATRRC = True
        Else !jobe:Exchange48HourOption = 1
            ExchangeNumber# = job:Exchange_Unit_Number
        End !jobe:Exchange48HourOption = 1

        If ExchangeNumber# <> 0
            !Exchange Made!
            Access:Exchange.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = ExchangeNumber#
            IF Access:Exchange.Fetch(xch:Ref_Number_Key) = Level:Benign
                ExchangeAttached# = True
                Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                mod:Model_Number = xch:Model_Number
                If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                    !Found
                    Philips:line1   = Clip(Philips:line1) & ',"' & Format(mod:Product_Type,@s13) & '"'
                Else!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Philips:line1   = Clip(Philips:line1) & ',""'
                End!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                Philips:line1   = Clip(Philips:line1) & ',"' & Format(xch:ESN,@s15) & '"'
            END
        End !If ShowExchange# = True
    End !job:Exchange_unit_Number <> 0

    If ExchangeAttached# = False
        Philips:line1   = Clip(Philips:line1) & ',""'
        Philips:line1   = Clip(Philips:line1) & ',""'
    End !If ExchangeAttached# = False

    !Work sheet number 1
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:Ref_Number,@s12) & '"'
    !Work sheet number 2
    Philips:line1   = Clip(Philips:line1) & ',""'
    !Costing!

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    !Parts Total
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(jobe:ClaimPartsCost,@n_8v2) & '"'
    !Labour Total
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(jobe:ClaimValue,@n_8v2) & '"'
    !Other
    Philips:line1   = Clip(Philips:line1) & ',""'
    !Total
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(jobe:ClaimValue + jobe:ClaimPartsCost,@n_8v2) & '"'

    !Customer Name
    !Show Company Name if no name is present - 3314 (DBH: 24-09-2003)
    If job:Surname <> ''
        Philips:line1   = Clip(Philips:line1) & ',"' & StripComma(Format(CLIP(job:Title)&' '&CLIP(job:Surname),@s20)) & '"'
    Else !If job:Surname <> ''
        Philips:line1   = Clip(Philips:line1) & ',"' & StripComma(Format(job:Company_Name,@s20)) & '"'
    End !If job:Surname <> ''

    !Customer Address
    Philips:line1   = Clip(Philips:line1) & ',"' & StripComma(Format(CLIP(job:Address_Line1)&' '&CLIP(job:Address_Line2)&' '&CLIP(job:Address_Line3),@s50)) & '"'
    !Telephone Number
    Philips:line1   = Clip(Philips:line1) & ',"' & StripComma(Format(job:Telephone_Number,@s20)) & '"'

    !Action Code!
    Philips:line1   = Clip(Philips:line1) & ',"' & StripComma(Format(job:Fault_Code4,@s3)) & '"'
    !Action 2
    Philips:line1   = Clip(Philips:line1) & ',""'

    !Part / Quantity / Repair Code
    !Next!
    l# = 0

    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:Ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:Ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'
        l#+=1
        If wpr:Part_Number = 'ADJUSTMENT'
            Philips:line1   = Clip(Philips:line1) & ',""'
            Philips:line1   = Clip(Philips:line1) & ',""'
        Else !If wpr:Part_Number = 'ADJUSTMENT'
            Philips:line1   = Clip(Philips:line1) & ',"' & Format(wpr:part_number,@s12) & '"'
            Philips:line1   = Clip(Philips:line1) & ',"' & Format(wpr:Quantity,@n02) & '"'
        End !If wpr:Part_Number = 'ADJUSTMENT'
        Philips:line1   = Clip(Philips:line1) & ',"' & Format(wpr:fault_code1,@s6) & '"'
    end !loop
    !Leave a blank space for the reamining parts, if there are any
    If l# < 4
        Loop Until l# => 4
            l# += 1
            Philips:line1   = Clip(Philips:line1) & ',""'
            Philips:line1   = Clip(Philips:line1) & ',""'
            Philips:line1   = Clip(Philips:line1) & ',""'
        End !Loop Until l# => 4
    End !If l# = 0
    Philips:line1   = Clip(Philips:line1) & ',"' & Format(job:Fault_Code12,@s5) & '"'
    Append(FilePhilips)
    setcursor()
!local.SagemExport        Procedure(Byte f:FirstSecondYear)
!Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
!RECORD      RECORD
!Out_Group     GROUP
!Line1           STRING(1030)
!          . . .
!
!!out_file    DOS,PRE(ouf),NAME(filename)
!Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
!EDI_No         String(15)
!Order_No       STRING(28)
!Customer       STRING(28)
!Warranty       STRING(5)
!IMEI_In        STRING(18)
!Sag_Ref_In     STRING(12)
!Date_Code      STRING(10)
!IMEI_Out       STRING(18)
!Sag_Ref_Out    STRING(12)
!Battery        STRING(12)
!Charger        STRING(12)
!Antenna        STRING(6)
!DOP            STRING(11)
!Booked         STRING(11)
!Est_Out        STRING(11)
!Est_In         STRING(11)
!Despatched     STRING(11)
!Cust_Code      STRING(8)
!Fault_Code     STRING(8)
!Soft_In        STRING(14)
!Soft_Out       STRING(14)
!Repair_Level   STRING(8)
!Part1          STRING(12)
!Part2          STRING(12)
!Part3          STRING(12)
!Part4          STRING(12)
!Part5          STRING(12)
!Part6          STRING(12)
!Part7          STRING(12)
!Part8          STRING(12)
!Part9          STRING(12)
!Refurb1        STRING(12)
!Refurb2        STRING(12)
!Refurb3        STRING(12)
!Refurb4        STRING(12)
!Refurb5        STRING(12)
!Refurb6        STRING(12)
!Refurb7        STRING(12)
!RFS            STRING(12)
!ModelNumber    String(30)
!Re_Rep         STRING(30)
!            .
!Out_Header GROUP,OVER(Ouf:Out_Group),PRE(L2)
!field1         STRING(11)
!field2         STRING(33)
!field3         STRING(11)
!filer          STRING(255)
!filer1         STRING(23)
!            .
!tmp:StartDate        Date()
!tmp:EndDate          Date()
!locBasicFilename        String(255)
!Code
!    Do GetEDIPath
!
!    locBasicFilename = Clip(man:edi_account_number) & '_' & FORMAT(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.RPT'
!    
!    If f:FirstSecondYear
!        locBasicFilename = Clip(f:Manufacturer) & ' 2y.PRT'
!    End !If func:SecondYear
!    Filename = CLIP(CLIP(MAN:EDI_Path)&'\'& locBasicFileName)
!
!    Remove(Out_File)
!    Create(Out_File)
!    Open(Out_File)
!    If Error()
!        If f:Silent = 0
!            Case Missive('Unable to create EDI File.'&|
!              '<13,10>'&|
!              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
!        End ! If f:Silent = 0
!        tmp:Return = 1
!        Return
!    End ! If Error()
!
!    Do Prog:ProgressSetup
!    Prog:TotalRecords = Records(JOBS)
!    Prog:ShowPercentage = 1 !Show Percentage Figure
!    ?Prog:UserString{Prop:Text} = 'Building EDI'
!
!    Access:JOBS.Clearkey(job:EDI_Key)
!    job:Manufacturer = f:Manufacturer
!    job:EDI            = 'NO'
!    Set(job:EDI_Key,job:EDI_Key)
!
!    x# = 1
!    Accept
!        Case Event()
!            Of Event:Timer
!                Loop 25 Times
!                    !Inside Loop
!                    If Access:JOBS.Next()
!                        Prog:Exit = 1
!                        Break
!                    End ! If Access:ORDERS.Next()
!                    If job:Manufacturer <> f:Manufacturer Or |
!                        job:EDI <> 'NO'
!                        Prog:exit = 1
!                        Break
!                    End ! job:EDI <> 'YES'
!
!                    If x# = 1
!                        tmp:StartDate = job:Date_Booked
!                        tmp:EndDate   = tmp:StartDate
!                        x# = 2
!                    Else ! If x# = 1
!                        If job:Date_Booked < tmp:StartDate
!                            tmp:StartDate = job:Date_Booked
!                        End ! If job:Date_Booked < tmp:StartDate
!                        If job:Date_Booked > tmp:EndDate
!                            tmp:EndDate = job:Date_Booked
!                        End ! If job:Date_Booked > tmp:EndDate
!                    End ! If x# = 1
!
!                    Prog:RecordCount += 1
!
!                    Do Prog:UpdateScreen
!                End ! Loop 25 Times
!            Of Event:CloseWindow
!    !            Prog:Exit = 1
!    !            Prog:Cancelled = 1
!    !            Break
!            Of Event:Accepted
!                If Field() = ?Prog:Cancel
!                    Beep(Beep:SystemQuestion)  ;  Yield()
!                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
!                                   icon:Question,'&Yes|&No',2,2)
!                        Of 1 ! &Yes Button
!                            tmp:Return = 1
!                            Prog:Exit = 1
!                            Prog:Cancelled = 1
!                            Break
!                        Of 2 ! &No Button
!                    End!Case Message
!                End ! If Field() = ?ProgressCancel
!        End ! Case Event()
!        If Prog:Exit
!            Break
!        End ! If Prog:Exit
!    End ! Accept
!    Do Prog:ProgressFinished
!
!    Do ExportSagem2
!
!    Do Prog:ProgressSetup
!    Prog:TotalRecords = tmp:RecordsCount
!    Prog:ShowPercentage = 1 !Show Percentage Figure
!
!    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
!    jow:Status = 'NO'
!    jow:Manufacturer = f:Manufacturer
!    jow:FirstSecondYear = f:FirstSecondYear
!    jow:ClaimSubmitted = f:StartDate
!    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)
!
!    Accept
!        Case Event()
!            Of Event:Timer
!                Loop 25 Times
!                    !Inside Loop
!                    If Access:JOBSWARR.NEXT()
!                        Prog:Exit = 1
!                        Break
!                    End !If
!                    If jow:Status <> 'NO'   |
!                    Or jow:Manufacturer <> f:Manufacturer   |
!                    Or jow:FirstSecondYear <> f:FirstSecondYear |
!                    Or jow:ClaimSubmitted > f:EndDate
!                        Prog:Exit = 1
!                        Break
!                    End ! If jow:ClaimSubmitted > f:EndDate
!
!                    Access:JOBS.ClearKey(job:Ref_Number_Key)
!                    job:Ref_Number = jow:RefNumber
!                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!                        !Found
!                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!                        !Error
!                        Cycle
!                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!
!                    Do ExportSagem
!                    Do UpdateJOBSE
!
!                    Prog:RecordCount += 1
!
!                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords
!
!                    Do Prog:UpdateScreen
!                End ! Loop 25 Times
!            Of Event:CloseWindow
!    !            Prog:Exit = 1
!    !            Prog:Cancelled = 1
!    !            Break
!            Of Event:Accepted
!                If Field() = ?Prog:Cancel
!                    Beep(Beep:SystemQuestion)  ;  Yield()
!                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
!                                   icon:Question,'&Yes|&No',2,2)
!                        Of 1 ! &Yes Button
!                            Prog:Exit = 1
!                            Prog:Cancelled = 1
!                            Break
!                        Of 2 ! &No Button
!                    End!Case Message
!                End ! If Field() = ?ProgressCancel
!        End ! Case Event()
!        If Prog:Exit
!            Break
!        End ! If Prog:Exit
!    End ! Accept
!    Do Prog:ProgressFinished
!
!    If f:Silent = 0
!        Beep(Beep:SystemAsterisk);  Yield()
!        Case Missive('Technical Report Created.'&|
!            '|'&|
!            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
!                       'midea.jpg','/OK') 
!            Of 1 ! OK Button
!        End ! Case Missive
!    ELSE
!        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
!    End ! If f:Silent = 0
!
!    Close(Out_File)
!
!ExportSagem        Routine
!    Clear(ouf:record)
!    L1:Edi_No       = '"'&Format(Upper(MAN:EDI_Account_Number),@s12)&'";'
!    L1:Order_No     = '"'&UPPER(Strippoint(Format(job:ref_number,@s25)))&'";'
!    L1:Customer     = '"'&UPPER(Format(job:surname,@s25))&'";'
!    L1:Warranty     = '"UW";'
!    IMEIError# = 0
!    If job:Third_Party_Site <> ''
!        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
!        jot:RefNumber = job:Ref_Number
!        Set(jot:RefNumberKey,jot:RefNumberKey)
!        If Access:JOBTHIRD.NEXT()
!            IMEIError# = 1
!        Else !If Access:JOBTHIRD.NEXT()
!            If jot:RefNumber <> job:Ref_Number
!                IMEIError# = 1
!            Else !If jot:RefNumber <> job:Ref_Number
!                IMEIError# = 0
!            End !If jot:RefNumber <> job:Ref_Number
!        End !If Access:JOBTHIRD.NEXT()
!    Else !job:Third_Party_Site <> ''
!        IMEIError# = 1
!    End !job:Third_Party_Site <> ''
!
!    If IMEIError# = 1
!        L1:IMEI_In      = '"'&UPPER(Format(job:esn,@s15))&'";'
!        L1:Sag_Ref_In   = '"'&UPPER(Format(job:msn,@s9))&'";'
!    Else !IMEIError# = 1
!        L1:IMEI_In      = '"'&UPPER(Format(jot:OriginalIMEI,@s15))&'";'
!        L1:Sag_Ref_In   = '"'&UPPER(Format(jot:OriginalMSN,@s9))&'";'
!    End !IMEIError# = 1
!    L1:IMEI_Out      = '"'&UPPER(Format(job:esn,@s15))&'";'
!    L1:Sag_Ref_Out   = '"'&UPPER(Format(job:msn,@s9))&'";'
!
!    L1:Date_Code    = '"'&UPPER(Format(job:fault_code8,@s7))&'";'
!    access:exchange.clearkey(xch:ref_number_key)
!    xch:ref_number = job:exchange_unit_number
!    if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
!        L1:IMEI_Out     = '"'&UPPER(Format(xch:esn,@s15))&'";'
!        L1:Sag_Ref_Out  = '"'&UPPER(Format(xch:msn,@s9))&'";'
!    End!if access:exchange.tryfetch(xch:ref_number_key)
!    L1:Battery      = '"'&UPPER(Format(job:fault_code3,@s9))&'";'
!    L1:Charger      = '"'&UPPER(Format(job:fault_code4,@s9))&'";'
!    L1:Antenna      = '"'&UPPER(Format(job:fault_code5,@s3))&'";'
!    If job:dop <> ''
!        L1:DOP          = '"'&UPPER(Format(FORMAT(DAY(job:dop),@n02)&FORMAT(MONTH(job:dop),@n02)&YEAR(job:dop),@s8))&'";'
!    Else!If l1:dop <> ''
!        l1:dop          = '"        ";'
!    End!If l1:dop <> ''
!    L1:Booked       = '"'&UPPER(Format(FORMAT(DAY(job:date_booked),@n02)&FORMAT(MONTH(job:date_booked),@n02)&YEAR(job:date_booked),@s8))&'";'
!    !loop through contacts to get dates for the estimate!
!    found_out# = 0
!    found_in# = 0
!
!    access:audit.clearkey(aud:ref_number_key)
!    aud:ref_number = job:ref_number
!    set(aud:ref_number_key,aud:ref_number_key)
!    loop
!        if access:audit.next()
!           break
!        end !if
!        if aud:ref_number <> job:ref_number      |
!            then break.  ! end if
!        If Instring(aud:action,1,1) = '520 ESTIMATE SENT'
!            L1:Est_Out      = '"'&UPPER(Format(FORMAT(DAY(aud:date),@n02)&FORMAT(MONTH(aud:date),@n02)&YEAR(aud:date),@s8))&'";'
!            found_out# = 1
!        End!If Instring(aud:action,1,1) = '520 ESTIMATE SENT'
!        If instring(aud:action,1,1) = '535 ESTIMATE ACCEPTED'
!            L1:Est_In       = '"'&UPPER(Format(FORMAT(DAY(aud:date),@n02)&FORMAT(MONTH(aud:date),@n02)&YEAR(aud:date),@s8))&'";'
!            found_in# = 1
!        End!If instring(aud:action,1,1) = '535 ESTIMATE ACCEPTED'
!    end !loop
!
!    If found_out# = 0
!        l1:est_out  = '"        ";'
!    End!If found_out# = 0
!    If found_out# = 0
!        l1:est_in   = '"        ";'
!    End!If found_out# = 0
!    If job:Exchange_Despatched <> ''
!        L1:Despatched   = '"'&UPPER(Format(FORMAT(DAY(job:Exchange_Despatched),@n02)&FORMAT(MONTH(job:Exchange_Despatched),@n02)&YEAR(job:Exchange_Despatched),@s8))&'";'
!    Else !If job:Exchange_Despatched <> ''
!        L1:Despatched   = '"'&UPPER(Format(FORMAT(DAY(job:date_completed),@n02)&FORMAT(MONTH(job:Date_completed),@n02)&YEAR(job:date_completed),@s8))&'";'
!    End !If job:Exchange_Despatched <> ''
!
!
!    L1:Cust_Code    = '"'&UPPER(Format(job:fault_code6,@s5))&'";'
!    L1:Fault_Code   = '"'&UPPER(Format(job:fault_code7,@s5))&'";'
!    L1:Soft_In      = '"'&(Format(job:fault_code1,@s11))&'";'
!    L1:Soft_Out     = '"'&(Format(job:fault_code2,@s11))&'";'
!    access:reptydef.clearkey(rtd:warranty_key)
!    rtd:warranty    = 'YES'
!    rtd:repair_type = job:repair_type_warranty
!    if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
!        L1:Repair_Level = '"' & Upper(Format(rtd:WarrantyCode,@s5))&'";'
!    Else!if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
!        L1:Repair_Level = '"'&UPPER(Format(job:repair_type_warranty,@s5))&'";'
!    End!if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
!
!    xp#=0
!
!    access:warparts.clearkey(wpr:part_number_key)
!    wpr:ref_number  = job:ref_number
!    set(wpr:part_number_key,wpr:part_number_key)
!    loop
!        if access:warparts.next()
!           break
!        end !if
!        if wpr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!            Cycle
!        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!        If wpr:Part_Number = 'EXCH'
!            Cycle
!        End !If wpr:Part_Number = 'EXCH'
!
!        xp#+=1
!        CASE xp#
!          OF 1
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part1 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part1 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 2
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part2 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part2 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 3
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part3 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part3 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 4
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part4 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part4 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 5
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part5 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part5 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 6
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part6 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part6 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:art_number = 'ADJUSTMENT'
!          OF 7
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part7 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part7 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 8
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part8 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part8 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!          OF 9
!            If wpr:part_number = 'ADJUSTMENT'
!                L1:part9 = '"'&Format('',@s9)&'";'
!            Else!If wpr:part_number = 'ADJUSTMENT'
!                L1:part9 = '"'&Format(wpr:part_number,@s9)&'";'
!            End!If wpr:part_number = 'ADJUSTMENT'
!
!        END
!
!    end !loop
!
!    IF l1:part1 = ''
!      L1:part1 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part2 = ''
!      L1:part2 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part3 = ''
!      L1:part3 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part4 = ''
!      L1:part4 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part5 = ''
!      L1:part5 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part6 = ''
!      L1:part6 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part7 = ''
!      L1:part7 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part8 = ''
!      L1:part8 = '"'&Format('',@s9)&'";'
!    END
!    IF l1:part9 = ''
!      L1:part9 = '"'&Format('',@s9)&'";'
!    END
!    L1:refurb1 = '"'&Format('',@s9)&'";'
!    L1:refurb2 = '"'&Format('',@s9)&'";'
!    L1:refurb3 = '"'&Format('',@s9)&'";'
!    L1:refurb4 = '"'&Format('',@s9)&'";'
!    L1:refurb5 = '"'&Format('',@s9)&'";'
!    L1:refurb6 = '"'&Format('',@s9)&'";'
!    L1:refurb7 = '"'&Format('',@s9)&'";'
!    L1:RFS     = '"'&UPPER(Format(FORMAT(DAY(job:date_completed),@n02)&FORMAT(MONTH(job:Date_Completed),@n02)&YEAR(job:Date_Completed),@s8))&'";'
!    L1:ModelNumber   = '"'&Format(job:model_number,@s30)&'";'
!    L1:Re_Rep  = '"'&UPPER(Format(job:fault_code9,@s30))&'"'
!
!    Append(out_file)
!
!ExportSagem2        Routine
!    CLEAR(ouf:RECORD)
!
!    Set(defaults)
!    access:defaults.next()
!
!    L2:Field1 = '"'&Format('UK',@s8)&'";'
!    L2:Field2 = '"'&Format(def:User_Name,@s30)&'";'
!    L2:Field3 = '"'&Format(man:EDI_Account_Number,@s8)&'"'
!    Append(out_file)
!    L2:Field1 = '"'&Format(FORMAT(DAY(TODAY()),@n02)&FORMAT(MONTH(TODAY()),@n02)&YEAR(TODAY()),@s8)&'";'
!    L2:Field2 = '"'&Format(FORMAT(DAY(tmp:StartDate),@n02)&FORMAT(MONTH(tmp:StartDate),@n02)&YEAR(tmp:StartDate),@s30)&'";'
!    L2:Field3 = '"'&Format(FORMAT(DAY(tmp:EndDate),@n02)&FORMAT(MONTH(tmp:EndDate),@n02)&YEAR(tmp:EndDate),@s8)&'"'
!    Append(out_file)
!! Changing (DBH 05/06/2008) # 9792 - No more batch numbers
!!    L2:Field1 = '"'&Format(Strippoint(Upper(Clip(f:BatchNumber))),@s8)&'";'
!! to (DBH 05/06/2008) # 9792
!    L2:Field1 = '""'
!! End (DBH 05/06/2008) #9792
!    L2:Field2 = '""'
!    L2:Field3 = ''
!    Append(out_file)
!    CLEAR(ouf:RECORD)
!    LOOP h# = 1 to 7
!      L2:Field1 = ''
!      Append(out_file)
!    END
!    L1:Edi_NO       = '"'&Format('A0',@s12)&'";'
!    L1:Order_No     = '"'&format('A1',@s25)&'";'
!    L1:Customer     = '"'&format('A2',@s25)&'";'
!    L1:Warranty     = '"'&Format('A3',@s2)&'";'
!    L1:IMEI_In      = '"'&Format('A4',@s15)&'";'
!    L1:Sag_Ref_In   = '"'&Format('A5',@s9)&'";'
!    L1:Date_Code    = '"'&Format('A6',@s7)&'";'
!    L1:IMEI_Out     = '"'&Format('A7',@s15)&'";'
!    L1:Sag_Ref_Out  = '"'&Format('A8',@s9)&'";'
!    L1:Battery      = '"'&Format('A9',@s9)&'";'
!    L1:Charger      = '"'&Format('A10',@s9)&'";'
!    L1:Antenna      = '"'&Format('A11',@s3)&'";'
!    L1:DOP          = '"'&Format('A12',@s8)&'";'
!    L1:Booked       = '"'&Format('A13',@s8)&'";'
!    L1:Est_Out      = '"'&FOrmat('A14',@s8)&'";'
!    L1:Est_In       = '"'&Format('A15',@s8)&'";'
!    L1:Despatched   = '"'&Format('A16',@s8)&'";'
!    L1:Cust_Code    = '"'&Format('A17',@s5)&'";'
!    L1:Fault_Code   = '"'&Format('A18',@s5)&'";'
!    L1:Soft_In      = '"'&Format('A19',@s11)&'";'
!    L1:Soft_Out     = '"'&Format('A20',@s11)&'";'
!    L1:Repair_Level = '"'&Format('A21',@s5)&'";'
!    L1:part1        = '"'&Format('A22',@s9)&'";'
!    L1:part2        = '"'&Format('A23',@s9)&'";'
!    L1:part3        = '"'&Format('A24',@s9)&'";'
!    L1:part4        = '"'&Format('A25',@s9)&'";'
!    L1:part5        = '"'&Format('A26',@s9)&'";'
!    L1:part6        = '"'&Format('A27',@s9)&'";'
!    L1:part7        = '"'&Format('A28',@s9)&'";'
!    L1:part8        = '"'&Format('A29',@s9)&'";'
!    L1:part9        = '"'&Format('A30',@s9)&'";'
!    L1:Refurb1      = '"'&Format('A31',@s9)&'";'
!    L1:Refurb2      = '"'&Format('A32',@s9)&'";'
!    L1:Refurb3      = '"'&Format('A33',@s9)&'";'
!    L1:Refurb4      = '"'&Format('A34',@s9)&'";'
!    L1:Refurb5      = '"'&Format('A35',@s9)&'";'
!    L1:Refurb6      = '"'&Format('A36',@s9)&'";'
!    L1:Refurb7      = '"'&Format('A37',@s9)&'";'
!    L1:RFS          = '"'&Format('A38',@s9)&'";'
!    L1:ModelNumber  = '"'&Format('A39',@s9)&'";'
!    L1:Re_Rep       = '"'&Format('A40',@s9)&'"'
!
!    Append(out_file)
!
local.SagemExport   Procedure()
qJobsList               QUEUE()
AccountNumber               STRING(30)
JobNumber                   LONG()
                        END
qAccountsList           QUEUE()
AccountNumber               STRING(30)
MRRCODE                     STRING(30)
FirstRepairDate             DATE()
LastRepairDate              DATE()
                        END

locBasicFilename        String(255)
locDateBooked           DATE()
locTimeBooked           TIME()
locGenDate              DATE()
locGenTime              TIME()

qFileNames                      QUEUE()
ExportFileName                      STRING(10)
                                end
locFilenameList                 STRING(255)

    CODE
    
        Access:JOBSWARR.Clearkey(jow:ClaimStatusManKey)
        jow:Status   = 'NO'
        jow:Manufacturer     = f:Manufacturer
        jow:ClaimSubmitted   = f:StartDate
        Set(jow:ClaimStatusManKey,jow:ClaimStatusManKey)
        LOOP UNTIL Access:JOBSWARR.Next()
            IF (jow:Status <> 'NO' OR |
                jow:Manufacturer <> f:Manufacturer OR |
                jow:ClaimSubmitted > f:EndDate)
                BREAK
            END

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number = jow:RefNumber
            IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))
                CYCLE
            END

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.Tryfetch(wob:RefNumberKey))
                CYCLE
            END

            Access:ASVACC.Clearkey(asv:TradeAccManufKey)
            asv:TradeAccNo = wob:HeadAccountNumber
            asv:Manufacturer = f:Manufacturer
            IF (Access:ASVACC.TryFetch(asv:TradeAccManufKey))
                CYCLE
            END

            qAccountsList.AccountNumber = wob:HeadAccountNumber
            qAccountsList.MRRCODE = asv:ASVCode
            GET(qAccountsList,qAccountsList.AccountNumber)
            IF (ERROR())
                qAccountsList.FirstRepairDate = job:Date_Booked
                qAccountsList.LastRepairDate = job:Date_Booked
                ADD(qAccountsList)
            ELSE
                If (job:Date_Booked < qAccountsList.FirstRepairDate)
                    qAccountsList.FirstRepairDate = job:Date_Booked
                    PUT(qAccountsList)
                END
                IF (job:Date_Booked > qAccountsList.LastRepairDate)
                    qAccountsList.LastRepairDate = job:Date_Booked
                    PUT(qAccountsList)
                END
            END

            qJobsList.JobNumber = job:Ref_Number
            qJobsList.AccountNumber = wob:HeadAccountNumber
            GET(qJobsList,qJobsList.JobNumber)
            If (ERROR())
                ADD(qJobsList)
            END

        END

        SORT(qAccountsList,qAccountsList.AccountNumber)
        SORT(qJobsList,qJobsList.AccountNumber)
        IF (RECORDS(qAccountsList))
            LOOP x# = 1 To RECORDS(qAccountsList)
                GET(qAccountsList,x#)

                header# = 1

                LOOP y# = 1 To RECORDS(qJobsList)
                    GET(qJobsList,y#)
                    IF (qJobsList.AccountNumber <> qAccountsList.AccountNumber)
                        CYCLE
                    END


                    IF (header# = 1)

                        Close(ExportFile)

                        locBasicFilename = Clip(qAccountsList.MRRCODE) & '_' & FORMAT(Year(Today()),@n04) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) & '.RPT'
                        qFileNames.ExportFileName = locBasicFilename
                        ADD(qFileNames)

                        tmp:ExportFile = CLIP(CLIP(MAN:EDI_Path)&'\'& locBasicFileName)

                        Remove(ExportFile)
                        Create(ExportFile)
                        Open(ExportFile)

                        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                        tra:Account_Number = qAccountsList.AccountNumber
                        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

                            Do Export:Header
                            header# = 0
                        END ! IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                    END !If (header# = 1)

                    Do Export:DataLine

                END ! LOOP y# = 1 To RECORDS(qJobsList)
                CLOSE(ExportFile)
            END
        END ! IF (RECORDS(qAccountsList))

        LOOP xx# = 1 TO RECORDS(qFilenames)
            GET(qFileNames,xx#)
            locFilenameList = CLIP(locFilenameList) & ' ' & CLIP(qFilenames.ExportFileName)
        END

        If f:Silent = 0
            Beep(Beep:SystemAsterisk);  Yield()
            Case Missive('Technical Report Created.'&|Ex
                '|Folder:' & CLIP(man:Edi_Path) & |
                '|' & Clip(locFilenameList) & '.','ServiceBase 3g',|
                'midea.jpg','/OK')
            Of 1 ! OK Button
            End ! Case Missive
        ELSE
            f:FileList = locFilenameList
        End ! If f:Silent = 0




Export:Header       ROUTINE
    ! Line 1
    ! Country
    ExportLine('RSA',';','BEGIN')
    ! ARC Name
    ExportLine(tra:Company_Name,';')
    ! ARC Code
    ExportLine(qAccountsList.MRRCODE,';','END')

    ! Line 2
    ! Edition Date
    ExportLine(Format(Today(),@d06),';','BEGIN')
    ! Date Of First Repair
    ExportLine(Format(qAccountsList.FirstRepairDate,@d06),';')
    ! Date Of Last Repair
    ExportLine(Format(qAccountsList.LastRepairDate,@d06),';','END')

    ! Line 3
    ExportLine(';;;;','','BEGIN',1)

    ! Line 4
    ! Version Number
    ExportLine(man:SagemVersionNumber,'','BEGIN',1)

    ! Line 5
    ExportLine('','','BLANK')

    ! Line 6
    ! File name
    ExportLine(locBasicFilename,'','BEGIN',1)

    ! Line 7 - 10
    ExportLine('','','BLANK')
    ExportLine('','','BLANK')
    ExportLine('','','BLANK')
    ExportLine('','','BLANK')
    ! Title
    ExportLine('A0',';','BEGIN')
    col# = 1
    Loop col# = 1 to 44
        ExportLine('A' & col#,';')
    END
    ExportLine('A45',';','END')

Export:DataLine     ROUTINE
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = qJobsList.JobNumber
    IF (Access:JOBS.TryfetcH(job:Ref_Number_Key))
        EXIT
    END
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    IF (Access:WEBJOB.Tryfetch(wob:RefNumberKey))
        EXIT
    END
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:ReFNumber = job:Ref_Number
    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
        EXIT
    END
    ! A0: ARC Code
    ExportLine(qAccountsList.MRRCode,';','BEGIN')
    ! A1: Job Number
    ExportLine(job:Ref_Number,';')
    ! A2: Customer Name
    If (job:Surname <> '')
        ExportLine(CLIP(job:Title) & ' ' & CLIP(job:Initial) & ' ' & CLIP(job:Surname),';')
    ELSE
        ExportLine(job:Company_Name,';')
    END
    ! A3: Warranty Status
    ExportLine('1',';')
    ! A4: Incoming IMEI Number
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''
    If IMEIError# = 1
        ExportLine(job:ESN,';')
    Else !IMEIError# = 1
        ExportLine(jot:OriginalIMEI,';')
    End !IMEIError# = 1
    ! A5: Incomging Sagem Reference
    ExportLine(job:Fault_Code1,';')
    ! A6: Production Date
    ExportLine(job:Fault_Code2,';')
    ! A7: Outgoing IMEI
    ! Only show if it's exchange or 3rd party exchange
    IF (job:Exchange_Unit_Number > 0)
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            ExportLine(job:ESN,';')
        ELSE
            ExportLine(xch:ESN,';')
        END
    ELSE
        ExportLine(job:ESN,';')
    END
    ! A8: Swap Sagem Reference
    ExportLine(job:Fault_Code3,';')
    ! A9: Incoming Battery Presence
    Access:JOBACC.Clearkey(jac:Ref_Number_Key)
    jac:Ref_Number = job:Ref_Number
    jac:Accessory = 'BATTERY'
    IF (Access:JOBACC.TryFetch(jac:Ref_Number_Key))
        ExportLine('0',';')
    ELSE
        ExportLine('1',';')
    END
    ! A10: Incoming Charger Prescence
    Access:JOBACC.Clearkey(jac:Ref_Number_Key)
    jac:Ref_Number = job:Ref_Number
    jac:Accessory = 'CHARGER'
    IF (Access:JOBACC.TryFetch(jac:Ref_Number_Key))
        ExportLine('0',';')
    ELSE
        ExportLine('1',';')
    END
    ! A11: Incoming Antenna Prescence
    Access:JOBACC.Clearkey(jac:Ref_Number_Key)
    jac:Ref_Number = job:Ref_Number
    jac:Accessory = 'ANTENNA'
    IF (Access:JOBACC.TryFetch(jac:Ref_Number_Key))
        ExportLine('0',';')
    ELSE
        ExportLine('1',';')
    END
    ! A12: Date Of Purchase
    ExportLine(FORMAT(job:DOP,@d06),';')
    ! A13: Receiption Date in ARC
    locDateBooked = job:Date_Booked
    locTimeBooked = job:Time_Booked
    GetBookingDate(job:Ref_Number,locDateBooked,locTimeBooked,job:Exchange_Unit_Number,jobe:ExchangedATRRC)
    ExportLine(FORMAT(locDateBooked,@d06),';')
    ! A14: Quotation Date
    ExportLine('',';')
    ! A15: Quotations Acceptance Date
    ExportLine('',';')
    ! A16: Date Of Shipment
    IF (job:Exchange_Unit_Number > 0)
        locGenDate = 0
        locGenTime = 0
        GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locGenDate,locGenTime,'EXC')
        IF (locGenDate <> 0)
            ExportLine(FORMAT(locGenDate,@d06),';')
        ELSE
            ExportLine('',';')
        END
    ELSE ! IF (job:Exchange_Unit_Number > 0)
        found# = 0
        Access:AUDSTATS.Clearkey(aus:StatusTimeKey)
        aus:RefNumber = job:Ref_Number
        aus:Type = 'JOB'
        aus:DateChanged = 0
        SET(aus:StatusTimeKey,aus:StatusTimeKey)
        LOOP UNTIL Access:AUDSTATS.Next()
            IF (aus:RefNumber <> job:Ref_Number OR |
                aus:Type <> 'JOB')
                BREAK
            END
            IF (SUB(aus:NewStatus,1,3) = '705')
                ExportLine(FORMAT(aus:DateChanged,@d06),';')
                found# = 1
                BREAK
            END
        END
        IF (found# = 0)
            ExportLine('',';')
        END   
    END ! IF (job:Exchange_Unit_Number > 0)
    ! A17: Sagem Symptom Code Declared By Customer
    ExportLine(job:Fault_Code4,';')
    ! A18: Sagem Symptom Code Confirmed By Technician
    ExportLine(job:Fault_Code5,';')
    ! A19: Incoming Software Version
    ExportLine(job:Fault_Code6,';')
    ! A20: Outgoing Softweare Version
    ExportLine(job:Fault_Code7,';')
    ! A21: Repair Level
    ExportLine(job:Fault_Code8,';')
    ! A22 - A29 Part Number
    CountParts# = 0
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
    LOOP UNTIL Access:WARPARTS.Next()
        IF (wpr:Ref_Number <> job:Ref_Number)
            BREAK
        END
        CountParts# += 1
        ExportLine(wpr:Part_Number,';')
        If (CountParts# >= 8)
            BREAK
        END
    END
    Loop rest# = CountParts# To 7
        ExportLine('',';')
    END
    ! A30: A37 Blank
    LOOP 8 TIMES
        ExportLine('',';')
    END
    ! A38: Specific Process
    found# = 0
    Access:JOBRPNOT.Clearkey(jrn:TheDateKey)
    jrn:RefNumber = job:Ref_Number
    jrn:TheDate = 0
    Set(jrn:TheDateKey,jrn:TheDateKey)
    LOOP UNTIL Access:JOBRPNOT.Next()
        IF (jrn:RefNumber <> job:Ref_Number)
            BREAK
        END
        ExportLine(jrn:Notes,';',,,8)
        found# = 1
        BREAK
    END
    IF (Found# = 0)
        ExportLine('',';')
    END
    ! A39: Date Of Receipting In RRC From Customer
    If (jobe:WebJob = 0)
        ExportLine('',';')
    ELSE
        ! Booking date, unless come from VCP
        locGenDate = job:Date_Booked
        IF (job:Who_Booked = 'WEB')
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'JOB'
            aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
            SET(aud:TypeActionKey,aud:TypeActionKey)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> job:Ref_Number OR |
                    aud:Type <> 'JOB' OR |
                    aud:ACtion <> 'UNIT RECEIVED AT RRC FROM PUP')
                    BREAK
                END
                locGenDate = aud:Date
                
                BREAK
            END
        END
        ExportLine(Format(locGenDate,@d06),';')
    END
    ! A40: Date Of Shipment From RRC To ARC
    IF (jobe:WebJob = 0)
        ExportLine('',';')
    ELSE
        ! Use despatched to ARC status
        locGenDate = 0
        Access:AUDSTATS.Clearkey(aus:StatusTimeKey)
        aus:RefNumber = job:Ref_Number
        aus:Type = 'JOB'
        aus:DateChanged = 0
        SET(aus:StatusTimeKey,aus:StatusTimeKey)
        LOOP UNTIL Access:AUDSTATS.Next()
            IF (aus:RefNumber <> job:Ref_Number OR |
                aus:Type <> 'JOB')
                BREAK
            END
            IF (SUB(aus:NewStatus,1,3) = Sub(GETINI('RRC','StatusDespatchedToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
                locGenDate = aus:DateChanged
                BREAK
            END
        END
        If (locGenDate = 0)
            ExportLine('',';')
        ELSE
            ExportLine(Format(locGenDate,@d06),';')
        END
    END
    ! A41: Date Receipting In RRC From ARC
    IF (jobe:WebJob = 0 Or SentToHub(job:Ref_Number) = 0)
        ExportLine('',';')
    ELSE
        locGenDate = 0
        locGenTime = 0
        IF (job:Exchange_Unit_Number > 0)
            GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locGenDate,locGenTime,'EXC')
        ELSE
            ! Use date received back from ARC
            locGenDate = 0
            Access:LOCATLOG.Clearkey(lot:DateKey)
            lot:RefNumber = job:Ref_Number
            SET(lot:DateKey,lot:DateKey)
            LOOP UNTIL Access:LOCATLOG.Next()
                IF (lot:RefNumber <> job:Ref_Number)
                    BREAK
                END
                IF (lot:NewLocation = Clip(GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')) AND |
                    lot:PreviousLocation = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')))
                    locGenDate = lot:TheDate
                    BREAK
                END
            END
        END
        If (locGenDate = 0)
            ExportLine('',';')
        ELSE
            ExportLine(Format(locGenDate,@d06),';')
        END
    END
    ! A42:  Date of collection by end-customer in the collecting point
    found# = 0
    locGenDate = 0
    Access:AUDSTATS.Clearkey(aus:StatusTimeKey)
    aus:RefNumber = job:Ref_Number
    IF (job:Exchange_Unit_Number > 0)
        ! If exchanged, check when the exchange was despatched
        aus:Type = 'EXC'
    ELSE
        aus:Type = 'JOB'
    END
    aus:DateChanged = 0
    SET(aus:StatusTimeKey,aus:StatusTimeKey)
    LOOP UNTIL Access:AUDSTATS.Next()
        IF (aus:RefNumber <> job:Ref_Number)
            BREAK
        END
        If (job:Exchange_Unit_Number > 0)
            If (aus:Type <> 'EXC')
                BREAK
            END
        ELSE
            IF (aus:Type <> 'JOB')
                BREAK
            END
        END
        IF (INLIST(SUB(aus:NewStatus,1,3),'901','905','910','464','468'))
            locGenDate = aus:DateChanged
        END
    END
    If (locGenDate = 0)
        ExportLine('',';')
    ELSE
        ExportLine(Format(locGenDate,@d06),';')
    END 
    ! A43 = A44 Blank
    ExportLine('',';')
    ExportLine('',';')
    ! 45: Faulty Element
    ExportLine(job:Fault_Code9,';','END')
        
    
    
    
    
           
        
    
!local.SamsungSAExport        Procedure(Byte f:FirstSecondYear)     ! #11813 New process (Bryan: 30/11/2010)
!FileSamsungSA    File,Driver('ASCII'),Pre(SamsungSA),Name(Filename),Create,Bindable,Thread
!Record              Record
!Line1                    String(2000)
!                    End
!                End
!Code
!    Do GetEDIPath
!
!    Filename = Clip(man:EDI_Path) & '\SAM.' & Sub(man:EDI_Account_Number,1,2) & '_'
!    If f:FirstSecondYear
!        Filename = Clip(man:EDI_Path) & '\' & Clip(f:Manufacturer) & ' 2y .' & Sub(man:EDI_Account_Number,1,2) & '_'
!    End !If func:SecondYear
!
!    Remove(FileSamsungSA)
!    Create(FileSamsungSA)
!    Open(FileSamsungSA)
!    If Error()
!        If f:Silent = 0
!            Case Missive('Unable to create EDI File.'&|
!              '<13,10>'&|
!              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
!        End ! If f:Silent = 0
!        tmp:Return = 1
!        Return
!    End ! If Error()
!
!
!    Do Prog:ProgressSetup
!    Prog:TotalRecords = tmp:RecordsCount
!    Prog:ShowPercentage = 1 !Show Percentage Figure
!
!    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
!    jow:Status = 'NO'
!    jow:Manufacturer = f:Manufacturer
!    jow:FirstSecondYear = f:FirstSecondYear
!    jow:ClaimSubmitted = f:StartDate
!    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)
!
!    Accept
!        Case Event()
!            Of Event:Timer
!                Loop 25 Times
!                    !Inside Loop
!                    If Access:JOBSWARR.NEXT()
!                        Prog:Exit = 1
!                        Break
!                    End !If
!                    If jow:Status <> 'NO'   |
!                    Or jow:Manufacturer <> f:Manufacturer   |
!                    Or jow:FirstSecondYear <> f:FirstSecondYear |
!                    Or jow:ClaimSubmitted > f:EndDate
!                        Prog:Exit = 1
!                        Break
!                    End ! If jow:ClaimSubmitted > f:EndDate
!
!                    Access:JOBS.ClearKey(job:Ref_Number_Key)
!                    job:Ref_Number = jow:RefNumber
!                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!                        !Found
!                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!                        !Error
!                        Cycle
!                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!
!                    ! Insert --- Open webjob to get AVC number correctly (DBH: 18/01/2010) #11261
!                    Access:WEBJOB.Clearkey(wob:refNumberKey)
!                    wob:refNumber    = job:ref_Number
!                    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
!                        ! Found
!                    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
!                        ! Error
!                        Cycle
!                    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
!                    ! end --- (DBH: 18/01/2010) #11261
!
!
!                    Do ExportSamsungSA
!                    Do UpdateJOBSE
!
!                    Prog:RecordCount += 1
!
!                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords
!
!                    Do Prog:UpdateScreen
!                End ! Loop 25 Times
!            Of Event:CloseWindow
!    !            Prog:Exit = 1
!    !            Prog:Cancelled = 1
!    !            Break
!            Of Event:Accepted
!                If Field() = ?Prog:Cancel
!                    Beep(Beep:SystemQuestion)  ;  Yield()
!                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
!                                   icon:Question,'&Yes|&No',2,2)
!                        Of 1 ! &Yes Button
!                            tmp:Return = 1
!                            Prog:Exit = 1
!                            Prog:Cancelled = 1
!                            Break
!                        Of 2 ! &No Button
!                    End!Case Message
!                End ! If Field() = ?ProgressCancel
!        End ! Case Event()
!        If Prog:Exit
!            Break
!        End ! If Prog:Exit
!    End ! Accept
!    Do Prog:ProgressFinished
!
!    If f:Silent = 0
!        Beep(Beep:SystemAsterisk);  Yield()
!        Case Missive('Technical Report Created.'&|
!            '|'&|
!            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
!                       'midea.jpg','/OK') 
!            Of 1 ! OK Button
!        End ! Case Missive
!    End ! If f:Silent = 0
!
!    Close(FileSamsungSA)
!
!ExportSamsungSA        Routine
!    Clear(SamsungSA:record)
!    SamsungSA:Line1   =  Format(man:EDI_Account_Number,@s8)
!    SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:Ref_Number,@s12)
!    SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>CI'
!    !Show "NONE" if there is no name - L939 (DBH: 01-09-2003)
!    If job:Surname = ''
!        ! Insert --- Show Company Name If No Name (DBH: 14/12/2009) #11214
!        if (job:Company_Name = '')
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format('NONE',@s20)
!        else ! if (job:Company_Name = '')
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(clip(job:Company_Name),@s20)
!        end ! if (job:Company_Name = '')
!        ! end --- (DBH: 14/12/2009) #11214
!    Else !If job:Surname = ''
!        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(CLIP(job:Title)&' '&CLIP(job:Surname),@s20)
!    End !If job:Surname = ''
!
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(CLIP(job:Address_Line1),@s50)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(CLIP(job:Address_Line2),@s50)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(CLIP(job:Address_Line3),@s50)
!    !Show "NONE" if there is no telephone number - L939 (DBH: 01-09-2003)
!    If job:Telephone_Number = ''
!        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format('NONE',@s20)
!    Else !If job:Telephone_Number = ''
!        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:Telephone_Number,@s20)
!    End !If job:Telephone_Number = ''
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(CLIP(job:Postcode),@s50)
!
!    ! Insert --- Show the new AVC Code (DBH: 14/12/2009) #11214
!    If jow:RepairedAt = 'ARC' then
!        !this is an ARC job - so look up th head account
!        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
!        ASV:TradeAccNo      = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!        ASV:Manufacturer    = 'SAMSUNG'
!        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
!            !entry exists - does it contain a code?
!            If clip(ASV:ASVCode) <> '' then
!                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & clip(asv:ASVCode)
!                !!e1.writeToCell(asv:ASVCode,e1.columnName(55) & tmp:currentRow)
!            End !If clip(ASV:ASVCode) <> '' then
!        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
!    Else
!        !this is an RRC job - so look up the RRC account
!        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
!        ASV:TradeAccNo      = wob:HeadAccountNumber
!        ASV:Manufacturer    = 'SAMSUNG'
!        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
!            !entry exists - does it contain a code?
!            If clip(ASV:ASVCode) <> '' then
!                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & clip(asv:ASVCode)
!                !!e1.writeToCell(asv:ASVCode,e1.columnName(55) & tmp:currentRow)
!            End !If clip(ASV:ASVCode) <> '' then
!        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
!    End !If x# = True then
!    ! end --- (DBH: 14/12/2009) #11214
!    
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(clip(job:Model_Number),@s20)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:msn,@s14)
!
!    IMEIError# = 0
!    If job:Third_Party_Site <> ''
!        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
!        jot:RefNumber = job:Ref_Number
!        Set(jot:RefNumberKey,jot:RefNumberKey)
!        If Access:JOBTHIRD.NEXT()
!            IMEIError# = 1
!        Else !If Access:JOBTHIRD.NEXT()
!            If jot:RefNumber <> job:Ref_Number
!                IMEIError# = 1
!            Else !If jot:RefNumber <> job:Ref_Number
!                IMEIError# = 0
!            End !If jot:RefNumber <> job:Ref_Number
!        End !If Access:JOBTHIRD.NEXT()
!    Else !job:Third_Party_Site <> ''
!        IMEIError# = 1
!    End !job:Third_Party_Site <> ''
!    If IMEIError# = 1
!        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:esn,@s15)
!    Else !IMEIError# = 1
!        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(jot:OriginalIMEI,@s15)
!    End !IMEIError# = 1
!
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:fault_code3,@s2)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:fault_code1,@s1)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:fault_code2,@s3)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:fault_code4,@s1)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(job:fault_code5,@s3)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(Year(job:dop),@n04) & |
!                                                Format(Month(job:dop),@n02) & |
!                                                Format(Day(job:dop),@n02)
!
!    ! Has the job been estimated??
!    found# = 0
!    Access:AUDSTATS.Clearkey(aus:statusTimeKey)
!    aus:refNumber    = job:ref_Number
!    aus:type    = 'JOB'
!    set(aus:statusTimeKey,aus:statusTimeKey)
!    loop
!        if (Access:AUDSTATS.Next())
!            Break
!        end ! if (Access:AUDSTATS.Next())
!        if (aus:refNumber    <> job:ref_Number)
!            Break
!        end ! if (aus:refNumber    <> job:ref_Number)
!        if (aus:type    <> 'JOB')
!            Break
!        end ! if (aus:type    <> 'JOB')
!        if (instring('ESIMATE SENT',Upper(aus:NewStatus)))
!            found# = 1
!                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(aus:DateChanged),@n04) & |
!                                                    Format(Month(aus:DateChanged),@n02) & |
!                                                Format(Day(aus:DateChanged),@n02)
!
!            break
!        end ! if (instring('ESIMATE SENT',aus:NewStatus))
!    end ! loop
!
!    if (found# = 0)
!        ! Insert --- Use Date Receive AT RRC if PUP, otherwise use date booked (DBH: 14/12/2009) #11214
!        if (job:who_Booked = 'WEB')
!            Access:AUDIT.Clearkey(aud:TypeActionKey)
!            aud:ref_Number    = job:Ref_Number
!            aud:type    = 'JOB'
!            aud:action    = 'UNIT RECEIVED AT RRC FROM PUP'
!            set(aud:TypeActionKey,aud:TypeActionKey)
!            loop
!                if (Access:AUDIT.Next())
!                    Break
!                end ! if (Access:AUDIT.Next())
!                if (aud:ref_Number    <> job:Ref_Number)
!                    Break
!                end ! if (aud:ref_Number    <> job:Ref_Number)
!                if (aud:type    <> 'JOB')
!                    Break
!                end ! if (aud:type    <> 'JOB')
!                if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
!                    Break
!                end ! if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
!
!                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(aud:Date),@n04) & |
!                                                    Format(Month(aud:Date),@n02) & |
!                                                Format(Day(aud:Date),@n02)
!                break
!            end ! loop
!        else ! if (job:who_Booked = 'WEB')
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(job:date_booked),@n04) & |
!                                                    Format(Month(job:date_booked),@n02) & |
!                                                Format(Day(job:date_Booked),@n02)
!        end !if (job:who_Booked = 'WEB')
!    end ! if (found# = 0)
!    ! end --- (DBH: 14/12/2009) #11214
!
!
!!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(job:date_booked),@n04) & |
!!                                            Format(Month(job:date_booked),@n02) & |
!!                                            Format(Day(job:date_Booked),@n02)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(Year(job:date_completed),@N04) & |
!                                            Format(Month(job:date_completed),@n02) & |
!                                            Format(Day(job:date_completed),@n02)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(Year(job:date_completed),@N04) & |
!                                            Format(Month(job:date_completed),@n02) & |
!                                            Format(Day(job:date_completed),@n02)
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!
!    !Next!
!    l# = 0
!    access:warparts.clearkey(wpr:part_number_key)
!    wpr:ref_number  = job:Ref_number
!    set(wpr:part_number_key,wpr:part_number_key)
!    loop
!        if access:warparts.next()
!           break
!        end !if
!        if wpr:ref_number  <> job:Ref_number      |
!            then break.  ! end if
!        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!            Cycle
!        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!        If wpr:Part_Number = 'EXCH'
!            Cycle
!        End !If wpr:Part_Number = 'EXCH'
!        l#+=1
!        IF l# = 8
!          BREAK
!        END
!        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(wpr:fault_code1,@s6)
!        If wpr:Part_Number = 'ADJUSTMENT'
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
!        Else !If wpr:Part_Number = 'ADJUSTMENT'
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(wpr:part_number,@s16)
!            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(wpr:Quantity,@n02)
!        End !If wpr:Part_Number = 'ADJUSTMENT'
!
!    end !loop
!    !ADD(FileSamsungSA)
!    Append(FileSamsungSA)
!    setcursor()
local.SamsungSAExport       PROCEDURE()
FileSamsungSA                   FILE,DRIVER('ASCII'),PRE(SamsungSA),NAME(filename),CREATE,BINDABLE,THREAD
record                              RECORD
Line1                                   STRING(2000)
                                    END
                                END

locLineCount                    BYTE(0)
locFileSuffix                   LONG(0)
qFileNames                      QUEUE()
ExportFileName                      STRING(10)
                                end
locFilenameList                 STRING(255)
locMRRCCode                     LIKE(asv:ASVCode)

totalLineCount                  Equate(1000)
locDate Date()
locTime Time()
    code
        locFileSuffix = 1
        filename = CLIP(man:edi_path) & '\SAM_' & CLIP(locFileSuffix) & '.txt'   ! #11813 Add '.txt' extension (Bryan: 15/12/2010)
        !message('Filename ='&clip(Filename))
        
        qFileNames.ExportFileName = 'SAM_' & CLIP(locFileSuffix) & '.txt'
        ADD(qFileNames)
        
        REMOVE(FileSamsungSA)
        CREATE(FileSamsungSA)
        OPEN(FileSamsungSA)
        IF (ERROR())
            If f:Silent = 0
                Case Missive('Unable to create EDI File.'&|
                    '<13,10>'&|
                    '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                    'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            End ! If f:Silent = 0
            tmp:Return = 1
            Return
        END
        
        Do Prog:ProgressSetup
        Prog:TotalRecords = tmp:RecordsCount
        Prog:ShowPercentage = 1 !Show Percentage Figure

        Access:JOBSWARR.Clearkey(jow:ClaimStatusManKey)
        jow:Status = 'NO'
        jow:Manufacturer = f:Manufacturer
        jow:ClaimSubmitted = f:StartDate
        SET(jow:ClaimStatusManKey,jow:ClaimStatusManKey)
        
        Accept
            Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    IF (Access:JOBSWARR.Next())
                        Prog:Exit = 1
                        BREAK
                    END
                    IF (jow:Status <> 'NO')
                        Prog:Exit = 1
                        BREAK
                    END
                    IF (jow:Manufacturer <> f:Manufacturer)
                        Prog:Exit = 1
                        BREAK
                    END
                    
                    IF (jow:ClaimSubmitted > f:EndDate)
                        Prog:Exit = 1
                        BREAK
                    END
            
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                        CYCLE
                    END

                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
                        CYCLE
                    END
                    
                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                        CYCLE
                    END

                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber = job:Ref_Number
                    IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
                        CYCLE
                    END
                    
                    locLineCount += 1
                    
                    IF (locLineCount > totalLineCount)
                        ! #11813 Only allow 1000 lines per file. (Bryan: 30/11/2010)
                        CLOSE(FileSamsungSA)
                        locFileSuffix += 1
                        filename = CLIP(man:edi_path) & '\SAM_' & CLIP(locFileSuffix) & '.txt'
        
                        REMOVE(FileSamsungSA)
                        CREATE(FileSamsungSA)
                        OPEN(FileSamsungSA)
                        
                        ! Keep a record of files created
                        qFileNames.ExportFileName = 'SAM_' & CLIP(locFileSuffix) & '.txt'
                        ADD(qFileNames)
                        
                    END
                    
                    DO ExportSamsungSA
                    
                    DO UpdateJOBSE
                    
                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                END
                
                    
        
            Of Event:CloseWindow
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                        icon:Question,'&Yes|&No',2,2)
                    Of 1 ! &Yes Button
                        tmp:Return = 1
                        Prog:Exit = 1
                        Prog:Cancelled = 1
                        Break
                    Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
            End ! Case Event()
            If Prog:Exit
                Break
            End ! If Prog:Exit
        End ! Accept
        Do Prog:ProgressFinished
            
        LOOP xx# = 1 TO RECORDS(qFilenames)
            GET(qFileNames,xx#)
            locFilenameList = CLIP(locFilenameList) & ' ' & CLIP(qFilenames.ExportFileName)
        END
            
        If f:Silent = 0
            Beep(Beep:SystemAsterisk);  Yield()
            Case Missive('Technical Report Created.'&|
                '|Folder:' & CLIP(man:Edi_Path) & |
                '|' & Clip(locFilenameList) & '.','ServiceBase 3g',|
                'midea.jpg','/OK') 
            Of 1 ! OK Button
            End ! Case Missive
        ELSE
            f:FileList = locFilenameList
        End ! If f:Silent = 0

        Close(FileSamsungSA)
        
        
ExportSamsungSA     Routine
    DATA
locAccessories  STRING(62)
    CODE
        Clear(SamsungSA:record)
        ! A - Partner
        locMRRCCode = man:EDI_Account_Number
        IF (jow:RepairedAT = 'ARC')
            Access:ASVACC.Clearkey(asv:TradeACCManufKey)
            asv:TradeAccNo = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
            asv:Manufacturer = f:Manufacturer
            IF (Access:ASVACC.TryFetch(asv:TradeACCManufKey) = Level:Benign)
                locMRRCCode = clip(asv:ASVCode)
            END ! IF (Access:ASVACC.TryFetch(asv:TradeACCManufKey) = Level:Benign)

        ELSE ! IF (jow:RepairedAT = 'ARC')
            Access:ASVACC.Clearkey(asv:TradeAccManufKey)
            asv:TradeAccNo = wob:HeadAccountNumber
            asv:Manufacturer = f:Manufacturer
            IF (Access:ASVACC.TryFetch(asv:TradeAccManufKey) = Level:Benign)
                locMRRCCode = clip(asv:ASVCode)
            END ! IF (Access:ASVACC.TryFetch(asv:TradeAccManufKey) = Level:Benign)
        END ! IF (jow:RepairedAT = 'ARC')

        SamsungSA:Line1   =  Sub(locMRRCCode,1,10) !Format(man:EDI_Account_Number,@s8)

        ! B - ASC Claim No
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & job:Ref_Number
        ! C - ASC Job No
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & job:Ref_Number
        ! D - Local Point
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
        ! E - Service Type 
        !SamsungSA:Line1   = CLIP(SamsungSA:Line1) & '<9>CI'
        !tb13222 - J - 17/02/14 changes to  "job service code" job fault with field number 9
        SamsungSA:Line1   = CLIP(SamsungSA:Line1) & '<9>'&clip(job:Fault_Code9)
        !END tb13222

        ! F - Consumer Type
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>1'
        ! G - Consumer Name
        ! H - Consumer Address 1
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & CLIP(tra:Company_Name)
            SamsungSA:Line1 = CLIP(SamsungSA:Line1) & '<9>' & CLIP(tra:Address_Line3)
        ELSE
            SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
            SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
        END
        ! I - Consumer Address 2
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & job:Company_Name
        ! J - Consumer Address 3
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & SUB(Clip(job:Address_Line1) & ' ' & Clip(job:Address_Line2),1,32) !J
        ! K - Region
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
        ! L - Country - added 17/05/12 - JC - TB 12582 - following lines are bumpted to next columns
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>ZA'
        !note all columns bumped up by one
        ! M - Consumer Telephone
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & job:Mobile_Number
        ! N - Consumer Fax
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
        ! O - Zip Code
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
        ! P - Accessory
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
        LOOP UNTIL Access:JOBACC.Next()
            IF (jac:Ref_Number <> job:Ref_Number)
                BREAK
            END
            IF (locAccessories = '')
                locAccessories = jac:Accessory
            ELSE
                locAccessories = CLIP(locAccessories) & ' ' & jac:Accessory
            END
        END

        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & locAccessories
        ! Q - Model
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & job:Model_Number
        ! R - Purchase Price
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>'
        ! S - Serial Number
        SamsungSA:Line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:MSN,1,16)
        ! T - CRT Serial Number
        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 0
                End !If jot:RefNumber <> job:Ref_Number
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1
        End !job:Third_Party_Site <> ''
        If IMEIError# = 1
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:esn,1,18)
        Else !IMEIError# = 1
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(jot:OriginalIMEI,1,18)
        End !IMEIError# = 1
        ! U - Defect Type
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:fault_code3,1,2)
        ! V - IRIS Condition
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:fault_code1,1,1)
        ! W - IRIS Symptom
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:fault_code2,1,3)
        ! X - IRIS Defect
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:fault_code4,1,4)
        ! Y - IRIS Repair
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(job:fault_code5,1,4)
        ! Z - Defect Block
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        ! AA - Defect Description
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(BHStripNonAlphaNum(jbn:Fault_Description,' '),1,70)
        ! AB - Repair Description
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & SUB(BHStripNonAlphaNum(jbn:Engineers_Notes,' '),1,70)
        ! AC - Appointment Date
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        ! AD - Appointment Time
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        ! AE - Purchase Date
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(Year(job:dop),@n04) & |
            Format(Month(job:dop),@n02) & |
            Format(Day(job:dop),@n02)
        ! AF - Repair Receive Date
        ! AG - Repair Received Time
        locDate = job:Date_Booked
        locTime = job:Time_Booked
        GetBookingDate(job:Ref_Number,locDate,locTime,job:Exchange_Unit_Number,jobe:ExchangedATRRC) ! #11604 Use "clever" date (Bryan: 10/02/2011)
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(locDate),@n04) & |
            Format(Month(locDate),@n02) & |
            Format(Day(locDate),@n02)
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(locTime,@t05)
!        ! Has the job been estimated??
!        found# = 0
!        Access:AUDSTATS.Clearkey(aus:statusTimeKey)
!        aus:refNumber    = job:ref_Number
!        aus:type    = 'JOB'
!        set(aus:statusTimeKey,aus:statusTimeKey)
!        loop
!            if (Access:AUDSTATS.Next())
!                Break
!            end ! if (Access:AUDSTATS.Next())
!            if (aus:refNumber    <> job:ref_Number)
!                Break
!            end ! if (aus:refNumber    <> job:ref_Number)
!            if (aus:type    <> 'JOB')
!                Break
!            end ! if (aus:type    <> 'JOB')
!            if (instring('ESIMATE SENT',Upper(aus:NewStatus)))
!                found# = 1
!                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(aus:DateChanged),@n04) & |
!                    Format(Month(aus:DateChanged),@n02) & |
!                    Format(Day(aus:DateChanged),@n02)
!                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(aus:TimeChanged,@t05)
!                
!                break
!            end ! if (instring('ESIMATE SENT',aus:NewStatus))
!        end ! loop
!
!        if (found# = 0)
!            If (SentToHub(job:Ref_Number))
!                ! #11922 If sent to the ARC, use the date that it was received at the ARC. (Bryan: 19/01/2011)
!                Access:LOCATLOG.Clearkey(lot:DateKey)
!                lot:RefNumber = job:Ref_Number
!                Set(lot:DateKey,lot:DateKey)
!                Loop Until Access:LOCATLOG.Next()
!                    If (lot:RefNumber <> job:Ref_Number)
!                        BREAK
!                    END
!                    IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
!                        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(lot:TheDate),@n04) & |
!                            Format(Month(lot:TheDate),@n02) & |
!                            Format(Day(lot:TheDate),@n02)
!                        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(lot:TheTime,@t05)
!                        found# = 1
!                        break
!                    END
!                END
!                IF (Found# = 0)
!                    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(job:date_booked),@n04) & |
!                        Format(Month(job:date_booked),@n02) & |
!                        Format(Day(job:date_Booked),@n02)
!                    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(job:Time_Booked,@t05)
!                END
!            ELSE ! If (SentToHub(job:Ref_Number))
!            
!                ! Insert --- Use Date Receive AT RRC if PUP, otherwise use date booked (DBH: 14/12/2009) #11214
!                if (job:who_Booked = 'WEB')
!                    Access:AUDIT.Clearkey(aud:TypeActionKey)
!                    aud:ref_Number    = job:Ref_Number
!                    aud:type    = 'JOB'
!                    aud:action    = 'UNIT RECEIVED AT RRC FROM PUP'
!                    set(aud:TypeActionKey,aud:TypeActionKey)
!                    loop
!                        if (Access:AUDIT.Next())
!                            Break
!                        end ! if (Access:AUDIT.Next())
!                        if (aud:ref_Number    <> job:Ref_Number)
!                            Break
!                        end ! if (aud:ref_Number    <> job:Ref_Number)
!                        if (aud:type    <> 'JOB')
!                            Break
!                        end ! if (aud:type    <> 'JOB')
!                        if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
!                            Break
!                        end ! if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
!
!                        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(aud:Date),@n04) & |
!                            Format(Month(aud:Date),@n02) & |
!                            Format(Day(aud:Date),@n02)
!                        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(aud:Time,@t05)
!                        break
!                    end ! loop
!                else ! if (job:who_Booked = 'WEB')
!                    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(year(job:date_booked),@n04) & |
!                        Format(Month(job:date_booked),@n02) & |
!                        Format(Day(job:date_Booked),@n02)
!                    SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(job:Time_Booked,@t05)
!                end !if (job:who_Booked = 'WEB')
!            End ! If (SentToHub(job:Ref_Number))
!        end ! if (found# = 0)
        ! AG - Complete Date
        !012656 Completed date on Tech reps, complex sorting
        Do GetDateCompleted
        LocDate = DateCompleted
        LocTime = TimeCompleted
        !Replaces the following
        !locDate = job:Date_Completed
        !locTime = job:Time_Completed
        IF (job:Exchange_Unit_Number > 0)
            GetAuditDateTime(job:Ref_Number,'EXCHANGE UNIT ATTACHED TO JOB',locDate,locTime,'EXC')    ! #11604 Use "clever" date (Bryan: 10/02/2011)
        END !IF (job:Exchange_Unit_Number > 0)

        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(Year(locDate),@N04) & |
            Format(Month(locDate),@n02) & |
            Format(Day(locDate),@n02)
        ! AI - Complete Time
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(locTime,@t05)
        ! AJ - Delivery Date
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(Year(job:date_completed),@N04) & |
            Format(Month(job:date_completed),@n02) & |
            Format(Day(job:date_completed),@n02)
        ! AK - Delivery Time
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & FORMAT(job:Time_Completed,@t05)
        ! AL - AW Blank
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        ! AX - New Model
        ! AY - New Serial Number
        ! AZ - New CRT Serial No
        IF (job:Exchange_Unit_Number > 0)
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & xch:Model_Number
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & xch:MSN    
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & xch:ESN
            ELSE
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
            END !IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
             
        ELSE ! IF (job:Exchange_Unit_Number > 0)
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        END ! IF (job:Exchange_Unit_Number > 0)
        ! BA - BI Blank
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        ! BJ - BL Parts * however many there are
        !Next!
        l# = 0
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:Ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
                break
            end !if
            if wpr:ref_number  <> job:Ref_number      |
                then break.  ! end if
            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                Cycle
            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            If wpr:Part_Number = 'EXCH'
                Cycle
            End !If wpr:Part_Number = 'EXCH'
            l#+=1
            IF l# = 16
                BREAK
            END
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(wpr:fault_code1,@s10)
            If wpr:Part_Number = 'ADJUSTMENT'
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
            Else !If wpr:Part_Number = 'ADJUSTMENT'
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(wpr:part_number,@s16)
                SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>' & Format(wpr:Quantity,@n02)
            End !If wpr:Part_Number = 'ADJUSTMENT'
            ! BM - Document No
            ! BN - Part Serial No
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
            SamsungSA:line1   = Clip(SamsungSA:line1) & '<9>'
        end !loop

        Append(FileSamsungSA)
        setcursor()
local.SiemensExport        Procedure(Byte f:FirstSecondYear)
FileSiemens    File,Driver('ASCII'),Pre(Siemens),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
locBasicFilename    String(255)
Code
    Do GetEDIPath

    If Month(Today()) <> Month(man:SiemensDate)
        man:SiemensNumber = 1
        man:SiemensDate = Today()
    Else ! If Month(Today()) <> Month(man:SiemensDate)
        man:SiemensNumber += 1
        man:SiemensDate = Today()
    End ! If Month(Today()) <> Month(man:SiemensDate)

    locBasicFileName = Clip(man:EDI_Account_Number) & '_' & Format(Year(Today()),@n04) & Format(Month(Today()),@n02) & Format(man:SiemensNumber,@n03) & '.wsr'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.wsr'
    End !If func:SecondYear
    FileName   = Clip(man:EDI_Path) & '\' & locBasicFilename

    Access:MANUFACT.Update()

    Remove(FileSiemens)
    Create(FileSiemens)
    Open(FileSiemens)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportSiemens
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileSiemens)

ExportSiemens        Routine
    Clear(Siemens:Record)
    Siemens:Line1   = 'R'
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(man:edi_Account_Number)
    ! Changing (DBH 23/02/2006) #7153 - Add "001." prefix
    ! Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Ref_Number)
    ! to (DBH 23/02/2006) #7153
    Siemens:Line1   = Clip(Siemens:Line1) & ';001.' & Clip(job:Ref_Number)
    ! End (DBH 23/02/2006) #7153
    !Remove column for 'M' record - 3293 (DBH: 23-09-2003)
    !Change the field back to Order Number - 3437 (DBH: 03-12-2003)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Order_Number)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code4)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Model_Number)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_code1)
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:esn)                !Allow for Third Party
    Else !IMEIError# = 1
        Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(jot:OriginalIMEI)                !Allow for Third Party
    End !IMEIError# = 1

    !This must be the exchange unit IMEI Number - L940 (DBH: 01-09-2003)
    !Apply the same code as Service History to EDI - 3788 (DBH: 15-04-2004)
    ExchangeAttached# = False
    If job:Exchange_unit_Number <> 0
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !RRC exchanges for normal jobs appear on the report
        !48 Hour RRC exchanges do not
        !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
        If jobe:Engineer48HourOption = 1
            If jobe:ExchangedATRRC = True
                If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = jobe:SecondExchangeNumber
                Else !If jobe:SecondExchangeNumber <> 0
                    ExchangeNumber# = 0
                End !If jobe:SecondExchangeNumber <> 0
            End !If jobe:ExchangeATRRC = True
        Else !jobe:Exchange48HourOption = 1
            ExchangeNumber# = job:Exchange_Unit_Number
        End !jobe:Exchange48HourOption = 1

        If ExchangeNumber# <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = ExchangeNumber#
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                ExchangeAttached# = True
                Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(xch:ESN)
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        End !If ShowExchange# = True
    End !job:Exchange_unit_Number <> 0

    If ExchangeAttached# = False
        Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    End !If ExchangeAttached# = False

    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code3)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Format(job:DOP,@d12)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Format(job:Date_Booked,@d12)

    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Format(job:Date_Completed,@d12)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Format(job:Date_Completed,@d12)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code5)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code6)
    !Voacom didn't want this field showing, now they do again - 3437 (DBH: 03-12-2003)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code2)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code7)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code8)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code9)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code10)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(job:Fault_Code11)
    Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
    Append(FileSiemens)

    Clear(Siemens:Record)
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'
        Siemens:Line1   = 'M'
        !Show "Adjustment Reason" when the part is
        !an ADJUSTMENT - L940 (DBH: 01-09-2003)
        If wpr:Part_Number = 'ADJUSTMENT'
            Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(wpr:Fault_Code3)
        Else !If wpr:Part_Number = 'ADJUSTMENT'
            Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(wpr:Part_Number)
        End !If wpr:Part_Number = 'ADJUSTMENT'

        Siemens:Line1   = Clip(Siemens:Line1) & ';' & ''
        Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(wpr:Fault_Code1)
        Siemens:Line1   = Clip(Siemens:Line1) & ';' & Clip(wpr:Fault_Code2)
        Append(FileSiemens)
    End !Loop
local.SonyExport        Procedure(Byte f:FirstSecondYear)
FileSony    File,Driver('ASCII'),Pre(Sony),Name(Filename),Create,Bindable,Thread
Record              Record
Line1                    String(2000)
                    End
                End
locBasicFilename    String(255)
Code
    Do GetEDIPath
    locBasicFilename = 'SON.CSV'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear

    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(FileSony)
    Create(FileSony)
    Open(FileSony)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()

    Clear(Sony:record)
    Sony:line1   = 'RegionCode,CustomerID,RepairNo,ModelNo,IMEI,DatePurchase,DateReceived,DateShipped,' &|
                    'WarrantyCode,ConditionCode,SymptomCode,DefectCode,1stPartNo,1stPartQty,' &|
                    '2ndPartNo,2ndPartQty,3rdPartNo,3rdPartQty,4thPartNo,4thPartQty,' &|
                    '5thPartNo,5thPartQty,LabourClaim,PartsClaim,TotalClaim'
    Append(FileSony)

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportSony
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(FileSony)

ExportSony        Routine
    Clear(Sony:Record)
    Sony:line1   = 'IP1,'
    Sony:line1   = Clip(Sony:line1) & Clip(man:edi_account_number) & ','
    Sony:line1   = Clip(Sony:line1) & Clip(job:ref_number) & ','
    Sony:line1   = Clip(Sony:line1) & Clip(job:model_number) & ','
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        Sony:line1   = Clip(Sony:line1) & Clip(job:esn) & ','
    Else !IMEIError# = 1
        Sony:line1   = Clip(Sony:line1) & Clip(jot:OriginalIMEI) & ','
    End !IMEIError# = 1

    If job:dop <> ''
        Sony:line1   = Clip(Sony:line1) & Format(Day(job:dop),@n02) & '.' & Format(Month(job:dop),@n02) &|
                                        '.' & Format(Year(job:dop),@n04) & ','

    Else!If job:dop <> ''
        Sony:line1   = Clip(Sony:Line1) & ','
    End!If job:dop <> ''
    Sony:line1   = Clip(Sony:line1) & Format(Day(job:date_booked),@n02) & '.' & Format(Month(job:date_booked),@n02) &|
                                    '.' & Format(Year(job:date_booked),@n04) & ','
    If job:exchange_unit_number <> ''
        If job:exchange_despatched <> ''
            Sony:line1   = Clip(Sony:line1) & Format(Day(job:Exchange_Despatched),@n02) & '.' & Format(Month(job:Exchange_Despatched),@n02) &|
                                        '.' & Format(Year(job:Exchange_Despatched),@n04) & ','
        Else!If job:exchange_despatched <> ''
            Sony:line1   = Clip(Sony:line1) & ','
        End!If job:exchange_despatched <> ''
    Else!If job:exchange_unit_number <> ''
        If job:date_despatched <> ''
            Sony:line1   = Clip(Sony:line1) & Format(Day(job:Date_Despatched),@n02) & '.' & Format(Month(job:Date_Despatched),@n02) &|
                                        '.' & Format(Year(job:Date_Despatched),@n04) & ','
        Else!If job:date_despatched <> ''
            Sony:line1   = Clip(Sony:line1) & ','
        End!If job:date_despatched <> ''
    End!If job:exchange_unit_number <> ''

    Sony:line1   = Clip(Sony:line1) & Clip(job:fault_code1) & ','     !Warranty Code
    Sony:line1   = Clip(Sony:line1) & Clip(job:fault_code2) & ','     !Condition Code
    Sony:line1   = Clip(Sony:line1) & Clip(job:fault_code3) & ','     !Symptom Code
    Sony:line1   = Clip(Sony:line1) & Clip(job:fault_code4) & ','     !Defect Code

    count_parts# = 0
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        count_parts# += 1
        Sony:line1   = Clip(Sony:line1) & Clip(wpr:part_number) & ','
        Sony:line1   = Clip(Sony:line1) & Clip(wpr:quantity) & ','
        If count_parts# => 5
            Break
        End!If count_parts# => 5
    end !loop
    count_parts# += 1
    Loop x# = count_parts# To 5
        Sony:line1   = Clip(Sony:line1) & ','
        Sony:line1   = Clip(Sony:line1) & ','
    End!Loop x# = count_parts# To 5

    Sony:line1   = Clip(Sony:line1) & Format(job:labour_cost_warranty,@n10.2) & ','
    Sony:line1   = Clip(Sony:line1) & Format(job:parts_cost_warranty,@n10.2) & ','
    Sony:line1   = Clip(Sony:line1) & Format(job:sub_total_warranty,@n10.2) & ','
    Append(FileSony)
    Clear(Sony:Record)
local.TelitalExport        Procedure(Byte f:FirstSecondYear)
Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(82)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
serv_code      STRING(4)
book_date      STRING(9)
comp_date      STRING(9)
prod_code      STRING(16)
esn_code       STRING(16)
perc_fault     STRING(4)
cause_fault    STRING(4)
found_defect   STRING(4)
part_code      STRING(16)
           .
locBasicFilename        String(255)
Code
    Do GetEDIPath

    locBasicFilename = 'TEL.CSV'
    
    If f:FirstSecondYear
        locBasicFilename = Clip(f:Manufacturer) & ' 2y.CSV'
    End !If func:SecondYear
    Filename = Clip(man:EDI_Path) & '\' & locBasicFilename

    Remove(Out_File)
    Create(Out_File)
    Open(Out_File)
    If Error()
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
              '<13,10>'&|
              '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
    End ! If Error()


    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportTelital
                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    Close(Out_File)

ExportTelital        Routine
      CLEAR(ouf:RECORD)
      countp# = 0
      setcursor(cursor:wait)

      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job:ref_number      |
              then break.  ! end if
          If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
              Cycle
          End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
          If wpr:Part_Number = 'EXCH'
              Cycle
          End !If wpr:Part_Number = 'EXCH'
          countp#+=1
          l1:serv_code    = SUB(MAN:EDI_Account_Number,1,3)&','
          l1:book_date    = FORMAT(DAY(job:date_booked),@n02)&'/'&FORMAT(MONTH(job:date_booked),@n02)&'/'&FORMAT(SUB(YEAR(job:date_booked),3,2),@n02)&','
          l1:comp_date    = FORMAT(DAY(job:Date_Completed),@n02)&'/'&FORMAT(MONTH(job:Date_Completed),@n02)&'/'&FORMAT(SUB(YEAR(job:Date_Completed),3,2),@n02)&','
          l1:prod_code    = SUB(job:Fault_Code1,1,15)&','
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number
                        IMEIError# = 0
                    End !If jot:RefNumber <> job:Ref_Number
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                l1:esn_code     = SUB(job:ESN,1,15)&','
            Else !IMEIError# = 1
                l1:esn_code     = SUB(JOt:OriginalIMEI,1,15)&','
            End !IMEIError# = 1

          l1:perc_fault   = SUB(job:Fault_Code2,1,3)&','
          l1:cause_fault  = SUB(job:Fault_Code6,1,3)&','
          l1:found_defect = SUB(job:Fault_Code3,1,3)&','
          l1:part_code    = SUB(WPR:Part_Number,1,15)
          Append(Out_File)
      end !loop
      setcursor()

      IF countp# = 0
        l1:serv_code    = SUB(MAN:EDI_Account_Number,1,3)&','
        l1:book_date    = FORMAT(DAY(job:date_booked),@n02)&'/'&FORMAT(MONTH(job:date_booked),@n02)&'/'&FORMAT(SUB(YEAR(job:date_booked),3,2),@n02)&','
        l1:comp_date    = FORMAT(DAY(job:Date_Completed),@n02)&'/'&FORMAT(MONTH(job:Date_Completed),@n02)&'/'&FORMAT(SUB(YEAR(job:Date_Completed),3,2),@n02)&','
        l1:prod_code    = SUB(job:Fault_Code1,1,15)&','
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number
                        IMEIError# = 0
                    End !If jot:RefNumber <> job:Ref_Number
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                l1:esn_code     = SUB(job:ESN,1,15)&','
            Else !IMEIError# = 1
                l1:esn_code     = SUB(JOt:OriginalIMEI,1,15)&','
            End !IMEIError# = 1
        l1:perc_fault   = SUB(job:Fault_Code2,1,3)&','
        l1:cause_fault  = SUB(job:Fault_Code4,1,3)&','
        l1:found_defect = SUB(job:Fault_Code3,1,3)&','
        l1:part_code    = SUB('None Given',1,15)
        Append(Out_File)
      END
      CLEAR(ouf:RECORD)
local.ZTEExport        Procedure(Byte f:FirstSecondYear)
!tmp:CurrentRow      Long()
tmp:ZTEFilename     Long()
tmp:MRRCCode        String(30)
tmp:Level1parts     String(100)
tmp:Level2Parts     String(100)
tmp:Level3Parts     String(100)
ZTESEQUEUE          QUEUE,PRE(ZTSE)
RecordNumber        Long()
    End
locBasicFilename        String(255)
Code
    !ZTE Export Added by Paul - 08/10/2010 Log no 11629
    Do GetEDIPath

    !Get the next ZTE file number -
    tmp:ZTEFilename = getini('ZTEEDI','LastNumber',0,path() & '\SB2KDEF.INI') + 1

    locBasicFilename = 'ZTE' & Format(tmp:ZTEFilename,@N07)
    

    If f:FirstSecondYear
       locBasicFileName =  clip(locBasicFilename) & ' 2y'
    End !If func:SecondYear

    locBasicFileName = Clip(locBasicFileName) & '.xls'

    Filename = Clip(man:EDI_Path) & locBasicFilename


    If E1.Init(0,0) = False
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
                'Please check your EDI Defaults for this manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
        Error# = 1
    End ! If E1.Init(0,0) = False

    E1.NewWorkBook()
    E1.RenameWorkSheet('ZTE')
    E1.WriteToCell('JobNo'                          ,'A1') !1
    E1.WriteToCell('SubmittingDate'                 ,'B1') !2
    E1.WriteToCell('CustomerName'                   ,'C1') !3
    E1.WriteToCell('CustomerTitle'                  ,'D1') !4
    E1.WriteToCell('LocalTel'                       ,'E1') !5
    E1.WriteToCell('PhoneNo'                        ,'F1') !6
    E1.WriteToCell('Address'                        ,'G1') !7
    E1.WriteToCell('Customertype'                   ,'H1') !8
    E1.WriteToCell('HandsetType'                    ,'I1') !9
    E1.WriteToCell('HandsetModel'                   ,'J1') !10
    E1.WriteToCell('IMEIESNSN'                      ,'K1') !11
    E1.WriteToCell('RecievedDate'                   ,'L1') !12
    E1.WriteToCell('PurchaseDate'                   ,'M1') !13
    E1.WriteToCell('ASCCode'                        ,'N1') !14
    E1.WriteToCell('Accessories1-Battery'           ,'O1') !15
    E1.WriteToCell('Accessories2-Recharger'         ,'P1') !16
    E1.WriteToCell('Accessories3-Earphone'          ,'Q1') !17
    E1.WriteToCell('Accessories4-Card'              ,'R1') !18
    E1.WriteToCell('Accessories5-PickUpHead'        ,'S1') !19
    E1.WriteToCell('Accessories6-RubberPlug'        ,'T1') !20
    E1.WriteToCell('MainFaultsCategoryCode'         ,'U1') !21
    E1.WriteToCell('MainFaultsSubClassCode'         ,'V1') !22
    E1.WriteToCell('SecondaryFaultsCategoryCode'    ,'W1') !23
    E1.WriteToCell('SecondaryFaultsSubClassCode'    ,'X1') !24
    E1.WriteToCell('OtherFaults'                    ,'Y1') !25
    E1.WriteToCell('WarrantyTypeCode'               ,'Z1') !26
    E1.WriteToCell('RepairLevel'                    ,'AA1') !27
    E1.WriteToCell('RepairMethodName1'              ,'AB1') !28
    E1.WriteToCell('RepairMethodName2'              ,'AC1') !29
    E1.WriteToCell('RepairMethodName3'              ,'AD1') !30
    E1.WriteToCell('NewIMEIESNSN'                   ,'AE1') !31
    E1.WriteToCell('ReplacingMaterialsCode1'        ,'AF1') !32
    E1.WriteToCell('ReplacedmaterialsCode1'         ,'AG1') !33
    E1.WriteToCell('ReplacingMaterialsCode2'        ,'AH1') !34
    E1.WriteToCell('ReplacedMaterialsCode2'         ,'AI1') !35
    E1.WriteToCell('ReplacingmaterialsCode3'        ,'AJ1') !36
    E1.WriteToCell('ReplacedmaterialsCode3'         ,'AK1') !37
    E1.WriteToCell('Engineer'                       ,'AL1') !38
    E1.WriteToCell('RepairedDate'                   ,'AM1') !39
    E1.WriteToCell('DeliveryDate'                   ,'AN1') !40
    E1.WriteToCell('AbnormalSheetOrNot'             ,'AO1') !41
    E1.WriteToCell('Remarks'                        ,'AP1') !42
    E1.SetCellFontStyle('Bold','A1','AP1')

    Do Prog:ProgressSetup
    Prog:TotalRecords = tmp:RecordsCount
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Free(ZTESEQUEUE)

    Access:JOBSWARR.Clearkey(jow:ClaimStatusManFirstKey)
    jow:Status = 'NO'
    jow:Manufacturer = f:Manufacturer
    jow:FirstSecondYear = f:FirstSecondYear
    jow:ClaimSubmitted = f:StartDate
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    tmp:CurrentRow = 2

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If jow:Status <> 'NO'   |
                    Or jow:Manufacturer <> f:Manufacturer   |
                    Or jow:FirstSecondYear <> f:FirstSecondYear |
                    Or jow:ClaimSubmitted > f:EndDate
                        Prog:Exit = 1
                        Break
                    End ! If jow:ClaimSubmitted > f:EndDate

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Do ExportZTE

                    !create a queue of jobs we are exporting for later use in the service export
                    ZTSE:RecordNumber = jow:RefNumber
                    Add(ZTESEQUEUE)

                    Do UpdateJOBSE

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Export: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    SetTarget()
                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            tmp:Return = 1
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    E1.SetCellFontName('Tahoma','A1','AP' & tmp:CurrentRow)
    E1.SetCellFontSize(8,'A1','AP' & tmp:CurrentRow)
    !E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,,'D2','E' & tmp:CurrentRow)
    E1.SetColumnWidth('A','AP')
    E1.SelectCells('A2')
    E1.SaveAs(FileName)
    E1.CloseWorkBook(2)
    E1.Kill
    ! Inserting (DBH 18/01/2006) #6964 - Reopen the document to reformat the date
    ExcelSetup(0)
    ExcelOpenDoc(FileName)
    ExcelSelectSheet('ZTE')
    !ExcelFormatRange('L2:O' & tmp:CurrentRow,'0#######')
    ExcelSaveWorkBook(FileName)
    ExcelClose()
    ! End (DBH 18/01/2006) #6964

    putini('ZTEEDI','LastNumber',tmp:ZTEFilename,path() & '\SB2KDEF.INI')

    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Technical Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        f:FileList = Clip(f:FileList) & '|' & locBasicFilename
    End ! If f:Silent = 0

    if records(ZTESEQUEUE) > 0 then
        Do ZTESEExport
    End

ExportZTE        Routine
Data
local:Parts     Byte(0)
local:ARCSiteLocation String(30)
local:PurchaseCost  Real()
Code
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    ! Inserting (DBH 15/06/2006) #7847 - Not getting webjobs
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    ! End (DBH 15/06/2006) #7847

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Found
        local:ARCSiteLocation = tra:SiteLocation
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Error
    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    !lookup the ASVAcc file
    If jow:RepairedAt = 'ARC' then
        !this is an ARC job - so look up th head account
        tmpheadaccount" = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')

        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
        ASV:TradeAccNo      = tmpheadaccount"
        ASV:Manufacturer    = f:Manufacturer
        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
            !entry exists - does it contain a code?
            tmp:MRRCCode = clip(ASV:ASVCode)
        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

    Else
        !this is an RRC job - so look up the RRC account
        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
        ASV:TradeAccNo      = wob:HeadAccountNumber
        ASV:Manufacturer    = f:Manufacturer
        If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then
            !entry exists - does it contain a code?
            tmp:MRRCCode = clip(ASV:ASVCode)
        End !If access:ASVACC.fetch(ASV:TradeACCManufKey) = level:benign then

    End !If x# = True then




    !got all the info - now output the data **********************************************************************************

    E1.WriteToCell(job:Ref_Number                                   ,'A' & tmp:CurrentRow)  !1 'JobNo'
    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'B2','B' & tmp:CurrentRow)
    E1.WriteToCell(Format(today(),@D10-)                            ,'B' & tmp:CurrentRow)  !2 'SubmittingDate'


    If clip(job:Surname) = '' then
        !check the Company name field
        E1.WriteToCell(clip(job:Company_Name)                       ,'C' & tmp:CurrentRow) !3 'CustomerName'
    Else
        E1.WriteToCell(clip(job:Initial) & ' ' & clip(job:Surname)  ,'C' & tmp:CurrentRow) !3 'CustomerName'
    End

    E1.WriteToCell('1'                                              ,'D' & tmp:CurrentRow)  !4 'CustomerTitle'
    E1.WriteToCell(''                                               ,'E' & tmp:CurrentRow)  !5 'LocalTel'
    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'F2','F' & tmp:CurrentRow)
    E1.WriteToCell(clip(job:Mobile_Number)                          ,'F' & tmp:CurrentRow)  !6 'PhoneNo'
    E1.WriteToCell(clip(job:Address_Line3)                          ,'G' & tmp:CurrentRow)  !7 'Address'

    If clip(job:who_booked) = 'WEB' then
        !vcp job
        E1.WriteToCell('3'                                          ,'H' & tmp:CurrentRow)  !8 'Customertype'
    Else
        If INSTRING('WALK-IN',clip(job:Transit_Type),1,1) then
            E1.WriteToCell('1'                                      ,'H' & tmp:CurrentRow)  !8 'Customertype'
        Else
            E1.WriteToCell('2'                                      ,'H' & tmp:CurrentRow)  !8 'Customertype'
        End
    End

    E1.WriteToCell('GSM'                                            ,'I' & tmp:CurrentRow)  !9 'HandsetType'
    E1.WriteToCell(clip(job:Model_Number)                           ,'J' & tmp:CurrentRow)  !10 'HandsetModel'

    !has this been swapped by a third party
    Access:JobThird.clearkey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    If Access:jobThird.tryfetch(jot:RefNumberKey) = level:benign then
        !jobthird record
        E1.SetCellNumberFormat(oix:NumberFormatText,,,,'K2','K' & tmp:CurrentRow)
        E1.WriteToCell(clip(jot:OriginalIMEI)                                ,'K' & tmp:CurrentRow)  !11 'IMEIESNSN'
    Else
        !no job third record
        E1.SetCellNumberFormat(oix:NumberFormatText,,,,'K2','K' & tmp:CurrentRow)
        E1.WriteToCell(clip(job:ESN)                                ,'K' & tmp:CurrentRow)  !11 'IMEIESNSN'
    End


    if (job:who_Booked = 'WEB')
        found# = 0
        Access:AUDIT.Clearkey(aud:TypeActionKey)
        aud:ref_Number    = job:Ref_Number
        aud:type    = 'JOB'
        aud:action    = 'UNIT RECEIVED AT RRC FROM PUP'
        set(aud:TypeActionKey,aud:TypeActionKey)
        loop
            if (Access:AUDIT.Next())
                Break
            end ! if (Access:AUDIT.Next())
            if (aud:ref_Number    <> job:Ref_Number)
                Break
            end ! if (aud:ref_Number    <> job:Ref_Number)
            if (aud:type    <> 'JOB')
                Break
            end ! if (aud:type    <> 'JOB')
            if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
                Break
            end ! if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'L2','L' & tmp:CurrentRow)
            E1.WriteToCell(Format(aud:Date,@D10-)                   ,'L' & tmp:CurrentRow)  !12 'RecievedDate'
            found# = 1
            break
        end ! loop
        if (found# = 0)
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'L2','L' & tmp:CurrentRow)
            E1.WriteToCell(Format(job:date_Booked,@D10-)            ,'L' & tmp:CurrentRow)  !12 'RecievedDate'
        end ! if (found# = 0)
    else ! if (job:who_Booked = 'WEB')
        !Has it been sent off to an ARC for repair
        Access:Locatlog.clearkey(lot:NewLocationKey)
        lot:RefNumber   = job:Ref_Number
        lot:NewLocation = 'RECEIVED AT ARC'
        If access:Locatlog.tryfetch(lot:NewLocationKey) = level:benign then
            !put the date in
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'L2','L' & tmp:CurrentRow)
            E1.WriteToCell(Format(lot:TheDate,@D10-)                ,'L' & tmp:CurrentRow)  !12 'RecievedDate'
        Else
            !use booking date
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'L2','L' & tmp:CurrentRow)
            E1.WriteToCell(Format(job:date_Booked,@D10-)                ,'L' & tmp:CurrentRow)  !12 'RecievedDate'
        End
    end !if (job:who_Booked = 'WEB')

    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'M2','M' & tmp:CurrentRow)
    E1.WriteToCell(Format(job:DOP,@D10-)                            ,'M' & tmp:CurrentRow)  !13 'PurchaseDate'
    E1.WriteToCell(clip(tmp:MRRCCode)                               ,'N' & tmp:CurrentRow)  !14 'ASCCode'
    !What accessories are on this job
    !E1.WriteToCell(''           ,'O' & tmp:CurrentRow)  !'Accessories1-Battery'
    Access:JobAcc.Clearkey(jac:Ref_Number_Key)
    jac:Ref_Number = job:Ref_Number
    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
    Loop
        If Access:JobAcc.next() then
            break
        End
        If jac:Ref_Number <> job:Ref_Number then
            break
        End
        Case upper(clip(jac:Accessory))
        of 'BATTERY'
            E1.WriteToCell('1'      ,'O' & tmp:CurrentRow)  !15 'Accessories1-Battery'
        of 'RECHARGER'
            E1.WriteToCell('2'      ,'P' & tmp:CurrentRow)  !16 'Accessories2-Recharger'
        of 'EARPHONE'
            E1.WriteToCell('3'      ,'Q' & tmp:CurrentRow)  !17 'Accessories3-Earphone'
        of 'CARD'
            E1.WriteToCell('4'      ,'R' & tmp:CurrentRow)  !18 'Accessories4-Card'
        of 'PICKUPHEAD'
            E1.WriteToCell('5'      ,'S' & tmp:CurrentRow)  !19 'Accessories5-PickUpHead'
        of 'RUBBERPLUG'
            E1.WriteToCell('6'      ,'T' & tmp:CurrentRow)  !20 'Accessories6-RubberPlug'
        End
    End

    E1.WriteToCell(clip(job:Fault_Code1)            ,'U' & tmp:CurrentRow)  !21 'MainFaultsCategoryCode'
    E1.WriteToCell(clip(job:Fault_Code2)            ,'V' & tmp:CurrentRow)  !22 'MainFaultsSubClassCode'
    E1.WriteToCell(''                               ,'W' & tmp:CurrentRow)  !23 'SecondaryFaultsCategoryCode'
    E1.WriteToCell(''                               ,'X' & tmp:CurrentRow)  !24 'SecondaryFaultsSubClassCode'
    E1.WriteToCell(''                               ,'Y' & tmp:CurrentRow)  !25 'OtherFaults'
    E1.WriteToCell('128'                            ,'Z' & tmp:CurrentRow)  !26 'WarrantyTypeCode'
    E1.WriteToCell(clip(job:Fault_Code4)            ,'AA' & tmp:CurrentRow) !27 'RepairLevel'

    !are there any spares on the job?
    local:Parts = 0
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
            Break
        End
        If wpr:Ref_Number  <> job:Ref_Number  then
            Break
        End
        If wpr:Part_Number = 'EXCH' then
            cycle
        End
        local:Parts += 1
    End

    If local:Parts > 0 then
        E1.WriteToCell(''                           ,'AB' & tmp:CurrentRow) !28 'RepairMethodName1'
    Else
        E1.WriteToCell('SOFTWARE UPGRADE'           ,'AB' & tmp:CurrentRow) !28 'RepairMethodName1'
    End

    E1.WriteToCell(''                               ,'AC' & tmp:CurrentRow) !29 'RepairMethodName2'
    E1.WriteToCell(''                               ,'AD' & tmp:CurrentRow) !30 'RepairMethodName3'
    !is there an exchange on the job?
    If job:Exchange_Unit_Number > 0 then
        !now look up the exchange IMEI no
        Access:Exchange.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        If Access:Exchange.TryFetch(xch:Ref_Number_Key) = Level:benign then
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AE2','AE' & tmp:CurrentRow)
            E1.WriteToCell(clip(xch:ESN)            ,'AE' & tmp:CurrentRow) !31 'NewIMEIESNSN'
        Else
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AE2','AE' & tmp:CurrentRow)
            E1.WriteToCell(job:ESN                       ,'AE' & tmp:CurrentRow) !31 'NewIMEIESNSN'
        End
    Else
        !was a third party swap done
        Access:JobThird.clearkey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        If Access:jobThird.tryfetch(jot:RefNumberKey) = level:benign then
            !jobthird record
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AE2','AE' & tmp:CurrentRow)
            E1.WriteToCell(job:ESN                       ,'AE' & tmp:CurrentRow) !31 'NewIMEIESNSN'
        Else
            !no job third record
            E1.WriteToCell(''                           ,'AE' & tmp:CurrentRow) !31 'NewIMEIESNSN'
        End
    End

    !what parts are used on the job
    tmp:Level1Parts = ''
    tmp:Level2Parts = ''
    tmp:Level3Parts = ''
    Access:Warparts.clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    loop
        If access:Warparts.next() then
            break
        End
        If wpr:Ref_Number <> job:Ref_Number then
            break
        End
        If wpr:Part_Number = 'EXCH' then
            cycle
        End
        !ok - what level of part is this
        Access:Stock.clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        If access:Stock.tryfetch(sto:Ref_Number_Key) = level:benign then
            !now we need to check what level of stock this is
            If sto:E3 = TRUE then
                !level3 stock
                If clip(tmp:Level3Parts) = '' then
                    !first part added
                    tmp:Level3Parts = clip(wpr:Part_Number)
                Else
                    tmp:Level3Parts = clip(tmp:Level3Parts) & ';' & clip(wpr:Part_Number)
                End
            Else
                if sto:E2 = TRUE then
                    !level2 stock
                    If clip(tmp:Level2Parts) = '' then
                        !first part added
                        tmp:Level2Parts = clip(wpr:Part_Number)
                    Else
                        tmp:Level2Parts = clip(tmp:Level2Parts) & ';' & clip(wpr:Part_Number)
                    End
                Else
                    !level 1 and 0 parts go in the same column
                    If clip(tmp:Level1Parts) = '' then
                        !first part added
                        tmp:Level1Parts = clip(wpr:Part_Number)
                    Else
                        tmp:Level1Parts = clip(tmp:Level1Parts) & ';' & clip(wpr:Part_Number)
                    End
                End
            End
        End
    End
    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AF2','AF' & tmp:CurrentRow)
    E1.WriteToCell(clip(tmp:Level1Parts)            ,'AF' & tmp:CurrentRow) !32 'ReplacingMaterialsCode1'
    E1.WriteToCell(''                               ,'AG' & tmp:CurrentRow) !33 'ReplacedmaterialsCode1'
    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AH2','AH' & tmp:CurrentRow)
    E1.WriteToCell(clip(tmp:Level2Parts)            ,'AH' & tmp:CurrentRow) !34 'ReplacingMaterialsCode2'
    E1.WriteToCell(''                               ,'AI' & tmp:CurrentRow) !35 'ReplacedMaterialsCode2'
    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AJ2','AJ' & tmp:CurrentRow)
    E1.WriteToCell(clip(tmp:Level3Parts)            ,'AJ' & tmp:CurrentRow) !36 'ReplacingmaterialsCode3'
    E1.WriteToCell(''                               ,'AK' & tmp:CurrentRow) !37 'ReplacedmaterialsCode3'
    !check audstats to find out the engineer who passed the job for QA
    Access:Audstats.Clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    Loop
        if Access:Audstats.next() then
            break
        End
        If aus:RefNumber <> job:Ref_Number then
            break
        End
        If clip(aus:NewStatus) = '605 QA CHECK REQUIRED' then
            !who was the tech on the job
            Access:Users.clearkey(use:User_Code_Key)
            use:User_Code = aus:UserCode
            If Access:Users.Tryfetch(use:User_Code_Key) = level:benign then
                E1.WriteToCell(clip(use:Forename) & ' ' & clip(use:Surname)     ,'AL' & tmp:CurrentRow) !38 'Engineer'
            Else
                E1.WriteToCell(''                                               ,'AL' & tmp:CurrentRow) !38 'Engineer'
            End
            break
        End
    End

    !012656 Completed date on Tech reps, complex sorting
    DateCompleted = 1       !this can be used as a flag that something has happened
    If job:Third_Party_Site <> ''  !this has gone to a third party
        IF (_PreClaimThirdParty(tra:Company_Name,job:Manufacturer))     !using the pre-claim system
            Do GetDateCompleted
            !    DateCompleted       !replaces both the audstats stuff that follows
            !    TimeCompleted
        END !if _preclaim
    END !if third party
    !Replaces in the following

    !the date the job was move to completed
    Access:Audstats.Clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    Loop
        if Access:Audstats.next() then
            break
        End
        If aus:RefNumber <> job:Ref_Number then
            break
        End
        If instring('COMPLETED',clip(aus:NewStatus),1,1) then
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AM2','AM' & tmp:CurrentRow)
            if DateCompleted > 1 then
                E1.WriteToCell(Format(DateCompleted,@D10-)                        ,'AM' & tmp:CurrentRow) !39 'RepairedDate'
            ELSE
                E1.WriteToCell(Format(aus:DateChanged,@D10-)                      ,'AM' & tmp:CurrentRow) !39 'RepairedDate'
            END
            break
        End
    End

    !the date the job was move to ready to despatch
    Access:Audstats.Clearkey(aus:RefRecordNumberKey)
    aus:RefNumber = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    Loop
        if Access:Audstats.next() then
            break
        End
        If aus:RefNumber <> job:Ref_Number then
            break
        End
        If instring('READY TO DESPATCH',clip(aus:NewStatus),1,1) then
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'AN2','AN' & tmp:CurrentRow)
            if DateCompleted > 1 then
                E1.WriteToCell(Format(DateCompleted,@D10-)                        ,'AN' & tmp:CurrentRow) !40 'DeliveryDate'
            ELSE
                E1.WriteToCell(Format(aus:DateChanged,@D10-)                      ,'AN' & tmp:CurrentRow) !40 'DeliveryDate'
            END
            break
        End
    End

    E1.WriteToCell('N'                              ,'AO' & tmp:CurrentRow) !41 'AbnormalSheetOrNot'
    E1.WriteToCell(''                               ,'AP' & tmp:CurrentRow) !42 'Remarks'
    


    tmp:CurrentRow += 1



ZTESEExport             ROUTINE

    Do GetEDIPath


    Filename = Clip(man:EDI_Path) & 'SE ZTE' & Format(tmp:ZTEFilename,@N07)

    If f:FirstSecondYear
       Filename = clip(Filename) & ' 2y'
    End !If func:SecondYear

    Filename = Clip(Filename) & '.xls'


    If E1.Init(0,0) = False
        If f:Silent = 0
            Case Missive('Unable to create EDI File.'&|
                'Please check your EDI Defaults for this manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Silent = 0
        tmp:Return = 1
        Return
        Error# = 1
    End ! If E1.Init(0,0) = False

    E1.NewWorkBook()
    E1.RenameWorkSheet('ZTE SE')
    E1.WriteToCell('Sn.'                                ,'A1')
    E1.WriteToCell('JOB No'                             ,'B1')
    E1.WriteToCell('Commerce Office'                    ,'C1')
    E1.WriteToCell('Receiving date/Date/Time In'        ,'D1')
    E1.WriteToCell('Repair Finished dateDate/Time Out'  ,'E1')
    E1.WriteToCell('Repair Time'                        ,'F1')
    E1.WriteToCell('ESN/IMEI'                           ,'G1')
    E1.WriteToCell('Model'                              ,'H1')
    E1.WriteToCell('Complain Description by Customer'   ,'I1')
    E1.WriteToCell('Version'                            ,'J1')
    E1.WriteToCell('Major Fault State Sort'             ,'K1')
    E1.WriteToCell('Lowest Fault State Sort'            ,'L1')
    E1.WriteToCell('Fault Reason'                       ,'M1')
    E1.WriteToCell('Repairing Description'              ,'N1')
    E1.WriteToCell('Cliam Level'                        ,'O1')
    E1.WriteToCell('Warranty'                           ,'P1')
    E1.WriteToCell('Agree Level by ZTE'                 ,'Q1')
    E1.WriteToCell('Spare Parts Used 1'                 ,'R1')
    E1.WriteToCell('Spare Parts Used 2'                 ,'S1')
    E1.WriteToCell('SPare Parts Used 3'                 ,'T1')
    E1.WriteToCell('Parts Fee'                          ,'U1')
    E1.WriteToCell('Labour Fee'                         ,'V1')
    E1.WriteToCell('Transportation Fee'                 ,'W1')
    E1.WriteToCell('New ESN'                            ,'X1')
    E1.WriteToCell('Name of Customer'                   ,'Y1')
    E1.WriteToCell('Tel No.'                            ,'Z1')
    E1.WriteToCell('Remark'                             ,'AA1')
    E1.SetCellFontStyle('BOLD','A1','AA1')

    tmp:CurrentRow = 2

    Do Prog:ProgressSetup
    Prog:TotalRecords = records(ZTESEQueue)
    Prog:ShowPercentage = 1 !Show Percentage Figure

    Sort(ZTESEQueue,+ZTSE:RecordNumber)
    Loop x# = 1 to records(ZTESEQueue)
        Get(ZTESEQueue,x#)
        If Error() then
            break
        End

        Prog:RecordCount += 1

        ?Prog:UserString{Prop:Text} = 'Creating Service Report: ' & Prog:RecordCount & '/' & Prog:TotalRecords

        SetTarget()
        Do Prog:UpdateScreen
        !now get the relevant jobswarr record
        Access:Jobswarr.clearkey(jow:RefNumberKey)
        jow:RefNumber = ZTSE:RecordNumber
        If Access:Jobswarr.Tryfetch(jow:RefNumberKey) = level:benign then

            !open up the jobs record
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jow:RefNumber
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                ! Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                ! Error
            End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


            !process the record

            E1.WriteToCell(x#                                   ,'A' & tmp:CurrentRow) !Sn.
            E1.WriteToCell(job:Ref_Number                       ,'B' & tmp:CurrentRow) !JOB No
            E1.WriteToCell(''                                   ,'C' & tmp:CurrentRow) !Commerce Office

            if (job:who_Booked = 'WEB')
                found# = 0
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:ref_Number    = job:Ref_Number
                aud:type    = 'JOB'
                aud:action    = 'UNIT RECEIVED AT RRC FROM PUP'
                set(aud:TypeActionKey,aud:TypeActionKey)
                loop
                    if (Access:AUDIT.Next())
                        Break
                    end ! if (Access:AUDIT.Next())
                    if (aud:ref_Number    <> job:Ref_Number)
                        Break
                    end ! if (aud:ref_Number    <> job:Ref_Number)
                    if (aud:type    <> 'JOB')
                        Break
                    end ! if (aud:type    <> 'JOB')
                    if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
                        Break
                    end ! if (aud:action    <> 'UNIT RECEIVED AT RRC FROM PUP')
                    E1.WriteToCell(Format(aud:Date,@D06)            ,'D' & tmp:CurrentRow)  !'RecievedDate'
                    found# = 1
                    break
                end ! loop
                if (found# = 0)
                    E1.WriteToCell(Format(job:date_Booked,@D06)     ,'D' & tmp:CurrentRow)  !'RecievedDate'
                end ! if (found# = 0)
            else ! if (job:who_Booked = 'WEB')
                !Has it been sent off to an ARC for repair
                Access:Locatlog.clearkey(lot:NewLocationKey)
                lot:RefNumber   = job:Ref_Number
                lot:NewLocation = 'RECEIVED AT ARC'
                If access:Locatlog.tryfetch(lot:NewLocationKey) = level:benign then
                    !put the date in
                    E1.WriteToCell(Format(lot:TheDate,@D06)                ,'D' & tmp:CurrentRow)  !'RecievedDate'
                Else
                    !use booking date
                    E1.WriteToCell(Format(job:date_Booked,@D06)                ,'D' & tmp:CurrentRow)  !'RecievedDate'
                End
            end !if (job:who_Booked = 'WEB')

            Access:Audstats.Clearkey(aus:RefRecordNumberKey)
            aus:RefNumber = job:Ref_Number
            Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
            Loop
                if Access:Audstats.next() then
                    break
                End
                If aus:RefNumber <> job:Ref_Number then
                    break
                End
                If instring('COMPLETED',clip(aus:NewStatus),1,1) then
                    E1.WriteToCell(Format(aus:DateChanged,@D06)     ,'E' & tmp:CurrentRow) !'RepairedDate'
                    break
                End
            End

            E1.WriteToCell(''                                   ,'F' & tmp:CurrentRow) !Repair Time

            !has this been swapped by a third party
            Access:JobThird.clearkey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            If Access:jobThird.tryfetch(jot:RefNumberKey) = level:benign then
                !jobthird record
                E1.SetCellNumberFormat(oix:NumberFormatText,,,,'G2','G' & tmp:CurrentRow)
                E1.WriteToCell(clip(jot:OriginalIMEI)                                ,'G' & tmp:CurrentRow)  !ESN/IMEI
            Else
                !no job third record
                E1.SetCellNumberFormat(oix:NumberFormatText,,,,'G2','G' & tmp:CurrentRow)
                E1.WriteToCell(clip(job:ESN)                                ,'G' & tmp:CurrentRow)  !ESN/IMEI
            End

            E1.WriteToCell(clip(job:Model_Number)               ,'H' & tmp:CurrentRow) !Model
            E1.WriteToCell(''                                   ,'I' & tmp:CurrentRow) !Complain Description by Customer
            E1.WriteToCell(''                                   ,'J' & tmp:CurrentRow) !Version
            E1.WriteToCell(''                                   ,'K' & tmp:CurrentRow) !Major Fault State Sort
            E1.WriteToCell(''                                   ,'L' & tmp:CurrentRow) !Lowest Fault State Sort
            E1.WriteToCell(clip(job:Fault_Code3)                ,'M' & tmp:CurrentRow) !Fault Reason
            E1.WriteToCell(clip(job:Fault_Code5)                ,'N' & tmp:CurrentRow) !Repairing Description
            E1.WriteToCell(clip(job:Fault_Code4)                ,'O' & tmp:CurrentRow) !Cliam Level
            E1.WriteToCell('YES'                                ,'P' & tmp:CurrentRow) !Warranty
            E1.WriteToCell(''                                   ,'Q' & tmp:CurrentRow) !Agree Level by ZTE
            E1.WriteToCell(''                                   ,'R' & tmp:CurrentRow) !Spare Parts Used 1
            E1.WriteToCell(''                                   ,'S' & tmp:CurrentRow) !Spare Parts Used 2
            E1.WriteToCell(''                                   ,'T' & tmp:CurrentRow) !SPare Parts Used 3
            E1.WriteToCell(job:Parts_Cost_Warranty              ,'U' & tmp:CurrentRow) !Parts Fee
            E1.WriteToCell(jobe:ClaimValue                      ,'V' & tmp:CurrentRow) !Labour Fee

            If jow:RepairedAt = 'ARC' then
                !was it booked at ARC
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    If tra:BranchIdentification <> jow:BranchID then
                        E1.WriteToCell(man:EDItransportFee              ,'W' & tmp:CurrentRow) !Transportation Fee
                    Else
                        E1.WriteToCell(''                               ,'W' & tmp:CurrentRow) !Transportation Fee
                    End
                Else
                    E1.WriteToCell(man:EDItransportFee              ,'W' & tmp:CurrentRow) !Transportation Fee
                End

            Else
                E1.WriteToCell(''                               ,'W' & tmp:CurrentRow) !Transportation Fee

            End !If x# = True then

            If job:Exchange_Unit_Number > 0 then
                !now look up the exchange IMEI no
                Access:Exchange.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number = job:Exchange_Unit_Number
                If Access:Exchange.TryFetch(xch:Ref_Number_Key) = Level:benign then
                    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'X2','X' & tmp:CurrentRow)
                    E1.WriteToCell(clip(xch:ESN)            ,'X' & tmp:CurrentRow) !'NewIMEIESNSN'
                Else
                    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'X2','X' & tmp:CurrentRow)
                    E1.WriteToCell(job:ESN                       ,'X' & tmp:CurrentRow) !'NewIMEIESNSN'
                End
            Else
                !was a third party swap done
                Access:JobThird.clearkey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                If Access:jobThird.tryfetch(jot:RefNumberKey) = level:benign then
                    !jobthird record
                    E1.SetCellNumberFormat(oix:NumberFormatText,,,,'X2','X' & tmp:CurrentRow)
                    E1.WriteToCell(job:ESN                       ,'X' & tmp:CurrentRow) !'NewIMEIESNSN'
                Else
                    !no job third record
                    E1.WriteToCell(''                           ,'X' & tmp:CurrentRow) !'NewIMEIESNSN'
                End
            End

            E1.WriteToCell(''                                   ,'Y' & tmp:CurrentRow) !Name of Customer
            E1.WriteToCell(''                                   ,'Z' & tmp:CurrentRow) !Tel No.
            E1.WriteToCell(''                                   ,'AA' & tmp:CurrentRow) !Remark

            tmp:CurrentRow += 1
        End
    End


    E1.SetCellFontName('Tahoma','A1','AA' & tmp:CurrentRow)
    E1.SetCellFontSize(8,'A1','AA' & tmp:CurrentRow)
    !E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,,'D2','E' & tmp:CurrentRow)
    E1.SetColumnWidth('A','AA')
    E1.SelectCells('A2')
    E1.SaveAs(FileName)
    E1.CloseWorkBook(2)
    E1.Kill
    ! Inserting (DBH 18/01/2006) #6964 - Reopen the document to reformat the date
    ExcelSetup(0)
    ExcelOpenDoc(FileName)
    ExcelSelectSheet('ZTE SE')
    !ExcelFormatRange('L2:O' & tmp:CurrentRow,'0#######')
    ExcelSaveWorkBook(FileName)
    ExcelClose()
    ! End (DBH 18/01/2006) #6964


    If f:Silent = 0
        Beep(Beep:SystemAsterisk);  Yield()
        Case Missive('Service Report Created.'&|
            '|'&|
            '|File: ' & Clip(FileName) & '.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive

    End ! If f:Silent = 0
local.ServiceHistoryReport        Procedure(Long f:Records,Byte f:FirstSecondYear)
local:ExcelFileName         String(255)
tmp:InvoiceTotal            Real()
tmp:InvoiceDate             Date()
tmp:Franchise               String(30)
tmp:BranchCode              String(30)
tmp:Branch_Code             String(30)
tmp:SwapIMEI                String(30)
tmp:SwapMSN                 String(30)
tmp:Site_Location           String(30)
tmp:Stock_Type              String(30)
tmp:Comment                 String(255)
tmp:DateCode                String(30)
!these have been moved to local data
!tmp:InFault                 String(30)
!tmp:InFaultDescription      String(30)
!tmp:OutFault                String(30)
!tmp:OutFaultDescription     String(30)

Code
    Do Prog:ProgressSetup
    Prog:TotalRecords = f:Records
    Prog:ShowPercentage = 1 !Show Percentage Figure
    tmp:ExportFile = Clip(f:Manufacturer) & ' SHR'
    If f:FirstSecondYear
        tmp:ExportFile    = Clip(tmp:ExportFile) & ' 2yr'
        f:FileList = clip(f:FileList) & '|' & clip(tmp:ExportFile) & '.csv'
    ELSE
        f:FileList = clip(f:FileList) & '|' & clip(tmp:ExportFile) & '.csv'
    End !If func:SecondYear

    Do GetEDIPath

    tmp:ExportFile = Clip(man:EDI_Path) & Clip(tmp:ExportFile)

    local:ExcelFileName = Clip(tmp:ExportFile) & '.xls'
    tmp:ExportFile = Clip(tmp:ExportFile) & '.csv'

    Remove(ExportFile)
    Remove(local:ExcelFileName)
    Create(ExportFile)
    Open(ExportFile)

    Clear(expfil:Record)
    expfil:Line = 'Date Booked,Date Completed,Purchase Date,Administrator,Claim Submitted Date,Claim Submitted Time,Claim Status'
    Case f:Type
    Of 'QUE'
        expfil:Line   = Clip(expfil:Line) & ',Query Date'
    Of 'YES'
        expfil:Line   = Clip(expfil:Line) & ',Accepted Date'
    Of 'FIN'
        expfil:Line   = Clip(expfil:Line) & ',Reconciled Date'
    Of 'EXC'
        expfil:Line   = Clip(expfil:Line) & ',Rejected Date'
    End ! Case f:Type
    expfil:Line   = Clip(expfil:Line) & ',Franchise,Repair Location,Job Number,Engineer Option,' &|
                    'Branch Code,Customer Name,Invoice Total,Labour Total,Spares Total,Exchange Cost,MFTR Invoice Total,MFTR Labour Total,MFTR Parts Total,Make,Model,' & |
                    'I.M.E.I. Number,Serial Number,Swap I.M.E.I. Number,Swap Serial Number,Date Code, In Fault Code,' & |
                    'In Fault Description, Out Fault Code, Out Fault Description, Labour Level,Comment,' &|
                    'Exchange Site Location,Exchange Branch Code,Exchange Stock Type'

    Loop Parts# = 1 To 10
        expfil:Line = Clip(expfil:Line) & ',Part Number,Description,Quantity,Total Value'
    End ! Loop Parts# = 1 To 10
    Add(ExportFile)

    Access:JOBSWARR.ClearKey(jow:ClaimStatusManFirstKey)
    If f:Type <> ''
        jow:Status = f:Type
    Else ! If f:Type <> ''
        jow:Status          = 'NO'
    End ! If f:Type <> ''
    
    jow:Manufacturer    = f:Manufacturer
    jow:ClaimSubmitted  = f:StartDate
    jow:FirstSecondYear = f:FirstSecondYear
    Set(jow:ClaimStatusManFirstKey,jow:ClaimStatusManFirstKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:JOBSWARR.NEXT()
                        Prog:Exit = 1
                        Break
                    End !If
                    If f:Type <> ''
                        If jow:Status <> f:Type
                            Prog:Exit = 1
                            Break
                        End ! If jow:Status <> f:Type
                    Else ! If f:Type <> ''
                        If jow:Status <> 'NO'
                            Prog:Exit = 1
                            Break
                        End ! If jow:Status <> 'NO'
                    End ! If f:Type <> ''
                    If jow:Manufacturer    <> f:Manufacturer      |
                    Or jow:ClaimSubmitted > f:EndDate      |
                    Or jow:FirstSecondYear <> f:FirstSecondYear      
                        Prog:Exit = 1
                        Break
                    End

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = jow:RefNumber
                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = jow:RefNumber
                    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign


                    tmp:InvoiceTotal = jobe:ClaimValue + jobe:ClaimPartsCost + job:Courier_Cost_Warranty

                    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                    inv:invoice_Number = job:Invoice_Number_Warranty
                    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                        !Found
                        tmp:InvoiceDate = inv:Date_Created
                    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                        !Error
                        tmp:InvoiceDate = 0
                    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

                    tmp:Franchise = ''
                    tmp:BranchCode = ''
                    tmp:Branch_Code = ''

                    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        tmp:Franchise = tra:Company_Name
                        tmp:BranchCode = tra:BranchIdentification
                    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

                    ExchangeNumber# = 0
                    If job:Exchange_Unit_Number > 0
                        !RRC exchanges for normal jobs appear on the report
                        !48 Hour RRC exchanges do not
                        !2nd exchanges appear if it exists - 3788 (DBH: 06-04-2004)
                        If jobe:Engineer48HourOption = 1
                            If jobe:ExchangedAtRRC = True
                                If jobe:SecondExchangeNumber > 0
                                    ExchangeNumber# = jobe:SecondExchangeNumber
                                Else ! If jobe:SecondExchangeNumber > 0
                                    ExchangeNumber# = 0
                                End ! If jobe:SecondExchangeNumber > 0
                            End ! If jobe:ExchangedAtRRC = True
                        Else ! If jobe:Engineer48HourOption = 1
                            ExchangeNumber# = job:Exchange_Unit_Number
                        End ! If jobe:Engineer48HourOption = 1
                    End ! If job:Exchange_Unit_Number > 0

                    tmp:SwapIMEI = ''
                    tmp:SwapMSN = ''
                    tmp:Site_Location = ''
                    tmp:Stock_Type = ''

                    If ExchangeNumber# > 0
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = ExchangeNumber#
                        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                            !Found
                            tmp:SwapIMEI = xch:ESN
                            tmp:SwapMSN = xch:MSN
                            tmp:Site_Location = xch:Location
                            tmp:Stock_Type = xch:Stock_Type
                        Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign

                    End ! If ExchangeNumber# > 0

                    Access:TRADEACC.ClearKey(tra:SiteLocationKey)
                    tra:SiteLocation = tmp:Site_Location
                    If Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign
                        !Found
                        tmp:Branch_Code = tra:BranchIdentification
                    Else ! If Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign
                        !Error
                    End ! If Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign

                    !following moved to routines so they can be called from elsewhere
                   !JC 02/04/12 - for TB12360

                    Do GetInFault           !returns values in tmp:InFault and tmp:InFaultDescription
                    Do GetOutFault          !writes values to tmp:OutFault and Tmp:outFaultDecsription


                    tmp:Comment = ''
                    Access:JOBOUTFL.ClearKey(joo:LevelKey)
                    joo:JobNumber = job:Ref_Number
                    joo:Level     = 0
                    Set(joo:LevelKey,joo:LevelKey)
                    Loop
                        If Access:JOBOUTFL.NEXT()
                           Break
                        End !If
                        If joo:JobNumber <> job:Ref_Number      |
                        Or joo:Level     <> 0      |
                            Then Break.  ! End If
                        If tmp:Comment <> ''
                            tmp:Comment = Clip(tmp:Comment) & ', '
                        End !If tmp:Comment <> ''
                        tmp:Comment = Clip(tmp:Comment) & joo:Description
                    End !Loop

                    !Work out Date Code
                    Case job:Manufacturer
                        Of 'SIEMENS'
                            tmp:DateCode    = job:Fault_Code3
                        Of 'ALCATEL'
                            tmp:DateCode    = job:Fault_Code3
                        Of 'ERICSSON'
                            tmp:DateCode    = ''''&Clip(job:Fault_Code7) & Clip(job:Fault_Code8)
                        Else
                            tmp:DateCode    = '0'
                    End !job:Manufacturer

                    Clear(expfil:Record)
                    !Date Booked
                    expfil:Line        = '"' & Format(job:Date_Booked,@d06b)
                    !Date Completed
                    expfil:Line        = Clip(expfil:Line) & '","' & Format(job:Date_Completed,@d06b)
                    !Purchase Date
                    expfil:Line        = Clip(expfil:Line) & '","' & Format(job:DOP,@d06b)

                    ! Inserting (DBH 26/06/2007) # 9188 - Add vetting details from audit trail
                    !Administrator / Date/Time Claimed
                    Found# = 0
                    Access:AUDIT.Clearkey(aud:TypeActionKey)
                    aud:Ref_Number = job:Ref_Number
                    aud:Type = 'JOB'
                    aud:Date = TOday()
                    aud:Action = 'WARRANTY CLAIM SUBMITTED TO MFTR'
                    Set(aud:TypeActionKey,aud:TypeActionKey)
                    Loop ! Begin Loop
                        If Access:AUDIT.Next()
                            Break
                        End ! If Access:AUDIT.Next()
                        If aud:Ref_Number <> job:Ref_Number
                            Break
                        End ! If aud:Ref_Number <> job:Ref_Number
                        If aud:Type <> 'JOB'
                            Break
                        End ! If aud:Type <> 'JOB'
                        If aud:Action <> 'WARRANTY CLAIM SUBMITTED TO MFTR'
                            Break
                        End ! If aud:Action <> 'WARRANTY SUBMITTED'
                        Access:USERS.ClearKey(use:User_Code_Key)
                        use:User_Code = aud:User
                        If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                            !Found
                            Found# = 1
                            expfil:Line = Clip(expfil:Line) & '","' & Clip(use:Forename) & ' ' & Clip(use:Surname)
                            expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                            expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Time,@t01b)
                            Break
                        Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                            !Error
                            
                        End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    End ! Loop



!                    Found# = 0
!                    Access:AUDIT.Clearkey(aud:TypeRefKey)
!                    aud:Ref_Number = job:Ref_Number
!                    aud:Type = 'JOB'
!                    aud:Date = Today()
!                    Set(aud:TypeRefKey,aud:TypeRefKey)
!                    Loop ! Begin Loop
!                        If Access:AUDIT.Next()
!                            Break
!                        End ! If Access:AUDIT.Next()
!                        If aud:Ref_Number <> job:Ref_Number
!                            Break
!                        End ! If aud:Ref_Number <> job:Ref_Number
!                        If aud:Type <> 'JOB'
!                            Break
!                        End ! If aud:Type <> 'JOB'
!                        If aud:Date > Today()
!                            Break
!                        End ! If aud:Date > Today()
!! Changing (DBH 03/08/2007) # 9246 - Also check for "Warranty Claim Rejected" entry
!!                       If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
!! to (DBH 03/08/2007) # 9246
!                        If Instring('CLAIM VETTED',Upper(aud:Action),1,1) Or Upper(aud:Action) = 'WARRANTY CLAIM REJECTED'
!! End (DBH 03/08/2007) #9246
!                            Access:USERS.ClearKey(use:User_Code_Key)
!                            use:User_Code = aud:User
!                            If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
!                                !Found
!                                Found# = 1
!                                expfil:Line = Clip(expfil:Line) & '","' & Clip(use:Forename) & ' ' & Clip(use:Surname)
!                                expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
!                                expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Time,@t01b)
!                                Break
!                            Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
!                                !Error
!                                
!                            End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
!                        End ! If Instring('CLAIM VETTED',Upper(aud:Action),1,1)
!                    End ! Loop
                    If Found# = 0
                        expfil:Line = Clip(expfil:Line) & '","'
                        expfil:Line = Clip(expfil:Line) & '","'
                        expfil:Line = Clip(expfil:Line) & '","'
                    End ! If Found# = 0
                    ! End (DBH 26/06/2007) #9188
                    ! Claim Status
                    expfil:Line   = Clip(expfil:Line) & '","' & jobe:WarrantyClaimStatus
                    ! Dates
                    If f:Type = 'QUE' Or f:Type = 'YES' Or f:Type = 'FIN' Or f:Type = 'EXC'
                        Access:AUDIT.Clearkey(aud:Ref_Number_Key)
                        aud:Ref_Number = job:Ref_Number
                        aud:Date = TOday()
                        Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
                        Loop ! Begin Loop
                            If Access:AUDIT.Next()
                                Break
                            End ! If Access:AUDIT.Next()
                            If aud:Ref_Number <> job:Ref_Number
                                Break
                            End ! If aud:Ref_Number <> job:Ref_Number

                            Case f:Type
                            Of 'QUE'
                                If aud:Action = 'WARRANTY CLAIM QUERY'
                                    Found# = 1
                                    expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                                    Break
                                End ! If aud:Action = 'WARRANTY CLAIM QUERY'
                                If aud:Action = 'CLAIM VETTED: QUERY'
                                    Found# = 1
                                    expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                                    Break
                                End ! If aud:Action = 'WARRANTY CLAIM QUERY'
                            Of 'YES'
                                If aud:Action = 'WARRANTY CLAIM ACCEPTED'
                                    Found# = 1
                                    expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                                    Break
                                End ! If aud:Action = 'WARRANTY CLAIM QUERY'
                            Of 'FIN'
                                If aud:Action <> 'WARRANTY CLAIM RECONCILED'
                                    Found# = 1
                                    expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                                    Break
                                End ! If aud:Action = 'WARRANTY CLAIM QUERY'
                            Of 'EXC'
                                If aud:Action <> 'WARRANTY CLAIM REJECTED'
                                    Found# = 1
                                    expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                                    Break
                                End ! If aud:Action = 'WARRANTY CLAIM QUERY'
                                If aud:Action = 'CLAIM VETTED: REJECTED'
                                    Found# = 1
                                    expfil:Line = Clip(expfil:Line) & '","' & Format(aud:Date,@d06b)
                                    Break
                                End ! If aud:Action = 'WARRANTY CLAIM QUERY'
                            End ! Case f:Type
                        End ! Loop
                        If Found# = 0
                            expfil:Line   = Clip(expfil:Line) & '","'
                        End ! If Found# = 0
                    End ! If f:Type = 'QUE' Or f:Type = 'YES' Of f:Type = 'FIN' Or f:Type = 'EXC'

                    !Franchise
                    expfil:Line        = Clip(expfil:Line) & '","' & Clip(tmp:Franchise)
                    !Repair Location
                    If jobe:WebJob And SentToHub(job:Ref_Number) = 0
                        expfil:Line        = Clip(expfil:Line) & '","RRC'
                    Else ! If SentToHub(job:Ref_Number)
                        expfil:Line        = Clip(expfil:Line) & '","ARC'
                    End ! If SentToHub(job:Ref_Number)
                    !Job Number
                    expfil:Line        = Clip(expfil:Line) & '","' & job:Ref_Number
                    !Engineer Option
                    Case jobe:Engineer48HourOption
                    Of 1
                        expfil:Line        = Clip(expfil:Line) & '","48H'
                    Of 2
                        expfil:Line        = Clip(expfil:Line) & '","ARC'
                    Of 3
                        expfil:Line        = Clip(expfil:Line) & '","7DT'
                    Of 4
                        expfil:Line        = Clip(expfil:Line) & '","STD'
                    Else
                        expfil:Line        = Clip(expfil:Line) & '","'
                    End ! Case jobe:Engineer48HourOption
               
                    !Branch Code
                    expfil:Line        = Clip(expfil:Line) & '","' & Clip(tmp:BranchCode)
                    !Customer Name
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(job:Company_Name)
                    !Invoice Total
                    expfil:Line         = Clip(expfil:Line) & '","' & Format(tmp:InvoiceTotal,@n_14.2)
                    !Labour Total
                    expfil:Line         = Clip(expfil:Line) & '","' & Format(jobe:ClaimValue,@n_14.2)
                    !Spares Total
                    expfil:Line         = Clip(expfil:Line) & '","' & FOrmat(jobe:ClaimPartsCost,@n_14.2)
                    !Exchange Cost
                    expfil:Line         = Clip(expfil:Line) & '","' & FOrmat(job:Courier_Cost_Warranty,@n_14.2)
                    !MFTR Invoice Total
                    !MFTR Invoice Labour
                    !MFTR Invoice Parts
                    Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
                    sta:Model_Number = job:Model_Number
                    sta:Charge_Type = job:Warranty_Charge_Type
                    sta:Unit_Type = job:Unit_Type
                    sta:Repair_Type = job:Repair_Type_Warranty
                    If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                        !Found
                        expfil:Line   = Clip(expfil:Line) & '","' & Format(sta:WarrantyClaimRate + jobe:CLaimPartsCost,@n_14.2)
                        expfil:Line   = Clip(expfil:Line) & '","' & Format(sta:WarrantyCLaimRate,@n_14.2)
                        expfil:Line   = Clip(expfil:Line) & '","' & Format(jobe:ClaimPartsCost,@n_14.2)
                    Else ! If Access:STDCHRG.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                        !Error
                        expfil:Line   = Clip(expfil:Line) & '","'
                        expfil:Line   = Clip(expfil:Line) & '","'
                        expfil:Line   = Clip(expfil:Line) & '","'
                    End ! If Access:STDCHRG.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign

                    !Make
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(job:Manufacturer)
                    !Model
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(job:Model_Number)
                    !IMEI Number
                    expfil:Line         = Clip(expfil:Line) & '","''' & Clip(job:ESN)
                    !Serial Number
                    If job:MSN <> '' And job:MSN <> 'N/A'
                        expfil:Line         = Clip(expfil:Line) & '","' & Clip(job:MSN)
                    Else ! If job:MSN <> '' And job:MSN <> 'N/A'
                        expfil:Line         = Clip(expfil:Line) & '","'
                    End ! If job:MSN <> '' And job:MSN <> 'N/A'
                    !Swap IMEI Number
                    expfil:Line         = Clip(expfil:Line) & '","''' & Clip(tmp:SwapIMEI)
                    !Swap Serial Number
                    If tmp:SwapMSN <> '' And tmp:SwapMSN <> 'N/A'
                        expfil:Line         = Clip(expfil:Line) & '","''' & Clip(tmp:SwapMSN)
                    Else ! If tmp:SwapMSN <> '' And tmp:SwapMSN <> 'N/A'
                        expfil:Line         = Clip(expfil:Line) & '","'
                    End ! If tmp:SwapMSN <> '' And tmp:SwapMSN <> 'N/A'
                    !Date Code
                    If tmp:DateCode = 0
                        expfil:Line         = Clip(expfil:Line) & '","'
                    Else ! If tmp:DateCode = 0
                        expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:DateCode)
                    End ! If tmp:DateCode = 0
                    !In Fault Code
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:InFault)
                    !In Fault Description
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:InFaultDescription)
                    !Out Fault Code
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:OutFault)
                    !Out Fault Description
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:OutFaultDescription)
                    !Labour Level
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(job:Repair_Type_Warranty)
                    !Comment
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(BHStripNonAlphaNum(tmp:Comment,' '))
                    !Exchange Site Location
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:Site_Location)
                    !Exchange Branch Code
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:Branch_Code)
                    !Exchange Stock Type
                    expfil:Line         = Clip(expfil:Line) & '","' & Clip(tmp:Stock_Type)
                    !Part Number
                    !Description
                    !Quantity
                    !Total Value
                    Count# = 0
                    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                    wpr:Ref_Number = job:Ref_Number
                    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    Loop ! Begin Loop
                        If Access:WARPARTS.Next()
                            Break
                        End ! If Access:WARPARTS.Next()
                        If wpr:Ref_Number <> job:Ref_Number
                            Break
                        End ! If wpr:Part_Number <> job:Ref_Number
                        Count# += 1
                        ! Only allow 10 parts (DBH: 27/02/2007)
                        If Count# > 10
                            Break
                        End ! If COunt# > 10
                        expfil:Line         = Clip(expfil:Line) & '","' & Clip(wpr:Part_Number)
                        expfil:Line         = Clip(expfil:Line) & '","' & Clip(wpr:Description)
                        expfil:Line         = Clip(expfil:Line) & '","' & Clip(wpr:Quantity)
                        expfil:Line         = Clip(expfil:Line) & '","' & Format(wpr:Quantity * wpr:Purchase_Cost,@n_14.2)

                    End ! Loop

                    expfil:Line = Clip(expfil:Line) & '"'
                    Append(ExportFile)

                    Prog:RecordCount += 1

                    ?Prog:UserString{Prop:Text} = 'Creating Report: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Close(ExportFile)
!    E1.Init(0,0)
!    E1.OpenWorkBook(tmp:ExportFile)
!    E1.SelectCells('A1')
!    E1.AutoFilter('A1','BQ1')
!    E1.SetCellFontStyle('Bold','A1','BQ1')
!    E1.SetColumnWidth('A','BQ')
!    E1.SelectCells('A2')
!    E1.FreezePanes
!    E1.SaveAs(Sub(tmp:ExportFile,1,Len(Clip(tmp:ExportFile)) - 4) & '.xls')
!    E1.CloseWorkBook(2)
!    E1.Kill()
!    Remove(tmp:ExportFile)
    Do Prog:ProgressFinished



!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
PrintTechnicalReport PROCEDURE (f:Manufacturer,f:SHist,f:Type) !Generated from procedure template - Window

tmp:DateRangeType    BYTE(0)
tmp:DateFrom         DATE
tmp:DateTo           DATE
locFileList          STRING(255),AUTO
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       GROUP,AT(356,242,95,28),USE(?Group2),HIDE
                         PROMPT('Claim Submitted Date To'),AT(356,242),USE(?tmp:DateTo:Prompt:2),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(356,255,64,10),USE(tmp:DateTo,,?tmp:DateTo:2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Claim Submitted Date To'),TIP('Claim Submitted Date To'),REQ,UPR
                         BUTTON,AT(424,252),USE(?PopCalendar:3),TRN,FLAT,ICON('lookupp.jpg')
                       END
                       GROUP,AT(352,146,105,66),USE(?Group1)
                         PROMPT('Claim Submitted Date From'),AT(356,146),USE(?tmp:DateFrom:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         BUTTON,AT(424,156),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                         ENTRY(@d6),AT(356,159,64,10),USE(tmp:DateFrom),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Claim Submitted Date From'),TIP('Claim Submitted Date From'),REQ,UPR
                         PROMPT('Claim Submitted Date To'),AT(356,182),USE(?tmp:DateTo:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         BUTTON,AT(424,191),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         ENTRY(@d6),AT(356,196,64,10),USE(tmp:DateTo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Claim Submitted Date To'),TIP('Claim Submitted Date To'),REQ,UPR
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel5),FILL(09A6A7CH)
                       OPTION,AT(182,152,62,120),USE(tmp:DateRangeType),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Print Jobs Between Date Range'),AT(192,170),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('Print Jobs Before Date'),AT(192,248),USE(?tmp:DateRangeType:Radio2),VALUE('1')
                       END
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Print Warranty Reports'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Button:Print),TRN,FLAT,ICON('printp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020722'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PrintTechnicalReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:DateTo:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:DateFrom = Today()
  tmp:DateTo = Today()
  SELF.AddItem(?Cancel,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:DateTo = TINCALENDARStyle1(tmp:DateTo)
          Display(?tmp:DateTo:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:DateFrom = TINCALENDARStyle1(tmp:DateFrom)
          Display(?tmp:DateFrom)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:DateTo = TINCALENDARStyle1(tmp:DateTo)
          Display(?tmp:DateTo)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:DateRangeType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:DateRangeType, Accepted)
      Case tmp:DateRangeType
      Of 0
          ?Group1{prop:Hide} = 0
          ?Group2{prop:Hide} = 1
      Of 1
          ?Group1{prop:Hide} = 1
          ?Group2{prop:Hide} = 0
      End ! Case tmp:DateRangeType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:DateRangeType, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020722'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020722'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020722'&'0')
      ***
    OF ?Button:Print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Print, Accepted)
      If tmp:DateRangeType = 1
          tmp:DateFrom = Deformat('01/01/2000',@d06)
      End ! If tmp:DateRangeType = 1
      locFileList = ''
      If GenericEDI(f:Manufacturer,tmp:DateFrom,tmp:DateTo,1,f:SHist,f:Type,locFileList) = 0
          IF (locFileList <> '')
              Beep(Beep:SystemAsterisk);  Yield()
              Case Missive('File(s) Created: ' & |
                          '|' & Clip(Sub(locFileList,2,255)),'ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          ELSE ! If (locFileList <> '')
              Beep(Beep:SystemAsterisk);  Yield()
              Case Missive('Report(s) Created.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          END ! If (locFileList <> '')
      Else ! If GenericEDI(f:Manufacturer,0,tmp:DateFrom,tmp:DateTo,0) = 0
      End ! If GenericEDI(f:Manufacturer,0,tmp:DateFrom,tmp:DateTo,0) = 0
          
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
EVO_Export           PROCEDURE  (SentType,SentRecord,Verbose) ! Declare Procedure
GRNQueue             QUEUE,PRE(GRQ)
GRN_RecordNo         LONG
GRN_OrderNumber      LONG
GRN_DespatchNote     STRING(20)
GRN_MinimumLineNo    LONG
GRN_DocumentAmount   DECIMAL(19,4)
                     END
OrderPartQueue       QUEUE,PRE(OPQ)
RecordNumber         LONG
OrderNumber          LONG
part_number          STRING(30)
Purchase_cost        REAL
PostedTo             LONG
                     END
SB2KDEFINI           STRING(255)
EVO_CreateFiles      BYTE
EVO_Company_code     STRING(4)
EVO_XREF1_HD         STRING(240)
EVO_Tax_Code         STRING(2)
EVO_Tax_Code_Exempt  STRING(2)
EVO_Cost_Centre_Parts STRING(10)
EVO_Cost_Centre_3rdParty STRING(10)
EVO_Vendor_Line_No   STRING(10)
EVO_Tax_Line_No      STRING(10)
TempDespatchNoteNumber STRING(30)
TempMinLineNo        LONG
TempDocumentAmount   DECIMAL(19,4)
TempGRNSuffix        LONG
TempOrderNumber      STRING(30)
TempOrderedCurrency  STRING(30)
TempOrderedDailyRate REAL
TmpReal              REAL
TmpReal2             REAL
QueueCount           LONG
OrderCount           LONG
ProcessCount         LONG
ErrorFound           BYTE
ErrorMessage         STRING(100)
ProcCall             STRING(20)
LocalFilename        STRING(500)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:GRNOTES.Open
   Relate:ORDERS.Open
   Relate:ORDPARTS.Open
   Relate:EVO_AP.Open
   Relate:EVO_GL.Open
   Relate:EVO_HEAD.Open
   Relate:EVO_TAX.Open
   Relate:SUPPLIER.Open
   Relate:TRDBATCH.Open
   Relate:TRDPARTY.Open
   Relate:VATCODE.Open
!(SentType,SentRecord,Verbose)
!SentType = Order or Third
!sentRecord = number of record to process   (0 implies all ready to send records)
!Verbose = 'Y' = echo messages to the screen (else log them in a file)

    if Getini('EVO','CREATE',0 ,clip(path())&'\SB2KDEF.INI') = true then

        !Set up
        do ReadDefaults

        Case SentType

            of 'ALL'
                ProcCall = 'AUTO 3RD PARTY'
                do ThirdPartyProcedure
                do OrderProcedure
            

            of 'THIRD'
                ProcCall = 'AUTO 3RD PARTY'
                do ThirdPartyProcedure
            

            of 'ORDER'
                ProcCall = 'AUTO INVOICES'
                do OrderProcedure

        END !case sent Type

        !closedown
        do FinishOff

    END !if using EVO
   Relate:GRNOTES.Close
   Relate:ORDERS.Close
   Relate:ORDPARTS.Close
   Relate:EVO_AP.Close
   Relate:EVO_GL.Close
   Relate:EVO_HEAD.Close
   Relate:EVO_TAX.Close
   Relate:SUPPLIER.Close
   Relate:TRDBATCH.Close
   Relate:TRDPARTY.Close
   Relate:VATCODE.Close
ReadDefaults    Routine

    ErrorFound = true
    LocalFilename = ''

    !EVO defaults: 12777 JAC: 09/10/12 - taken from the defaults screen
    SB2KDEFINI = clip(path())&'\SB2KDEF.INI'
    EVO_CreateFiles             = Getini('EVO','CREATE'                 ,0    ,SB2KDEFINI)
    EVO_Company_code            = Getini('EVO','COMPANY_CODE'           ,''   ,SB2KDEFINI)
    EVO_XREF1_HD                = Getini('EVO','XREF_HD'                ,''   ,SB2KDEFINI)
    EVO_Tax_Code                = Getini('EVO','TAX_CODE'               ,''   ,SB2KDEFINI)
    EVO_Tax_Code_Exempt         = Getini('EVO','TAX_CODE_EXEMPT'        ,''   ,SB2KDEFINI)
    EVO_Cost_Centre_Parts       = Getini('EVO','COST_CENTRE_PARTS'      ,''   ,SB2KDEFINI)
    EVO_Cost_Centre_3rdParty    = Getini('EVO','COST_CENTRE_3RD_PARTY'  ,''   ,SB2KDEFINI)
    EVO_Vendor_Line_No          = Getini('EVO','VENDOR_LINE_NO'         ,''   ,SB2KDEFINI)
    EVO_Tax_Line_No             = Getini('EVO','TAX_LINE_NO'            ,''   ,SB2KDEFINI)
    ProcessCount = 0    !need to reset for both routines

    !dare we proceed?
    if EVO_CreateFiles                = 0  then
        ErrorMessage = 'Fatal Error: EVO_Create  not set'
        Do HandleError
        exit
    END
    if clip(EVO_Company_code)         = '' then
        ErrorMessage = 'Fatal Error: EVO_Company_code  not set'
        Do HandleError
        exit
    END
    if clip(EVO_XREF1_HD)             = '' then
        ErrorMessage = 'Fatal Error: EVO_XREF1_HD  not set'
        Do HandleError
        exit
    END
    if clip(EVO_Tax_Code)             = '' then
        ErrorMessage = 'Fatal Error: EVO_Tax_Code  not set'
        Do HandleError
        exit
    END

    !Check up the VAT code
    Access:vatcode.clearkey(vat:Vat_code_Key)
    vat:VAT_Code = EVO_Tax_Code
    if access:Vatcode.fetch(vat:Vat_code_Key)
        !not found??
        ErrorMessage = 'Fatal Error: EVO_Tax_Code ('&clip(EVO_Tax_Code)&')not valid'
        Do HandleError
        exit
    END
    if clip(EVO_Tax_Code_Exempt)      = '' then
        ErrorMessage = 'Fatal Error: EVO_Tax_Code_Exempt not set'
        Do HandleError
        exit
    END
    !Check up the VAT code
    Access:vatcode.clearkey(vat:Vat_code_Key)
    vat:VAT_Code = EVO_Tax_Code_Exempt
    if access:Vatcode.fetch(vat:Vat_code_Key)
        !not found??
        ErrorMessage = 'Fatal Error: EVO_Tax_Code_Exempt ('&clip(EVO_Tax_Code_exempt)&') not valid'
        Do HandleError
        exit
    END

!Changed 11/1/13 - cost centres can now be blank
!    if clip(EVO_Cost_Centre_Parts)    = '' then
!        ErrorMessage='Fatal Error: EVO_Cost_Centre_Parts  not set'
!        Do HandleError
!        exit
!    END
!    if clip(EVO_Cost_Centre_3rdParty) = '' then
!        ErrorMessage='Fatal Error: EVO_Cost_Centre_3rdParty  not set'
!        Do HandleError
!        exit
!    END

    if clip(EVO_Vendor_Line_No)       = '' then
        ErrorMessage = 'Fatal Error: EVO_Vendor_Line_No  not set'
        Do HandleError
        exit
    END
    if clip(EVO_Tax_Line_No)          = '' then
        ErrorMessage = 'Fatal Error: EVO_Tax_Line_No not set'
        Do HandleError
        exit
    END

    !if we get this far there are no errors
    ErrorFound = false

    EXIT
OrderProcedure        Routine

    if ErrorFound then exit.

    free(GRNQueue)
    clear(GRNQueue)

    If SentRecord = 0 then

        !load the "R" ready to send records into a queue
        !later this will be used to track the order numbers
        !later still this will be used to set these GRNotes to "S" not "R"

        !need the minimum orderpart record number on this order at the same time
        !also work out the document total to save looking up elsewhere

        Access:GRNOTES.ClearKey(grn:KeyEVO_Status)
        grn:EVO_Status = 'R'
        Set(grn:KeyEVO_Status,grn:KeyEVO_Status)
        Loop

            if access:GRNotes.next() then break.
            if grn:EVO_Status <> 'R' then break.

            !reset variables to defaults
            TempDespatchNoteNumber  = ''            !used as a flag to say we found an ordpart record
            TempMinLineNo           = 2147483645    !Maximum of a long = 2147483647
            TempDocumentAmount      = 0

            !ignore grn:Uncaptured
            if grn:Uncaptured = false then

                Do CheckOrdParts

            END !if grn:Uncaptured = false then

            !did we find one in the loop?
            !Add all 'R' to the queue - so they get marked as not R at the end
            !if clip(TempDespatchNoteNumber) <> '' then
                GRQ:GRN_RecordNo      = grn:Goods_Received_Number     !existing parts from the GRN record
                GRQ:GRN_OrderNumber   = grn:Order_Number
                GRQ:GRN_DespatchNote  = TempDespatchNoteNumber        !from the ordparts record - will be blank if none found
                GRQ:GRN_MinimumLineNo = TempMinLineNo
                GRQ:GRN_DocumentAmount= TempDocumentAmount
                Add(GRNQueue)
            !END

        END !loop through GRNotes
    ELSE    !if sentRecord = 0
        !Just one record to deal with
        Access:GRnotes.clearkey(grn:Goods_Received_Number_Key)
        grn:Goods_Received_Number = SentRecord
        if access:GRnotes.fetch(grn:Goods_Received_Number_Key)
            ErrorMessage = 'Fatal Error: This record number '&clip(SentRecord)&' does not exist'
            Do HandleError
            exit
        END
        if grn:Uncaptured = true then
            if Verbose = 'Y' then  !the automatic call will be made when the uncaptured GRnotes is made and should not error
                                   !so this error will only be shown when the call is made from the resubmit screen
                ErrorMessage = 'Fatal Error: This record does '&clip(SentRecord)&' not include prices'
                Do HandleError
            END !if verbose mode
            exit
        END

        !reset variables to defaults
        TempDespatchNoteNumber  = ''            !used as a flag to say we found an ordpart record
        TempMinLineNo           = 2147483645    !Maximum of a long = 2147483647
        TempDocumentAmount      = 0

        Do CheckOrdParts

        GRQ:GRN_RecordNo      = grn:Goods_Received_Number     !existing parts from the GRN record
        GRQ:GRN_OrderNumber   = grn:Order_Number
        GRQ:GRN_DespatchNote  = TempDespatchNoteNumber        !from the ordparts record - will be blank if none found
        GRQ:GRN_MinimumLineNo = TempMinLineNo
        GRQ:GRN_DocumentAmount= TempDocumentAmount
        Add(GRNQueue)

    END     !if sentRecord = 0

    Loop QueueCount = 1 to records(GRNQueue)

        Get(GRNQueue,QueueCount)

        if  clip(GRQ:GRN_DespatchNote) = '' then cycle.  !not to be added to details - but leave in queue to be marked as sent

        ProcessCount += 1

        !need the sequence number of this order - so an odd way of looking it up
        TempGRNSuffix = 0
        Access:GRNotes.ClearKey(grn:Order_Number_Key)
        grn:Order_Number        = GRQ:GRN_OrderNumber
        grn:Goods_Received_Date = 0
        Set(grn:Order_Number_Key,grn:Order_Number_Key)
        Loop

            If Access:GRNotes.NEXT() then Break.
            If grn:Order_Number  <> GRQ:GRN_OrderNumber then break.

            !ignore "uncaptured" types of GRN
            if grn:Uncaptured = true then cycle.

            !update the suffix until we meet the one we want
            TempGRNSuffix += 1

            if GRQ:GRN_RecordNo = grn:Goods_Received_Number
                !this is the record we want
                Break
            END

        END

        TempOrderNumber = 'SS' & Format(grn:Order_Number, @n08) & '/' & Format(TempGRNSuffix, @n02) !Orders.Order_Number with prefix and suffix

        !header details
        Access:EVO_Head.primerecord()
        EVOH:HEADER_TXT       = TempOrderNumber !Orders.Order_Number with prefix and suffix
        EVOH:COMP_CODE        = EVO_Company_code        !new default
        EVOH:DOC_DATE         = format(grn:Goods_Received_Date,@d12)
        EVOH:PSTNG_DATE       = format(grn:Goods_Received_Date,@d12)
        EVOH:REF_DOC_NO       = GRQ:GRN_DespatchNote    !Ordparts.DespatchNoteNumber from queue
        EVOH:XREF1_HD         = EVO_XREF1_HD
        EVOH:XREF2_HD         = GRQ:GRN_DespatchNote    !Ordparts.DespatchNoteNumber from queue - duplicate
        !TB13613 - J - 13/02/14 - if the document amount is zero then post with R (Reserved) not N
        if GRQ:GRN_DocumentAmount = 0 then
            EVOH:Sent_to_EVO_Flag = 'R'
        ELSE
            EVOH:Sent_to_EVO_Flag = 'N'
        END
        Access:EVO_HEAD.update()


        !For each item on the list a GL record
        !but first we have to order these in a list so they can be numbered 1, 2, 3 etc
        Access:Ordparts.clearkey(orp:Order_Number_Key)
        orp:Order_Number = grn:Order_Number
        Set(orp:Order_Number_Key,orp:Order_Number_Key)
        Loop
            if access:Ordparts.next() then break.
            if orp:Order_Number <> grn:Order_Number then break.

            if orp:GRN_Number = grn:Goods_Received_Number then
                !try to combine rows that have the same part_number and Purchase_Cost
                !have we already got this partnumber/purchase cost in the queue?
                Sort(OrderPartQueue,OPQ:part_number,OPQ:Purchase_cost)
                OPQ:part_number    = orp:Part_Number
                OPQ:Purchase_cost  = orp:Purchase_Cost
                Get(OrderPartQueue,OPQ:part_number,OPQ:Purchase_cost)
                if error() then
                    !not found make a new record
                    OPQ:RecordNumber   = orp:Record_Number
                    OPQ:OrderNumber    = 0
                    OPQ:part_number    = ORP:part_number
                    OPQ:Purchase_cost  = ORP:Purchase_cost
                    OPQ:PostedTo       = 0
                    add(OrderPartQueue)
                END !if not found in the queueu
            END !if grn numbers match
        END !loop
        Sort(OrderPartQueue,OPQ:RecordNumber)
        Loop OrderCount = 1 to records(OrderPartQueue)
            get(OrderPartQueue, OrderCount)
            OPQ:OrderNumber = OrderCount
            put(OrderPartQueue)
        END

        !now we can start with the GL records
        Access:Ordparts.clearkey(orp:Order_Number_Key) !this is the same order as creating list so should be found in same order.
        orp:Order_Number = grn:Order_Number
        Set(orp:Order_Number_Key,orp:Order_Number_Key)
        Loop

            if access:Ordparts.next() then break.
            if orp:Order_Number <> grn:Order_Number then break.

            if orp:GRN_Number = grn:Goods_Received_Number then
                !found a matching GRN part

                !NEED THE SUPPLIER - get via order
                Access:orders.clearkey(ord:Order_Number_Key)
                ord:Order_Number = grn:Order_number
                if access:orders.fetch(ord:Order_Number_Key)
                    !error
                END
                Access:supplier.clearkey(sup:Company_Name_Key)
                sup:Company_Name = ord:Supplier
                if access:Supplier.fetch(sup:Company_Name_Key)
                    !error
                END

                !try to combine rows that have the same part_number and Purchase_Cost
                !have we already got this partnumber/purchase cost in the queue?
                Sort(OrderPartQueue,OPQ:part_number,OPQ:Purchase_cost)
                OPQ:part_number    = orp:Part_Number
                OPQ:Purchase_cost  = orp:Purchase_Cost
                Get(OrderPartQueue,OPQ:part_number,OPQ:Purchase_cost)
                !Cannot error - I have made this just above


                if OPQ:PostedTo = 0 then
                    !ready to create new record
                    EVOG:HEADER                = EVOH:RECORD_NO
                    EVOG:GL_ITEMNO_ACC         = OPQ:OrderNumber        !orp:Record_Number - GRQ:GRN_MinimumLineNo + 1 !Changes record no into 1, 2, 3 etc
                    EVOG:GL_ACCOUNT            = sup:EVO_GL_Acc_No
                    EVOG:GL_ITEM_TEXT          = orp:Description        !changed to description - was TempOrderNumber !Orders.Order_Number with prefix and suffix
                    if sup:EVO_TaxExempt then
                        EVOG:GL_TAX_CODE       = EVO_Tax_Code_Exempt
                    ELSE
                        EVOG:GL_TAX_CODE       = EVO_Tax_Code
                    END
                    EVOG:GL_PROFITCENTRE       = sup:EVO_Profit_Centre  !was EVOG:GL_COS_CENTER         = EVO_Cost_Centre_Parts
                    if orp:ReceivedDailyRate <> 0 !tb13161 - was orp:OrderedDailyRate <> 0 - JC - 30/09/13
                        EVOG:GL_DOCUMENT_AMOUNT    = format(orp:Purchase_Cost * orp:Number_Received/orp:ReceivedDailyRate,@n_25.4) !tb13161 - was orp:OrderedDailyRate - JC - 30/09/13
                        EVOG:GL_DOCUMENT_CURRENC   = orp:OrderedCurrency
                        EVOG:GL_LOCAL_AMOUNT       = format(orp:Purchase_Cost * orp:Number_Received ,@n_25.4)
                    ELSE
                        EVOG:GL_DOCUMENT_AMOUNT    = format(orp:Purchase_Cost * orp:Number_Received,@n_25.4)
                        EVOG:GL_DOCUMENT_CURRENC   = 'ZAR'
                        EVOG:GL_LOCAL_AMOUNT       = format(orp:Purchase_Cost * orp:Number_Received ,@n_25.4)
                    END
                    EVOG:GL_EXCHANGE_RATE      = format(orp:ReceivedDailyRate,@n_11.4)  !tb13161 - was orp:OrderedDailyRate  - JC - 30/09/13
                    EVOG:GL_LOCAL_CURRENCY     = 'ZAR'

                    !add a posted flag to the record
                    Access:EVO_GL.Insert()
                    OPQ:PostedTo = 1
                    put(OrderPartQueue)

                ELSE !have posted this one - so find orignial and update it
                    !update an existing line
                    !how the hell do I find one? No key to help - can only use the EVOG:KeyHeader_Number
                    Access:EVO_GL.clearkey(EVOG:KeyHeader_Number)
                    EVOG:HEADER = EVOH:RECORD_NO
                    set(EVOG:KeyHeader_Number,EVOG:KeyHeader_Number)
                    Loop

                        if access:EVO_Gl.next() then break.
                        if EVOG:HEADER <> EVOH:Record_no then break.

                        if EVOG:GL_ITEMNO_ACC = OPQ:OrderNumber then
                            !update the existing amounts by adding this record
                            TmpReal = deformat(EVOG:GL_DOCUMENT_AMOUNT)
                            TmpReal2 = deformat(EVOG:GL_LOCAL_AMOUNT)
                            if orp:ReceivedDailyRate <> 0 !tb13161 - was orp:OrderedDailyRate  - JC - 30/09/13
                                EVOG:GL_DOCUMENT_AMOUNT    = format(TmpReal + orp:Purchase_Cost * orp:Number_Received/orp:ReceivedDailyRate,@n_25.4)  !tb13161 - was orp:OrderedDailyRate  - JC - 30/09/13
                                EVOG:GL_LOCAL_AMOUNT       = format(TmpReal2 + orp:Purchase_Cost * orp:Number_Received ,@n_25.4)
                            ELSE
                                EVOG:GL_DOCUMENT_AMOUNT    = format(TmpReal + orp:Purchase_Cost * orp:Number_Received,@n_25.4)
                                EVOG:GL_LOCAL_AMOUNT       = format(TmpReal2 + orp:Purchase_Cost * orp:Number_Received ,@n_25.4)
                            END
                            Access:EVO_GL.update()
                            BREAK   !have found the only record
                        END !if found by ordernumber
                    END !loop through EVOG:KeyHeader_Number
                END
                !remember the orderedcurrency on this orp before the break changes it
                TempOrderedCurrency = orp:OrderedCurrency
                TempOrderedDailyRate = orp:ReceivedDailyRate        !tb13161 - was orp:OrderedDailyRate  - JC - 30/09/13

            END !if GRN numbers match
        END !loop through ordparts to find matching GRN numbers

        !A summary AP
        EVOA:HEADER_NUMBER         = EVOH:RECORD_NO
        EVOA:AP_ITEMNO_ACC         = EVO_Vendor_Line_No  
        EVOA:AP_VENDOR_NO          = sup:EVO_Vendor_Number
        EVOA:AP_ITEM_TEXT          = TempOrderNumber !Orders.Order_Number with prefix and suffix

        ! #12777 Move the tax code lookup hear as it's needed for the doc/local amounts (DBH: 01/02/2013)
        if sup:EVO_TaxExempt then
            EVOT:TAX_TAX_CODE           = EVO_Tax_Code_Exempt
        ELSE
            EVOT:TAX_TAX_CODE           = EVO_Tax_Code
        END

        !look up the VAT
        Access:vatcode.clearkey(vat:Vat_code_Key)
        vat:VAT_Code = EVOT:TAX_TAX_CODE
        if access:Vatcode.fetch(vat:Vat_code_Key)
            !not found? This was checked at the start
            vat:VAT_Rate = 0
        END
        
        if TempOrderedCurrency = '' then
            IF (sup:EVO_TaxExempt = 0)
                ! #12777 Include VAT if supplier is NOT Tax Exempt (DBH: 01/02/2013)
                EVOA:AP_DOCUMENT_AMOUNT    = format((GRQ:GRN_DocumentAmount + (GRQ:GRN_DocumentAmount * (vat:VAT_Rate/100))) * (-1),@n-_25.4)
            ELSE ! IF
                EVOA:AP_DOCUMENT_AMOUNT    = format(GRQ:GRN_DocumentAmount * (-1),@n-_25.4)
            END ! IF
            EVOA:AP_DOCUMENT_CURRENC  = 'ZAR'
            !EVOA:AP_LOCAL_AMOUNT      = format(GRQ:GRN_DocumentAmount * (-1),@n-25.4)
            EVOA:AP_LOCAL_AMOUNT      = EVOA:AP_DOCUMENT_AMOUNT  ! #12777 These two fields are always the same here (DBH: 01/02/2013)
            EVOA:AP_EXCHANGE_RATE     = '0.0000'
        ELSE
            IF (sup:EVO_TaxExempt = 0)
                ! #12777 Include VAT if supplier is NOT Tax Exempt (DBH: 01/02/2013)
                EVOA:AP_DOCUMENT_AMOUNT    = format((GRQ:GRN_DocumentAmount + (GRQ:GRN_DocumentAmount * (vat:VAT_Rate/100))) * (-1)/ TempOrderedDailyRate,@n-_25.4)
            ELSE ! IF
                EVOA:AP_DOCUMENT_AMOUNT    = format(GRQ:GRN_DocumentAmount * (-1)/ TempOrderedDailyRate,@n-_25.4)
            END ! IF
            EVOA:AP_DOCUMENT_CURRENC   = TempOrderedCurrency
            IF (sup:EVO_TaxExempt = 0)
                ! #12777 Include VAT if supplier is NOT Tax Exempt (DBH: 01/02/2013)
                EVOA:AP_LOCAL_AMOUNT       = format((GRQ:GRN_DocumentAmount + (GRQ:GRN_DocumentAmount * (vat:VAT_Rate/100))) * (-1),@n-_25.4)
            ELSE ! IF
                EVOA:AP_LOCAL_AMOUNT       = format(GRQ:GRN_DocumentAmount * (-1),@n-_25.4)
            END ! IF
            EVOA:AP_EXCHANGE_RATE      = format(TempOrderedDailyRate,@n-_11.4)
        END
        EVOA:AP_LOCAL_CURRENCY     = 'ZAR'
        
        Access:EVO_AP.insert()



        !A new summary TAX 
        EVOT:HEADER_NUMBER          = EVOH:RECORD_NO
        EVOT:TAX_ITEMNO_ACC         = EVO_Tax_Line_No


        !these should be the net amout, before tax
        if TempOrderedCurrency  = '' then
            EVOT:TAX_DOCUMENT_CURRENC   = 'ZAR'
            EVOT:TAX_BASEAMOUNT         = format(GRQ:GRN_DocumentAmount, @n-_25.4)
            EVOT:TAX_LOCALBASEAMOUNT    = EVOT:TAX_BASEAMOUNT
        ELSE
            EVOT:TAX_DOCUMENT_CURRENC   = TempOrderedCurrency
            EVOT:TAX_BASEAMOUNT         = format(GRQ:GRN_DocumentAmount / TempOrderedDailyRate, @n-_25.4)
            EVOT:TAX_LOCALBASEAMOUNT    = format(GRQ:GRN_DocumentAmount, @n-_25.4)
        END

! Moved code higher up
!        if sup:EVO_TaxExempt then
!            EVOT:TAX_TAX_CODE           = EVO_Tax_Code_Exempt
!        ELSE
!            EVOT:TAX_TAX_CODE           = EVO_Tax_Code
!        END
!
!        !look up the VAT
!        Access:vatcode.clearkey(vat:Vat_code_Key)
!        vat:VAT_Code = EVOT:TAX_TAX_CODE
!        if access:Vatcode.fetch(vat:Vat_code_Key)
!            !not found? This was checked at the start
!            vat:VAT_Rate = 0
!        END

        !These should be the amount of VAT charged
        if TempOrderedDailyRate = 0 then
            EVOT:TAX_DOCUMENT_AMOUNT    = format(GRQ:GRN_DocumentAmount  * vat:VAT_Rate / 100  ,@n-_25.4)
            EVOT:TAX_LOCAL_AMOUNT       = EVOT:TAX_DOCUMENT_AMOUNT
        ELSE
            EVOT:TAX_LOCAL_AMOUNT       = format(GRQ:GRN_DocumentAmount   * vat:VAT_Rate / 100 ,@n-_25.4)
            EVOT:TAX_DOCUMENT_AMOUNT    = format((GRQ:GRN_DocumentAmount/ TempOrderedDailyRate) * VAT:VAT_Rate / 100,@n-_25.4)
        END

        EVOT:TAX_LOCAL_CURRENCY     = 'ZAR'
        Access:EVO_Tax.insert()



    End !loop through GRNQueue

    !Use the grnqueue to set the EVO_Status to sent
    Loop QueueCount = 1 to records(GRNQueue)
        Get(GRNQueue,QueueCount)
        Access:GRNotes.clearkey(grn:Goods_Received_Number_Key)
        grn:Goods_Received_Number = GRQ:GRN_RecordNo
        if access:GRNotes.fetch(grn:Goods_Received_Number_Key) = level:Benign
            if  clip(GRQ:GRN_DespatchNote) = '' then
                grn:EVO_Status = 'X'
            ELSE
                grn:EVO_Status = 'S'
            END
            Access:GRnotes.update()
        END
    END !loop through GRNQueue

    EXIT




CheckOrdParts   Routine        !shared by multiple and single checks


    !"oft we jolly well go" (c)
    Access:Ordparts.clearkey(orp:Order_Number_Key)
    orp:Order_Number = grn:Order_Number
    Set(orp:Order_Number_Key,orp:Order_Number_Key)
    Loop

        if access:Ordparts.next() then break.
        if orp:Order_Number <> grn:Order_Number then break.


        if orp:GRN_Number = grn:Goods_Received_Number then

            !minimum line number                 
            if orp:Record_Number < TempMinLineNo then TempMinLineNo = orp:Record_Number.

            !NEED THE SUPPLIER - get via order
            Access:orders.clearkey(ord:Order_Number_Key)
            ord:Order_Number = grn:Order_number
            if access:orders.fetch(ord:Order_Number_Key)
                !error
                Errormessage = 'Error on fetch Order from order number: '&clip(grn:Order_number)
                do HandleError
                TempDespatchNoteNumber = ''
                break
            END
            Access:supplier.clearkey(sup:Company_Name_Key)
            sup:Company_Name = ord:Supplier
            if access:Supplier.fetch(sup:Company_Name_Key)
                !error
                Errormessage='Error on fetch Supplier from order number: '&clip(grn:Order_number)
                Do HandleError
                TempDespatchNoteNumber = ''
                break
            END

            if sup:EVO_Excluded = 1 then
                !TB13163 - J - 13/02/14 - This is excluded - no error, but the record is not to be posted
                TempDespatchNoteNumber = ''
                break
            END

            if sup:EVO_GL_Acc_No = '' or sup:EVO_Vendor_Number = '' !or sup:EVO_Profit_Centre = '' then
                Errormessage='Supplier '&clip(sup:Company_Name)&' is not set up to use EVO'
                Do HandleError
                TempDespatchNoteNumber = ''
                break
            END

            !Note the suppliers invoice number and update the cost
            TempDespatchNoteNumber = orp:DespatchNoteNumber       !this should be the same for all parts, but may be different!!
                                                                  !assume the first one found is the correct one
            TempDocumentAmount    += orp:Purchase_Cost * orp:Number_Received

        END !if GRN numbers match

    END !loop through ordparts to find matching GRN numbers
ThirdPartyProcedure     Routine

    if ErrorFound then exit.

    free(GRNQueue)
    clear(GRNQueue)

    if sentRecord <> 0 then
        Access:TrdBatch.clearkey(trb:RecordNumberKey)
        trb:RecordNumber = SentRecord
        if access:TrdBatch.fetch(trb:RecordNumberKey)
            ErrorMessage = 'Fatal Error: Record does not exist for this number'
            do HandleError
            EXIT
        END
        if trb:Status = 'OUT' then
            ErrorMessage = 'Fatal Error: This is not an incomming record'
            do HandleError
            EXIT
        END
        !check the supplier detail here
        Access:trdparty.clearkey(trd:Company_Name_Key)
        trd:Company_Name = trb:Company_Name
        if access:trdparty.fetch(trd:Company_Name_Key)
            ErrorMessage = 'Company '&clip(trb:Company_Name)&' not found for trdbatch '&clip(trb:RecordNumber)
            do HandleError
            EXIT
        ELSE
            if trd:EVO_Excluded = 1 then
                !TB13163 - J - 13/02/14  this third party does not get posted to EVO, nor does it get reported as "not set up"
                EXIT
            END
            if trd:EVO_AccNumber = '' or trd:EVO_VendorNumber = '' !or trd:EVO_Profit_Centre = '' then
                ErrorMessage = 'Company '&clip(trb:Company_Name)&' is not set up for EVO use'
                do HandleError
                EXIT
            ELSE
                !at last
                GRQ:GRN_RecordNo = trb:RecordNumber
                Add(GRNQueue)
            END
        END

    ELSE
        Access:TRDBATCH.clearkey(trb:KeyEVO_Status)
        trb:EVO_Status = 'R'
        set(trb:KeyEVO_Status,trb:KeyEVO_Status)
        loop

            if access:TrdBatch.next() then break.
            if trb:EVO_Status <> 'R' then break.

            !check the supplier detail here
            Access:trdparty.clearkey(trd:Company_Name_Key)
            trd:Company_Name = trb:Company_Name
            if access:trdparty.fetch(trd:Company_Name_Key)
                ErrorMessage = 'Company '&clip(trb:Company_Name)&' not found for trdbatch '&clip(trb:RecordNumber)
                do HandleError
            ELSE
                if trd:EVO_AccNumber = '' or trd:EVO_VendorNumber = '' !or trd:EVO_Profit_Centre = '' then
                    ErrorMessage = 'Company '&clip(trb:Company_Name)&' is not set up for EVO use'
                    do HandleError
                ELSE
                    !at last
                    GRQ:GRN_RecordNo = trb:RecordNumber
                    Add(GRNQueue)
                END
            END
        END !loop through trdbatch by evo_status
    END !if sentrecord = 0

    Loop QueueCount = 1 to records(GRNQueue)

        ProcessCount += 1

        Get(GRNQueue,QueueCount)

        Access:TRDBATCH.clearkey(trb:RecordNumberKey)
        trb:RecordNumber = GRQ:GRN_RecordNo
        if access:TRDBATCH.fetch(trb:RecordNumberKey)
            !error
        ELSE
            !update this record to say sent
            if trb:Status = 'OUT' then
                trb:evo_Status = 'X'
            ELSE
                trb:EVO_Status = 'S'
            END
            Access:Trdbatch.update()
        END

        !header details
        Access:EVO_Head.primerecord()
        EVOH:HEADER_TXT       = 'SS3'&format(trb:PurchaseOrderNumber,@n07)&'/01'
        EVOH:COMP_CODE        = EVO_Company_code        !new default
        EVOH:DOC_DATE         = format(trb:ThirdPartyInvoiceDate,@d12)
        EVOH:PSTNG_DATE       = format(trb:ThirdPartyInvoiceDate,@d12)
        EVOH:REF_DOC_NO       = trb:ThirdPartyInvoiceNo  
        EVOH:XREF1_HD         = EVO_XREF1_HD
        EVOH:XREF2_HD         = trb:ThirdPartyInvoiceNo
        !TB13163 - J - 13/02/4 - if this is a zero value sale then post with Sent flag R = Resevered not N
        if trb:ThirdPartyInvoiceCharge = 0 then
            EVOH:Sent_to_EVO_Flag = 'R'
        ELSE
            EVOH:Sent_to_EVO_Flag = 'N'
        END
        Access:EVO_HEAD.update()

        !For the one item on the list a GL record
        !need the trdpart details
        Access:trdparty.clearkey(trd:Company_Name_Key)
        trd:Company_Name = trb:Company_Name
        if access:trdparty.fetch(trd:Company_Name_Key)
            !Error checked above
        END

        !ready to create record
        EVOG:HEADER                = EVOH:RECORD_NO
        EVOG:GL_ITEMNO_ACC         = 1
        EVOG:GL_ACCOUNT            = trd:EVO_AccNumber
        EVOG:GL_ITEM_TEXT          = EVOH:HEADER_TXT !Orders.Order_Number with prefix and suffix
        EVOG:GL_TAX_CODE           = EVO_Tax_Code
        EVOG:GL_PROFITCENTRE       = trd:EVO_Profit_Centre      !was EVOG:GL_COS_CENTER         = EVO_Cost_Centre_3rdParty
        !EVOG:GL_DOCUMENT_AMOUNT    = format(trb:ThirdPartyInvoiceCharge + trb:ThirdPartyVAT ,@n25.4)
        EVOG:GL_DOCUMENT_AMOUNT    = format(trb:ThirdPartyInvoiceCharge ,@n_25.4)  ! #12777 Do not include VAT (DBH: 01/02/2013)
        EVOG:GL_DOCUMENT_CURRENC   = 'ZAR'
        EVOG:GL_LOCAL_AMOUNT       = EVOG:GL_DOCUMENT_AMOUNT       
        EVOG:GL_LOCAL_CURRENCY     = 'ZAR'
        EVOG:GL_EXCHANGE_RATE      = '0.0'
        Access:EVO_GL.Insert()

        !A summary AP
        EVOA:HEADER_NUMBER         = EVOH:RECORD_NO
        EVOA:AP_ITEMNO_ACC         = EVO_Vendor_Line_No
        EVOA:AP_VENDOR_NO          = trd:EVO_VendorNumber
        EVOA:AP_ITEM_TEXT          = EVOH:HEADER_TXT
        EVOA:AP_DOCUMENT_AMOUNT    = format((trb:ThirdPartyInvoiceCharge + trb:ThirdPartyVAT) * (-1) ,@n-_25.4)        !CLIip(EVOG:GL_DOCUMENT_AMOUNT)
        EVOA:AP_DOCUMENT_CURRENC   = 'ZAR'
        EVOA:AP_LOCAL_AMOUNT       = EVOA:AP_DOCUMENT_AMOUNT
        EVOA:AP_EXCHANGE_RATE      = '0.0000'
        EVOA:AP_LOCAL_CURRENCY     = 'ZAR'
        Access:EVO_AP.insert()

        !A summary TAX
        EVOT:HEADER_NUMBER          = EVOH:RECORD_NO
        EVOT:TAX_ITEMNO_ACC         = EVO_Tax_Line_No
        !these are the net amounts
        EVOT:TAX_BASEAMOUNT         = format(trb:ThirdPartyInvoiceCharge,@n-_25.4)                  !format(trb:ThirdPartyVAT,@n-25.4)
        EVOT:TAX_LOCALBASEAMOUNT    = EVOT:TAX_BASEAMOUNT
        !tax
        EVOT:TAX_TAX_CODE           = EVO_Tax_Code
        !look up the VAT
        Access:vatcode.clearkey(vat:Vat_code_Key)
        vat:VAT_Code = EVOT:TAX_TAX_CODE
        if access:Vatcode.fetch(vat:Vat_code_Key)
            !This was checked at the start
        END

        !these are the amounts of VAT to pay
        EVOT:TAX_DOCUMENT_AMOUNT    = format(trb:ThirdPartyInvoiceCharge * vat:VAT_Rate/100 , @n-_25.4)
        EVOT:TAX_DOCUMENT_CURRENC   = 'ZAR'
        EVOT:TAX_LOCAL_AMOUNT       = EVOT:TAX_DOCUMENT_AMOUNT
        EVOT:TAX_LOCAL_CURRENCY     = 'ZAR'
        Access:EVO_Tax.insert()

    End !loop through GRNQueue

    EXIT
FinishOff       Routine

    if clip(LocalFilename) <> '' or verbose = 'Y' then

        !say we have finished
        ErrorMessage = 'Procedure has completed at '&format(clock(),@t4)&'<13,10>'&clip(ProcessCount)&' Records added'
        Do HandleError

    END

    EXIT
HandleError     Routine

    if Verbose = 'Y'
        !Message(clip(ErrorMessage))
        miss# = Missive(CLIP(ErrorMessage),'ServiceBase 3g','midea.jpg','/&OK')
    ELSE

        !have there been previous errors?
        if clip(LocalFilename) = '' then
          
            LocalFilename = clip(path())&'\EVO_Process_'&format(Today(),@d12)&'_'&format(clock(),@t5)&'.log'
            Remove(LocalFilename)

            Lineprint('Processing for '&clip(ProcCall)&' started '&format(today(),@d06)&' at about '&format(clock(),@T04),LocalFilename)
            
        END

        Lineprint(ErrorMessage,LocalFilename)

    END !    if Verbose = 'Y'

   EXIT
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
