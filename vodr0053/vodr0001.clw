

   MEMBER('vodr0053.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


StatusAnalysisReport PROCEDURE                        !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------
                     MAP   ! UpdateSummaryQueue
!AppendString         PROCEDURE( STRING, STRING, STRING ),STRING

!DateReturnedToRRC       PROCEDURE(), STRING
DateToString        PROCEDURE( DATE ), STRING
FillStatusQueue             PROCEDURE()
GetDeliveryDateAtARC        PROCEDURE( LONG ), BYTE
GetHeadAccount PROCEDURE( STRING )
GetReturnDateToRRC        PROCEDURE( LONG, DATE ), DATE
GetSubAccount PROCEDURE( STRING )
HoursBetween                PROCEDURE  (TIME, TIME),LONG

LoadAUDSTATS   PROCEDURE( LONG, STRING ), LONG ! BOOL
LoadEngineer         PROCEDURE( STRING ), LONG ! BOOL
!LoadEXCHANGE   PROCEDURE( LONG ), LONG ! BOOL
!LoadLOCATLOG   PROCEDURE( LONG, *LONG ), LONG ! BOOL
!LoadLOCATLOG_NewLocationKey   PROCEDURE( LONG, STRING, LONG ), LONG ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
!LoadSUBTRACCByTradeAcc_Alias   PROCEDURE( STRING, *LONG ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB          PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
SaveRRCSheet            PROCEDURE()
UpdateEngineerQueue  PROCEDURE( STRING )
UpdateSummaryQueue   PROCEDURE( STRING, LONG, LONG, TIME, TIME )
WorkingDaysBetween          PROCEDURE  (DATE, DATE, BYTE, BYTE), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME, LONG, LONG),LONG
WriteColumn PROCEDURE( STRING, LONG=False )
!WriteDebug      PROCEDURE( STRING )
    END !MAP
!--------------------------------------------------------------
Automatic            BYTE
tmp:VersionNumber    STRING(30)
Local                CLASS
TimeDifference       Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime, String func:AccountNumber),Long
WriteRowPerType      Procedure(String func:Type)
                     END
save_job_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
save_aus_id          USHORT
nbx                  SHORT
Param_Group          GROUP,PRE()
LOC:ReportType       STRING(20)
LOC:EndDate          DATE
LOC:StartDate        DATE
LOC:GreaterThanDays  LONG
LOC:StartJob         LONG
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:TimeFormat     STRING('hh:mm')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('W')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Local_Group          GROUP,PRE(LOC)
Sectioned_Data       BYTE
ApplicationName      STRING('ServiceBase')
actual_date_change   DATE
Actual_Time_Change   TIME
OldStatus            STRING(30)
ARCDateBooked        DATE
RRCDateBooked        DATE
ARCLocation          STRING(30)
RRCLocation          STRING(30)
RRCDateReturned      DATE
StatusReceivedAtRRC  STRING(30)
DesktopPath          STRING(255)
Filename             STRING(255)
Path                 STRING(255)
DespatchToCustomer   STRING(30)
ProgramName          STRING(100)
RepairCentreType     STRING(3)
SectionName          STRING(32)
SheetName            STRING(255)
Text                 STRING(255)
Username             STRING(100)
Version              STRING('3.1.0001')
WorkingHours         DECIMAL(10,4)
PUPDateRecived       STRING(10)
PUPflag              BYTE
                     END
Progress_Group       GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
EngineerQueue        QUEUE,PRE(engq)
UserCode             STRING(3)
Forename             STRING(30)
Surname              STRING(30)
JobsBooked           LONG
SubJobsBooked        LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
IncludeSaturday      LONG
IncludeSunday        LONG
StartWorkHours       TIME
EndWorkHours         TIME
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
Count                LONG
SubCount             LONG
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
JobNumber_Queue      QUEUE,PRE(jobnumQ)
JobNumber            LONG
HeadAccountNumber    STRING(15)
                     END
Misc_Group           GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
Result               BYTE
                     END
Status_Queue         QUEUE,PRE(statusQ)
Status               STRING(30),NAME('statusQStatus')
JobType              STRING(3),NAME('StatusQJobType')
TotalJobsCount       LONG
LessThanDay          LONG
Over60Days           LONG
Days                 LONG,DIM(15)
Sub_Group            GROUP,PRE(sub)
SubTotalJobsCount    LONG
SubLessThanDay       LONG
SubOver60Days        LONG
SubDays              LONG,DIM(15)
                     END
                     END
tmp:StatusReport     STRING(255),STATIC
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(652,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Status Analysis Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,56,552,306),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(312,268,52,12),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON('sho&W tags'),AT(312,293,52,12),USE(?DASSHOWTAG),DISABLE,HIDE
                           STRING('Skip Days Less Than'),AT(244,152),USE(?StrDaysGreaterThan),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n-14),AT(344,152,64,10),USE(LOC:GreaterThanDays),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),TIP('Skip days less than or equal to<13,10>   Number from report'),REQ
                           STRING('Start Job Booking Date'),AT(244,112),USE(?StrDaysGreaterThan:2),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(344,112,64,10),USE(LOC:StartDate),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),REQ
                           BUTTON,AT(412,108),USE(?PopCalendarStart),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('(setting this will ignore Skip Days value)'),AT(448,130,112,22),USE(?Prompt1),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('End Job Booking Date'),AT(244,132),USE(?String3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(344,132,64,10),USE(LOC:EndDate),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(412,128),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('All Repair Centres'),AT(192,168),USE(DoAll),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(556,112),USE(excel:Visible),HIDE,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                         END
                       END
                       LIST,AT(192,180,296,176),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(496,220),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(496,254),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(496,290),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,374),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------
!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------
!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------
!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
CommandLine STRING(255)
tmpPos      LONG
!--- Day Columns ---------------------------------------------------------------------------
! MOORED: As per our previous correspondence and when we did the spec the days are incorrect.
!         Should be 0 to 5 then 10, 14, 30, 60 and more than 60 days.

Day01 EQUATE(1)
Day02 EQUATE(2)
Day03 EQUATE(3)
Day04 EQUATE(4)
Day05 EQUATE(5)
Day10 EQUATE(6)
Day14 EQUATE(7)
Day30 EQUATE(8)
Day60 EQUATE(9)
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
TempFilePath         CSTRING(255)

StatusReportExport    File,Driver('TOPSPEED'),Pre(starep),Name(tmp:StatusReport),Create,Bindable,Thread
JobNumberKey        Key(starep:AccountNumber,starep:JobNumber),NOCASE
HubRepairKey        Key(starep:HubRepair,starep:JobNumber),NOCASE
Record                  Record
JobNumber               Long
AccountNumber           String(30)
HubRepair               Byte(0)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------
OKButtonPressed             ROUTINE
    DATA
    CODE
        LOC:Sectioned_Data = FALSE
        IF LOC:EndDate = 0
          LOC:EndDate             = TODAY() - LOC:GreaterThanDays ! 15 Oct 2002 John For use by FillJobNumberQueue
        ELSE
          LOC:Sectioned_Data = TRUE
        END

        IF LOC:GreaterThanDays < 0
            LOC:GreaterThanDays = 0
        END !IF

        !TB13253 get a start job for the RRC loop
        Access:JOBS.ClearKey(job:Date_Booked_Key)
        job:date_booked = loc:StartDate
        Set(job:Date_Booked_Key,job:Date_Booked_Key)
        If Access:JOBS.NEXT() then exit.    !they have chosen a start date beyond which there are not jobs ??
        LOC:StartJob = job:Ref_Number

        !-------------------------------
        IF Automatic = TRUE
            If GetTempPathA(255,TempFilePath)
                If Sub(TempFilePath,-1,1) = '\'
                    tmp:StatusReport = Clip(TempFilePath) & 'STATUSREPORT' & Clock() & '.TMP'
                Else !If Sub(TempFilePath,-1,1) = '\'
                    tmp:StatusReport = Clip(TempFilePath) & '\STATUSREPORT' & Clock() & '.TMP'
                End !If Sub(TempFilePath,-1,1) = '\'
            End

            !message('Tempfile = '&clip(tmp:StatusReport))
            Remove(StatusReportExport)

            !Build List of Job Numbers and Accounts
            Create(StatusReportExport)
            Open(StatusReportExport)
            If Error()
                Stop(Error())
            End !If Error()

            Prog.ProgressSetup(Records(JOBS))
            Prog.ProgressText('Building Report Data...')

            Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:date_booked = loc:StartDate
            Set(job:Date_Booked_Key,job:Date_Booked_Key)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:date_booked > loc:EndDate      |
                    Then Break.  ! End If
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop

                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    Clear(starep:Record)
                    starep:JobNumber        = job:Ref_Number
                    starep:AccountNumber    = wob:HeadAccountNumber
                    If SentToHub(job:Ref_Number)
                        starep:HubRepair    = 1
                    Else !If SentToHub(job:Ref_Number)
                        starep:HubRepair    = 0
                    End !If SentToHub(job:Ref_Number)
                    Add(StatusReportExport)
                Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            End !Loop

            Prog.ProgressFinish()
        !ELSE
            !message('Not automatic')
        End !IF Automatic = TRUE

        !-----------------------------------------------------------------
        !message('OKButtonPressed(Second tranch)')

        CancelPressed = False
        !debug:Active  = True
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        DO XL_Setup
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
        !message('have excel:OperatingSystem of '&excel:OperatingSystem)
        !-----------------------------------------------------------------

        IF LOC:GreaterThanDays < 0
            LOC:GreaterThanDays = 0
        END !IF

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !-----------------------------------------------

        !-----------------------------------------------------------------
        ! 15 Oct 2002 John
        ! From Joe change report name from ARC for all trade accounts to match their role (arc/rrc)
        !
        !LOC:ProgramName        = 'ARC ' & CLIP(LOC:GreaterThanDays) & ' Day Turnaround Time Progress Report'
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! DespatchToCustomer     = DESPATCHED
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        !LOC:StartDate          = TODAY() - LOC:GreaterThanDays
        LOC:Sectioned_Data = FALSE
        IF LOC:EndDate = 0
          LOC:EndDate             = TODAY() - LOC:GreaterThanDays ! 15 Oct 2002 John For use by FillJobNumberQueue
        ELSE
          LOC:Sectioned_Data = TRUE
        END
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        LOC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        ! 15 Oct 2002 John
        ! Exclude if location indicates the unit is being passed back to customer
        !
        LOC:DespatchToCustomer = GETINI('RRC', 'DespatchToCustomer',      'DESPATCHED', '.\SB2KDEF.INI') ! [RRC]DespatchToCustomer=DESPATCHED
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        DO XL_AddWorkbook

        DO InitColumns

        !------------------------------
        ! 20 Nov 2002 John
        ! MOORED: As per Joe spec that we signed off this should only display if there is phones in the count.
        ! Commented out
        !FillStatusQueue() 
        !------------------------------

        DO CreateTitleSheet

        DO ExportSetup
        nbx = 0
        !-----------------------------------------------------------------
        If Automatic = True
            !Get account criteria for automated ini  (DBH: 06-05-2004)
            Free(glo:Queue2)
            Set(tra_ali:Account_Number_Key)
            Loop
                If Access:TRADEACC_ALIAS.NEXT()
                   Break
                End !If
                If tra_ali:BranchIdentification = ''
                    Cycle
                End !If tra:BranchIdentification = ''

                If GETINI('STATUSANALYSIS',tra_ali:Account_Number,,CLIP(Path()) & '\REPAUTO.INI') = 1
                    glo:Pointer2 = tra_ali:RecordNumber
                    Add(glo:Queue2)
                End !If GETINI('TAT','tra:Account_Number',,CLIP(Path()) & '\REPAUTO.INI') = 1
            End !Loop
        End !If Automatic = True

        !message('Start trade account loop')
        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)

        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            !WriteDebug('OKButtonPressed(LOOP="' & CLIP(tra_ali:Account_Number) & '")')

            IF CancelPressed
                BREAK
            END !IF

            CASE tra_ali:Account_Number
            OF LocalHeadAccount
                LOC:RepairCentreType = 'ARC'
                !MESSAGE('ARC')
            OF 'XXXRRC' ! Is this a Vodacom exclusive ?
                !WriteDebug('OKButtonPressed(XXXRRC)')
                CYCLE

            ELSE
                IF tra_ali:RemoteRepairCentre = 0
                    !WriteDebug('OKButtonPressed(NOT RRC)')
                    CYCLE
                END !IF

                LOC:RepairCentreType = 'RRC'

            END !CASE

            IF DoAll <> 'Y' THEN
                glo:Queue2.Pointer2 = tra_ali:RecordNumber
                GET(glo:Queue2,glo:Queue2.Pointer2)
                IF ERROR() THEN CYCLE.
            END !IF
            IF Automatic = TRUE
              IF tra_ali:EmailAddress = ''
                CYCLE
              END
            END
            nbx = 1
            !debug:Active  = False
            DO ExportBody
            !debug:Active  = True
        END !LOOP

        If Automatic = True
            Close(StatusReportExport)
            Remove(StatusReportExport)
        End !If Automatic = True

        !WriteDebug('OKButtonPressed(LOOP EXIT)')

        DO WriteHeadSummary

        DO ExportFinalize
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise

        SETCURSOR()
        !-----------------------------------------------------------------
        !WriteDebug('OKButtonPressed(EXIT)')
        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF LOC:FileName = ''
            Case Missive('No filename chosen. '&|
              '<13,10>Enter a filename and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            CancelPressed = True

            EXIT
        END !IF LOC:FileName = ''

        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 5)) <> '.xlsx'
              LOC:FileName = CLIP(LOC:FileName) & '.xlsx'
           END !IF
        END !IF
        !-----------------------------------------------------------------
    EXIT

ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('ExportBody()')

        DO CreateDataSheet
        SaveRRCSheet

        DO WriteSubSummary
        SaveRRCSheet
        !-----------------------------------------------------------------
    EXIT
        
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('CreateTitleSheet()')
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:HeadLastCol = 'R'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("C:C").ColumnWidth'}                         = 12
        Excel{'ActiveSheet.Columns("D:D").ColumnWidth'}                         = 12
        Excel{'ActiveSheet.Columns("E:' & sheet:HeadLastCol & '").ColumnWidth'} = 10
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}

        !WriteDebug('CreateTitleSheet(' & Excel{'ActiveSheet.Name'} & ')')
        !-----------------------------------------------------------------
    EXIT

CreateSubTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('CreateSubTitleSheet()')
        !-----------------------------------------------------------------
        DO XL_AddSheet
        !-----------------------------------------------------------------
        LOC:Text          = CLIP(tra_ali:Account_Number) & '_Summary'
        sheet:HeadLastCol = 'R'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("C:C").ColumnWidth'}                         = 12
        Excel{'ActiveSheet.Columns("D:D").ColumnWidth'}                         = 12
        Excel{'ActiveSheet.Columns("E:' & sheet:HeadLastCol & '").ColumnWidth'} = 10
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}

        !WriteDebug('CreateSubTitleSheet(' & Excel{'ActiveSheet.Name'} & ')')
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

!        FREE(EngineerQueue)
!        CLEAR(EngineerQueue)
!        DO ClearSubStatusQueue
        !FREE(Status_Queue)
        !CLEAR(Status_Queue)
!        FREE(SubAccount_Queue)
!        CLEAR(SubAccount_Queue)
        !-----------------------------------------------------------------
        DO XL_AddSheet
        DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        LOC:SectionName   = 'Checking ' & CLIP(tra_ali:Account_Number) ! 'Detailed'
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        !------------------------------------------

        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = RECORDS(Jobs)
        DO ProgressBar_LoopPre

        !Vodacom have to run the report manually, even though they are emailed
        !it everyday from the automatic procedure. Why do I bother!?

        If Automatic = True
        !-AUTO----------------------------------------
            If LOC:RepairCentreType = 'RRC'
                Clear(starep:Record)
                starep:AccountNumber    = tra_Ali:Account_Number
                Set(starep:JobNumberKey,starep:JobNumberKey)
                Loop
                    Next(StatusReportExport)
                    If Error()
                        Break
                    End !If Error()
                    IF starep:AccountNumber    <> tra_Ali:Account_Number
                      BREAK
                    END
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = starep:JobNumber
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Found

                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                        jobe:RefNumber  = job:Ref_Number
                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Found

                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                        Access:WEBJOB.Clearkey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Found

                            DO ProgressBar_Loop

                            IF CancelPressed
                                BREAK
                            END !IF

                            If job:Date_Booked < loc:StartDate
                                Cycle
                            End !If job:Date_Booked < loc:StartDate

                            !-------------------------------------------------------------
                            IF LOC:Sectioned_Data = TRUE
                              IF job:Date_Booked > LOC:EndDate
                                Cycle
                              END
                            END

                            DO WriteColumns
                            !-------------------------------------------------------------

                        Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Error
                        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                End !Loop

            End !If LOC:RepairCentreType ='RRC'
            !If ARC account, then include all jobs
            If loc:RepairCentreType = 'ARC'
                Clear(starep:Record)
                starep:HubRepair = 1
                Set(starep:HubRepairKey,starep:HubRepairKey)
                Loop
                    Next(StatusReportExport)
                    If Error()
                        Break
                    End !If Error()
                    IF starep:HubRepair <> 1
                      BREAK
                    END
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = starep:JobNumber
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                        jobe:RefNumber  = job:Ref_Number
                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Found
                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                        If job:Date_Booked < loc:StartDate
                            Cycle
                        End !If job:Date_Booked < loc:StartDate

                        !-------------------------------------------------------------
                        IF LOC:Sectioned_Data = TRUE
                          IF job:Date_Booked > LOC:EndDate
                            Cycle
                          END
                        END
                        DO ProgressBar_Loop

                        IF CancelPressed
                            BREAK
                        END !IF
                        IF LOC:Sectioned_Data = TRUE
                          IF job:Date_Booked > LOC:EndDate
                            Break
                          END
                        END

                        DO WriteColumns

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                End !Loop

            End !If LOC:RepairCentreType = 'RRC'

        Else !If Automatic = True

        !-MANUAL------------------------------------------
            !For RRC Account, only pick up RRC jobs.
            If loc:RepairCentreType = 'RRC'     !this was going through every job for this trade account - for ever
                Save_wob_ID = Access:WEBJOB.SaveFile()
                !Access:WEBJOB.ClearKey(wob:HeadJobNumberKey)
                !wob:HeadAccountNumber = tra_ali:Account_Number
                !Set(wob:HeadJobNumberKey,wob:HeadJobNumberKey)

                !swap to a key on head account number and a ref_number so we can start in the work
                Access:WEBJOB.ClearKey(wob:HeadRefNumberKey)
                wob:HeadAccountNumber = tra_ali:Account_Number
                wob:RefNumber = LOC:StartJob
                Set(wob:HeadRefNumberKey,wob:HeadRefNumberKey)

                Loop

                    If Access:WEBJOB.NEXT() then break.
                    If wob:HeadAccountNumber <> tra_ali:Account_Number then break.

                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = wob:RefNumber
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                        jobe:RefNumber  = job:Ref_Number
                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Found

                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        If job:Date_Booked < loc:StartDate
                            Cycle
                        End !If job:Date_Booked < loc:StartDate
                        IF LOC:Sectioned_Data = TRUE
                          IF job:Date_Booked > LOC:EndDate
                            Cycle
                          END
                        END
                        DO ProgressBar_Loop

                        IF CancelPressed
                            BREAK
                        END !IF


                        DO WriteColumns

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                End !Loop
                Access:WEBJOB.RestoreFile(Save_wob_ID)
            Else !If loc:RepairCentreType = 'RRC'
                !this is ARC we want all jobs booked at ARC or sent to ARC
                Save_job_ID = Access:JOBS.SaveFile()
                Access:JOBS.ClearKey(job:Date_Booked_Key)
                job:date_booked = loc:StartDate
                Set(job:Date_Booked_Key,job:Date_Booked_Key)
                Loop
                    If Access:JOBS.NEXT()
                       Break
                    End !If
                    If job:date_booked > loc:EndDate       |
                        Then Break.  ! End If

                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found

                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

!                    !dont want this if it was not sent to arc - or was booked at ARC
!                    If jobe:HubRepair then cycle.
!                    if jobe:HubRepairDate <> '' then cycle.

                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                        !tb13270 - J - 11/04/14
                        !Send through anything booked at ARC, or sent to ARC
                        if wob:HeadAccountNumber <> LocalHeadAccount   !not booked at ARC
                            If jobe:HubRepair = 0 then cycle.          !not repaired at ARC
                            if jobe:HubRepairDate = '' then cycle.     !although marked to be repaired at hub it never has been
                        END

                        !Found
                        DO ProgressBar_Loop

                        IF CancelPressed
                            BREAK
                        END !IF

                        DO WriteColumns

                    Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                End !Loop
                Access:JOBS.RestoreFile(Save_job_ID)
            End !If loc:RepairCentreType = 'RRC'

        End !If Automatic = True

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        LOC:Text = CLIP(tra_ali:Account_Number)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! Remove "Section Name:", "Detailed"
        ! Keep sheet name as "Detailed"
        !
!        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
!            Excel{'ActiveCell.Formula'}         = 'Section Name:'
!            DO XL_ColRight
!                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
!                DO XL_SetBold
!                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------


        Excel{'Range("A' & sheet:DataSectionRow & '").Select'} ! was E
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------


        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number) 
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16

        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}              = 'VODR0053 - ' & CLIP(LOC:Version)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'}            = 8

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}              = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        CurrentRow += 1
           Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Booking Start Date:'
               DO XL_ColRight
                   DO XL_FormatDate
                   DO XL_HorizontalAlignmentLeft
                   Excel{'ActiveCell.Formula'}       = DateToString(LOC:StartDate)


        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Booking End Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(LOC:EndDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Skip Days Less Than:'
                DO XL_ColRight
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = LOC:GreaterThanDays

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
        !-----------------------------------------------
        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2
        sheet:HeadSummaryRow = sheet:DataSectionRow
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Franchise Branch Code'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'Status Type'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'Old Status'
            head:ColumnWidth  = 30.00 ! 6 Nov 2002 John
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Current Status'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Status Date change'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Status Time change'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:TimeFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Days In Current Status'
            head:ColumnWidth  = 10.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)


        !TB13236 - new column - J - 18/02/14
        head:ColumnName       = 'Date Received At PUP'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)
        !END TB13236 - new column - J - 18/02/14


        head:ColumnName       = 'Date Received At RRC'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Received At ARC'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Total Days Since Receipt'
            head:ColumnWidth  = 10.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Complete'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        !TB13236 - new column - J - 18/02/14
        head:ColumnName       = 'Repair Location'
            head:ColumnWidth  = 10.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !END TB13236 - new column - J - 18/02/14


        head:ColumnName       = 'Make' ! Changed from 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = '2nd Exchange IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange Status'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Loan Status'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! Insert --- New Columns (DBH: 20/03/2009) #10591
        head:ColumnName       = 'Warranty Job'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Third Party Site'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Days Since Quote Accepted'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        ! end --- (DBH: 20/03/2009) #10591

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !PutIniCount += 1
        !PUTINI(CLIP(LOC:ProgramName),PutIniCount, 'SetColumns','C:\' & LOC:ApplicationName & '_Debug.INI')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !WriteDebug('WriteColumns(JOB="' & job:Ref_Number & '")')
        !-----------------------------------------------


        !-----------------------------------------------
        IF  LOC:RepairCentreType = 'RRC'
            !-------------------------------------------
            IF GetDeliveryDateAtARC(job:Ref_Number)
                ! RRC -> ARC  <====================(1)
                !
                EXIT
            END !IF
            !-------------------------------------------
            ! RRC -> RRC
            !
            LOC:RRCDateBooked   = job:date_booked
            LOC:ARCDateBooked   = ''
            LOC:RRCDateReturned = ''
            !-------------------------------------------
!            IF NOT LoadWEBJOB(job:Ref_Number)
!                ! ERROR
!                EXIT
!            END !IF

            GetHeadAccount(wob:HeadAccountNumber)
!            !-----------------------------------------
!            IF wob:HeadAccountNumber <> tra_ali:Account_Number
!              EXIT
!            END
            !MESSAGE('writing')
            DO WriteColumns_ALL !iteColumns_RRC
            !-------------------------------------------
        ELSE
            !-------------------------------------------
            ! LOC:RepairCentreType = 'ARC'
            !-------------------------------------------
            IF NOT LoadWEBJOB(job:Ref_Number)
                ! ERROR

                EXIT
            END !IF

            GetHeadAccount(wob:HeadAccountNumber)
            !-------------------------------------------

            IF HaQ:AccountNumber = LocalHeadAccount
                !---------------------------------------
                ! ARC -> ARC
                !---------------------------------------
                ! 22 Oct 2002 John
                ! If the job was booked at the ARC, the RRC date received should be blank.
                ! The date displayed here is the booking date for the job not the date it was received at the RRC.
                ! In fact none of these jobs were booked at the RRC therefore this column [RRC Date Received-john] should be blank.
                !
                LOC:RRCDateBooked   = ''
                LOC:ARCDateBooked   = job:Date_Booked
                LOC:RRCDateReturned = ''

                DO WriteColumns_ALL ! teColumns_ARC
                !---------------------------------------
            ELSE
              IF GetDeliveryDateAtARC(job:Ref_Number) = TRUE
                !---------------------------------------
                ! RRC -> ARC  <====================(1)
                !---------------------------------------
                LOC:RRCDateBooked   = job:Date_Booked
                !OC:ARCDateBooked   = set by GetDeliveryDateAtARC()
                LOC:RRCDateReturned = GetReturnDateToRRC(job:Ref_Number, LOC:ARCDateBooked)

                IF LOC:RRCDateReturned = 0
                  LOC:RRCDateReturned = ''
                END
                !MESSAGE('HIT CRITERIA - JOB NO'&job:ref_Number)
                DO WriteColumns_ALL ! WriteColumns_ARC
                !---------------------------------------
              ELSE
                !MESSAGE('FAILED?!')
              END
            !ELSE
                !---------------------------------------
                ! RRC -> RRC Unreachable code
                !---------------------------------------
            !    EXIT
                !---------------------------------------
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
    EXIT
WriteColumns_ALL                                                       ROUTINE
    DATA
WorkingDays       LONG
StatusWorkingDays LONG
StatusWorkingHours LONG ! WorkingHoursBetween()

JobWorkingDays    LONG
local:JobType       String('JOB')
    CODE
        IF CLIP(job:Location) = CLIP(LOC:DespatchToCustomer)
            EXIT
        END !IF
        IF job:Date_Despatched > 0
            EXIT
        END !IF
!        IF (job:Exchange_Unit_Number > 0) ! AND (job:Exchange_Despatched > 0)
!            EXIT
!        END !IF

        GetHeadAccount(wob:HeadAccountNumber)
        IF LOC:ARCDateBooked > 0
            WorkingDays = WorkingDaysBetween(LOC:ARCDateBooked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        ELSE
            WorkingDays = WorkingDaysBetween(job:date_booked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        END

! -----------------------------------------
! DBH 14/10/2008 #10512
! Changing: Not counting just "less than"
!        IF NOT WorkingDays > LOC:GreaterThanDays
! to: DBH 14/10/2008 # 10512
        If loc:GreaterThanDays > WorkingDays
! End: DBH 14/10/2008 #10512
! -----------------------------------------

!            Stop(job:Ref_Number & ': ' & workingdays & ' - ' & loc:GreaterThanDays)
            !WriteDebug('WriteColumns_ALL(FAIL)(IF WorkingDays="' & WorkingDays & '" > LOC:GreaterThanDays="' & LOC:GreaterThanDays & '")')
              !MESSAGE('4-'&job:ref_number)
            EXIT
        END !IF

        !-----------------------------------------------
        IF LoadAUDSTATS( job:Ref_Number, job:Current_Status)
            StatusWorkingDays = WorkingDaysBetween(loc:actual_date_change, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        ELSE
            StatusWorkingDays = 0
        END !IF LoadAUDSTATS( job:Ref_Number, job:Current_Status)

        GetSubAccount(job:Account_Number)
        saQ:Count    += 1
        saQ:SubCount += 1
        PUT(SubAccount_Queue)

        GetHeadAccount(wob:HeadAccountNumber)
        !-----------------------------------------------
        !WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday) ! LONG
        IF LOC:ARCDateBooked > 0
            StatusWorkingHours = WorkingHoursBetween(LOC:ARCDateBooked, TODAY(), haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday)
        ELSE
            StatusWorkingHours = WorkingHoursBetween(  job:date_booked, TODAY(), haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday)
        END

        !UpdateSummaryQueue( job:Current_Status, StatusWorkingDays, StatusWorkingHours, haQ:StartWorkHours, haQ:EndWorkHours )
        !-----------------------------------------------
        UpdateEngineerQueue( job:Engineer )
        !-----------------------------------------------
        !-----------------------------------------------
        !What sort of job is this? - TrkBs: 4857 (DBH: 28-02-2005)
        If job:Exchange_Unit_Number <> 0
            Case Sub(job:Exchange_Status,1,3)
                Of 125      |
                Orof 815    |
                Orof 901
! -----------------------------------------
! DBH 13/10/2008 #10512
! Changing: Carry on if the job is still not completed
!                Exit
! to: DBH 13/10/2008 # 10512
                Else
                    local.WriteRowPerType('EXC')
! End: DBH 13/10/2008 #10512
! -----------------------------------------

            End ! Case Sub(job:Exchange_Status,1,3)
            

        End ! If job:Exchange_Unit_Number <> 0

        If jobe:SecondExchangeNumber <> 0
            Case Sub(jobe:SecondExchangeStatus,1,3)
                Of 125      |
                Orof 815    |
                Orof 901
! -----------------------------------------
! DBH 13/10/2008 #10512
! Changing: Carry on if job is still not completed
!                Exit
! to: DBH 13/10/2008 # 10512
                Else
                    local.WriteRowPerType('2NE')
! End: DBH 13/10/2008 #10512
! -----------------------------------------

            End ! Case Sub(jobe:SecondExchangeStatus,1,3)
            
        End ! If jobe:SecondExchangeNumber <> 0

        If job:Loan_Unit_Number <> 0
            Case Sub(job:Loan_Status,1,3)
                Of 816      |
                Orof 901
! -----------------------------------------
! DBH 13/10/2008 #10512
! Changing: Carry on if the job is still not completed
!                Exit
! to: DBH 13/10/2008 # 10512
                Else
                    local.WriteRowPerType('LOA')
! End: DBH 13/10/2008 #10512
! -----------------------------------------
            End ! Case Sub(job:Loan_Status,1,3)
            
        End ! If job:Loan_Unit_Number <> 0

        Case Sub(job:Current_Status,1,3)
            Of 901      |
            Orof 905    |
            Orof 910    |
            Orof 125    |
            Orof 710    |
            Orof 799    |
            Orof 814    |
            Orof 815    |
            Orof 816
            Exit
        End ! Case Sub(job:Current_Status,1,3)
        local.WriteRowPerType('JOB')



!        WriteColumn( job:Ref_Number, True                 ) ! SB Job Number
!        WriteColumn( haQ:AccountName                      ) ! Branch Name
!        WriteColumn( 'JOB'                                ) ! Job Type
!
!        IF StatusWorkingDays >= 0
!            WriteColumn( loc:OldStatus                    ) ! Old Status
!            WriteColumn( job:Current_Status               ) ! Current Status
!            WriteColumn( DateToString(loc:actual_date_change)    ) ! Status Date change
!            WriteColumn( Format(loc:Actual_Time_Change,@t1)    ) ! Status Time Change
!            WriteColumn( StatusWorkingDays                ) ! Days In Current Status
!        ELSE
!            WriteColumn( 'ERROR'                               ) ! Old Status
!            WriteColumn( job:Current_Status               ) ! Current Status
!            WriteColumn( ''                               ) ! Status Date change
!            WriteColumn( ''                               ) ! Days In Current Status
!        END !IF StatusWorkingDays > 0
!
!        WriteColumn( DateToString(LOC:RRCDateBooked)      ) ! Date Received At RRC
!        WriteColumn( DateToString(LOC:ARCDateBooked)      ) ! Date Received At ARC
!
!        JobWorkingDays = WorkingDaysBetween(job:Date_Booked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
!        WriteColumn( JobWorkingDays                       ) ! Total Days Since Receipt (at ???)
!
!        WriteColumn( DateToString(job:Date_Completed)     ) ! Date Complete
!        WriteColumn( job:Manufacturer                     ) ! Manufacturer
!        WriteColumn( job:Model_Number                     ) ! Model
!        WriteColumn( saQ:AccountName                      ) ! Account Name
!        WriteColumn( job:ESN                              ) ! IMEI
!        WriteColumn( job:Exchange_Status                  ) ! Exchange Status
!        WriteColumn( job:Loan_Status                      ) ! Loan Status
!        !-----------------------------------------------

        !---------------------------------------------------
    EXIT
!--------------------------------------------------------------
WriteColumn_JobNumber                       ROUTINE ! Job Number
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = job:Ref_Number
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        YIELD()

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseJobNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Access:WEBJOB.CLEARKEY(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF Access:WEBJOB.Fetch(wob:RefNumberKey) = Level:Benign
            !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!            IF excel:OperatingSystem < 5 THEN
!               clip:Value = CLIP(clip:Value) & '<09>' & wob:JobNumber
!
!               EXIT
!            END !IF
            !-----------------------------------------------
            Excel{'ActiveCell.Formula'}  = wob:JobNumber
            !-----------------------------------------------
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseBranchNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! 22 Oct 2002 John
        ! There are 862 jobs in this list out of 6000 jobs currently booked onto the system.
        !   It is impossible to believe that some of the overdue jobs in the database were not originally booked in at the RRC.
        !   In that event this column should show the RRC Branch number as well as the ARC branch number.
        !
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & saQ:BranchIdentification ! tra_ali:BranchIdentification
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:BranchIdentification ! tra_ali:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_RRCDateBooked  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & DateToString(LOC:RRCDateBooked)
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = DateToString(LOC:RRCDateBooked)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ARCDateBooked  ROUTINE
    DATA
Temp STRING(30)
    CODE
        !-----------------------------------------------
        IF LOC:ARCDateBooked > 1
            Temp = DateToString(LOC:ARCDateBooked)
        END !IF
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & Temp
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DaysSinceRecAtARC  ROUTINE
    DATA
WorkingDays LONG
    CODE
        !-----------------------------------------------
        IF LOC:ARCDateBooked > 1
            !-------------------------------------------
            ! 8 Oct 2002 John
            ! ANKE: Must show working days only -  weekends to be excluded from the  calculation
            !
            WorkingDays = WorkingDaysBetween(LOC:ARCDateBooked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5 THEN
!           clip:Value = CLIP(clip:Value) & '<09>' & WorkingDays
!
!           EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = WorkingDays

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DaysSinceRecAtRRC  ROUTINE
    DATA
WorkingDays LONG
    CODE
        !-----------------------------------------------
        WorkingDays = WorkingDaysBetween(LOC:RRCDateBooked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5 THEN
!           clip:Value = CLIP(clip:Value) & '<09>' & WorkingDays
!
!           EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = WorkingDays

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateCompleted                   ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & DateToString(job:Date_Completed)
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = DateToString(job:Date_Completed)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Turnaround                      ROUTINE ! Turnaround
    DATA
Temp            LONG
IncludeSaturday LONG(False)
IncludeSunday   LONG(False)
    CODE
        !-----------------------------------------------
        IF tra:UseTimingsFrom = 'TRA'
            IncludeSaturday = tra:IncludeSaturday
            IncludeSunday   = tra:IncludeSunday
        ELSE
            IF def:Include_Saturday = 'YES'
                IncludeSaturday = True
            END !IF

            IF def:Include_Saturday = 'YES'
                IncludeSunday   = True
            END !IF
        END !IF

        Temp = WorkingDaysBetween(job:date_booked, job:Date_Completed, IncludeSaturday, IncludeSunday)
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & Temp
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp ! (job:Date_Completed - job:date_booked)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Manufacturer                ROUTINE ! Make
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Manufacturer
!                         
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Manufacturer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ModelNumber                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Model
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_TradeAccountNumber                              ROUTINE ! Account Number
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Account_Number
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Account_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_TradeAccountName            ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & saQ:HeadAccountName
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:HeadAccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ESN                         ROUTINE ! IMEI
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Job                       ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Current_Status
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Current_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Exchange                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Exchange_Status
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Exchange_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Loan                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Loan_Status
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Loan_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Engineer                ROUTINE
    DATA
Temp STRING(100)
    CODE
        !-----------------------------------------------
        !Temp = CLIP(AppendString(engq:Surname, engq:Forename, ', '))
        !Temp = CLIP(AppendString(Temp,         engq:UserCode, '-' ))
        temp = Clip(engq:Surname)&', '&clip(engq:Forename)&'-'&clip(engq:UserCode)
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & Temp
!
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!WriteColumn_UnitType                    ROUTINE ! Unit Type
!    DATA
!    CODE
!        !-----------------------------------------------
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Unit_Type
!
!            EXIT
!        END !IF
!        !-----------------------------------------------
!        Excel{'ActiveCell.Formula'}  = job:Unit_Type
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_MSN                         ROUTINE ! MSN
!    DATA
!    CODE
!        !-----------------------------------------------
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:MSN
!
!            EXIT
!        END !IF
!        !-----------------------------------------------
!        Excel{'ActiveCell.Formula'}  = job:MSN
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!--------------------------------------------------------------
WriteDataSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns

        Excel{'Range("B' & sheet:DataSectionRow & '").Select'} ! was F 15 Oct 2002 John
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("C' & sheet:DataSectionRow & '").Select'} ! was G 15 Oct 2002 John
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("D' & sheet:DataSectionRow & '").Select'} ! was H 15 Oct 2002 John
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------      
    EXIT
WriteHeadSummary                    ROUTINE
    DATA
    CODE
        !-------------------------------
        ! 10 Oct 2002 John
        ! 1) Remove Summary Sheet Completely
        ! 6 Nov 2002 John
        ! New Summary
        !EXIT
        !-------------------------------


        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        DO Summary
        DO SummarySubAccount
        DO SummaryEngineer
        !-----------------------------------------------------------------
    EXIT

WriteSubSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        DO CreateSubTitleSheet
        !-----------------------------------------------------------------
        DO SubSummary
        DO SubSummarySubAccount
        DO SubSummaryEngineer
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
Summary         ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

CurrentRow   LONG
HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(Status_Queue)

        CurrentRow = Excel{'ActiveCell.Row'}  ! first blank line
        HeaderRow  = CurrentRow + 1
        FirstRow   = HeaderRow  + 1
        LastRow    = HeaderRow  + ResultsCount
        !-----------------------------------------------------------------
        Excel{'Range("A' & CurrentRow & ':B' & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("C' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Days At Current Status'

        Excel{'Range("C' & CurrentRow & ':' & sheet:HeadLastCol & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        !-----------------------------------------------------------------
        ! 20 Nov 2002 John
        ! MOORED: As per our previous correspondence and when we did the spec the days are incorrect.
        !         Should be 0 to 5 then 10, 14, 30, 60 and more than 60 days.
        !
        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0,  0).Formula'}                = 'Status'
            Excel{'ActiveCell.Offset(0,  1).Formula'}                = 'Job Type'
            Excel{'ActiveCell.Offset(0,  2).Formula'}                = 'Total Jobs Count'
            Excel{'ActiveCell.Offset(0,  3).Formula'}                = '1 Hr'
            Excel{'ActiveCell.Offset(0,  4).Formula'}                = '4 Hrs'
            Excel{'ActiveCell.Offset(0,  5).Formula'}                = '8 Hrs'
            Excel{'ActiveCell.Offset(0,  6).Formula'}                = '1 Day'
            Excel{'ActiveCell.Offset(0,  7).Formula'}                = '2 Days'
            Excel{'ActiveCell.Offset(0,  8).Formula'}                = '3 Days'
            Excel{'ActiveCell.Offset(0,  9).Formula'}                = '4 Days'
            Excel{'ActiveCell.Offset(0, 10).Formula'}                = '5 Days'
            Excel{'ActiveCell.Offset(0, 11).Formula'}                = '7 Days'
            Excel{'ActiveCell.Offset(0, 12).Formula'}                = '9 Days'
            Excel{'ActiveCell.Offset(0, 13).Formula'}                = '14 Days'
            Excel{'ActiveCell.Offset(0, 14).Formula'}                = '30 Days'
            Excel{'ActiveCell.Offset(0, 15).Formula'}                = '60 Days'
            Excel{'ActiveCell.Offset(0, 16).Formula'}                = '90 Days'
            Excel{'ActiveCell.Offset(0, 17).Formula'}                = '>90 Days'
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}                   = 'No Status Found'

            EXIT
        END !IF

        SORT(Status_Queue, +statusQ:Status)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(Status_Queue, QueueIndex)

            !-------------------------------------------------------------
            ! 20 Nov 2002 John
            ! MOORED: As per our previous correspondence and when we did the spec the days are incorrect.
            !         Should be 0 to 5 then 10, 14, 30, 60 and more than 60 days.
            !
            Excel{'ActiveCell.Offset(0,  0).Formula'}                = statusQ:Status
            Excel{'ActiveCell.Offset(0,  1).Formula'}                = statusQ:JobType
            Excel{'ActiveCell.Offset(0,  2).Formula'}                = statusQ:TotalJobsCount
            Excel{'ActiveCell.Offset(0,  3).Formula'}                = statusQ:Days[1]
            Excel{'ActiveCell.Offset(0,  4).Formula'}                = statusQ:Days[2]
            Excel{'ActiveCell.Offset(0,  5).Formula'}                = statusQ:Days[3]
            Excel{'ActiveCell.Offset(0,  6).Formula'}                = statusQ:Days[4]
            Excel{'ActiveCell.Offset(0,  7).Formula'}                = statusQ:Days[5]
            Excel{'ActiveCell.Offset(0,  8).Formula'}                = statusQ:Days[6]
            Excel{'ActiveCell.Offset(0,  9).Formula'}                = statusQ:Days[7]
            Excel{'ActiveCell.Offset(0, 10).Formula'}                = statusQ:Days[8]
            Excel{'ActiveCell.Offset(0, 11).Formula'}                = statusQ:Days[9]
            Excel{'ActiveCell.Offset(0, 12).Formula'}                = statusQ:Days[10]
            Excel{'ActiveCell.Offset(0, 13).Formula'}                = statusQ:Days[11]
            Excel{'ActiveCell.Offset(0, 14).Formula'}                = statusQ:Days[12]
            Excel{'ActiveCell.Offset(0, 15).Formula'}                = statusQ:Days[13]
            Excel{'ActiveCell.Offset(0, 16).Formula'}                = statusQ:Days[14]
            Excel{'ActiveCell.Offset(0, 17).Formula'}                = statusQ:Days[15]
            !-------------------------------------------------------------

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Sub-Totals'
            LOOP x# = 3 TO 18 ! 23
                Excel{'ActiveCell.Offset(0, ' & (x#-1) & ').Formula'} = '=SUBTOTAL(9, ' & NumberToColumn(x#) & FirstRow & ':' & NumberToColumn(x#) & LastRow & ')'
            END !LOOP
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SummarySubAccount       ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
    CODE
        EXIT
        !Not Needed - TrkBs:  (DBH: 14-03-2005)

    !_____________________________________________________________________


        !-----------------------------------------------------------------
        ! Totals Trade Account
        !
        ResultsCount = RECORDS(SubAccount_Queue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Account'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'

        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}                       = 'No Sub Accounts Found'
        END !IF

        SORT(SubAccount_Queue, +saQ:AccountNumber)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(SubAccount_Queue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = saQ:AccountNumber
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = saQ:AccountName
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = saQ:Count
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SummaryEngineer     ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ! Totals Engineer
        !
        ResultsCount = RECORDS(EngineerQueue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Engineer'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'

        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'No Engineers Found'
        END !IF

        SORT(EngineerQueue, +engq:UserCode)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(EngineerQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = clip(engq:Surname)&', '&clip(engq:Forename)   !AppendString(engq:Surname, engq:Forename, ', ')
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = engq:UserCode
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = engq:JobsBooked
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & HeaderRow+ResultsCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SubSummary         ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

CurrentRow   LONG
HeaderRow    LONG
FirstRow     LONG
LastRow      LONG

Found        LONG(0)
    CODE
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(Status_Queue)

        CurrentRow = Excel{'ActiveCell.Row'}  ! first blank line
        HeaderRow  = CurrentRow + 1
        FirstRow   = HeaderRow  + 1
        LastRow    = HeaderRow  + ResultsCount
        !-----------------------------------------------------------------
        Excel{'Range("A' & CurrentRow & ':B' & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("C' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Days At Current Status'

        Excel{'Range("C' & CurrentRow & ':' & sheet:HeadLastCol & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        !-----------------------------------------------------------------
        ! 20 Nov 2002 John
        ! MOORED: As per our previous correspondence and when we did the spec the days are incorrect.
        !         Should be 0 to 5 then 10, 14, 30, 60 and more than 60 days.
        !
        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0,  0).Formula'}                = 'Status'
            Excel{'ActiveCell.Offset(0,  1).Formula'}                = 'Job Type'
            Excel{'ActiveCell.Offset(0,  2).Formula'}                = 'Total Jobs Count'
            Excel{'ActiveCell.Offset(0,  3).Formula'}                = '1 Hr'
            Excel{'ActiveCell.Offset(0,  4).Formula'}                = '4 Hrs'
            Excel{'ActiveCell.Offset(0,  5).Formula'}                = '8 Hrs'
            Excel{'ActiveCell.Offset(0,  6).Formula'}                = '1 Day'
            Excel{'ActiveCell.Offset(0,  7).Formula'}                = '2 Days'
            Excel{'ActiveCell.Offset(0,  8).Formula'}                = '3 Days'
            Excel{'ActiveCell.Offset(0,  9).Formula'}                = '4 Days'
            Excel{'ActiveCell.Offset(0, 10).Formula'}                = '5 Days'
            Excel{'ActiveCell.Offset(0, 11).Formula'}                = '7 Days'
            Excel{'ActiveCell.Offset(0, 12).Formula'}                = '9 Days'
            Excel{'ActiveCell.Offset(0, 13).Formula'}                = '14 Days'
            Excel{'ActiveCell.Offset(0, 14).Formula'}                = '30 Days'
            Excel{'ActiveCell.Offset(0, 15).Formula'}                = '60 Days'
            Excel{'ActiveCell.Offset(0, 16).Formula'}                = '90 Days'
            Excel{'ActiveCell.Offset(0, 17).Formula'}                = '>90 Days'
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}

        SORT(Status_Queue, +statusQ:Status)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(Status_Queue, QueueIndex)

            IF statusQ:SubTotalJobsCount < 1
                CYCLE
            END !IF
            !-------------------------------------------------------------
            Found += 1

            ! 20 Nov 2002 John
            ! MOORED: As per our previous correspondence and when we did the spec the days are incorrect.
            !         Should be 0 to 5 then 10, 14, 30, 60 and more than 60 days.
            !
            Excel{'ActiveCell.Offset(0,  0).Formula'}                = statusQ:Status
            Excel{'ActiveCell.Offset(0,  1).Formula'}                = statusQ:JobType
            Excel{'ActiveCell.Offset(0,  2).Formula'}                = statusQ:SubTotalJobsCount
            Excel{'ActiveCell.Offset(0,  3).Formula'}                = statusQ:SubDays[1]
            Excel{'ActiveCell.Offset(0,  4).Formula'}                = statusQ:SubDays[2]
            Excel{'ActiveCell.Offset(0,  5).Formula'}                = statusQ:SubDays[3]
            Excel{'ActiveCell.Offset(0,  6).Formula'}                = statusQ:SubDays[4]
            Excel{'ActiveCell.Offset(0,  7).Formula'}                = statusQ:SubDays[5]
            Excel{'ActiveCell.Offset(0,  8).Formula'}                = statusQ:SubDays[6]
            Excel{'ActiveCell.Offset(0,  9).Formula'}                = statusQ:SubDays[7]
            Excel{'ActiveCell.Offset(0, 10).Formula'}                = statusQ:SubDays[8]
            Excel{'ActiveCell.Offset(0, 11).Formula'}                = statusQ:SubDays[9]
            Excel{'ActiveCell.Offset(0, 12).Formula'}                = statusQ:SubDays[10]
            Excel{'ActiveCell.Offset(0, 13).Formula'}                = statusQ:SubDays[11]
            Excel{'ActiveCell.Offset(0, 14).Formula'}                = statusQ:SubDays[12]
            Excel{'ActiveCell.Offset(0, 15).Formula'}                = statusQ:SubDays[13]
            Excel{'ActiveCell.Offset(0, 16).Formula'}                = statusQ:SubDays[14]
            Excel{'ActiveCell.Offset(0, 17).Formula'}                = statusQ:SubDays[15]
            !-------------------------------------------------------------
            ! Reset Sub Group for next trade account
            !
            CLEAR(statusQ:Sub_Group)
                CLEAR(statusQ:SubTotalJobsCount)
                CLEAR(statusQ:SubLessThanDay)
                CLEAR(statusQ:SubDays)
                CLEAR(statusQ:SubOver60Days)
            PUT(Status_Queue)
            !-------------------------------------------------------------

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        IF Found = 0
            Excel{'ActiveCell.Formula'}                   = 'No Status Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF
        !-----------------------------------------------------------------
        LastRow = HeaderRow + Found

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Sub-Totals'
            LOOP x# = 3 TO 18 ! 23
                Excel{'ActiveCell.Offset(0, ' & (x#-1) & ').Formula'} = '=SUBTOTAL(9, ' & NumberToColumn(x#) & FirstRow & ':' & NumberToColumn(x#) & LastRow & ')'
            END !LOOP
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SubSummarySubAccount       ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG

Found        LONG(0)
    CODE
        EXIT
        !Not needed - TrkBs:  (DBH: 14-03-2005)

    !_____________________________________________________________________


        !-----------------------------------------------------------------
        ! Totals Trade Account
        !
        ResultsCount = RECORDS(SubAccount_Queue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Account'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'

        Excel{'Range("A' & FirstRow & '").Select'}

        SORT(SubAccount_Queue, +saQ:AccountNumber)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(SubAccount_Queue, QueueIndex)

            IF saQ:SubCount < 1
                CYCLE
            END !IF

            Found += 1
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = saQ:AccountNumber
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = saQ:AccountName
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = saQ:SubCount
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'
            !-------------------------------------------------------------
            CLEAR(saQ:SubCount)
            PUT(SubAccount_Queue)
            !-------------------------------------------------------------
            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        IF Found = 0
            Excel{'ActiveCell.Formula'}                       = 'No Sub Accounts Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF
        !-----------------------------------------------------------------
        LastRow = HeaderRow + Found

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SubSummaryEngineer     ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG

Found        LONG(0)
    CODE
        !-----------------------------------------------------------------
        ! Totals Engineer
        !
        ResultsCount = RECORDS(EngineerQueue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Engineer'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'

        Excel{'Range("A' & FirstRow & '").Select'}

        SORT(EngineerQueue, +engq:UserCode)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(EngineerQueue, QueueIndex)

            IF engq:SubJobsBooked < 1
                CYCLE
            END !IF

            Found += 1
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = clip(engq:Surname)&', '&clip(engq:Forename)      !AppendString(engq:Surname, engq:Forename, ', ')
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = engq:UserCode
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = engq:SubJobsBooked
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
            ! Reset for next trade account
            !
            engq:SubJobsBooked = 0
            PUT(EngineerQueue)
            !-------------------------------------------------------------
        END !LOOP

        IF Found = 0
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'No Engineers Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF
        !-----------------------------------------------------------------
        LastRow = HeaderRow + Found

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
ClearStatusQueue         ROUTINE
    DATA
    CODE

        !----------------------------------------------------
        Progress:Text    = 'Clearing Status Code Details'
        RecordsToProcess = RECORDS(Status_Queue)

        DO ProgressBar_LoopPre
        !----------------------------------------------------
        LOOP x# = 1 TO RECORDS(Status_Queue)
            !------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !------------------------------------------------
            RecordsProcessed += 1
                                                  
            GET(Status_Queue, x#)
                CLEAR(Status_Queue)
                CLEAR(statusQ:TotalJobsCount)
                CLEAR(statusQ:Days)
            ADD(Status_Queue)
            !------------------------------------------------
        END !LOOP
        !----------------------------------------------------
        SORT(Status_Queue, +statusQ:Status)
        !----------------------------------------------------
        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !----------------------------------------------------
    EXIT
ClearSubStatusQueue         ROUTINE
    DATA
    CODE
        !----------------------------------------------------
        Progress:Text    = 'Clearing Status Code Details'
        RecordsToProcess = RECORDS(Status_Queue)

        DO ProgressBar_LoopPre
        !----------------------------------------------------
        LOOP x# = 1 TO RECORDS(Status_Queue)
            !------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !------------------------------------------------
            RecordsProcessed += 1
                                                  
            GET(Status_Queue, x#)
                CLEAR(statusQ:Sub_Group)
                !CLEAR(statusQ:SubTotalJobsCount)
                !CLEAR(statusQ:SubDays)
            ADD(Status_Queue)
            !------------------------------------------------
        END !LOOP
        !----------------------------------------------------
        SORT(Status_Queue, +statusQ:Status)
        !----------------------------------------------------
        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !----------------------------------------------------
    EXIT
FillJobNumberQueue              ROUTINE
    DATA
    CODE
        !------------------------------------------
        ! AccountNumberKey KEY( job:Account_Number, job:Ref_Number ),DUP,NOCASE
        ! Date_Booked_Key  KEY( job:date_booked                    ),DUP,NOCASE
        ! DateCompletedKey KEY( job:Date_Completed                 ),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:Date_Booked = LOC:StartDate
        SET(job:Date_Booked_Key,job:Date_Booked_Key)
        !------------------------------------------
        Progress:Text    = 'Finding Jobs'
        RecordsToProcess = RECORDS(Jobs)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.NEXT()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:Date_Booked > LOC:EndDate
                BREAK
            END !IF

            IF job:Date_Despatched > 0
                CYCLE
            ELSIF (job:Exchange_Unit_Number > 0)
                CYCLE
            ELSIF NOT LoadSUBTRACC(job:Account_Number)
                CYCLE
            ELSIF NOT LoadTRADEACC(sub:Main_Account_Number)
                CYCLE
            END !IF
            !=============================================================
            CLEAR(JobNumber_Queue)
                jobnumQ:JobNumber         = job:Ref_Number
                jobnumQ:HeadAccountNumber = sub:Main_Account_Number
            ADD(JobNumber_Queue)
            !-------------------------------------------------------------
        END !LOOP

        SORT(JobNumber_Queue, +jobnumQ:HeadAccountNumber, +jobnumQ:JobNumber)

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0057 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0053 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0053 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('Attempting to use ' & Clip(loc:ProgramName) & ' without using ' & Clip(loc:ApplicationName) & '.'&|
              '<13,10>'&|
              '<13,10>Start ' & Clip(loc:ApplicationName) & ' and run the report from there.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Unable to find your logged in user details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = clip(use:Forename)&' '&clip(use:Surname)       !AppendString(use:Forename, use:Surname, ' ')

!        IF CLIP(LOC:UserName) = ''
!            LOC:UserName = use:Surname
!        ELSE
!            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
!        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetUserName2                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE
        tmpPos = INSTRING('%', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Status Analysis Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & CLIP(excel:ProgramName) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!!        IF CLIP(LOC:FileName) <> ''
!!            ! Already generated 
!!            EXIT
!!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END
!            desktop = CLIP(desktop)&'\EMAILED'
!            IF NOT EXISTS(CLIP(desktop))
!                IF MKdir( Desktop )
!                  MESSAGE('ERROR')
!                END
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0057 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) &  ' VODR0053 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
LoadSUBTRACC                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            EXIT
        END !IF

        IF sub:Account_Number <> job:Account_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
SaveAllRRCSheets            ROUTINE
    DATA
Sheets    LONG
SheetName STRING(30)
FileName  STRING(255)
WorkBook  STRING(255)
    CODE
        !-----------------------------------------------------------------
        Sheets = Excel{'Sheets.Count'}
        !WriteDebug('SaveAllRRCSheets(' & Sheets & ')')

        IF Sheets < 1
            !WriteDebug('SaveAllRRCSheets(FAIL=IF Sheets < 1)')
            EXIT
        END !IF

        WorkBook = Excel{'ActiveWorkbook.Name'}
        !WriteDebug('SaveAllRRCSheets(WorkBook="' & CLIP(WorkBook) & '")')

        LOOP x# = 0 TO Sheets
            IF (x# = Sheets)
                BREAK
            END !IF

            Excel{'Sheets(' & x# & ').Select'}
            SheetName = Excel{'ActiveSheet.Name'}
                !-----------------------------------------------
                ! 23 Oct 2002 John
                ! R003, berrjo :Insert the report exe name EG VODR56.V01
                ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
                ! Make this standard on all reports sent for correction.
                ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
                !
                !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0057 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
                !FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
            FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' VODR0053 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
                !-----------------------------------------------
            
            !WriteDebug('SaveAllRRCSheets(SheetName="' & CLIP(SheetName) & '", FileName="' & CLIP(FileName) & '")')

            CASE CLIP(SheetName)
            OF 'Summary'
                ! NULL
            OF 'Sheet1'
                ! NULL
            OF 'Sheet3'
                ! NULL
            ELSE
                Excel{'ActiveSheet.Copy'}
                !WriteDebug('SaveAllRRCSheets(COPY)')

                Excel{'ActiveWorkbook.SaveAs("' & CLIP(FileName) & '")'}
                !WriteDebug('SaveAllRRCSheets(SaveAs)')

                Excel{'ActiveWindow.Close'}
                !WriteDebug('SaveAllRRCSheets(Close)')
            END ! CASE
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
!XL_thingy Routines
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT

XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT

XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT

XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT

XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT

XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
!not used
!XL_AddComment                ROUTINE
!    DATA
!xlComment CSTRING(20)
!    CODE
!        !-----------------------------------------------
!        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}
!
!!        xlComment{'Shape.IncrementLeft'} = 127.50
!!        xlComment{'Shape.IncrementTop'}  =   8.25
!!        xlComment{'Shape.ScaleWidth'}    =   1.76
!!        xlComment{'Shape.ScaleHeight'}   =   0.69
!        !-----------------------------------------------
!    EXIT

XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT

!not used
!XL_ColLeft                   ROUTINE
!    DATA
!CurrentCol LONG
!    CODE
!        !-----------------------------------------------
!        CurrentCol = Excel{'ActiveCell.Col'}
!        IF CurrentCol = 1
!            EXIT
!        END !IF
!
!        Excel{'ActiveCell.Offset(0, -1).Select'}
!        !-----------------------------------------------
!    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT

XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT

XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT

XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT

XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            EXIT
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT

XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
        excel:OperatingSystem = 6.0    ! Default/error value
!
!        OperatingSystem       = Excel{'Application.OperatingSystem'}
!        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))
!
!        LOOP x# = LEN_OperatingSystem TO 1 BY -1
!
!            CASE SUB(OperatingSystem, x#, 1)
!            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
!                ! NULL
!            ELSE
!                tmpLen                = LEN_OperatingSystem-x#+1
!                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))
!
!                EXIT
!            END !CASE
!        END !LOOP
        
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020611'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StatusAnalysisReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDSTATS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STATUS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:USERS.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSE.UseFile
  Access:TRABUSHR.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  !============== Version Number =====================
  Include('..\ReportVersion.inc')
  
  tmp:VersionNumber = Clip(tmp:VersionNumber) & '505'
  loc:Version = tmp:VersionNumber
  ?ReportVersion{prop:Text} = 'Report Version: ' & Clip(loc:Version)
      LOC:ProgramName         = 'Status Analysis Report'  ! Job=2093    Cust=VodaCom
      MainWindow{PROP:Text}   = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}        = CLIP(LOC:ProgramName) & ' Criteria'
  
      excel:Visible  = FALSE
      !debug:Active   = False
  
      !----------------------------------------------------------------
      ! 15 Oct 2002 John
      ! To speed up report generation add criteria to run the report from
      !   a chosen date
      !
      LOC:GreaterThanDays = 7
      LOC:StartDate       = TODAY() - 90
      LOC:EndDate         = TODAY()
      !----------------------------------------------------------------
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName2
  
      IF GUIMode = 1 THEN
         LocalTimeOut = 500
         MainWindow{PROP:ICONIZE} = TRUE
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  
      !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
      If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
          LOC:UserName = 'AUTOMATED PROCEDURE'
          Automatic = True
      End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  
      !Start - *** YOU MUST REMOVE THIS AFTER TESTING *** - TrkBs: 4857 (DBH: 22-02-2005)
      !Automatic = False
      !End   - *** YOU MUST REMOVE THIS AFTER TESTING *** - TrkBs: 4857 (DBH: 22-02-2005)
  
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        LOC:StartDate       = TODAY()-60 !DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        LOC:EndDate       = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
        LOC:GreaterThanDays = 0
      END
  
      DISPLAY
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
      IF Automatic = TRUE
        IF loc:startdate = ''
          LOC:StartDate = TODAY()
          LOC:EndDate = TODAY()
        END
!        DoAll = 'Y'
        loc:GreaterThanDays = 0

        !Start Date is Today - 4 Months  (DBH: 07-05-2004)
        If Command('/DAILY')
            month#  = Month(Today())
            If month# < 5
                loc:StartDate = Deformat(Day(Today()) & '/' & 8 + month# & '/' & Year(Today())-1,@d6)
            Else !If month# = 1
                loc:StartDate = Deformat(Day(Today()) & '/' & Month(Today())-4 & '/' & Year(Today()),@d6)
            End !If month# = 1
        End !If Command('/DAILY')

        !Weekly and Monthly Not Requested For This Report  (DBH: 07-05-2004)
        If Command('/WEEKLY')
            Loop day# = Today() To Today()-7 By -1
                If day# %7 = 1
                    loc:StartDate = day#
                    Break
                End !If day# %7 = 2
            End !Loop day# = Today() To Today()-7 By -1
            Loop day# = Today() To Today() + 7
                IF day# %7 = 0
                    loc:EndDate = day#
                    Break
                End !IF day# %7 = 0
            End !Loop day# = Today() To Today() + 7 By -1
        End !If Command('/WEEKLY')
        If Command('/MONTHLY')
            loc:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
            !loc:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
        End !If Command('/MONTHLY')

        DO OKButtonPressed
        IF nbx = 1
            !POST(Event:Closewindow)
            !Call email program!
            PUTINI('MAIN','InputDir',CLIP(Loc:Path),CLIP(PATH()) & '\AUTOEMAIL.INI')
            PUTINI('MAIN','ExportDir',CLIP(Loc:Path)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
            RUN('EMAILDIR',1)
        END
        POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDSTATS.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STATUS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020611'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020611'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020611'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendarStart
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendarStart)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.WriteRowPerType     Procedure(String func:Type)
local:FoundStatusHistory    Byte(0)
local:CurrentStatus         String(30)
local:DaysInCurrentStatus   Long()
local:FinalDaysInCurrentStatus  Long()
local:DateEstimateAccepted      Date()

Code

    !SB Job Number
    WriteColumn(job:Ref_Number,True)
    !Franchise Branch Code
    WriteColumn(haq:AccountName)
    !StatusType
    WriteColumn(func:Type)

    Case func:Type
        Of 'JOB'
            local:CurrentStatus = job:Current_Status
        Of 'EXC'
            local:CurrentStatus = job:Exchange_Status
        Of 'LOA'
            local:CurrentStatus = job:Loan_Status
        Of '2NE'
            local:CurrentStatus = jobe:SecondExchangeStatus
    End ! Case func:Type

    loc:OldStatus = ''
    loc:Actual_Date_Change = ''
    loc:Actual_Time_Change = ''

    Save_aus_ID = Access:AUDSTATS.SaveFile()
    Access:AUDSTATS.ClearKey(aus:NewStatusKey)
    aus:RefNumber = job:Ref_Number
    aus:Type      = func:Type
    aus:NewStatus = local:CurrentStatus
    Set(aus:NewStatusKey,aus:NewStatusKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber <> job:Ref_Number      |
        Or aus:Type      <> func:Type      |
        Or aus:NewStatus <> local:CurrentStatus      |
            Then Break.  ! End If
        loc:OldStatus = aus:OldStatus
        loc:Actual_Date_Change = aus:DateChanged
        loc:Actual_Time_Change = aus:TimeChanged
        local:FoundStatusHistory = True
    End !Loop
    Access:AUDSTATS.RestoreFile(Save_aus_ID)

    If local:FoundStatusHistory = False
        If func:Type = 'JOB'
            loc:OldStatus = job:PreviousStatus
            loc:Actual_Date_Change = wob:Current_Status_Date
        End ! If func:Type = 'JOB'
        If func:Type = 'EXC'
            loc:Actual_Date_Change = wob:Exchange_Status_Date
        End ! If func:Type = 'EXC'
    End !If local:FoundStatusHistory = False

    If jobe:OBFValidated And func:Type = 'JOB'
        loc:OldStatus = Clip(loc:OldStatus) & ' (OBF)'
        local:CurrentStatus = Clip(local:CurrentStatus) & ' (OBF)'
    End ! jobe:OBFValidated And func:Type = 'JOB'

    !Old Status
    WriteColumn(loc:OldStatus)
    !Current Status
    WriteColumn(local:CurrentStatus)
    !Status Date Change
    WriteColumn(Format(loc:Actual_Date_Change,@d17b))
    !Status Time Change
    WriteColumn(Format(loc:Actual_Time_Change,@t1))
    !Days In Current Status
        !Work out the "actual trade" minutes in the current status - TrkBs:  (DBH: 11-03-2005)
        local:DaysInCurrentStatus = local.TimeDifference(loc:Actual_Date_Change,Today(),loc:Actual_Time_Change,Clock(),LOC:RepairCentreType)

        ! Work out time based on 9 hrs for ARC, 8.5 for RRC and 10 for AA40 - TrkBs: 6020 (DBH: 20-10-2005)
        Case LOC:RepairCentreType
        Of 'RRC'
            If wob:HeadAccountNumber = 'AA40'
                local:FinalDaysInCurrentStatus = (INT(local:DaysInCurrentStatus / 600))
            Else ! If wob:HeadAccountNumber = 'AA40'
                local:FinalDaysInCurrentStatus = (INT(local:DaysInCurrentStatus / 510))
            End ! If wob:HeadAccountNumber = 'AA40'
        Of 'ARC'
            local:FinalDaysInCurrentStatus = (INT(local:DaysInCurrentStatus / 540))
        End ! Case LOC:RepairCentreType

    WriteColumn(local:FinalDaysInCurrentStatus)

    !TB13236 - J - 18/02/14 - add new field  Date Received At PUP
    !if applicable Date Rec at PUP will be booking date and Date Recevied at RRC becomed date it arrived
    LOC:PUPDateRecived = ''    !reset to blank
    LOC:PUPflag = 0            !used to indicate this has been at PUP
    
    Access:LocatLog.ClearKey(lot:DateKey)
    lot:RefNumber = job:Ref_number
    lot:TheDate = 0
    SET(lot:DateKey,lot:DateKey)
    LOOP
        if Access:LocatLog.NEXT() then break.
        IF lot:RefNumber <> job:Ref_Number then break.
        
        IF instring('PUP',lot:NewLocation,1,1) then
            LOC:PUPflag = 1    !it has been at the PUP
            LOC:PUPDateRecived = Format(job:date_booked,@d17)
                                       !loc:RRCDateBooked
            loc:RRCDateBooked = ''
        END

        if lot:NewLocation = LOC:ARCLocation then break.  !has been sent to ARC, there will be a return date that we do not want

        IF lot:NewLocation <> LOC:RRCLocation then cycle.

        !by here it has arrived at the RRC ...
        if LOC:PUPflag = 1 then
            loc:RRCDateBooked = lot:TheDate
        END

        BREAK

    END !LOOP

    WriteColumn(LOC:PUPDateRecived)
    !end TB13236

    !Date Received At RRC
    if loc:RRCDateBooked > 0 then
        WriteColumn(Format(loc:RRCDateBooked,@d17))
    ELSE
        WriteColumn('')
    END
    !Date Received At ARC
    If loc:ARCDateBooked > 0 then
        WriteColumn(Format(loc:ARCDateBooked,@d17))
    ELSE
        WriteColumn('')
    END

    !Total Days Since Receipt
    !Number of days from booking until today - TrkBs: 4857 (DBH: 01-03-2005)
    WriteColumn(WorkingDaysBetween(job:Date_Booked,Today(),haq:IncludeSaturday,haq:IncludeSunday))

    !Date Completed
    if job:Date_Completed > 0 then
        WriteColumn(Format(job:Date_Completed,@d17))
    ELSE
        WriteColumn('')
    END

    !TB13236 - J - 18/02/14 - add new field Repair Location - If exchange issued at RRC then this is RRC otherwise as Status Report
    If job:Exchange_Unit_Number <> ''  and  jobe:ExchangedATRRC then
        WriteColumn('RRC')
    ELSE 
        If SentToHub(job:Ref_Number)
            If job:Third_Party_Site <> ''
                WriteColumn('3RD')
            Else !If job:Third_Party_Site <> ''
                WriteColumn('ARC')
            End !If job:Third_Party_Site <> ''
        Else !If SentToHub(job:Ref_Number)
            WriteColumn('RRC')
        End !If SentToHub(job:Ref_Number)
    END  !END of  TB13236
    !END TB13236 - J - 18/02/14 - add new field Repair Location

    !Make
    WriteColumn(job:Manufacturer)
    !Model
    WriteColumn(job:Model_Number)
    !Account Name
    WriteColumn(saq:AccountName)
    !I.M.E.I.
    WriteColumn(job:ESN)
    !Exchange I.M.E.I.
    If func:Type = 'EXC' or func:Type = '2NE'
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            WriteColumn(xch:ESN)
        Else !If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Error
            WriteColumn('** ERROR **')
        End !If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign        
    Else ! If func:Type = 'EXC'
        WriteColumn('')
    End ! If func:Type = 'EXC'
    !2nd Exchange I.M.E.I.
    If func:Type = 'EXC' or func:Type = '2NE'
        If jobe:SecondExchangeNumber <> 0
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = jobe:SecondExchangeNumber
            If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                WriteColumn(xch:ESN)
            Else !If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                WriteColumn('')
            End !If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign            
        Else ! If jobe:SecondExchangeNumber <> 0
            WriteColumn('')
        End ! If jobe:SecondExchangeNumber <> 0
    Else ! If func:Type = 'EXC' or func:Type = '2NE'
        WriteColumn('')    
    End ! If func:Type = 'EXC' or func:Type = '2NE'
    !Exchange Status
    If func:Type = 'EXC'
        WriteColumn(job:Exchange_Status) 
    Else ! If func:Type = 'EXC'
        WriteColumn('')    
    End ! If func:Type = 'EXC'
    !Loan Status
    If func:Type = 'LOA'
        WriteColumn(job:Loan_Status)
    Else ! If func:Type = 'LOA'
        WriteColumn('')    
    End ! If func:Type = 'LOA'
! Insert --- New columns (DBH: 20/03/2009) #10591
    !Warranty Job
    if (job:Warranty_Job = 'YES')
        WriteColumn('YES')
    else ! end ! if (job:Warranty_Job = 'YES')
        WriteColumn('NO')
    end ! end ! if (job:Warranty_Job = 'YES')
    !Third Party Site
    WriteColumn(job:Third_Party_Site)
    !Days Since Quote Accepted
    local:DateEstimateAccepted = 0
    Access:AUDIT.Clearkey(aud:Ref_Number_Key)
    aud:Ref_Number    = job:Ref_Number
    aud:Date    = Today()
    set(aud:Ref_Number_Key,aud:Ref_Number_Key)
    loop
        if (Access:AUDIT.Next())
            Break
        end ! if (Access:AUDIT.Next())
        if (aud:Ref_Number    <> job:Ref_Number)
            Break
        end ! if (aud:Ref_Number    <> job:Ref_Number)
        if (Instring('ESTIMATE ACCEPTED',aud:Action,1,1))
            local:DateEstimateAccepted = aud:Date
            Break
        end ! if (Instring('ESTIMATE ACCEPTED',aud:Action,1,1))
    end ! loop

    if (local:DateEstimateACcepted = 0)
        WriteColumn('')
    else ! if (local:DateEstimateACcepted = 0)
        countDays# = 0
        Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
        tra_ali:Account_Number = wob:HeadAccountNumber
        if (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)
            ! Found
            loop x# = local:DateEstimateAccepted To Today()

                if (tra_ali:IncludeSaturday = 0)
                    if (x# % 7 = 6)
                        cycle
                    end ! if (x# % 7 = 6)
                end ! if (tra_ali:IncludeSaturday = 0)
                if (tra_ali:IncludeSunday = 0)
                    if (x# % 7 = 0)
                        cycle
                    end ! if (x# % 7 = 6)
                end ! if (tra_ali:IncludeSaturday = 0)

                Access:TRABUSHR.Clearkey(tbh:TypeDateKey)
                tbh:RefNumber    = tra_ali:RecordNumber
                tbh:TheDate        = x#
                if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
                    ! Found
                    if (tbh:ExceptionType = 1)
                        cycle
                    end ! if (tbh:ExceptionType = 1)
                else ! if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
                    ! Error
                end ! if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)

                countDays# += 1
            end ! loop x# = local:DateEstimateAccepted To Today()
        else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        WriteColumn(countDays#)
    end ! if (local:DateEstimateACcepted = 0)
! end --- (DBH: 20/03/2009) #10591

!TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!    IF excel:OperatingSystem < 5
!        DO PassViaClipboard
!    END !IF
    !---------------------------------------------------
    DO XL_ColFirst
    DO XL_RowDown

    RecordCount += 1

    !__________________-__________________________________________________

    !Work out which column to count this job in - TrkBs:  (DBH: 14-03-2005)

    Row# = 0
    If local:DaysInCurrentStatus < 60
        !1 Hr - TrkBs:  (DBH: 14-03-2005)
        Row# = 1
    End ! If local:DaysInCurrentStatus < 60
    If local:DaysInCurrentStatus >= 60 And local:DaysInCurrentStatus < 240
        !4 Hrs - TrkBs:  (DBH: 14-03-2005)
        Row# = 2
    End ! If local:DaysInCurrentStatus >= 60 And local:DaysInCurrentStatus < 240
    If local:DaysInCurrentStatus >= 240 And local:DaysInCurrentStatus < 480
        !8 Hrs - TrkBs:  (DBH: 14-03-2005)
        Row# = 3
    End ! If local:DaysInCurrentStatus >= 240 And local:DaysInCurrentStatus < 480
    If local:DaysInCurrentStatus >= 480
        ! 1 Day (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus < 1
            Row# = 4
        End ! If local:FinalDaysInCurrentStatus < 2e
        ! 2 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 1 And local:FinalDaysInCurrentStatus < 2
            Row# = 5
        End ! If local:FinalDaysInCurrentStatus >= 2 And local:FinalDaysInCurrentStatus < 3
        ! ! 3 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 2 And local:FinalDaysInCurrentStatus < 3
            Row# = 6
        End ! If local:FinalDaysInCurrentStatus >= 3 And local:FinalDaysInCurrentStatus < 4
        ! 4 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 3 And local:FinalDaysInCurrentStatus < 4
            Row# = 7
        End ! If local:FinalDaysInCurrentStatus >= 4 And local:FinalDaysInCurrentStatus < 5
        ! 5 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 4 And local:FinalDaysInCurrentStatus < 5
            Row# = 8
        End ! If local:FinalDaysInCurrentStatus >= 5 And local:FinalDaysInCurrentStatus < 7
        ! 7 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 5 And local:FinalDaysInCurrentStatus < 7
            Row# = 9
        End ! If local:FinalDaysInCurrentStatus >= 7 And local:FinalDaysInCurrentStatus < 9
        ! 9 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 7 And local:FinalDaysInCurrentStatus < 9
            Row# = 10
        End ! If local:FinalDaysInCurrentStatus >= 9 And local:FinalDaysInCurrentStatus < 14
        ! 14 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 9 And local:FinalDaysInCurrentStatus < 14
            Row# = 11
        End ! If local:FinalDaysInCurrentStatus >=14 And local:FinalDaysInCurrentStatus < 30
        ! 30 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 14 And local:FinalDaysInCurrentStatus < 30
            Row# = 12
        End ! If local:FinalDaysInCurrentStatus >= 30 And local:FinalDaysInCurrentStatus <60
        ! 60 DAys (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 30 And local:FinalDaysInCurrentStatus < 60
            Row# = 13
        End ! If local:FinalDaysInCurrentStatus >= 60 And local:FinalDaysInCurrentStatus < 90
        ! 90 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 60 And local:FinalDaysInCurrentStatus < 90
            Row# = 14
        End ! If local:FinalDaysInCurrentStatus >= 60 And local:FinalDaysInCurrentStatus < 90
        ! > 90 Days (DBH: 20-10-2005)
        If local:FinalDaysInCurrentStatus >= 90
            Row# = 15
        End ! If local:FinalDaysInCurrentStatus >= 90
    End !If local:DaysInCurrentStatus >= 480
    !Add to the summary queue and count in the right column - TrkBs:  (DBH: 14-03-2005)
    Clear(Status_Queue)
    statusQ:Status = local:CurrentStatus
    statusQ:JobType = func:Type
    Get(Status_Queue,'statusQStatus,statusQJobType')
    If Error()
        statusQ:Status      = local:CurrentStatus
        statusQ:JobType     = func:Type
        statusQ:Days[Row#]  = 1
        statusQ:TotalJobsCount  = 1
        statusQ:SubDays[Row#]   = 1
        statusQ:SubTotalJobsCount   = 1
        Add(Status_Queue)
    Else ! If Error()
        statusQ:JobType     = func:Type
        statusQ:Days[Row#]  += 1
        statusQ:TotalJobsCount  += 1
        statusQ:SubDays[Row#]   += 1
        statusQ:SubTotalJobsCount   += 1
        Put(Status_Queue)
    End ! If Error()


Local.TimeDifference        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime, String func:AccountNumber)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

local:Type      Byte(0)

Code
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    Case func:AccountNumber
        Of 'RRC' !RRC
            tra_ali:Account_Number = wob:HeadAccountNumber
            If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Found

            Else !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Error
                Return 0
            End !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
            local:Type = 0
        Of 'ARC' !ARC
            tra_ali:Account_Number = LocalHeadAccount
            If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Found

            Else !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Error
                Return 0
            End !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
            local:Type = 0

    End !Case func:AccountNumber


    Loop x# = func:StartDate to func:EndDate
        If Command('/DEBUG')
            PUTINI(Clip(job:Ref_Number),'Working Out Trade (' & CLip(aus:RecordNumber) & ')',Clip(tra_ali:Account_Number),'c:\debug.ini')
        End !If Command('/DEBUG')

        Access:TRABUSHR.ClearKey(tbh:TypeDateKey)
        tbh:RefNumber = tra_ali:RecordNumber
        tbh:TheDate   = x#
        If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
            !Found
            Case tbh:ExceptionType            
                Of 0 !Exception
                    local:StartTime   = tbh:StartTime
                    local:EndTime     = tbh:EndTime
                    If x# = func:StartDate
                        If func:StartTime > tbh:StartTime
                            If func:StartTime > tbh:EndTime
                                local:StartTime = 0
                                local:EndTime = 0
                            Else !If func:StartTime > tbh:EndTime
                                local:StartTime = func:StartTime
                            End !If func:StartTime > tbh:EndTime
                        End !If local:StartTime < tbh:StartTime
                    End !If x# = func:StartDate

                    If x# = func:EndDate
                        If func:EndTime < tbh:EndTime
                            If func:EndTime < tbh:StartTime
                                local:StartTime = 0
                                local:EndTime = 0
                            Else !If func:EndTime < tbh:StarTime
                                local:EndTime = func:EndTime
                            End !If func:EndTime < tbh:StarTime
                        End !If local:EndTime > tbh:EndTime
                    End !If x# = func:EndDate
                Of 1 !Free Day
                    local:StartTime   = 0
                    local:EndTime     = 0
            End !Case tbh:ExceptionType            
        Else !If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
            !Error
            Case x# % 7
                Of 0 !Sunday
                    local:StartTime   = tra_ali:SunStartWorkHours
                    local:EndTime     = tra_ali:SunEndWorkHours
                    ! Never include the weekend, no matter what - TrkBs: 6020 (DBH: 20-10-2005)

!                    If tra_ali:IncludeSunday = 'YES'
!                        If x# = func:StartDate
!                            If func:StartTime > tra_ali:SunStartWorkHours
!                                If func:StartTime > tra_ali:SunEndWorkHours
!                                    local:EndTime = 0
!                                    local:StartTime = 0
!                                Else !If func:StartTime > tra_ali:EndWorkHours
!                                    local:StartTime = func:StartTime
!                                End !If func:StartTime > tra_ali:EndWorkHours
!                            End !If local:StartTime > tra_ali:StartWorkHours                        
!                        End !If x# = func:StartDate
!                        If x# = func:EndDate
!                            If func:EndTime < tra_ali:SunEndWorkHours
!                                If func:EndTime < tra_ali:SunStartWorkHours
!                                    local:EndTime = 0
!                                    local:StartTime = 0
!                                Else !If func:EndTime < tra_ali:StartWorkHours
!                                    local:EndTime = func:EndTime
!                                End !If func:EndTime < tra_ali:StartWorkHours
!                            End !If local:EndTime < tra_ali:EndWorkHours
!                        End !If x# = func:EndDate
!
!                    Else !If tra_ali:IncludeSunday = 'YES'
                        local:StartTime   = 0
                        local:EndTime    = 0
!                    End !If tra_ali:IncludeSunday = 'YES'
                Of 6 !Saturday
                    local:StartTime   = tra_ali:SatStartWorkHours
                    local:EndTime     = tra_ali:SatEndWorkHours
!                    If tra_ali:IncludeSaturday = 'YES'
!                        If x# = func:StartDate
!                            If func:StartTime > tra_ali:SatStartWorkHours
!                                If func:StartTime > tra_ali:SatEndWorkHours
!                                    local:EndTime = 0
!                                    local:StartTime = 0
!                                Else !If func:StartTime > tra_ali:EndWorkHours
!                                    local:StartTime = func:StartTime
!                                End !If func:StartTime > tra_ali:EndWorkHours
!                            End !If local:StartTime > tra_ali:StartWorkHours                        
!                        End !If x# = func:StartDate
!                        If x# = func:EndDate
!                            If func:EndTime < tra_ali:SatEndWorkHours
!                                If func:EndTime < tra_ali:SatStartWorkHours
!                                    local:EndTime = 0
!                                    local:StartTime = 0
!                                Else !If func:EndTime < tra_ali:StartWorkHours
!                                    local:EndTime = func:EndTime
!                                End !If func:EndTime < tra_ali:StartWorkHours
!                            End !If local:EndTime < tra_ali:EndWorkHours
!                        End !If x# = func:EndDate
!
!                    Else !If tra_ali:IncludeSaturday = 'YES'
                        local:StartTime   = 0
                        local:EndTime     = 0
!                    End !If tra_ali:IncludeSaturday = 'YES'
                Else
                    local:StartTime   = tra_ali:StartWorkHours
                    local:EndTime     = tra_ali:EndWorkHours
                    If x# = func:StartDate
                        If func:StartTime > tra_ali:StartWorkHours
                            If func:StartTime > tra_ali:EndWorkHours
                                local:EndTime = 0
                                local:StartTime = 0
                            Else !If func:StartTime > tra_ali:EndWorkHours
                                local:StartTime = func:StartTime
                            End !If func:StartTime > tra_ali:EndWorkHours
                        End !If local:StartTime > tra_ali:StartWorkHours                        
                    End !If x# = func:StartDate
                    If x# = func:EndDate
                        If func:EndTime < tra_ali:EndWorkHours
                            If func:EndTime < tra_ali:StartWorkHours
                                local:EndTime = 0
                                local:StartTime = 0
                            Else !If func:EndTime < tra_ali:StartWorkHours
                                local:EndTime = func:EndTime
                            End !If func:EndTime < tra_ali:StartWorkHours
                        End !If local:EndTime < tra_ali:EndWorkHours
                    End !If x# = func:EndDate
            End !Case func:EndDate
        End !If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
 
        local:1Time = Format(local:EndTime,@t2)          !these stay at t2 - used for splitting up the time
        local:2Time = Format(local:StartTime,@t2)

        local:1a = Sub(local:1Time,4,1)
        local:1b = Sub(local:1Time,3,1)
        local:1c = Sub(local:1time,2,1)
        local:1d = Sub(local:1time,1,1)

        local:2a = Sub(local:2Time,4,1)
        local:2b = Sub(local:2Time,3,1)
        local:2c = Sub(local:2time,2,1)
        local:2d = Sub(local:2time,1,1)

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

! ======================================================
!AppendString        PROCEDURE(IN:Start, IN:Add, IN:Separator)!STRING
!ReturnValue STRING(255)
!    CODE
!        !-----------------------------------------------------------------
!        ! IN:Start,     Accumulating string
!        ! IN:Add,       String to add to accumulating string
!        ! IN:Separator, Separator if both non null
!        !               ie ',' for comma delimited, or ' AND ' for SQL
!        !               Use (literals) or (variables without excesive trailing white space).
!        !-----------------------------------------------------------------
!        IF CLIP(IN:Start) = ''
!            ReturnValue = IN:Add
!        ELSIF CLIP(IN:Add) = ''
!            ReturnValue = IN:Start
!        ELSE
!            ReturnValue = CLIP(IN:Start) & IN:Separator & CLIP(IN:Add)
!        END !IF
!
!        RETURN ReturnValue
!        !-----------------------------------------------------------------
!        ! Example
!        !SQL STRING(255)
!        !   CODE
!        !   IF ?CHOICE1
!        !       SQL = '"CANCELLED" = "YES"'
!        !   END !IF
!        !
!        !   IF ?Date1 > 0
!        !       SQL = AppendString( SQL, '"DATE_BOOKED" = "' LEFT(FORMAT(job:Date_Booked, @D10)) '"', ' AND ')
!        !   END !IF
!        !
!        !   IF ?CHOICE2
!        !       SQL = AppendString( SQL, '"COMPLETE" = "YES"', ' AND ')
!        !   END !IF
!        !-----------------------------------------------------------------
!DateReturnedToRRC       PROCEDURE()! STRING
!    CODE
!        !-----------------------------------------------------------------
!        ! 23 Oct 2002 John
!        ! R002, MICHALAN: Include a new column indicating "Date Received back at RRC" if repaired and completed at ARC
!        ! Currently this function is a placeholder for when the correct algorithm has
!        !   been decided.
!        RETURN ''  ! DateToString(DateReturned)
!        !-----------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
FillStatusQueue             PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            RETURN
        END !IF
        !-----------------------------------------------------------------
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
        Access:STATUS.ClearKey(sts:Status_Key)
        !sts:Status = ''
        SET(sts:Status_Key, sts:Status_Key)
        !------------------------------------------
        Progress:Text    = 'Finding All Status Codes' 
        RecordsToProcess = 1000 * (LOC:EndDate - LOC:StartDate + 1) !RECORDS(JOBS)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP WHILE Access:STATUS.NEXT() = Level:Benign
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            RecordCount += 1

            CLEAR(Status_Queue)
                statusQ:Status = sts:Status
            ADD(Status_Queue, +statusQ:Status)
            !-------------------------------------------------------------
        END !LOOP

        SORT(Status_Queue, +statusQ:Status)
        !-----------------------------------------------------------------
        IF CancelPressed
            RETURN
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        !-----------------------------------------------------------------
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-----------------------------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL

    CODE
    SentToARC# = False
    LOC:ARCDateBooked = ''

    Access:LOCATLOG.Clearkey(lot:DateKey)
    lot:RefNumber = in:JOBNumber
    Set(lot:DateKey,lot:DateKey)
    Loop 
        If Access:LOCATLOG.Next()
            Break
        End !
        If lot:RefNumber <> in:JobNumber 
            Break
        End !
        If lot:NewLocation = loc:ARCLocation
            SentToARC# = True
            loc:ARCDateBooked = lot:TheDate
            !MESSAGE('TRUE CONDITION MET')
        Break
        End!    
      
    End !Loop

    Return SentToARC#




!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------







!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            CLEAR(HeadAccount_Queue)
                haq:AccountNumber                  = IN:AccountNumber

                IF LoadTRADEACC(IN:AccountNumber)
                    haq:AccountName                = tra:Company_Name
                    haq:BranchIdentification       = tra:BranchIdentification

                    IF tra:UseTimingsFrom = 'DEF'
                        IF def:Include_Saturday    = 'YES'
                            haQ:IncludeSaturday    = True
                        ELSE
                            haQ:IncludeSaturday    = False
                        END !IF

                        IF def:Include_Sunday = 'YES'
                            haQ:IncludeSunday      = True
                        ELSE
                            haQ:IncludeSunday      = False
                        END !IF

                        haQ:StartWorkHours         = def:Start_Work_Hours
                        haQ:EndWorkHours           = def:End_Work_Hours

                    ELSE
                        IF tra:IncludeSaturday = 'YES'
                            haQ:IncludeSaturday    = True
                        ELSE
                            haQ:IncludeSaturday    = False
                        END !IF

                        IF tra:IncludeSunday = 'YES'
                            haQ:IncludeSunday      = True
                        ELSE
                            haQ:IncludeSunday      = False
                        END !IF

                        haQ:StartWorkHours         = tra:StartWorkHours
                        haQ:EndWorkHours           = tra:EndWorkHours

                    END !IF

                ELSE
                    haq:AccountName                = '*T'
                    haq:BranchIdentification       = '*T'

                    haQ:StartWorkHours             = def:Start_Work_Hours
                    haQ:EndWorkHours               = def:End_Work_Hours
                END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
GetReturnDateToRRC        PROCEDURE( IN:JobNumber, IN:DelivedToARCDate )! DATE
First LONG(True)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetReturnDateToRRC(' & IN:JobNumber & ')')

        LOC:RRCDateReturned = ''
        First = False

        Access:LocatLog.ClearKey(lot:DateKey)
            lot:RefNumber = IN:JobNumber
            lot:TheDate = IN:DelivedToARCDate
        SET(lot:DateKey,lot:DateKey)

        LOOP WHILE Access:LocatLog.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF    lot:RefNumber <> IN:JobNumber
                BREAK
            END

            IF lot:NewLocation <> LOC:RRCLocation !LOC:StatusReceivedAtRRC
              CYCLE
            END
            !MESSAGE('OK')
            IF (lot:TheDate > IN:DelivedToARCDate)
                !WriteDebug('GetReturnDateToRRC(OK) lot:TheDate"' & DateToString(lot:TheDate) & '" > IN:DelivedToARCDate"' & DateToString(IN:DelivedToARCDate) & '"')
                LOC:RRCDateReturned = lot:TheDate

                BREAK
            END !IF
            !-------------------------------------------------------------
        END !LOOP

        RETURN LOC:RRCDateReturned
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            CLEAR(SubAccount_Queue)
                saq:AccountNumber            = IN:AccountNumber

                IF LoadSUBTRACC(job:Account_Number)
                    GetHeadAccount(sub:Main_Account_Number)

                    saq:AccountName          = sub:Company_Name
                    saQ:HeadAccountNumber    = sub:Main_Account_Number
                    saQ:HeadAccountName      = haQ:AccountName
                    saQ:BranchIdentification = haQ:BranchIdentification

                END !IF
            ADD(SubAccount_Queue, +saq:AccountNumber)
        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
!        saQ:Count += 1
!        PUT(SubAccount_Queue)
        !-----------------------------------------------------------------
LoadAUDSTATS   PROCEDURE( IN:JobNumber, IN:CurrentStatus )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('LoadAUDSTATS(' & IN:JobNumber & ', "' & CLIP(IN:CurrentStatus) & '")')

        !Modify Code to get *LAST* entry..not first :)!
        Access:AUDSTATS.ClearKey(aus:NewStatusKey)
            aus:Type      = 'JOB'
            aus:RefNumber = IN:JobNumber
            aus:NewStatus = IN:CurrentStatus
        SET(aus:NewStatusKey, aus:NewStatusKey)

        !Record the time the status was changed - TrkBs: 4857 (DBH: 22-02-2005)
        loc:actual_date_change = ''
        loc:Actual_Time_Change = ''
        loc:OldStatus = ''
        LOOP
            IF Access:AudStats.Next()
                BREAK
            END
            IF aus:Type      <> 'JOB'
                BREAK
            END
            IF aus:RefNumber <> IN:JobNumber
                BREAK
            END
            IF aus:NewStatus <> IN:CurrentStatus
                BREAK
            END
            loc:actual_date_change = aus:DateChanged
            loc:Actual_Time_Change = aus:TimeChanged
            loc:OldStatus = aus:OldStatus
        END


!        IF NOT Access:AUDSTATS.NEXT() = Level:Benign
!            !WriteDebug('LoadAUDSTATS(EOF)')
!
!            RETURN False
!        END !IF
!
!        IF NOT aus:Type = 'JOB'
!            !WriteDebug('LoadAUDSTATS(EOI aus:Type = "JOB")')
!
!            RETURN False
!        END !IF
!
!        IF NOT aus:RefNumber = IN:JobNumber
!            !WriteDebug('LoadAUDSTATS(EOI)')
!
!            RETURN False
!        END !IF
!
!        IF NOT aus:NewStatus = IN:CurrentStatus
!            !WriteDebug('LoadAUDSTATS(EOI)')
!
!            RETURN False
!        END !IF
               
        !WriteDebug('LoadAUDSTATS(OK)')

        RETURN True
 
!LoadLOCATLOG   PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        !WriteDebug('LoadLOCATLOG(' & IN:JobNumber & ', ' & IN:First & ')')
!
!        IF IN:First
!            IN:First = False
!            !WriteDebug('LoadLOCATLOG(First)')
!
!            Access:LOCATLOG.ClearKey(lot:DateKey)
!                lot:RefNumber = IN:JobNumber
!            SET(lot:DateKey, lot:DateKey)
!        END !IF
!
!        IF Access:LOCATLOG.NEXT() <> Level:Benign
!            !WriteDebug('LoadLOCATLOG(NEXT=Fail)')
!
!            RETURN False
!        END !IF
!
!        IF NOT lot:RefNumber = IN:JobNumber
!            !WriteDebug('LoadLOCATLOG(NOT Found)')
!
!            RETURN False
!        END !IF
!
!        !WriteDebug('LoadLOCATLOG(OK)')
!
!        RETURN True
!        !-----------------------------------------------
!LoadLOCATLOG_NewLocationKey   PROCEDURE( IN:JobNumber, IN:NewLocation, IN:First )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        !WriteDebug('LoadLOCATLOG_NewLocationKey(' & IN:JobNumber & ', ' & IN:First & ')')
!
!        IF IN:First
!            IN:First = False
!            !WriteDebug('LoadLOCATLOG_NewLocationKey(First)')
!
!            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
!                lot:RefNumber   = IN:JobNumber
!                lot:NewLocation = IN:NewLocation
!            SET(lot:NewLocationKey, lot:NewLocationKey)
!        END !IF
!
!        IF Access:LOCATLOG.NEXT() <> Level:Benign
!            !WriteDebug('LoadLOCATLOG_NewLocationKey(NEXT=Fail)')
!
!            RETURN False
!        END !IF
!
!        IF NOT lot:RefNumber = IN:JobNumber
!            !WriteDebug('LoadLOCATLOG_NewLocationKey(JobNumber NOT Found)')
!
!            RETURN False
!        END !IF
!
!        IF NOT lot:NewLocation = IN:NewLocation
!            !WriteDebug('LoadLOCATLOG_NewLocationKey(NewLoaction NOT Found)')
!
!            RETURN False
!        END !IF
!               
!        !WriteDebug('LoadLOCATLOG_NewLocationKey(OK)')
!
!        RETURN True
!        !-----------------------------------------------
LoadEngineer    PROCEDURE(IN:UserCode)!,BYTE BOOL
    CODE
        !-----------------------------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = IN:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        If Access:USERS.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT use:User_Code = IN:UserCode
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------------------------
!LoadEXCHANGE   PROCEDURE( IN:ExchangeUnitNumber )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
!        xch:Ref_Number = xch:Ref_Number
!
!        IF Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) <> Level:Benign
!            RETURN False
!        END !IF
!
!        IF NOT xch:Ref_Number = xch:Ref_Number
!            RETURN False
!        END !IF
!
!        RETURN True
!        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
!LoadSUBTRACCByTradeAcc_Alias   PROCEDURE( IN:AccountNumber, INOUT:First )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        IF INOUT:First
!            INOUT:First= False
!
!            Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
!            sub:Main_Account_Number = IN:AccountNumber
!        END !IF
!
!        IF Access:SUBTRACC.TryFetch(sub:Main_Account_Key) <> Level:Benign
!            RETURN False
!        END !IF
!
!        IF sub:Main_Account_Number = IN:AccountNumber
!            RETURN False
!        END !IF
!
!        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB          PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.CLEARKEY(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.Fetch(wob:RefNumberKey) = Level:Benign
            RETURN True
        END !IF

        RETURN False
        !-----------------------------------------------

NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
SaveRRCSheet            PROCEDURE()
Sheets    LONG
SheetName STRING(30)
FileName  STRING(255)
!WorkBook  STRING(255)
    CODE
        !-----------------------------------------------------------------
        Sheets = Excel{'Sheets.Count'}
        !WriteDebug('SaveRRCSheet(' & Sheets & ')')

        IF Sheets < 1
            !WriteDebug('SaveRRCSheet(FAIL=IF Sheets < 1)')

            RETURN
        END !IF

        !WorkBook = Excel{'ActiveWorkbook.Name'}

        SheetName = Excel{'ActiveSheet.Name'}
                !-----------------------------------------------
                ! 23 Oct 2002 John
                ! R003, berrjo :Insert the report exe name EG VODR56.V01
                ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
                ! Make this standard on all reports sent for correction.
                ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
                !
                !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0057 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
                !FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
        !FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' VODR0053 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
        FileName  = CLIP(LOC:Path) &'\' & CLIP(SheetName) & ' VODR0053 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xlsx'
                !-----------------------------------------------


        Excel{'ActiveSheet.Copy'}

        Excel{'ActiveWorkbook.SaveAs("' & CLIP(FileName) & '")'}

        Excel{'ActiveWindow.Close'}
        !-----------------------------------------------------------------
UpdateEngineerQueue     PROCEDURE(IN:UserCode)
    CODE
        !-----------------------------------------------------------------
        SORT(EngineerQueue, +engq:UserCode)
        engq:UserCode  = IN:UserCode
        GET(EngineerQueue, +engq:UserCode)
        IF ERRORCODE()
            ! Not in queue - ADD
            IF LoadEngineer(IN:UserCode)
                engq:Forename   = use:Forename
                engq:Surname    = use:Surname
            ELSE
                engq:Forename   = 'UNALLOCATED JOB'
                engq:Surname    = ''
            END !IF
            engq:UserCode       = IN:UserCode
            engq:JobsBooked     = 0
            engq:SubJobsBooked   = 0
            ADD(EngineerQueue, +engq:UserCode)
        END !IF
        !-----------------------------------------------------------------
        ! In queue - AMEND
        engq:JobsBooked    += 1
        engq:SubJobsBooked += 1

        PUT(EngineerQueue, +engq:UserCode)
        !-----------------------------------------------------------------
UpdateSummaryQueue PROCEDURE( IN:Status, IN:StatusWorkingDays, IN:StatusWorkingHours, IN:StartTime, IN:EndTime )
WorkingDay LONG
    CODE
        !-----------------------------------------------------------------
        statusQ:Status  = IN:Status
        GET(Status_Queue, +statusQ:Status)
        CASE ERRORCODE()
        OF 00
            !-------------------------------------------------------------
            IF (statusQ:Status = IN:Status)
                !---------------------------------------------------------
                ! Found - NULL
                !---------------------------------------------------------
            ELSE
                !---------------------------------------------------------
                ! Partial Match - ADD
                CLEAR(Status_Queue)
                    statusQ:Status = IN:Status
                ADD(Status_Queue, +statusQ:Status)
                !---------------------------------------------------------
            END !IF
            !-------------------------------------------------------------
        OF 30
            !-------------------------------------------------------------
            ! NOT Found - ADD
            CLEAR(Status_Queue)
                statusQ:Status = IN:Status
            ADD(Status_Queue, +statusQ:Status)
            !-------------------------------------------------------------
        ELSE
            !-------------------------------------------------------------
            CancelPressed = True

            !WriteDebug('UpdateSummaryQueue() Error ' & ERRORCODE() & ', "' & CLIP(ERROR()) & '"')

             RETURN
            !-------------------------------------------------------------
        END !CASE
        !-----------------------------------------------------------------
        ! In queue - AMEND
        !
        statusQ:TotalJobsCount                      += 1
        statusQ:SubTotalJobsCount                   += 1
        !-----------------------------------------------------------------
        ! using ranges instead of exact Day values
        IF IN:StatusWorkingDays     <  01
            statusQ:LessThanDay                     += 1
            statusQ:SubLessThanDay                  += 1

        ELSIF  IN:StatusWorkingDays  > 60
            statusQ:Over60Days                      += 1
            statusQ:SubOver60Days                   += 1

        ELSIF  IN:StatusWorkingDays  > 30
            statusQ:Days[   Day60]                  += 1
            statusQ:SubDays[Day60]                  += 1

        ELSIF  IN:StatusWorkingDays  > 14
            statusQ:Days[   Day30]                  += 1
            statusQ:SubDays[Day30]                  += 1

        ELSIF  IN:StatusWorkingDays  > 10
            statusQ:Days[   Day14]                  += 1
            statusQ:SubDays[Day14]                  += 1

        ELSIF  IN:StatusWorkingDays  > 5
            statusQ:Days[   Day10]                  += 1
            statusQ:SubDays[Day10]                  += 1

        ELSIF  IN:StatusWorkingDays = 1
            !-------------------------------------------------------------
            WorkingDay  = (IN:EndTime - IN:StartTime) / (60 * 60 * 100)

            IF IN:StatusWorkingHours < WorkingDay
                statusQ:LessThanDay                 += 1
                statusQ:SubLessThanDay              += 1
            ELSE
                statusQ:Days[   1]                  += 1
                statusQ:SubDays[1]                  += 1
            END !IF
            !-------------------------------------------------------------
        ELSE ! IF  IN:StatusWorkingDays >= 1
            statusQ:Days[   IN:StatusWorkingDays]   += 1
            statusQ:SubDays[IN:StatusWorkingDays]   += 1
        END !IF

        PUT(Status_Queue, +statusQ:Status)
        !-----------------------------------------------------------------

!        ! MOORED: As per our previous correspondence and when we did the spec the days are incorrect.
!        !         Should be 0 to 5 then 10, 14, 30, 60 and more than 60 days.
!        statusQ:TotalJobsCount                      += 1
!        statusQ:SubTotalJobsCount                   += 1
!
!        IF IN:StatusWorkingDays     <  01
!            statusQ:LessThanDay                     += 1
!            statusQ:SubLessThanDay                  += 1
!
!        ELSIF  IN:StatusWorkingDays > 60
!            statusQ:Over60Days                      += 1
!            statusQ:SubOver60Days                   += 1
!
!        ELSIF  IN:StatusWorkingDays = 60
!            statusQ:Days[Day60]                     += 1
!            statusQ:SubDays[Day60]                  += 1
!
!        ELSIF  IN:StatusWorkingDays = 30
!            statusQ:Days[Day30]                     += 1
!            statusQ:SubDays[Day30]                  += 1
!
!        ELSIF  IN:StatusWorkingDays = 14
!            statusQ:Days[Day14]                     += 1
!            statusQ:SubDays[Day14]                  += 1
!
!        ELSIF  IN:StatusWorkingDays = 10
!            statusQ:Days[Day10]                     += 1
!            statusQ:SubDays[Day10]                  += 1
!
!        ELSIF  IN:StatusWorkingDays > 05
!            ! NULL
!
!        ELSIF  IN:StatusWorkingDays > 1
!            statusQ:Days[IN:StatusWorkingDays+1]       += 1
!            statusQ:SubDays[IN:StatusWorkingDays+1]    += 1
!
!        ELSIF  IN:StatusWorkingDays = 1
!            !-------------------------------------------------------------
!            WorkingDay  = (IN:EndTime - IN:StartTime) / (60 * 60 * 100)
!
!            IF IN:StatusWorkingHours < WorkingDay
!                statusQ:LessThanDay                 += 1
!                statusQ:SubLessThanDay              += 1
!            ELSE
!                statusQ:Days[1]                     += 1
!                statusQ:SubDays[1]                  += 1
!            END !IF
!            !-------------------------------------------------------------
!        END !IF

!==============================================================================
WorkingDaysBetween PROCEDURE  (StartDay, EndDay, countingSaturdays, countingSundays)   !,FullCount

FullCount               Long    !number of days including sat and sun between dates
CountToFirstSaturday    Long    !number of days between start and first saturday
CountFromLastSaturday   Long    !number of day between last Saturday and end date
WeeksCount              long    !how many full weeks in range
WeeksDays               long    !how many days relevant in WeeksCount

    Code

    FullCount = EndDay - StartDay

    If countingSaturdays and countingSundays then return(FullCount).

    !OK we have to do this the hard way - How many weeks and how many odd days before and after
    CountToFirstSaturday = 0
    loop
        !if this came in on a saturday it will record as zero. Max is six
        !if StartDay + countToFirstSaturday is A Saturday then break.
        if (StartDay + countToFirstSaturday) %7 = 6 then break.
        CountToFirstSaturday += 1
    END

    !was it finished before the first saturday?
    If countToFirstSaturday >= FullCount then return(FullCount).

    !how many weeks, and how many days left over
    !days left over first
    CountFromLastSaturday = (FullCount - CountToFirstSaturday) % 7

    !First two days left over are Saturday and Sunday 
    if Not countingSaturdays and countFromLastSaturday > 1 then countFromLastSaturday -= 1.
    if Not countingSundays   and countFromLastSaturday > 2 then countFromLastSaturday -= 1.

    !now we can calcualte the weeks from first to last saturday
    WeeksCount = int((FullCount - countToFirstSaturday - countFromLastSaturday) / 7)

    !counting Sat and counting Sun covered above
    If not countingSaturdays and not countingSundays then
        WeeksDays = WeeksCount * 5
    ELSE
        !only counting one of them, does not matter which
        WeeksDays = WeeksCount * 6
    END

    !reset the fullcount variable
    FullCount = countToFirstSaturday + WeeksDays + countFromLastSaturday
    Return(FullCount)

!==============================================================================





!------------------------------------------------------------------------------------------------------------------------------------
!WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
!
!Weeks        LONG
!DaysDiff     LONG
!Days         LONG(0)
!DaysPerWeek  LONG(5)
!TempDate1    DATE
!TempDate2    DATE
!
!    CODE
!        !-------------------------------------------
!        !WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
!        !-------------------------------------------
!        IF (IN:StartDate = 0)
!            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
!            RETURN 0
!        ELSIF (IN:EndDate = 0)
!            !WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
!            RETURN 0
!        ELSIF (IN:StartDate = IN:EndDate)
!            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
!            RETURN 0
!        ELSIF (IN:StartDate > IN:EndDate)
!            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
!            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
!        END !IF
!        !-------------------------------------------
!        IF IN:IncludeSaturday
!            DaysPerWeek += 1
!        END !IF
!
!        IF IN:IncludeSunday
!            DaysPerWeek += 1
!        END !IF
!        !-------------------------------------------
!        TempDate1 = IN:StartDate
!        
!        LOOP
!            If (TempDate1 = IN:EndDate) Then
!                !WriteDebug('WorkingDaysBetween(If (TempDate1 = IN:EndDate) Then)')
!                RETURN Days
!            End !If
!
!            CASE (TempDate1 % 7)
!            OF 0
!                If IN:IncludeSunday
!                    Days += 1
!                End !If
!            OF 6
!                If IN:IncludeSaturday
!                    Days += 1
!                End !If
!            ELSE
!                Days += 1
!            END !CASE
!
!            TempDate1 += 1
!
!        END !LOOP
!
!------------------------------------------------------------------------------------------------------------------------------------

!WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
!Weeks        LONG
!
!DaysDiff     LONG
!Days         LONG(0)
!
!DaysPerWeek  LONG(5)
!TempDate1    DATE
!TempDate2    DATE
!    CODE
!        !-------------------------------------------
!        !WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
!        !-------------------------------------------
!        IF (IN:StartDate = 0)
!            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
!            RETURN 0
!        ELSIF (IN:EndDate = 0)
!            !WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
!            RETURN 0
!        ELSIF (IN:StartDate = IN:EndDate)
!            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
!            RETURN 0
!        ELSIF (IN:StartDate > IN:EndDate)
!            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
!            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
!        END !IF
!        !-------------------------------------------
!        IF IN:IncludeSaturday
!            DaysPerWeek += 1
!        END !IF
!
!        IF IN:IncludeSunday
!            DaysPerWeek += 1
!        END !IF
!        !-------------------------------------------
!        TempDate1 = IN:StartDate
!        
!        LOOP
!            If (TempDate1 = IN:EndDate) Then
!                !WriteDebug('WorkingDaysBetween(If (TempDate1 = IN:EndDate) Then)')
!                RETURN Days
!            End !If
!
!            CASE (TempDate1 % 7)
!            OF 0
!                If IN:IncludeSunday
!                    Days += 1
!                End !If
!            OF 5
!                BREAK
!            OF 6
!                If IN:IncludeSaturday
!                    Days += 1
!                End !If
!            ELSE
!                Days += 1
!            END !CASE
!
!            TempDate1 += 1
!
!        END !LOOP
!        !-------------------------------------------
!        TempDate2     = IN:EndDate
!        
!        LOOP
!            If (TempDate1 = TempDate2) Then
!                !WriteDebug('WorkingDaysBetween(If (TempDate1 = TempDate2) Then)')
!                RETURN Days
!            End !If
!
!            CASE (TempDate2 % 7)
!            OF 0
!                If IN:IncludeSunday
!                    Days += 1
!                End !If
!            OF 5
!                BREAK
!            OF 6
!                If IN:IncludeSaturday
!                    Days += 1
!                End !If
!            ELSE
!                Days += 1
!            END !CASE
!
!            TempDate2 -= 1
!
!        END !LOOP
!        !-------------------------------------------
!        Weeks = (TempDate2 - TempDate1) / 7
!        !-------------------------------------------
!!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!!                ' ===================== <13,10>'                             & |
!!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )
!        !WriteDebug('WorkingDaysBetween(OK="' & Days + (Weeks * DaysPerWeek) & '")')
!        RETURN Days + (Weeks * DaysPerWeek)
!        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday) ! LONG
DaysBetween   LONG
WorkingHours  LONG
Hours         LONG
    CODE
        DaysBetween  = WorkingDaysBetween(StartDate, EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        WorkingHours = (EndTime - StartTime) / (60 * 60 * 100)
        Hours        = 0

        IF DaysBetween = 0
            Hours += HoursBetween(StartTime, EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(StartTime,            haQ:EndWorkHours) !def:End_Work_Hours)
            !Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
            Hours += HoursBetween(haQ:StartWorkHours,    EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * WorkingHours

            Hours += HoursBetween(StartTime,             haQ:EndWorkHours) ! def:End_Work_Hours)
            !Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
            Hours += HoursBetween(haQ:StartWorkHours,    EndTime           )

        END !IF

        RETURN Hours
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        !TB13253 - J - 21/03/14 - all excel work at Vodacom is beyond version 5
!        IF excel:OperatingSystem < 5
!            IF IN:StartNewRow
!                clip:Value = Temp
!            ELSE
!                clip:Value = CLIP(clip:Value) & '<09>' & Temp
!            END !IF
!
!            RETURN
!        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
!WriteDebug      PROCEDURE( IN:Message )
!    CODE
!        !-----------------------------------------------
!        !IF NOT debug:Active
!            RETURN
!        !END !IF
!
!        !debug:Count += 1
!
!        !PUTINI(CLIP(LOC:ProgramName) & ' '  & CLIP(tra_ali:Account_Number), debug:Count, IN:Message, 'C:\Debug.ini')
!        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 200
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
