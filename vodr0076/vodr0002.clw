

   MEMBER('vodr0076.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


ExchangeUnitsReport PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_aud_id          USHORT,AUTO
Save:Refnumber       USHORT
Save_Aud_ID2         USHORT
Safe:RefNumber       LONG
Save:RecordNumber    LONG
tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE()
LOC:StartDate        DATE
LOC:EndDate          DATE
                     END
SummaryQueue         QUEUE,PRE(sq)
StockType            STRING(30)
JobCount             LONG
                     END
SummaryQueue2        QUEUE,PRE(sq2)
StockType            STRING(30),NAME('sq2StockType')
Manufacturer         STRING(30),NAME('sq2Manufacturer')
ModelNumber          STRING(30),NAME('sq2ModelNumber')
JobCount             LONG,NAME('sq2JobCount')
                     END
Local_Group          GROUP,PRE(LOC)
ARCDateBooked        DATE
ARCLocation          STRING(30)
RRCLocation          STRING(30)
StatusReceivedAtRRC  STRING(30)
Account_Name         STRING(30)
ApplicationName      STRING('ServiceBase')
CommentText          STRING(255)
CompanyName          STRING(30)
CountAllJobs         LONG
CountJobs            LONG
DesktopPath          STRING(255)
ExchNumber           LONG
FileName             STRING(255)
JobNumber            LONG
JobsExtendedInSync   BYTE
LoanNumber           LONG
Path                 STRING(255)
ProgramName          STRING(50)
RepairCentreType     STRING(3)
SectionName          STRING(30)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(50)
Version              STRING('3.1.0000')
WorkingHours         DECIMAL(7,2)
                     END
Misc_Group           GROUP,PRE()
DoAll                BYTE(0)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalARCCompanyName  STRING(40)
ThirdCoName          STRING(40)
LocalTag             STRING(1)
LocalTimeOut         LONG
OPTION1              SHORT
RecordCount          LONG
WebJOB_OK            BYTE
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('D')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(40)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Sheet_Queue          QUEUE,PRE(shQ)
AccountNumber        STRING(15)
SheetName            STRING(30)
RecordCount          LONG
                     END
tmp:AllStockTypes    BYTE(0)
tmp:Tag              STRING(1)
tmp:AllRepairSites   BYTE(0)
tmp:Tag2             STRING(1)
X                    LONG
JobQueue             QUEUE,PRE(que)
JobNumber            LONG
OriginalIMEI         STRING(20)
OriginalMSN          STRING(20)
ExchangeIMEI         STRING(20)
ExchangeMSN          STRING(20)
DateOfSwap           DATE
ExchangeDetails      STRING(3)
ThirdPartyNumber     LONG
auditrecord_number   LONG
Manufacturer         STRING(30)
Model                STRING(30)
StockType            STRING(30)
                     END
IncludeManufacturer  STRING('Y')
BRW4::View:Browse    VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:Tag2               LIKE(tmp:Tag2)                 !List box control field - type derived from local data
tmp:Tag2_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Exchange Units Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Exchange Units Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Start Issue Date'),AT(255,114),USE(?LOC:StartDate:Prompt)
                           ENTRY(@d17),AT(331,114,64,10),USE(LOC:StartDate),LEFT,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(399,110),USE(?LookuipStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Issue Date'),AT(255,136),USE(?LOC:EndDate:Prompt)
                           ENTRY(@d17),AT(331,136,64,10),USE(LOC:EndDate),LEFT,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(399,132),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Select Exchange Stock Type'),AT(240,156),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select All'),AT(388,156),USE(tmp:AllStockTypes),MSG('Select All'),TIP('Select All'),VALUE('1','0')
                           CHECK('Include 3rd Party Exchanges'),AT(452,156),USE(IncludeManufacturer),VALUE('Y','N')
                           LIST,AT(240,170,200,186),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(181,210,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(181,231,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(452,212),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(452,250),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(452,288),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Select Repair Centre'),AT(220,108),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select All'),AT(412,108),USE(tmp:AllRepairSites),MSG('Select All'),TIP('Select All'),VALUE('1','0')
                           LIST,AT(220,122,244,234),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@65L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(173,218,1,1),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(165,250,1,1),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(476,200),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(476,238),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(476,276),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(240,366),USE(?VSBackButton),TRN,FLAT,HIDE,LEFT,ICON('backp.jpg')
                       BUTTON,AT(308,366),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,374),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(480,366),USE(?RunReport),TRN,FLAT,LEFT,ICON('printp.jpg')
                     END

Wizard6         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
    MAP
DateToString        PROCEDURE( DATE ), STRING
GetDeliveryDateAtARC        PROCEDURE( LONG ), LONG ! BOOL
HoursBetween                PROCEDURE  (TIME, TIME),LONG

NumberToColumn PROCEDURE( LONG ), STRING
SetSheetTo              PROCEDURE( STRING )
UpdateSummaryQueue  PROCEDURE( STRING )
WriteColumn PROCEDURE( STRING, LONG = False )
WriteDebug      PROCEDURE( STRING )

WorkingDaysBetween          PROCEDURE  (DATE, DATE, BYTE, BYTE), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME, LONG, LONG),LONG
    END !MAP
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = stp:Stock_Type
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = stp:Stock_Type
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = stp:Stock_Type
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::8:QUEUE = glo:Queue
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer = stp:Stock_Type
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = stp:Stock_Type
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW7.UpdateBuffer
   glo:Queue2.Pointer2 = tra:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:Tag2 = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:Tag2 = ''
  END
    Queue:Browse:1.tmp:Tag2 = tmp:Tag2
  IF (tmp:tag2 = '*')
    Queue:Browse:1.tmp:Tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:Tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::9:QUEUE = glo:Queue2
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue2)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer2 = tra:Account_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------------------------------------------
OKButtonPressed         ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! LOC:Version = '3.1.0001'
        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! DespatchToCustomer     = DESPATCHED
        !
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        LOC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !-----------------------------------------------------------------
        DO InitColumns
        !-----------------------------------------------------------------
        !WriteDebug('OKButtonPressed(Option1="' & Option1 & '"')

        !Go through audit trail for all occurances where an
        !exchange unit was issued -  (DBH: 14-08-2003)
        DO ExportSetup

    !          IF LOC:FileName = '' THEN CYCLE.
        DO ExportBody

        DO ExportFinalize

        If Command('/SCHEDULE')
            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                  '<13,10>Report Finished'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
        End

        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        debug:Active  = True
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName = '' THEN
            EXIT
        END !IF

!        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
!            LOC:FileName = CLIP(LOC:FileName) & '.xls'
!        END !IF
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        DO XL_Setup

        DO XL_AddWorkbook
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        FREE(SummaryQueue)
        CLEAR(SummaryQueue)

        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheets
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
CreateTitleSheet                ROUTINE !Creates Summary Sheet
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        !WriteDebug('CreateTitleSheet START')

        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:' & sheet:HeadLastCol & '").ColumnWidth'} = 20
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}

        !WriteDebug('CreateTitleSheet END')
        !-----------------------------------------------------------------
    EXIT
CreateDataSheets                ROUTINE !Creates Detail Sheets
    DATA
    CODE
        !---------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        !WriteDebug('CreateDataSheets(START Option1="' & Option1 & '"')
        !---------------------------------------------------------
        DO CreateDataSheet

        !WriteDebug('CreateDataSheets End')
        !---------------------------------------------------------
    EXIT

CreateDataSheet                                                      ROUTINE
    DATA
Save_trb_ID USHORT
Save_job_ID USHORT
Save_aud_ID2 USHORT
save:RefNUmber  Long
save:RecordNumber  Long
    CODE
        !---------------------------------------------------------
        !WriteDebug('CreateDataSheet(' & CLIP(LOC:CompanyName) & ')')
        !---------------------------------------------------------
        !Progress:Text    = CLIP(tra_ali:Account_Number) & '-' & CLIP(LOC:CompanyName)
        RecordsToProcess = (LOC:EndDate - LOC:StartDate) * 100

        DO ProgressBar_LoopPre
        !---------------------------------------------------------
        DO XL_AddSheet
            DO XL_SetWorksheetLandscape

        LOC:SectionName   = LEFT(CLIP(LOC:CompanyName), 30)
        IF CLIP(LOC:SectionName) = ''
            LOC:SectionName = 'Detailed'
        END !IF
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        LOC:CountJobs = 0
        !---------------------------------------------------------
        Do GenerateJobQueue
        !message('Back from GenerateJobQueue, records ='&clip(records(JobQueue)))

        !now all the detail is in the jobqueue - we just need to write it out
        Sort(JobQueue,que:DateOfSwap)


        Loop x= 1 to records(JobQueue)

            Get(jobQueue,x)
            if error() then break.

            Access:AUDIT.clearkey(aud:Record_Number_Key)
            aud:record_number = que:auditrecord_number
            if access:Audit.fetch(aud:Record_Number_Key) then cycle.

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = aud:Ref_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) then cycle.

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) then cycle.

            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) then cycle.

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) then cycle.

            !exchange unit details now held in the queue

            Do WriteColumns

        End !Loop through jobsqueue


        DO ProgressBar_LoopPost

        IF CancelPressed
            EXIT
        END !IF
        !---------------------------------------------------------
        RecordCount = LOC:CountJobs

        DO WriteDataSummary
        !---------------------------------------------------------
        !WriteDebug('CreateDataSheet END')
        !---------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        LOC:Text = CLIP(LOC:SectionName)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader               ROUTINE !***WORKSHEET HEADER***
    DATA
CurrentRow LONG(4)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16
!
        Excel{'Range("C3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Font.Size'} = 8
!
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.FormulaR1C1'}      = 'Start Batch Date:'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:StartDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'End Batch Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = FORMAT(TODAY(), @D8)

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow + 2
        sheet:DataHeaderRow  = CurrentRow + 4

        sheet:HeadSummaryRow = CurrentRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
InitColumns             ROUTINE  !***SETUP COLUMN NAMES***
    DATA
    CODE
        !-----------------------------------------------
        !Format
        !Short Number:  '###0'
        !Long Number:   '##########'
        !String:        Chr(64)
        !Date:          excel:DateFormat

        FREE(HeaderQueue)

        head:ColumnName       = 'Job Number'
            head:ColumnWidth  = 13.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'ARC Receive Date'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Sub Account Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Incoming IMEI Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Incoming MSN Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############' ! chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Incoming Make'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Incoming Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange IMEI Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange MSN Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############' ! chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange Make'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'User Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Despatched Date'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Consignment Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Job Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Warranty Charge Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Inv No/Billing'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Inv Amount'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !Two new columns Added TB114942 - JC 6/6/2012
        head:ColumnName       = 'Exchange Details'
            head:ColumnWidth  = 40.00       !needs to be bigger to get text in
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Location Where Swop Was Performed'  !columnName was made 40 char to take this
                                !"S W _O_ P"  spelling was part of the specification, I would have used swap :-)
            head:ColumnWidth  = 40.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
GenerateJobQueue        Routine

    !message('at start of generate job queue')
    !message('Records glo:queue ='&records(glo:Queue)&'|Glo:Queue2 = '&records(glo:Queue2))

!    loop x# = 1 to records(Glo:Queue)
!        get(glo:Queue,x#)
!        message('Stock ='&GLO:Pointer)
!    END !loop through stock queue
!
!    loop x# = 1 to records(glo:Queue2)
!        get(glo:queue2,x#)
!        message('Account ='&glo:Pointer2)
!    END


    !first get all the ARC/RRC ones
    Save_aud_ID = Access:AUDIT.SaveFile()
    Access:AUDIT.ClearKey(aud:ActionOnlyKey)
    aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
    aud:Date   = loc:StartDate
    Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
    Loop

        If Access:AUDIT.NEXT() then
            break
        END
        If aud:Action <> 'EXCHANGE UNIT ATTACHED TO JOB' then
            break
        END
        if aud:Date   > loc:EndDate then
            break
        END


        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = aud:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) then
            cycle
        END
        !Found

        If job:Exchange_Unit_Number = 0 then
            cycle
        END

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
            !If unit attached more than once, only count
            !the unit that is currently attached to job.
            !Check notes to see that the unit number matches - L937 (DBH: 02-09-2003)
            If ~Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),aud2:Notes,1,1)
                Cycle
            End !If ~Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),aud:Notes,1,1)

        END ! IF


        save:RefNumber = aud:Ref_Number

   

!        Save_aud_ID2 = Access:AUDIT.SaveFile()
!        Access:AUDIT.ClearKey(aud:TypeRefKey)
!        aud:Ref_Number = save:RefNumber
!        aud:Type       = 'EXC'
!        aud:Date       = Today()
!        Set(aud:TypeRefKey,aud:TypeRefKey)
!        Loop
!            If Access:AUDIT.NEXT()
!               Break
!            End !If
!            If aud:Ref_Number <> save:RefNumber      |
!            Or aud:Type       <> 'EXC'      |
!            Or aud:Date       > Today()      |
!                Then Break.  ! End If
!            If aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
!                save:RecordNumber = aud:Record_Number
!                Break
!            End !If aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
!        End !Loop
!        Access:AUDIT.RestoreFile(save_aud_ID2)
!
!        If aud:Record_Number <> save:RecordNumber
!            Cycle
!        End !If aud:RecordNumber <> save:RecordNumber

        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) then cycle.


        Access:Webjob.clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        if access:WEbjob.fetch(wob:RefNumberKey) then cycle.

        

        If ~tmp:AllStockTypes
            glo:Pointer = xch:Stock_Type
            Get(glo:Queue,glo:Pointer)
            If Error()
                !message('Not found stock type '&clip(xch:Stock_Type))
                Cycle
            End !If Error()
        End !If ~tmp:AllStockTypes

        If ~tmp:AllRepairSites
            glo:Pointer2 = wob:HeadAccountNumber
            Get(glo:Queue2,glo:Pointer2)
            If Error()
                !message('Not found account number '&clip(wob:HeadAccountNumber))
                Cycle
            End !If Error()
        End !If ~tmp:AllRepairSites

        !need job extention for exchanged at rrc
        Access:JOBSE.clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        if access:jobse.fetch(jobe:RefNumberKey)
            !message('error on fetch')
        END

        !now ready to add to the queue
        que:JobNumber = job:Ref_Number
        que:OriginalIMEI = job:ESN
        que:OriginalMSN  = job:MSN
        que:ExchangeIMEI = xch:ESN
        que:ExchangeMSN  = xch:MSN
        que:Manufacturer = xch:Manufacturer
        que:Model        = xch:Model_Number
        que:StockType    = xch:Stock_Type
        que:DateOfSwap = aud:Date
        If jobe:ExchangedATRRC
            que:ExchangeDetails = 'RRC'
        Else ! If jobe:ExchangedATRRC
            que:ExchangeDetails = 'ARC'
        End ! If jobe:ExchangedATRRC
        que:ThirdPartyNumber = 0
        que:auditrecord_number = aud:Record_number
        Add(JobQueue)
        


    END !loop through audit

    !message('BEFORE the third party have records = '&clip(records(JobQueue)))

    !now get all the manufacturer's exchanges
    if IncludeManufacturer = 'Y' then
        Access:AUDIT.ClearKey(aud:ActionOnlyKey)
        aud:Action = '3RD PARTY AGENT: UNIT RETURNED'
        aud:Date   = loc:StartDate
        Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
        Loop
            If Access:AUDIT.NEXT() then break.
            If aud:Action <> '3RD PARTY AGENT: UNIT RETURNED' then break.
            if aud:Date   > loc:EndDate then break.

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = aud:Ref_Number
            If Access:JOBS.TryFetch(job:Ref_Number_Key) then break.

            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

            END ! IF

            If Instring('NEW I.M.E.I. NO.: ' & Clip(job:ESN),aud2:Notes,1,1)
                Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                Loop ! Begin Loop
                    If Access:JOBTHIRD.Next() then break.
                    If jot:RefNumber <> job:Ref_Number then break.

                    If jot:InIMEI = job:ESN
                        que:JobNumber = job:Ref_Number
                        Get(JobQueue,que:JobNumber)

                        If Error()
                            que:JobNumber = job:Ref_Number
                            que:OriginalIMEI = jot:OriginalIMEI
                            que:ExchangeIMEI     = jot:InIMEI
                            que:ExchangeMSN  = jot:InMSN
                            que:OriginalMSN = jot:OriginalMSN
                            que:DateOfSwap = aud:Date
                            que:ExchangeDetails = 'MAN'
                            que:ThirdPartyNumber = jot:ThirdPartyNumber
                            que:auditrecord_number = aud:record_number
                            que:Manufacturer  = job:Manufacturer
                            que:Model         = job:Model_Number
                            que:StockType     = 'MANUFACTURER'
                            
                            Add(JobQueue)
                            !message('Adding new MAN details')
                        Else ! If Error()
                            If que:ExchangeDetails = ''
                                If aud:Date >= que:DateOfSwap
                                    que:JobNumber = job:Ref_Number
                                    que:OriginalIMEI = jot:OriginalIMEI
                                    que:ExchangeIMEI     = jot:InIMEI
                                    que:OriginalMSN = jot:OriginalMSN
                                    que:ExchangeMSN  = jot:InMSN
                                    que:DateOfSwap = aud:Date
                                    que:ExchangeDetails = 'MAN'
                                    que:ThirdPartyNumber = jot:ThirdPartyNumber
                                    que:auditrecord_number = aud:record_number
                                    que:Manufacturer  = job:Manufacturer
                                    que:Model         = job:Model_Number
                                    que:StockType     = 'MANUFACTURER'
                                    Put(JobQueue)
                                    !message('Ammending to MAN details')
                                End ! If aud:Date >= que:DateOfSwap
                            Else ! If que:ExchangeDetails = ''
                                ! This job has already been picked up, but was for an exchange. Add New Entry (DBH: 02/08/2006)
                                If aud:Date >= que:DateOfSwap
                                    que:JobNumber = job:Ref_Number
                                    que:OriginalIMEI = jot:OriginalIMEI
                                    que:OriginalMSN  = jot:OriginalMSN
                                    que:ExchangeIMEI     = jot:InIMEI
                                    que:ExchangeMSN  = jot:InMSN
                                    que:DateOfSwap = aud:Date
                                    que:ExchangeDetails = 'MAN'
                                    que:ThirdPartyNumber = jot:ThirdPartyNumber
                                    que:auditrecord_number = aud:record_number
                                    que:Manufacturer  = job:Manufacturer
                                    que:Model         = job:Model_Number
                                    que:StockType     = 'MANUFACTURER'

                                    Add(JobQueue)
                                    !message('Adding second MAN details')
                                End ! If aud:Date >= que:DateOfSwap
                            End ! If que:ExchangeDetails = ''
                        End ! If Error()
                        
                        Break
                    End ! If jot:InIMEI = job:ESN
                End ! Loop

            End ! If Instring('NEW I.M.E.I. NO.: ' & Clip(job:ESN),aud:Notes,1,1)
        End !Loop
    END !if IncludeManufacturer = 'Y' then

    !message('At end of third party have records = '&clip(records(JobQueue)))


    EXIT
WriteColumns                ROUTINE  !***WRITE INTO COLUMNS from the job queue***
    DATA
StatusWorkingDays LONG
    CODE
        !Leading Zero: WriteColumns('''' & sto:Part_Number)

        !-----------------------------------------------
        !WriteDebug('WriteColumns(' & trb:Ref_Number & ')')

        DO ProgressBar_Loop

        IF CancelPressed
            EXIT
        END !IF
        !===============================================
        !WriteDebug('WriteColumns(' & trb:Ref_Number & '), Found')

        RecordCount      += 1
        !-----------------------------------------------
        LOC:CountJobs    += 1
        LOC:CountAllJobs += 1

        !Job Number
        WriteColumn(job:Ref_Number)
        !Franchise Branch Number
        WriteColumn(tra:BranchIdentification)
        !ARC Receive Date
        If wob:HeadAccountNumber = LocalHeadAccount
            WriteColumn(DateToString(job:Date_Booked))
        Else !If wob:HeadAccountNumber = LocalHeadAccount
            !Date received at ARC -  (DBH: 14-08-2003)
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber   = job:Ref_Number
            lot:NewLocation = loc:ARCLocation
            If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                !Found
                WriteColumn(DateToString(lot:TheDate))
            Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                !Error
                WriteColumn(DateToString(job:Date_Booked))
            End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
        End !If wob:HeadAccountNumber = LocalHeadAccount
        !Sub Account Number
        WriteColumn(job:Account_Number)
        !Check to see if unit exchanged by Third Party - 3335 (DBH: 01-10-2003)
        !Incoming IMEI/MSN
        IMEIError# = 0
        ThirdCoName = ''
        if que:ThirdPartyNumber > 0 then
        
            Access:TRDBATCH.ClearKey(trb:RecordNumberKey)
            trb:RecordNumber = que:ThirdPartyNumber
            If Access:TRDBATCH.TryFetch(trb:RecordNumberKey) = Level:Benign
                !Found
                ThirdCoName   = Clip(trb:Company_Name)
            Else ! If Access:TRDBATCH.TryFetch(trb:RecordNumberKey) = Level:Benign
                !Error
                ThirdCoName  = Clip(job:Third_Party_Site)
            End ! If Access:TRDBATCH.TryFetch(trb:RecordNumberKey) = Level:Benign

        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        
        WriteColumn(que:OriginalIMEI)
        WriteColumn(que:OriginalMSN)


        !Incoming Make
        WriteColumn(job:Manufacturer)
        !Incoming Model
        WriteColumn(job:Model_Number)

        !exchange imei etc are set on the queue
        WriteColumn(que:ExchangeIMEI)
        WriteColumn(que:ExchangeMSN)
        writeColumn(que:Manufacturer)
        writeColumn(que:Model)

        !User Name
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code   = aud:User
        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Found
            WriteColumn(Clip(use:Forename) & ' ' & Clip(use:Surname))
        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Error
            WriteColumn(aud:User)
        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Despatched Date
        !Use the status change date - L937 (DBH: 04-09-2003)
        Access:AUDSTATS.ClearKey(aus:NewStatusKey)
        aus:RefNumber = job:Ref_Number
        aus:Type      = 'EXC'
        aus:NewStatus = '458 EXCH DESPATCHED TO RRC'
        If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
            !Found
            WriteColumn(DateToString(aus:DateChanged))
        Else !If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
            !Error
            Access:AUDSTATS.ClearKey(aus:NewStatusKey)
            aus:RefNumber = job:Ref_Number
            aus:Type      = 'EXC'
            aus:NewStatus = '901 DESPATCHED'
            If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                !Found
                WriteColumn(DateToString(aus:DateChanged))
            Else !If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                !Error
                WriteColumn('')
            End !If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign

        End !If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
        !Consignment Number
        WriteColumn(job:Exchange_Consignment_Number)
        !Exchange Type
        WriteColumn(que:StockType)            !xch:Stock_Type)
        !Job Type
        If job:Chargeable_Job = 'YES'
            If job:Warranty_Job = 'YES'
                WriteColumn('CHARGEABLE/WARRANTY')
            Else !If job:Warranty_Job = 'YES'
                WriteColumn('CHARGEABLE')
            End !If job:Warranty_Job = 'YES'
        Else !If job:Chargeable_Job = 'YES'
            If job:Warranty_job = 'YES'
                WriteColumn('WARRANTY')
            Else !If job:Warranty_job = 'YES'
                WriteColumn('?')
            End !If job:Warranty_job = 'YES'
        End !If job:Chargeable_Job = 'YES'
        !Warranty Charge Type
        If job:Warranty_Job = 'YES'
            WriteColumn(job:Warranty_Charge_Type)
        Else !If job:Warranty_Job = 'YES'
            WriteColumn('')
        End !If job:Warranty_Job = 'YES'

        !Inv No / Billing
        If job:Chargeable_Job = 'YES'
            WriteColumn(job:Invoice_Number)
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number  = job:Invoice_Number
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                WriteColumn(Format(job:Invoice_Sub_Total + (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                    (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                                    (job:Invoice_Courier_Cost * inv:Vat_rate_Labour/100),@n_14.2))
            Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                WriteColumn('')
                !WriteColumn('')    !removed JC 06/06/12 - causing an extra column
            End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        Else !If job:Chargeable_Job = 'YES'
            WriteColumn('')
            WriteColumn('')
        End !If job:Chargeable_Job = 'YES'

        !Added TB114942 - JC 6/6/2012
        Case que:ExchangeDetails
            of 'MAN'
                WriteColumn('Manufacturer Exchange Unit Attached')
                WriteColumn(clip(ThirdCoName))
            of 'RRC'
                WriteColumn('RRC Exchange Unit Attached')     !Exchange details
                WriteColumn(Clip(tra:Company_Name))           !Swap place
            of 'ARC'
                WriteColumn('ARC Exchange Unit Attached')
                WriteColumn(Clip(LocalARCCompanyName))
        END !case


!end of each line
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
!        !-----------------------------------------------

    !_____________________________________________________________________

    !Summary
    sq:StockType    = que:StockType     !xch:Stock_Type
    Get(SummaryQueue,sq:StockType)
    If Error()
        sq:StockType = que:StockType    !xch:Stock_Type
        sq:JobCount = 1
        Add(SummaryQueue)
    Else !If Error()
        sq:JobCount += 1
        Put(SummaryQueue)
    End !If Error()

    sq2:StockType    = que:StockType    !xch:StocK_Type
    sq2:Manufacturer = job:Manufacturer
    sq2:ModelNumber  = job:Model_Number
    Get(SummaryQueue2,'sq2StockType,sq2Manufacturer,sq2ModelNumber')
    If Error()
        sq2:StockType   = que:StockType     !xch:Stock_Type
        sq2:Manufacturer    = job:Manufacturer
        sq2:ModelNumber = job:Model_Number
        sq2:JobCount = 1
        Add(SummaryQueue2)
    Else !If Error()
        sq2:JobCount += 1
        Put(SummaryQueue2)
    End !If Error()
    EXIT
!-----------------------------------------------------------------------------------
WriteDataSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------      
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        IF LOC:CountAllJobs < 1
            Excel{'ActiveCell.Formula'} = 'No Units Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
WriteSummary                                        ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

TitleRow     LONG
FirstRow     LONG
LastRow      LONG
TotalRow     LONG
    CODE
        !-----------------------------------------------------------------
        SORT(SummaryQueue, +sq:StockType)
        ResultsCount = RECORDS(SummaryQueue)
        !WriteDebug('WriteSummary(' & ResultsCount  & ')')
        !-----------------------------------------------------------------
        TitleRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + ResultsCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & TitleRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Exchange Type'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Count'
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}          = 'No Units Found'

            DO XL_RowDown
            DO XL_RowDown

!            EXIT
        END !IF

        LOOP QueueIndex = 1 TO RECORDS(SummaryQueue)
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = sq:StockType
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = sq:JobCount

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        ! Totals
        Excel{'Range("A' & TotalRow & ':' & sheet:HeadLastCol & TotalRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Total'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = '=SUM(B' & FirstRow & ':B' & LastRow & ')'

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------

    !_____________________________________________________________________
        Sort(SummaryQueue2, '+sq2StockType,sq2Manufacturer,sq2ModelNumber')
        ResultsCount = RECORDS(SummaryQueue2)

        !-----------------------------------------------------------------
        TitleRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + ResultsCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & TitleRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Exchange Type'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Make Of Unit'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Model Of Unit'
            Excel{'ActiveCell.Offset(0, 3).Formula'}          = 'Count'
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}          = 'No Units Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF

        LOOP QueueIndex = 1 TO RECORDS(SummaryQueue2)
            !-------------------------------------------------------------
            GET(SummaryQueue2, QueueIndex)
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = sq2:StockType
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = sq2:Manufacturer
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = sq2:ModelNumber
            Excel{'ActiveCell.Offset(0, 3).Formula'}          = sq2:JobCount

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        ! Totals
        Excel{'Range("A' & TotalRow & ':' & sheet:HeadLastCol & TotalRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Total'
            Excel{'ActiveCell.Offset(0, 3).Formula'}          = '=SUM(D' & FirstRow & ':D' & LastRow & ')'

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT

ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        If Command('/SCHEDULE')
            Exit
        End ! If Command('/SCHEDULE')
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Unable to find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName             ROUTINE !***SET EXCEL FILENAME***
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE

    excel:ProgramName = 'Exchanged Units Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

!    If Exists(excel:FileName & '.xls')
!        Remove(excel:FileName & '.xls')
!        If Error()
!            Beep(Beep:SystemHand)  ;  Yield()
!            Case Missive('Cannot get access to the selected document:'&|
!                '|' & Clip(excel:FileName) & ''&|
!                '|'&|
!                '|Ensure the file is not in use and try again.','ServiceBase',|
!                           'mstop.jpg','/&OK')
!                Of 1 ! &OK Button
!            End!Case Message
!            Exit
!        End !If Error()
!    End !If Exists(excel:FileName)
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                !MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & 'Exchanged Units Report VODR0076 ' & FORMAT(TODAY(), @D12) & ' .xls'
!        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        !?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF 0{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------------------------------------------
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE  !**SET REPORT TITLE**
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

!        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020622'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeUnitsReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:REPSCHAC.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBS.UseFile
  Access:LOCATLOG.UseFile
  Access:INVOICE.UseFile
  Access:JOBSE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBTHIRD.UseFile
  Access:REPSCHCR.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  Access:REPSCHST.UseFile
  Access:TRDBATCH.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = LocalHeadAccount
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      LocalARCCompanyName = tra:Company_Name
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:STOCKTYP,SELF)
  BRW7.Init(?List:2,Queue:Browse:1.ViewPosition,BRW7::View:Browse,Queue:Browse:1,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      !** SET REPORT NAME**
      LOC:ProgramName       = 'Exchanged Units Report'  ! Job=2234    Cust=Vodacom
      0{PROP:Text} = CLIP(LOC:ProgramName)
      !?Tab1{PROP:Text}      = CLIP(LOC:ProgramName) & ' Criteria'
  
      excel:Visible = False
      debug:Active  = False
  
      LOC:EndDate        = TODAY()
      LOC:StartDate      = DATE( MONTH(LOC:EndDate), 1, YEAR(LOC:EndDate) )
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
  
      !SELECT(?Option1:Radio1)
      !Option1 = 1
  
      IF GUIMode = 1 THEN
         LocalTimeOut             = 500
         DoAll                    = 'Y'
         0{PROP:ICONIZE} = TRUE
      END !IF
  
  
  
      DISPLAY
  
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard6.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?RunReport, |                    ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,stp:Use_Exchange_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,stp:Use_Exchange,1,BRW4)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:Tag,BRW4.Q.tmp:Tag)
  BRW4.AddField(stp:Stock_Type,BRW4.Q.stp:Stock_Type)
  BRW4.AddField(stp:Use_Exchange,BRW4.Q.stp:Use_Exchange)
  BRW7.Q &= Queue:Browse:1
  BRW7.AddSortOrder(,tra:Account_Number_Key)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,tra:Account_Number,1,BRW7)
  BIND('tmp:Tag2',tmp:Tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:Tag2,BRW7.Q.tmp:Tag2)
  BRW7.AddField(tra:Account_Number,BRW7.Q.tra:Account_Number)
  BRW7.AddField(tra:Company_Name,BRW7.Q.tra:Company_Name)
  BRW7.AddField(tra:RecordNumber,BRW7.Q.tra:RecordNumber)
  IF ?tmp:AllStockTypes{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllStockTypes{Prop:Checked} = False
    ENABLE(?List)
  END
  IF ?tmp:AllRepairSites{Prop:Checked} = True
    DISABLE(?List:2)
  END
  IF ?tmp:AllRepairSites{Prop:Checked} = False
    ENABLE(?List:2)
  END
  BRW4.AddToolbarTarget(Toolbar)
  BRW7.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              If rpc:AllStockTypes
                  tmp:AllStockTypes = 1
              Else ! If rpc:AllStockTypes
                  Access:REPSCHST.Clearkey(rpk:StockTypeKey)
                  rpk:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpk:StockTypeKey,rpk:StockTypeKey)
                  Loop ! Begin Loop
                      If Access:REPSCHST.Next()
                          Break
                      End ! If Access:REPSCHST.Next()
                      If rpk:REPSCHCRREcordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpk:REPSCHCRREcordNumber <> rpc:RecordNumber
                      glo:Pointer = rpk:StockType
                      Add(glo:Queue)
                  End ! Loop
              End ! If rpc:AllStockTypes
  
              If rpc:AllAccounts
                  tmp:AllRepairSites = 1
              Else ! If rpc:AllAccounts
                  Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
                  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
                  Loop ! Begin Loop
                      If Access:REPSCHAC.Next()
                          Break
                      End ! If Access:REPSCHAC.Next()
                      If rpa:REPSCHCRREcordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpa:REPSCHCRREcordNumber <> rpc:RecordNumber
                      glo:Pointer2 = rpa:AccountNumber
                      Add(glo:Queue2)
                  End ! Loop
              End ! If rpc:AllAccounts
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  loc:StartDate = Today()
                  loc:EndDate = Today()
              Of 2 ! 1st Of Month
                  loc:StartDate = Date(Month(Today()),1,Year(Today()))
                  loc:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  loc:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  loc:StartDate = Date(Month(loc:EndDate),1,Year(loc:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do OKButtonPressed
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:REPSCHAC.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard6.Validate()
        DISABLE(Wizard6.NextControl())
     ELSE
        ENABLE(Wizard6.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020622'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020622'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020622'&'0')
      ***
    OF ?LookuipStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:AllStockTypes
      IF ?tmp:AllStockTypes{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllStockTypes{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllRepairSites
      IF ?tmp:AllRepairSites{Prop:Checked} = True
        DISABLE(?List:2)
      END
      IF ?tmp:AllRepairSites{Prop:Checked} = False
        ENABLE(?List:2)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard6.TakeAccepted()
      ! Bodge to get around the "standard" windows behaviour of disabled buttons (DBH: 16-05-2005)
      If ?VSBackButton{prop:Disable} = True
          ?VSBackButton{prop:Hide} = True
      Else ! ?VSBackButton{prop:Disable} = True
          ?VSBackButton{prop:Hide} = False
      End ! ?VSBackButton{prop:Disable} = True
      If ?VSNextButton{prop:Disable} = True
          ?VSNextButton{prop:Hide} = True
      Else ! ?VSNextButton{prop:Disable} = True
          ?VSNextButton{prop:Hide} = False
      End ! ?VSNextButton{prop:Disable} = True
    OF ?VSNextButton
      ThisWindow.Update
         Wizard6.TakeAccepted()
      ! Bodge to get around the "standard" windows behaviour of disabled buttons (DBH: 16-05-2005)
      If ?VSBackButton{prop:Disable} = True
          ?VSBackButton{prop:Hide} = True
      Else ! ?VSBackButton{prop:Disable} = True
          ?VSBackButton{prop:Hide} = False
      End ! ?VSBackButton{prop:Disable} = True
      
      If ?VSNextButton{prop:Disable} = True
          ?VSNextButton{prop:Hide} = True
      Else ! ?VSNextButton{prop:Disable} = True
          ?VSNextButton{prop:Hide} = False
      End ! ?VSNextButton{prop:Disable} = True
    OF ?RunReport
      ThisWindow.Update
      do OKButtonPressed
      
      
      
      
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookuipStartDate)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
    ! DateKey                  KEY( lot:RefNumber,   lot:TheDate     ),DUP,NOCASE
    ! NewLocationKey           KEY( lot:RefNumber,   lot:NewLocation ),DUP,NOCASE
    ! RecordNumberKey          KEY( lot:RecordNumber                 ),NOCASE,PRIMARY

    SentToARC# = False
    LOC:ARCDateBooked = ''
    Access:LOCATLOG.Clearkey(lot:DateKey)
    lot:RefNumber = in:JOBNumber
    Set(lot:DateKey,lot:DateKey)
    Loop 
        If Access:LOCATLOG.Next()
            Break
        End !
        If lot:RefNumber <> in:JobNumber 
            Break
        End !
        If lot:NewLocation = loc:ARCLocation
            SentToARC# = True
            loc:ARCDateBooked = lot:TheDate
            !MESSAGE('TRUE CONDITION MET')
        Break
        End!    
      
    End !Loop

    Return SentToARC#



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        !-----------------------------------------------------------------
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-----------------------------------------------------------------
SetSheetTo              PROCEDURE( IN:SectionName )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        CLEAR(Sheet_Queue)
            shQ:AccountNumber = IN:SectionName
        GET(Sheet_Queue, +shQ:AccountNumber)

        CASE ERRORCODE()
        OF 00
            IF shQ:AccountNumber = IN:SectionName
                ! Found
                Excel{'Sheets("' & CLIP(shQ:SheetName) & '").Select'}

                RETURN
            ELSE
                ! NOT Found, Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------
        IF DoADD = True
            DO XL_AddSheet
            DO XL_SetWorksheetLandscape

            LOC:SectionName   = LEFT(CLIP(IN:SectionName), 30)
            IF CLIP(LOC:SectionName) = ''
                LOC:SectionName = 'Detailed'
            END !IF
            excel:CommentText = ''
            DO CreateSectionHeader

            !DO SetColumns

            CLEAR(Sheet_Queue)
                shQ:AccountNumber = IN:SectionName
                shQ:SheetName     = Excel{'ActiveSheet.Name'}
            ADD(Sheet_Queue, +shQ:AccountNumber)

            RETURN
        END !IF
        !-----------------------------------------------
!        DO XL_AddSheet
!            DO XL_SetWorksheetLandscape
!
!        LOC:SectionName   = LEFT(CLIP(LOC:CompanyName), 30)
!        IF CLIP(LOC:SectionName) = ''
!            LOC:SectionName = 'Detailed'
!        END !IF
!        excel:CommentText = ''
!        sheet:TempLastCol = sheet:DataLastCol
!
!        DO CreateSectionHeader
WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
    CODE
        !-------------------------------------------
        !WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
        !-------------------------------------------
        IF (IN:StartDate = 0)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
            RETURN 0
        ELSIF (IN:EndDate = 0)
            !WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
            RETURN 0
        ELSIF (IN:StartDate = IN:EndDate)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
            RETURN 0
        ELSIF (IN:StartDate > IN:EndDate)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        IF IN:IncludeSaturday
            DaysPerWeek += 1
        END !IF

        IF IN:IncludeSunday
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        
        LOOP
            If (TempDate1 = IN:EndDate) Then
                !WriteDebug('WorkingDaysBetween(If (TempDate1 = IN:EndDate) Then)')
                RETURN Days
            End !If

            CASE (TempDate1 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1

        END !LOOP
        !-------------------------------------------
        TempDate2     = IN:EndDate
        
        LOOP
            If (TempDate1 = TempDate2) Then
                !WriteDebug('WorkingDaysBetween(If (TempDate1 = TempDate2) Then)')
                RETURN Days
            End !If

            CASE (TempDate2 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 -= 1

        END !LOOP
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7
        !-------------------------------------------
!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!                ' ===================== <13,10>'                             & |
!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )
        !WriteDebug('WorkingDaysBetween(OK="' & Days + (Weeks * DaysPerWeek) & '")')
        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday) ! LONG
DaysBetween   LONG
Hours         LONG
    CODE
        DaysBetween = WorkingDaysBetween(StartDate, EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        Hours       = 0

        IF DaysBetween = 0
            Hours += HoursBetween(StartTime, EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * LOC:WorkingHours

            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        END !IF

        RETURN Hours
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
UpdateSummaryQueue  PROCEDURE( IN:CompanyName )
    CODE
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
!-----------------------------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stp:Stock_Type
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stp:Stock_Type
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:Tag2 = ''
    ELSE
      tmp:Tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag2 = '*')
    SELF.Q.tmp:Tag2_Icon = 2
  ELSE
    SELF.Q.tmp:Tag2_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  If tra:Account_Number <> LocalHeadAccount
      If tra:RemoteRepairCentre <> 1
          Return Record:Filtered
      Else !If tra:RemoteRepairCentre <> 1
          If tra:Account_Number = 'XXXRRC'
              Return Record:Filtered
          End !If tra:Account = 'XXXRRC'
      End !If tra:RemoteRepairCentre <> 1
  End !tra:Account_Number <> LocalHeadAccount
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

Wizard6.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW7.Q &= NULL) ! Has Browse Object been initialized?
       BRW7.ResetQueue(Reset:Queue)
    END

Wizard6.TakeBackEmbed PROCEDURE
   CODE

Wizard6.TakeNextEmbed PROCEDURE
   CODE

Wizard6.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
