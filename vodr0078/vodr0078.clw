   PROGRAM


OfficeInside:TemplateVersion equate('2.83')
_No_OfficeInside_       equate(0)
CapesoftMessageBox:TemplateVersion equate('2.17')
FileExplorer:TemplateVersion equate('4.02')
NetTalk:TemplateVersion equate('5.51')
ActivateNetTalk   EQUATE(1)
  include('NetCrit.inc'),once
  include('NetMap.inc'),once
  include('NetAll.inc'),once
  include('NetTalk.inc'),once
  include('NetSimp.inc'),once
  include('NetHttp.inc'),once
  include('NetWww.inc'),once
  include('NetWeb.inc'),once
  include('NetEmail.inc'),once
  include('NetFile.inc'),once
  include('NetWebSms.inc'),once
INCLUDE('VodacomDeclarations.inc')
    INCLUDE('VodacomClass.inc','Declarations')
INCLUDE('GlobalFileNames.inc')

   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
INCLUDE('csoffice.inc'),ONCE
INCLUDE('oiABPROI.INC'),ONCE
!--- Define Special Folder Constants --- After Global INCLUDE's --------------------
CSIDL_ADMINTOOLS                EQUATE(30H)    ! {user name}\Start Menu\Programs\Administrative Tools
CSIDL_ALTSTARTUP                EQUATE(1DH)    ! non localized startup
CSIDL_APPDATA                   EQUATE(26)     ! Application Data Folder
CSIDL_BITBUCKET                 EQUATE(0AH)    ! {desktop}\Recycle Bin
CSIDL_COMMON_ADMINTOOLS         EQUATE(2FH)    ! All Users\Start Menu\Programs\Administrative Tools
CSIDL_COMMON_ALTSTARTUP         EQUATE(1EH)    ! non localized common startup
CSIDL_COMMON_APPDATA            EQUATE(23H)    ! All Users\Application Data
CSIDL_COMMON_DESKTOPDIRECTORY   EQUATE(25)     ! Common Desktop Folder
CSIDL_COMMON_DOCUMENTS          EQUATE(2EH)    ! All Users\Documents
CSIDL_COMMON_FAVORITES          EQUATE(31)     ! Common Favorites Folder
CSIDL_COMMON_PROGRAMS           EQUATE(23)     ! Common Program Groups Folder
CSIDL_COMMON_STARTMENU          EQUATE(22)     ! Common Start Menu Folder
CSIDL_COMMON_STARTUP            EQUATE(24)     ! Common Startup Group Folder
CSIDL_COMMON_TEMPLATES          EQUATE(2DH)    ! All Users\Templates
CSIDL_CONTROLS                  EQUATE(3H)     ! My Computer\Control Panel
CSIDL_COOKIES                   EQUATE(33)     ! Cookies Folder
CSIDL_DESKTOP                   EQUATE(0H)     ! {desktop}
CSIDL_DESKTOPDIRECTORY          EQUATE(16)     ! Desktop Folder <-----------------------
CSIDL_DRIVES                    EQUATE(11H)    ! My Computer
CSIDL_FAVORITES                 EQUATE( 6)     ! Favorites Folder
CSIDL_FLAG_CREATE               EQUATE(8000H)  ! combine with CSIDL_ value to force create on SHGetSpecialFolderLocation()
CSIDL_FLAG_DONT_VERIFY          EQUATE(4000H)  ! combine with CSIDL_ value to force create on SHGetSpecialFolderLocation()
CSIDL_FLAG_MASK                 EQUATE(0FF00H) ! mask for all possible flag values
CSIDL_FONTS                     EQUATE(14H)    ! windows\fonts
CSIDL_HISTORY                   EQUATE(34)     ! History Folder
CSIDL_INTERNET                  EQUATE(1H)     ! Internet Explorer (icon on desktop)
CSIDL_INTERNET_CACHE            EQUATE(32)     ! Temp. Internet Files Folder
CSIDL_LOCAL_APPDATA             EQUATE(1CH)    ! {user name}\Local Settings\Application Data (non roaming)
CSIDL_MYPICTURES                EQUATE(27H)    ! C:\Program Files\My Pictures
CSIDL_NETHOOD                   EQUATE(19)     ! Network Neighborhood Folder
CSIDL_PERSONAL                  EQUATE( 5)     ! (My Documents) Personal Documents Folder
CSIDL_PRINTERS                  EQUATE(4H)     ! My Computer\Printers
CSIDL_PRINTHOOD                 EQUATE(27)     ! Printers Folder
CSIDL_PROFILE                   EQUATE(28H)    ! USERPROFILE
CSIDL_PROGRAM_FILES             EQUATE(26H)    ! C:\Program Files
CSIDL_PROGRAM_FILES_COMMON      EQUATE(2BH)    ! C:\Program Files\Common
CSIDL_PROGRAM_FILES_COMMONX86   EQUATE(2CH)    ! x86 Program Files\Common on RISC
CSIDL_PROGRAM_FILESX86          EQUATE(2AH)    ! x86 C:\Program Files on RISC
CSIDL_PROGRAMS                  EQUATE( 2)     ! Program Groups Folder
CSIDL_RECENT                    EQUATE( 8)     ! Recently Used Documents Folder
CSIDL_SENDTO                    EQUATE( 9)     ! Send To Folder
CSIDL_STARTMENU                 EQUATE(11)     ! Start Menu Folder
CSIDL_STARTUP                   EQUATE( 7)     ! Startup Group Folder
CSIDL_SYSTEM                    EQUATE(25H)    ! GetSystemDirectory()
CSIDL_SYSTEMX86                 EQUATE(29H)    ! x86 system directory on RISC
CSIDL_TEMPLATES                 EQUATE(21)     ! Document Templates Folder
CSIDL_WINDOWS                   EQUATE(24H)    ! GetWindowsDirectory()
!-----------------------------------------------------------------------------------

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

   INCLUDE('FE.INC'),ONCE
  include('cwsynchc.inc'),once  ! added by NetTalk

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('VODR0BC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
     MODULE('FileExplorerDLL.Lib')
fe_ClassVersion        PROCEDURE(byte Flag=0),string,name('fe_ClassVersion'),DLL(dll_mode)
feDispose              PROCEDURE(),name('feDispose'),DLL(dll_mode)
fe_OnlineStatus        PROCEDURE(byte pOption=0),byte,name('fe_OnlineStatus'),DLL(dll_mode)
fe_OnlineStatusConnectionName PROCEDURE(byte pOption=0),string,name('fe_OnlineStatusConnectionName'),DLL(dll_mode)
fe_StartIE             PROCEDURE(string pURL),name('fe_StartIE'),DLL(dll_mode)
fe_DebugString         PROCEDURE(string DbgMessage),name('fe_DebugString'),DLL(dll_mode)
fe_GetRegValue         PROCEDURE(string ParentFolder, string NameOfKey, string NameOfValue),string,name('fe_GetRegValue'),DLL(dll_mode)
fe_GetOSVersionInfo    PROCEDURE(),string,name('fe_GetOSVersionInfo'),DLL(dll_mode)
fe_GetIEVersionInfo    PROCEDURE(byte pTranslate=0),string,name('fe_GetIEVersionInfo'),DLL(dll_mode)
fe_ShellExecute        PROCEDURE(string pParm1),name('fe_ShellExecute'),DLL(dll_mode)
fe_SetFocus            PROCEDURE(long pHandle),name('fe_SetFocus'),DLL(dll_mode)
fe_MCIPlay             PROCEDURE(string pFileName),byte,proc,name('fe_MCIPlay'),DLL(dll_mode)
fe_MCIStop             PROCEDURE(<string pFileName>),byte,proc,name('fe_MCIStop'),DLL(dll_mode)
fe_ShowWindow          PROCEDURE(byte pOption=0),name('fe_ShowWindow'),DLL(dll_mode)
     END
     MODULE('OfficeInsideDLL.Lib')
oi_WordAvailable       PROCEDURE(),byte,name('oi_WordAvailable'),DLL(dll_mode)
oi_OpenDoc             PROCEDURE(string),byte,proc,name('oi_OpenDoc'),DLL(dll_mode)
oi_GetOSVersionInfo    PROCEDURE(),string,name('oi_GetOSVersionInfo'),DLL(dll_mode)
oi_UnloadCOM           PROCEDURE(byte pOption),byte,proc,name('oi_UnloadCOM'),DLL(dll_mode)
oi_Version             PROCEDURE(byte pOption=0),string,name('oi_Version'),DLL(dll_mode)
oi_SysTempDir          PROCEDURE(),string,name('oi_SysTempDir'),DLL(dll_mode)
oi_SetForegroundWindow PROCEDURE(string WindowTitle),name('oi_SetForegroundWindow'),DLL(dll_mode)
oi_SetFocusWindow      PROCEDURE(string WindowTitle),name('oi_SetFocusWindow'),DLL(dll_mode)
oi_DebugMessage        PROCEDURE(string Parm1),name('oi_DebugMessage'),DLL(dll_mode)
oi_GetWorkAreaWidth    PROCEDURE(),long,name('oi_GetWorkAreaWidth'),DLL(dll_mode)
oi_GetWorkAreaHeight   PROCEDURE(),long,name('oi_GetWorkAreaHeight'),DLL(dll_mode)
oi_GetRegValue         PROCEDURE(string ParentFolder, string NameOfKey, string NameOfValue),string,name('oi_GetRegValue'),DLL(dll_mode)
oi_SetRegValue         PROCEDURE(string ParentFolder, string NameOfKey, string NameOfValue, string NewValue),string,proc,name('oi_SetRegValue'),DLL(dll_mode)
oi_ChangeDefaultShowMessageIcon PROCEDURE(string pIconName),name('oi_ChangeDefaultShowMessageIcon'),DLL(dll_mode)
oi_SendEmail           PROCEDURE(string pToAddress, string pCCAddress, string pBCCAddress, string pSubject, string pAttachments, string pBody, byte pPlainText, byte pOpen=false),byte,proc,name('oi_SendEmail'),DLL(dll_mode)
oi_ClarionVersion      PROCEDURE(),string,name('oi_ClarionVersion'),DLL(dll_mode)
oi_GetFileVersionInfo  PROCEDURE(string pFileName),string,name('oi_GetFileVersionInfo'),DLL(dll_mode)
oi_LongToHex           PROCEDURE(long pLong),string,name('oi_LongToHex'),DLL(dll_mode)
oi_HexToLong           PROCEDURE(string pHex),long,name('oi_HexToLong'),DLL(dll_mode)
oi_RGBToBGRLong        PROCEDURE(long pR, long pG, long pB),long,name('oi_RGBToBGRLong'),DLL(dll_mode)
oi_RGBLongToBGRLong    PROCEDURE(long pRGB),long,name('oi_RGBLongToBGRLong'),DLL(dll_mode)
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('VODR0002.CLW')
RRC_TATReport          PROCEDURE   !Version Number: 5024
ds_Stop                PROCEDURE(<string StopText>)   !
ds_Halt                PROCEDURE(UNSIGNED Level=0,<STRING HaltText>)   !
ds_Message             FUNCTION(STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE),UNSIGNED,PROC   !
HelpBrowser            PROCEDURE(String)   !Doesn't need a SRN, as this *IS* the help screen
     END
         Module('winapi')
             DBHGGetExitCodeProcess(unsigned, ulong),signed,pascal,NAME('GetExitCodeProcess')
             DBHGCreateProcess(ulong, ulong, ulong, ulong, signed, ulong, ulong, ulong, ulong, ulong ),signed,raw,pascal,name('CreateProcessA')
             DBHGSleep(ulong),pascal,raw,name('Sleep')
             DBHGetTempPathA(ULONG,*Cstring),Ulong,PASCAL,RAW,NAME('GetTempPathA')
         End
         MODULE('WINAPI')
             GetDesktopWindowDBHFT(),UNSIGNED,PASCAL,NAME('GetDesktopWindow')
         END
         MODULE('SHELL32.LIB')
             SHGetSpecialFolderPathDBHFT(UNSIGNED,*CSTRING,LONG,LONG)LONG,PASCAL,RAW,PROC,NAME('SHGetSpecialFolderPathA')
         END
         INCLUDE('clib.clw'),ONCE
     
         Module('winapi')
             GetDesktopWindow(), Unsigned, Pascal
             GetTempPathA(ULONG,*Cstring),Ulong,PASCAL,RAW
         End !MODULE
         MODULE('shell32.LIB')
             SHGetSpecialFolderPath(             Unsigned,        *CString,           long,            long)long, Pascal, Raw, proc, NAME('SHGetSpecialFolderPathA')
         END !MODULE
         INCLUDE('clib.clw'),ONCE
     
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
     INCLUDE('VodacomGlobalMaps.inc')
   END

glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:DateModify       DATE
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:WebJob           BYTE(0)
glo:ARCAccountNumber STRING(30)
glo:ARCSiteLocation  STRING(30)
glo:FaultCodeGroup   GROUP,PRE(glo)
FaultCode1             STRING(30)
FaultCode2             STRING(30)
FaultCode3             STRING(30)
FaultCode4             STRING(30)
FaultCode5             STRING(30)
FaultCode6             STRING(30)
FaultCode7             STRING(30)
FaultCode8             STRING(30)
FaultCode9             STRING(30)
FaultCode10            STRING(255)
FaultCode11            STRING(255)
FaultCode12            STRING(255)
FaultCode13            STRING(30)
FaultCode14            STRING(30)
FaultCode15            STRING(30)
FaultCode16            STRING(30)
FaultCode17            STRING(30)
FaultCode18            STRING(30)
FaultCode19            STRING(30)
FaultCode20            STRING(30)
                     END
glo:IPAddress        STRING(30)
glo:HostName         STRING(60)
glo:ReportName       STRING(255)
glo:ExportReport     BYTE(0)
glo:EstimateReportName STRING(255)
glo:ReportCopies     BYTE
glo:Vetting          BYTE(0)
glo:TagFile          STRING(255)
glo:sbo_outfaultparts STRING(255)
glo:UserAccountNumber STRING(30)
glo:UserCode         STRING(3)
glo:Location         STRING(30)
glo:EDI_Reason       STRING(60)
glo:Insert_Global    STRING(3)
glo:RelocateStore    BYTE
glo:RelocatedFrom    STRING(30)
glo:RelocatedMark_Up REAL
glo:Sbo_outparts     STRING(255)
glo:sbo_GenericFile  STRING(255)
glo:ExportToCSV      STRING(1)
glo:ExportFile       CSTRING(255)
glo:ARCExportFile    CSTRING(255)
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

BHQFileList         QUEUE,TYPE
qFilename               STRING(255)
                    END
    Map
BHStripReplace           Procedure(String func:String,String func:Strip,String func:Replace),String ! Replace characters
BHStripNonAlphaNum           Procedure(String func:String,String func:Replace),String ! Replace characters between ASCII 32 and 124
BHStripForFilename           Procedure(String func:String),String ! Remove illegal character in a filename
!!!<summary>
!!!Allow user to remove all character apart from Numbers, Uppercase Letters and Spaces
!!!</summary>
!!!<param name="func:String">String to remove characters from</param>
!!!<param name="fExclusions">String of characters NOT to remove from the string</param>
BHStripAlphaNumOnly          Procedure(STRING func:String,<STRING fExclusions>),STRING ! Remove non character/numbers
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5Days   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5DaysMonday   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart),Long
BHGetFileFromPath			Procedure(String fullPath),String
BHGetPathFromFile			Procedure(String fullPath,<Byte removeSlash>),String
BHGetFileNoExtension        Procedure(String fullPath),String
BHPutFileIntoQueue          Procedure(STRING fullPath,*bhQFileList Q)
BHAddBackSlash              Procedure(STRING fullPath),STRING

!!! <summary>
!!! Check If File Is In User
!!! </summary>
!!! <param name="fFilename">Name Of File</param>
!!! <param name="fKeyName">Optional: Check file in use by using specific key</param>
!!! <param name="fNoRelease">Optional: Do NOT release the file after the check has been made</param>
!!! <param name="fShowMessage">Optional: Display a "file in use" error message</param>
!!! <param name="fMessageTest">Optional: Description Text to display in above message
BHFileInUse                 Procedure(FILE ffilename,<KEY fKeyName>,BYTE fNoRelease = 0, BYTE fShowMessage = 0,<STRING fMessageText>),BYTE
BHGetTempFolder             Procedure(),STRING ! Return Temp Folder including backslash

!!! <summary>
!!! Return Personal Documents Folder
!!! </summary>
!!! <param name="fSubFolder1">Optional: Create sub folder with variable name</param>
!!! <param name="fSubFolder2">Optional: Create sub folder with variable name</param>
!!! <param name="fSubFolder3">Optional: Create sub folder with variable name</param>
BHGetDocumentsFolder        PROCEDURE(<STRING fSubFolder1>,<STRING fSubFolder2>,<STRING fSubFolder3>),STRING ! Return Documents Folder
BHRunDOS                    Procedure(String f:Command,<Byte f:Wait>,<Byte f:NoTimeOut>),Long
    End
STDCHRGE             FILE,DRIVER('Btrieve'),NAME('STDCHRGE.DAT'),PRE(sta),CREATE,BINDABLE,THREAD
Model_Number_Charge_Key  KEY(sta:Model_Number,sta:Charge_Type,sta:Unit_Type,sta:Repair_Type),NOCASE,PRIMARY
Charge_Type_Key          KEY(sta:Model_Number,sta:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(sta:Model_Number,sta:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(sta:Model_Number,sta:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(sta:Model_Number,sta:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(sta:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(sta:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(sta:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
WarrantyClaimRate           REAL
HandlingFee                 REAL
Exchange                    REAL
RRCRate                     REAL
                         END
                     END                       

EXCHPREP             FILE,DRIVER('Btrieve'),OEM,PRE(EAP),CREATE,BINDABLE,THREAD
LeyRefNumber             KEY(EAP:RefNumber),NOCASE,PRIMARY
KeyAudit                 KEY(EAP:Audit_Number),DUP,NOCASE
KeyAuditUser             KEY(EAP:Audit_Number,EAP:User_Code),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Audit_Number                LONG
IMEI                        STRING(20)
User_Code                   STRING(3)
Method                      STRING(10)
                         END
                     END                       

EVO_HEAD             FILE,DRIVER('Btrieve'),OEM,PRE(EVOH),CREATE,BINDABLE,THREAD
KeyRecord_number         KEY(EVOH:RECORD_NO),NOCASE,PRIMARY
KeySentToEVO             KEY(EVOH:Sent_to_EVO_Flag),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NO                   LONG
HEADER_TXT                  STRING(25)
COMP_CODE                   STRING(4)
DOC_DATE                    STRING(8)
PSTNG_DATE                  STRING(8)
REF_DOC_NO                  STRING(16)
XREF1_HD                    STRING(240)
XREF2_HD                    STRING(240)
Sent_to_EVO_Flag            STRING(1)
                         END
                     END                       

STOAUUSE             FILE,DRIVER('Btrieve'),NAME('STOAUUSE.DAT'),PRE(STOU),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(STOU:Record_No),NOCASE,PRIMARY
KeyAuditTypeNo           KEY(STOU:AuditType,STOU:Audit_No),DUP,NOCASE
KeyAuditTypeNoUser       KEY(STOU:AuditType,STOU:Audit_No,STOU:User_code),DUP,NOCASE
KeyTypeNoPart            KEY(STOU:AuditType,STOU:Audit_No,STOU:StockPartNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
AuditType                   STRING(1)
Audit_No                    LONG
User_code                   STRING(3)
Current                     STRING(1)
StockPartNumber             STRING(30)
                         END
                     END                       

PREJOB               FILE,DRIVER('Btrieve'),OEM,NAME('PREJOB.DAT'),PRE(PRE),CREATE,BINDABLE,THREAD
KeyRefNumber             KEY(PRE:RefNumber),NOCASE,PRIMARY
KeyAPIRecordNo           KEY(PRE:APIRecordNumber),DUP,NOCASE
KeyJobRef_Number         KEY(PRE:JobRef_number),DUP,NOCASE
KeyESN                   KEY(PRE:ESN),DUP,NOCASE
KeyDateTime              KEY(PRE:Date_booked,PRE:Time_Booked),DUP,NOCASE
KeyESNDateTime           KEY(PRE:ESN,PRE:Date_booked,PRE:Time_Booked),DUP,NOCASE
KeyPortalRef             KEY(PRE:VodacomPortalRef),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
APIRecordNumber             LONG
JobRef_number               LONG
VodacomPortalRef            STRING(30)
Title                       STRING(5)
Initial                     STRING(1)
Surname                     STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Suburb                      STRING(30)
Region                      STRING(30)
Telephone_number            STRING(15)
Mobile_number               STRING(15)
ID_number                   STRING(30)
ESN                         STRING(20)
Manufacturer                STRING(30)
Model_number                STRING(30)
DOP                         DATE
FaultText                   STRING(200)
RepairOutletRef             LONG
Date_booked                 DATE
Time_Booked                 TIME
ChargeType                  STRING(30)
                         END
                     END                       

APITOKEN             FILE,DRIVER('Btrieve'),OEM,PRE(API),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(API:RecordNumber),NOCASE,PRIMARY
KeyToken                 KEY(API:Token),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Token                       STRING(20)
UserName                    STRING(50)
JobRef_number               LONG
PreJobRefNo                 LONG
LastCall                    STRING(30)
EntryDate                   DATE
EntryTime                   TIME
                         END
                     END                       

PRIBAND              FILE,DRIVER('Btrieve'),OEM,NAME('PRIBAND.DAT'),PRE(prb),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(prb:RecordNumber),NOCASE,PRIMARY
KeyBandName              KEY(prb:BandName),NOCASE
KeyMinPrice              KEY(prb:MinPrice),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BandName                    STRING(30)
MinPrice                    REAL
MaxPrice                    REAL
                         END
                     END                       

TRADEAC2             FILE,DRIVER('Btrieve'),OEM,PRE(TRA2),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(TRA2:RecordNo),NOCASE,PRIMARY
KeyAccountNumber         KEY(TRA2:Account_Number),NOCASE
KeyPreBookRegion         KEY(TRA2:Pre_Book_Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Account_Number              STRING(15)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
UseSparesRequest            STRING(3)
Pre_Book_Region             STRING(30)
SBOnlineDespatch            BYTE
SBOnlineStock               BYTE
SBOnlineQAEtc               BYTE
SBOnlineWarrClaims          BYTE
SBOnlineWaybills            BYTE
SBOnlineTradeAccs           BYTE
SBOnlineEngineer            BYTE
SBOnlineAudits              BYTE
SBOnlineUsers               BYTE
SBOnlineLoansExch           BYTE
SBOnlineBouncers            BYTE
SBOnlineReports             BYTE
SBOnlineSpare1              BYTE
SBOnlineSpare2              BYTE
SBOnlineSpare3              BYTE
                         END
                     END                       

AUDIT2               FILE,DRIVER('Btrieve'),NAME('AUDIT2.DAT'),PRE(aud2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aud2:RecordNumber),NOCASE,PRIMARY
AUDRecordNumberKey       KEY(aud2:AUDRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AUDRecordNumber             LONG
Notes                       STRING(255)
                         END
                     END                       

SMSRECVD             FILE,DRIVER('Btrieve'),OEM,NAME('SMSRECVD.DAT'),PRE(SMR),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(SMR:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMR:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMR:Job_Ref_Number,-SMR:DateReceived,-SMR:TimeReceived),DUP,NOCASE
KeyAccount_Date_Time     KEY(SMR:AccountNumber,SMR:DateReceived,SMR:TextReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
AccountNumber               STRING(15)
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
SMSType                     STRING(1)
                         END
                     END                       

TRAHUBAC             FILE,DRIVER('Btrieve'),OEM,NAME('TRAHUBAC.DAT'),PRE(TRA1),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(TRA1:RecordNo),NOCASE,PRIMARY
HeadAccSubAccKey         KEY(TRA1:HeadAcc,TRA1:SubAcc),DUP,NOCASE
HeadAccSubAccNameKey     KEY(TRA1:HeadAcc,TRA1:SubAccName),DUP,NOCASE
HeadAccSubAccBranchKey   KEY(TRA1:HeadAcc,TRA1:SubAccBranch),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
HeadAcc                     STRING(15)
SubAcc                      STRING(15)
SubAccName                  STRING(30)
SubAccBranch                STRING(30)
                         END
                     END                       

JOBSLOCK             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSLOCK.DAT'),PRE(lock),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lock:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(lock:JobNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
DateLocked                  DATE
TimeLocked                  TIME
SessionValue                LONG
                         END
                     END                       

ASVACC               FILE,DRIVER('Btrieve'),OEM,NAME('ASVACC.DAT'),PRE(ASV),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(ASV:RecordNo),NOCASE,PRIMARY
TradeACCManufKey         KEY(ASV:TradeAccNo,ASV:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
TradeAccNo                  STRING(20)
Manufacturer                STRING(30)
ASVCode                     STRING(30)
                         END
                     END                       

TagFile              FILE,DRIVER('TOPSPEED'),RECLAIM,OEM,NAME(glo:TagFile),PRE(tag),CREATE,BINDABLE,THREAD
keyTagged                KEY(tag:sessionID,tag:taggedValue),DUP,NOCASE,OPT
keyID                    KEY(tag:taggedValue,tag:sessionID),DUP,NOCASE,OPT
Record                   RECORD,PRE()
sessionID                   LONG
taggedValue                 STRING(30)
tagged                      BYTE
                         END
                     END                       

VETTREAS             FILE,DRIVER('Btrieve'),OEM,NAME('VETTREAS.DAT'),PRE(vet),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(vet:RecordNumber),NOCASE,PRIMARY
VettingReasonKey         KEY(vet:VettingReason),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
VettingReason               STRING(100)
                         END
                     END                       

ErrorVCPJob          FILE,DRIVER('Btrieve'),OEM,NAME('ErrorVCPJob.DAT'),PRE(err),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
RecordID                    LONG
DateOfError                 DATE
ErrorDesc                   STRING(250)
ErrorRecord                 STRING(3600)
                         END
                     END                       

SRNTEXT              FILE,DRIVER('Btrieve'),OEM,NAME('SRNTEXT.DAT'),PRE(srn),CREATE,BINDABLE,THREAD
KeyRefNo                 KEY(srn:RefNo),NOCASE,PRIMARY
KeySRN                   KEY(srn:SRNumber),NOCASE
Record                   RECORD,PRE()
RefNo                       LONG
SRNumber                    STRING(7)
ScreenSize                  STRING(1)
TipText                     STRING(255)
                         END
                     END                       

EXCEXCH              FILE,DRIVER('Btrieve'),OEM,NAME('EXCEXCH.DAT'),PRE(eix1),CREATE,BINDABLE,THREAD
WIPEXCKey                KEY(eix1:WIPEXCID),NOCASE,PRIMARY
StatusTypeKey            KEY(eix1:Location,eix1:Status),DUP,NOCASE
Record                   RECORD,PRE()
WIPEXCID                    LONG
Location                    STRING(30)
Status                      STRING(30)
NoIMEI                      BYTE
                         END
                     END                       

GRNOTES              FILE,DRIVER('Btrieve'),OEM,NAME('GRNOTES.DAT'),PRE(grn),CREATE,BINDABLE,THREAD
Goods_Received_Number_Key KEY(grn:Goods_Received_Number),NOCASE,PRIMARY
Order_Number_Key         KEY(grn:Order_Number,grn:Goods_Received_Date),DUP,NOCASE
Date_Recd_Key            KEY(grn:Goods_Received_Date),DUP,NOCASE
NotPrintedGRNKey         KEY(grn:BatchRunNotPrinted,grn:Goods_Received_Number),DUP,NOCASE
NotPrintedOrderKey       KEY(grn:BatchRunNotPrinted,grn:Order_Number,grn:Goods_Received_Number),DUP,NOCASE
KeyEVO_Status            KEY(grn:EVO_Status),DUP,NOCASE
Record                   RECORD,PRE()
Goods_Received_Number       LONG
Order_Number                LONG
Goods_Received_Date         DATE
CurrencyCode                STRING(30)
DailyRate                   REAL
DivideMultiply              STRING(1)
BatchRunNotPrinted          BYTE
Uncaptured                  BYTE
EVO_Status                  STRING(1)
                         END
                     END                       

STOFAULT             FILE,DRIVER('Btrieve'),OEM,NAME('STOFAULT.DAT'),PRE(stf),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stf:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(stf:PartNumber),DUP,NOCASE
DescriptionKey           KEY(stf:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
PurchaseCost                REAL
ModelNumber                 STRING(30)
IMEI                        STRING(30)
Engineer                    STRING(3)
StoreUserCode               STRING(3)
TheDate                     DATE
TheTime                     TIME
PartType                    STRING(3)
                         END
                     END                       

JOBOUTFL             FILE,DRIVER('Btrieve'),OEM,NAME('JOBOUTFL.DAT'),PRE(joo),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joo:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(joo:JobNumber,joo:FaultCode),DUP,NOCASE
LevelKey                 KEY(joo:JobNumber,joo:Level),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
FaultCode                   STRING(30)
Description                 STRING(255)
Level                       LONG
                         END
                     END                       

ACCSTAT              FILE,DRIVER('Btrieve'),OEM,NAME('ACCSTAT.DAT'),PRE(acs),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(acs:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(acs:AccessArea,acs:Status),DUP,NOCASE
StatusOnlyKey            KEY(acs:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccessArea                  STRING(30)
Status                      STRING(30)
                         END
                     END                       

QAPARTSTEMP          FILE,DRIVER('Btrieve'),OEM,NAME(glo:FileName),PRE(qap),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(qap:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(qap:PartNumber),DUP,NOCASE
DescriptionKey           KEY(qap:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PartRecordNumber            LONG
PartType                    STRING(3)
                         END
                     END                       

EXCHAMF              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAMF.DAT'),PRE(emf),CREATE,BINDABLE,THREAD
Audit_Number_Key         KEY(emf:Audit_Number),NOCASE,PRIMARY
Secondary_Key            KEY(emf:Complete_Flag,emf:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Date                        DATE
Time                        LONG
User                        STRING(3)
Status                      STRING(30)
Site_location               STRING(30)
Complete_Flag               BYTE
                         END
                     END                       

AUDITE               FILE,DRIVER('Btrieve'),OEM,NAME('AUDITE.DAT'),PRE(aude),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aude:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(aude:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IPAddress                   STRING(30)
HostName                    STRING(60)
                         END
                     END                       

AUDVCPMS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDVCPMS.MKD'),PRE(avms),BINDABLE,THREAD
Audit_Number_Key         KEY(avms:Audit_Number),DUP,NOCASE
StockType_Audit_Key      KEY(avms:Stock_Type,avms:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Stock_Type                  STRING(30)
Audit_Date                  DATE
                         END
                     END                       

EXCHAUI              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAUI.DAT'),PRE(eau),CREATE,BINDABLE,THREAD
Internal_No_Key          KEY(eau:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(eau:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(eau:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(eau:Audit_Number,eau:Exists),DUP,NOCASE
AuditIMEIKey             KEY(eau:Audit_Number,eau:New_IMEI),DUP,NOCASE
Locate_IMEI_Key          KEY(eau:Audit_Number,eau:Site_Location,eau:IMEI_Number),DUP,NOCASE
Main_Browse_Key          KEY(eau:Audit_Number,eau:Confirmed,eau:Site_Location,eau:Stock_Type,eau:Shelf_Location,eau:Internal_No),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Shelf_Location              STRING(30)
Location                    STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
Exists                      BYTE
New_IMEI                    BYTE
IMEI_Number                 STRING(20)
Confirmed                   BYTE
Stock_Type                  STRING(30)
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSENG.DAT'),PRE(joe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE
UserCodeStatusKey        KEY(joe:UserCode,joe:StatusDate,joe:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
UserCode                    STRING(3)
DateAllocated               DATE
TimeAllocated               STRING(20)
AllocatedBy                 STRING(3)
EngSkillLevel               LONG
JobSkillLevel               STRING(30)
Status                      STRING(30)
StatusDate                  DATE
StatusTime                  TIME
                         END
                     END                       

MULDESPJ             FILE,DRIVER('Btrieve'),OEM,NAME('MULDESPJ.DAT'),PRE(mulj),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mulj:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(mulj:RefNumber,mulj:JobNumber),DUP,NOCASE
CurrentJobKey            KEY(mulj:RefNumber,mulj:Current,mulj:JobNumber),DUP,NOCASE
JobNumberOnlyKey         KEY(mulj:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobNumber                   LONG
IMEINumber                  STRING(30)
MSN                         STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
Current                     BYTE
SecurityPackNumber          STRING(30)
                         END
                     END                       

MULDESP              FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(muld),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(muld:RecordNumber),NOCASE,PRIMARY
BatchNumberKey           KEY(muld:BatchNumber),DUP,NOCASE
AccountNumberKey         KEY(muld:AccountNumber),DUP,NOCASE
CourierKey               KEY(muld:Courier),DUP,NOCASE
HeadBatchNumberKey       KEY(muld:HeadAccountNumber,muld:BatchNumber),DUP,NOCASE
HeadAccountKey           KEY(muld:HeadAccountNumber,muld:AccountNumber),DUP,NOCASE
HeadCourierKey           KEY(muld:HeadAccountNumber,muld:Courier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BatchNumber                 STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
BatchTotal                  LONG
BatchType                   STRING(3)
HeadAccountNumber           STRING(30)
                         END
                     END                       

STOCKLOG             FILE,DRIVER('Btrieve'),OEM,NAME('STOCKLOG.DAT'),PRE(sal),CREATE,BINDABLE,THREAD
Audit_Number_Key         KEY(sal:Audit_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Audit_Number                LONG
Date_created                DATE
Time_Created                TIME
User                        STRING(20)
Checked_Date                DATE
Checked_Time                TIME
Checked_By                  STRING(3)
Status                      STRING(10)
Scanned_Count               LONG
Stock_Status                STRING(10)
                         END
                     END                       

IMEILOG              FILE,DRIVER('Btrieve'),OEM,NAME('IMEILOG.DAT'),PRE(ime),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ime:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
Audit_Number                LONG
IMEI_Number                 STRING(30)
Stock_Type                  STRING(30)
Status_Code                 STRING(6)
Model_No                    STRING(30)
Manufacturer                STRING(30)
                         END
                     END                       

KEXPORT              FILE,DRIVER('ASCII'),OEM,NAME(glo:filename),PRE(kex),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
field1                      STRING(2000)
                         END
                     END                       

SWAPIMEI             FILE,DRIVER('Btrieve'),OEM,NAME('SWAPIMEI.DAT'),PRE(swp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(swp:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 STRING(255)
                         END
                     END                       

JOBBOUNCER           FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:FileName),PRE(jobb),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobb:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(jobb:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
AccountName                 STRING(30)
Booked                      DATE
Completed                   DATE
ChargeType                  STRING(30)
TheDays                     LONG
Type                        STRING(1)
                         END
                     END                       

RTNAWAIT             FILE,DRIVER('Btrieve'),OEM,NAME('RTNAWAIT.DAT'),PRE(rta),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rta:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(rta:Archive,rta:Processed,rta:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(rta:Archive,rta:Processed,rta:Description),DUP,NOCASE
ArcProExcPartKey         KEY(rta:Archive,rta:Processed,rta:ExchangeOrder,rta:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(rta:Archive,rta:Processed,rta:ExchangeOrder,rta:Description),DUP,NOCASE
ArcProCNRKey             KEY(rta:Archive,rta:Processed,rta:CNRRecordNumber),DUP,NOCASE
ArcProExcCNRKey          KEY(rta:Archive,rta:Processed,rta:ExchangeOrder,rta:CNRRecordNumber),DUP,NOCASE
RTNORDERKey              KEY(rta:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(rta:CNRRecordNumber),DUP,NOCASE
GRNNumberKey             KEY(rta:GRNNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
Location                    STRING(30)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
NewRefNumber                LONG
GRNNumber                   LONG
                         END
                     END                       

STMASAUD             FILE,DRIVER('Btrieve'),NAME('STMASAUD.DAT'),PRE(stom),CREATE,BINDABLE,THREAD
AutoIncrement_Key        KEY(-stom:Audit_No),NOCASE,PRIMARY
Compeleted_Key           KEY(stom:Complete),DUP,NOCASE
Sent_Key                 KEY(stom:Send),DUP,NOCASE
Record                   RECORD,PRE()
Audit_No                    LONG
branch                      STRING(30)
branch_id                   LONG
Audit_Date                  LONG
Audit_Time                  LONG
End_Date                    LONG
End_Time                    LONG
Audit_User                  STRING(3)
Complete                    STRING(1)
Send                        STRING(1)
CompleteType                STRING(1)
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTATS.DAT'),PRE(aus),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE
StatusTimeKey            KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged),DUP,NOCASE
StatusDateKey            KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged),DUP,NOCASE
StatusDateRecordKey      KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE
StatusTypeRecordKey      KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE
RefRecordNumberKey       KEY(aus:RefNumber,aus:RecordNumber),DUP,NOCASE
RefDateRecordKey         KEY(aus:RefNumber,aus:DateChanged,aus:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 TIME
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

KIMPORT              FILE,DRIVER('BASIC'),OEM,NAME(glo:filename),PRE(kim),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
field1                      STRING(255)
field2                      STRING(255)
field3                      STRING(255)
field4                      STRING(255)
field5                      STRING(255)
                         END
                     END                       

AUDVCPLN             FILE,DRIVER('Btrieve'),OEM,NAME('AUDVCPLN.MKD'),PRE(avcp),BINDABLE,THREAD
Audit_Number_Key         KEY(avcp:Audit_Number),DUP,NOCASE
StockType_Audit_Key      KEY(avcp:Stock_Type,avcp:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Stock_Type                  STRING(30)
Ref_Number                  LONG
Audit_Date                  DATE
Audit_Option                STRING(15)
                         END
                     END                       

MANSAMP              FILE,DRIVER('Btrieve'),OEM,NAME('MANSAMP.DAT'),PRE(msp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(msp:RecordNumber),NOCASE,PRIMARY
PartPrefixKey            KEY(msp:PartNumberPrefix,msp:LabType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartNumberPrefix            STRING(30)
LabType                     STRING(30)
                         END
                     END                       

REPMAILP             FILE,DRIVER('Btrieve'),OEM,PRE(rpp),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(rpp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(rpp:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
Parameter_Name              STRING(50)
Type                        STRING(20)
Value                       STRING(255)
                         END
                     END                       

RAPIDLST             FILE,DRIVER('Btrieve'),OEM,NAME('RAPIDLST.DAT'),PRE(RAP),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(RAP:RecordNumber),NOCASE,PRIMARY
OrderKey                 KEY(RAP:RapidOrder,RAP:ProgramName),DUP,NOCASE
NormalOrderKey           KEY(RAP:RapidOrder,RAP:Normal,RAP:ProgramName),DUP,NOCASE
WebOrderKey              KEY(RAP:RapidOrder,RAP:Web,RAP:ProgramName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RapidOrder                  LONG
ProgramName                 STRING(60)
Description                 STRING(255)
EXEPath                     STRING(255)
Normal                      BYTE
Web                         BYTE
                         END
                     END                       

NETWORKS             FILE,DRIVER('Btrieve'),OEM,NAME('NETWORKS.DAT'),PRE(net),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(net:RecordNumber),NOCASE,PRIMARY
NetworkKey               KEY(net:Network),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Network                     STRING(30)
                         END
                     END                       

REPSCHED             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHED.DAT'),PRE(rpd),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpd:RecordNumber),NOCASE,PRIMARY
ReportNameKey            KEY(rpd:ReportName),DUP,NOCASE
ReportCriteriaKey        KEY(rpd:ReportName,rpd:ReportCriteriaType,rpd:NextReportDate,rpd:NextReportTime),DUP,NOCASE
MachineNextDateKey       KEY(rpd:MachineIP,rpd:NextReportDate,rpd:NextReportTime),DUP,NOCASE
ReportNameActiveKey      KEY(rpd:Active,rpd:ReportName,rpd:ReportCriteriaType),DUP,NOCASE
ActiveMachineNextDateKey KEY(rpd:Active,rpd:MachineIP,rpd:NextReportDate,rpd:NextReportTime),DUP,NOCASE
ActiveDateKey            KEY(rpd:Active,rpd:NextReportDate,rpd:NextReportTime),DUP,NOCASE
NextReportDateKey        KEY(rpd:NextReportDate,rpd:NextReportTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportName                  STRING(60)
ReportCriteriaType          STRING(60)
Frequency                   BYTE
LastReportDate              DATE
LastReportTime              TIME
NextReportDate              DATE
NextReportTime              TIME
MachineIP                   STRING(15)
Active                      BYTE
                         END
                     END                       

ORDTEMP              FILE,DRIVER('Btrieve'),NAME('ORDTEMP.DAT'),PRE(ort),CREATE,BINDABLE,THREAD
recordnumberkey          KEY(ort:recordnumber),NOCASE,PRIMARY
Keyordhno                KEY(ort:ordhno),DUP,NOCASE
Record                   RECORD,PRE()
recordnumber                LONG
ordhno                      LONG
manufact                    STRING(40)
partno                      STRING(20)
partdiscription             STRING(60)
qty                         LONG
itemcost                    REAL
totalcost                   REAL
thedate                     DATE
                         END
                     END                       

NMSPRE               FILE,DRIVER('Btrieve'),OEM,NAME('NMSPRE.DAT'),PRE(nms),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(nms:RecordNumber),NOCASE,PRIMARY
ProcessedIMEIKey         KEY(nms:Processed,nms:IMEINumber),DUP,NOCASE
ProcessedJobNumberKey    KEY(nms:Processed,nms:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReceivedDate                DATE
ReceivedTime                TIME
JobNumber                   LONG
IMEINumber                  STRING(30)
ThirdPartyID                STRING(30)
WaybillNumber               STRING(30)
FaultSymptomCode            STRING(30)
WarrantyOption              STRING(3)
Processed                   BYTE
ProcessedDate               DATE
ProcessedTime               TIME
                         END
                     END                       

ORDITEMS             FILE,DRIVER('Btrieve'),NAME('ORDITEMS.DAT'),PRE(ori),CREATE,BINDABLE,THREAD
Keyordhno                KEY(ori:ordhno),DUP,NOCASE
recordnumberkey          KEY(ori:recordnumber),NOCASE,PRIMARY
OrdHNoPartKey            KEY(ori:ordhno,ori:partno),DUP,NOCASE
PartNoKey                KEY(ori:partno),DUP,NOCASE
Record                   RECORD,PRE()
recordnumber                LONG
ordhno                      LONG
manufact                    STRING(40)
partno                      STRING(20)
partdiscription             STRING(60)
qty                         LONG
itemcost                    REAL
totalcost                   REAL
OrderDate                   DATE
OrderTime                   TIME
                         END
                     END                       

KSTAGES              FILE,DRIVER('Btrieve'),OEM,NAME('KSTAGES.DAT'),PRE(ksta),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ksta:RecordNumber),NOCASE,PRIMARY
KeyStageKey              KEY(ksta:KeyStage),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
KeyStage                    STRING(30)
                         END
                     END                       

ORDHEAD              FILE,DRIVER('Btrieve'),NAME('ORDHEAD.DAT'),PRE(orh),CREATE,BINDABLE,THREAD
KeyOrder_no              KEY(orh:Order_no),NOCASE,PRIMARY
book_date_key            KEY(orh:thedate),DUP,NOCASE
pro_date_key             KEY(-orh:pro_date),DUP,NOCASE
pro_ord_no_key           KEY(orh:procesed,orh:thedate,orh:Order_no),DUP,NOCASE
pro_cust_name_key        KEY(orh:procesed,orh:pro_date,orh:CustName),DUP,NOCASE
KeyCustName              KEY(orh:CustName),DUP,NOCASE
AccountDateKey           KEY(orh:account_no,orh:thedate),DUP,NOCASE
SalesNumberKey           KEY(orh:SalesNumber),DUP,NOCASE
ProcessSaleNoKey         KEY(orh:procesed,orh:SalesNumber),DUP,NOCASE
DateDespatchedKey        KEY(orh:DateDespatched),DUP,NOCASE
Record                   RECORD,PRE()
Order_no                    LONG
account_no                  STRING(40)
CustName                    STRING(30)
CustAdd1                    STRING(30)
CustAdd2                    STRING(30)
CustAdd3                    STRING(30)
CustPostCode                STRING(10)
CustTel                     STRING(20)
CustFax                     STRING(20)
dName                       STRING(30)
dAdd1                       STRING(30)
dAdd2                       STRING(30)
dAdd3                       STRING(30)
dPostCode                   STRING(10)
dTel                        STRING(20)
dFax                        STRING(20)
thedate                     DATE
thetime                     TIME
procesed                    BYTE
pro_date                    DATE
WhoBooked                   STRING(30)
Department                  STRING(30)
CustOrderNumber             STRING(30)
DateDespatched              DATE
SalesNumber                 LONG
                         END
                     END                       

KLOCS                FILE,DRIVER('Btrieve'),OEM,NAME('KLOCS.DAT'),PRE(kloc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(kloc:RecordNumber),NOCASE,PRIMARY
KeyLocationKey           KEY(kloc:KeyStage,kloc:Location),DUP,NOCASE
RefKeyLocKey             KEY(kloc:KeyStage,kloc:RefNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
KeyStage                    STRING(30)
Location                    STRING(60)
MenuOption                  STRING(255)
SpecialReq                  STRING(255)
ResultStatus                STRING(255)
DocsProduced                STRING(255)
DummyField                  STRING(2)
                         END
                     END                       

IMEISHIP             FILE,DRIVER('Btrieve'),OEM,NAME('IMEISHIP.DAT'),PRE(IMEI),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(IMEI:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEI:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEI:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,NAME('JOBTHIRD.DAT'),PRE(jot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                     END                       

DEFAULTV             FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULTV.DAT'),PRE(defv),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(defv:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
VersionNumber               STRING(30)
NagDate                     DATE
ReUpdate                    BYTE
                         END
                     END                       

LOGRETRN             FILE,DRIVER('Btrieve'),OEM,NAME('LOGRETRN.DAT'),PRE(lrtn),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lrtn:RefNumber),NOCASE,PRIMARY
ClubNokiaKey             KEY(lrtn:ClubNokiaSite,lrtn:Date),DUP,NOCASE
DateKey                  KEY(lrtn:Date),DUP,NOCASE
ReturnsNoKey             KEY(lrtn:ReturnsNumber,lrtn:Date),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
ClubNokiaSite               STRING(30)
ModelNumber                 STRING(30)
Quantity                    LONG
ReturnsNumber               STRING(30)
Date                        DATE
ReturnType                  STRING(3)
DummyField                  STRING(1)
                         END
                     END                       

LOCATLOG             FILE,DRIVER('Btrieve'),OEM,NAME('LOCATLOG.DAT'),PRE(lot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lot:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(lot:RefNumber,lot:TheDate),DUP,NOCASE
NewLocationKey           KEY(lot:RefNumber,lot:NewLocation),DUP,NOCASE
DateNewLocationKey       KEY(lot:NewLocation,lot:TheDate,lot:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(30)
PreviousLocation            STRING(30)
NewLocation                 STRING(30)
                         END
                     END                       

PRODCODE             FILE,DRIVER('Btrieve'),OEM,NAME('PRODCODE.DAT'),PRE(prd),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(prd:RecordNumber),NOCASE,PRIMARY
ProductCodeKey           KEY(prd:ProductCode),DUP,NOCASE
ModelProductKey          KEY(prd:ModelNumber,prd:ProductCode),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
ProductCode                 STRING(30)
HandsetReplacementValue     REAL
                         END
                     END                       

EVO_TAX              FILE,DRIVER('Btrieve'),OEM,PRE(EVOT),CREATE,BINDABLE,THREAD
KeyHeaderNumber          KEY(EVOT:HEADER_NUMBER),DUP,NOCASE
Record                   RECORD,PRE()
HEADER_NUMBER               LONG
TAX_ITEMNO_ACC              LONG
TAX_BASEAMOUNT              STRING(25)
TAX_LOCALBASEAMOUNT         STRING(25)
TAX_TAX_CODE                STRING(2)
TAX_DOCUMENT_AMOUNT         STRING(25)
TAX_DOCUMENT_CURRENC        STRING(5)
TAX_LOCAL_AMOUNT            STRING(25)
TAX_LOCAL_CURRENCY          STRING(5)
                         END
                     END                       

EVO_AP               FILE,DRIVER('Btrieve'),OEM,PRE(EVOA),CREATE,BINDABLE,THREAD
KeyHeader_Number         KEY(EVOA:HEADER_NUMBER),DUP,NOCASE
Record                   RECORD,PRE()
HEADER_NUMBER               LONG
AP_ITEMNO_ACC               LONG
AP_VENDOR_NO                STRING(10)
AP_ITEM_TEXT                STRING(50)
AP_DOCUMENT_AMOUNT          STRING(25)
AP_DOCUMENT_CURRENC         STRING(5)
AP_LOCAL_AMOUNT             STRING(25)
AP_LOCAL_CURRENCY           STRING(5)
AP_EXCHANGE_RATE            STRING(11)
                         END
                     END                       

EVO_GL               FILE,DRIVER('Btrieve'),OEM,PRE(EVOG),CREATE,BINDABLE,THREAD
KeyHeader_Number         KEY(EVOG:HEADER),DUP,NOCASE
Record                   RECORD,PRE()
HEADER                      LONG
GL_ITEMNO_ACC               LONG
GL_ACCOUNT                  STRING(10)
GL_ITEM_TEXT                STRING(50)
GL_TAX_CODE                 STRING(2)
GL_PROFITCENTRE             STRING(10)
GL_DOCUMENT_AMOUNT          STRING(25)
GL_DOCUMENT_CURRENC         STRING(5)
GL_LOCAL_AMOUNT             STRING(25)
GL_LOCAL_CURRENCY           STRING(5)
GL_EXCHANGE_RATE            STRING(11)
                         END
                     END                       

RAPENGLS             FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:FileName),PRE(rapl),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rapl:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(rapl:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
DateAllocated               DATE
TimeAllocated               TIME
ReportedFault               STRING(255)
JobStatus                   STRING(30)
SkillLevel                  LONG
                         END
                     END                       

LOG2TEMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name3),PRE(lo2tmp),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lo2tmp:RefNumber),NOCASE,PRIMARY
IMEIKey                  KEY(lo2tmp:IMEI),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
IMEI                        STRING(30)
Marker                      BYTE
                         END
                     END                       

LOGSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOCK.DAT'),PRE(logsto),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logsto:RefNumber),NOCASE,PRIMARY
SalesKey                 KEY(logsto:SalesCode),DUP,NOCASE
DescriptionKey           KEY(logsto:Description),DUP,NOCASE
SalesModelNoKey          KEY(logsto:SalesCode,logsto:ModelNumber),NOCASE
RefModelNoKey            KEY(logsto:RefNumber,logsto:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(logsto:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
ModelNumber                 STRING(30)
DummyField                  STRING(4)
                         END
                     END                       

ESNMODAL             FILE,DRIVER('Btrieve'),OEM,NAME('ESNMODAL.DAT'),PRE(esa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(esa:RecordNumber),NOCASE,PRIMARY
TACCodeKey               KEY(esa:RefNumber,esa:TacCode),DUP,NOCASE
TacModelKey              KEY(esa:RefNumber,esa:TacCode,esa:ModelNumber),DUP,NOCASE
RefModelNumberKey        KEY(esa:RefNumber,esa:ModelNumber),DUP,NOCASE
ModelNumberOnlyKey       KEY(esa:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TacCode                     STRING(8)
ModelNumber                 STRING(30)
                         END
                     END                       

LABLGTMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name2),PRE(lab),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lab:RefNumber),NOCASE,PRIMARY
DateKey                  KEY(lab:Date,lab:AccountNo,lab:Postcode),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
AccountNo                   STRING(15)
AccountName                 STRING(30)
Postcode                    STRING(15)
Date                        DATE
JobNumber                   LONG
ConsignmentNo               STRING(30)
Count                       LONG
                         END
                     END                       

LETTERS              FILE,DRIVER('Btrieve'),OEM,NAME('LETTERS.DAT'),PRE(let),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(let:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(let:Description),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(255)
Subject                     STRING(255)
TelephoneNumber             STRING(20)
FaxNumber                   STRING(20)
LetterText                  STRING(10000)
UseStatus                   STRING(3)
Status                      STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

EXPGEN               FILE,DRIVER('ASCII'),OEM,NAME(glo:file_name),PRE(gen),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Line1                       STRING(2000)
                         END
                     END                       

STOAUDIT             FILE,DRIVER('Btrieve'),NAME('STOAUDIT.DAT'),PRE(stoa),CREATE,BINDABLE,THREAD
Internal_AutoNumber_Key  KEY(stoa:Internal_AutoNumber),NOCASE,PRIMARY
Audit_Ref_No_Key         KEY(stoa:Audit_Ref_No),DUP,NOCASE
Cellular_Key             KEY(stoa:Site_Location,stoa:Stock_Ref_No),DUP,NOCASE
Stock_Ref_No_Key         KEY(stoa:Stock_Ref_No),DUP,NOCASE
Stock_Site_Number_Key    KEY(stoa:Audit_Ref_No,stoa:Site_Location),DUP,NOCASE
Lock_Down_Key            KEY(stoa:Audit_Ref_No,stoa:Stock_Ref_No),DUP,NOCASE
Main_Browse_Key          KEY(stoa:Audit_Ref_No,stoa:Confirmed,stoa:Site_Location,stoa:Shelf_Location,stoa:Second_Location,stoa:Internal_AutoNumber),DUP,NOCASE
Report_Key               KEY(stoa:Audit_Ref_No,stoa:Shelf_Location),DUP,NOCASE
Record                   RECORD,PRE()
Internal_AutoNumber         LONG
Site_Location               STRING(30)
Stock_Ref_No                LONG
Original_Level              LONG
New_Level                   LONG
Audit_Reason                STRING(255)
Audit_Ref_No                LONG
Preliminary                 STRING(1)
Shelf_Location              STRING(30)
Second_Location             STRING(30)
Confirmed                   STRING(1)
                         END
                     END                       

DEFAULT2             FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULT2.DAT'),PRE(de2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(de2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
OverdueBackOrderText        STRING(255)
EmailUserName               STRING(255)
EmailPassword               STRING(60)
GlobalPrintText             STRING(255)
DefaultFromEmail            STRING(100)
PLE                         DATE
J_Collection_Rate           LONG
                         END
                     END                       

ACTION               FILE,DRIVER('Btrieve'),OEM,NAME('ACTION.DAT'),PRE(act),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(act:Record_Number),NOCASE,PRIMARY
Action_Key               KEY(act:Action),NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Action                      STRING(30)
                         END
                     END                       

DEFRAPID             FILE,DRIVER('Btrieve'),OEM,NAME('DEFRAPID'),PRE(der),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(der:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               LONG
Print_Job_Card              STRING(20)
Print_Job_Label             STRING(3)
Use_Transit_Type            STRING(20)
Transit_Type                STRING(30)
Use_Workshop                STRING(20)
Workshop                    STRING(20)
                         END
                     END                       

RETSALES             FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(ret),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ret:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(ret:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(ret:Despatched,ret:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(ret:Despatched,ret:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(ret:Despatched,ret:Account_Number,ret:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(ret:Account_Number,ret:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(ret:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(ret:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(ret:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(ret:Account_Number,ret:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(ret:Despatched,ret:Purchase_Order_Number,ret:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(ret:Despatch_Number,ret:Ref_Number),DUP,NOCASE
AccountPurchaseNumberKey KEY(ret:Account_Number,ret:Purchase_Order_Number),DUP,NOCASE
WaybillNumberKey         KEY(ret:WaybillNumber,ret:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
WaybillNumber               LONG
DatePickingNotePrinted      DATE
TimePickingNotePrinted      TIME
ExchangeOrder               BYTE
LoanOrder                   BYTE
                         END
                     END                       

LOANALC              FILE,DRIVER('Btrieve'),OEM,NAME('LOANALC.DAT'),PRE(lac1),CREATE,BINDABLE,THREAD
Main_Browse_Key          KEY(lac1:Audit_Number,lac1:Location,lac1:Stock_Type,lac1:Confirmed),DUP,NOCASE
Locate_key               KEY(lac1:Audit_Number,lac1:Location,lac1:Stock_Type),DUP,NOCASE
AutoNumber_key           KEY(lac1:Internal_No),NOCASE,PRIMARY
Record                   RECORD,PRE()
Internal_No                 LONG
Location                    STRING(30)
Stock_Type                  STRING(30)
Audit_Number                LONG
Confirmed                   BYTE
                         END
                     END                       

WIPALC               FILE,DRIVER('Btrieve'),OEM,NAME('WIPALC.DAT'),PRE(wil),CREATE,BINDABLE,THREAD
Main_Browse_Key          KEY(wil:Audit_Number,wil:Location,wil:Status,wil:Confirmed),DUP,NOCASE
Locate_key               KEY(wil:Audit_Number,wil:Location,wil:Status),DUP,NOCASE
AutoNumber_key           KEY(wil:Internal_No),NOCASE,PRIMARY
Record                   RECORD,PRE()
Internal_No                 LONG
Location                    STRING(30)
Status                      STRING(30)
Audit_Number                LONG
Confirmed                   BYTE
                         END
                     END                       

LOANAMF              FILE,DRIVER('Btrieve'),OEM,NAME('LOANAMF.DAT'),PRE(lmf),CREATE,BINDABLE,THREAD
Audit_Number_Key         KEY(lmf:Audit_Number),NOCASE,PRIMARY
Secondary_Key            KEY(lmf:Complete_Flag,lmf:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Date                        DATE
Time                        LONG
User                        STRING(3)
Status                      STRING(30)
Site_location               STRING(30)
Complete_Flag               BYTE
                         END
                     END                       

BOUNCER              FILE,DRIVER('Btrieve'),OEM,NAME('BOUNCER.DAT'),PRE(bou),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(bou:Record_Number),NOCASE,PRIMARY
Bouncer_Job_Number_Key   KEY(bou:Original_Ref_Number,bou:Bouncer_Job_Number),NOCASE
Bouncer_Job_Only_Key     KEY(bou:Bouncer_Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Original_Ref_Number         REAL
Bouncer_Job_Number          REAL
                         END
                     END                       

EXCHORNO             FILE,DRIVER('Btrieve'),OEM,NAME('EXCHORNO.DAT'),PRE(eno),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(eno:RecordNumber),NOCASE,PRIMARY
LocationRecordKey        KEY(eno:Location,eno:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
                         END
                     END                       

LOANORNO             FILE,DRIVER('Btrieve'),OEM,NAME('LOANORNO.DAT'),PRE(lno),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lno:RecordNumber),NOCASE,PRIMARY
LocationRecordKey        KEY(lno:Location,lno:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
                         END
                     END                       

EXCHOR48             FILE,DRIVER('Btrieve'),OEM,NAME('EXCHOR48.DAT'),PRE(ex4),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ex4:RecordNumber),NOCASE,PRIMARY
LocationModelKey         KEY(ex4:Location,ex4:Manufacturer,ex4:ModelNumber),DUP,NOCASE
ReturningModelKey        KEY(ex4:Received,ex4:Returning,ex4:Location,ex4:Manufacturer,ex4:ModelNumber),DUP,NOCASE
LocationJobKey           KEY(ex4:Received,ex4:Returning,ex4:Location,ex4:JobNumber),DUP,NOCASE
JobNumberKey             KEY(ex4:Location,ex4:JobNumber),DUP,NOCASE
AttachedToJobKey         KEY(ex4:AttachedToJob,ex4:Location,ex4:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
Received                    BYTE
Notes                       STRING(255)
DateCreated                 DATE
TimeCreated                 TIME
DateOrdered                 DATE
TimeOrdered                 TIME
JobNumber                   REAL
Returning                   BYTE
OrderUnitNumber             LONG
AttachedToJob               BYTE
                         END
                     END                       

LOANAUI              FILE,DRIVER('Btrieve'),OEM,NAME('LOANAUI.DAT'),PRE(lau),CREATE,BINDABLE,THREAD
Internal_No_Key          KEY(lau:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(lau:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(lau:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(lau:Audit_Number,lau:Exists),DUP,NOCASE
AuditIMEIKey             KEY(lau:Audit_Number,lau:New_IMEI),DUP,NOCASE
Locate_IMEI_Key          KEY(lau:Audit_Number,lau:Site_Location,lau:IMEI_Number),DUP,NOCASE,OPT
Main_Browse_Key          KEY(lau:Audit_Number,lau:Confirmed,lau:Site_Location,lau:Stock_Type,lau:Shelf_Location,lau:Internal_No),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Shelf_Location              STRING(30)
Location                    STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
Exists                      BYTE
New_IMEI                    BYTE
IMEI_Number                 STRING(20)
Confirmed                   BYTE
Stock_Type                  STRING(30)
                         END
                     END                       

LOAORDR              FILE,DRIVER('Btrieve'),OEM,PRE(lor),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(lor:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(lor:Location,lor:Manufacturer,lor:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(lor:Location,lor:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(lor:Received,lor:Location,lor:Manufacturer,lor:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(lor:OrderNumber),DUP,NOCASE
LocationDateReceivedKey  KEY(lor:Location,lor:DateReceived),DUP,NOCASE
LoanRefNumberKey         KEY(lor:LoanRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
LoanRefNumber               LONG
                         END
                     END                       

WIPAMF               FILE,DRIVER('Btrieve'),OEM,NAME('WIPAMF.DAT'),PRE(wim),CREATE,BINDABLE,THREAD
Audit_Number_Key         KEY(wim:Audit_Number),NOCASE,PRIMARY
Secondary_Key            KEY(wim:Complete_Flag,wim:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Date                        DATE
Time                        LONG
User                        STRING(3)
Status                      STRING(30)
Site_location               STRING(30)
Complete_Flag               BYTE
Ignore_IMEI                 BYTE
Ignore_Job_Number           BYTE
Date_Completed              DATE
Time_Completed              TIME
                         END
                     END                       

TEAMS                FILE,DRIVER('Btrieve'),OEM,NAME('TEAMS.DAT'),PRE(tea),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(tea:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(tea:Team),NOCASE
KeyTraceAccount_number   KEY(tea:TradeAccount_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
TradeAccount_Number         STRING(15)
Associated                  STRING(1)
                         END
                     END                       

EXCHALC              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHALC.DAT'),PRE(eac),CREATE,BINDABLE,THREAD
Main_Browse_Key          KEY(eac:Audit_Number,eac:Location,eac:Stock_Type,eac:Confirmed),DUP,NOCASE
Locate_key               KEY(eac:Audit_Number,eac:Location,eac:Stock_Type),DUP,NOCASE
AutoNumber_key           KEY(eac:Internal_No),NOCASE,PRIMARY
Record                   RECORD,PRE()
Internal_No                 LONG
Location                    STRING(30)
Stock_Type                  STRING(30)
Audit_Number                LONG
Confirmed                   BYTE
                         END
                     END                       

WIPAUI               FILE,DRIVER('Btrieve'),OEM,NAME('WIPAUI.DAT'),PRE(wia),CREATE,BINDABLE,THREAD
Internal_No_Key          KEY(wia:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(wia:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(wia:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(wia:Audit_Number),DUP,NOCASE
AuditIMEIKey             KEY(wia:Audit_Number,wia:New_In_Status),DUP,NOCASE
Locate_IMEI_Key          KEY(wia:Audit_Number,wia:Site_Location,wia:Ref_Number),DUP,NOCASE
Main_Browse_Key          KEY(wia:Audit_Number,wia:Confirmed,wia:Site_Location,wia:Status,wia:Ref_Number),DUP,NOCASE
AuditRefNumberKey        KEY(wia:Audit_Number,wia:Ref_Number),DUP,NOCASE
AuditStatusRefNoKey      KEY(wia:Audit_Number,wia:Status,wia:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Status                      STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
New_In_Status               BYTE
Confirmed                   BYTE
IsExchange                  BYTE
                         END
                     END                       

MERGE                FILE,DRIVER('Btrieve'),OEM,NAME('MERGE.DAT'),PRE(mer),BINDABLE,CREATE,THREAD
RefNumberKey             KEY(mer:RefNumber),NOCASE,PRIMARY
FieldNameKey             KEY(mer:FieldName),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
FieldName                   STRING(30)
FileName                    STRING(60)
Type                        STRING(3)
Description                 STRING(255)
Capitals                    BYTE
                         END
                     END                       

EXCHORDR             FILE,DRIVER('Btrieve'),OEM,NAME('EXCHORDR.DAT'),PRE(exo),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(exo:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(exo:Location,exo:Manufacturer,exo:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(exo:Location,exo:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(exo:Received,exo:Location,exo:Manufacturer,exo:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(exo:OrderNumber),DUP,NOCASE
LocationDateReceivedKey  KEY(exo:Location,exo:DateReceived),DUP,NOCASE
ExchangeRefNumberKey     KEY(exo:ExchangeRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
ExchangeRefNumber           LONG
TimeCreated                 TIME
                         END
                     END                       

UPDDATA              FILE,DRIVER('Btrieve'),OEM,NAME('UPDDATA.DAT'),PRE(upd),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(upd:RefNumber),NOCASE,PRIMARY
TypeKey                  KEY(upd:Type,upd:DataField),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Type                        STRING(3)
DataField                   STRING(30)
DataField2                  STRING(30)
DataField3                  STRING(30)
DataField4                  STRING(30)
DataField5                  STRING(30)
DataField6                  STRING(30)
DataField7                  STRING(30)
Description                 STRING(500)
                         END
                     END                       

EXPSPARES            FILE,DRIVER('BASIC'),OEM,NAME(glo:file_name),PRE(expspa),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    STRING(30)
Sale_Cost                   STRING(20)
Total_Cost                  STRING(20)
                         END
                     END                       

WEBDEFLT             FILE,DRIVER('Btrieve'),OEM,NAME('WEBDEFLT.DAT'),PRE(web),CREATE,BINDABLE,THREAD
ordnokey                 KEY(web:ordno),NOCASE,PRIMARY
Record                   RECORD,PRE()
ordno                       LONG
                         END
                     END                       

EXCAUDIT             FILE,DRIVER('Btrieve'),OEM,NAME('EXCAUDIT.DAT'),PRE(exa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(exa:Record_Number),NOCASE,PRIMARY
Audit_Number_Key         KEY(exa:Stock_Type,exa:Audit_Number),NOCASE
StockUnitNoKey           KEY(exa:Stock_Unit_Number),DUP,NOCASE
ReplaceUnitNoKey         KEY(exa:Replacement_Unit_Number),DUP,NOCASE
TypeStockNumber          KEY(exa:Stock_Type,exa:Stock_Unit_Number,exa:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Stock_Type                  STRING(30)
Audit_Number                REAL
Stock_Unit_Number           REAL
Replacement_Unit_Number     REAL
                         END
                     END                       

GRNOTESP             FILE,DRIVER('Btrieve'),OEM,NAME('GRNOTESP.DAT'),PRE(grp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(grp:RecordNumber),NOCASE,PRIMARY
RTARecordNumberKey       KEY(grp:RTARecordNumber,grp:DateCreated),DUP,NOCASE
DateCreatedKey           KEY(grp:DateCreated,grp:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RTARecordNumber             LONG
DateCreated                 DATE
TimeCreated                 TIME
Usercode                    STRING(3)
AcceptReject                STRING(6)
                         END
                     END                       

COMMONFA             FILE,DRIVER('Btrieve'),OEM,NAME('COMMONFA.DAT'),PRE(com),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(com:Ref_Number),NOCASE,PRIMARY
Description_Key          KEY(com:Model_Number,com:Category,com:Description),DUP,NOCASE
DescripOnlyKey           KEY(com:Model_Number,com:Description),DUP,NOCASE
Ref_Model_Key            KEY(com:Model_Number,com:Category,com:Ref_Number),DUP,NOCASE
RefOnlyKey               KEY(com:Model_Number,com:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Category                    STRING(30)
date_booked                 DATE
time_booked                 TIME
who_booked                  STRING(3)
Model_Number                STRING(30)
Description                 STRING(30)
Chargeable_Job              STRING(3)
Chargeable_Charge_Type      STRING(30)
Chargeable_Repair_Type      STRING(30)
Warranty_Job                STRING(3)
Warranty_Charge_Type        STRING(30)
Warranty_Repair_Type        STRING(30)
Auto_Complete               STRING(3)
Attach_Diagram              STRING(3)
Diagram_Setting             STRING(1)
Diagram_Path                STRING(255)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(255)
Fault_Code11                  STRING(255)
Fault_Code12                  STRING(255)
                            END
Invoice_Text                STRING(255)
Engineers_Notes             STRING(255)
DummyField                  STRING(2)
                         END
                     END                       

PARAMSS              FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(prm),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Account_Ref                 STRING(8)
Name                        STRING(60)
Address_1                   STRING(60)
Address_2                   STRING(60)
Address_3                   STRING(60)
Address_4                   STRING(60)
Address_5                   STRING(60)
Del_Address_1               STRING(60)
Del_Address_2               STRING(60)
Del_Address_3               STRING(60)
Del_Address_4               STRING(60)
Del_Address_5               STRING(60)
Cust_Tel_Number             STRING(30)
Contact_Name                STRING(30)
Notes_1                     STRING(60)
Notes_2                     STRING(60)
Notes_3                     STRING(60)
Taken_By                    STRING(60)
Order_Number                STRING(7)
Cust_Order_Number           STRING(30)
Payment_Ref                 STRING(8)
Global_Nom_Code             STRING(8)
Global_Details              STRING(60)
Items_Net                   STRING(8)
Items_Tax                   STRING(8)
Stock_Code                  STRING(30)
Description                 STRING(60)
Nominal_Code                STRING(8)
Qty_Order                   STRING(8)
Unit_Price                  STRING(8)
Net_Amount                  STRING(8)
Tax_Amount                  STRING(8)
Comment_1                   STRING(60)
Comment_2                   STRING(60)
Unit_Of_Sale                STRING(8)
Full_Net_Amount             STRING(8)
Invoice_Date                STRING(10)
Data_filepath               STRING(255)
User_Name                   STRING(30)
Password                    STRING(30)
Set_Invoice_Number          STRING(8)
Invoice_No                  STRING(8)
                         END
                     END                       

GRNOTESR             FILE,DRIVER('Btrieve'),OEM,NAME('GRNOTESR.DAT'),PRE(grr),CREATE,BINDABLE,THREAD
Goods_Received_Number_Key KEY(grr:Goods_Received_Number),NOCASE,PRIMARY
Order_Number_Key         KEY(grr:Order_Number,grr:Goods_Received_Date),DUP,NOCASE
Date_Recd_Key            KEY(grr:Goods_Received_Date),DUP,NOCASE
Record                   RECORD,PRE()
Goods_Received_Number       REAL
Order_Number                REAL
Goods_Received_Date         DATE
                         END
                     END                       

CONSIGN              FILE,DRIVER('Btrieve'),OEM,NAME('CONSIGN.DAT'),PRE(cns),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(cns:Record_Number),NOCASE,PRIMARY
Consignment_Number_Key   KEY(cns:Consignment_Note_Number,cns:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Consignment_Note_Number     STRING(30)
Ref_Number                  REAL
Type                        STRING(3)
                         END
                     END                       

HANDOJOB             FILE,DRIVER('Btrieve'),OEM,NAME('HANDOJOB.DAT'),PRE(haj),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(haj:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(haj:RefNumber,haj:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobNumber                   LONG
IMEINumber                  STRING(30)
ModelNumber                 STRING(30)
Accessories                 STRING(255)
                         END
                     END                       

JOBEXACC             FILE,DRIVER('Btrieve'),OEM,NAME('JOBEXACC.DAT'),PRE(jea),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jea:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(jea:Job_Ref_Number,jea:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Job_Ref_Number              REAL
Stock_Ref_Number            REAL
Part_Number                 STRING(30)
Description                 STRING(30)
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCOL.DAT'),PRE(moc),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Colour                      STRING(30)
                         END
                     END                       

COMMCAT              FILE,DRIVER('Btrieve'),OEM,NAME('COMMCAT.DAT'),PRE(cmc),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(cmc:Record_Number),NOCASE,PRIMARY
Category_Key             KEY(cmc:Model_Number,cmc:Category),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Category                    STRING(30)
                         END
                     END                       

MESSAGES             FILE,DRIVER('Btrieve'),NAME('MESSAGES.DAT'),PRE(mes),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(mes:Record_Number),NOCASE,PRIMARY
Read_Key                 KEY(mes:Read,mes:Message_For,-mes:Date),DUP,NOCASE
Who_To_Key               KEY(mes:Message_For,-mes:Date),DUP,NOCASE
Who_From_Key             KEY(mes:Message_For,mes:Message_From),DUP,NOCASE
Read_Only_Key            KEY(mes:Read,-mes:Date),DUP,NOCASE
Date_Only_Key            KEY(-mes:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Date                        DATE
Time                        TIME
Message_For                 STRING(3)
Message_From                STRING(3)
Comment                     STRING(80)
Read                        STRING(3)
Message_Memo                STRING(1000)
                         END
                     END                       

LOGEXHE              FILE,DRIVER('Btrieve'),OEM,NAME('LOGEXHE.DAT'),PRE(log1),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(log1:Batch_No),NOCASE,PRIMARY
Processed_Key            KEY(log1:Processed,log1:Batch_No),DUP,NOCASE
Record                   RECORD,PRE()
Batch_No                    LONG
Processed                   STRING(1)
Date                        DATE
SMPF_No                     STRING(30)
ModelNumber                 STRING(30)
Qty                         LONG
                         END
                     END                       

DEFCRC               FILE,DRIVER('Btrieve'),OEM,NAME('DEFCRC.DAT'),PRE(dfc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(dfc:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
TransitType                 STRING(30)
Location                    STRING(30)
PAccountNumber              STRING(30)
PTransitType                STRING(30)
PLocation                   STRING(30)
EAccountNumber              STRING(30)
ETransitType                STRING(30)
ELocation                   STRING(30)
SAccountNumber              STRING(30)
STransitType                STRING(30)
STransitType2               STRING(30)
SLocation                   STRING(30)
                         END
                     END                       

PAYTYPES             FILE,DRIVER('Btrieve'),NAME('PAYTYPES.DAT'),PRE(pay),CREATE,BINDABLE,THREAD
Payment_Type_Key         KEY(pay:Payment_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Payment_Type                STRING(30)
Credit_Card                 STRING(3)
                         END
                     END                       

COMMONWP             FILE,DRIVER('Btrieve'),NAME('COMMONWP.DAT'),PRE(cwp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cwp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(cwp:Ref_Number),DUP,NOCASE
Description_Key          KEY(cwp:Ref_Number,cwp:Description),DUP,NOCASE
RefPartNumberKey         KEY(cwp:Ref_Number,cwp:Part_Number),DUP,NOCASE
PartNumberKey            KEY(cwp:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

LOGEXCH              FILE,DRIVER('Btrieve'),NAME('LOGEXCH.DAT'),PRE(xch1),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(xch1:Batch_Number),DUP,NOCASE
Ref_Number_Key           KEY(xch1:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(xch1:ESN),DUP,NOCASE
MSN_Only_Key             KEY(xch1:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(xch1:Stock_Type,xch1:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(xch1:Stock_Type,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(xch1:Stock_Type,xch1:ESN),DUP,NOCASE
MSN_Key                  KEY(xch1:Stock_Type,xch1:MSN),DUP,NOCASE
ESN_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:ESN),DUP,NOCASE
MSN_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:MSN),DUP,NOCASE
Ref_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(xch1:Available,xch1:Stock_Type,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(xch1:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(xch1:Available,xch1:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(xch1:Available,xch1:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(xch1:Available,xch1:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(xch1:Available,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
                         END
                     END                       

QAREASON             FILE,DRIVER('Btrieve'),OEM,NAME('QAREASON.DAT'),PRE(qar),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(qar:RecordNumber),NOCASE,PRIMARY
ReasonKey                KEY(qar:Reason),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Reason                      STRING(60)
                         END
                     END                       

COMMONCP             FILE,DRIVER('Btrieve'),NAME('COMMONCP.DAT'),PRE(ccp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ccp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ccp:Ref_Number),DUP,NOCASE
Description_Key          KEY(ccp:Ref_Number,ccp:Description),DUP,NOCASE
RefPartNumberKey         KEY(ccp:Ref_Number,ccp:Part_Number),DUP,NOCASE
PartNumberKey            KEY(ccp:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

ADDSEARCH            FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:File_Name),PRE(addtmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(addtmp:RecordNumber),NOCASE,PRIMARY
AddressLine1Key          KEY(addtmp:AddressLine1),DUP,NOCASE
PostcodeKey              KEY(addtmp:Postcode),DUP,NOCASE
IncomingIMEIKey          KEY(addtmp:IncomingIMEI),DUP,NOCASE
ExchangedIMEIKey         KEY(addtmp:ExchangedIMEI),DUP,NOCASE
FinalIMEIKey             KEY(addtmp:FinalIMEI),DUP,NOCASE
JobNumberKey             KEY(addtmp:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(30)
JobNumber                   LONG
IncomingIMEI                STRING(30)
ExchangedIMEI               STRING(30)
FinalIMEI                   STRING(30)
Surname                     STRING(30)
                         END
                     END                       

JOBSSL               FILE,DRIVER('Btrieve'),OEM,NAME('JOBSSL.DAT'),PRE(jsl),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jsl:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jsl:RefNumber),DUP,NOCASE
SLNumberKey              KEY(jsl:SLNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
SLNumber                    LONG
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(cht),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE
Record_Number_Key        KEY(cht:Record_Number),NOCASE,PRIMARY
KeyRefSticky             KEY(cht:Ref_Number,cht:SN_StickyNote),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(2000)
SystemHistory               BYTE
SN_StickyNote               STRING(1)
SN_Completed                STRING(1)
SN_EngAlloc                 STRING(1)
SN_EngUpdate                STRING(1)
SN_CustService              STRING(1)
SN_WaybillConf              STRING(1)
SN_Despatch                 STRING(1)
                         END
                     END                       

CONTACTS             FILE,DRIVER('Btrieve'),NAME('CONTACTS.DAT'),PRE(con),CREATE,BINDABLE,THREAD
Name_Key                 KEY(con:Name),NOCASE,PRIMARY
Record                   RECORD,PRE()
Name                        STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name1               STRING(30)
Contact_Name2               STRING(30)
EmailAddress                STRING(255)
                         END
                     END                       

EXPJOBS              FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(expjob),BINDABLE,CREATE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Batch_Number                STRING(20)
who_booked                  STRING(20)
date_booked                 STRING(20)
time_booked                 STRING(20)
Current_Status              STRING(30)
Title                       STRING(20)
Initial                     STRING(20)
Surname                     STRING(30)
Account_Number              STRING(15)
Order_Number                STRING(30)
Chargeable_Job              STRING(20)
Charge_Type                 STRING(30)
Repair_Type                 STRING(30)
Warranty_Job                STRING(20)
Warranty_Charge_Type        STRING(30)
Repair_Type_Warranty        STRING(30)
Unit_Details_Group          GROUP
Model_Number                  STRING(30)
Manufacturer                  STRING(30)
ESN                           STRING(16)
MSN                           STRING(16)
Unit_Type                     STRING(30)
                            END
Mobile_Number               STRING(15)
Workshop                    STRING(20)
Location                    STRING(30)
Authority_Number            STRING(30)
DOP                         STRING(20)
Physical_Damage             STRING(20)
Intermittent_Fault          STRING(20)
Transit_Type                STRING(30)
Job_Priority                STRING(30)
Engineer                    STRING(20)
Address_Group               GROUP
Postcode                      STRING(20)
Company_Name                  STRING(30)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
Telephone_Number              STRING(15)
Fax_Number                    STRING(15)
                            END
Address_Collection_Group    GROUP
Postcode_Collection           STRING(20)
Company_Name_Collection       STRING(30)
Address_Line1_Collection      STRING(30)
Address_Line2_Collection      STRING(30)
Address_Line3_Collection      STRING(30)
Telephone_Collection          STRING(15)
                            END
Address_Delivery_Group      GROUP
Postcode_Delivery             STRING(20)
Company_Name_Delivery         STRING(30)
Address_Line1_Delivery        STRING(30)
Address_Line2_Delivery        STRING(30)
Address_Line3_Delivery        STRING(30)
Telephone_Delivery            STRING(15)
                            END
In_Repair_Group             GROUP
In_Repair                     STRING(20)
Date_In_Repair                STRING(20)
Time_In_Repair                STRING(20)
                            END
On_Test_Group               GROUP
On_Test                       STRING(20)
Date_On_Test                  STRING(20)
Time_On_Test                  STRING(20)
                            END
Completed_Group             GROUP
Date_Completed                STRING(20)
Time_Completed                STRING(20)
                            END
QA_Group                    GROUP
QA_Passed                     STRING(20)
Date_QA_Passed                STRING(20)
Time_QA_Passed                STRING(20)
QA_Rejected                   STRING(20)
Date_QA_Rejected              STRING(20)
Time_QA_Rejected              STRING(20)
QA_Second_Passed              STRING(20)
Date_QA_Second_Passed         STRING(20)
Time_QA_Second_Passed         STRING(20)
                            END
Estimate_Ready              STRING(20)
Estimate_Group              GROUP
Estimate                      STRING(20)
Estimate_If_Over              STRING(20)
Estimate_Accepted             STRING(20)
Estimate_Rejected             STRING(20)
                            END
Chargeable_Costs            GROUP
Courier_Cost                  STRING(20)
Labour_Cost                   STRING(20)
Parts_Cost                    STRING(20)
Sub_Total                     STRING(20)
                            END
Estimate_Costs              GROUP
Courier_Cost_Estimate         STRING(20)
Labour_Cost_Estimate          STRING(20)
Parts_Cost_Estimate           STRING(20)
Sub_Total_Estimate            STRING(20)
                            END
Warranty_Costs              GROUP
Courier_Cost_Warranty         STRING(20)
Labour_Cost_Warranty          STRING(20)
Parts_Cost_Warranty           STRING(20)
Sub_Total_Warranty            STRING(20)
                            END
Payment_Group               GROUP
Paid                          STRING(20)
Paid_Warranty                 STRING(20)
Date_Paid                     STRING(20)
Paid_User                     STRING(20)
                            END
Loan_Status                 STRING(30)
Loan_Issued_Date            STRING(20)
Loan_Unit_Number            STRING(20)
Loan_User                   STRING(20)
Loan_Despatched_Group       GROUP
Loan_Courier                  STRING(30)
Loan_Consignment_Number       STRING(30)
Loan_Despatched_User          STRING(20)
Loan_Despatch_Number          STRING(20)
                            END
Exchange_Status             STRING(30)
Exchange_Unit_Number        STRING(20)
Exchange_Issued_Date        STRING(20)
Exchange_User               STRING(20)
Exchange_Despatched_Group   GROUP
Exchange_Courier              STRING(30)
Exchange_Consignment_Number   STRING(30)
Exchange_Despatched_User      STRING(20)
Exchange_Despatch_Number      STRING(20)
                            END
Despatch_Group              GROUP
Date_Despatched               STRING(20)
Despatch_Number               STRING(20)
Despatch_User                 STRING(20)
Courier                       STRING(30)
Consignment_Number            STRING(30)
                            END
Incoming_Group              GROUP
Incoming_Courier              STRING(30)
Incoming_Consignment_Number   STRING(30)
Incoming_Date                 STRING(20)
                            END
Despatched                  STRING(20)
Third_Party_Site            STRING(30)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(255)
Fault_Code11                  STRING(255)
Fault_Code12                  STRING(255)
                            END
Special_Instructions        STRING(30)
EDI_Batch_Number            STRING(20)
Fault_Description           STRING(255)
Notes                       STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Fault_Report                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
                         END
                     END                       

EXPAUDIT             FILE,DRIVER('BASIC'),NAME(glo:file_name4),PRE(expaud),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Date                        STRING(20)
Time                        STRING(20)
User                        STRING(20)
Action                      STRING(80)
Notes                       STRING(10000)
                         END
                     END                       

JOBBATCH             FILE,DRIVER('Btrieve'),NAME('JOBBATCH.DAT'),PRE(jbt),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(jbt:Batch_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Batch_Number                REAL
Date                        DATE
Time                        TIME
                         END
                     END                       

DEFEDI2              FILE,DRIVER('Btrieve'),NAME('DEFEDI2.DAT'),PRE(ed2),CREATE,BINDABLE,THREAD
record_number_key        KEY(ed2:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
record_number               REAL
Country_Code                STRING(3)
                         END
                     END                       

DEFPRINT             FILE,DRIVER('Btrieve'),NAME('DEFPRINT.DAT'),PRE(dep),CREATE,BINDABLE,THREAD
Printer_Name_Key         KEY(dep:Printer_Name),NOCASE,PRIMARY
Record                   RECORD,PRE()
Printer_Name                STRING(60)
Printer_Path                STRING(255)
Background                  STRING(3)
Copies                      LONG
                         END
                     END                       

DEFWEB               FILE,DRIVER('Btrieve'),NAME('DEFWEB.DAT'),PRE(dew),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(dew:Record_Number),NOCASE,PRIMARY
Account_Number_Key       KEY(dew:Account_Number),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Account_Number              STRING(15)
Warranty_Charge_Type        STRING(30)
Location                    STRING(30)
Transit_Type                STRING(30)
Job_Priority                STRING(30)
Unit_Type                   STRING(30)
Status_Type                 STRING(30)
HandSet                     STRING(30)
Chargeable_Charge_Type      STRING(30)
Insurance_Charge_Type       STRING(30)
AllowJobBooking             BYTE
AllowJobProgress            BYTE
AllowOrderParts             BYTE
AllowOrderProgress          BYTE
Option5                     BYTE
Option6                     BYTE
Option7                     BYTE
Option8                     BYTE
Option9                     BYTE
Option10                    BYTE
                         END
                     END                       

EXPCITY              FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(epc),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Account_Number              STRING(8)
Ref_Number                  STRING(31)
Customer_Name               STRING(31)
Contact_Name                STRING(31)
Address_Line1               STRING(31)
Address_Line2               STRING(31)
Town                        STRING(31)
County                      STRING(31)
Postcode                    STRING(9)
City_Service                STRING(2)
City_Instructions           STRING(31)
Pudamt                      STRING(5)
Return_It                   STRING(2)
Saturday                    STRING(2)
Dog                         STRING(31)
Nol                         STRING(3)
                         END
                     END                       

PROCCODE             FILE,DRIVER('Btrieve'),NAME('PROCCODE.DAT'),PRE(pro),CREATE,BINDABLE,THREAD
Code_Number_Key          KEY(pro:Code_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Code_Number                 STRING(3)
Description                 STRING(30)
Allow_Loan                  STRING(3)
Accessory_Only              STRING(3)
                         END
                     END                       

COLOUR               FILE,DRIVER('Btrieve'),OEM,NAME('COLOUR.DAT'),PRE(col),CREATE,BINDABLE,THREAD
Colour_Key               KEY(col:Colour),NOCASE,PRIMARY
Record                   RECORD,PRE()
Colour                      STRING(30)
                         END
                     END                       

VODAIMP              FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(vdi),BINDABLE,THREAD
Record                   RECORD,PRE()
Line                        STRING(255)
                         END
                     END                       

JOBSTAMP             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSTAMP.DAT'),PRE(jos),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jos:RecordNumber),NOCASE,PRIMARY
JOBSRefNumberKey         KEY(jos:JOBSRefNumber),DUP,NOCASE
DateTimeKey              KEY(jos:DateStamp,jos:TimeStamp),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JOBSRefNumber               LONG
DateStamp                   DATE
TimeStamp                   TIME
                         END
                     END                       

JOBSVODA             FILE,DRIVER('Btrieve'),NAME('JOBSVODA.DAT'),PRE(jvf),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jvf:Ref_Number),NOCASE,PRIMARY
Ref_Pending_Key          KEY(jvf:Pending,jvf:Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(jvf:Pending,jvf:Order_Number),DUP,NOCASE
Process_Code_Key         KEY(jvf:Pending,jvf:Process_Code,jvf:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(jvf:Pending,jvf:Model_Number,jvf:Unit_Type),DUP,NOCASE
ESN_Key                  KEY(jvf:Pending,jvf:ESN),DUP,NOCASE
Job_Number_Key           KEY(jvf:Pending,jvf:Job_Number),DUP,NOCASE
Model_Ref_key            KEY(jvf:Pending,jvf:Model_Number,jvf:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Import_Query_Reason         STRING(1000)
Ref_Number                  REAL
Job_Number                  REAL
Status                      STRING(30)
Account_Number              STRING(15)
Account_Type                STRING(30)
Order_Number                STRING(30)
Process_Code                STRING(5)
Process_Description         STRING(30)
Job_Type                    STRING(30)
Customer_Name               STRING(60)
Contact_Number              STRING(15)
Mobile_Number               STRING(15)
Reported_Fault              STRING(2)
Delegation                  STRING(30)
Consignment_Number          STRING(30)
Date_Dispatch               DATE
Time_Dispatch               TIME
Accessory_String            STRING(255)
Pending_Reason              STRING(255)
Customer_Unit_Group         GROUP
ESN                           STRING(30)
Model_Number                  STRING(30)
Manufacturer                  STRING(30)
Unit_Type                     STRING(30)
Colour                        STRING(30)
                            END
Exchange_Unit_Group         GROUP
Exchange_Unit_Number          REAL
ESN_Exchange                  STRING(30)
Model_Number_Exchange         STRING(30)
Manufacturer_Exchange         STRING(30)
Unit_Type_Exchange            STRING(30)
                            END
Pending                     STRING(3)
Delivery_Address_Group      GROUP
Postcode_Delivery             STRING(10)
Address_Line1_Delivery        STRING(30)
Address_Line2_Delivery        STRING(30)
Address_Line3_Delivery        STRING(30)
                            END
Collection_Address_Group    GROUP
Postcode_Collection           STRING(10)
Address_Line1_Collection      STRING(30)
Address_Line2_Collection      STRING(30)
Address_Line3_Collection      STRING(30)
                            END
DummyField                  STRING(1)
                         END
                     END                       

XREPACT              FILE,DRIVER('BASIC'),NAME(glo:File_Name),PRE(xre),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Field1                      STRING(40)
Field2                      STRING(40)
Field3                      STRING(40)
Field4                      STRING(40)
                         END
                     END                       

ACCESDEF             FILE,DRIVER('Btrieve'),NAME('ACCESDEF.DAT'),PRE(acd),CREATE,BINDABLE,THREAD
Accessory_Key            KEY(acd:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Accessory                   STRING(30)
                         END
                     END                       

STANTEXT             FILE,DRIVER('Btrieve'),NAME('STANTEXT.DAT'),PRE(stt),CREATE,BINDABLE,THREAD
Description_Key          KEY(stt:Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Description                 STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
Text                        STRING(1000)
                         END
                     END                       

NOTESENG             FILE,DRIVER('Btrieve'),NAME('NOTESENG.DAT'),PRE(noe),CREATE,BINDABLE,THREAD
Notes_Key                KEY(noe:Reference,noe:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

JOBACCNO             FILE,DRIVER('Btrieve'),OEM,NAME('JOBACCNO.DAT'),PRE(joa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joa:RecordNumber),NOCASE,PRIMARY
AccessoryNumberKey       KEY(joa:RefNumber,joa:AccessoryNumber),DUP,NOCASE
AccessoryNoOnlyKey       KEY(joa:AccessoryNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccessoryNumber             STRING(30)
                         END
                     END                       

EXPLABG              FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(epg),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Account_Number              STRING(8)
Ref_Number                  STRING(31)
Customer_Name               STRING(31)
Contact_Name                STRING(31)
Address_Line1               STRING(31)
Address_Line2               STRING(31)
Address_Line3               STRING(31)
Address_Line4               STRING(31)
Postcode                    STRING(5)
City_Service                STRING(2)
City_Instructions           STRING(31)
Pudamt                      STRING(5)
Return_It                   STRING(2)
Saturday                    STRING(2)
Dog                         STRING(31)
Nol                         STRING(3)
JobNo                       STRING(9)
Weight                      STRING(6)
                         END
                     END                       

JOBRPNOT             FILE,DRIVER('Btrieve'),OEM,NAME('JOBRPNOT.DAT'),PRE(jrn),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jrn:RecordNumber),NOCASE,PRIMARY
TheDateKey               KEY(jrn:RefNumber,jrn:TheDate,jrn:TheTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
User                        STRING(3)
Notes                       STRING(255)
                         END
                     END                       

JOBSOBF              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jof),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jof:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jof:RefNumber),DUP,NOCASE
StatusRefNumberKey       KEY(jof:Status,jof:RefNumber),DUP,NOCASE
StatusIMEINumberKey      KEY(jof:Status,jof:IMEINumber),DUP,NOCASE
HeadAccountCompletedKey  KEY(jof:HeadAccountNumber,jof:DateCompleted,jof:RefNumber),DUP,NOCASE
HeadAccountProcessedKey  KEY(jof:HeadAccountNumber,jof:DateProcessed,jof:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IMEINumber                  STRING(30)
Status                      BYTE
Replacement                 BYTE
StoreReferenceNumber        STRING(30)
RNumber                     STRING(30)
RejectionReason             STRING(255)
ReplacementIMEI             STRING(30)
LAccountNumber              STRING(30)
UserCode                    STRING(3)
HeadAccountNumber           STRING(30)
DateCompleted               DATE
TimeCompleted               TIME
DateProcessed               DATE
TimeProcessed               TIME
                         END
                     END                       

JOBSINV              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSINV.DAT'),PRE(jov),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jov:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jov:RefNumber,jov:RecordNumber),DUP,NOCASE
DateCreatedKey           KEY(jov:RefNumber,jov:DateCreated,jov:TimeCreated),DUP,NOCASE
DateCreatedOnlyKey       KEY(jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE
TypeRecordKey            KEY(jov:RefNumber,jov:Type,jov:RecordNumber),DUP,NOCASE
TypeSuffixKey            KEY(jov:RefNumber,jov:Type,jov:Suffix),DUP,NOCASE
InvoiceNumberKey         KEY(jov:InvoiceNumber,jov:RecordNumber),DUP,NOCASE
InvoiceTypeKey           KEY(jov:InvoiceNumber,jov:Type,jov:RecordNumber),DUP,NOCASE
BookingDateTypeKey       KEY(jov:BookingAccount,jov:Type,jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE
TypeDateKey              KEY(jov:Type,jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
InvoiceNumber               LONG
Type                        STRING(1)
Suffix                      STRING(1)
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
OriginalTotalCost           REAL
NewTotalCost                REAL
CreditAmount                REAL
BookingAccount              STRING(30)
NewInvoiceNumber            STRING(30)
ChargeType                  STRING(30)
RepairType                  STRING(30)
HandlingFee                 REAL
ExchangeRate                REAL
ARCCharge                   REAL
RRCLostLoanCost             REAL
RRCPartsCost                REAL
RRCPartsSelling             REAL
RRCLabour                   REAL
ARCMarkUp                   REAL
RRCVAT                      REAL
Paid                        REAL
Outstanding                 REAL
Refund                      REAL
                         END
                     END                       

JOBSCONS             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSCONS.DAT'),PRE(joc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joc:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(joc:RefNumber,joc:TheDate,joc:TheTime),DUP,NOCASE
ConsignmentNumberKey     KEY(joc:ConsignmentNumber,joc:RefNumber),DUP,NOCASE
DespatchFromDateKey      KEY(joc:DespatchFrom,joc:TheDate,joc:RefNumber),DUP,NOCASE
DateOnlyKey              KEY(joc:TheDate,joc:RefNumber),DUP,NOCASE
CourierKey               KEY(joc:Courier,joc:RefNumber),DUP,NOCASE
DespatchFromCourierKey   KEY(joc:DespatchFrom,joc:Courier,joc:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(3)
DespatchFrom                STRING(30)
DespatchTo                  STRING(30)
Courier                     STRING(30)
ConsignmentNumber           STRING(30)
DespatchType                STRING(3)
                         END
                     END                       

JOBSWARR             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSWARR.DAT'),PRE(jow),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jow:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jow:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:RefNumber),DUP,NOCASE
StatusManKey             KEY(jow:Status,jow:Manufacturer,jow:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(jow:Status,jow:Manufacturer,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(jow:RepairedAt,jow:RRCStatus,jow:DateReconciled,jow:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(jow:BranchID,jow:RepairedAt,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(jow:BranchID,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:DateAccepted,jow:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(jow:BranchID,jow:DateAccepted,jow:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(jow:DateAccepted,jow:RefNumber),DUP,NOCASE
RejectedBranchKey        KEY(jow:BranchID,jow:DateRejected,jow:RefNumber),DUP,NOCASE
RejectedKey              KEY(jow:DateRejected,jow:RefNumber),DUP,NOCASE
FinalRejectionBranchKey  KEY(jow:BranchID,jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE
FinalRejectionKey        KEY(jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
DateRejected                DATE
DateFinalRejection          DATE
Orig_Sub_Date               DATE
                         END
                     END                       

CURRENCY             FILE,DRIVER('Btrieve'),OEM,NAME('CURRENCY.DAT'),PRE(cur),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cur:RecordNumber),NOCASE,PRIMARY
CurrencyCodeKey          KEY(cur:CurrencyCode),NOCASE
LastUpdateDateKey        KEY(cur:LastUpdateDate,cur:CurrencyCode),DUP,NOCASE
CorrelationCodeKey       KEY(cur:CorrelationCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
CurrencyCode                STRING(30)
Description                 STRING(60)
DailyRate                   REAL
DivideMultiply              STRING(1)
LastUpdateDate              DATE
CorrelationCode             STRING(30)
                         END
                     END                       

STOCKRECEIVETMP      FILE,DRIVER('Btrieve'),OEM,NAME(glo:FileName),PRE(stotmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stotmp:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(stotmp:SessionID,stotmp:PartNumber),DUP,NOCASE
ReceivedPartNumberKey    KEY(stotmp:SessionID,stotmp:Received,stotmp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartNumber                  STRING(30)
Description                 STRING(30)
ItemCost                    REAL
Quantity                    LONG
QuantityReceived            LONG
RESRecordNumber             LONG
ExchangeOrder               BYTE
LoanOrder                   BYTE
Received                    BYTE
SessionID                   LONG
                         END
                     END                       

JOBSE2               FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE2.DAT'),PRE(jobe2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
POPConfirmed                BYTE
ThirdPartyHandlingFee       REAL
InvThirdPartyHandlingFee    REAL
                         END
                     END                       

TRDSPEC              FILE,DRIVER('Btrieve'),NAME('TRDSPEC.DAT'),PRE(tsp),CREATE,BINDABLE,THREAD
Short_Description_Key    KEY(tsp:Short_Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Short_Description           STRING(30)
Long_Description            STRING(255)
                         END
                     END                       

EPSCSV               FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(eps),BINDABLE,THREAD
Record                   RECORD,PRE()
LABEL1                      STRING(32)
LABEL2                      STRING(32)
LABEL3                      STRING(32)
LABEL4                      STRING(32)
LABEL5                      STRING(32)
LABEL6                      STRING(32)
LABEL7                      STRING(32)
LABEL8                      STRING(32)
LABEL9                      STRING(32)
LABEL10                     STRING(32)
LABEL11                     STRING(32)
LABEL12                     STRING(32)
LABEL13                     STRING(32)
LABEL14                     STRING(32)
LABEL15                     STRING(1000)
LABEL16                     STRING(32)
                         END
                     END                       

EXPGENDM             FILE,DRIVER('BASIC'),OEM,NAME(glo:file_name),PRE(exp),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
line1                       STRING(60),DIM(50)
                         END
                     END                       

EDIBATCH             FILE,DRIVER('Btrieve'),NAME('EDIBATCH.DAT'),PRE(ebt),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(ebt:Manufacturer,ebt:Batch_Number),NOCASE
BatchFirstSecondKey      KEY(ebt:Manufacturer,ebt:SecondYearWarranty,ebt:Batch_Number),DUP,NOCASE
Record                   RECORD,PRE()
Batch_Number                REAL
Manufacturer                STRING(30)
Date                        DATE
Time                        TIME
ApprovedDate                DATE
RRCReconciledDate           DATE
SecondYearWarranty          BYTE
                         END
                     END                       

ORDPEND              FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ope),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ope:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ope:Supplier,ope:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ope:Supplier,ope:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ope:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ope:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ope:Awaiting_Stock,ope:Supplier,ope:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ope:PartRecordNumber),DUP,NOCASE
ReqPartNumber            KEY(ope:StockReqNumber,ope:Part_Number),DUP,NOCASE
ReqDescriptionKey        KEY(ope:StockReqNumber,ope:Description),DUP,NOCASE
KeyPrevStoReqNo          KEY(ope:PrevStoReqNo),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
StockReqNumber              LONG
PrevStoReqNo                LONG
                         END
                     END                       

STOHIST              FILE,DRIVER('Btrieve'),NAME('STOHIST.DAT'),PRE(shi),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(shi:Ref_Number,shi:Date),DUP,NOCASE
record_number_key        KEY(shi:Record_Number),NOCASE,PRIMARY
Transaction_Type_Key     KEY(shi:Ref_Number,shi:Transaction_Type,shi:Date),DUP,NOCASE
DateKey                  KEY(shi:Date),DUP,NOCASE
JobNumberKey             KEY(shi:Ref_Number,shi:Transaction_Type,shi:Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
User                        STRING(3)
Transaction_Type            STRING(3)
Despatch_Note_Number        STRING(30)
Job_Number                  LONG
Sales_Number                LONG
Quantity                    REAL
Date                        DATE
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Notes                       STRING(255)
Information                 STRING(255)
StockOnHand                 LONG
                         END
                     END                       

RETTYPES             FILE,DRIVER('Btrieve'),OEM,NAME('RETTYPES.DAT'),PRE(rtt),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rtt:RecordNumber),NOCASE,PRIMARY
ActiveDescriptionKey     KEY(rtt:Active,rtt:Description),DUP,NOCASE
DescriptionKey           KEY(rtt:Description),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Active                      BYTE
Description                 STRING(30)
RejectionText               STRING(255)
UseReturnDays               BYTE
SparesReturnDays            LONG
ExchangeReturnDays          LONG
                         END
                     END                       

NEWFEAT              FILE,DRIVER('Btrieve'),NAME('NEWFEAT.DAT'),PRE(fea),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(fea:Record_Number),NOCASE,PRIMARY
date_key                 KEY(-fea:date,fea:description),DUP,NOCASE
description_Key          KEY(fea:description,fea:date),DUP,NOCASE
DateTypeKey              KEY(fea:ReportType,-fea:date,-fea:description),DUP,NOCASE
DescriptionTypeKey       KEY(fea:ReportType,fea:description),DUP,NOCASE
DescriptionOnlyKey       KEY(fea:description),DUP,NOCASE
RefNoKey                 KEY(fea:RefNumber),DUP,NOCASE
RefNoTypeKey             KEY(fea:ReportType,fea:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
date                        DATE
description                 STRING(60)
Text                        STRING(1000)
ReportType                  BYTE
RefNumber                   STRING(30)
DocumentPath                STRING(255)
                         END
                     END                       

REPTYDEF             FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(rtd),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rtd:RecordNumber),NOCASE,PRIMARY
ManRepairTypeKey         KEY(rtd:Manufacturer,rtd:Repair_Type),NOCASE
Repair_Type_Key          KEY(rtd:Repair_Type),DUP,NOCASE
Chargeable_Key           KEY(rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(rtd:Warranty,rtd:Repair_Type),DUP,NOCASE
ChaManRepairTypeKey      KEY(rtd:Manufacturer,rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE
WarManRepairTypeKey      KEY(rtd:Manufacturer,rtd:Warranty,rtd:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
ExcludeFromBouncer          BYTE
PromptForExchange           BYTE
JobWeighting                LONG
SkillLevel                  LONG
Manufacturer                STRING(30)
RepairLevel                 LONG
ForceAdjustment             BYTE
ScrapExchange               BYTE
ExcludeHandlingFee          BYTE
NotAvailable                BYTE
NoParts                     STRING(1)
SMSSendType                 STRING(1)
                         END
                     END                       

EPSIMP               FILE,DRIVER('Btrieve'),NAME(glo:file_name2),PRE(epi),BINDABLE,CREATE,THREAD
Record_Number_Key        KEY(epi:Record_Number),NOCASE,PRIMARY
Fault_Description           MEMO(1000)
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Telephone_Collection        STRING(15)
ESN                         STRING(30)
Fault_Code1                 STRING(30)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Postcode_Collection         STRING(10)
Order_Number                STRING(30)
Passed                      STRING(3)
                         END
                     END                       

RETACCOUNTSLIST      FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name),PRE(retacc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(retacc:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(retacc:RefNumber,retacc:AccountNumber),DUP,NOCASE
DateOrderedKey           KEY(retacc:RefNumber,retacc:DateOrdered,retacc:AccountNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccountNumber               STRING(30)
CompanyName                 STRING(30)
DateOrdered                 DATE
QuantityOrdered             LONG
QuantityToShip              LONG
SaleNumber                  LONG
OrigRecordNumber            LONG
SaleType                    STRING(4)
                         END
                     END                       

PRIORITY             FILE,DRIVER('Btrieve'),NAME('PRIORITY.DAT'),PRE(pri),CREATE,BINDABLE,THREAD
Priority_Type_Key        KEY(pri:Priority_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Priority_Type               STRING(30)
Time                        REAL
Book_Before                 TIME
                         END
                     END                       

DEFEPS               FILE,DRIVER('Btrieve'),NAME('DEFEPS.DAT'),PRE(dee),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(dee:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               REAL
Account_Number              STRING(15)
Warranty_Charge_Type        STRING(30)
Location                    STRING(30)
Transit_Type                STRING(30)
Job_Priority                STRING(30)
Unit_Type                   STRING(30)
Status_Type                 STRING(30)
Delivery_Text               STRING(1000)
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE
WarrStatusDateKey        KEY(jobe:WarrantyStatusDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
HubRepair                   BYTE
Network                     STRING(30)
POPConfirmed                BYTE
HubRepairDate               DATE
HubRepairTime               TIME
ClaimValue                  REAL
HandlingFee                 REAL
ExchangeRate                REAL
InvoiceClaimValue           REAL
InvoiceHandlingFee          REAL
InvoiceExchangeRate         REAL
BoxESN                      STRING(20)
ValidPOP                    STRING(3)
ReturnDate                  DATE
TalkTime                    REAL
OriginalPackaging           BYTE
OriginalBattery             BYTE
OriginalCharger             BYTE
OriginalAntenna             BYTE
OriginalManuals             BYTE
PhysicalDamage              BYTE
OriginalDealer              CSTRING(255)
BranchOfReturn              STRING(30)
COverwriteRepairType        BYTE
WOverwriteRepairType        BYTE
LabourAdjustment            REAL
PartsAdjustment             REAL
SubTotalAdjustment          REAL
IgnoreClaimCosts            BYTE
RRCCLabourCost              REAL
RRCCPartsCost               REAL
RRCCPartsSale               REAL
RRCCSubTotal                REAL
InvRRCCLabourCost           REAL
InvRRCCPartsCost            REAL
InvRRCCPartsSale            REAL
InvRRCCSubTotal             REAL
RRCWLabourCost              REAL
RRCWPartsCost               REAL
RRCWPartsSale               REAL
RRCWSubTotal                REAL
InvRRCWLabourCost           REAL
InvRRCWPartsCost            REAL
InvRRCWPartsSale            REAL
InvRRCWSubTotal             REAL
ARC3rdPartyCost             REAL
InvARC3rdPartCost           REAL
WebJob                      BYTE
RRCELabourCost              REAL
RRCEPartsCost               REAL
RRCESubTotal                REAL
IgnoreRRCChaCosts           REAL
IgnoreRRCWarCosts           REAL
IgnoreRRCEstCosts           REAL
OBFvalidated                BYTE
OBFvalidateDate             DATE
OBFvalidateTime             TIME
DespatchType                STRING(3)
Despatched                  STRING(3)
WarrantyClaimStatus         STRING(30)
WarrantyStatusDate          DATE
InSecurityPackNo            STRING(30)
JobSecurityPackNo           STRING(30)
ExcSecurityPackNo           STRING(30)
LoaSecurityPackNo           STRING(30)
ExceedWarrantyRepairLimit   BYTE
BouncerClaim                BYTE
Sub_Sub_Account             STRING(15)
ConfirmClaimAdjustment      BYTE
ARC3rdPartyVAT              REAL
ARC3rdPartyInvoiceNumber    STRING(30)
ExchangeAdjustment          REAL
ExchangedATRRC              BYTE
EndUserTelNo                STRING(15)
ClaimColour                 BYTE
ARC3rdPartyMarkup           REAL
Ignore3rdPartyCosts         BYTE
POPType                     STRING(30)
OBFProcessed                BYTE
LoanReplacementValue        REAL
PendingClaimColour          BYTE
AccessoryNotes              STRING(255)
ClaimPartsCost              REAL
InvClaimPartsCost           REAL
Booking48HourOption         BYTE
Engineer48HourOption        BYTE
ExcReplcamentCharge         BYTE
SecondExchangeNumber        LONG
SecondExchangeStatus        STRING(30)
VatNumber                   STRING(30)
ExchangeProductCode         STRING(30)
SecondExcProdCode           STRING(30)
ARC3rdPartyInvoiceDate      DATE
ARC3rdPartyWaybillNo        STRING(30)
ARC3rdPartyRepairType       STRING(30)
ARC3rdPartyRejectedReason   STRING(255)
ARC3rdPartyRejectedAmount   REAL
VSACustomer                 BYTE
HandsetReplacmentValue      REAL
SecondHandsetRepValue       REAL
t                           STRING(20)
                         END
                     END                       

REPEXTTP             FILE,DRIVER('Btrieve'),OEM,NAME('REPEXTTP.DAT'),PRE(rpt),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpt:RecordNumber),NOCASE,PRIMARY
ReportTypeKey            KEY(rpt:ReportType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportType                  STRING(80)
                         END
                     END                       

MANFAULT             FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(maf),CREATE,BINDABLE,THREAD
Field_Number_Key         KEY(maf:Manufacturer,maf:Field_Number),NOCASE,PRIMARY
MainFaultKey             KEY(maf:Manufacturer,maf:MainFault),DUP,NOCASE
InFaultKey               KEY(maf:Manufacturer,maf:InFault),DUP,NOCASE
ScreenOrderKey           KEY(maf:Manufacturer,maf:ScreenOrder),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
MainFault                   BYTE
PromptForExchange           BYTE
InFault                     BYTE
CompulsoryIfExchange        BYTE
NotAvailable                BYTE
CharCompulsory              BYTE
CharCompulsoryBooking       BYTE
ScreenOrder                 LONG
RestrictAvailability        BYTE
RestrictServiceCentre       BYTE
GenericFault                BYTE
NotCompulsoryThirdParty     BYTE
HideThirdParty              BYTE
HideRelatedCodeIfBlank      BYTE
BlankRelatedCode            LONG
FillFromDOP                 BYTE
CompulsoryForRepairType     BYTE
CompulsoryRepairType        STRING(30)
                         END
                     END                       

INVPARTS             FILE,DRIVER('Btrieve'),OEM,NAME('INVPARTS.DAT'),PRE(ivp),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(ivp:RecordNumber),NOCASE,PRIMARY
InvoiceNoKey             KEY(ivp:InvoiceNumber,ivp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
InvoiceNumber               LONG
RetstockNumber              LONG
CreditQuantity              LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
JobNumber                   LONG
                         END
                     END                       

JOBSEARC             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(jobser),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobser:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobser:RefNumber),DUP,NOCASE
ConsignmentNoKey         KEY(jobser:ConsignmentNumber,jobser:RefNumber),DUP,NOCASE
AccountNoKey             KEY(jobser:AccountNumber),DUP,NOCASE
CourierKey               KEY(jobser:Courier),DUP,NOCASE
IMEIKey                  KEY(jobser:IMEI),DUP,NOCASE
JobTypeKey               KEY(jobser:JobType,jobser:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ConsignmentNumber           STRING(30)
JobType                     STRING(30)
IMEI                        STRING(30)
Courier                     STRING(30)
AccountNumber               STRING(30)
                         END
                     END                       

POPTYPES             FILE,DRIVER('Btrieve'),OEM,NAME('POPTYPES.DAT'),PRE(pop),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(pop:RecordNumber),NOCASE,PRIMARY
POPTypeKey               KEY(pop:POPType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
POPType                     STRING(60)
Result                      STRING(30)
                         END
                     END                       

INVPATMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(ivptmp),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(ivptmp:RecordNumber),NOCASE,PRIMARY
InvoiceNoKey             KEY(ivptmp:InvoiceNumber,ivptmp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
InvoiceNumber               LONG
RetstockNumber              LONG
CreditQuantity              LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
JobNumber                   LONG
                         END
                     END                       

TRACHAR              FILE,DRIVER('Btrieve'),NAME('TRACHAR.DAT'),PRE(tch),CREATE,BINDABLE,THREAD
Account_Number_Key       KEY(tch:Account_Number,tch:Charge_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
                         END
                     END                       

CITYSERV             FILE,DRIVER('Btrieve'),OEM,NAME('CITYSERV.DAT'),PRE(cit),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(cit:RefNumber),NOCASE,PRIMARY
ServiceKey               KEY(cit:Service),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Service                     STRING(1)
Description                 STRING(30)
                         END
                     END                       

DISCOUNT             FILE,DRIVER('Btrieve'),NAME('DISCOUNT.DAT'),PRE(dis),CREATE,BINDABLE,THREAD
Discount_Code_Key        KEY(dis:Discount_Code),NOCASE,PRIMARY
Record                   RECORD,PRE()
Discount_Code               STRING(2)
Discount_Rate               REAL
                         END
                     END                       

LOCVALUE             FILE,DRIVER('Btrieve'),OEM,NAME('LOCVALUE.DAT'),PRE(lov),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lov:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(lov:Location,lov:TheDate),DUP,NOCASE
DateOnly                 KEY(lov:TheDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
TheDate                     DATE
PurchaseTotal               REAL
SaleCostTotal               REAL
RetailCostTotal             REAL
QuantityTotal               LONG
                         END
                     END                       

STOCKTYP             FILE,DRIVER('Btrieve'),NAME('STOCKTYP.DAT'),PRE(stp),CREATE,BINDABLE,THREAD
Stock_Type_Key           KEY(stp:Stock_Type),NOCASE,PRIMARY
Use_Loan_Key             KEY(stp:Use_Loan,stp:Stock_Type),DUP,NOCASE
Use_Exchange_Key         KEY(stp:Use_Exchange,stp:Stock_Type),DUP,NOCASE
Record                   RECORD,PRE()
Use_Loan                    STRING(3)
Use_Exchange                STRING(3)
Stock_Type                  STRING(30)
Available                   BYTE
FranchiseViewOnly           BYTE
                         END
                     END                       

COURIER              FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(cou),CREATE,BINDABLE,THREAD
Courier_Key              KEY(cou:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(cou:Courier_Type,cou:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
PrintWaybill                BYTE
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             BYTE
IncludeSunday               BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
EmailAddress                STRING(255)
FromEmailAddress            STRING(255)
                         END
                     END                       

TRDPARTY             FILE,DRIVER('Btrieve'),NAME('TRDPARTY.DAT'),PRE(trd),CREATE,BINDABLE,THREAD
Company_Name_Key         KEY(trd:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(trd:Account_Number),NOCASE
Special_Instructions_Key KEY(trd:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(trd:Courier),DUP,NOCASE
DeactivateCompanyKey     KEY(trd:Deactivate,trd:Company_Name),DUP,NOCASE
ASCIDKey                 KEY(trd:ASCID),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
VATRate                     REAL
Markup                      REAL
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
Deactivate                  BYTE
LHubAccount                 BYTE
ASCID                       STRING(30)
PreCompletionClaiming       BYTE
EVO_AccNumber               STRING(20)
EVO_VendorNumber            STRING(20)
EVO_Profit_Centre           STRING(30)
EVO_Excluded                BYTE
                         END
                     END                       

JOBSTMP              FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(jobpre),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jobpre:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
TransitType                 STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
ModelNumber                 STRING(30)
Manufacturer                STRING(30)
UnitType                    STRING(30)
Colour                      STRING(30)
DOP                         DATE
MobileNumber                STRING(30)
Location                    STRING(30)
AccountNumber               STRING(30)
OrderNumber                 STRING(30)
ChargeableJob               STRING(3)
WarrantyJob                 STRING(3)
CChargeType                 STRING(30)
WChargeType                 STRING(30)
Intermittent                STRING(3)
InCourier                   STRING(30)
InConsignNo                 STRING(30)
InDate                      DATE
Title                       STRING(5)
Initial                     STRING(1)
Surname                     STRING(30)
CompanyName                 STRING(30)
Postcode                    STRING(15)
Address1                    STRING(30)
Address2                    STRING(30)
Address3                    STRING(30)
Telephone                   STRING(30)
Fax                         STRING(30)
CCompanyName                STRING(30)
CPostcode                   STRING(15)
CAddress1                   STRING(30)
CAddress2                   STRING(30)
CAddress3                   STRING(30)
CTelephone                  STRING(30)
DCompanyName                STRING(30)
DPostcode                   STRING(15)
DAddress1                   STRING(30)
DAddress2                   STRING(30)
DAddress3                   STRING(30)
DTelephone                  STRING(30)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(255)
FaultCode11                 STRING(255)
FaultCode12                 STRING(255)
FaultText                   STRING(255)
EngineerText                STRING(255)
                         END
                     END                       

HANDOVER             FILE,DRIVER('Btrieve'),OEM,NAME('HANDOVER.DAT'),PRE(han),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(han:RecordNumber),NOCASE,PRIMARY
RRCRecordNumberKey       KEY(han:RRCAccountNumber,han:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
RRCAccountNumber            STRING(30)
HandoverType                STRING(3)
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

PROINV               FILE,DRIVER('Btrieve'),OEM,NAME('PROINV.DAT'),PRE(prv),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(prv:RefNumber),NOCASE,PRIMARY
AccountKey               KEY(prv:Invoiced,prv:AccountNumber,prv:BatchNumber),DUP,NOCASE
AccStatusKey             KEY(prv:Status,prv:AccountNumber,prv:BatchNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
AccountNumber               STRING(15)
AccountName                 STRING(30)
AccountType                 STRING(3)
BatchNumber                 LONG
DateCreated                 DATE
User                        STRING(3)
BatchValue                  REAL
NoOfJobs                    LONG
Status                      STRING(3)
Invoiced                    STRING(3)
                         END
                     END                       

EXPBUS               FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(exb),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Job_Date                    STRING(9)
Customer_Name               STRING(31)
Address_Line1               STRING(31)
Address_Line2               STRING(31)
Town                        STRING(31)
Postcode                    STRING(31)
Contact_Name                STRING(31)
Telephone_Number            STRING(21)
Business_Service            STRING(5)
Ref_Number                  STRING(21)
Alt_Ref                     STRING(21)
Items                       STRING(11)
Weight                      STRING(11)
Spec_1                      STRING(51)
Spec_2                      STRING(51)
Comp_No                     STRING(2)
                         END
                     END                       

IMPCITY              FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(imc),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
ref_number                  STRING(30)
consignment_number          STRING(30)
date                        STRING(20)
                         END
                     END                       

JOBACTMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(jactmp),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jactmp:Record_Number),NOCASE,PRIMARY
Accessory_Key            KEY(jactmp:Accessory),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Accessory                   STRING(30)
                         END
                     END                       

RETPARTSLIST         FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name2),PRE(retpar),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(retpar:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(retpar:Processed,retpar:PartNumber),DUP,NOCASE
LocPartNumberKey         KEY(retpar:Location,retpar:Processed,retpar:PartNumber),DUP,NOCASE
LocDescriptionKey        KEY(retpar:Location,retpar:Processed,retpar:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartRefNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Location                    STRING(30)
QuantityStock               LONG
QuantityBackOrder           LONG
QuantityOnOrder             LONG
Processed                   BYTE
                         END
                     END                       

TRANTYPE             FILE,DRIVER('Btrieve'),NAME('TRANTYPE.DAT'),PRE(trt),CREATE,BINDABLE,THREAD
Transit_Type_Key         KEY(trt:Transit_Type),NOCASE,PRIMARY
RRCKey                   KEY(trt:RRC,trt:Transit_Type),DUP,NOCASE
ARCKey                   KEY(trt:ARC,trt:Transit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Transit_Type                STRING(30)
Courier_Collection_Report   STRING(3)
Collection_Address          STRING(3)
Delivery_Address            STRING(3)
Loan_Unit                   STRING(3)
Exchange_Unit               STRING(3)
Force_DOP                   STRING(3)
Force_Location              STRING(3)
Skip_Workshop               STRING(3)
HideLocation                BYTE
InternalLocation            STRING(30)
Workshop_Label              STRING(3)
Job_Card                    STRING(3)
JobReceipt                  STRING(3)
Initial_Status              STRING(30)
Initial_Priority            STRING(30)
ExchangeStatus              STRING(30)
LoanStatus                  STRING(30)
Location                    STRING(3)
ANCCollNote                 STRING(3)
InWorkshop                  STRING(3)
HubRepair                   BYTE
ARC                         BYTE
RRC                         BYTE
OBF                         BYTE
WaybillComBook              BYTE
WaybillComComp              BYTE
                         END
                     END                       

MANFAURL             FILE,DRIVER('Btrieve'),OEM,NAME('MANFAURL.DAT'),PRE(mnr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mnr:RecordNumber),NOCASE,PRIMARY
FieldKey                 KEY(mnr:MANFAULORecordNumber,mnr:PartFaultCode,mnr:FieldNumber),DUP,NOCASE
LinkedRecordNumberKey    KEY(mnr:MANFAULORecordNumber,mnr:PartFaultCode,mnr:LinkedRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
MANFAULORecordNumber        LONG
FieldNumber                 LONG
LinkedRecordNumber          LONG
PartFaultCode               BYTE
RelatedPartFaultCode        LONG
                         END
                     END                       

DESBATCH             FILE,DRIVER('Btrieve'),NAME('DESBATCH.DAT'),PRE(dbt),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(dbt:Batch_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Batch_Number                REAL
Date                        DATE
Time                        TIME
                         END
                     END                       

MODELCCT             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCCT.DAT'),PRE(mcc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mcc:RecordNumber),NOCASE,PRIMARY
CCTRefKey                KEY(mcc:ModelNumber,mcc:CCTReferenceNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
CCTReferenceNumber          STRING(30)
                         END
                     END                       

MANFAUEX             FILE,DRIVER('Btrieve'),OEM,NAME('MANFAUEX.DAT'),PRE(max),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(max:RecordNumber),NOCASE,PRIMARY
ModelNumberKey           KEY(max:MANFAULORecordNumber,max:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
MANFAULORecordNumber        LONG
ModelNumber                 STRING(30)
                         END
                     END                       

MANFPARL             FILE,DRIVER('Btrieve'),OEM,NAME('MANFPARL.DAT'),PRE(mpr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mpr:RecordNumber),NOCASE,PRIMARY
FieldKey                 KEY(mpr:MANFPALORecordNumber,mpr:JobFaultCode,mpr:FieldNumber),DUP,NOCASE
LinkedRecordNumberKey    KEY(mpr:MANFPALORecordNumber,mpr:JobFaultCode,mpr:LinkedRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
MANFPALORecordNumber        LONG
FieldNumber                 LONG
LinkedRecordNumber          LONG
JobFaultCode                BYTE
                         END
                     END                       

JOBVODAC             FILE,DRIVER('Btrieve'),NAME('JOBVODAC.DAT'),PRE(jva),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jva:Ref_Number,jva:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

ESNMODEL             FILE,DRIVER('Btrieve'),NAME('ESNMODEL.DAT'),PRE(esn),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(esn:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(esn:ESN,esn:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(esn:ESN),DUP,NOCASE
Model_Number_Key         KEY(esn:Model_Number,esn:ESN),DUP,NOCASE
Manufacturer_Key         KEY(esn:Manufacturer,esn:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(8)
Model_Number                STRING(30)
Manufacturer                STRING(30)
Include48Hour               BYTE
Active                      STRING(1)
                         END
                     END                       

JOBPAYMT             FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jpt:Record_Number),NOCASE,PRIMARY
All_Date_Key             KEY(jpt:Ref_Number,jpt:Date),DUP,NOCASE
Loan_Deposit_Key         KEY(jpt:Ref_Number,jpt:Loan_Deposit,jpt:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
Loan_Deposit                BYTE
                         END
                     END                       

MANREJR              FILE,DRIVER('Btrieve'),OEM,NAME('MANREJR.DAT'),PRE(mar),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mar:RecordNumber),NOCASE,PRIMARY
CodeKey                  KEY(mar:MANRecordNumber,mar:CodeNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
MANRecordNumber             LONG
CodeNumber                  STRING(60)
Description                 STRING(255)
                         END
                     END                       

LOGASSSTTEMP         FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name4),PRE(logasttmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logasttmp:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(logasttmp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
PartRefNumber               LONG
Location                    STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

STOCK                FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(sto:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(sto:Sundry_Item),DUP,NOCASE
Description_Key          KEY(sto:Location,sto:Description),DUP,NOCASE
Description_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Description),DUP,NOCASE
Shelf_Location_Key       KEY(sto:Location,sto:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Supplier_Key             KEY(sto:Location,sto:Suspend,sto:Supplier),DUP,NOCASE
Location_Key             KEY(sto:Location,sto:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(sto:Location,sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(sto:Location,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(sto:Location,sto:Part_Number,sto:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE
SecondLocKey             KEY(sto:Location,sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE
RequestedKey             KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE
DateBookedKey            KEY(sto:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(sto:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(sto:Location,sto:Suspend,sto:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(sto:Location,sto:Suspend,sto:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(sto:Location,sto:Suspend,sto:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(sto:Location,sto:Suspend,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
ExchangeModelKey         KEY(sto:Location,sto:Manufacturer,sto:ExchangeModelNumber),DUP,NOCASE
LoanModelKey             KEY(sto:Location,sto:Manufacturer,sto:LoanModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
ExchangeModelNumber         STRING(30)
LoanUnit                    BYTE
LoanModelNumber             STRING(30)
                         END
                     END                       

LOGGED               FILE,DRIVER('Btrieve'),NAME('LOGGED.DAT'),PRE(log),CREATE,BINDABLE,THREAD
Surname_Key              KEY(log:Surname,log:Date),DUP,NOCASE
record_number_key        KEY(log:record_number),NOCASE,PRIMARY
User_Code_Key            KEY(log:User_Code,log:Time),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
record_number               REAL
Surname                     STRING(30)
Forename                    STRING(30)
Date                        DATE
Time                        TIME
                         END
                     END                       

MODEXCHA             FILE,DRIVER('Btrieve'),OEM,NAME('MODEXCHA.DAT'),PRE(moa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(moa:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(moa:Manufacturer,moa:ModelNumber,moa:AccountNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
AccountNumber               STRING(30)
CompanyName                 STRING(30)
                         END
                     END                       

LOGRTHIS             FILE,DRIVER('Btrieve'),OEM,NAME('LOGRTHIS.DAT'),PRE(lsrh),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lsrh:RefNumber),DUP,NOCASE
DateIMEIKey              KEY(lsrh:Date,lsrh:IMEI),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
IMEI                        STRING(30)
Quantity                    LONG
Date                        DATE
ModelNumber                 STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

ORDWEBPR             FILE,DRIVER('Btrieve'),OEM,NAME('ORDWEBPR.DAT'),PRE(orw),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(orw:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(orw:AccountNumber,orw:PartNumber),DUP,NOCASE
DescriptionKey           KEY(orw:AccountNumber,orw:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
ItemCost                    REAL
                         END
                     END                       

ESREJRES             FILE,DRIVER('Btrieve'),OEM,NAME('ESREJRES.DAT'),PRE(esr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(esr:RecordNumber),NOCASE,PRIMARY
RejectionReasonKey       KEY(esr:RejectionReason),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RejectionReason             STRING(60)
                         END
                     END                       

MANMARK              FILE,DRIVER('Btrieve'),OEM,NAME('MANMARK.DAT'),PRE(mak),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mak:RecordNumber),NOCASE,PRIMARY
SiteLocationKey          KEY(mak:RefNumber,mak:SiteLocation),NOCASE
SiteLocationOnlyKey      KEY(mak:SiteLocation),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
SiteLocation                STRING(30)
InWarrantyMarkup            LONG
                         END
                     END                       

REPSCHMP             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHMP.DAT'),PRE(rpo),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpo:RecordNumber),NOCASE,PRIMARY
ModelNumberKey           KEY(rpo:REPSCHCRRecordNumber,rpo:ModelNumber),DUP,NOCASE
ManufacturerKey          KEY(rpo:REPSCHCRRecordNumber,rpo:Manufacturer,rpo:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
PartNumber1                 STRING(30)
PartNumber2                 STRING(30)
PartNumber3                 STRING(30)
PartNumber4                 STRING(30)
PartNumber5                 STRING(30)
                         END
                     END                       

LOGSTHIS             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTHIS.DAT'),PRE(logsth),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logsth:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logsth:RefNumber),DUP,NOCASE
DateKey                  KEY(logsth:RefNumber,logsth:Date),DUP,NOCASE
StatusDateKey            KEY(logsth:RefNumber,logsth:Status,logsth:Date),DUP,NOCASE
DateStaClubKey           KEY(logsth:Date,logsth:Status,logsth:ModelNumber,logsth:Location,logsth:SMPFNumber,logsth:DespatchNoteNo,logsth:ClubNokia),DUP,NOCASE
RefModelNoKey            KEY(logsth:RefNumber,logsth:ModelNumber),DUP,NOCASE
DespatchNoKey            KEY(logsth:DespatchNo),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Date                        DATE
UserCode                    STRING(3)
StockIn                     LONG
StockOut                    LONG
Status                      STRING(10)
ClubNokia                   STRING(30)
SMPFNumber                  STRING(30)
DespatchNoteNo              STRING(30)
ModelNumber                 STRING(30)
Location                    STRING(30)
OrderRefNo                  STRING(30)
DespatchNo                  LONG
Deletion_Reason             STRING(40)
DummyField                  STRING(6)
                         END
                     END                       

MODELPART            FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:FileName),PRE(model),CREATE,BINDABLE,THREAD
ModelNumberKey           KEY(model:ModelNumber),DUP,NOCASE
ManufacturerKey          KEY(model:Manufacturer,model:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
PartNumber1                 STRING(30)
PartNumber2                 STRING(30)
PartNumber3                 STRING(30)
PartNumber4                 STRING(30)
PartNumber5                 STRING(30)
                         END
                     END                       

LOGASSST             FILE,DRIVER('Btrieve'),OEM,NAME('LOGASSST.DAT'),PRE(logast),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logast:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logast:RefNumber,logast:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PartRefNumber               LONG
Location                    STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

MODPROD              FILE,DRIVER('Btrieve'),OEM,NAME('MODPROD.DAT'),PRE(mop),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mop:RecordNumber),NOCASE,PRIMARY
ProductCodeKey           KEY(mop:ModelNumber,mop:ProductCode),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
ProductCode                 STRING(30)
                         END
                     END                       

LOGSTOLC             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOLC.DAT'),PRE(logstl),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logstl:RefNumber),NOCASE,PRIMARY
LocationKey              KEY(logstl:Location),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Location                    STRING(30)
                         END
                     END                       

LOGTEMP              FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name2),PRE(logtmp),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logtmp:RefNumber),NOCASE,PRIMARY
IMEIKey                  KEY(logtmp:IMEI),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
IMEI                        STRING(30)
Marker                      BYTE
                         END
                     END                       

MANUDATE             FILE,DRIVER('Btrieve'),OEM,NAME('MANUDATE.DAT'),PRE(mad),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mad:RecordNumber),NOCASE,PRIMARY
DateCodeKey              KEY(mad:Manufacturer,mad:DateCode),NOCASE
AlcatelDateCodeKey       KEY(mad:Manufacturer,mad:AlcatelYearCode,mad:AlcatelMonthCode,mad:AlcatelDayCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
DateCode                    STRING(6)
TheYear                     STRING(4)
TheMonth                    STRING(2)
AlcatelYearCode             STRING(1)
AlcatelYear                 STRING(4)
AlcatelMonthCode            STRING(1)
AlcatelMonth                STRING(2)
AlcatelDayCode              STRING(1)
AlcatelDay                  STRING(2)
                         END
                     END                       

LOGSERST             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSERST.DAT'),PRE(logser),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logser:RecordNumber),NOCASE,PRIMARY
ESNKey                   KEY(logser:ESN),DUP,NOCASE
RefNumberKey             KEY(logser:RefNumber,logser:ESN),DUP,NOCASE
ESNStatusKey             KEY(logser:RefNumber,logser:Status,logser:ESN),DUP,NOCASE
NokiaStatusKey           KEY(logser:RefNumber,logser:Status,logser:ClubNokia,logser:ESN),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ESN                         STRING(30)
Status                      STRING(10)
ClubNokia                   STRING(30)
DummyField                  STRING(4)
                         END
                     END                       

LOGDEFLT             FILE,DRIVER('Btrieve'),OEM,NAME('LOGDEFLT.DAT'),PRE(ldef),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ldef:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
                         END
                     END                       

LOGSTLOC             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTLOC.DAT'),PRE(lstl),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lstl:RecordNumber),NOCASE,PRIMARY
RefLocationKey           KEY(lstl:RefNumber,lstl:Location),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Location                    STRING(30)
AvlQuantity                 LONG
DesQuantity                 LONG
DummyField                  STRING(1)
                         END
                     END                       

MULTIDEF             FILE,DRIVER('Btrieve'),OEM,NAME('MULTIDEF.DAT'),PRE(mul),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mul:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(mul:Description),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(255)
ModelNumber                 STRING(30)
UnitType                    STRING(30)
Accessories                 STRING(30)
OrderNumber                 STRING(30)
InternalLocation            STRING(30)
FaultDescription            STRING(255)
DOP                         DATE
ChaChargeType               STRING(30)
ChaRepairType               STRING(30)
WarChargeType               STRING(30)
WarRepairType               STRING(30)
                         END
                     END                       

TRAFAULO             FILE,DRIVER('Btrieve'),NAME('TRAFAULO.DAT'),PRE(tfo),CREATE,BINDABLE,THREAD
Field_Key                KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Field,tfo:Description),NOCASE,PRIMARY
DescriptionKey           KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

WEBJOBNO             FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOBNO.DAT'),PRE(wej),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wej:RecordNumber),NOCASE,PRIMARY
HeadAccountNumberKey     KEY(wej:HeadAccountNumber),DUP,NOCASE
JobNumberKey             KEY(wej:LastWEBJOBNumber),DUP,NOCASE
HeadJobNumberKey         KEY(wej:HeadAccountNumber,wej:LastWEBJOBNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
HeadAccountNumber           STRING(30)
LastWEBJOBNumber            LONG
                         END
                     END                       

TRAFAULT             FILE,DRIVER('Btrieve'),NAME('TRAFAULT.DAT'),PRE(taf),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(taf:RecordNumber),NOCASE,PRIMARY
Field_Number_Key         KEY(taf:AccountNumber,taf:Field_Number),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                     END                       

OBFBROWSE            FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:File_Name),PRE(obftmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(obftmp:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(obftmp:Processed,obftmp:JobNumber),DUP,NOCASE
ManufacturerJobNoKey     KEY(obftmp:Processed,obftmp:Manufacturer,obftmp:JobNumber),DUP,NOCASE
JobNumberOnlyKey         KEY(obftmp:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
Site                        STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
PartsCost                   REAL
LabourCost                  REAL
POPType                     STRING(30)
POPDate                     DATE
Exchanged                   BYTE
Accessory                   BYTE
Processed                   BYTE
                         END
                     END                       

EXMINLEV             FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name),PRE(exm),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(exm:RecordNumber),NOCASE,PRIMARY
ManufacturerKey          KEY(exm:Manufacturer,exm:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(exm:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
MinimumLevel                LONG
Available                   LONG
                         END
                     END                       

ORACLECN             FILE,DRIVER('Btrieve'),OEM,NAME('ORACLECN.DAT'),PRE(orac),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(orac:RecordNumber),NOCASE,PRIMARY
OracleNumberKey          KEY(orac:OracleNumber,orac:AuditRecordNumber),DUP,NOCASE
AuditRecordNumberKey     KEY(orac:AuditRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
OracleNumber                LONG
AuditRecordNumber           LONG
                         END
                     END                       

PENDAUD              FILE,DRIVER('Btrieve'),OEM,PRE(PED),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(PED:RecordNumber),NOCASE,PRIMARY
KeyReqRecordNumber       KEY(PED:ReqRecordNumber),DUP,NOCASE
KeyOrderDateTimeDesc     KEY(PED:OrdOrder_Number,-PED:ActionDate,-PED:ActionTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReqRecordNumber             LONG
OrdOrder_Number             LONG
Action                      STRING(20)
ActionDate                  DATE
ActionTime                  TIME
UserName                    STRING(50)
AuthorisingUser             STRING(50)
                         END
                     END                       

ORACSPEX             FILE,DRIVER('Btrieve'),OEM,NAME('ORACSPEX.DAT'),PRE(ora1),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ora1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(3)
                         END
                     END                       

LOGSTHII             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTHII.DAT'),PRE(logsti),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logsti:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logsti:RefNumber,logsti:IMEI),DUP,NOCASE
IMEIRefNoKey             KEY(logsti:IMEI,logsti:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IMEI                        STRING(30)
DummyField                  STRING(2)
                         END
                     END                       

LOGCLSTE             FILE,DRIVER('Btrieve'),OEM,NAME('LOGCLSTE.DAT'),PRE(logclu),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logclu:RecordNumber),NOCASE,PRIMARY
ClubNokiaKey             KEY(logclu:ClubNokia),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ClubNokia                   STRING(30)
Postcode                    STRING(15)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

RESUBMIT             FILE,DRIVER('Btrieve'),OEM,NAME('RESUBMIT.DAT'),PRE(reb),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(reb:RecordNumber),NOCASE,PRIMARY
TheDateKey               KEY(reb:TheDate,reb:TheTime),DUP,NOCASE
MessageTypeKey           KEY(reb:MessageType,reb:TheDate,reb:TheTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
Usercode                    STRING(3)
TheDate                     DATE
TheTime                     TIME
MessageType                 STRING(3)
Notes                       STRING(255)
                         END
                     END                       

OBFREASN             FILE,DRIVER('Btrieve'),OEM,NAME('OBFREASN.DAT'),PRE(obf),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(obf:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(obf:Description),NOCASE
ActiveDescriptionKey     KEY(obf:Active,obf:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
RejectionText               STRING(255)
Active                      BYTE
                         END
                     END                       

STATCRIT             FILE,DRIVER('Btrieve'),OEM,NAME('STATCRIT.DAT'),PRE(stac),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stac:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(stac:Description),DUP,NOCASE
DescriptionOptionKey     KEY(stac:Description,stac:OptionType),DUP,NOCASE
LocationDescriptionKey   KEY(stac:Location,stac:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
OptionType                  STRING(30)
FieldValue                  STRING(40)
Location                    STRING(30)
                         END
                     END                       

LOGDESNO             FILE,DRIVER('Btrieve'),OEM,NAME('LOGDESNO.DAT'),PRE(ldes),CREATE,BINDABLE,THREAD
DespatchNoKey            KEY(ldes:DespatchNo),NOCASE,PRIMARY
Record                   RECORD,PRE()
DespatchNo                  LONG
Date                        DATE
Time                        TIME
                         END
                     END                       

CPNDPRTS             FILE,DRIVER('Btrieve'),OEM,NAME('CPNDPRTS.DAT'),PRE(tmppen),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tmppen:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(tmppen:UserID,tmppen:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
UserID                      STRING(30)
JobNumber                   LONG
DateBooked                  DATE
AccountNumber               STRING(30)
ModelNumber                 STRING(30)
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
DateOrdered                 DATE
Location                    STRING(30)
Status                      STRING(30)
                         END
                     END                       

LOGSALCD             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSALCD.DAT'),PRE(logsal),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logsal:RefNumber),NOCASE,PRIMARY
SalesCodeKey             KEY(logsal:SalesCode),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

DEFSTOCK             FILE,DRIVER('Btrieve'),NAME('DEFSTOCK.DAT'),PRE(dst),CREATE,BINDABLE,THREAD
record_number_key        KEY(dst:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Use_Site_Location           STRING(3)
record_number               REAL
Site_Location               STRING(30)
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),NAME('JOBLOHIS.DAT'),PRE(jlh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ORACLEEX             FILE,DRIVER('Btrieve'),OEM,NAME('ORACLEEX.DAT'),PRE(ora),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ora:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(3)
                         END
                     END                       

LOAN                 FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(loa:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(loa:ESN),DUP,NOCASE
MSN_Only_Key             KEY(loa:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(loa:Stock_Type,loa:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(loa:Stock_Type,loa:ESN),DUP,NOCASE
MSN_Key                  KEY(loa:Stock_Type,loa:MSN),DUP,NOCASE
ESN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:ESN),DUP,NOCASE
MSN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:MSN),DUP,NOCASE
Ref_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(loa:Available,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(loa:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(loa:Model_Number,loa:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(loa:Available,loa:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(loa:Available,loa:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(loa:Available,loa:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(loa:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(loa:Location,loa:Stock_Type,loa:Available,loa:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(loa:Location,loa:Stock_Type,loa:ESN),DUP,NOCASE
LocIMEIKey               KEY(loa:Location,loa:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(loa:Location,loa:Stock_Type,loa:Available,loa:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(loa:Location,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE
LocRefKey                KEY(loa:Location,loa:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(loa:Location,loa:Stock_Type,loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(loa:Location,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
LocModelKey              KEY(loa:Location,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(loa:Location,loa:Stock_Type,loa:Available,loa:MSN),DUP,NOCASE
LocStockMSNKey           KEY(loa:Location,loa:Stock_Type,loa:MSN),DUP,NOCASE
LocMSNKey                KEY(loa:Location,loa:MSN),DUP,NOCASE
AvailLocIMEI             KEY(loa:Available,loa:Location,loa:ESN),DUP,NOCASE
AvailLocMSN              KEY(loa:Available,loa:Location,loa:MSN),DUP,NOCASE
AvailLocRef              KEY(loa:Available,loa:Location,loa:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(loa:Available,loa:Location,loa:Model_Number),DUP,NOCASE
InTransitLocationKey     KEY(loa:InTransit,loa:Location,loa:ESN),DUP,NOCASE
InTransitKey             KEY(loa:InTransit,loa:ESN),DUP,NOCASE
StatusChangeDateKey      KEY(loa:StatusChangeDate),DUP,NOCASE
LocStatusChangeDateKey   KEY(loa:Location,loa:StatusChangeDate),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
DummyField                  STRING(1)
StatusChangeDate            DATE
InTransit                   BYTE
                         END
                     END                       

NOTESCON             FILE,DRIVER('Btrieve'),NAME('NOTESCON.DAT'),PRE(noc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(noc:RecordNumber),NOCASE,PRIMARY
Notes_Key                KEY(noc:Reference,noc:Notes),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

VATCODE              FILE,DRIVER('Btrieve'),NAME('VATCODE.DAT'),PRE(vat),CREATE,BINDABLE,THREAD
Vat_code_Key             KEY(vat:VAT_Code),NOCASE,PRIMARY
Record                   RECORD,PRE()
VAT_Code                    STRING(2)
VAT_Rate                    REAL
                         END
                     END                       

SMSMAIL              FILE,DRIVER('Btrieve'),OEM,NAME('SMSMAIL.DAT'),PRE(sms),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sms:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(sms:SendToSMS,sms:SMSSent),DUP,NOCASE
EmailSentKey             KEY(sms:SendToEmail,sms:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(sms:SMSSent,sms:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(sms:EmailSent,sms:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(sms:RefNumber,sms:DateInserted,sms:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(sms:MSISDN,-sms:DateSMSSent),DUP,NOCASE
KeyRefDateTimeDec        KEY(sms:RefNumber,-sms:DateInserted,-sms:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
EmailSubject                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
                         END
                     END                       

LOANHIST             FILE,DRIVER('Btrieve'),NAME('LOANHIST.DAT'),PRE(loh),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(loh:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(loh:Ref_Number,-loh:Date,-loh:Time),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
Notes                       STRING(255)
                         END
                     END                       

TURNARND             FILE,DRIVER('Btrieve'),NAME('TURNARND.DAT'),PRE(tur),CREATE,BINDABLE,THREAD
Turnaround_Time_Key      KEY(tur:Turnaround_Time),NOCASE,PRIMARY
Record                   RECORD,PRE()
Turnaround_Time             STRING(30)
Days                        REAL
Hours                       REAL
                         END
                     END                       

ORDJOBS              FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(orjtmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(orjtmp:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(orjtmp:PartNumber,orjtmp:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
OrderNumber                 LONG
PartNumber                  STRING(30)
JobNumber                   LONG
RefNumber                   LONG
CharWarr                    STRING(1)
Quantity                    LONG
                         END
                     END                       

MERGETXT             FILE,DRIVER('Btrieve'),OEM,NAME('MERGETXT'),PRE(mrt),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(mrt:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Letter                      STRING(10000)
                         END
                     END                       

MERGELET             FILE,DRIVER('Btrieve'),NAME('MERGELET.DAT'),PRE(mrg),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(mrg:RefNumber),NOCASE,PRIMARY
LetterNameKey            KEY(mrg:LetterName),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
LetterName                  STRING(30)
UseStatus                   STRING(3)
Status                      STRING(30)
                         END
                     END                       

ORDSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_name2),PRE(orstmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(orstmp:RecordNumber),NOCASE,PRIMARY
LocationKey              KEY(orstmp:PartNumber,orstmp:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
OrderNumber                 LONG
PartNumber                  STRING(30)
Location                    STRING(30)
Quantity                    LONG
                         END
                     END                       

EXCHHIST             FILE,DRIVER('Btrieve'),NAME('EXCHHIST.DAT'),PRE(exh),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(exh:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(exh:Ref_Number,-exh:Date,-exh:Time),DUP,NOCASE
DateStatusKey            KEY(exh:Date,exh:Status,exh:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
record_number               LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
Notes                       STRING(255)
                         END
                     END                       

ACCESSOR             FILE,DRIVER('Btrieve'),NAME('ACCESSOR.DAT'),PRE(acr),CREATE,BINDABLE,THREAD
Accesory_Key             KEY(acr:Model_Number,acr:Accessory),NOCASE,PRIMARY
Model_Number_Key         KEY(acr:Accessory,acr:Model_Number),NOCASE
AccessOnlyKey            KEY(acr:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
Accessory                   STRING(30)
Model_Number                STRING(30)
                         END
                     END                       

SUBACCAD             FILE,DRIVER('Btrieve'),OEM,NAME('SUBACCAD.DAT'),PRE(sua),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sua:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sua:RefNumber,sua:AccountNumber),DUP,NOCASE
CompanyNameKey           KEY(sua:RefNumber,sua:CompanyName),DUP,NOCASE
AccountNumberOnlyKey     KEY(sua:AccountNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccountNumber               STRING(15)
CompanyName                 STRING(30)
Postcode                    STRING(15)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(15)
FaxNumber                   STRING(15)
EmailAddress                STRING(255)
ContactName                 STRING(30)
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(wpr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE
WebOrderKey              KEY(wpr:WebOrder,wpr:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(wpr:PartAllocated,wpr:Part_Number),DUP,NOCASE
StatusKey                KEY(wpr:Status,wpr:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(wpr:PartAllocated,wpr:Status,wpr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
CostAdjustment              REAL
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE
Supplier_Key             KEY(par:Supplier),DUP,NOCASE
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE
WebOrderKey              KEY(par:WebOrder,par:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(par:PartAllocated,par:Part_Number),DUP,NOCASE
StatusKey                KEY(par:Status,par:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(par:PartAllocated,par:Status,par:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),NAME('JOBACC.DAT'),PRE(jac),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY
DamagedKey               KEY(jac:Ref_Number,jac:Damaged,jac:Accessory),DUP,NOCASE
DamagedPirateKey         KEY(jac:Ref_Number,jac:Damaged,jac:Pirate,jac:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
Damaged                     BYTE
Pirate                      BYTE
Attached                    BYTE
                         END
                     END                       

STOCKALL             FILE,DRIVER('Btrieve'),OEM,NAME('STOCKALL.DAT'),PRE(stl),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stl:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(stl:Location,stl:PartNumber),DUP,NOCASE
StatusPartNumberKey      KEY(stl:Location,stl:Status,stl:PartNumber),DUP,NOCASE
DescriptionKey           KEY(stl:Location,stl:Description),DUP,NOCASE
StatusDescriptionKey     KEY(stl:Location,stl:Status,stl:Description),DUP,NOCASE
ShelfLocationKey         KEY(stl:Location,stl:ShelfLocation),DUP,NOCASE
StatusShelfLocationKey   KEY(stl:Location,stl:Status,stl:ShelfLocation),DUP,NOCASE
EngineerKey              KEY(stl:Location,stl:Engineer,stl:JobNumber),DUP,NOCASE
StatusEngineerKey        KEY(stl:Location,stl:Status,stl:Engineer,stl:JobNumber),DUP,NOCASE
JobNumberKey             KEY(stl:Location,stl:JobNumber),DUP,NOCASE
StatusJobNumberKey       KEY(stl:Location,stl:Status,stl:JobNumber),DUP,NOCASE
PartRecordNumberKey      KEY(stl:Location,stl:PartType,stl:PartRecordNumber),DUP,NOCASE
PartRecordTypeKey        KEY(stl:PartType,stl:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
ShelfLocation               STRING(30)
PartNumber                  STRING(30)
Description                 STRING(30)
Engineer                    STRING(3)
Status                      STRING(3)
JobNumber                   LONG
PartRefNumber               LONG
PartRecordNumber            LONG
PartType                    STRING(3)
Quantity                    LONG
                         END
                     END                       

REPSCHLC             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHLC.DAT'),PRE(rpl),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpl:RecordNumber),NOCASE,PRIMARY
LocationKey              KEY(rpl:REPSCHCRRecordNumber,rpl:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
Location                    STRING(30)
                         END
                     END                       

REPSCHCT             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHCT.DAT'),PRE(rpr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpr:RecordNumber),NOCASE,PRIMARY
ChargeTypeKey            KEY(rpr:REPSCHCRRecordNumber,rpr:ChargeType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
ChargeType                  STRING(30)
                         END
                     END                       

USELEVEL             FILE,DRIVER('Btrieve'),NAME('USELEVEL.DAT'),PRE(lev),CREATE,BINDABLE,THREAD
User_Level_Key           KEY(lev:User_Level),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Level                  STRING(30)
                         END
                     END                       

REPSCHMA             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHMA.DAT'),PRE(rpu),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpu:RecordNumber),NOCASE,PRIMARY
ManufacturerKey          KEY(rpu:REPSCHCRRecordNumber,rpu:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
Manufacturer                STRING(30)
                         END
                     END                       

REPSCHSL             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHSL.DAT'),PRE(rpf),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpf:RecordNumber),NOCASE,PRIMARY
LocationKey              KEY(rpf:REPSCHCRRecordNumber,rpf:ShelfLocation),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
ShelfLocation               STRING(30)
                         END
                     END                       

REPSCHST             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHST.DAT'),PRE(rpk),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpk:RecordNumber),NOCASE,PRIMARY
StockTypeKey             KEY(rpk:REPSCHCRRecordNumber,rpk:StockType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
StockType                   STRING(30)
                         END
                     END                       

REPSCHLG             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHLG.DAT'),PRE(rlg),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rlg:RecordNumber),NOCASE,PRIMARY
TheDateKey               KEY(rlg:REPSCHEDRecordNumber,rlg:TheDate,rlg:TheTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHEDRecordNumber        LONG
TheDate                     DATE
TheTime                     TIME
Information                 STRING(255)
                         END
                     END                       

REPSCHCR             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHCR.DAT'),PRE(rpc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpc:RecordNumber),NOCASE,PRIMARY
ReportCriteriaKey        KEY(rpc:ReportName,rpc:ReportCriteriaType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportName                  STRING(60)
ReportCriteriaType          STRING(60)
CharIncReportStyle          BYTE
CharIncZeroSuppression      BYTE
ChaIncReportOrder           BYTE
WarIncAll                   BYTE
WarIncSubmitted             BYTE
WarIncResubmitted           BYTE
WarIncRejected              BYTE
WarIncRejectionAcknowledged BYTE
WarIncReconciled            BYTE
WarIncIncludeARCRepairedJobs BYTE
WarIncSuppressZeros         BYTE
WarIncReportOrder           BYTE
DateRangeType               BYTE
AllAccounts                 BYTE
AllManufacturers            BYTE
AllChargeTypes              BYTE
SelectManufacturer          BYTE
Manufacturer                STRING(30)
LeewayDays                  LONG
NumberOfDays                LONG
IntervalPeriod              LONG
SiteLocation                STRING(30)
IncludeAccessories          BYTE
AllThirdPartySites          BYTE
ThirdPartySite              STRING(30)
AllLocations                BYTE
AllStockTypes               BYTE
IncludeARCRepairedRRCJobs   BYTE
                         END
                     END                       

REPSCHAC             FILE,DRIVER('Btrieve'),OEM,NAME('REPSCHAC.DAT'),PRE(rpa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpa:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(rpa:REPSCHCRRecordNumber,rpa:AccountNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
REPSCHCRRecordNumber        LONG
AccountNumber               STRING(30)
                         END
                     END                       

ALLLEVEL             FILE,DRIVER('Btrieve'),NAME('ALLLEVEL.DAT'),PRE(all),CREATE,BINDABLE,THREAD
Access_Level_Key         KEY(all:Access_Level),NOCASE,PRIMARY
Record                   RECORD,PRE()
Access_Level                STRING(30)
Description                 STRING(500)
ExpertUser                  BYTE
Administrator               BYTE
Engineer                    BYTE
Miscellaneous               BYTE
Accounts                    BYTE
StockControl                BYTE
                         END
                     END                       

ACCAREAS             FILE,DRIVER('Btrieve'),NAME('ACCAREAS.DAT'),PRE(acc),CREATE,BINDABLE,THREAD
Access_level_key         KEY(acc:User_Level,acc:Access_Area),NOCASE,PRIMARY
AccessOnlyKey            KEY(acc:Access_Area),DUP,NOCASE
Record                   RECORD,PRE()
Access_Area                 STRING(30)
User_Level                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(aud),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Type                        STRING(3)
                         END
                     END                       

RAPIDSTOCK           FILE,DRIVER('Btrieve'),OEM,NAME(glo:FileName),PRE(rapsto),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rapsto:RecordNumber),NOCASE,PRIMARY
LocationKey              KEY(rapsto:ShelfLocation,rapsto:SecondLocation),DUP,NOCASE
DescriptionKey           KEY(rapsto:Description),DUP,NOCASE
PartNumberKey            KEY(rapsto:PartNumber),DUP,NOCASE
EngineerKey              KEY(rapsto:Engineer,rapsto:JobNumber),DUP,NOCASE
StatusLocationKey        KEY(rapsto:Status,rapsto:ShelfLocation),DUP,NOCASE
StatusDescriptionKey     KEY(rapsto:Status,rapsto:Description),DUP,NOCASE
StatusPartNumberKey      KEY(rapsto:Status,rapsto:PartNumber),DUP,NOCASE
StatusJobNumberKey       KEY(rapsto:Status,rapsto:Engineer,rapsto:JobNumber),DUP,NOCASE
JobNumberKey             KEY(rapsto:Status,rapsto:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Location                    STRING(30)
ShelfLocation               STRING(30)
SecondLocation              STRING(30)
Engineer                    STRING(3)
JobNumber                   LONG
Quantity                    LONG
PartType                    STRING(1)
PartRecordNumber            LONG
Status                      STRING(3)
StockRefNumber              LONG
                         END
                     END                       

USERS                FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use),CREATE,BINDABLE,THREAD
User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE
Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE
User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE
surname_key              KEY(use:Surname),DUP,NOCASE
password_key             KEY(use:Password),NOCASE
Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE
Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE
Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE
LocationSurnameKey       KEY(use:Location,use:Surname),DUP,NOCASE
LocationForenameKey      KEY(use:Location,use:Forename),DUP,NOCASE
LocActiveSurnameKey      KEY(use:Active,use:Location,use:Surname),DUP,NOCASE
LocActiveForenameKey     KEY(use:Active,use:Location,use:Forename),DUP,NOCASE
TeamStatusKey            KEY(use:IncludeInEngStatus,use:Team,use:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
RenewPassword               LONG
PasswordLastChanged         DATE
RestrictParts               BYTE
RestrictChargeable          BYTE
RestrictWarranty            BYTE
IncludeInEngStatus          BYTE
MobileNumber                STRING(20)
                         END
                     END                       

LOCSHELF             FILE,DRIVER('Btrieve'),NAME('LOCSHELF.DAT'),PRE(los),CREATE,BINDABLE,THREAD
Shelf_Location_Key       KEY(los:Site_Location,los:Shelf_Location),NOCASE,PRIMARY
Record                   RECORD,PRE()
Site_Location               STRING(30)
Shelf_Location              STRING(30)
                         END
                     END                       

DEFAULTS             FILE,DRIVER('Btrieve'),NAME('DEFAULTS.DAT'),PRE(def),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(def:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
                         END
                     END                       

REPTYCAT             FILE,DRIVER('Btrieve'),OEM,NAME('REPTYCAT.DAT'),PRE(repc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(repc:RecordNumber),NOCASE,PRIMARY
RepairTypeKey            KEY(repc:RepairType),DUP,NOCASE
CategoryKey              KEY(repc:Category,repc:RepairType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RepairType                  STRING(30)
Category                    STRING(30)
                         END
                     END                       

REPTYPETEMP          FILE,DRIVER('Btrieve'),OEM,NAME(glo:RepairTypes),PRE(rept),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rept:RecordNumber),NOCASE,PRIMARY
RepairTypeKey            KEY(rept:RepairType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RepairType                  STRING(30)
Cost                        REAL
                         END
                     END                       

RTNORDER             FILE,DRIVER('Btrieve'),OEM,NAME('RTNORDER.DAT'),PRE(rtn),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rtn:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(rtn:Location,rtn:ExchangeOrder,rtn:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(rtn:Location,rtn:ExchangeOrder,rtn:Status,rtn:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:Status,rtn:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(rtn:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(rtn:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(rtn:ExchangeOrder,rtn:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:ReturnType,rtn:Status,rtn:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(rtn:Archived,rtn:Ordered,rtn:Location,rtn:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(rtn:Archived,rtn:Ordered,rtn:WaybillNumber),DUP,NOCASE
ArcLocDateKey            KEY(rtn:Archived,rtn:Location,rtn:DateCreated),DUP,NOCASE
ArcDateKey               KEY(rtn:Archived,rtn:DateCreated),DUP,NOCASE
DateOrderedKey           KEY(rtn:Location,rtn:DateOrdered,rtn:TimeOrdered),DUP,NOCASE
DateOrderedReceivedKey   KEY(rtn:Location,rtn:Received,rtn:DateOrdered,rtn:TimeOrdered),DUP,NOCASE
ArcOrdExcWaybillKey      KEY(rtn:Archived,rtn:Ordered,rtn:ExchangeOrder,rtn:WaybillNumber),DUP,NOCASE
LocArcOrdExcWaybillKey   KEY(rtn:Archived,rtn:Ordered,rtn:Location,rtn:ExchangeOrder,rtn:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
WhoProcessed                STRING(3)
                         END
                     END                       

MANFPALO             FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(mfp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mfp:RecordNumber),NOCASE,PRIMARY
Field_Key                KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Field),NOCASE
DescriptionKey           KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Description),DUP,NOCASE
AvailableFieldKey        KEY(mfp:NotAvailable,mfp:Manufacturer,mfp:Field_Number,mfp:Field),DUP,NOCASE
AvailableDescriptionKey  KEY(mfp:Manufacturer,mfp:NotAvailable,mfp:Field_Number,mfp:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
RestrictLookup              BYTE
RestrictLookupType          BYTE
NotAvailable                BYTE
ForceJobFaultCode           BYTE
ForceFaultCodeNumber        LONG
SetPartFaultCode            BYTE
SelectPartFaultCode         LONG
PartFaultCodeValue          STRING(30)
JobTypeAvailability         BYTE
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),NAME('JOBSTAGE.DAT'),PRE(jst),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Job_Stage                   STRING(30)
Date                        DATE
Time                        TIME
User                        STRING(3)
                         END
                     END                       

SBO_OutParts         FILE,DRIVER('TOPSPEED'),RECLAIM,OEM,NAME(glo:sbo_outparts),PRE(sout),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sout:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(sout:SessionID,sout:PartNumber),DUP,NOCASE
DescriptionKey           KEY(sout:SessionID,sout:Description),DUP,NOCASE
PartDescDateRaisedKey    KEY(sout:SessionID,sout:LineType,sout:PartNumber,sout:Description,sout:DateRaised),DUP,NOCASE
PartDescDateRaisedProcessedKey KEY(sout:SessionID,sout:LineType,sout:PartNumber,sout:Description,sout:DateRaised,sout:DateProcessed),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SessionID                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
DateRaised                  DATE
DateProcessed               DATE
LineType                    STRING(1)
                         END
                     END                       

SBO_GenericFile      FILE,DRIVER('TOPSPEED'),RECLAIM,OEM,NAME(glo:SBO_GenericFile),PRE(sbogen),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sbogen:RecordNumber),NOCASE,PRIMARY
Long1Key                 KEY(sbogen:SessionID,sbogen:Long1),DUP,NOCASE
String1Key               KEY(sbogen:SessionID,sbogen:String1),DUP,NOCASE
Long2Key                 KEY(sbogen:SessionID,sbogen:Long2),DUP,NOCASE
String2Key               KEY(sbogen:SessionID,sbogen:String2),DUP,NOCASE
Long3Key                 KEY(sbogen:SessionID,sbogen:Long3),DUP,NOCASE
String3Key               KEY(sbogen:SessionID,sbogen:String3),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SessionID                   LONG
Long1                       LONG
Long2                       LONG
Long3                       LONG
String1                     STRING(30)
String2                     STRING(30)
String3                     STRING(30)
Byte1                       BYTE
Byte2                       BYTE
Byte3                       BYTE
Real1                       REAL
Real2                       REAL
Real3                       REAL
Notes                       STRING(255)
                         END
                     END                       

MANFAUPA             FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(map),CREATE,BINDABLE,THREAD
Field_Number_Key         KEY(map:Manufacturer,map:Field_Number),NOCASE,PRIMARY
MainFaultKey             KEY(map:Manufacturer,map:MainFault),DUP,NOCASE
ScreenOrderKey           KEY(map:Manufacturer,map:ScreenOrder),DUP,NOCASE
KeyRepairKey             KEY(map:Manufacturer,map:KeyRepair),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
MainFault                   BYTE
UseRelatedJobCode           BYTE
NotAvailable                BYTE
ScreenOrder                 LONG
CopyFromJobFaultCode        BYTE
CopyJobFaultCode            LONG
NAForAccessory              BYTE
CompulsoryForAdjustment     BYTE
KeyRepair                   BYTE
CCTReferenceFaultCode       BYTE
NAForSW                     BYTE
                         END
                     END                       

SMSText              FILE,DRIVER('Btrieve'),OEM,NAME('SMSText.Dat'),PRE(SMT),CREATE,BINDABLE,THREAD
Key_Record_No            KEY(SMT:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMT:Status_Type,SMT:Location,SMT:Trigger_Status),DUP,NOCASE
Key_Description          KEY(SMT:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMT:Status_Type,SMT:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMT:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMT:Location,SMT:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMT:Location,SMT:Duplicate_Estimate),DUP,NOCASE
Key_Location_BER         KEY(SMT:Location,SMT:BER),DUP,NOCASE
Key_Location_Liquid      KEY(SMT:Location,SMT:LiquidDamage),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
LiquidDamage                STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                     END                       

EXPPARTS             FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(exppar),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 STRING(20)
Sale_Cost                     STRING(20)
                            END
Quantity                    STRING(20)
Exclude_From_Order          STRING(20)
Despatch_Note_Number        STRING(30)
Date_Ordered                STRING(20)
Order_Number                STRING(20)
Date_Received               STRING(20)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

REPEXTRP             FILE,DRIVER('Btrieve'),OEM,NAME('REPEXTRP.DAT'),PRE(rex),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rex:RecordNumber),NOCASE,PRIMARY
ReportNameKey            KEY(rex:ReportType,rex:ReportName),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportType                  STRING(80)
ReportName                  STRING(80)
EXEName                     STRING(30)
                         END
                     END                       

GENSHORT             FILE,DRIVER('Btrieve'),NAME('GENSHORT.DAT'),PRE(gens),CREATE,BINDABLE,THREAD
AutoNumber_Key           KEY(gens:Autonumber_Field),NOCASE,PRIMARY
Lock_Down_Key            KEY(gens:Stock_Ref_No,gens:Audit_No),DUP,NOCASE
Record                   RECORD,PRE()
Autonumber_Field            LONG
branch_id                   LONG
Audit_No                    LONG
Stock_Ref_No                LONG
Stock_Qty                   LONG
                         END
                     END                       

LOCINTER             FILE,DRIVER('Btrieve'),NAME('LOCINTER.DAT'),PRE(loi),CREATE,BINDABLE,THREAD
Location_Key             KEY(loi:Location),NOCASE,PRIMARY
Location_Available_Key   KEY(loi:Location_Available,loi:Location),DUP,NOCASE
Record                   RECORD,PRE()
Location                    STRING(30)
Location_Available          STRING(3)
Allocate_Spaces             STRING(3)
Total_Spaces                REAL
Current_Spaces              STRING(6)
                         END
                     END                       

STAHEAD              FILE,DRIVER('Btrieve'),NAME('STAHEAD.DAT'),PRE(sth),CREATE,BINDABLE,THREAD
Heading_Key              KEY(sth:Ref_Number,sth:Heading),NOCASE,PRIMARY
Ref_Number_Key           KEY(sth:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Heading                     STRING(30)
                         END
                     END                       

NOTESINV             FILE,DRIVER('Btrieve'),NAME('NOTESINV.DAT'),PRE(noi),CREATE,BINDABLE,THREAD
Notes_Key                KEY(noi:Reference,noi:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Reference                   STRING(30)
Notes                       STRING(80)
RepairLevel                 LONG
SkillLevel                  LONG
                         END
                     END                       

NOTESFAU             FILE,DRIVER('Btrieve'),NAME('NOTESFAU.DAT'),PRE(nof),CREATE,BINDABLE,THREAD
Notes_Key                KEY(nof:Manufacturer,nof:Reference,nof:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

MANFAULO             FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(mfo),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mfo:RecordNumber),NOCASE,PRIMARY
RelatedFieldKey          KEY(mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Field),DUP,NOCASE
Field_Key                KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Field),DUP,NOCASE
DescriptionKey           KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE
ManFieldKey              KEY(mfo:Manufacturer,mfo:Field),DUP,NOCASE
HideFieldKey             KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:Field_Number,mfo:Field),DUP,NOCASE
HideDescriptionKey       KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE
RelatedDescriptionKey    KEY(mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Description),DUP,NOCASE
HideRelatedFieldKey      KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Field),DUP,NOCASE
HideRelatedDescKey       KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Description),DUP,NOCASE
FieldNumberKey           KEY(mfo:Manufacturer,mfo:Field_Number),DUP,NOCASE
PrimaryLookupKey         KEY(mfo:Manufacturer,mfo:Field_Number,mfo:PrimaryLookup),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
ImportanceLevel             LONG
SkillLevel                  LONG
RepairType                  STRING(30)
RepairTypeWarranty          STRING(30)
HideFromEngineer            BYTE
ForcePartCode               LONG
RelatedPartCode             LONG
PromptForExchange           BYTE
ExcludeFromBouncer          BYTE
ReturnToRRC                 BYTE
RestrictLookup              BYTE
RestrictLookupType          BYTE
NotAvailable                BYTE
PrimaryLookup               BYTE
SetJobFaultCode             BYTE
SelectJobFaultCode          LONG
JobFaultCodeValue           STRING(30)
JobTypeAvailability         BYTE
                         END
                     END                       

STATREP              FILE,DRIVER('Btrieve'),OEM,NAME('STATREP.DAT'),PRE(star),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(star:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(star:Description),DUP,NOCASE
LocationDescriptionKey   KEY(star:Location,star:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
Location                    STRING(30)
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),NAME('JOBEXHIS.DAT'),PRE(jxh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

STARECIP             FILE,DRIVER('Btrieve'),OEM,NAME('STARECIP.DAT'),PRE(str),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(str:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(str:RefNumber,str:RecipientType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
                         END
                     END                       

STOPARTS             FILE,DRIVER('Btrieve'),OEM,NAME('STOPARTS.DAT'),PRE(spt),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(spt:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(spt:STOCKRefNumber,spt:DateChanged,spt:RecordNumber),DUP,NOCASE
LocationDateKey          KEY(spt:Location,spt:DateChanged,spt:RecordNumber),DUP,NOCASE
LocationNewPartKey       KEY(spt:Location,spt:NewPartNumber,spt:NewDescription),DUP,NOCASE
LocationOldPartKey       KEY(spt:Location,spt:OldPartNumber,spt:OldDescription),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
STOCKRefNumber              LONG
Location                    STRING(30)
DateChanged                 DATE
TimeChanged                 TIME
UserCode                    STRING(3)
OldPartNumber               STRING(30)
OldDescription              STRING(30)
NewPartNumber               STRING(30)
NewDescription              STRING(30)
Notes                       STRING(255)
                         END
                     END                       

RETSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(res),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(res:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(res:Ref_Number,res:Part_Number),DUP,NOCASE
Despatched_Key           KEY(res:Ref_Number,res:Despatched,res:Despatch_Note_Number,res:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(res:Ref_Number,res:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(res:Ref_Number,res:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(res:Ref_Number,res:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(res:Ref_Number,res:Order_Number),DUP,NOCASE
DespatchedKey            KEY(res:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(res:Ref_Number,res:Despatched,res:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(res:Amend_Site_Loc,res:Amended,res:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(res:Ref_Number,res:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(res:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(res:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(res:Despatched,res:Part_Number),DUP,NOCASE
ExchangeRefNumberKey     KEY(res:Ref_Number,res:ExchangeRefNumber),DUP,NOCASE
LoanRefNumberKey         KEY(res:Ref_Number,res:LoanRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
ExchangeRefNumber           LONG
LoanRefNumber               LONG
                         END
                     END                       

STOCKALX             FILE,DRIVER('Btrieve'),OEM,PRE(STLX),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(STLX:RecordNo),NOCASE,PRIMARY
KeySTLRecordNumber       KEY(STLX:StlRecordNumber),DUP,NOCASE
KeyRequestDateTime       KEY(STLX:RequestDate,STLX:RequestTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
StlRecordNumber             LONG
JobNo                       LONG
PartNo                      STRING(30)
Description                 STRING(30)
RequestDate                 DATE
RequestTime                 TIME
AllocateDate                DATE
AllocateTime                TIME
AllocateUser                STRING(3)
                         END
                     END                       

RETPAY               FILE,DRIVER('Btrieve'),OEM,NAME('RETPAY.DAT'),PRE(rtp),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(rtp:Record_Number),NOCASE,PRIMARY
Date_Key                 KEY(rtp:Ref_Number,rtp:Date,rtp:Payment_Type),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
Authorisation_Code          STRING(30)
Card_Holder                 STRING(30)
                         END
                     END                       

RETDESNO             FILE,DRIVER('Btrieve'),OEM,NAME('RETDESNO.DAT'),PRE(rdn),CREATE,BINDABLE,THREAD
Despatch_Number_Key      KEY(rdn:Despatch_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Despatch_Number             LONG
Date                        DATE
Time                        TIME
Consignment_Number          STRING(30)
Courier                     STRING(30)
Sale_Number                 LONG
                         END
                     END                       

ORDPARTS             FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(orp),CREATE,BINDABLE,THREAD
Order_Number_Key         KEY(orp:Order_Number,orp:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(orp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(orp:Part_Ref_Number,orp:Part_Number,orp:Description),DUP,NOCASE
Part_Number_Key          KEY(orp:Order_Number,orp:Part_Number,orp:Description),DUP,NOCASE
Description_Key          KEY(orp:Order_Number,orp:Description,orp:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(orp:All_Received,orp:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(orp:Account_Number,orp:Part_Type,orp:All_Received,orp:Part_Number),DUP,NOCASE
Allocated_Key            KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Account_Number,orp:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(orp:Order_Number,orp:Part_Type,orp:All_Received,orp:Part_Number,orp:Description),DUP,NOCASE
PartRecordNumberKey      KEY(orp:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
GRN_Number                  LONG
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
ReceivedCurrency            STRING(30)
ReceivedDailyRate           REAL
ReceivedDivideMultiply      STRING(1)
FreeExchangeStock           BYTE
TimeReceived                TIME
DatePriceCaptured           DATE
TimePriceCaptured           TIME
UncapturedGRNNumber         LONG
                         END
                     END                       

WAYBILLS             FILE,DRIVER('Btrieve'),OEM,NAME('WAYBILLS.DAT'),PRE(way),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(way:RecordNumber),NOCASE,PRIMARY
WayBillNumberKey         KEY(way:WayBillNumber),NOCASE
RecWayBillNoKey          KEY(way:Received,way:WayBillNumber),DUP,NOCASE
TypeNumberKey            KEY(way:WayBillType,way:WayBillNumber),DUP,NOCASE
TypeRecNumberKey         KEY(way:WayBillType,way:Received,way:WayBillNumber),DUP,NOCASE
TypeAccountRecNumberKey  KEY(way:WayBillType,way:AccountNumber,way:Received,way:WayBillNumber),DUP,NOCASE
DateKey                  KEY(way:TheDate,way:TheTime),DUP,NOCASE
WaybillTypeDateKey       KEY(way:WayBillType,way:TheDate,way:TheTime),DUP,NOCASE
AccountDateKey           KEY(way:AccountNumber,way:TheDate,way:TheTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
WayBillNumber               LONG
TheDate                     DATE
TheTime                     TIME
Received                    BYTE
WayBillType                 BYTE
WaybillID                   STRING(3)
FromAccount                 STRING(30)
ToAccount                   STRING(30)
SecurityPackNumber          STRING(30)
UserNotes                   STRING(255)
Courier                     STRING(30)
OtherAccountNumber          STRING(30)
OtherCompanyName            STRING(30)
OtherAddress1               STRING(30)
OtherAddress2               STRING(30)
OtherAddress3               STRING(30)
OtherPostcode               STRING(30)
OtherTelephoneNo            STRING(30)
OtherContactName            STRING(60)
OtherEmailAddress           STRING(255)
OtherHub                    STRING(30)
                         END
                     END                       

STOCKMIN             FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name),PRE(smin),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(smin:RecordNumber),NOCASE,PRIMARY
LocationPartNoKey        KEY(smin:Location,smin:PartNumber),DUP,NOCASE
LocationDescriptionKey   KEY(smin:Location,smin:Description),DUP,NOCASE
LocationSuppPartKey      KEY(smin:Location,smin:Supplier,smin:PartNumber),DUP,NOCASE
LocationSuppDescKey      KEY(smin:Location,smin:Supplier,smin:Description),DUP,NOCASE
PartRefNumberKey         KEY(smin:PartRefNumber),DUP,NOCASE
PartNumberKey            KEY(smin:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
PartNumber                  STRING(30)
Description                 STRING(20)
Supplier                    STRING(30)
Usage                       LONG
MinLevel                    LONG
QuantityOnOrder             LONG
QuantityInStock             LONG
QuantityRequired            LONG
PartRefNumber               LONG
QuantityJobsPending         LONG
                         END
                     END                       

LOCATION             FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(loc:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(loc:Location),NOCASE
Main_Store_Key           KEY(loc:Main_Store,loc:Location),DUP,NOCASE
ActiveLocationKey        KEY(loc:Active,loc:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(loc:Active,loc:Main_Store,loc:Location),DUP,NOCASE
VirtualLocationKey       KEY(loc:VirtualSite,loc:Location),DUP,NOCASE
VirtualMainStoreKey      KEY(loc:VirtualSite,loc:Main_Store,loc:Location),DUP,NOCASE
FaultyLocationKey        KEY(loc:FaultyPartsLocation),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
VirtualSite                 BYTE
Level1                      BYTE
Level2                      BYTE
Level3                      BYTE
InWarrantyMarkUp            REAL
OutWarrantyMarkUp           REAL
UseRapidStock               BYTE
FaultyPartsLocation         BYTE
                         END
                     END                       

STOHISTE             FILE,DRIVER('Btrieve'),OEM,NAME('STOHISTE.DAT'),PRE(stoe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stoe:RecordNumber),NOCASE,PRIMARY
SHIRecordNumberKey       KEY(stoe:SHIRecordNumber),DUP,NOCASE
KeyArcStatus             KEY(stoe:ARC_Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SHIRecordNumber             LONG
PreviousAveragePurchaseCost REAL
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
HistTime                    TIME
ARC_Status                  STRING(1)
                         END
                     END                       

STOMPFAU             FILE,DRIVER('Btrieve'),OEM,NAME('STOMPFAU.DAT'),PRE(stu),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stu:RecordNumber),NOCASE,PRIMARY
FieldKey                 KEY(stu:RefNumber,stu:FieldNumber,stu:Field),DUP,NOCASE
DescriptionKey           KEY(stu:RefNumber,stu:FieldNumber,stu:Description),DUP,NOCASE
ManufacturerFieldKey     KEY(stu:Manufacturer,stu:FieldNumber,stu:Field),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Manufacturer                STRING(30)
FieldNumber                 LONG
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

STOMJFAU             FILE,DRIVER('Btrieve'),OEM,NAME('STOMJFAU.DAT'),PRE(stj),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stj:RecordNumber),NOCASE,PRIMARY
FieldKey                 KEY(stj:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(255)
FaultCode11                 STRING(255)
FaultCode12                 STRING(255)
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

WPARTTMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name2),PRE(wartmp),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wartmp:Ref_Number,wartmp:Part_Number),DUP,NOCASE
record_number_key        KEY(wartmp:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(wartmp:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wartmp:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(wartmp:Ref_Number,wartmp:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(wartmp:Ref_Number,wartmp:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wartmp:Ref_Number,wartmp:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wartmp:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Main_Part                   STRING(3)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

CREDNOTR             FILE,DRIVER('Btrieve'),OEM,NAME('CREDNOTR.DAT'),PRE(cnr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cnr:RecordNumber),NOCASE,PRIMARY
ReceivedKey              KEY(cnr:Received,cnr:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Usercode                    STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
ReceivedUsercode            STRING(3)
                         END
                     END                       

PARTSTMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(partmp),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(partmp:Ref_Number,partmp:Part_Number),DUP,NOCASE
record_number_key        KEY(partmp:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(partmp:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(partmp:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(partmp:Ref_Number,partmp:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(partmp:Ref_Number,partmp:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(partmp:Ref_Number,partmp:Order_Number),DUP,NOCASE
Supplier_Key             KEY(partmp:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

EXCHANGE             FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(xch:Ref_Number),NOCASE,PRIMARY
AvailLocIMEI             KEY(xch:Available,xch:Location,xch:ESN),DUP,NOCASE
AvailLocMSN              KEY(xch:Available,xch:Location,xch:MSN),DUP,NOCASE
AvailLocRef              KEY(xch:Available,xch:Location,xch:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(xch:Available,xch:Location,xch:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(xch:ESN),DUP,NOCASE
MSN_Only_Key             KEY(xch:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(xch:Stock_Type,xch:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(xch:Stock_Type,xch:ESN),DUP,NOCASE
MSN_Key                  KEY(xch:Stock_Type,xch:MSN),DUP,NOCASE
ESN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:ESN),DUP,NOCASE
MSN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:MSN),DUP,NOCASE
Ref_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(xch:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(xch:Model_Number,xch:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(xch:Available,xch:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(xch:Available,xch:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(xch:Available,xch:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(xch:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(xch:Location,xch:Stock_Type,xch:Available,xch:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(xch:Location,xch:Stock_Type,xch:ESN),DUP,NOCASE
LocIMEIKey               KEY(xch:Location,xch:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(xch:Location,xch:Stock_Type,xch:Available,xch:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(xch:Location,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE
LocRefKey                KEY(xch:Location,xch:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(xch:Location,xch:Stock_Type,xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(xch:Location,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
LocModelKey              KEY(xch:Location,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(xch:Location,xch:Stock_Type,xch:Available,xch:MSN),DUP,NOCASE
LocStockMSNKey           KEY(xch:Location,xch:Stock_Type,xch:MSN),DUP,NOCASE
LocMSNKey                KEY(xch:Location,xch:MSN),DUP,NOCASE
InTransitLocationKey     KEY(xch:InTransit,xch:Location,xch:ESN),DUP,NOCASE
InTransitKey             KEY(xch:InTransit,xch:ESN),DUP,NOCASE
StatusChangeDateKey      KEY(xch:StatusChangeDate),DUP,NOCASE
LocStatusChangeDatekey   KEY(xch:Location,xch:StatusChangeDate),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
InTransit                   BYTE
FreeStockPurchased          STRING(1)
StatusChangeDate            DATE
                         END
                     END                       

STOESN               FILE,DRIVER('Btrieve'),OEM,NAME('STOESN.DAT'),PRE(ste),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(ste:Record_Number),NOCASE,PRIMARY
Sold_Key                 KEY(ste:Ref_Number,ste:Sold,ste:Serial_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Serial_Number               STRING(16)
Sold                        STRING(3)
                         END
                     END                       

LOANACC              FILE,DRIVER('Btrieve'),NAME('LOANACC.DAT'),PRE(lac),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(lac:Ref_Number,lac:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
Accessory_Status            STRING(6)
                         END
                     END                       

SUPPLIER             FILE,DRIVER('Btrieve'),NAME('SUPPLIER.DAT'),PRE(sup),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sup:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sup:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(sup:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
UseForeignCurrency          BYTE
CurrencyCode                STRING(30)
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
UseFreeStock                BYTE
EVO_GL_Acc_No               STRING(10)
EVO_Vendor_Number           STRING(10)
EVO_TaxExempt               BYTE
EVO_Profit_Centre           STRING(30)
EVO_Excluded                BYTE
                         END
                     END                       

SUBURB               FILE,DRIVER('Btrieve'),OEM,NAME('SUBURB.DAT'),PRE(sur),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sur:RecordNumber),NOCASE,PRIMARY
SuburbKey                KEY(sur:Suburb),DUP,NOCASE
PostcodeKey              KEY(sur:Postcode),DUP,NOCASE
HubKey                   KEY(sur:Hub),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Suburb                      STRING(30)
Postcode                    STRING(30)
Hub                         STRING(30)
Region                      STRING(30)
                         END
                     END                       

EXCHACC              FILE,DRIVER('Btrieve'),NAME('EXCHACC.DAT'),PRE(xca),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(xca:Ref_Number,xca:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

COUBUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('COUBUSHR.DAT'),PRE(cbh),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cbh:RecordNumber),NOCASE,PRIMARY
TypeDateKey              KEY(cbh:Courier,cbh:TheDate),NOCASE
EndTimeKey               KEY(cbh:Courier,cbh:TheDate,cbh:StartTime,cbh:EndTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Courier                     STRING(30)
TheDate                     DATE
ExceptionType               BYTE
StartTime                   TIME
EndTime                     TIME
                         END
                     END                       

RECIPTYP             FILE,DRIVER('Btrieve'),OEM,NAME('RECIPTYP.DAT'),PRE(rec),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rec:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(rec:RecipientType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RecipientType               STRING(30)
                         END
                     END                       

SUPVALA              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALA.DAT'),PRE(suva),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(suva:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(suva:Supplier,suva:RunDate),DUP,NOCASE
DateOnly                 KEY(suva:RunDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
OldBackOrder                DATE
OldOutOrder                 DATE
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),NAME('ESTPARTS.DAT'),PRE(epr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE
record_number_key        KEY(epr:Record_Number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(epr:PartAllocated,epr:Part_Number),DUP,NOCASE
StatusKey                KEY(epr:Status,epr:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(epr:PartAllocated,epr:Status,epr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    LONG
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Date_Received               DATE
Order_Part_Number           LONG
Status_Date                 DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
UsedOnRepair                BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
                         END
                     END                       

REGIONS              FILE,DRIVER('Btrieve'),OEM,NAME('REGIONS.DAT'),PRE(reg),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(reg:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(reg:Region),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Region                      STRING(30)
                         END
                     END                       

HUBS                 FILE,DRIVER('Btrieve'),OEM,NAME('HUBS.DAT'),PRE(hub),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(hub:RecordNumber),NOCASE,PRIMARY
HubKey                   KEY(hub:Hub),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Hub                         STRING(30)
                         END
                     END                       

SBO_OutFaultParts    FILE,DRIVER('TOPSPEED'),RECLAIM,OEM,NAME(glo:sbo_outfaultparts),PRE(sofp),CREATE,BINDABLE,THREAD
FaultKey                 KEY(sofp:sessionID,sofp:partType,sofp:fault),DUP,NOCASE,OPT
Record                   RECORD,PRE()
sessionID                   LONG
partType                    STRING(1)
fault                       STRING(30)
description                 STRING(60)
level                       LONG
                         END
                     END                       

TRAHUBS              FILE,DRIVER('Btrieve'),OEM,NAME('TRAHUBS.DAT'),PRE(trh),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(trh:RecordNumber),NOCASE,PRIMARY
HubKey                   KEY(trh:TRADEACCAccountNumber,trh:Hub),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
TRADEACCAccountNumber       STRING(30)
Hub                         STRING(30)
                         END
                     END                       

TRAEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('TRAEMAIL.DAT'),PRE(tre),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tre:RecordNumber),NOCASE,PRIMARY
RecipientKey             KEY(tre:RefNumber,tre:RecipientType),DUP,NOCASE
ContactNameKey           KEY(tre:RefNumber,tre:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
SendStatusEmails            BYTE
SendReportEmails            BYTE
                         END
                     END                       

SUPVALB              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALB.DAT'),PRE(suvb),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(suvb:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(suvb:Supplier,suvb:RunDate),DUP,NOCASE
LocationKey              KEY(suvb:Supplier,suvb:RunDate,suvb:Location),DUP,NOCASE
DateOnly                 KEY(suvb:RunDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
Location                    STRING(30)
BackOrderValue              REAL
                         END
                     END                       

SUBBUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('SUBBUSHR.DAT'),PRE(sbh),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sbh:RecordNumber),NOCASE,PRIMARY
TypeDateKey              KEY(sbh:RefNumber,sbh:TheDate),NOCASE
EndTimeKey               KEY(sbh:RefNumber,sbh:TheDate,sbh:StartTime,sbh:EndTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
ExceptionType               BYTE
StartTime                   TIME
EndTime                     TIME
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job:Surname),DUP,NOCASE
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job:ESN),DUP,NOCASE
MSN_Key                  KEY(job:MSN),DUP,NOCASE
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(mod),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(mod:Manufacturer,mod:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
Active                      STRING(1)
ExchReplaceValue            REAL
RRCOrderCap                 LONG
                         END
                     END                       

TRABUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('TRABUSHR.DAT'),PRE(tbh),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tbh:RecordNumber),NOCASE,PRIMARY
TypeDateKey              KEY(tbh:RefNumber,tbh:TheDate),NOCASE
EndTimeKey               KEY(tbh:RefNumber,tbh:TheDate,tbh:StartTime,tbh:EndTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
ExceptionType               BYTE
StartTime                   TIME
EndTime                     TIME
                         END
                     END                       

SUBEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('SUBEMAIL.DAT'),PRE(sue),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sue:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(sue:RefNumber,sue:RecipientType),DUP,NOCASE
ContactNameKey           KEY(sue:RefNumber,sue:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
SendStatusEmails            BYTE
SendReportEmails            BYTE
                         END
                     END                       

SUPPTEMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(suptmp),CREATE,BINDABLE,THREAD
Company_Name_Key         KEY(suptmp:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(suptmp:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Address_Group               GROUP
Postcode                      STRING(10)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
                            END
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
Notes                       STRING(1000)
                         END
                     END                       

ORDERS               FILE,DRIVER('Btrieve'),NAME('ORDERS.DAT'),PRE(ord),CREATE,BINDABLE,THREAD
Order_Number_Key         KEY(ord:Order_Number),NOCASE,PRIMARY
Printed_Key              KEY(ord:Printed,ord:Order_Number),DUP,NOCASE
Supplier_Printed_Key     KEY(ord:Printed,ord:Supplier,ord:Order_Number),DUP,NOCASE
Received_Key             KEY(ord:All_Received,ord:Order_Number),DUP,NOCASE
Supplier_Key             KEY(ord:Supplier,ord:Order_Number),DUP,NOCASE
Supplier_Received_Key    KEY(ord:Supplier,ord:All_Received,ord:Order_Number),DUP,NOCASE
DateKey                  KEY(ord:Date),DUP,NOCASE
NotPrintedOrderKey       KEY(ord:BatchRunNotPrinted,ord:Order_Number),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Supplier                    STRING(30)
Date                        DATE
Printed                     STRING(3)
All_Received                STRING(3)
User                        STRING(3)
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
BatchRunNotPrinted          BYTE
                         END
                     END                       

UNITTYPE             FILE,DRIVER('Btrieve'),NAME('UNITTYPE.DAT'),PRE(uni),CREATE,BINDABLE,THREAD
Unit_Type_Key            KEY(uni:Unit_Type),NOCASE,PRIMARY
ActiveKey                KEY(uni:Active,uni:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Unit_Type                   STRING(30)
Active                      BYTE
                         END
                     END                       

STATUS               FILE,DRIVER('Btrieve'),NAME('STATUS.DAT'),PRE(sts),CREATE,BINDABLE,THREAD
Ref_Number_Only_Key      KEY(sts:Ref_Number),NOCASE,PRIMARY
Status_Key               KEY(sts:Status),DUP,NOCASE
Heading_Key              KEY(sts:Heading_Ref_Number,sts:Status),NOCASE
Ref_Number_Key           KEY(sts:Heading_Ref_Number,sts:Ref_Number),NOCASE
LoanKey                  KEY(sts:Loan,sts:Status),DUP,NOCASE
ExchangeKey              KEY(sts:Exchange,sts:Status),DUP,NOCASE
JobKey                   KEY(sts:Job,sts:Status),DUP,NOCASE
TurnJobKey               KEY(sts:Use_Turnaround_Time,sts:Job,sts:Status),DUP,NOCASE
Record                   RECORD,PRE()
Heading_Ref_Number          REAL
Status                      STRING(30)
Ref_Number                  REAL
Use_Turnaround_Time         STRING(3)
Use_Turnaround_Days         STRING(3)
Turnaround_Days             LONG
Use_Turnaround_Hours        STRING(3)
Turnaround_Hours            LONG
Notes                       STRING(1000)
Loan                        STRING(3)
Exchange                    STRING(3)
Job                         STRING(3)
SystemStatus                STRING(3)
EngineerStatus              BYTE
EnableEmail                 BYTE
SenderEmailAddress          STRING(255)
EmailSubject                STRING(255)
EmailBody                   STRING(500)
EmailFooter                 STRING(255)
RefManufacturer             BYTE
RefModelNumber              BYTE
RefLabourCost               BYTE
RefPartsCost                BYTE
RefTotalCost                BYTE
RefEstCost                  BYTE
TurnaroundTimeReport        BYTE
                         END
                     END                       

REPAIRTY             FILE,DRIVER('Btrieve'),NAME('REPAIRTY.DAT'),PRE(rep),CREATE,BINDABLE,THREAD
Repair_Type_Key          KEY(rep:Manufacturer,rep:Model_Number,rep:Repair_Type),NOCASE,PRIMARY
Manufacturer_Key         KEY(rep:Manufacturer,rep:Repair_Type),DUP,NOCASE
Model_Number_Key         KEY(rep:Model_Number,rep:Repair_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(rep:Repair_Type),DUP,NOCASE
Model_Chargeable_Key     KEY(rep:Model_Number,rep:Chargeable,rep:Repair_Type),DUP,NOCASE
Model_Warranty_Key       KEY(rep:Model_Number,rep:Warranty,rep:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
                         END
                     END                       

TRDACC               FILE,DRIVER('Btrieve'),OEM,NAME('TRDACC.DAT'),PRE(trr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(trr:RecordNumber),NOCASE,PRIMARY
AccessoryKey             KEY(trr:RefNumber,trr:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
EntryDate                   DATE
EntryTime                   TIME
Accessory                   STRING(30)
                         END
                     END                       

TRDMAN               FILE,DRIVER('Btrieve'),OEM,NAME('TRDMAN.DAT'),PRE(tdm),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tdm:RecordNumber),NOCASE,PRIMARY
ManufacturerKey          KEY(tdm:ThirdPartyCompanyName,tdm:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ThirdPartyCompanyName       STRING(30)
Manufacturer                STRING(30)
                         END
                     END                       

TRDPONO              FILE,DRIVER('Btrieve'),OEM,NAME('TRDPONO.DAT'),PRE(tro),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tro:RecordNumber),NOCASE,PRIMARY
CompanyNameKey           KEY(tro:CompanyName,tro:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
CompanyName                 STRING(30)
                         END
                     END                       

TRADETMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:tradetmp),PRE(tratmp),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(tratmp:RefNumber),NOCASE,PRIMARY
AccountNoKey             KEY(tratmp:Account_Number),DUP,NOCASE
CompanyNameKey           KEY(tratmp:Company_Name),DUP,NOCASE
TypeKey                  KEY(tratmp:Type,tratmp:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Account_Number              STRING(15)
Company_Name                STRING(30)
Type                        STRING(3)
                         END
                     END                       

EXCCHRGE             FILE,DRIVER('Btrieve'),NAME('EXCCHRGE.DAT'),PRE(exc),CREATE,BINDABLE,THREAD
Manufacturer_Key         KEY(exc:Manufacturer,exc:Charge_Type,exc:Model_Number,exc:Unit_Type,exc:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
                         END
                     END                       

INVOICE              FILE,DRIVER('Btrieve'),NAME('INVOICE.DAT'),PRE(inv),CREATE,BINDABLE,THREAD
Invoice_Number_Key       KEY(inv:Invoice_Number),NOCASE,PRIMARY
Invoice_Type_Key         KEY(inv:Invoice_Type,inv:Invoice_Number),DUP,NOCASE
Account_Number_Type_Key  KEY(inv:Invoice_Type,inv:Account_Number,inv:Invoice_Number),DUP,NOCASE
Account_Number_Key       KEY(inv:Account_Number,inv:Invoice_Number),DUP,NOCASE
Date_Created_Key         KEY(inv:Date_Created),DUP,NOCASE
Batch_Number_Key         KEY(inv:Manufacturer,inv:Batch_Number),DUP,NOCASE
OracleDateKey            KEY(inv:ExportedOracleDate,inv:Invoice_Number),DUP,NOCASE
OracleNumberKey          KEY(inv:OracleNumber,inv:Invoice_Number),DUP,NOCASE
Account_Date_Key         KEY(inv:Account_Number,inv:Date_Created),DUP,NOCASE
RRCInvoiceDateKey        KEY(inv:RRCInvoiceDate),DUP,NOCASE
ARCInvoiceDateKey        KEY(inv:ARCInvoiceDate),DUP,NOCASE
ReconciledDateKey        KEY(inv:Reconciled_Date),DUP,NOCASE
AccountReconciledKey     KEY(inv:Account_Number,inv:Reconciled_Date),DUP,NOCASE
Record                   RECORD,PRE()
Invoice_Number              REAL
Invoice_Type                STRING(3)
Job_Number                  REAL
Date_Created                DATE
Account_Number              STRING(15)
AccountType                 STRING(3)
Total                       REAL
Vat_Rate_Labour             REAL
Vat_Rate_Parts              REAL
Vat_Rate_Retail             REAL
VAT_Number                  STRING(30)
Invoice_VAT_Number          STRING(30)
Currency                    STRING(30)
Batch_Number                REAL
Manufacturer                STRING(30)
Claim_Reference             STRING(30)
Total_Claimed               REAL
Courier_Paid                REAL
Labour_Paid                 REAL
Parts_Paid                  REAL
Reconciled_Date             DATE
jobs_count                  REAL
PrevInvoiceNo               LONG
InvoiceCredit               STRING(3)
UseAlternativeAddress       BYTE
EuroExhangeRate             REAL
RRCVatRateLabour            REAL
RRCVatRateParts             REAL
RRCVatRateRetail            REAL
ExportedRRCOracle           BYTE
ExportedARCOracle           BYTE
ExportedOracleDate          DATE
OracleNumber                LONG
RRCInvoiceDate              DATE
ARCInvoiceDate              DATE
                         END
                     END                       

TRDREPTY             FILE,DRIVER('Btrieve'),OEM,NAME('TRDREPTY.DAT'),PRE(try),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(try:RecordNumber),NOCASE,PRIMARY
RepairTypeKey            KEY(try:RepairType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RepairType                  STRING(30)
Description                 STRING(60)
                         END
                     END                       

TRDBATCH             FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(trb),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(trb:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(trb:Company_Name,trb:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(trb:Status,trb:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(trb:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(trb:Status,trb:Company_Name,trb:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(trb:ESN),DUP,NOCASE
ESN_Status_Key           KEY(trb:Status,trb:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(trb:Company_Name,trb:Batch_Number,trb:ESN),DUP,NOCASE
AuthorisationKey         KEY(trb:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(trb:Status,trb:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(trb:Company_Name,trb:Status,trb:DateReturn),DUP,NOCASE
JobStatusKey             KEY(trb:Status,trb:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(trb:Ref_Number),DUP,NOCASE
CompanyDespatchedKey     KEY(trb:Company_Name,trb:DateDespatched),DUP,NOCASE
ReturnDateKey            KEY(trb:DateReturn),DUP,NOCASE
ReturnCompanyKey         KEY(trb:DateReturn,trb:Company_Name),DUP,NOCASE
NotPrintedManJobKey      KEY(trb:BatchRunNotPrinted,trb:Status,trb:Company_Name,trb:Ref_Number),DUP,NOCASE
PurchaseOrderKey         KEY(trb:PurchaseOrderNumber,trb:Ref_Number),DUP,NOCASE
KeyEVO_Status            KEY(trb:EVO_Status),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
DateDespatched              DATE
TimeDespatched              TIME
ReturnUser                  STRING(3)
ReturnWaybillNo             STRING(30)
ReturnRepairType            STRING(30)
ReturnRejectedAmount        REAL
ReturnRejectedReason        STRING(255)
ThirdPartyInvoiceNo         STRING(30)
ThirdPartyInvoiceDate       DATE
ThirdPartyInvoiceCharge     REAL
ThirdPartyVAT               REAL
ThirdPartyMarkup            REAL
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
BatchRunNotPrinted          BYTE
PurchaseOrderNumber         LONG
NewThirdPartySite           STRING(30)
NewThirdPartySiteID         STRING(30)
EVO_Status                  STRING(1)
                         END
                     END                       

TRACHRGE             FILE,DRIVER('Btrieve'),NAME('TRACHRGE.DAT'),PRE(trc),CREATE,BINDABLE,THREAD
Account_Charge_Key       KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type,trc:Unit_Type,trc:Repair_Type),NOCASE,PRIMARY
Model_Repair_Key         KEY(trc:Model_Number,trc:Repair_Type),DUP,NOCASE
Charge_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(trc:Account_Number,trc:Model_Number,trc:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(trc:Account_Number,trc:Model_Number,trc:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(trc:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(trc:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(trc:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
WarrantyClaimRate           REAL
HandlingFee                 REAL
Exchange                    REAL
RRCRate                     REAL
                         END
                     END                       

TRDMODEL             FILE,DRIVER('Btrieve'),NAME('TRDMODEL.DAT'),PRE(trm),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(trm:Company_Name,trm:Model_Number),NOCASE,PRIMARY
Model_Number_Only_Key    KEY(trm:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Model_Number                STRING(30)
                         END
                     END                       

STOMODEL             FILE,DRIVER('Btrieve'),NAME('STOMODEL.DAT'),PRE(stm),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stm:RecordNumber),NOCASE,PRIMARY
Model_Number_Key         KEY(stm:Ref_Number,stm:Manufacturer,stm:Model_Number),NOCASE
Model_Part_Number_Key    KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE
Description_Key          KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE
Manufacturer_Key         KEY(stm:Manufacturer,stm:Model_Number),DUP,NOCASE
Ref_Part_Description     KEY(stm:Ref_Number,stm:Part_Number,stm:Location,stm:Description),DUP,NOCASE
Location_Part_Number_Key KEY(stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE
Location_Description_Key KEY(stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE
Mode_Number_Only_Key     KEY(stm:Ref_Number,stm:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Manufacturer                STRING(30)
Model_Number                STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Location                    STRING(30)
Accessory                   STRING(3)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(30)
FaultCode11                 STRING(30)
FaultCode12                 STRING(30)
RecordNumber                LONG
                         END
                     END                       

TRADEACC             FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(tra),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tra:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(tra:Account_Number),NOCASE
Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(tra:ReplicateAccount,tra:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(tra:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(tra:SiteLocation),DUP,NOCASE
RegionKey                KEY(tra:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
                         END
                     END                       

QUERYREA             FILE,DRIVER('Btrieve'),OEM,NAME('QUERYREA.DAT'),PRE(que),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(que:RefNumber),NOCASE,PRIMARY
QueryKey                 KEY(que:Query),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Query                       STRING(60)
                         END
                     END                       

REQUISIT             FILE,DRIVER('Btrieve'),OEM,NAME('REQUISIT.DAT'),PRE(req),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(req:RecordNumber),NOCASE,PRIMARY
SupplierKey              KEY(req:Supplier),DUP,NOCASE
OrderedNumberKey         KEY(req:Ordered,req:RecordNumber),DUP,NOCASE
SupplierNumberKey        KEY(req:Ordered,req:Supplier),DUP,NOCASE
DateKey                  KEY(req:TheDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
TheDate                     DATE
TheTime                     TIME
Supplier                    STRING(30)
Ordered                     BYTE
Approved                    STRING(1)
                         END
                     END                       

SUBCHRGE             FILE,DRIVER('Btrieve'),NAME('SUBCHRGE.DAT'),PRE(suc),CREATE,BINDABLE,THREAD
Model_Repair_Type_Key    KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type,suc:Unit_Type,suc:Repair_Type),NOCASE,PRIMARY
Account_Charge_Key       KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(suc:Account_Number,suc:Model_Number,suc:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(suc:Account_Number,suc:Model_Number,suc:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(suc:Account_Number,suc:Model_Number,suc:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(suc:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(suc:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(suc:Unit_Type),DUP,NOCASE
Model_Repair_Key         KEY(suc:Model_Number,suc:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
WarrantyClaimRate           REAL
HandlingFee                 REAL
Exchange                    REAL
RRCRate                     REAL
                         END
                     END                       

DEFCHRGE             FILE,DRIVER('Btrieve'),NAME('DEFCHRGE.DAT'),PRE(dec),CREATE,BINDABLE,THREAD
Charge_Type_Key          KEY(dec:Charge_Type),NOCASE,PRIMARY
Repair_Type_Only_Key     KEY(dec:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(dec:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
                         END
                     END                       

SUBTRACC             FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(sub),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sub:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE
Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(sub:Account_Number),NOCASE
Branch_Key               KEY(sub:Branch),DUP,NOCASE
Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(sub:ReplicateAccount,sub:Account_Number),DUP,NOCASE
GenericAccountKey        KEY(sub:Generic_Account,sub:Account_Number),DUP,NOCASE
GenericCompanyKey        KEY(sub:Generic_Account,sub:Company_Name),DUP,NOCASE
GenericBranchKey         KEY(sub:Generic_Account,sub:Branch),DUP,NOCASE
RegionKey                KEY(sub:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
ReplicateAccount            STRING(30)
FactorAccount               BYTE
ForceEstimate               BYTE
EstimateIfOver              REAL
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
Generic_Account             BYTE
StoresAccountNumber         STRING(30)
Force_Customer_Name         BYTE
FinanceContactName          STRING(30)
FinanceTelephoneNo          STRING(30)
FinanceEmailAddress         STRING(255)
FinanceAddress1             STRING(30)
FinanceAddress2             STRING(30)
FinanceAddress3             STRING(30)
FinancePostcode             STRING(30)
AccountLimit                REAL
ExcludeBouncer              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
ExcludeFromTATReport        BYTE
OverrideHeadVATNo           BYTE
Line500AccountNumber        STRING(30)
OverrideHeadMobile          BYTE
OverrideMobileDefault       BYTE
SIDJobBooking               BYTE
SIDJobEnquiry               BYTE
Region                      STRING(30)
AllowVCPLoanUnits           BYTE
Hub                         STRING(30)
RefurbishmentAccount        BYTE
VCPWaybillPrefix            STRING(30)
PrintOOWVCPFee              BYTE
OOWVCPFeeLabel              STRING(30)
OOWVCPFeeAmount             REAL
PrintWarrantyVCPFee         BYTE
WarrantyVCPFeeAmount        REAL
DealerID                    STRING(30)
                         END
                     END                       

CHARTYPE             FILE,DRIVER('Btrieve'),NAME('CHARTYPE.DAT'),PRE(cha),CREATE,BINDABLE,THREAD
Charge_Type_Key          KEY(cha:Charge_Type),NOCASE,PRIMARY
Ref_Number_Key           KEY(cha:Ref_Number,cha:Charge_Type),DUP,NOCASE
Physical_Damage_Key      KEY(cha:Allow_Physical_Damage,cha:Charge_Type),DUP,NOCASE
Warranty_Key             KEY(cha:Warranty,cha:Charge_Type),DUP,NOCASE
Warranty_Ref_Number_Key  KEY(cha:Warranty,cha:Ref_Number,cha:Charge_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Force_Warranty              STRING(3)
Allow_Physical_Damage       STRING(3)
Allow_Estimate              STRING(3)
Force_Estimate              STRING(3)
Invoice_Customer            STRING(3)
Invoice_Trade_Customer      STRING(3)
Invoice_Manufacturer        STRING(3)
No_Charge                   STRING(3)
Zero_Parts                  STRING(3)
Warranty                    STRING(3)
Ref_Number                  REAL
Exclude_EDI                 STRING(3)
ExcludeInvoice              STRING(3)
ExcludeFromBouncer          BYTE
ForceAuthorisation          BYTE
Zero_Parts_ARC              BYTE
SecondYearWarranty          BYTE
                         END
                     END                       

WAYBPRO              FILE,DRIVER('Btrieve'),OEM,NAME('WAYBPRO.DAT'),PRE(wyp),CREATE,BINDABLE,THREAD
WAYBPROIDKey             KEY(wyp:WAYBPROID),NOCASE,PRIMARY
AccountJobNumberKey      KEY(wyp:AccountNumber,wyp:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
WAYBPROID                   LONG
AccountNumber               STRING(30)
JobNumber                   LONG
ModelNumber                 STRING(30)
IMEINumber                  STRING(20)
SecurityPackNumber          STRING(30)
                         END
                     END                       

WAYAUDIT             FILE,DRIVER('Btrieve'),OEM,NAME('WAYAUDIT.DAT'),PRE(waa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(waa:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(waa:WAYBILLSRecordNumber,waa:TheDate,waa:TheTime),DUP,NOCASE
DateOnlyKey              KEY(waa:TheDate,waa:TheTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WAYBILLSRecordNumber        LONG
UserCode                    STRING(3)
TheDate                     DATE
TheTime                     TIME
Action                      STRING(60)
                         END
                     END                       

WAYBAWT              FILE,DRIVER('Btrieve'),OEM,NAME('WAYBAWT.DAT'),PRE(wya),CREATE,BINDABLE,THREAD
WAYBAWTIDKey             KEY(wya:WAYBAWTID),NOCASE,PRIMARY
AccountJobNumberKey      KEY(wya:AccountNumber,wya:JobNumber),DUP,NOCASE
JobNumberKey             KEY(wya:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
WAYBAWTID                   LONG
JobNumber                   LONG
AccountNumber               STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
IMEINumber                  STRING(20)
                         END
                     END                       

WAYITEMS             FILE,DRIVER('Btrieve'),OEM,NAME('WAYITEMS.DAT'),PRE(wai),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wai:RecordNumber),NOCASE,PRIMARY
WayBillNumberKey         KEY(wai:WayBillNumber),DUP,NOCASE
WaybillLookupKey         KEY(wai:IsExchange,wai:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WayBillNumber               LONG
IsExchange                  BYTE
Ref_Number                  LONG
JobNumber48Hour             LONG
IsReplenishment             BYTE
                         END
                     END                       

WAYLAWT              FILE,DRIVER('Btrieve'),OEM,NAME('WAYLAWT.DAT'),PRE(wal),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wal:RecordNumber),NOCASE,PRIMARY
IMEINumberKey            KEY(wal:IMEINumber),DUP,NOCASE
LOANRefNumberKey         KEY(wal:LOANRefNumber),NOCASE
ReadyIMEINumberKey       KEY(wal:Ready,wal:IMEINumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
LOANRefNumber               LONG
Ready                       BYTE
RTARecordNumber             LONG
Location                    STRING(30)
                         END
                     END                       

WAYSUND              FILE,DRIVER('Btrieve'),OEM,NAME('WAYSUND.DAT'),PRE(was),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(was:RecordNumber),NOCASE,PRIMARY
EnteredKey               KEY(was:WAYBILLSRecordNumber,was:RecordNumber),DUP,NOCASE
DescriptionKey           KEY(was:WAYBILLSRecordNumber,was:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WAYBILLSRecordNumber        LONG
Description                 STRING(30)
Quantity                    STRING(30)
                         END
                     END                       

WAYJOBS              FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:FileName),PRE(wayjob),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wayjob:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(wayjob:WayBillNumber,wayjob:Processed,wayjob:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
IMEINumber                  STRING(30)
ModelNumber                 STRING(30)
DateBooked                  DATE
JobType                     STRING(1)
ExchangeUnitNumber          LONG
WayBillNumber               LONG
Processed                   BYTE
                         END
                     END                       

WAYSUND_TEMP         FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:FileName),PRE(wastmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wastmp:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(wastmp:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(30)
Quantity                    STRING(30)
                         END
                     END                       

WAYCNR               FILE,DRIVER('Btrieve'),OEM,NAME('WAYCNR.DAT'),PRE(wcr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wcr:RecordNumber),NOCASE,PRIMARY
EnteredKey               KEY(wcr:WAYBILLSRecordnumber,wcr:RecordNumber),DUP,NOCASE
PartNumberKey            KEY(wcr:WAYBILLSRecordnumber,wcr:ExchangeOrder,wcr:PartNumber),DUP,NOCASE
ProcessedKey             KEY(wcr:WAYBILLSRecordnumber,wcr:Processed,wcr:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WAYBILLSRecordnumber        LONG
PartNumber                  STRING(30)
Description                 STRING(30)
ExchangeOrder               BYTE
Quantity                    LONG
QuantityReceived            LONG
Processed                   BYTE
RefNumber                   LONG
                         END
                     END                       

WAYBILLJ             FILE,DRIVER('Btrieve'),OEM,NAME('WAYBILLJ.DAT'),PRE(waj),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(waj:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(waj:WayBillNumber,waj:JobNumber),DUP,NOCASE
DescRecordNoKey          KEY(-waj:RecordNumber),DUP,NOCASE
DescWaybillNoKey         KEY(-waj:WayBillNumber),DUP,NOCASE
DescJobNumberKey         KEY(-waj:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WayBillNumber               LONG
JobNumber                   LONG
ReportJobNumber             STRING(30)
IMEINumber                  STRING(30)
OrderNumber                 STRING(30)
SecurityPackNumber          STRING(30)
JobType                     STRING(3)
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(man),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(man:Manufacturer),NOCASE
EDIFileTypeKey           KEY(man:EDIFileType,man:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
ThirdPartyHandlingFee       REAL
ProductCodeCompulsory       STRING(3)
Inactive                    BYTE
                         END
                     END                       

USMASSIG             FILE,DRIVER('Btrieve'),NAME('USMASSIG.DAT'),PRE(usm),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(usm:User_Code,usm:Model_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Code                   STRING(3)
Model_Number                STRING(30)
                         END
                     END                       

EXPWARPARTS          FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(expwar),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 STRING(20)
Sale_Cost                     STRING(20)
                            END
Quantity                    STRING(20)
Exclude_From_Order          STRING(20)
Despatch_Note_Number        STRING(30)
Date_Ordered                STRING(20)
Order_Number                STRING(20)
Date_Received               STRING(20)
UnitType                    STRING(30)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
FirstJob                    STRING(1)
                         END
                     END                       

WEBJOB               FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOB.DAT'),PRE(wob),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wob:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(wob:RefNumber),DUP,NOCASE
HeadJobNumberKey         KEY(wob:HeadAccountNumber,wob:JobNumber),DUP,NOCASE
HeadOrderNumberKey       KEY(wob:HeadAccountNumber,wob:OrderNumber,wob:RefNumber),DUP,NOCASE
HeadMobileNumberKey      KEY(wob:HeadAccountNumber,wob:MobileNumber,wob:RefNumber),DUP,NOCASE
HeadModelNumberKey       KEY(wob:HeadAccountNumber,wob:ModelNumber,wob:RefNumber),DUP,NOCASE
HeadIMEINumberKey        KEY(wob:HeadAccountNumber,wob:IMEINumber,wob:RefNumber),DUP,NOCASE
HeadMSNKey               KEY(wob:HeadAccountNumber,wob:MSN),DUP,NOCASE
HeadEDIKey               KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:JobNumber),DUP,NOCASE
ValidationIMEIKey        KEY(wob:HeadAccountNumber,wob:Validation,wob:IMEINumber),DUP,NOCASE
HeadSubKey               KEY(wob:HeadAccountNumber,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE
HeadRefNumberKey         KEY(wob:HeadAccountNumber,wob:RefNumber),DUP,NOCASE
OracleNumberKey          KEY(wob:OracleExportNumber),DUP,NOCASE
HeadCurrentStatusKey     KEY(wob:HeadAccountNumber,wob:Current_Status,wob:RefNumber),DUP,NOCASE
HeadExchangeStatus       KEY(wob:HeadAccountNumber,wob:Exchange_Status,wob:RefNumber),DUP,NOCASE
HeadLoanStatusKey        KEY(wob:HeadAccountNumber,wob:Loan_Status,wob:RefNumber),DUP,NOCASE
ExcWayBillNoKey          KEY(wob:ExcWayBillNumber),DUP,NOCASE
LoaWayBillNoKey          KEY(wob:LoaWayBillNumber),DUP,NOCASE
JobWayBillNoKey          KEY(wob:JobWayBillNumber),DUP,NOCASE
RRCWInvoiceNumberKey     KEY(wob:RRCWInvoiceNumber,wob:RefNumber),DUP,NOCASE
DateBookedKey            KEY(wob:HeadAccountNumber,wob:DateBooked),DUP,NOCASE
DateCompletedKey         KEY(wob:HeadAccountNumber,wob:DateCompleted),DUP,NOCASE
CompletedKey             KEY(wob:HeadAccountNumber,wob:Completed),DUP,NOCASE
DespatchKey              KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:RefNumber),DUP,NOCASE
DespatchSubKey           KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE
DespatchCourierKey       KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE
DespatchSubCourierKey    KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE
EDIKey                   KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:RefNumber),DUP,NOCASE
EDIRefNumberKey          KEY(wob:HeadAccountNumber,wob:EDI,wob:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
RefNumber                   LONG
HeadAccountNumber           STRING(30)
SubAcountNumber             STRING(30)
OrderNumber                 STRING(30)
IMEINumber                  STRING(30)
MSN                         STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
MobileNumber                STRING(30)
EDI                         STRING(3)
Validation                  STRING(3)
OracleExportNumber          LONG
Current_Status              STRING(30)
Exchange_Status             STRING(30)
Loan_Status                 STRING(30)
Current_Status_Date         DATE
Exchange_Status_Date        DATE
Loan_Status_Date            DATE
ExcWayBillNumber            LONG
LoaWayBillNumber            LONG
JobWayBillNumber            LONG
DateJobDespatched           DATE
RRCEngineer                 STRING(3)
ReconciledMarker            BYTE
RRCWInvoiceNumber           LONG
DateBooked                  DATE
TimeBooked                  TIME
DateCompleted               DATE
TimeCompleted               TIME
Completed                   STRING(3)
ReadyToDespatch             BYTE
DespatchCourier             STRING(30)
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

WIPEXC               FILE,DRIVER('Btrieve'),OEM,NAME('WIPEXC.DAT'),PRE(wix),CREATE,BINDABLE,THREAD
WIPEXCKey                KEY(wix:WIPEXCID),NOCASE,PRIMARY
StatusTypeKey            KEY(wix:Location,wix:Status),DUP,NOCASE
Record                   RECORD,PRE()
WIPEXCID                    LONG
Location                    STRING(30)
Status                      STRING(30)
NoIMEI                      BYTE
                         END
                     END                       

USUASSIG             FILE,DRIVER('Btrieve'),NAME('USUASSIG.DAT'),PRE(usu),CREATE,BINDABLE,THREAD
Unit_Type_Key            KEY(usu:User_Code,usu:Unit_Type),NOCASE,PRIMARY
Unit_Type_Only_Key       KEY(usu:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Unit_Type                   STRING(30)
                         END
                     END                       

DEFEDI               FILE,DRIVER('Btrieve'),NAME('DEFEDI.DAT'),PRE(edi),CREATE,BINDABLE,THREAD
record_number_key        KEY(edi:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Supplier_Code               STRING(8)
record_number               REAL
Country_Code                STRING(3)
Dealer_ID                   STRING(3)
                         END
                     END                       

WIPSCAN              FILE,DRIVER('Btrieve'),OEM,NAME('WIPSCAN.DAT'),PRE(wsc),CREATE,BINDABLE,THREAD
Internal_No_Key          KEY(wsc:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(wsc:Audit_Number),DUP,NOCASE
Completed_Audit_Key      KEY(wsc:Completed,wsc:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Audit_Number                LONG
User                        STRING(3)
Completed                   BYTE
                         END
                     END                       

REPMAIL              FILE,DRIVER('Btrieve'),OEM,PRE(rpm),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(rpm:Ref_Number),NOCASE,PRIMARY
Reports_Key              KEY(rpm:Report_Name),DUP,NOCASE
Date_Key                 KEY(rpm:Next_Scheduled_Date,rpm:Next_Scheduled_Time),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Report_Name                 STRING(30)
Recipient_Type              STRING(30)
Frequency                   STRING(3)
Frequency_Day               STRING(3)
Frequency_Date              DATE
Frequency_Time              TIME
Subject_Line                STRING(30)
Body_Text                   STRING(255)
Email_Address               STRING(255)
Attachment_Path             STRING(255)
On_Hold                     BYTE
Report_Last_Sent_Date       DATE
Report_Last_Sent_Time       TIME
Status                      STRING(30)
Next_Scheduled_Date         DATE
Next_Scheduled_Time         TIME
                         END
                     END                       

PRIBAND_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('PRIBAND.DAT'),PRE(prb_ali),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(prb_ali:RecordNumber),NOCASE,PRIMARY
KeyBandName              KEY(prb_ali:BandName),NOCASE
KeyMinPrice              KEY(prb_ali:MinPrice),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BandName                    STRING(30)
MinPrice                    REAL
MaxPrice                    REAL
                         END
                     END                       

SMSRECVD_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('SMSRECVD.DAT'),PRE(SMR1),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(SMR1:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMR1:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMR1:Job_Ref_Number,-SMR1:DateReceived,-SMR1:TimeReceived),DUP,NOCASE
KeyAccount_Date_Time     KEY(SMR1:AccountNumber,SMR1:DateReceived,SMR1:TextReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
AccountNumber               STRING(15)
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
SMSType                     STRING(1)
                         END
                     END                       

SMSText_Alias        FILE,DRIVER('Btrieve'),OEM,NAME('SMSText.Dat'),PRE(SMT1),CREATE,BINDABLE,THREAD
Key_Record_No            KEY(SMT1:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMT1:Status_Type,SMT1:Location,SMT1:Trigger_Status),DUP,NOCASE
Key_Description          KEY(SMT1:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMT1:Status_Type,SMT1:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMT1:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMT1:Location,SMT1:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMT1:Location,SMT1:Duplicate_Estimate),DUP,NOCASE
Key_Location_BER         KEY(SMT1:Location,SMT1:BER),DUP,NOCASE
Key_Location_Liquid      KEY(SMT1:Location,SMT1:LiquidDamage),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
LiquidDamage                STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                     END                       

WAYLAWT_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('WAYLAWT.DAT'),PRE(wal_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wal_ali:RecordNumber),NOCASE,PRIMARY
IMEINumberKey            KEY(wal_ali:IMEINumber),DUP,NOCASE
LOANRefNumberKey         KEY(wal_ali:LOANRefNumber),NOCASE
ReadyIMEINumberKey       KEY(wal_ali:Ready,wal_ali:IMEINumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
LOANRefNumber               LONG
Ready                       BYTE
RTARecordNumber             LONG
Location                    STRING(30)
                         END
                     END                       

RTNAWAIT_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('RTNAWAIT.DAT'),PRE(rta_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rta_ali:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(rta_ali:Archive,rta_ali:Processed,rta_ali:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(rta_ali:Archive,rta_ali:Processed,rta_ali:Description),DUP,NOCASE
ArcProExcPartKey         KEY(rta_ali:Archive,rta_ali:Processed,rta_ali:ExchangeOrder,rta_ali:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(rta_ali:Archive,rta_ali:Processed,rta_ali:ExchangeOrder,rta_ali:Description),DUP,NOCASE
ArcProCNRKey             KEY(rta_ali:Archive,rta_ali:Processed,rta_ali:CNRRecordNumber),DUP,NOCASE
ArcProExcCNRKey          KEY(rta_ali:Archive,rta_ali:Processed,rta_ali:ExchangeOrder,rta_ali:CNRRecordNumber),DUP,NOCASE
RTNORDERKey              KEY(rta_ali:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(rta_ali:CNRRecordNumber),DUP,NOCASE
GRNNumberKey             KEY(rta_ali:GRNNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
Location                    STRING(30)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
NewRefNumber                LONG
GRNNumber                   LONG
                         END
                     END                       

CURRENCY_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('CURRENCY.DAT'),PRE(cur_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cur_ali:RecordNumber),NOCASE,PRIMARY
CurrencyCodeKey          KEY(cur_ali:CurrencyCode),NOCASE
LastUpdateDateKey        KEY(cur_ali:LastUpdateDate,cur_ali:CurrencyCode),DUP,NOCASE
CorrelationCodeKey       KEY(cur_ali:CorrelationCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
CurrencyCode                STRING(30)
Description                 STRING(60)
DailyRate                   REAL
DivideMultiply              STRING(1)
LastUpdateDate              DATE
CorrelationCode             STRING(30)
                         END
                     END                       

Audit_Alias          FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(AUD1),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(AUD1:Ref_Number,-AUD1:Date,-AUD1:Time,AUD1:Action),DUP,NOCASE
Action_Key               KEY(AUD1:Ref_Number,AUD1:Action,-AUD1:Date),DUP,NOCASE
User_Key                 KEY(AUD1:Ref_Number,AUD1:User,-AUD1:Date),DUP,NOCASE
Record_Number_Key        KEY(AUD1:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(AUD1:Action,AUD1:Date),DUP,NOCASE
TypeRefKey               KEY(AUD1:Ref_Number,AUD1:Type,-AUD1:Date,-AUD1:Time,AUD1:Action),DUP,NOCASE
TypeActionKey            KEY(AUD1:Ref_Number,AUD1:Type,AUD1:Action,-AUD1:Date),DUP,NOCASE
TypeUserKey              KEY(AUD1:Ref_Number,AUD1:Type,AUD1:User,-AUD1:Date),DUP,NOCASE
DateActionJobKey         KEY(AUD1:Date,AUD1:Action,AUD1:Ref_Number),DUP,NOCASE
DateJobKey               KEY(AUD1:Date,AUD1:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(AUD1:Date,AUD1:Type,AUD1:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(AUD1:Date,AUD1:Type,AUD1:Action,AUD1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Type                        STRING(3)
                         END
                     END                       

STOMJFAU_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('STOMJFAU.DAT'),PRE(stj_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stj_ali:RecordNumber),NOCASE,PRIMARY
FieldKey                 KEY(stj_ali:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(255)
FaultCode11                 STRING(255)
FaultCode12                 STRING(255)
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

STOMPFAU_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('STOMPFAU.DAT'),PRE(stu_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stu_ali:RecordNumber),NOCASE,PRIMARY
FieldKey                 KEY(stu_ali:RefNumber,stu_ali:FieldNumber,stu_ali:Field),DUP,NOCASE
DescriptionKey           KEY(stu_ali:RefNumber,stu_ali:FieldNumber,stu_ali:Description),DUP,NOCASE
ManufacturerFieldKey     KEY(stu_ali:Manufacturer,stu_ali:FieldNumber,stu_ali:Field),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Manufacturer                STRING(30)
FieldNumber                 LONG
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

JOBSWARR_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('JOBSWARR.DAT'),PRE(jow_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jow_ali:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jow_ali:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(jow_ali:Status,jow_ali:Manufacturer,jow_ali:FirstSecondYear,jow_ali:RefNumber),DUP,NOCASE
StatusManKey             KEY(jow_ali:Status,jow_ali:Manufacturer,jow_ali:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(jow_ali:Status,jow_ali:Manufacturer,jow_ali:ClaimSubmitted,jow_ali:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(jow_ali:Status,jow_ali:Manufacturer,jow_ali:FirstSecondYear,jow_ali:ClaimSubmitted,jow_ali:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(jow_ali:BranchID,jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(jow_ali:BranchID,jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:Manufacturer,jow_ali:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(jow_ali:BranchID,jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:Manufacturer,jow_ali:RRCDateReconciled,jow_ali:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(jow_ali:BranchID,jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:RRCDateReconciled,jow_ali:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:Manufacturer,jow_ali:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:DateReconciled,jow_ali:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:Manufacturer,jow_ali:RRCDateReconciled,jow_ali:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(jow_ali:BranchID,jow_ali:RepairedAt,jow_ali:ClaimSubmitted,jow_ali:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(jow_ali:BranchID,jow_ali:ClaimSubmitted,jow_ali:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(jow_ali:ClaimSubmitted,jow_ali:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(jow_ali:BranchID,jow_ali:RepairedAt,jow_ali:RRCStatus,jow_ali:Manufacturer,jow_ali:DateAccepted,jow_ali:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(jow_ali:BranchID,jow_ali:DateAccepted,jow_ali:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(jow_ali:DateAccepted,jow_ali:RefNumber),DUP,NOCASE
RejectedBranchKey        KEY(jow_ali:BranchID,jow_ali:DateRejected,jow_ali:RefNumber),DUP,NOCASE
RejectedKey              KEY(jow_ali:DateRejected,jow_ali:RefNumber),DUP,NOCASE
FinalRejectionBranchKey  KEY(jow_ali:BranchID,jow_ali:DateFinalRejection,jow_ali:RefNumber),DUP,NOCASE
FinalRejectionKey        KEY(jow_ali:DateFinalRejection,jow_ali:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
DateRejected                DATE
DateFinalRejection          DATE
Orig_Sub_Date               DATE
                         END
                     END                       

MANUFACT_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(man_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(man_ali:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(man_ali:Manufacturer),NOCASE
EDIFileTypeKey           KEY(man_ali:EDIFileType,man_ali:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
ThirdPartyHandlingFee       REAL
ProductCodeCompulsory       STRING(3)
Inactive                    BYTE
                         END
                     END                       

MANFAUPA_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(map_ali),CREATE,BINDABLE,THREAD
Field_Number_Key         KEY(map_ali:Manufacturer,map_ali:Field_Number),NOCASE,PRIMARY
MainFaultKey             KEY(map_ali:Manufacturer,map_ali:MainFault),DUP,NOCASE
ScreenOrderKey           KEY(map_ali:Manufacturer,map_ali:ScreenOrder),DUP,NOCASE
KeyRepairKey             KEY(map_ali:Manufacturer,map_ali:KeyRepair),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
MainFault                   BYTE
UseRelatedJobCode           BYTE
NotAvailable                BYTE
ScreenOrder                 LONG
CopyFromJobFaultCode        BYTE
CopyJobFaultCode            LONG
NAForAccessory              BYTE
CompulsoryForAdjustment     BYTE
KeyRepair                   BYTE
CCTReferenceFaultCode       BYTE
NAForSW                     BYTE
                         END
                     END                       

JOBSE2_ALIAS         FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE2.DAT'),PRE(jobe2_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe2_ali:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe2_ali:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
POPConfirmed                BYTE
ThirdPartyHandlingFee       REAL
InvThirdPartyHandlingFee    REAL
                         END
                     END                       

MANFAULT_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(maf_ali),CREATE,BINDABLE,THREAD
Field_Number_Key         KEY(maf_ali:Manufacturer,maf_ali:Field_Number),NOCASE,PRIMARY
MainFaultKey             KEY(maf_ali:Manufacturer,maf_ali:MainFault),DUP,NOCASE
InFaultKey               KEY(maf_ali:Manufacturer,maf_ali:InFault),DUP,NOCASE
ScreenOrderKey           KEY(maf_ali:Manufacturer,maf_ali:ScreenOrder),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
MainFault                   BYTE
PromptForExchange           BYTE
InFault                     BYTE
CompulsoryIfExchange        BYTE
NotAvailable                BYTE
CharCompulsory              BYTE
CharCompulsoryBooking       BYTE
ScreenOrder                 LONG
RestrictAvailability        BYTE
RestrictServiceCentre       BYTE
GenericFault                BYTE
NotCompulsoryThirdParty     BYTE
HideThirdParty              BYTE
HideRelatedCodeIfBlank      BYTE
BlankRelatedCode            LONG
FillFromDOP                 BYTE
CompulsoryForRepairType     BYTE
CompulsoryRepairType        STRING(30)
                         END
                     END                       

MANFAULO_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(mfo_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mfo_ali:RecordNumber),NOCASE,PRIMARY
RelatedFieldKey          KEY(mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE
Field_Key                KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE
DescriptionKey           KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE
ManFieldKey              KEY(mfo_ali:Manufacturer,mfo_ali:Field),DUP,NOCASE
HideFieldKey             KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE
HideDescriptionKey       KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE
RelatedDescriptionKey    KEY(mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE
HideRelatedFieldKey      KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE
HideRelatedDescKey       KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE
FieldNumberKey           KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number),DUP,NOCASE
PrimaryLookupKey         KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:PrimaryLookup),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
ImportanceLevel             LONG
SkillLevel                  LONG
RepairType                  STRING(30)
RepairTypeWarranty          STRING(30)
HideFromEngineer            BYTE
ForcePartCode               LONG
RelatedPartCode             LONG
PromptForExchange           BYTE
ExcludeFromBouncer          BYTE
ReturnToRRC                 BYTE
RestrictLookup              BYTE
RestrictLookupType          BYTE
NotAvailable                BYTE
PrimaryLookup               BYTE
SetJobFaultCode             BYTE
SelectJobFaultCode          LONG
JobFaultCodeValue           STRING(30)
JobTypeAvailability         BYTE
                         END
                     END                       

MANFPALO_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(mfp_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mfp_ali:RecordNumber),NOCASE,PRIMARY
Field_Key                KEY(mfp_ali:Manufacturer,mfp_ali:Field_Number,mfp_ali:Field),NOCASE
DescriptionKey           KEY(mfp_ali:Manufacturer,mfp_ali:Field_Number,mfp_ali:Description),DUP,NOCASE
AvailableFieldKey        KEY(mfp_ali:NotAvailable,mfp_ali:Manufacturer,mfp_ali:Field_Number,mfp_ali:Field),DUP,NOCASE
AvailableDescriptionKey  KEY(mfp_ali:Manufacturer,mfp_ali:NotAvailable,mfp_ali:Field_Number,mfp_ali:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
RestrictLookup              BYTE
RestrictLookupType          BYTE
NotAvailable                BYTE
ForceJobFaultCode           BYTE
ForceFaultCodeNumber        LONG
SetPartFaultCode            BYTE
SelectPartFaultCode         LONG
PartFaultCodeValue          STRING(30)
JobTypeAvailability         BYTE
                         END
                     END                       

JOBSOBF_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jofali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jofali:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jofali:RefNumber),DUP,NOCASE
StatusRefNumberKey       KEY(jofali:Status,jofali:RefNumber),DUP,NOCASE
StatusIMEINumberKey      KEY(jofali:Status,jofali:IMEINumber),DUP,NOCASE
HeadAccountCompletedKey  KEY(jofali:HeadAccountNumber,jofali:DateCompleted,jofali:RefNumber),DUP,NOCASE
HeadAccountProcessedKey  KEY(jofali:HeadAccountNumber,jofali:DateProcessed,jofali:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IMEINumber                  STRING(30)
Status                      BYTE
Replacement                 BYTE
StoreReferenceNumber        STRING(30)
RNumber                     STRING(30)
RejectionReason             STRING(255)
ReplacementIMEI             STRING(30)
LAccountNumber              STRING(30)
UserCode                    STRING(3)
HeadAccountNumber           STRING(30)
DateCompleted               DATE
TimeCompleted               TIME
DateProcessed               DATE
TimeProcessed               TIME
                         END
                     END                       

GRNOTESAlias         FILE,DRIVER('Btrieve'),OEM,NAME('GRNOTES.DAT'),PRE(grnali),CREATE,BINDABLE,THREAD
Goods_Received_Number_Key KEY(grnali:Goods_Received_Number),NOCASE,PRIMARY
Order_Number_Key         KEY(grnali:Order_Number,grnali:Goods_Received_Date),DUP,NOCASE
Date_Recd_Key            KEY(grnali:Goods_Received_Date),DUP,NOCASE
NotPrintedGRNKey         KEY(grnali:BatchRunNotPrinted,grnali:Goods_Received_Number),DUP,NOCASE
NotPrintedOrderKey       KEY(grnali:BatchRunNotPrinted,grnali:Order_Number,grnali:Goods_Received_Number),DUP,NOCASE
KeyEVO_Status            KEY(grnali:EVO_Status),DUP,NOCASE
Record                   RECORD,PRE()
Goods_Received_Number       LONG
Order_Number                LONG
Goods_Received_Date         DATE
CurrencyCode                STRING(30)
DailyRate                   REAL
DivideMultiply              STRING(1)
BatchRunNotPrinted          BYTE
Uncaptured                  BYTE
EVO_Status                  STRING(1)
                         END
                     END                       

JOBSE_ALIAS          FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe_ali:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe_ali:RefNumber),DUP,NOCASE
WarrStatusDateKey        KEY(jobe_ali:WarrantyStatusDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
HubRepair                   BYTE
Network                     STRING(30)
POPConfirmed                BYTE
HubRepairDate               DATE
HubRepairTime               TIME
ClaimValue                  REAL
HandlingFee                 REAL
ExchangeRate                REAL
InvoiceClaimValue           REAL
InvoiceHandlingFee          REAL
InvoiceExchangeRate         REAL
BoxESN                      STRING(20)
ValidPOP                    STRING(3)
ReturnDate                  DATE
TalkTime                    REAL
OriginalPackaging           BYTE
OriginalBattery             BYTE
OriginalCharger             BYTE
OriginalAntenna             BYTE
OriginalManuals             BYTE
PhysicalDamage              BYTE
OriginalDealer              CSTRING(255)
BranchOfReturn              STRING(30)
COverwriteRepairType        BYTE
WOverwriteRepairType        BYTE
LabourAdjustment            REAL
PartsAdjustment             REAL
SubTotalAdjustment          REAL
IgnoreClaimCosts            BYTE
RRCCLabourCost              REAL
RRCCPartsCost               REAL
RRCCPartsSale               REAL
RRCCSubTotal                REAL
InvRRCCLabourCost           REAL
InvRRCCPartsCost            REAL
InvRRCCPartsSale            REAL
InvRRCCSubTotal             REAL
RRCWLabourCost              REAL
RRCWPartsCost               REAL
RRCWPartsSale               REAL
RRCWSubTotal                REAL
InvRRCWLabourCost           REAL
InvRRCWPartsCost            REAL
InvRRCWPartsSale            REAL
InvRRCWSubTotal             REAL
ARC3rdPartyCost             REAL
InvARC3rdPartCost           REAL
WebJob                      BYTE
RRCELabourCost              REAL
RRCEPartsCost               REAL
RRCESubTotal                REAL
IgnoreRRCChaCosts           REAL
IgnoreRRCWarCosts           REAL
IgnoreRRCEstCosts           REAL
OBFvalidated                BYTE
OBFvalidateDate             DATE
OBFvalidateTime             TIME
DespatchType                STRING(3)
Despatched                  STRING(3)
WarrantyClaimStatus         STRING(30)
WarrantyStatusDate          DATE
InSecurityPackNo            STRING(30)
JobSecurityPackNo           STRING(30)
ExcSecurityPackNo           STRING(30)
LoaSecurityPackNo           STRING(30)
ExceedWarrantyRepairLimit   BYTE
BouncerClaim                BYTE
Sub_Sub_Account             STRING(15)
ConfirmClaimAdjustment      BYTE
ARC3rdPartyVAT              REAL
ARC3rdPartyInvoiceNumber    STRING(30)
ExchangeAdjustment          REAL
ExchangedATRRC              BYTE
EndUserTelNo                STRING(15)
ClaimColour                 BYTE
ARC3rdPartyMarkup           REAL
Ignore3rdPartyCosts         BYTE
POPType                     STRING(30)
OBFProcessed                BYTE
LoanReplacementValue        REAL
PendingClaimColour          BYTE
AccessoryNotes              STRING(255)
ClaimPartsCost              REAL
InvClaimPartsCost           REAL
Booking48HourOption         BYTE
Engineer48HourOption        BYTE
ExcReplcamentCharge         BYTE
SecondExchangeNumber        LONG
SecondExchangeStatus        STRING(30)
VatNumber                   STRING(30)
ExchangeProductCode         STRING(30)
SecondExcProdCode           STRING(30)
ARC3rdPartyInvoiceDate      DATE
ARC3rdPartyWaybillNo        STRING(30)
ARC3rdPartyRepairType       STRING(30)
ARC3rdPartyRejectedReason   STRING(255)
ARC3rdPartyRejectedAmount   REAL
VSACustomer                 BYTE
HandsetReplacmentValue      REAL
SecondHandsetRepValue       REAL
t                           STRING(20)
                         END
                     END                       

REPTYDEF_ALIAS       FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(rtd_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rtd_ali:RecordNumber),NOCASE,PRIMARY
ManRepairTypeKey         KEY(rtd_ali:Manufacturer,rtd_ali:Repair_Type),NOCASE
Repair_Type_Key          KEY(rtd_ali:Repair_Type),DUP,NOCASE
Chargeable_Key           KEY(rtd_ali:Chargeable,rtd_ali:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(rtd_ali:Warranty,rtd_ali:Repair_Type),DUP,NOCASE
ChaManRepairTypeKey      KEY(rtd_ali:Manufacturer,rtd_ali:Chargeable,rtd_ali:Repair_Type),DUP,NOCASE
WarManRepairTypeKey      KEY(rtd_ali:Manufacturer,rtd_ali:Warranty,rtd_ali:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
ExcludeFromBouncer          BYTE
PromptForExchange           BYTE
JobWeighting                LONG
SkillLevel                  LONG
Manufacturer                STRING(30)
RepairLevel                 LONG
ForceAdjustment             BYTE
ScrapExchange               BYTE
ExcludeHandlingFee          BYTE
NotAvailable                BYTE
NoParts                     STRING(1)
SMSSendType                 STRING(1)
                         END
                     END                       

SUBEMAIL_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('SUBEMAIL.DAT'),PRE(sue_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sue_ali:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(sue_ali:RefNumber,sue_ali:RecipientType),DUP,NOCASE
ContactNameKey           KEY(sue_ali:RefNumber,sue_ali:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
SendStatusEmails            BYTE
SendReportEmails            BYTE
                         END
                     END                       

SUBCHRGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('SUBCHRGE.DAT'),PRE(suc_ali),CREATE,BINDABLE,THREAD
Model_Repair_Type_Key    KEY(suc_ali:Account_Number,suc_ali:Model_Number,suc_ali:Charge_Type,suc_ali:Unit_Type,suc_ali:Repair_Type),NOCASE,PRIMARY
Account_Charge_Key       KEY(suc_ali:Account_Number,suc_ali:Model_Number,suc_ali:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(suc_ali:Account_Number,suc_ali:Model_Number,suc_ali:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(suc_ali:Account_Number,suc_ali:Model_Number,suc_ali:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(suc_ali:Account_Number,suc_ali:Model_Number,suc_ali:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(suc_ali:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(suc_ali:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(suc_ali:Unit_Type),DUP,NOCASE
Model_Repair_Key         KEY(suc_ali:Model_Number,suc_ali:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
WarrantyClaimRate           REAL
HandlingFee                 REAL
Exchange                    REAL
RRCRate                     REAL
                         END
                     END                       

SUBACCAD_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('SUBACCAD.DAT'),PRE(sua_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sua_ali:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sua_ali:RefNumber,sua_ali:AccountNumber),DUP,NOCASE
CompanyNameKey           KEY(sua_ali:RefNumber,sua_ali:CompanyName),DUP,NOCASE
AccountNumberOnlyKey     KEY(sua_ali:AccountNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccountNumber               STRING(15)
CompanyName                 STRING(30)
Postcode                    STRING(15)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(15)
FaxNumber                   STRING(15)
EmailAddress                STRING(255)
ContactName                 STRING(30)
                         END
                     END                       

TRADEACC_ALIAS       FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(tra_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tra_ali:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(tra_ali:Account_Number),NOCASE
Company_Name_Key         KEY(tra_ali:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(tra_ali:ReplicateAccount,tra_ali:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(tra_ali:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(tra_ali:SiteLocation),DUP,NOCASE
RegionKey                KEY(tra_ali:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
                         END
                     END                       

SUBTRACC_ALIAS       FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(sub_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sub_ali:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(sub_ali:Main_Account_Number,sub_ali:Account_Number),NOCASE
Main_Name_Key            KEY(sub_ali:Main_Account_Number,sub_ali:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(sub_ali:Account_Number),NOCASE
Branch_Key               KEY(sub_ali:Branch),DUP,NOCASE
Main_Branch_Key          KEY(sub_ali:Main_Account_Number,sub_ali:Branch),DUP,NOCASE
Company_Name_Key         KEY(sub_ali:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(sub_ali:ReplicateAccount,sub_ali:Account_Number),DUP,NOCASE
GenericAccountKey        KEY(sub_ali:Generic_Account,sub_ali:Account_Number),DUP,NOCASE
GenericCompanyKey        KEY(sub_ali:Generic_Account,sub_ali:Company_Name),DUP,NOCASE
GenericBranchKey         KEY(sub_ali:Generic_Account,sub_ali:Branch),DUP,NOCASE
RegionKey                KEY(sub_ali:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
ReplicateAccount            STRING(30)
FactorAccount               BYTE
ForceEstimate               BYTE
EstimateIfOver              REAL
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
Generic_Account             BYTE
StoresAccountNumber         STRING(30)
Force_Customer_Name         BYTE
FinanceContactName          STRING(30)
FinanceTelephoneNo          STRING(30)
FinanceEmailAddress         STRING(255)
FinanceAddress1             STRING(30)
FinanceAddress2             STRING(30)
FinanceAddress3             STRING(30)
FinancePostcode             STRING(30)
AccountLimit                REAL
ExcludeBouncer              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
ExcludeFromTATReport        BYTE
OverrideHeadVATNo           BYTE
Line500AccountNumber        STRING(30)
OverrideHeadMobile          BYTE
OverrideMobileDefault       BYTE
SIDJobBooking               BYTE
SIDJobEnquiry               BYTE
Region                      STRING(30)
AllowVCPLoanUnits           BYTE
Hub                         STRING(30)
RefurbishmentAccount        BYTE
VCPWaybillPrefix            STRING(30)
PrintOOWVCPFee              BYTE
OOWVCPFeeLabel              STRING(30)
OOWVCPFeeAmount             REAL
PrintWarrantyVCPFee         BYTE
WarrantyVCPFeeAmount        REAL
DealerID                    STRING(30)
                         END
                     END                       

ESNMODEL_ALIAS       FILE,DRIVER('Btrieve'),NAME('ESNMODEL.DAT'),PRE(esn_ali),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(esn_ali:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(esn_ali:ESN,esn_ali:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(esn_ali:ESN),DUP,NOCASE
Model_Number_Key         KEY(esn_ali:Model_Number,esn_ali:ESN),DUP,NOCASE
Manufacturer_Key         KEY(esn_ali:Manufacturer,esn_ali:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(8)
Model_Number                STRING(30)
Manufacturer                STRING(30)
Include48Hour               BYTE
Active                      STRING(1)
                         END
                     END                       

ESNMODAL_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('ESNMODAL.DAT'),PRE(esa_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(esa_ali:RecordNumber),NOCASE,PRIMARY
TACCodeKey               KEY(esa_ali:RefNumber,esa_ali:TacCode),DUP,NOCASE
TacModelKey              KEY(esa_ali:RefNumber,esa_ali:TacCode,esa_ali:ModelNumber),DUP,NOCASE
RefModelNumberKey        KEY(esa_ali:RefNumber,esa_ali:ModelNumber),DUP,NOCASE
ModelNumberOnlyKey       KEY(esa_ali:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TacCode                     STRING(8)
ModelNumber                 STRING(30)
                         END
                     END                       

MULDESP_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(muld_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(muld_ali:RecordNumber),NOCASE,PRIMARY
BatchNumberKey           KEY(muld_ali:BatchNumber),DUP,NOCASE
AccountNumberKey         KEY(muld_ali:AccountNumber),DUP,NOCASE
CourierKey               KEY(muld_ali:Courier),DUP,NOCASE
HeadBatchNumberKey       KEY(muld_ali:HeadAccountNumber,muld_ali:BatchNumber),DUP,NOCASE
HeadAccountKey           KEY(muld_ali:HeadAccountNumber,muld_ali:AccountNumber),DUP,NOCASE
HeadCourierKey           KEY(muld_ali:HeadAccountNumber,muld_ali:Courier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BatchNumber                 STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
BatchTotal                  LONG
BatchType                   STRING(3)
HeadAccountNumber           STRING(30)
                         END
                     END                       

MULDESPJ_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('MULDESPJ.DAT'),PRE(mulj_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mulj_ali:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(mulj_ali:RefNumber,mulj_ali:JobNumber),DUP,NOCASE
CurrentJobKey            KEY(mulj_ali:RefNumber,mulj_ali:Current,mulj_ali:JobNumber),DUP,NOCASE
JobNumberOnlyKey         KEY(mulj_ali:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobNumber                   LONG
IMEINumber                  STRING(30)
MSN                         STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
Current                     BYTE
SecurityPackNumber          STRING(30)
                         END
                     END                       

JOBS2_ALIAS          FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job2),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job2:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job2:Model_Number,job2:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job2:Engineer,job2:Completed,job2:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job2:Engineer,job2:Workshop,job2:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job2:Surname),DUP,NOCASE
MobileNumberKey          KEY(job2:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job2:ESN),DUP,NOCASE
MSN_Key                  KEY(job2:MSN),DUP,NOCASE
AccountNumberKey         KEY(job2:Account_Number,job2:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job2:Account_Number,job2:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job2:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job2:Engineer,job2:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job2:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job2:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job2:Model_Number,job2:Date_Completed),DUP,NOCASE
By_Status                KEY(job2:Current_Status,job2:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job2:Current_Status,job2:Location,job2:Ref_Number),DUP,NOCASE
Location_Key             KEY(job2:Location,job2:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job2:Job_Priority,job2:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job2:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job2:Manufacturer,job2:EDI,job2:EDI_Batch_Number,job2:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job2:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job2:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job2:Batch_Number,job2:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job2:Batch_Number,job2:Current_Status,job2:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job2:Batch_Number,job2:Model_Number,job2:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job2:Batch_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job2:Batch_Number,job2:Completed,job2:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job2:Chargeable_Job,job2:Account_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job2:Invoice_Exception,job2:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job2:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job2:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job2:Despatched,job2:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job2:Despatched,job2:Account_Number,job2:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job2:Despatched,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job2:Despatched,job2:Account_Number,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job2:Despatch_Number,job2:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job2:Courier,job2:Date_Despatched,job2:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job2:Loan_Courier,job2:Loan_Despatched,job2:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job2:Exchange_Courier,job2:Exchange_Despatched,job2:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job2:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job2:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job2:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job2:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job2:Bouncer,job2:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job2:Engineer,job2:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job2:Exchange_Status,job2:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job2:Loan_Status,job2:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job2:Exchange_Status,job2:Location,job2:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job2:Loan_Status,job2:Location,job2:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:InvoiceStatus,job2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

LOGASSST_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('LOGASSST.DAT'),PRE(logast_alias),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logast_alias:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logast_alias:RefNumber,logast_alias:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PartRefNumber               LONG
Location                    STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

LOGSTOCK_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOCK.DAT'),PRE(logsto_ali),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logsto_ali:RefNumber),NOCASE,PRIMARY
SalesKey                 KEY(logsto_ali:SalesCode),DUP,NOCASE
DescriptionKey           KEY(logsto_ali:Description),DUP,NOCASE
SalesModelNoKey          KEY(logsto_ali:SalesCode,logsto_ali:ModelNumber),NOCASE
RefModelNoKey            KEY(logsto_ali:RefNumber,logsto_ali:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(logsto_ali:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
ModelNumber                 STRING(30)
DummyField                  STRING(4)
                         END
                     END                       

LOGSERST_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('LOGSERST.DAT'),PRE(logser_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logser_ali:RecordNumber),NOCASE,PRIMARY
ESNKey                   KEY(logser_ali:ESN),DUP,NOCASE
RefNumberKey             KEY(logser_ali:RefNumber,logser_ali:ESN),DUP,NOCASE
ESNStatusKey             KEY(logser_ali:RefNumber,logser_ali:Status,logser_ali:ESN),DUP,NOCASE
NokiaStatusKey           KEY(logser_ali:RefNumber,logser_ali:Status,logser_ali:ClubNokia,logser_ali:ESN),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ESN                         STRING(30)
Status                      STRING(10)
ClubNokia                   STRING(30)
DummyField                  STRING(4)
                         END
                     END                       

COURIER_ALIAS        FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(cou_ali),CREATE,BINDABLE,THREAD
Courier_Key              KEY(cou_ali:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(cou_ali:Courier_Type,cou_ali:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
PrintWaybill                BYTE
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             BYTE
IncludeSunday               BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
EmailAddress                STRING(255)
FromEmailAddress            STRING(255)
                         END
                     END                       

JOBNOTES_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn_ali),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jbn_ali:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

TRDBATCH_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(trb_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(trb_ali:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(trb_ali:Company_Name,trb_ali:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(trb_ali:Status,trb_ali:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(trb_ali:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(trb_ali:Status,trb_ali:Company_Name,trb_ali:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(trb_ali:ESN),DUP,NOCASE
ESN_Status_Key           KEY(trb_ali:Status,trb_ali:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(trb_ali:Company_Name,trb_ali:Batch_Number,trb_ali:ESN),DUP,NOCASE
AuthorisationKey         KEY(trb_ali:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(trb_ali:Status,trb_ali:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(trb_ali:Company_Name,trb_ali:Status,trb_ali:DateReturn),DUP,NOCASE
JobStatusKey             KEY(trb_ali:Status,trb_ali:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(trb_ali:Ref_Number),DUP,NOCASE
CompanyDespatchedKey     KEY(trb_ali:Company_Name,trb_ali:DateDespatched),DUP,NOCASE
ReturnDateKey            KEY(trb_ali:DateReturn),DUP,NOCASE
ReturnCompanyKey         KEY(trb_ali:DateReturn,trb_ali:Company_Name),DUP,NOCASE
NotPrintedManJobKey      KEY(trb_ali:BatchRunNotPrinted,trb_ali:Status,trb_ali:Company_Name,trb_ali:Ref_Number),DUP,NOCASE
PurchaseOrderKey         KEY(trb_ali:PurchaseOrderNumber,trb_ali:Ref_Number),DUP,NOCASE
KeyEVO_Status            KEY(trb_ali:EVO_Status),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
DateDespatched              DATE
TimeDespatched              TIME
ReturnUser                  STRING(3)
ReturnWaybillNo             STRING(30)
ReturnRepairType            STRING(30)
ReturnRejectedAmount        REAL
ReturnRejectedReason        STRING(255)
ThirdPartyInvoiceNo         STRING(30)
ThirdPartyInvoiceDate       DATE
ThirdPartyInvoiceCharge     REAL
ThirdPartyVAT               REAL
ThirdPartyMarkup            REAL
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
BatchRunNotPrinted          BYTE
PurchaseOrderNumber         LONG
NewThirdPartySite           STRING(30)
NewThirdPartySiteID         STRING(30)
EVO_Status                  STRING(1)
                         END
                     END                       

INVOICE_ALIAS        FILE,DRIVER('Btrieve'),NAME('INVOICE.DAT'),PRE(inv_ali),CREATE,BINDABLE,THREAD
Invoice_Number_Key       KEY(inv_ali:Invoice_Number),NOCASE,PRIMARY
Invoice_Type_Key         KEY(inv_ali:Invoice_Type,inv_ali:Invoice_Number),DUP,NOCASE
Account_Number_Type_Key  KEY(inv_ali:Invoice_Type,inv_ali:Account_Number,inv_ali:Invoice_Number),DUP,NOCASE
Account_Number_Key       KEY(inv_ali:Account_Number,inv_ali:Invoice_Number),DUP,NOCASE
Date_Created_Key         KEY(inv_ali:Date_Created),DUP,NOCASE
Batch_Number_Key         KEY(inv_ali:Manufacturer,inv_ali:Batch_Number),DUP,NOCASE
OracleDateKey            KEY(inv_ali:ExportedOracleDate,inv_ali:Invoice_Number),DUP,NOCASE
OracleNumberKey          KEY(inv_ali:OracleNumber,inv_ali:Invoice_Number),DUP,NOCASE
Account_Date_Key         KEY(inv_ali:Account_Number,inv_ali:Date_Created),DUP,NOCASE
RRCInvoiceDateKey        KEY(inv_ali:RRCInvoiceDate),DUP,NOCASE
ARCInvoiceDateKey        KEY(inv_ali:ARCInvoiceDate),DUP,NOCASE
ReconciledDateKey        KEY(inv_ali:Reconciled_Date),DUP,NOCASE
AccountReconciledKey     KEY(inv_ali:Account_Number,inv_ali:Reconciled_Date),DUP,NOCASE
Record                   RECORD,PRE()
Invoice_Number              REAL
Invoice_Type                STRING(3)
Job_Number                  REAL
Date_Created                DATE
Account_Number              STRING(15)
AccountType                 STRING(3)
Total                       REAL
Vat_Rate_Labour             REAL
Vat_Rate_Parts              REAL
Vat_Rate_Retail             REAL
VAT_Number                  STRING(30)
Invoice_VAT_Number          STRING(30)
Currency                    STRING(30)
Batch_Number                REAL
Manufacturer                STRING(30)
Claim_Reference             STRING(30)
Total_Claimed               REAL
Courier_Paid                REAL
Labour_Paid                 REAL
Parts_Paid                  REAL
Reconciled_Date             DATE
jobs_count                  REAL
PrevInvoiceNo               LONG
InvoiceCredit               STRING(3)
UseAlternativeAddress       BYTE
EuroExhangeRate             REAL
RRCVatRateLabour            REAL
RRCVatRateParts             REAL
RRCVatRateRetail            REAL
ExportedRRCOracle           BYTE
ExportedARCOracle           BYTE
ExportedOracleDate          DATE
OracleNumber                LONG
RRCInvoiceDate              DATE
ARCInvoiceDate              DATE
                         END
                     END                       

ORDPEND_ALIAS        FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ope_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ope_ali:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ope_ali:Supplier,ope_ali:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ope_ali:Supplier,ope_ali:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ope_ali:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ope_ali:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ope_ali:Awaiting_Stock,ope_ali:Supplier,ope_ali:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ope_ali:PartRecordNumber),DUP,NOCASE
ReqPartNumber            KEY(ope_ali:StockReqNumber,ope_ali:Part_Number),DUP,NOCASE
ReqDescriptionKey        KEY(ope_ali:StockReqNumber,ope_ali:Description),DUP,NOCASE
KeyPrevStoReqNo          KEY(ope_ali:PrevStoReqNo),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
StockReqNumber              LONG
PrevStoReqNo                LONG
                         END
                     END                       

RETSALES_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(res_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(res_ali:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(res_ali:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(res_ali:Despatched,res_ali:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(res_ali:Despatched,res_ali:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(res_ali:Despatched,res_ali:Account_Number,res_ali:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(res_ali:Account_Number,res_ali:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(res_ali:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(res_ali:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(res_ali:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(res_ali:Account_Number,res_ali:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(res_ali:Despatched,res_ali:Purchase_Order_Number,res_ali:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(res_ali:Despatch_Number,res_ali:Ref_Number),DUP,NOCASE
AccountPurchaseNumberKey KEY(res_ali:Account_Number,res_ali:Purchase_Order_Number),DUP,NOCASE
WaybillNumberKey         KEY(res_ali:WaybillNumber,res_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
WaybillNumber               LONG
DatePickingNotePrinted      DATE
TimePickingNotePrinted      TIME
ExchangeOrder               BYTE
LoanOrder                   BYTE
                         END
                     END                       

RETSTOCK_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(ret_ali),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(ret_ali:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(ret_ali:Ref_Number,ret_ali:Part_Number),DUP,NOCASE
Despatched_Key           KEY(ret_ali:Ref_Number,ret_ali:Despatched,ret_ali:Despatch_Note_Number,ret_ali:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(ret_ali:Ref_Number,ret_ali:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(ret_ali:Ref_Number,ret_ali:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(ret_ali:Ref_Number,ret_ali:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(ret_ali:Ref_Number,ret_ali:Order_Number),DUP,NOCASE
DespatchedKey            KEY(ret_ali:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(ret_ali:Ref_Number,ret_ali:Despatched,ret_ali:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(ret_ali:Amend_Site_Loc,ret_ali:Amended,ret_ali:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(ret_ali:Ref_Number,ret_ali:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(ret_ali:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(ret_ali:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(ret_ali:Despatched,ret_ali:Part_Number),DUP,NOCASE
ExchangeRefNumberKey     KEY(ret_ali:Ref_Number,ret_ali:ExchangeRefNumber),DUP,NOCASE
LoanRefNumberKey         KEY(ret_ali:Ref_Number,ret_ali:LoanRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
ExchangeRefNumber           LONG
LoanRefNumber               LONG
                         END
                     END                       

LOCATION_ALIAS       FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(loc_ali:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(loc_ali:Location),NOCASE
Main_Store_Key           KEY(loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE
ActiveLocationKey        KEY(loc_ali:Active,loc_ali:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(loc_ali:Active,loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE
VirtualLocationKey       KEY(loc_ali:VirtualSite,loc_ali:Location),DUP,NOCASE
VirtualMainStoreKey      KEY(loc_ali:VirtualSite,loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE
FaultyLocationKey        KEY(loc_ali:FaultyPartsLocation),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
VirtualSite                 BYTE
Level1                      BYTE
Level2                      BYTE
Level3                      BYTE
InWarrantyMarkUp            REAL
OutWarrantyMarkUp           REAL
UseRapidStock               BYTE
FaultyPartsLocation         BYTE
                         END
                     END                       

COMMONFA_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('COMMONFA.DAT'),PRE(com_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(com_ali:Ref_Number),NOCASE,PRIMARY
Description_Key          KEY(com_ali:Model_Number,com_ali:Category,com_ali:Description),DUP,NOCASE
DescripOnlyKey           KEY(com_ali:Model_Number,com_ali:Description),DUP,NOCASE
Ref_Model_Key            KEY(com_ali:Model_Number,com_ali:Category,com_ali:Ref_Number),DUP,NOCASE
RefOnlyKey               KEY(com_ali:Model_Number,com_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Category                    STRING(30)
date_booked                 DATE
time_booked                 TIME
who_booked                  STRING(3)
Model_Number                STRING(30)
Description                 STRING(30)
Chargeable_Job              STRING(3)
Chargeable_Charge_Type      STRING(30)
Chargeable_Repair_Type      STRING(30)
Warranty_Job                STRING(3)
Warranty_Charge_Type        STRING(30)
Warranty_Repair_Type        STRING(30)
Auto_Complete               STRING(3)
Attach_Diagram              STRING(3)
Diagram_Setting             STRING(1)
Diagram_Path                STRING(255)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(255)
Fault_Code11                  STRING(255)
Fault_Code12                  STRING(255)
                            END
Invoice_Text                STRING(255)
Engineers_Notes             STRING(255)
DummyField                  STRING(2)
                         END
                     END                       

COMMONCP_ALIAS       FILE,DRIVER('Btrieve'),NAME('COMMONCP.DAT'),PRE(ccp_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ccp_ali:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ccp_ali:Ref_Number),DUP,NOCASE
Description_Key          KEY(ccp_ali:Ref_Number,ccp_ali:Description),DUP,NOCASE
RefPartNumberKey         KEY(ccp_ali:Ref_Number,ccp_ali:Part_Number),DUP,NOCASE
PartNumberKey            KEY(ccp_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

COMMONWP_ALIAS       FILE,DRIVER('Btrieve'),NAME('COMMONWP.DAT'),PRE(cwp_alias),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cwp_alias:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(cwp_alias:Ref_Number),DUP,NOCASE
Description_Key          KEY(cwp_alias:Ref_Number,cwp_alias:Description),DUP,NOCASE
RefPartNumberKey         KEY(cwp_alias:Ref_Number,cwp_alias:Part_Number),DUP,NOCASE
PartNumberKey            KEY(cwp_alias:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

STOMODEL_ALIAS       FILE,DRIVER('Btrieve'),NAME('STOMODEL.DAT'),PRE(stm_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stm_ali:RecordNumber),NOCASE,PRIMARY
Model_Number_Key         KEY(stm_ali:Ref_Number,stm_ali:Manufacturer,stm_ali:Model_Number),NOCASE
Model_Part_Number_Key    KEY(stm_ali:Accessory,stm_ali:Model_Number,stm_ali:Location,stm_ali:Part_Number),DUP,NOCASE
Description_Key          KEY(stm_ali:Accessory,stm_ali:Model_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE
Manufacturer_Key         KEY(stm_ali:Manufacturer,stm_ali:Model_Number),DUP,NOCASE
Ref_Part_Description     KEY(stm_ali:Ref_Number,stm_ali:Part_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE
Location_Part_Number_Key KEY(stm_ali:Model_Number,stm_ali:Location,stm_ali:Part_Number),DUP,NOCASE
Location_Description_Key KEY(stm_ali:Model_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE
Mode_Number_Only_Key     KEY(stm_ali:Ref_Number,stm_ali:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Manufacturer                STRING(30)
Model_Number                STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Location                    STRING(30)
Accessory                   STRING(3)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(30)
FaultCode11                 STRING(30)
FaultCode12                 STRING(30)
RecordNumber                LONG
                         END
                     END                       

JOBPAYMT_ALIAS       FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt_ali),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jpt_ali:Record_Number),NOCASE,PRIMARY
All_Date_Key             KEY(jpt_ali:Ref_Number,jpt_ali:Date),DUP,NOCASE
Loan_Deposit_Key         KEY(jpt_ali:Ref_Number,jpt_ali:Loan_Deposit,jpt_ali:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
Loan_Deposit                BYTE
                         END
                     END                       

PARTSTMP_ALIAS       FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(partmp_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(partmp_ali:Ref_Number,partmp_ali:Part_Number),DUP,NOCASE
record_number_key        KEY(partmp_ali:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(partmp_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(partmp_ali:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(partmp_ali:Ref_Number,partmp_ali:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(partmp_ali:Ref_Number,partmp_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(partmp_ali:Ref_Number,partmp_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(partmp_ali:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

WPARTTMP_ALIAS       FILE,DRIVER('Btrieve'),NAME(glo:file_name2),PRE(wartmp_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wartmp_ali:Ref_Number,wartmp_ali:Part_Number),DUP,NOCASE
record_number_key        KEY(wartmp_ali:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(wartmp_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wartmp_ali:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(wartmp_ali:Ref_Number,wartmp_ali:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(wartmp_ali:Ref_Number,wartmp_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wartmp_ali:Ref_Number,wartmp_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wartmp_ali:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Main_Part                   STRING(3)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

LOAN_ALIAS           FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(loa_ali:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(loa_ali:ESN),DUP,NOCASE
MSN_Only_Key             KEY(loa_ali:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE
ESN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE
MSN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE
Ref_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(loa_ali:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(loa_ali:Available,loa_ali:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(loa_ali:Available,loa_ali:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(loa_ali:Available,loa_ali:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(loa_ali:Available,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(loa_ali:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE
LocIMEIKey               KEY(loa_ali:Location,loa_ali:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE
LocRefKey                KEY(loa_ali:Location,loa_ali:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
LocModelKey              KEY(loa_ali:Location,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:MSN),DUP,NOCASE
LocStockMSNKey           KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE
LocMSNKey                KEY(loa_ali:Location,loa_ali:MSN),DUP,NOCASE
AvailLocIMEI             KEY(loa_ali:Available,loa_ali:Location,loa_ali:ESN),DUP,NOCASE
AvailLocMSN              KEY(loa_ali:Available,loa_ali:Location,loa_ali:MSN),DUP,NOCASE
AvailLocRef              KEY(loa_ali:Available,loa_ali:Location,loa_ali:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(loa_ali:Available,loa_ali:Location,loa_ali:Model_Number),DUP,NOCASE
InTransitLocationKey     KEY(loa_ali:InTransit,loa_ali:Location,loa_ali:ESN),DUP,NOCASE
InTransitKey             KEY(loa_ali:InTransit,loa_ali:ESN),DUP,NOCASE
StatusChangeDateKey      KEY(loa_ali:StatusChangeDate),DUP,NOCASE
LocStatusChangeDateKey   KEY(loa_ali:Location,loa_ali:StatusChangeDate),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
DummyField                  STRING(1)
StatusChangeDate            DATE
InTransit                   BYTE
                         END
                     END                       

EXCHANGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(xch_ali:Ref_Number),NOCASE,PRIMARY
AvailLocIMEI             KEY(xch_ali:Available,xch_ali:Location,xch_ali:ESN),DUP,NOCASE
AvailLocMSN              KEY(xch_ali:Available,xch_ali:Location,xch_ali:MSN),DUP,NOCASE
AvailLocRef              KEY(xch_ali:Available,xch_ali:Location,xch_ali:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(xch_ali:Available,xch_ali:Location,xch_ali:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(xch_ali:ESN),DUP,NOCASE
MSN_Only_Key             KEY(xch_ali:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE
ESN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE
MSN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE
Ref_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(xch_ali:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(xch_ali:Available,xch_ali:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(xch_ali:Available,xch_ali:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(xch_ali:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE
LocIMEIKey               KEY(xch_ali:Location,xch_ali:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE
LocRefKey                KEY(xch_ali:Location,xch_ali:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
LocModelKey              KEY(xch_ali:Location,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:MSN),DUP,NOCASE
LocStockMSNKey           KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE
LocMSNKey                KEY(xch_ali:Location,xch_ali:MSN),DUP,NOCASE
InTransitLocationKey     KEY(xch_ali:InTransit,xch_ali:Location,xch_ali:ESN),DUP,NOCASE
InTransitKey             KEY(xch_ali:InTransit,xch_ali:ESN),DUP,NOCASE
StatusChangeDateKey      KEY(xch_ali:StatusChangeDate),DUP,NOCASE
LocStatusChangeDatekey   KEY(xch_ali:Location,xch_ali:StatusChangeDate),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
InTransit                   BYTE
FreeStockPurchased          STRING(1)
StatusChangeDate            DATE
                         END
                     END                       

ACCAREAS_ALIAS       FILE,DRIVER('Btrieve'),NAME('ACCAREAS.DAT'),PRE(acc_ali),CREATE,BINDABLE,THREAD
Access_level_key         KEY(acc_ali:User_Level,acc_ali:Access_Area),NOCASE,PRIMARY
AccessOnlyKey            KEY(acc_ali:Access_Area),DUP,NOCASE
Record                   RECORD,PRE()
Access_Area                 STRING(30)
User_Level                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

USERS_ALIAS          FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use_ali),CREATE,BINDABLE,THREAD
User_Code_Key            KEY(use_ali:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(use_ali:User_Type,use_ali:Active,use_ali:Surname),DUP,NOCASE
Surname_Active_Key       KEY(use_ali:Active,use_ali:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(use_ali:Active,use_ali:User_Code),DUP,NOCASE
User_Type_Key            KEY(use_ali:User_Type,use_ali:Surname),DUP,NOCASE
surname_key              KEY(use_ali:Surname),DUP,NOCASE
password_key             KEY(use_ali:Password),NOCASE
Logged_In_Key            KEY(use_ali:Logged_In,use_ali:Surname),DUP,NOCASE
Team_Surname             KEY(use_ali:Team,use_ali:Surname),DUP,NOCASE
Active_Team_Surname      KEY(use_ali:Team,use_ali:Active,use_ali:Surname),DUP,NOCASE
LocationSurnameKey       KEY(use_ali:Location,use_ali:Surname),DUP,NOCASE
LocationForenameKey      KEY(use_ali:Location,use_ali:Forename),DUP,NOCASE
LocActiveSurnameKey      KEY(use_ali:Active,use_ali:Location,use_ali:Surname),DUP,NOCASE
LocActiveForenameKey     KEY(use_ali:Active,use_ali:Location,use_ali:Forename),DUP,NOCASE
TeamStatusKey            KEY(use_ali:IncludeInEngStatus,use_ali:Team,use_ali:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
RenewPassword               LONG
PasswordLastChanged         DATE
RestrictParts               BYTE
RestrictChargeable          BYTE
RestrictWarranty            BYTE
IncludeInEngStatus          BYTE
MobileNumber                STRING(20)
                         END
                     END                       

WARPARTS_ALIAS       FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(war_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(war_ali:Ref_Number,war_ali:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(war_ali:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(war_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(war_ali:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(war_ali:Ref_Number,war_ali:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(war_ali:Ref_Number,war_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(war_ali:Ref_Number,war_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(war_ali:Supplier),DUP,NOCASE
Order_Part_Key           KEY(war_ali:Ref_Number,war_ali:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(war_ali:Supplier,war_ali:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(war_ali:Supplier,war_ali:Date_Received),DUP,NOCASE
RequestedKey             KEY(war_ali:Requested,war_ali:Part_Number),DUP,NOCASE
WebOrderKey              KEY(war_ali:WebOrder,war_ali:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(war_ali:PartAllocated,war_ali:Part_Number),DUP,NOCASE
StatusKey                KEY(war_ali:Status,war_ali:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(war_ali:PartAllocated,war_ali:Status,war_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
CostAdjustment              REAL
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

ORDPARTS_ALIAS       FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(orp_ali),CREATE,BINDABLE,THREAD
Order_Number_Key         KEY(orp_ali:Order_Number,orp_ali:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(orp_ali:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(orp_ali:Part_Ref_Number,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE
Part_Number_Key          KEY(orp_ali:Order_Number,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE
Description_Key          KEY(orp_ali:Order_Number,orp_ali:Description,orp_ali:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(orp_ali:All_Received,orp_ali:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(orp_ali:Account_Number,orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Part_Number),DUP,NOCASE
Allocated_Key            KEY(orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Allocated_To_Sale,orp_ali:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Allocated_To_Sale,orp_ali:Account_Number,orp_ali:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(orp_ali:Order_Number,orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE
PartRecordNumberKey      KEY(orp_ali:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
GRN_Number                  LONG
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
ReceivedCurrency            STRING(30)
ReceivedDailyRate           REAL
ReceivedDivideMultiply      STRING(1)
FreeExchangeStock           BYTE
TimeReceived                TIME
DatePriceCaptured           DATE
TimePriceCaptured           TIME
UncapturedGRNNumber         LONG
                         END
                     END                       

PARTS_ALIAS          FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(par_ali:Ref_Number,par_ali:Part_Number),DUP,NOCASE
recordnumberkey          KEY(par_ali:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(par_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(par_ali:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(par_ali:Ref_Number,par_ali:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(par_ali:Ref_Number,par_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(par_ali:Ref_Number,par_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(par_ali:Supplier),DUP,NOCASE
Order_Part_Key           KEY(par_ali:Ref_Number,par_ali:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(par_ali:Supplier,par_ali:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(par_ali:Supplier,par_ali:Date_Received),DUP,NOCASE
RequestedKey             KEY(par_ali:Requested,par_ali:Part_Number),DUP,NOCASE
WebOrderKey              KEY(par_ali:WebOrder,par_ali:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(par_ali:PartAllocated,par_ali:Part_Number),DUP,NOCASE
StatusKey                KEY(par_ali:Status,par_ali:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(par_ali:PartAllocated,par_ali:Status,par_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

STOCK_ALIAS          FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(sto_ali:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(sto_ali:Sundry_Item),DUP,NOCASE
Description_Key          KEY(sto_ali:Location,sto_ali:Description),DUP,NOCASE
Description_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Description),DUP,NOCASE
Shelf_Location_Key       KEY(sto_ali:Location,sto_ali:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
Supplier_Key             KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Supplier),DUP,NOCASE
Location_Key             KEY(sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(sto_ali:Location,sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(sto_ali:Location,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Description),DUP,NOCASE
SecondLocKey             KEY(sto_ali:Location,sto_ali:Shelf_Location,sto_ali:Second_Location,sto_ali:Part_Number),DUP,NOCASE
RequestedKey             KEY(sto_ali:QuantityRequested,sto_ali:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Description),DUP,NOCASE
DateBookedKey            KEY(sto_ali:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(sto_ali:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
ExchangeModelKey         KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:ExchangeModelNumber),DUP,NOCASE
LoanModelKey             KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:LoanModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
ExchangeModelNumber         STRING(30)
LoanUnit                    BYTE
LoanModelNumber             STRING(30)
                         END
                     END                       

JOBS_ALIAS           FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job_ali),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job_ali:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job_ali:Model_Number,job_ali:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job_ali:Engineer,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job_ali:Engineer,job_ali:Workshop,job_ali:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job_ali:Surname),DUP,NOCASE
MobileNumberKey          KEY(job_ali:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(job_ali:MSN),DUP,NOCASE
AccountNumberKey         KEY(job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job_ali:Account_Number,job_ali:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job_ali:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job_ali:Engineer,job_ali:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job_ali:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job_ali:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job_ali:Model_Number,job_ali:Date_Completed),DUP,NOCASE
By_Status                KEY(job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job_ali:Current_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
Location_Key             KEY(job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job_ali:Job_Priority,job_ali:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job_ali:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job_ali:Manufacturer,job_ali:EDI,job_ali:EDI_Batch_Number,job_ali:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job_ali:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job_ali:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job_ali:Batch_Number,job_ali:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job_ali:Batch_Number,job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job_ali:Batch_Number,job_ali:Model_Number,job_ali:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job_ali:Batch_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job_ali:Batch_Number,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job_ali:Chargeable_Job,job_ali:Account_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job_ali:Invoice_Exception,job_ali:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job_ali:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job_ali:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job_ali:Despatched,job_ali:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job_ali:Despatched,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job_ali:Despatch_Number,job_ali:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job_ali:Courier,job_ali:Date_Despatched,job_ali:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job_ali:Loan_Courier,job_ali:Loan_Despatched,job_ali:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job_ali:Exchange_Courier,job_ali:Exchange_Despatched,job_ali:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job_ali:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job_ali:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job_ali:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job_ali:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job_ali:Bouncer,job_ali:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job_ali:Engineer,job_ali:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job_ali:Exchange_Status,job_ali:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job_ali:Loan_Status,job_ali:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job_ali:Exchange_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job_ali:Loan_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:InvoiceStatus,job_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

STDCHRGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('STDCHRGE.DAT'),PRE(sta_ali),CREATE,BINDABLE,THREAD
Model_Number_Charge_Key  KEY(sta_ali:Model_Number,sta_ali:Charge_Type,sta_ali:Unit_Type,sta_ali:Repair_Type),NOCASE,PRIMARY
Charge_Type_Key          KEY(sta_ali:Model_Number,sta_ali:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(sta_ali:Model_Number,sta_ali:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(sta_ali:Model_Number,sta_ali:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(sta_ali:Model_Number,sta_ali:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(sta_ali:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(sta_ali:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(sta_ali:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
WarrantyClaimRate           REAL
HandlingFee                 REAL
Exchange                    REAL
RRCRate                     REAL
                         END
                     END                       


         include('MessageBox.inc'),once

ThisMessageBox       class(csStandardMessageClass) 
Init                     procedure  (long Reserved1=0,long Reserved2=0)  ,virtual
                      end
StartSearchEXEName          long(1)
EndSearchEXEName            long(0)
vod                 VodacomClass

Access:STDCHRGE      &FileManager,THREAD                   ! FileManager for STDCHRGE
Relate:STDCHRGE      &RelationManager,THREAD               ! RelationManager for STDCHRGE
Access:EXCHPREP      &FileManager,THREAD                   ! FileManager for EXCHPREP
Relate:EXCHPREP      &RelationManager,THREAD               ! RelationManager for EXCHPREP
Access:EVO_HEAD      &FileManager,THREAD                   ! FileManager for EVO_HEAD
Relate:EVO_HEAD      &RelationManager,THREAD               ! RelationManager for EVO_HEAD
Access:STOAUUSE      &FileManager,THREAD                   ! FileManager for STOAUUSE
Relate:STOAUUSE      &RelationManager,THREAD               ! RelationManager for STOAUUSE
Access:PREJOB        &FileManager,THREAD                   ! FileManager for PREJOB
Relate:PREJOB        &RelationManager,THREAD               ! RelationManager for PREJOB
Access:APITOKEN      &FileManager,THREAD                   ! FileManager for APITOKEN
Relate:APITOKEN      &RelationManager,THREAD               ! RelationManager for APITOKEN
Access:PRIBAND       &FileManager,THREAD                   ! FileManager for PRIBAND
Relate:PRIBAND       &RelationManager,THREAD               ! RelationManager for PRIBAND
Access:TRADEAC2      &FileManager,THREAD                   ! FileManager for TRADEAC2
Relate:TRADEAC2      &RelationManager,THREAD               ! RelationManager for TRADEAC2
Access:AUDIT2        &FileManager,THREAD                   ! FileManager for AUDIT2
Relate:AUDIT2        &RelationManager,THREAD               ! RelationManager for AUDIT2
Access:SMSRECVD      &FileManager,THREAD                   ! FileManager for SMSRECVD
Relate:SMSRECVD      &RelationManager,THREAD               ! RelationManager for SMSRECVD
Access:TRAHUBAC      &FileManager,THREAD                   ! FileManager for TRAHUBAC
Relate:TRAHUBAC      &RelationManager,THREAD               ! RelationManager for TRAHUBAC
Access:JOBSLOCK      &FileManager,THREAD                   ! FileManager for JOBSLOCK
Relate:JOBSLOCK      &RelationManager,THREAD               ! RelationManager for JOBSLOCK
Access:ASVACC        &FileManager,THREAD                   ! FileManager for ASVACC
Relate:ASVACC        &RelationManager,THREAD               ! RelationManager for ASVACC
Access:TagFile       &FileManager,THREAD                   ! FileManager for TagFile
Relate:TagFile       &RelationManager,THREAD               ! RelationManager for TagFile
Access:VETTREAS      &FileManager,THREAD                   ! FileManager for VETTREAS
Relate:VETTREAS      &RelationManager,THREAD               ! RelationManager for VETTREAS
Access:ErrorVCPJob   &FileManager,THREAD                   ! FileManager for ErrorVCPJob
Relate:ErrorVCPJob   &RelationManager,THREAD               ! RelationManager for ErrorVCPJob
Access:SRNTEXT       &FileManager,THREAD                   ! FileManager for SRNTEXT
Relate:SRNTEXT       &RelationManager,THREAD               ! RelationManager for SRNTEXT
Access:EXCEXCH       &FileManager,THREAD                   ! FileManager for EXCEXCH
Relate:EXCEXCH       &RelationManager,THREAD               ! RelationManager for EXCEXCH
Access:GRNOTES       &FileManager,THREAD                   ! FileManager for GRNOTES
Relate:GRNOTES       &RelationManager,THREAD               ! RelationManager for GRNOTES
Access:STOFAULT      &FileManager,THREAD                   ! FileManager for STOFAULT
Relate:STOFAULT      &RelationManager,THREAD               ! RelationManager for STOFAULT
Access:JOBOUTFL      &FileManager,THREAD                   ! FileManager for JOBOUTFL
Relate:JOBOUTFL      &RelationManager,THREAD               ! RelationManager for JOBOUTFL
Access:ACCSTAT       &FileManager,THREAD                   ! FileManager for ACCSTAT
Relate:ACCSTAT       &RelationManager,THREAD               ! RelationManager for ACCSTAT
Access:QAPARTSTEMP   &FileManager,THREAD                   ! FileManager for QAPARTSTEMP
Relate:QAPARTSTEMP   &RelationManager,THREAD               ! RelationManager for QAPARTSTEMP
Access:EXCHAMF       &FileManager,THREAD                   ! FileManager for EXCHAMF
Relate:EXCHAMF       &RelationManager,THREAD               ! RelationManager for EXCHAMF
Access:AUDITE        &FileManager,THREAD                   ! FileManager for AUDITE
Relate:AUDITE        &RelationManager,THREAD               ! RelationManager for AUDITE
Access:AUDVCPMS      &FileManager,THREAD                   ! FileManager for AUDVCPMS
Relate:AUDVCPMS      &RelationManager,THREAD               ! RelationManager for AUDVCPMS
Access:EXCHAUI       &FileManager,THREAD                   ! FileManager for EXCHAUI
Relate:EXCHAUI       &RelationManager,THREAD               ! RelationManager for EXCHAUI
Access:JOBSENG       &FileManager,THREAD                   ! FileManager for JOBSENG
Relate:JOBSENG       &RelationManager,THREAD               ! RelationManager for JOBSENG
Access:MULDESPJ      &FileManager,THREAD                   ! FileManager for MULDESPJ
Relate:MULDESPJ      &RelationManager,THREAD               ! RelationManager for MULDESPJ
Access:MULDESP       &FileManager,THREAD                   ! FileManager for MULDESP
Relate:MULDESP       &RelationManager,THREAD               ! RelationManager for MULDESP
Access:STOCKLOG      &FileManager,THREAD                   ! FileManager for STOCKLOG
Relate:STOCKLOG      &RelationManager,THREAD               ! RelationManager for STOCKLOG
Access:IMEILOG       &FileManager,THREAD                   ! FileManager for IMEILOG
Relate:IMEILOG       &RelationManager,THREAD               ! RelationManager for IMEILOG
Access:KEXPORT       &FileManager,THREAD                   ! FileManager for KEXPORT
Relate:KEXPORT       &RelationManager,THREAD               ! RelationManager for KEXPORT
Access:SWAPIMEI      &FileManager,THREAD                   ! FileManager for SWAPIMEI
Relate:SWAPIMEI      &RelationManager,THREAD               ! RelationManager for SWAPIMEI
Access:JOBBOUNCER    &FileManager,THREAD                   ! FileManager for JOBBOUNCER
Relate:JOBBOUNCER    &RelationManager,THREAD               ! RelationManager for JOBBOUNCER
Access:RTNAWAIT      &FileManager,THREAD                   ! FileManager for RTNAWAIT
Relate:RTNAWAIT      &RelationManager,THREAD               ! RelationManager for RTNAWAIT
Access:STMASAUD      &FileManager,THREAD                   ! FileManager for STMASAUD
Relate:STMASAUD      &RelationManager,THREAD               ! RelationManager for STMASAUD
Access:AUDSTATS      &FileManager,THREAD                   ! FileManager for AUDSTATS
Relate:AUDSTATS      &RelationManager,THREAD               ! RelationManager for AUDSTATS
Access:KIMPORT       &FileManager,THREAD                   ! FileManager for KIMPORT
Relate:KIMPORT       &RelationManager,THREAD               ! RelationManager for KIMPORT
Access:AUDVCPLN      &FileManager,THREAD                   ! FileManager for AUDVCPLN
Relate:AUDVCPLN      &RelationManager,THREAD               ! RelationManager for AUDVCPLN
Access:MANSAMP       &FileManager,THREAD                   ! FileManager for MANSAMP
Relate:MANSAMP       &RelationManager,THREAD               ! RelationManager for MANSAMP
Access:REPMAILP      &FileManager,THREAD                   ! FileManager for REPMAILP
Relate:REPMAILP      &RelationManager,THREAD               ! RelationManager for REPMAILP
Access:RAPIDLST      &FileManager,THREAD                   ! FileManager for RAPIDLST
Relate:RAPIDLST      &RelationManager,THREAD               ! RelationManager for RAPIDLST
Access:NETWORKS      &FileManager,THREAD                   ! FileManager for NETWORKS
Relate:NETWORKS      &RelationManager,THREAD               ! RelationManager for NETWORKS
Access:REPSCHED      &FileManager,THREAD                   ! FileManager for REPSCHED
Relate:REPSCHED      &RelationManager,THREAD               ! RelationManager for REPSCHED
Access:ORDTEMP       &FileManager,THREAD                   ! FileManager for ORDTEMP
Relate:ORDTEMP       &RelationManager,THREAD               ! RelationManager for ORDTEMP
Access:NMSPRE        &FileManager,THREAD                   ! FileManager for NMSPRE
Relate:NMSPRE        &RelationManager,THREAD               ! RelationManager for NMSPRE
Access:ORDITEMS      &FileManager,THREAD                   ! FileManager for ORDITEMS
Relate:ORDITEMS      &RelationManager,THREAD               ! RelationManager for ORDITEMS
Access:KSTAGES       &FileManager,THREAD                   ! FileManager for KSTAGES
Relate:KSTAGES       &RelationManager,THREAD               ! RelationManager for KSTAGES
Access:ORDHEAD       &FileManager,THREAD                   ! FileManager for ORDHEAD
Relate:ORDHEAD       &RelationManager,THREAD               ! RelationManager for ORDHEAD
Access:KLOCS         &FileManager,THREAD                   ! FileManager for KLOCS
Relate:KLOCS         &RelationManager,THREAD               ! RelationManager for KLOCS
Access:IMEISHIP      &FileManager,THREAD                   ! FileManager for IMEISHIP
Relate:IMEISHIP      &RelationManager,THREAD               ! RelationManager for IMEISHIP
Access:JOBTHIRD      &FileManager,THREAD                   ! FileManager for JOBTHIRD
Relate:JOBTHIRD      &RelationManager,THREAD               ! RelationManager for JOBTHIRD
Access:DEFAULTV      &FileManager,THREAD                   ! FileManager for DEFAULTV
Relate:DEFAULTV      &RelationManager,THREAD               ! RelationManager for DEFAULTV
Access:LOGRETRN      &FileManager,THREAD                   ! FileManager for LOGRETRN
Relate:LOGRETRN      &RelationManager,THREAD               ! RelationManager for LOGRETRN
Access:LOCATLOG      &FileManager,THREAD                   ! FileManager for LOCATLOG
Relate:LOCATLOG      &RelationManager,THREAD               ! RelationManager for LOCATLOG
Access:PRODCODE      &FileManager,THREAD                   ! FileManager for PRODCODE
Relate:PRODCODE      &RelationManager,THREAD               ! RelationManager for PRODCODE
Access:EVO_TAX       &FileManager,THREAD                   ! FileManager for EVO_TAX
Relate:EVO_TAX       &RelationManager,THREAD               ! RelationManager for EVO_TAX
Access:EVO_AP        &FileManager,THREAD                   ! FileManager for EVO_AP
Relate:EVO_AP        &RelationManager,THREAD               ! RelationManager for EVO_AP
Access:EVO_GL        &FileManager,THREAD                   ! FileManager for EVO_GL
Relate:EVO_GL        &RelationManager,THREAD               ! RelationManager for EVO_GL
Access:RAPENGLS      &FileManager,THREAD                   ! FileManager for RAPENGLS
Relate:RAPENGLS      &RelationManager,THREAD               ! RelationManager for RAPENGLS
Access:LOG2TEMP      &FileManager,THREAD                   ! FileManager for LOG2TEMP
Relate:LOG2TEMP      &RelationManager,THREAD               ! RelationManager for LOG2TEMP
Access:LOGSTOCK      &FileManager,THREAD                   ! FileManager for LOGSTOCK
Relate:LOGSTOCK      &RelationManager,THREAD               ! RelationManager for LOGSTOCK
Access:ESNMODAL      &FileManager,THREAD                   ! FileManager for ESNMODAL
Relate:ESNMODAL      &RelationManager,THREAD               ! RelationManager for ESNMODAL
Access:LABLGTMP      &FileManager,THREAD                   ! FileManager for LABLGTMP
Relate:LABLGTMP      &RelationManager,THREAD               ! RelationManager for LABLGTMP
Access:LETTERS       &FileManager,THREAD                   ! FileManager for LETTERS
Relate:LETTERS       &RelationManager,THREAD               ! RelationManager for LETTERS
Access:EXPGEN        &FileManager,THREAD                   ! FileManager for EXPGEN
Relate:EXPGEN        &RelationManager,THREAD               ! RelationManager for EXPGEN
Access:STOAUDIT      &FileManager,THREAD                   ! FileManager for STOAUDIT
Relate:STOAUDIT      &RelationManager,THREAD               ! RelationManager for STOAUDIT
Access:DEFAULT2      &FileManager,THREAD                   ! FileManager for DEFAULT2
Relate:DEFAULT2      &RelationManager,THREAD               ! RelationManager for DEFAULT2
Access:ACTION        &FileManager,THREAD                   ! FileManager for ACTION
Relate:ACTION        &RelationManager,THREAD               ! RelationManager for ACTION
Access:DEFRAPID      &FileManager,THREAD                   ! FileManager for DEFRAPID
Relate:DEFRAPID      &RelationManager,THREAD               ! RelationManager for DEFRAPID
Access:RETSALES      &FileManager,THREAD                   ! FileManager for RETSALES
Relate:RETSALES      &RelationManager,THREAD               ! RelationManager for RETSALES
Access:LOANALC       &FileManager,THREAD                   ! FileManager for LOANALC
Relate:LOANALC       &RelationManager,THREAD               ! RelationManager for LOANALC
Access:WIPALC        &FileManager,THREAD                   ! FileManager for WIPALC
Relate:WIPALC        &RelationManager,THREAD               ! RelationManager for WIPALC
Access:LOANAMF       &FileManager,THREAD                   ! FileManager for LOANAMF
Relate:LOANAMF       &RelationManager,THREAD               ! RelationManager for LOANAMF
Access:BOUNCER       &FileManager,THREAD                   ! FileManager for BOUNCER
Relate:BOUNCER       &RelationManager,THREAD               ! RelationManager for BOUNCER
Access:EXCHORNO      &FileManager,THREAD                   ! FileManager for EXCHORNO
Relate:EXCHORNO      &RelationManager,THREAD               ! RelationManager for EXCHORNO
Access:LOANORNO      &FileManager,THREAD                   ! FileManager for LOANORNO
Relate:LOANORNO      &RelationManager,THREAD               ! RelationManager for LOANORNO
Access:EXCHOR48      &FileManager,THREAD                   ! FileManager for EXCHOR48
Relate:EXCHOR48      &RelationManager,THREAD               ! RelationManager for EXCHOR48
Access:LOANAUI       &FileManager,THREAD                   ! FileManager for LOANAUI
Relate:LOANAUI       &RelationManager,THREAD               ! RelationManager for LOANAUI
Access:LOAORDR       &FileManager,THREAD                   ! FileManager for LOAORDR
Relate:LOAORDR       &RelationManager,THREAD               ! RelationManager for LOAORDR
Access:WIPAMF        &FileManager,THREAD                   ! FileManager for WIPAMF
Relate:WIPAMF        &RelationManager,THREAD               ! RelationManager for WIPAMF
Access:TEAMS         &FileManager,THREAD                   ! FileManager for TEAMS
Relate:TEAMS         &RelationManager,THREAD               ! RelationManager for TEAMS
Access:EXCHALC       &FileManager,THREAD                   ! FileManager for EXCHALC
Relate:EXCHALC       &RelationManager,THREAD               ! RelationManager for EXCHALC
Access:WIPAUI        &FileManager,THREAD                   ! FileManager for WIPAUI
Relate:WIPAUI        &RelationManager,THREAD               ! RelationManager for WIPAUI
Access:MERGE         &FileManager,THREAD                   ! FileManager for MERGE
Relate:MERGE         &RelationManager,THREAD               ! RelationManager for MERGE
Access:EXCHORDR      &FileManager,THREAD                   ! FileManager for EXCHORDR
Relate:EXCHORDR      &RelationManager,THREAD               ! RelationManager for EXCHORDR
Access:UPDDATA       &FileManager,THREAD                   ! FileManager for UPDDATA
Relate:UPDDATA       &RelationManager,THREAD               ! RelationManager for UPDDATA
Access:EXPSPARES     &FileManager,THREAD                   ! FileManager for EXPSPARES
Relate:EXPSPARES     &RelationManager,THREAD               ! RelationManager for EXPSPARES
Access:WEBDEFLT      &FileManager,THREAD                   ! FileManager for WEBDEFLT
Relate:WEBDEFLT      &RelationManager,THREAD               ! RelationManager for WEBDEFLT
Access:EXCAUDIT      &FileManager,THREAD                   ! FileManager for EXCAUDIT
Relate:EXCAUDIT      &RelationManager,THREAD               ! RelationManager for EXCAUDIT
Access:GRNOTESP      &FileManager,THREAD                   ! FileManager for GRNOTESP
Relate:GRNOTESP      &RelationManager,THREAD               ! RelationManager for GRNOTESP
Access:COMMONFA      &FileManager,THREAD                   ! FileManager for COMMONFA
Relate:COMMONFA      &RelationManager,THREAD               ! RelationManager for COMMONFA
Access:PARAMSS       &FileManager,THREAD                   ! FileManager for PARAMSS
Relate:PARAMSS       &RelationManager,THREAD               ! RelationManager for PARAMSS
Access:GRNOTESR      &FileManager,THREAD                   ! FileManager for GRNOTESR
Relate:GRNOTESR      &RelationManager,THREAD               ! RelationManager for GRNOTESR
Access:CONSIGN       &FileManager,THREAD                   ! FileManager for CONSIGN
Relate:CONSIGN       &RelationManager,THREAD               ! RelationManager for CONSIGN
Access:HANDOJOB      &FileManager,THREAD                   ! FileManager for HANDOJOB
Relate:HANDOJOB      &RelationManager,THREAD               ! RelationManager for HANDOJOB
Access:JOBEXACC      &FileManager,THREAD                   ! FileManager for JOBEXACC
Relate:JOBEXACC      &RelationManager,THREAD               ! RelationManager for JOBEXACC
Access:MODELCOL      &FileManager,THREAD                   ! FileManager for MODELCOL
Relate:MODELCOL      &RelationManager,THREAD               ! RelationManager for MODELCOL
Access:COMMCAT       &FileManager,THREAD                   ! FileManager for COMMCAT
Relate:COMMCAT       &RelationManager,THREAD               ! RelationManager for COMMCAT
Access:MESSAGES      &FileManager,THREAD                   ! FileManager for MESSAGES
Relate:MESSAGES      &RelationManager,THREAD               ! RelationManager for MESSAGES
Access:LOGEXHE       &FileManager,THREAD                   ! FileManager for LOGEXHE
Relate:LOGEXHE       &RelationManager,THREAD               ! RelationManager for LOGEXHE
Access:DEFCRC        &FileManager,THREAD                   ! FileManager for DEFCRC
Relate:DEFCRC        &RelationManager,THREAD               ! RelationManager for DEFCRC
Access:PAYTYPES      &FileManager,THREAD                   ! FileManager for PAYTYPES
Relate:PAYTYPES      &RelationManager,THREAD               ! RelationManager for PAYTYPES
Access:COMMONWP      &FileManager,THREAD                   ! FileManager for COMMONWP
Relate:COMMONWP      &RelationManager,THREAD               ! RelationManager for COMMONWP
Access:LOGEXCH       &FileManager,THREAD                   ! FileManager for LOGEXCH
Relate:LOGEXCH       &RelationManager,THREAD               ! RelationManager for LOGEXCH
Access:QAREASON      &FileManager,THREAD                   ! FileManager for QAREASON
Relate:QAREASON      &RelationManager,THREAD               ! RelationManager for QAREASON
Access:COMMONCP      &FileManager,THREAD                   ! FileManager for COMMONCP
Relate:COMMONCP      &RelationManager,THREAD               ! RelationManager for COMMONCP
Access:ADDSEARCH     &FileManager,THREAD                   ! FileManager for ADDSEARCH
Relate:ADDSEARCH     &RelationManager,THREAD               ! RelationManager for ADDSEARCH
Access:JOBSSL        &FileManager,THREAD                   ! FileManager for JOBSSL
Relate:JOBSSL        &RelationManager,THREAD               ! RelationManager for JOBSSL
Access:CONTHIST      &FileManager,THREAD                   ! FileManager for CONTHIST
Relate:CONTHIST      &RelationManager,THREAD               ! RelationManager for CONTHIST
Access:CONTACTS      &FileManager,THREAD                   ! FileManager for CONTACTS
Relate:CONTACTS      &RelationManager,THREAD               ! RelationManager for CONTACTS
Access:EXPJOBS       &FileManager,THREAD                   ! FileManager for EXPJOBS
Relate:EXPJOBS       &RelationManager,THREAD               ! RelationManager for EXPJOBS
Access:EXPAUDIT      &FileManager,THREAD                   ! FileManager for EXPAUDIT
Relate:EXPAUDIT      &RelationManager,THREAD               ! RelationManager for EXPAUDIT
Access:JOBBATCH      &FileManager,THREAD                   ! FileManager for JOBBATCH
Relate:JOBBATCH      &RelationManager,THREAD               ! RelationManager for JOBBATCH
Access:DEFEDI2       &FileManager,THREAD                   ! FileManager for DEFEDI2
Relate:DEFEDI2       &RelationManager,THREAD               ! RelationManager for DEFEDI2
Access:DEFPRINT      &FileManager,THREAD                   ! FileManager for DEFPRINT
Relate:DEFPRINT      &RelationManager,THREAD               ! RelationManager for DEFPRINT
Access:DEFWEB        &FileManager,THREAD                   ! FileManager for DEFWEB
Relate:DEFWEB        &RelationManager,THREAD               ! RelationManager for DEFWEB
Access:EXPCITY       &FileManager,THREAD                   ! FileManager for EXPCITY
Relate:EXPCITY       &RelationManager,THREAD               ! RelationManager for EXPCITY
Access:PROCCODE      &FileManager,THREAD                   ! FileManager for PROCCODE
Relate:PROCCODE      &RelationManager,THREAD               ! RelationManager for PROCCODE
Access:COLOUR        &FileManager,THREAD                   ! FileManager for COLOUR
Relate:COLOUR        &RelationManager,THREAD               ! RelationManager for COLOUR
Access:VODAIMP       &FileManager,THREAD                   ! FileManager for VODAIMP
Relate:VODAIMP       &RelationManager,THREAD               ! RelationManager for VODAIMP
Access:JOBSTAMP      &FileManager,THREAD                   ! FileManager for JOBSTAMP
Relate:JOBSTAMP      &RelationManager,THREAD               ! RelationManager for JOBSTAMP
Access:JOBSVODA      &FileManager,THREAD                   ! FileManager for JOBSVODA
Relate:JOBSVODA      &RelationManager,THREAD               ! RelationManager for JOBSVODA
Access:XREPACT       &FileManager,THREAD                   ! FileManager for XREPACT
Relate:XREPACT       &RelationManager,THREAD               ! RelationManager for XREPACT
Access:ACCESDEF      &FileManager,THREAD                   ! FileManager for ACCESDEF
Relate:ACCESDEF      &RelationManager,THREAD               ! RelationManager for ACCESDEF
Access:STANTEXT      &FileManager,THREAD                   ! FileManager for STANTEXT
Relate:STANTEXT      &RelationManager,THREAD               ! RelationManager for STANTEXT
Access:NOTESENG      &FileManager,THREAD                   ! FileManager for NOTESENG
Relate:NOTESENG      &RelationManager,THREAD               ! RelationManager for NOTESENG
Access:JOBACCNO      &FileManager,THREAD                   ! FileManager for JOBACCNO
Relate:JOBACCNO      &RelationManager,THREAD               ! RelationManager for JOBACCNO
Access:EXPLABG       &FileManager,THREAD                   ! FileManager for EXPLABG
Relate:EXPLABG       &RelationManager,THREAD               ! RelationManager for EXPLABG
Access:JOBRPNOT      &FileManager,THREAD                   ! FileManager for JOBRPNOT
Relate:JOBRPNOT      &RelationManager,THREAD               ! RelationManager for JOBRPNOT
Access:JOBSOBF       &FileManager,THREAD                   ! FileManager for JOBSOBF
Relate:JOBSOBF       &RelationManager,THREAD               ! RelationManager for JOBSOBF
Access:JOBSINV       &FileManager,THREAD                   ! FileManager for JOBSINV
Relate:JOBSINV       &RelationManager,THREAD               ! RelationManager for JOBSINV
Access:JOBSCONS      &FileManager,THREAD                   ! FileManager for JOBSCONS
Relate:JOBSCONS      &RelationManager,THREAD               ! RelationManager for JOBSCONS
Access:JOBSWARR      &FileManager,THREAD                   ! FileManager for JOBSWARR
Relate:JOBSWARR      &RelationManager,THREAD               ! RelationManager for JOBSWARR
Access:CURRENCY      &FileManager,THREAD                   ! FileManager for CURRENCY
Relate:CURRENCY      &RelationManager,THREAD               ! RelationManager for CURRENCY
Access:STOCKRECEIVETMP &FileManager,THREAD                 ! FileManager for STOCKRECEIVETMP
Relate:STOCKRECEIVETMP &RelationManager,THREAD             ! RelationManager for STOCKRECEIVETMP
Access:JOBSE2        &FileManager,THREAD                   ! FileManager for JOBSE2
Relate:JOBSE2        &RelationManager,THREAD               ! RelationManager for JOBSE2
Access:TRDSPEC       &FileManager,THREAD                   ! FileManager for TRDSPEC
Relate:TRDSPEC       &RelationManager,THREAD               ! RelationManager for TRDSPEC
Access:EPSCSV        &FileManager,THREAD                   ! FileManager for EPSCSV
Relate:EPSCSV        &RelationManager,THREAD               ! RelationManager for EPSCSV
Access:EXPGENDM      &FileManager,THREAD                   ! FileManager for EXPGENDM
Relate:EXPGENDM      &RelationManager,THREAD               ! RelationManager for EXPGENDM
Access:EDIBATCH      &FileManager,THREAD                   ! FileManager for EDIBATCH
Relate:EDIBATCH      &RelationManager,THREAD               ! RelationManager for EDIBATCH
Access:ORDPEND       &FileManager,THREAD                   ! FileManager for ORDPEND
Relate:ORDPEND       &RelationManager,THREAD               ! RelationManager for ORDPEND
Access:STOHIST       &FileManager,THREAD                   ! FileManager for STOHIST
Relate:STOHIST       &RelationManager,THREAD               ! RelationManager for STOHIST
Access:RETTYPES      &FileManager,THREAD                   ! FileManager for RETTYPES
Relate:RETTYPES      &RelationManager,THREAD               ! RelationManager for RETTYPES
Access:NEWFEAT       &FileManager,THREAD                   ! FileManager for NEWFEAT
Relate:NEWFEAT       &RelationManager,THREAD               ! RelationManager for NEWFEAT
Access:REPTYDEF      &FileManager,THREAD                   ! FileManager for REPTYDEF
Relate:REPTYDEF      &RelationManager,THREAD               ! RelationManager for REPTYDEF
Access:EPSIMP        &FileManager,THREAD                   ! FileManager for EPSIMP
Relate:EPSIMP        &RelationManager,THREAD               ! RelationManager for EPSIMP
Access:RETACCOUNTSLIST &FileManager,THREAD                 ! FileManager for RETACCOUNTSLIST
Relate:RETACCOUNTSLIST &RelationManager,THREAD             ! RelationManager for RETACCOUNTSLIST
Access:PRIORITY      &FileManager,THREAD                   ! FileManager for PRIORITY
Relate:PRIORITY      &RelationManager,THREAD               ! RelationManager for PRIORITY
Access:DEFEPS        &FileManager,THREAD                   ! FileManager for DEFEPS
Relate:DEFEPS        &RelationManager,THREAD               ! RelationManager for DEFEPS
Access:JOBSE         &FileManager,THREAD                   ! FileManager for JOBSE
Relate:JOBSE         &RelationManager,THREAD               ! RelationManager for JOBSE
Access:REPEXTTP      &FileManager,THREAD                   ! FileManager for REPEXTTP
Relate:REPEXTTP      &RelationManager,THREAD               ! RelationManager for REPEXTTP
Access:MANFAULT      &FileManager,THREAD                   ! FileManager for MANFAULT
Relate:MANFAULT      &RelationManager,THREAD               ! RelationManager for MANFAULT
Access:INVPARTS      &FileManager,THREAD                   ! FileManager for INVPARTS
Relate:INVPARTS      &RelationManager,THREAD               ! RelationManager for INVPARTS
Access:JOBSEARC      &FileManager,THREAD                   ! FileManager for JOBSEARC
Relate:JOBSEARC      &RelationManager,THREAD               ! RelationManager for JOBSEARC
Access:POPTYPES      &FileManager,THREAD                   ! FileManager for POPTYPES
Relate:POPTYPES      &RelationManager,THREAD               ! RelationManager for POPTYPES
Access:INVPATMP      &FileManager,THREAD                   ! FileManager for INVPATMP
Relate:INVPATMP      &RelationManager,THREAD               ! RelationManager for INVPATMP
Access:TRACHAR       &FileManager,THREAD                   ! FileManager for TRACHAR
Relate:TRACHAR       &RelationManager,THREAD               ! RelationManager for TRACHAR
Access:CITYSERV      &FileManager,THREAD                   ! FileManager for CITYSERV
Relate:CITYSERV      &RelationManager,THREAD               ! RelationManager for CITYSERV
Access:DISCOUNT      &FileManager,THREAD                   ! FileManager for DISCOUNT
Relate:DISCOUNT      &RelationManager,THREAD               ! RelationManager for DISCOUNT
Access:LOCVALUE      &FileManager,THREAD                   ! FileManager for LOCVALUE
Relate:LOCVALUE      &RelationManager,THREAD               ! RelationManager for LOCVALUE
Access:STOCKTYP      &FileManager,THREAD                   ! FileManager for STOCKTYP
Relate:STOCKTYP      &RelationManager,THREAD               ! RelationManager for STOCKTYP
Access:COURIER       &FileManager,THREAD                   ! FileManager for COURIER
Relate:COURIER       &RelationManager,THREAD               ! RelationManager for COURIER
Access:TRDPARTY      &FileManager,THREAD                   ! FileManager for TRDPARTY
Relate:TRDPARTY      &RelationManager,THREAD               ! RelationManager for TRDPARTY
Access:JOBSTMP       &FileManager,THREAD                   ! FileManager for JOBSTMP
Relate:JOBSTMP       &RelationManager,THREAD               ! RelationManager for JOBSTMP
Access:HANDOVER      &FileManager,THREAD                   ! FileManager for HANDOVER
Relate:HANDOVER      &RelationManager,THREAD               ! RelationManager for HANDOVER
Access:JOBNOTES      &FileManager,THREAD                   ! FileManager for JOBNOTES
Relate:JOBNOTES      &RelationManager,THREAD               ! RelationManager for JOBNOTES
Access:PROINV        &FileManager,THREAD                   ! FileManager for PROINV
Relate:PROINV        &RelationManager,THREAD               ! RelationManager for PROINV
Access:EXPBUS        &FileManager,THREAD                   ! FileManager for EXPBUS
Relate:EXPBUS        &RelationManager,THREAD               ! RelationManager for EXPBUS
Access:IMPCITY       &FileManager,THREAD                   ! FileManager for IMPCITY
Relate:IMPCITY       &RelationManager,THREAD               ! RelationManager for IMPCITY
Access:JOBACTMP      &FileManager,THREAD                   ! FileManager for JOBACTMP
Relate:JOBACTMP      &RelationManager,THREAD               ! RelationManager for JOBACTMP
Access:RETPARTSLIST  &FileManager,THREAD                   ! FileManager for RETPARTSLIST
Relate:RETPARTSLIST  &RelationManager,THREAD               ! RelationManager for RETPARTSLIST
Access:TRANTYPE      &FileManager,THREAD                   ! FileManager for TRANTYPE
Relate:TRANTYPE      &RelationManager,THREAD               ! RelationManager for TRANTYPE
Access:MANFAURL      &FileManager,THREAD                   ! FileManager for MANFAURL
Relate:MANFAURL      &RelationManager,THREAD               ! RelationManager for MANFAURL
Access:DESBATCH      &FileManager,THREAD                   ! FileManager for DESBATCH
Relate:DESBATCH      &RelationManager,THREAD               ! RelationManager for DESBATCH
Access:MODELCCT      &FileManager,THREAD                   ! FileManager for MODELCCT
Relate:MODELCCT      &RelationManager,THREAD               ! RelationManager for MODELCCT
Access:MANFAUEX      &FileManager,THREAD                   ! FileManager for MANFAUEX
Relate:MANFAUEX      &RelationManager,THREAD               ! RelationManager for MANFAUEX
Access:MANFPARL      &FileManager,THREAD                   ! FileManager for MANFPARL
Relate:MANFPARL      &RelationManager,THREAD               ! RelationManager for MANFPARL
Access:JOBVODAC      &FileManager,THREAD                   ! FileManager for JOBVODAC
Relate:JOBVODAC      &RelationManager,THREAD               ! RelationManager for JOBVODAC
Access:ESNMODEL      &FileManager,THREAD                   ! FileManager for ESNMODEL
Relate:ESNMODEL      &RelationManager,THREAD               ! RelationManager for ESNMODEL
Access:JOBPAYMT      &FileManager,THREAD                   ! FileManager for JOBPAYMT
Relate:JOBPAYMT      &RelationManager,THREAD               ! RelationManager for JOBPAYMT
Access:MANREJR       &FileManager,THREAD                   ! FileManager for MANREJR
Relate:MANREJR       &RelationManager,THREAD               ! RelationManager for MANREJR
Access:LOGASSSTTEMP  &FileManager,THREAD                   ! FileManager for LOGASSSTTEMP
Relate:LOGASSSTTEMP  &RelationManager,THREAD               ! RelationManager for LOGASSSTTEMP
Access:STOCK         &FileManager,THREAD                   ! FileManager for STOCK
Relate:STOCK         &RelationManager,THREAD               ! RelationManager for STOCK
Access:LOGGED        &FileManager,THREAD                   ! FileManager for LOGGED
Relate:LOGGED        &RelationManager,THREAD               ! RelationManager for LOGGED
Access:MODEXCHA      &FileManager,THREAD                   ! FileManager for MODEXCHA
Relate:MODEXCHA      &RelationManager,THREAD               ! RelationManager for MODEXCHA
Access:LOGRTHIS      &FileManager,THREAD                   ! FileManager for LOGRTHIS
Relate:LOGRTHIS      &RelationManager,THREAD               ! RelationManager for LOGRTHIS
Access:ORDWEBPR      &FileManager,THREAD                   ! FileManager for ORDWEBPR
Relate:ORDWEBPR      &RelationManager,THREAD               ! RelationManager for ORDWEBPR
Access:ESREJRES      &FileManager,THREAD                   ! FileManager for ESREJRES
Relate:ESREJRES      &RelationManager,THREAD               ! RelationManager for ESREJRES
Access:MANMARK       &FileManager,THREAD                   ! FileManager for MANMARK
Relate:MANMARK       &RelationManager,THREAD               ! RelationManager for MANMARK
Access:REPSCHMP      &FileManager,THREAD                   ! FileManager for REPSCHMP
Relate:REPSCHMP      &RelationManager,THREAD               ! RelationManager for REPSCHMP
Access:LOGSTHIS      &FileManager,THREAD                   ! FileManager for LOGSTHIS
Relate:LOGSTHIS      &RelationManager,THREAD               ! RelationManager for LOGSTHIS
Access:MODELPART     &FileManager,THREAD                   ! FileManager for MODELPART
Relate:MODELPART     &RelationManager,THREAD               ! RelationManager for MODELPART
Access:LOGASSST      &FileManager,THREAD                   ! FileManager for LOGASSST
Relate:LOGASSST      &RelationManager,THREAD               ! RelationManager for LOGASSST
Access:MODPROD       &FileManager,THREAD                   ! FileManager for MODPROD
Relate:MODPROD       &RelationManager,THREAD               ! RelationManager for MODPROD
Access:LOGSTOLC      &FileManager,THREAD                   ! FileManager for LOGSTOLC
Relate:LOGSTOLC      &RelationManager,THREAD               ! RelationManager for LOGSTOLC
Access:LOGTEMP       &FileManager,THREAD                   ! FileManager for LOGTEMP
Relate:LOGTEMP       &RelationManager,THREAD               ! RelationManager for LOGTEMP
Access:MANUDATE      &FileManager,THREAD                   ! FileManager for MANUDATE
Relate:MANUDATE      &RelationManager,THREAD               ! RelationManager for MANUDATE
Access:LOGSERST      &FileManager,THREAD                   ! FileManager for LOGSERST
Relate:LOGSERST      &RelationManager,THREAD               ! RelationManager for LOGSERST
Access:LOGDEFLT      &FileManager,THREAD                   ! FileManager for LOGDEFLT
Relate:LOGDEFLT      &RelationManager,THREAD               ! RelationManager for LOGDEFLT
Access:LOGSTLOC      &FileManager,THREAD                   ! FileManager for LOGSTLOC
Relate:LOGSTLOC      &RelationManager,THREAD               ! RelationManager for LOGSTLOC
Access:MULTIDEF      &FileManager,THREAD                   ! FileManager for MULTIDEF
Relate:MULTIDEF      &RelationManager,THREAD               ! RelationManager for MULTIDEF
Access:TRAFAULO      &FileManager,THREAD                   ! FileManager for TRAFAULO
Relate:TRAFAULO      &RelationManager,THREAD               ! RelationManager for TRAFAULO
Access:WEBJOBNO      &FileManager,THREAD                   ! FileManager for WEBJOBNO
Relate:WEBJOBNO      &RelationManager,THREAD               ! RelationManager for WEBJOBNO
Access:TRAFAULT      &FileManager,THREAD                   ! FileManager for TRAFAULT
Relate:TRAFAULT      &RelationManager,THREAD               ! RelationManager for TRAFAULT
Access:OBFBROWSE     &FileManager,THREAD                   ! FileManager for OBFBROWSE
Relate:OBFBROWSE     &RelationManager,THREAD               ! RelationManager for OBFBROWSE
Access:EXMINLEV      &FileManager,THREAD                   ! FileManager for EXMINLEV
Relate:EXMINLEV      &RelationManager,THREAD               ! RelationManager for EXMINLEV
Access:ORACLECN      &FileManager,THREAD                   ! FileManager for ORACLECN
Relate:ORACLECN      &RelationManager,THREAD               ! RelationManager for ORACLECN
Access:PENDAUD       &FileManager,THREAD                   ! FileManager for PENDAUD
Relate:PENDAUD       &RelationManager,THREAD               ! RelationManager for PENDAUD
Access:ORACSPEX      &FileManager,THREAD                   ! FileManager for ORACSPEX
Relate:ORACSPEX      &RelationManager,THREAD               ! RelationManager for ORACSPEX
Access:LOGSTHII      &FileManager,THREAD                   ! FileManager for LOGSTHII
Relate:LOGSTHII      &RelationManager,THREAD               ! RelationManager for LOGSTHII
Access:LOGCLSTE      &FileManager,THREAD                   ! FileManager for LOGCLSTE
Relate:LOGCLSTE      &RelationManager,THREAD               ! RelationManager for LOGCLSTE
Access:RESUBMIT      &FileManager,THREAD                   ! FileManager for RESUBMIT
Relate:RESUBMIT      &RelationManager,THREAD               ! RelationManager for RESUBMIT
Access:OBFREASN      &FileManager,THREAD                   ! FileManager for OBFREASN
Relate:OBFREASN      &RelationManager,THREAD               ! RelationManager for OBFREASN
Access:STATCRIT      &FileManager,THREAD                   ! FileManager for STATCRIT
Relate:STATCRIT      &RelationManager,THREAD               ! RelationManager for STATCRIT
Access:LOGDESNO      &FileManager,THREAD                   ! FileManager for LOGDESNO
Relate:LOGDESNO      &RelationManager,THREAD               ! RelationManager for LOGDESNO
Access:CPNDPRTS      &FileManager,THREAD                   ! FileManager for CPNDPRTS
Relate:CPNDPRTS      &RelationManager,THREAD               ! RelationManager for CPNDPRTS
Access:LOGSALCD      &FileManager,THREAD                   ! FileManager for LOGSALCD
Relate:LOGSALCD      &RelationManager,THREAD               ! RelationManager for LOGSALCD
Access:DEFSTOCK      &FileManager,THREAD                   ! FileManager for DEFSTOCK
Relate:DEFSTOCK      &RelationManager,THREAD               ! RelationManager for DEFSTOCK
Access:JOBLOHIS      &FileManager,THREAD                   ! FileManager for JOBLOHIS
Relate:JOBLOHIS      &RelationManager,THREAD               ! RelationManager for JOBLOHIS
Access:ORACLEEX      &FileManager,THREAD                   ! FileManager for ORACLEEX
Relate:ORACLEEX      &RelationManager,THREAD               ! RelationManager for ORACLEEX
Access:LOAN          &FileManager,THREAD                   ! FileManager for LOAN
Relate:LOAN          &RelationManager,THREAD               ! RelationManager for LOAN
Access:NOTESCON      &FileManager,THREAD                   ! FileManager for NOTESCON
Relate:NOTESCON      &RelationManager,THREAD               ! RelationManager for NOTESCON
Access:VATCODE       &FileManager,THREAD                   ! FileManager for VATCODE
Relate:VATCODE       &RelationManager,THREAD               ! RelationManager for VATCODE
Access:SMSMAIL       &FileManager,THREAD                   ! FileManager for SMSMAIL
Relate:SMSMAIL       &RelationManager,THREAD               ! RelationManager for SMSMAIL
Access:LOANHIST      &FileManager,THREAD                   ! FileManager for LOANHIST
Relate:LOANHIST      &RelationManager,THREAD               ! RelationManager for LOANHIST
Access:TURNARND      &FileManager,THREAD                   ! FileManager for TURNARND
Relate:TURNARND      &RelationManager,THREAD               ! RelationManager for TURNARND
Access:ORDJOBS       &FileManager,THREAD                   ! FileManager for ORDJOBS
Relate:ORDJOBS       &RelationManager,THREAD               ! RelationManager for ORDJOBS
Access:MERGETXT      &FileManager,THREAD                   ! FileManager for MERGETXT
Relate:MERGETXT      &RelationManager,THREAD               ! RelationManager for MERGETXT
Access:MERGELET      &FileManager,THREAD                   ! FileManager for MERGELET
Relate:MERGELET      &RelationManager,THREAD               ! RelationManager for MERGELET
Access:ORDSTOCK      &FileManager,THREAD                   ! FileManager for ORDSTOCK
Relate:ORDSTOCK      &RelationManager,THREAD               ! RelationManager for ORDSTOCK
Access:EXCHHIST      &FileManager,THREAD                   ! FileManager for EXCHHIST
Relate:EXCHHIST      &RelationManager,THREAD               ! RelationManager for EXCHHIST
Access:ACCESSOR      &FileManager,THREAD                   ! FileManager for ACCESSOR
Relate:ACCESSOR      &RelationManager,THREAD               ! RelationManager for ACCESSOR
Access:SUBACCAD      &FileManager,THREAD                   ! FileManager for SUBACCAD
Relate:SUBACCAD      &RelationManager,THREAD               ! RelationManager for SUBACCAD
Access:WARPARTS      &FileManager,THREAD                   ! FileManager for WARPARTS
Relate:WARPARTS      &RelationManager,THREAD               ! RelationManager for WARPARTS
Access:PARTS         &FileManager,THREAD                   ! FileManager for PARTS
Relate:PARTS         &RelationManager,THREAD               ! RelationManager for PARTS
Access:JOBACC        &FileManager,THREAD                   ! FileManager for JOBACC
Relate:JOBACC        &RelationManager,THREAD               ! RelationManager for JOBACC
Access:STOCKALL      &FileManager,THREAD                   ! FileManager for STOCKALL
Relate:STOCKALL      &RelationManager,THREAD               ! RelationManager for STOCKALL
Access:REPSCHLC      &FileManager,THREAD                   ! FileManager for REPSCHLC
Relate:REPSCHLC      &RelationManager,THREAD               ! RelationManager for REPSCHLC
Access:REPSCHCT      &FileManager,THREAD                   ! FileManager for REPSCHCT
Relate:REPSCHCT      &RelationManager,THREAD               ! RelationManager for REPSCHCT
Access:USELEVEL      &FileManager,THREAD                   ! FileManager for USELEVEL
Relate:USELEVEL      &RelationManager,THREAD               ! RelationManager for USELEVEL
Access:REPSCHMA      &FileManager,THREAD                   ! FileManager for REPSCHMA
Relate:REPSCHMA      &RelationManager,THREAD               ! RelationManager for REPSCHMA
Access:REPSCHSL      &FileManager,THREAD                   ! FileManager for REPSCHSL
Relate:REPSCHSL      &RelationManager,THREAD               ! RelationManager for REPSCHSL
Access:REPSCHST      &FileManager,THREAD                   ! FileManager for REPSCHST
Relate:REPSCHST      &RelationManager,THREAD               ! RelationManager for REPSCHST
Access:REPSCHLG      &FileManager,THREAD                   ! FileManager for REPSCHLG
Relate:REPSCHLG      &RelationManager,THREAD               ! RelationManager for REPSCHLG
Access:REPSCHCR      &FileManager,THREAD                   ! FileManager for REPSCHCR
Relate:REPSCHCR      &RelationManager,THREAD               ! RelationManager for REPSCHCR
Access:REPSCHAC      &FileManager,THREAD                   ! FileManager for REPSCHAC
Relate:REPSCHAC      &RelationManager,THREAD               ! RelationManager for REPSCHAC
Access:ALLLEVEL      &FileManager,THREAD                   ! FileManager for ALLLEVEL
Relate:ALLLEVEL      &RelationManager,THREAD               ! RelationManager for ALLLEVEL
Access:ACCAREAS      &FileManager,THREAD                   ! FileManager for ACCAREAS
Relate:ACCAREAS      &RelationManager,THREAD               ! RelationManager for ACCAREAS
Access:AUDIT         &FileManager,THREAD                   ! FileManager for AUDIT
Relate:AUDIT         &RelationManager,THREAD               ! RelationManager for AUDIT
Access:RAPIDSTOCK    &FileManager,THREAD                   ! FileManager for RAPIDSTOCK
Relate:RAPIDSTOCK    &RelationManager,THREAD               ! RelationManager for RAPIDSTOCK
Access:USERS         &FileManager,THREAD                   ! FileManager for USERS
Relate:USERS         &RelationManager,THREAD               ! RelationManager for USERS
Access:LOCSHELF      &FileManager,THREAD                   ! FileManager for LOCSHELF
Relate:LOCSHELF      &RelationManager,THREAD               ! RelationManager for LOCSHELF
Access:DEFAULTS      &FileManager,THREAD                   ! FileManager for DEFAULTS
Relate:DEFAULTS      &RelationManager,THREAD               ! RelationManager for DEFAULTS
Access:REPTYCAT      &FileManager,THREAD                   ! FileManager for REPTYCAT
Relate:REPTYCAT      &RelationManager,THREAD               ! RelationManager for REPTYCAT
Access:REPTYPETEMP   &FileManager,THREAD                   ! FileManager for REPTYPETEMP
Relate:REPTYPETEMP   &RelationManager,THREAD               ! RelationManager for REPTYPETEMP
Access:RTNORDER      &FileManager,THREAD                   ! FileManager for RTNORDER
Relate:RTNORDER      &RelationManager,THREAD               ! RelationManager for RTNORDER
Access:MANFPALO      &FileManager,THREAD                   ! FileManager for MANFPALO
Relate:MANFPALO      &RelationManager,THREAD               ! RelationManager for MANFPALO
Access:JOBSTAGE      &FileManager,THREAD                   ! FileManager for JOBSTAGE
Relate:JOBSTAGE      &RelationManager,THREAD               ! RelationManager for JOBSTAGE
Access:SBO_OutParts  &FileManager,THREAD                   ! FileManager for SBO_OutParts
Relate:SBO_OutParts  &RelationManager,THREAD               ! RelationManager for SBO_OutParts
Access:SBO_GenericFile &FileManager,THREAD                 ! FileManager for SBO_GenericFile
Relate:SBO_GenericFile &RelationManager,THREAD             ! RelationManager for SBO_GenericFile
Access:MANFAUPA      &FileManager,THREAD                   ! FileManager for MANFAUPA
Relate:MANFAUPA      &RelationManager,THREAD               ! RelationManager for MANFAUPA
Access:SMSText       &FileManager,THREAD                   ! FileManager for SMSText
Relate:SMSText       &RelationManager,THREAD               ! RelationManager for SMSText
Access:EXPPARTS      &FileManager,THREAD                   ! FileManager for EXPPARTS
Relate:EXPPARTS      &RelationManager,THREAD               ! RelationManager for EXPPARTS
Access:REPEXTRP      &FileManager,THREAD                   ! FileManager for REPEXTRP
Relate:REPEXTRP      &RelationManager,THREAD               ! RelationManager for REPEXTRP
Access:GENSHORT      &FileManager,THREAD                   ! FileManager for GENSHORT
Relate:GENSHORT      &RelationManager,THREAD               ! RelationManager for GENSHORT
Access:LOCINTER      &FileManager,THREAD                   ! FileManager for LOCINTER
Relate:LOCINTER      &RelationManager,THREAD               ! RelationManager for LOCINTER
Access:STAHEAD       &FileManager,THREAD                   ! FileManager for STAHEAD
Relate:STAHEAD       &RelationManager,THREAD               ! RelationManager for STAHEAD
Access:NOTESINV      &FileManager,THREAD                   ! FileManager for NOTESINV
Relate:NOTESINV      &RelationManager,THREAD               ! RelationManager for NOTESINV
Access:NOTESFAU      &FileManager,THREAD                   ! FileManager for NOTESFAU
Relate:NOTESFAU      &RelationManager,THREAD               ! RelationManager for NOTESFAU
Access:MANFAULO      &FileManager,THREAD                   ! FileManager for MANFAULO
Relate:MANFAULO      &RelationManager,THREAD               ! RelationManager for MANFAULO
Access:STATREP       &FileManager,THREAD                   ! FileManager for STATREP
Relate:STATREP       &RelationManager,THREAD               ! RelationManager for STATREP
Access:JOBEXHIS      &FileManager,THREAD                   ! FileManager for JOBEXHIS
Relate:JOBEXHIS      &RelationManager,THREAD               ! RelationManager for JOBEXHIS
Access:STARECIP      &FileManager,THREAD                   ! FileManager for STARECIP
Relate:STARECIP      &RelationManager,THREAD               ! RelationManager for STARECIP
Access:STOPARTS      &FileManager,THREAD                   ! FileManager for STOPARTS
Relate:STOPARTS      &RelationManager,THREAD               ! RelationManager for STOPARTS
Access:RETSTOCK      &FileManager,THREAD                   ! FileManager for RETSTOCK
Relate:RETSTOCK      &RelationManager,THREAD               ! RelationManager for RETSTOCK
Access:STOCKALX      &FileManager,THREAD                   ! FileManager for STOCKALX
Relate:STOCKALX      &RelationManager,THREAD               ! RelationManager for STOCKALX
Access:RETPAY        &FileManager,THREAD                   ! FileManager for RETPAY
Relate:RETPAY        &RelationManager,THREAD               ! RelationManager for RETPAY
Access:RETDESNO      &FileManager,THREAD                   ! FileManager for RETDESNO
Relate:RETDESNO      &RelationManager,THREAD               ! RelationManager for RETDESNO
Access:ORDPARTS      &FileManager,THREAD                   ! FileManager for ORDPARTS
Relate:ORDPARTS      &RelationManager,THREAD               ! RelationManager for ORDPARTS
Access:WAYBILLS      &FileManager,THREAD                   ! FileManager for WAYBILLS
Relate:WAYBILLS      &RelationManager,THREAD               ! RelationManager for WAYBILLS
Access:STOCKMIN      &FileManager,THREAD                   ! FileManager for STOCKMIN
Relate:STOCKMIN      &RelationManager,THREAD               ! RelationManager for STOCKMIN
Access:LOCATION      &FileManager,THREAD                   ! FileManager for LOCATION
Relate:LOCATION      &RelationManager,THREAD               ! RelationManager for LOCATION
Access:STOHISTE      &FileManager,THREAD                   ! FileManager for STOHISTE
Relate:STOHISTE      &RelationManager,THREAD               ! RelationManager for STOHISTE
Access:STOMPFAU      &FileManager,THREAD                   ! FileManager for STOMPFAU
Relate:STOMPFAU      &RelationManager,THREAD               ! RelationManager for STOMPFAU
Access:STOMJFAU      &FileManager,THREAD                   ! FileManager for STOMJFAU
Relate:STOMJFAU      &RelationManager,THREAD               ! RelationManager for STOMJFAU
Access:WPARTTMP      &FileManager,THREAD                   ! FileManager for WPARTTMP
Relate:WPARTTMP      &RelationManager,THREAD               ! RelationManager for WPARTTMP
Access:CREDNOTR      &FileManager,THREAD                   ! FileManager for CREDNOTR
Relate:CREDNOTR      &RelationManager,THREAD               ! RelationManager for CREDNOTR
Access:PARTSTMP      &FileManager,THREAD                   ! FileManager for PARTSTMP
Relate:PARTSTMP      &RelationManager,THREAD               ! RelationManager for PARTSTMP
Access:EXCHANGE      &FileManager,THREAD                   ! FileManager for EXCHANGE
Relate:EXCHANGE      &RelationManager,THREAD               ! RelationManager for EXCHANGE
Access:STOESN        &FileManager,THREAD                   ! FileManager for STOESN
Relate:STOESN        &RelationManager,THREAD               ! RelationManager for STOESN
Access:LOANACC       &FileManager,THREAD                   ! FileManager for LOANACC
Relate:LOANACC       &RelationManager,THREAD               ! RelationManager for LOANACC
Access:SUPPLIER      &FileManager,THREAD                   ! FileManager for SUPPLIER
Relate:SUPPLIER      &RelationManager,THREAD               ! RelationManager for SUPPLIER
Access:SUBURB        &FileManager,THREAD                   ! FileManager for SUBURB
Relate:SUBURB        &RelationManager,THREAD               ! RelationManager for SUBURB
Access:EXCHACC       &FileManager,THREAD                   ! FileManager for EXCHACC
Relate:EXCHACC       &RelationManager,THREAD               ! RelationManager for EXCHACC
Access:COUBUSHR      &FileManager,THREAD                   ! FileManager for COUBUSHR
Relate:COUBUSHR      &RelationManager,THREAD               ! RelationManager for COUBUSHR
Access:RECIPTYP      &FileManager,THREAD                   ! FileManager for RECIPTYP
Relate:RECIPTYP      &RelationManager,THREAD               ! RelationManager for RECIPTYP
Access:SUPVALA       &FileManager,THREAD                   ! FileManager for SUPVALA
Relate:SUPVALA       &RelationManager,THREAD               ! RelationManager for SUPVALA
Access:ESTPARTS      &FileManager,THREAD                   ! FileManager for ESTPARTS
Relate:ESTPARTS      &RelationManager,THREAD               ! RelationManager for ESTPARTS
Access:REGIONS       &FileManager,THREAD                   ! FileManager for REGIONS
Relate:REGIONS       &RelationManager,THREAD               ! RelationManager for REGIONS
Access:HUBS          &FileManager,THREAD                   ! FileManager for HUBS
Relate:HUBS          &RelationManager,THREAD               ! RelationManager for HUBS
Access:SBO_OutFaultParts &FileManager,THREAD               ! FileManager for SBO_OutFaultParts
Relate:SBO_OutFaultParts &RelationManager,THREAD           ! RelationManager for SBO_OutFaultParts
Access:TRAHUBS       &FileManager,THREAD                   ! FileManager for TRAHUBS
Relate:TRAHUBS       &RelationManager,THREAD               ! RelationManager for TRAHUBS
Access:TRAEMAIL      &FileManager,THREAD                   ! FileManager for TRAEMAIL
Relate:TRAEMAIL      &RelationManager,THREAD               ! RelationManager for TRAEMAIL
Access:SUPVALB       &FileManager,THREAD                   ! FileManager for SUPVALB
Relate:SUPVALB       &RelationManager,THREAD               ! RelationManager for SUPVALB
Access:SUBBUSHR      &FileManager,THREAD                   ! FileManager for SUBBUSHR
Relate:SUBBUSHR      &RelationManager,THREAD               ! RelationManager for SUBBUSHR
Access:JOBS          &FileManager,THREAD                   ! FileManager for JOBS
Relate:JOBS          &RelationManager,THREAD               ! RelationManager for JOBS
Access:MODELNUM      &FileManager,THREAD                   ! FileManager for MODELNUM
Relate:MODELNUM      &RelationManager,THREAD               ! RelationManager for MODELNUM
Access:TRABUSHR      &FileManager,THREAD                   ! FileManager for TRABUSHR
Relate:TRABUSHR      &RelationManager,THREAD               ! RelationManager for TRABUSHR
Access:SUBEMAIL      &FileManager,THREAD                   ! FileManager for SUBEMAIL
Relate:SUBEMAIL      &RelationManager,THREAD               ! RelationManager for SUBEMAIL
Access:SUPPTEMP      &FileManager,THREAD                   ! FileManager for SUPPTEMP
Relate:SUPPTEMP      &RelationManager,THREAD               ! RelationManager for SUPPTEMP
Access:ORDERS        &FileManager,THREAD                   ! FileManager for ORDERS
Relate:ORDERS        &RelationManager,THREAD               ! RelationManager for ORDERS
Access:UNITTYPE      &FileManager,THREAD                   ! FileManager for UNITTYPE
Relate:UNITTYPE      &RelationManager,THREAD               ! RelationManager for UNITTYPE
Access:STATUS        &FileManager,THREAD                   ! FileManager for STATUS
Relate:STATUS        &RelationManager,THREAD               ! RelationManager for STATUS
Access:REPAIRTY      &FileManager,THREAD                   ! FileManager for REPAIRTY
Relate:REPAIRTY      &RelationManager,THREAD               ! RelationManager for REPAIRTY
Access:TRDACC        &FileManager,THREAD                   ! FileManager for TRDACC
Relate:TRDACC        &RelationManager,THREAD               ! RelationManager for TRDACC
Access:TRDMAN        &FileManager,THREAD                   ! FileManager for TRDMAN
Relate:TRDMAN        &RelationManager,THREAD               ! RelationManager for TRDMAN
Access:TRDPONO       &FileManager,THREAD                   ! FileManager for TRDPONO
Relate:TRDPONO       &RelationManager,THREAD               ! RelationManager for TRDPONO
Access:TRADETMP      &FileManager,THREAD                   ! FileManager for TRADETMP
Relate:TRADETMP      &RelationManager,THREAD               ! RelationManager for TRADETMP
Access:EXCCHRGE      &FileManager,THREAD                   ! FileManager for EXCCHRGE
Relate:EXCCHRGE      &RelationManager,THREAD               ! RelationManager for EXCCHRGE
Access:INVOICE       &FileManager,THREAD                   ! FileManager for INVOICE
Relate:INVOICE       &RelationManager,THREAD               ! RelationManager for INVOICE
Access:TRDREPTY      &FileManager,THREAD                   ! FileManager for TRDREPTY
Relate:TRDREPTY      &RelationManager,THREAD               ! RelationManager for TRDREPTY
Access:TRDBATCH      &FileManager,THREAD                   ! FileManager for TRDBATCH
Relate:TRDBATCH      &RelationManager,THREAD               ! RelationManager for TRDBATCH
Access:TRACHRGE      &FileManager,THREAD                   ! FileManager for TRACHRGE
Relate:TRACHRGE      &RelationManager,THREAD               ! RelationManager for TRACHRGE
Access:TRDMODEL      &FileManager,THREAD                   ! FileManager for TRDMODEL
Relate:TRDMODEL      &RelationManager,THREAD               ! RelationManager for TRDMODEL
Access:STOMODEL      &FileManager,THREAD                   ! FileManager for STOMODEL
Relate:STOMODEL      &RelationManager,THREAD               ! RelationManager for STOMODEL
Access:TRADEACC      &FileManager,THREAD                   ! FileManager for TRADEACC
Relate:TRADEACC      &RelationManager,THREAD               ! RelationManager for TRADEACC
Access:QUERYREA      &FileManager,THREAD                   ! FileManager for QUERYREA
Relate:QUERYREA      &RelationManager,THREAD               ! RelationManager for QUERYREA
Access:REQUISIT      &FileManager,THREAD                   ! FileManager for REQUISIT
Relate:REQUISIT      &RelationManager,THREAD               ! RelationManager for REQUISIT
Access:SUBCHRGE      &FileManager,THREAD                   ! FileManager for SUBCHRGE
Relate:SUBCHRGE      &RelationManager,THREAD               ! RelationManager for SUBCHRGE
Access:DEFCHRGE      &FileManager,THREAD                   ! FileManager for DEFCHRGE
Relate:DEFCHRGE      &RelationManager,THREAD               ! RelationManager for DEFCHRGE
Access:SUBTRACC      &FileManager,THREAD                   ! FileManager for SUBTRACC
Relate:SUBTRACC      &RelationManager,THREAD               ! RelationManager for SUBTRACC
Access:CHARTYPE      &FileManager,THREAD                   ! FileManager for CHARTYPE
Relate:CHARTYPE      &RelationManager,THREAD               ! RelationManager for CHARTYPE
Access:WAYBPRO       &FileManager,THREAD                   ! FileManager for WAYBPRO
Relate:WAYBPRO       &RelationManager,THREAD               ! RelationManager for WAYBPRO
Access:WAYAUDIT      &FileManager,THREAD                   ! FileManager for WAYAUDIT
Relate:WAYAUDIT      &RelationManager,THREAD               ! RelationManager for WAYAUDIT
Access:WAYBAWT       &FileManager,THREAD                   ! FileManager for WAYBAWT
Relate:WAYBAWT       &RelationManager,THREAD               ! RelationManager for WAYBAWT
Access:WAYITEMS      &FileManager,THREAD                   ! FileManager for WAYITEMS
Relate:WAYITEMS      &RelationManager,THREAD               ! RelationManager for WAYITEMS
Access:WAYLAWT       &FileManager,THREAD                   ! FileManager for WAYLAWT
Relate:WAYLAWT       &RelationManager,THREAD               ! RelationManager for WAYLAWT
Access:WAYSUND       &FileManager,THREAD                   ! FileManager for WAYSUND
Relate:WAYSUND       &RelationManager,THREAD               ! RelationManager for WAYSUND
Access:WAYJOBS       &FileManager,THREAD                   ! FileManager for WAYJOBS
Relate:WAYJOBS       &RelationManager,THREAD               ! RelationManager for WAYJOBS
Access:WAYSUND_TEMP  &FileManager,THREAD                   ! FileManager for WAYSUND_TEMP
Relate:WAYSUND_TEMP  &RelationManager,THREAD               ! RelationManager for WAYSUND_TEMP
Access:WAYCNR        &FileManager,THREAD                   ! FileManager for WAYCNR
Relate:WAYCNR        &RelationManager,THREAD               ! RelationManager for WAYCNR
Access:WAYBILLJ      &FileManager,THREAD                   ! FileManager for WAYBILLJ
Relate:WAYBILLJ      &RelationManager,THREAD               ! RelationManager for WAYBILLJ
Access:MANUFACT      &FileManager,THREAD                   ! FileManager for MANUFACT
Relate:MANUFACT      &RelationManager,THREAD               ! RelationManager for MANUFACT
Access:USMASSIG      &FileManager,THREAD                   ! FileManager for USMASSIG
Relate:USMASSIG      &RelationManager,THREAD               ! RelationManager for USMASSIG
Access:EXPWARPARTS   &FileManager,THREAD                   ! FileManager for EXPWARPARTS
Relate:EXPWARPARTS   &RelationManager,THREAD               ! RelationManager for EXPWARPARTS
Access:WEBJOB        &FileManager,THREAD                   ! FileManager for WEBJOB
Relate:WEBJOB        &RelationManager,THREAD               ! RelationManager for WEBJOB
Access:WIPEXC        &FileManager,THREAD                   ! FileManager for WIPEXC
Relate:WIPEXC        &RelationManager,THREAD               ! RelationManager for WIPEXC
Access:USUASSIG      &FileManager,THREAD                   ! FileManager for USUASSIG
Relate:USUASSIG      &RelationManager,THREAD               ! RelationManager for USUASSIG
Access:DEFEDI        &FileManager,THREAD                   ! FileManager for DEFEDI
Relate:DEFEDI        &RelationManager,THREAD               ! RelationManager for DEFEDI
Access:WIPSCAN       &FileManager,THREAD                   ! FileManager for WIPSCAN
Relate:WIPSCAN       &RelationManager,THREAD               ! RelationManager for WIPSCAN
Access:REPMAIL       &FileManager,THREAD                   ! FileManager for REPMAIL
Relate:REPMAIL       &RelationManager,THREAD               ! RelationManager for REPMAIL
Access:PRIBAND_ALIAS &FileManager,THREAD                   ! FileManager for PRIBAND_ALIAS
Relate:PRIBAND_ALIAS &RelationManager,THREAD               ! RelationManager for PRIBAND_ALIAS
Access:SMSRECVD_ALIAS &FileManager,THREAD                  ! FileManager for SMSRECVD_ALIAS
Relate:SMSRECVD_ALIAS &RelationManager,THREAD              ! RelationManager for SMSRECVD_ALIAS
Access:SMSText_Alias &FileManager,THREAD                   ! FileManager for SMSText_Alias
Relate:SMSText_Alias &RelationManager,THREAD               ! RelationManager for SMSText_Alias
Access:WAYLAWT_ALIAS &FileManager,THREAD                   ! FileManager for WAYLAWT_ALIAS
Relate:WAYLAWT_ALIAS &RelationManager,THREAD               ! RelationManager for WAYLAWT_ALIAS
Access:RTNAWAIT_ALIAS &FileManager,THREAD                  ! FileManager for RTNAWAIT_ALIAS
Relate:RTNAWAIT_ALIAS &RelationManager,THREAD              ! RelationManager for RTNAWAIT_ALIAS
Access:CURRENCY_ALIAS &FileManager,THREAD                  ! FileManager for CURRENCY_ALIAS
Relate:CURRENCY_ALIAS &RelationManager,THREAD              ! RelationManager for CURRENCY_ALIAS
Access:Audit_Alias   &FileManager,THREAD                   ! FileManager for Audit_Alias
Relate:Audit_Alias   &RelationManager,THREAD               ! RelationManager for Audit_Alias
Access:STOMJFAU_ALIAS &FileManager,THREAD                  ! FileManager for STOMJFAU_ALIAS
Relate:STOMJFAU_ALIAS &RelationManager,THREAD              ! RelationManager for STOMJFAU_ALIAS
Access:STOMPFAU_ALIAS &FileManager,THREAD                  ! FileManager for STOMPFAU_ALIAS
Relate:STOMPFAU_ALIAS &RelationManager,THREAD              ! RelationManager for STOMPFAU_ALIAS
Access:JOBSWARR_ALIAS &FileManager,THREAD                  ! FileManager for JOBSWARR_ALIAS
Relate:JOBSWARR_ALIAS &RelationManager,THREAD              ! RelationManager for JOBSWARR_ALIAS
Access:MANUFACT_ALIAS &FileManager,THREAD                  ! FileManager for MANUFACT_ALIAS
Relate:MANUFACT_ALIAS &RelationManager,THREAD              ! RelationManager for MANUFACT_ALIAS
Access:MANFAUPA_ALIAS &FileManager,THREAD                  ! FileManager for MANFAUPA_ALIAS
Relate:MANFAUPA_ALIAS &RelationManager,THREAD              ! RelationManager for MANFAUPA_ALIAS
Access:JOBSE2_ALIAS  &FileManager,THREAD                   ! FileManager for JOBSE2_ALIAS
Relate:JOBSE2_ALIAS  &RelationManager,THREAD               ! RelationManager for JOBSE2_ALIAS
Access:MANFAULT_ALIAS &FileManager,THREAD                  ! FileManager for MANFAULT_ALIAS
Relate:MANFAULT_ALIAS &RelationManager,THREAD              ! RelationManager for MANFAULT_ALIAS
Access:MANFAULO_ALIAS &FileManager,THREAD                  ! FileManager for MANFAULO_ALIAS
Relate:MANFAULO_ALIAS &RelationManager,THREAD              ! RelationManager for MANFAULO_ALIAS
Access:MANFPALO_ALIAS &FileManager,THREAD                  ! FileManager for MANFPALO_ALIAS
Relate:MANFPALO_ALIAS &RelationManager,THREAD              ! RelationManager for MANFPALO_ALIAS
Access:JOBSOBF_ALIAS &FileManager,THREAD                   ! FileManager for JOBSOBF_ALIAS
Relate:JOBSOBF_ALIAS &RelationManager,THREAD               ! RelationManager for JOBSOBF_ALIAS
Access:GRNOTESAlias  &FileManager,THREAD                   ! FileManager for GRNOTESAlias
Relate:GRNOTESAlias  &RelationManager,THREAD               ! RelationManager for GRNOTESAlias
Access:JOBSE_ALIAS   &FileManager,THREAD                   ! FileManager for JOBSE_ALIAS
Relate:JOBSE_ALIAS   &RelationManager,THREAD               ! RelationManager for JOBSE_ALIAS
Access:REPTYDEF_ALIAS &FileManager,THREAD                  ! FileManager for REPTYDEF_ALIAS
Relate:REPTYDEF_ALIAS &RelationManager,THREAD              ! RelationManager for REPTYDEF_ALIAS
Access:SUBEMAIL_ALIAS &FileManager,THREAD                  ! FileManager for SUBEMAIL_ALIAS
Relate:SUBEMAIL_ALIAS &RelationManager,THREAD              ! RelationManager for SUBEMAIL_ALIAS
Access:SUBCHRGE_ALIAS &FileManager,THREAD                  ! FileManager for SUBCHRGE_ALIAS
Relate:SUBCHRGE_ALIAS &RelationManager,THREAD              ! RelationManager for SUBCHRGE_ALIAS
Access:SUBACCAD_ALIAS &FileManager,THREAD                  ! FileManager for SUBACCAD_ALIAS
Relate:SUBACCAD_ALIAS &RelationManager,THREAD              ! RelationManager for SUBACCAD_ALIAS
Access:TRADEACC_ALIAS &FileManager,THREAD                  ! FileManager for TRADEACC_ALIAS
Relate:TRADEACC_ALIAS &RelationManager,THREAD              ! RelationManager for TRADEACC_ALIAS
Access:SUBTRACC_ALIAS &FileManager,THREAD                  ! FileManager for SUBTRACC_ALIAS
Relate:SUBTRACC_ALIAS &RelationManager,THREAD              ! RelationManager for SUBTRACC_ALIAS
Access:ESNMODEL_ALIAS &FileManager,THREAD                  ! FileManager for ESNMODEL_ALIAS
Relate:ESNMODEL_ALIAS &RelationManager,THREAD              ! RelationManager for ESNMODEL_ALIAS
Access:ESNMODAL_ALIAS &FileManager,THREAD                  ! FileManager for ESNMODAL_ALIAS
Relate:ESNMODAL_ALIAS &RelationManager,THREAD              ! RelationManager for ESNMODAL_ALIAS
Access:MULDESP_ALIAS &FileManager,THREAD                   ! FileManager for MULDESP_ALIAS
Relate:MULDESP_ALIAS &RelationManager,THREAD               ! RelationManager for MULDESP_ALIAS
Access:MULDESPJ_ALIAS &FileManager,THREAD                  ! FileManager for MULDESPJ_ALIAS
Relate:MULDESPJ_ALIAS &RelationManager,THREAD              ! RelationManager for MULDESPJ_ALIAS
Access:JOBS2_ALIAS   &FileManager,THREAD                   ! FileManager for JOBS2_ALIAS
Relate:JOBS2_ALIAS   &RelationManager,THREAD               ! RelationManager for JOBS2_ALIAS
Access:LOGASSST_ALIAS &FileManager,THREAD                  ! FileManager for LOGASSST_ALIAS
Relate:LOGASSST_ALIAS &RelationManager,THREAD              ! RelationManager for LOGASSST_ALIAS
Access:LOGSTOCK_ALIAS &FileManager,THREAD                  ! FileManager for LOGSTOCK_ALIAS
Relate:LOGSTOCK_ALIAS &RelationManager,THREAD              ! RelationManager for LOGSTOCK_ALIAS
Access:LOGSERST_ALIAS &FileManager,THREAD                  ! FileManager for LOGSERST_ALIAS
Relate:LOGSERST_ALIAS &RelationManager,THREAD              ! RelationManager for LOGSERST_ALIAS
Access:COURIER_ALIAS &FileManager,THREAD                   ! FileManager for COURIER_ALIAS
Relate:COURIER_ALIAS &RelationManager,THREAD               ! RelationManager for COURIER_ALIAS
Access:JOBNOTES_ALIAS &FileManager,THREAD                  ! FileManager for JOBNOTES_ALIAS
Relate:JOBNOTES_ALIAS &RelationManager,THREAD              ! RelationManager for JOBNOTES_ALIAS
Access:TRDBATCH_ALIAS &FileManager,THREAD                  ! FileManager for TRDBATCH_ALIAS
Relate:TRDBATCH_ALIAS &RelationManager,THREAD              ! RelationManager for TRDBATCH_ALIAS
Access:INVOICE_ALIAS &FileManager,THREAD                   ! FileManager for INVOICE_ALIAS
Relate:INVOICE_ALIAS &RelationManager,THREAD               ! RelationManager for INVOICE_ALIAS
Access:ORDPEND_ALIAS &FileManager,THREAD                   ! FileManager for ORDPEND_ALIAS
Relate:ORDPEND_ALIAS &RelationManager,THREAD               ! RelationManager for ORDPEND_ALIAS
Access:RETSALES_ALIAS &FileManager,THREAD                  ! FileManager for RETSALES_ALIAS
Relate:RETSALES_ALIAS &RelationManager,THREAD              ! RelationManager for RETSALES_ALIAS
Access:RETSTOCK_ALIAS &FileManager,THREAD                  ! FileManager for RETSTOCK_ALIAS
Relate:RETSTOCK_ALIAS &RelationManager,THREAD              ! RelationManager for RETSTOCK_ALIAS
Access:LOCATION_ALIAS &FileManager,THREAD                  ! FileManager for LOCATION_ALIAS
Relate:LOCATION_ALIAS &RelationManager,THREAD              ! RelationManager for LOCATION_ALIAS
Access:COMMONFA_ALIAS &FileManager,THREAD                  ! FileManager for COMMONFA_ALIAS
Relate:COMMONFA_ALIAS &RelationManager,THREAD              ! RelationManager for COMMONFA_ALIAS
Access:COMMONCP_ALIAS &FileManager,THREAD                  ! FileManager for COMMONCP_ALIAS
Relate:COMMONCP_ALIAS &RelationManager,THREAD              ! RelationManager for COMMONCP_ALIAS
Access:COMMONWP_ALIAS &FileManager,THREAD                  ! FileManager for COMMONWP_ALIAS
Relate:COMMONWP_ALIAS &RelationManager,THREAD              ! RelationManager for COMMONWP_ALIAS
Access:STOMODEL_ALIAS &FileManager,THREAD                  ! FileManager for STOMODEL_ALIAS
Relate:STOMODEL_ALIAS &RelationManager,THREAD              ! RelationManager for STOMODEL_ALIAS
Access:JOBPAYMT_ALIAS &FileManager,THREAD                  ! FileManager for JOBPAYMT_ALIAS
Relate:JOBPAYMT_ALIAS &RelationManager,THREAD              ! RelationManager for JOBPAYMT_ALIAS
Access:PARTSTMP_ALIAS &FileManager,THREAD                  ! FileManager for PARTSTMP_ALIAS
Relate:PARTSTMP_ALIAS &RelationManager,THREAD              ! RelationManager for PARTSTMP_ALIAS
Access:WPARTTMP_ALIAS &FileManager,THREAD                  ! FileManager for WPARTTMP_ALIAS
Relate:WPARTTMP_ALIAS &RelationManager,THREAD              ! RelationManager for WPARTTMP_ALIAS
Access:LOAN_ALIAS    &FileManager,THREAD                   ! FileManager for LOAN_ALIAS
Relate:LOAN_ALIAS    &RelationManager,THREAD               ! RelationManager for LOAN_ALIAS
Access:EXCHANGE_ALIAS &FileManager,THREAD                  ! FileManager for EXCHANGE_ALIAS
Relate:EXCHANGE_ALIAS &RelationManager,THREAD              ! RelationManager for EXCHANGE_ALIAS
Access:ACCAREAS_ALIAS &FileManager,THREAD                  ! FileManager for ACCAREAS_ALIAS
Relate:ACCAREAS_ALIAS &RelationManager,THREAD              ! RelationManager for ACCAREAS_ALIAS
Access:USERS_ALIAS   &FileManager,THREAD                   ! FileManager for USERS_ALIAS
Relate:USERS_ALIAS   &RelationManager,THREAD               ! RelationManager for USERS_ALIAS
Access:WARPARTS_ALIAS &FileManager,THREAD                  ! FileManager for WARPARTS_ALIAS
Relate:WARPARTS_ALIAS &RelationManager,THREAD              ! RelationManager for WARPARTS_ALIAS
Access:ORDPARTS_ALIAS &FileManager,THREAD                  ! FileManager for ORDPARTS_ALIAS
Relate:ORDPARTS_ALIAS &RelationManager,THREAD              ! RelationManager for ORDPARTS_ALIAS
Access:PARTS_ALIAS   &FileManager,THREAD                   ! FileManager for PARTS_ALIAS
Relate:PARTS_ALIAS   &RelationManager,THREAD               ! RelationManager for PARTS_ALIAS
Access:STOCK_ALIAS   &FileManager,THREAD                   ! FileManager for STOCK_ALIAS
Relate:STOCK_ALIAS   &RelationManager,THREAD               ! RelationManager for STOCK_ALIAS
Access:JOBS_ALIAS    &FileManager,THREAD                   ! FileManager for JOBS_ALIAS
Relate:JOBS_ALIAS    &RelationManager,THREAD               ! RelationManager for JOBS_ALIAS
Access:STDCHRGE_ALIAS &FileManager,THREAD                  ! FileManager for STDCHRGE_ALIAS
Relate:STDCHRGE_ALIAS &RelationManager,THREAD              ! RelationManager for STDCHRGE_ALIAS

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END

lCurrentFDSetting    LONG                                  ! Used by window frame dragging
lAdjFDSetting        LONG                                  ! ditto

  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('.\vodr0078.INI', NVD_INI)                   ! Configure INIManager to use INI file
  DctInit
                             ! Begin Generated by NetTalk Extension Template
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 5.51')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & sub (6300,1,1) & '.' & sub (6300,2,1))
      NetDebugTrace ('[Nettalk Template] NetTalk Object version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
      NetDebugTrace ('[Nettalk Template] ABC Template Chain')
    end
                             ! End Generated by Extension Template
  SYSTEM{PROP:Icon} = 'cellular3g.ico'
                 !CapeSoft MessageBox init code
  ThisMessageBox.init()
                 !End of CapeSoft MessageBox init code
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)       ! Configure frame dragging
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  RRC_TATReport
  INIMgr.Update
                             ! Begin Generated by NetTalk Extension Template
    NetCloseCallBackWindow() ! Tell NetTalk DLL to shutdown it's WinSock Call Back Window
  
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 5.51')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & sub (6300,1,1) & '.' & sub (6300,2,1))
      NetDebugTrace ('[Nettalk Template] Closing Down NetTalk (Object) version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
    end
                             ! End Generated by Extension Template
      ThisMessageBox.Kill()                     !CapeSoft MessageBox template generated code
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher
    

BHStripReplace            Procedure(String func:String,String func:Strip,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1
    StripLength#    = Len(func:Strip)

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

        IF SUB(func:String,STR_POS#,StripLength#) = func:Strip
            If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            Else !If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) &  SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            End !If func:Replace <> ''

        End
    End
    RETURN(func:String)
BHStripNonAlphaNum      Procedure(String func:String,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

     IF VAL(SUB(func:string,STR_POS#,1)) < 32 Or VAL(SUB(func:string,STR_POS#,1)) > 126 Or |
        VAL(SUB(func:string,STR_POS#,1)) = 34 Or VAL(SUB(func:string,STR_POS#,1)) = 44
           func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    End
    RETURN(func:String)
BHStripAlphaNumOnly          Procedure(STRING func:String,<STRING fExclusions>)
    CODE
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#


        IF (VAL(SUB(func:string,STR_POS#,1)) < 48) OR |
            (VAL(SUB(func:string,STR_POS#,1)) > 57 AND VAL(SUB(func:string,STR_POS#,1)) < 65) OR |
            (VAL(SUB(func:string,STR_POS#,1)) > 90)

            ! Check for exclusions
            Allowed# = 0
            LOOP ll# = 1 TO LEN(CLIP(fExclusions))
                IF (SUB(fExclusions,ll#,1) = SUB(func:String,STR_POS#,1))
                    Allowed# = 1
                    BREAK
                END
            END
            IF (Allowed# = 1)
                CYCLE
            END

            func:String = SUB(func:String,1,STR_POS#-1) & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    End
    RETURN (func:String)
BHStripForFilename      Procedure(String func:String)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    !func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

        IF SUB(func:String,STR_POS#,1) = '\' OR |
           SUB(func:String,STR_POS#,1) = '/' OR |
           SUB(func:String,STR_POS#,1) = ':' OR |
           SUB(func:String,STR_POS#,1) = '*' OR |
           SUB(func:String,STR_POS#,1) = '?' OR |
           SUB(func:String,STR_POS#,1) = '"' OR |
           SUB(func:String,STR_POS#,1) = '<<' OR |
           SUB(func:String,STR_POS#,1) = '>' OR |
           SUB(func:String,STR_POS#,1) = '|'

           func:String = SUB(func:String,1,STR_POS#-1) & ' ' & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    END
    RETURN(func:String)
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
Code
    func:Hours = 0
    func:Days = 0
    func:Mins = func:TotalMins
    If func:Totalmins >= 60
        Loop Until func:Mins < 60
            func:Mins -= 60
            func:Hours += 1
            If func:Hours > 23
                func:Days += 1
                func:Hours = 0
            End !If func:Hours > 23
        End !Loop Until local:MinutesLeft < 60
    End !If func:Minutes > 60
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5Days    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5DaysMonday    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        If (x# % 7) = 1
            local:StartTime   = Deformat(Clip(func:MondayStart),@t4)
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)
        Else !If (x# % 7) = 1
            local:StartTime   = Deformat('00:00:00',@t4)
            local:2a = 0
            local:2b = 0
            local:2c = 0
            local:2d = 0
        End !If (x# % 7) = 1

        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2



        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)
BHGetFileFromPath		Procedure(String fullPath)
count                                           LONG()
locFilename                                     STRING(255)
    CODE
        locFilename = fullPath
        LOOP count = LEN(CLIP(fullPath)) TO 1 BY -1
            IF (SUB(fullPath,count,1) = '\')
                locFilename = CLIP(SUB(fullPath,count + 1,255))
                BREAK
            END
        END
        RETURN CLIP(locFilename)

BHGetPathFromFile			Procedure(String fullPath,<Byte removeSlash>)
count                                           LONG()
locPath                                         STRING(255)
    CODE
        locPath = fullPath
        LOOP count = LEN(CLIP(fullPath)) TO 1 BY -1
            IF (SUB(fullPath,count,1) = '\')
                IF (removeSlash)
                    locPath = CLIP(SUB(fullPath,1,count - 1))
                ELSE
                    locPath = CLIP(SUB(fullPath,1,count))
                END

                BREAK
            END
        END
        RETURN CLIP(locPath)
BHGetFileNoExtension    PROCEDURE(STRING fullPath)
count LONG()
locFilename                                             STRING(255)
    code
        locFilename = BHGetFileFromPath(fullPath)
        LOOP count = LEN(CLIP(locFilename)) TO 1 BY -1
            IF (SUB(locFilename,count,1) = '.')
                locFilename = CLIP(SUB(locFilename,1,count - 1))
                BREAK
            END
        END
        RETURN CLIP(locFilename)
BHPutFileIntoQueue          Procedure(STRING fullPath,*bhQFileList Q)
locBreakPoint                                   LONG()
locStartPoint                                   LONG()
locFilePath                                     STRING(255)
    code
        locBreakPoint = INSTRING('<124>',fullPath,1,1)

        IF (locBreakPoint > 0)
            locFilePath = SUB(fullPath,1,locBreakPoint - 1) & '\'

            locStartPoint = locBreakPoint + 1
            LOOP xx# = (locBreakPoint + 1) TO LEN(CLIP(fullPath))
                IF (SUB(fullPath,xx#,1) = '<124>')
                    Q.qFilename = CLIP(locFilePath) & CLIP(SUB(fullPath,locStartPoint,(xx# - locStartPoint)))
                    ADD(Q)
                    locStartPoint = xx# + 1
                END
            END
            Q.qFilename = CLIP(locFilePath) & CLIP(SUB(fullPath,locStartPoint,(xx# - locStartPoint)))
            ADD(Q)
        ELSE
            Q.qFilename = CLIP(fullPath)
            ADD(Q)
        end
BHAddBackSlash              Procedure(STRING fullPath)
ReturnPath  STRING(255)
    code
        ReturnPath = fullPath
        IF (SUB(CLIP(fullPath),-1,1) <> '\')
            ReturnPath = CLIP(fullPath) & '\'
        END
        RETURN CLIP(ReturnPath)

BHFileInUse                  Procedure(FILE ffilename,<KEY fKeyName>,BYTE fNoRelease = 0, BYTE fShowMessage = 0,<STRING fMessageText>)
    CODE
        IF (fKeyName &= NULL)
            POINTER# = POINTER(fFileName)
            HOLD(fFileName,1)
            GET(fFileName,POINTER#)
        ELSE
            HOLD(fFileName,1)
            GET(fFileName,fKeyName)
        END

        IF (ERRORCODE() = 43)
            IF (fShowMessage = 1)
                Beep(Beep:SystemHand)  ;  Yield()
                Case Message(Clip(fMessageText)&|
                    '|'&|
                    '|The selected record is being accessed by another user.','Record In Use',|
                    icon:Hand,'&OK',1,1)
                Of 1 ! &OK Button
                End

            END
            RETURN TRUE
        END
        IF (fNoRelease <> 1)
            RELEASE(fFileName)
        END

        RETURN FALSE
BHGetTempFolder   PROCEDURE()
TEMPFOLDER         EQUATE(001CH)
kMyDocuments                            CSTRING(255)
    CODE
        SHGetSpecialFolderPathDBHFT(GetDesktopWindowDBHFT(),kMyDocuments,TEMPFOLDER,0)

        IF (SUB(kMyDocuments,-1,1) <> '\')
            kMyDocuments = CLIP(kMyDocuments) & '\'
        END

        RETURN kMyDocuments
BHGetDocumentsFolder        PROCEDURE(<STRING fSubFolder1>,<STRING fSubFolder2>,<STRING fSubFolder3>)
PERSONALFOLDER      EQUATE(0005H)
kMyDocuments        CSTRING(255)
    CODE
        SHGetSpecialFolderPathDBHFT(GetDesktopWindowDBHFT(),kMyDocuments,PERSONALFOLDER,0)

        IF (SUB(kMyDocuments,-1,1) <> '\')
            kMyDocuments = CLIP(kMyDocuments) & '\'
        END

        IF (fSubFolder1 <> '')
            kMyDocuments = CLIP(kMyDocuments) & CLIP(fSubFolder1)
            IF (NOT EXISTS(kMyDocuments))
                IF (MkDir(kMyDocuments))
                    RETURN kMyDocuments
                END
            END
        END

        IF (SUB(kMyDocuments,-1,1) <> '\')
            kMyDocuments = CLIP(kMyDocuments) & '\'
        END

        IF (fSubFolder2 <> '')
            kMyDocuments = CLIP(kMyDocuments) & CLIP(fSubFolder2)
            IF (NOT EXISTS(kMyDocuments))
                IF (MkDir(kMyDocuments))
                    RETURN kMyDocuments
                END
            END
        END

        IF (SUB(kMyDocuments,-1,1) <> '\')
            kMyDocuments = CLIP(kMyDocuments) & '\'
        END

        IF (fSubFolder3 <> '')
            kMyDocuments = CLIP(kMyDocuments) & CLIP(fSubFolder3)
            IF (NOT EXISTS(kMyDocuments))
                IF (MkDir(kMyDocuments))
                    RETURN kMyDocuments
                END
            END
        END

        IF (SUB(kMyDocuments,-1,1) <> '\')
            kMyDocuments = CLIP(kMyDocuments) & '\'
        END

        RETURN kMyDocuments

BHRunDOS                Procedure(String f:Command,<f:Wait>,<f:NoTimeOut>)
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

!STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
Code
    CommandLine = clip(f:Command)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = DBHGCreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = clock() + (f:Wait * 100)
    setcursor(cursor:wait)

    if f:Wait > 0 or f:NoTimeOut > 0 then
        loop
            DBHGSleep(100)
            ! check for when process is finished
            ReturnCode# = DBHGGetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
            if (ProcessExitCode <> 259)  ! 259 = Still Active
                break
            end
            if (f:NoTimeOut <> 1)
                if (clock() < timeout#)
                    break
                end
            end
        end
    end
    setcursor
    return(true)

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
ThisMessageBox.Init                     procedure  (long Reserved1=0,long Reserved2=0)   !CapeSoft MessageBox Object Procedure
TempVar         long,dim(2)
TMPLogFileName  string(252)

  Code
    parent.Init (Reserved1,Reserved2)
    system{prop:MessageHook} = address(ds_Message)
    system{prop:StopHook} = address(ds_Stop)
    system{prop:HaltHook} = address(ds_Halt)

    INCLUDE('VodacomClass.inc','Procedures')


Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

