

   MEMBER('FixSBOnlineVettingJobs.clw')                    ! This is a MEMBER module

                     MAP
                       INCLUDE('FIXSBONLINEVETTINGJOBS001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main                 PROCEDURE                             ! Declare Procedure
locSavePath                 CSTRING(255)
locExportFile               CSTRING(255),STATIC
ExportFile                  FILE,DRIVER('BASIC'),PRE(expfil),Name(locExportFile),CREATE,BINDABLE,THREAD
RECORD                          RECORD
JobNumber                           STRING(30)
                                END
                            END

progressBar                 LONG()
ProgressWindow              WINDOW('Progress...'),AT(,,257,59),CENTER,GRAY,FONT('Tahoma',8,, |
                                FONT:regular,CHARSET:ANSI),TIMER(1),DOUBLE
                                PROGRESS,AT(24,15,223,12),USE(progressBar),RANGE(0,100)
                                STRING(''),AT(13,3),FULL,USE(?Progress:UserString),CENTER
                                STRING(''),AT(4,30,,10),FULL,USE(?Progress:PctText),CENTER
                                BUTTON('Cancel'),AT(103,42,50,15),USE(?Progress:Cancel)
                            END
FilesOpened     BYTE(0)
                    MAP
AddToLogFile            PROCEDURE()
ProcessFile             PROCEDURE()
SentToHub               PROCEDURE(LONG fJobNumber),LONG
                    END ! MAP

  CODE
!region Processed Code
    ProcessFile()
!endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:CHARTYPE.Close
     Access:JOBSE.Close
     Access:AUDIT2.Close
     Access:AUDIT.Close
     Access:JOBSWARR.Close
     Access:WEBJOB.Close
     Access:JOBS.Close
     FilesOpened = False
  END
ProcessFile         PROCEDURE()
recordCount             LONG
totalRecords            LONG
    CODE
        DO OpenFiles

        OPEN(ProgressWindow)
        DISPLAY()

        Access:JOBS.ClearKey(job:DateCompletedKey)
        job:Date_Completed = TODAY() - 1
        SET(job:DateCompletedKey,job:DateCompletedKey)
        LOOP UNTIL Access:JOBS.Next() <> Level:Benign
            IF (job:Date_Completed > TODAY())
                BREAK
            END ! IF
            totalRecords += 1
        END ! LOOP

        ?progressBar{prop:RangeHigh} = totalRecords


        Access:JOBS.ClearKey(job:DateCompletedKey)
        job:Date_Completed = TODAY() - 1
        SET(job:DateCompletedKey,job:DateCompletedKey)
        LOOP UNTIL Access:JOBS.Next() <> Level:Benign
            IF (job:Date_Completed > TODAY())
                BREAK
            END ! IF
            
            recordCount += 1
            ?progressBar{prop:Progress} = recordCount
            ?progress:PctText{prop:Text} = INT(recordCount / totalRecords * 100) & ' %'
            DISPLAY()
            
            IF (job:Warranty_Job <> 'YES')
                ! Ignore not warranty jobs
                CYCLE
            END ! IF
            
            IF (job:EDI <> '' AND job:EDI <> 'XXX')
                ! Job is a viable warranty job
                
                Access:JOBSWARR.ClearKey(jow:RefNumberKEy)
                jow:RefNumber = job:Ref_Number
                IF (Access:JOBSWARR.TryFetch(jow:RefNumberKEy) = Level:Benign)
                    ! Job already exists in the warranty table
                    CYCLE
                END ! IF
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                    CYCLE
                END ! IF
                
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                    CYCLE
                END ! IF
                
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = wob:HeadAccountNumber
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                    CYCLE
                END ! IF

                IF (Access:JOBSWARR.PrimeRecord() = Level:Benign)
                    jow:BranchID        = tra:BranchIdentification
                    jow:RefNumber       = job:Ref_Number
                    jow:Orig_Sub_Date   = job:Date_Completed
                    jow:ClaimSubmitted  = job:Date_Completed
                    jow:Status          = 'NO'
                    jow:RRCStatus       = 'NO'
                    IF (SentToHub(job:Ref_Number))
                        jow:RepairedAT  = 'ARC'
                    ELSE ! IF
                        jow:RepairedAT  = 'RRC'
                    END ! IF
                    jow:Manufacturer    = job:Manufacturer
                    
                    Access:CHARTYPE.ClearKey(cha:Warranty_Key)
                    cha:Warranty = 'YES'
                    cha:Charge_Type = job:Warranty_Charge_Type
                    IF (Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign)
                        IF (cha:SecondYearWarranty)
                            jow:FirstSecondYear = 1
                        ELSE ! IF
                            jow:FirstSecondYear = 0
                        END ! IF
                    END ! IF
                    jow:Submitted = 1
                    
                    IF (Access:JOBSWARR.TryInsert())
                        Access:JOBSWARR.CancelAutoInc()
                        CYCLE
                    END ! IF
                    
                    IF (Access:AUDIT.PrimeRecord() = Level:Benign)
                        aud:Date = job:Date_Completed
                        aud:Time = job:Time_Completed
                        aud:Ref_Number = job:Ref_Number
                        aud:User = '&SB'
                        aud:Action = 'ORIGINAL SUBMISSION TO WARRANTY PROCESS SCREEN'
                        aud:Type = 'JOB'
                        IF (Access:AUDIT.TryInsert())
                            Access:AUDIT.CancelAutoInc()
                        ELSE
                            IF (Access:AUDIT2.PrimeRecord() = Level:Benign)
                                aud2:AUDRecordNumber = aud:Record_Number
                                aud2:Notes = 'JOB HAS BEEN PUT ON THE PENDING TAB OF THE WARRANTY PROCESS SCREEN'
                                IF (Access:AUDIT2.TryInsert())
                                    Access:AUDIT2.CancelAutoInc()
                                END ! IF
                            END ! IF
                        END ! IF
                    END ! IF
                    
                    AddToLogFile()
                END ! IF
            END ! IF
        END ! LOOP

        DO CloseFiles

        CLOSE(ProgressWindow)
        
        
SentToHub           PROCEDURE(LONG fJobNumber)!,LONG
save_lot_id             USHORT
returnValue             BYTE(0)
    CODE
        IF (jobe:HubRepair = 1 OR jobe:HubRepairDate <> '')
            returnValue = 1
        Else !If jobe:HubRepair
            Relate:LOCATLOG.Open()
            save_lot_id = Access:LOCATLOG.SaveFile()
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber   = fJobNumber
            lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                returnValue = 1
            End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
            Access:LOCATLOG.RestoreFile(save_lot_id)
            Relate:LOCATLOG.Close()
        End !If jobe:HubRepair

        Return returnValue        
             
        
AddToLogFile        PROCEDURE()
    CODE
        locExportFile = PATH() & '\_Logging\' & FORMAT(TODAY(),@d012) & ' FixSBOnlineVettingJobs.log'
        
        IF ~(EXISTS(locExportFile))
            CREATE(ExportFile)
            IF (ERRORCODE())
                RETURN
            END ! IF
            
        END ! IF
        
        SHARE(ExportFile)

        IF (ERRORCODE())
            RETURN
        END ! IF
        
        expfil:JobNumber = job:Ref_Number
        ADD(ExportFile)
        
        CLOSE(ExportFile)
        
