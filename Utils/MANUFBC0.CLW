  MEMBER('manufactfix.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
MANUFBC0:DctInit    PROCEDURE
MANUFBC0:DctKill    PROCEDURE
MANUFBC0:FilesInit  PROCEDURE
  END

Hide:Access:STDCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STDCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDIT2   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDIT2   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAHUBAC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAHUBAC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSLOCK CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSLOCK CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBOUTFL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBOUTFL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDITE   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDITE   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSENG  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSENG  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STMASAUD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STMASAUD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDSTATS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDSTATS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBTHIRD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBTHIRD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCATLOG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOCATLOG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PRODCODE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PRODCODE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOAUDIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOAUDIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:BOUNCER  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:BOUNCER  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WIPAMF   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WIPAMF   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TEAMS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TEAMS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WIPAUI   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:WIPAUI   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODELCOL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODELCOL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CONTHIST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:CONTHIST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSTAMP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSTAMP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

MANUFBC0:DctInit PROCEDURE
  CODE
  Relate:STDCHRGE &= Hide:Relate:STDCHRGE
  Relate:AUDIT2 &= Hide:Relate:AUDIT2
  Relate:TRAHUBAC &= Hide:Relate:TRAHUBAC
  Relate:JOBSLOCK &= Hide:Relate:JOBSLOCK
  Relate:JOBOUTFL &= Hide:Relate:JOBOUTFL
  Relate:AUDITE &= Hide:Relate:AUDITE
  Relate:JOBSENG &= Hide:Relate:JOBSENG
  Relate:STMASAUD &= Hide:Relate:STMASAUD
  Relate:AUDSTATS &= Hide:Relate:AUDSTATS
  Relate:JOBTHIRD &= Hide:Relate:JOBTHIRD
  Relate:LOCATLOG &= Hide:Relate:LOCATLOG
  Relate:PRODCODE &= Hide:Relate:PRODCODE
  Relate:STOAUDIT &= Hide:Relate:STOAUDIT
  Relate:BOUNCER &= Hide:Relate:BOUNCER
  Relate:WIPAMF &= Hide:Relate:WIPAMF
  Relate:TEAMS &= Hide:Relate:TEAMS
  Relate:WIPAUI &= Hide:Relate:WIPAUI
  Relate:MODELCOL &= Hide:Relate:MODELCOL
  Relate:CONTHIST &= Hide:Relate:CONTHIST
  Relate:JOBSTAMP &= Hide:Relate:JOBSTAMP

MANUFBC0:FilesInit PROCEDURE
  CODE
  Hide:Relate:STDCHRGE.Init
  Hide:Relate:AUDIT2.Init
  Hide:Relate:TRAHUBAC.Init
  Hide:Relate:JOBSLOCK.Init
  Hide:Relate:JOBOUTFL.Init
  Hide:Relate:AUDITE.Init
  Hide:Relate:JOBSENG.Init
  Hide:Relate:STMASAUD.Init
  Hide:Relate:AUDSTATS.Init
  Hide:Relate:JOBTHIRD.Init
  Hide:Relate:LOCATLOG.Init
  Hide:Relate:PRODCODE.Init
  Hide:Relate:STOAUDIT.Init
  Hide:Relate:BOUNCER.Init
  Hide:Relate:WIPAMF.Init
  Hide:Relate:TEAMS.Init
  Hide:Relate:WIPAUI.Init
  Hide:Relate:MODELCOL.Init
  Hide:Relate:CONTHIST.Init
  Hide:Relate:JOBSTAMP.Init


MANUFBC0:DctKill PROCEDURE
  CODE
  Hide:Relate:STDCHRGE.Kill
  Hide:Relate:AUDIT2.Kill
  Hide:Relate:TRAHUBAC.Kill
  Hide:Relate:JOBSLOCK.Kill
  Hide:Relate:JOBOUTFL.Kill
  Hide:Relate:AUDITE.Kill
  Hide:Relate:JOBSENG.Kill
  Hide:Relate:STMASAUD.Kill
  Hide:Relate:AUDSTATS.Kill
  Hide:Relate:JOBTHIRD.Kill
  Hide:Relate:LOCATLOG.Kill
  Hide:Relate:PRODCODE.Kill
  Hide:Relate:STOAUDIT.Kill
  Hide:Relate:BOUNCER.Kill
  Hide:Relate:WIPAMF.Kill
  Hide:Relate:TEAMS.Kill
  Hide:Relate:WIPAUI.Kill
  Hide:Relate:MODELCOL.Kill
  Hide:Relate:CONTHIST.Kill
  Hide:Relate:JOBSTAMP.Kill


Hide:Access:STDCHRGE.Init PROCEDURE
  CODE
  SELF.Init(STDCHRGE,GlobalErrors)
  SELF.Buffer &= sta:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sta:Model_Number_Charge_Key,'By Charge Type',0)
  SELF.AddKey(sta:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(sta:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(sta:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(sta:Cost_Key,'By Cost',0)
  SELF.AddKey(sta:Charge_Type_Only_Key,'sta:Charge_Type_Only_Key',0)
  SELF.AddKey(sta:Repair_Type_Only_Key,'sta:Repair_Type_Only_Key',0)
  SELF.AddKey(sta:Unit_Type_Only_Key,'sta:Unit_Type_Only_Key',0)
  Access:STDCHRGE &= SELF


Hide:Relate:STDCHRGE.Init PROCEDURE
  CODE
  Hide:Access:STDCHRGE.Init
  SELF.Init(Access:STDCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:STDCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STDCHRGE &= NULL


Hide:Relate:STDCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:STDCHRGE.Kill
  PARENT.Kill
  Relate:STDCHRGE &= NULL


Hide:Access:AUDIT2.Init PROCEDURE
  CODE
  SELF.Init(AUDIT2,GlobalErrors)
  SELF.Buffer &= aud2:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aud2:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(aud2:AUDRecordNumberKey,'Link To AUDIT',0)
  Access:AUDIT2 &= SELF


Hide:Relate:AUDIT2.Init PROCEDURE
  CODE
  Hide:Access:AUDIT2.Init
  SELF.Init(Access:AUDIT2,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDIT)


Hide:Access:AUDIT2.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDIT2 &= NULL


Hide:Relate:AUDIT2.Kill PROCEDURE

  CODE
  Hide:Access:AUDIT2.Kill
  PARENT.Kill
  Relate:AUDIT2 &= NULL


Hide:Access:TRAHUBAC.Init PROCEDURE
  CODE
  SELF.Init(TRAHUBAC,GlobalErrors)
  SELF.Buffer &= TRA1:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(TRA1:RecordNoKey,'TRA1:RecordNoKey',1)
  SELF.AddKey(TRA1:HeadAccSubAccKey,'TRA1:HeadAccSubAccKey',0)
  SELF.AddKey(TRA1:HeadAccSubAccNameKey,'TRA1:HeadAccSubAccNameKey',0)
  SELF.AddKey(TRA1:HeadAccSubAccBranchKey,'TRA1:HeadAccSubAccBranchKey',0)
  Access:TRAHUBAC &= SELF


Hide:Relate:TRAHUBAC.Init PROCEDURE
  CODE
  Hide:Access:TRAHUBAC.Init
  SELF.Init(Access:TRAHUBAC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAHUBAC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAHUBAC &= NULL


Hide:Relate:TRAHUBAC.Kill PROCEDURE

  CODE
  Hide:Access:TRAHUBAC.Kill
  PARENT.Kill
  Relate:TRAHUBAC &= NULL


Hide:Access:JOBSLOCK.Init PROCEDURE
  CODE
  SELF.Init(JOBSLOCK,GlobalErrors)
  SELF.Buffer &= lock:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lock:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lock:JobNumberKey,'By Job Number',0)
  Access:JOBSLOCK &= SELF


Hide:Relate:JOBSLOCK.Init PROCEDURE
  CODE
  Hide:Access:JOBSLOCK.Init
  SELF.Init(Access:JOBSLOCK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSLOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSLOCK &= NULL


Hide:Access:JOBSLOCK.PrimeFields PROCEDURE

  CODE
  lock:DateLocked = Today()
  lock:TimeLocked = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBSLOCK.Kill PROCEDURE

  CODE
  Hide:Access:JOBSLOCK.Kill
  PARENT.Kill
  Relate:JOBSLOCK &= NULL


Hide:Access:JOBOUTFL.Init PROCEDURE
  CODE
  SELF.Init(JOBOUTFL,GlobalErrors)
  SELF.Buffer &= joo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joo:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joo:JobNumberKey,'By Code',0)
  SELF.AddKey(joo:LevelKey,'By Level',0)
  Access:JOBOUTFL &= SELF


Hide:Relate:JOBOUTFL.Init PROCEDURE
  CODE
  Hide:Access:JOBOUTFL.Init
  SELF.Init(Access:JOBOUTFL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBOUTFL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBOUTFL &= NULL


Hide:Relate:JOBOUTFL.Kill PROCEDURE

  CODE
  Hide:Access:JOBOUTFL.Kill
  PARENT.Kill
  Relate:JOBOUTFL &= NULL


Hide:Access:AUDITE.Init PROCEDURE
  CODE
  SELF.Init(AUDITE,GlobalErrors)
  SELF.Buffer &= aude:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aude:RecordNumberKey,'Record Number Key',1)
  SELF.AddKey(aude:RefNumberKey,'By Ref Number',0)
  Access:AUDITE &= SELF


Hide:Relate:AUDITE.Init PROCEDURE
  CODE
  Hide:Access:AUDITE.Init
  SELF.Init(Access:AUDITE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDIT)


Hide:Access:AUDITE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDITE &= NULL


Hide:Relate:AUDITE.Kill PROCEDURE

  CODE
  Hide:Access:AUDITE.Kill
  PARENT.Kill
  Relate:AUDITE &= NULL


Hide:Access:JOBSENG.Init PROCEDURE
  CODE
  SELF.Init(JOBSENG,GlobalErrors)
  SELF.Buffer &= joe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joe:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joe:UserCodeKey,'By Date',0)
  SELF.AddKey(joe:UserCodeOnlyKey,'By Date',0)
  SELF.AddKey(joe:AllocatedKey,'By Date',0)
  SELF.AddKey(joe:JobNumberKey,'By Date',0)
  SELF.AddKey(joe:UserCodeStatusKey,'By Date',0)
  Access:JOBSENG &= SELF


Hide:Relate:JOBSENG.Init PROCEDURE
  CODE
  Hide:Access:JOBSENG.Init
  SELF.Init(Access:JOBSENG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSENG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSENG &= NULL


Hide:Relate:JOBSENG.Kill PROCEDURE

  CODE
  Hide:Access:JOBSENG.Kill
  PARENT.Kill
  Relate:JOBSENG &= NULL


Hide:Access:STMASAUD.Init PROCEDURE
  CODE
  SELF.Init(STMASAUD,GlobalErrors)
  SELF.Buffer &= stom:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stom:AutoIncrement_Key,'stom:AutoIncrement_Key',1)
  SELF.AddKey(stom:Compeleted_Key,'stom:Compeleted_Key',0)
  SELF.AddKey(stom:Sent_Key,'stom:Sent_Key',0)
  Access:STMASAUD &= SELF


Hide:Relate:STMASAUD.Init PROCEDURE
  CODE
  Hide:Access:STMASAUD.Init
  SELF.Init(Access:STMASAUD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOAUDIT,RI:CASCADE,RI:CASCADE,stoa:Audit_Ref_No_Key)
  SELF.AddRelationLink(stom:Audit_No,stoa:Audit_Ref_No)


Hide:Access:STMASAUD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STMASAUD &= NULL


Hide:Relate:STMASAUD.Kill PROCEDURE

  CODE
  Hide:Access:STMASAUD.Kill
  PARENT.Kill
  Relate:STMASAUD &= NULL


Hide:Access:AUDSTATS.Init PROCEDURE
  CODE
  SELF.Init(AUDSTATS,GlobalErrors)
  SELF.Buffer &= aus:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aus:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(aus:DateChangedKey,'By Date Changed',0)
  SELF.AddKey(aus:NewStatusKey,'By New Status',0)
  SELF.AddKey(aus:StatusTimeKey,'By Time',0)
  SELF.AddKey(aus:StatusDateKey,'By Time',0)
  SELF.AddKey(aus:StatusDateRecordKey,'By Record Number',0)
  SELF.AddKey(aus:StatusTypeRecordKey,'By Record Number',0)
  SELF.AddKey(aus:RefRecordNumberKey,'By Record Number',0)
  SELF.AddKey(aus:RefDateRecordKey,'By Record Number',0)
  Access:AUDSTATS &= SELF


Hide:Relate:AUDSTATS.Init PROCEDURE
  CODE
  Hide:Access:AUDSTATS.Init
  SELF.Init(Access:AUDSTATS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:AUDSTATS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDSTATS &= NULL


Hide:Relate:AUDSTATS.Kill PROCEDURE

  CODE
  Hide:Access:AUDSTATS.Kill
  PARENT.Kill
  Relate:AUDSTATS &= NULL


Hide:Access:JOBTHIRD.Init PROCEDURE
  CODE
  SELF.Init(JOBTHIRD,GlobalErrors)
  SELF.Buffer &= jot:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jot:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jot:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jot:OutIMEIKey,'By Outgoing IMEI Number',0)
  SELF.AddKey(jot:InIMEIKEy,'By Incoming IMEI Number',0)
  SELF.AddKey(jot:OutDateKey,'By Outgoing Date',0)
  SELF.AddKey(jot:ThirdPartyKey,'jot:ThirdPartyKey',0)
  SELF.AddKey(jot:OriginalIMEIKey,'By Original I.M.E.I. No',0)
  Access:JOBTHIRD &= SELF


Hide:Relate:JOBTHIRD.Init PROCEDURE
  CODE
  Hide:Access:JOBTHIRD.Init
  SELF.Init(Access:JOBTHIRD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDBATCH)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBTHIRD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBTHIRD &= NULL


Hide:Relate:JOBTHIRD.Kill PROCEDURE

  CODE
  Hide:Access:JOBTHIRD.Kill
  PARENT.Kill
  Relate:JOBTHIRD &= NULL


Hide:Access:LOCATLOG.Init PROCEDURE
  CODE
  SELF.Init(LOCATLOG,GlobalErrors)
  SELF.Buffer &= lot:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lot:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lot:DateKey,'By Date',0)
  SELF.AddKey(lot:NewLocationKey,'By Location',0)
  SELF.AddKey(lot:DateNewLocationKey,'By New Location',0)
  Access:LOCATLOG &= SELF


Hide:Relate:LOCATLOG.Init PROCEDURE
  CODE
  Hide:Access:LOCATLOG.Init
  SELF.Init(Access:LOCATLOG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:LOCATLOG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCATLOG &= NULL


Hide:Access:LOCATLOG.PrimeFields PROCEDURE

  CODE
  lot:TheDate = Today()
  lot:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:LOCATLOG.Kill PROCEDURE

  CODE
  Hide:Access:LOCATLOG.Kill
  PARENT.Kill
  Relate:LOCATLOG &= NULL


Hide:Access:PRODCODE.Init PROCEDURE
  CODE
  SELF.Init(PRODCODE,GlobalErrors)
  SELF.Buffer &= prd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(prd:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(prd:ProductCodeKey,'By Product Code',0)
  SELF.AddKey(prd:ModelProductKey,'By Product Code',0)
  Access:PRODCODE &= SELF


Hide:Relate:PRODCODE.Init PROCEDURE
  CODE
  Hide:Access:PRODCODE.Init
  SELF.Init(Access:PRODCODE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:PRODCODE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PRODCODE &= NULL


Hide:Relate:PRODCODE.Kill PROCEDURE

  CODE
  Hide:Access:PRODCODE.Kill
  PARENT.Kill
  Relate:PRODCODE &= NULL


Hide:Access:STOAUDIT.Init PROCEDURE
  CODE
  SELF.Init(STOAUDIT,GlobalErrors)
  SELF.Buffer &= stoa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stoa:Internal_AutoNumber_Key,'stoa:Internal_AutoNumber_Key',1)
  SELF.AddKey(stoa:Audit_Ref_No_Key,'stoa:Audit_Ref_No_Key',0)
  SELF.AddKey(stoa:Cellular_Key,'stoa:Cellular_Key',0)
  SELF.AddKey(stoa:Stock_Ref_No_Key,'stoa:Stock_Ref_No_Key',0)
  SELF.AddKey(stoa:Stock_Site_Number_Key,'stoa:Stock_Site_Number_Key',0)
  SELF.AddKey(stoa:Lock_Down_Key,'stoa:Lock_Down_Key',0)
  SELF.AddKey(stoa:Main_Browse_Key,'stoa:Main_Browse_Key',0)
  SELF.AddKey(stoa:Report_Key,'stoa:Report_Key',0)
  Access:STOAUDIT &= SELF


Hide:Relate:STOAUDIT.Init PROCEDURE
  CODE
  Hide:Access:STOAUDIT.Init
  SELF.Init(Access:STOAUDIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)
  SELF.AddRelation(Relate:STMASAUD)


Hide:Access:STOAUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOAUDIT &= NULL


Hide:Relate:STOAUDIT.Kill PROCEDURE

  CODE
  Hide:Access:STOAUDIT.Kill
  PARENT.Kill
  Relate:STOAUDIT &= NULL


Hide:Access:BOUNCER.Init PROCEDURE
  CODE
  SELF.Init(BOUNCER,GlobalErrors)
  SELF.Buffer &= bou:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(bou:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(bou:Bouncer_Job_Number_Key,'By Job Number',0)
  SELF.AddKey(bou:Bouncer_Job_Only_Key,'By Job Number',0)
  Access:BOUNCER &= SELF


Hide:Relate:BOUNCER.Init PROCEDURE
  CODE
  Hide:Access:BOUNCER.Init
  SELF.Init(Access:BOUNCER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS_ALIAS)


Hide:Access:BOUNCER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:BOUNCER &= NULL


Hide:Relate:BOUNCER.Kill PROCEDURE

  CODE
  Hide:Access:BOUNCER.Kill
  PARENT.Kill
  Relate:BOUNCER &= NULL


Hide:Access:WIPAMF.Init PROCEDURE
  CODE
  SELF.Init(WIPAMF,GlobalErrors)
  SELF.Buffer &= wim:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wim:Audit_Number_Key,'wim:Audit_Number_Key',1)
  SELF.AddKey(wim:Secondary_Key,'wim:Secondary_Key',0)
  Access:WIPAMF &= SELF


Hide:Relate:WIPAMF.Init PROCEDURE
  CODE
  Hide:Access:WIPAMF.Init
  SELF.Init(Access:WIPAMF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:WIPAUI,RI:CASCADE,RI:CASCADE,wia:Audit_Number_Key)
  SELF.AddRelationLink(wim:Audit_Number,wia:Audit_Number)


Hide:Access:WIPAMF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WIPAMF &= NULL


Hide:Relate:WIPAMF.Kill PROCEDURE

  CODE
  Hide:Access:WIPAMF.Kill
  PARENT.Kill
  Relate:WIPAMF &= NULL


Hide:Access:TEAMS.Init PROCEDURE
  CODE
  SELF.Init(TEAMS,GlobalErrors)
  SELF.Buffer &= tea:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tea:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(tea:Team_Key,'By Team',0)
  SELF.AddKey(tea:KeyTraceAccount_number,'tea:KeyTraceAccount_number',0)
  Access:TEAMS &= SELF


Hide:Relate:TEAMS.Init PROCEDURE
  CODE
  Hide:Access:TEAMS.Init
  SELF.Init(Access:TEAMS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS,RI:CASCADE,RI:RESTRICT,use:Team_Surname)
  SELF.AddRelationLink(tea:Team,use:Team)


Hide:Access:TEAMS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TEAMS &= NULL


Hide:Relate:TEAMS.Kill PROCEDURE

  CODE
  Hide:Access:TEAMS.Kill
  PARENT.Kill
  Relate:TEAMS &= NULL


Hide:Access:WIPAUI.Init PROCEDURE
  CODE
  SELF.Init(WIPAUI,GlobalErrors)
  SELF.Buffer &= wia:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wia:Internal_No_Key,'wia:Internal_No_Key',1)
  SELF.AddKey(wia:Audit_Number_Key,'wia:Audit_Number_Key',0)
  SELF.AddKey(wia:Ref_Number_Key,'wia:Ref_Number_Key',0)
  SELF.AddKey(wia:Exch_Browse_Key,'wia:Exch_Browse_Key',0)
  SELF.AddKey(wia:AuditIMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(wia:Locate_IMEI_Key,'wia:Locate_IMEI_Key',0)
  SELF.AddKey(wia:Main_Browse_Key,'wia:Main_Browse_Key',0)
  SELF.AddKey(wia:AuditRefNumberKey,'By Ref Number',0)
  SELF.AddKey(wia:AuditStatusRefNoKey,'By Ref Number',0)
  Access:WIPAUI &= SELF


Hide:Relate:WIPAUI.Init PROCEDURE
  CODE
  Hide:Access:WIPAUI.Init
  SELF.Init(Access:WIPAUI,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:WIPAMF)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:WIPAUI.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WIPAUI &= NULL


Hide:Access:WIPAUI.PrimeFields PROCEDURE

  CODE
  wia:Confirmed = 0
  wia:IsExchange = 0
  PARENT.PrimeFields


Hide:Relate:WIPAUI.Kill PROCEDURE

  CODE
  Hide:Access:WIPAUI.Kill
  PARENT.Kill
  Relate:WIPAUI &= NULL


Hide:Access:MODELCOL.Init PROCEDURE
  CODE
  SELF.Init(MODELCOL,GlobalErrors)
  SELF.Buffer &= moc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(moc:Record_Number_Key,'moc:Record_Number_Key',1)
  SELF.AddKey(moc:Colour_Key,'By Colour',0)
  Access:MODELCOL &= SELF


Hide:Relate:MODELCOL.Init PROCEDURE
  CODE
  Hide:Access:MODELCOL.Init
  SELF.Init(Access:MODELCOL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODELCOL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODELCOL &= NULL


Hide:Relate:MODELCOL.Kill PROCEDURE

  CODE
  Hide:Access:MODELCOL.Kill
  PARENT.Kill
  Relate:MODELCOL &= NULL


Hide:Access:CONTHIST.Init PROCEDURE
  CODE
  SELF.Init(CONTHIST,GlobalErrors)
  SELF.Buffer &= cht:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cht:Ref_Number_Key,'By Date',0)
  SELF.AddKey(cht:Action_Key,'By Action',0)
  SELF.AddKey(cht:User_Key,'By User',0)
  SELF.AddKey(cht:Record_Number_Key,'cht:Record_Number_Key',1)
  SELF.AddKey(cht:KeyRefSticky,'cht:KeyRefSticky',0)
  Access:CONTHIST &= SELF


Hide:Relate:CONTHIST.Init PROCEDURE
  CODE
  Hide:Access:CONTHIST.Init
  SELF.Init(Access:CONTHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:CONTHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONTHIST &= NULL


Hide:Access:CONTHIST.PrimeFields PROCEDURE

  CODE
  cht:SystemHistory = 0
  PARENT.PrimeFields


Hide:Relate:CONTHIST.Kill PROCEDURE

  CODE
  Hide:Access:CONTHIST.Kill
  PARENT.Kill
  Relate:CONTHIST &= NULL


Hide:Access:JOBSTAMP.Init PROCEDURE
  CODE
  SELF.Init(JOBSTAMP,GlobalErrors)
  SELF.Buffer &= jos:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jos:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jos:JOBSRefNumberKey,'By RefNumber',0)
  SELF.AddKey(jos:DateTimeKey,'By Date Time',0)
  Access:JOBSTAMP &= SELF


Hide:Relate:JOBSTAMP.Init PROCEDURE
  CODE
  Hide:Access:JOBSTAMP.Init
  SELF.Init(Access:JOBSTAMP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSTAMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSTAMP &= NULL


Hide:Relate:JOBSTAMP.Kill PROCEDURE

  CODE
  Hide:Access:JOBSTAMP.Kill
  PARENT.Kill
  Relate:JOBSTAMP &= NULL

