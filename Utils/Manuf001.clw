

   MEMBER('manufactfix.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('MANUF001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

Count                LONG
tmp:long             LONG
window               WINDOW('Inactive Manufacturer Fix'),AT(,,300,130),FONT('Tahoma',8,,FONT:regular),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(12,8,276,114),USE(?Panel1),FILL(COLOR:White)
                       STRING('This program will update Manufacturers'),AT(24,28),USE(?String1),TRN,FONT(,,,,CHARSET:ANSI)
                       STRING('so that those marked inactive under the old system'),AT(24,38),USE(?String2),TRN
                       STRING('will be marked inactive under the new system.'),AT(24,48),USE(?String3),TRN
                       STRING('This only needs to be run once following update to version 13.2.5.09'),AT(24,68),USE(?String4),TRN
                       BUTTON('Run'),AT(27,95,56,16),USE(?Button2)
                       BUTTON('EXIT'),AT(191,95,56,16),USE(?Button1),STD(STD:Close)
                       STRING('Count:'),AT(99,98),USE(?String5),TRN
                       STRING(@n-5),AT(128,98),USE(Count),TRN,LEFT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      Count = Records(Manufact)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button2
      Count = Records(Manufact)
      
      Access:manufact.clearkey(man:RecordNumberKey)
      man:RecordNumber = 0
      set(man:RecordNumberKey,man:RecordNumberKey)
      Loop
      
          if access:Manufact.next() then break.
      
          Count -= 1
          if count < 0 then count = 0.
      
          display()
      
          if man:Notes[1:8] = 'INACTIVE' then
      
              Tmp:Long = len(clip(Man:notes))
              if Tmp:Long <= 9 then
                   man:notes = ''
              ELSE
                   man:Notes = left(Man:notes[9: tmp:Long])
              END
                       
              man:Inactive = 1
              Access:Manufact.update()
      
          END
      
      END !loop
      
      
      Count = 0
      Message('Complete')
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

